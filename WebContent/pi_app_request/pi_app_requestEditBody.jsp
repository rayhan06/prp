<%@page import="login.LoginDTO" %>
<%@page import="pi_app_request.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="user.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="pi_app_request_details.Pi_app_request_detailsDTO" %>
<%@ page import="pi_app_request_details.Pi_app_request_detailsDAO" %>
<%@ page import="procurement_package.Procurement_packageRepository" %>
<%@ page import="procurement_package.Procurement_packageDTO" %>
<%@ page import="office_units.OfficeUnitModel" %>
<%@ page import="procurement_goods.Procurement_goodsDAO" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="pi_package_final.Pi_package_finalDTO" %>
<%@ page import="pi_package_final.Pi_package_finalRepository" %>
<%@ page import="pi_package_final.PiPackageLotFinalDTO" %>
<%@ page import="pi_package_final.PiPackageLotFinalRepository" %>
<%@ page import="pi_package_item_map.PiPackageItemMapChildDTO" %>
<%@ page import="pi_package_item_map.PiPackageItemMapChildDAO" %>
<%@ page import="pi_app_request_package_lot_item_list.Pi_app_request_package_lot_item_listDTO" %>
<%@ page import="pi_app_request_package_lot_item_list.Pi_app_request_package_lot_item_listDAO" %>

<%

    Pi_app_requestDTO pi_app_requestDTO = new Pi_app_requestDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        pi_app_requestDTO = (Pi_app_requestDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
        ;
    }
    System.out.println("ID = " + ID);

    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    System.out.println(userDTO);
    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "ajax_add";
    } else {
        actionName = "ajax_edit";
    }

    int i = 0;


    int childTableStartingID = 1;


    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    CommonDAO.language = Language;
    CatDAO.language = Language;
    String formTitle = LM.getText(LC.PI_APP_REQUEST_ADD_PI_APP_REQUEST_ADD_FORMNAME, loginDTO);

    Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();
    List<Fiscal_yearDTO> fiscal_yearDTOS = Fiscal_yearRepository.getInstance().getFiscal_yearList();

    List<OptionDTO> procurementMethodOptionList = CatRepository.getDTOs("procurement_method")
            .stream()
            .map(e -> new OptionDTO(e.languageTextEnglish, e.languageTextEnglish, String.valueOf(e.value)))
            .collect(Collectors.toList());


    long currentOfficeUnitId = -1;

    String nameOfOffice = "";
    if (actionName.equalsIgnoreCase("ajax_edit")) {
        Office_unitsDTO unit = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(pi_app_requestDTO.officeUnitId);
        if (unit != null) {
            nameOfOffice = UtilCharacter.getDataByLanguage(Language, unit.unitNameBng, unit.unitNameEng);
        }
    } else {
        OfficeUnitOrganograms organogramsDTO = OfficeUnitOrganogramsRepository.getInstance().getById(userDTO.organogramID);
        if (organogramsDTO != null) {
            Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(organogramsDTO.office_unit_id);
            if (office_unitsDTO != null) {
                nameOfOffice = UtilCharacter.getDataByLanguage(Language, office_unitsDTO.unitNameBng, office_unitsDTO.unitNameEng);
                currentOfficeUnitId = office_unitsDTO.iD;
            }
        }
    }

    long allPackageTotalSum = 0;


%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=pi_app_requestDTO.iD%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.PI_APP_REQUEST_ADD_FISCALYEARID, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>

                                        <div class="col-md-9">

                                            <select class='form-control' name='fiscalYearId' id='fiscalYearId'
                                                    tag='pb_html'>
                                                <%
                                                    Fiscal_yearDTO cur_fis_dto = fiscal_yearDAO.getFiscalYearBYDateLong(System.currentTimeMillis());

                                                    String fiscalYearOptions = Utils.buildSelectOption(isLanguageEnglish);
                                                    StringBuilder option = new StringBuilder();
                                                    for (Fiscal_yearDTO fiscal_yearDTO : fiscal_yearDTOS) {

                                                        if (actionName.equals("ajax_edit")) {
                                                            if (pi_app_requestDTO.fiscalYearId == fiscal_yearDTO.id) {
                                                                option.append("<option value = '").append(fiscal_yearDTO.id).append("' selected>");
                                                            } else {
                                                                option.append("<option value = '").append(fiscal_yearDTO.id).append("'>");
                                                            }
                                                        } else {
                                                            if (cur_fis_dto.id == fiscal_yearDTO.id) {
                                                                option.append("<option value = '").append(fiscal_yearDTO.id).append("' selected>");
                                                            } else {
                                                                option.append("<option value = '").append(fiscal_yearDTO.id).append("'>");
                                                            }
                                                        }

                                                        option.append(isLanguageEnglish ? fiscal_yearDTO.nameEn : fiscal_yearDTO.nameBn).append("</option>");
                                                    }
                                                    fiscalYearOptions += option.toString();
                                                %>
                                                <%=fiscalYearOptions%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.PI_APP_REQUEST_ADD_OFFICEUNITID, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>

                                        <div class="col-8 col-md-6">
                                            <input type="hidden" name='officeUnitId'
                                                   id='office_units_id_input' value="">
                                            <button type="button"
                                                    class="btn btn-secondary form-control shadow btn-border-radius"
                                                    disabled id="office_units_id_text"></button>
                                        </div>
                                        <div class="col-4 col-md-3 text-right">
                                            <button type="button"
                                                    class="btn btn-primary btn-block shadow btn-border-radius"
                                                    id="office_units_id_modal_button"
                                                    onclick="officeModalButtonClicked();">
                                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5">
                    <div class="d-flex justify-content-md-between flex-column flex-md-row align-items-md-end">
                        <div>
                            <h5 class="table-title">
                                <%=UtilCharacter.getDataByLanguage(Language, "প্যাকেজ লটের লিস্ট", "Package Lot List")%>
                            </h5>
                        </div>
                        <div class="d-flex align-items-center text-nowrap mb-2">
                            <%--                            <button type="button"--%>
                            <%--                                    class="btn btn-sm btn-primary shadow btn-border-radius"--%>
                            <%--                                    id="fiscal_year_select_button"--%>
                            <%--                                    onclick="fiscalYearClicked()">--%>
                            <%--                                <%=UtilCharacter.getDataByLanguage(Language, "পূর্বের অর্থবছর থেকে তালিকা প্রস্তুত",--%>
                            <%--                                        "Prepare List From Previous Fiscal Year")%>--%>
                            <%--                            </button>--%>
                            <button type="button"
                                    class="btn btn-sm btn-primary shadow btn-border-radius mx-2"
                                    id="package_select_button"
                                    onclick="openPackageLotModal();">
                                <%=UtilCharacter.getDataByLanguage(Language, "প্যাকেজ নির্বাচন", "Package Select")%>
                            </button>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="tableData" class="table table-bordered table-striped">
                            <thead class="text-nowrap">
                            <tr>
                                <th><%=(isLanguageEnglish ? "Serial Number" : "ক্রমিক নং")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Package No" : "প্যাকেজ নং")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Description of Procurement Package Goods" : "প্রকিউরমেন্ট প্যাকেজের মালামালের বিবরণ")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Quantity/ Items" : "পরিমান/আইটেম")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Unit Price" : "একক দাম")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Estimated Cost in Tk" : "আনুমানিক খরচ (টাকায়)")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Procurement Method and Type" : "ক্রয় পদ্ধতি এবং প্রকার")%>
                                </th>
                                <th></th>
                                <th><%=(isLanguageEnglish ? "Contract Approving Authority" : "চুক্তি অনুমোদনকারী কর্তৃপক্ষ")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Source of Fund" : "তহবিলের উৎস")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Time Code for Process" : "প্রক্রিয়ার জন্য সময় কোড")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Invite/ Advertisement Tender" : "আমন্ত্রণ/বিজ্ঞাপন দরপত্র")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Tender Opening" : "টেন্ডার ওপেনিং")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Tender Evaluation" : "দরপত্র মূল্যায়ন")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Approval to Award" : "পুরস্কারের অনুমোদন")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Notification of Award" : "পুরস্কারের বিজ্ঞপ্তি")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Signing of Contract" : "চুক্তি স্বাক্ষর")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Total Time to Contract Signature" : "চুক্তি স্বাক্ষরের মোট সময়")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Time for Completion of Contract" : "চুক্তি সমাপ্তির সময়")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Remark" : "রিমার্ক")%>
                                </th>
                                <th><%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ITEM_REMOVE, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody id='package-body'>
                            <%
                                if (actionName.equals("ajax_edit")) {
                                    String newLine = "<br>";
                                    List<Pi_app_request_detailsDTO> detailsDTOS = Pi_app_request_detailsDAO
                                            .getInstance().getByAppId(pi_app_requestDTO.iD)
                                            .stream().sorted(Comparator.comparingLong(dto -> dto.serialNum))
                                            .collect(Collectors.toList());
                                    for (Pi_app_request_detailsDTO model : detailsDTOS) {
                                        Pi_package_finalDTO packageModel = Pi_package_finalRepository.getInstance()
                                                .getPi_package_finalDTOByiD(model.packageFinalId);
                                        PiPackageLotFinalDTO lotModel = PiPackageLotFinalRepository.getInstance()
                                                .getPiPackageLotFinalDTOByiD(model.lotFinalId);
                                        String packageNo = "";
                                        if (packageModel != null)
                                            packageNo = UtilCharacter.getDataByLanguage(Language, packageModel.packageNumberBn, packageModel.packageNumberEn);
                                        if (lotModel != null) packageNo += " (" +
                                                UtilCharacter.getDataByLanguage(Language, lotModel.lotNumberBn, lotModel.lotNumberEn) + ")";
                                        String description = "";
                                        if (packageModel != null)
                                            description = UtilCharacter.getDataByLanguage(Language, packageModel.packageNameBn, packageModel.packageNameEn);
                                        if (lotModel != null)
                                            description = UtilCharacter.getDataByLanguage(Language, lotModel.lotNameBn, lotModel.lotNameEn);

                                        // QUANTITY / ITEMS CELL DATA GENERATION
                                        List<Pi_app_request_package_lot_item_listDTO> models = (List<Pi_app_request_package_lot_item_listDTO>)
                                                Pi_app_request_package_lot_item_listDAO.getInstance()
                                                        .getDTOsByParent("app_details_id", model.iD);
                                        HashMap<Long, Long> officeItemCount = new HashMap<Long, Long>();
                                        for (Pi_app_request_package_lot_item_listDTO quantityModel : models) {
                                            if (officeItemCount.containsKey(quantityModel.officeUnitId)) {
                                                long prevCount = officeItemCount.get(quantityModel.officeUnitId);
                                                officeItemCount.put(quantityModel.officeUnitId, prevCount + 1);
                                            } else {
                                                officeItemCount.put(quantityModel.officeUnitId, 1l);
                                            }
                                        }

                                        StringBuilder quantity = new StringBuilder();

                                        for (Map.Entry<Long, Long> entity : officeItemCount.entrySet()) {
                                            quantity.append(Office_unitsRepository.getInstance().geText(Language, entity.getKey()))
                                                    .append(newLine)
                                                    .append(Utils.getDigits(entity.getValue(), Language))
                                                    .append(UtilCharacter.getDataByLanguage(Language, " টি", " Piece"))
                                                    .append(newLine);
                                        }
                                        String quantityText = quantity.toString();


                                        // GENERATE DATA OF EST COST CELL
                                        officeItemCount = new HashMap<Long, Long>();
                                        long grandTotal = 0;
                                        for (Pi_app_request_package_lot_item_listDTO estCostModel : models) {
                                            long estimatedTotalPrice = Pi_app_request_package_lot_item_listDAO.getInstance()
                                                    .getTotalPriceByAppStatus(estCostModel, pi_app_requestDTO.status);
                                            if (officeItemCount.containsKey(estCostModel.officeUnitId)) {
                                                long prevCount = officeItemCount.get(estCostModel.officeUnitId);
                                                officeItemCount.put(estCostModel.officeUnitId, prevCount +
                                                        estimatedTotalPrice);
                                            } else {
                                                officeItemCount.put(estCostModel.officeUnitId, estimatedTotalPrice);
                                            }
                                            grandTotal += estimatedTotalPrice;
                                        }
                                        allPackageTotalSum += grandTotal;

                                        StringBuilder estCost = new StringBuilder();

                                        for (Map.Entry<Long, Long> entity : officeItemCount.entrySet()) {
                                            estCost.append(Utils.getDigits(entity.getValue(), Language)).append(newLine);
                                        }
                                        estCost.append("-------------------").append(newLine);
                                        estCost.append(Utils.getDigits(grandTotal, Language));
                                        String estCostText = estCost.toString();
                            %>
                            <tr class="text-center">
                                <input type="hidden" id="pi_app_details_id_<%=childTableStartingID%>"
                                       name='piAppDetails.iD' value="<%=model.iD%>">
                                <input type="hidden" id="pi_app_package_item_map_id_<%=childTableStartingID%>"
                                       name='piAppDetails.piPackageItemMapId' value="<%=model.piPackageItemMapId%>">
                                <input type="hidden" id="pi_app_package_id_<%=childTableStartingID%>"
                                       name='piAppDetails.packageFinalId' value="<%=model.packageFinalId%>">
                                <input type="hidden" id="pi_app_lot_id_<%=childTableStartingID%>"
                                       name='piAppDetails.lotFinalId' value="<%=model.lotFinalId%>">
                                <input type="hidden" id="total_quantity_<%=childTableStartingID%>"
                                       name='piAppDetails.quantity' value="<%=model.quantity%>">
                                <input type="hidden" id="pi_app_details_est_cost_<%=childTableStartingID%>"
                                       name='piAppDetails.estCost' value="<%=model.estCost%>">
                                <input type="hidden" id="pi_app_details_total_price_<%=childTableStartingID%>"
                                       name='piAppDetails.totalPrice' value="<%=grandTotal%>">
                                <td>
                                    <input class="form-control w-auto" type="number" min="0"
                                           id="pi_app_details_serial_number_<%=childTableStartingID%>"
                                           name='piAppDetails.serialNumber' value="<%=model.serialNum%>">
                                </td>
                                <td id="package_no_<%=childTableStartingID%>" class="highlight-modal-td"
                                    onclick="openItemListModal(this)"><%=packageNo%>
                                </td>
                                <td id='description_<%=childTableStartingID%>'>
                                    <%=description%>
                                </td>
                                <td id='total_quantity_text_<%=childTableStartingID%>'>
                                    <%=quantityText%>
                                </td>
                                <td id="unit_price_<%=childTableStartingID%>">
                                    <input type="text" class='form-control w-auto' name="piAppDetails.packageUnitPrice"
                                           value="<%=model.packageUnitPrice%>">
                                </td>
                                <td id="total_price_<%=childTableStartingID%>" class="highlight-modal-td"
                                    onclick="openItemListModal(this)">
                                    <%=estCostText%>
                                </td>
                                <td>
                                    <select multiple="multiple" class='form-control w-auto'
                                            name='piAppDetails.procurementMethod'
                                            onchange="procurementMethodChange(this)"
                                            value="<%=model.procurementMethodId%>"
                                            id='procurementMethod_<%=childTableStartingID%>'
                                            tag='pb_html'>
                                        <%
                                            Options = Utils.buildOptionsMultipleSelection(procurementMethodOptionList, Language, model.procurementMethodId);
                                        %>
                                        <%=Options%>
                                    </select>
                                </td>
                                <td>
                                    <input type='checkbox' name='copyProcurement'
                                           id='copyProcurement_<%=childTableStartingID%>'
                                           class="form-control-sm "
                                           oninput="copyProcurementMethod(this)" tag='pb_html'/>
                                </td>
                                <td>
                                    <select class='form-control w-auto' name='piAppDetails.approvalAuthority'
                                            onchange="approvalAuthorityChange(this)"
                                            id='approvalAuthority_<%=childTableStartingID%>'
                                            tag='pb_html'>
                                        <%
                                            Options = CatRepository.getInstance().buildOptionsWithoutSelectOption
                                                    ("pi_contract_approving_authority",
                                                            Language, Math.toIntExact(model.contractAppAuthId));
                                        %>
                                        <%=Options%>
                                    </select>
                                </td>
                                <td>
                                    <select class='form-control w-auto' name='piAppDetails.sourceFund'
                                            onchange="sourceOfFundChange(this)"
                                            id='sourceFund_<%=childTableStartingID%>' tag='pb_html'>
                                        <%
                                            Options = CatRepository.getInstance().buildOptionsWithoutSelectOption
                                                    ("pi_source_of_fund", Language, Math.toIntExact(model.sourceFundId));
                                        %>
                                        <%=Options%>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto'
                                           name="piAppDetails.timeCodeForProcess"
                                           value="<%=model.timeCodeForProcess%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto'
                                           name="piAppDetails.inviteAdvertiseTender"
                                           value="<%=model.inviteAdvertiseTender%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto' name="piAppDetails.tenderOpening"
                                           value="<%=model.tenderOpening%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto' name="piAppDetails.tenderEvaluation"
                                           value="<%=model.tenderEvaluation%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto' name="piAppDetails.approvalToAward"
                                           value="<%=model.approvalToAward%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto'
                                           name="piAppDetails.notificationOfAward"
                                           value="<%=model.notificationOfAward%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto' name="piAppDetails.signingOfContract"
                                           value="<%=model.signingOfContract%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto'
                                           name="piAppDetails.totalTimeToContractSignature"
                                           value="<%=model.totalTimeToContractSignature%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto'
                                           name="piAppDetails.timeForCompletionOfContract"
                                           value="<%=model.timeForCompletionOfContract%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto' name="piAppDetails.remark"
                                           value="<%=model.remark%>">
                                </td>
                                <td>
                                    <button type="button"
                                            class="btn btn-sm remove-btn shadow ml-2 pl-4"
                                            style="color: #ff6a6a"
                                            onclick="removePackageLot(this);">
                                        <i class="fa fa-trash"></i>&nbsp;
                                    </button>
                                </td>
                            </tr>
                            <%
                                        childTableStartingID++;
                                    }
                                }
                            %>
                            </tbody>

                            <%Pi_app_request_detailsDTO template = new Pi_app_request_detailsDTO();%>
                            <tr class="text-center hiddenTr template-package-lot-row">
                                <input type="hidden" id="pi_app_details_id" value="<%=template.iD%>">
                                <input type="hidden" id="pi_app_package_item_map_id"
                                       value="<%=template.piPackageItemMapId%>">
                                <input type="hidden" id="pi_app_package_id" value="<%=template.packageFinalId%>">
                                <input type="hidden" id="pi_app_lot_id" value="<%=template.lotFinalId%>">
                                <input type="hidden" id="total_quantity" value="<%=template.quantity%>">
                                <input type="hidden" id="pi_app_details_est_cost" value="<%=template.estCost%>">
                                <input type="hidden" id="pi_app_details_total_price" value="0">
                                <td>
                                    <input class="form-control w-auto" type="number" min="0"
                                           id="pi_app_details_serial_number" value="<%=template.serialNum%>">
                                </td>
                                <td id="package_no">
                                </td>
                                <td id='description'>
                                </td>
                                <td id='total_quantity_text'>
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto'
                                           id="unit_price" value="<%=template.packageUnitPrice%>">
                                </td>
                                <td id="total_price">
                                </td>
                                <td>
                                    <select multiple="multiple" class='form-control w-auto'
                                            onchange="procurementMethodChange(this)"
                                            value="<%=template.procurementMethodId%>"
                                            id='procurementMethod'
                                            tag='pb_html'>
                                        <%
                                            Options = Utils.buildOptionsMultipleSelection(procurementMethodOptionList, Language, template.procurementMethodId);
                                        %>
                                        <%=Options%>
                                    </select>
                                </td>
                                <td>
                                    <input type='checkbox'
                                           id='copyProcurement'
                                           class="form-control-sm "
                                           oninput="copyProcurementMethod(this)" tag='pb_html'/>
                                </td>
                                <td>
                                    <select class='form-control w-auto'
                                            onchange="approvalAuthorityChange(this)"
                                            id='approvalAuthority'
                                            tag='pb_html'>
                                        <%
                                            Options = CatRepository.getInstance().buildOptionsWithoutSelectOption
                                                    ("pi_contract_approving_authority",
                                                            Language, Math.toIntExact(template.contractAppAuthId));
                                        %>
                                        <%=Options%>
                                    </select>
                                </td>
                                <td>
                                    <select class='form-control w-auto' name='piAppDetails.sourceFund'
                                            onchange="sourceOfFundChange(this)"
                                            id='sourceFund' tag='pb_html'>
                                        <%
                                            Options = CatRepository.getInstance().buildOptionsWithoutSelectOption
                                                    ("pi_source_of_fund", Language, Math.toIntExact(template.sourceFundId));
                                        %>
                                        <%=Options%>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto' id="timeCodeForProcess"
                                           value="<%=template.timeCodeForProcess%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto' id="inviteAdvertiseTender"
                                           value="<%=template.inviteAdvertiseTender%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto' id="tenderOpening"
                                           value="<%=template.tenderOpening%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto' id="tenderEvaluation"
                                           value="<%=template.tenderEvaluation%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto' id="approvalToAward"
                                           value="<%=template.approvalToAward%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto' id="notificationOfAward"
                                           value="<%=template.notificationOfAward%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto' id="signingOfContract"
                                           value="<%=template.signingOfContract%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto' id="totalTimeToContractSignature"
                                           value="<%=template.totalTimeToContractSignature%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto' id="timeForCompletionOfContract"
                                           value="<%=template.timeForCompletionOfContract%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto' id="remark"
                                           value="<%=template.remark%>">
                                </td>
                                <td>
                                    <button type="button"
                                            class="btn btn-sm remove-btn shadow ml-2 pl-4"
                                            style="color: #ff6a6a"
                                            onclick="removePackageLot(this);">
                                        <i class="fa fa-trash"></i>&nbsp;
                                    </button>
                                </td>
                            </tr>
                        </table>
                        <div>
                            <h5 class="table-title">
                                <%=UtilCharacter.getDataByLanguage(Language, "সর্বমোট ব্যয়: ", "Grand Total: ")%>
                                <%
                                    String allPackageTotalSumText = Utils.getDigits(allPackageTotalSum, Language);
                                %>
                                <span id="grand_total"><%=allPackageTotalSumText%></span>
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="mt-3">
                    <div class="form-actions text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                            <%=LM.getText(LC.PI_APP_REQUEST_ADD_PI_APP_REQUEST_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                type="button" onclick="submitForm()">
                            <%=LM.getText(LC.PI_APP_REQUEST_ADD_PI_APP_REQUEST_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<jsp:include page="pi_app_final_package_lotModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<jsp:include page="fiscalYearModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<jsp:include page="pi_app_package_lot_item_listModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<%@ include file="../recruitment_seat_plan/manualLoader.jsp" %>

<style>
    .hiddenTr {
        display: none;
    }
    .highlight-modal-td {
        cursor: pointer;
        color: #0c5460;
    }
</style>

<script type="text/javascript">
    let actionName = '<%=actionName%>';
    <%--let language = '<%=Language%>';--%>
    let child_table_extra_id = <%=childTableStartingID%>;

    let tableBody = $("#itemTBody");
    const appForm = $("#bigform");
    let lang = '<%=Language%>';
    let itemIds = [];
    let templateBody;
    let templateTrClass = 'template-package-lot-row';
    const fullPageLoader = $('#full-page-loader');


    packageLotModal = {
        passModalData: function (packageLotList) {
            for (let row of packageLotList) {
                setDataToTable(row);
            }
        }
    }

    function setName(clonedRow) {
        clonedRow.querySelector('#pi_app_details_id').name = 'piAppDetails.iD';
        clonedRow.querySelector('#pi_app_package_item_map_id').name = 'piAppDetails.piPackageItemMapId';
        clonedRow.querySelector('#pi_app_package_id').name = 'piAppDetails.packageFinalId';
        clonedRow.querySelector('#pi_app_lot_id').name = 'piAppDetails.lotFinalId';
        clonedRow.querySelector('#total_quantity').name = 'piAppDetails.quantity';
        clonedRow.querySelector('#pi_app_details_est_cost').name = 'piAppDetails.estCost';
        clonedRow.querySelector('#pi_app_details_total_price').name = 'piAppDetails.totalPrice';
        clonedRow.querySelector('#pi_app_details_serial_number').name = 'piAppDetails.serialNumber';
        clonedRow.querySelector('#unit_price').name = 'piAppDetails.packageUnitPrice';
        clonedRow.querySelector('#procurementMethod').name = 'piAppDetails.procurementMethod';
        clonedRow.querySelector('#approvalAuthority').name = 'piAppDetails.approvalAuthority';
        clonedRow.querySelector('#sourceFund').name = 'piAppDetails.sourceFund';
        clonedRow.querySelector('#timeCodeForProcess').name = 'piAppDetails.timeCodeForProcess';
        clonedRow.querySelector('#inviteAdvertiseTender').name = 'piAppDetails.inviteAdvertiseTender';
        clonedRow.querySelector('#tenderOpening').name = 'piAppDetails.tenderOpening';
        clonedRow.querySelector('#tenderEvaluation').name = 'piAppDetails.tenderEvaluation';
        clonedRow.querySelector('#approvalToAward').name = 'piAppDetails.approvalToAward';
        clonedRow.querySelector('#notificationOfAward').name = 'piAppDetails.notificationOfAward';
        clonedRow.querySelector('#signingOfContract').name = 'piAppDetails.signingOfContract';
        clonedRow.querySelector('#totalTimeToContractSignature').name = 'piAppDetails.totalTimeToContractSignature';
        clonedRow.querySelector('#timeForCompletionOfContract').name = 'piAppDetails.timeForCompletionOfContract';
        clonedRow.querySelector('#remark').name = 'piAppDetails.remark';
    }

    function setValue(clonedRow, row) {
        clonedRow.querySelector('#pi_app_package_item_map_id').value = row["package-item-map-id"];
        clonedRow.querySelector('#pi_app_details_total_price').value = row["total-price"];
        clonedRow.querySelector('#pi_app_details_serial_number').value = child_table_extra_id;
        clonedRow.querySelector('#pi_app_package_id').value = row['package-id'];
        clonedRow.querySelector('#pi_app_lot_id').value = row['lot-id'];
        clonedRow.querySelector('#total_quantity').value = getValueByLanguage(language, row["quantity-bn"], row["quantity-en"]);
        clonedRow.querySelector('#total_quantity_text').innerText = getValueByLanguage(language, row["quantity-bn"], row["quantity-en"]);
        clonedRow.querySelector('#pi_app_details_est_cost').value = row['est-cost'];
        clonedRow.querySelector('#total_price').innerText = row['est-cost-text'];
        clonedRow.querySelector('#package_no').innerText = row["package-no"];
        clonedRow.querySelector('#description').innerText = row["description"];
        clonedRow.querySelector('#procurementMethod').id = 'procurementMethod_' + child_table_extra_id;
    }

    function removeHiddenClass(element) {
        element.classList.remove('hiddenTr');
    }

    function removeTemplateClass(tableRow) {
        tableRow.classList.remove(templateTrClass);
    }

    function preprocessRow(tableRow) {
        removeHiddenClass(tableRow);
        removeTemplateClass(tableRow);
    }

    function convertProcurementMethodToSelect2() {
        select2MultiSelector('#procurementMethod_' + child_table_extra_id, '<%=Language%>');
    }

    function postProcessRow() {
        convertProcurementMethodToSelect2();
        calculateGrandTotal();
        child_table_extra_id++;
    }

    function setDataToTable(row) {
        let tableBody = document.querySelector('#package-body');
        let clonedNode = document.querySelector('.template-package-lot-row').cloneNode(true);
        preprocessRow(clonedNode);
        setName(clonedNode);
        setValue(clonedNode, row);
        tableBody.append(clonedNode);
        postProcessRow();
    }

    function calculateGrandTotal() {
        let allEstCosts = document.getElementsByName('piAppDetails.totalPrice');
        let total = 0;
        allEstCosts.forEach(input => {
            if (input.value && input.value !== "") total += parseInt(input.value);
        });
        fillGrandTotal(total);
    }

    function fillGrandTotal(total) {
        let grandTotal = document.getElementById('grand_total');
        grandTotal.innerText = getDigitByLanguage(language, total);
    }

    function openPackageLotModal() {
        loadPackageLotList();
        $('#pi_app_package_lot_modal').modal();
    }

    function removePackageLot(element) {
        element.parentNode.parentNode.remove();
        calculateGrandTotal();
    }

    function openItemListModal(element) {
        let appDetailsIdElement = element.parentNode.querySelector("input[name='piAppDetails.iD']");
        let appDetailsId = appDetailsIdElement ? appDetailsIdElement.value : '';
        let approveLevel = 3;
        loadItemListOfPackageLot(appDetailsId, approveLevel, true);
        $('#pi_app_package_lot_item_list_modal').modal();
    }

    function copyProcurementMethod(element) {
        let el = document.getElementById(element.id);
        if (!el.checked) {
            return;
        }
        let selectedIdIndex = element.id.split('_')[1];
        let prevIndex = getPreviousIndex(selectedIdIndex);
        if (prevIndex < 0) {
            return;
        }

        let prevRowValue = $("#procurementMethod_" + prevIndex).val();
        if (prevRowValue) {
            let selectorItem = $("#procurementMethod_" + selectedIdIndex);
            selectorItem.val(prevRowValue).trigger('change');
        }
    }

    function getNextIndex(x) {
        x = x / 1;
        let tablename = 'package-body';
        let items = Object.values(document.getElementById(tablename).childNodes).filter(el => el.nodeType === Node.ELEMENT_NODE);
        if (items.length < 2) {
            return -1;
        }

        for (let i = 0; i < items.length; i++) {
            let currentIndex = items[i].id.split('_')[1] / 1;
            if (currentIndex === x && i + 1 < items.length) {
                return items[i + 1].id.split('_')[1] / 1;
            }
        }

        return -1;
    }

    function arraysEqual(a1, a2) {
        /* WARNING: arrays must not contain {objects} or behavior may be undefined */
        return JSON.stringify(a1) === JSON.stringify(a2);
    }

    function procurementMethodChange(element) {
        let selectedIdIndex = element.id.split('_')[1];
        let currentSelectBox = document.getElementById("copyProcurement_" + selectedIdIndex);
        let prevIndex = getPreviousIndex(selectedIdIndex);
        let curRowValue = $("#procurementMethod_" + selectedIdIndex).val();


        if (currentSelectBox && currentSelectBox.checked && prevIndex >= 0) {
            let prevRowValue = $("#procurementMethod_" + prevIndex).val();
            if (prevRowValue && curRowValue && !arraysEqual(prevRowValue, curRowValue)) {
                currentSelectBox.checked = false;
            }
        }


        let nextIndex = getNextIndex(selectedIdIndex);
        if (nextIndex < 0) {
            return;
        }
        let checkBoxChecked = document.getElementById("copyProcurement_" + nextIndex);
        if (!checkBoxChecked) {
            return;
        }
        if (checkBoxChecked.checked) {
            let selectorItem = $("#procurementMethod_" + nextIndex);
            selectorItem.val(curRowValue).trigger('change');
        }
    }

    function approvalAuthorityChange(element) {
        let el = document.getElementById(element.id);
        let selectedIdIndex = element.id.split('_')[1];
        let currentSelectBox = document.getElementById("copyApproval_" + selectedIdIndex);
        let prevIndex = getPreviousIndex(selectedIdIndex);
        if (currentSelectBox && currentSelectBox.checked && prevIndex >= 0) {
            let prevRowValue = $("#approvalAuthority_" + prevIndex).val();
            if (prevRowValue && el.value && prevRowValue !== el.value) {
                currentSelectBox.checked = false;
            }
        }
        let nextIndex = getNextIndex(selectedIdIndex);
        if (nextIndex < 0) {
            return;
        }
        let nextCheckBoxChecked = document.getElementById("copyApproval_" + nextIndex);
        if (!nextCheckBoxChecked) {
            return;
        }
        if (nextCheckBoxChecked.checked && el.value) {
            $("#approvalAuthority_" + nextIndex).val(el.value);
            approvalAuthorityChange(document.getElementById("approvalAuthority_" + nextIndex));
        }
    }

    function sourceOfFundChange(element) {

        let el = document.getElementById(element.id);
        let selectedIdIndex = element.id.split('_')[1];
        let currentSelectBox = document.getElementById("copySourceFund_" + selectedIdIndex);
        let prevIndex = getPreviousIndex(selectedIdIndex);
        if (currentSelectBox && currentSelectBox.checked && prevIndex >= 0) {
            let prevRowValue = $("#sourceFund_" + prevIndex).val();
            if (prevRowValue && el.value && prevRowValue !== el.value) {
                currentSelectBox.checked = false;
            }
        }
        let nextIndex = getNextIndex(selectedIdIndex);
        if (nextIndex < 0) {
            return;
        }
        let nextCheckBoxChecked = document.getElementById("copySourceFund_" + nextIndex);
        if (!nextCheckBoxChecked) {
            return;
        }
        if (nextCheckBoxChecked.checked && el.value) {
            $("#sourceFund_" + nextIndex).val(el.value);
            sourceOfFundChange(document.getElementById("sourceFund_" + nextIndex));
        }
    }

    function getPreviousIndex(x) {
        x = x / 1;
        let tablename = 'package-body';
        let items = Object.values(document.getElementById(tablename).childNodes).filter(el => el.nodeType === Node.ELEMENT_NODE);
        if (items.length < 2) {
            return -1;
        }

        let prevValue = -1;

        for (let i = 0; i < items.length; i++) {
            let currentIndex = items[i].id.split('_')[1] / 1;
            if (currentIndex === x) {
                return prevValue;
            }

            prevValue = currentIndex;
        }

        return -1;

    }

    function officeValidation() {
        let flag = false;
        let officeValue = $('#office_units_id_input').val();
        if (officeValue && officeValue.length > 0) {
            flag = true;
        } else {
            let errMsg = valueByLanguage(lang, 'শাখা নির্বাচন করুন', 'Please select section');
            $('#toast_message').css('background-color', '#ff6063');
            showToastSticky(errMsg, errMsg);
        }

        return flag;
    }

    function processMultipleSelectBoxBeforeSubmit2(name) {

        $("[name='" + name + "']").each(function (i) {
            var selectedInputs = $(this).val();
            var temp = "";
            if (selectedInputs != null) {
                selectedInputs.forEach(function (value, index, array) {
                    if (index > 0) {
                        temp += ", ";
                    }
                    temp += value;
                });
            }
            if (temp.includes(',')) {
                $(this).append('<option value="' + temp + '"></option>');
            }
            $(this).val(temp);

        });
    }

    function PreprocessBeforeSubmitting() {
        processMultipleSelectBoxBeforeSubmit2("piAppDetails.procurementMethod");
        return true;
    }

    function submitForm() {
        if (PreprocessBeforeSubmitting()) {
            $.ajax({
                type: "POST",
                url: "Pi_app_requestServlet?actionType=<%=actionName%>",
                data: appForm.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        showToastSticky("সাবমিট সফল হয়েছে", "Submit Successful");
                        setTimeout(() => {
                            window.location.replace(getContextPath() + response.msg);
                        }, 1500);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        }

    }

    function init() {
        if (actionName === 'ajax_edit') {
            viewOfficeIdInInput({
                name: '<%=nameOfOffice%>',
                id: <%=pi_app_requestDTO.officeUnitId%>
            });

            for (let i = 1; i <= child_table_extra_id; i++) {
                if ($("#procurementMethod_" + i)) {
                    select2MultiSelector("#procurementMethod_" + i, '<%=Language%>');
                }

            }


        } else {
            viewOfficeIdInInput({
                name: '<%=nameOfOffice%>',
                id: <%=currentOfficeUnitId%>
            });
        }

    }

    $(document).ready(function () {
        init();
        calculateGrandTotal();
        fullPageLoader.hide();

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        $.validator.addMethod('validSelector', function (value, element) {
            return value && value !== -1 && value.toString().trim().length > 0;
        });

        let fisErr;
        if (lang.toUpperCase() === 'ENGLISH') {
            fisErr = 'Please Select Fiscal Year';
        } else {
            fisErr = 'অর্থবছর বাছাই করুন';
        }

        appForm.validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                fiscalYearId: {
                    validSelector: true,
                },
            },
            messages: {
                fiscalYearId: fisErr,
            }
        });

    });

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    // related to fiscal year modal
    function fiscalYearClicked() {
        fisSelectModalUsage = 'fisId';
        $('#search_fiscal_year_modal').modal();
    }

    let fisSelectModalUsage = 'none';
    let fisSelectModalOptionsMap = new Map([
        ['fisId', {
            itemSelectedCallback: getfisItem
        }]
    ]);

    function getfisItem(item) {
        if (item && item.id === '') {
            return;
        }

        if (officeValidation()) {
            loadPrevFiscalYearData($('#office_units_id_input').val(), item.id);
        }
    }


    // related to item modal

    function itemSelectClicked() {
        $('#search_proc_modal').modal();
    }

    necessaryCollectionForProcurementModal = {
        callBackFunction: function (item) {
            getItem(item);
        }
    };

    function exists(arr, target) {
        for (let i = 0; i < arr.length; i++) {
            if (arr[i] === target) {
                return true;
            }
        }
        return false;
    }

    function getItem(item) {
        if (!item || item.iD === '') {
            return;
        }


        let t = $("#template-items");
        tableBody.append(t.html());
        SetCheckBoxValues("itemTBody");
        let tr = tableBody.find("tr:last-child");

        tr.attr("id", "item_" + child_table_extra_id);

        tr.find("[tag='pb_html']").each(function (index) {
            let prev_id = $(this).attr('id');
            $(this).attr('id', prev_id + child_table_extra_id);
        });

        tr.find("#itemPackage_" + child_table_extra_id).val(valueByLanguage(lang, item.procurementPackageNameBn,
            item.procurementPackageNameEn));
        tr.find("#itemName_" + child_table_extra_id).val(item.circularData);
        tr.find("#itemPackageId_" + child_table_extra_id).val(item.procurementPackageId);
        tr.find("#itemTypeId_" + child_table_extra_id).val(item.procurementGoodsTypeId);
        tr.find("#itemId_" + child_table_extra_id).val(item.iD);

        select2MultiSelector("#procurementMethod_" + child_table_extra_id, '<%=Language%>');
        child_table_extra_id++;

        if (exists(itemIds, item.iD)) {
            tr.attr('id', 'blink');
        } else {
            itemIds.push(item.iD);
        }
    }


    // Related to office selector modal

    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id_input').val(selectedOffice.id);
    }

    let officeSelectModalUsage = 'none';
    let officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function valueByLanguage(lang, bnValue, enValue) {
        if (lang === 'english' || lang === 'English') {
            return enValue;
        } else {
            return bnValue;
        }
    }


    function loadPrevFiscalYearData(unitId, fiId) {
        let url = "Pi_app_requestServlet?actionType=getAppDetailsByUnitIdAndFiscalYearId&fiscalYearId="
            + fiId + "&unitId=" + unitId;
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                const response = JSON.parse(fetchedData);
                if (response && response.responseCode === 200) {
                    let itemData = JSON.parse(response.msg);
                    setFiscalYearData(itemData);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
            }
        });
    }

    // test();

    function test() {
        let url = "Pi_app_requestServlet?actionType=getItemByUnitIdAndFiscalYearId&fiscalYearId="
            + 3 + "&unitId=" + 38 + "&itemId=" + 1100;
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                const response = JSON.parse(fetchedData);
                console.log(response)
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
            }
        });
    }


</script>