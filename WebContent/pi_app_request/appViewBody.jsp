<%@ page import="login.LoginDTO" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="user.UserDTO" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="pi_app_request.Pi_app_requestDTO" %>
<%@ page import="pi_app_request.Pi_app_requestDAO" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="pi_app_request_details.Pi_app_request_detailsDTO" %>
<%@ page import="pi_app_request_details.Pi_app_request_detailsDAO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="util.UtilCharacter" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="procurement_package.Procurement_packageDTO" %>
<%@ page import="procurement_package.Procurement_packageRepository" %>
<%@ page import="procurement_goods.Procurement_goodsDTO" %>
<%@ page import="procurement_goods.Procurement_goodsRepository" %>
<%@ page import="javax.rmi.CORBA.Util" %>
<%@ page import="pb.Utils" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="procurement_goods.Procurement_goodsDAO" %>
<%@page pageEncoding="UTF-8" %>
<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    long ID = Long.parseLong(request.getParameter("ID"));
    Fiscal_yearDTO fiscalYearDTO = Fiscal_yearRepository.getInstance().getFiscal_yearDTOByid(ID);
    Pi_app_request_detailsDAO childDAO = Pi_app_request_detailsDAO.getInstance();

    List<Pi_app_requestDTO> requestDTOS = Pi_app_requestDAO.getInstance().getDataByFiscalYear
            (Arrays.asList(ID), false);
    List<Pi_app_request_detailsDTO> detailsDTOS = childDAO.getByAppIds
            (requestDTOS.stream().map(i -> i.iD).collect(Collectors.toList()))
            .stream().filter(i -> childDAO.allowedValue().contains(i.isSelected)).collect(Collectors.toList());
    String formName = UtilCharacter.getDataByLanguage(Language, "বার্ষিক ক্রয় পরিকল্পনা", "ANNUAL PURCHASE PLAN");
    String value = "";
    int rowsPerPage = 10;
    int totalRowCount = 1;
    Procurement_goodsDAO procurement_goodsDAO = Procurement_goodsDAO.getInstance();
%>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    #to-print-div * {
        font-size: 12px;
    }

    #to-print-div h1 {
        font-size: 16px;
        font-weight: bold;
    }

    #to-print-div h2 {
        font-size: 14px;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: 2in .5in .4in .5in;
        background: white;
        margin-bottom: 10px;
    }

    .page {
        width: 210mm;
        padding: .4in;
        background: white;
        margin-bottom: 10px;
    }

    .table-bordered > :not(caption) > * {
        border-color: black;
    }

    .table-bordered > :not(caption) > * > * {
        border-color: black;
    }

    .table-bordered td,
    .table-bordered th {
        padding: 5px;
    }
</style>

<div class="kt-content" id="kt_content">
    <div class="row">
        <div class="kt-portlet">
            <div class="kt-portlet__body page-bg" id="bill-div">
                <div class="kt-subheader__main ml-4">
                    <label class="h2 kt-subheader__title" style="color: #00a1d4;">
                        <%=formName%>
                    </label>
                </div>

                <div class="ml-auto m-3" id = 'printer'>
                    <button type="button" class="btn"
                            onclick="printDiv('kt_content')">
                        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn" id='download-pdf'
                            onclick="downloadDiv()">
                        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                </div>

                <div style="margin: auto;">
                    <div class="container" id="to-print-div">
                        <section class="page shadow" data-size="A4">
                            <div>
                                <h5 class="table-title">
                                    <%=UtilCharacter.getDataByLanguage(Language, "অর্থবছর: " + fiscalYearDTO.nameBn,
                                            "Financial Year: " + fiscalYearDTO.nameEn)%>
                                </h5>
                            </div>
                            <table  class=" table-bordered w-100">
                                <thead >
                                <tr>
                                    <th></th>
                                    <th><%=UtilCharacter.getDataByLanguage(Language, "প্যাকেজ ", "Package No")%></th>
                                    <th></th>
                                    <th><%=UtilCharacter.getDataByLanguage(Language, "পণ্য ", "Item No")%></th>
                                    <th><%=UtilCharacter.getDataByLanguage(Language, "পরিমান", "Quantity")%></th>
                                    <th><%=(isLanguageEnglish ? "Unit Price in Tk" : "একক দাম (টাকায়)")%></th>
                                    <th><%=(isLanguageEnglish ? "Estimated Cost in Tk" : "আনুমানিক খরচ (টাকায়)")%></th>
                                    <th><%=(isLanguageEnglish ? "Procurement Method and Type" : "ক্রয় পদ্ধতি এবং প্রকার")%></th>
                                    <th><%=(isLanguageEnglish ? "Contract Approving Authority" : "চুক্তি অনুমোদনকারী কর্তৃপক্ষ")%></th>
                                    <th><%=(isLanguageEnglish ? "Source of Fund" : "তহবিলের উৎস")%></th>

                                </tr>
                                </thead>
                                <tbody>

                                <%
                                    for(Pi_app_requestDTO requestDTO: requestDTOS){
                                        Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().
                                                getOffice_unitsDTOByID(requestDTO.officeUnitId);

                                        if(office_unitsDTO != null){

                                %>
                                    <%@include file="appViewBodyRowRelated.jsp" %>
                                    <tr>
                                        <td colspan = "10" style="text-align: center;">
                                            <%=UtilCharacter.getDataByLanguage(Language, office_unitsDTO.unitNameBng,
                                                    office_unitsDTO.unitNameEng)%>
                                        </td>
                                        <%
                                            totalRowCount++;
                                        %>
                                    </tr>
                                <%

                                    }

                                    List<Pi_app_request_detailsDTO> currentDetails = detailsDTOS.stream()
                                            .filter(i -> i.app_id == requestDTO.iD).collect(Collectors.toList());
                                    List<Long> packageIds = currentDetails.stream().map(i -> i.packageId).distinct().collect(Collectors.toList());
                                    List<Procurement_packageDTO> packageDTOS = Procurement_packageRepository.getInstance().getDTOsByIds(packageIds);
                                    for(int k = 0; k <packageDTOS.size(); k++){
                                        Procurement_packageDTO packageDTO = packageDTOS.get(k);
                                        List<Pi_app_request_detailsDTO> detailsByPackage = currentDetails.stream().filter(i -> i.packageId == packageDTO.iD)
                                                .collect(Collectors.toList());
                                        if(detailsByPackage.isEmpty()) continue;
                                        double totalValue = 0;

                                %>
                                <%@include file="appViewBodyRowRelated.jsp" %>
                                <tr>

                                    <td rowspan="<%=detailsByPackage.size() + 1%>">
                                        <%=Utils.getDigits(k+1, Language)%>
                                    </td>

                                    <td rowspan="<%=detailsByPackage.size() + 1%>">
                                        <%=UtilCharacter.getDataByLanguage(Language, packageDTO.nameBn, packageDTO.nameEn)%>
                                    </td>

                                        <%
                                            for(int i = 0; i < detailsByPackage.size(); i++){
                                                if(i != 0){

                                        %>
                                    <%@include file="appViewBodyRowRelated.jsp" %>
                                            <tr>
                                    <%
                                                    if(totalRowCount % rowsPerPage == 0){
                                                        int rowSpanSize =  detailsByPackage.size() - i + 1;
                                    %>
                                            <td rowspan="<%=rowSpanSize%>">
                                                <%=Utils.getDigits(k+1, Language)%>
                                            </td>

                                            <td rowspan="<%=rowSpanSize%>">
                                                <%=UtilCharacter.getDataByLanguage(Language, packageDTO.nameBn, packageDTO.nameEn)%>
                                            </td>
                                    <%

                                                    }
                                                }
                                    %>
                                    <td>
                                        <%=Utils.getDigits(i+1, Language)%>
                                    </td>
                                    <td>
                                        <%
                                            value = procurement_goodsDAO.getCircularData(detailsByPackage.get(i).itemId, Language);

                                        %>
                                        <%=value%>
                                    </td>
                                    <td style="text-align: right">
                                        <%=Utils.getDigits((int)detailsByPackage.get(i).approveThreeQuantity, Language)%>
                                    </td>
                                    <td style="text-align: right">
                                        <%=Utils.getDigits(String.format("%.2f",
                                                detailsByPackage.get(i).approveThreeUnitPrice), Language)%>
                                    </td>
                                    <td style="text-align: right">
                                        <%=Utils.getDigits(String.format("%.2f",
                                                detailsByPackage.get(i).approveThreeEstimatedCost), Language)%>
                                    </td>

                                    <td>
                                        <%
                                            value = Pi_app_request_detailsDAO.getInstance().getProcurementMethodData
                                                    (detailsByPackage.get(i).approveThreeProcurementMethodId, isLanguageEnglish);
                                        %>
                                        <%=value%>
                                    </td>
                                    <td>
                                        <%
                                            value = CatRepository.getInstance().getText(Language, "pi_contract_approving_authority", detailsByPackage.get(i).approveThreeContractAppAuthId);
                                        %>

                                        <%=value%>
                                    </td>
                                    <td>
                                        <%
                                            value = CatRepository.getInstance().getText(Language, "pi_source_of_fund", detailsByPackage.get(i).approveThreeSourceFundId);
                                        %>

                                        <%=value%>
                                    </td>

                                    <%
                                        totalRowCount++;
                                    %>

                                </tr>
                                <%
                                        totalValue += detailsByPackage.get(i).approveThreeEstimatedCost;
                                    }
                                %>

                                <%@include file="appViewBodyRowRelated.jsp" %>

                                <tr>
                                    <%
                                        if(totalRowCount % rowsPerPage == 0){
                                    %>
                                            <td >
                                                <%=Utils.getDigits(k+1, Language)%>
                                            </td>

                                            <td >
                                                <%=UtilCharacter.getDataByLanguage(Language, packageDTO.nameBn, packageDTO.nameEn)%>
                                            </td>
                                    <%
                                        }
                                    %>
                                    <td  colspan = "3" style="text-align: right">
                                        <%=UtilCharacter.getDataByLanguage(Language, "মোট=", "Total=")%>
                                    </td>
                                    <td colspan = "2" style="text-align: right">
                                        <%=Utils.getDigits(String.format("%.2f", totalValue), Language)%>
                                    </td>
                                    <td></td>
                                    <td>

                                    </td>
                                    <td></td>
                                    <%
                                        totalRowCount++;
                                    %>
                                </tr>
                                <%

                                        }

                                    }
                                %>


                                </tbody>
                            </table>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






<script>
    let buttonDiv = $("#printer");
    function printDiv(divName) {
        $("#printer").hide();
        let printContents = document.getElementById(divName).innerHTML;
        let originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
        $("#printer").show();
    }

    function downloadDiv(){
        let content = document.getElementById('to-print-div');
        const opt = {
            margin: 0.5,
            filename: 'APP.pdf',
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4'}
        };
        html2pdf().from(content).set(opt).save();
    }
</script>


