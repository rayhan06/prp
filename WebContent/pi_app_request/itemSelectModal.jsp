<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="util.CommonConstant" %>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@ page import="pb.CommonDAO" %>
<%@ page contentType="text/html;charset=UTF-8"  %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = (userDTO != null && userDTO.languageID == CommonConstant.Language_ID_English) ? "English" : "Bangla";
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
%>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="search_item_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h4 class="caption table-title">
                    <%=isLanguageEnglish? "Item Select" : "আইটেম নির্বাচন"%>
                </h4>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="kt-form" >

                            <div  class="form-group ">
                                <label for="packageId">
                                    <%=(isLanguageEnglish ? "Package" : "প্যাকেজ")%>
                                    <span class="required"> * </span>
                                </label>
                                <div>
                                    <select class='form-control' name='packageId'
                                            id='packageId' tag='pb_html' onchange="packageChange()">
                                        <%
                                            String Options = CommonDAO.getOptions(Language, "procurement_package", -1);
                                        %>
                                        <%=Options%>
                                    </select>
                                </div>
                            </div>

                            <div  class="form-group ">
                                <label for="itemType">
                                    <%=(isLanguageEnglish ? "Item Type" : "আইটেম ধরণ")%>
                                    <span class="required"> * </span>
                                </label>
                                <div>
                                    <select class='form-control' name='itemType'
                                            id='itemType' tag='pb_html' onchange="itemTypeChange()">
                                    </select>
                                </div>
                            </div>

                            <div  class="form-group ">
                                <label for="item">
                                    <%=(isLanguageEnglish ? "Item" : "আইটেম")%>
                                    <span class="required"> * </span>
                                </label>
                                <div>
                                    <select class='form-control' name='item'
                                            id='item' tag='pb_html'>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <%--------------------------------FOOTER----------------------------------------------%>
            <div class="modal-footer border-top-0">
                <button type="button" class="btn btn-success shadow btn-border-radius"
                         onclick="selectItem();">
                    <%=isLanguageEnglish? "Item Select" : "আইটেম নির্বাচন"%>
                </button>
                <button type="button" class="btn cancel-btn text-white shadow btn-border-radius" data-dismiss="modal">
                    <%=LM.getText(LC.CLOSE_CLOSE, loginDTO)%>
                </button>
            </div>
        </div>
    </div>
</div>

<script>

    let modalSelector = $('#search_item_modal');

    // modal on load event
    modalSelector.on('show.bs.modal', function () {
        $('#packageId').val('-1');
        $('#itemType').html('');
        $('#item').html('');
    });


    function selectItem(){
        let itemValue = $("#item").val();
        modalSelector.modal('hide');
        if(!itemValue || itemValue === '' ){
            showToastSticky("No Item Selected","No Item Selected");
        } else {
            let lastSelectedItem = {
                id: itemValue,
                name: $( "#item option:selected" ).text(),
                packageId : $("#packageId").val(),
                packageName: $( "#packageId option:selected" ).text(),
            }

            if(itemSelectModalUsage !== 'none'){
                let options = itemSelectModalOptionsMap.get(itemSelectModalUsage);
                if(options.itemSelectedCallback){
                    options.itemSelectedCallback(lastSelectedItem);
                }
            }
        }

    }

    function packageChange(){
        let packageId = $("#packageId").val();
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                $('#itemType').html(this.responseText);
                itemTypeChange();
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Procurement_packageServlet?actionType=getByPackageId&ID=" + packageId, true);
        xhttp.send();
    }

    function itemTypeChange(){
        let itemTypeId = $("#itemType").val();
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                $('#item').html(this.responseText);
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Procurement_packageServlet?actionType=getByItemTypeId&ID=" + itemTypeId, true);
        xhttp.send();
    }
</script>
