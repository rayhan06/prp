<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="user.UserDTO" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="java.util.List" %>
<%@ page import="util.UtilCharacter" %>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page pageEncoding="UTF-8" %>
<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    List<Fiscal_yearDTO> fiscal_yearDTOS = Fiscal_yearRepository.getInstance().getFiscal_yearList();
    String formTitle = UtilCharacter.getDataByLanguage(Language, "বার্ষিক ক্রয় পরিকল্পনা খুঁজুন",
            "ANNUAL PURCHASE PLAN SEARCH");
    int i = 0;



%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action=""
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting()">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-10 offset-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-3 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_SELECT_FISCAL_YEAR, loginDTO)%>
                                            </label>
                                            <div class="col-9">
                                                <select class='form-control' name='fiscalYearId'
                                                        id='fiscalYearId_hidden_<%=i%>' tag='pb_html' required>
                                                    <%
                                                        //														String fiscalYearOptions = Utils.buildSelectOption(isLanguageEnglish);
                                                        StringBuilder selectOption = new StringBuilder();
                                                        selectOption.append("<option value = '-100'>");
                                                        if (isLanguageEnglish) {
                                                            selectOption.append("All</option>");
                                                        } else {
                                                            selectOption.append("সকল </option>");
                                                        }
                                                        String fiscalYearOptions = selectOption.toString();

                                                        StringBuilder option = new StringBuilder();
                                                        for (Fiscal_yearDTO fiscal_yearDTO : fiscal_yearDTOS) {
                                                            //System.out.println("fiscal dto id: "+fiscal_yearDTO.id);
                                                            option.append("<option value = '").append(fiscal_yearDTO.id).append("'>");
                                                            option.append(isLanguageEnglish ? fiscal_yearDTO.nameEn : fiscal_yearDTO.nameBn).append("</option>");
                                                        }
                                                        fiscalYearOptions += option.toString();
                                                    %>
                                                    <%=fiscalYearOptions%>
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-10 form-actions text-right mt-3">
                        <button onclick="loadData()" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                type="submit">
                            <%=UtilCharacter.getDataByLanguage(Language, "লোড", "Load")%>
                        </button>
                    </div>
                </div>
                <div id="data-div" class="  mb-4" style="display: none">
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    let lang = '<%=Language%>';
    $(document).ready(function () {
        select2MultiSelector("#fiscalYearId_hidden_0", '<%=Language%>');
    });

    function valueByLanguage(lang, bnValue, enValue) {
        if (lang === 'english' || lang === 'English') {
            return enValue;
        } else {
            return bnValue;
        }
    }

    function validation() {

        let fiscalYearIds = $("#fiscalYearId_hidden_0").val();
        if (!fiscalYearIds || fiscalYearIds.length === 0) {
            let errMsg = valueByLanguage(lang, "অর্থবছর নির্বাচন করুন", "Please select fiscal year");
            $('#toast_message').css('background-color','#ff6063');
            showToastSticky(errMsg,errMsg);
            return false;
        }
        return true;
    }

    function loadData() {
        event.preventDefault();
        let dataDiv = $("#data-div")
        dataDiv.hide();
        if (validation()) {
            let fiscalYearIds = $("#fiscalYearId_hidden_0").val();
            let url = "Pi_app_requestServlet?actionType=getFiscalYearData&fiscalYearIds=" + fiscalYearIds;
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    dataDiv.html(fetchedData);
                    dataDiv.show();
                },
                error: function (error) {
                    console.log(error);
                }
            });

        }
    }

</script>
