<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="util.CommonConstant" %>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@ page import="pb.CommonDAO" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="java.util.List" %>
<%@ page import="pb.Utils" %>
<%@ page contentType="text/html;charset=UTF-8"  %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = (userDTO != null && userDTO.languageID == CommonConstant.Language_ID_English) ? "English" : "Bangla";
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");

    List<Fiscal_yearDTO> fiscal_yearDTOS = Fiscal_yearRepository.getInstance().getFiscal_yearList();
    String fiscalYearOptions = Utils.buildSelectOption(isLanguageEnglish);
    StringBuilder option = new StringBuilder();
    for(Fiscal_yearDTO fiscal_yearDTO:fiscal_yearDTOS){
        option.append("<option value = '").append(fiscal_yearDTO.id).append("'>");
        option.append(isLanguageEnglish ? fiscal_yearDTO.nameEn : fiscal_yearDTO.nameBn).append("</option>");
    }
    fiscalYearOptions += option.toString();
%>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="search_fiscal_year_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h4 class="caption table-title">
                    <%=isLanguageEnglish? "Fiscal Year Select" : "অর্থবছর নির্বাচন করুন"%>
                </h4>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="kt-form" >

                            <div  class="form-group ">
                                <label for="fiscalYearInModal">
                                    <%=LM.getText(LC.PI_APP_REQUEST_ADD_FISCALYEARID, loginDTO)%>
                                </label>
                                <div>
                                    <select class='form-control' name='fiscalYearInModal'
                                            id='fiscalYearInModal' tag='pb_html' >
                                        <%=fiscalYearOptions%>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <%--------------------------------FOOTER----------------------------------------------%>
            <div class="modal-footer border-top-0">
                <button type="button" class="btn btn-success shadow btn-border-radius"
                         onclick="selectFiscalItem()">
                    <%=isLanguageEnglish? "Import" : "ইম্পোর্ট"%>
                </button>
                <button type="button" class="btn cancel-btn text-white shadow btn-border-radius" data-dismiss="modal">
                    <%=LM.getText(LC.CLOSE_CLOSE, loginDTO)%>
                </button>
            </div>
        </div>
    </div>
</div>

<script>

    let modalFisSelector = $('#search_fiscal_year_modal');

    // modal on load event
    modalFisSelector.on('show.bs.modal', function () {
        $('#fiscalYearInModal').val('');
    });


    function selectFiscalItem(){
        let itemValue = $("#fiscalYearInModal").val();
        modalFisSelector.modal('hide');
        if(!itemValue || itemValue === '' ){
            showToastSticky("No Fiscal Year Selected","No Fiscal Year Selected");
        }
        else {
            let lastSelectedItem = {
                id: itemValue,
            }

            if(fisSelectModalUsage !== 'none'){
                let options = fisSelectModalOptionsMap.get(fisSelectModalUsage);
                if(options.itemSelectedCallback){
                    options.itemSelectedCallback(lastSelectedItem);
                }
            }
        }

    }

</script>
