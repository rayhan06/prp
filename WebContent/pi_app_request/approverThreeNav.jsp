<%@page import="language.LC"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="language.LM"%>
<%@ page language="java" %>
<%@ page import="java.util.Arrays"%>
<%@ page import="searchform.SearchForm"%>
<%@ page import="pb.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="java.util.List" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="util.*" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
	System.out.println("Inside nav.jsp");
	String url = "Pi_app_requestServlet?actionType=forApprovalThree";

	Office_unitsDTO reqOfficeUnitDTO = null;
	String reqOfficeUnitIdStr = request.getParameter("reqOfficeUnitId");
	if (reqOfficeUnitIdStr != null && reqOfficeUnitIdStr.length() > 0) {
		reqOfficeUnitDTO = Office_unitsRepository.getInstance()
				.getOffice_unitsDTOByID(Long.parseLong(reqOfficeUnitIdStr));
	}
%>
<%@include file="../pb/navInitializer.jsp"%>

<%
	boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
%>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
				<input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text' class='form-control border-0'
					   onKeyUp='allfield_changed("",0,false)' id='anyfield'  name='anyfield'
					   value = '<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
				>
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
<%--        <div class="kt-portlet__head-toolbar">--%>
<%--            <div class="kt-portlet__head-group">--%>
<%--                <div class="tooltip tooltip-portlet tooltip bs-tooltip-top" role="tooltip" id="tooltip_p6zf7aqcpv"--%>
<%--                     aria-hidden="true" x-placement="top"--%>
<%--                     style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">--%>
<%--                    <div class="tooltip-arrow arrow" style="left: 34px;"></div>--%>
<%--                    <div class="tooltip-inner">Collapse</div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
    </div>        
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
		<div class="row">

			<div class="col-md-6">
				<div class="form-group row">
					<label class="col-md-3 col-form-label">
						<%=LM.getText(LC.PI_APP_REQUEST_ADD_FISCALYEARID, loginDTO)%>
					</label>
					<div class="col-md-9">
						<%
							List<Fiscal_yearDTO> fiscal_yearDTOS = Fiscal_yearRepository.getInstance().getFiscal_yearList();
							String fiscalYearOptions = Utils.buildSelectOption(isLanguageEnglish);
							StringBuilder option = new StringBuilder();
							for(Fiscal_yearDTO fiscal_yearDTO:fiscal_yearDTOS){
								option.append("<option value = '").append(fiscal_yearDTO.id).append("'>");
								option.append(isLanguageEnglish ? fiscal_yearDTO.nameEn : fiscal_yearDTO.nameBn).append("</option>");
							}
							fiscalYearOptions += option.toString();
						%>
						<select class='form-control' name='fiscalYearInModal'
								id='fiscalYearInModal' tag='pb_html' >
							<%=fiscalYearOptions%>
						</select>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group row">
					<label class="col-md-4 col-xl-3 col-form-label">
						<%=UtilCharacter.getDataByLanguage(Language, "অফিস", "Office")%>
					</label>
					<div class="col-md-8 col-xl-9 col-form-label">
						<%--Office Select Modal Button--%>
						<button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
								id="officeUnit_modal_button">
							<%=UtilCharacter.getDataByLanguage(Language, "অফিস খুঁজুন", "Search Office")%>
						</button>
						<div class="input-group mr-1" id="office_units_id_div" style="display: none">
							<input type="hidden" id='office_units_id' name="office_units_id" value='' tag='pb_html'>
							<button type="button"
									class="btn btn-secondary btn-block shadow-sm btn-border-radius form-control"
									id="office_units_id_text" onclick="officeModalEditButtonClicked()">
							</button>
							<span class="input-group-btn" tag='pb_html'>
							<button type="button" class="btn btn-outline-danger"
									id='office_units_id_crs_btn' tag='pb_html'>
								x
							</button>
						</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
							onclick="allfield_changed('',0,true)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp"%>
<jsp:include page="../employee_assign/officeSearchModal.jsp">
	<jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>


<template id = "loader">
	<div class="modal-body">
		<div class="search-loader-container-circle ">
			<div class="search-loader-circle"></div>
		</div>
	</div>
</template>

<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>
<script type="text/javascript">


	const anyFieldSelector = $('#anyfield');
	const officeUnitDiv = $('#office_units_id_div');
	const officeUnitModalButton = $('#officeUnit_modal_button');
	const officeUnitIdSelector = $("#office_units_id");
	const fiscalYearSelector = $('#fiscalYearInModal');
	let officeModel;

	function resetInputs(){
		crossButtonClick();
		fiscalYearSelector.val('');
		anyFieldSelector.val('');
	}

	window.addEventListener('popstate',e=>{
		if(e.state){
			let params = e.state.params;
			dosubmit(params,false);
			resetInputs();
			officeModel = e.state.officeModel;
			if (officeModel) {
				viewOfficeIdInInput(officeModel);
			}
			let arr = params.split('&');
			arr.forEach(e=>{
				let item = e.split('=');
				if(item.length === 2){
					switch (item[0]){
						case 'reqOfficeUnitId':
							if (item[1].length === 0){
								crossButtonClick();
							}
							break;
						case 'fiscalYearInModal':
							fiscalYearSelector.val(item[1]);
							break;
						case 'AnyField':
							anyFieldSelector.val(item[1]);
							break;
						default:
							setPaginationFields(item);
					}
				}
			});
		}else{
			dosubmit(null,false);
			resetInputs();
			resetPaginationFields();
		}
	});

	$(document).ready(() => {
		readyInit('Pi_app_requestServlet');

		<%if(reqOfficeUnitDTO != null) {%>
		officeModel = {
			id: <%=reqOfficeUnitDTO.iD%>,
			name : '<%=UtilCharacter.getDataByLanguage(Language, reqOfficeUnitDTO.unitNameBng,
				 reqOfficeUnitDTO.unitNameEng)%>'
		}
		viewOfficeIdInInput(officeModel);
		<%}%>
	});

	function dosubmit(params, pushState = true)
	{
		document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
		let xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function () {
			if (this.readyState === 4 && this.status === 200) {
				if (pushState) {
					let stateParam = {
						'params': params,
						'officeModel': officeModel
					}
					history.pushState(stateParam, '', 'Pi_app_requestServlet?actionType=forApprovalThree&' + params);
				}

				setTimeout(() => {
					document.getElementById('tableForm').innerHTML = this.responseText;
					setPageNo();
					searchChanged = 0;
				}, 1000);
			} else if (this.readyState === 4 && this.status !== 200) {
				alert('failed ' + this.status);
			}
		};

		let url = "<%=action%>&ajax=true&isPermanentTable=true";
		if(params){
			url+="&"+params;
		}
		xhttp.open("Get", url, false);
		xhttp.send();
		
	}

	function allfield_changed(go, pagination_number, pushState)
	{
		let params = 'search=true';
		if(anyFieldSelector.val()){
			params += '&AnyField=' + anyFieldSelector.val();
		}
		if (officeUnitIdSelector.val()) {
			params += '&reqOfficeUnitId=' + officeUnitIdSelector.val();
		}
		params +=  '&fiscalYearInModal='+ fiscalYearSelector.val();
		
		let extraParams = document.getElementsByName('extraParam');
		extraParams.forEach((param) => {
			params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

		let pageNo = document.getElementsByName('pageno')[0].value;
		let rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

		let totalRecords = 0;
		let lastSearchTime = 0;
		if(document.getElementById('hidden_totalrecords'))
		{
			totalRecords = document.getElementById('hidden_totalrecords').value;
			lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
		}


		if(go !== '' && searchChanged === 0)
		{
			params += '&go=1';
			pageNo = document.getElementsByName('pageno')[pagination_number].value;
			rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
			setPageNoInAllFields(pageNo);
			setRPPInAllFields(rpp);
		}
		params += '&pageno=' + pageNo;
		params += '&RECORDS_PER_PAGE=' + rpp;
		params += '&TotalRecords=' + totalRecords;
		params += '&lastSearchTime=' + lastSearchTime;
		dosubmit(params, pushState);
	
	}

	function viewOfficeIdInInput(selectedOffice) {
		officeModel = selectedOffice;
		if (selectedOffice.id === '') {
			return;
		}

		officeUnitModalButton.hide();
		officeUnitDiv.show();
		document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
		$('#office_units_id').val(selectedOffice.id);
	}

	function crossButtonClick(){
		officeUnitModalButton.show();
		officeUnitDiv.hide();
		$('#office_units_id').val('');
		document.getElementById('office_units_id_text').innerHTML = '';
	}

	$('#office_units_id_crs_btn').on('click', function () {
		crossButtonClick();
	});

	// Office Select Modal
	// modal trigger button
	// this part is needed because if one may have more than one place to select office
	officeSelectModalUsage = 'none';
	officeSelectModalOptionsMap = new Map([
		['officeUnit', {
			officeSelectedCallback: viewOfficeIdInInput
		}]
	]);

	officeUnitModalButton.on('click', function () {
		officeSelectModalUsage = 'officeUnit';
		$('#search_office_modal').modal();
	});

	function officeModalEditButtonClicked() {
		officeSelectModalUsage = 'officeUnit';
		officeSearchSetSelectedOfficeLayers($('#office_units_id').val());
		$('#search_office_modal').modal();
	}

</script>

