<%@page import="login.LoginDTO" %>
<%@page import="pi_app_request.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="user.*" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="pi_app_request_details.Pi_app_request_detailsDTO" %>
<%@ page import="pi_app_request_details.Pi_app_request_detailsDAO" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>
<%@ page import="pi_package_final.Pi_package_finalDTO" %>
<%@ page import="pi_package_final.Pi_package_finalRepository" %>
<%@ page import="pi_package_final.PiPackageLotFinalDTO" %>
<%@ page import="pi_package_final.PiPackageLotFinalRepository" %>
<%@ page import="pi_app_request_package_lot_item_list.Pi_app_request_package_lot_item_listDTO" %>
<%@ page import="pi_app_request_package_lot_item_list.Pi_app_request_package_lot_item_listDAO" %>

<%
    Pi_app_requestDTO pi_app_requestDTO;
    long ID = Long.parseLong(request.getParameter("ID"));
    pi_app_requestDTO = Pi_app_requestDAO.getInstance().getDTOByID(ID);
    System.out.println("ID = " + ID);

    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    System.out.println(userDTO);
    String actionName = "ajax_approver_one_add";
    System.out.println("actionType = " + request.getParameter("actionType"));

    int i = 0;


    int childTableStartingID = 1;


    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String Options;
    CommonDAO.language = Language;
    CatDAO.language = Language;
    String formTitle = UtilCharacter.getDataByLanguage(Language, "বার্ষিক ক্রয় পরিকল্পনা অনুমোদন",
            "ANNUAL PURCHASE PLAN APPROVAL");

    Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();
    List<Fiscal_yearDTO> fiscal_yearDTOS = Fiscal_yearRepository.getInstance().getFiscal_yearList();

    List<OptionDTO> procurementMethodOptionList = CatRepository.getDTOs("procurement_method")
            .stream()
            .map(e -> new OptionDTO(e.languageTextEnglish, e.languageTextEnglish, String.valueOf(e.value)))
            .collect(Collectors.toList());

    String nameOfOffice = "";

    Office_unitsDTO unit = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(pi_app_requestDTO.officeUnitId);
    if (unit != null) {
        nameOfOffice = UtilCharacter.getDataByLanguage(Language, unit.unitNameBng, unit.unitNameEng);
    }

    boolean submitButtonVisible = pi_app_requestDTO.status == CommonApprovalStatus.PENDING.getValue();

    long allPackageTotalSum = 0;
%>

<style>
    .highlight-modal-td {
        cursor: pointer;
        color: #0c5460;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=pi_app_requestDTO.iD%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.PI_APP_REQUEST_ADD_FISCALYEARID, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>

                                        <div class="col-md-9">

                                            <select class='form-control' name='fiscalYearId' id='fiscalYearId'
                                                    tag='pb_html'>
                                                <%
                                                    Fiscal_yearDTO cur_fis_dto = fiscal_yearDAO.getFiscalYearBYDateLong(System.currentTimeMillis());

                                                    String fiscalYearOptions = Utils.buildSelectOption(isLanguageEnglish);
                                                    StringBuilder option = new StringBuilder();
                                                    for (Fiscal_yearDTO fiscal_yearDTO : fiscal_yearDTOS) {

                                                        if (actionName.equals("ajax_edit")) {
                                                            if (pi_app_requestDTO.fiscalYearId == fiscal_yearDTO.id) {
                                                                option.append("<option value = '").append(fiscal_yearDTO.id).append("' selected>");
                                                            } else {
                                                                option.append("<option value = '").append(fiscal_yearDTO.id).append("'>");
                                                            }
                                                        } else {
                                                            if (cur_fis_dto.id == fiscal_yearDTO.id) {
                                                                option.append("<option value = '").append(fiscal_yearDTO.id).append("' selected>");
                                                            } else {
                                                                option.append("<option value = '").append(fiscal_yearDTO.id).append("'>");
                                                            }
                                                        }

                                                        option.append(isLanguageEnglish ? fiscal_yearDTO.nameEn : fiscal_yearDTO.nameBn).append("</option>");
                                                    }
                                                    fiscalYearOptions += option.toString();
                                                %>
                                                <%=fiscalYearOptions%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.PI_APP_REQUEST_ADD_OFFICEUNITID, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>

                                        <div class="col-8 col-md-6">
                                            <input type="hidden" name='officeUnitId'
                                                   id='office_units_id_input' value="">
                                            <button type="button"
                                                    class="btn btn-secondary form-control shadow btn-border-radius"
                                                    disabled id="office_units_id_text"></button>
                                        </div>
                                        <div class="col-4 col-md-3 text-right">
                                            <button type="button"
                                                    class="btn btn-primary btn-block shadow btn-border-radius"
                                                    id="office_units_id_modal_button"
                                                    onclick="officeModalButtonClicked();">
                                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5">
                    <div class="d-flex justify-content-md-between flex-column flex-md-row align-items-md-end">
                        <div>
                            <h5 class="table-title">
                                <%=UtilCharacter.getDataByLanguage(Language, "প্যাকেজ লটের লিস্ট", "Package Lot List")%>
                            </h5>
                        </div>
                        <%--                        <div class="d-flex align-items-center text-nowrap mb-2">--%>
                        <%--                            <button type="button"--%>
                        <%--                                    class="btn btn-sm btn-primary shadow btn-border-radius"--%>
                        <%--                                    id="fiscal_year_select_button"--%>
                        <%--                                    onclick="fiscalYearClicked()">--%>
                        <%--                                <%=UtilCharacter.getDataByLanguage(Language, "পূর্বের অর্থবছর থেকে তালিকা প্রস্তুত",--%>
                        <%--                                        "Prepare List From Previous Fiscal Year")%>--%>
                        <%--                            </button>--%>
                        <%--                            <button type="button"--%>
                        <%--                                    class="btn btn-sm btn-primary shadow btn-border-radius mx-2"--%>
                        <%--                                    id="package_select_button"--%>
                        <%--                                    onclick="openPackageLotModal();">--%>
                        <%--                                <%=UtilCharacter.getDataByLanguage(Language, "প্যাকেজ নির্বাচন", "Package Select")%>--%>
                        <%--                            </button>--%>
                        <%--                        </div>--%>
                    </div>
                    <div class="table-responsive">
                        <table id="tableData" class="table table-bordered table-striped">
                            <thead class="text-nowrap">
                            <tr>
                                <th><%=(isLanguageEnglish ? "Serial Number" : "ক্রমিক নং")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Package No" : "প্যাকেজ নং")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Description of Procurement Package Goods" : "প্রকিউরমেন্ট প্যাকেজের মালামালের বিবরণ")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Quantity/ Items" : "পরিমান/আইটেম")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Unit Price" : "একক দাম")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Estimated Cost in Tk" : "আনুমানিক খরচ (টাকায়)")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Procurement Method and Type" : "ক্রয় পদ্ধতি এবং প্রকার")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Contract Approving Authority" : "চুক্তি অনুমোদনকারী কর্তৃপক্ষ")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Source of Fund" : "তহবিলের উৎস")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Time Code for Process" : "প্রক্রিয়ার জন্য সময় কোড")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Invite/ Advertisement Tender" : "আমন্ত্রণ/বিজ্ঞাপন দরপত্র")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Tender Opening" : "টেন্ডার ওপেনিং")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Tender Evaluation" : "দরপত্র মূল্যায়ন")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Approval to Award" : "পুরস্কারের অনুমোদন")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Notification of Award" : "পুরস্কারের বিজ্ঞপ্তি")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Signing of Contract" : "চুক্তি স্বাক্ষর")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Total Time to Contract Signature" : "চুক্তি স্বাক্ষরের মোট সময়")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Time for Completion of Contract" : "চুক্তি সমাপ্তির সময়")%>
                                </th>
                                <th><%=(isLanguageEnglish ? "Remark" : "রিমার্ক")%>
                                </th>
                            </tr>
                            </thead>
                            <tbody id='package-body'>
                            <%
                                String newLine = "<br>";
                                List<Pi_app_request_detailsDTO> detailsDTOS = Pi_app_request_detailsDAO
                                        .getInstance().getByAppId(pi_app_requestDTO.iD);
                                for (Pi_app_request_detailsDTO model : detailsDTOS) {
                                    Pi_package_finalDTO packageModel = Pi_package_finalRepository.getInstance()
                                            .getPi_package_finalDTOByiD(model.packageFinalId);
                                    PiPackageLotFinalDTO lotModel = PiPackageLotFinalRepository.getInstance()
                                            .getPiPackageLotFinalDTOByiD(model.lotFinalId);
                                    String packageNo = "";
                                    if (packageModel != null)
                                        packageNo = UtilCharacter.getDataByLanguage(Language, packageModel.packageNumberBn, packageModel.packageNumberEn);
                                    if (lotModel != null) packageNo += " (" +
                                            UtilCharacter.getDataByLanguage(Language, lotModel.lotNumberBn, lotModel.lotNumberEn) + ")";
                                    String description = "";
                                    if (packageModel != null)
                                        description = UtilCharacter.getDataByLanguage(Language, packageModel.packageNameBn, packageModel.packageNameEn);
                                    if (lotModel != null)
                                        description = UtilCharacter.getDataByLanguage(Language, lotModel.lotNameBn, lotModel.lotNameEn);

                                    // QUANTITY / ITEMS CELL DATA GENERATION
                                    List<Pi_app_request_package_lot_item_listDTO> models = (List<Pi_app_request_package_lot_item_listDTO>)
                                            Pi_app_request_package_lot_item_listDAO.getInstance()
                                                    .getDTOsByParent("app_details_id", model.iD);
                                    HashMap<Long, Long> officeItemCount = new HashMap<>();
                                    for (Pi_app_request_package_lot_item_listDTO quantityModel : models) {
                                        if (officeItemCount.containsKey(quantityModel.officeUnitId)) {
                                            long prevCount = officeItemCount.get(quantityModel.officeUnitId);
                                            officeItemCount.put(quantityModel.officeUnitId, prevCount + 1);
                                        } else {
                                            officeItemCount.put(quantityModel.officeUnitId, 1l);
                                        }
                                    }

                                    boolean approverOneEdited = false;
                                    StringBuilder quantity = new StringBuilder();

                                    for (Map.Entry<Long, Long> entity : officeItemCount.entrySet()) {
                                        quantity.append(Office_unitsRepository.getInstance().geText(Language, entity.getKey()))
                                                .append(newLine)
                                                .append(Utils.getDigits(entity.getValue(), Language))
                                                .append(UtilCharacter.getDataByLanguage(Language, " টি", " Piece"))
                                                .append(newLine);
                                    }
                                    String quantityText = quantity.toString();


                                    // GENERATE DATA OF EST COST CELL
                                    officeItemCount = new HashMap<>();
                                    long grandTotal = 0;
                                    for (Pi_app_request_package_lot_item_listDTO estCostModel : models) {
                                        approverOneEdited = !(estCostModel.approverOneUnitPrice == -1 &&
                                                estCostModel.approverOneQuantity == -1 &&
                                                estCostModel.approverOneEstimatedTotalPrice == -1);

                                        if (officeItemCount.containsKey(estCostModel.officeUnitId)) {
                                            long prevCount = officeItemCount.get(estCostModel.officeUnitId);
                                            if (approverOneEdited)
                                                officeItemCount.put(estCostModel.officeUnitId, prevCount +
                                                        estCostModel.approverOneUnitPrice * estCostModel.approverOneQuantity);
                                            else officeItemCount.put(estCostModel.officeUnitId, prevCount +
                                                    estCostModel.requestedUnitPrice * estCostModel.requestedQuantity);
                                        } else {
                                            if (approverOneEdited)
                                                officeItemCount.put(estCostModel.officeUnitId,
                                                        estCostModel.approverOneUnitPrice * estCostModel.approverOneQuantity);
                                            else officeItemCount.put(estCostModel.officeUnitId,
                                                    estCostModel.requestedUnitPrice * estCostModel.requestedQuantity);
                                        }
                                        if (approverOneEdited)
                                            grandTotal += estCostModel.approverOneUnitPrice * estCostModel.approverOneQuantity;
                                        else
                                            grandTotal += estCostModel.requestedUnitPrice * estCostModel.requestedQuantity;
                                    }
                                    allPackageTotalSum += grandTotal;

                                    StringBuilder estCost = new StringBuilder();

                                    for (Map.Entry<Long, Long> entity : officeItemCount.entrySet()) {
                                        estCost.append(Utils.getDigits(entity.getValue(), Language)).append(newLine);
                                    }
                                    estCost.append("-------------------").append(newLine);
                                    estCost.append(Utils.getDigits(grandTotal, Language));
                                    String estCostText = estCost.toString();
                            %>
                            <tr class="text-center">
                                <input type="hidden" id="pi_app_details_id_<%=childTableStartingID%>"
                                       name="piAppDetails.appDetailsId" value="<%=model.iD%>">
                                <input type="hidden" id="pi_app_package_item_map_id_<%=childTableStartingID%>"
                                       name='piAppDetails.piPackageItemMapId' value="<%=model.piPackageItemMapId%>">
                                <input type="hidden" id="pi_app_package_id_<%=childTableStartingID%>"
                                       name='piAppDetails.packageFinalId' value="<%=model.packageFinalId%>">
                                <input type="hidden" id="pi_app_lot_id_<%=childTableStartingID%>"
                                       name='piAppDetails.lotFinalId' value="<%=model.lotFinalId%>">
                                <input type="hidden" id="pi_app_details_serial_number_<%=childTableStartingID%>"
                                       name='piAppDetails.serialNumber' value="<%=model.serialNum%>">
                                <input type="hidden" id="total_quantity_<%=childTableStartingID%>"
                                       name='piAppDetails.quantity' value="<%=model.quantity%>">
                                <input type="hidden" id="pi_app_details_est_cost_<%=childTableStartingID%>"
                                       name='piAppDetails.estCost' value="<%=model.estCost%>">
                                <input type="hidden" id="pi_app_details_total_price_<%=childTableStartingID%>"
                                       name='piAppDetails.totalPrice' value="<%=model.estCost%>">
                                <td id="serial_number_<%=childTableStartingID%>"><%=model.serialNum%>
                                </td>
                                <td id="package_no_<%=childTableStartingID%>" class="highlight-modal-td"
                                    onclick="openItemListModal(this)"><%=packageNo%>
                                </td>
                                <td id='description_<%=childTableStartingID%>'><%=description%>
                                </td>
                                <td id='total_quantity_text_<%=childTableStartingID%>'><%=quantityText%>
                                </td>
                                <td id="unit_price_<%=childTableStartingID%>">
                                    <input type="text" class='form-control w-auto' name="piAppDetails.packageUnitPrice"
                                           value="<%=model.packageUnitPrice%>">
                                </td>
                                </td>
                                <td id="total_price_<%=childTableStartingID%>" class="highlight-modal-td"
                                    onclick="openItemListModal(this)">
                                    <%=estCostText%>
                                </td>
                                <td>
                                    <select multiple="multiple" class='form-control w-auto'
                                            name='piAppDetails.procurementMethod'
                                            onchange="procurementMethodChange(this)"
                                            value="<%=model.procurementMethodId%>"
                                            id='procurementMethod_<%=childTableStartingID%>'
                                            tag='pb_html'>
                                        <%
                                            Options = Utils.buildOptionsMultipleSelection(procurementMethodOptionList, Language, model.procurementMethodId);
                                        %>
                                        <%=Options%>
                                    </select>
                                    <input type='hidden' name='procurementMethodHiddenId'
                                           id='procurementMethodHiddenId_<%=childTableStartingID%>'
                                           tag='pb_html'/>
                                </td>
                                <td>
                                    <select class='form-control w-auto' name='approvalAuthority'
                                            onchange="approvalAuthorityChange(this)"
                                            id='approvalAuthority_<%=childTableStartingID%>'
                                            tag='pb_html'>
                                        <%
                                            Options = CatRepository.getInstance().buildOptionsWithoutSelectOption("pi_contract_approving_authority", Language, Math.toIntExact(model.approveOneContractAppAuthId));
                                        %>
                                        <%=Options%>
                                    </select>
                                </td>
                                <td>
                                    <select class='form-control w-auto' name='sourceFund'
                                            onchange="sourceOfFundChange(this)"
                                            id='sourceFund_<%=childTableStartingID%>' tag='pb_html'>
                                        <%
                                            Options = CatRepository.getInstance().buildOptionsWithoutSelectOption("pi_source_of_fund", Language, Math.toIntExact(model.sourceFundId));
                                        %>
                                        <%=Options%>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto'
                                           name="piAppDetails.timeCodeForProcess"
                                           value="<%=model.timeCodeForProcess%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto'
                                           name="piAppDetails.inviteAdvertiseTender"
                                           value="<%=model.inviteAdvertiseTender%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto' name="piAppDetails.tenderOpening"
                                           value="<%=model.tenderOpening%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto' name="piAppDetails.tenderEvaluation"
                                           value="<%=model.tenderEvaluation%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto' name="piAppDetails.approvalToAward"
                                           value="<%=model.approvalToAward%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto'
                                           name="piAppDetails.notificationOfAward"
                                           value="<%=model.notificationOfAward%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto' name="piAppDetails.signingOfContract"
                                           value="<%=model.signingOfContract%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto'
                                           name="piAppDetails.totalTimeToContractSignature"
                                           value="<%=model.totalTimeToContractSignature%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto'
                                           name="piAppDetails.timeForCompletionOfContract"
                                           value="<%=model.timeForCompletionOfContract%>">
                                </td>
                                <td>
                                    <input type="text" class='form-control w-auto' name="piAppDetails.remark"
                                           value="<%=model.remark%>">
                                </td>
                            </tr>
                            <%
                                    childTableStartingID++;
                                }
                            %>
                            </tbody>
                        </table>
                        <div>
                            <h5 class="table-title">
                                <%=UtilCharacter.getDataByLanguage(Language, "সর্বমোট ব্যয়: ", "Grand Total: ")%>
                                <%
                                    String allPackageTotalSumText = Utils.getDigits(allPackageTotalSum, Language);
                                %>
                                <span id="grand_total"><%=allPackageTotalSumText%></span>
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="mt-3">
                    <div class="form-row text-left">
                        <label class="col-4 col-form-label text-right">
                            <%=UtilCharacter.getDataByLanguage(Language, "সরাসরি উইং প্রধান বরাবর ফরওয়ার্ড করুন", "Forward To Wing Head Directly")%>
                        </label>
                        <div class="col-8">
                            <input type='checkbox' class='form-control-sm' name='isDirectWingHead'
                                   id='isDirectWingHead'
                                   onclick="toggle(this)" >
                        </div>
                    </div>
                </div>
                <div class="mt-3">
                    <div class="form-actions text-right">
                        <button id="cancel-btn" class="btn btn-danger shadow ml-2" style="border-radius: 8px;">
                            <%=UtilCharacter.getDataByLanguage(Language, "বাতিল করুন", "Reject")%>
                        </button>
                        <button id="submit-btn" class="btn btn-success shadow ml-2" style="border-radius: 8px;"
                                type="submit" onclick="submitForm()">
                            <%=UtilCharacter.getDataByLanguage(Language, "অনুমোদন করুন", "Approve")%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<jsp:include page="pi_app_final_package_lotModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<jsp:include page="fiscalYearModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<jsp:include page="pi_app_package_lot_item_listModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<%@ include file="../recruitment_seat_plan/manualLoader.jsp" %>

<script type="text/javascript">
    let actionName = '<%=actionName%>';
    let child_table_extra_id = <%=childTableStartingID%>;

    let tableBody = $("#itemTBody");
    const appForm = $("#bigform");
    let lang = '<%=Language%>';
    const fullPageLoader = $('#full-page-loader');

    function openItemListModal(element) {
        let appDetailsIdElement = element.parentNode.querySelector("input[name='piAppDetails.appDetailsId']");
        let appDetailsId = appDetailsIdElement ? appDetailsIdElement.value : '';
        let approveLevel = 1;
        loadItemListOfPackageLot(appDetailsId, approveLevel);
        $('#pi_app_package_lot_item_list_modal').modal();
    }

    function getNextIndex(x) {
        x = x / 1;
        let tablename = 'itemTBody';
        let items = Object.values(document.getElementById(tablename).childNodes).filter(el => el.nodeType === Node.ELEMENT_NODE);
        if (items.length < 2) {
            return -1;
        }

        for (let i = 0; i < items.length; i++) {
            let currentIndex = items[i].id.split('_')[1] / 1;
            if (currentIndex === x && i + 1 < items.length) {
                return items[i + 1].id.split('_')[1] / 1;
            }
        }

        return -1;
    }

    function arraysEqual(a1, a2) {
        /* WARNING: arrays must not contain {objects} or behavior may be undefined */
        return JSON.stringify(a1) === JSON.stringify(a2);
    }

    function procurementMethodChange(element) {
        let selectedIdIndex = element.id.split('_')[1];
        let currentSelectBox = document.getElementById("copyProcurement_" + selectedIdIndex);
        let prevIndex = getPreviousIndex(selectedIdIndex);
        let curRowValue = $("#procurementMethod_" + selectedIdIndex).val();


        if (currentSelectBox && currentSelectBox.checked && prevIndex >= 0) {
            let prevRowValue = $("#procurementMethod_" + prevIndex).val();
            if (prevRowValue && curRowValue && !arraysEqual(prevRowValue, curRowValue)) {
                currentSelectBox.checked = false;
            }
        }


        let nextIndex = getNextIndex(selectedIdIndex);
        if (nextIndex < 0) {
            return;
        }
        let checkBoxChecked = document.getElementById("copyProcurement_" + nextIndex);
        if (!checkBoxChecked) {
            return;
        }
        if (checkBoxChecked.checked) {
            let selectorItem = $("#procurementMethod_" + nextIndex);
            selectorItem.val(curRowValue).trigger('change');
        }
    }

    function approvalAuthorityChange(element) {
        let el = document.getElementById(element.id);
        let selectedIdIndex = element.id.split('_')[1];
        let currentSelectBox = document.getElementById("copyApproval_" + selectedIdIndex);
        let prevIndex = getPreviousIndex(selectedIdIndex);
        if (currentSelectBox && currentSelectBox.checked && prevIndex >= 0) {
            let prevRowValue = $("#approvalAuthority_" + prevIndex).val();
            if (prevRowValue && el.value && prevRowValue !== el.value) {
                currentSelectBox.checked = false;
            }
        }
        let nextIndex = getNextIndex(selectedIdIndex);
        if (nextIndex < 0) {
            return;
        }
        let nextCheckBoxChecked = document.getElementById("copyApproval_" + nextIndex);
        if (!nextCheckBoxChecked) {
            return;
        }
        if (nextCheckBoxChecked.checked && el.value) {
            $("#approvalAuthority_" + nextIndex).val(el.value);
            approvalAuthorityChange(document.getElementById("approvalAuthority_" + nextIndex));
        }
    }

    function sourceOfFundChange(element) {

        let el = document.getElementById(element.id);
        let selectedIdIndex = element.id.split('_')[1];
        let currentSelectBox = document.getElementById("copySourceFund_" + selectedIdIndex);
        let prevIndex = getPreviousIndex(selectedIdIndex);
        if (currentSelectBox && currentSelectBox.checked && prevIndex >= 0) {
            let prevRowValue = $("#sourceFund_" + prevIndex).val();
            if (prevRowValue && el.value && prevRowValue !== el.value) {
                currentSelectBox.checked = false;
            }
        }
        let nextIndex = getNextIndex(selectedIdIndex);
        if (nextIndex < 0) {
            return;
        }
        let nextCheckBoxChecked = document.getElementById("copySourceFund_" + nextIndex);
        if (!nextCheckBoxChecked) {
            return;
        }
        if (nextCheckBoxChecked.checked && el.value) {
            $("#sourceFund_" + nextIndex).val(el.value);
            sourceOfFundChange(document.getElementById("sourceFund_" + nextIndex));
        }
    }

    function getPreviousIndex(x) {
        x = x / 1;
        let tablename = 'itemTBody';
        let items = Object.values(document.getElementById(tablename).childNodes).filter(el => el.nodeType === Node.ELEMENT_NODE);
        if (items.length < 2) {
            return -1;
        }

        let prevValue = -1;

        for (let i = 0; i < items.length; i++) {
            let currentIndex = items[i].id.split('_')[1] / 1;
            if (currentIndex === x) {
                return prevValue;
            }

            prevValue = currentIndex;
        }

        return -1;

    }

    function copyProcurementMethod(element) {
        let el = document.getElementById(element.id);
        if (!el.checked) {
            return;
        }
        let selectedIdIndex = element.id.split('_')[1];
        let prevIndex = getPreviousIndex(selectedIdIndex);
        if (prevIndex < 0) {
            return;
        }

        let prevRowValue = $("#procurementMethod_" + prevIndex).val();
        if (prevRowValue) {
            let selectorItem = $("#procurementMethod_" + selectedIdIndex);
            selectorItem.val(prevRowValue).trigger('change');
        }
    }

    function copyApprovalAuthority(element) {
        let el = document.getElementById(element.id);
        if (!el.checked) {
            return;
        }
        let selectedIdIndex = element.id.split('_')[1];
        let prevIndex = getPreviousIndex(selectedIdIndex);
        if (prevIndex < 0) {
            return;
        }

        let prevRowValue = $("#approvalAuthority_" + prevIndex).val();
        if (prevRowValue) {
            $("#approvalAuthority_" + selectedIdIndex).val(prevRowValue);
            approvalAuthorityChange(document.getElementById("approvalAuthority_" + selectedIdIndex));
        }
    }

    function copySourceOfFund(element) {
        let el = document.getElementById(element.id);
        if (!el.checked) {
            return;
        }
        let selectedIdIndex = element.id.split('_')[1];
        let prevIndex = getPreviousIndex(selectedIdIndex);
        if (prevIndex < 0) {
            return;
        }

        let prevRowValue = $("#sourceFund_" + prevIndex).val();
        if (prevRowValue) {
            $("#sourceFund_" + selectedIdIndex).val(prevRowValue);
            sourceOfFundChange(document.getElementById("sourceFund_" + selectedIdIndex));

        }
    }

    function showEstimatedValue(element) {
        let el = document.getElementById(element.id);
        let selectedIdIndex = element.id.split('_')[1];
        let rowEl = el.parentElement.parentElement;
        let quantity = rowEl.querySelector('#quantity_' + selectedIdIndex).value;
        let unitPrice = rowEl.querySelector('#unitPrice_' + selectedIdIndex).value;
        if (quantity && unitPrice && quantity.trim().length > 0 && unitPrice.trim().length > 0) {
            let val = (quantity / 1) * (unitPrice / 1);
            rowEl.querySelector('#etsCost_' + selectedIdIndex).value = val.toFixed(2);
        } else {
            rowEl.querySelector('#etsCost_' + selectedIdIndex).value = ""
        }
    }

    function removeItemFromTable() {
        let tablename = 'itemTBody';
        let i = 0;
        let element = document.getElementById(tablename);
        let j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            let tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                let checkbox = tr.querySelector('input[deletecb="true"]');
                if (checkbox.checked === true) {
                    tr.remove();
                }
                j++;
            }

        }
    }

    function officeValidation() {
        let flag = false;
        let officeValue = $('#office_units_id_input').val();
        if (officeValue && officeValue.length > 0) {
            flag = true;
        } else {
            let errMsg = valueByLanguage(lang, 'শাখা নির্বাচন করুন', 'Please select section');
            $('#toast_message').css('background-color', '#ff6063');
            showToastSticky(errMsg, errMsg);
        }

        return flag;
    }

    function PreprocessBeforeSubmiting() {
        // return officeValidation() && itemValidation();
        return true;
    }

    function itemValidation() {
        let tablename = 'itemTBody';
        let items = Object.values(document.getElementById(tablename).childNodes).filter(el => el.nodeType === Node.ELEMENT_NODE);
        if (items.length === 0) {
            let errMsg = valueByLanguage(lang, 'নূন্যতম ১টি আইটেম বাছাই করুন', 'Please select minimum one item');
            $('#toast_message').css('background-color', '#ff6063');
            showToastSticky(errMsg, errMsg);
            return false;
        }

        // process checkbox and multi-selector

        for (let i = 0; i < items.length; i++) {
            let currentIndex = items[i].id.split('_')[1] / 1;
            let checkboxItem = document.getElementById('isMaintenance_' + currentIndex);
            document.getElementById('isMaintenanceHiddenId_' + currentIndex).value = !!(checkboxItem && checkboxItem.checked);

            $("#procurementMethodHiddenId_" + currentIndex).val($("#procurementMethod_" + currentIndex).val())

        }

        // processMultipleSelectBoxBeforeSubmit("procurementMethod");

        return true;
    }

    function submitForm() {
        event.preventDefault();
        // PreprocessBeforeSubmiting()
        // console.log('submit');
        buttonStateChange(true);
        if (PreprocessBeforeSubmiting()) {
            fullPageLoader.show();
            $.ajax({
                type: "POST",
                url: "Pi_app_requestServlet?actionType=<%=actionName%>",
                data: appForm.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        showToastSticky("সাবমিট সফল হয়েছে", "Submit Successful");
                        setTimeout(() => {
                            window.location.replace(getContextPath() + response.msg);
                        }, 3000);
                    }
                    fullPageLoader.hide();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                    fullPageLoader.hide();
                }
            });
        } else {
            buttonStateChange(false);
        }
    }

    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id_input').val(selectedOffice.id);
    }

    function init() {
        $("#fiscalYear").attr("disabled", true);
        viewOfficeIdInInput({
            name: '<%=nameOfOffice%>',
            id: <%=pi_app_requestDTO.officeUnitId%>
        });

        for (let i = 1; i <= child_table_extra_id; i++) {
            if ($("#procurementMethod_" + i)) {
                select2MultiSelector("#procurementMethod_" + i, '<%=Language%>');
            }
        }
    }

    $(document).ready(function () {
        init();
        fullPageLoader.hide();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        appForm.validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {},
            messages: {}
        });

    });

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    // related to item modal

    function itemSelectClicked() {
        $('#search_proc_modal').modal();
    }

    necessaryCollectionForProcurementModal = {
        callBackFunction: function (item) {
            getItem(item);
        }
    };

    function getItem(item) {
        if (!item || item.iD === '') {
            return;
        }

        let t = $("#template-items");
        tableBody.append(t.html());
        SetCheckBoxValues("itemTBody");
        let tr = tableBody.find("tr:last-child");

        tr.attr("id", "item_" + child_table_extra_id);

        tr.find("[tag='pb_html']").each(function (index) {
            let prev_id = $(this).attr('id');
            $(this).attr('id', prev_id + child_table_extra_id);
        });

        tr.find("#itemPackage_" + child_table_extra_id).val(valueByLanguage(lang, item.procurementPackageNameBn,
            item.procurementPackageNameEn));
        tr.find("#itemName_" + child_table_extra_id).val(item.circularData);
        tr.find("#itemPackageId_" + child_table_extra_id).val(item.procurementPackageId);
        tr.find("#itemId_" + child_table_extra_id).val(item.iD);

        select2MultiSelector("#procurementMethod_" + child_table_extra_id, '<%=Language%>');
        child_table_extra_id++;
    }

    function valueByLanguage(lang, bnValue, enValue) {
        if (lang === 'english' || lang === 'English') {
            return enValue;
        } else {
            return bnValue;
        }
    }

    function toggle(element) {
        if (element.value) {
            if (element.value === "true") element.value = false;
            else element.value = true;
        } else {
            element.value = true;
        }
    }
</script>