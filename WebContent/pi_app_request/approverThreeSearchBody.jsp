<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="pb.*" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="util.UtilCharacter" %>
<%@page pageEncoding="UTF-8" %>

<%
    String navigator = (String) request.getAttribute("navigator");
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

    String pageno = "";

    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    if (rn == null) {
        rn = (RecordNavigator) request.getAttribute("recordNavigator");
    }
    String tableName = rn.m_tableName;
    String servletName;
    if(request.getAttribute("servletName") != null){
        servletName = (String) request.getAttribute("servletName");
    }else {
        servletName = Utils.capitalizeFirstLetter(tableName) + "Servlet";
    }

    request.setAttribute("navName", "../" + tableName + "/approverThreeNav.jsp");


    request.setAttribute("formName", "../" + tableName + "/approverThreeSearchForm.jsp");

    String url = servletName + "?actionType=forApprovalThree";
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();
    boolean isPermanentTable = rn.m_isPermanentTable;

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String pageNamePrefix = tableName.toUpperCase() + "_SEARCH";
    String pageNameConstant = tableName.toUpperCase() + "_SEARCH_FORMNAME";
    int pagination_number = 0;

%>
<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <i class="fa fa-search fa-2x" style="color:#0098bf !important;"></i>&nbsp;
        <h3 class="kt-subheader__title">
            &nbsp; <%=UtilCharacter.getDataByLanguage(Language, "বার্ষিক ক্রয় পরিকল্পনা তৃতীয় অনুমোদন খুঁজুন", "APP Third Approval Search")%>
        </h3>
    </div>
</div>
<!-- end:: Subheader -->


<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content" style="background: white">
    <div class="row shadow-none border-0">
        <div class="col-lg-12">
            <jsp:include page="${navName}" flush="true">
                <jsp:param name="url" value="<%=url%>"/>
                <jsp:param name="navigator" value="<%=navigator%>"/>
                <jsp:param name="pageName"
                           value='<%=LM.getText(pageNamePrefix, pageNameConstant, loginDTO)%>'/>
            </jsp:include>
            <div style="height: 1px; background: #ecf0f5"></div>
            <div class="kt-portlet shadow-none">
                <div class="kt-portlet__body">
                    <form id="tableForm" >
                        <jsp:include page="${formName}" flush="true">
                            <jsp:param name="pageName"
                                       value='<%=LM.getText(pageNamePrefix, pageNameConstant, loginDTO)%>'/>
                        </jsp:include>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <% pagination_number = 1;%>
    <%@include file="../common/pagination_with_go2.jsp" %>
</div>

<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        initDeleteCheckBoxes();
        dateTimeInit("<%=Language%>");
    });

</script>
