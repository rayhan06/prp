<%@page import="sessionmanager.SessionConstants" %>
<%@page import="user.UserRepository" %>
<%@page import="user.UserDTO" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.CommonConstant" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="java.util.List" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String modalTitle = request.getParameter("modalTitle");
    if (modalTitle == null) {
        modalTitle = UtilCharacter.getDataByLanguage(Language, "প্যাকেজ লটের লিস্ট", "List of Package Lot");
    }

    List<Fiscal_yearDTO> fiscal_yearDTOS = Fiscal_yearRepository.getInstance().getFiscal_yearList();
%>

<div class="modal fade bd-example-modal-xl" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="pi_app_package_lot_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h4 class="caption" style="color: #0098bf;">
                    <%=modalTitle%>
                </h4>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body modal-lg">
                <%@include file="pi_app_final_package_lotModalBody.jsp" %>
            </div>

            <%--------------------------------FOOTER----------------------------------------------%>
            <div class="modal-footer border-0">
                <button type="button" class="btn submit-btn text-white shadow btn-border-radius"
                        id="modal-submit" onclick="submitSelectedPackage()">
                    <%=LM.getText(LC.GLOBAL_SUBMIT, loginDTO)%>
                </button>
                <button type="button" class="btn cancel-btn text-white shadow btn-border-radius"
                        onclick="closeModal()">
                    <%=LM.getText(LC.CLOSE_CLOSE, loginDTO)%>
                </button>
            </div>
            <%--------------------------------SELECTED DATA-----------------------------------------%>
            <input type="hidden" id="selected_package_id" value="-1">
            <input type="hidden" id="selected_lot_id" value="-1">
            <input type="hidden" id="selected_package_name" value="-1">
            <input type="hidden" id="selected_lot_name" value="-1">
            <input type="hidden" id="selected_total_price" value="-1">
            <input type="hidden" id="selected_fiscal_year_text" value="-1">
        </div>
    </div>
</div>


<script type="text/javascript">
    let packageLotModal = {};
    let selectedPackageLot = [];

    $('#pi_app_package_lot_modal').on('show.bs.modal', function () {
        event.preventDefault();
    });

    function closeModal() {
        $('#pi_app_package_lot_modal').modal('hide');
    }

    function fiscalYearValidation() {

    }

    function getFiscalYearId() {
        let fiscalYearId = document.getElementById('modalFiscalYearId').value;
        if (fiscalYearId === null || fiscalYearId === '') {
            swal.fire(getValueByLanguage(language, "অর্থবছর নির্বাচন করুন!", "Please select fiscal year!"));
            return '-1';
        }
        return fiscalYearId;
    }

    function cleanPackageLotList() {
        let tBody = document.getElementById('modal-package-lot-body');
        Array.from(tBody.children).forEach(tr => {
            tr.remove();
        });
    }

    function loadPackageLotList() {
        cleanPackageLotList();
        let fiscalYearId = getFiscalYearId();
        if (fiscalYearId === '-1') return;
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let packageLotList = JSON.parse(this.responseText);
                setDataToPackageLotTable(packageLotList.msg);
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("GET", "Pi_package_item_mapServlet?actionType=getItemListByPackageLot&fiscalYearId=" + fiscalYearId, false);
        xhttp.send();
    }

    function setDataToPackageLotTable(packageLotList) {
        let modalBody = document.getElementById('modal-package-lot-body');
        let tableRow = document.getElementById('template-modal-package-lot-row');
        packageLotList = JSON.parse(packageLotList);
        removeAllChildNodes(modalBody);
        for (let row of packageLotList) {
            let clonedRow = tableRow.cloneNode(true);
            clonedRow.classList.remove('hiddenTr');
            clonedRow.querySelector('#hidden_package_item_map_id').value = row['piPackageItemMapId'];
            clonedRow.querySelector('#hidden_package_id').value = row['piPackageFinalId'];
            clonedRow.querySelector('#hidden_lot_id').value = row['piLotFinalId'];
            clonedRow.querySelector('#hidden_package_number').value = getValueByLanguage(language, row['packageNumberBn'], row['packageNumberEn']);
            clonedRow.querySelector('#hidden_package_name').value = getValueByLanguage(language, row['packageNameBn'], row['packageNameEn']);
            clonedRow.querySelector('#hidden_lot_number').value = getValueByLanguage(language, row['lotNumberBn'], row['lotNumberEn']);
            clonedRow.querySelector('#hidden_lot_name').value = getValueByLanguage(language, row['lotNameBn'], row['lotNameEn']);
            clonedRow.querySelector('#hidden_quantity_en').value = row['quantityEn'];
            clonedRow.querySelector('#hidden_quantity_bn').value = row['quantityBn'];
            clonedRow.querySelector('#hidden_est_cost').value = row['estCost'];
            clonedRow.querySelector('#hidden_est_cost_text').value = row['estCostText'];
            clonedRow.querySelector('#hidden_total_price').value = row['totalPrice'];
            clonedRow.querySelector('#hidden_fiscal_year').value = row['fiscalYearId'];
            clonedRow.querySelector('#serial_number_').innerText = getDigitByLanguage(language, row['serialNumber']);
            clonedRow.querySelector('#package_').innerText = getValueByLanguage(language, row['packageNumberBn'], row['packageNumberEn']);
            clonedRow.querySelector('#lot_').innerText = getValueByLanguage(language, row['lotNumberBn'], row['lotNumberEn']);
            modalBody.append(clonedRow);
        }
    }

    function removeAllChildNodes(parent) {
        while (parent.firstChild) {
            parent.removeChild(parent.firstChild);
        }
    }

    function constructPackage(row) {
        let packageNo = row.querySelector("input[id='hidden_package_number']").value +
            ' (' + row.querySelector("input[id='hidden_lot_number']").value + ')';
        let description = row.querySelector("input[id='hidden_package_name']").value +
            row.querySelector("input[id='hidden_lot_name']").value;
        return {
            "package-item-map-id": row.querySelector("input[id='hidden_package_item_map_id']").value,
            "package-id": row.querySelector("input[id='hidden_package_id']").value,
            "lot-id": row.querySelector("input[id='hidden_lot_id']").value,
            "package-no": packageNo,
            "description": description,
            "quantity-en": row.querySelector("input[id='hidden_quantity_en']").value,
            "quantity-bn": row.querySelector("input[id='hidden_quantity_bn']").value,
            "est-cost": row.querySelector("input[id='hidden_est_cost']").value,
            "est-cost-text": row.querySelector("input[id='hidden_est_cost_text']").value,
            "total-price": row.querySelector("input[id='hidden_total_price']").value
        };
    }

    function loadSelectedPackageLot() {
        let modalBody = document.getElementById('modal-package-lot-body');
        selectedPackageLot.length = 0;
        [...modalBody.children].forEach(row => {
            let checkBox = row.querySelector("td>input[name='checkbox']");
            if (checkBox && checkBox.value === "true") {
                selectedPackageLot.push(constructPackage(row));
            }
        });
    }

    function submitSelectedPackage() {
        loadSelectedPackageLot();
        if (packageLotModal.passModalData) {
            packageLotModal.passModalData(selectedPackageLot);
        }
        $('#pi_app_package_lot_modal').modal('hide');
    }
</script>