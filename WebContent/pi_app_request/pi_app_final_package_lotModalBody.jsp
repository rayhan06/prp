<%@ page import="util.UtilCharacter" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page import="pb.Utils" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>

<div id="drop_down" class="">
    <div class="">
        <div id="ajax-content">
            <div class="mt-4">
                <div class="form-body">
                    <h5 class="table-title">

                    </h5>
                    <div class="form-header">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label text-md-right">
                                <%=LM.getText(LC.PI_APP_REQUEST_ADD_FISCALYEARID, loginDTO)%>
                                <span class="required"> * </span>
                            </label>

                            <div class="col-md-9">
                                <select class='form-control' name='modalFiscalYearId' id='modalFiscalYearId'
                                        tag='pb_html' onchange="loadPackageLotList()">
                                    <%
                                        Fiscal_yearDTO cur_fis_dto = new Fiscal_yearDAO().getFiscalYearBYDateLong(System.currentTimeMillis());

                                        String fiscalYearOptions = Utils.buildSelectOption(isLanguageEnglish);
                                        StringBuilder option = new StringBuilder();
                                        for (Fiscal_yearDTO fiscal_yearDTO : fiscal_yearDTOS) {
                                            if (cur_fis_dto.id == fiscal_yearDTO.id) {
                                                option.append("<option value = '").append(fiscal_yearDTO.id).append("' selected>");
                                            } else {
                                                option.append("<option value = '").append(fiscal_yearDTO.id).append("'>");
                                            }

                                            option.append(isLanguageEnglish ? fiscal_yearDTO.nameEn : fiscal_yearDTO.nameBn).append("</option>");
                                        }
                                        fiscalYearOptions += option.toString();
                                    %>
                                    <%=fiscalYearOptions%>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="modal-product-view-table">
                            <thead>
                            <th style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "ক্রমিক নং", "Serial Number")%>
                            </th>
                            <th style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "প্যাকেজ", "Package")%>
                            </th>
                            <th style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "লট", "Lot")%>
                            </th>
                            <th style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "সিলেক্ট করুন", "Select")%>
                            </th>
                            </thead>
                            <tbody id="modal-package-lot-body">

                            </tbody>
                            <tr class="hiddenTr" id="template-modal-package-lot-row">
                                <input type="hidden" id="hidden_package_item_map_id" value="-1">
                                <input type="hidden" id="hidden_package_id" value="-1">
                                <input type="hidden" id="hidden_lot_id" value="-1">
                                <input type="hidden" id="hidden_package_number" value="">
                                <input type="hidden" id="hidden_package_name" value="">
                                <input type="hidden" id="hidden_lot_number" value="">
                                <input type="hidden" id="hidden_lot_name" value="">
                                <input type="hidden" id="hidden_quantity_en" value="">
                                <input type="hidden" id="hidden_quantity_bn" value="">
                                <input type="hidden" id="hidden_est_cost" value="">
                                <input type="hidden" id="hidden_est_cost_text" value="">
                                <input type="hidden" id="hidden_total_price" value="">
                                <input type="hidden" id="hidden_fiscal_year" value="-1">
                                <td id="serial_number_"></td>
                                <td id="package_"></td>
                                <td id="lot_"></td>
                                <td>
                                    <input type="checkbox" name="checkbox" id="checkbox_" onclick="toggle(this)">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .hiddenTr {
        display: none;
    }
</style>

<script>
    function toggle(element) {
        if (element.value) {
            if (element.value === "true") element.value = false;
            else element.value = true;
        } else {
            element.value = true;
        }
    }
</script>