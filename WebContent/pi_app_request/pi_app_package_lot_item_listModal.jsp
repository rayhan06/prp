<%@page import="sessionmanager.SessionConstants" %>
<%@page import="user.UserRepository" %>
<%@page import="user.UserDTO" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.CommonConstant" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
    String modalTitle = request.getParameter("modalTitle");
    if (modalTitle == null) {
        modalTitle = UtilCharacter.getDataByLanguage(Language, "আইটেম লিস্ট", "Item List");
    }
%>

<div class="modal fade bd-example-modal-xl" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="pi_app_package_lot_item_list_modal" data-backdrop="static"
     data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h4 class="caption" style="color: #0098bf;">
                    <%=modalTitle%>
                </h4>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body modal-lg">
                <%@include file="pi_app_package_lot_item_listModalBody.jsp" %>
            </div>

            <%--------------------------------FOOTER----------------------------------------------%>
            <div class="modal-footer border-0">
                <button type="button" class="btn submit-btn text-white shadow btn-border-radius"
                        id="item-list-modal-submit" onclick="saveApproverData()">
                    <%=LM.getText(LC.GLOBAL_SUBMIT, loginDTO)%>
                </button>
                <button type="button" class="btn cancel-btn text-white shadow btn-border-radius"
                        onclick="closeItemListModal()">
                    <%=LM.getText(LC.CLOSE_CLOSE, loginDTO)%>
                </button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    let language = "<%=Language%>";
    let approveLevelGlobal = 0;

    $('#pi_app_package_lot_item_list_modal').on('show.bs.modal', function () {
        event.preventDefault();
    });

    function closeItemListModal() {
        $('#pi_app_package_lot_item_list_modal').modal('hide');
    }

    function loadItemListOfPackageLot(appDetailsId, approveLevel, isReadOnly = false) {
        approveLevelGlobal = approveLevel;
        let url = "Pi_app_request_package_lot_item_listServlet?actionType=getApproverItemList&appDetailsId=" + appDetailsId;

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let packageLotList = JSON.parse(this.responseText);
                setDataToItemListTable(packageLotList.msg, approveLevel, isReadOnly);
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("GET", url, false);
        xhttp.send();
    }

    function setDataToItemListTable(itemList, approveLevel, isReadOnly) {
        let modalBody = document.getElementById('item-list-body');
        let tableRow = document.getElementById('template-item-list-row');
        itemList = JSON.parse(itemList);
        removeAllChildNodes(modalBody);
        for (let item of itemList) {
            let approverData = getApproverData(item, approveLevel);
            let clonedRow = tableRow.cloneNode(true);
            clonedRow.classList.remove('hiddenTr');
            setNameModal(clonedRow);
            setValueModal(clonedRow, item, approverData, isReadOnly);
            modalBody.append(clonedRow);
        }
    }

    function setNameModal(clonedRow){
        clonedRow.querySelector('#iD').name = 'itemPackageLot.iD';
        clonedRow.querySelector('#modal_item_quantity').name = 'itemPackageLot.quantity';
        clonedRow.querySelector('#modal_item_unit_price').name = 'itemPackageLot.unitPrice';
        clonedRow.querySelector('#modal_item_total_price').name = 'itemPackageLot.totalPrice';

    }

    function setValueModal(clonedRow, item, approverData, isReadOnly){
        if(isReadOnly) {
            setValueOnReadMode(clonedRow, item, approverData);
            hideSubmitButton();
        } else {
            setValueOnEditMode(clonedRow, item, approverData);
            showSubmitButton();
        }
    }

    function setValueOnReadMode(clonedRow, item, approverData){
        clonedRow.querySelector('#iD').value = item['iD'];
        clonedRow.querySelector('#iD').readOnly = true;
        clonedRow.querySelector('#modal_item_serial_').innerText = item['serialNumber'];
        clonedRow.querySelector('#modal_item_description_').innerText = item['itemIdText'];
        clonedRow.querySelector('#modal_item_office_unit_name_').innerText = item['officeUnitIdText'];
        clonedRow.querySelector('#modal_item_quantity').value = approverData.quantity;
        clonedRow.querySelector('#modal_item_quantity').readOnly = true;
        clonedRow.querySelector('#modal_item_unit_name').innerText = item['unitIdText'];
        clonedRow.querySelector('#modal_item_unit_price').value = approverData.unitPrice;
        clonedRow.querySelector('#modal_item_unit_price').readOnly = true;
        clonedRow.querySelector('#modal_item_total_price').value = approverData.totalPrice;
        clonedRow.querySelector('#modal_item_total_price').readOnly = true;
    }

    function setValueOnEditMode(clonedRow, item, approverData){
        clonedRow.querySelector('#iD').value = item['iD'];
        clonedRow.querySelector('#modal_item_serial_').innerText = item['serialNumber'];
        clonedRow.querySelector('#modal_item_description_').innerText = item['itemIdText'];
        clonedRow.querySelector('#modal_item_office_unit_name_').innerText = item['officeUnitIdText'];
        clonedRow.querySelector('#modal_item_quantity').value = approverData.quantity;
        clonedRow.querySelector('#modal_item_unit_name').innerText = item['unitIdText'];
        clonedRow.querySelector('#modal_item_unit_price').value = approverData.unitPrice;
        clonedRow.querySelector('#modal_item_total_price').value = approverData.totalPrice;
    }

    function removeAllChildNodes(parent) {
        while (parent.firstChild) {
            parent.removeChild(parent.firstChild);
        }
    }

    function getApproverData(item, approveLevel) {
        if (approveLevel === 1) return getApproverOneData(item);
        else if (approveLevel === 2) return getApproverTwoData(item);
        else if (approveLevel === 3) return getApproverThreeData(item);
    }

    function getApproverOneData(item) {
        let addFlag = (item ["approverOneQuantity"] === -1 && item ["approverOneUnitPrice"] === -1 && item ["approverOneEstimatedTotalPrice"] === -1);
        if (addFlag) {
            return makeObject(item ["requestedQuantity"], item ["requestedUnitPrice"], item ["requestedEstimatedTotalPrice"]);
        } else {
            return makeObject(item ["approverOneQuantity"], item ["approverOneUnitPrice"], item ["approverOneEstimatedTotalPrice"]);
        }
    }

    function getApproverTwoData(item) {
        let addFlag = (item ["approverTwoQuantity"] === -1 && item ["approverTwoUnitPrice"] === -1 && item ["approverTwoEstimatedTotalPrice"] === -1);
        if (addFlag) {
            return makeObject(item ["approverOneQuantity"], item ["approverOneUnitPrice"], item ["approverOneEstimatedTotalPrice"]);
        } else {
            return makeObject(item ["approverTwoQuantity"], item ["approverTwoUnitPrice"], item ["approverTwoEstimatedTotalPrice"]);
        }
    }

    function getApproverThreeData(item) {
        let addFlag = (item ["approverThreeQuantity"] === -1 && item ["approverThreeUnitPrice"] === -1 && item ["approverThreeEstimatedTotalPrice"] === -1);
        if (addFlag) {
            return makeObject(item ["approverTwoQuantity"], item ["approverTwoUnitPrice"], item ["approverTwoEstimatedTotalPrice"]);
        } else {
            return makeObject(item ["approverThreeQuantity"], item ["approverThreeUnitPrice"], item ["approverThreeEstimatedTotalPrice"]);
        }
    }

    function makeObject(quantity, unitPrice, totalPrice) {
        return {
            quantity: quantity,
            unitPrice: unitPrice,
            totalPrice: totalPrice,
        }
    }

    function calculateTotalPrice(element) {
        let tr = element.parentNode.parentNode;
        let quantity = tr.querySelector("#modal_item_quantity").value;
        let unitPrice = tr.querySelector("#modal_item_unit_price").value;
        let totalPrice = quantity * unitPrice;
        if (totalPrice !== "")
            tr.querySelector("#modal_item_total_price").value = totalPrice;
    }

    function saveApproverData() {
        let form = $("#item-list-modal-form");
        closeItemListModal();
        fullPageLoader.show();
        $.ajax({
            type: "POST",
            url: "Pi_app_request_package_lot_item_listServlet?actionType=saveApproverData&approveLevel=" + approveLevelGlobal,
            data: form.serialize(),
            dataType: 'JSON',
            success: function (response) {
                if (response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                    buttonStateChange(false);
                } else if (response.responseCode === 200) {
                    showToastSticky("সাবমিট সফল হয়েছে", "Submit Successful");
                }
                fullPageLoader.hide();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChange(false);
                fullPageLoader.hide();
            }
        });
    }

    function showSubmitButton(){
        let submitButton = document.getElementById('item-list-modal-submit');
        submitButton.classList.remove('hiddenTr');
    }

    function hideSubmitButton(){
        let submitButton = document.getElementById('item-list-modal-submit');
        submitButton.classList.add('hiddenTr');
    }
</script>