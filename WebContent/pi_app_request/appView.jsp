<%@ page import="util.UtilCharacter" %>
<%@ page import="util.HttpRequestUtils" %>
<%@page pageEncoding="UTF-8" %>
<%
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    String formName = UtilCharacter.getDataByLanguage(Language, "বার্ষিক ক্রয় পরিকল্পনা", "ANNUAL PURCHASE PLAN");
%>
<jsp:include page="../common/layout.jsp" flush="true">
    <jsp:param name="title" value="<%=formName%>" />
    <jsp:param name="body" value="../pi_app_request/appViewBody.jsp" />
</jsp:include>