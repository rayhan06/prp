<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="recruitment_job_description.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@page pageEncoding="UTF-8" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="vm_maintenance.BillRegisterItemDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="org.apache.commons.codec.language.bm.Lang" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="pi_app_request.DataByFiscalYearDTO" %>


<%

    List<DataByFiscalYearDTO> data = (List<DataByFiscalYearDTO>) request.getAttribute("data");
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    String value = "";


%>

<div class="table-responsive mt-5">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead class="thead-light">
        <tr>
            <th>
                <%=UtilCharacter.getDataByLanguage(Language, "অর্থবছর ", "Fiscal Year")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "মোট প্যাকেজ ", "Total Package")%></th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "মোট পণ্য ", "Total Item")%></th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "মোট খরচ", "Total Estimated Cost")%></th>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>

        </tr>
        </thead>
        <tbody>
        <%


            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        DataByFiscalYearDTO dto = data.get(i);


        %>
        <tr id='tr_<%=i%>'>


            <td>
                <%=dto.fiscalYearName%>
            </td>

            <td>
                <%
                    value = Utils.getDigits(dto.packageCount, Language);
                %>
                <%=value%>
            </td>

            <td>
                <%
                    value = Utils.getDigits((int)dto.itemCount, Language);
                %>
                <%=value%>
            </td>

            <td>
                <%
                    value = Utils.getDigits(String.format("%.2f", dto.cost), Language);
                %>
                <%=value%>
            </td>

            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ff6b6b;"
                        onclick="location.href='Pi_app_requestServlet?actionType=appView&ID=<%=dto.fiscalYearId%>'"
                >
                    <i class="fa fa-eye"></i>
                </button>
            </td>


        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>


			