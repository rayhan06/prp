<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_vehicle_office_assignment.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentRepository" %>

<%
    String servletName = "Vm_vehicle_office_assignmentServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Vm_vehicle_office_assignmentDTO vm_vehicle_office_assignmentDTO = Vm_vehicle_office_assignmentRepository.getInstance().
            getVm_vehicle_office_assignmentDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important; margin-bottom: -18px">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_ADD_VM_VEHICLE_OFFICE_ASSIGNMENT_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>

        <div class="kt-portlet__body form-body">
            <div class="">
                <h5 class="table-title">
                    <%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_ADD_VM_VEHICLE_OFFICE_ASSIGNMENT_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_ADD_VEHICLETYPECAT, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicle_office_assignmentDTO.vehicleTypeCat + "";
                                %>
                                <%
                                    value = CatRepository.getName(Language, "vehicle_type", vm_vehicle_office_assignmentDTO.vehicleTypeCat);
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_ADD_VEHICLEID, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vm_vehicle_office_assignmentDTO.vehicleId);
                                    value = "";
                                    if (vm_vehicleDTO != null) {
                                        value = vm_vehicleDTO.regNo;
                                    }
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_ADD_OFFICEID, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = "";
                                    Office_unitsDTO unit = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(vm_vehicle_office_assignmentDTO.officeId);
                                    if (unit != null) {
                                        value = UtilCharacter.getDataByLanguage(Language, unit.unitNameBng, unit.unitNameEng);
                                    }
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>

                        <tr>
                            <td style="width:30%">
                                <b><%=UtilCharacter.getDataByLanguage(Language, "শুরুর তারিখ", "Start Date")%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicle_office_assignmentDTO.start_time + "";
                                %>
                                <%
                                    String formatted_dateOfExam = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_dateOfExam, Language)%>


                            </td>

                        </tr>

                        <tr>
                            <td style="width:30%">
                                <b><%=UtilCharacter.getDataByLanguage(Language, "শেষের তারিখ", "End Date")%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicle_office_assignmentDTO.end_time + "";
                                %>
                                <%
                                    formatted_dateOfExam = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_dateOfExam, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_ADD_REMARKS, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicle_office_assignmentDTO.remarks + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div class="modal-content viewmodal"> -->
