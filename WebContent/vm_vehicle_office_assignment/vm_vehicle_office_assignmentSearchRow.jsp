<%@page pageEncoding="UTF-8" %>

<%@page import="vm_vehicle_office_assignment.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="offices.OfficesRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_VM_VEHICLE_OFFICE_ASSIGNMENT;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Vm_vehicle_office_assignmentDTO vm_vehicle_office_assignmentDTO = (Vm_vehicle_office_assignmentDTO) request.getAttribute("vm_vehicle_office_assignmentDTO");
    CommonDTO commonDTO = vm_vehicle_office_assignmentDTO;
    String servletName = "Vm_vehicle_office_assignmentServlet";


    System.out.println("vm_vehicle_office_assignmentDTO = " + vm_vehicle_office_assignmentDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Vm_vehicle_office_assignmentDAO vm_vehicle_office_assignmentDAO = (Vm_vehicle_office_assignmentDAO) request.getAttribute("vm_vehicle_office_assignmentDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td id='<%=i%>_vehicleTypeCat'>
    <%
        value = vm_vehicle_office_assignmentDTO.vehicleTypeCat + "";
    %>
    <%
        value = CatRepository.getName(Language, "vehicle_type", vm_vehicle_office_assignmentDTO.vehicleTypeCat);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_vehicleId'>
    <%
        Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vm_vehicle_office_assignmentDTO.vehicleId);
        value = "";
        if (vm_vehicleDTO != null) {
            value = vm_vehicleDTO.regNo;
        }
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_officeId'>
    <%
        value = "";
        Office_unitsDTO unit = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(vm_vehicle_office_assignmentDTO.officeId);
        if (unit != null) {
            value = UtilCharacter.getDataByLanguage(Language, unit.unitNameBng, unit.unitNameEng);
        }


    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<%--											<td id = '<%=i%>_remarks'>--%>
<%--											<%--%>
<%--											value = vm_vehicle_office_assignmentDTO.remarks + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="view('<%=vm_vehicle_office_assignmentDTO.iD%>')">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="edit('<%=vm_vehicle_office_assignmentDTO.iD%>')">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'>
			<input type='checkbox' name='ID' value='<%=vm_vehicle_office_assignmentDTO.iD%>'/>
		</span>
    </div>
</td>
																						
											

