<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_vehicle_office_assignment.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="util.UtilCharacter" %>

<%
    Vm_vehicle_office_assignmentDTO vm_vehicle_office_assignmentDTO;
    vm_vehicle_office_assignmentDTO = (Vm_vehicle_office_assignmentDTO) request.getAttribute("vm_vehicle_office_assignmentDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (vm_vehicle_office_assignmentDTO == null) {
        vm_vehicle_office_assignmentDTO = new Vm_vehicle_office_assignmentDTO();

    }
    System.out.println("vm_vehicle_office_assignmentDTO = " + vm_vehicle_office_assignmentDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_ADD_VM_VEHICLE_OFFICE_ASSIGNMENT_ADD_FORMNAME, loginDTO);
    String servletName = "Vm_vehicle_office_assignmentServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    String nameOfOffice = "";
    if (actionName.equalsIgnoreCase("edit")) {
        Office_unitsDTO unit = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(vm_vehicle_office_assignmentDTO.officeId);
        if (unit != null) {
            nameOfOffice = UtilCharacter.getDataByLanguage(Language, unit.unitNameBng, unit.unitNameEng);
        }
    }

    int year = Calendar.getInstance().get(Calendar.YEAR);
%>

<style>
    .required {
        color: red;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Vm_vehicle_office_assignmentServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="formSubmit()">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD'
                                           id='iD_hidden_<%=i%>'
                                           value='<%=vm_vehicle_office_assignmentDTO.iD%>'
                                           tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_ADD_VEHICLETYPECAT, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='vehicleTypeCat'
                                                    id='vehicleTypeCat_category_<%=i%>'
                                                    onchange="loadVehicleList()" tag='pb_html'>
                                                <%
                                                    Options = CatRepository.getOptions(Language, "vehicle_type", vm_vehicle_office_assignmentDTO.vehicleTypeCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row" id="vehicleIdDiv">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_ADD_VEHICLEID, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='vehicleId'
                                                    id='givenVehicleId_select_<%=i%>' tag='pb_html'>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "শুরুর তারিখ", "Start Date")%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID"
                                                           value="start-date-js"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                                <jsp:param name="END_YEAR" value="<%=year + 20%>"/>
                                            </jsp:include>

                                            <input type='hidden' class='form-control'
                                                   id='startDate_<%=i%>'
                                                   name='startDate' value='' tag='pb_html'/>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "শেষের তারিখ", "End Date")%>
                                        </label>
                                        <div class="col-md-9">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID"
                                                           value="end-date-js"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                                <jsp:param name="END_YEAR" value="<%=year + 20%>"/>
                                            </jsp:include>

                                            <input type='hidden' class='form-control'
                                                   id='endDate_<%=i%>'
                                                   name='endDate' value='' tag='pb_html'/>

                                        </div>
                                    </div>
                                    <%--														<input type='hidden' class='form-control'  name='vehicleId' id = 'vehicleId_hidden_<%=i%>' value='<%=vm_vehicle_office_assignmentDTO.vehicleId%>' tag='pb_html'/>--%>
                                    <%--														<input type='hidden' class='form-control'  name='officeId' id = 'officeId_hidden_<%=i%>' value='<%=vm_vehicle_office_assignmentDTO.officeId%>' tag='pb_html'/>--%>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_ADD_REMARKS, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                                            <textarea class='form-control' name='remarks'
                                                                      id='remarks_text_<%=i%>'><%=actionName.equals("edit") ? (vm_vehicle_office_assignmentDTO.remarks) : (" ")%></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_ADD_OFFICEID, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>

                                        <div class="col-8 col-md-7">
                                            <input type="hidden" name='officeId'
                                                   id='office_units_id_input' value="">
                                            <button type="button" class="btn btn-secondary form-control shadow btn-border-radius"
                                                    disabled id="office_units_id_text"></button>
                                        </div>
                                        <div class="col-4 col-md-2">
                                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                                    id="office_units_id_modal_button"
                                                    onclick="officeModalButtonClicked();">
                                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                                            </button>
                                        </div>


                                    </div>
                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=vm_vehicle_office_assignmentDTO.insertedByUserId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=vm_vehicle_office_assignmentDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=vm_vehicle_office_assignmentDTO.insertionDate%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value='<%=vm_vehicle_office_assignmentDTO.isDeleted%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=vm_vehicle_office_assignmentDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=vm_vehicle_office_assignmentDTO.searchColumn%>'
                                           tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 form-actions text-right mt-3">
                        <a id="cancel-btn" class="btn btn-sm cancel-btn text-white shadow btn-border-radius"
                           href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_ADD_VM_VEHICLE_OFFICE_ASSIGNMENT_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button id="submit-btn" class="btn  btn-sm submit-btn text-white shadow btn-border-radius ml-2"
                                type="submit"><%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_ADD_VM_VEHICLE_OFFICE_ASSIGNMENT_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">

    function dateValidation() {
        $('#startDate_0').val(getDateStringById('start-date-js'));
        $('#endDate_0').val(getDateStringById('end-date-js'));

        return dateValidator('start-date-js', true, {
            errorEn: "শুরুর তারিখ নির্বাচন করুন ",
            errorBn: "Select Start Date",
        });
    }

    function officeValidation() {
        let flag = false;
        let officeValue = $('#office_units_id_input').val();
        if (officeValue && officeValue.length > 0) {
            flag = true;
        } else {
            let errMsg = '';
            if (lang == 'english') {
                errMsg = 'Please select office';
            } else {
                errMsg = 'দপ্তর নির্বাচন করুন ';
            }
            toastr.error(errMsg);
        }


        return flag;
    }

    function formSubmitAfterValidation() {
        buttonStateChange(true);
        let form = $("#bigform");
        let url = form.attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            dataType: 'JSON',
            success: function (response) {
                if (response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                    buttonStateChange(false);
                } else if (response.responseCode === 200) {
                    window.location.replace(getContextPath() + response.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChange(false);
            }
        });
    }

    function formSubmit() {
        event.preventDefault();
        let officeValid = officeValidation();
        let dateValid = dateValidation();

        let form = $("#bigform");
        form.validate();
        let valid = form.valid();

        if (valid && officeValid && dateValid) {
            // formSubmitAfterValidation();
            let vehicleId = $("#givenVehicleId_select_0").val();
            let id = '<%=vm_vehicle_office_assignmentDTO.iD%>';
            let url = "Vm_vehicle_office_assignmentServlet?actionType=getValidationByVehicleId&vehicleId="
                + vehicleId + "&type=" + actionName + "&iD=" + id;
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    if (fetchedData) {
                        formSubmitAfterValidation();
                    } else {
                        let errMsg1 = "Selected vehicle already assigned to another office."
                        let errMsg1Bn = "নির্বাচিত গাড়ি ইতিমধ্যে অন্য অফিসে নির্বাচিত রয়েছে";
                        let errMsg2 = "Do you want to assign it for this office?";
                        let errMsg2Bn = "আপনি কি এই অফিসের জন্যে নির্বাচিত করতে চান?";
                        messageDialog(valueByLanguage(lang, errMsg1Bn, errMsg1),
                            valueByLanguage(lang, errMsg2Bn, errMsg2), 'warning', true, valueByLanguage(lang, "হ্যাঁ", "Yes"), valueByLanguage(lang, "না", "No"),
                            formSubmitAfterValidation, h2);
                    }

                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }

    function valueByLanguage(lang, bnValue, enValue) {
        if (lang == 'english' || lang == 'English') {
            return enValue;
        } else {
            return bnValue;
        }
    }

    function h2() {
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }


    function init() {
        if (actionName == 'edit') {
            loadVehicleList();
            viewOfficeIdInInput({
                name: '<%=nameOfOffice%>',
                id: <%=vm_vehicle_office_assignmentDTO.officeId%>
            });
            setDateByTimestampAndId('start-date-js', <%=vm_vehicle_office_assignmentDTO.start_time%>);
            setDateByTimestampAndId('end-date-js', <%=vm_vehicle_office_assignmentDTO.end_time%>);
        } else {
            setDateByTimestampAndId('start-date-js', <%=System.currentTimeMillis()%>);
            setDateByTimestampAndId('end-date-js', <%=System.currentTimeMillis()%>);
        }

        $('#start-date-js').on('datepicker.change', () => {
            setMinDateById('end-date-js', getDateStringById('start-date-js'));
        });


    }

    let row = 0;
    let actionName = '<%=actionName%>';
    let vehicleId = '<%=vm_vehicle_office_assignmentDTO.vehicleId%>';
    let lang = '<%=Language%>';
    $(document).ready(function () {
        init();

        $.validator.addMethod('validSelector', function (value, element) {
            return value && value != -1;
        });


        let errVehicleTypeCat;
        let errVehicle;

        if (lang == 'english') {
            errVehicleTypeCat = 'Please select vehicle type';
            errVehicle = 'Please select vehicle';


        } else {
            errVehicleTypeCat = 'যানবাহনের ধরণ নির্বাচন করুন ';
            errVehicle = 'যানবাহন নির্বাচন করুন ';

        }

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                vehicleTypeCat: {
                    required: true,
                    validSelector: true,
                },
                vehicleId: {
                    required: true,
                    validSelector: true,
                },
            },
            messages: {
                vehicleTypeCat: errVehicleTypeCat,
                vehicleId: errVehicle,

            }
        });
    });

    function loadVehicleList() {

        let typeValue = document.getElementById('vehicleTypeCat_category_0').value;
        let url = "Vm_vehicleServlet?actionType=getByVehicleTypeForAssignment&type=" + typeValue;
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                // const response = JSON.parse(fetchedData);
                $("#givenVehicleId_select_0").html(fetchedData);
                if (actionName == 'edit') {
                    $("#givenVehicleId_select_0").val(vehicleId);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });

    }


    // Related to office selector modal

    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        // console.log(selectedOffice);
        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id_input').val(selectedOffice.id);
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    function officeModalButtonClicked() {
        // console.log('Button Clicked!');
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }


</script>






