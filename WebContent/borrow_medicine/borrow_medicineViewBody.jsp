

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="borrow_medicine.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>
<%@page import="drug_information.*"%>
<%@page import="medical_equipment_name.*"%>



<%
String servletName = "Borrow_medicineServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Borrow_medicineDTO borrow_medicineDTO = Borrow_medicineDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = borrow_medicineDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>
<%
String formTitle = "";
if(borrow_medicineDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_DRUG)
{
	formTitle = LM.getText(LC.HM_BORROW, loginDTO) + " " + LM.getText(LC.HM_DRUGS, loginDTO);
}
else if(borrow_medicineDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_EQUIPMENT)
{
	formTitle = LM.getText(LC.HM_BORROW, loginDTO) + " " + LM.getText(LC.HM_EQUIPMENT, loginDTO);
}
String context = request.getContextPath() + "/";
boolean isDeliveryFromShop = false;
if(request.getAttribute("isDeliveryFromShop") != null)
{
	isDeliveryFromShop = (boolean)request.getAttribute("isDeliveryFromShop");
}
%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
    .signature-image {
        border-bottom: 2px solid #a406dc !important;
        width: 200px !important;
        height: 53px !important;
    }

    .signature-div {

        font-size: 10px !important;
    }

    .signature-div * {
        font-size: 10px !important;
    }
</style>

<div class="ml-auto mr-2 mt-4">
	<button type="button" class="btn" id='pdf'
            onclick="downloadPdf('requisition.pdf','modalbody')">
        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
    <button type="button" class="btn" id='printer'
            onclick="printAnyDiv('modalbody')">
        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
</div>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<div class="kt-content prescription-details-report-page-for-browser" id="kt_content">
    <div class="kt-portlet" id="modalbody" style="background-color: #f2f2f2!important;">
        <div class="kt-portlet__body m-3" style="background-color: #f6f9fb!important;">
            <table class="w-100 mt-5">
                <tr>
                    <td align="center">
                        <img
                                class="parliament-logo"
                                width="8%"
                                src="<%=context%>assets/images/perliament_logo_final2.png"
                                alt=""
                        />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <p class="mb-0"><%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <a class="website-link" href="www.parliament.gov.bd"
                        >www.parliament.gov.bd</a
                        >
                    </td>
                </tr>
            </table>
            <div class="mb-5">
                <table class="w-100 mt-5">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="pt-1">
                                        <img width="65%" src="<%=context%>assets/images/prp_logo.png"
                                             alt="parliament logo"/>
                                    </td>
                                    <td>
                                        <table style="margin-left: -60px">
                                            <tr>
                                                <td>
                                                  
                                                   <%=LM.getText(LC.HM_BORROW, loginDTO) + " " + LM.getText(LC.HM_EQUIPMENT, loginDTO)%>
                                                    <p class="mb-0">prp.parliament.gov.bd</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="right">
                            <table>
                                <tr>
                                    <td>
                                        <p class="mb-0"><%=Utils.capitalizeFirstLettersOfWords(LM.getText(LC.HM_PARLIAMENT_MEDICAL_CENTRE, loginDTO))%>
                                        </p>
                                        <p class="mb-0"><%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                                        </p>
                                        <p class="mb-0"><%=LM.getText(LC.HM_PARLIAMENT_PHONE, loginDTO)%>
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                
                  <div class="border-top border-dark mt-3"></div>
                <table class="w-100 mt-4">
                    <tr>
                        <td style = "width:33%; padding-bottom:1.5rem; vertical-align:top">
                            
                                <strong><%=Language.equalsIgnoreCase("english")?"Application Date":"আবেদনের তারিখ"%>
                                    :</strong> <%=Utils.getDigits(simpleDateFormat.format(new Date(borrow_medicineDTO.insertionDate)), Language)%>
                            
                        </td>
                    
                        <td style = "width:33%; padding-bottom:1.5rem; vertical-align:top">
                            
                                <strong><%=Language.equalsIgnoreCase("english")?"Reference Employee":"রেফারেন্স"%>
                                    :</strong> <%=WorkflowController.getNameFromUserName(borrow_medicineDTO.borrowedForUserName, Language)%><br>
                                    <%=WorkflowController.getOrganogramName(borrow_medicineDTO.organogramId, Language)%><br>
                                	<%=WorkflowController.getUnitNameFromOrganogramId(borrow_medicineDTO.organogramId, Language)%>
                                <br><strong><%=Language.equalsIgnoreCase("english")?"Borrower":"প্রাপক"%>:</strong>
								<%=borrow_medicineDTO.borrowedForName%><br> <%=Utils.getDigits(borrow_medicineDTO.borrowedForPhone, Language)%>
                                    
                            
                        </td>
						
						<td style = "width:34%; padding-bottom:1.5rem; vertical-align:top">
								<strong><%=Language.equalsIgnoreCase("english")?"Entried By":"এন্ট্রিকর্তা"%>:</strong>
								<%=WorkflowController.getNameFromUserId(borrow_medicineDTO.insertedByUserId, Language)%><br>
                                    <%=WorkflowController.getOrganogramName(borrow_medicineDTO.insertedByOrganogramId, Language)%><br>
                                	<%=WorkflowController.getUnitNameFromOrganogramId(borrow_medicineDTO.insertedByOrganogramId, Language)%>
								
						</td>
					</tr>
					
					<%
					if(borrow_medicineDTO.approvalStatus != 0)
					{
					%>
					
					<tr>
					 	<td style = "width:33%; padding-bottom:1.5rem; vertical-align:top">
                            
                                <strong><%=Language.equalsIgnoreCase("english")?"Approval/Rejection Date":"অনুমোদন/বাতিলের তারিখ"%>
                                    :</strong> <%=Utils.getDigits(simpleDateFormat.format(new Date(borrow_medicineDTO.approvalDate)), Language)%>
                            
                        </td>
                        
                        <td style = "width:33%; padding-bottom:1.5rem; vertical-align:top">
                            
                                <strong><%=Language.equalsIgnoreCase("english")?"Approval Status":"অনুমোদনের অবস্থা"%>
                                    :</strong> <%=Utils.getApprovalStatus(borrow_medicineDTO.approvalStatus, Language)%>
                            
                        </td>
                        
                        <td style = "width:34%; padding-bottom:1.5rem; vertical-align:top">
                            
                                <strong><%=Language.equalsIgnoreCase("english")?"Action taken By":"পদক্ষেপগ্রহীতা"%>
                                    :</strong>  <%=WorkflowController.getNameFromUserName(borrow_medicineDTO.approvedByUserName, Language)%><br>
                                    <%=WorkflowController.getOrganogramName(borrow_medicineDTO.approvedByUserName, Language)%>
                                    <br><%=WorkflowController.getUnitNameFromUserName(borrow_medicineDTO.approvedByUserName, Language)%>
                            
                        </td>
                    
                       
                        
                       
                    </tr>
                    <%
					}
                    %>
					
					<%
					if(borrow_medicineDTO.isDelivered)
					{
					%>
					<tr>
                        
                        <td style = "width:33%; padding-bottom:1.5rem; vertical-align:top">
                            
                                <strong><%=Language.equalsIgnoreCase("english")?"Delivery Date":"ডেলিভারির  তারিখ"%>
                                    :</strong> <%=Utils.getDigits(simpleDateFormat.format(new Date(borrow_medicineDTO.sellingDate)), Language)%>
                            
                        </td>
                        
                        <td style = "width:33%; padding-bottom:1.5rem; vertical-align:top">
                            
                                <strong><%=Language.equalsIgnoreCase("english")?"Delivered By":"সরবরাহকারী"%>
                                    :</strong>  <%=WorkflowController.getNameFromUserName(borrow_medicineDTO.sellerUserName, Language)%><br>
                                    <%=WorkflowController.getOrganogramName(borrow_medicineDTO.sellerUserName, Language)%>
                                    <br><%=WorkflowController.getUnitNameFromUserName(borrow_medicineDTO.sellerUserName, Language)%>
                            
                        </td>
                    
                        <td style = "width:34%; padding-bottom:1.5rem; vertical-align:top">
                            
                                <strong><%=Language.equalsIgnoreCase("english")?"Received By":"গ্রাহক"%>
                                    :</strong> <%=borrow_medicineDTO.borrowerName%><br><%=Utils.getDigits(borrow_medicineDTO.borrowerPhone, Language)%>
                            
                        </td>
                        
                       
                    </tr>
					<%
					}
					%>
                </table>
                
                <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DEPARTMENT_REQUISITION_ITEM, loginDTO)%></h5>
                        <table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.HM_TYPE, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_NAME, loginDTO)%></th>
								<th><%=LM.getText(LC.BORROW_MEDICINE_ADD_BORROW_MEDICINE_DETAILS_QUANTITY, loginDTO)%></th>
								
							</tr>
							<%
                        	BorrowMedicineDetailsDAO borrowMedicineDetailsDAO = BorrowMedicineDetailsDAO.getInstance();
                         	List<BorrowMedicineDetailsDTO> borrowMedicineDetailsDTOs = (List<BorrowMedicineDetailsDTO>)borrowMedicineDetailsDAO.getDTOsByParent("borrow_medicine_id", borrow_medicineDTO.iD);

                         	for(BorrowMedicineDetailsDTO borrowMedicineDetailsDTO: borrowMedicineDetailsDTOs)
                         	{
                         		%>
                         			<tr>
                         				<td>
											<%
											if(borrowMedicineDetailsDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_EQUIPMENT)
											{
												Medical_equipment_nameDTO medical_equipment_nameDTO = Medical_equipment_nameRepository.getInstance().getMedical_equipment_nameDTOByID(borrowMedicineDetailsDTO.drugInformationType);
												value = CatDAO.getName(Language, "medical_equipment", medical_equipment_nameDTO.medicalEquipmentCat);
											}
											%>
										
										
											<%=value%>
				
			
										</td>
										
										<td>
											<%
											if(borrowMedicineDetailsDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_DRUG)
											{
												Drug_informationDTO drug_informationDTO = Drug_informationRepository.getInstance().getDrug_informationDTOByID(borrowMedicineDetailsDTO.drugInformationType);
	
												value = CommonDAO.getName((int) drug_informationDTO.medicineGenericNameType, "medicine_generic_name",  "name_en", "id");
		                                        value += " (" + drug_informationDTO.nameEn + "," + drug_informationDTO.strength + ")";
											}
											else if(borrowMedicineDetailsDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_EQUIPMENT)
											{
												Medical_equipment_nameDTO medical_equipment_nameDTO = Medical_equipment_nameRepository.getInstance().getMedical_equipment_nameDTOByID(borrowMedicineDetailsDTO.drugInformationType);
												value = medical_equipment_nameDTO.nameEn;
											}
											%>
										
										
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = borrowMedicineDetailsDTO.quantity + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										
										
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
					</div>                    
                </div>
                 <%
		        if(isDeliveryFromShop)
		        {
		        %>
		        
		        <form class="form-horizontal"
              		action="Borrow_medicineServlet?actionType=confirmDelivery&id=<%=borrow_medicineDTO.iD%>"
              		id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              		onsubmit="return PreprocessBeforeSubmiting(0,'')"
              		>
              		 <div class="kt-portlet__body form-body">
		                <div class="row mb-4">
		                    <div class="col-md-8 offset-md-2">
		                        <div class="onlyborder">
		                            <div class="row">
		                                <div class="col-md-10 offset-md-1">
		                                    <div class="sub_title_top">
		                                        <div class="sub_title">
													<div class="form-group row">
														<label class="col-md-4 col-form-label text-md-right"><%=Language.equalsIgnoreCase("english") ? "Received By" : "গ্রাহক"%>
														</label>
														<div class="col-md-8">
															<button type="button"
																class="btn text-white shadow form-control btn-border-radius"
																style="background-color: #4a87e2"
																onclick="addEmployeeWithRow('employeeToSet_button_1')"
																id="employeeToSet_button_1"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
															</button>
															<table class="table table-bordered table-striped">
																<tbody id="employeeToSet_table_1"></tbody>
															</table>
															<input type='hidden' class='form-control'
																name='borrowedByUserName' id='borrowedByUserName' value=''
																tag='pb_html' />
														</div>
													</div>
									
													<div class="form-group row">
														<label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.NURSE_ACTION_ADD_NAME, loginDTO)%></label>
														<div class="col-md-8">
															<input type='text' class='form-control' name='name' id='name'
																value='' tag='pb_html' />
														</div>
													</div>
													<div class="form-group row">
														<label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.NURSE_ACTION_ADD_PHONE, loginDTO)%></label>
														<div class="col-md-8">
															<div class="input-group mb-2">
																<div class="input-group-prepend">
																	<div class="input-group-text"><%=Language.equalsIgnoreCase("english") ? "+88" : "+৮৮"%></div>
																</div>
																<input type='text' class='form-control' required name='phone'
																	id='phone' value='' tag='pb_html'>
															</div>
														</div>
													</div>
													<div class="form-actions text-right mb-2 mt-4">
									                    
									                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit" 
									                    >
									                        <%=LM.getText(LC.HM_DELIVER, loginDTO)%>
									                    </button>
									                </div>
								              	</div>
							              	</div>
						              	</div>
					              	</div>
             					</div>
							</div>
						</div>
					</div>
                </form>

		        <%
		        	}
		        %>
		        <%
		        	if (borrow_medicineDTO.approvalStatus == 1) {
		        %>
		     
                <div class=" mt-2">
                    <div class="w-100 d-flex justify-content-end">
		                <div class="signature-div text-center">
		                    <div>
		                        <%
			                    byte [] encodeBase64Photo = WorkflowController.getBase64SignatureFromUserName(borrow_medicineDTO.approvedByUserName);
			                    %>
			                    <img src='data:image/jpg;base64,<%=encodeBase64Photo != null ? new String(encodeBase64Photo) : ""%>' style="width:150px"
			                                     id="signature-img">
		                        <br>

		                    </div>
		                    <div>
		                        <%=WorkflowController.getNameFromUserName(borrow_medicineDTO.approvedByUserName, Language)%><br>
		                        <%
		                        long approverOrgId = WorkflowController.getOrganogramIDFromUserName(borrow_medicineDTO.approvedByUserName);
		                        %>
		                        <%=WorkflowController.getOrganogramName(approverOrgId, Language)%><br>
		                        <%=WorkflowController.getOfficeNameFromOrganogramId(approverOrgId, Language)%><br>
		                    </div>
		                </div>
		            </div>
		        </div>
                <%
		        }
                %>
             </div>
		</div>
	</div>
</div>  
                
<script type="text/javascript">
function employee_inputted(value) {
    console.log('changed value: ' + value);
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
        	fillUserInfo(this.responseText)
        } else {
            //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
        }
    };

    xhttp.open("POST", "FamilyServlet?actionType=getEmployeeInfo&userName=" + value + "&language=<%=Language%>", true);
    xhttp.send();
}


function fillUserInfo(responseText)
{
	$("#empdiv").css("display", "block");
    var userNameAndDetails = JSON.parse(responseText);
    console.log("name = " + userNameAndDetails.name);
    $("#name").val(userNameAndDetails.name);      
    $("#phone").val(phoneNumberRemove88ConvertLanguage(userNameAndDetails.phone, '<%=Language%>'));
   
}

function patient_inputted(userName, orgId) {
	console.log("table = " + modal_button_dest_table + " username = " + userName);
	if(modal_button_dest_table == "employeeToSet_table_1")
		{
		$("#borrowedByUserName").val(userName);
		employee_inputted(userName);
		}
	else
	{
		$("#borrowedForUserName").val(userName);
	}
}

function PreprocessBeforeSubmiting(row, action)
{
	var convertedPhoneNumber = phoneNumberAdd88ConvertLanguage($('#phone').val(), '<%=Language%>');
	if($("#phone").val() == "" || $("#name").val() == "")
	{
		toastr.error("Name or Phone number cannot be empty.");
		return false
	}
	$("#phone").val(convertedPhoneNumber);
	
}
</script>