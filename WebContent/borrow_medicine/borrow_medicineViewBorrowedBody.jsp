

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="borrow_medicine.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>
<%@page import="drug_information.*"%>
<%@page import="medical_equipment_name.*"%>



<%
String servletName = "Borrow_medicineServlet";
String userName = (String)request.getAttribute("userName");
%>
<%@include file="../pb/viewInitializer.jsp"%>
<%
int medicalItemType = SessionConstants.MEDICAL_ITEM_DRUG;
if(request.getParameter("medicalItemType") != null)
{
	medicalItemType= Integer.parseInt(request.getParameter("medicalItemType"));
}

String formTitle = LM.getText(LC.HM_PERSONAL_STOCK, loginDTO);
%>

<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=formTitle%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.HM_NAME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=WorkflowController.getNameFromUserName(userName, Language)%>
				
			
                                    </div>
                                </div>
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%
                            if(medicalItemType == SessionConstants.MEDICAL_ITEM_DRUG)
                            {
                            	%>
                            	<%=LM.getText(LC.HM_DRUGS, loginDTO)%>
                            	<%
                            }
                            else if(medicalItemType == SessionConstants.MEDICAL_ITEM_EQUIPMENT)
                            {
                            	%>
                            	<%=LM.getText(LC.HM_EQUIPMENT, loginDTO)%>
                            	<%
                            }
                            %>
                         </h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.HM_NAME, loginDTO)%></th>
								<th><%=LM.getText(LC.BORROW_MEDICINE_ADD_BORROW_MEDICINE_DETAILS_QUANTITY, loginDTO)%></th>
							</tr>
							<%
                        	PersonalStockDAO personalStockDAO = PersonalStockDAO.getInstance();
                         	List<PersonalStockDTO> personalStockDTOs = (List<PersonalStockDTO>)personalStockDAO.getDTOsByDrId(userName, medicalItemType);
                         	Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
                         	for(PersonalStockDTO personalStockDTO: personalStockDTOs)
                         	{
                         		%>
                         			<tr>
										
										<td>
											<%
											if(personalStockDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_DRUG)
											{
												Drug_informationDTO drug_informationDTO = drug_informationDAO.getDrugInfoDetailsById(personalStockDTO.drugInformationType);
	
												value = drug_informationDTO.drugText;
											}
											else if(personalStockDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_EQUIPMENT)
											{
												Medical_equipment_nameDTO medical_equipment_nameDTO = Medical_equipment_nameRepository.getInstance().getMedical_equipment_nameDTOByID(personalStockDTO.drugInformationType);
												value = medical_equipment_nameDTO.nameEn;
											}
											%>
										
										
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = personalStockDTO.quantity + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										
									
										
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
        </div>
    </div>
</div>