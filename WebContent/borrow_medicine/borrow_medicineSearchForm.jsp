
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="borrow_medicine.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>

<%@page pageEncoding="UTF-8" %>
<%
String navigator2 = "navBORROW_MEDICINE";
String servletName = "Borrow_medicineServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				

				<input type = "hidden" id = "titleText"	value = "<%=LM.getText(LC.HM_BORRROWED, loginDTO)%> <%=LM.getText(LC.HM_ITEM, loginDTO)%>" />	
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.HM_SL, loginDTO)%>
            					</th>
								<th><%=LM.getText(LC.HM_DATE, loginDTO)%></th>
								<th><%=Language.equalsIgnoreCase("english")?"Reference Employee":"রেফারেন্স কর্মকর্তা"%></th>	
								<th><%=Language.equalsIgnoreCase("english")?"Borrowed For":"প্রাপক"%></th>	
								<th><%=Language.equalsIgnoreCase("english")?"Received By":"গ্রাহক"%></th>	
								<th><%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_ISAPPROVED, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>
								<th><%=Language.equalsIgnoreCase("english")?"Personal Stock":"ব্যক্তিগত স্টক"%></th>										
								
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Borrow_medicineDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Borrow_medicineDTO borrow_medicineDTO = (Borrow_medicineDTO) data.get(i);
																																
											
											%>
											<tr>
											<td>
												<%=Utils.getDigits(i + 1 + ((rn2.getCurrentPageNo() - 1) * rn2.getPageSize()), Language) %>
											</td>
											
											
											<td>

				
											<%=Utils.getDigits(simpleDateFormat.format(new Date(borrow_medicineDTO.insertionDate)), Language)%>
				
			
											</td>
								
		
											<td>

				
											<%=WorkflowController.getNameFromUserName(borrow_medicineDTO.borrowedForUserName, Language)%>,
											<%=WorkflowController.getOrganogramName(borrow_medicineDTO.organogramId, Language)%>,
                                			<%=WorkflowController.getUnitNameFromOrganogramId(borrow_medicineDTO.organogramId, Language)%>
				
			
											</td>
											
											<td>
											<%=borrow_medicineDTO.borrowedForName%>
											<br>
											<%=Utils.getDigits(borrow_medicineDTO.borrowedForPhone, Language)%>
				
			
											</td>
		
										
		
											<td>
											<%=borrow_medicineDTO.borrowerName%>
											<br>
											<%=Utils.getDigits(borrow_medicineDTO.borrowerPhone, Language)%>
				
			
											</td>
		
											<td>
											<%=Utils.getApprovalStatus(borrow_medicineDTO.approvalStatus, Language)%>
											
											<%
											if(borrow_medicineDTO.approvalMessage != null && !borrow_medicineDTO.approvalMessage.equalsIgnoreCase(""))
											{
												%>
												<br><%=borrow_medicineDTO.approvalMessage%>
												<%
											}
											%>
											</td>

	
											<%CommonDTO commonDTO = borrow_medicineDTO; %>
											<td>
											    <button
											            type="button"
											            class="btn-sm border-0 shadow bg-light btn-border-radius"
											            style="color: #ff6b6b;"
											            onclick="location.href='<%=servletName%>?actionType=view&ID=<%=commonDTO.iD%>'"
											    >
											        <i class="fa fa-eye"></i>
											    </button>
											    
											     <%
											    if(borrow_medicineDTO.approvalStatus == 0 && (userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE 
														|| userDTO.roleID == SessionConstants.ADMIN_ROLE ))
											    {
											    %>
											    <button
											            type="button"
											            class="btn-sm border-0 shadow bg-light btn-border-radius"
											            style="color: #ff6b6b;"
											            onclick="location.href='<%=servletName%>?actionType=getApprovalPage&ID=<%=commonDTO.iD%>'"
											    >
											        <i class="fa fa-stamp"></i>
											    </button>
											    <%
											    }
											    %>
											    
											    <%
											    if(borrow_medicineDTO.approvalStatus == 1 && !borrow_medicineDTO.isDelivered && 
											    (userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.NURSE_ROLE ))
											    {
											    %>
											    <button
											            type="button"
											            class="btn-sm border-0 shadow bg-light btn-border-radius"
											            style="color: #886b6b;"
											            onclick="location.href='<%=servletName%>?actionType=deliverFromShop&ID=<%=commonDTO.iD%>'"
											    >
											        <i class="fa fa-plus"></i>
											    </button>
											    <%
											    }
											    %>
											</td>
											
											<td>
												<button
											            type="button"
											            class="btn-sm border-0 shadow bg-light btn-border-radius"
											            style="color: #997b7b;"
											            onclick="location.href='<%=servletName%>?actionType=viewBorrowed&userName=<%=borrow_medicineDTO.borrowedForUserName%>&medicalItemType=<%=SessionConstants.MEDICAL_ITEM_EQUIPMENT%>'"
											    >
											        <i class="fa fa-eye"></i>
											    </button>
											    
											    <%
											    if(userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.NURSE_ROLE)
											    {
											    %>
											    <button
											            type="button"
											            class="btn-sm border-0 shadow bg-light btn-border-radius"
											            style="color: #997b7b;"
											            onclick="location.href='Deliver_or_return_medicineServlet?actionType=getAddPage&userName=<%=borrow_medicineDTO.borrowedForUserName%>&medicalItemType=<%=SessionConstants.MEDICAL_ITEM_EQUIPMENT%>'"
											    >
											        <i class="fas fa-minus"></i>
											    </button>
											    <%
											    }
											    %>
											</td>	
											
											
																						
																						
											
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />

<script type="text/javascript">
$(document).ready(function(){	
	$("#subHeader").html($("#titleText").val());
});

</script>

			