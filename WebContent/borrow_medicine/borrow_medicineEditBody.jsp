<%@page import="com.sun.xml.internal.ws.addressing.model.ActionNotSupportedException" %>
<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="borrow_medicine.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>

<%
    Borrow_medicineDTO borrow_medicineDTO = new Borrow_medicineDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        borrow_medicineDTO = Borrow_medicineDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = borrow_medicineDTO;
    String tableName = "borrow_medicine";

%>
<%@include file="../pb/addInitializer2.jsp" %>
<%

    String servletName = "Borrow_medicineServlet";
    if (actionName.equalsIgnoreCase("ajax_add")) {
        borrow_medicineDTO.medicalItemType = Integer.parseInt(request.getParameter("medicalItemType"));
    }

    String formTitle = "";
    if (borrow_medicineDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_DRUG) {
        formTitle = LM.getText(LC.HM_BORROW, loginDTO) + " " + LM.getText(LC.HM_DRUGS, loginDTO);
    } else if (borrow_medicineDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_EQUIPMENT) {
        formTitle = LM.getText(LC.HM_BORROW, loginDTO) + " " + LM.getText(LC.HM_EQUIPMENT, loginDTO);
    }

%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <jsp:include page="../employee_assign/employeeSearchModal.jsp">
            <jsp:param name="isHierarchyNeeded" value="false"/>
        </jsp:include>
        <form class="form-horizontal"
              action="Borrow_medicineServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>


                                        <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                               value='<%=borrow_medicineDTO.iD%>' tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='medicalItemType'
                                               id='medicalItemType_text_<%=i%>'
                                               value='<%=borrow_medicineDTO.medicalItemType%>' tag='pb_html'/>

                                        <%
                                            if (userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE
                                            		|| userDTO.roleID == SessionConstants.NURSE_ROLE
                                            ) {
                                        %>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=Language.equalsIgnoreCase("english")?"Borrowed For":"প্রাপক"%>
                                            </label>
                                            <div class="col-md-8">
                                                 <button type="button"
                                                        class="btn text-white shadow form-control btn-border-radius"
                                                        style="background-color: #4a87e2"
                                                        onclick="addEmployeeWithRow('employeeToSet_button_0')"
                                                        id="employeeToSet_button_0"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
                                                </button>
                                                 <table class="table table-bordered table-striped">
                                                    <tbody id="employeeToSet_table_0"></tbody>
                                                </table> 
                                                <input type='hidden' class='form-control' name='borrowedForUserName'
                                               		id='borrowedForUserName'
                                               		value='' tag='pb_html'/>                                                   
                                            </div>
                                        </div>
                                        
                                     	<div class="form-group row">
											<label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.NURSE_ACTION_ADD_NAME, loginDTO)%></label>
											<div class="col-md-8">
												<input type='text' class='form-control' name='name' id='name'
													value='' tag='pb_html' />
											</div>
										</div>
										<div class="form-group row">
											<label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.NURSE_ACTION_ADD_PHONE, loginDTO)%></label>
											<div class="col-md-8">
												<div class="input-group mb-2">
													<div class="input-group-prepend">
														<div class="input-group-text"><%=Language.equalsIgnoreCase("english") ? "+88" : "+৮৮"%></div>
													</div>
													<input type='text' class='form-control' required name='phone'
														id='phone' value='' tag='pb_html'>
												</div>
											</div>
										</div>
                                        
                                  
										<%
											}
                                            else 
                                            {
										%>
                                        <input type="hidden" name='doctorId' value="<%=userDTO.organogramID%>"/>
                                        <%
                                        	}
                                        %>
                                        <div class="form-group row" style = "display:none">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.HM_LOT, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='stockLotNumber' 
                                                       id='stockLotNumber_text_<%=i%>'
                                                       value='<%=borrow_medicineDTO.stockLotNumber%>' tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.BORROW_MEDICINE_ADD_REMARKS, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='remarks'
                                                       id='remarks_text_<%=i%>' value='<%=borrow_medicineDTO.remarks%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%
                                if (borrow_medicineDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_DRUG) {
                            %>
                            <%=LM.getText(LC.HM_DRUGS, loginDTO)%>
                            <%
                            } else if (borrow_medicineDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_EQUIPMENT) {
                            %>
                            <%=LM.getText(LC.HM_EQUIPMENT, loginDTO)%>
                            <%
                                }
                            %>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped text-nowrap">
                                <thead>
                                <tr>
                                	<th><%=LM.getText(LC.HM_TYPE, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.HM_NAME, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.BORROW_MEDICINE_ADD_BORROW_MEDICINE_DETAILS_QUANTITY, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.BORROW_MEDICINE_ADD_BORROW_MEDICINE_DETAILS_REMOVE, loginDTO)%>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="field-BorrowMedicineDetails">


                                <%
                                    if (actionName.equals("ajax_edit")) {
                                        int index = -1;


                                        for (BorrowMedicineDetailsDTO borrowMedicineDetailsDTO : borrow_medicineDTO.borrowMedicineDetailsDTOList) {
                                            index++;

                                            System.out.println("index index = " + index);

                                %>

                                <tr id="BorrowMedicineDetails_<%=index + 1%>">
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control' name='borrowMedicineDetails.iD'
                                               id='iD_hidden_<%=childTableStartingID%>'
                                               value='<%=borrowMedicineDetailsDTO.iD%>' tag='pb_html'/>

                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='borrowMedicineDetails.borrowMedicineId'
                                               id='borrowMedicineId_hidden_<%=childTableStartingID%>'
                                               value='<%=borrowMedicineDetailsDTO.borrowMedicineId%>' tag='pb_html'/>
                                    </td>

                                    <td>
                                    <%
                                    	if (borrow_medicineDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_EQUIPMENT)
                                    	{
                                    		%>
                                    		<select class='form-control' name='medicalInventoryIn.medicalEquipmentCat'
                                            id='equipmentCat_category_' tag='pb_html' onchange = 'fillEquipemnts(this.id)'>
	                                        <%
	                                           
	                                                Options = CatDAO.getOptions(Language, "medical_equipment", -1);
	                                            
	                                        %>
	                                        <%=Options%>
	                                    	</select>
	
		                                    <select class='form-control mitem w-100'
		                                            name='medicalInventoryIn.medicalEquipmentNameType'
		                                            id='medicalEquipmentNameType_select_' tag='pb_html'>
		                                        <%
		                                            
		                                                Options = CommonDAO.getEquipments(-1);
		                                            
		                                        %>
		                                        <%=Options%>
		                                    </select>
                                    		<%
                                    	}
                                    	else
                                    	{
                                    		%>
                                    		<select class='form-control' name='borrowMedicineDetails.drugInformationType'
                                                id='drugInformationType_select_<%=childTableStartingID%>' tag='pb_html'>
                                           
                                        	</select>
                                    		<%
                                    	
                                    	}
                                    %>

                                        

                                    </td>

                                    <td>

                                        <%
                                            value = "";
                                            if (borrowMedicineDetailsDTO.quantity != -1) {
                                                value = borrowMedicineDetailsDTO.quantity + "";
                                            }
                                        %>
                                        <input type='number' class='form-control' name='borrowMedicineDetails.quantity'
                                               id='quantity_number_<%=childTableStartingID%>' value='<%=value%>'
                                               tag='pb_html'>
                                    </td>

                                    <td>
										<span id='chkEdit'>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
                                                   class="form-control-sm"/>
										</span>
                                    </td>
                                </tr>
                                <%
                                            childTableStartingID++;
                                        }
                                    }
                                %>

                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-9 text-right">
                                <button
                                        id="add-more-BorrowMedicineDetails"
                                        name="add-moreBorrowMedicineDetails"
                                        type="button"
                                        class="btn btn-sm text-white add-btn shadow">
                                    <i class="fa fa-plus"></i>
                                    <%=LM.getText(LC.HM_ADD, loginDTO)%>
                                </button>
                                <button
                                        id="remove-BorrowMedicineDetails"
                                        name="removeBorrowMedicineDetails"
                                        type="button"
                                        class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        </div>

                        <%BorrowMedicineDetailsDTO borrowMedicineDetailsDTO = new BorrowMedicineDetailsDTO();%>

                        <template id="template-BorrowMedicineDetails">
                            <tr>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='borrowMedicineDetails.iD'
                                           id='iD_hidden_' value='<%=borrowMedicineDetailsDTO.iD%>' tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='borrowMedicineDetails.borrowMedicineId' id='borrowMedicineId_hidden_'
                                           value='<%=borrowMedicineDetailsDTO.borrowMedicineId%>' tag='pb_html'/>
                                </td>
                                <td>
                                	<%
                                    	if (borrow_medicineDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_EQUIPMENT)
                                    	{
                                    		%>
                                    		<select class='form-control' name='borrowMedicineDetails.medicalEquipmentCat'
                                            id='equipmentCat_category_' tag='pb_html' onchange = 'fillEquipemnts(this.id)'>
		                                        <%
		                                           
		                                                Options = CatDAO.getOptions(Language, "medical_equipment", -1);
		                                            
		                                        %>
	                                        	<%=Options%>
	                                    	</select>
                                    		<%
                                    	}
                                    		%>
                                </td>

                                <td>
                                	<%
                                		if (borrow_medicineDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_EQUIPMENT)
                                    	{
                                    		%>
                                    		
	
		                                    <select class='form-control mitem w-100'
		                                            name='borrowMedicineDetails.drugInformationType'
		                                            id='medicalEquipmentNameType_select_' tag='pb_html'>
		                                        <%
		                                            
		                                                Options = CommonDAO.getEquipments(-1);
		                                            
		                                        %>
		                                        <%=Options%>
		                                    </select>
                                    		<%
                                    	}
                                    	else
                                    	{
                                    		%>
                                
                                				<div class="input-group">
											        <input
											          class="form-control py-2  border searchBox w-auto"
											          type="search"
											          placeholder="Search"
											          name='suggested_drug'
                                                      id='suggested_drug_'
                                                      tag='pb_html'
                                                      onkeyup="getDrugs(this.id)"
                                                      autocomplete="off"
											        />
											        
											      </div>
											      <div
											        id="searchSgtnSection_"
											        class="search-sgtn-section shadow-sm bg-white"
													tag='pb_html'
											      >
											        <div class="">
											          <ul class="px-0" style="list-style: none" id="drug_ul_" tag='pb_html'>
											          </ul>
											        </div>
											      </div>
											      
											      <input name='formStr'
                                                   id='formStr_' type="hidden"
                                                   tag='pb_html'
                                                   value=''></input>
                                                   
                                     				<input name='borrowMedicineDetails.drugInformationType'
                                                   id='drugInformationType_select_' type="hidden"
                                                   tag='pb_html'
                                                   value='<%=borrowMedicineDetailsDTO.drugInformationType%>'></input>
                                	
													<div name="drug_modal_textdiv"
				                                         id="drug_modal_textdiv_"
				                                         tag='pb_html'>
				                                    </div>
				                       <%
                                    	}
				                       %>

                                   

                                </td>

                                <td>

                                    <%
                                        value = "";
                                        if (borrowMedicineDetailsDTO.quantity != -1) {
                                            value = borrowMedicineDetailsDTO.quantity + "";
                                        }
                                    %>
                                    <input type='number' class='form-control' name='borrowMedicineDetails.quantity'
                                           id='quantity_number_' value='<%=value%>' tag='pb_html'>
                                </td>
                                <td>
									<span id='chkEdit'>
										<input type='checkbox' name='checkbox' value='' deletecb='true'
											   class="form-control-sm"/>
									</span>
                                </td>
                            </tr>

                        </template>
                    </div>
                </div>
                <div class="form-actions text-center mb-2 mt-5">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.BORROW_MEDICINE_ADD_BORROW_MEDICINE_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.BORROW_MEDICINE_ADD_BORROW_MEDICINE_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, action) {
        console.log("action = " + action);
        
        var convertedPhoneNumber = phoneNumberAdd88ConvertLanguage($('#phone').val(), '<%=Language%>');
    	if($("#phone").val() == "" || $("#name").val() == "")
    	{
    		toastr.error("Name or Phone number cannot be empty.");
    		return false
    	}
    	$("#phone").val(convertedPhoneNumber);

        for (i = 1; i < child_table_extra_id; i++) {
            if (document.getElementById("transactionDate_date_" + i)) {
                if (document.getElementById("transactionDate_date_" + i).getAttribute("processed") == null) {
                    preprocessDateBeforeSubmitting('transactionDate', i);
                    document.getElementById("transactionDate_date_" + i).setAttribute("processed", "1");
                }
            }
        }
        var drugs = [];
        var invalidFound = false;
        $("[name= 'borrowMedicineDetails.drugInformationType']").each(function (index) {
            if (drugs.includes($(this).val())) {
                toastr.error("Duplicate Medicine is not allowed");
                invalidFound = true;

            } else if ($(this).val() == -1) {
                toastr.error("Invalid Medicine");
                invalidFound = true;

            } else {
                drugs.push($(this).val());
            }


        });
        if (!invalidFound) {
            submitAddForm2();
        }

        return false;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Borrow_medicineServlet");
    }

    function init(row) {


        for (i = 1; i < child_table_extra_id; i++) {
            setDateByStringAndId('transactionDate_js_' + i, $('#transactionDate_date_' + i).val());
        }

    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })

       
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-BorrowMedicineDetails").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-BorrowMedicineDetails");

            $("#field-BorrowMedicineDetails").append(t.html());
            SetCheckBoxValues("field-BorrowMedicineDetails");

            var tr = $("#field-BorrowMedicineDetails").find("tr:last-child");

            tr.attr("id", "BorrowMedicineDetails_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                console.log(index + ": " + $(this).attr('id'));
            });



            child_table_extra_id++;

        });


    $("#remove-BorrowMedicineDetails").click(function (e) {
        var tablename = 'field-BorrowMedicineDetails';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[deletecb="true"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
    });


    
    function drugClicked(rowId, drugText, drugId, form, stock)
    {
    	console.log("drugClicked with " + rowId + ", drugId = " + drugId + ", form = " + form);
    	$("#suggested_drug_" + rowId).val(drugText);
    	$("#suggested_drug_" + rowId).attr("style", "width: " + ($("#suggested_drug_" + rowId).val().length + 1)*8 + "px !important");
    	$("#drug_ul_" + rowId).html("");
		$("#drugInformationType_select_" + rowId).val(drugId);
		$("#formStr_" + rowId).val(form);
		$("#formStr_" + rowId).val(form);
		
		var text = stock + " pieces in stock";
        $('#quantity_number_' + rowId).attr("max", stock);


        <%
        if(Language.equalsIgnoreCase("bangla"))
        {
        %>
        text = stock + " পিস আছে স্টকে";
        text = text.getDigitBanglaFromEnglish();
        <%
        }
        %>
         
        $("#drug_modal_textdiv_" + rowId).html(text);

    }
    
    
    
    function patient_inputted(userName, orgId) {
    	console.log("table = " + modal_button_dest_table + " username = " + userName);
    	
   		$("#borrowedForUserName").val(userName);
   		employee_inputted(userName);
    	
    }
    
    function employee_inputted(value) {
        console.log('changed value: ' + value);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
            	fillUserInfo(this.responseText)
            } else {
                //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
            }
        };

        xhttp.open("POST", "FamilyServlet?actionType=getEmployeeInfo&userName=" + value + "&language=<%=Language%>", true);
        xhttp.send();
    }


    function fillUserInfo(responseText)
    {
    	$("#empdiv").css("display", "block");
        var userNameAndDetails = JSON.parse(responseText);
        console.log("name = " + userNameAndDetails.name);
        $("#name").val(userNameAndDetails.name);      
        $("#phone").val(phoneNumberRemove88ConvertLanguage(userNameAndDetails.phone, '<%=Language%>'));
       
    }
    
   
	
	function fillEquipemnts(id)
	{
		var rowId = id.split("_")[2];
		var eqNameId = "medicalEquipmentNameType_select_" + rowId;
		var medicalDeptCat = -1;
		var medicalEquipmentCat = $("#" + id).val();
		
		console.log("medicalDeptCat = " + medicalDeptCat + " medicalEquipmentCat = " + medicalEquipmentCat )
		
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {
				$("#" + eqNameId).html(this.responseText);
			} else if (this.readyState == 4 && this.status != 200) {
				alert('failed ' + this.status);
			}
		};

		xhttp.open("GET",  "Medical_equipment_nameServlet?actionType=getEquipments&medicalDeptCat=" + medicalDeptCat + "&medicalEquipmentCat=" + medicalEquipmentCat, true);
		xhttp.send();
	}



</script>






