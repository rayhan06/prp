<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page contentType="text/html;charset=utf-8" %>
<%@ page import="pb.*" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.ASSET_DETAILS_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row mx-2">
    <div class="col-12">
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.ASSET_DETAILS_REPORT_WHERE_ASSETCATEGORYTYPE, loginDTO)%>
				</label>
				<div class="col-md-9">
					<select class='form-control'  name='assetCategoryType' id = 'assetCategoryType' onchange="getModels(this.value)">		
						<%		
						Options = CommonDAO.getOptions(Language, "asset_category", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.HM_MODEL, loginDTO)%>
				</label>
				<div class="col-md-9">
					
					
					<select class='form-control'  name='model' id = 'model' >		
						<option value = ''><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
					</select>							
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.HM_STATUS, loginDTO)%>
				</label>
				<div class="col-md-9">
					<select class='form-control'  name='assignmentStatus' id = 'assignmentStatus'>
						<option value = ''><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
						<option value = '1'><%=LM.getText(LC.HM_ASSIGNED, loginDTO)%></option>
						<option value = '-1'><%=LM.getText(LC.HM_FREE, loginDTO)%></option>
					</select>					
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.HM_SL, loginDTO)%>
				</label>
				<div class="col-md-9">
					<input class='form-control'  name='sl' id = 'sl' value=""/>							
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%>
				</label>
				<div class="col-md-9">
					<div>
		                <button type="button" class="btn btn-block submit-btn btn-border-radius text-white"
		                        onclick="addEmployee()"
		                        id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
		                </button>
		                <table class="table table-bordered table-striped">
		                    <tbody id="employeeToSet"></tbody>
		                </table>
		                <input class='form-control' type='hidden' name='employeeUnit' id='officeUnitType' value=''/>
		                <input class='form-control' type='hidden' name='assignedOrganogramId' id='assignedOrganogramId' value=''/>
		            </div>						
				</div>
			</div>
		</div>
		
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					 <%=Language.equalsIgnoreCase("english") ? "Room" : "কক্ষ"%>
				</label>
				<div class="col-md-9">
					 <select class='form-control' name='roomNoCat'
                              id='roomNoCat' tag='pb_html' onchange = "setOrgFromRoom(this.value)">
                          <%=CatRepository.getInstance().buildOptions("room_no", Language, -1)%>
                      </select>						
				</div>
			</div>
		</div>
        <%@include file="../pbreport/yearmonth.jsp"%>
        <%@include file="../pbreport/calendar.jsp"%>
    </div>
</div>
<script type="text/javascript">
function init()
{
    dateTimeInit($("#Language").val());
    addables = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
}
function PreprocessBeforeSubmiting()
{
}
function patient_inputted(userName, orgId) {
    console.log("patient_inputted " + userName);
    $("#assignedOrganogramId").val(orgId);
}

function getModels(category)
{
	fillSelect("model", "Asset_modelServlet?actionType=getModelsNameByCategory&cat=" + category);
}

function setOrgFromRoom(roomNo)
{
	if(roomNo == -1)
	{
		$("#assignedOrganogramId").val("");
	}
	else
	{
		var orgId = <%=SessionConstants.ROOM_OFFSET%> + parseInt(roomNo);
		$("#assignedOrganogramId").val(orgId);
	}
}
</script>