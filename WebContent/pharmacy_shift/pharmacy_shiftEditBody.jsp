<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="pharmacy_shift.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
Pharmacy_shiftDTO pharmacy_shiftDTO = new Pharmacy_shiftDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	pharmacy_shiftDTO = Pharmacy_shiftDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = pharmacy_shiftDTO;
String tableName = "pharmacy_shift";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.PHARMACY_SHIFT_ADD_PHARMACY_SHIFT_ADD_FORMNAME, loginDTO);
String servletName = "Pharmacy_shiftServlet";
actionName = "ajax_edit";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Pharmacy_shiftServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=pharmacy_shiftDTO.iD%>' tag='pb_html'/>

               <div class="mt-4">
                    <div class="form-body">
                       
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
									<tr>
										
										<th><%=LM.getText(LC.PHARMACY_SHIFT_ADD_PHARMACY_SHIFT_DETAILS_DAYCAT, loginDTO)%></th>
										<th><%=LM.getText(LC.PHARMACY_SHIFT_ADD_PHARMACY_SHIFT_DETAILS_ISOPEN, loginDTO)%></th>
										<th><%=LM.getText(LC.PHARMACY_SHIFT_ADD_PHARMACY_SHIFT_DETAILS_STARTTIME, loginDTO)%></th>
										<th><%=LM.getText(LC.PHARMACY_SHIFT_ADD_PHARMACY_SHIFT_DETAILS_ENDTIME, loginDTO)%></th>

									</tr>
								</thead>
							<tbody id="field-PharmacyShiftDetails">
						
						
								<%
									{
										int index = 0;
										
										
										while(index < 7)
										{
											
											PharmacyShiftDetailsDTO pharmacyShiftDetailsDTO = PharmacyShiftDetailsDAO.getInstance().getDTOByDay(index);
											if(pharmacyShiftDetailsDTO == null)
											{
												pharmacyShiftDetailsDTO = new PharmacyShiftDetailsDTO();
												System.out.println("null found for day = " + index);
											}
											else
											{
												System.out.println("not null found for day = " + index + " pharmacyShiftDetailsDTO.startTime = " + pharmacyShiftDetailsDTO.startTime);
											}
											
											System.out.println("index index = "+index);

								%>	
							
								<tr id = "PharmacyShiftDetails_<%=index + 1%>">
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='pharmacyShiftDetails.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=pharmacyShiftDetailsDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='pharmacyShiftDetails.pharmacyShiftId' id = 'pharmacyShiftId_hidden_<%=childTableStartingID%>' value='<%=pharmacyShiftDetailsDTO.pharmacyShiftId%>' tag='pb_html'/>
									</td>
									
									<td>										


														<%=CatDAO.getName(Language, "day", index) %>
														<input name='pharmacyShiftDetails.dayCat' value = '<%=index%>' type = 'hidden' />


	
									</td>
									<td>										





																<input type='checkbox' class='form-control-sm' name='pharmacyShiftDetails.isOpen_cb'
																onchange = "checkboxChecked(this.id)"
																 id = 'isOpen_checkbox_<%=childTableStartingID%>' value='true' 	<%=(String.valueOf(pharmacyShiftDetailsDTO.isOpen).equals("true"))?("checked"):""%>
   																tag='pb_html'>
   																<input type = "hidden" name='pharmacyShiftDetails.isOpen' id = "open_<%=childTableStartingID%>" value = "<%=pharmacyShiftDetailsDTO.isOpen%>" />
									</td>
									<td>										





																<%value = "startTime_js_" + childTableStartingID;%>
																<jsp:include page="/time/time.jsp">
																   <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
																   <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																   <jsp:param name="IS_AMPM" value="true"></jsp:param>
																</jsp:include>
																<input type='hidden' value="<%=TimeFormat.getInAmPmFormat(pharmacyShiftDetailsDTO.startTime)%>"
																 name='pharmacyShiftDetails.startTime' id = 'startTime_time_<%=childTableStartingID%>'  tag='pb_html'/>			
									</td>
									<td>										





																<%value = "endTime_js_" + childTableStartingID;%>
																<jsp:include page="/time/time.jsp">
																   <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
																   <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																   <jsp:param name="IS_AMPM" value="true"></jsp:param>
																</jsp:include>
																<input type='hidden' value="<%=TimeFormat.getInAmPmFormat(pharmacyShiftDetailsDTO.endTime)%>" 
																name='pharmacyShiftDetails.endTime' id = 'endTime_time_<%=childTableStartingID%>'  tag='pb_html'/>			
									</td>
									
								</tr>								
								<%	
											index++;
											childTableStartingID ++;
										}
									}
								%>						
						
								</tbody>
							</table>
						</div>

	                  </div>
                    </div>
                <div class="form-actions text-right mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.PHARMACY_SHIFT_ADD_PHARMACY_SHIFT_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.PHARMACY_SHIFT_ADD_PHARMACY_SHIFT_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>				

<script type="text/javascript">

function checkboxChecked(id)
{
	var rowId = id.split("_")[2];
	if($("#" + id).prop("checked"))
	{
		$("#open_" + rowId).val(true);
	}
	else
	{
		$("#open_" + rowId).val(false);
	}
}

function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);

	for(i = 1; i < child_table_extra_id; i ++)
	{
		
		if(document.getElementById("startTime_time_" + i))
		{
			if(document.getElementById("startTime_time_" + i).getAttribute("processed") == null)
			{
				preprocessTimeBeforeSubmitting('startTime', i);
				document.getElementById("startTime_time_" + i).setAttribute("processed","1");
			}
		}
		if(document.getElementById("endTime_time_" + i))
		{
			if(document.getElementById("endTime_time_" + i).getAttribute("processed") == null)
			{
				preprocessTimeBeforeSubmitting('endTime', i);
				document.getElementById("endTime_time_" + i).setAttribute("processed","1");
			}
		}
	}
	submitAddForm2();
	return false;
}


function init(row)
{


	for(i = 1; i < child_table_extra_id; i ++)
	{
		//preprocessTimeBeforeSubmitting('startTime', i);
	    //preprocessTimeBeforeSubmitting('endTime', i);
		setTimeById('startTime_js_' + i, $('#startTime_time_' + i).val(), true);  //Implement it after ajax callback, not supported by pb
		setTimeById('endTime_js_' + i, $('#endTime_time_' + i).val(), true);  //Implement it after ajax callback, not supported by pb
	}
	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;


</script>






