

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="pharmacy_shift.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Pharmacy_shiftServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Pharmacy_shiftDTO pharmacy_shiftDTO = Pharmacy_shiftDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = pharmacy_shiftDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PHARMACY_SHIFT_ADD_PHARMACY_SHIFT_ADD_FORMNAME, loginDTO)%>
                </h3>
                <div class="ml-auto mr-3">
				    <button type="button" class="btn" id='printer2'
				            onclick="location.href='Pharmacy_shiftServlet?actionType=getAddPage'">
				        <i class="fa fa-edit fa-2x" style="color: gray" aria-hidden="true"></i>
				    </button>			    			   
				</div>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
           		

             <div class="mt-5">
                <div class=" div_border attachement-div">
                     
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.PHARMACY_SHIFT_ADD_PHARMACY_SHIFT_DETAILS_DAYCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.PHARMACY_SHIFT_ADD_PHARMACY_SHIFT_DETAILS_ISOPEN, loginDTO)%></th>
								
								<th><%=LM.getText(LC.PHARMACY_SHIFT_ADD_PHARMACY_SHIFT_DETAILS_STARTTIME, loginDTO)%></th>
								<th><%=LM.getText(LC.PHARMACY_SHIFT_ADD_PHARMACY_SHIFT_DETAILS_ENDTIME, loginDTO)%></th>
							</tr>
							<%
                        	PharmacyShiftDetailsDAO pharmacyShiftDetailsDAO = PharmacyShiftDetailsDAO.getInstance();
                         	List<PharmacyShiftDetailsDTO> pharmacyShiftDetailsDTOs = (List<PharmacyShiftDetailsDTO>)pharmacyShiftDetailsDAO.getDTOsByParent("pharmacy_shift_id", pharmacy_shiftDTO.iD);
                         	
                         	for(PharmacyShiftDetailsDTO pharmacyShiftDetailsDTO: pharmacyShiftDetailsDTOs)
                         	{
                         		%>
                         			<tr>
                         				<td>
											<%=CatRepository.getInstance().getText(Language, "day", pharmacyShiftDetailsDTO.dayCat)%>
										</td>
										<td>
											<%=Utils.getYesNo(pharmacyShiftDetailsDTO.isOpen, Language)%>
										</td>
										
										<td>
											<%=Utils.getDigits(TimeFormat.getInAmPmFormat(pharmacyShiftDetailsDTO.startTime), Language)%>
										</td>
										<td>
											<%=Utils.getDigits(TimeFormat.getInAmPmFormat(pharmacyShiftDetailsDTO.endTime), Language)%>
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
        </div>
    </div>
</div>