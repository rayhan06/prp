<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="cheque_register.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>


<%
    String servletName = "Cheque_registerServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Cheque_registerDTO cheque_registerDTO = Cheque_registerDAO.getInstance().getDTOByID(id);
    CommonDTO commonDTO = cheque_registerDTO;
%>
<%@include file="../pb/viewInitializer.jsp" %>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.CHEQUE_REGISTER_ADD_CHEQUE_REGISTER_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-md-8 offset-md-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.CHEQUE_REGISTER_ADD_CHEQUE_REGISTER_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>


                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.CHEQUE_REGISTER_ADD_CHEQUEDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = cheque_registerDTO.chequeDate + "";
                                        %>
                                        <%
                                            String formatted_chequeDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                        %>
                                        <%=Utils.getDigits(formatted_chequeDate, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.CHEQUE_REGISTER_ADD_TOKENNUMBER, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = cheque_registerDTO.tokenNumber + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.CHEQUE_REGISTER_ADD_TOKENDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = cheque_registerDTO.tokenDate + "";
                                        %>
                                        <%
                                            String formatted_tokenDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                        %>
                                        <%=Utils.getDigits(formatted_tokenDate, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.CHEQUE_REGISTER_ADD_BILLNUMBER, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = cheque_registerDTO.billRegisterId + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.CHEQUE_REGISTER_ADD_BILLDESCRIPTION, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = cheque_registerDTO.billDescription + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.CHEQUE_REGISTER_ADD_BILLAMOUNT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = cheque_registerDTO.billAmount + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.CHEQUE_REGISTER_ADD_SUPPLIERNAME, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = cheque_registerDTO.supplierName + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.CHEQUE_REGISTER_ADD_CHEQUESTATUSCAT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = cheque_registerDTO.chequeStatusCat + "";
                                        %>
                                        <%
                                            value = CatRepository.getInstance().getText(Language, "cheque_status", cheque_registerDTO.chequeStatusCat);
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.CHEQUE_REGISTER_ADD_FILEDROPZONE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(cheque_registerDTO.fileDropzone);
                                        %>

                                        <%
                                            if (fileList != null) {
                                                for (int j = 0; j < fileList.size(); j++) {
                                                    FilesDTO filesDTO = fileList.get(j);
                                                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                        %>
                                        <td>
                                            <%
                                                if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                            %>
                                            <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)
						%>' style='width:100px'/>
                                            <%
                                                }
                                            %>
                                            <a href='<%=servletName%>?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                               download><%=filesDTO.fileTitle%>
                                            </a>
                                        </td>
                                        <%
                                                }
                                            }
                                        %>


                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>