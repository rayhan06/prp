<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="cheque_register.*" %>
<%@ page import="util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="util.StringUtils" %>


<%
    String navigator2 = "navCHEQUE_REGISTER";
    String servletName = "Cheque_registerServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.CHEQUE_REGISTER_ADD_BILLNUMBER, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.CHEQUE_REGISTER_ADD_BILLDESCRIPTION, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.CHEQUE_REGISTER_ADD_BILLAMOUNT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.CHEQUE_REGISTER_ADD_CHEQUENUMBER, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.CHEQUE_REGISTER_ADD_CHEQUEDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.CHEQUE_REGISTER_ADD_TOKENNUMBER, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.CHEQUE_REGISTER_ADD_TOKENDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.CHEQUE_REGISTER_ADD_SUPPLIERNAME, loginDTO)%>
            </th>
            <th><%=getDataByLanguage(Language, "চেক পরিমাণ", "Cheque Amount")%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.CHEQUE_REGISTER_SEARCH_CHEQUE_REGISTER_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span>All</span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Cheque_registerDTO> data = (List<Cheque_registerDTO>) recordNavigator.list;
            try {
                if (data != null) {
                    for (Cheque_registerDTO cheque_registerDTO : data) {
        %>
        <tr>
            <td>
                <%=Utils.getDigits(cheque_registerDTO.billRegisterId,Language)%>
            </td>
            <td>
                <%=cheque_registerDTO.billDescription%>
            </td>
            <td>
                <%=Utils.getDigits(cheque_registerDTO.billAmount, Language)%>/-
            </td>
            <td>
                <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, cheque_registerDTO.chequeNumber)%>
            </td>
            <td>
                <%=StringUtils.getFormattedDate(Language, cheque_registerDTO.chequeDate)%>
            </td>
            <td>
                <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, cheque_registerDTO.tokenNumber)%>
            </td>
            <td>
                <%
                    String formatted_tokenDate = simpleDateFormat.format(new Date(cheque_registerDTO.tokenDate));
                %>
                <%=Utils.getDigits(formatted_tokenDate, Language)%>
            </td>
            <td>
                <%=cheque_registerDTO.supplierName%>
            </td>
            <td>
                <%=Utils.getDigits(cheque_registerDTO.chequeAmount, Language)%>/-
            </td>
            <%CommonDTO commonDTO = cheque_registerDTO; %>
            <%@include file="../pb/searchAndViewButton.jsp" %>
            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID' value='<%=cheque_registerDTO.iD%>'/></span>
                </div>
            </td>
        </tr>
        <%
                    }
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>