<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="cheque_register.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="bill_register.Bill_registerDTO" %>
<%@ page import="bill_register.Bill_registerDAO" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@include file="../pb/addInitializer.jsp" %>
<%
    Cheque_registerDTO cheque_registerDTO;
    Bill_registerDTO bill_registerDTO;
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    long billRegisterId=-1;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        cheque_registerDTO = Cheque_registerDAO.getInstance().getDTOByID(Long.parseLong(ID));
        bill_registerDTO=Bill_registerDAO.getInstance().getDTOFromID(cheque_registerDTO.billRegisterId);
        billRegisterId=cheque_registerDTO.billRegisterId;
    } else {
        actionName = "add";
        cheque_registerDTO = new Cheque_registerDTO();
        billRegisterId = request.getParameter("billRegisterId") != null ? Long.parseLong(request.getParameter("billRegisterId")) : -1;
        bill_registerDTO = Bill_registerDAO.getInstance().getDTOFromID(billRegisterId);
    }
    if (bill_registerDTO == null)
        bill_registerDTO = new Bill_registerDTO();
    String tableName = "cheque_register";
    String context = request.getContextPath() + "/";
%>

<%
    String formTitle = LM.getText(LC.CHEQUE_REGISTER_ADD_CHEQUE_REGISTER_ADD_FORMNAME, loginDTO);
    String servletName = "Cheque_registerServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" action="Cheque_registerServlet?actionType=ajax_<%=actionName%>&isPermanentTable=true"
              id="chequeRegisterForm" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=cheque_registerDTO.iD%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='billRegisterId'
                                           id='bill_register_id_hidden_<%=i%>'
                                           value='<%=billRegisterId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='billDescription'
                                           id='bill_description_hidden_<%=i%>'
                                           value='<%=bill_registerDTO.description%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='billAmount'
                                           id='bill_amount_hidden_<%=i%>'
                                           value='<%=bill_registerDTO.billAmount%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='cashType'
                                           id='cash_type_hidden_<%=i%>'
                                           value='<%=bill_registerDTO.cashType%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.CHEQUE_REGISTER_ADD_CHEQUENUMBER, loginDTO)%>
                                            <span class="required">*</span> </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='chequeNumber'
                                                   id='chequeNumber_text_<%=i%>'
                                                   onkeypress="return event.charCode != 32"
                                                   value='<%=cheque_registerDTO.chequeNumber%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.CHEQUE_REGISTER_ADD_CHEQUEDATE, loginDTO)%>
                                            <span class="required">*</span> </label>
                                        <div class="col-md-8">
                                            <%value = "chequeDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='chequeDate' id='chequeDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(cheque_registerDTO.chequeDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.CHEQUE_REGISTER_ADD_TOKENNUMBER, loginDTO)%>
                                            <span class="required">*</span> </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='tokenNumber'
                                                   id='tokenNumber_text_<%=i%>' onkeypress="return event.charCode != 32"
                                                   value='<%=cheque_registerDTO.tokenNumber%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.CHEQUE_REGISTER_ADD_TOKENDATE, loginDTO)%>
                                            <span class="required">*</span></label>
                                        <div class="col-md-8">
                                            <%value = "tokenDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='tokenDate' id='tokenDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(cheque_registerDTO.tokenDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.CHEQUE_REGISTER_ADD_BILLDESCRIPTION, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 form-control" style="height: auto">
                                            <%=Utils.getDigits(bill_registerDTO.description, Language)%>
                                        </div>
                                    </div>

                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.CHEQUE_REGISTER_ADD_BILLAMOUNT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 form-control">
                                            <%=bill_registerDTO.billAmount%>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=getDataByLanguage(Language, "চেক পরিমাণ", "Cheque Amount")%>
                                            <span class="required">*</span> </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='chequeAmount'
                                                   id='chequeAmount_number_<%=i%>'
                                                   value='<%=cheque_registerDTO.chequeAmount>0?cheque_registerDTO.chequeAmount:""%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class=" form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.CHEQUE_REGISTER_ADD_SUPPLIERNAME, loginDTO)%>
                                             </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='supplierName'
                                                   id='supplierName_text_<%=i%>'
                                                   value='<%=actionName.equals("edit") ? cheque_registerDTO.supplierName : bill_registerDTO.recipientName%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.CHEQUE_REGISTER_ADD_FILEDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                fileColumnName = "fileDropzone";
                                                if (actionName.equals("edit")) {
                                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(cheque_registerDTO.fileDropzone);
                                            %>
                                            <%@include file="../pb/dropzoneEditor.jsp" %>
                                            <%
                                                } else {
                                                    ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                    cheque_registerDTO.fileDropzone = ColumnID;
                                                }
                                            %>

                                            <div class="dropzone"
                                                 action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=cheque_registerDTO.fileDropzone%>">
                                                <input type='file' style="display:none"
                                                       name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value=''
                                                   tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=cheque_registerDTO.fileDropzone%>'/>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.CHEQUE_REGISTER_ADD_CHEQUE_REGISTER_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="button"
                                    onclick="submitCheckRegisterForm()">
                                <%=LM.getText(LC.CHEQUE_REGISTER_ADD_CHEQUE_REGISTER_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<script type="text/javascript">
    const chequeRegisterForm = $('#chequeRegisterForm');
    const cancelBtn = $('#cancel-btn');
    const submitBtn = $('#submit-btn');
    const isLangEng = '<%=HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng%>';

    function isFromValid() {

        preprocessDateBeforeSubmitting('chequeDate', row);
        preprocessDateBeforeSubmitting('tokenDate', row);
        const jQueryValid = chequeRegisterForm.valid();
        const chequeDateValid = dateValidator('chequeDate_js_0', true, {
            'errorEn': '<%=LM.getInstance().getText("English", LC.EMPLOYEE_ACR_ADD_ENTER_VALID_DATE)%>',
            'errorBn': '<%=LM.getInstance().getText("Bangla", LC.EMPLOYEE_ACR_ADD_ENTER_VALID_DATE)%>'
        });
        const tokenDateValid = dateValidator('tokenDate_js_0', true, {
            'errorEn': '<%=LM.getInstance().getText("English", LC.EMPLOYEE_ACR_ADD_ENTER_VALID_DATE)%>',
            'errorBn': '<%=LM.getInstance().getText("Bangla", LC.EMPLOYEE_ACR_ADD_ENTER_VALID_DATE)%>'
        });
        return jQueryValid && chequeDateValid && tokenDateValid;
    }

    function submitCheckRegisterForm() {
        buttonStateChange(true);

        if (isFromValid()) {
            let msg = null;
            let params = chequeRegisterForm.serialize().split('&');

            for (let i = 0; i < params.length; i++) {
                let param = params[i].split('=');
                switch (param[0]) {
                    case 'chequeNumber':
                        if (!param[1]) {
                            msg = isLangEng ? "Cheque number is misisng" : "চেক নাম্বার যুক্ত করা হয় নি";
                        }
                        break;
                    case 'tokenNumber':
                        if (!param[1]) {
                            msg = isLangEng ? "Token number is missing" : "টোকেন নাম্বার যুক্ত করা হয় নি";
                        }
                        break;
                    case 'chequeAmount':
                        if (!param[1]) {
                            msg = isLangEng ? "Bill amount is missing" : "বিলের পরিমাণ যুক্ত করা হয় নি";
                        }
                        break;


                    case 'chequeStatusCat':
                        if (!param[1]) {
                            msg = isLangEng ? "Cheque status is not selected" : "চেকের স্ট্যাটাস যুক্ত করা হয় নি";
                        }
                        break;

                }
                if (msg) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToast(msg, msg);
                    buttonStateChange(false);
                    return;
                }
            }
            submitAjaxForm('chequeRegisterForm');
        } else {
            setButtonState(false);
        }
    }

    function buttonStateChange(value) {
        cancelBtn.prop("disabled", value);
        submitBtn.prop("disabled", value);
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Cheque_registerServlet");
    }

    function init(row) {
        document.getElementById('chequeAmount_number_0').onkeydown = keyDownEvent;
        setDateByStringAndId('chequeDate_js_' + row, $('#chequeDate_date_' + row).val());
        setDateByStringAndId('tokenDate_js_' + row, $('#tokenDate_date_' + row).val());


    }

    function keyDownEvent(e) {
        console.log(e);
        let isvalid = inputValidationForIntValue(e, $(this), null);
        return true == isvalid;
    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        // CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    function updateDescriptionLen() {
        $('#purpose_len').text($('#billDescription_text_0').val().length + "/1000");

        let len = $('#billDescription_text_0').val().length;
        if (Number(len) <= 512) {
            $('#purpose_len').text(len + "/512");
            $('#purpose_len').css('color', 'black');
        } else {
            $('#purpose_len').text(len + "/512");
            $('#purpose_len').css('color', 'red');
        }
    }
</script>






