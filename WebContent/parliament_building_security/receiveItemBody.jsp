<%@page import="pb.CatDAO"%>
<%@page import="gate_pass_type.Gate_pass_typeDTO"%>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="gate_pass.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="gate_pass_affiliated_person.Gate_pass_affiliated_personDAO" %>
<%@ page import="java.util.List" %>
<%@ page import="gate_pass_item.Gate_pass_itemDAO" %>
<%@ page import="org.apache.poi.ss.formula.eval.BlankEval" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.GATE_PASS_EDIT_LANGUAGE, loginDTO);

    Long gatePassId = Long.valueOf((String) request.getAttribute("gatePassId"));
    List<GatePassCardIssueModalItem> gatePassCardIssueModalItems = (List<GatePassCardIssueModalItem>) request.getAttribute("gatePassCardIssueModalItems");

    String context = request.getContextPath() + "/";
    Gate_pass_affiliated_personDAO personDAO = new Gate_pass_affiliated_personDAO();
    Gate_pass_itemDAO gatePassItemDAO = new Gate_pass_itemDAO();
    String visitorDropdownOptionString = personDAO.buildVisitorOptions(Language, gatePassId);
    System.out.println("visitorDropdownOptionString = " + visitorDropdownOptionString);
    Gate_passDTO gate_passDTO = Gate_passRepository.getInstance().getGate_passDTOByid(gatePassId);
    boolean isGalleryPass = gate_passDTO.gatePassTypeId == Gate_pass_typeDTO.GALLERY_PASS;
    System.out.println("isGalleryPass = " + isGalleryPass);
%>

<style>

    /*.table th, .table td, .table thead th {*/
    /*    border: 1px solid black;*/
    /*}*/

    /*table, tr, th, td {*/
    /*    border: 1px solid black;*/
    /*}*/
    .fa-trash{
        color: #ff6a6a;
        cursor: pointer;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-list-alt"></i>&nbsp;&nbsp;
                    <%=Language.equalsIgnoreCase("English") ? "Personal Item Clearance" : "ব্যক্তিগত আইটেম ছাড়পত্র"%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body" style="background: #FAFAFA">
            <form class="form-horizontal"
                  action="Gate_pass_itemServlet?actionType=issueCard&gatePassId=<%=gatePassId%>&isPermanentTable=true"
                  id="issue-card-form" name="issue-card-form" method="POST"
                  onsubmit="return PreprocessBeforeSubmitting()"
                  enctype="multipart/form-data">
                <div class="form-body">
                    <div class="modal-content rounded" style="box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;">
                        <div class="modal-header">
                            <h5 class="modal-title table-title" id="exampleModalLongTitle"
                                style="font-size: large; font-weight: bolder;">
                                <i class="fa fa-archive"></i>&nbsp;
                                <%=Language.equalsIgnoreCase("English") ?
                                        "SUBMIT ITEM" : "যা জমা রাখিতে চান"%>
                            </h5>
                        </div>
                        <div class="modal-body">
                            <div class="row inline-block">
                                <label class="col-5" style="font-weight: bold">
                                </label>
                                <div class="col-7">
                                </div>
                            </div>
                     

                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="card-issue-items-tableData"
                                       class="table table-bordered table-striped table-full-width">
                                    <thead>
                                    <tr>
                                        <th class="text-center"
                                            style=" color:#0a6aa1"><%=Language.equalsIgnoreCase("English") ? "VISITOR" : "দর্শনার্থী"%>
                                        </th>
                                        <th class="text-center"
                                            style=" color:#0a6aa1"><%=Language.equalsIgnoreCase("English") ? "CARD/BOX NUMBER" : "কার্ড/বক্স নাম্বার"%>
                                        </th>
                                        <%
                                        if(isGalleryPass)
                                        {
                                        	%>
                                        <th class="text-center"
                                            style=" color:#0a6aa1"><%=Language.equalsIgnoreCase("English") ? "Gallery" : "গ্যালারি"%>
                                        </th>
                                        	<%
                                        }
                                        %>
                                        <th class="text-center"
                                            style=" color:#0a6aa1; width: 15rem;"><%=Language.equalsIgnoreCase("English") ? "ITEM" : "আইটেম"%>
                                        </th>
                                        <th class="text-center"
                                            style=" color:#0a6aa1;"><%=Language.equalsIgnoreCase("English") ? "AMOUNT" : "পরিমান"%>
                                        </th>
                                        <th class="text-center"
                                            style=" color:#0a6aa1"><%=Language.equalsIgnoreCase("English") ? "DESCRIPTION" : "বর্ণনা"%>
                                        </th>
                                        <th class="text-center"
                                            style=" color:#0a6aa1"><%=Language.equalsIgnoreCase("English") ? "Remove" : "মুছুন"%>
                                        </th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <tr id="gate_pass_item_1">
                                        <input type="hidden" name="additionItemId_1" id="additionItemId_1"
                                               value="0"/>
                                        <td>
                                            <select class='form-control submitter'
                                                    name='parliamentNameId_1'
                                                    id='parliamentNameId_1'
                                                    tag='pb_html'>
                                                <%=visitorDropdownOptionString%>
                                            </select>
                                        </td>
                                        <td>
                                            <input type='text'
                                                   class='form-control card-number'
                                                   name='card_number_1'
                                                   id='card_number_1'
                                                   tag='pb_html'/>
                                        </td>
                                         <%
                                        if(isGalleryPass)
                                        {
                                        	%>
                                        <td>
                                            <select class='form-control submitter'
                                                    name='gallery_1'
                                                    id='gallery_1'
                                                    tag='pb_html'>
                                                <%=CatDAO.getOptions(Language, "gallery", -1)%>
                                            </select>
                                        </td>
                                        <%
                                        }
                                        %>
                                        <td>

                                            <select multiple="multiple" class='form-control'
                                                    name='parliamentItemId_1'
                                                    id='parliamentItemId_1' tag='pb_html'>

                                            </select>

                                        </td>
                                        <td>
                                            <input type='number'
                                                   class='form-control'
                                                   name='amount_1'
                                                   id='amount_1' value="1"
                                                   min="1"
                                                   tag='pb_html'>
                                        </td>
                                        <td>
                                            <input type='text'
                                                   class='form-control'
                                                   name='description_1'
                                                   id='description_1'
                                                   tag='pb_html'/>
                                        </td>
                                        <td class="text-center">
                                            <i class="fa fa-trash remove-previously-chosen-item my-2 ml-1"></i>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <input type='hidden' class='form-control'
                                   name='totalItemCount' id='totalItemCount'
                                   value="1" tag='pb_html'/>

                            <div class="row form-group mt-4">
                                <div class="col-12 text-right">
                                    <button class="btn btn-outline-info rounded"
                                            type="button" id="add_more_item_button">
                                        <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_ADD_MORE_ITEM, loginDTO)%>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-content mt-4 rounded" style="box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;">
                        <div class="modal-header" style = "<%=isGalleryPass?"display:none":"" %>">
                            <h5 class="modal-title table-title" id="exampleModalLongTitleSecond"
                                style="font-size: large; font-weight: bolder;">
                                <i class="fa fa-cart-plus"></i>&nbsp;
                                <%=Language.equalsIgnoreCase("English") ?
                                        "HOLD ITEM" : "যা সাথে করে নিয়ে যেতে চান"%>
                            </h5>
                        </div>
                        <div class="modal-body" style = "<%=isGalleryPass?"display:none":"" %>">
                            <div class="row inline-block">
                                <label class="col-5" style="font-weight: bold">
                                </label>
                                <div class="col-7">
                                </div>
                            </div>
                    

                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="card-hold-items-tableData"
                                       class="table table-bordered table-striped table-full-width">
                                    <thead>
                                    <tr>
                                        <th class="text-center"
                                            style=" color:#0a6aa1"><%=Language.equalsIgnoreCase("English") ? "VISITOR" : "দর্শনার্থী"%>
                                        </th>
                                        <th class="text-center"
                                            style=" color:#0a6aa1; width: 15rem;"><%=Language.equalsIgnoreCase("English") ? "ITEM" : "আইটেম"%>
                                        </th>
                                        <th class="text-center"
                                            style=" color:#0a6aa1;"><%=Language.equalsIgnoreCase("English") ? "AMOUNT" : "পরিমান"%>
                                        </th>
                                        <th class="text-center"
                                            style=" color:#0a6aa1"><%=Language.equalsIgnoreCase("English") ? "Remove" : "মুছুন"%>
                                        </th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <tr id="gate_pass_item_hold_1">
                                        <input type="hidden" name="additionItemId_hold_1" id="additionItemId_hold_1"
                                               value="0"/>
                                        <td>
                                            <select class='form-control holder'
                                                    name='parliamentNameId_hold_1'
                                                    id='parliamentNameId_hold_1'
                                                    tag='pb_html'>
                                                <%=visitorDropdownOptionString%>
                                            </select>
                                        </td>
                                        <td>

                                            <select class='form-control pih'
                                                    name='parliamentItemId_hold_1'
                                                    id='parliamentItemId_hold_1' onchange="getMaxItemAmount(this)"
                                                    tag='pb_html'>
                                                <%=gatePassItemDAO.buildHoldingItemsDropdown(Language, gatePassId)%>
                                            </select>

                                        </td>
                                        <td>
                                            <input type='number'
                                                   class='form-control'
                                                   name='amount_hold_1'
                                                   id='amount_hold_1' value="1"
                                                   min="1"
                                                   tag='pb_html'>
                                        </td>
                                        <td class="text-center">
                                            <i class="fa fa-trash remove-previously-chosen-item-hold my-2 ml-1"></i>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <input type='hidden' class='form-control'
                                   name='totalItemCountHold' id='totalItemCountHold'
                                   value="1" tag='pb_html'/>

                            <div class="row form-group mt-4">
                                <div class="col-12 text-right">
                                    <button class="btn btn-outline-info rounded"
                                            type="button" id="add_more_hold_item_button">
                                        <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_ADD_MORE_ITEM, loginDTO)%>
                                    </button>
                                </div>
                            </div>
                        </div>


                        <div class="modal-footer">
                            <button type="submit"
                                    class="btn-sm shadow border-0 text-white submit-btn"
                            >
                                <%=Language.equalsIgnoreCase("English") ? "Submit" : "জমা দিন"%>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



<template id="full-page-loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>

<script type="text/javascript">
    const fullPageLoader = $('#full-page-loader');
    let selectedItemCat;
    let selectedItemCatArr;
    let selectedItemCatArrForAjax;
    let totalItemCount;
    let maxRowCount;
    let totalItemCountHold;
    let maxRowCountHold;
    let isStoredItem = 0;

    $(document).ready(function () {
        totalItemCount = 1;
        maxRowCount = totalItemCount;
        totalItemCountHold = 1;
        maxRowCountHold = totalItemCountHold;


        $('#add_more_item_button').click(() => {
            let currentItemCount = totalItemCount;
            let prevCardNo = $('#card_number_' + currentItemCount).val();
            if (prevCardNo == undefined) {
                prevCardNo = '';
            }

            currentItemCount++;
            let url = "Gate_pass_itemServlet?actionType=getItemRowIssueCard" + "&language=<%=Language%>&rowNumber="
                + currentItemCount + "&prevCardNo=" + prevCardNo + "&gatePassId=" + '<%=gatePassId%>' + "&isStoredItem=" + isStoredItem;

            // Loader starts
            fullPageLoader.show();

            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (data) {
                    $('#card-issue-items-tableData > tbody').append(data);
                    let itemId = 'parliamentItemId_' + currentItemCount;
                    select2MultiSelector('#' + itemId, '<%=Language%>');
                    fetchItemsCat(selectedItemCatArrForAjax, itemId);
                    
                    
                    var submitter = [];
                	$('.submitter').each(function() {
                		if($(this).val() != '' && $(this).val() != null)
                		{
                			submitter.push($(this).val());
                		}
                		
                	});
                	
                	for(var i = 0; i < submitter.length; i ++)
                	{
                		$("#parliamentNameId_" + currentItemCount + " option[value='" + submitter[i] + "']").remove();
                	}

                },
                error: function (error) {
                    console.log(error);
                }
            });

            // Loader ends
            fullPageLoader.hide();

            $('#totalItemCount').val(currentItemCount);
            totalItemCount = totalItemCount + 1;
            maxRowCount = maxRowCount + 1;
        });

        $('#add_more_hold_item_button').click(() => {
            let currentItemCount = totalItemCountHold;

            currentItemCount++;
            let url = "Gate_pass_itemServlet?actionType=getHoldingItemRowIssueCard" + "&language=<%=Language%>&rowNumber="
                + currentItemCount + "&gatePassId=" + '<%=gatePassId%>';

            // Loader starts
            fullPageLoader.show();

            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (data) {
                    $('#card-hold-items-tableData > tbody').append(data);
                },
                error: function (error) {
                    console.log(error);
                }
            });

            // Loader ends
            fullPageLoader.hide();

            $('#totalItemCountHold').val(currentItemCount);
            totalItemCountHold = totalItemCountHold + 1;
            maxRowCountHold = maxRowCountHold + 1;
        });

        <%
            if (gatePassCardIssueModalItems.isEmpty()) {
        %>
        select2MultiSelector("#parliamentItemId_1", '<%=Language%>');
        fetchItemsCat(selectedItemCatArr, 'parliamentItemId_1');
        <%
            } else {
        %>
        isStoredItem = 1;
        setStoredModalItem();
        isStoredItem = 0;
        <%
            }
        %>

        <%--select2SingleSelector("#parliamentItemId_hold_1", '<%=Language%>');--%>
        $('input[type="number"]').on('keyup', function () {
            let v = parseInt($(this).val());
            let min = parseInt($(this).attr('min'));
            let max = parseInt($(this).attr('max'));

            if (v < min) {
                $(this).val(min);
            } else if (v > max) {
                $(this).val(max);
            }
        })
    });

    $(document.body).on('click', '.remove-previously-chosen-item', function (e) {
        if (totalItemCount > 1) {
            let trId = $(this).closest('tr').attr('id');
            $('#' + trId).remove();
            totalItemCount = totalItemCount - 1;
            console.log("table row id: " + trId);
        }
    });

    $(document.body).on('click', '.remove-previously-chosen-item-hold', function (e) {
        if (totalItemCountHold > 1) {
            let trId = $(this).closest('tr').attr('id');
            $('#' + trId).remove();
            totalItemCountHold = totalItemCountHold - 1;
            console.log("table row id: " + trId);
        }
    });

    function fetchItemsCat(inputItemCatArr, itemId) {
        let language = '<%=Language%>';
        let url = "Gate_pass_itemServlet?actionType=getAllItemList";
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {

                $('#' + itemId).html("");
                let o;
                let str;

                $('#' + itemId).append(o);
                const response = JSON.parse(fetchedData);
                if (response && response.length > 0) {
                    for (let x in response) {
                        if (language === 'English') {
                            str = response[x].englishText;
                        } else {
                            str = response[x].banglaText;
                        }

                        if (inputItemCatArr && inputItemCatArr.length > 0) {
                            if (inputItemCatArr.includes(response[x].value + '')) {
                                o = new Option(str, response[x].value, false, true);
                            } else {
                                o = new Option(str, response[x].value);
                            }
                        } else {
                            o = new Option(str, response[x].value);
                        }
                        $(o).html(str);
                        $('#' + itemId).append(o);
                    }
                }


            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function setStoredModalItem() {
        let trId;
        let tdId;
        <%
            int modalItemIteratorCountIncremented;
            for (int modalItemIteratorCount = 0; modalItemIteratorCount < gatePassCardIssueModalItems.size(); ++modalItemIteratorCount) {
                modalItemIteratorCountIncremented = modalItemIteratorCount + 1;
                if (modalItemIteratorCount > 0) {
        %>
        $('#add_more_item_button').click();
        <%
            }
        %>
        trId = 'gate_pass_item_' + '<%=modalItemIteratorCountIncremented%>';

        tdId = 'additionItemId_' + '<%=modalItemIteratorCountIncremented%>';
        $('#' + tdId).val('<%=gatePassCardIssueModalItems.get(modalItemIteratorCount).id%>');
        tdId = 'parliamentNameId_' + '<%=modalItemIteratorCountIncremented%>';
        $('#' + tdId).val('<%=gatePassCardIssueModalItems.get(modalItemIteratorCount).visitorName%>');
        tdId = 'amount_' + '<%=modalItemIteratorCountIncremented%>';
        $('#' + tdId).val('<%=gatePassCardIssueModalItems.get(modalItemIteratorCount).amount%>');
        tdId = 'description_' + '<%=modalItemIteratorCountIncremented%>';
        $('#' + tdId).val('<%=gatePassCardIssueModalItems.get(modalItemIteratorCount).description%>');
        tdId = 'card_number_' + '<%=modalItemIteratorCountIncremented%>';
        $('#' + tdId).val('<%=gatePassCardIssueModalItems.get(modalItemIteratorCount).cardNumber%>');

        //set multi item select input
        tdId = 'parliamentItemId_' + '<%=modalItemIteratorCountIncremented%>';
        select2MultiSelector("#" + tdId, '<%=Language%>');
        selectedItemCat = '<%=gatePassCardIssueModalItems.get(modalItemIteratorCount).itemIdList%>'
        selectedItemCatArr = selectedItemCat.split(',').map(function (item) {
            return item.trim();
        });
        fetchItemsCat(selectedItemCatArr, tdId);

        // disabling pointer event
        $('#' + trId).css('pointer-events', 'none');
        $('#' + trId).css("background-color", "#f7e4e4");
        <%
            }
        %>
    }

    function processMultipleSelectBoxBeforeSubmit(name) {
    	
    	

        $("[name='" + name + "']").each(function (i) {
            var selectedInputs = $(this).val();
            var temp = "";
            if (selectedInputs != null) {
                selectedInputs.forEach(function (value, index, array) {
                    if (index > 0) {
                        temp += ", ";
                    }
                    temp += value;
                });
            }
            if (temp.includes(',')) {
                $(this).append('<option value="' + temp + '"></option>');
            }
            $(this).val(temp);

        });
    }

    function PreprocessBeforeSubmitting() {
        let maxRow = maxRowCount;
        let itemIdName;
        let iterateCounter;
        
        console.log("PreprocessBeforeSubmitting");

        var submitter = [];
    	$('.submitter').each(function() {
    		var id = $(this).attr('id');
    		var rowIndex = id.split("_")[1];
    		var selectId = "parliamentItemId_" + rowIndex;
    		var items= $("#" + selectId).val(); 
    		console.log("items = " + items);
    		if(items != null && items != '')
    		{
    			submitter.push($(this).val());
    		}
    		else
    		{
    			console.log("null items, not pushing");
    		}
    		
    	});
    	
    	var holder = [];
    	$('.holder').each(function() {
    		holder.push($(this).val());
    	});
    	
    	var cardNumber = [];
    	var dc = false;
    	$('.card-number').each(function() {
    		if(cardNumber.includes($(this).val()))
    		{
    			toastr.error("Duplicate card number not allowed");
    			dc = true;
    			return false;
    		}
    		else if($(this).val() == '')
    		{
    			toastr.error("Card Number cannot be empty");
    			dc = true;
    			return false;
    		}
    		else
    		{
    			cardNumber.push($(this).val());
    		}
    	});
    	
    	if(dc)
    	{
    		return false;
    	}
    	
    	
    	for(var i = 0; i < submitter.length; i ++)
    	{
    		console.log("submitter: " + submitter[i]);
    		if(submitter[i] == null || submitter[i] == '')
    		{
    			//toastr.error("Visitor name must be chosen");
    			return false;
    		}
    	}
    	
    	for(var i = 0; i < holder.length; i ++)
    	{
    		console.log("holder " + holder[i]);
    		if(holder[i] == null || holder[i] == '')
    		{
    			//toastr.error("Holder name must be chosen");
    			return false;
    		}
    	}
    	
    	for(var i = 0; i < holder.length; i ++)
    	{
    		if(!submitter.includes(holder[i]))
    		{
    			toastr.error(holder[i] + " must submit something ");
    			return false;
    		}
    	}
    	
    	//return false;
    	
        for (iterateCounter = 1; iterateCounter < maxRow + 1;) {
            itemIdName = 'parliamentItemId_' + iterateCounter;
            if (parseInt(iterateCounter) == (parseInt(maxRow) + 1)) {
                return true;
            } else {
                iterateCounter = iterateCounter + 1;
            }
            if ($("#" + itemIdName).length == 0) {
                console.log(itemIdName + ' does not exist!');
            } else {
                processMultipleSelectBoxBeforeSubmit(itemIdName);
            }
        }
        
        console.log("totalItemCount " + $("#totalItemCount").val());
        console.log("totalItemCountHold " + $("#totalItemCountHold").val());
        
    }

    function getMaxItemAmount(param) {

        if (param.value !== '') {
            setMaxInputAttribute('amount_hold_' + param.id.split('_').pop(), param.value);
        }
    }

    function setMaxInputAttribute(inputFieldId, gatePassItemId) {
        let url = "Gate_pass_itemServlet?actionType=maxNumberOfHoldingItem" + "&gatePassItemId=" + gatePassItemId;
		var thisRowId = inputFieldId.split('_')[2];
        // Loader starts
        fullPageLoader.show();

        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (data) {
            	var soFarFound = 0;
            	$( ".pih" ).each(function() {
            		  if($( this ).val() == gatePassItemId)
            		  {
            			  var rowId = $( this ).attr("id").split('_')[2];
            			  if(thisRowId != rowId)
            			  {
            				  console.log("found, rowId = " + rowId);
                			  soFarFound += parseInt($("#amount_hold_" + rowId).val());
            			  }
            			  
            		  }
            	});
            	
            	var max = parseInt(data) - soFarFound;
            	console.log("soFarFound = " + soFarFound + " max = " + max);
                $('#' + inputFieldId).prop('max', max);
                $('#' + inputFieldId).prop('value', max);
            },
            error: function (error) {
                console.log(error);
            }
        });

        // Loader ends
        fullPageLoader.hide();
    }

</script>


