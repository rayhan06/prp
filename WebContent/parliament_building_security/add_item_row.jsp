<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="gate_pass_item.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="gate_pass_affiliated_person.Gate_pass_affiliated_personDAO" %>
<%@ page import="org.apache.commons.codec.language.bm.Lang" %>
<%@page import="gate_pass.*" %>
<%@page import="pb.CatDAO"%>
<%@page import="gate_pass_type.Gate_pass_typeDTO"%>

<%
    Gate_pass_itemDTO gate_pass_itemDTO;
    gate_pass_itemDTO = (Gate_pass_itemDTO) request.getAttribute("gate_pass_itemDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (gate_pass_itemDTO == null) {
        gate_pass_itemDTO = new Gate_pass_itemDTO();
    }
    System.out.println("gate_pass_itemDTO = " + gate_pass_itemDTO);

    String Language = LM.getText(LC.GATE_PASS_ITEM_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    int rowNumber = Integer.parseInt(request.getParameter("rowNumber"));
    String prevCardNo = request.getParameter("prevCardNo");
    long gatePassId = Long.parseLong(request.getParameter("gatePassId"));
    Gate_pass_affiliated_personDAO personDAO = new Gate_pass_affiliated_personDAO();
    String isStoredItem = request.getParameter("isStoredItem");
    System.out.println("isStoredItem = " + isStoredItem);
    Gate_passDTO gate_passDTO = Gate_passRepository.getInstance().getGate_passDTOByid(gatePassId);
    boolean isGalleryPass = gate_passDTO.gatePassTypeId == Gate_pass_typeDTO.GALLERY_PASS;
    System.out.println("isGalleryPass = " + isGalleryPass);
%>

<tr id="gate_pass_item_<%=rowNumber%>">
    <input type="hidden" name="additionItemId_<%=rowNumber%>" id="additionItemId_<%=rowNumber%>" value="0"/>
    <td>
        <select class='form-control submitter'
                name='parliamentNameId_<%=rowNumber%>'
                id='parliamentNameId_<%=rowNumber%>'
                tag='pb_html'>
            <%
                if (isStoredItem.equals("0")) {
            %>
                <%=personDAO.buildEligibleVisitorOptions(Language, gatePassId)%>
            <%
                } else {
            %>
            <%=personDAO.buildVisitorOptions(Language, gatePassId)%>
            <%
                }
            %>
        </select>
    </td>
    <td>
        <input type='text' class='form-control card-number' name='card_number_<%=rowNumber%>'
               id='card_number_<%=rowNumber%>' value="" required tag='pb_html'/>
    </td>
<%
     if(isGalleryPass)
     {
     	%>
     <td>
         <select class='form-control submitter'
                 name='gallery_<%=rowNumber%>'
                 id='gallery_<%=rowNumber%>'
                 tag='pb_html'>
             <%=CatDAO.getOptions(Language, "gallery", -1)%>
         </select>
     </td>
     <%
     }
     %>
    <td>
        <select multiple="multiple" class='form-control' name='parliamentItemId_<%=rowNumber%>'
                id='parliamentItemId_<%=rowNumber%>' tag='pb_html'>

        </select>
    </td>
    <td>
        <input type='number' class='form-control' name='amount_<%=rowNumber%>'
               id='amount_<%=rowNumber%>' value="1" min="1" tag='pb_html'>
    </td>
    <td>
        <input type='text' class='form-control' name='description_<%=rowNumber%>'
               id='description_<%=rowNumber%>' tag='pb_html'/>
    </td>
    <td class="text-center">

<%--        <button class="btn delete-trash-btn">--%>
            <i class="fa fa-trash remove-previously-chosen-item my-2 ml-1"></i>
<%--        </button>--%>
    </td>
</tr>






