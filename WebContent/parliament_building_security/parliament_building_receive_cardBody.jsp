<%@page pageEncoding="UTF-8" %>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="org.apache.commons.codec.language.bm.Lang" %>
<%@ page import="util.StringUtils" %>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.GATE_PASS_VISITOR_EDIT_LANGUAGE, loginDTO);

    String context = "../../.." + request.getContextPath() + "/";
%>

<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <i class="fa fa-id-card fa-2x" style="color:#0098bf !important;"></i>&nbsp;
        <h3 class="kt-subheader__title">
            &nbsp;<%=Language.equalsIgnoreCase("English") ? "Receive Card" : "কার্ড জমা নিন"%>
        </h3>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content" style="background: white">
    <div class="row shadow-none border-0">
        <div class="col-lg-12">

            <div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
                 style="">
                <div class="kt-portlet__body" >
                    <!-- BEGIN FORM-->
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6" >
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label"><%=Language.equalsIgnoreCase("English") ? "Card Number" : "কার্ড নাম্বার"%>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control englishDigitOnly" id="card_number" name="card_number"
                                               placeholder='<%=Language.equalsIgnoreCase("English") ? "Enter card number" : "কার্ড নাম্বার দিন"%>'>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row bottom-two">
                            <div class="col-md-6" >
                                <div class="ml-auto text-right">
                                    <button type="submit" class="btn shadow green-meadow btn-outline sbold uppercase advanceseach"
                                            onclick="getReceivedItems()"
                                            style="background-color: #00a1d4; color: white; border-radius: 6px!important; border-radius: 8px;">
                                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div id="search-result" style="display: none">
                            <div class="row">
                                <div class="col-sm-12" >
                                    <div id="horizontal-line" style="height: 1px; background: #ecf0f5"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                                    <div class="table-responsive">
                                        <table id="tableData" class="table table-bordered table-striped text-nowrap">
                                            <thead>
                                            <tr>
                                                <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "Visitor Name" : "দর্শনার্থীর নাম"%>
                                                </th>
                                                <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "Item" : "আইটেম"%>
                                                </th>
                                                <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "Description" : "বিবরণ"%>
                                                </th>
                                                <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "Amount" : "পরিমান"%>
                                                </th>
                                                <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "Box Number" : "বক্স নাম্বার"%>
                                            </tr>
                                            </thead>
                                            <tbody>
<%--                                            <tr>--%>
<%--                                                <td id="name"></td>--%>
<%--                                                <td id="item"></td>--%>
<%--                                                <td id="amount"></td>--%>
<%--                                                <td id="box"></td>--%>

<%--                                            </tr>--%>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12" >
                                    <div class="ml-auto text-right">
                                        <button type="submit" id="receive_button" class="btn shadow green-meadow btn-outline sbold uppercase advanceseach"
                                                onclick="cardReceived()"
                                                style="background-color: #00a1d4; color: white; border-radius: 6px!important; border-radius: 8px;">
                                            <%=Language.equalsIgnoreCase("English") ? "RECEIVE" : "জমা নিন"%>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
</div>

<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>

<script>
    var box_number;
    function getReceivedItems() {
        let card_number = $('#card_number').val();
        box_number = card_number;

        // Ajax call for data
        let url = "Gate_passServlet?actionType=receiveCardData&cardNumber="+card_number;
        $.ajax({
            url: url,
            type: "GET",
            async: true,
            success: function (data) {

                if (data.localeCompare('noString') === -1) {
                    let modelArray = JSON.parse(data);
                    const tbody = $('#tableData > tbody');
                    tbody.empty();
                    for(model in modelArray){

                        <%
                            if (Language.equalsIgnoreCase("English")) {
                        %>
                        tbody.html(
                            tbody.html() +
                            "<tr><td>" + modelArray[model].visitorName + "</td>" +
                            "<td>" + modelArray[model].itemNameEng + "</td>" +
                            "<td>" + modelArray[model].description + "</td>" +
                            "<td>" + modelArray[model].amountEng + "</td>" +
                            "<td>" + modelArray[model].cardNumberEng + "</td></tr>"
                        );
                        <%
                            } else {
                        %>
                        tbody.html(
                            tbody.html() +
                            "<tr><td>" + modelArray[model].visitorName + "</td>" +
                            "<td>" + modelArray[model].itemNameBng + "</td>" +
                            "<td>" + modelArray[model].description + "</td>" +
                            "<td>" + modelArray[model].amountBng + "</td>" +
                            "<td>" + modelArray[model].cardNumberBng + "</td></tr>"
                        );
                        <%
                            }
                        %>
                    }
                    $('#search-result').show();

                } else {
                    Swal.fire({
                        position: 'center',
                        type: 'warning',
                        title: '<%=Language.equalsIgnoreCase("English") ? "NOTIFICATION" : "বিজ্ঞপ্তি"%>',
                        text: '<%=Language.equalsIgnoreCase("English") ? "Sorry, card has not been found!" : "দুঃখিত, কার্ডটি খুজে পাওয়া যায় নি!"%>',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    $('#search-result').hide();
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function cardReceived() {
        let url = "Gate_passServlet?actionType=receiveCard&cardNumber="+box_number;
        $.ajax({
            url: url,
            type: "GET",
            async: true,
            success: function (data) {
                $('#tableData > tbody').empty();
                $('#search-result').hide();
                Swal.fire({
                    position: 'center',
                    type: 'success',
                    text: '<%=Language.equalsIgnoreCase("English") ? "Please, Receive the card now!" : "অনুগ্রহ করে কার্ডটি গ্রহন করুন!"%>',
                    showConfirmButton: false,
                    timer: 3000
                });
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
</script>

<style>
    td {
        text-align: center;
    }
    .bottom-two {
        margin-bottom: 2cm;
    }
</style>


