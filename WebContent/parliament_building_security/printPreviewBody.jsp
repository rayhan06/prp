<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="gate_pass.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="files.*" %>
<%@page import="util.StringUtils" %>
<%@ page import="pb.Utils" %>
<%@ page import="parliament_gallery.Parliament_galleryDAO" %>
<%@ page import="gate_pass_affiliated_person.Gate_pass_affiliated_personDAO" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.GATE_PASS_EDIT_LANGUAGE, loginDTO);

    Gate_passDAO gate_passDAO = new Gate_passDAO("gate_pass");

    GatePassModel gatePassModel = (GatePassModel) request.getAttribute("GatePassModel");

    String viewPageTitle = Language.equalsIgnoreCase("English") ? (gatePassModel.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ? gatePassModel.gatePassTypeEng + " ( " + gatePassModel.gatePassSubTypeEng + " ) " : gatePassModel.gatePassTypeEng) :
            (gatePassModel.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ? gatePassModel.gatePassTypeBng + " ( " + gatePassModel.gatePassSubTypeBng + " ) " : gatePassModel.gatePassTypeBng);

    if (Language.equalsIgnoreCase("English")) {
        viewPageTitle = viewPageTitle.substring(0, 1).toUpperCase() + viewPageTitle.substring(1);
    }

    int count = 1;

    String context = request.getContextPath() + "/";

    String galleryName = "";
    if (gatePassModel.gatePassTypeEng.equalsIgnoreCase("parliament gallery pass")) {
        Parliament_galleryDAO parliament_galleryDAO = new Parliament_galleryDAO();
        galleryName = parliament_galleryDAO.getGalleryNameFromGatePassGalleryId(gate_passDAO.getDTOByID(gatePassModel.gatePassId).gatePassGalleryId, Language);
    }

    Gate_pass_affiliated_personDAO personDAO = new Gate_pass_affiliated_personDAO();
%>

<style>
    .font-color {
        color: #00a1d4 !important;
    }

    .signature-image {
        border-bottom: 2px solid #a406dc !important;
        width: 200px !important;
        height: 53px !important;
    }

    .signature-div {
        font-size: .95rem;
        color: #a406dc;
    }

    .table th, .table td, .table thead th {
        border: 1px solid black;
    }

    table, tr, th, td {
        border: 1px solid black;
    }

    @media print {
        th, td, .kt-content, .kt-portlet__body, .kt-portlet__head {
            font-size: 1.5rem !important;
        }

        .button_process, .button_approve, .button_dismiss {
            display: none !important;
        }
    }
</style>

<!-- begin:: Content -->
<div class="kt-content" id="kt_content" style="font-family: Roboto, SolaimanLipi!important;">
    <div class="" id="modalbody">
        <div class="kt-portlet" style="background-color: #F2F2F2!important;">
            <div class="kt-portlet__body m-4" style="background-color: #f6f9fb!important;">
                <div class="row">
                    <div class="col-12 d-flex justify-content-center">
                        <div class="text-center">
                            <img
                                    width="110"
                                    src="<%=context%>assets/static/parliament_logo.png"
                                    alt="logo"
                                    class="logo-default"
                            />
                            <h2 class="mt-3" style="color: #0098bf !important">
                                বাংলাদেশ জাতীয় সংসদ সচিবালয়
                            </h2>
                            <h4 class="mt-2" style="color: #0098bf !important">
                                শেরেবাংলা নগর, ঢাকা-১২০৭
                            </h4>
                            <h5 class="mt-2">
                                <%=gatePassModel.visitingDateBng%> ইং
                                তারিখ <%=gatePassModel.gatePassTypeBng%>
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="mt-5">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <th style="color: #0098bf !important">
                                    <strong>
                                        <%=Language.equals("English") ? "Serial No." : "ক্রমিক নং"%>
                                    </strong>
                                </th>
                                <th style="color: #0098bf !important">
                                    <strong>
                                        <%=Language.equals("English") ? "Card No." : "কার্ড নং"%>
                                    </strong>
                                </th>
                                <th style="color: #0098bf !important">
                                    <strong
                                    ><%=Language.equals("English") ? "Person/Persons Pass Issued For" : "যাদের/যাহাদের জন্য পাস  ইস্যু করা হবে"%>
                                    </strong>
                                </th>
                                <th style="color: #0098bf !important">
                                    <strong>
                                        <%=Language.equals("English") ? "Full Address" : "সম্পূর্ণ ঠিকানা"%>
                                    </strong>
                                </th>
                            </tr>
                            <%
                                if (!gatePassModel.cardNumber.isEmpty()) {
                            %>
                            <tr>
                                <td><%=StringUtils.convertToBanNumber(Integer.toString(count++))%>
                                </td>
                                <td><%=StringUtils.convertToBanNumber(gatePassModel.cardNumber)%>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-5">
                                            <b><%=LM.getText(LC.GATE_PASS_VIEW_VISITOR_NAME, loginDTO)%>
                                            </b></div>
                                        <div class="col-7"><%=gatePassModel.visitorName%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5">
                                            <b><%=LM.getText(LC.GATE_PASS_VIEW_VISITOR_FATHER_NAME, loginDTO)%>
                                            </b></div>
                                        <div class="col-7"><%=gatePassModel.visitorFatherName%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5"><b><%=LM.getText(LC.GATE_PASS_VIEW_CREDENTIAL, loginDTO)%>
                                        </b></div>
                                        <div class="col-7"><%=Language.equalsIgnoreCase("English") ? gatePassModel.visitorCredentialTypeEng + "-" + Utils.getDigits(gatePassModel.visitorCredentialNo, "English") :
                                                gatePassModel.visitorCredentialTypeBng + " - " + Utils.getDigits(gatePassModel.visitorCredentialNo, "Bangla")%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5"><b><%=LM.getText(LC.GATE_PASS_VIEW_MOBILE, loginDTO)%>
                                        </b></div>
                                        <div class="col-7"><%=Language.equalsIgnoreCase("English") ? gatePassModel.visitorMobileNumberEng : gatePassModel.visitorMobileNumberBng%>
                                        </div>
                                    </div>
                                    <%
                                        if (gatePassModel.gatePassType == 10) {
                                    %>
                                    <div class="row">
                                        <div class="col-5">
                                            <b><%=LM.getText(LC.GATE_PASS_PARLIAMENT_BUILDING_REASON_FOR_VISIT, loginDTO)%>
                                            </b></div>
                                        <div class="col-7"><%=gatePassModel.reasonForVisit%>
                                        </div>
                                    </div>
                                    <%
                                        }
                                    %>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-5">
                                            <b><%=LM.getText(LC.GATE_PASS_VISITOR_ADD_VILLAGE, loginDTO)%>
                                            </b></div>
                                        <div class="col-7"><%=gatePassModel.currentAddress.addressText%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5">
                                            <b><%=LM.getText(LC.GATE_PASS_VISITOR_ADD_UNION, loginDTO)%>
                                            </b></div>
                                        <div class="col-7"><%=Language.equalsIgnoreCase("English") ? gatePassModel.currentAddress.unionEng : gatePassModel.currentAddress.unionBng%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5">
                                            <b><%=LM.getText(LC.GATE_PASS_VISITOR_ADD_UPOZILLA, loginDTO)%>
                                            </b></div>
                                        <div class="col-7"><%=Language.equalsIgnoreCase("English") ? gatePassModel.currentAddress.thanaEng : gatePassModel.currentAddress.thanaBng%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5">
                                            <b><%=LM.getText(LC.GATE_PASS_VISITOR_ADD_DISTRICT, loginDTO)%>
                                            </b></div>
                                        <div class="col-7"><%=Language.equalsIgnoreCase("English") ? gatePassModel.currentAddress.districtEng : gatePassModel.currentAddress.districtBng%>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <%
                                }
                            %>

                            <%
                                for (GatePassPersonModel gatePassPersonModel : gatePassModel.persons) {
                                    if (!gatePassPersonModel.cardNumber.isEmpty()) {
                            %>
                            <tr>
                                <td><%=StringUtils.convertToBanNumber(Integer.toString(count++))%>
                                </td>
                                <td><%=StringUtils.convertToBanNumber(gatePassPersonModel.cardNumber)%>
                                <td>
                                    <div class="row">
                                        <div class="col-5">
                                            <b><%=LM.getText(LC.GATE_PASS_VIEW_VISITOR_NAME, loginDTO)%>
                                            </b></div>
                                        <div class="col-7"><%=gatePassPersonModel.name%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5"><b><%=LM.getText(LC.GATE_PASS_VIEW_MOBILE, loginDTO)%>
                                        </b></div>
                                        <div class="col-7"><%=Language.equalsIgnoreCase("English") ? gatePassPersonModel.mobileNoEng : gatePassPersonModel.mobileNoBng%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5"><b><%=LM.getText(LC.GATE_PASS_VIEW_CREDENTIAL, loginDTO)%>
                                        </b></div>
                                        <div class="col-7"><%=Language.equalsIgnoreCase("English") ? gatePassPersonModel.credentialTypeEng + "-" + Utils.getDigits(gatePassPersonModel.credentialNo, "English") :
                                                gatePassPersonModel.credentialTypeBng + " - " + Utils.getDigits(gatePassPersonModel.credentialNo, "Bangla")%>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-5">
                                            <b><%=LM.getText(LC.GATE_PASS_VISITOR_ADD_VILLAGE, loginDTO)%>
                                            </b></div>
                                        <div class="col-7"><%=gatePassModel.currentAddress.addressText%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5">
                                            <b><%=LM.getText(LC.GATE_PASS_VISITOR_ADD_UNION, loginDTO)%>
                                            </b></div>
                                        <div class="col-7"><%=Language.equalsIgnoreCase("English") ? gatePassModel.currentAddress.unionEng : gatePassModel.currentAddress.unionBng%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5">
                                            <b><%=LM.getText(LC.GATE_PASS_VISITOR_ADD_UPOZILLA, loginDTO)%>
                                            </b></div>
                                        <div class="col-7"><%=Language.equalsIgnoreCase("English") ? gatePassModel.currentAddress.thanaEng : gatePassModel.currentAddress.thanaBng%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5">
                                            <b><%=LM.getText(LC.GATE_PASS_VISITOR_ADD_DISTRICT, loginDTO)%>
                                            </b></div>
                                        <div class="col-7"><%=Language.equalsIgnoreCase("English") ? gatePassModel.currentAddress.districtEng : gatePassModel.currentAddress.districtBng%>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <%
                                    }
                                }
                            %>
                        </table>
                    </div>
                </div>
                <%
                    if (gatePassModel.items.size() > 0) {
                %>
                <div class="mt-5">
                    <h6><%=Language.equalsIgnoreCase("English") ? "What visitor will bring with him:" : "সাথে যা যা নিতে পারবেনঃ"%>
                    </h6>
                    <div class="table-responsive">
                        <table class="table text-center">
                            <thead>
                            <tr>
                                <th style="color: #0098bf !important"><%=Language.equalsIgnoreCase("English") ? "ITEM (DESCRIPTION)" : "আইটেম (বিবরণ)"%>
                                </th>
                                <th style="color: #0098bf !important"><%=Language.equalsIgnoreCase("English") ? "AMOUNT" : "পরিমান"%>
                                </th>
                                <th style="color: #0098bf !important"><%=Language.equalsIgnoreCase("English") ? "ITEM (DESCRIPTION)" : "আইটেম (বিবরণ)"%>
                                </th>
                                <th style="color: #0098bf !important"><%=Language.equalsIgnoreCase("English") ? "AMOUNT" : "পরিমান"%>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <%
                                int itemCountForNewDesign = 1;
                                for (GatePassItemModel gatePassItemModel : gatePassModel.items) {
                                    if (itemCountForNewDesign % 2 == 1) {
                            %>
                            <tr>
                                <%
                                    }
                                %>
                                <th><%=Language.equalsIgnoreCase("English") ? (gatePassItemModel.description.equals("") ? gatePassItemModel.nameEng : gatePassItemModel.nameEng + " (" + gatePassItemModel.description + ")") :
                                        (gatePassItemModel.description.equals("") ? gatePassItemModel.nameBng : gatePassItemModel.nameBng + " (" + gatePassItemModel.description + ")")%>
                                </th>


                                <td><%=Language.equalsIgnoreCase("English") ? gatePassItemModel.approvedAmount : StringUtils.convertToBanNumber(String.valueOf(gatePassItemModel.approvedAmount))%>
                                </td>

                                <%
                                    if (itemCountForNewDesign % 2 == 0) {
                                %>

                            </tr>
                            <%
                                }
                            %>
                            <%
                                    itemCountForNewDesign++;
                                }

                                if (gatePassModel.items.size() % 2 == 1) {
                            %>
                            <td></td>
                            <td></td>
                            </tr>
                            <%
                                }
                            %>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%
                    }
                %>

                <%
                    if (gatePassModel.gatePassType == 10) {
                %>
                <div class="row mt-4">
                    <div class="col-lg-6">
                        <b><%=LM.getText(LC.GATE_PASS_PARLIAMENT_BUILDING_REASON_FOR_VISIT, loginDTO)%>
                            : </b><%=gatePassModel.reasonForVisit%>
                    </div>
                </div>
                <%
                    }
                %>

                <div class="row mt-2">
                    <div class="col-4 text-center">
                        <div class="signature-div d-flex justify-content-start">
                            <div>
                                <div>
                                    <img class="signature-image"
                                         src='<%=StringUtils.getBase64EncodedImageStr(gatePassModel.referrerSignature)%>'/>
                                    <br>
                                    <%=StringUtils.getFormattedTime(false, gatePassModel.firstLayerApprovalTime)%>
                                </div>
                                <div>
                                    <%=gatePassModel.referrerBng%><br>
                                    <%=gatePassModel.referrerOfficeOrgBng%><br>
                                    <%=gatePassModel.referrerOfficeBng%><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4 text-center if-approved-by-sergeant" style="display: none">
                        <div class="signature-div">
                            <img
                                    width="200"
                                    style="object-fit: contain; transform: rotate(10.2deg)"
                                    src="<%=context%>assets/static/approval_seal.png"
                                    alt="logo"
                                    class="logo-default"
                            />
                        </div>
                    </div>
                    <div class="col-4 text-center if-approved-by-sergeant" style="display: none">
                        <div class="signature-div">
                            <div>
                                <div>
                                    <img class="signature-image"
                                         src='<%=StringUtils.getBase64EncodedImageStr(gatePassModel.sergeantSignature)%>'/>
                                    <br>
                                    <%=StringUtils.getFormattedTime(false, gatePassModel.secondLayerApprovalTime)%>
                                </div>
                                <div>
                                    <%=gatePassModel.sergeantNameBng%><br>
                                    <%=gatePassModel.sergeantOfficeOrgBng%><br>
                                    <%=gatePassModel.sergeantOfficeBng%><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mt-5 text-right no-print">
                    <button id=go-back' class="btn btn-sm cancel-btn shadow text-white btn-border-radius"
                            onclick="redirectToSearch()">
                        <%=Language.equalsIgnoreCase("English") ? "GO BACK TO SEARCH PAGE" : "সার্চ পেইজে ফিরুন"%>
                    </button>
                    <button id='print-btn' class="btn btn-sm shadow ml-2 text-white btn-border-radius"
                            style="background-color: #0bd398">
                        <%=Language.equalsIgnoreCase("English") ? "PRINT" : "প্রিন্ট করুন"%>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script
        src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.print/1.6.2/jQuery.print.min.js"
        integrity="sha512-t3XNbzH2GEXeT9juLjifw/5ejswnjWWMMDxsdCg4+MmvrM+MwqGhxlWeFJ53xN/SBHPDnW0gXYvBx/afZZfGMQ=="
        crossorigin="anonymous"
        referrerpolicy="no-referrer"
></script>

<script type="text/javascript">

    $(document).ready(function () {

        <%
            if (gatePassModel.secondLayerApprovalStatus!=Gate_passDAO.approvalStatusApproved) {
        %>
        $('#issue-card').attr('disabled', true);
        <%
            } else {
        %>
        document.getElementsByClassName('if-approved-by-sergeant')[0].style.display = 'block';
        document.getElementsByClassName('if-approved-by-sergeant')[1].style.display = 'block';
        <%
            }
        %>
    });

    $("#print-btn").click(function () {
        $.print("#modalbody");
    });

    function redirectToSearch() {
        window.location.href = '<%=request.getContextPath()%>' + '/Gate_passServlet?actionType=parliamentBuildingSecuritySearch';
    }

</script>

