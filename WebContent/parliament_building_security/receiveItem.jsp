<%
    String context = "../../.."  + request.getContextPath() + "/";
%>
<link href="<%=context%>/assets/css/pagecustom.css" rel="stylesheet" type="text/css"/>
<jsp:include page="../common/layout.jsp" flush="true">
    <jsp:param name="title" value="Receive Item" />
    <jsp:param name="body" value="../parliament_building_security/receiveItemBody.jsp" />
</jsp:include>