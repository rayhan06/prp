<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="gate_pass_item.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="gate_pass_affiliated_person.Gate_pass_affiliated_personDAO" %>

<%
    Gate_pass_itemDTO gate_pass_itemDTO;
    gate_pass_itemDTO = (Gate_pass_itemDTO) request.getAttribute("gate_pass_itemDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (gate_pass_itemDTO == null) {
        gate_pass_itemDTO = new Gate_pass_itemDTO();
    }
    System.out.println("gate_pass_itemDTO = " + gate_pass_itemDTO);

    String Language = LM.getText(LC.GATE_PASS_ITEM_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    int rowNumber = Integer.parseInt(request.getParameter("rowNumber"));
    long gatePassId = Long.parseLong(request.getParameter("gatePassId"));
    Gate_pass_affiliated_personDAO personDAO = new Gate_pass_affiliated_personDAO();
    Gate_pass_itemDAO gatePassItemDAO = new Gate_pass_itemDAO();
%>

<tr id="gate_pass_item_hold_<%=rowNumber%>">
    <input type="hidden" name="additionItemId_hold_<%=rowNumber%>" id="additionItemId_hold_<%=rowNumber%>" value="0"/>
    <td>
        <select class='form-control holder'
                name='parliamentNameId__hold<%=rowNumber%>'
                id='parliamentNameId__hold<%=rowNumber%>'
                tag='pb_html'>
            <%=personDAO.buildVisitorOptions(Language, gatePassId)%>
        </select>
    </td>
    <td>
        <select class='form-control pih' name='parliamentItemId_hold_<%=rowNumber%>'
                id='parliamentItemId_hold_<%=rowNumber%>' onchange="getMaxItemAmount(this)" tag='pb_html'>
            <%=gatePassItemDAO.buildHoldingItemsDropdown(Language, gatePassId)%>
        </select>
    </td>
    <td>
        <input type='number' class='form-control' name='amount_hold_<%=rowNumber%>'
               id='amount_hold_<%=rowNumber%>' value="1" min="1" tag='pb_html'>
    </td>
    <td class="text-center">

        <%--        <button class="btn delete-trash-btn">--%>
        <i class="fa fa-trash remove-previously-chosen-item-hold my-2 ml-1"></i>
        <%--        </button>--%>
    </td>
</tr>






