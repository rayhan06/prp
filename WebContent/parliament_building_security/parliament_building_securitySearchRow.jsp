<%@page pageEncoding="UTF-8" %>

<%@page import="gate_pass.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.GATE_PASS_EDIT_LANGUAGE, loginDTO);
    Gate_pass_searchModel gate_passDTO = (Gate_pass_searchModel) request.getAttribute("gate_passDTO");
    String value = "";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<td id='gatePassType'>
    <%=Language.equalsIgnoreCase("english") ? gate_passDTO.gatePassTypeEng : gate_passDTO.gatePassTypeBng %>
</td>

<td id='visitingDate'>
    <%
        value = gate_passDTO.visitingDate + "";
    %>
    <%
        String formatted_visitingDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%="<b>" + Utils.getDigits(formatted_visitingDate, Language) + "</b>"%>

</td>

<td id='gatePassVisitorName'>
    <%="<em><b>" + gate_passDTO.name + "</b></em>"%>


</td>

<td id='gatePassReferrerName'>
    <%
        value = gate_passDTO.mobileNumber + "";
    %>

    <%=Utils.getDigits(value, Language)%>
</td>

<td id='gatePassVisitorCredential'>
    <%=Language.equalsIgnoreCase("english") ? gate_passDTO.credentialTypeEng + " - " : gate_passDTO.credentialTypeBng + " - " %>
    <%
        value = gate_passDTO.credentialNo + "";
    %>

    <%=Utils.getDigits(value, Language)%>

</td>

<td id='gatePassAprStat'>
    <%
        if (gate_passDTO.isIssued == 1) {
    %>
        <span class="text-primary font-italic font-weight-bold">
            <%=Language.equalsIgnoreCase("English") ? "Issued" : "ইস্যুকৃত"%>
        </span>
    <%
        } else if (gate_passDTO.firstLayerApprovalStatus==Gate_passDAO.approvalStatusPending) {
    %>
        <span class="text-primary font-italic font-weight-bold">
            <%=Language.equalsIgnoreCase("English") ? "Pending" : "অনিষ্পন্ন"%>
        </span>
    <%
        } else if (gate_passDTO.firstLayerApprovalStatus==Gate_passDAO.approvalStatusCancelled) {
    %>
        <span class="text-danger font-italic font-weight-bold">
                <%=Language.equalsIgnoreCase("English") ? "Dismissed" : "অননুমোদিত"%>
        </span>
    <%
        } else if (gate_passDTO.firstLayerApprovalStatus==Gate_passDAO.approvalStatusApproved) {
            if (gate_passDTO.secondLayerApprovalStatus==Gate_passDAO.approvalStatusPending) {
    %>
        <span class="text-primary font-italic font-weight-bold">
                    <%=Language.equalsIgnoreCase("English") ? "pending" : "অনিষ্পন্ন"%>
        </span>
    <%
            } else if (gate_passDTO.secondLayerApprovalStatus==Gate_passDAO.approvalStatusCancelled) {
    %>
        <span class="text-danger font-italic font-weight-bold">
                    <%=Language.equalsIgnoreCase("English") ? "Dismissed" : "অননুমোদিত"%>
        </span>
    <%
            } else if (gate_passDTO.secondLayerApprovalStatus==Gate_passDAO.approvalStatusApproved) {
    %>
        <span class="text-success font-italic font-weight-bold">
                    <%=Language.equalsIgnoreCase("English") ? "Approved" : "অনুমোদিত"%>
        </span>
    <%
            } else if (gate_passDTO.secondLayerApprovalStatus==Gate_passDAO.approvalStatusProcessing) {
    %>
    <span class="text-info font-italic font-weight-bold">
                    <%=Language.equalsIgnoreCase("English") ? "Processing" : "প্রক্রিয়াধীন"%>
        </span>
    <%
            }
        }
    %>

</td>

<td>
    <button
            type="button" class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Gate_passServlet?actionType=parliamentBuildingSecurityView&ID=<%=gate_passDTO.gate_pass_id%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>
