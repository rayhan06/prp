<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="other_office_departments_list.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
Other_office_departments_listDTO other_office_departments_listDTO = new Other_office_departments_listDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	other_office_departments_listDTO = Other_office_departments_listDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = other_office_departments_listDTO;
String tableName = "other_office_departments_list";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.OTHER_OFFICE_DEPARTMENTS_LIST_ADD_OTHER_OFFICE_DEPARTMENTS_LIST_ADD_FORMNAME, loginDTO);
String servletName = "Other_office_departments_listServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Other_office_departments_listServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-11">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=other_office_departments_listDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.OTHER_OFFICE_DEPARTMENTS_LIST_ADD_OTHEROFFICETYPE, loginDTO)%>															</label>
                                                            <div class="col-8">
																<select class='form-control'  name='otherOfficeType' id = 'otherOfficeType_select_<%=i%>'   tag='pb_html'>
																<%
																	Options = CommonDAO.getOptions(Language, "other_office", other_office_departments_listDTO.otherOfficeType);
																%>
																<%=Options%>
																</select>
		
															</div>
                                                      </div>									
											
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
               <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.OTHER_OFFICE_DEPARTMENTS_LIST_ADD_OTHER_OFFICE_DEPARTMENT, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
									<tr>
										<th><%=LM.getText(LC.OTHER_OFFICE_DEPARTMENTS_LIST_ADD_OTHER_OFFICE_DEPARTMENT_NAMEEN, loginDTO)%></th>
										<th><%=LM.getText(LC.OTHER_OFFICE_DEPARTMENTS_LIST_ADD_OTHER_OFFICE_DEPARTMENT_NAMEBN, loginDTO)%></th>

										<th><%=LM.getText(LC.OTHER_OFFICE_DEPARTMENTS_LIST_ADD_OTHER_OFFICE_DEPARTMENT_REMOVE, loginDTO)%></th>
									</tr>
								</thead>
							<tbody id="field-OtherOfficeDepartment">
						
						
								<%
									if(actionName.equals("ajax_edit")){
										int index = -1;
										
										
										for(OtherOfficeDepartmentDTO otherOfficeDepartmentDTO: other_office_departments_listDTO.otherOfficeDepartmentDTOList)
										{
											index++;
											
											System.out.println("index index = "+index);

								%>	
							
								<tr id = "OtherOfficeDepartment_<%=index + 1%>">
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='otherOfficeDepartment.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=otherOfficeDepartmentDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='otherOfficeDepartment.otherOfficeDepartmentsListId' id = 'otherOfficeDepartmentsListId_hidden_<%=childTableStartingID%>' value='<%=otherOfficeDepartmentDTO.otherOfficeDepartmentsListId%>' tag='pb_html'/>
									</td>
								
									<td>										





																<input type='text' class='form-control'  name='otherOfficeDepartment.nameEn' id = 'nameEn_text_<%=childTableStartingID%>' value='<%=otherOfficeDepartmentDTO.nameEn%>'   tag='pb_html'/>					
									</td>
									<td>										





																<input type='text' class='form-control'  name='otherOfficeDepartment.nameBn' id = 'nameBn_text_<%=childTableStartingID%>' value='<%=otherOfficeDepartmentDTO.nameBn%>'   tag='pb_html'/>					
									</td>
									
									
									<td>
										<span id='chkEdit'>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
												   class="form-control-sm"/>
										</span>
									</td>
								</tr>								
								<%	
											childTableStartingID ++;
										}
									}
								%>						
						
								</tbody>
							</table>
						</div>
						<div class="form-group">
								<div class="col-xs-9 text-right">
									<button
											id="add-more-OtherOfficeDepartment"
											name="add-moreOtherOfficeDepartment"
											type="button"
											onclick="childAdded(event, 'OtherOfficeDepartment')"
											class="btn btn-sm text-white add-btn shadow">
										<i class="fa fa-plus"></i>
										<%=LM.getText(LC.HM_ADD, loginDTO)%>
									</button>
									<button
											id="remove-OtherOfficeDepartment"
											name="removeOtherOfficeDepartment"
											type="button"
											onclick="childRemoved(event, 'OtherOfficeDepartment')"
											class="btn btn-sm remove-btn shadow ml-2 pl-4">
										<i class="fa fa-trash"></i>
									</button>
								</div>
							</div>
					
							<%OtherOfficeDepartmentDTO otherOfficeDepartmentDTO = new OtherOfficeDepartmentDTO();%>
					
							<template id="template-OtherOfficeDepartment" >						
								<tr>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='otherOfficeDepartment.iD' id = 'iD_hidden_' value='<%=otherOfficeDepartmentDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='otherOfficeDepartment.otherOfficeDepartmentsListId' id = 'otherOfficeDepartmentsListId_hidden_' value='<%=otherOfficeDepartmentDTO.otherOfficeDepartmentsListId%>' tag='pb_html'/>
									</td>
									
									<td>





																<input type='text' class='form-control'  name='otherOfficeDepartment.nameEn' id = 'nameEn_text_' value='<%=otherOfficeDepartmentDTO.nameEn%>'   tag='pb_html'/>					
									</td>
									<td>





																<input type='text' class='form-control'  name='otherOfficeDepartment.nameBn' id = 'nameBn_text_' value='<%=otherOfficeDepartmentDTO.nameBn%>'   tag='pb_html'/>					
									</td>
									
								
									<td>
											<span id='chkEdit'>
												<input type='checkbox' name='checkbox' value='' deletecb='true'
													   class="form-control-sm"/>
											</span>
									</td>
								</tr>								
						
							</template>
                        </div>
                    </div>
                <div class="form-actions text-right mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.OTHER_OFFICE_DEPARTMENTS_LIST_ADD_OTHER_OFFICE_DEPARTMENTS_LIST_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.OTHER_OFFICE_DEPARTMENTS_LIST_ADD_OTHER_OFFICE_DEPARTMENTS_LIST_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>				

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);

	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Other_office_departments_listServlet");	
}

function init(row)
{


	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;

function processRowsWhileAdding(childName)
{
	if(childName == "OtherOfficeDepartment")
	{			
	}
}

</script>






