

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="other_office_departments_list.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Other_office_departments_listServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Other_office_departments_listDTO other_office_departments_listDTO = Other_office_departments_listDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = other_office_departments_listDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.OTHER_OFFICE_DEPARTMENTS_LIST_ADD_OTHER_OFFICE_DEPARTMENTS_LIST_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.OTHER_OFFICE_DEPARTMENTS_LIST_ADD_OTHER_OFFICE_DEPARTMENTS_LIST_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.OTHER_OFFICE_DEPARTMENTS_LIST_ADD_OTHEROFFICETYPE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=CommonDAO.getName(Language, "other_office", other_office_departments_listDTO.otherOfficeType)%>
                                    </div>
                                </div>

							</div>
                        </div>
                    </div>
                </div>
            </div>			

             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%=LM.getText(LC.OTHER_OFFICE_DEPARTMENTS_LIST_ADD_OTHER_OFFICE_DEPARTMENT, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.OTHER_OFFICE_DEPARTMENTS_LIST_ADD_OTHER_OFFICE_DEPARTMENT_NAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.OTHER_OFFICE_DEPARTMENTS_LIST_ADD_OTHER_OFFICE_DEPARTMENT_NAMEBN, loginDTO)%></th>
								
							</tr>
							<%
                        	OtherOfficeDepartmentDAO otherOfficeDepartmentDAO = OtherOfficeDepartmentDAO.getInstance();
                         	List<OtherOfficeDepartmentDTO> otherOfficeDepartmentDTOs = (List<OtherOfficeDepartmentDTO>)otherOfficeDepartmentDAO.getDTOsByParent("other_office_departments_list_id", other_office_departments_listDTO.iD);
                         	
                         	for(OtherOfficeDepartmentDTO otherOfficeDepartmentDTO: otherOfficeDepartmentDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%=otherOfficeDepartmentDTO.nameEn%>
										</td>
										<td>
											<%=otherOfficeDepartmentDTO.nameBn%>
										</td>
										
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
        </div>
    </div>
</div>