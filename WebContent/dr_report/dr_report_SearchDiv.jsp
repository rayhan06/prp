<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@ page import="pb.*"%>
<%
String Language = LM.getText(LC.DR_REPORT_EDIT_LANGUAGE, loginDTO);
String Options;
int i = 0;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<%@include file="../pbreport/yearmonth.jsp"%>
<%@include file="../pbreport/calendar.jsp"%>
<div id = "doctorType" class="search-criteria-div">
        <div class="form-group">
        	<label class="col-sm-3 control-label">
        		<%=LM.getText(LC.DR_REPORT_WHERE_DOCTORTYPE, loginDTO)%>
        	</label>
        	<div class="col-sm-6">
				<select class='form-control'  name='doctorType' id = 'doctorType' onChange="ajaxSubmit();">		
					<%		
					Options = CommonDAO.getDoctorsByOrganogramID(-1, "", CommonDAO.DR, false);								
					%>
					<%=Options%>
				</select>
        	</div>
        </div>
</div>
<div id = "patientName" class="search-criteria-div">
        <div class="form-group">
        	<label class="col-sm-3 control-label">
        		<%=LM.getText(LC.DR_REPORT_WHERE_PATIENTNAME, loginDTO)%>
        	</label>
        	<div class="col-sm-6">
				<input class='form-control'  name='patientName' id = 'patientName' onKeyUp="ajaxSubmit();" value=""/>							
        	</div>
        </div>
</div>
<script type="text/javascript">
function init()
{
	addables = [1, 0, 0];
    dateTimeInit($("#Language").val());
}
function PreprocessBeforeSubmiting()
{
}
</script>