<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="committees_mapping.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="committees.CommitteesRepository" %>
<%@ page import="employee_records.Employee_recordsRepository" %>

<style>
    td {
        text-align: center;
        vertical-align: middle;
    }

    td.hidden {
        display: none;
    }
</style>

<%
    Committees_mappingDTO committees_mappingDTO = new Committees_mappingDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        committees_mappingDTO = Committees_mappingDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = committees_mappingDTO;
    String tableName = "committees_mapping";
    long today = System.currentTimeMillis();
    int endYear = Calendar.getInstance().get(Calendar.YEAR) + 10;
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.COMMITTEES_MAPPING_ADD_COMMITTEES_MAPPING_ADD_FORMNAME, loginDTO);
    String servletName = "Committees_mappingServlet";

%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Committees_mappingServlet?actionType=<%=actionName%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=committees_mappingDTO.iD%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='electionWiseMpId'
                                           id='electionWiseMpId'
                                           value='<%=committees_mappingDTO.electionWiseMpId%>' tag='pb_html'/>


<%--                                    Committee Name, Parliament Number, start date, end date and then enter MP name with constituency and hit add button --%>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=Language.equalsIgnoreCase("English") ? "Committee Name" : "কমিটির নাম"%><span
                                                class="required">*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='committeesId'
                                                    id='committeesId' tag='pb_html'>
                                                <%--                                                <%= actionName.equalsIgnoreCase("ajax_edit") ? CommitteesRepository.getInstance().buildOptions(Language, committees_mappingDTO.committeesId) : CommitteesRepository.getInstance().buildOptions(Language, null) %>--%>
                                                <%=CommitteesRepository.getInstance().buildOptions(Language, committees_mappingDTO.committeesId)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=Language.equalsIgnoreCase("English") ? "Parliament Number" : "পার্লামেন্ট নাম্বার"%><span
                                                class="required">*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='electionDetailsId' id='electionDetailsId'
                                                    onchange="getElectionConstituencyListWithMpName()"  tag='pb_html'>
                                                <%= Election_detailsRepository.getInstance().buildOptions(Language, committees_mappingDTO.electionDetailsId) %>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.COMMITTEES_MAPPING_ADD_STARTDATE, loginDTO)%><span
                                                class="required">*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="start-date-js"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='startDate' id='start-date'
                                                   value=''
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.COMMITTEES_MAPPING_ADD_ENDDATE, loginDTO)%><span
                                                class="required">*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="end-date-js"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                <jsp:param name="END_YEAR" value="<%=endYear%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='endDate' id='end-date'
                                                   value=''
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"
                                               for="employeeRecordsId">
                                            <%=Language.equalsIgnoreCase("English") ? "Member of Parliament" : "সাংসদ"%><span
                                                class="required">*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='employeeRecordsId'
                                                    id='employeeRecordsId'
                                                    onchange='activateAddButton()'>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                        </label>
                                        <div class="col-md-9 text-right">
                                            <button type="button" id="addButton" onclick="addCommitteesRow()" class="btn-success btn-sm shadow text-white border-0 submit-btn" disabled><%=Language.equalsIgnoreCase("English") ? "Add MP" : "যোগ করুন"%></button>
                                        </div>
                                    </div>

                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=committees_mappingDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedBy'
                                           id='insertedBy_hidden_<%=i%>' value='<%=committees_mappingDTO.insertedBy%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='modifiedBy'
                                           id='modifiedBy_hidden_<%=i%>' value='<%=committees_mappingDTO.modifiedBy%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=committees_mappingDTO.searchColumn%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>' value='<%=committees_mappingDTO.isDeleted%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=committees_mappingDTO.lastModificationTime%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='encodedString' id='encodedString' value='' tag='pb_html'/>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive mt-5" id="tableDataDiv" style="display: none;">
                    <table id="tableData" class="table table-bordered table-striped text-nowrap">
                        <thead>
                        <tr>
                            <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "Serial No" : "ক্রমিক নং"%>
                            </th>
                            <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "MP Name" : "সাংসদের নাম"%>
                            </th>
                            <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "Constituency Number" : "নির্বাচনী এলাকা নং"%>
                            </th>
                            <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "Constituency Name" : "নির্বাচনী এলাকার নাম"%>
                            </th>
                            <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "Role in Committee" : "কমিটিতে ভূমিকা"%>
                            </th>
                            <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "Remove" : "মুছুন"%>
                            </th>
                        </tr>
                        </thead>
                        <tbody></tbody>

                    </table>
                </div>
                <div class="row mt-4" id="main_submit_button" style="display:none;">
                    <div class="col-12">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.COMMITTEES_MAPPING_ADD_COMMITTEES_MAPPING_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit" onclick="submitCommittee()">
                                <%=LM.getText(LC.COMMITTEES_MAPPING_ADD_COMMITTEES_MAPPING_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<script type="text/javascript">

    let committeeRoleOptions;
    let committeeMemberMap;


    function getElectionConstituencyListWithMpName() {
        let electionDetailsId = $("#electionDetailsId").val();
        if (electionDetailsId != -1 && electionDetailsId != "") {
            const url = 'Committees_mappingServlet?actionType=getElectionConstituencyListWithMpName&electionDetailsId=' + electionDetailsId;

            $.ajax({
                url: url,
                type: "GET",
                async: true,
                success: function (response) {
                    document.getElementById('employeeRecordsId').innerHTML = response;
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

        $("#addButton").prop("disabled",true);
    }

    function activateAddButton() {
        $("#addButton").prop("disabled",false);
    }

    function addDataToCommitteeMemberMap(mpInfo, emp_record_id) {
        committeeMemberMap.set(emp_record_id, mpInfo);
    }


    function addCommitteesRow() {
        $('#tableDataDiv').show();

        let mpInfo = $("#employeeRecordsId :selected").text();
        let emp_record_id = $("#employeeRecordsId").val();

        addDataToCommitteeMemberMap(mpInfo, emp_record_id);
        appendDataToTable(committeeMemberMap);
    }

    function appendDataToTable(committeeMap) {

        // clear previous table data
        $('#tableData > tbody').empty();

        let serialNumber = 0;
        for (const [key, value] of committeeMap.entries()) {
            let mpInfo = value;
            const mpInfoArray = mpInfo.split("|");
            let emp_record_id = key;

            const newRowStr = "<tr><td>" + getDigits(serialNumber + 1) + "</td>" +
                "<td>" + mpInfoArray[2] + "</td>" +
                "<td>" + mpInfoArray[0] + "</td>" +
                "<td>" + mpInfoArray[1] + "</td>" +
                "<td><select class='form-control committeeDesignation' id=\"committeesRoleCat_" + serialNumber + "\" tag='pb_html' required>"+ committeeRoleOptions +"</select></td>" +
                "<td><i class=\"fa fa-trash\" style='color: #ff6a6a; cursor: pointer' onclick='deleteEntryFromMap("+ key +")'></i></td>" +
                "<td id=\"emp_record_id_" + serialNumber + "\" class=\"hidden\">" + emp_record_id + "</td></tr>";

            $('#tableData > tbody').append(stringToHtmlElement(newRowStr));

            serialNumber = serialNumber + 1;
        }

        if (committeeMap.size===0) {
            $('#main_submit_button').hide();
        } else {
            $('#main_submit_button').show();
        }
    }

    function appendDataToTableAtEdit(committeeMap) {

        // clear previous table data
        $('#tableData > tbody').empty();

        const language = '<%=Language%>';
        const englishLangName = 'English';
        let committeeRoleOptionsString;
        let serialNumber = 0;
        for (const [key, value] of committeeMap.entries()) {
            let mpInfo = value;
            const mpInfoArray = mpInfo.split("|");
            let emp_record_id = key;

            if (parseInt(mpInfoArray[3], 10)===1) {
                if (language.toUpperCase() === englishLangName.toUpperCase()) {
                    committeeRoleOptionsString = "<option value = ''>Select Designation</option><option title = '' value = '1' selected>Chairman</option> <option title = '' value = '2'>Member</option>";
                } else {
                    committeeRoleOptionsString = "<option value = ''>পদবি বাছাই করুন</option><option title = '' value = '1' selected>সভাপতি</option> <option title = '' value = '2'>সদস্য</option>";
                }
            } else {
                if (language.toUpperCase() === englishLangName.toUpperCase()) {
                    committeeRoleOptionsString = "<option value = ''>Select Designation</option><option title = '' value = '1'>Chairman</option> <option title = '' value = '2' selected>Member</option>";
                } else {
                    committeeRoleOptionsString = "<option value = ''>পদবি বাছাই করুন</option><option title = '' value = '1'>সভাপতি</option> <option title = '' value = '2' selected>সদস্য</option>";
                }
            }

            const newRowStr = "<tr><td>" + getDigits(serialNumber + 1) + "</td>" +
                "<td>" + mpInfoArray[2] + "</td>" +
                "<td>" + mpInfoArray[0] + "</td>" +
                "<td>" + mpInfoArray[1] + "</td>" +
                "<td><select class='form-control committeeDesignation' id=\"committeesRoleCat_" + serialNumber + "\" tag='pb_html' required>"+ committeeRoleOptionsString +"</select></td>" +
                "<td><i  class=\"fa fa-trash\" style='color: #ff6a6a; cursor: pointer' onclick='deleteEntryFromMap("+ key +")'></i></td>" +
                "<td id=\"emp_record_id_" + serialNumber + "\" class=\"hidden\">" + emp_record_id + "</td></tr>";

            $('#tableData > tbody').append(stringToHtmlElement(newRowStr));

            serialNumber = serialNumber + 1;
        }

        if (committeeMap.size===0) {
            $('#main_submit_button').hide();
        } else {
            $('#main_submit_button').show();
        }
    }

    function deleteEntryFromMap(entryKey) {
        committeeMemberMap.delete(entryKey.toString());
        appendDataToTable(committeeMemberMap);
    }

    function stringToHtmlElement(str) {
        const template = document.createElement('template');
        template.innerHTML = str;
        return template.content.childNodes;
    }

    function onEditPage() {

        const url = 'Committees_mappingServlet?actionType=getEditPageRecord&electionDetailsId=' + <%=committees_mappingDTO.electionDetailsId%>
            +'&employeeRecordsId=' + <%=committees_mappingDTO.employeeRecordsId%>+'&committeesId=' + <%=committees_mappingDTO.committeesId%>;

        $.ajax({
            url: url,
            type: "GET",
            async: true,
            success: function (response) {
                document.getElementById('employeeRecordsId').innerHTML = response.options;
                setCommitteeMemberMap(response.allCommitteeMembers);
                $('#tableDataDiv').show();
                appendDataToTableAtEdit(committeeMemberMap);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    const row = 0;

    function submitCommittee() {
        let modelData = "";
        let elementName = "";
        for (let k = 0; k < committeeMemberMap.size; k++) {
            elementName = 'emp_record_id_' + k;
            modelData += $('#' + elementName).text();

            elementName = 'committeesRoleCat_' + k;
            modelData += ',' + $('#' + elementName).val() + ';';

        }

        $('#encodedString').val(modelData);

        $('#start-date').val(getDateStringById('start-date-js'));
        $('#end-date').val(getDateStringById('end-date-js'));
        let startDateValid = dateValidator('start-date-js', true, {
            'errorEn': 'Enter a valid committee start date!',
            'errorBn': 'কমিটি শুরু দিন টুকে ফেলুন!'
        });
        let endDateValid = dateValidator('end-date-js', true, {
            'errorEn': 'Enter a valid committee end date!',
            'errorBn': 'কমিটি বিলুপ্তির তারিখ টুকুন!'
        });

        if($('#bigform').valid() && startDateValid && endDateValid){
            submitAjaxForm();
        }
    }

    function buttonStateChange(value){
        $('#submit-btn').prop('disabled',value);
        $('#cancel-btn').prop('disabled',value);
    }


    $(document).ready(function () {

        const language = '<%=Language%>';
        const englishLangName = 'English';
        committeeMemberMap = new Map();

        if (language.toUpperCase() === englishLangName.toUpperCase()) {
            committeeRoleOptions = "<option value = ''>Select Designation</option><option title = '' value = '1'>Chairman</option> <option title = '' value = '2'>Member</option>";
        } else {
            committeeRoleOptions = "<option value = ''>পদবি বাছাই করুন</option><option title = '' value = '1'>সভাপতি</option> <option title = '' value = '2'>সদস্য</option>";
        }

        init();

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        $.validator.addMethod('committeesIdValidator', function (value, element) {
            return value;
        });

        // $.validator.addMethod('committeesRoleCatValidator', function (value, element) {
        //     return value;
        // });

        $.validator.addMethod('electionDetailsIdValidator', function (value, element) {
            return value;
        });

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                committeesId: {
                    required: false,
                    committeesIdValidator: true
                },
                // committeesRoleCat: {
                //     required: false,
                //     committeesRoleCatValidator: true
                // },
                electionDetailsId: {
                    required: false,
                    electionDetailsIdValidator: true
                }
            },
            messages: {
                committeesId: '<%=Language.equalsIgnoreCase("English") ? "Select Committee!" : "সংসদীয় স্থায়ী কমিটি বাছাই করুন!"%>',
                <%--committeesRoleCat: '<%=Language.equalsIgnoreCase("English") ? "Select designation in the committee!" : "পদবি বাছাই করুন!"%>',--%>
                electionDetailsId: '<%=Language.equalsIgnoreCase("English") ? "Select parliament number!" : "কততম নির্বাচন বাছাই করুন!"%>'
            }
        });


        $('.committeeDesignation').each(function() {
            $(this).rules('add', {
                required: true,
                messages: {
                    required:  "Select designation1"
                }
            });
        });

        $('#start-date-js').on('datepicker.change', () => {
            setMinDateById('end-date-js', getDateStringById('start-date-js'));
        });


        <%
            if (actionName.equalsIgnoreCase("ajax_add")) {
        %>
        setDateByTimestampAndId('start-date-js', <%=today%>);
        setDateByTimestampAndId('end-date-js', <%=today%>);
        <%
         } else {
        %>
        setDateByTimestampAndId('start-date-js', <%=committees_mappingDTO.startDate%>);
        setDateByTimestampAndId('end-date-js', <%=committees_mappingDTO.endDate%>);
        <%
        }
        %>
    });


    function init() {
        select2SingleSelector('#electionDetailsId', '<%=Language%>');
        select2SingleSelector('#employeeRecordsId', '<%=Language%>');
        select2SingleSelector('#committeesId', '<%=Language%>');

        <%
            if (actionName.equalsIgnoreCase("ajax_edit")) {
        %>
            onEditPage();
        <%
            }
        %>

    }

    function getDigits(number) {
        let input = number + '';
        let numbers;
        <%
            if(Language.equalsIgnoreCase("English")) {
        %>
        numbers = {
            '০': 0,
            '১': 1,
            '২': 2,
            '৩': 3,
            '৪': 4,
            '৫': 5,
            '৬': 6,
            '৭': 7,
            '৮': 8,
            '৯': 9
        };
        <%
            } else {
        %>
        numbers = {
            '0': '০',
            '1': '১',
            '2': '২',
            '3': '৩',
            '4': '৪',
            '5': '৫',
            '6': '৬',
            '7': '৭',
            '8': '৮',
            '9': '৯'
        };

        <%
            }
        %>

        let output = [];
        for (let i = 0; i < input.length; ++i) {
            if (numbers.hasOwnProperty(input[i])) {
                // console.log("here man!" + input[i] + " " + numbers);
                output.push(numbers[input[i]]);
            } else {
                output.push(input[i]);
            }
        }
        return output.join('');
    }

    function setCommitteeMemberMap(allMemberMap) {

        Object.entries(allMemberMap).forEach(([key, value]) => {
            committeeMemberMap.set(key, value);
        });
    }
</script>






