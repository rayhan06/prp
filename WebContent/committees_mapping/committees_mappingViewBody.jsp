<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="committees_mapping.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>
<%@ page import="committees.CommitteesRepository" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="election_wise_mp.Election_wise_mpRepository" %>
<%@ page import="election_constituency.Election_constituencyRepository" %>


<%
    String servletName = "Committees_mappingServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Committees_mappingDTO committees_mappingDTO = Committees_mappingDAO.getInstance().getDTOByID(id);
    CommonDTO commonDTO = committees_mappingDTO;
%>
<%@include file="../pb/viewInitializer.jsp" %>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.COMMITTEES_MAPPING_ADD_COMMITTEES_MAPPING_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8  offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-md-10 offset-md-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.COMMITTEES_MAPPING_ADD_COMMITTEES_MAPPING_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>


                                <div class="form-group row ">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=Language.equalsIgnoreCase("English") ? "Committee Name" : "কমিটির নাম"%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=CommitteesRepository.getInstance().getText(Language, committees_mappingDTO.committeesId)%>
                                    </div>
                                </div>

                                <div class="form-group row ">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=Language.equalsIgnoreCase("English") ? "MP name" : "সংসদ সদস্যের নাম"%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=Employee_recordsRepository.getInstance().getEmployeeName(committees_mappingDTO.employeeRecordsId, Language)%>
                                    </div>
                                </div>

                                <div class="form-group row ">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=Language.equalsIgnoreCase("English") ? "Parliament Number" : "পার্লামেন্ট নাম্বার"%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=Utils.getDigits(committees_mappingDTO.electionDetailsId, Language)%>
                                    </div>
                                </div>

                                <div class="form-group row ">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=Language.equalsIgnoreCase("English") ? "Election Constituency" : "নির্বাচনী এলাকা"%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=Election_constituencyRepository.getInstance().getConstituencyText(Election_wise_mpRepository.getInstance().getDTOByID(committees_mappingDTO.electionWiseMpId).electionConstituencyId, Language)%>
                                    </div>
                                </div>

                                <div class="form-group row ">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=Language.equalsIgnoreCase("English") ? "Designation" : "পদবি"%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=CatRepository.getInstance().getText(Language, "committees_role", committees_mappingDTO.committeesRoleCat)%>
                                    </div>
                                </div>

                                <div class="form-group row ">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.COMMITTEES_MAPPING_ADD_STARTDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = committees_mappingDTO.startDate + "";
                                        %>
                                        <%
                                            String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                        %>
                                        <%=Utils.getDigits(formatted_startDate, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row ">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.COMMITTEES_MAPPING_ADD_ENDDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = committees_mappingDTO.endDate + "";
                                        %>
                                        <%
                                            String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                        %>
                                        <%=Utils.getDigits(formatted_endDate, Language)%>


                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>