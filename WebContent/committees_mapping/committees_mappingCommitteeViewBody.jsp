<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="committees_mapping.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>
<%@ page import="committees.CommitteesRepository" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="election_wise_mp.Election_wise_mpRepository" %>
<%@ page import="election_constituency.Election_constituencyRepository" %>
<%@ page import="org.apache.commons.codec.language.bm.Lang" %>
<%@ page import="util.StringUtils" %>
<%@ page import="election_constituency.Election_constituencyDTO" %>


<%
    String servletName = "Committees_mappingServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Committees_mappingDTO committees_mappingDTO = Committees_mappingDAO.getInstance().getDTOByID(id);
    List<Committees_mappingDTO> committees_mappingDTOList = Committees_mappingDAO.getDTOsByCommitteeIdAndParliamentNo(committees_mappingDTO.committeesId, committees_mappingDTO.electionDetailsId);
    CommonDTO commonDTO = committees_mappingDTO;
    int serialNo = 1;
%>
<%@include file="../pb/viewInitializer.jsp" %>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.COMMITTEES_MAPPING_ADD_COMMITTEES_MAPPING_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row h5">
                <div class="col-md-6">
                    <label>
                        <strong><%=Language.equalsIgnoreCase("English") ? "Committee Name:  " : "কমিটির নামঃ "%></strong> <%=CommitteesRepository.getInstance().getText(Language, committees_mappingDTO.committeesId)%>
                    </label>
                </div>
                <div class="col-md-6 text-md-right">
                    <label>
                        <strong><%=Language.equalsIgnoreCase("English") ? "Parliament No: " : "পার্লামেন্ট নংঃ "%></strong> <%=Utils.getDigits(committees_mappingDTO.electionDetailsId, Language)%>
                    </label>
                </div>
            </div>
            <div class="mt-2">
                <div class="table-responsive ">
                    <table class="table table-striped table-bordered text-nowrap">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col"><%=Language.equalsIgnoreCase("English") ? "Serial No." : "ক্রমিক নং"%>
                            </th>
                            <th scope="col"><%=Language.equalsIgnoreCase("English") ? "Name of Hon'ble MP" : "সদস্যগণের নাম"%>
                            </th>
                            <th scope="col"><%=Language.equalsIgnoreCase("English") ? "Constituency" : "নির্বাচনী এলাকা"%>
                            </th>
                            <th scope="col"><%=Language.equalsIgnoreCase("English") ? "Designation" : "পদবি"%>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <%
                            String constituencyInfo = "";
                            Election_constituencyDTO election_constituencyDTO;
                            for (Committees_mappingDTO dto : committees_mappingDTOList) {
                                election_constituencyDTO = Election_constituencyRepository.getInstance().getElection_constituencyDTOByID(Election_wise_mpRepository.getInstance().getDTOByID(dto.electionWiseMpId).electionConstituencyId);
                                constituencyInfo = Language.equalsIgnoreCase("English") ? String.valueOf(election_constituencyDTO.constituencyNumber).concat(" ").concat(election_constituencyDTO.constituencyNameEn) :
                                        StringUtils.convertToBanNumber(String.valueOf(election_constituencyDTO.constituencyNumber)).concat(" ").concat(election_constituencyDTO.constituencyNameBn);
                        %>
                        <tr>
                            <th scope="row"><%=Utils.getDigits(serialNo++, Language)%>
                            </th>
                            <td><%=Employee_recordsRepository.getInstance().getEmployeeName(dto.employeeRecordsId, Language)%>
                            </td>
                            <td><%=constituencyInfo%>
                            </td>
                            <td><%=CatRepository.getName(Language, "committees_role", dto.committeesRoleCat)%>
                            </td>
                        </tr>
                        <%
                            }
                        %>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-right mt-1">
                    <button class="btn btn-sm btn-success shadow btn-border-radius" onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=commonDTO.iD%>'">
                        <%=Language.equalsIgnoreCase("English") ? "Edit Committee" : "কমিটি পরিবর্তন করুন"%></button>
                </div>
            </div>
        </div>
    </div>
</div>