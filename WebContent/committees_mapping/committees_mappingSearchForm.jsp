<%@page contentType="text/html;charset=utf-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="committees_mapping.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="committees.CommitteesRepository" %>
<%@ page import="employee_records.Employee_recordsRepository" %>


<%
    String navigator2 = "navCOMMITTEES_MAPPING";
    String servletName = "Committees_mappingServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead class="text-nowrap">
        <tr>
            <th><%=Language.equalsIgnoreCase("English") ? "Committee Name" : "কমিটির নাম"%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "MP name" : "সংসদ সদস্যের নাম"%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "Designation" : "পদবি"%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "Start Date" : "কমিটির পারম্ভিক দিন"%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "End Date" : "কমিটি বিলুপ্তির দিন"%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "View Committee" : "কমিটি দেখুন"%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "View" : "দেখুন"%>
            </th>
            <th><%=LM.getText(LC.COMMITTEES_MAPPING_SEARCH_COMMITTEES_MAPPING_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span><%=Language.equalsIgnoreCase("English") ? "All" : "সবগুলো"%></span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<Committees_mappingDTO>) rn2.list;

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Committees_mappingDTO committees_mappingDTO = (Committees_mappingDTO) data.get(i);


        %>
        <tr>


            <td>
                <%=CommitteesRepository.getInstance().getText(Language, committees_mappingDTO.committeesId)%>
            </td>

            <td>
                <%=Employee_recordsRepository.getInstance().getEmployeeName(committees_mappingDTO.employeeRecordsId, Language)%>
            </td>

            <td>
                <%=CatRepository.getInstance().getText(Language, "committees_role", committees_mappingDTO.committeesRoleCat)%>
            </td>

            <td>
                <%
                    value = committees_mappingDTO.startDate + "";
                %>
                <%
                    String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                %>
                <%=Utils.getDigits(formatted_startDate, Language)%>


            </td>

            <td>
                <%
                    value = committees_mappingDTO.endDate + "";
                %>
                <%
                    String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                %>
                <%=Utils.getDigits(formatted_endDate, Language)%>


            </td>

            <%CommonDTO commonDTO = committees_mappingDTO; %>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=viewCommittee&ID=<%=commonDTO.iD%>'"
                >
                    <i class="fa fa-eye"></i>
                </button>
            </td>
            <%@include file="../pb/searchAndViewButton.jsp" %>

            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID'
                                                 value='<%=committees_mappingDTO.iD%>'/></span>
                </div>
            </td>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			