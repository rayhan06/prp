<%@ page import="util.UtilCharacter" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="kt-portlet__body" style="background-color: white">
    <div id="office_req_status" style="height: 300px;"></div>
</div>

<script type="text/javascript">
    $(document).ready(()=>{
        officeReqStatus();
    })
    let officeReqStatusData;



    function officeReqStatus(){
            let url = "AssetDashboardServlet?actionType=office_request";
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    officeReqStatusData = fetchedData;
                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawOfficeReqStatus);
                },
                error: function (error) {
                    console.log(error);
                }
            });
    }

    function drawOfficeReqStatus() {
        const data = new google.visualization.DataTable();
        data.addColumn('string', 'Status');
        data.addColumn('number', 'Count');
        if(officeReqStatusData){
            for(let i in officeReqStatusData){
                data.addRows([[String(i),officeReqStatusData[i]]]);
            }
        }

        const view = new google.visualization.DataView(data);

        const options = {
            title: '<%=UtilCharacter.getDataByLanguage(language, "অবস্থা অনুযায়ী অফিস বরাদ্দের আবেদন", "Office Assignment Request Count By Status")%>',
            // is3D: true,
            // legend: { position: "none" }
        };
        const chart = new google.visualization.PieChart(document.getElementById('office_req_status'));
        chart.draw(view, options);
    }
</script>