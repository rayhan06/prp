<%@ page import="util.UtilCharacter" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="kt-portlet__body" style="background-color: white">
    <div id="house_count" style="height: 300px;"></div>
</div>

<script type="text/javascript">
    $(document).ready(()=>{
        getHouseCountData();
    });

    let houseCountData = [] ;

    function drawChartHouse() {
        let dataArray = [];

        for(let i = 0; i < houseCountData.length; i++){
            if(houseCountData[i] && houseCountData[i].type1) {
                dataArray.push([houseCountData[i].type1,
                    (i === 0 ? houseCountData[i].type2 : houseCountData[i].type2 / 1),
                    (i === 0 ? houseCountData[i].type3 : houseCountData[i].type3 / 1),
                    (i === 0 ? '<%=isLanEng ? "Not Used" : "অব্যবহৃত"%>' : houseCountData[i].type2 - houseCountData[i].type3)
                ]);
            }
        }



        const data = google.visualization.arrayToDataTable(dataArray);

        const options = {
            title: '<%=UtilCharacter.getDataByLanguage(language, "বাসা", "House")%>',
            legend: {position: 'bottom'}
        };
        const chart = new google.charts.Bar(document.getElementById('house_count'));
        chart.draw(data, google.charts.Bar.convertOptions(options));


    }

    function getHouseCountData(){
        let url = "AssetDashboardServlet?actionType=house_count";
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                if(fetchedData){
                    houseCountData = fetchedData;
                    google.charts.load('current', {'packages':['bar']});
                    google.charts.setOnLoadCallback(drawChartHouse);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

</script>