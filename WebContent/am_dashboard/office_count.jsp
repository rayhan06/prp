<%@ page import="util.UtilCharacter" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="kt-portlet__body" style="background-color: white">
    <div id="office_count" style="height: 300px;"></div>
</div>


<script type="text/javascript">

    $(document).ready(()=>{
        getOfficeCountData();
    });

    let officeCountData = [] ;

    function drawChartOffice() {
        let dataArray = [];

        for(let i = 0; i < officeCountData.length; i++){
            if(officeCountData[i] && officeCountData[i].type1) {
                dataArray.push([officeCountData[i].type1,
                    (i === 0 ? officeCountData[i].type2 : officeCountData[i].type2 / 1),
                    (i === 0 ? officeCountData[i].type3 : officeCountData[i].type3 / 1),
                    (i === 0 ? '<%=isLanEng ? "Not Used" : "অব্যবহৃত"%>': officeCountData[i].type2 - officeCountData[i].type3)
                ]);
            }
        }


        const data = google.visualization.arrayToDataTable(dataArray);
        const options = {
            title: '<%=UtilCharacter.getDataByLanguage(language, "অফিস", "Office")%>',
            legend: {position: 'bottom'}
        };
        const chart = new google.visualization.BarChart(document.getElementById('office_count'));
        chart.draw(data, options);

    }

    function getOfficeCountData(){
        let url = "AssetDashboardServlet?actionType=office_count";
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                if(fetchedData){
                    officeCountData = fetchedData;
                    google.load('visualization', 'current', {packages: ['corechart', 'bar']});
                    google.charts.setOnLoadCallback(drawChartOffice);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

</script>