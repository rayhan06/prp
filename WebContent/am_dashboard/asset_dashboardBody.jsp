﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.*" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="sessionmanager.SessionConstants" %>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<%


    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String language = LM.getText(LC.TRAINING_CALENDER_EDIT_LANGUAGE, loginDTO);
    boolean isLanEng = language.equalsIgnoreCase("English");


%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=UtilCharacter.getDataByLanguage(language, "ড্যাশবোর্ড", "Dashboard")%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body" style="background-color: #f2f2f2">
            <div class="row">
                <div class="col-12">
                    <div class="row" id = 'countDiv'>
                    </div>
                    <div class="row mt-4">
                        <div class="col-lg-6 mb-3">
                            <div class="shadow dashboard-card-border-radius overflow-hidden">
                                <div class="">
                                    <%@include file="house_count.jsp" %>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 mb-3">
                            <div class="shadow dashboard-card-border-radius overflow-hidden">
                                <div class="">
                                    <%@include file="office_count.jsp" %>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 mb-3">
                            <div class="shadow dashboard-card-border-radius overflow-hidden">
                                <div class="">
                                    <%@include file="house_request_status.jsp" %>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 mb-3">
                            <div class="shadow dashboard-card-border-radius overflow-hidden">
                                <div class="">
                                    <%@include file="office_request_status.jsp" %>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>


<script type="text/javascript">
    $(document).ready(()=>{

    });



</script>




