
<div id="log_book_discrepancy" style="width:100%; height:340px;"></div>

<script type="text/javascript">
    google.load('visualization', 'current', {packages: ['corechart', 'bar']});
    google.setOnLoadCallback(drawChart);

    function drawChart() {
        var data1 = google.visualization.arrayToDataTable([
            ['Metric Tonnes', 'HSFO', 'LSFO', 'MGO'],
            ['BULK CARRIER', 30, 40, 40],
            ['OIL TANKER', 60, 40, 45]
        ]);

        var options1 = {
            title: 'Log Book Discrepancy',
            // chartArea: {width: '50%'},
        };

        var chart1 = new google.visualization.BarChart(document.getElementById('log_book_discrepancy'));
        chart1.draw(data1, options1);
    }
</script>
