<%@ page import="util.UtilCharacter" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="kt-portlet__body" style="background-color: white">
    <div id="house_req_status" style="height: 300px;"></div>
</div>

<script type="text/javascript">
    $(document).ready(()=>{
        houseReqStatus();
    })
    let houseReqStatusData;



    function houseReqStatus(){
            let url = "AssetDashboardServlet?actionType=house_request";
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    houseReqStatusData = fetchedData;
                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawHouseReqStatus);
                },
                error: function (error) {
                    console.log(error);
                }
            });
    }

    function drawHouseReqStatus() {
        const data = new google.visualization.DataTable();
        data.addColumn('string', 'Status');
        data.addColumn('number', 'Count');
        if(houseReqStatusData){
            for(let i in houseReqStatusData){
                data.addRows([[String(i),houseReqStatusData[i]]]);
            }
        }

        const view = new google.visualization.DataView(data);

        const options = {
            title: '<%=UtilCharacter.getDataByLanguage(language, "অবস্থা অনুযায়ী বাসা বরাদ্দের আবেদন", "House Allocation Request Count By Status")%>',
            // is3D: true,
            // legend: { position: "none" }
        };
        const chart = new google.visualization.PieChart(document.getElementById('house_req_status'));
        chart.draw(view, options);
    }
</script>