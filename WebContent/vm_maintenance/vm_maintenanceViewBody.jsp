<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_maintenance.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentDTO" %>
<%@ page import="vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentRepository" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="vm_vehicle_parts.Vm_vehicle_partsRepository" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page import="java.util.stream.Collectors" %>


<%
    String servletName = "Vm_maintenanceServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
//out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.VM_MAINTENANCE_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
Vm_maintenanceDAO vm_maintenanceDAO = new Vm_maintenanceDAO("vm_maintenance");
Vm_maintenanceDTO vm_maintenanceDTO = Vm_maintenanceRepository.getInstance().
		getVm_maintenanceDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vm_maintenanceDTO.vehicleId);
String vehicleNumber = "";
if(vm_vehicleDTO != null){
    vehicleNumber = vm_vehicleDTO.regNo;
}

/*diver name start*/
    Vm_vehicle_driver_assignmentDTO assignmentDTO = Vm_vehicle_driver_assignmentRepository.getInstance()
            .getVm_vehicle_driver_assignmentDTOByID(vm_maintenanceDTO.vehicleDriverAssignmentId);
    String driverName = "";
    if (assignmentDTO != null) {
        driverName = UtilCharacter.getDataByLanguage(Language,
                assignmentDTO.employeeRecordNameBn, assignmentDTO.employeeRecordName);
    }
/*driver name end*/

/*last maintenanceDate date start*/
    String lastMaintenanceDate  = "";
    if (vm_vehicleDTO != null && vm_vehicleDTO.last_maintenance_date > 0) {
        lastMaintenanceDate = simpleDateFormat.format(new Date(Long.parseLong
                (vm_vehicleDTO.last_maintenance_date + "")));
    }
/*last maintenanceDate date end*/

/*current fiscal year*/
    String lastMaintenanceCost = "";
    Double lastMaintenanceCst = 0.0;
    int currentYearCount = 0;
    int prevYearCount = 0;
    double currentYearCost = 0.0;
    double prevYearCost = 0.0;

    if (vm_vehicleDTO != null) {
        if (vm_vehicleDTO.last_maintenance_date > 0) {
            lastMaintenanceDate = simpleDateFormat.format(new Date(Long.parseLong
                    (vm_vehicleDTO.last_maintenance_date + "")));
        }
        long maintenanceId = vm_vehicleDTO.last_maintenance_id;
        if (maintenanceId > 0) {
            Vm_maintenanceDTO mDTO = Vm_maintenanceRepository.getInstance().
                    getVm_maintenanceDTOByID(maintenanceId);
            if (mDTO != null) {
                lastMaintenanceCost = Utils.getDigits(mDTO.totalWithVat + "", Language);
                lastMaintenanceCst =  mDTO.totalWithVat;

                long fiscalYearId = mDTO.fiscalYearId;
                System.out.println(fiscalYearId);
                if (fiscalYearId > 0) {
                    List<Vm_maintenanceDTO> currentYearDTOs = vm_maintenanceDAO.
                            getAllVm_maintenanceByVehicleIdAndFiscalYear(vm_vehicleDTO.iD, fiscalYearId).
                            stream().filter(e -> e.status == 5 || e.status == 6).collect(Collectors.toList());
                    currentYearCount = currentYearDTOs.size();
                    for (Vm_maintenanceDTO item : currentYearDTOs) {
                        currentYearCost += item.totalWithVat;
                    }

                    Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();
                    long prevFiscalYearId = fiscal_yearDAO.getPrevFiscalYearID(fiscalYearId);
                    if (prevFiscalYearId != fiscalYearId) {
                        List<Vm_maintenanceDTO> prevYearDTOs = vm_maintenanceDAO.
                                getAllVm_maintenanceByVehicleIdAndFiscalYear(vm_vehicleDTO.iD, prevFiscalYearId).
                                stream().filter(e -> e.status == 5 || e.status == 6).collect(Collectors.toList());
                        prevYearCount = prevYearDTOs.size();
                        for (Vm_maintenanceDTO item : prevYearDTOs) {
                            prevYearCost += item.totalWithVat;
                        }
                    }

                }
            }
        }
    }

/*current fiscal year*/

%>

<style>

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .4in;
        background: white;
        margin-bottom: 10px;
    }

</style>


<div class="kt-content p-0" id="kt_content">
    <div class="">
        <div class="kt-portlet">
            <div class="kt-portlet__body page-bg p-0" id="bill-div">
                <div class="ml-auto my-5">
                    <button type="button" class="btn" id='download-pdf'
                            onclick="downloadTemplateAsPdf('to-print-div', 'Vehicle maintenance');">
                        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                </div>

                <div class="mx-1">
                    <div class="container" id="to-print-div">
                        <section class="page shadow" data-size="A4">
                            <div class="row">
                                <div class="col-12 text-center">

                                    <h5 class="mt-2 font-weight-bold">
                                        <%=UtilCharacter.getDataByLanguage(Language, "বাংলাদেশ জাতীয় সংসদ সচিবালয়", "Bangladesh National Parliament Secretariat")%>
                                    </h5>
                                    <h5 class="text-dark">
                                        <%=UtilCharacter.getDataByLanguage(Language, "পরিবহন শাখা", "Transportation Branch")%>
                                    </h5>
                                    <h5 class="text-dark">
                                        <%=UtilCharacter.getDataByLanguage(Language, "www.Parliament.gov.bd", "www.Parliament.gov.bd")%>
                                    </h5>

                                </div>
                            </div>

                            <div class="row mt-2 mx-3 my-2">
                                <div class="col-10 " style="display: flex;justify-content: space-between;">

                                    <span>
                                        <%=UtilCharacter.getDataByLanguage(Language, "বিষয়ঃ- ", "Subject:- ")%>&nbsp;<u style="text-underline-position: under;"><%=UtilCharacter.getDataByLanguage(Language, "গাড়ি মেরামত প্রসঙ্গে", "In the context of car repair")%></u>
                                    </span>
                                    <span>
                                        <%=UtilCharacter.getDataByLanguage(Language, "আবেদন তারিখঃ", "Date of application:")%>&nbsp;<%=Utils.getDigits(simpleDateFormat.format(new Date(Long.parseLong(String.valueOf(vm_maintenanceDTO.applicationDate)))), Language)%>
                                    </span>
                                </div>
                            </div>

                            <div class="row mt-10 mx-2">

                                <table
                                        class="mt-3 w-100 rounded"
                                        border="1px solid #323233;"
                                        style="border-collapse: collapse"
                                >
                                    <tbody>

                                    <tr>
                                        <td colspan="1" style="padding: 5px 5px"><%=UtilCharacter.getDataByLanguage(Language, "গাড়ির নম্বর", "Vehicle number")%>
                                        </td>
                                        <td colspan="1" style="text-align: center;">&nbsp;<%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%>&nbsp;
                                        </td>
                                        <td colspan="10" style="padding: 5px 10px">&nbsp;<%=Utils.getDigits(vehicleNumber,Language)%>&nbsp;
                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="1" style="padding: 5px 5px"><%=UtilCharacter.getDataByLanguage(Language, "গাড়িচালকের নাম", "Driver's name")%>
                                        </td>
                                        <td colspan="1" style="text-align: center;">&nbsp;<%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%>&nbsp;
                                        </td>
                                        <td colspan="10" style="padding: 5px 10px">&nbsp;<%=Utils.getDigits(driverName, Language)%>&nbsp;
                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="1" style="padding: 5px 5px"><%=UtilCharacter.getDataByLanguage(Language, "সর্বশেষ অন্য মেরামতের তারিখ", "Last other repair date")%>
                                        </td>
                                        <td colspan="1" style="text-align: center;">&nbsp;<%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%>&nbsp;
                                        </td>
                                        <td colspan="10" style="padding: 5px 10px">&nbsp;<%=Utils.getDigits(lastMaintenanceDate,Language)%>&nbsp;
                                        </td>

                                    </tr>

                                    </tbody>
                                </table>

                            </div>

                            <div class="row mx-3 mt-2">
                                <div class="col-10 " style="display: flex;justify-content: space-between;">
                                    <span><u style="text-underline-position: under;"><%=UtilCharacter.getDataByLanguage(Language, "মেরামতের বিবরণঃ", "Details of repairs:")%></u></span>

                                </div>
                            </div>
                            <div class="row mx-2 mt-1">
                                <table
                                        class="mt-3 w-100 rounded"
                                        border="1px solid #323233;"
                                        style="border-collapse: collapse"
                                >
                                    <thead>
                                    <tr>
                                        <th class="text-center" style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "ক্রমিক নং", "Serial No")%>
                                        </th>
                                        <th class="text-center" style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "মেরামতে/প্রতিস্থাপন যন্ত্রাংশের নাম", "Name of repair / replacement parts")%>
                                        </th>
                                        <th class="text-center"  style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "মেরামত/প্রতিস্থাপন", "Repair / Replacement")%>
                                        </th>
                                        <th class="text-center"  style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "যন্ত্রাংশ পূর্বে কত তারিখে মেরামত/প্রতিস্থাপন করা হয়েছে", "The date on which the parts were previously repaired / replaced")%>
                                        </th>
                                        <th class="text-center"  style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "নির্ধারিত মূল্য", "Fixed value")%>
                                        </th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    <%
                                        List<VmMaintenanceItemDTO> vmMaintenanceItemDTOs = VmMaintenanceItemRepository.getInstance().
                                                getVmMaintenanceItemDTOByMaintenanceId(vm_maintenanceDTO.iD);
                                        int serialNo = 0;
                                        double totalFixedValue = vmMaintenanceItemDTOs.stream().map(dto->dto.driverPrice)
                                                .collect(Collectors.summingDouble(Double::doubleValue));
                                        for (VmMaintenanceItemDTO vmMaintenanceItemDTO : vmMaintenanceItemDTOs) {
                                           String itemLastMaintenanceDate = simpleDateFormat.format(new Date(Long.parseLong
                                                    (vmMaintenanceItemDTO.lastModificationTime + "")));
                                    %>
                                    <tr>
                                        <td style="padding: 5px 10px"><%=UtilCharacter.convertDataByLanguage(Language, String.valueOf(++serialNo))%>
                                        </td>
                                        <td style="padding: 5px 10px">
                                            <%
                                                value = Vm_vehicle_partsRepository.getInstance().getText(Language,vmMaintenanceItemDTO.vmVehiclePartsType);
                                            %>
                                            <%
                                            %>

                                            <%=Utils.getDigits(value, Language)%>
                                            
                                        </td>
                                        <td style="padding: 5px 10px">

                                            <%
                                                value = vmMaintenanceItemDTO.vehicleMaintenanceCat + "";
                                            %>
                                            <%
                                                value = CatRepository.getInstance().getText(Language, "vehicle_maintenance", vmMaintenanceItemDTO.vehicleMaintenanceCat);
                                            %>

                                            <%=Utils.getDigits(value, Language)%>

                                        </td>
                                        <td style="padding: 5px 10px">

                                            <%=Utils.getDigits(itemLastMaintenanceDate,Language)%>

                                        </td>
                                        <td style="padding: 5px 10px">


                                            <%
                                                value = vmMaintenanceItemDTO.driverPrice + "";
                                            %>
                                            <%
                                                value = String.format("%.2f", vmMaintenanceItemDTO.driverPrice);
                                            %>

                                            <%=Utils.getDigits(value, Language)%>

                                        </td>

                                    </tr>
                                    <%
                                                }
                                    %>

                                    <%if(serialNo<14){
                                        while(serialNo<14){
                                            %>

                                    <tr>
                                        <td style="padding: 5px 10px"><%=UtilCharacter.convertDataByLanguage(Language, String.valueOf(++serialNo))%>
                                        </td>
                                        <td style="padding: 5px 10px">



                                        </td>
                                        <td style="padding: 5px 10px">



                                        </td>
                                        <td style="padding: 5px 10px">



                                        </td>
                                        <td style="padding: 5px 10px">




                                        </td>

                                    </tr>

                                        <%
                                        }}%>

                                    <%--                                TOTAL CALCULATION ROW--%>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <%String total = Language.equalsIgnoreCase("English") ? "Total" : "মোট";%>
                                        <td><%=total%></td>
                                        <td><%=Utils.getDigits(totalFixedValue, Language)%></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row mt-5 mx-5 mb-4">
                                <div class="col-10 " style="display: flex;justify-content: space-between;">

                                    <span>
                                        <u style="text-underline-position: under;"><%=UtilCharacter.getDataByLanguage(Language, "প্রতিস্বাক্ষরকারীর কর্মকর্তার নাম", "Name of counter signatory officer")%></u>
                                    </span>
                                    <span>
                                        <u style="text-underline-position: under;"><%=UtilCharacter.getDataByLanguage(Language, "গাড়ি চালকের স্বাক্ষর", "Driver's signature")%></u>
                                    </span>
                                </div>
                            </div>
                            <hr style="height:1px;border-width:0;color:black;background-color:black">
                            <div class="row  mt-2" style="padding-left: 3px;">
                                <div class="col-10 " style="display: flex;justify-content: space-between;">
                                    <span><u style="text-underline-position: under;"><%=UtilCharacter.getDataByLanguage(Language, "পরিবহন শাখা কর্তৃক পূরণযোগ্য", "Filling by transport branch")%></u></span>

                                </div>
                            </div>
                            <div class="row mx-1 mt-2">
                                <table
                                        class="mt-3 w-100 rounded"
                                        border="1px solid #323233;"
                                        style="border-collapse: collapse"
                                >
                                    <thead>
                                    <tr>

                                        <th class="text-center" style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "সর্বশেষ অন্য মেরামতের তারিখ", "Last other repair date")%>
                                        </th>
                                        <th class="text-center"  style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "সর্বশেষ মেরামতের ব্যয়", "The cost of the latest repairs")%>
                                        </th>
                                        <th class="text-center"  style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "সর্বশেষ মেরামতের ব্যয় রেজিষ্টারে উঠেছে কি?", "What is the latest repair cost register?")%>
                                        </th>
                                        <th class="text-center"  style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "গত অর্থবছরে কতবার মেরামত করা হয়েছে?", "How many repairs have been done in the last financial year?")%>
                                        </th>
                                        <th class="text-center"  style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "গত অর্থবছরে কত টাকা মেরামত করা হয়েছে?", "How much money has been repaired in the last financial year?")%>
                                        </th>
                                        <th class="text-center"  style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "এ অর্থবছরে কতবার মেরামত করা হয়েছে?", "How many repairs have been done in this financial year?")%>
                                        </th>
                                        <th class="text-center"  style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "এ অর্থবছরে কত ব্যয় হয়েছে?", "How much has been spent in this financial year?")%>
                                        </th>

                                    </tr>
                                    </thead>
                                    <tbody>


                                    <tr>

                                        <td style="padding: 5px 10px">

                                            <div style="height: 50px; overflow:hidden;">
                                                <%=Utils.getDigits(lastMaintenanceDate,Language)%>
                                            </div>
                                        </td>
                                        <td style="padding: 5px 10px">

                                            <div style="height: 50px; overflow:hidden;">
                                                <%=Utils.getDigits(lastMaintenanceCst,Language)%>
                                            </div>
                                        </td>
                                        <td style="padding: 5px 10px">

                                            <div style="height: 50px; overflow:hidden;">
                                                <%
                                                    String bngText = "না";
                                                    String engText = "No";
                                                    if(lastMaintenanceCost.length()>0){
                                                        bngText = "হ্যাঁ";
                                                        engText = "Yes";
                                                    }
                                                %>
                                                <%=UtilCharacter.getDataByLanguage(Language, bngText, engText)%>
                                            </div>
                                        </td>
                                        <td style="padding: 5px 10px">
                                            <div style="height: 50px; overflow:hidden;">
                                                <%=Utils.getDigits(prevYearCount,Language)%>
                                            </div>
                                        </td>
                                        <td style="padding: 5px 10px">
                                            <div style="height: 50px; overflow:hidden;">
                                                <%=Utils.getDigits(prevYearCost,Language)%>
                                            </div>
                                        </td>
                                        <td style="padding: 5px 10px">
                                            <div style="height: 50px; overflow:hidden;">
                                                <%=Utils.getDigits(currentYearCount,Language)%>
                                            </div>
                                        </td>
                                        <td style="padding: 5px 10px">
                                            <div style="height: 50px; overflow:hidden;">
                                                <%=Utils.getDigits(currentYearCost,Language)%>
                                            </div>
                                        </td>

                                    </tr>


                                    </tbody>
                                </table>
                            </div>

                            <div class="row mt-4">
                                <div class="col-12" style="display: flex;justify-content: flex-end;">

                                    <span>
                                        <u style="text-underline-position: under;"><%=UtilCharacter.getDataByLanguage(Language, "পরিবহন শাখা প্রধানের স্বাক্ষর", "Signature of the head of transport branch")%></u>
                                    </span>
                                </div>
                            </div>
                            
                        </section>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function downloadTemplateAsPdf(divId, fileName) {
        let content = document.getElementById(divId);
        const opt = {
            margin: 0.5,
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4'}
        };
        html2pdf().from(content).set(opt).save();
    }
</script>