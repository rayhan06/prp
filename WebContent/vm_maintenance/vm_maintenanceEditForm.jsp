<%@ page import="util.UtilCharacter" %>
<%@ page import="vm_vehicle_parts.Vm_vehicle_partsRepository" %>
<%@page pageEncoding="UTF-8" %>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Vm_maintenanceServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="formSubmit()">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=vm_maintenanceDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLETYPECAT, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='vehicleTypeCat'
                                                    id='vehicleTypeCat_category_<%=i%>' tag='pb_html'
                                                    onchange="loadVehicleList()">
                                                <%
                                                    Options = CatRepository.getInstance().buildOptions("vehicle_type", Language, vm_maintenanceDTO.vehicleTypeCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row" id="vehicleIdDiv">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLEID, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='vehicleId'
                                                    id='givenVehicleId_select_<%=i%>' tag='pb_html'
                                                    onchange="vehicleSelect()">
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row" id="driverDiv">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLEDRIVERASSIGNMENTID, loginDTO)%>
                                        </label>

                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='' id='driverText'
                                                   readonly="readonly" tag='pb_html'/>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='vehicleDriverAssignmentId'
                                           id='vehicleDriverAssignmentId'
                                           value='<%=vm_maintenanceDTO.vehicleDriverAssignmentId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=vm_maintenanceDTO.searchColumn%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=vm_maintenanceDTO.insertedByUserId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=vm_maintenanceDTO.insertedByOrganogramId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=vm_maintenanceDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>' value='<%=vm_maintenanceDTO.isDeleted%>'
                                           tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=vm_maintenanceDTO.lastModificationTime%>' tag='pb_html'/>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-body mt-5">
                        <h5 class="table-title">
                            <%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ITEM, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped text-nowrap">
                                <thead>
                                <tr>
                                    <th class="text-nowrap"><%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ITEM_VMVEHICLEPARTSTYPE, loginDTO)%>
                                    </th>
                                    <th class="text-nowrap"><%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ITEM_VEHICLEMAINTENANCECAT, loginDTO)%>
                                    </th>
                                    <th class="text-nowrap"><%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ITEM_AMOUNT, loginDTO)%>
                                    </th>
                                    <th class="text-nowrap"><%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ITEM_REMARKS, loginDTO)%>
                                    </th>
                                    <th class="text-nowrap"><%=UtilCharacter.getDataByLanguage(Language, "নির্ধারিত মূল্য", "Fixed value")%>
                                    </th>
                                    <th class="text-nowrap"><%=UtilCharacter.getDataByLanguage(Language, "ডিলিট করুন", "Remove")%>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="field-VmMaintenanceItem">


                                <%
                                    if (actionName.equals("edit")) {
                                        int index = -1;


                                        for (VmMaintenanceItemDTO vmMaintenanceItemDTO : vm_maintenanceDTO.vmMaintenanceItemDTOList) {
                                            index++;

                                            System.out.println("index index = " + index);

                                %>

                                <tr id="VmMaintenanceItem_<%=index + 1%>">
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control' name='vmMaintenanceItem.iD'
                                               id='iD_hidden_<%=childTableStartingID%>'
                                               value='<%=vmMaintenanceItemDTO.iD%>' tag='pb_html'/>

                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='vmMaintenanceItem.vmMaintenanceId'
                                               id='vmMaintenanceId_hidden_<%=childTableStartingID%>'
                                               value='<%=vmMaintenanceItemDTO.vmMaintenanceId%>' tag='pb_html'/>
                                    </td>
                                    <td>


                                        <select class='form-control' name='vmMaintenanceItem.vmVehiclePartsType'
                                                id='vmVehiclePartsType_select_<%=childTableStartingID%>'
                                                tag='pb_html'>
                                            <%
                                                Options = Vm_vehicle_partsRepository.getInstance()
                                                        .getBuildOptions(Language, vmMaintenanceItemDTO.vmVehiclePartsType + "");
//                                                Options = CommonDAO.getOptions(Language, "vm_vehicle_parts", vmMaintenanceItemDTO.vmVehiclePartsType);
                                            %>
                                            <%=Options%>
                                        </select>

                                    </td>
                                    <td>


                                        <select class='form-control' name='vmMaintenanceItem.vehicleMaintenanceCat'
                                                id='vehicleMaintenanceCat_category_<%=childTableStartingID%>'
                                                tag='pb_html'>
                                            <%
                                                Options = CatRepository.getInstance().buildOptions("vehicle_maintenance", Language, vmMaintenanceItemDTO.vehicleMaintenanceCat);
                                            %>
                                            <%=Options%>
                                        </select>

                                    </td>
                                    <td>


                                        <%
                                            value = "";
                                            if (vmMaintenanceItemDTO.amount != -1) {
                                                value = vmMaintenanceItemDTO.amount + "";
                                            }
                                        %>
                                        <input type='number' class='form-control' name='vmMaintenanceItem.amount'
                                               id='amount_number_<%=childTableStartingID%>' value='<%=value%>'
                                               tag='pb_html'>
                                    </td>
                                    <td>


                                        <input type='text' class='form-control' name='vmMaintenanceItem.remarks'
                                               id='remarks_text_<%=childTableStartingID%>'
                                               value='<%=vmMaintenanceItemDTO.remarks%>' tag='pb_html'/>
                                    </td>
                                    <td>


                                        <%
                                            value = "";
                                            if (vmMaintenanceItemDTO.driverPrice != -1) {
                                                value = vmMaintenanceItemDTO.driverPrice + "";
                                            }
                                        %>
                                        <input type='number' class='form-control'
                                               name='vmMaintenanceItem.driverPrice'
                                               id='driverPrice_number_<%=childTableStartingID%>' value='<%=value%>'
                                               tag='pb_html' onkeyup="totalSum()" onchange="totalSum()">
                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='vmMaintenanceItem.searchColumn'
                                               id='searchColumn_hidden_<%=childTableStartingID%>'
                                               value='<%=vmMaintenanceItemDTO.searchColumn%>' tag='pb_html'/>
                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='vmMaintenanceItem.insertedByUserId'
                                               id='insertedByUserId_hidden_<%=childTableStartingID%>'
                                               value='<%=vmMaintenanceItemDTO.insertedByUserId%>' tag='pb_html'/>
                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='vmMaintenanceItem.insertedByOrganogramId'
                                               id='insertedByOrganogramId_hidden_<%=childTableStartingID%>'
                                               value='<%=vmMaintenanceItemDTO.insertedByOrganogramId%>'
                                               tag='pb_html'/>
                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='vmMaintenanceItem.insertionDate'
                                               id='insertionDate_hidden_<%=childTableStartingID%>'
                                               value='<%=vmMaintenanceItemDTO.insertionDate%>' tag='pb_html'/>
                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control' name='vmMaintenanceItem.isDeleted'
                                               id='isDeleted_hidden_<%=childTableStartingID%>'
                                               value='<%=vmMaintenanceItemDTO.isDeleted%>' tag='pb_html'/>

                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='vmMaintenanceItem.lastModificationTime'
                                               id='lastModificationTime_hidden_<%=childTableStartingID%>'
                                               value='<%=vmMaintenanceItemDTO.lastModificationTime%>'
                                               tag='pb_html'/>
                                    </td>
                                    <td>
										<span id='chkEdit'>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
                                                   class="form-control-sm"/>
										</span>
                                    </td>
                                </tr>
                                <%
                                            childTableStartingID++;
                                        }
                                    }
                                %>
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex justify-content-start">
                                    <h5 class="table-title">
                                        <%=UtilCharacter.getDataByLanguage(Language, "মোট মূল্য", "Total Cost")%>:
                                    </h5>
                                    <h5 id='total-value' class="table-title ml-2"></h5>
                                </div>
                                <div class="mt-3">
                                    <button
                                            id="add-more-VmMaintenanceItem"
                                            name="add-moreVmMaintenanceItem"
                                            type="button"
                                            class="btn btn-sm text-white add-btn shadow">
                                        <i class="fa fa-plus"></i>
                                        <%=LM.getText(LC.HM_ADD, loginDTO)%>
                                    </button>
                                    <button
                                            id="remove-VmMaintenanceItem"
                                            name="removeVmMaintenanceItem"
                                            type="button"
                                            class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <%VmMaintenanceItemDTO vmMaintenanceItemDTO = new VmMaintenanceItemDTO();%>
                        <template id="template-VmMaintenanceItem">
                            <tr>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='vmMaintenanceItem.iD'
                                           id='iD_hidden_' value='<%=vmMaintenanceItemDTO.iD%>' tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='vmMaintenanceItem.vmMaintenanceId'
                                           id='vmMaintenanceId_hidden_'
                                           value='<%=vmMaintenanceItemDTO.vmMaintenanceId%>' tag='pb_html'/>
                                </td>
                                <td>


                                    <select class='form-control' name='vmMaintenanceItem.vmVehiclePartsType'
                                            id='vmVehiclePartsType_select_' tag='pb_html'>
                                        <%
                                            Options = Vm_vehicle_partsRepository.getInstance().getBuildOptions
                                                    (Language, vmMaintenanceItemDTO.vmVehiclePartsType + "");
//                                            Options = CommonDAO.getOptions(Language, "vm_vehicle_parts", vmMaintenanceItemDTO.vmVehiclePartsType);
                                        %>
                                        <%=Options%>
                                    </select>

                                </td>
                                <td>


                                    <select class='form-control' name='vmMaintenanceItem.vehicleMaintenanceCat'
                                            id='vehicleMaintenanceCat_category_' tag='pb_html'>
                                        <%
                                            Options = CatRepository.getInstance().buildOptions("vehicle_maintenance", Language, vmMaintenanceItemDTO.vehicleMaintenanceCat);
                                        %>
                                        <%=Options%>
                                    </select>

                                </td>
                                <td>


                                    <%
                                        value = "";
                                        if (vmMaintenanceItemDTO.amount != -1) {
                                            value = vmMaintenanceItemDTO.amount + "";
                                        }
                                    %>
                                    <input type='number' class='form-control' name='vmMaintenanceItem.amount'
                                           id='amount_number_' value='<%=value%>' tag='pb_html'>
                                </td>
                                <td>


                                    <input type='text' class='form-control' name='vmMaintenanceItem.remarks'
                                           id='remarks_text_' value='<%=vmMaintenanceItemDTO.remarks%>'
                                           tag='pb_html'/>
                                </td>
                                <td>


                                    <%
                                        value = "";
                                        if (vmMaintenanceItemDTO.driverPrice != -1) {
                                            value = vmMaintenanceItemDTO.driverPrice + "";
                                        }
                                    %>
                                    <input type='number' class='form-control' name='vmMaintenanceItem.driverPrice'
                                           id='driverPrice_number_' value='<%=value%>' onkeyup="totalSum()"
                                           onchange="totalSum()"
                                           tag='pb_html'>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='vmMaintenanceItem.searchColumn'
                                           id='searchColumn_hidden_' value='<%=vmMaintenanceItemDTO.searchColumn%>'
                                           tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='vmMaintenanceItem.insertedByUserId'
                                           id='insertedByUserId_hidden_'
                                           value='<%=vmMaintenanceItemDTO.insertedByUserId%>' tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='vmMaintenanceItem.insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_'
                                           value='<%=vmMaintenanceItemDTO.insertedByOrganogramId%>' tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='vmMaintenanceItem.insertionDate'
                                           id='insertionDate_hidden_'
                                           value='<%=vmMaintenanceItemDTO.insertionDate%>'
                                           tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='vmMaintenanceItem.isDeleted'
                                           id='isDeleted_hidden_' value='<%=vmMaintenanceItemDTO.isDeleted%>'
                                           tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='vmMaintenanceItem.lastModificationTime'
                                           id='lastModificationTime_hidden_'
                                           value='<%=vmMaintenanceItemDTO.lastModificationTime%>' tag='pb_html'/>
                                </td>
                                <td>
											<span id=''>
												<input type='checkbox' name='checkbox' value='' deletecb='true'
                                                       class="form-control-sm"/>
											</span>
                                </td>
                            </tr>

                        </template>
                </div>
                <div class="form-actions text-center mt-5">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button id = "submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<style>
    .required {
        color: red;
    }
</style>
