<%@page pageEncoding="UTF-8" %>

<%@page import="vm_maintenance.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="files.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentDTO" %>
<%@ page import="vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentRepository" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VM_MAINTENANCE_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_VM_MAINTENANCE;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Vm_maintenanceDTO vm_maintenanceDTO = (Vm_maintenanceDTO) request.getAttribute("vm_maintenanceDTO");
    CommonDTO commonDTO = vm_maintenanceDTO;
    String servletName = "Vm_maintenanceServlet";


    System.out.println("vm_maintenanceDTO = " + vm_maintenanceDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Vm_maintenanceDAO vm_maintenanceDAO = (Vm_maintenanceDAO) request.getAttribute("vm_maintenanceDAO");

    FilesDAO filesDAO = new FilesDAO();

    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<%--											<td id = '<%=i%>_vehicleTypeCat'>--%>
<%--											<%--%>
<%--											value = vm_maintenanceDTO.vehicleTypeCat + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											value = CatRepository.getInstance().getText(Language, "vehicle_type", vm_maintenanceDTO.vehicleTypeCat);--%>
<%--											%>	--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>


<td id='<%=i%>_vehicleId'>
    <%
        Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vm_maintenanceDTO.vehicleId);
        value = "";
        if (vm_vehicleDTO != null) {
            value = vm_vehicleDTO.regNo;
        }
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_vehicleDriverAssignmentId'>
    <%
        Vm_vehicle_driver_assignmentDTO assignmentDTO = Vm_vehicle_driver_assignmentRepository.getInstance()
                .getVm_vehicle_driver_assignmentDTOByID(vm_maintenanceDTO.vehicleDriverAssignmentId);
        value = "";
        if (assignmentDTO != null) {
            value = UtilCharacter.getDataByLanguage(Language,
                    assignmentDTO.employeeRecordNameBn, assignmentDTO.employeeRecordName);
        }
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_lastMaintenanceDate'>
    <%
        value = "";
        if (vm_vehicleDTO != null && vm_vehicleDTO.last_maintenance_date > 0) {
            value = simpleDateFormat.format(new Date(Long.parseLong
                    (vm_vehicleDTO.last_maintenance_date + "")));
        }

//											value = vm_maintenanceDTO.lastMaintenanceDate + "";
    %>
    <%
    %>
    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_applicationDate'>
    <%
        value = vm_maintenanceDTO.applicationDate + "";
    %>
    <%
        String formatted_applicationDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_applicationDate, Language)%>


</td>

<td id='<%=i%>_vehicleOfficeName'>
    <%
        value = UtilCharacter.getDataByLanguage(Language,
                vm_maintenanceDTO.vehicleOfficeNameBn, vm_maintenanceDTO.vehicleOfficeName);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_status'>
    <%
        value = Vm_maintenanceDAO.getStatus(vm_maintenanceDTO.status, Language);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_aoApproveDate'>
    <%
        value = "";
    %>
    <%
        if (vm_maintenanceDTO.aoApproveDate > 0) {
            value = simpleDateFormat.format(new Date(Long.parseLong(vm_maintenanceDTO.aoApproveDate + "")));

        }
    %>
    <%=Utils.getDigits(value, Language)%>


</td>


<%--											<td id = '<%=i%>_requesterPostId'>--%>
<%--											<%--%>
<%--											value = vm_maintenanceDTO.requesterPostId + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>

<%--											<td id = '<%=i%>_requesterUnitId'>--%>
<%--											<%--%>
<%--											value = vm_maintenanceDTO.requesterUnitId + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>

<%--											<td id = '<%=i%>_requesterEmployeeRecordId'>--%>
<%--											<%--%>
<%--											value = vm_maintenanceDTO.requesterEmployeeRecordId + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>


<%--											<td id = '<%=i%>_vehicleOfficeNameBn'>--%>
<%--											<%--%>
<%--											value = vm_maintenanceDTO.vehicleOfficeNameBn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>

<%--											<td id = '<%=i%>_isChecked'>--%>
<%--											<%--%>
<%--											value = vm_maintenanceDTO.isChecked + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getYesNo(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>

<%--											<td id = '<%=i%>_mechanicRemarks'>--%>
<%--											<%--%>
<%--											value = vm_maintenanceDTO.mechanicRemarks + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>

<%--											<td id = '<%=i%>_mechanicApproveDate'>--%>
<%--											<%--%>
<%--											value = vm_maintenanceDTO.mechanicApproveDate + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											String formatted_mechanicApproveDate = simpleDateFormat.format(new Date(Long.parseLong(value))).toString();--%>
<%--											%>--%>
<%--											<%=Utils.getDigits(formatted_mechanicApproveDate, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>


<%--											<td id = '<%=i%>_vatPercentage'>--%>
<%--											<%--%>
<%--											value = vm_maintenanceDTO.vatPercentage + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											value = String.format("%.1f", vm_maintenanceDTO.vatPercentage);--%>
<%--											%>												--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_totalWithoutVat'>--%>
<%--											<%--%>
<%--											value = vm_maintenanceDTO.totalWithoutVat + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											value = String.format("%.1f", vm_maintenanceDTO.totalWithoutVat);--%>
<%--											%>												--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_totalWithVat'>--%>
<%--											<%--%>
<%--											value = vm_maintenanceDTO.totalWithVat + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											value = String.format("%.1f", vm_maintenanceDTO.totalWithVat);--%>
<%--											%>												--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_totalVat'>--%>
<%--											<%--%>
<%--											value = vm_maintenanceDTO.totalVat + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											value = String.format("%.1f", vm_maintenanceDTO.totalVat);--%>
<%--											%>												--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_paymentReceivedDate'>--%>
<%--											<%--%>
<%--											value = vm_maintenanceDTO.paymentReceivedDate + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											String formatted_paymentReceivedDate = simpleDateFormat.format(new Date(Long.parseLong(value))).toString();--%>
<%--											%>--%>
<%--											<%=Utils.getDigits(formatted_paymentReceivedDate, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_fiscalYearId'>--%>
<%--											<%--%>
<%--											value = vm_maintenanceDTO.fiscalYearId + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_sarokNumber'>--%>
<%--											<%--%>
<%--											value = vm_maintenanceDTO.sarokNumber + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--		--%>
<%--											<td id = '<%=i%>_paymentRemarks'>--%>
<%--											<%--%>
<%--											value = vm_maintenanceDTO.paymentRemarks + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>


<td>
	<button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
			onclick="location.href='Vm_maintenanceServlet?actionType=view&ID=<%=vm_maintenanceDTO.iD%>'">
		<i class="fa fa-eye"></i>
	</button>
</td>

<td id='<%=i%>_Edit'>

    <%
        if (vm_maintenanceDTO.status == 1) {
    %>

	<button type="button" class="btn-sm border-0 shadow btn-border-radius text-white" style="background-color: #ff6b6b;"
			onclick="location.href='Vm_maintenanceServlet?actionType=getEditPage&ID=<%=vm_maintenanceDTO.iD%>'">
		<i class="fa fa-edit"></i>
	</button>
    <% } %>

</td>


<%--											<td id='<%=i%>_checkbox'>--%>
<%--												<div class='checker'>--%>
<%--													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=vm_maintenanceDTO.iD%>'/></span>--%>
<%--												</div>--%>
<%--											</td>--%>
																						
											

