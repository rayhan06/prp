<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_maintenance.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.Office_unitsDAO" %>
<%@ page import="office_units.Office_unitsDTO" %>

<%
    Vm_maintenanceDTO vm_maintenanceDTO;
    vm_maintenanceDTO = (Vm_maintenanceDTO) request.getAttribute("vm_maintenanceDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (vm_maintenanceDTO == null) {
        vm_maintenanceDTO = new Vm_maintenanceDTO();

    }
    System.out.println("vm_maintenanceDTO = " + vm_maintenanceDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ADD_FORMNAME, loginDTO);
    String servletName = "Vm_maintenanceServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID = -1;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_MAINTENANCE_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;

//	UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//	OfficeUnitOrganograms org =
//			OfficeUnitOrganogramsRepository.getInstance().getById(userDTO.organogramID);
//	Office_unitsDTO unit = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(org.office_unit_id);
//	System.out.println("CHECK");
//	System.out.println(userDTO);
//	System.out.println(org);
//	System.out.println(unit);
//	System.out.println("CHECK");
%>

<%@include file="vm_maintenanceEditForm.jsp" %>

<script type="text/javascript">


    function valueCheck(value) {
        if (value && value.toString().length > 0 && value / 1 > -1) {
            return false;
        } else {
            return true;
        }
    }

    function childElementValidation() {
        let flag = true;
        for (let i = 1; i < child_table_extra_id; i++) {

            let partEl = document.getElementById("vmVehiclePartsType_select_" + i);
            if (partEl) {
                let Value = partEl.value;
                if (valueCheck(Value)) {
                    flag = false;
                    toastr.error("Select Parts")
                    break;
                }
            }


            let typeEl = document.getElementById("vehicleMaintenanceCat_category_" + i);
            if (typeEl) {
                let Value = typeEl.value;
                if (valueCheck(Value)) {
                    flag = false;
                    toastr.error("Select Type")
                    break;
                }
            }


            let quantityEl = document.getElementById("amount_number_" + i);
            if (quantityEl) {
                let Value = quantityEl.value;
                if (valueCheck(Value)) {
                    flag = false;
                    toastr.error("Quantity Needed")
                    break;
                }
            }

            let dAmount = document.getElementById("driverPrice_number_" + i);
            if (dAmount) {
                let Value = dAmount.value;
                if (valueCheck(Value)) {
                    flag = false;
                    toastr.error("Driver Amount Needed")
                    break;
                }
            }
        }

        return flag;
    }

    function PreprocessBeforeSubmiting() {

        let driverId = $("#vehicleDriverAssignmentId").val();
        if (!driverId || driverId == -1) {
            toastr.error("No driver assigned to this vehicle")
            return false;
        }


        // preprocessDateBeforeSubmitting('applicationDate', row);
        // preprocessDateBeforeSubmitting('lastMaintenanceDate', row);
        // preprocessCheckBoxBeforeSubmitting('isChecked', row);
        // preprocessDateBeforeSubmitting('mechanicApproveDate', row);
        // preprocessDateBeforeSubmitting('aoApproveDate', row);
        // preprocessDateBeforeSubmitting('paymentReceivedDate', row);

        for (i = 1; i < child_table_extra_id; i++) {
        }


        return childElementValidation();
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Vm_maintenanceServlet");
    }

    function init(row) {
        if (actionName == 'edit') {
            loadVehicleList();

        }
        totalSum();

        // setDateByStringAndId('applicationDate_js_' + row, $('#applicationDate_date_' + row).val());
        // setDateByStringAndId('lastMaintenanceDate_js_' + row, $('#lastMaintenanceDate_date_' + row).val());
        // setDateByStringAndId('mechanicApproveDate_js_' + row, $('#mechanicApproveDate_date_' + row).val());
        // setDateByStringAndId('aoApproveDate_js_' + row, $('#aoApproveDate_date_' + row).val());
        // setDateByStringAndId('paymentReceivedDate_js_' + row, $('#paymentReceivedDate_date_' + row).val());

        for (i = 1; i < child_table_extra_id; i++) {
        }

    }

    let row = 0;
    let actionName = '<%=actionName%>';
    let language = '<%=Language%>';
    let vehicleId = '<%=vm_maintenanceDTO.vehicleId%>';
    $(document).ready(function () {
        $.validator.addMethod('validSelector', function (value, element) {
            return value && value != -1;
        });

        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        let errVehicleTypeCat;
        let errVehicle;
        let errBn;
        let errEn;
        let errStatus;
        let errRemark;

        if (language === 'English') {
            errVehicleTypeCat = 'Please select vehicle type';
            errVehicle = 'Please select vehicle';
            errRemark = 'Please write Description';
        } else {
            errVehicleTypeCat = 'যানবাহনের ধরণ নির্বাচন করুন ';
            errVehicle = 'যানবাহন নির্বাচন করুন ';
            errRemark = 'বিবরণ লিখুন ';
        }

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                vehicleTypeCat: {
                    required: true,
                    validSelector: true,
                },
                vehicleId: {
                    required: true,
                    validSelector: true,
                },
                "vmMaintenanceItem.remarks":{
                    required: true,
                    validSelector: true,
                }
            },
            messages: {
                vehicleTypeCat: errVehicleTypeCat,
                vehicleId: errVehicle,
                "vmMaintenanceItem.remarks": errRemark,
            }
        });
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-VmMaintenanceItem").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-VmMaintenanceItem");

            $("#field-VmMaintenanceItem").append(t.html());
            SetCheckBoxValues("field-VmMaintenanceItem");

            var tr = $("#field-VmMaintenanceItem").find("tr:last-child");

            tr.attr("id", "VmMaintenanceItem_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                console.log(index + ": " + $(this).attr('id'));
            });

            // let driverElement = document.getElementById("driverPrice_number_" + child_table_extra_id);
            // console.log("---")
            // console.log(driverElement)
            child_table_extra_id++;
            // driverElement.onkeyup = function (){
            // 	totalSum();
            // };
            // console.log(driverElement)


        });


    $("#remove-VmMaintenanceItem").click(function (e) {
        var tablename = 'field-VmMaintenanceItem';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[deletecb="true"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
        totalSum();
    });


    function loadVehicleList() {

        let typeValue = document.getElementById('vehicleTypeCat_category_0').value;
        let url = "Vm_vehicleServlet?actionType=getByVehicleTypeForAssignment&type=" + typeValue;
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                // const response = JSON.parse(fetchedData);
                $("#givenVehicleId_select_0").html(fetchedData);
                if (actionName == 'edit') {
                    $("#givenVehicleId_select_0").val(vehicleId);
                }
                vehicleSelect();
            },
            error: function (error) {
                console.log(error);
            }
        });

    }

    function valueByLanguage(language, bnValue, enValue) {
        if (language == 'english' || language == 'English') {
            return enValue;
        } else {
            return bnValue;
        }
    }

    function totalSum() {
        var sum = 0;
        for (let i = 1; i < child_table_extra_id; i++) {
            let el = document.getElementById("driverPrice_number_" + i);
            if (el) {
                sum += el.value / 1;
            }
        }

        $("#total-value").text(sum);
        // console.log(sum);
    }

    // totalSum();

    function vehicleSelect() {
        let vehicleId = document.getElementById('givenVehicleId_select_0').value;
        if (vehicleId.toString().length === 0) {
            $("#driverText").val("");
            $("#vehicleDriverAssignmentId").val("");
            return;
        }
        // $("#driverText").val(vehicleId);
        // console.log(vehicleId);
        let url = "Vm_vehicle_driver_assignmentServlet?actionType=driverAssigned&vehicleId=" + vehicleId;
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                if (fetchedData && fetchedData.flag) {
                    $("#driverText").val(valueByLanguage
                    (language, fetchedData.driverNameBn, fetchedData.driverNameEn));
                    $("#vehicleDriverAssignmentId").val(fetchedData.assignmentId);
                } else if (fetchedData && !fetchedData.flag) {
                    toastr.error("No driver assigned to this vehicle");
                    $("#driverText").val("");
                    $("#vehicleDriverAssignmentId").val("");
                }
                // console.log(fetchedData);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function formSubmit(){
        event.preventDefault();
        let form = $("#bigform");
        form.validate();
        let valid = form.valid() && PreprocessBeforeSubmiting();

        if(valid){
            buttonStateChange(true);
            let url = form.attr('action');
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });

        }
    }


</script>






