<td style="vertical-align: middle;text-align: left"><%=isLanguageEnglish ? nomineeInfoModel.firstNameEng : nomineeInfoModel.firstNameBan%>
</td>
<td style="vertical-align: middle;text-align: left"><%=isLanguageEnglish ? nomineeInfoModel.relativeTypeEng : nomineeInfoModel.relativeTypeBan%>
</td>
<td style="vertical-align: middle;text-align: left"><%=isLanguageEnglish ? nomineeInfoModel.nomineeCategoryEng : nomineeInfoModel.nomineeCategoryBan%>
</td>
<td style="vertical-align: middle;text-align: left"><%=isLanguageEnglish ? nomineeInfoModel.percentageEng : nomineeInfoModel.percentageBan%>
</td>
<td style="vertical-align: middle;text-align: center">
    <form action="Employee_nomineeServlet?isPermanentTable=true&actionType=delete&tab=2&ID=<%=nomineeInfoModel.dto.iD%>&empId=<%=ID%>&userId=<%=request.getParameter("userId")%>"
          method="POST" id="tableForm_nominee<%=nomineeIndex%>" enctype="multipart/form-data">
        <div class="btn-group" role="group" aria-label="Basic example">
            <button class="btn-success" type="button" title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_EDIT, loginDTO) %>"
                    onclick="location.href='<%=request.getContextPath()%>/Employee_nomineeServlet?actionType=getEditPage&tab=6&iD=<%=nomineeInfoModel.dto.iD%>&empId=<%=ID%>&userId=<%=request.getParameter("userId")%>'">
                <i class="fa fa-edit"></i>&nbsp
            </button>
            <button class="btn-danger" type="button" title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_DELETE, loginDTO) %>"
                    onclick="deleteItem('tableForm_nominee',<%=nomineeIndex%>)"><i class="fa fa-trash"></i>&nbsp
            </button>
        </div>
    </form>
</td>