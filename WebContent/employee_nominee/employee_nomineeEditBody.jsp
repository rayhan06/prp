<%@page import="employee_nominee.EmployeeNomineeModel" %>
<%@page import="employee_nominee.Employee_nomineeDAO" %>

<%@page import="employee_nominee.Employee_nomineeDTO" %>
<%@page import="language.LC" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.util.List" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    String empId = request.getParameter("empId");
    if (empId == null) {
        empId = String.valueOf(request.getAttribute("empId"));
    }
    if (empId == null || empId.isEmpty()) {
        empId = "0";
    }
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

    Employee_nomineeDTO employee_nomineeDTO;
    String actionName,formTitle;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        employee_nomineeDTO = (Employee_nomineeDTO)request.getAttribute(BaseServlet.DTO_FOR_JSP);
        formTitle = LM.getText(LC.EMPLOYEE_NOMINEE_EDIT_EMPLOYEE_NOMINEE_EDIT_FORMNAME, loginDTO);
    } else {
        actionName = "add";
        employee_nomineeDTO = new Employee_nomineeDTO();
        formTitle = LM.getText(LC.EMPLOYEE_NOMINEE_ADD_EMPLOYEE_NOMINEE_ADD_FORMNAME, loginDTO);
    }
    int i = 0;
    Employee_nomineeDAO employeeNomineeDAO = Employee_nomineeDAO.getInstance();
    List<EmployeeNomineeModel> nomineeModels = employeeNomineeDAO.getEmployeeNomineeModelListByEmployeeId(Long.parseLong(empId), employee_nomineeDTO.iD);
    double sum = nomineeModels.stream()
            .mapToDouble(model -> Double.parseDouble(model.percentageEng))
            .sum();
    double maxPercentage = 100.00 - sum;
    DecimalFormat df2 = new DecimalFormat("#.##");
    String remainingValue = df2.format(maxPercentage);
    String Language = LM.getText(LC.EMPLOYEE_NOMINEE_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String url = "Employee_nomineeServlet?actionType=" + "ajax_" + actionName + "&isPermanentTable=true&empId=" + empId;
    if (request.getParameter("tab") != null) {
        url += "&tab=" + request.getParameter("tab")+"&userId="+request.getParameter("userId");
    }
    String context = request.getContextPath() + "/";
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form" action="<%=url%>" id="nominee-form-id" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8  offset-md-2">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD'
                                           id='iD_hidden_<%=i%>' value='<%=employee_nomineeDTO.iD%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='iD'
                                           id='iD_hidden_<%=i%>'
                                           value='<%=employee_nomineeDTO.iD%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='employeeRecordsId'
                                           id='employeeRecordsId_hidden_<%=i%>'
                                           value=<%=empId%> tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='employeeFamilyInfoId'
                                           id='employeeFamilyInfoId_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + employee_nomineeDTO.employeeFamilyInfoId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label text-md-right" for="nominee_<%=i%>">
                                            <%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_FAMILY_MEMBER, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9" id='nominee_div_<%=i%>'>
                                            <select class='form-control' name='nomineeId'
                                                    id='nominee_<%=i%>' tag='pb_html'>
                                                <%=employeeNomineeDAO.getNomineeOptions(Long.parseLong(empId), Language, employee_nomineeDTO.employeeFamilyInfoId)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label text-md-right" for="nomineeCat_category_<%=i%>">
                                            <%=LM.getText(LC.EMPLOYEE_NOMINEE_ADD_NOMINEECAT, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9 " id='nomineeCat_div_<%=i%>'>
                                            <select class='form-control' name='nomineeCat'
                                                    id='nomineeCat_category_<%=i%>' tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("nominee", Language, employee_nomineeDTO.nomineeCat)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label text-md-right" for="percentage_text">
                                            <%=LM.getText(LC.EMPLOYEE_NOMINEE_ADD_PERCENTAGE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9 " id='percentage_div_<%=i%>'>
                                            <input type='text' class='form-control' name='percentage'
                                                   id='percentage_text'
                                                   value='<%=employee_nomineeDTO.percentage>0 ? employee_nomineeDTO.percentage:""%>'   tag='pb_html'/>
                                            <label style="color:red;"><%=isLanguageEnglish?remainingValue:StringUtils.convertToBanNumber(remainingValue)%>% <%=isLanguageEnglish?"Remaining":"অবশিষ্ট"%></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                type="button" onclick="location.href = '<%=request.getHeader("referer")%>'">
                            <%=LM.getText(LC.EMPLOYEE_NOMINEE_EDIT_EMPLOYEE_NOMINEE_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                                onclick="submitForm()">
                            <%=LM.getText(LC.EMPLOYEE_NOMINEE_EDIT_EMPLOYEE_NOMINEE_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
                <div class="view-all-nominee mt-5" id="view-all-nominee-id">
                    <%@include file="employee_all_nominee_view.jsp" %>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<script type="text/javascript">
    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';
    const nomineeForm = $('#nominee-form-id');

    $(document).ready(function () {

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        select2SingleSelector("#nominee_0", '<%=Language%>');
        select2SingleSelector("#nomineeCat_category_0", '<%=Language%>');
        validation();
        $.validator.addMethod('nomineeSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('nomineeCatSelection', function (value, element) {
            return value != 0;
        });
        $("#nominee-form-id").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                percentage: "required",
                nomineeId: {
                    required: true,
                    nomineeSelection: true
                },
                nomineeCat: {
                    required: true,
                    nomineeCatSelection: true
                }
            },

            messages: {
                percentage: "<%=LM.getText(LC.EMPLOYEE_NOMINEE_ADD_EMPLOYEE_NOMINEE_PLEASE_ENTER_PERCENTAGE, userDTO)%>",
                nomineeId: "<%=LM.getText(LC.EMPLOYEE_NOMINEE_ADD_EMPLOYEE_NOMINEE_PLEASE_ENTER_FAMILY_MEMBER, userDTO)%>",
                nomineeCat: "<%=LM.getText(LC.EMPLOYEE_NOMINEE_ADD_EMPLOYEE_NOMINEE_PLEASE_ENTER_NOMINEE_TYPE, userDTO)%>"
            }
        });
    });

    function validation() {
        $('#percentage_text').keydown(function (e) {
            return inputValidationForFloatValue(e, $(this), 2, <%=remainingValue%>);
        });
    }

    function submitForm() {
        if (nomineeForm.valid()) {
            submitAjaxForm('nominee-form-id');
        }
    }

    function buttonStateChange(value) {
        $(':button').prop('disabled', value);
    }
</script>