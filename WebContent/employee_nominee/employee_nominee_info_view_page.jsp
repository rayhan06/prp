<%@ page import="employee_nominee.EmployeeNomineeModel" %>
<%@ page import="java.util.List" %>
<input type="hidden" data-ajax-marker="true">
<table class="table table-bordered table-striped" style="font-size:14px">
    <thead>
        <tr>
            <th><b><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAME, loginDTO) %></b></th>
            <th><b><%= LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_RELATIONTYPE, loginDTO) %></b></th>
            <th><b><%= LM.getText(LC.EMPLOYEE_NOMINEE_ADD_NOMINEECAT, loginDTO) %></b></th>
            <th><b><%= LM.getText(LC.EMPLOYEE_NOMINEE_ADD_PERCENTAGE, loginDTO) %></b></th>
            <th><b></b></th>
        </tr>
    </thead>
    <tbody>
        <%
            List<EmployeeNomineeModel> savedNomineeModelList = (List<EmployeeNomineeModel>) request.getAttribute("savedNomineeModelList");
            int nomineeIndex = 0;
            if(savedNomineeModelList !=null && savedNomineeModelList.size()>0){
                for(EmployeeNomineeModel nomineeInfoModel: savedNomineeModelList){
                    ++nomineeIndex;
        %>
            <tr><%@include file="/employee_nominee/employee_nominee_info_item.jsp"%></tr>
        <%} }%>
    </tbody>
</table>


<div class="row">
    <div class="col-12 mt-3 text-right">
        <button class="btn btn-gray m-t-10 rounded-pill"
                onclick="location.href='Employee_nomineeServlet?actionType=getAddPage&tab=6&empId=<%=ID%>&userId=<%=request.getParameter("userId")%>'">
            <i class="fa fa-plus"></i>&nbsp; <%= LM.getText(LC.HR_MANAGEMENT_OTHERS_ADD_NOMINEE, loginDTO) %>
        </button>
    </div>
</div>