<%@ page import="employee_nominee.*" %>
<%@ page import="util.StringUtils" %>
<div class="row">
    <div class="col-2"></div>
    <div class="col-8">
        <table id="tableData" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_FAMILY_MEMBER, loginDTO)%>
                </th>
                <th><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_RELATIONTYPE, loginDTO)%>
                </th>
                <th><%=LM.getText(LC.EMPLOYEE_NOMINEE_EDIT_NOMINEECAT, loginDTO)%>
                </th>
                <th><%=LM.getText(LC.EMPLOYEE_NOMINEE_EDIT_PERCENTAGE, loginDTO)%>
                </th>
            </tr>
            </thead>
            <tbody>
            <%
                if (nomineeModels != null && nomineeModels.size()>0) {
                    for (EmployeeNomineeModel nomineeModel : nomineeModels) {
            %>
            <tr>
                <%@include file="employee_all_nominee_view_row.jsp" %>
            </tr>
            <%
                    }
                }
            %>
            <tr>
                <td></td>
                <td></td>
                <td><%=LM.getText(LC.CENTRE_REPORT_OTHER_TOTAL, loginDTO)%>
                </td>
                <td><%=StringUtils.convertBanglaIfLanguageIsBangla(Language,String.valueOf(sum))%>
                </td>
            </tr>


            </tbody>

        </table>
    </div>
    <div class="col-2"></div>
</div>