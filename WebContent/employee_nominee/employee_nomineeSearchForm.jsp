<%@page import="employee_family_info.Employee_family_infoDAO" %>
<%@page import="employee_nominee.Employee_nomineeDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.UserDTO" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLangEng = "English".equalsIgnoreCase(Language);
    RecordNavigator rn2 = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
%>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.EMPLOYEE_NOMINEE_EDIT_EMPLOYEERECORDSID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_NOMINEE_EDIT_EMPLOYEEFAMILYINFOID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_NOMINEE_EDIT_NOMINEECAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_NOMINEE_EDIT_PERCENTAGE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_NOMINEE_SEARCH_EMPLOYEE_NOMINEE_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="text-center">
                <span><%="English".equalsIgnoreCase(Language) ? "All" : "সকল"%></span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Employee_nomineeDTO> data = (List<Employee_nomineeDTO>) rn2.list;
            if (data != null && data.size() > 0) {
                List<Long> ids = data.stream()
                        .map(e -> e.employeeFamilyInfoId)
                        .distinct()
                        .collect(Collectors.toList());
                Map<Long, Employee_family_infoDTO> mapByEmployeeFamilyInfoId = Employee_family_infoDAO.getInstance().getDTOs(ids)
                        .stream()
                        .collect(Collectors.toMap(e -> e.iD, e -> e));
                for (int i = 0; i < data.size(); i++) {
                    Employee_nomineeDTO employee_nomineeDTO = data.get(i);
        %>
        <tr id='tr_<%=i%>'>
            <%@include file="employee_nomineeSearchRow.jsp" %>
        </tr>
        <%
                }
            }
        %>
        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>