<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_family_info.Employee_family_infoDTO" %>
<%@ page import="util.StringUtils" %>
<%
    String empId = request.getParameter("empId");
    if (empId == null) {
        empId = String.valueOf(userDTO.employee_record_id);
    }
%>

<td>
    <%=Employee_recordsRepository.getInstance().getEmployeeName(employee_nomineeDTO.employeeRecordsId, Language)%>
</td>


<td>
    <%
        Employee_family_infoDTO employee_family_infoDTO = mapByEmployeeFamilyInfoId.get(employee_nomineeDTO.employeeFamilyInfoId);
    %>

    <%=employee_family_infoDTO == null ? "" :
            (isLangEng ? employee_family_infoDTO.firstNameEn : employee_family_infoDTO.firstNameBn)%>
</td>


<td>
    <%=CatRepository.getInstance().getText(Language, "nominee", employee_nomineeDTO.nomineeCat)%>
</td>


<td>
    <%=isLangEng ? employee_nomineeDTO.percentage : StringUtils.convertToBanNumber(String.valueOf(employee_nomineeDTO.percentage))%>
</td>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Employee_nomineeServlet?actionType=getEditPage&ID=<%=employee_nomineeDTO.iD%>&empId=<%=empId%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=employee_nomineeDTO.iD%>'/></span>
    </div>
</td>