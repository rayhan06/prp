<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="gate_pass_visitor.Gate_pass_visitorDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>
<%@page import="geolocation.GeoLocationDAO2"%>

<%
Gate_pass_visitorDTO gate_pass_visitorDTO = (Gate_pass_visitorDTO)request.getAttribute("gate_pass_visitorDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(gate_pass_visitorDTO == null)
{
	gate_pass_visitorDTO = new Gate_pass_visitorDTO();
	
}
System.out.println("gate_pass_visitorDTO = " + gate_pass_visitorDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.GATE_PASS_VISITOR_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>

			
<%=("<td id = '" + i + "_id" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='id' id = 'id_hidden_<%=i%>' value='<%=gate_pass_visitorDTO.id%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_name'>")%>
			
	
	<div class="form-inline" id = 'name_div_<%=i%>'>
		<input type='text' class='form-control'  name='name' id = 'name_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.name + "'"):("'" + "" + "'")%>
   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_fatherName" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='fatherName' id = 'fatherName_text_<%=i%>' value='<%=gate_pass_visitorDTO.fatherName%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_motherName" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='motherName' id = 'motherName_text_<%=i%>' value='<%=gate_pass_visitorDTO.motherName%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_permanentAddress" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='permanentAddress' id = 'permanentAddress_geolocation_<%=i%>' value='<%=gate_pass_visitorDTO.permanentAddress%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_currentAddress" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='currentAddress' id = 'currentAddress_geolocation_<%=i%>' value='<%=gate_pass_visitorDTO.currentAddress%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_credentialType" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='credentialType' id = 'credentialType_select_<%=i%>' value='<%=gate_pass_visitorDTO.credentialType%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_credentialNo'>")%>
			
	
	<div class="form-inline" id = 'credentialNo_div_<%=i%>'>
		<input type='text' class='form-control'  name='credentialNo' id = 'credentialNo_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.credentialNo + "'"):("'" + "" + "'")%>
   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_mobileNumber'>")%>
			
	
	<div class="form-inline" id = 'mobileNumber_div_<%=i%>'>
		<input type='text' class='form-control'  name='mobileNumber' id = 'mobileNumber_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.mobileNumber + "'"):("'" + "0,1000000" + "'")%>
 <% 
	if(!actionName.equals("edit"))
	{
%>
		required="required"  pattern="880[0-9]{10}" title="mobileNumber must start with 880, then contain 10 digits"
<%
	}
%>
  tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_email'>")%>
			
	
	<div class="form-inline" id = 'email_div_<%=i%>'>
		<input type='text' class='form-control'  name='email' id = 'email_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.email + "'"):("'" + "" + "'")%>
 <% 
	if(!actionName.equals("edit"))
	{
%>
		required="required"  pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$" title="email must be a of valid email address format"
<%
	}
%>
  tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_genderType" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='genderType' id = 'genderType_select_<%=i%>' value='<%=gate_pass_visitorDTO.genderType%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_officerType" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='officerType' id = 'officerType_select_<%=i%>' value='<%=gate_pass_visitorDTO.officerType%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=gate_pass_visitorDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value='<%=gate_pass_visitorDTO.insertedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=gate_pass_visitorDTO.modifiedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.isDeleted + "'"):("'" + "false" + "'")%>
 tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=gate_pass_visitorDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Gate_pass_visitorServlet?actionType=view&ID=<%=gate_pass_visitorDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Gate_pass_visitorServlet?actionType=view&modal=1&ID=<%=gate_pass_visitorDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	