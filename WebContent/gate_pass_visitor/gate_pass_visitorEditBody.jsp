<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="gate_pass_visitor.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="gate_pass.Gate_passDTO" %>
<%@ page import="gate_pass.Gate_passDAO" %>
<%@ page import="org.apache.commons.codec.language.bm.Lang" %>
<%@ page import="gate_pass_item.Gate_pass_itemDTO" %>
<%@ page import="parliament_item.Parliament_itemRepository" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    Gate_pass_visitorDTO gate_pass_visitorDTO;
    Gate_pass_itemDTO gate_pass_itemDTO;
    gate_pass_visitorDTO = (Gate_pass_visitorDTO) request.getAttribute("gate_pass_visitorDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (gate_pass_visitorDTO == null) {
        gate_pass_visitorDTO = new Gate_pass_visitorDTO();

    }
    System.out.println("gate_pass_visitorDTO = " + gate_pass_visitorDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
//    String formTitle = LM.getText(LC.GATE_PASS_VISITOR_ADD_GATE_PASS_VISITOR_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;


    Gate_passDTO gate_passDTO = (Gate_passDTO) session.getAttribute("gate_passDTO");
    System.out.println("Men in Gate: " + gate_passDTO);
    String Language = LM.getText(LC.GATE_PASS_VISITOR_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    String formTitle = Gate_passDAO.getFormTitle(gate_passDTO, Language);

    String context = request.getContextPath() + "/";
    
    
	boolean isLangEng = Language.equalsIgnoreCase("english");
	String number = isLangEng ?"number":"নম্বর";
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Gate_pass_visitorServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="visitor-form" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="row mx-2 mx-md-0">
                                        <div class="col-lg-12">
                                            <div class="sub_title_top">
                                                <div class="sub_title">
                                                    <h4 style="background: white">
                                                        <%=formTitle%>
                                                    </h4>
                                                </div>
                                            </div>
                                            <input type='hidden' class='form-control' name='id'
                                                   id='id_hidden_<%=i%>'
                                                   value='<%=gate_pass_visitorDTO.id%>' tag='pb_html'/>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_MOBILENUMBER, loginDTO)%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <div class="input-group" id='mobileNumber_div_<%=i%>'>
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"
                                                                  id="basic-addon3"><%=isLangEng ? "+88" : "+৮৮"%></span>
                                                        </div>
                                                        <input type='text'
                                                               class='form-control englishDigitOrCharOnly'
                                                               name='mobileNumber'
                                                               id='mobileNumber'
                                                               value='<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.mobileNumber + "'"):("'" + "" + "'")%>'
                                                               placeholder='<%=isLangEng ? "enter 11 digit mobile number, ex: 01710101010" : "১১ ডিজিটের মোবাইল নাম্বারটি দিন, উদাহরণঃ 01710101010"%>'
                                                               tag='pb_html'/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_CREDENTIALTYPE, loginDTO)%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <div id='credentialType_div_<%=i%>'>
                                                        <select class='form-control' name='credentialType'
                                                                id='credentialType'
                                                                onchange = "setCredentialNo(this.id)"
                                                                tag='pb_html'>
                                                            <%=CatRepository.getInstance().buildOptions("credential", Language, null)%>
                                                        </select>
                                                    </div>
                                                    <div id='credentialNo_div_<%=i%>'>
                                                        <input type='text' class='form-control englishDigitOrCharOnly'
                                                               name='credentialNo' id='credentialNo'
                                                               value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.credentialNo + "'"):("'" + "" + "'")%>
                                                                 
                                                               tag='pb_html'/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_EMAIL, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <div id='email_div_<%=i%>'>

                                                        <input type='text' class='form-control' name='email'
                                                               id='email'
                                                               value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.email + "'"):("'" + "" + "'")%>
                                                               placeholder='<%=isLangEng ? "enter valid email" : "ইমেইল এড্রেস প্রবেশ করান"%>'
                                                               tag='pb_html'/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_NAME, loginDTO)%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <div id='name_div_<%=i%>'>
                                                        <input type='text' class='form-control' name='name'
                                                               id='name'
                                                               value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.name + "'"):("'" + "" + "'")%>
                                                                       placeholder='<%=isLangEng ? "enter visitor name" : "সাক্ষাৎ প্রার্থীর নাম প্রবেশ করান"%>'
                                                               tag='pb_html'/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_FATHERNAME, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <div id='fatherName_div_<%=i%>'>
                                                        <input type='text' class='form-control'
                                                               name='fatherName' id='fatherName'
                                                               value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.fatherName + "'"):("'" + "" + "'")%>
                                                                       placeholder='<%=isLangEng ? "enter visitor`s father`s name" : "সাক্ষাৎ প্রার্থীর পিতার নাম প্রবেশ করান"%>'
                                                               tag='pb_html'/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_MOTHERNAME, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <div id='motherName_div_<%=i%>'>
                                                        <input type='text' class='form-control'
                                                               name='motherName' id='motherName'
                                                               value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.motherName + "'"):("'" + "" + "'")%>
                                                                       placeholder='<%=isLangEng ? "enter visitor`s mother`s name" : "সাক্ষাৎ প্রার্থীর মাতার নাম প্রবেশ করান"%>'
                                                               tag='pb_html'/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row" style="display: none" id="addressSelector">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                </label>
                                                <div class="col-md-9 d-flex align-items-center">
                                                    <input class="form-control-sm form-check-input ml-1"
                                                           type="checkbox" value=""
                                                           id="checkNewAddress">
                                                    <label class="form-check-label ml-4 mt-1"
                                                           for="checkNewAddress"
                                                           style="font-size:small;font-weight: bolder; padding-left: 5px">
                                                        <%=isLangEng ? "check, if you want to update address" : "ঠিকানা পরিবর্তন করতে চাইলে টিক দিন"%>
                                                    </label>
                                                </div>
                                            </div>
                                            <div id="address_without_data">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-md-right">
                                                        <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_PERMANENTADDRESS, loginDTO)%>
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <div id='permanentAddress_div_<%=i%>'>
                                                            <div id='permanentAddress_geoDIV_<%=i%>'
                                                                 tag='pb_html'>
                                                                <select class='form-control'
                                                                        name='permanentAddress_active'
                                                                        id='permanentAddress_geoSelectField_<%=i%>'
                                                                        onchange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'permanentAddress', this.getAttribute('row'))"
                                                                        tag='pb_html' row='<%=i%>'></select>
                                                            </div>
                                                            <%if (isLangEng) {%>
                                                            <input type='text'
                                                                   class='englishAddressValidation form-control rounded'
                                                                   name='permanentAddress_text'
                                                                   id='permanentAddress_geoTextField_<%=i%>'
                                                                   value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(gate_pass_visitorDTO.permanentAddress)  + "'"):("'" + "" + "'")%>
                                                                           placeholder='<%=isLangEng ? "Village/House Number" : "গ্রাম/বাড়ি নাম্বার"%>'
                                                                   tag='pb_html'>
                                                            <%} else {%>
                                                            <input type='text'
                                                                   class='noEnglish form-control rounded'
                                                                   name='permanentAddress_text'
                                                                   id='permanentAddress_geoTextField_<%=i%>'
                                                                   value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(gate_pass_visitorDTO.permanentAddress)  + "'"):("'" + "" + "'")%>
                                                                           placeholder='<%=isLangEng ? "Village/House Number" : "গ্রাম/বাড়ি নাম্বার"%>'
                                                                   tag='pb_html'>
                                                            <%
                                                                }
                                                            %>

                                                            <input type='hidden' class='form-control'
                                                                   name='permanentAddress'
                                                                   id='permanentAddress_geolocation_<%=i%>'
                                                                   value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseID(gate_pass_visitorDTO.permanentAddress)  + "'"):("'" + "1" + "'")%>
                                                                           tag='pb_html'>
                                                            <%
                                                                if (actionName.equals("edit")) {
                                                            %>
                                                            <label class="control-label"><%=GeoLocationDAO2.parseText(gate_pass_visitorDTO.permanentAddress, Language) + "," + GeoLocationDAO2.parseDetails(gate_pass_visitorDTO.permanentAddress)%>
                                                            </label>
                                                            <%
                                                                }
                                                            %>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row d-flex align-items-center">
                                                    <label class="col-md-3 col-form-label text-md-right">

                                                    </label>
                                                    <div class="col-md-9 d-flex align-items-center">
                                                        <input class="form-control-sm form-check-input ml-1"
                                                               type="checkbox" value=""
                                                               id="checkCurrentAddress">
                                                        <label class="form-check-label ml-4 mt-1"
                                                               for="checkCurrentAddress"
                                                               style="font-size:small;font-weight: bolder; padding-left: 5px">
                                                            <%=isLangEng ? "check, if permanent address and current/official address is same" : "বর্তমান/দাপ্তরিক ঠিকানা এবং স্থায়ী ঠিকানা একই হলে টিক দিন"%>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group row" id='currentAddress_div'>
                                                    <label class="col-md-3 col-form-label text-md-right">
                                                        <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_CURRENTADDRESS, loginDTO)%>
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <div id='currentAddress_geoDIV_<%=i%>'
                                                             tag='pb_html'>
                                                            <select class='form-control'
                                                                    name='currentAddress_active'
                                                                    id='currentAddress_geoSelectField_<%=i%>'
                                                                    onchange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'currentAddress', this.getAttribute('row'))"
                                                                    tag='pb_html' row='<%=i%>'></select>
                                                        </div>
                                                        <%if (isLangEng) {%>
                                                        <input type='text'
                                                               class='englishAddressValidation form-control rounded'
                                                               name='currentAddress_text'
                                                               id='currentAddress_geoTextField_<%=i%>'
                                                               value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(gate_pass_visitorDTO.currentAddress)  + "'"):("'" + "" + "'")%>
                                                                       placeholder='<%=isLangEng ? "Village/House Number" : "গ্রাম/বাড়ি নাম্বার"%>'
                                                               tag='pb_html'>
                                                        <%} else {%>
                                                        <input type='text'
                                                               class='noEnglish form-control rounded'
                                                               name='currentAddress_text'
                                                               id='currentAddress_geoTextField_<%=i%>'
                                                               value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(gate_pass_visitorDTO.currentAddress)  + "'"):("'" + "" + "'")%>
                                                                       placeholder='<%=isLangEng ? "Village/House Number" : "গ্রাম/বাড়ি নাম্বার"%>'
                                                               tag='pb_html'>
                                                        <%
                                                            }
                                                        %>
                                                        <input type='hidden' class='form-control'
                                                               name='currentAddress'
                                                               id='currentAddress_geolocation_<%=i%>'
                                                               value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseID(gate_pass_visitorDTO.currentAddress)  + "'"):("'" + "1" + "'")%>
                                                                       tag='pb_html'>
                                                        <%
                                                            if (actionName.equals("edit")) {
                                                        %>

                                                        <label class="control-label"><%=GeoLocationDAO2.parseText(gate_pass_visitorDTO.currentAddress, Language) + "," + GeoLocationDAO2.parseDetails(gate_pass_visitorDTO.currentAddress)%>
                                                        </label>
                                                        <%
                                                            }
                                                        %>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="address_with_data" style="display:none">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-md-right">
                                                        <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_PERMANENTADDRESS, loginDTO)%>
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                                    <span id='stored_permanent_address'
                                                                          style="font-weight:bold"></span>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-md-right">
                                                        <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_CURRENTADDRESS, loginDTO)%>
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-9">
                                                                    <span id='stored_current_address'
                                                                          style="font-weight:bold"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng ? "Gender" : "জেন্ডার"%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9 " id='genderType_div_<%=i%>'>
                                                    <select class='form-control' name='genderType'
                                                            id='genderType_select_<%=i%>' tag='pb_html'>
                                                        <%=CatRepository.getInstance().buildOptions("gender", Language, null)%>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=isLangEng ? "Has Vehicle?" : "যানবাহন আছে কি?"%>
                                                   
                                                </label>
                                                <div class="col-md-1 " id='genderType_div_<%=i%>'>
                                                    <input class="form-control-sm form-check-input ml-1"
                                                           type="checkbox"  onchange = "vehicleChecked()"
                                                           id="hasVehicle" name="hasVehicle">
                                                </div>
                                                
                                                
                                            </div>
                                            
                                            <div id = 'vehicleDiv' style = 'display:none'>
	                                            <div class="form-group row">
	                                                <label class="col-md-3 col-form-label text-md-right">
	                                                    <%=isLangEng?"Vehicle Description":"যানবাহনের বর্ণনা"%>
	                                                   <span class="required"> * </span>
	                                                </label>
	                                                <div class="col-md-9" id='officerType_div_<%=i%>'>
	                                                    <input class="form-control"
		                                                           type="text" value=""
		                                                           placeholder = '<%=isLangEng?"Vehicle Description":"যানবাহনের বর্ণনা"%>'
		                                                           id="vehicleDescription" name="vehicleDescription">
	                                                </div>
	                                            </div>
	                                            <div class="form-group row">
	                                                <label class="col-md-3 col-form-label text-md-right">
	                                                    <%=isLangEng?"Vehicle Number":"যানবাহনের নম্বর"%>
	                                                    <span class="required"> * </span>
	                                                </label>
	                                                <div class="col-md-9" id='officerType_div_<%=i%>'>
	                                                    <input class="form-control"
		                                                           type="text" value=""
		                                                            placeholder = '<%=isLangEng?"Vehicle Number":"যানবাহনের নম্বর"%>'
		                                                           
		                                                           id="vehicleNumber" name="vehicleNumber">
	                                                </div>
	                                            </div>
                                            </div>
                                     
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_OFFICERTYPE, loginDTO)%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9" id='officerType_div_<%=i%>'>
                                                    <select class='form-control' name='officerType'
                                                            id='officerType_select_<%=i%>' tag='pb_html'>
                                                        <%=CatRepository.getInstance().buildOptions("officer", Language, null)%>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=LM.getText(LC.HM_OFFICE, loginDTO)%>
                                                    
                                                </label>
                                                <div class="col-md-9" id='officeName_div_<%=i%>'>
                                                
                                                	 <input type='text' class='form-control'
                                                               name='officeName' id='officeName'
                                                               value=''
                                                               placeholder='<%=isLangEng ? "enter visitor`s office" : "সাক্ষাৎ প্রার্থীর অফিস প্রবেশ করান"%>'
                                                               tag='pb_html'/>
                                                    
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=LM.getText(LC.HM_DESIGNATION, loginDTO)%>
                                                    
                                                </label>
                                                <div class="col-md-9" id='designation_div_<%=i%>'>
                                                
                                                	 <input type='text' class='form-control'
                                                               name='designation' id='designation'
                                                               value=''
                                                               placeholder='<%=isLangEng ? "enter visitor`s designation" : "সাক্ষাৎ প্রার্থীর পদবী প্রবেশ করান"%>'
                                                               tag='pb_html'/>
                                                    
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=LM.getText(LC.HM_DESIGNATION, loginDTO)%>
                                                    
                                                </label>
                                                <div class="col-md-9" id='designation_div_<%=i%>'>
                                                
                                                	 <input type='text' class='form-control'
                                                               name='designation' id='designation'
                                                               value=''
                                                               placeholder='<%=isLangEng ? "enter visitor`s designation" : "সাক্ষাৎ প্রার্থীর পদবী প্রবেশ করান"%>'
                                                               tag='pb_html'/>
                                                    
                                                </div>
                                            </div>
                                            
                                            <div class="mb-1 row" id="affiliated-persons-add-lebel">
                                                <div class="col-12 col-form-label">
                                                    <h5 class="font-weight-bold">
                                                        <%=isLangEng ? "Address the affiliated persons:" : "আপনার সাথে প্রবেশকারী ব্যক্তিবর্গের পরিচয় দিনঃ"%>
                                                    </h5>
                                                </div>
                                            </div>
                                            <div id="affiliated-persons-add">
                                                <div class="form-group row" id="add_person_div">
                                                    <label class="col-md-3 col-form-label text-md-right"
                                                           id="gatePassSubTypeIdLabel">
                                                        <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_ADDITIONAL_NUMBER_OF_PERSON, loginDTO)%>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-7">
                                                                <input type='number'
                                                                       class='form-control'
                                                                       name='additional_person'
                                                                       id='additional_person'
                                                                       min="0" value="0" tag='pb_html'/>
                                                            </div>
                                                            <div class="col-md-5 text-md-right mt-3 mt-md-0">
                                                                <button class="btn btn-info btn-block shadow btn-border-radius"
                                                                        type="button"
                                                                        id="additional_person_button">

                                                                    <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_GIVE_THEIR_INFO, loginDTO)%>

                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <input type='hidden' class='form-control'
                                                       name='additionalPersonCount'
                                                       id='additionalPersonCount'
                                                       value="0" tag='pb_html'/>

                                            </div>
                                            <input type='hidden' class='form-control' name='addNewAddress'
                                                   id='addNewAddress'
                                                   value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.insertionDate + "'"):("'" + "0" + "'")%>
                                                           tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertionDate'
                                                   id='insertionDate_hidden_<%=i%>'
                                                   value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.insertionDate + "'"):("'" + "0" + "'")%>
                                                           tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertedBy'
                                                   id='insertedBy_hidden_<%=i%>'
                                                   value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.insertedBy + "'"):("'" + "" + "'")%>
                                                           tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='modifiedBy'
                                                   id='modifiedBy_hidden_<%=i%>'
                                                   value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.modifiedBy + "'"):("'" + "" + "'")%>
                                                           tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='isDeleted'
                                                   id='isDeleted_hidden_<%=i%>'
                                                   value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.isDeleted + "'"):("'" + "false" + "'")%>
                                                           tag='pb_html'/>
                                            <input type='hidden' class='form-control'
                                                   name='lastModificationTime'
                                                   id='lastModificationTime_hidden_<%=i%>'
                                                   value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.lastModificationTime + "'"):("'" + "0" + "'")%>
                                                           tag='pb_html'/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5" id="add-item-div-lebel">
                    <h5 class="table-title">
                        <%=isLangEng ? "Select items you want to carry with" : "আপনি কি কি আইটেম সাথে নিয়ে সংসদ ভবনে প্রবেশ করতে চানঃ"%>
                    </h5>
                    <div id="add-item-div">
                        <div id="parliament-items-add" class="table-responsive">
                            <table id="parliament-items-tableData"
                                   class="table table-bordered table-striped text-nowrap">
                                <thead>
                                <tr>
                                    <th style=" color:#0a6aa1"><%=LM.getText(LC.GATE_PASS_ITEM_ADD_PARLIAMENTITEMID, loginDTO)%>
                                    </th>
                                    <th style=" color:#0a6aa1"><%=LM.getText(LC.GATE_PASS_ITEM_ADD_AMOUNT, loginDTO)%>
                                    </th>
                                    <th style=" color:#0a6aa1"><%=LM.getText(LC.GATE_PASS_ITEM_ADD_DESCRIPTION, loginDTO)%>
                                    </th>
                                    <th style=" color:red"><%=LM.getText(LC.GLOBAL_DELETE, loginDTO)%>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>
                              
                                </tbody>
                            </table>
                            <input type='hidden' class='form-control'
                                   name='totalItemCount' id='totalItemCount'
                                   value="1" tag='pb_html'/>
                        </div>
                        <div class="row">
                            <div class="col-12 form-actions text-right mt-3">
                                <button class="btn btn-sm add-btn text-white shadow btn-border-radius"
                                        type="button" id="add_more_item_button">
                                    <i class="fa fa-plus"></i>&nbsp;
                                    <%=isLangEng ? "Add Item" : "বস্তু যোগ করুন"%>

                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row mt-5">
                    <div class="col-md-12 form-actions text-center">
                        <button class="btn submit-btn text-white shadow btn-border-radius mt-3" type="submit">
                            <%=isLangEng ? "SUBMIT" : "জমা দিন"%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<template id="full-page-loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">
    const fullPageLoader = $('#full-page-loader');
    let dataLoaded = false;
    const language = '<%=Language%>';
    let error1, error2, error3;
    if (language == 'English') {
        error1 = 'select credential type!';
        error2 = 'enter credential number!';
        error3 = 'enter 11 digit phone number!';
        error4 = 'enter your full name!';
    } else {
        error1 = 'পরিচয়পত্রের ধরণ বাছাই করুন!';
        error2 = 'পরিচয়পত্রের নম্বর প্রদান করুন!';
        error3 = '১১ সংখ্যার ফোন নম্বর প্রদান করুন!';
        error4 = 'আপনার পুরো নাম প্রবেশ করুন!';
    }
    $('#visitor-form').on('submit', () => {
        let valid = true;
        $('.credential-type').each((index, element) => {
            if (!$(element).val().match('[0-9]+')) {
                $(element).closest('.affliatedPerson').find('.affiliated-type-error').show();
                $(element).closest('.affliatedPerson').find('.affiliated-type-error').text(error1);
                valid = false;
            }
        });
        $('.credential-no').each((index, element) => {
            if ($(element).val() == "") {
                $(element).closest('.affliatedPerson').find('.affiliated-id-error').show();
                $(element).closest('.affliatedPerson').find('.affiliated-id-error').text(error2);
                valid = false;
            }
        });
        $('.name').each((index, element) => {
            if ($(element).val() == "") {
                $(element).closest('.affliatedPerson').find('.name-error').show();
                $(element).closest('.affliatedPerson').find('.name-error').text(error4);
                valid = false;
            }
        });
        $('.mobile-number').each((index, element) => {
            if (!$(element).val().match('^01[0-9]{9}$') && !$(element).val().match('^০১[০-৯]{9}$')) {
                $(element).closest('.affliatedPerson').find('.affiliated-mobile-error').show();
                $(element).closest('.affliatedPerson').find('.affiliated-mobile-error').text(error3);
                valid = false;
            }
        });

        return valid;
    });

    function init() {

        $("#credentialType").select2({
            dropdownAutoWidth: true
        });

        $("#genderType_select_0").select2({
            dropdownAutoWidth: true
        });

        $("#officerType_select_0").select2({
            dropdownAutoWidth: true
        });
    }
	
    var additionalButtonClicked = false;

    $(document).ready(function () {
        init();
        $('#checkNewAddress').prop('checked', true);

        initGeoLocation('permanentAddress_geoSelectField_', 0, "Gate_pass_visitorServlet");
        initGeoLocation('currentAddress_geoSelectField_', 0, "Gate_pass_visitorServlet");

        const currentAddressHTML = $('#currentAddress_div').html();
        let lastPersonCount = 0;
        let currentItemCount = 1;

        $('#additional_person_button').click(() => {
        	additionalButtonClicked = true;
            fullPageLoader.show();
            const currentPersonCount = $('#additional_person').val();
            if (!isNaN(parseInt(currentPersonCount))) {
                for (let i = lastPersonCount - 1; i >= currentPersonCount; i--) {
                    $('#affliatedPerson_div_' + i).remove();
                }
                if (lastPersonCount < currentPersonCount) {
                    let url = "Gate_pass_affiliated_personServlet?actionType=getAddPage" + "&language=<%=Language%>&personCount="
                        + currentPersonCount + "&startVal=" + lastPersonCount;
                    // console.log("office_unit ajax url : " + url);
                    $.ajax({
                        url: url,
                        type: "GET",
                        async: true,
                        success: function (data) {

                            $('#add_person_div').append(data);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                }
                lastPersonCount = currentPersonCount;
                $('#additionalPersonCount').val(lastPersonCount);
            }
            fullPageLoader.hide();
        });

        $('#add_more_item_button').click(() => {
            fullPageLoader.show();
            currentItemCount++;
            let url = "Gate_pass_itemServlet?actionType=getAddPage" + "&language=<%=Language%>&rowNumber="
                + currentItemCount;
            // console.log("office_unit ajax url : " + url);
            $.ajax({
                url: url,
                type: "GET",
                async: true,
                success: function (data) {

                    $('#parliament-items-tableData > tbody').append(data);
                },
                error: function (error) {
                    console.log(error);
                }
            });
            $('#totalItemCount').val(currentItemCount);
            fullPageLoader.hide();
        });

        // validation on input
        $.validator.addMethod('isValidMobileNumber', function (value, element) {
            let english = /^[A-Za-z0-9]*$/;
            let mobileNo = $('#mobileNumber').val();
            if (!english.test(mobileNo)) {
                return value.match('[\u09E6-\u09EF]');
            } else {
                return value.match('^01[3-9]{1}[0-9]{8}$');
            }
        });

        $.validator.addMethod('genderSelection', function (value, element) {
            return value != 0;
        });


        $.validator.addMethod('officerSelection', function (value, element) {
            return value != 0;
        });


        $.validator.addMethod('credentialSelection', function (value, element) {
            return value != 0;
        });

        $("#visitor-form").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                name: "required",
                // fatherName: "required",
                // motherName: "required",
                credentialNo: "required",
                genderType: {
                    required: true,
                    genderSelection: true
                },
                officerType: {
                    required: true,
                    officerSelection: true
                },
                credentialType: {
                    required: true,
                    credentialSelection: true
                },
                mobileNumber: {
                    required: true,
                    isValidMobileNumber: true
                }
                // permanentAddress_active: {
                //     required: true,
                //     permanentAddress_activeSelection: true
                // },
                // currentAddress_active: {
                //     required: true,
                //     currentAddress_activeSelection: true
                // }
            },
            messages: {
                name: '<%=isLangEng ? "enter visitor`s name!" : "সাক্ষাৎ প্রার্থীর নাম প্রবেশ করান!"%>',
                <%--fatherName: '<%=isLangEng ? "enter visitor`s father`s name!" : "সাক্ষাৎ প্রার্থীর পিতার নাম প্রবেশ করান!"%>',--%>
                <%--motherName: '<%=isLangEng ? "enter visitor`s mother`s name!" : "সাক্ষাৎ প্রার্থীর মাতার নাম প্রবেশ করান!"%>',--%>
                credentialNo: '<%=isLangEng ? "enter credential number!" : "পরিচয়পত্রের নাম্বার প্রবেশ করান!"%>',
                genderType: '<%=isLangEng ? "select gender!" : "লিঙ্গ বাছাই করুন!"%>',
                officerType: '<%=isLangEng ? "select officer type!" : "কর্মকর্তার ধরন বাছাই করুন!"%>',
                credentialType: '<%=isLangEng ? "select credential type!" : "পরিচয়পত্রের ধরন বাছাই করুন!"%>',
                mobileNumber: '<%=isLangEng ? "mobile number must be 11 digit, ex: 01710101010" : "মোবাইল নাম্বারটি অবশ্যই ১১ ডিজিটের হতে হবে, উদাহরণঃ 01710101010"%>'
                <%--permanentAddress_active: '<%=isLangEng ? "select permanent address!" : "স্থায়ী ঠিকানা বাছাই করুন!"%>',--%>
                <%--currentAddress_active: '<%=isLangEng ? "select current/official address!" : "বর্তমান/দাপ্তরিক ঠিকানা বাছাই করুন!"%>'--%>
            }
        });


        // checkbox for permanent and current/official address

        $('#checkCurrentAddress').click(function () {

            if ($('#checkCurrentAddress').prop('checked')) {
                $('#currentAddress_div').hide();
            } else {
                $('#currentAddress_div').html(currentAddressHTML);
                initGeoLocation('currentAddress_geoSelectField_', 0, "Gate_pass_visitorServlet");
                $('#currentAddress_div').show();
            }
        });

        $('#checkNewAddress').click(function () {

            if ($('#checkNewAddress').prop('checked')) {
                $('#address_without_data').show();
                $('#address_with_data').hide();
            } else {
                $('#address_without_data').hide();
                $('#address_with_data').show();
            }
        });


        const gate_pass_type_id = '<%=gate_passDTO.gatePassTypeId%>';
        if (gate_pass_type_id === '1') {
            $('#affiliated-persons-add').show();
            $('#add-item-div').hide();
            $('#affiliated-persons-add-lebel').show();
            $('#add-item-div-lebel').hide();
        }
        else if (gate_pass_type_id === '10') {
            $('#affiliated-persons-add').show();
            $('#add-item-div').show();
            $('#affiliated-persons-add-lebel').show();
            $('#add-item-div-lebel').show();
        }
        else if (gate_pass_type_id === '11') {
            $('#affiliated-persons-add').hide();
            $('#add-item-div').hide();
            $('#affiliated-persons-add-lebel').hide();
            $('#add-item-div-lebel').hide();
        }
        else {
            $('#affiliated-persons-add').show();
            $('#add-item-div').show();
            $('#affiliated-persons-add-lebel').show();
            $('#add-item-div-lebel').show();
        }


        function populateInputFields() {
            let mobileNo = $('#mobileNumber').val();
            let credentialNo = $('#credentialNo').val();
            let credentialType = $('#credentialType').val();

            if (mobileNo.length === 11 && credentialType && credentialNo) {

                mobileNo = '88' + mobileNo;
                console.log(mobileNo);
                // ajax call for stored data
                let url = "Gate_pass_visitorServlet?actionType=getStoredDataByMobileNumber" + "&mobileNumber=" + mobileNo
                    + "&credentialNo=" + credentialNo;

                $.ajax({
                    url: url,
                    type: "GET",
                    async: true,
                    success: function (data) {

                        if (data.localeCompare('noString') === -1) {

                            let dto = JSON.parse(data);
                            // $('#isPreviousData').val('true');
                            // Now populate data with stored data
                            PopulateData(dto);
                            dataLoaded = true;
                        } else if (data.localeCompare('noString') === 1) {
                            // $('#isPreviousData').val('false');
                            // Now unpopulate data with stored data
                            if (dataLoaded) {
                                UnPopulateData();
                                dataLoaded = false;
                            }
                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
        }


        $("#mobileNumber").on("input", () => {
            let mobileNo = $('#mobileNumber').val();
            if (mobileNo.length > 11) {
                mobileNo = mobileNo.substr(0, 11);
            }
            const english = /^[A-Za-z0-9]*$/;
            if (!english.test(mobileNo)) {
                mobileNo = convertToEnglishNumber(mobileNo);
                $('#mobileNumber').val(mobileNo);
            } else {
                $('#mobileNumber').val(mobileNo);
            }
            if (mobileNo.length === 11) {
                populateInputFields();
            }
        });

        $("#credentialNo").focusout(function(){
            populateInputFields();
        });

        $("#credentialType").on("change", () => {
            populateInputFields();
        });

    });


    function PopulateData(dto) {
        $('#addressSelector').show();
        $('#checkNewAddress').prop('checked', false);

        $('#name').val(dto.name);
        // $('#name').prop("readonly", true);

        $('#fatherName').val(dto.fatherName);
        // $('#fatherName').prop("readonly", true);

        $('#motherName').val(dto.motherName);
        // $('#motherName').prop("readonly", true);

        $('#email').val(dto.email);
        // $('#email').prop("readonly", true);

        // $('#credentialNo').val(dto.credentialNo);
        // $('#credentialNo').prop("readonly", true);

        // $('#credentialType').val(dto.credentialType);
        // $('#credentialType').attr("disabled", true);

        $('#genderType_select_0').val(dto.genderType);
        // $('#genderType_select_0').attr("disabled", true);

        $('#officerType_select_0').val(dto.officerType);
        // $('#officerType_select_0').attr("disabled", true);

        $('#address_without_data').hide();

        <%
            if(isLangEng) {
        %>

        document.getElementById("stored_permanent_address").textContent = parseEnglishAddress(dto.permanentAddress);
        document.getElementById("stored_current_address").textContent = parseEnglishAddress(dto.currentAddress);
        $('#address_with_data').show();
        <%
            } else {
        %>
        document.getElementById("stored_permanent_address").textContent = parseBanglaAddress(dto.permanentAddress);
        document.getElementById("stored_current_address").textContent = parseBanglaAddress(dto.currentAddress);
        $('#address_with_data').show();
        <%
            }
        %>
        let currentAddr = dto.currentAddress.split(':')[0] + '$' + dto.currentAddress.split(':')[2];
        let permanentAddr = dto.permanentAddress.split(':')[0] + '$' + dto.permanentAddress.split(':')[2];
        $('#currentAddress_geolocation_0').val(currentAddr);
        $('#permanentAddress_geolocation_0').val(permanentAddr);
    }

    function UnPopulateData() {
        // Now unpopulate data with stored data
        $('#addressSelector').hide();
        $('#checkNewAddress').prop('checked', true);

        $('#name').val('');
        // $('#name').prop("readonly", false);

        $('#fatherName').val('');
        // $('#fatherName').prop("readonly", false);

        $('#motherName').val('');
        // $('#motherName').prop("readonly", false);

        $('#email').val('');
        // $('#email').prop("readonly", false);

        // $('#credentialNo').val('');
        // $('#credentialNo').prop("readonly", false);

        // $('#credentialType').prop('selectedIndex', 0);
        // $('#credentialType').attr("disabled", false);

        $('#genderType_select_0').prop('selectedIndex', 0);
        // $('#genderType_select_0').attr("disabled", false);

        $('#officerType_select_0').prop('selectedIndex', 0);
        // $('#officerType_select_0').attr("disabled", false);

        $('#address_without_data').show();

        $('#address_with_data').hide();
    }

    function PreprocessBeforeSubmiting(row) {
    	if($('#additional_person').val() != '0' && !additionalButtonClicked)
    	{
    		toastr.error("Additional Person Information Required");
    		return false;
    	}
        if ($('#checkNewAddress').prop('checked')) {
            preprocessGeolocationBeforeSubmitting('permanentAddress', row, false);

            if ($('#checkCurrentAddress').prop('checked')) {
                $('#currentAddress_geolocation_0').val($('#permanentAddress_geolocation_0').val());
            } else {
                preprocessGeolocationBeforeSubmitting('currentAddress', row, false);
            }
        }
        console.log($('#currentAddress_geolocation_0').val());
        let noSamePass = true;
        const mobileNo = '88' + $('#mobileNumber').val();
        const credentialNo = $('#credentialNo').val();

        let url = "Gate_pass_visitorServlet?actionType=samePassCheck" + "&mobileNumber=" + mobileNo
            + "&credentialNo=" + credentialNo + "&visitingDate=<%=gate_passDTO.visitingDate%>"
            + "&gatePassType=<%=gate_passDTO.gatePassTypeId%>&gatePassSubType=<%=gate_passDTO.gatePassSubTypeId%>";

        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (data) {
                console.log(data);
                if (data.localeCompare('foundSamePass') === 0) {
                    noSamePass = false;
                    console.log("Same pass detected");
                    Swal.fire({
                        position: 'center',
                        type: 'error',
                        title: '<%=isLangEng ? "Already a "+formTitle+" application is listed for you this date!" : "পূর্ব হতেই আপনার আজকের দিনের "+formTitle+" এর আবেদন করা আছে!"%>',
                        showConfirmButton: false,
                        timer: 3000
                    });
                }
            },
            error: function (error) {
                console.log(error);
            }
        });

        return noSamePass;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Gate_pass_visitorServlet");
    }

    let child_table_extra_id = <%=childTableStartingID%>;


    function parseEnglishAddress(address) {
        let addresses = address.split(":");

        return addresses[2] + ", " + addresses[1];
    }

    function parseBanglaAddress(address) {
        let addresses = address.split(":");

        return addresses[2] + ", " + addresses[3];
    }


    $(".englishDigitOrCharOnly").keypress(function (event) {
        var ew = event.which;
        if (ew == 32)
            return true;
        var charStr = String.fromCharCode(ew);

        if (charStr == "0") {
            return true;
        } else if (charStr == "1") {
            return true;
        } else if (charStr == "2") {
            return true;
        } else if (charStr == "3") {
            return true;
        } else if (charStr == "4") {
            return true;
        } else if (charStr == "5") {
            return true;
        } else if (charStr == "6") {
            return true;
        } else if (charStr == "7") {
            return true;
        } else if (charStr == "8") {
            return true;
        } else if (charStr == "9") {
            return true;
        } else if (charStr >= 'A' && charStr <= 'Z') {
            return true;
        } else if (charStr >= 'a' && charStr <= 'z') {
            return true;
        }
        return false;
    });


    function convertToEnglishNumber(input) {
        let numbers = {
            '০': 0,
            '১': 1,
            '২': 2,
            '৩': 3,
            '৪': 4,
            '৫': 5,
            '৬': 6,
            '৭': 7,
            '৮': 8,
            '৯': 9
        };
        let output = [];
        for (let i = 0; i < input.length; ++i) {
            if (numbers.hasOwnProperty(input[i])) {
                output.push(numbers[input[i]]);
            } else {
                output.push(input[i]);
            }
        }
        return output.join('');
    }
    
    function setCredentialNo(id)
    {
    	 if(parseInt($("#" + id).val()) >= 0)
         {
         	var text = $("#" + id + " :selected").text() + ' <%=number%>';
         	$("#credentialNo").attr('placeholder', text);
         }
         else
         {
        	 $("#credentialNo").attr('placeholder', '');
         }
    }
    
    function vehicleChecked()
    {
    	if($("#hasVehicle").prop("checked"))
   		{
   			$("#vehicleDiv").show();
   			$('#vehicleDescription').attr('required', true);
   			$('#vehicleNumber').attr('required', true);
   		}
    	else
    	{
    		$("#vehicleDiv").hide();
    		$('#vehicleDescription').removeAttr('required');
   			$('#vehicleNumber').removeAttr('required');
    	}
    }

</script>