<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="parliament_supplier.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>

<%@ page import="geolocation.*" %>


<%
    String servletName = "Parliament_supplierServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.PARLIAMENT_SUPPLIER_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Parliament_supplierDAO parliament_supplierDAO = new Parliament_supplierDAO("parliament_supplier");
    Parliament_supplierDTO parliament_supplierDTO = parliament_supplierDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
	<div class="kt-portlet">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title prp-page-title">
					<i class="fa fa-gift"></i>&nbsp;
					<%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_PARLIAMENT_SUPPLIER_ADD_FORMNAME, loginDTO)%>
				</h3>
			</div>
		</div>
		<div class="kt-portlet__body form-body">
			<div>
				<h5 class="table-title">
					<%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_PARLIAMENT_SUPPLIER_ADD_FORMNAME, loginDTO)%>
				</h5>
				<div class="table-responsive">
					<table class="table table-bordered table-striped text-nowrap">
						<tr>
							<td><b><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_NAMEEN, loginDTO)%>
							</b></td>
							<td>

								<%
									value = parliament_supplierDTO.nameEn + "";
								%>

								<%=Utils.getDigits(value, Language)%>


							</td>

						</tr>


						<tr>
							<td><b><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_NAMEBN, loginDTO)%>
							</b></td>
							<td>

								<%
									value = parliament_supplierDTO.nameBn + "";
								%>

								<%=Utils.getDigits(value, Language)%>


							</td>

						</tr>


						<tr>
							<td><b><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_AGREEMENTDATE, loginDTO)%>
							</b></td>
							<td>

								<%
									value = parliament_supplierDTO.agreementDate + "";
								%>
								<%
									String formatted_agreementDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
								%>
								<%=Utils.getDigits(formatted_agreementDate, Language)%>


							</td>

						</tr>


						<tr>
							<td><b><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_FILESDROPZONE, loginDTO)%>
							</b></td>
							<td>

								<%
									value = parliament_supplierDTO.filesDropzone + "";
								%>
								<%
									{
										List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(parliament_supplierDTO.filesDropzone);
								%>
								<%@include file="../pb/dropzoneViewer.jsp" %>
								<%
									}
								%>


							</td>

						</tr>


						<tr>
							<td>
								<b><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_TRADELICENSENUMBER, loginDTO)%>
								</b></td>
							<td>

								<%
									value = parliament_supplierDTO.tradeLicenseNumber + "";
								%>

								<%=Utils.getDigits(value, Language)%>


							</td>

						</tr>


						<tr>
							<td>
								<b><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_TRADELICENSEEXPIRYDATE, loginDTO)%>
								</b></td>
							<td>

								<%
									value = parliament_supplierDTO.tradeLicenseExpiryDate + "";
								%>
								<%
									String formatted_tradeLicenseExpiryDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
								%>
								<%=Utils.getDigits(formatted_tradeLicenseExpiryDate, Language)%>


							</td>

						</tr>


						<tr>
							<td><b><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_SUPPLIERCODE, loginDTO)%>
							</b></td>
							<td>

								<%
									value = parliament_supplierDTO.supplierCode + "";
								%>

								<%=Utils.getDigits(value, Language)%>


							</td>

						</tr>


						<tr>
							<td><b><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_ISFOREIGN, loginDTO)%>
							</b></td>
							<td>

								<%
									value = parliament_supplierDTO.isForeign + "";
								%>

								<%=Utils.getDigits(value, Language)%>


							</td>

						</tr>


						<tr>
							<td><b><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_SUPPLIERBIN, loginDTO)%>
							</b></td>
							<td>

								<%
									value = parliament_supplierDTO.supplierBin + "";
								%>

								<%=Utils.getDigits(value, Language)%>


							</td>

						</tr>


						<tr>
							<td><b><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_SUPPLIERADDRESS, loginDTO)%>
							</b></td>
							<td>

								<%
									value = parliament_supplierDTO.supplierAddress + "";
								%>
								<%=GeoLocationDAO2.getAddressToShow(value, Language)%>


							</td>

						</tr>


						<tr>
							<td><b><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_SUPPLIERMOBILE1, loginDTO)%>
							</b></td>
							<td>

								<%
									value = parliament_supplierDTO.supplierMobile1 + "";
								%>

								<%=Utils.getDigits(value, Language)%>


							</td>

						</tr>


						<tr>
							<td><b><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_SUPPLIERMOBILE2, loginDTO)%>
							</b></td>
							<td>

								<%
									value = parliament_supplierDTO.supplierMobile2 + "";
								%>

								<%=Utils.getDigits(value, Language)%>


							</td>

						</tr>


						<tr>
							<td><b><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_SUPPLIEREMAIL, loginDTO)%>
							</b></td>
							<td>

								<%
									value = parliament_supplierDTO.supplierEmail + "";
								%>

								<%=Utils.getDigits(value, Language)%>


							</td>

						</tr>


						<tr>
							<td><b><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_REMARKS, loginDTO)%>
							</b></td>
							<td>

								<%
									value = parliament_supplierDTO.remarks + "";
								%>

								<%=Utils.getDigits(value, Language)%>


							</td>

						</tr>


					</table>
				</div>
			</div>
		</div>
	</div>
</div>