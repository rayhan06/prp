<%@page pageEncoding="UTF-8" %>

<%@page import="parliament_supplier.*" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="files.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.PARLIAMENT_SUPPLIER_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_PARLIAMENT_SUPPLIER;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Parliament_supplierDTO parliament_supplierDTO = (Parliament_supplierDTO) request.getAttribute("parliament_supplierDTO");
    CommonDTO commonDTO = parliament_supplierDTO;
    String servletName = "Parliament_supplierServlet";


    System.out.println("parliament_supplierDTO = " + parliament_supplierDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Parliament_supplierDAO parliament_supplierDAO = (Parliament_supplierDAO) request.getAttribute("parliament_supplierDAO");

    FilesDAO filesDAO = new FilesDAO();

    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td id='<%=i%>_nameEn' class="text-nowrap">
    <%
        value = parliament_supplierDTO.nameEn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_nameBn' class="text-nowrap">
    <%
        value = parliament_supplierDTO.nameBn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_agreementDate' class="text-nowrap">
    <%
        value = parliament_supplierDTO.agreementDate + "";
    %>
    <%
        String formatted_agreementDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_agreementDate, Language)%>


</td>


<td id='<%=i%>_tradeLicenseNumber' class="text-nowrap">
    <%
        value = parliament_supplierDTO.tradeLicenseNumber + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_tradeLicenseExpiryDate' class="text-nowrap">
    <%
        value = parliament_supplierDTO.tradeLicenseExpiryDate + "";
    %>
    <%
        String formatted_tradeLicenseExpiryDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_tradeLicenseExpiryDate, Language)%>


</td>

<td id='<%=i%>_supplierCode' class="text-nowrap">
    <%
        value = parliament_supplierDTO.supplierCode + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_isForeign' class="text-nowrap">
    <%
        value = parliament_supplierDTO.isForeign + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_supplierBin' class="text-nowrap">
    <%
        value = parliament_supplierDTO.supplierBin + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_supplierAddress' class="text-nowrap">
    <%
        value = parliament_supplierDTO.supplierAddress + "";
    %>
    <%=GeoLocationDAO2.getAddressToShow(value, Language)%>


</td>

<td id='<%=i%>_supplierMobile1' class="text-nowrap">
    <%
        value = parliament_supplierDTO.supplierMobile1 + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_supplierMobile2' class="text-nowrap">
    <%
        value = parliament_supplierDTO.supplierMobile2 + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_supplierEmail' class="text-nowrap">
    <%
        value = parliament_supplierDTO.supplierEmail + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_remarks' class="text-nowrap">
    <%
        value = parliament_supplierDTO.remarks + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Parliament_supplierServlet?actionType=view&ID=<%=parliament_supplierDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Parliament_supplierServlet?actionType=getEditPage&ID=<%=parliament_supplierDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td class="text-right" id='<%=i%>_checkbox'>
	<div class='checker'>
        <span class='chkEdit'>
            <input type='checkbox' name='ID' value='<%=parliament_supplierDTO.iD%>'/>
        </span>
	</div>
</td>
																						
											

