<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="parliament_supplier.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    Parliament_supplierDTO parliament_supplierDTO;
    parliament_supplierDTO = (Parliament_supplierDTO) request.getAttribute("parliament_supplierDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (parliament_supplierDTO == null) {
        parliament_supplierDTO = new Parliament_supplierDTO();

    }
    System.out.println("parliament_supplierDTO = " + parliament_supplierDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_PARLIAMENT_SUPPLIER_ADD_FORMNAME, loginDTO);
    String servletName = "Parliament_supplierServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID = -1;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;
    String Language = LM.getText(LC.PARLIAMENT_SUPPLIER_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Parliament_supplierServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=parliament_supplierDTO.iD%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_NAMEEN, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='nameEn'
                                                   id='nameEn_text_<%=i%>'
                                                   value='<%=parliament_supplierDTO.nameEn%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_NAMEBN, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='nameBn'
                                                   id='nameBn_text_<%=i%>'
                                                   value='<%=parliament_supplierDTO.nameBn%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_AGREEMENTDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%value = "agreementDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='agreementDate'
                                                   id='agreementDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(parliament_supplierDTO.agreementDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_FILESDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                fileColumnName = "filesDropzone";
                                                if (actionName.equals("edit")) {
                                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(parliament_supplierDTO.filesDropzone);
                                            %>
                                            <%@include file="../pb/dropzoneEditor.jsp" %>
                                            <%
                                                } else {
                                                    ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                    parliament_supplierDTO.filesDropzone = ColumnID;
                                                }
                                            %>

                                            <div class="dropzone"
                                                 action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=parliament_supplierDTO.filesDropzone%>">
                                                <input type='file' style="display:none"
                                                       name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>'
                                                       tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value=''
                                                   tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=parliament_supplierDTO.filesDropzone%>'/>


                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_TRADELICENSENUMBER, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='tradeLicenseNumber'
                                                   id='tradeLicenseNumber_text_<%=i%>'
                                                   value='<%=parliament_supplierDTO.tradeLicenseNumber%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_TRADELICENSEEXPIRYDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%value = "tradeLicenseExpiryDate_js_" + i;
                                            int year = Calendar.getInstance().get(Calendar.YEAR);%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                <jsp:param name="END_YEAR" value="<%=year + 5%>"/>
                                            </jsp:include>
                                            <input type='hidden' name='tradeLicenseExpiryDate'
                                                   id='tradeLicenseExpiryDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(parliament_supplierDTO.tradeLicenseExpiryDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_SUPPLIERCODE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='supplierCode'
                                                   id='supplierCode_text_<%=i%>'
                                                   value='<%=parliament_supplierDTO.supplierCode%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_ISFOREIGN, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='checkbox' class='form-control-sm mt-1' name='isForeign'
                                                   id='isForeign_checkbox_<%=i%>'
                                                   value='true'                                                                <%=(String.valueOf(parliament_supplierDTO.isForeign).equals("true"))?("checked"):""%>
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_SUPPLIERBIN, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='supplierBin'
                                                   id='supplierBin_text_<%=i%>'
                                                   value='<%=parliament_supplierDTO.supplierBin%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_SUPPLIERADDRESS, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <div id='supplierAddress_geoDIV_<%=i%>' tag='pb_html'>
                                                <select class='form-control' name='supplierAddress_active'
                                                        id='supplierAddress_geoSelectField_<%=i%>'
                                                        onChange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'supplierAddress', this.getAttribute('row'))"
                                                        tag='pb_html' row='<%=i%>'></select>
                                            </div>
                                            <input type='text' class='form-control' name='supplierAddress_text'
                                                   id='supplierAddress_geoTextField_<%=i%>'
                                                   onkeypress="return (event.charCode != 36 && event.keyCode != 36)"
                                                   value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(parliament_supplierDTO.supplierAddress)  + "'"):("'" + "" + "'")%>
                                                           placeholder='Road Number, House Number etc'
                                            tag='pb_html'>
                                            <input type='hidden' class='form-control' name='supplierAddress'
                                                   id='supplierAddress_geolocation_<%=i%>'
                                                   value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseID(parliament_supplierDTO.supplierAddress)  + "'"):("'" + "1" + "'")%>
                                                           tag='pb_html'>
                                            <%
                                                if (actionName.equals("edit")) {
                                            %>
                                            <label class="control-label"><%=GeoLocationDAO2.parseText(parliament_supplierDTO.supplierAddress, Language) + "," + GeoLocationDAO2.parseDetails(parliament_supplierDTO.supplierAddress)%>
                                            </label>
                                            <%
                                                }
                                            %>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_SUPPLIERMOBILE1, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='supplierMobile1'
                                                   id='supplierMobile1_text_<%=i%>'
                                                   value='<%=parliament_supplierDTO.supplierMobile1%>'                                                                <%
                                                if (!actionName.equals("edit")) {
                                            %>
                                                    pattern="880[0-9]{10}"
                                                   title="supplierMobile1 must start with 880, then contain 10 digits"
                                                    <%
                                                        }
                                                    %>
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_SUPPLIERMOBILE2, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='supplierMobile2'
                                                   id='supplierMobile2_text_<%=i%>'
                                                   value='<%=parliament_supplierDTO.supplierMobile2%>'                                                                <%
                                                if (!actionName.equals("edit")) {
                                            %>
                                                    pattern="880[0-9]{10}"
                                                   title="supplierMobile2 must start with 880, then contain 10 digits"
                                                    <%
                                                        }
                                                    %>
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_SUPPLIEREMAIL, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='supplierEmail'
                                                   id='supplierEmail_text_<%=i%>'
                                                   value='<%=parliament_supplierDTO.supplierEmail%>'                                                                <%
                                                if (!actionName.equals("edit")) {
                                            %>
                                                   
                                                   pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$"
                                                   title="supplierEmail must be a of valid email address format"
                                                    <%
                                                        }
                                                    %>
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_REMARKS, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='remarks'
                                                   id='remarks_text_<%=i%>'
                                                   value='<%=parliament_supplierDTO.remarks%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=parliament_supplierDTO.insertedByUserId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=parliament_supplierDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=parliament_supplierDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=parliament_supplierDTO.searchColumn%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value='<%=parliament_supplierDTO.isDeleted%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=parliament_supplierDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11">
                        <div class="form-actions text-right mt-3">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn btn-border-radius">
                                <%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_PARLIAMENT_SUPPLIER_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius" type="submit">
                                <%=LM.getText(LC.PARLIAMENT_SUPPLIER_ADD_PARLIAMENT_SUPPLIER_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {


        preprocessDateBeforeSubmitting('agreementDate', row);
        preprocessDateBeforeSubmitting('tradeLicenseExpiryDate', row);
        preprocessCheckBoxBeforeSubmitting('isForeign', row);
        return  preprocessGeolocationBeforeSubmitting('supplierAddress', row, false);

        // return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Parliament_supplierServlet");
    }

    function init(row) {

        setDateByStringAndId('agreementDate_js_' + row, $('#agreementDate_date_' + row).val());
        setDateByStringAndId('tradeLicenseExpiryDate_js_' + row, $('#tradeLicenseExpiryDate_date_' + row).val());
        initGeoLocation('supplierAddress_geoSelectField_', row, "Parliament_supplierServlet");


    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






