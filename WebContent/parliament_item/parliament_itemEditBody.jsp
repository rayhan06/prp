<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="parliament_item.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>

<%
    Parliament_itemDTO parliament_itemDTO;
    parliament_itemDTO = (Parliament_itemDTO) request.getAttribute("parliament_itemDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (parliament_itemDTO == null) {
        parliament_itemDTO = new Parliament_itemDTO();

    }
    System.out.println("parliament_itemDTO = " + parliament_itemDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.PARLIAMENT_ITEM_ADD_PARLIAMENT_ITEM_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Parliament_itemServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <%@ page import="java.text.SimpleDateFormat" %>
                                    <%@ page import="java.util.Date" %>
                                    <%@ page import="pb.*" %>
                                    <%
                                        String Language = LM.getText(LC.PARLIAMENT_ITEM_EDIT_LANGUAGE, loginDTO);
                                        String Options;
                                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                        Date date = new Date();
                                        String datestr = dateFormat.format(date);
                                        CommonDAO.language = Language;
                                        CatDAO.language = Language;
                                    %>
                                    <input type='hidden' class='form-control' name='ID'
                                           id='id_hidden_<%=i%>'
                                           value='<%=parliament_itemDTO.iD%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.PARLIAMENT_ITEM_ADD_NAMEENG, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <div id='nameEng_div_<%=i%>'>
                                                <input type='text' class='form-control' name='nameEng'
                                                       id='nameEng_text_<%=i%>'
                                                       value=<%=actionName.equals("edit")?("'" + parliament_itemDTO.nameEng + "'"):("'" + "" + "'")%>
                                                               tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.PARLIAMENT_ITEM_ADD_NAMEBNG, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <div id='nameBng_div_<%=i%>'>
                                                <input type='text' class='form-control' name='nameBng'
                                                       id='nameBng_text_<%=i%>'
                                                       value=<%=actionName.equals("edit")?("'" + parliament_itemDTO.nameBng + "'"):("'" + "" + "'")%>
                                                               tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.PARLIAMENT_ITEM_ADD_DESCRIPTION, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <div class="" id='description_div_<%=i%>'>
                                                                <textarea
                                                                        class='form-control'
                                                                        name='description'
                                                                        id='description_textarea_<%=i%>'
                                                                        tag='pb_html'><%=actionName.equals("edit") ? (parliament_itemDTO.description) : ("")%>
                                                                    >
                                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.PARLIAMENT_ITEM_ADD_IMAGE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <div class="" id='image_div_<%=i%>'>
                                                <%
                                                    if (parliament_itemDTO.image != null) {
                                                        byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(parliament_itemDTO.image);
                                                        value = parliament_itemDTO.image + "";
                                                %>
                                                <img src='data:image/jpg;base64,<%=new String(encodeBase64)%>'
                                                     style='width:100px'>
                                                <a href='Parliament_itemServlet?actionType=downloadBlob&name=image&id=<%=parliament_itemDTO.iD%>'
                                                   download='<%=value%>'>Download</a>
                                                <%
                                                    }
                                                %>
                                                <input type='file' class='form-control' name='image'
                                                       id='image_blob_jpg_<%=i%>'
                                                       value=<%=actionName.equals("edit")?("'" + parliament_itemDTO.image + "'"):("'" + "" + "'")%>
                                                               tag='pb_html'/>


                                            </div>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + parliament_itemDTO.insertionDate + "'"):("'" + "0" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedBy'
                                           id='insertedBy_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + parliament_itemDTO.insertedBy + "'"):("'" + "" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='modifiedBy'
                                           id='modifiedBy_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + parliament_itemDTO.modifiedBy + "'"):("'" + "" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + parliament_itemDTO.isDeleted + "'"):("'" + "false" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + parliament_itemDTO.lastModificationTime + "'"):("'" + "0" + "'")%>
                                                   tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-actions text-right mt-3">
                            <button class="btn-sm shadow text-white border-0 cancel-btn" style="border-radius: 8px">
                                <%=LM.getText(LC.LAB_TEST_ADD_LAB_TEST_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.PARLIAMENT_ITEM_ADD_PARLIAMENT_ITEM_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">

    $(document).ready(() => {
        $(".cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    let i = 0;
    $(document).ready(function () {
        <%--$('#nameBng').mouseover( () => {--%>
        <%--    let object = {--%>
        <%--        'LANGUAGE' : '<%=Language%>',--%>
        <%--        'DATE_ID' : 'date_'+i,--%>
        <%--        'START_YEAR' : '2001',--%>
        <%--        'END_YEAR' : '2025'--%>
        <%--    };--%>
        <%--     createDatePicker('dynamic_date_div', object);--%>
        <%--     i++;--%>
        <%--});--%>
        dateTimeInit("<%=Language%>");
    });

    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Parliament_itemServlet");
    }

    function init(row) {


    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






