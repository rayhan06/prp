<%@page pageEncoding="UTF-8" %>

<%@page import="parliament_item.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.PARLIAMENT_ITEM_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_PARLIAMENT_ITEM;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Parliament_itemDTO parliament_itemDTO = (Parliament_itemDTO) request.getAttribute("parliament_itemDTO");
    CommonDTO commonDTO = parliament_itemDTO;
    String servletName = "Parliament_itemServlet";


    System.out.println("parliament_itemDTO = " + parliament_itemDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Parliament_itemDAO parliament_itemDAO = (Parliament_itemDAO) request.getAttribute("parliament_itemDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_nameEng'>
    <%
        value = parliament_itemDTO.nameEng + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_nameBng'>
    <%
        value = parliament_itemDTO.nameBng + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td>
    <button type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Parliament_itemServlet?actionType=view&ID=<%=parliament_itemDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Parliament_itemServlet?actionType=getEditPage&ID=<%=parliament_itemDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=parliament_itemDTO.iD%>'/></span>
    </div>
</td>
																						
											

