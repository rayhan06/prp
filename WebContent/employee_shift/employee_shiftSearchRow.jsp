<%@page pageEncoding="UTF-8" %>

<%@page import="employee_shift.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EMPLOYEE_SHIFT_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_EMPLOYEE_SHIFT;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Employee_shiftDTO employee_shiftDTO = (Employee_shiftDTO) request.getAttribute("employee_shiftDTO");
    CommonDTO commonDTO = employee_shiftDTO;
    String servletName = "Employee_shiftServlet";


    System.out.println("employee_shiftDTO = " + employee_shiftDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Employee_shiftDAO employee_shiftDAO = (Employee_shiftDAO) request.getAttribute("employee_shiftDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_employeeRecordsId'>

    <%=Employee_shiftDAO.getEmployeeText(Language, employee_shiftDTO.employeeRecordsId)%>
</td>


<td id='<%=i%>_employeeOfficesId'>
    <%=Office_unitsRepository.getInstance().geText(Language, employee_shiftDTO.employeeOfficesId)%>


</td>


<td id='<%=i%>_officeShiftDetailsId'>
    <%=Employee_shiftDAO.getShiftText(Language, employee_shiftDTO.officeShiftDetailsId)%>


</td>


<td id='<%=i%>_startDate'>
    <%
        value = employee_shiftDTO.startDate + "";
    %>
    <%
        String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_startDate, Language)%>


</td>


<td id='<%=i%>_endDate'>
    <%
        value = employee_shiftDTO.endDate + "";
    %>
    <%
        String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_endDate, Language)%>


</td>


<td>
	<button
			type="button"
			class="btn-sm border-0 shadow bg-light btn-border-radius"
			style="color: #ff6b6b;"
			onclick="location.href='Employee_shiftServlet?actionType=view&ID=<%=employee_shiftDTO.id%>'">
		<i class="fa fa-eye"></i>
	</button>
</td>

<td id='<%=i%>_Edit'>
	<button
			class="btn-sm border-0 shadow btn-border-radius text-white"
			style="background-color: #ff6b6b;"
			onclick="location.href='Employee_shiftServlet?actionType=getEditPage&ID=<%=employee_shiftDTO.id%>'"
	>
		<i class="fa fa-edit"></i>
	</button>
</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=employee_shiftDTO.id%>'/></span>
    </div>
</td>
																						
											

