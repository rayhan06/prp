<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="employee_shift.Employee_shiftDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Employee_shiftDTO employee_shiftDTO = (Employee_shiftDTO)request.getAttribute("employee_shiftDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(employee_shiftDTO == null)
{
	employee_shiftDTO = new Employee_shiftDTO();
	
}
System.out.println("employee_shiftDTO = " + employee_shiftDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.EMPLOYEE_SHIFT_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>

			
<%=("<td id = '" + i + "_id" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='id' id = 'id_hidden_<%=i%>' value='<%=employee_shiftDTO.id%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeRecordsId'>")%>
			
	
	<div class="form-inline" id = 'employeeRecordsId_div_<%=i%>'>
		<select class='form-control'  name='employeeRecordsId' id = 'employeeRecordsId_select2_<%=i%>'   tag='pb_html'>
			<option class='form-control'  value='0' <%=(actionName.equals("edit") && String.valueOf(employee_shiftDTO.employeeRecordsId).equals("0"))?("selected"):""%>>0<br>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeOfficesId'>")%>
			
	
	<div class="form-inline" id = 'employeeOfficesId_div_<%=i%>'>
		<select class='form-control'  name='employeeOfficesId' id = 'employeeOfficesId_select2_<%=i%>'   tag='pb_html'>
			<option class='form-control'  value='0' <%=(actionName.equals("edit") && String.valueOf(employee_shiftDTO.employeeOfficesId).equals("0"))?("selected"):""%>>0<br>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_officeShiftDetailsId'>")%>
			
	
	<div class="form-inline" id = 'officeShiftDetailsId_div_<%=i%>'>
		<select class='form-control'  name='officeShiftDetailsId' id = 'officeShiftDetailsId_select2_<%=i%>'   tag='pb_html'>
			<option class='form-control'  value='0' <%=(actionName.equals("edit") && String.valueOf(employee_shiftDTO.officeShiftDetailsId).equals("0"))?("selected"):""%>>0<br>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_overtimeApplicable" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='overtimeApplicable' id = 'overtimeApplicable_checkbox_<%=i%>' value='<%=employee_shiftDTO.overtimeApplicable%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_selfAttendanceEnabled" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='selfAttendanceEnabled' id = 'selfAttendanceEnabled_checkbox_<%=i%>' value='<%=employee_shiftDTO.selfAttendanceEnabled%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_startDate'>")%>
			
	
	<div class="form-inline" id = 'startDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'startDate_date_<%=i%>' name='startDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_startDate = dateFormat.format(new Date(employee_shiftDTO.startDate));
	%>
	'<%=formatted_startDate%>'
	<%
}
else
{
	%>
	''
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_endDate'>")%>
			
	
	<div class="form-inline" id = 'endDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'endDate_date_<%=i%>' name='endDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_endDate = dateFormat.format(new Date(employee_shiftDTO.endDate));
	%>
	'<%=formatted_endDate%>'
	<%
}
else
{
	%>
	''
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isActive" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isActive' id = 'isActive_checkbox_<%=i%>' value='<%=employee_shiftDTO.isActive%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=employee_shiftDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value='<%=employee_shiftDTO.insertedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=employee_shiftDTO.modifiedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + employee_shiftDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=employee_shiftDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_searchColumn" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=employee_shiftDTO.searchColumn%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Employee_shiftServlet?actionType=view&ID=<%=employee_shiftDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Employee_shiftServlet?actionType=view&modal=1&ID=<%=employee_shiftDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	