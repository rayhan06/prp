<%@page import="office_shift_details.Office_shift_detailsDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="employee_shift.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsDAO" %>
<%@ page import="workflow.WorkflowController" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    Employee_shiftDTO employee_shiftDTO;
    employee_shiftDTO = (Employee_shiftDTO) request.getAttribute("employee_shiftDTO");
    List<Office_shift_detailsDTO> officeShiftDetailsDTOs = (List<Office_shift_detailsDTO>) request.getAttribute("officeShiftDetailsDTOs");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (employee_shiftDTO == null) {
        employee_shiftDTO = new Employee_shiftDTO();
    }
    System.out.println("employee_shiftDTO = " + employee_shiftDTO);
    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.EMPLOYEE_SHIFT_ADD_EMPLOYEE_SHIFT_ADD_FORMNAME, loginDTO);
    Employee_recordsDTO employee_recordsDTO = null;
    if (actionName.equals("edit")) {
        employee_recordsDTO = new Employee_recordsDAO().getEmployee_recordsDTOByID(employee_shiftDTO.employeeRecordsId);
    }

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
%>
<%
    String Language = LM.getText(LC.EMPLOYEE_SHIFT_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    boolean isLanguageEnglish = Language.equals("English");
%>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i> &nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form"
              action="Employee_shiftServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <input type='hidden' class='form-control' name='id' id='id_hidden_<%=i%>'
                   value='<%=employee_shiftDTO.id%>' tag='pb_html'/>
            <div class="kt-portlet__body form-body mt-5">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 text-md-right col-form-label">
                                                <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_EMPLOYEERECORDSTYPE, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>

                                            <div class="col-md-9">
                                                <div  id='employeeRecords_div_<%=i%>'>


                                                    <input type='hidden' class='form-control'
                                                           name='employeeRecordsId' id='employeeRecordsId'
                                                           tag='pb_html'/>
                                                    <input type='hidden' class='form-control'
                                                           name='employeeOfficesId' id='employeeOfficesId'
                                                           tag='pb_html'/>
                                                    <button type="button"
                                                            class="btn btn-primary btn-block shadow btn-border-radius mb-3"
                                                            id="tagEmp_modal_button">
                                                        <%=LM.getText(LC.HM_SELECT, userDTO)%>
                                                    </button>
                                                    <table class="table table-bordered table-striped">
                                                        <thead></thead>

                                                        <tbody id="tagged_emp_table">
                                                        <tr style="display: none;">
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>
                                                                <button type="button"
                                                                        class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4"
                                                                        onclick="remove_containing_row(this,'tagged_emp_table');">
                                                                    <i class="fa fa-trash"></i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                        <% if (employee_recordsDTO != null) { %>
                                                        <tr>
                                                            <td><%=employee_recordsDTO.employeeNumber%>
                                                            </td>
                                                            <td><%=employee_recordsDTO.nameEng%>
                                                            </td>
                                                            <td>
                                                                <%
                                                                    EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employee_recordsDTO.iD);
                                                                    Office_unitsDTO officeUnitsDTO = null;
                                                                    if (employeeOfficeDTO != null) {
                                                                        officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
                                                                    }
                                                                %>
                                                                <%=officeUnitsDTO == null ? "" : HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng ? officeUnitsDTO.unitNameEng : officeUnitsDTO.unitNameBng%>

                                                            </td>
                                                            <td>
                                                                <button type="button"
                                                                        class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4"
                                                                        onclick="remove_containing_row(this,'tagged_emp_table');">
                                                                    <i class="fa fa-trash"></i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                        <%}%>
                                                        </tbody>
                                                    </table>

                                                    <%--<select class='form-control' name='employeeRecordsType' id='employeeRecordsType'
                                                            tag='pb_html'>

                                                        <%= actionName.equals("edit") ? Employee_recordsDAO.buildOptionsWithSingleEmployee(Language, employee_attendanceDTO.employeeRecordsType) : ""%>
                                                    </select>--%>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 text-md-right col-form-label">
                                                <%=LM.getText(LC.EMPLOYEE_SHIFT_ADD_OFFICESHIFTDETAILSID, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>

                                            <div class="col-md-9">
                                                <div  id='officeShiftDetailsId_div_<%=i%>'>
                                                    <select onchange="onSelectOfficeShift()"
                                                            class='form-control' name='officeShiftDetailsId'
                                                            id='officeShiftDetailsId'
                                                            tag='pb_html'>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 text-md-right col-form-label">
                                                <%=LM.getText(LC.EMPLOYEE_SHIFT_ADD_OVERTIMEAPPLICABLE, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                <div  id='overtimeApplicable_div_<%=i%>'>
                                                    <input type='checkbox'
                                                           class='form-control-sm mt-1' name='overtimeApplicable'
                                                           id='overtimeApplicable_checkbox_<%=i%>'
                                                           value='true' <%=(actionName.equals("edit") && employee_shiftDTO.overtimeApplicable==1)?("checked"):""%>
                                                           tag='pb_html'><br>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 text-md-right col-form-label">
                                                <%=LM.getText(LC.EMPLOYEE_SHIFT_ADD_SELFATTENDANCEENABLED, loginDTO)%>
                                            </label>

                                            <div class="col-md-9">
                                                <div  id='selfAttendanceEnabled_div_<%=i%>'>
                                                    <input type='checkbox'
                                                           class='form-control-sm mt-1' name='selfAttendanceEnabled'
                                                           id='selfAttendanceEnabled_checkbox_<%=i%>'
                                                           value='true' <%=(actionName.equals("edit") && employee_shiftDTO.selfAttendanceEnabled==1)?("checked"):""%>
                                                           tag='pb_html'><br>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 text-md-right col-form-label">
                                                <%=LM.getText(LC.EMPLOYEE_SHIFT_ADD_STARTDATE, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>

                                            <div class="col-md-9">
                                                <div  id='startDate_div_<%=i%>'>

                                                    <%--                        <input type='text' class='form-control formRequired ' readonly="readonly"--%>
                                                    <%--                               data-label="Document Date" id='start-date' name='startDate' tag='pb_html'>--%>
                                                    <jsp:include page="/date/date.jsp">
                                                        <jsp:param name="DATE_ID"
                                                                   value="start_date_js"></jsp:param>
                                                        <jsp:param name="LANGUAGE"
                                                                   value="<%=Language%>"></jsp:param>
                                                        <jsp:param name="START_YEAR"
                                                                   value="1971"></jsp:param>
                                                    </jsp:include>

                                                    <input type='hidden' class='form-control'
                                                           name='startDate' id='start-date' value=""
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 text-md-right col-form-label">
                                                <%=LM.getText(LC.EMPLOYEE_SHIFT_ADD_ENDDATE, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>

                                            <div class="col-md-9">
                                                <div  id='endDate_div_<%=i%>'>

                                                    <%--                        <input type='text' class='form-control formRequired datepicker' readonly="readonly"--%>
                                                    <%--                               data-label="Document Date" id='end-date' name='endDate'  tag='pb_html'>--%>
                                                    <jsp:include page="/date/date.jsp">
                                                        <jsp:param name="DATE_ID"
                                                                   value="end_date_js"></jsp:param>
                                                        <jsp:param name="LANGUAGE"
                                                                   value="<%=Language%>"></jsp:param>
                                                        <jsp:param name="START_YEAR"
                                                                   value="1971"></jsp:param>
                                                    </jsp:include>

                                                    <input type='hidden' class='form-control' name='endDate'
                                                           id='end-date' value="" tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 text-md-right col-form-label">
                                                <%=LM.getText(LC.EMPLOYEE_SHIFT_ADD_ISACTIVE, loginDTO)%>
                                            </label>

                                            <div class="col-9">
                                                <div  id='isActive_div_<%=i%>'>

                                                    <input type='checkbox'
                                                           class='form-control-sm mt-1' name='isActive'
                                                           id='isActive_checkbox_<%=i%>'
                                                           value='true' <%=(actionName.equals("edit") && employee_shiftDTO.isActive==1)?("checked"):""%>
                                                           tag='pb_html'><br>
                                                </div>
                                            </div>
                                        </div>
                                        <input type='hidden' class='form-control' name='insertionDate'
                                               id='insertionDate_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + employee_shiftDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='insertedBy'
                                               id='insertedBy_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + employee_shiftDTO.insertedBy + "'"):("'" + "" + "'")%> tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='modifiedBy'
                                               id='modifiedBy_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + employee_shiftDTO.modifiedBy + "'"):("'" + "" + "'")%> tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='isDeleted'
                                               id='isDeleted_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + employee_shiftDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
                                        <input type='hidden' class='form-control'
                                               name='lastModificationTime'
                                               id='lastModificationTime_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + employee_shiftDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='searchColumn'
                                               id='searchColumn_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + employee_shiftDTO.searchColumn + "'"):("'" + "" + "'")%> tag='pb_html'/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-11 text-right">
                        <a class="btn btn-sm cancel-btn text-white shadow"
                           href="<%=request.getHeader("referer")%>">
                            <%=LM.getText(LC.EMPLOYEE_SHIFT_ADD_EMPLOYEE_SHIFT_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button class="btn btn-sm submit-btn text-white shadow ml-2" type="submit">
                            <%=LM.getText(LC.EMPLOYEE_SHIFT_ADD_EMPLOYEE_SHIFT_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script type="text/javascript">
    var startDateMap = new Map();
    var endDateMap = new Map();

    $(document).ready(function () {
        loadMap();
        dateTimeInit("<%=Language%>");
        /*  if ($('#employeeOfficesId').val()) {
              fetchEmployee($('#employeeOfficesId').val(), '



        <%=Language%>', '



        <%=employee_shiftDTO.employeeRecordsId%>');
        }*/
        <%if(employee_recordsDTO!=null) {%>
        fetchShift('<%=employee_shiftDTO.employeeOfficesId%>', '<%=employee_shiftDTO.employeeRecordsId%>', '<%=Language%>', '<%=employee_shiftDTO.officeShiftDetailsId%>');
        <%} %>

        $.validator.addMethod('officeShiftValidator', function (value, element) {
            return value != 0;

        });
        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                officeShiftDetailsId: {
                    required: true,
                    officeShiftValidator: true
                }
            },
            messages: {
                officeShiftDetailsId: '<%=Language.equalsIgnoreCase("English") ? "Kindly enter shift!" : "অনুগ্রহ করে শিফট প্রদান করুন!"%>'
            }
        });

    });

    $('#bigform').on('submit', () => {
        $('#start-date').val(getDateStringById('start_date_js'));
        $('#end-date').val(getDateStringById('end_date_js'));

        return dateValidator('start_date_js', true) && dateValidator('end_date_js', true);
    });

    $('#start_date_js').on('datepicker.change', () => {
        setMinDateById('end_date_js', getDateStringById('start_date_js'));
    });

    function PreprocessBeforeSubmiting(row, validate) {

        let employeeRecordsId = added_employee_info_map.keys().next().value;

        <%if(employee_recordsDTO!=null) {%>
        employeeRecordsId = <%=employee_shiftDTO.employeeRecordsId%>;
        <%} %>

        document.getElementById('employeeRecordsId').value = +employeeRecordsId;

        if (!document.getElementById('employeeRecordsId').value || document.getElementById('employeeRecordsId').value == 'NaN') {
            if ("<%=Language%>" == 'English') {
                alert('Please select employee !');
            } else {
                alert('কর্মকর্তা নির্ধারণ করুন');
            }

            return false;
        }

        let officesId = added_employee_info_map.get(employeeRecordsId).officeUnitId;

        <%if(employee_recordsDTO!=null) {%>
        officesId = <%=employee_shiftDTO.employeeOfficesId%>;
        <%} %>

        document.getElementById('employeeOfficesId').value = +officesId;
        //added_employee_info_map.get("3204100").officeUnitId
        if (validate == "report") {
        }

        preprocessCheckBoxBeforeSubmitting('overtimeApplicable', row);
        preprocessCheckBoxBeforeSubmitting('selfAttendanceEnabled', row);
        preprocessCheckBoxBeforeSubmitting('isActive', row);

        return true;
    }

    function loadMap() {
        <%
        for(Office_shift_detailsDTO dto: officeShiftDetailsDTOs) {
            String dateStr = dateFormat.format(new Date(dto.startDate));%>
        startDateMap[<%=dto.id%>] = '<%=dateStr%>';
        <%dateStr = dateFormat.format(new Date(dto.endDate));%>
        endDateMap[<%=dto.id%>] = '<%=dateStr%>';
        <%}
        %>

    }

    function onSelectOfficeShift() {
        let val = $('#officeShiftDetailsId').val();

        setMinDateById('start_date_js', startDateMap[val]);
        setMaxDateById('start_date_js', endDateMap[val]);
        setMinDateById('end_date_js', startDateMap[val]);
        setMaxDateById('end_date_js', endDateMap[val]);
        // $("#start-date").datepicker('option', 'minDate', startDateMap[val]);
        // $("#start-date").datepicker('option', 'maxDate', endDateMap[val]);
        // $("#end-date").datepicker('option', 'minDate', startDateMap[val]);
        // $("#end-date").datepicker('option', 'maxDate', endDateMap[val]);
        console.log(val);
    }

    $(function () {

        // $("#start-date").datepicker({
        //     dateFormat: 'dd/mm/yy',
        //     yearRange: '1900:2100',
        //     changeYear: true,
        //     changeMonth: true,
        //     buttonText: "<i class='fa fa-calendar'></i>",
        //     onSelect: function (dateText) {
        //         $("#end-date").datepicker('option', 'minDate', dateText);
        //     }
        // });
        // $("#end-date").datepicker({
        //     dateFormat: 'dd/mm/yy',
        //     yearRange: '1900:2100',
        //     changeYear: true,
        //     changeMonth: true,
        //     buttonText: "<i class='fa fa-calendar'></i>"
        // });


        //    $("#leave-start-date").datepicker('option', 'minDate', new Date());
        //   $("#leave-end-date").datepicker('option', 'minDate', new Date());

        $("#start-date").val('');
        $("#end-date").val('');

        <%
     if(actionName.equals("edit")) {
    	String startDate= employee_shiftDTO.startDate > SessionConstants.MIN_DATE ? dateFormat.format(new Date(employee_shiftDTO.startDate)) : "";
    	String endDate = employee_shiftDTO.endDate > SessionConstants.MIN_DATE ? dateFormat.format(new Date(employee_shiftDTO.endDate)) : "";
    	%>
        setDateByStringAndId('start_date_js', '<%=startDate%>');
        setDateByStringAndId('end_date_js', '<%=endDate%>');
        <%}
        %>
    });

    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Employee_shiftServlet");
    }

    function init(row) {

        $("#employeeRecordsId").select2({
            dropdownAutoWidth: true
        });

        $("#employeeOfficesId").select2({
            dropdownAutoWidth: true
        });

        $("#officeShiftDetailsId").select2({
            dropdownAutoWidth: true
        });

    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;


    /*    function fetchEmployee(officeUnitId, language, selectedId) {
            let url = "Employee_shiftServlet?actionType=getEmployeeName&office_unit_id=" + officeUnitId + "&language=" + language + "&selectedId=" + selectedId;
            console.log("url : " + url);
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    console.log(fetchedData);
                    document.getElementById('employeeRecordsId').innerHTML = fetchedData;
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }*/

    function fetchShift(officeUnitId, employeeRecordId, language, selectedId) {
        let url = "Employee_shiftServlet?actionType=getShiftName&office_unit_id=" + officeUnitId + "&language=" + language + "&selectedId=" + selectedId + "&employeeRecordId=" + employeeRecordId;
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                console.log(fetchedData);
                document.getElementById('officeShiftDetailsId').innerHTML = fetchedData;
                $("#start-date").val('');
                $("#end-date").val('');
            },
            error: function (error) {
                console.log(error);
            }
        });
    }


    /*    $("#employeeOfficesId").change(function () {
<%--fetchEmployee($('#employeeOfficesId').val(), '<%=Language%>', '<%=employee_shiftDTO.employeeRecordsId%>');--%>
        <%--fetchShift($('#employeeOfficesId').val(), '<%=Language%>', '<%=employee_shiftDTO.officeShiftDetailsId%>');--%>
    });*/

    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button

    // map to store and send added employee data
    added_employee_info_map = new Map();

    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_emp_table', {
                info_map: added_employee_info_map,
                isSingleEntry: true,
                callBackFunction: function (data) {
                    console.log(data);
                    fetchShift(data.officeUnitId, data.employeeRecordId, '<%=Language%>', '<%=employee_shiftDTO.officeShiftDetailsId%>');
                }
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagEmp_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_emp_table';
        $('#search_emp_modal').modal();
    });


    function remove_containing_row(button, table_name) {
        let containing_row = button.parentNode.parentNode;
        let containing_table = containing_row.parentNode;
        containing_table.deleteRow(containing_row.rowIndex);

        // button id = "<employee record id>_button"
        let td_button = button.parentNode;
        let employee_record_id = td_button.id.split("_")[0];

        let added_info_map = table_name_to_collcetion_map.get(table_name).info_map;
        console.log("delete (employee_record_id)");
        added_info_map.delete(employee_record_id);
        console.log(added_info_map);
    }
</script>






