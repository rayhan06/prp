<%@page import="office_units.Office_unitsRepository" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="employee_shift.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.EMPLOYEE_SHIFT_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Employee_shiftDAO employee_shiftDAO = new Employee_shiftDAO("employee_shift");
    Employee_shiftDTO employee_shiftDTO = employee_shiftDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<%--
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">
		<h3 class="kt-subheader__title"> Asset Management </h3>
	</div>
</div>

<!-- end:: Subheader -->--%>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=LM.getText(LC.EMPLOYEE_SHIFT_ADD_EMPLOYEE_SHIFT_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>

        <div class="kt-portlet__body form-body">
            <div>
                <h5 class="table-title">
                    <%=LM.getText(LC.EMPLOYEE_SHIFT_ADD_EMPLOYEE_SHIFT_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.EMPLOYEE_SHIFT_ADD_EMPLOYEERECORDSID, loginDTO)%>
                                </b></td>
                            <td>

                                <%=Employee_shiftDAO.getEmployeeText(Language, employee_shiftDTO.employeeRecordsId)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.EMPLOYEE_SHIFT_ADD_EMPLOYEEOFFICESID, loginDTO)%>
                                </b></td>
                            <td>

                                <%=Office_unitsRepository.getInstance().geText(Language, employee_shiftDTO.employeeOfficesId)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.EMPLOYEE_SHIFT_ADD_OFFICESHIFTDETAILSID, loginDTO)%>
                                </b></td>
                            <td>

                                <%=Employee_shiftDAO.getShiftText(Language, employee_shiftDTO.officeShiftDetailsId)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_SHIFT_ADD_STARTDATE, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = employee_shiftDTO.startDate + "";
                                %>
                                <%
                                    String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_startDate, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_SHIFT_ADD_ENDDATE, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = employee_shiftDTO.endDate + "";
                                %>
                                <%
                                    String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_endDate, Language)%>


                            </td>

                        </tr>


                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
