<%@page contentType="text/html;charset=utf-8" %>
<%@page import="office_shift.Office_shiftRepository" %>
<%@page import="employee_records.Employee_recordsDAO" %>
<%@page import="office_units.Office_unitsRepository" %>
<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>

<%
    System.out.println("Inside nav.jsp");
    String url = request.getParameter("url");
    String navigator = request.getParameter("navigator");
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";
    String pageno = "";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String[][] searchFieldInfo = rn.getSearchFieldInfo();
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";
    int row = 0;

    String Language = LM.getText(LC.EMPLOYEE_SHIFT_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    int pagination_number = 0;
    boolean isPermanentTable = rn.m_isPermanentTable;
    System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    boolean isLanguageEnglish = Language.equals("English");
%>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <%
                    out.println("<input placeholder='অনুসন্ধান করুন' autocomplete='off' type='text' class='form-control border-0' onKeyUp='allfield_changed(\"\",0)' id='anyfield'  name='" + LM.getText(LC.ASSET_MANUFACTURER_SEARCH_ANYFIELD, loginDTO) + "' ");
                    String value = (String) session.getAttribute(searchFieldInfo[searchFieldInfo.length - 1][1]);

                    if (value != null) {
                        out.println("value = '" + value + "'");
                    } else {
                        out.println("value=''");
                    }

                    out.println("/><br />");
                %>
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
<%--        <div class="kt-portlet__head-toolbar">--%>
<%--            <div class="kt-portlet__head-group">--%>
<%--                <div class="tooltip tooltip-portlet tooltip bs-tooltip-top" role="tooltip" id="tooltip_p6zf7aqcpv"--%>
<%--                     aria-hidden="true" x-placement="top"--%>
<%--                     style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">--%>
<%--                    <div class="tooltip-arrow arrow" style="left: 34px;"></div>--%>
<%--                    <div class="tooltip-inner">Collapse</div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=LM.getText(LC.EMPLOYEE_SHIFT_ADD_EMPLOYEERECORDSID, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius mb-3"
                                    id="tagEmp_modal_button">
                                <%=LM.getText(LC.HM_SELECT, userDTO)%>
                            </button>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead></thead>
                                    <tbody id="tagged_emp_table">
                                    <tr style="display: none;">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <button type="button" class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4"
                                                    onclick="remove_containing_row(this,'tagged_emp_table');">
                                                <i class="fa fa-trash mll-1"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=LM.getText(LC.EMPLOYEE_SHIFT_ADD_OFFICESHIFTDETAILSID, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='office_shift_details_id' id='office_shift_details_id'
                                    tag='pb_html'>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=LM.getText(LC.EMPLOYEE_SHIFT_ADD_STARTDATE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="start_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                <jsp:param name="START_YEAR" value="1971"></jsp:param>
                            </jsp:include>

                            <input type='hidden' class='form-control' name='start_date' id='start_date' value=""
                                   tag='pb_html'/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=LM.getText(LC.EMPLOYEE_SHIFT_ADD_ENDDATE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="end_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                <jsp:param name="START_YEAR" value="1971"></jsp:param>
                            </jsp:include>

                            <input type='hidden' class='form-control' name='end_date' id='end_date' value=""
                                   tag='pb_html'/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit" class="btn shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4; color: white; border-radius: 6px!important; border-radius: 8px;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
        <div>
            <jsp:include page="../employee_assign/employeeSearchModal.jsp">
                <jsp:param name="isHierarchyNeeded" value="false"/>
            </jsp:include>
        </div>
    </div>
</div>

<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>

<script type="text/javascript">

    $('#start_date_js').on('datepicker.change', () => {
        setSearchChanged();
        setMinDateById('end_date_js', getDateStringById('start_date_js'));
    });

    $('#end_date_js').on('datepicker.change', () => {
        setSearchChanged();
    });

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function init() {

        $("#employee_offices_id").select2({
            dropdownAutoWidth: true,
            theme: "classic",
            width: '100%'
        });
        $("#employee_records_id").select2({
            dropdownAutoWidth: true,
            theme: "classic",
            width: '100%'
        });
        $("#office_shift_details_id").select2({
            dropdownAutoWidth: true,
            theme: "classic",
            width: '100%'
        });


    }

    window.onload = function () {
        init();
    }

    function allfield_changed(go, pagination_number) {
        $('#start_date').val(getDateStringById('start_date_js'));
        $('#end_date').val(getDateStringById('end_date_js'));

        var params = 'AnyField=' + document.getElementById('anyfield').value;

        params += '&start_date=' + getBDFormattedDate('start_date');
        params += '&end_date=' + getBDFormattedDate('end_date');
        //params += '&employee_offices_id=' + document.getElementById('employee_offices_id').value;
        //params += '&employee_records_id=' + document.getElementById('employee_records_id').value;
        params += '&office_shift_details_id=' + document.getElementById('office_shift_details_id').value;

        let employeeRecordsId = added_employee_info_map.keys().next().value;
        if (employeeRecordsId)
            params += '&employee_records_id=' + employeeRecordsId;
        //	if(added_employee_info_map.size>0)
        //	params += '&employee_offices_id=' + added_employee_info_map.get(employeeRecordsId).officeUnitId;
        //document.getElementById('employeeRecordsId').value = +employeeRecordsId;
        //document.getElementById('employeeOfficesId').value = added_employee_info_map.get(employeeRecordsId).officeUnitId;

        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

    function fetchShift(officeUnitId, language, selectedId) {
        let url = "Employee_shiftServlet?actionType=getShiftName&office_unit_id=" + officeUnitId + "&language=" + language + "&selectedId=" + selectedId;
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                console.log(fetchedData);
                document.getElementById('office_shift_details_id').innerHTML = fetchedData;
                $("#start-date").val('');
                $("#end-date").val('');
            },
            error: function (error) {
                console.log(error);
            }
        });
    }


    $("#employee_offices_id").change(function () {
        fetchShift($('#employee_offices_id').val(), '<%=Language%>', '');
    });

    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button

    // map to store and send added employee data
    added_employee_info_map = new Map();

    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_emp_table', {
                info_map: added_employee_info_map,
                isSingleEntry: true,
                callBackFunction: function (data) {
                    console.log(data);
                    fetchShift(data.officeUnitId, '<%=Language%>', '');
                }
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagEmp_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_emp_table';
        $('#search_emp_modal').modal();
    });


    function remove_containing_row(button, table_name) {
        let containing_row = button.parentNode.parentNode;
        let containing_table = containing_row.parentNode;
        containing_table.deleteRow(containing_row.rowIndex);

        // button id = "<employee record id>_button"
        let td_button = button.parentNode;
        let employee_record_id = td_button.id.split("_")[0];

        let added_info_map = table_name_to_collcetion_map.get(table_name).info_map;
        console.log("delete (employee_record_id)");
        added_info_map.delete(employee_record_id);
        console.log(added_info_map);
    }
</script>

