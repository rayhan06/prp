<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@ page import="util.HttpRequestUtils" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.ROOMWISE_TICKET_COUNT_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row">
    <div class="col-12">
		
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=isLangEng ? "Room" : "কক্ষ"%>

				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='roomNoCat_1' id = 'roomNoCat_1' >		
						<%		
						Options = CatDAO.getOptions(Language, "room_no", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
        <%@include file="../pbreport/yearmonth.jsp"%>
        <%@include file="../pbreport/calendar.jsp"%>
    </div>
</div>
<script type="text/javascript">
function init()
{
    dateTimeInit($("#Language").val());
    add1WithEnd = true;
}
function PreprocessBeforeSubmiting()
{
}
</script>