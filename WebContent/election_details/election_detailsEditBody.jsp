<%@page import="login.LoginDTO" %>
<%@page import="election_details.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    String actionName;
    Election_detailsDTO election_detailsDTO;
    int year = Calendar.getInstance().get(Calendar.YEAR);
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        election_detailsDTO = (Election_detailsDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    } else {
        actionName = "add";
        election_detailsDTO = new Election_detailsDTO();
    }
    String formTitle = LM.getText(LC.ELECTION_DETAILS_ADD_ELECTION_DETAILS_ADD_FORMNAME, loginDTO);
    int i = 0;
    long ColumnID;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i><%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal kt-form" id="bigform" name="bigform" enctype="multipart/form-data">
            <!-- FORM BODY SKULL -->
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="parliamentNumber_text_<%=i%>">
                                            <%=LM.getText(LC.ELECTION_DETAILS_ADD_PARLIAMENTNUMBER, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8" id='parliamentNumber_div_<%=i%>'>
                                            <input type='text' class='form-control' name='parliamentNumber'
                                                   id='parliamentNumber_text_<%=i%>' required tag='pb_html'
                                                   value='<%=election_detailsDTO.parliamentNumber>0?election_detailsDTO.parliamentNumber:""%>'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="electionDate_date_<%=i%>">
                                            <%=LM.getText(LC.ELECTION_DETAILS_ADD_ELECTIONDATE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8" id='electionDate_div_<%=i%>'>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="electionDate_date_js"/>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                            </jsp:include>
                                        </div>
                                        <input type='hidden'
                                               class='form-control'
                                               id='electionDate_date_<%=i%>'
                                               name='electionDate'
                                               value='' tag='pb_html'/>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="gazetteDate_date_<%=i%>">
                                            <%=LM.getText(LC.ELECTION_DETAILS_ADD_GAZETTEDATE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8" id='gazetteDate_div_<%=i%>'>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="gazetteDate_date_js"/>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                            </jsp:include>
                                        </div>
                                        <input type='hidden'
                                               class='form-control'
                                               id='gazetteDate_date_<%=i%>'
                                               name='gazetteDate'
                                               value='' tag='pb_html'/>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="generralOathDate_date_<%=i%>">
                                            <%=LM.getText(LC.ELECTION_DETAILS_ADD_GENERRALOATHDATE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8" id='generralOathDate_div_<%=i%>'>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="generralOathDate_date_js"/>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                            </jsp:include>
                                        </div>
                                        <input type='hidden'
                                               class='form-control'
                                               id='generralOathDate_date_<%=i%>'
                                               name='generralOathDate'
                                               value='' tag='pb_html'/>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="parliamentLastDate_date_<%=i%>">
                                            <%=LM.getText(LC.ELECTION_DETAILS_ADD_PARLIAMENTLASTDATE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8" id='parliamentLastDate_div_<%=i%>'>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="parliamentLastDate_date_js"/>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                                <jsp:param name="END_YEAR" value="<%=year + 5%>"/>
                                            </jsp:include>
                                        </div>
                                        <input type='hidden'
                                               class='form-control'
                                               id='parliamentLastDate_date_<%=i%>'
                                               name='parliamentLastDate'
                                               value='' tag='pb_html'/>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.ELECTION_DETAILS_ADD_FILESDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8" id='filesDropzone_div_<%=i%>'>
                                            <%
                                                if (actionName.equals("edit")) {
                                                    List<FilesDTO> filesDropzoneDTOList = new FilesDAO().getMiniDTOsByFileID(election_detailsDTO.filesDropzone);
                                            %>
                                            <table>
                                                <tr>
                                                    <%
                                                        if (filesDropzoneDTOList != null) {
                                                            for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                                                FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                                                byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                    %>
                                                    <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                                        <%
                                                            if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                        %>
                                                        <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                             style='width:100px'/>
                                                        <%
                                                            }
                                                        %>
                                                        <a href='Election_detailsServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                           download><%=filesDTO.fileTitle%>
                                                        </a>
                                                        <a class='btn btn-danger'
                                                           onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>
                                                    </td>
                                                    <%
                                                            }
                                                        }
                                                    %>
                                                </tr>
                                            </table>
                                            <%
                                                }
                                            %>

                                            <%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>
                                            <div class="dropzone"
                                                 action="Election_detailsServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?election_detailsDTO.filesDropzone:ColumnID%>">
                                                <input type='file' style="display:none" name='filesDropzoneFile'
                                                       id='filesDropzone_dropzone_File_<%=i%>' tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='filesDropzoneFilesToDelete'
                                                   id='filesDropzoneFilesToDelete_<%=i%>'
                                                   value='' tag='pb_html'/>
                                            <input type='hidden' name='filesDropzone'
                                                   id='filesDropzone_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=actionName.equals("edit")?election_detailsDTO.filesDropzone:ColumnID%>'/>
                                        </div>
                                    </div>
                                    <!-- Hidden Fields -->
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=election_detailsDTO.iD%>' tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                            <%=LM.getText(LC.ELECTION_DETAILS_ADD_ELECTION_DETAILS_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                type="submit" onclick="submitForm()">
                            <%=LM.getText(LC.ELECTION_DETAILS_ADD_ELECTION_DETAILS_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    const form = $("#bigform");

    $(document).ready(function () {
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
        dateTimeInit("<%=Language%>");
        <%if (actionName.equals("edit")) {%>
            setDateByTimestampAndId('electionDate_date_js', <%=election_detailsDTO.electionDate%>);
            setDateByTimestampAndId('generralOathDate_date_js', <%=election_detailsDTO.generralOathDate%>);
            setDateByTimestampAndId('gazetteDate_date_js', <%=election_detailsDTO.gazetteDate%>);
            setDateByTimestampAndId('parliamentLastDate_date_js', <%=election_detailsDTO.parliamentLastDate%>);
        <%}%>
    });

    function PreprocessBeforeSubmiting() {
        $('#electionDate_date_0').val(getDateStringById('electionDate_date_js'));
        $('#generralOathDate_date_0').val(getDateStringById('generralOathDate_date_js'));
        $('#gazetteDate_date_0').val(getDateStringById('gazetteDate_date_js'));
        $('#parliamentLastDate_date_0').val(getDateStringById('parliamentLastDate_date_js'));

        const datesValid = [
            dateValidator('electionDate_date_js', true),
            dateValidator('generralOathDate_date_js', true),
            dateValidator('gazetteDate_date_js', true),
            dateValidator('parliamentLastDate_date_js', true)
        ]
        return datesValid.every(cond => cond === true);
    }

    function submitForm(){
        buttonStateChange(true);
        console.log(form.serialize());
        if(PreprocessBeforeSubmiting()){
            $.ajax({
                type : "POST",
                url : "Election_detailsServlet?actionType=ajax_<%=actionName%>",
                data : form.serialize(),
                dataType : 'JSON',
                success : function(response) {
                    if(response.responseCode === 0){
                        $('#toast_message').css('background-color','#ff6063');
                        showToastSticky(response.msg,response.msg);
                        buttonStateChange(false);
                    }else if(response.responseCode === 200){
                        window.location.replace(getContextPath()+response.msg);
                    }
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        }else{
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value){
        $('#submit-btn').prop('disabled',value);
        $('#cancel-btn').prop('disabled',value);
    }
</script>