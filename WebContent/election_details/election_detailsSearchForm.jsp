<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="election_details.*" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.List" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="com.google.gson.Gson" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    RecordNavigator rn2 = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    String context = "../../.." + request.getContextPath();
    Map<Integer, Election_detailsDTO> mapByRowId = new HashMap<>();
%>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.ELECTION_DETAILS_ADD_PARLIAMENTNUMBER, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.ELECTION_DETAILS_ADD_ELECTIONDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.ELECTION_DETAILS_ADD_GENERRALOATHDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.ELECTION_DETAILS_ADD_GAZETTEDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.ELECTION_DETAILS_ADD_PARLIAMENTLASTDATE, loginDTO)%>
            </th>
            <th><%=isLangEng ? "Tenure of Parliament" : "সংসদের মেয়াদ"%>
            </th>
            <th><%=isLangEng ? "Status" : "অবস্থা" %>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.ELECTION_DETAILS_SEARCH_ELECTION_DETAILS_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="text-center">
                <span><%="English".equalsIgnoreCase(Language) ? "All" : "সকল"%></span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody class="text-nowrap">
        <%
            List<Election_detailsDTO> data = (List<Election_detailsDTO>) rn2.list;
            if (data != null && data.size() > 0) {
                Election_detailsDTO runningElectionDetailsDTO = Election_detailsRepository.getInstance().getRunningElectionDetailsDTO();
                for (int i = 0; i < data.size(); i++) {
                    Election_detailsDTO election_detailsDTO = data.get(i);
                    if (election_detailsDTO.parliamentStatusCat == ElectionDetailsStatusEnum.UPCOMING.getValue()) {
                        mapByRowId.put(i, election_detailsDTO);
                    }
        %>
        <tr id="row_<%=i%>">
            <%@include file="election_detailsSearchRow.jsp" %>
        </tr>
        <%
                }
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>
<script src="<%=context%>/assets/scripts/util1.js"></script>
<script>
    let isLangEng = <%=isLangEng%>;
    let cancelBtnText = isLangEng ? 'CANCEL' : 'বাতিল';
    let submitBtnText = isLangEng ? 'SUBMIT' : 'সাবমিট';
    let upcomingArrays;
    <% if(mapByRowId.size()>0){ %>
    upcomingArrays = JSON.parse('<%=new Gson().toJson(mapByRowId)%>');
    console.log('upcomingArrays : ' + upcomingArrays);
    <% }else{ %>
    upcomingArrays = {};
    <% } %>

    function activeParliament(i, id, parliament) {
        const activeMsg = isLangEng ? 'Do you want to active the parliament-' + parliament + '?'
            : 'আপনি কি সংসদ-' + convertNumberToBangla(parliament + '') + ' সক্রিয় করতে চান?';
        messageDialog('', activeMsg, 'success', true, submitBtnText, cancelBtnText, () => {
            buttonStateChange(true);
            $.ajax({
                type: "POST",
                url: "Election_detailsServlet?actionType=active",
                data: {'id': id},
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        $('#col_status_' + i)[0].innerHTML = '<button type="button" class="btn btn-sm border-0 btn-success shadow election_btn" ' +
                            ' style="color: white; border-radius: 8px; background-color: <%=ElectionDetailsStatusEnum.RUNNING.getColor()%>" ' +
                            ' onclick="closeParliament(' + i + ',' + id + ',' + upcomingArrays[i].parliamentNumber + ')">' +
                            '<%=isLangEng?ElectionDetailsStatusEnum.RUNNING.getEngText():ElectionDetailsStatusEnum.RUNNING.getBngText()%>' +
                            ' </button>';
                        delete upcomingArrays[i];
                        for (let item in upcomingArrays) {
                            $('#col_status_' + item)[0].innerHTML = '<span class="btn btn-sm border-0 shadow" ' +
                                ' style="background-color: <%=ElectionDetailsStatusEnum.UPCOMING.getColor()%>; color: white; border-radius: 8px;cursor: text"> ' +
                                '<%=isLangEng?ElectionDetailsStatusEnum.UPCOMING.getEngText():ElectionDetailsStatusEnum.UPCOMING.getBngText()%>' +
                                ' </span>'
                        }
                        buttonStateChange(false);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        }, () => {
        });
    }

    function closeParliament(i, id, parliament) {
        const activeMsg = isLangEng ? 'Do you want to close the parliament-' + parliament + '?'
            : 'আপনি কি সংসদ-' + convertNumberToBangla(parliament + '') + ' নিষ্ক্রিয় করতে চান?';
        messageDialog('', activeMsg, 'success', true, submitBtnText, cancelBtnText, () => {
            buttonStateChange(true);
            $.ajax({
                type: "POST",
                url: "Election_detailsServlet?actionType=closed",
                data: {'id': id},
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        $('#col_status_' + i)[0].innerHTML = '<span class="btn btn-sm border-0 shadow"' +
                            'style="background-color: <%=ElectionDetailsStatusEnum.CLOSED.getColor()%>; color: white; border-radius: 8px;cursor: text">' +
                            '<%=isLangEng?ElectionDetailsStatusEnum.CLOSED.getEngText():ElectionDetailsStatusEnum.CLOSED.getBngText()%>' + '</span>';
                        for (let item in upcomingArrays) {
                            let val = upcomingArrays[item];
                            $('#col_status_' + item)[0].innerHTML = '<button type="button" class="btn btn-sm border-0 btn-success shadow election_btn" ' +
                                ' style="color: white; border-radius: 8px; background-color: ' + '<%=ElectionDetailsStatusEnum.UPCOMING.getColor()%>' + ' "' +
                                ' onclick="activeParliament(' + item + ',' + val.iD + ',' + val.parliamentNumber + ')">' +
                                '<%=isLangEng?ElectionDetailsStatusEnum.UPCOMING.getEngText():ElectionDetailsStatusEnum.UPCOMING.getBngText()%>'
                                + '</button>';
                        }
                        buttonStateChange(false);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        }, () => {
        });
    }

    function buttonStateChange(flag) {
        $('.election_btn').prop('disabled', flag);
    }
</script>