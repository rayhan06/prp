<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="election_details.*" %>
<%@page import="java.util.*" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="files.*" %>
<%@page import="util.StringUtils" %>
<%@page import="util.HttpRequestUtils" %>
<%@page import="common.BaseServlet" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    Election_detailsDTO election_detailsDTO = (Election_detailsDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.ELECTION_DETAILS_ADD_ELECTION_DETAILS_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row px-4 px-md-0">
                            <div class="col-md-10 offset-md-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background-color: #FFFFFF">
                                            <%=LM.getText(LC.ELECTION_DETAILS_ADD_ELECTION_DETAILS_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped text-nowrap">
                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.ELECTION_DETAILS_ADD_PARLIAMENTNUMBER, loginDTO)%>
                                                </b></td>
                                            <td>
                                                <%="English".equalsIgnoreCase(Language) ? election_detailsDTO.parliamentNumber : String.valueOf(election_detailsDTO.parliamentNumber)%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.ELECTION_DETAILS_ADD_ELECTIONDATE, loginDTO)%>
                                                </b></td>
                                            <td>
                                                <%=StringUtils.getFormattedDate(Language,election_detailsDTO.electionDate)%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.ELECTION_DETAILS_ADD_GENERRALOATHDATE, loginDTO)%>
                                                </b></td>
                                            <td>
                                                <%=StringUtils.getFormattedDate(Language,election_detailsDTO.generralOathDate)%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.ELECTION_DETAILS_ADD_GAZETTEDATE, loginDTO)%>
                                                </b></td>
                                            <td>
                                                <%=StringUtils.getFormattedDate(Language,election_detailsDTO.gazetteDate)%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.ELECTION_DETAILS_ADD_PARLIAMENTLASTDATE, loginDTO)%>
                                                </b></td>
                                            <td>
                                                <%=StringUtils.getFormattedDate(Language,election_detailsDTO.parliamentLastDate)%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.ELECTION_DETAILS_ADD_FILESDROPZONE, loginDTO)%>
                                                </b></td>
                                            <td>
                                                <%
                                                    {
                                                        List<FilesDTO> FilesDTOList = new FilesDAO().getMiniDTOsByFileID(election_detailsDTO.filesDropzone);
                                                %>
                                                <table>
                                                    <tr>
                                                        <%
                                                            if (FilesDTOList != null && FilesDTOList.size()>0) {
                                                                for (FilesDTO filesDTO : FilesDTOList) {
                                                                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                        %>
                                                        <td>
                                                            <%
                                                                if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                            %>
                                                            <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                                 style='width:100px'/>
                                                            <%
                                                                }
                                                            %>
                                                            <a href='Election_detailsServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                               download><%=filesDTO.fileTitle%>
                                                            </a>
                                                        </td>
                                                        <%
                                                                }
                                                            }
                                                        %>
                                                    </tr>
                                                </table>
                                                <%
                                                    }
                                                %>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>