<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="bill_management.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoRepository" %>
<%@ page import="util.StringUtils" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@ page import="supplier_institution.Supplier_institutionRepository" %>
<%@ page import="budget_register.Budget_registerDTO" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="static util.StringUtils.convertBanglaIfLanguageIsBangla" %>
<%@ page import="static util.StringUtils.getFormattedDate" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@page contentType="text/html;charset=utf-8" %>

<%
    String navigator2 = "navBILL_MANAGEMENT";
    String servletName = "Bill_managementServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=getDataByLanguage(Language, "অর্থ বছর", "Economic Year")%></th>
            <th><%=getDataByLanguage(Language, "বাজেট অপারেশন কোড", "Budget Operation Code")%></th>
            <th><%=getDataByLanguage(Language, "অর্থনৈতিক কোড", "Economic Code")%></th>
            <th><%=getDataByLanguage(Language, "তারিখ", "Date")%></th>
            <th><%=getDataByLanguage(Language, "ভাউচার নম্বর", "Voucher Number")%></th>
            <th><%=getDataByLanguage(Language, "প্রাপকের নাম", "Recipient Name")%></th>
            <th><%=getDataByLanguage(Language, "বিলের বিবরণ", "Bill Description")%></th>
            <th><%=getDataByLanguage(Language, "বিলের টাকার পরিমাণ", "Bill Amount")%></th>
            <th><%=getDataByLanguage(Language, "বাজেট বরাদ্দ", "Allocated Budget")%></th>
            <th><%=getDataByLanguage(Language, "মোট খরচ", "Total Cost")%></th>
            <th><%=getDataByLanguage(Language, "অবশিষ্ট", "Remaining")%></th>
        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Budget_registerDTO> data = (List<Budget_registerDTO>) recordNavigator.list;
            try {
                if (data != null) {
                    for (Budget_registerDTO budgetRegisterDTO : data) {
        %>
                        <tr>
                            <td>
                                <%=BudgetSelectionInfoRepository.getInstance().getEconomicYearById(Language, budgetRegisterDTO.budgetSelectionInfoId)%>
                            </td>
                            <td>
                                <%=Budget_mappingRepository.getInstance().getCode(Language, budgetRegisterDTO.budgetMappingId)%>
                            </td>
                            <td>
                                <%=Economic_sub_codeRepository.getInstance().getCode(budgetRegisterDTO.economicSubCodeId, Language)%>
                            </td>
                            <td>
                                <%=getFormattedDate(Language, budgetRegisterDTO.insertionTime)%>
                            </td>
                            <td>
                                <%=convertBanglaIfLanguageIsBangla(
                                        Language,
                                        String.format("%d", budgetRegisterDTO.iD)
                                )%>
                            </td>
                            <td>
                                <%=budgetRegisterDTO.recipientName%>
                            </td>
                            <td style="max-width: 30%;">
                                <div class="text-center">
                                    <div style="display: inline-block; border-bottom: 1px solid black;">
                                        <%=getDataByLanguage(Language, "নং", "No")%>&nbsp;
                                        <%=convertBanglaIfLanguageIsBangla(Language, budgetRegisterDTO.issueNumber)%>&nbsp;
                                        <%=getDataByLanguage(Language, "তাং", "Date")%>&nbsp;
                                        <%=getFormattedDate(Language, budgetRegisterDTO.issueDate)%>
                                    </div>
                                </div>
                                <%=budgetRegisterDTO.description%>
                            </td>
                            <td>
                                <%=BangladeshiNumberFormatter.getFormattedNumber(convertBanglaIfLanguageIsBangla(
                                        Language,
                                        String.format("%d", budgetRegisterDTO.billAmount)
                                ))%>/-
                            </td>
                            <td>
                                <%=BangladeshiNumberFormatter.getFormattedNumber(convertBanglaIfLanguageIsBangla(
                                        Language,
                                        String.format("%d", budgetRegisterDTO.allocatedBudget)
                                ))%>/-
                            </td>
                            <td>
                                <%=BangladeshiNumberFormatter.getFormattedNumber(convertBanglaIfLanguageIsBangla(
                                        Language,
                                        String.format("%d", budgetRegisterDTO.totalCost)
                                ))%>/-
                            </td>
                            <td>
                                <%=BangladeshiNumberFormatter.getFormattedNumber(convertBanglaIfLanguageIsBangla(
                                        Language,
                                        String.format("%d", budgetRegisterDTO.remainingBudget)
                                ))%>/-
                            </td>
                        </tr>
        <%
                    }
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>">
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>">
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>">
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>">
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>">