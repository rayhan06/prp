<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoRepository" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoDTO" %>
<%@ page import="budget.BudgetUtils" %>
<%@ page import="budget_operation.Budget_operationRepository" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="bill_management.Bill_managementDAO" %>
<%@ page import="supplier_institution.Supplier_institutionRepository" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="economic_code.Economic_codeRepository" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@page contentType="text/html;charset=utf-8" %>

<%
    System.out.println("Inside nav.jsp");
    String url = "Budget_registerServlet?actionType=search";
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1">
    <div class="kt-portlet__body">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="budget_selection_info_id">
                            <%=LM.getText(LC.CODE_SELECTION_SEARCH_ECONOMIC_YEAR, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='budget_selection_info_id' id='budget_selection_info_id'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=BudgetSelectionInfoRepository.getInstance().buildEconomicYears(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="budget_mapping_id">
                            <%=LM.getText(LC.CODE_SELECTION_SEARCH_BUDGET_OPEATION_CODE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='budget_mapping_id' id='budget_mapping_id'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=Budget_operationRepository.getInstance().buildOperationCodes(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="date_from">
                            <%=UtilCharacter.getDataByLanguage(Language, "তারিখ হতে", "Date From")%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="date_from_js"/>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            </jsp:include>
                            <input type='hidden' id='date_from' name='date_from' value=''/>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="date_to">
                            <%=UtilCharacter.getDataByLanguage(Language, "তারিখ পর্যন্ত", "Date To")%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="date_to_js"/>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            </jsp:include>
                            <input type='hidden' id='date_to' name='date_to' value=''/>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="issue_number">
                            <%=UtilCharacter.getDataByLanguage(Language, "ইশ্যু নম্বর", "Issue Number")%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="issue_number" name="issue_number" onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="voucher_number">
                            <%=LM.getText(LC.BILL_MANAGEMENT_ADD_VOUCHERNUMBER, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="voucher_number"
                                   name="voucher_number" onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="budget_office_id">
                            <%=LM.getText(LC.BUDGET_OFFICE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='budget_office_id' id='budget_office_id'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=Budget_officeRepository.getInstance().buildOptions(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="economic_sub_code_id">
                            <%=UtilCharacter.getDataByLanguage(Language, "অর্থনৈতিক কোড", "Economic Code")%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='economic_sub_code_id' id='economic_sub_code_id'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=Economic_sub_codeRepository.getInstance().buildOptionWithAll(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-right">
                <input type="hidden" name="search" value="yes"/>
                <button type="submit"
                        class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                        onclick="allfield_changed('',0)"
                        style="background-color: #00a1d4;">
                    <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                </button>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->

<%@include file="../common/pagination_with_go2.jsp" %>

<template id="loader">
    <div class="modal-body">
        <div class="search-loader-container-circle ">
            <div class="search-loader-circle"></div>
        </div>
    </div>
</template>
<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>
<script type="text/javascript">
    const budgetInfoIdSelector = $('#budget_selection_info_id');
    const budgetMappingIdSelector = $('#budget_mapping_id');
    const budgetOfficeIdSelector = $('#budget_office_id');
    const economicSubCodeIdSelector = $('#economic_sub_code_id');
    const issueNumberText = $('#issue_number');
    const voucherNumberText = $('#voucher_number');

    function resetInputs() {
        budgetInfoIdSelector.select2("val", '-1');
        budgetMappingIdSelector.select2("val", '-1');
        resetDateById('date_from_js');
        resetDateById('date_to_js');
        budgetOfficeIdSelector.select2("val", '-1');
        economicSubCodeIdSelector.select2("val", '-1');
        issueNumberText.val('');
        voucherNumberText.val('');
    }

    window.addEventListener('popstate', e => {
        if (e.state) {
            let params = e.state;
            dosubmit(params, false);
            resetInputs();
            let arr = params.split('&');
            arr.forEach(e => {
                let item = e.split('=');
                if (item.length === 2) {
                    switch (item[0]) {
                        case 'budget_selection_info_id':
                            budgetInfoIdSelector.val(item[1]).trigger('change');
                            break;
                        case 'budget_mapping_id':
                            budgetMappingIdSelector.val(item[1]).trigger('change');
                            break;
                        case 'date_from':
                            setDateByTimestampAndId('date_from_js', item[1]);
                            break;
                        case 'date_to':
                            setDateByTimestampAndId('date_to_js', item[1]);
                            break;
                        case 'budget_office_id':
                            budgetOfficeIdSelector.val(item[1]).trigger('change');
                            break;
                        case 'economic_sub_code_id':
                            economicSubCodeIdSelector.val(item[1]).trigger('change');
                            break;
                        case 'issue_number':
                            issueNumberText.val(item[1]);
                            break;
                        case 'voucher_number':
                            voucherNumberText.val(item[1]);
                            break;
                        default:
                            setPaginationFields(item);
                    }
                }
            });
        } else {
            dosubmit(null, false);
            resetInputs();
            resetPaginationFields();
        }
    });

    $(document).ready(function () {
        select2SingleSelector("#budget_selection_info_id", '<%=Language%>');
        select2SingleSelector("#budget_mapping_id", '<%=Language%>');
        select2SingleSelector("#budget_office_id", '<%=Language%>');
        select2SingleSelector("#economic_sub_code_id", '<%=Language%>');
    });

    function dosubmit(params, pushState = true) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (pushState) {
                    history.pushState(params, '', '<%=action%>&' + params);
                }
                setTimeout(() => {
                    document.getElementById('tableForm').innerHTML = this.responseText;
                    setPageNo();
                    searchChanged = 0;
                }, 500);
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        let url = "<%=action%>&ajax=true&isPermanentTable=true";
        if (params) {
            url += "&" + params;
        }
        xhttp.open("GET", url, false);
        xhttp.send();

    }

    function convertBanglaDigitToEnglish(str) {
        str = String(str);
        str = str.replaceAll('০', '0');
        str = str.replaceAll('১', '1');
        str = str.replaceAll('২', '2');
        str = str.replaceAll('৩', '3');
        str = str.replaceAll('৪', '4');
        str = str.replaceAll('৫', '5');
        str = str.replaceAll('৬', '6');
        str = str.replaceAll('৭', '7');
        str = str.replaceAll('৮', '8');
        str = str.replaceAll('৯', '9');
        return str;
    }

    function allfield_changed(go, pagination_number) {
        let params = 'budget_selection_info_id=' + document.getElementById('budget_selection_info_id').value;
        params += '&budget_mapping_id=' + document.getElementById('budget_mapping_id').value;
        params += '&date_from=' + getDateTimestampById('date_from_js');
        params += '&date_to=' + getDateTimestampById('date_to_js');
        params += '&budget_office_id=' + document.getElementById('budget_office_id').value;
        params += '&economic_sub_code_id=' + document.getElementById('economic_sub_code_id').value;
        let issueNumber = convertBanglaDigitToEnglish(issueNumberText.val());
        params += '&issue_number=' + issueNumber;
        let voucher = convertBanglaDigitToEnglish(voucherNumberText.val()).replaceAll(/[a-z\-]/ig, '');
        params += '&voucher_number=' + voucher;

        params += '&search=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);
    }
</script>