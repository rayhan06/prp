<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="office_units.Office_unitsRepository" %>

<div id="drop_down" class="">
    <div class="">
        <div id="ajax-content">
            <div class="border rounded pl-5 pt-2">
                <label class="radio-inline">
                    <div class="d-flex align-items-center">
                        <input class="form-check-input" style="margin-top: 0" type="radio" name="search_by" value="by_organogram" checked>
                        <%=LM.getText(LC.EMPLOYEE_SEARCH_MODAL_SEARCH_BY_OFFICE, loginDTO)%>
                    </div>
                </label>
                <label class="radio-inline ml-md-5">
                    <div class="d-flex align-items-center">
                        <input class="form-check-input" style="margin-top: 0" type="radio" name="search_by" value="by_otherInfo">
                        <%=LM.getText(LC.EMPLOYEE_SEARCH_MODAL_SEARCH_BY_EMPLOYEE_INFORMATION, loginDTO)%>
                    </div>
                </label>
                <%--Tree Search Hidden For Now--%>
                <label class="radio-inline ml-5" style="display: none;">
                    <input class="form-check-input" type="radio" name="search_by" value="tree_search"> Tree Search
                </label>
            </div>
            <div class="search_by_div" id="by_organogram_div">
                <div class="kt-form" id="officeLayer_dropdown_div">
                    <%-- BEGIN: Making Dreopdown Dynamic --%>
                    <div id="officeLayer_div_1" class="form-group office-layer mt-4" style="">
                        <label><%=(isLanguageEnglish ? "Office" : "দপ্তর")%>
                        </label>
                        <div>
                            <select class='form-control' data-label="Office"
                                    id='officeLayer_select_1' tag='pb_html'
                                    onchange="layerChange(this);">
                            </select>
                        </div>
                    </div>
                    <%-- END: Making Dreopdown Dynamic --%>
                </div>
            </div>
            <div class="search_by_div mt-4" id="by_otherInfo_div" style="display: none;">
                <div class="kt-form">
                    <div class="form-group">
                        <label for="search_by_userName">
                            <%=LM.getText(LC.USER_ADD_USER_NAME, loginDTO)%>
                        </label>
                        <input type="text" class="form-control" name="userName" id="search_by_userName">
                    </div>
                    <div class="form-group">
                        <label for="search_by_nameEn">
                            <%=LM.getText(LC.PARLIAMENT_ITEM_ADD_NAMEENG, loginDTO)%>
                        </label>
                        <input type="text" class="form-control" name="nameEn" id="search_by_nameEn">
                    </div>
                    <div class="form-group">
                        <label for="search_by_nameBn">
                            <%=LM.getText(LC.PARLIAMENT_ITEM_ADD_NAMEBNG, loginDTO)%>
                        </label>
                        <input type="text" class="form-control" name="nameBn" id="search_by_nameBn">
                    </div>
                    <div class="form-group">
                        <label for="search_by_phoneNumber">
                            <%=LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO)%>
                        </label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><%=isLanguageEnglish ? "+88" : "+৮৮"%>
                                </div>
                            </div>
                            <input type="text" class="form-control" name="phoneNumber" id="search_by_phoneNumber">
                        </div>
                    </div>
                    <div class="form-group text-right mb-0">
                        <button type="button" id="onOtherInfoSearch-btn"
                                class="btn btn-primary shadow btn-border-radius">
                            <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
            <div class="search_by_div mt-4" id="tree_search_div" style="display: none;">
                <jsp:include page="/tree/tree.jsp">
                    <jsp:param name="TREE_ID" value="jsTree"></jsp:param>
                    <jsp:param name="REMOVE_PLUGIN" value="state"></jsp:param>
                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                </jsp:include>
            </div>
            <div class="" id="employeeSearchModal_table_div">
            </div>
        </div>
    </div>
</div>