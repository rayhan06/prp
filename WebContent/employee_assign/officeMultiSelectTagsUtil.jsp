<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<style>
    .template-tag {
        display: none !important;
    }

    .tag-container {
        border: 2px solid #eee;
        border-radius: 3px;
        display: flex;
        flex-wrap: wrap;
        align-content: flex-start;
        padding: 6px;
    }

    .tag-container .tag {
        height: 30px;
        margin: 5px;
        padding: 5px 6px;
        border: 1px solid #ccc;
        border-radius: 3px;
        background: #eee;
        display: flex;
        align-items: center;
        color: #333;
        box-shadow: 0 0 4px rgba(0, 0, 0, 0.2), inset 0 1px 1px #fff;
        cursor: default;
    }

    .tag i {
        font-size: 16px;
        color: #666;
        margin-left: 5px;
        cursor: pointer;
    }
</style>

<script>
    let selectedOfficesDiv;
    let templateTag;
    let officeUnitIdsInput;
    let selectedOfficesTagContainer;
    let selectedOfficeUnitIds;

    $(() => {
        selectedOfficesDiv = $('#selected-offices');
        templateTag = document.querySelector('.template-tag');
        officeUnitIdsInput = document.querySelector('#officeUnitIds_input');
        selectedOfficesTagContainer = document.querySelector('#selected-offices-tag-container');
        selectedOfficeUnitIds = new Map();
    });

    function removeThisButtonsTag() {
        const thisButtonsTag = this.closest('.tag');
        const tagData = JSON.parse(thisButtonsTag.dataset.jsonData);
        selectedOfficeUnitIds.delete(tagData.id);
        officeUnitIdsInput.value = JSON.stringify(
            Array.from(selectedOfficeUnitIds.keys())
        );
        thisButtonsTag.remove();
        if (selectedOfficeUnitIds.size === 0) {
            selectedOfficesDiv.hide();
            officeUnitIdsInput.value = '';
        }
    }

    function viewOfficeIdInTags(selectedOffice) {
        if (selectedOffice.id === '') return;
        if (selectedOfficeUnitIds.has(selectedOffice.id)) return;

        selectedOfficesDiv.show();
        const newTag = templateTag.cloneNode(true);
        newTag.classList.remove('template-tag');
        newTag.querySelector('.tag-name').innerText = selectedOffice.name;
        newTag.querySelector('.tag-remove-btn').onclick = removeThisButtonsTag;
        newTag.dataset.jsonData = JSON.stringify(selectedOffice);
        selectedOfficesTagContainer.append(newTag);

        selectedOfficeUnitIds.set(selectedOffice.id, selectedOffice);
        officeUnitIdsInput.value = JSON.stringify(
            Array.from(selectedOfficeUnitIds.keys())
        );
    }
    
    class SelectedOffice {
		  constructor(id, name) {
		    this.id = id;
		    this.name = name;
		  }
	}
    
    function viewOfficeIdInTagsByNameId(id, name) {
    	var selectedOffice = new SelectedOffice(id, name);

        selectedOfficesDiv.show();
        const newTag = templateTag.cloneNode(true);
        newTag.classList.remove('template-tag');
        newTag.querySelector('.tag-name').innerText = selectedOffice.name;
        newTag.querySelector('.tag-remove-btn').onclick = removeThisButtonsTag;
        newTag.dataset.jsonData = JSON.stringify(selectedOffice);
        selectedOfficesTagContainer.append(newTag);

        selectedOfficeUnitIds.set(selectedOffice.id, selectedOffice);
        officeUnitIdsInput.value = JSON.stringify(
            Array.from(selectedOfficeUnitIds.keys())
        );
    }

    function clearAllOfficeTags() {
        $('.tag i.tag-remove-btn').click();
    }
</script>