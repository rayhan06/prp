<!-- Modal -->
<%--
  Author: Moaz Mahmud
  USAGE:
--%>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="user.UserRepository" %>
<%@page import="user.UserDTO" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.CommonConstant" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String isHierarchyNeededStr = request.getParameter("isHierarchyNeeded");
    boolean isHierarchyNeeded = isHierarchyNeededStr != null && Boolean.parseBoolean(isHierarchyNeededStr);
%>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="search_office_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h4 class="caption table-title">
                    <%=isLanguageEnglish? "Find Office" : "দপ্তর খুঁজুন"%>
                </h4>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body">
                <%@include file="officeSearchModalBody.jsp" %>
            </div>

            <%--------------------------------FOOTER----------------------------------------------%>
            <div class="modal-footer border-top-0">
                <button type="button" class="btn btn-success shadow btn-border-radius"
                        id="selectOffice_submit_button" onclick="useOfficeSubmitButton();">
                    <%=LM.getText(LC.OFFICE_SEARCH_MODAL_SELECT_OFFICE, loginDTO)%>
                </button>
                <button type="button" class="btn cancel-btn text-white shadow btn-border-radius" data-dismiss="modal">
                    <%=LM.getText(LC.CLOSE_CLOSE, loginDTO)%>
                </button>
            </div>
        </div>
    </div>
</div>

<script>
    let officeSearchClearOnLoadFlag = true;
    let officeSearchFirstTimeLoad = true;

    function officeSearchInitLayer1(){
        // fetch options for new select
        let url = "Office_unitsServlet?actionType=<%=isHierarchyNeeded? "ajax_employee_office" : "ajax_office&parent_id=0"%>&language=" + '<%=Language%>';
        // console.log("office_unit ajax url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (data) {
                document.getElementById('officeLayer_select_1_searchOffice').innerHTML = data;
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function officeSearchLoadFirstLayer() {
        $('#officeLayer_select_1_searchOffice').val('');
        officeSearchInitLayer1();
        officeSearchClearNextLayersFrom(1);
        officeSearchFirstTimeLoad = false;
    }

    // modal on load event
    $('#search_office_modal').on('show.bs.modal', function () {
        let options = officeSelectModalOptionsMap.get(officeSelectModalUsage);
        if(options.keepLastSelectState === true) {
            if(officeSearchFirstTimeLoad) {
                officeSearchLoadFirstLayer();
            }
            return;
        }
        // fetch layer 1 options
        if(officeSearchClearOnLoadFlag) {
            officeSearchLoadFirstLayer();
        }
    });

    function officeSearchGetLayerId(officeLayerIdText) {
        // fromat: officeLayer_{select or div}_{layer id}_searchOffice
        return parseInt(officeLayerIdText.split('_')[2]);
    }

    function officeSearchClearNextLayersFrom(currentLayerId) {
        for (let selectElement of document.querySelectorAll('.office-layer_searchOffice')) {
            let layerId = officeSearchGetLayerId(selectElement.id);
            if (layerId > currentLayerId) {
                selectElement.remove();
            }
        }
    }

    newSelect = null;
    selectedOfficeId = null;
    newDiv = null;

    function officeSearchCloneSelect(currentLayerId) {
        // clone the first div
        newDiv = document.getElementById('officeLayer_div_1_searchOffice').cloneNode(true);
        newDiv.id = 'officeLayer_div_' + (currentLayerId + 1) + '_searchOffice';
        newSelect = newDiv.getElementsByTagName('select')[0];
        newSelect.id = 'officeLayer_select_' + (currentLayerId + 1) + '_searchOffice';
    }

    function officeSearchLayerChange(selectElement) {
        let layerId = officeSearchGetLayerId(selectElement.id);
        officeSearchClearNextLayersFrom(layerId);

        selectedOfficeId = selectElement.value;
        if (selectedOfficeId === '') {
            return;
        }

        officeSearchCloneSelect(layerId);

        // fetch options for new select
        let url = "Office_unitsServlet?actionType=<%=isHierarchyNeeded? "ajax_employee_office" : "ajax_office"%>&parent_id="
            + selectedOfficeId + "&language=" + '<%=Language%>';
        // console.log("office_unit ajax url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (data) {
                data = data.trim();
                if (newSelect !== null && data !== '') {
                    newSelect.innerHTML = data;
                    document.getElementById('officeLayer_dropdown_div_searchOffice').appendChild(newDiv);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function useOfficeSubmitButton(){
        let lastSelectedOffice = {
            id: '',
            name: ''
        }
        for (let selectElement of document.querySelectorAll('.office-layer_searchOffice')) {
            let selectElemSelected = $('#' + selectElement.id + ' option:selected');
            let value = selectElemSelected.val();
            if(value !== ''){
                lastSelectedOffice.id = value;
                lastSelectedOffice.name = selectElemSelected.text();
            }
        }

        let options = officeSelectModalOptionsMap.get(officeSelectModalUsage);
        if(options.isMultiSelect !== true) {
            $('#search_office_modal').modal('hide');
        }

        if(officeSelectModalUsage !== 'none'){
            if(options.officeSelectedCallback){
                options.officeSelectedCallback(lastSelectedOffice);
            }
        }
    }

    function officeSearchSetOptions(options) {
        if(!Array.isArray(options) || options.length === 0) return;

        document.getElementById('officeLayer_select_1_searchOffice').innerHTML = options[0];
        for(let i = 1; i < options.length; ++i) {
            officeSearchCloneSelect(i);
            if (newSelect !== null && options[i] !== '') {
                newSelect.innerHTML = options[i];
                document.getElementById('officeLayer_dropdown_div_searchOffice').appendChild(newDiv);
            }
        }
        officeSearchFirstTimeLoad = false;
    }

    async function officeSearchSetSelectedOfficeLayers(selectedOfficeUnitId) {
        if(selectedOfficeUnitId === null || selectedOfficeUnitId === undefined) return;

        selectedOfficeUnitId = Number(selectedOfficeUnitId);
        if(isNaN(selectedOfficeUnitId) || selectedOfficeUnitId < 0) return;

        $('#officeLayer_select_1_searchOffice').val('');
        officeSearchClearNextLayersFrom(1);
        officeSearchClearOnLoadFlag = false;

        const url = 'Office_unitsServlet?actionType=ajax_getAllLayerOptions&selectedOfficeUnitId=' + selectedOfficeUnitId;
        const res = await fetch(url);
        try {
            const options = await res.json();
            officeSearchSetOptions(options);
        } catch (error) {
            console.error(error);
        } finally {
            officeSearchClearOnLoadFlag = true;
        }
    }
</script>

<%-- Author: Moaz Mahmud --%>