<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="office_units.Office_unitsRepository" %>

<div class="row">
    <div class="col-md-12">
        <div class="kt-form" id="officeLayer_dropdown_div_searchOffice">
            <%-- BEGIN: Making Dreopdown Dynamic --%>
            <div id="officeLayer_div_1_searchOffice" class="form-group office-layer_searchOffice">
                <label>
                    <%=(isLanguageEnglish ? "Office" : "দপ্তর")%>
                    <span class="required"> * </span>
                </label>
                <div>
                    <select
                            class='form-control formRequired'
                            required data-label="Office"
                            id='officeLayer_select_1_searchOffice'
                            tag='pb_html'
                            onchange="officeSearchLayerChange(this);"
                    >
                    </select>
                </div>
            </div>
            <%-- END: Making Dreopdown Dynamic --%>
        </div>
    </div>
</div>