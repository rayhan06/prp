<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="java.util.Map" %>
<%@ page import="employee_assign.EmployeeAssignDTO" %>
<%@ page import="test_lib.util.Pair" %>
<%@ page import="employee_office_report.InChargeLevelEnum" %>
<%@ page import="util.StringUtils" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="office_unit_organogram.Office_unit_organogramDTO" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="office_unit_organograms.Office_unit_organogramsDTO" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="grade_wise_pay_scale.Grade_wise_pay_scaleRepository" %>
<%@page pageEncoding="utf-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) failureMessage = "";
    String Language = LM.getText(LC.EMPLOYEE_HISTORY_EDIT_LANGUAGE, loginDTO);
    int year = Calendar.getInstance().get(Calendar.YEAR);
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String context = request.getContextPath() + "/";
%>

<style>
    .btn:hover {
        color: #fff;
    }
</style>

<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead class="text-nowrap">
        <tr>
            <th></th>
            <th>
                <%=isLangEng ? "Designation" : "পদবি"%>
            </th>
            <th>
                <%=isLangEng ? "Employee" : "কর্মকর্তা/কর্মচারী"%>
            </th>
            <th style="min-width: 25rem">
                <%=isLangEng ? "Joining Date" : "যোগদানের তারিখ"%>
            </th>
            <th>
                <%=isLangEng ? "In charge Level" : "ইনচার্জ লেভেল"%>
            </th>
            <th>
                <%=isLangEng ? "Grade" : "গ্রেড"%>
            </th>
            <th>
                <%=isLangEng ? "Pay scale" : "পে স্কেল"%>
            </th>
            <th>
                <%=isLangEng ? "Salary Grade" : "বেতনভিত্তিক গ্রেড"%>
            </th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        <%
            int rowIndex = 0;
            try {
                List<Pair> data = (List<Pair>) session.getAttribute("data");
                if (data != null) {

                    long currentTime = System.currentTimeMillis();
                    for (Pair pair : data) {
                        Map<Object, Object> unitMap = (Map<Object, Object>) pair.getKey();
                        String unitNameEng = (String) unitMap.get("unit_name_eng");
                        String unitNameBng = (String) unitMap.get("unit_name_bng");
        %>

        <tr>
            <td id="selected_office_unit" colspan="100%" style="font-weight: bold;">
                <%=isLangEng ? ((unitNameEng != null && unitNameEng.length() > 0) ? unitNameEng : "Unit Name Not Found")
                        : ((unitNameBng != null && unitNameBng.length() > 0) ? unitNameBng : "ইউনিটের নাম পাওয়া যায়নি")%>
            </td>
        </tr>

        <%
            List<EmployeeAssignDTO> employeeList = (List<EmployeeAssignDTO>) pair.getValue();
            for (EmployeeAssignDTO employeeAssignDTO : employeeList) {
                OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeAssignDTO.organogram_id);
                boolean isPostOccupied = employeeAssignDTO.last_office_date == SessionConstants.MIN_DATE
                        || currentTime <= employeeAssignDTO.last_office_date;
        %>

        <tr>
            <td>
                <label class='kt-radio kt-radio--bold kt-radio--brand'>
                    <input name='radio_check'
                           type='radio'
                        <%= isPostOccupied ? " disabled"
                                           : " onclick='assignEmployeeCheck("
                                           + rowIndex + ","
                                           + employeeAssignDTO.employee_id + ","
                                           + employeeAssignDTO.officeId + ","
                                           + employeeAssignDTO.office_unit_id + ","
                                           + employeeAssignDTO.organogram_id + ","
                                           + employeeAssignDTO.designation_level + ","
                                           + employeeAssignDTO.designation_sequence + ","
                                           + employeeAssignDTO.employee_office_id + ")'"%>
                    >
                    <span></span>
                </label>
            </td>

            <td id="<%=employeeAssignDTO.organogram_id%>"
                title="<%=isLangEng?employeeAssignDTO.briefDescriptionEng:employeeAssignDTO.briefDescriptionBng%>">
                <%=isLangEng ? employeeAssignDTO.organogram_name_eng : employeeAssignDTO.organogram_name_bng%>
            </td>

            <%if (isPostOccupied) {%>
            <td>
                <%=isLangEng ? employeeAssignDTO.employee_name_eng : employeeAssignDTO.employee_name_bng%>
                <br>
                <%=isLangEng ? employeeAssignDTO.username : StringUtils.convertToBanNumber(employeeAssignDTO.username)%>
            </td>

            <td>
                <%=StringUtils.getFormattedDate(isLangEng, employeeAssignDTO.joining_date)%>
            </td>

            <td>
                <%=InChargeLevelEnum.getTextByValue(isLangEng, employeeAssignDTO.inChargeLabel)%>
            </td>
            <td>
                <%=CatRepository.getInstance().getText(Language, "job_grade", officeUnitOrganograms.job_grade_type_cat)%>
            </td>
            <td>
                <%=Grade_wise_pay_scaleRepository.getInstance().getText(Language, employeeAssignDTO.gradeTypeLevel)%>
            </td>
            <td>
                <%=CatRepository.getInstance().getText(Language, "job_grade", employeeAssignDTO.salaryGradeType)%>
            </td>
            <td></td>
            <%} else {%>
            <td></td>

            <td>
                <%
                    String value = "assign_date_js_" + rowIndex;
                %>
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                    <jsp:param name="END_YEAR"
                               value="<%=year+1%>"></jsp:param>
                </jsp:include>
            </td>

            <td>
                <select class="form-control" id="incharge_label_<%=rowIndex%>">
                    <%=InChargeLevelEnum.getBuildOptions(Language)%>
                </select>
            </td>
            <td>
                <%=CatRepository.getInstance().getText(Language, "job_grade", officeUnitOrganograms.job_grade_type_cat)%>
            </td>
            <td>
                <select class="form-control" id="grade_type_level_<%=rowIndex%>">
                    <%=Grade_wise_pay_scaleRepository.getInstance().buildOptions(Language, null)%>
                </select>
            </td>
            <td>
            	<select class="form-control" id="salaryGradeType_<%=rowIndex%>">
                    <%=CatRepository.getOptions(Language, "job_grade", employeeAssignDTO.salaryGradeType)%>
                </select>
                
            </td>
            <td>
                <button class="btn btn-outline-info" onclick="assignEmployeeOK('<%=rowIndex%>')">
                    <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_SUBMIT, loginDTO)%>
                </button>
            </td>
            <%
                    rowIndex++;
                }%>
        </tr>
        <%
                        }
                    }
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>


    </table>
</div>


