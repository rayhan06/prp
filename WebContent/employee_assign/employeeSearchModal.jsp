<!-- Modal -->
<%--
  Author: Moaz Mahmud
  USAGE: see disciplinary_log/disciplinary_logEditBody.jsp
--%>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="user.UserRepository" %>
<%@page import="user.UserDTO" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.CommonConstant" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String isHierarchyNeededStr = request.getParameter("isHierarchyNeeded");
    boolean isHierarchyNeeded = isHierarchyNeededStr != null && Boolean.parseBoolean(isHierarchyNeededStr);
    String modalTitle = request.getParameter("modalTitle");
    if(modalTitle == null){
        modalTitle = LM.getText(LC.EMPLOYEE_SEARCH_MODAL_FIND_EMPLOYEE, userDTO);
    }
    String emptyTableMessage = request.getParameter("emptyTableMessage");
    if(emptyTableMessage == null)
        emptyTableMessage = LM.getText(LC.EMPLOYEE_SEARCH_MODAL_NO_UNASSIGNED_EMPLOYEE_FOUND,loginDTO);

%>

<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="search_emp_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h4 class="caption" style="color: #56b2cf;">
                    <%=modalTitle%>
                </h4>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body modal-lg">
                <%@include file="employeeSearchModalBody.jsp" %>
            </div>

            <%--------------------------------FOOTER----------------------------------------------%>
            <div class="modal-footer border-0">
                <button type="button" class="btn cancel-btn text-white shadow btn-border-radius" data-dismiss="modal">
                    <%=LM.getText(LC.CLOSE_CLOSE, loginDTO)%>
                </button>
            </div>
        </div>
    </div>
</div>

<script>
	var language = '<%=Language%>';
    function initLayer1() {
        // fetch options for new select
        let url = "Office_unitsServlet?actionType=<%=isHierarchyNeeded? "ajax_employee_office" : "ajax_office&parent_id=0"%>&language=" + '<%=Language%>';
        // console.log("office_unit ajax url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (data) {
                document.getElementById('officeLayer_select_1').innerHTML = data;
                <%if(isHierarchyNeeded){%>
                let url = "EmployeeAssignServlet?actionType=getAllDescentEmployeeList";
                $.ajax({
                    type: "GET",
                    url: url,
                    async: true,
                    success: function (data) {
                        if (this.responseText !== '') {
                            showInSearchTable(data);
                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
                <%}%>
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function clearSearchByInfo() {
        // clear search table

        document.getElementById('employeeSearchModal_table_div').innerHTML = "";

        $('#search_by_userName').val('');
        $('#search_by_nameEn').val('');
        $('#search_by_nameBn').val('');
    }

    $('input:radio[name="search_by"]').change(function () {
        // TODO: confused on to clear organogram search on tab switch
        clearSearchByInfo();
        initLayer1()
        clearNextLayersFrom(1);

        $(".search_by_div").hide();
        let selectedSearchByType = $(this).val();
        $('#' + selectedSearchByType + "_div").show();
    });

    function hide_selected_employees(filter_from_table_name, selected_employees_map_or_set) {
        // console.log("filter function");
        // console.log("selected_employees_map_or_set");
        // console.log(selected_employees_map_or_set);
        let table_obj = document.getElementById(filter_from_table_name);
        let n_row = table_obj.rows.length;
        for (let i = 0; i < n_row; i++) {
            let row = table_obj.rows[i];
            let employee_record_id = row.id.split('_')[0];
            if (selected_employees_map_or_set.has(employee_record_id)) {
                hide(row);
            } else {
                un_hide(row);
            }
        }
    }

    function showInSearchTable(data) {
        document.getElementById('employeeSearchModal_table_div').innerHTML = '';
        document.getElementById('employeeSearchModal_table_div').innerHTML = data;
        // TODO: filter result here
        
        if (typeof table_name_to_collcetion_map !== 'undefined')
       	{
        	let info_map = table_name_to_collcetion_map.get(modal_button_dest_table).info_map;
            if(info_map) {
                hide_selected_employees('employeeSearchModal_tableData', info_map);
            }else{
                console.log('table_name_to_collcetion_map not set!');
            }
       	}
        

        const tableBody = document.querySelector('#employeeSearchModal_table_div table tbody');
        if(tableBody!= null && tableBody.innerText.trim() === ''){
            tableBody.innerHTML =
              '<tr><td colspan="4" class="text-center">'
            + '<%=emptyTableMessage%>'
            + '</td></tr>';
        }
    }

    function showEmployeeList(officeId) {
        let baseUrl = "EmployeeAssignServlet?actionType=getDisciplinaryEmployeeList";
        if (typeof table_name_to_collcetion_map !== 'undefined')
       	{
        	let options = table_name_to_collcetion_map.get(modal_button_dest_table);
            if(options.employeeSearchApiUrl){
                baseUrl = options.employeeSearchApiUrl;
            }
       	}
        
        let url = baseUrl + '&officeId=' + officeId;
        $.ajax({
            type: "GET",
            url: url,
            async: true,
            success: showInSearchTable,
            error: function (error) {
                console.log(error);
            }
        });
    }

    $("#onOtherInfoSearch-btn").click(function (e) {
        // e.preventDefault(); // avoid to execute the actual submit of the form.
        let userName = $('#search_by_userName').val();
        let nameEn = $('#search_by_nameEn').val();
        let nameBn = $('#search_by_nameBn').val();
        let phoneNumber = $('#search_by_phoneNumber').val();
        if(phoneNumber !== ''){
            phoneNumber = '88' + phoneNumber;
        }

        let url = 'EmployeeAssignServlet?actionType=searchEmployeeOfficeInfo';
        if(userName){
            url += '&userName='+convertBanglaDigitToEnglish(userName);
        }
        if(phoneNumber){
            url += '&phoneNumber='+convertBanglaDigitToEnglish(phoneNumber);
        }
        if(nameEn){
            url += '&nameEn='+nameEn;
        }
        if(nameBn){
            url += '&nameBn='+nameBn;
        }

        // console.log('AJAX URL ' + url);
        $.ajax({
            type: "GET",
            url: url,
            async: true,
            success: showInSearchTable,
            error: function (error) {
                console.log(error);
            }
        });
    });

    function convertBanglaDigitToEnglish(str){
        str = String(str);
        str = str.replaceAll('০', '0');
        str = str.replaceAll('১', '1');
        str = str.replaceAll('২', '2');
        str = str.replaceAll('৩', '3');
        str = str.replaceAll('৪', '4');
        str = str.replaceAll('৫', '5');
        str = str.replaceAll('৬', '6');
        str = str.replaceAll('৭', '7');
        str = str.replaceAll('৮', '8');
        str = str.replaceAll('৯', '9');
        return str;
    }
    
    function unassign(prefix, row)
    {
    	var tableId = prefix + "_table_" + row;
    	var hiddenOrd = prefix + "_hidden_" + row;
    	var buttonId = prefix + "_button_" + row;
    	var buttonHtml = "<button type='button' class='btn text-white shadow btn-border-radius pl-4' style='background-color: #4a87e2'"
    	+ " onclick='addEmployeeWithRow(\"" + buttonId + "\")'"
    	+ " id='" + buttonId + "'><i class='fa fa-plus'></i></button>";
    	$("#" + tableId).html("<td>" + buttonHtml + "</td>");
    	$("#" + hiddenOrd).val(-1);
    	
    	if (typeof unassignCallback == 'function')
   		{
    		unassignCallback(prefix, row);
   		}
    }

    function use_row_button(button, rowId) {
    	if (modal_button_dest_table !== 'none') {
    		console.log("use_row_button called");
            
            
            if (typeof table_name_to_collcetion_map !== 'undefined')
           	{
            	let emp_info = add_containing_row_to_dest_table(button, modal_button_dest_table);
            	let options = table_name_to_collcetion_map.get(modal_button_dest_table);
                if (options.isSingleEntry) {
                    $('#search_emp_modal').modal('hide');
                }
                if (options.callBackFunction) {
                    options.callBackFunction(emp_info);
                }
           	}
            else
           	{
            	let containing_row = $("#" + rowId);
            	console.log("id " +  containing_row.attr("id"));
            	var hiddenOrgRow = modal_button_dest_table.split("_")[2];
            	var hiddenOrgName = modal_button_dest_table.split("_")[0];
            	var hiddenOrgField = hiddenOrgName + "_hidden_" + hiddenOrgRow; 
            	
            	var removeButtonHtml = "<button class='btn btn-sm shadow btn-border-radius cancel-btn text-white pl-4'"
            	+ " style='background-color: #ff3b2b; padding-right: 14px'"
            	+  " type='button' onclick='unassign(\"" + 
            	hiddenOrgName + "\"," +  hiddenOrgRow + ")'><i class='fa fa-trash'></i></button>";
            	
            	containing_row.find("td").eq(0).attr("width", "20%");
            	containing_row.find("td").eq(1).attr("width", "40%");
            	
            	if (typeof canUnassign !== 'undefined')
           		{
            		containing_row.find("td").eq(2).attr("width", "30%");
            		containing_row.find("td:last").html(removeButtonHtml);
           		}
            	else
           		{
            		containing_row.find("td:last").remove();
           		}
            	
            	console.log(modal_button_dest_table);
            	var x = containing_row.wrapAll('<div>').parent().html(); 
            	//console.log("setting: " + x);
            	$("#" + modal_button_dest_table).html(x);
            	
            	console.log("calling patient inputted with value = " + $("#userName_" + rowId).val());
            	var orgId = $("#employeeId_" + rowId).val();
            	var erId = $("#employee_record_Id_" + rowId).val();
            	
            	if (typeof patient_inputted == 'function')
           		{
            		patient_inputted($("#userName_" + rowId).val(), orgId, erId);
           		}
            	else
            	{
            		console.log("patient_inputted is not a function " + (typeof patient_inputted));
            	}
            	
           
            	if (typeof assignCallback == 'function')
           		{
            		assignCallback($("#ename_" + orgId).html(), hiddenOrgRow);
           		}
            	
            	
            	
            	if($("#" + hiddenOrgField).length)
           		{
            		$("#" + hiddenOrgField).val(orgId);
           		} 
            	
            	$("#search_emp_modal").modal('hide');           	
           	}
        }    	
    }

    function remove_containing_row(button, table_name) {
        // td > button id = "<employee record id>_td_button"
        let td_button = button.parentNode;
        let employee_record_id = td_button.id.split("_")[0];

        let added_info_map = table_name_to_collcetion_map.get(table_name).info_map;
        added_info_map.delete(employee_record_id);
        console.log('Employee to Remove', employee_record_id);
        console.log('Map After Removal',added_info_map);

        let containing_row = button.parentNode.parentNode;
        // console.log('containing_row.remove()');
        containing_row.remove();
    }
    var modal_button_dest_table = 'none';

    function add_containing_row_to_dest_table(button, dest_table_name) {
        let dest_table_info = table_name_to_collcetion_map.get(dest_table_name);
    		if (dest_table_info === undefined || dest_table_info == null) {
                return;
            }

        let this_row = button.parentNode.parentNode;
        let dest_table = document.getElementById(dest_table_name);

        // if no table destination table found, return the employee info json
        if(!dest_table){
            return JSON.parse(
                $('#' + this_row.id + '_data').val()
            );
        }

        // clone the dummy template row
        let new_row = dest_table.rows[0].cloneNode(true);

        let n_col = this_row.cells.length;
        // copy every cell apart from last one
        // which is the remove button
        for (let i = 0; i < n_col - 1; i++) {
            new_row.cells[i].innerHTML = this_row.cells[i].innerHTML.trim();
        }

        let record_officeUnit_organogram_id_array = this_row.id.split('_');
        let employee_record_id = record_officeUnit_organogram_id_array[0];

        // skip if already added
        let added_info_map = dest_table_info.info_map;
        if (added_info_map.has(employee_record_id)) {
            return;
        }

        if (dest_table_info.isSingleEntry && added_info_map.size > 0) {
            dest_table.removeChild(dest_table.lastElementChild);
            added_info_map.clear();
        }

        // td that contains button : id = "<employee record id>_td_button"
        new_row.cells[n_col - 1].id = employee_record_id + "_td_button";

        dest_table.appendChild(new_row);
        un_hide(new_row);

        let emp_info = JSON.parse(
            $('#' + this_row.id + '_data').val()
        );

        added_info_map.set(employee_record_id, emp_info);
        // hide(this_row);
        this_row.remove();

        return emp_info;
    }

    function hide(elememt) {
        elememt.style.display = "none";
    }

    function un_hide(elememt) {
        elememt.style.display = "";
    }

    // modal on load event
    $('#search_emp_modal').on('show.bs.modal', function () {
        $('#officeLayer_select_1').val('');
        // fetch layer 1 options
        initLayer1();
        clearNextLayersFrom(1);
        clearSearchByInfo();
    });

    function getLayerId(officeLayerIdText) {
        // fromat: officeLayer_{select or div}_{layer id}
        return parseInt(officeLayerIdText.split('_')[2]);
    }

    //pb methods
    function addEmployee()
    {
    	modal_button_dest_table = 'employeeToSet';
        $('#search_emp_modal').modal();
    }
    
    function addEmployeeWithTableId(tableId)
    {
    	modal_button_dest_table = tableId;
        $('#search_emp_modal').modal();
    }
    
    function addEmployeeWithRow(buttonId)
    {
    	var rowId = buttonId.split("_")[2];
    	var name = buttonId.split("_")[0];
    	var tableId = name + "_table_" + rowId;
    	modal_button_dest_table = tableId;
        $('#search_emp_modal').modal();
    }
    //end of pb methods

    function clearNextLayersFrom(currentLayerId) {
        for (let selectElement of document.querySelectorAll('.office-layer')) {
            let layerId = getLayerId(selectElement.id);
            if (layerId > currentLayerId) {
                selectElement.remove();
            }
        }
    }

    newSelect = null;
    selectedOfficeId = null;
    newDiv = null;

    function layerChange(selectElement) {
    	console.log("id = " + selectElement.id);
        let layerId = getLayerId(selectElement.id);

        var text;

        if (typeof table_name_to_collcetion_map === 'undefined')
        {
        	text = $( "#" + selectElement.id + " option:selected" ).text(); //needed for pb gen codes
        	console.log("layerId = " + text);
        }
        
        console.log(`layer ${layerId}: changed to : ${selectElement.value}`);
        clearNextLayersFrom(layerId);

        selectedOfficeId = selectElement.value;

        //needed for pb gen code
        if (typeof table_name_to_collcetion_map === 'undefined')
        {
	        if($("#officeUnitType").length)
	    	{
	    		console.log("Setting office id = " + selectedOfficeId);
	    		$("#officeUnitType").val(selectedOfficeId);
	    		
	    		if($("#employeeToSet").length)
	   			{
	    			var office = "office";
	    			if(language == "Bangla")
	   				{
	    				office = "দপ্তর";
	   				}
	   				var tr = "<tr><td></td><td></td><td>" + office + ": " + text + "</td></td>";
	   				$("#employeeToSet").html(tr);
	   			}
	    		
	    		if (typeof office_inputted == 'function')
           		{
	    			office_inputted(selectedOfficeId);
           		}
	    		//ajaxSubmit();
	    	}
	    }
    	
        if (selectedOfficeId === '') {
            return;
        }
        else
       	{
        	if($("#officeUnitType").length)
        	{
        		console.log("Setting office id = " + selectedOfficeId)
        		$("#officeUnitType").val(selectedOfficeId);
        	}
       	
       	}

        // clone the first div
        newDiv = document.getElementById('officeLayer_div_1').cloneNode(true);
        newDiv.id = 'officeLayer_div_' + (layerId + 1);
        newSelect = newDiv.getElementsByTagName('select')[0];
        newSelect.id = 'officeLayer_select_' + (layerId + 1);

        // fetch options for new select
        let url = "Office_unitsServlet?actionType=<%=isHierarchyNeeded? "ajax_employee_office" : "ajax_office"%>&parent_id="
            + selectedOfficeId + "&language=" + '<%=Language%>';
        // console.log("office_unit ajax url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: true,
            success: function (data) {
                data = data.trim();
                if (newSelect !== null && data !== '') {
                    newSelect.innerHTML = data;
                    document.getElementById('officeLayer_dropdown_div').appendChild(newDiv);
                }
                showEmployeeList(selectedOfficeId);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
</script>

<%-- Author: Moaz Mahmud --%>