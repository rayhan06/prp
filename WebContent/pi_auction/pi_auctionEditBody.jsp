
<%@page import="pi_auction_items.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@ page import="pb.*"%>
<%@ page import="util.*"%>
<%@ page import="pi_auction.Pi_auctionDTO" %>
<%@ page import="pi_auction.Pi_auctionDAO" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>
<%@ page import="pi_unique_item.PiUniqueItemAssignmentDAO" %>
<%@ page import="pi_unique_item.PiStageEnum" %>
<%@ page import="common.BaseServlet" %>
<%@include file="../pb/addInitializer2.jsp"%>


<%

    Pi_auctionDTO pi_auctionDTO = new Pi_auctionDTO();
    long ID = -1;
    if(request.getParameter("ID") != null)
    {
        ID = Long.parseLong(request.getParameter("ID"));
        pi_auctionDTO = (Pi_auctionDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
        if(pi_auctionDTO.auctionStatus != CommonApprovalStatus.PENDING.getValue()){
            return;
        }
    }


    List<Pi_auction_itemsDTO> pi_auction_itemsDTOS = Pi_auction_itemsDAO.getInstance().getPiAuctionItemsByAuctionId(pi_auctionDTO.iD,userDTO.unitID);

%>


<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <i class="fa fa-search fa-2x" style="color:#0098bf !important;"></i>&nbsp;
        <h3 class="kt-subheader__title">
            &nbsp; <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_PRODUCTNAME, loginDTO)%>
        </h3>
    </div>
</div>
<!-- end:: Subheader -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__body">

            <div class="mt-5">
                <form class="form-horizontal"
                      id="bigform" name="bigform">

                    <div class="form-group row" >
                        <label class="col-md-3 col-form-label text-right"><%=LM.getText(LC.PI_AUCTION_ADD_AUCTIONPACKAGENAME, loginDTO)%></label>
                        <div class="col-md-7">
                            <input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=pi_auctionDTO.iD%>' tag='pb_html'/>
                            <input type='text' name='auctionPackageName'class='form-control' value="<%=pi_auctionDTO.auctionPackageName%>" tag='pb_html'>
                        </div>
                    </div>
                    <div class="table-responsive">

                        <table id="tableData" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_PRODUCTNAME, loginDTO)%></th>
                                <th><%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_SUPPLIERCOMPANYNAME, loginDTO)%></th>
                                <th><%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_BUYINGDATE, loginDTO)%></th>
                                <th><%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_FISCALYEARID, loginDTO)%></th>
                                <th><%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_UNITBUYINGPRICE, loginDTO)%></th>
                                <th><%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_STOCK, loginDTO)%></th>
                                <th><%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_SELLINGUNIT, loginDTO)%></th>
                                <th><%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_UNITSELLINGPRICE, loginDTO)%></th>



                            </tr>
                            </thead>
                            <tbody>
                            <%
                                for(Pi_auction_itemsDTO pi_auction_itemsDTO : pi_auction_itemsDTOS){

                                    int initialStockInt= PiUniqueItemAssignmentDAO.getInstance().getTotalItemCountWithStageAndPurchaseIdAllover(pi_auction_itemsDTO.officeUnitId, pi_auction_itemsDTO.itemId,pi_auction_itemsDTO.piPurchaseItemsId,
                                            PiStageEnum.IN_STOCK.getValue());

                            %>
                            <tr>


                                <input type='hidden' class='form-control'  name='piAuctionItemId' value="<%=pi_auction_itemsDTO.iD%>"     tag='pb_html'/>
                                <input type='hidden' class='form-control'  name='piPurchaseItemsId' value="<%=pi_auction_itemsDTO.piPurchaseItemsId%>"     tag='pb_html'/>
                                <input type='hidden' class='form-control'  name='itemId' value="<%=pi_auction_itemsDTO.itemId%>"     tag='pb_html'/>
                                <input type='hidden' class='form-control'  name='productName' value="<%=pi_auction_itemsDTO.productName%>"     tag='pb_html'/>
                                <input type='hidden' class='form-control'  name='supplierCompanyName' value="<%=pi_auction_itemsDTO.supplierCompanyName%>"     tag='pb_html'/>
                                <input type='hidden' class='form-control'  name='buyingDate' value="<%=pi_auction_itemsDTO.buyingDate%>"     tag='pb_html'/>
                                <input type='hidden' class='form-control'  name='fiscalYearId' value="<%=pi_auction_itemsDTO.fiscalYearId%>"     tag='pb_html'/>
                                <input type='hidden' class='form-control'  name='unitBuyingPrice' value="<%=pi_auction_itemsDTO.unitBuyingPrice%>"     tag='pb_html'/>
                                <input type='hidden' class='form-control'  name='stock' value="<%=initialStockInt%>"     tag='pb_html'/>
                                <input type='hidden' class='form-control'  name='officeUnitId' value="<%=pi_auction_itemsDTO.officeUnitId%>"     tag='pb_html'/>



                                <td>

                                    <%=pi_auction_itemsDTO.productName%>


                                </td>

                                <td>
                                    <%=pi_auction_itemsDTO.supplierCompanyName%>


                                </td>

                                <td>
                                    <%=StringUtils.getFormattedDate(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language,pi_auction_itemsDTO.buyingDate)%>
                                </td>

                                <td>
                                    <%=Fiscal_yearRepository.getInstance().getText(pi_auction_itemsDTO.fiscalYearId, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language)%>
                                </td>

                                <td>
                                    <%=Utils.getDigits(pi_auction_itemsDTO.unitBuyingPrice, Language)%>


                                </td>

                                <td>
                                    <%=Utils.getDigits(initialStockInt, Language)%>


                                </td>

                                <td>
                                    <%=Utils.getDigits(pi_auction_itemsDTO.sellingUnit,Language)%>
                                    <input type='hidden' readonly step="0.01" class='form-control'  name='sellingUnit' value="<%=pi_auction_itemsDTO.sellingUnit%>"     tag='pb_html'/>

                                </td>

                                <td>

                                    <input type='number' step="0.01" class='form-control'  name='unitSellingPrice' value="<%=pi_auction_itemsDTO.unitSellingPrice%>"    tag='pb_html'/>

                                </td>


                            </tr>
                            <%
                                }
                            %>



                            </tbody>

                        </table>


                    </div>


                    <div class="text-right mt-3">
                        <button class="btn btn-sm shadow text-white submit-btn "
                                type="button" id="submit-btn" onclick="submitForm()">
                            <%=LM.getText(LC.VM_REQUISITION_ADD_RECEIVE_VEHICLE_EDIT_SUBMIT_BUTTON, loginDTO)%></button>
                    </div>


                </form>
            </div>

        </div>
    </div>
</div>


<script type="text/javascript">
    const piAuctionPreviewForm = $("#bigform");

    function submitForm(){
        buttonStateChange(true);
        if(PreprocessBeforeSubmiting(0)){
            $.ajax({
                type : "POST",
                url : "Pi_auctionServlet?actionType=ajax_edit",
                data : piAuctionPreviewForm.serialize(),
                dataType : 'JSON',
                success : function(response) {
                    console.log(response);
                    if(response.responseCode === 0){
                        $('#toast_message').css('background-color','#ff6063');
                        showToastSticky(response.msg,response.msg);
                        buttonStateChange(false);
                    }else if(response.responseCode === 200){
                        window.location.replace(getContextPath()+response.msg);
                    }
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        }else{
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value){
        $('#submit-btn').prop('disabled',value);
        $('#cancel-btn').prop('disabled',value);
    }


    function PreprocessBeforeSubmiting(row, action)
    {
        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
    {
        addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Pi_auction_itemsServlet");
    }

    function init(row)
    {

        setDateByStringAndId('buyingDate_js_' + row, $('#buyingDate_date_' + row).val());


    }

    var row = 0;
    $(document).ready(function(){

        if($(document).prop('title') === 'Edit User')
        {
            $(document).prop('title', '<%=LM.getText(LC.PI_APP_REQUEST_DETAILS_SEARCH_PI_APP_REQUEST_DETAILS_EDIT_BUTTON, loginDTO)%>');
        }
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    });





</script>






