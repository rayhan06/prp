<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="pi_auction.*" %>
<%@ page import="util.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%
    String navigator2 = "navPI_AUCTION";
    String servletName = "Pi_auctionServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.PI_AUCTION_ADD_AUCTIONPACKAGENAME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_AUCTION_ADD_ITEMCOUNT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_AUCTION_ADD_TOTALBUYINGPRICE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_AUCTION_ADD_TOTALSELLINGPRICE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th colspan="3"><%=LM.getText(LC.HM_UPDATE, loginDTO)%>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<Pi_auctionDTO>) rn2.list;

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Pi_auctionDTO pi_auctionDTO = (Pi_auctionDTO) data.get(i);


        %>
        <tr>


            <td>
                <%
                    value = pi_auctionDTO.auctionPackageName + "";
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>
            <td>
                <%
                    value = pi_auctionDTO.itemCount + "";
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>


            <td>
                <%
                    value = pi_auctionDTO.totalBuyingPrice + "";
                %>
                <%
                    value = String.format("%.1f", pi_auctionDTO.totalBuyingPrice);
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>


            <td>
                <%
                    value = pi_auctionDTO.totalSellingPrice + "";
                %>
                <%
                    value = String.format("%.1f", pi_auctionDTO.totalSellingPrice);
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>


            <%CommonDTO commonDTO = pi_auctionDTO; %>
            <%@include file="../pb/searchAndViewButton.jsp" %>


            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow "
                        style="background-color: #1E90FF;color: white;border-radius: 8px;cursor: pointer;"
                        onclick="location.href='Pi_package_auctioneerServlet?actionType=getAddPage&piItemAuctionId=<%=pi_auctionDTO.iD%>'"

                >
                    <%=LM.getText(LC.PI_AUCTION_ADD_AUCTION, loginDTO)%>
                </button>
            </td>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow "
                        style="background-color: #B22222;color: white;border-radius: 8px;cursor: pointer;"
                        onclick="rejectApplication(<%=pi_auctionDTO.iD%>,<%=pi_auctionDTO.officeUnitId%>);"
                        id="reject_btn_<%=i%>"
                >
                    <%=LM.getText(LC.USER_ADD_CANCEL, loginDTO)%>
                </button>
            </td>


        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


<script>

    function rejectApplication(amHouseAllocationRequestId, officeUnitId) {


        let msg = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_REASON_FOR_DISSATISFACTION, loginDTO)%>';
        let placeHolder = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_TYPE_DISSATISFACTION_REASON_HERE, loginDTO)%>';
        let confirmButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_YES_DISSATISFIED, loginDTO)%>';
        let cancelButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARD_APPROVAL_MAPPING_CANCEL_BUTTON, loginDTO)%>';
        let emptyReasonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_PLEASE_WRITE_DISSATISFACTION_REASON, loginDTO)%>';

        dialogMessageWithTextBoxWithoutAnimation(msg, placeHolder, confirmButtonText, cancelButtonText, emptyReasonText, (reason) => {

            submitRejectForm(reason, amHouseAllocationRequestId, officeUnitId);
        }, () => {
        });
    }


    function submitRejectForm(reason, piAuctionId, officeUnitId) {


        $.ajax({
            type: "POST",
            url: "Pi_auctionServlet?actionType=rejectApplicationSearchRow",
            data: {"piAuctionId": piAuctionId, "reject_reason": reason, "officeUnitId": officeUnitId},
            dataType: 'JSON',
            success: function (response) {
                if (response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                } else if (response.responseCode === 200) {
                    window.location.replace(getContextPath() + response.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChange(false);
            }
        });

    }

    // function buttonStateChange(value){
    // 	$('#submit-btn').prop('disabled',value);
    // 	$('#cancel-btn').prop('disabled',value);
    // }


</script>


			