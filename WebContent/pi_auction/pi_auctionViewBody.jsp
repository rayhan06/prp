<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="pi_auction.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.*" %>
<%@ page import="common.BaseServlet" %>
<%
    String servletName = "Pi_auctionServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Pi_auctionDTO pi_auctionDTO = (Pi_auctionDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    CommonDTO commonDTO = pi_auctionDTO;
%>
<%@include file="../pb/viewInitializer.jsp" %>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PI_AUCTION_SEARCH_PI_AUCTION_SEARCH_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8  offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2 md-0">
                            <div class="col-md-10  offset-md-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.PI_AUCTION_SEARCH_PI_AUCTION_SEARCH_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_AUCTION_ADD_AUCTIONPACKAGENAME, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = pi_auctionDTO.auctionPackageName + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>


                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_AUCTION_ADD_ITEMCOUNT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = pi_auctionDTO.itemCount + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_AUCTION_ADD_TOTALSELLINGPRICE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = pi_auctionDTO.totalSellingPrice + "";
                                        %>
                                        <%
                                            value = String.format("%.1f", pi_auctionDTO.totalSellingPrice);
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_AUCTION_ADD_TOTALBUYINGPRICE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = pi_auctionDTO.totalBuyingPrice + "";
                                        %>
                                        <%
                                            value = String.format("%.1f", pi_auctionDTO.totalBuyingPrice);
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>