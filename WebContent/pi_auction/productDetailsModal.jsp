<%@page import="sessionmanager.SessionConstants" %>
<%@page import="user.UserRepository" %>
<%@page import="user.UserDTO" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.CommonDAO" %>
<%@ page import="pb.CatDAO" %>
<%@ page import="util.HttpRequestUtils" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = LM.getText(LC.CARD_INFO_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="product-details-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h4 class="caption" style="color: #56b2cf; margin-left: 10px">
                    <%=LM.getText(LC.PI_AUCTION_EDIT_PI_AUCTION_EDIT_FORMNAME, userDTO)%>
                </h4>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body modal-xl">
                <div id="drop_down" class="row">
                    <div class="col-md-12">

                        <div class="table-responsive px-3">
                            <table class="table table-bordered table-striped  text-nowrap" id="product-details-table">
                                <thead>
                                <tr>
                                    <th class="row-data-product-name">
                                        <%=LM.getText(LC.PI_AUCTION_ADD_PRODUCT_NAME, loginDTO)%>
                                    </th>
                                    <th class="row-data-product-supplier-name">
                                        <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_SUPPLIERCOMPANYNAME, loginDTO)%>
                                    </th>

                                    <th class="row-data-buying-date">
                                        <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_BUYINGDATE, loginDTO)%>
                                    </th>
                                    <th class="row-data-fiscal-year">
                                        <%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_FISCALYEARID, loginDTO)%>
                                    </th>
                                    <th class="row-data-unit-buying-price">
                                        <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_UNITBUYINGPRICE, loginDTO)%>
                                    </th>
                                    <th class="row-data-stock">
                                        <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_STOCK, loginDTO)%>
                                    </th>

                                    <th></th>
                                </tr>
                                </thead>

                                <tbody></tbody>

                                <%--Template Row-> to be cloned to add new row.
                                    CONVENTIONS:
                                        * one tr with template-row class
                                        * td with class -> row-data-filedNameInJson
                                 --%>
                                <tr class="template-row">
                                    <td class="row-data-product-name"></td>
                                    <td class="row-data-product-supplier-name"></td>
                                    <td class="row-data-buying-date"></td>
                                    <td class="row-data-fiscal-year"></td>
                                    <td class="row-data-unit-buying-price"></td>
                                    <td class="row-data-stock"></td>
                                    <td class="row-data-add-btn">
                                        <button class='btn btn-sm shadow d-flex justify-content-between align-items-center' style="background-color: #66ce5f; color: white; border-radius: 8px;" type="button"
                                                onclick="selectRow(this)">
                                            <i class="fa fa-plus"></i>
                                            <%=LM.getText(LC.HM_ADD, loginDTO)%>
                                        </button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <%--------------------------------FOOTER----------------------------------------------%>
                <div class="modal-footer border-0">
                    <button type="button" class="btn cancel-btn text-white shadow btn-border-radius" data-dismiss="modal">
                        <%=LM.getText(LC.CLOSE_CLOSE, loginDTO)%>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    const economicCodeIdPrefix = 'product-details-';
    const economicSubCodeMap = new Map(); // TODO: init map with exisitng data

    // modal on load event
    $('#economic-code-modal').on('show.bs.modal', function () {

        document.getElementById('product-details-table').querySelector('tbody').innerHTML = '';
    });

    async function buildProductModalData(productId){

        document.getElementById('product-details-table').querySelector('tbody').innerHTML = '';
        if (productId === '') return;
        const url = 'Pi_auction_itemsServlet?actionType=buildProductModalTableData&productId=' + productId+'&officeUnitId=' +<%=userDTO.unitID%>;
        const response = await fetch(url);
        const productDetailsJson = await response.json();
        showSubCodesInTable(productDetailsJson, economicSubCodeMap);
    }
    function showSubCodesInTable(productDetailsJson, economicSubCodeMap){
        if(!Array.isArray(productDetailsJson)) return;

        for(let productDetail of productDetailsJson){
            const productDetailId = Number(productDetail.iD);
            let stock = +productDetail.stock;
            if(economicSubCodeMap.has(productDetailId) || stock<=0) continue;

            addDataInTable('product-details-table', productDetail);
        }

        const tableBody = document.querySelector('#product-details-table tbody');
        if(tableBody.innerText.trim() === ''){
            tableBody.innerHTML =
                '<tr><td colspan="7" class="text-center">'
                + '<%=LM.getText(LC.PI_AUCTION_ITEMS_EDIT_PIAUCTIONID, loginDTO)%>'
                + '</td></tr>';
        }
    }

    function addDataInTable(tableId, jsonData, toInsertInEconomicGroup) {
        const table = document.getElementById(tableId);

        const templateRow = table.querySelector('.template-row').cloneNode(true);
        templateRow.id = economicCodeIdPrefix + jsonData.iD;
        templateRow.classList.remove('template-row');

        templateRow.dataset.rowData = JSON.stringify(jsonData);

        templateRow.querySelector('td.row-data-product-name').innerText = jsonData.productName;
        templateRow.querySelector('td.row-data-product-supplier-name').innerText = jsonData.supplierCompanyName;
        templateRow.querySelector('td.row-data-buying-date').innerText = jsonData.buyingDate;
        templateRow.querySelector('td.row-data-fiscal-year').innerText = jsonData.fiscalYearId;
        templateRow.querySelector('td.row-data-unit-buying-price').innerText = jsonData.unitBuyingPrice;
        templateRow.querySelector('td.row-data-stock').innerText = jsonData.stock;

        const tableBody = table.querySelector('tbody');

        // if(!toInsertInEconomicGroup) tableBody.append(templateRow);
        // else addWithEconomicGroup(jsonData, tableBody, templateRow);

        tableBody.append(templateRow);
    }

    function addWithEconomicGroup(jsonData, tableBody, economicSubCodeRow) {
        let economicCodeRow = document.getElementById(economicCodeIdPrefix + jsonData.iD);
        if (economicCodeRow == null) {
            let economicGroupRow = getEconomicGroupRow(jsonData, tableBody);


            const economicCodeObj = {
                productName: jsonData.productName,
                supplierCompanyName: jsonData.supplierCompanyName,
                buyingDate: jsonData.buyingDate,
                fiscalYearId: jsonData.fiscalYearId,
                unitBuyingPrice: jsonData.unitBuyingPrice,
                stock: jsonData.stock
            };
            economicCodeRow = createEconomicGroupingTr(economicCodeObj);

            $(economicCodeRow).insertAfter(economicGroupRow);
            economicGroupRow.dataset.numberOfChildren++;
        }
        economicCodeRow = document.getElementById(economicCodeIdPrefix + jsonData.iD);

        economicSubCodeRow.id = economicCodeIdPrefix + jsonData.iD;
        $(economicSubCodeRow).insertAfter(economicCodeRow);
        economicCodeRow.dataset.numberOfChildren++;
    }

    function createEconomicGroupingTr(economicCodeObj) {
        const tr = document.createElement('tr');
        tr.id = economicCodeIdPrefix + economicCodeObj.iD;
        const td = document.createElement('td');
        td.colSpan = 3;
        td.innerHTML = '<b>TEST</b>';
        tr.dataset.numberOfChildren = '0';
        tr.append(td);
        return tr;
    }

    function getEconomicGroupRow(jsonData,tableBody) {
        let economicGroupRow = document.getElementById(economicCodeIdPrefix + jsonData.iD);
        if (economicGroupRow == null) {
            const economicGroupObj = {
                productName: jsonData.productName,
                supplierCompanyName: jsonData.supplierCompanyName,
                buyingDate: jsonData.buyingDate,
                fiscalYearId: jsonData.fiscalYearId,
                unitBuyingPrice: jsonData.unitBuyingPrice,
                stock: jsonData.stock
            };
            economicGroupRow = createEconomicGroupingTr(economicGroupObj);
            tableBody.append(economicGroupRow);
        }
        return document.getElementById(economicCodeIdPrefix + jsonData.economicGroup);
    }



    function convertToBanglaBasedOnLanguage(numberStr, language){
        if("english" === language.toLowerCase()) return numberStr;
        let str = String(numberStr);
        str = str.replaceAll('0', '০');
        str = str.replaceAll('1', '১');
        str = str.replaceAll('2', '২');
        str = str.replaceAll('3', '৩');
        str = str.replaceAll('4', '৪');
        str = str.replaceAll('5', '৫');
        str = str.replaceAll('6', '৬');
        str = str.replaceAll('7', '৭');
        str = str.replaceAll('8', '৮');
        str = str.replaceAll('9', '৯');
        return str;
    }

    function getRowDataAndMoveRow(rowButtonElement, destTableId) {
        const containingRow = rowButtonElement.parentNode.parentNode;
        const rowData = JSON.parse(containingRow.dataset.rowData);
        if(destTableId) addDataInTable(destTableId, rowData, true);
        containingRow.remove();

        return rowData;
    }

    function selectRow(rowButtonElement){
        const economicSubCode = getRowDataAndMoveRow(rowButtonElement, 'product-view-table');
        economicSubCodeMap.set(
            Number(economicSubCode.iD),
            economicSubCode
        );
        nextBtnStateChange(false);
    }
    function unSelectRow(rowButtonElement){
        const economicSubCode = getRowDataAndMoveRow(rowButtonElement);
        economicSubCodeMap.delete(Number(economicSubCode.iD));

        const economicCodeTrId = economicCodeIdPrefix + economicSubCode.iD;
        const economicCodeTr = document.getElementById(economicCodeTrId);
        economicCodeTr.dataset.numberOfChildren--;
        if(economicCodeTr.dataset.numberOfChildren === '0'){
            economicCodeTr.remove();
            const economicGroupTrId = economicCodeIdPrefix + economicSubCode.economicGroup;
            const economicGroupTr = document.getElementById(economicGroupTrId);
            economicGroupTr.dataset.numberOfChildren--;
            if(economicGroupTr.dataset.numberOfChildren === '0')
                economicGroupTr.remove();
        }
        if(economicSubCodeMap.size === 0){
            nextBtnStateChange(true);
        }
    }

    function nextBtnStateChange(value){
        $('#next-btn').prop('disabled',value);
    }
</script>