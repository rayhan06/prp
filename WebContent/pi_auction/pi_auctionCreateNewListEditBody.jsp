<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="pi_auction.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>

<%
    Pi_auctionDTO pi_auctionDTO = new Pi_auctionDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        pi_auctionDTO = Pi_auctionDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = pi_auctionDTO;
    String tableName = "pi_auction";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.PI_AUCTION_ADD_PI_AUCTION_ADD_FORMNAME, loginDTO);
    String servletName = "Pi_auctionServlet";
    String context = request.getContextPath() + "/";
    String packageOptions = CommonDAO.getOptions(Language, "procurement_package", -1);

    String index = "1";
    request.getParameter("index");
    //int i = 0;
    if (index != null) {
        i = Integer.parseInt(index);
    }

%>

<style>
    .template-row {
        display: none;
    }
</style>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div id="ajax-content">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_PACKAGE, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='procurementPackageId'
                                                        id='procurementPackageId_<%=i%>' tag='pb_html'>
                                                    <%=packageOptions%>
                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_GOODS_TYPE, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='procurementGoodsTypeId'
                                                        id='procurementGoodsTypeId_<%=i%>' tag='pb_html'>
                                                </select>

                                            </div>
                                        </div>


                                        <div id="subType">
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10">
                        <div class="text-right">
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" id="add-new-list-btn"
                                    type="button">
                                <%=LM.getText(LC.TASK_TYPE_APPROVAL_PATH_SEARCH_TASK_TYPE_APPROVAL_PATH_SEARCH_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="mt-5">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered text-nowrap" id="product-view-table">
                            <thead>
                                <tr>
                                    <th class="row-data-product-name">
                                        <%=LM.getText(LC.PI_AUCTION_ADD_PRODUCT_NAME, loginDTO)%>
                                    </th>
                                    <th class="row-data-product-supplier-name">
                                        <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_SUPPLIERCOMPANYNAME, loginDTO)%>
                                    </th>

                                    <th class="row-data-buying-date">
                                        <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_BUYINGDATE, loginDTO)%>
                                    </th>
                                    <th class="row-data-fiscal-year">
                                        <%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_FISCALYEARID, loginDTO)%>
                                    </th>
                                    <th class="row-data-unit-buying-price">
                                        <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_UNITBUYINGPRICE, loginDTO)%>
                                    </th>
                                    <th class="row-data-stock">
                                        <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_STOCK, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.USER_SEARCH_DELETE, loginDTO)%></th>
                                </tr>
                            </thead>

                            <tbody></tbody>

                            <tr class="template-row">

                                <td class="row-data-product-name"></td>
                                <td class="row-data-product-supplier-name"></td>
                                <td class="row-data-buying-date"></td>
                                <td class="row-data-fiscal-year"></td>
                                <td class="row-data-unit-buying-price"></td>
                                <td class="row-data-stock"></td>
                                <td class="row-data-add-btn">
                                    <button class='btn btn-sm cancel-btn text-white shadow pl-4'
                                            style="padding-right: 14px;" type="button"
                                            onclick="unSelectRow(this);">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
                <div class="row mt-3">
                    <div class="col-12 text-right">
                        <form class="form-horizontal"
                              action="Pi_auction_itemsServlet?actionType=getSelectedProductData"
                              id="save-economic-code-form" method="POST" onsubmit="return PreprocessBeforeSubmitting()"
                              enctype="multipart/form-data">

                            <input type="hidden" name="budgetMappingId" id="budgetMappingId-input">
                            <input type="hidden" name="productIds" id="productIds-input">

                            <button class="btn btn-sm shadow text-white submit-btn"  id="next-btn"
                                    type="submit">
                                    <%=LM.getText(LC.NAVIGATION_NEXT, loginDTO)%>
                        </form>
                    </div>
                </div>
            </div>

    </div>
</div>

<jsp:include page="productDetailsModal.jsp"/>

<script type="text/javascript">
    const tableDataForm = $("#bigform");


    $('#save-economic-code-forms').submit(async (event) => {
        event.preventDefault();

        const productIds = Array.from(economicSubCodeMap.values());
        $('#productIds-input').val(JSON.stringify(productIds));

        const formElement = event.target;
        const formData = new FormData(formElement);
        const url = "Pi_auction_itemsServlet?actionType=getSelectedProductData";
        const searchParam = new URLSearchParams(formData);

        try {
            const response = await fetch(url, {
                method: 'post',
                body: searchParam
            });
            const json = await response.json();
            if (!json.success) throw new Error(json);

            $('#toast_message').css('background-color', '#04c73c');
            showToast(
                '<%=LM.getText(LC.BUDGET_CODE_SELECTION_ECONOMIC_CODE_SAVED,"Bangla")%>',
                '<%=LM.getText(LC.BUDGET_CODE_SELECTION_ECONOMIC_CODE_SAVED,"English")%>'
            );
        } catch (error) {
            console.error(error);
            $('#toast_message').css('background-color', '#ff6063');
            showToastSticky(
                '<%=LM.getText(LC.BUDGET_CODE_SELECTION_ECONOMIC_CODE_SAVE_FAILED,"Bangla")%>',
                '<%=LM.getText(LC.BUDGET_CODE_SELECTION_ECONOMIC_CODE_SAVE_FAILED,"English")%>'
            );
        }
    });


    function piProductChanged(selectElement) {
        const productId = selectElement.value;
        if (productId === '') {
            $('#kt_content').hide();
            return;
        }

        buildProductModalData(productId);
    }

    function PreprocessBeforeSubmitting() {
        const productIds = Array.from(economicSubCodeMap.values());
        $('#productIds-input').val(JSON.stringify(productIds));
        //return false;

    }

    function init(row) {

    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        initProcurementGoodsModal('0');
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
        document.querySelector('#product-view-table tbody').innerHTML = '';
        nextBtnStateChange(true);
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    $('#add-new-list-btn').on('click', function () {
        let selectedProduct = updateSelectedItem(1);
        $('#product-details-modal').modal();
        buildProductModalData(selectedProduct.iD);
    });

    var subTypeCount = <%=index%>;


    $('#search_proc_modal').on('show.bs.modal', function () {
        event.preventDefault();

        $("#procurementPackageId_<%=index%>").val(-1);
        $("#procurementGoodsTypeId_<%=index%>").html('');
        $("#subType").html('');

    })


    function updateSelectedItem(updateSelected) {
        if (updateSelected == 1) {
            selectedItemFromProcurementModal = lastItemFromProcurementModal;
            return selectedItemFromProcurementModal;
        }
    }


    function initProcurementGoodsModal(row) {

        $('#procurementPackageId_<%=i%>').change(function () {
            showOrHideProcurementGoodsType();
        });

        $('#procurementGoodsTypeId_<%=i%>').change(function () {
            loadProcurementGoodsByType(this.value);
        });

    }

    function showOrHideProcurementGoodsType() {
        if ($('#procurementPackageId_<%=i%>').val() && $('#procurementPackageId_<%=i%>').val() > 0) {
            loadProcurementGoodsType($('#procurementPackageId_<%=i%>').val());

        } else {
            $('#procurementGoodsTypeId_<%=i%>').val(null);

        }

    }

    function loadProcurementGoodsType(packageId) {

        $("#subType").html('');

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (!this.responseText.includes('option')) {
                } else {
                    $('#procurementGoodsTypeId_<%=i%>').html(this.responseText);
                }

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Procurement_packageServlet?actionType=getByPackageId&ID=" + packageId, true);

        if (packageId != undefined && packageId != null && packageId != '') xhttp.send();

    }


    function loadProcurementGoodsByType(goodsTypeId) {

        $("#subType").html('');

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (!this.responseText.includes('option')) {
                } else {

                    var totalResponse = JSON.parse(this.responseText);
                    var options = totalResponse.options;

                    $("#subType").append(options);
                    updateLabels();

                    $('#subType_select_' + subTypeCount).change(function () {
                        loadProcurementGoodsByParent(this);
                    });

                    subTypeCount++;
                }

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Procurement_goodsServlet?actionType=getByGoodsType&ID=" + goodsTypeId + "&index=" + subTypeCount, true);

        if (goodsTypeId != undefined && goodsTypeId != null && goodsTypeId != '') xhttp.send();

    }

    function loadProcurementGoodsByParent(element) {

        var nextIndexes = parseInt(element.id.toString().substring(15)) + 1;
        subTypeCount = nextIndexes;

        while ($("#subType_select_" + nextIndexes).val() != undefined && $("#subType_select_" + nextIndexes).val() != null) {
            $("#div_id_" + nextIndexes).remove();
            nextIndexes++;
        }

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (!this.responseText.includes('option')) {
                } else {

                    var totalResponse = JSON.parse(this.responseText);
                    var options = totalResponse.options;
                    lastItemFromProcurementModal = totalResponse.procurementGoodsResponseDTO;

                    $("#subType").append(options);
                    updateLabels();

                    $('#subType_select_' + subTypeCount).change(function () {
                        loadProcurementGoodsByParent(this);
                    });

                }

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Procurement_goodsServlet?actionType=getByParent&ID=" + element.value + "&index=" + subTypeCount, true);

        if (element.value != undefined && element.value != null && element.value != '') xhttp.send();

    }

    function updateLabels() {
        var goodsLabels = document.getElementById('subType').getElementsByClassName('col-form-label');

        var totalLabels = goodsLabels.length;
        for (var i=0; i< totalLabels - 1; i++) {
            goodsLabels[i].innerHTML = "<%=LM.getText(LC.PROCUREMENT_GOODS_ADD_ID, loginDTO)%>";
        }
        goodsLabels[totalLabels-1].innerHTML = "<%=LM.getText(LC.GATE_PASS_VIEW_ITEM_NAME, loginDTO)%>";
    }

    var selectedItemFromProcurementModal = new Map;
    var lastItemFromProcurementModal = new Map;
    var necessaryCollectionForProcurementModal;


</script>






