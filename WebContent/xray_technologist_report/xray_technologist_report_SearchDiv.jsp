<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="workflow.WorkflowController"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="java.util.*"%>
<%@ page import="employee_offices.*"%>

<%@ page import="pb.*" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.XRAY_TECHNOLOGIST_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row">
    <div class="col-12">
		
        <%@include file="../pbreport/yearmonth.jsp"%>
        <%@include file="../pbreport/calendar.jsp"%>
        
        <div  class="search-criteria-div">
	    <div class="form-group row">
			<label class="col-sm-3 control-label text-right">
				<%=Language.equalsIgnoreCase("english")?"X-Ray Technologist":"এক্স রে টেকনোলজিস্ট"%>
			</label>
			<div class="col-sm-9">
				<select name='xray_technologist_user_name' id = 'xray_technologist_user_name'	class='form-control'>
					<option value = ""><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
				 	<%
				     Set<Long> xrayTech = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.XRAY_GUY);
				
				 
	                 for(Long employee: xrayTech)
	                 {
	                 %>
	                 <option value = "<%=WorkflowController.getUserNameFromOrganogramId(employee)%>" >
	                  <%=WorkflowController.getNameFromOrganogramId(employee, Language)%>
	                 </option>
	                 <%
	                 }
	                 %>
				</select>					
			</div>
		</div>
	</div>
    </div>
    
</div>
<script type="text/javascript">
function init()
{
	dateTimeInit($("#Language").val());
    $("#search_by_date").prop('checked', true);
    $("#search_by_date").trigger("change");
    setDateByStringAndId('startDate_js', '<%=datestr%>');
    setDateByStringAndId('endDate_js', '<%=datestr%>');
    add1WithEnd = true;
    processNewCalendarDateAndSubmit();
}
function PreprocessBeforeSubmiting()
{
}
</script>