<%@ page import="login.LoginDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="language.LM" %>
<%@ page import="language.LC" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="feedback_question_training_calendar_mapping.FeedbackQuestionTrainingCalenderMappingDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="feedback_question.Feedback_questionDTO" %>
<%@ page import="feedback_question.Feedback_questionRepository" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="feedback_question_training_calendar_mapping.FeedbackQuestionTrainingCalenderMappingDAO" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="feedback_question_answer.FeedbackQuestionAnswerDTO" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="training_calendar_details.Training_calendar_detailsDTO" %>
<%@ page import="training_calender.Training_calenderDTO" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="training_calendar_details.Training_calendar_detailsDAO" %>
<%@ page import="util.StringUtils" %>
<%@ page import="feedback_question_answer.FeedbackQuestionAnswerDAO" %>
<%@ page import="pb.OrderByEnum" %>
<%@ page import="training_calender.Training_calenderDAO" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = LM.getText(LC.FAMILY_EDIT_LANGUAGE, loginDTO);
    String formTitle = LM.getText(LC.FEEDBACK_QUESTION_ANSWER_PARTICIPANT_FEEDBACK_FORM, loginDTO);
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    long trainingCalendarId = Long.parseLong(request.getParameter("trainCalId"));
    long empId;
    if (request.getParameter("empId") != null) {
        empId = Long.parseLong(request.getParameter("empId"));
    } else {
        empId = userDTO.employee_record_id;
    }

    Training_calenderDTO training_calenderDTO = Training_calenderDAO.getInstance().getDTOFromID(trainingCalendarId);
    Training_calendar_detailsDTO training_calendar_detailsDTO = new Training_calendar_detailsDAO().getDTOByCalendarIdAndEmployeeRecordId(trainingCalendarId, empId);
    training_calendar_detailsDTO.metaData1 = training_calendar_detailsDTO.metaData1 == null ? "" : training_calendar_detailsDTO.metaData1;
    training_calendar_detailsDTO.metaData2 = training_calendar_detailsDTO.metaData2 == null ? "" : training_calendar_detailsDTO.metaData2;


    List<Feedback_questionDTO> feedbackQuestionDTOList = Feedback_questionRepository.getInstance().getFeedback_questionList();
    Map<Long, Feedback_questionDTO> questionDTOMapById = feedbackQuestionDTOList.stream()
            .collect(Collectors.toMap(dto -> dto.iD, dto -> dto));
    List<FeedbackQuestionTrainingCalenderMappingDTO> mappingDTOList = FeedbackQuestionTrainingCalenderMappingDAO.getInstance().getByTrainingId(trainingCalendarId);
    Map<Long, FeedbackQuestionAnswerDTO> feedbackQuestionAnswerDTOMap = FeedbackQuestionAnswerDAO.getInstance().getFeedbackQuestionAnswerDTOByTrainingCalendarDetailsId(training_calendar_detailsDTO.iD);
    String submitURL = String.format("FeedbackQuestionAnswerServlet?empId=%d&trainCalId=%d",empId,trainingCalendarId);
    if(request.getParameter("data")!=null){
        submitURL += "&data="+request.getParameter("data");
    }
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-8 offset-2">
                <div class="onlyborder">
                    <div class="row">
                        <div class="col-11">
                            <div class="sub_title_top">
                                <div class="kt-subheader   kt-grid__item mb-3"
                                     id="kt_subheader">
                                    <div class="kt-subheader__main">
                                        <h3 class="kt-subheader__title">
                                            <%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_PATICIPANTS_INFORMATION, loginDTO)%>
                                        </h3>
                                    </div>
                                </div>
<%--                                <div class="sub_title">--%>
<%--                                    --%>
<%--                                </div>--%>
                                <div class="table-responsive">
                                    <table id="tableData1" class="table table-bordered table-striped">
                                        <tbody>
                                        <tr>
                                            <td><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_TITLE_OF_THE_TRAINING_PROGRAM, loginDTO)%>
                                            </td>
                                            <td colspan="3"><%=Language.equalsIgnoreCase("English") ? training_calenderDTO.nameEn : training_calenderDTO.nameBn%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="width: 15%"><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_TRAINING_TYPE, loginDTO)%>
                                            </td>
                                            <td style="width: 35%"><%=CatRepository.getInstance().getText(Language, "feedback_ques_ans_point_cat", training_calenderDTO.trainingTypeCat)%>
                                            </td>

                                            <td style="width: 15%"><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_FACILITATOR, loginDTO)%>
                                            </td>
                                            <td style="width: 35%"><%=training_calenderDTO.instituteName%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="width: 15%"><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_START_DATE, loginDTO)%>
                                            </td>
                                            <td style="width: 35%"><%=dateFormat.format(new Date(training_calenderDTO.startDate))%>
                                            </td>

                                            <td style="width: 15%"><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_END_DATE, loginDTO)%>
                                            </td>
                                            <td style="width: 35%"><%=dateFormat.format(new Date(training_calenderDTO.endDate))%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="width: 15%"><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_VENUE, loginDTO)%>
                                            </td>
                                            <td style="width: 35%"><%=training_calenderDTO.venue%>
                                            </td>

                                            <td style="width: 15%"><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_PARTICIPANT_NAME, loginDTO)%>
                                            </td>
                                            <td style="width: 35%"><%=Language.equalsIgnoreCase("English") ? training_calendar_detailsDTO.employeeNameEn : training_calendar_detailsDTO.employeeNameBn%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="width: 15%"><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_PARTICIPANT_OFFICE, loginDTO)%>
                                            </td>
                                            <td style="width: 35%"><%=Language.equalsIgnoreCase("English") ? training_calendar_detailsDTO.officeNameEn : training_calendar_detailsDTO.officeNameBn%>
                                            </td>

                                            <td style="width: 15%"><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_PARTICIPANT_DESIGNATION, loginDTO)%>
                                            </td>
                                            <td style="width: 35%"><%=Language.equalsIgnoreCase("English") ? training_calendar_detailsDTO.organogramNameEn : training_calendar_detailsDTO.organogramNameBn%>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>


                                <br><br>
                                <div class="kt-subheader   kt-grid__item mb-3"
                                     id="kt_subheader">
                                    <div class="kt-subheader__main">
                                        <h3 class="kt-subheader__title"><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_FEEDBACK_QUESTIONNAIRE, loginDTO)%>
                                        </h3>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table id="tableData2" class="table table-bordered table-striped">
                                        <thead class="thead-light">
                                        <tr>
                                            <th style="width: 10%"><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_SERIAL_NO, loginDTO)%>
                                            </th>
                                            <th style="width: 70%"><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_INQUIRY, loginDTO)%>
                                            </th>
                                            <th style="width: 20%"><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_FEEDBACK, loginDTO)%>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <%
                                            int serialNo = 0;
                                            int feedbackPoint;
                                            for (FeedbackQuestionTrainingCalenderMappingDTO mappingDTO : mappingDTOList) {
                                                Feedback_questionDTO tagDTO = questionDTOMapById.get(mappingDTO.feedbackQuestionId);
                                                if (tagDTO != null) {
                                                    serialNo = mappingDTO.sequence;
                                                    feedbackPoint = 0;
                                                    if (feedbackQuestionAnswerDTOMap != null && feedbackQuestionAnswerDTOMap.containsKey(mappingDTO.iD)) {
                                                        feedbackPoint = feedbackQuestionAnswerDTOMap.get(mappingDTO.iD).feedbackAnswerPoint;
                                                    }
                                        %>
                                        <tr>
                                            <td><%=Language.equalsIgnoreCase("English") ? serialNo : StringUtils.convertToBanNumber(Integer.toString(serialNo))%>
                                            </td>
                                            <td><%=Language.equalsIgnoreCase("English") ? tagDTO.questionEng : tagDTO.questionBng%>
                                            </td>
                                            <td>
                                                <select class='form-control rounded'
                                                        name='feedback_answer_<%=serialNo%>'
                                                        onchange="giveSummary()"
                                                        id='feedback_answer_<%=serialNo%>'>
                                                    <%=feedbackPoint == 0 ? CatRepository.getInstance().buildOptions("feedback_ques_ans_point_cat", Language, null, OrderByEnum.DSC) :
                                                            CatRepository.getInstance().buildOptions("feedback_ques_ans_point_cat", Language, feedbackPoint, OrderByEnum.DSC)%>
                                                </select>
                                            </td>
                                        </tr>
                                        <%
                                                }
                                            }
                                        %>

                                        </tbody>
                                    </table>

                                    <table id="tableData3" class="table table-bordered table-striped">
                                        <tbody>
                                        <tr>
                                            <%--                First Question--%>
                                            <td style="width: 10%"><%=Language.equalsIgnoreCase("English") ? ++serialNo : StringUtils.convertToBanNumber(Integer.toString(++serialNo))%>
                                            </td>
                                            <td style="width: 40%"><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_WRITTEN_QUESTION_1, loginDTO)%>
                                            </td>
                                            <td>
                                                    <textarea type='text' class='form-control' name='text_feedback_1'
                                                              id='text_feedback_1' rows="3"
                                                              style="text-align: left"><%=training_calendar_detailsDTO.metaData1%></textarea>
                                            </td>
                                        </tr>

                                        <%--                Second Question--%>
                                        <tr>
                                            <td style="width: 10%"><%=Language.equalsIgnoreCase("English") ? ++serialNo : StringUtils.convertToBanNumber(Integer.toString(++serialNo))%>
                                            </td>
                                            <td style="width: 40%"><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_WRITTEN_QUESTION_2, loginDTO)%>
                                            </td>
                                            <td>
                                                    <textarea type='text' class='form-control' name='text_feedback_2'
                                                              id='text_feedback_2' rows="3"
                                                              style="text-align: left"><%=training_calendar_detailsDTO.metaData2%></textarea>
                                            </td>
                                        </tr>

                                        <%--                summary--%>
                                        <tr>
                                            <td style="width: 10%"><%=Language.equalsIgnoreCase("English") ? ++serialNo : StringUtils.convertToBanNumber(Integer.toString(++serialNo))%>
                                            </td>
                                            <td style="width: 40%"><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_FEEDBACK_SUMMARY, loginDTO)%>
                                            </td>
                                            <td>
                                                    <textarea type='text' class='form-control' name='feedback-summary'
                                                              id='feedback-summary' rows="2" style="text-align: center"
                                                              readonly></textarea>
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form class="form-horizontal"
              action="<%=submitURL%>"
              id="feedback_answer_form" name="bigform" method="POST"
              onsubmit="return PreprocessBeforeSubmiting()">
            <div class="kt-portlet__body form-body">

                <input type="hidden" name="feedback-text-answer-1"
                       id="feedback-text-answer-1">
                <input type="hidden" name="feedback-text-answer-2"
                       id="feedback-text-answer-2">
                <input type="hidden" name="feedback-point" id="feedback-point">
            </div>
            <div class="row">
                <div class="col-8 offset-2">
                    <div class="form-actions text-md-right">
                        <a class="btn btn-danger rounded" href="<%=request.getHeader("referer")%>">
                            <%=LM.getText(LC.DISCIPLINARY_LOG_ADD_DISCIPLINARY_LOG_CANCEL_BUTTON, userDTO)%>
                        </a>
                        <button class="btn btn-success rounded" type="submit">
                            <%=LM.getText(LC.DISCIPLINARY_LOG_ADD_DISCIPLINARY_LOG_SUBMIT_BUTTON, userDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        giveSummary();
    });

    function giveSummary() {
        let feedback_dropdown_id_text;
        let point;
        let summaryPoint = 0;
        let summary = document.getElementById('feedback-summary');
        let averageSummaryPoint = 0;
        let count_feedback_number = 0;


        <%
            String feedback_dropdown_id_sum;
            for (FeedbackQuestionTrainingCalenderMappingDTO mappingDTO : mappingDTOList) {
                        feedback_dropdown_id_sum = "feedback_answer_" + mappingDTO.sequence;
        %>

        feedback_dropdown_id_text = '#' + '<%=feedback_dropdown_id_sum%>';
        point = $(feedback_dropdown_id_text).val();


        if (point === '1' || point === '2' || point === '3' || point === '4' || point === '5') {
            summaryPoint += parseInt(point);
            count_feedback_number++;
        } else {
            return 0;
        }

        <%
            }
        %>


        averageSummaryPoint = Math.floor(summaryPoint / count_feedback_number);
        if (averageSummaryPoint === 1) {
            summary.innerHTML = '<%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_TOO_MUCH_UNSATISFACTORY_TRAINING_FACILITIES, loginDTO)%>';
        } else if (averageSummaryPoint === 2) {
            summary.innerHTML = '<%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_UNSATISFACTORY_TRAINING_FACILITIES, loginDTO)%>';
        } else if (averageSummaryPoint === 3) {
            summary.innerHTML = '<%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_AVERAGE_TRAINING_FACILITIES, loginDTO)%>';
        } else if (averageSummaryPoint === 4) {
            summary.innerHTML = '<%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_SATISFACTORY_TRAINING_FACILITIES, loginDTO)%>';
        } else if (averageSummaryPoint === 5) {
            summary.innerHTML = '<%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_EXTRAORDINARY_TRAINING_FACILITIES, loginDTO)%>';
        }


        return 1;
    }


    function PreprocessBeforeSubmiting() {

        let feedback_dropdown_id_text;
        let point;
        let detailsid_mappingid_point = '';


        <%
            List<FeedbackQuestionAnswerDTO> feedbackQuestionAnswerDTOList = new ArrayList<>();
            String feedback_dropdown_id="";
            for (FeedbackQuestionTrainingCalenderMappingDTO mappingDTO : mappingDTOList) {
                        FeedbackQuestionAnswerDTO feedbackQuestionAnswerDTO = new FeedbackQuestionAnswerDTO();
                        feedbackQuestionAnswerDTO.feedbackMappingId = mappingDTO.iD;
                        feedbackQuestionAnswerDTO.trainingCalenderDetailsId = training_calendar_detailsDTO.iD;
                        // Training Calender Details Id
                        feedback_dropdown_id = "feedback_answer_" + mappingDTO.sequence;
        %>

        feedback_dropdown_id_text = '#' + '<%=feedback_dropdown_id%>';
        point = $(feedback_dropdown_id_text).val();

        if (point === '1' || point === '2' || point === '3' || point === '4' || point === '5') {
            detailsid_mappingid_point += <%=training_calendar_detailsDTO.iD%> +',' + <%=mappingDTO.iD%> +',' + point + ';';
        } else {
            Swal.fire({
                icon: 'alert',
                title: 'Missing Feedback',
                text: 'Please, select all the feedback from dropdown!'
            })

            return false;
        }

        <%
            }
        %>


        $('#feedback-text-answer-1').val($('#text_feedback_1').val());
        $('#feedback-text-answer-2').val($('#text_feedback_2').val());
        $('#feedback-point').val(detailsid_mappingid_point);

        return true;
    }


</script>
