<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="training_calender.*" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="training_calendar_details.Training_calendar_detailsDTO" %>
<%@ page import="training_calendar_details.Training_calendar_detailsDAO" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="feedback_question.FeedbackSummaryDTO" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = LM.getText(LC.FAMILY_EDIT_LANGUAGE, loginDTO);
    String formTitle = LM.getText(LC.FEEDBACK_QUESTION_ANSWER_PARTICIPANT_FEEDBACK_FORM, loginDTO);
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    long trainingCalendarId = Long.parseLong(request.getParameter("ID"));
    long empId = userDTO.employee_record_id;
    Training_calendar_detailsDAO training_calendar_detailsDAO = new Training_calendar_detailsDAO();
    Training_calendar_detailsDTO training_calendar_detailsDTO = training_calendar_detailsDAO.getDTOByCalendarIdAndEmployeeRecordId(trainingCalendarId, empId);

    List<FeedbackSummaryDTO> feedbackSummaryDTOs = (List<FeedbackSummaryDTO>) request.getAttribute("Feedback");
    Training_calenderDTO training_calenderDTO = Training_calenderDAO.getInstance().getDTOFromID(trainingCalendarId);

%>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%>
        </h3>
    </div>
    <div class="box-body">

        <div class="table-responsive">
            <h3><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_PATICIPANTS_INFORMATION, loginDTO)%>
            </h3>
            <table id="tableData1" class="table table-bordered table-striped">
                <thead>

                </thead>
                <tbody>
                <tr>
                    <td><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_TITLE_OF_THE_TRAINING_PROGRAM, loginDTO)%>
                    </td>
                    <td colspan="3"><%=Language.equalsIgnoreCase("English") ? training_calenderDTO.nameEn : training_calenderDTO.nameBn%>
                    </td>
                </tr>

                <tr>
                    <td style="width: 15%"><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_TRAINING_TYPE, loginDTO)%>
                    </td>
                    <td style="width: 35%"><%=CatRepository.getInstance().getText(Language, "feedback_ques_ans_point_cat", training_calenderDTO.trainingTypeCat)%>
                    </td>

                    <td style="width: 15%"><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_FACILITATOR, loginDTO)%>
                    </td>
                    <td style="width: 35%"><%=training_calenderDTO.instituteName%>
                    </td>
                </tr>

                <tr>
                    <td style="width: 15%"><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_START_DATE, loginDTO)%>
                    </td>
                    <td style="width: 35%"><%=dateFormat.format(new Date(training_calenderDTO.startDate))%>
                    </td>

                    <td style="width: 15%"><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_END_DATE, loginDTO)%>
                    </td>
                    <td style="width: 35%"><%=dateFormat.format(new Date(training_calenderDTO.endDate))%>
                    </td>
                </tr>


                </tbody>
            </table>
        </div>
        <div class="table-responsive">
            <h3>Training Feedbacks
            </h3>
            <table id="tableData2" class="table table-bordered table-striped">
                <thead>
                <th>User Name</th>
                <th>Feedback</th>
<%--                <th><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_WRITTEN_QUESTION_1, loginDTO)%>--%>
<%--                </th>--%>
<%--                <th><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_WRITTEN_QUESTION_2, loginDTO)%>--%>
<%--                </th>--%>
                </thead>
                <tbody>
                <%for (FeedbackSummaryDTO feedbackSummaryDTO : feedbackSummaryDTOs) {%>
                <tr>
                    <% String summary = "";
                        int averagePoint = feedbackSummaryDTO.totalPoint / (feedbackSummaryDTO.totalQuestion == 0 ? 1 : feedbackSummaryDTO.totalQuestion);
                        if (averagePoint <= 1) {
                            summary = LM.getText(LC.FEEDBACK_QUESTION_ANSWER_TOO_MUCH_UNSATISFACTORY_TRAINING_FACILITIES, loginDTO);
                        } else if (averagePoint== 2) {
                            summary = LM.getText(LC.FEEDBACK_QUESTION_ANSWER_UNSATISFACTORY_TRAINING_FACILITIES, loginDTO);
                        } else if (averagePoint == 3) {
                            summary = LM.getText(LC.FEEDBACK_QUESTION_ANSWER_AVERAGE_TRAINING_FACILITIES, loginDTO);
                        } else if (averagePoint== 4) {
                            summary = LM.getText(LC.FEEDBACK_QUESTION_ANSWER_SATISFACTORY_TRAINING_FACILITIES, loginDTO);
                        } else if (averagePoint == 5) {
                            summary = LM.getText(LC.FEEDBACK_QUESTION_ANSWER_EXTRAORDINARY_TRAINING_FACILITIES, loginDTO);
                        }%>
                    <td><%=feedbackSummaryDTO.employee_recordsDTO.nameEng%>
                    <td><%=summary%>
                    </td>
<%--                    <td><%=feedbackSummaryDTO.metadata1%>--%>
<%--                    </td>--%>
<%--                    <td><%=feedbackSummaryDTO.metadata2%>--%>
<%--                    </td>--%>
                </tr>
                <%}%>
                </tbody>
            </table>
        </div>
    </div>
</div>