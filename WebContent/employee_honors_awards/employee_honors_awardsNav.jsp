<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="pb.CatRepository" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="util.HttpRequestUtils" %>
<%@page contentType="text/html;charset=utf-8" %>


<%

    String context = "../../.." + request.getContextPath() + "/";
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    RecordNavigator rn = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
%>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1" style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>

    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label" for="award_type_cat"><%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_SEARCH_AWARDTYPECAT, loginDTO)%></label>

                        <div class="col-md-8">
                            <select class='form-control' name='award_type_cat' id='award_type_cat'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=CatRepository.getInstance().buildOptions("award_type", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label" for="titles_of_award"><%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_SEARCH_TITLESOFAWARD, loginDTO)%></label>

                        <div class="col-md-8">
                            <input type="text" class="form-control" id="titles_of_award" placeholder=""
                                   name="titles_of_award" onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label" for="awarding_institution"><%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_SEARCH_AWARDINGINSTITUTION, loginDTO)%>
                        </label>

                        <div class="col-md-8">
                            <input type="text" class="form-control" id="awarding_institution" placeholder=""
                                   name="awarding_institution" onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label
                                class="col-md-4 col-form-label"><%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_SEARCH_AWARDDATE, loginDTO)%>
                        </label>

                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="award_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type='hidden'
                                   class='form-control'
                                   id='award_date'
                                   name='award_date'
                                   value='' tag='pb_html'/>

                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->
<%@include file="../common/pagination_with_go2.jsp" %>

<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">
    $(document).ready(() => {
        select2SingleSelector('#award_type_cat', '<%=Language%>');
    });

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "Employee_honors_awardsServlet?actionType=search&isPermanentTable=true&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        $("#award_date").val(getDateStringById("award_date_js"));
        var params = 'AnyField=' + document.getElementById('anyfield').value;

        params += '&award_type_cat=' + document.getElementById('award_type_cat').value;
        params += '&titles_of_award=' + document.getElementById('titles_of_award').value;
        params += '&awarding_institution=' + document.getElementById('awarding_institution').value;
        params += '&award_date=' + document.getElementById('award_date').value;

        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

</script>

