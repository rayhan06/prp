<%@page import="login.LoginDTO" %>
<%@page import="employee_honors_awards.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="files.*" %>
<%@ page import="user.UserDTO" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@ page import="java.util.List" %>
<%@ page import="pb.CatRepository" %>
<%@page import="dbm.*" %>
<%@ page import="util.HttpRequestUtils" %>


<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    long empId;
    if (request.getParameter("empId") == null) {
        empId = userDTO.employee_record_id;
    } else {
        empId = Long.parseLong(request.getParameter("empId"));
    }
    Employee_honors_awardsDTO employee_honors_awardsDTO;
    String actionName;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        employee_honors_awardsDTO = (Employee_honors_awardsDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    } else {
        actionName = "add";
        employee_honors_awardsDTO = new Employee_honors_awardsDTO();
    }
    String formTitle = LM.getText(LC.EMPLOYEE_HONORS_AWARDS_ADD_EMPLOYEE_HONORS_AWARDS_ADD_FORMNAME, loginDTO);
    int i = 0;
    long ColumnID;

    String url = "Employee_honors_awardsServlet?actionType=" + "ajax_" + actionName + "&isPermanentTable=true&empId=" + empId + "&iD=" + employee_honors_awardsDTO.iD;
    if (request.getParameter("tab") != null) {
        url += "&tab=" + request.getParameter("tab")+"&userId="+request.getParameter("userId");
    }
%>
<style>
    .hon-button-cross-picker {
        height: 34px;
        width: 41px;
        border: 1px solid #c2cad8;
    }

    .input-width92 {
        width: 92%;
        float: left;
    }
</style>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form" action="<%=url%>" id="emp_honor_award_form" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 control-label"
                                                   for="awardTypeCat_category">
                                                <%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_ADD_AWARDTYPECAT, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9 " id='awardTypeCat_div_<%=i%>'>
                                                <select class='form-control' name='awardTypeCat'
                                                        id='awardTypeCat_category' tag='pb_html'>
                                                    <%=CatRepository.getInstance().buildOptions("award_type", Language, employee_honors_awardsDTO.awardTypeCat)%>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 control-label"
                                                   for="titlesOfAward_text_<%=i%>">
                                                <%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_ADD_TITLESOFAWARD, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9 " id='titlesOfAward_div_<%=i%>'>
                                                <input type='text' class='form-control' name='titlesOfAward'
                                                       id='titlesOfAward_text_<%=i%>'
                                                       value='<%=employee_honors_awardsDTO.titlesOfAward!=null?StringEscapeUtils.escapeHtml4(employee_honors_awardsDTO.titlesOfAward):""%>'
                                                       tag='pb_html'/>
                                            </div>

                                        </div>
                                        <div id="awardingInstitution_div">
                                            <div class="form-group row">
                                                <label class="col-md-3 control-label"
                                                       for="awardingInstitution_text">
                                                    <%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_ADD_AWARDINGINSTITUTION, loginDTO)%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9 " id='awardingInstitution_div_inner'>
                                                    <input type='text' class='form-control'
                                                           name='awardingInstitution'
                                                           id='awardingInstitution_text'
                                                           value='<%=employee_honors_awardsDTO.awardingInstitution!=null?StringEscapeUtils.escapeHtml4(employee_honors_awardsDTO.awardingInstitution):""%>'
                                                           tag='pb_html'/>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 control-label" for="ground_text_<%=i%>">
                                                <%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_ADD_GROUND, loginDTO)%>
                                            </label>
                                            <div class="col-md-9 " id='ground_div_<%=i%>'>
                                                <input type='text' class='form-control' name='ground'
                                                       id='ground_text_<%=i%>'
                                                       value='<%=employee_honors_awardsDTO.ground!=null?StringEscapeUtils.escapeHtml4(employee_honors_awardsDTO.ground):""%>'
                                                       tag='pb_html'/>
                                            </div>

                                        </div>


                                        <div class="form-group row">
                                            <label class="col-md-3 control-label">
                                                <%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_ADD_AWARDDATE, loginDTO)%>
                                            </label>
                                            <div class="col-md-9 " id='awardDate_div_<%=i%>'>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID"
                                                               value="award-date-js"></jsp:param>
                                                    <jsp:param name="LANGUAGE"
                                                               value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' class='form-control' id='award-date'
                                                       name='awardDate' value='' tag='pb_html'/>
                                            </div>

                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 control-label"
                                                   for="briefDescription_text_<%=i%>">
                                                <%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_ADD_BRIEFDESCRIPTION, loginDTO)%>
                                            </label>
                                            <div class="col-md-9 " id='briefDescription_div_<%=i%>'>
                                                            <textarea class='form-control' name='briefDescription'
                                                                      id='briefDescription_text_<%=i%>'
                                                                      style="resize: none" rows="4" maxlength="1024"
                                                                      tag='pb_html'
                                                            ><%=employee_honors_awardsDTO.briefDescription != null ? StringEscapeUtils.escapeHtml4(employee_honors_awardsDTO.briefDescription) : ""%></textarea>
                                            </div>

                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 control-label">
                                                <%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_ADD_FILESDROPZONE, loginDTO)%>
                                            </label>
                                            <div class="col-md-9 " id='filesDropzone_div_<%=i%>'>
                                                <%
                                                    if (actionName.equals("edit")) {
                                                        List<FilesDTO> filesDropzoneDTOList = new FilesDAO().getMiniDTOsByFileID(employee_honors_awardsDTO.filesDropzone);
                                                %>
                                                <table>
                                                    <tr>
                                                        <%
                                                            if (filesDropzoneDTOList != null) {
                                                                for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                                                    FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                                                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                        %>
                                                        <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                                            <%
                                                                if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                            %>
                                                            <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                                 style='width:100px'/>
                                                            <%
                                                                }
                                                            %>
                                                            <a href='Employee_honors_awardsServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                               download><%=filesDTO.fileTitle%>
                                                            </a>
                                                            <a class='btn btn-danger'
                                                               onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>
                                                        </td>
                                                        <%
                                                                }
                                                            }
                                                        %>
                                                    </tr>
                                                </table>
                                                <%
                                                    }
                                                %>

                                                <%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>
                                                <div class="dropzone"
                                                     action="Employee_honors_awardsServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?employee_honors_awardsDTO.filesDropzone:ColumnID%>">
                                                    <input type='file' style="display:none"
                                                           name='filesDropzoneFile'
                                                           id='filesDropzone_dropzone_File_<%=i%>'
                                                           tag='pb_html'/>
                                                </div>
                                                <input type='hidden' name='filesDropzoneFilesToDelete'
                                                       id='filesDropzoneFilesToDelete_<%=i%>' value=''
                                                       tag='pb_html'/>
                                                <input type='hidden' name='filesDropzone'
                                                       id='filesDropzone_dropzone_<%=i%>' tag='pb_html'
                                                       value='<%=actionName.equals("edit")?employee_honors_awardsDTO.filesDropzone:ColumnID%>'/>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                type="button" onclick="location.href = '<%=request.getHeader("referer")%>'">
                            <%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_ADD_EMPLOYEE_HONORS_AWARDS_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button class="btn-sm shadow text-white border-0 submit-btn ml-2" id="submit-btn" type="submit">
                            <%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_ADD_EMPLOYEE_HONORS_AWARDS_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>

<script type="text/javascript">
    const language = "<%=Language%>";
    const isLangEng = '<%=HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng%>';
    const awardForm = $('#emp_honor_award_form');
    const internal = isLangEng ? 'INTERNAL' : 'অভ্যন্তরীন';
    $(document).ready(function () {
        select2SingleSelector("#awardTypeCat_category", '<%=Language%>')
        if ('<%=actionName%>' === 'edit') {
            setDateByTimestampAndId('award-date-js', <%=employee_honors_awardsDTO.awardDate%>);
        }
        dateTimeInit("<%=Language%>");
        select2SingleSelector("#awardTypeCat_category", '<%=Language%>');
        showOrHideInstitutionDiv();
        $("#awardTypeCat_category").change(showOrHideInstitutionDiv);
        $.validator.addMethod('awardCategory', function (value, element) {
            return value != 0;
        });
        $("#emp_honor_award_form").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                titlesOfAward: {
                    required: true
                },
                awardTypeCat: {
                    required: true,
                    awardCategory: true
                },
                awardingInstitution: {
                    required: function () {
                        if ($('#awardingInstitution_div').is(":visible")) {
                            return true;
                        }
                        return false;
                    }
                },
            },
            messages: {
                titlesOfAward: "<%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_ADD_EMPLOYEE_HONORS_PLEASE_ENTER_AWARDS_TITLE, userDTO)%>",
                awardTypeCat: "<%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_ADD_EMPLOYEE_HONORS_PLEASE_SELECT_A_AWARD_TYPE, userDTO)%>",
                awardingInstitution: "<%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_ADD_EMPLOYEE_HONORS_PLEASE_ENTER_NAME_OF_INSTRITUTION, userDTO)%>",
            }
        });

        $('#submit-btn').click(e => {
            e.preventDefault();
            submitForm();
        })
    });

    function showOrHideInstitutionDiv() {
        if ($("#awardTypeCat_category option:selected").text() == internal) {
            $("#awardingInstitution_text").val("");
            $('#awardingInstitution_div').hide();
        } else {
            $('#awardingInstitution_div').show();
        }
    }

    function PreprocessBeforeSubmitting() {
        $('#award-date').val(getDateStringById('award-date-js'));
        $("#emp_honor_award_form").validate();
        const dateValid = dateValidator('award-date-js', false, {
            'errorEn': 'Enter valid date!',
            'errorBn': 'সঠিক তারিখ প্রবেশ করান!'
        });
        awardForm.validate();
        const jQueryValid = awardForm.valid();
        return jQueryValid && dateValid;
    }

    function submitForm() {
        buttonStateChangeFunction(true);
        if (PreprocessBeforeSubmitting()) {
            submitAjaxForm('emp_honor_award_form');
        } else {
            buttonStateChangeFunction(false);
        }
    }

    function buttonStateChangeFunction(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

</script>