<%@page import="util.StringUtils" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="employee_honors_awards.*" %>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="files.*" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="util.HttpRequestUtils" %>


<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    Employee_honors_awardsDTO employee_honors_awardsDTO = (Employee_honors_awardsDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.EMPLOYEE_PUBLICATION_SEARCH_EMPLOYEE_HONORS_AWARDS_DETAILS, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-10 offset-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background-color: white"><%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_SEARCH_ANYFIELD, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped text-nowrap">
                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_EDIT_AWARDTYPECAT, loginDTO)%>
                                                </b></td>
                                            <td>
                                                <%=CatRepository.getInstance().getText(Language, "award_type", employee_honors_awardsDTO.awardTypeCat)%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_EDIT_TITLESOFAWARD, loginDTO)%>
                                                </b></td>
                                            <td>
                                                <%=employee_honors_awardsDTO.titlesOfAward == null ? "" : employee_honors_awardsDTO.titlesOfAward%>
                                            </td>
                                        </tr>


                                        <%if (employee_honors_awardsDTO.awardTypeCat == 2) { %>
                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_EDIT_AWARDINGINSTITUTION, loginDTO)%>
                                                </b></td>
                                            <td>
                                                <%=employee_honors_awardsDTO.awardingInstitution == null ? "" : employee_honors_awardsDTO.awardingInstitution%>
                                            </td>
                                        </tr>
                                        <%}%>


                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_EDIT_GROUND, loginDTO)%>
                                                </b></td>
                                            <td>
                                                <%=employee_honors_awardsDTO.ground == null ? "" : employee_honors_awardsDTO.ground%>
                                            </td>
                                        </tr>


                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_EDIT_AWARDDATE, loginDTO)%>
                                                </b></td>
                                            <td>
                                                <%=StringUtils.getFormattedDate(Language, employee_honors_awardsDTO.awardDate)%>
                                            </td>
                                        </tr>


                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_EDIT_BRIEFDESCRIPTION, loginDTO)%>
                                                </b></td>
                                            <td>
                                                <%=employee_honors_awardsDTO.briefDescription == null ? "" : employee_honors_awardsDTO.briefDescription%>
                                            </td>
                                        </tr>


                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.VM_INCIDENT_ADD_FILESDROPZONE, loginDTO)%>
                                                </b></td>
                                            <td>
                                                <%
                                                    {
                                                        List<FilesDTO> FilesDTOList = new FilesDAO().getMiniDTOsByFileID(employee_honors_awardsDTO.filesDropzone);
                                                %>
                                                <table>
                                                    <tr>
                                                        <%
                                                            if (FilesDTOList != null) {
                                                                for (int j = 0; j < FilesDTOList.size(); j++) {
                                                                    FilesDTO filesDTO = FilesDTOList.get(j);
                                                                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                        %>
                                                        <td>
                                                            <%
                                                                if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                            %>
                                                            <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                                 style='width:100px'/>
                                                            <%
                                                                }
                                                            %>
                                                            <a href='Employee_honors_awardsServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                               download><%=filesDTO.fileTitle%>
                                                            </a>
                                                        </td>
                                                        <%
                                                                }
                                                            }
                                                        %>
                                                    </tr>
                                                </table>
                                                <%
                                                    }
                                                %>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>