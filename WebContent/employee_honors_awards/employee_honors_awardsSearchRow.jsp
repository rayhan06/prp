<%@page import="employee_records.Employee_recordsRepository" %>
<%@page import="util.StringUtils" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@ page import="pb.CatRepository" %>
<%
    String empId = request.getParameter("empId");
    if (empId == null) {
        empId = String.valueOf(employee_honors_awardsDTO.employeeRecordsId);
    }
%>
<td>
    <%=Employee_recordsRepository.getInstance().getEmployeeName(employee_honors_awardsDTO.employeeRecordsId, Language)%>
</td>

<td>
    <%=CatRepository.getInstance().getText(Language, "award_type", employee_honors_awardsDTO.awardTypeCat)%>
</td>

<td>
    <%=StringEscapeUtils.escapeHtml4(employee_honors_awardsDTO.titlesOfAward)%>
</td>

<td>
    <%=StringEscapeUtils.escapeHtml4(employee_honors_awardsDTO.awardingInstitution)%>
</td>

<td>
    <%=StringEscapeUtils.escapeHtml4(employee_honors_awardsDTO.ground)%>
</td>

<td>
    <%=StringUtils.getFormattedDate(Language, employee_honors_awardsDTO.awardDate)%>
</td>

<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Employee_honors_awardsServlet?actionType=view&ID=<%=employee_honors_awardsDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Employee_honors_awardsServlet?actionType=getEditPage&ID=<%=employee_honors_awardsDTO.iD%>&empId=<%=empId%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=employee_honors_awardsDTO.iD%>'/></span>
    </div>
</td>