<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="employee_honors_awards.*" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.List" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EMPLOYEE_HONORS_AWARDS_EDIT_LANGUAGE, loginDTO);
    RecordNavigator rn2 = (RecordNavigator) request.getAttribute("recordNavigator");
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
%>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_ADD_EMPLOYEERECORDSID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_EDIT_AWARDTYPECAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_EDIT_TITLESOFAWARD, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_EDIT_AWARDINGINSTITUTION, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_EDIT_GROUND, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_EDIT_AWARDDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_SEARCH_EMPLOYEE_HONORS_AWARDS_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="text-center">
                <span><%="English".equalsIgnoreCase(Language)?"All":"সকল"%></span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Employee_honors_awardsDTO> data = (List<Employee_honors_awardsDTO>) rn2.list;
                if (data != null && data.size()>0) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (Employee_honors_awardsDTO employee_honors_awardsDTO : data) {
        %>
                        <tr> <%@include file="employee_honors_awardsSearchRow.jsp"%> </tr>
        <% } }  %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>