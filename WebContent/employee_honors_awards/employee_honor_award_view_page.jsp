<%@ page import="employee_honors_awards.EmployeeAwardHonorModel" %>
<%@ page import="java.util.List" %>

<input type="hidden" data-ajax-marker="true">

<table class="table table-striped table-bordered" style="font-size: 14px">
    <thead>
    <tr>
        <th><b><%= LM.getText(LC.EMPLOYEE_HONORS_AWARDS_EDIT_AWARDTYPECAT, loginDTO) %>
        </b></th>
        <th><b><%= LM.getText(LC.EMPLOYEE_HONORS_AWARDS_EDIT_AWARDINGINSTITUTION, loginDTO) %>
        </b></th>
        <th><b><%= LM.getText(LC.EMPLOYEE_HONORS_AWARDS_EDIT_BRIEFDESCRIPTION, loginDTO) %>
        </b></th>
        <th><b><%= LM.getText(LC.EMPLOYEE_HONORS_AWARDS_EDIT_TITLESOFAWARD, loginDTO) %>
        </b></th>
        <th><b><%= LM.getText(LC.EMPLOYEE_HONORS_AWARDS_EDIT_GROUND, loginDTO) %>
        </b></th>
        <th><b><%= LM.getText(LC.EMPLOYEE_HONORS_AWARDS_EDIT_AWARDDATE, loginDTO) %>
        </b></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <%
        List<EmployeeAwardHonorModel> savedAwardModelList = (List<EmployeeAwardHonorModel>) request.getAttribute("savedAwardModelList");
        int awardIndex = 0;
        if (savedAwardModelList != null && savedAwardModelList.size() > 0) {
            for (EmployeeAwardHonorModel awardHonorModel : savedAwardModelList) {
                ++awardIndex;
    %>
    <tr>
        <%@include file="/employee_honors_awards/employee_honor_awards_item.jsp" %>
    </tr>
    <%
            }
        }
    %>
    </tbody>
</table>
<div class="row">
    <div class="col-12 mt-3 text-right">
        <button class="btn btn-gray m-t-10 rounded-pill"
                onclick="location.href='<%=request.getContextPath()%>/Employee_honors_awardsServlet?actionType=getAddPage&tab=6&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
            <i class="fa fa-plus"></i>&nbsp;<%= LM.getText(LC.HR_MANAGEMENT_OTHERS_ADD_AWARD, loginDTO) %>
        </button>
    </div>
</div>