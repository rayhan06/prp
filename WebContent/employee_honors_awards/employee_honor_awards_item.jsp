<%@page contentType="text/html;charset=utf-8" %>
<td style="vertical-align: middle;"><%=isLanguageEnglish ? awardHonorModel.awardEng : awardHonorModel.awardBan%>
</td>
<td style="vertical-align: middle;"><%=awardHonorModel.awardingInstitution%>
</td>
<td style="vertical-align: middle;"><%=awardHonorModel.briefDescription%>
</td>
<td style="vertical-align: middle;"><%=awardHonorModel.titlesOfAward%>
</td>
<td style="vertical-align: middle;"><%=awardHonorModel.ground%>
</td>
<td style="vertical-align: middle;"><%=isLanguageEnglish ? awardHonorModel.awardDateEng : awardHonorModel.awardDateBan%>
</td>

<td style="text-align: center; vertical-align: middle;">
    <form action="Employee_honors_awardsServlet?isPermanentTable=true&actionType=delete&tab=6&ID=<%=awardHonorModel.dto.iD%>&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>"
          method="POST" id="tableForm_award_honor<%=awardIndex%>" enctype="multipart/form-data">

        <div class="btn-group" role="group" aria-label="Basic example">
            <button class="btn-primary" type="button" title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_VIEW_DETAILS, loginDTO) %>"
                    onclick="location.href='Employee_honors_awardsServlet?actionType=view&iD=<%=awardHonorModel.dto.iD%>&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'"><i class="fa fa-eye"></i></button>&nbsp
            <button class="btn-success" title="Edit" type="button"
                    onclick="location.href='<%=request.getContextPath()%>/Employee_honors_awardsServlet?actionType=getEditPage&tab=6&iD=<%=awardHonorModel.dto.iD%>&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
                <i class="fa fa-edit"></i></button>&nbsp
            <button class="btn-danger" title="Delete" type="button"
                    onclick="deleteItem('tableForm_award_honor',<%=awardIndex%>)"><i
                    class="fa fa-trash"></i></button>
        </div>
    </form>
</td>