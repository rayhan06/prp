<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="gate_pass.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="files.*" %>
<%@page import="util.StringUtils" %>
<%@ page import="org.apache.commons.codec.language.bm.Lang" %>
<%@ page import="parliament_gallery.Parliament_galleryDAO" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.Utils" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.GATE_PASS_EDIT_LANGUAGE, loginDTO);

    Gate_passDAO gate_passDAO = new Gate_passDAO("gate_pass");

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    GatePassModel gatePassModel = (GatePassModel) request.getAttribute("GatePassModel");

    String viewPageTitle = Language.equalsIgnoreCase("English") ? (gatePassModel.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ? gatePassModel.gatePassTypeEng + "(" + gatePassModel.gatePassSubTypeEng + ")" : gatePassModel.gatePassTypeEng) :
            (gatePassModel.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ? gatePassModel.gatePassTypeBng + "(" + gatePassModel.gatePassSubTypeBng + ")" : gatePassModel.gatePassTypeBng);

    if (Language.equalsIgnoreCase("English")) {
        viewPageTitle = viewPageTitle.substring(0, 1).toUpperCase() + viewPageTitle.substring(1);
    }

    String context = request.getContextPath() + "/";
%>

<style>
    .font-color {
        color: #00a1d4 !important;
    }
</style>

<!-- begin:: Subheader -->
<div class="ml-auto mr-4 mt-4">
    <button type="button" class="print-btn border-0" id='printer1'
            onclick="downloadPdf('<%=gatePassModel.visitorName%>(' + '<%=gatePassModel.visitingDateEng%>' + ').pdf', 'modalbody')">
        <i class="fa fa-file-pdf fa-2x" style="color: gray; cursor: pointer" aria-hidden="true"></i>
    </button>
    <button type="button" class="print-btn border-0" id='printer2'
            onclick="printAnyDiv('modalbody')">
        <i class="fa fa-print fa-2x" style="color: gray; cursor: pointer" aria-hidden="true"></i>
    </button>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content" id="kt_content">
    <div class="" id="modalbody">
        <div class="kt-portlet" style="background-color: #F2F2F2!important;">
            <%--            need to change the background color--%>
            <div class="kt-portlet__body m-4" style="background-color: #f6f9fb">
                <div class="row">
                    <div class="col-md-3 mb-4 mb-md-0">
                        <span class="font-weight-bold lead font-color">
                            <%=LM.getText(LC.GATE_PASS_VIEW_GATE_NO, loginDTO)%>:
                        </span>
                        <span class="font-weight-bold lead font-color">
                            <%=Language.equalsIgnoreCase("English") ? gatePassModel.parliamentGateEng : gatePassModel.parliamentGateBng%>
                        </span>
                    </div>
                    <div class="col-md-6 text-center">
                        <img
                                width="23%"
                                src="<%=context%>assets/static/parliament_logo.png"
                                alt="logo"
                                class="logo-default"
                        />
                        <h1 class="font-color mt-3">
                            <%=Language.equalsIgnoreCase("English") ? "BANGLADESH NATIONAL PARLIAMENT" : "বাংলাদেশ জাতীয় সংসদ"%>
                        </h1>
                        <h3>
                            <%=Language.equalsIgnoreCase("English") ? "Parliament Area Pass" : "সংসদ এলাকা পাস"%>
                        </h3>
                    </div>
                </div>
                <div class="mt-5">
                    <div class="row my-3">
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VIEW_VISITING_DATE, loginDTO)%>
                            </span>
                        </div>
                        <div class="col-md-3">
                            <span>
                                 <%
                                     String today;
                                     if (gatePassModel.gatePassSubTypeEng.equalsIgnoreCase("official") && gatePassModel.firstLayerApprovalStatus == Gate_passDAO.approvalStatusApproved) {
                                         today = simpleDateFormat.format(new Date());
                                 %>
                                        <%=Utils.getDigits(today, Language)%>
                                <%
                                } else {
                                %>
                                    <%=Language.equalsIgnoreCase("English") ? gatePassModel.visitingDateEng : gatePassModel.visitingDateBng%>
                                <%
                                    }
                                %>
                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VIEW_MOBILE, loginDTO)%>
                            </span>
                        </div>
                        <div class="col-md-2">
                            <span>
                                 <%=Language.equalsIgnoreCase("English") ? gatePassModel.visitorMobileNumberEng : gatePassModel.visitorMobileNumberBng%>
                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VIEW_CREDENTIAL, loginDTO)%>
                            </span>
                        </div>
                        <div class="col-md-2">
                            <span>
                                 <%=Language.equalsIgnoreCase("English") ? gatePassModel.visitorCredentialTypeEng + "-" + gatePassModel.visitorCredentialNo :
                                         gatePassModel.visitorCredentialTypeBng + " - " + StringUtils.convertToBanNumber(gatePassModel.visitorCredentialNo)%>
                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VIEW_VISITOR_NAME, loginDTO)%>
                            </span>
                        </div>
                        <div class="col-md-2">
                            <span>
                                 <%=gatePassModel.visitorName%>
                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VIEW_VISITOR_FATHER_NAME, loginDTO)%>
                            </span>
                        </div>
                        <div class="col-md-2">
                            <span>
                                 <%=gatePassModel.visitorFatherName%>
                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VIEW_VISITOR_MOTHER_NAME, loginDTO)%>
                            </span>
                        </div>
                        <div class="col-md-2">
                            <span>
                                 <%=gatePassModel.visitorMotherName%>
                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_PERMANENTADDRESS, loginDTO)%>
                            </span>
                        </div>
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_DIVISION, loginDTO)%>
                            </span>
                            <br>
                            <span>
                                <%=Language == "English" ? gatePassModel.permanentAddress.divisionEng : gatePassModel.permanentAddress.divisionBng%>
                            </span>
                        </div>
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_DISTRICT, loginDTO)%>
                            </span>
                            <br>
                            <span>
                                <%=Language == "English" ? gatePassModel.permanentAddress.districtEng : gatePassModel.permanentAddress.districtBng%>
                            </span>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_UPOZILLA, loginDTO)%>
                            </span>
                            <br>
                            <span>
                                <%=Language == "English" ? gatePassModel.permanentAddress.thanaEng : gatePassModel.permanentAddress.thanaBng%>
                            </span>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_UNION, loginDTO)%>
                            </span>
                            <br>
                            <span>
                                <%=Language == "English" ? gatePassModel.permanentAddress.unionEng : gatePassModel.permanentAddress.unionBng%>
                            </span>
                        </div>
                        <div class="col-md-4 offset-md-2 my-4">
                            <span class="font-color font-weight-bold">
                               <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_VILLAGE, loginDTO)%>
                            </span>
                            <br>
                            <span>
                                 <%=gatePassModel.permanentAddress.addressText%>
                            </span>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_CURRENTADDRESS, loginDTO)%>
                            </span>
                        </div>
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_DIVISION, loginDTO)%>
                            </span>
                            <br>
                            <span>
                                <%=Language == "English" ? gatePassModel.currentAddress.divisionEng : gatePassModel.currentAddress.divisionBng%>
                            </span>
                        </div>
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_DISTRICT, loginDTO)%>
                            </span>
                            <br>
                            <span>
                                <%=Language == "English" ? gatePassModel.currentAddress.districtEng : gatePassModel.currentAddress.districtBng%>
                            </span>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_UPOZILLA, loginDTO)%>
                            </span>
                            <br>
                            <span>
                                <%=Language == "English" ? gatePassModel.currentAddress.thanaEng : gatePassModel.currentAddress.thanaBng%>
                            </span>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_UNION, loginDTO)%>
                            </span>
                            <br>
                            <span>
                                <%=Language == "English" ? gatePassModel.currentAddress.unionEng : gatePassModel.currentAddress.unionBng%>
                            </span>
                        </div>
                        <div class="col-md-4 offset-md-2 my-4">
                            <span class="font-color font-weight-bold">
                               <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_VILLAGE, loginDTO)%>
                            </span>
                            <br>
                            <span>
                                 <%=gatePassModel.currentAddress.addressText%>
                            </span>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VIEW_OFFICER_TYPE, loginDTO)%>
                            </span>
                        </div>
                        <div class="col-md-2">
                            <span>
                                 <%=Language.equalsIgnoreCase("English") ? gatePassModel.officerTypeEng : gatePassModel.officerTypeBng %>
                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VIEW_GENDER, loginDTO)%>
                            </span>
                        </div>
                        <div class="col-md-2">
                            <span>
                                 <%=Language.equalsIgnoreCase("English") ? gatePassModel.genderTypeEng : gatePassModel.genderTypeBng %>
                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                    </div>
                    <%if ((gatePassModel.gatePassTypeEng.equalsIgnoreCase("parliament area pass") || gatePassModel.gatePassTypeEng.equalsIgnoreCase("meeting/briefing pass") || gatePassModel.gatePassTypeEng.equalsIgnoreCase("visitor pass(parliament building entry)")) && (gatePassModel.persons.size() > 0)) {%>
                    <div class="row my-3">
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VIEW_AFFILIATED_PERSONS, loginDTO)%>
                            </span>
                        </div>
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">
                                 <%=LM.getText(LC.GATE_PASS_VIEW_VISITOR_NAME, loginDTO)%>
                            </span>
                            <%
                                for (GatePassPersonModel model : gatePassModel.persons) {
                            %>
                            <br>
                            <span>
                                <%=model.name%>
                            </span>
                            <%
                                }
                            %>
                        </div>
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">
                                 <%=LM.getText(LC.GATE_PASS_VIEW_MOBILE, loginDTO)%>
                            </span>
                            <%
                                for (GatePassPersonModel model : gatePassModel.persons) {
                            %>
                            <br>
                            <span>
                                <%=Language.equalsIgnoreCase("English") ? model.mobileNoEng : model.mobileNoBng%>
                            </span>
                            <%
                                }
                            %>
                        </div>
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VIEW_CREDENTIAL, loginDTO)%>
                            </span>
                            <%
                                for (GatePassPersonModel model : gatePassModel.persons) {
                            %>
                            <br>
                            <span>
                                <%=Language.equalsIgnoreCase("English") ? model.credentialTypeEng + "-" + model.credentialNo : model.credentialTypeBng + "-" + StringUtils.convertToBanNumber(model.credentialNo)%>
                            </span>
                            <%
                                }
                            %>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                    </div>
                    <%}%>
                    <%if ((gatePassModel.gatePassTypeEng.equalsIgnoreCase("visitor pass(parliament building entry)") || gatePassModel.gatePassTypeEng.equalsIgnoreCase("meeting/briefing pass")) && (gatePassModel.items.size() > 0)) {%>
                    <div class="row my-3">
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VIEW_PERMITTED_ITEMS, loginDTO)%>
                            </span>
                        </div>
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">
                                 <%=LM.getText(LC.GATE_PASS_VIEW_ITEM_NAME, loginDTO)%>
                            </span>
                            <%
                                for (GatePassItemModel model : gatePassModel.items) {
                            %>
                            <br>
                            <span>
                                <%=Language.equalsIgnoreCase("English") ? model.nameEng : model.nameBng%>
                            </span>
                            <%
                                }
                            %>
                        </div>
                        <div class="col-md-2">
                            <span class="font-color font-weight-bold">
                                <%=LM.getText(LC.GATE_PASS_VIEW_ITEM_AMOUNT, loginDTO)%>
                            </span>
                            <%
                                for (GatePassItemModel model : gatePassModel.items) {
                            %>
                            <br>
                            <span>
                                <%=Language.equalsIgnoreCase("English") ? model.approvedAmount : StringUtils.convertToBanNumber(String.valueOf(model.approvedAmount))%>
                            </span>
                            <%
                                }
                            %>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                        <div class="col-md-3">
                            <span class="font-color font-weight-bold">

                            </span>
                            <br>
                            <span>

                            </span>
                        </div>
                    </div>
                    <%}%>
                </div>
                <div class="my-5">
                    <span class="font-color font-weight-bold">
                        <%=Language.equalsIgnoreCase("English") ? "N.B: This pass have to return to the gate office!" : "বিঃ দ্রঃ -পাসটি ইস্যুকৃত গেইটে ফেরত প্রদান করতে হবে"%>
                    </span>
                </div>
                <div class="my-5 row">
                    <%--                    <div class="col-12 text-right">--%>
                    <%--                        &lt;%&ndash;                        need to add button&ndash;%&gt;--%>
                    <%--                    </div>--%>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        if (('<%=gatePassModel.gatePassType%>' == '1' && '<%=gatePassModel.firstLayerApprovalStatus%>' == '1')
            || ('<%=gatePassModel.gatePassType%>' != '1' && '<%=gatePassModel.secondLayerApprovalStatus%>' == '1')) {
            // do nothing
        } else {
            $('.print-btn').prop('disabled', 'true');
        }
    });
    $('.print-btn').on('click', () => {
        if ('<%=gatePassModel.gatePassType%>' == '1') {
            let url = "Gate_passServlet?actionType=deletePass&gate_pass_id=<%=gatePassModel.gatePassId%>";
            $.ajax({
                url: url,
                type: "GET",
                async: true,
                success: function () {
                    console.log('Gate pass has been printed!');
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    });
</script>