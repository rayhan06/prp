<%@page pageEncoding="UTF-8" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="gate_pass.*" %>
<%@page import="java.util.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="approval_execution_table.*" %>
<%@ page import="approval_path.*" %>
<%@ page import="user.*" %>
<%@page import="workflow.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="parliament_gate.Parliament_gateRepository" %>
<%@ page import="gate_pass_type.Gate_pass_typeRepository" %>
<%@ page import="gate_pass_sub_type.Gate_pass_sub_typeRepository" %>
<%@ page import="com.google.gson.JsonParser" %>
<%@ page import="com.google.gson.JsonArray" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = LM.getText(LC.GATE_PASS_EDIT_LANGUAGE, loginDTO);

    String context = request.getContextPath() + "/";
%>

<style>
    .main{
        height: 100%;
        width: 100%;
        background-color: white;
    }
    .title {
        color: #00a1d4;
    }
</style>

<div class="main">
    <main>
        <div class="w-100 text-center mt-5">
            <h1 class="title">
                <%=Language.equalsIgnoreCase("English") ? "Parliament Area Security Service" : "সংসদ এলাকার নিরাপত্তা সেবা"%>
            </h1>
        </div>
        <div class="w-100 text-center mt-5 pt-3">
            <img
                    width="15%"
                    src="<%=context%>assets/static/parliament_logo.png"
                    alt="logo"
                    class="logo-default"
            />
        </div>
        <div class="w-100 text-center mt-5 pt-3">
            <a class="btn-lg btn-primary text-white" style="border-radius: 8px; background-color: #19b8ad;"
               href="<%=request.getContextPath() + "/Gate_passServlet?actionType=parliamentAreaSecuritySearch"%>"><%=Language.equalsIgnoreCase("English") ? "Inspect Pass" : "পাস দেখুন"%>
            </a>
<%--            <a class="btn-lg btn-primary mx-3 text-white" style="border-radius: 8px; background-color: #4a87e2;"--%>
<%--               href="<%=request.getContextPath() + "/Gate_passServlet?actionType=getAddPageAtGate"%>"><%=Language.equalsIgnoreCase("English") ? "Apply for Pass" : "পাসের জন্য আবেদন করুন"%>--%>
<%--            </a>--%>
            <a class="btn-lg btn-primary mx-3 text-white" style="border-radius: 8px; background-color: #4a87e2;"
               href="<%=request.getContextPath() + "/Gate_passServlet?actionType=getAddPage&fromGate=true"%>"><%=Language.equalsIgnoreCase("English") ? "Apply for Pass" : "পাসের জন্য আবেদন করুন"%>
            </a>
        </div>
    </main>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">
</script>






