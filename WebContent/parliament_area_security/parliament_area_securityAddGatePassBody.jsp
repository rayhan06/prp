<%@page pageEncoding="UTF-8" %>
<%@page import="com.google.gson.JsonArray" %>
<%@page import="com.google.gson.JsonParser" %>
<%@page import="dbm.DBMW" %>
<%@page import="files.FilesDAO" %>
<%@page import="gate_pass.Gate_passDTO" %>

<%@page import="gate_pass_sub_type.Gate_pass_sub_typeRepository" %>
<%@page import="gate_pass_type.Gate_pass_typeRepository" %>
<%@page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="parliament_gate.Parliament_gateRepository" %>

<%@page import="pb.CatDAO" %>
<%@page import="pb.CommonDAO" %>

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="parliament_gate.Parliament_gateRepository" %>
<%@ page import="gate_pass_type.Gate_pass_typeRepository" %>
<%@ page import="gate_pass_sub_type.Gate_pass_sub_typeRepository" %>
<%@ page import="com.google.gson.JsonParser" %>
<%@ page import="com.google.gson.JsonArray" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="workflow.WorkflowController" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>

<%
    Gate_passDTO gate_passDTO;
    gate_passDTO = (Gate_passDTO) request.getAttribute("gate_passDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    if (gate_passDTO == null) {
        gate_passDTO = new Gate_passDTO();

    }
    System.out.println("gate_passDTO = " + gate_passDTO);

    String formTitle = LM.getText(LC.GATE_PASS_ADD_GATE_PASS_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    boolean isPermanentTable = true;
    if (request.getParameter("isPermanentTable") != null) {
        isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
    }

    String Language = LM.getText(LC.GATE_PASS_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    String jsonDTO = Gate_pass_sub_typeRepository.getInstance().buildJSON(Language);
    JsonArray jsonArray = new JsonParser().parse(jsonDTO).getAsJsonArray();

    String loginURL = request.getRequestURL().toString();
    String context_folder = request.getContextPath();

    LM.getInstance();
    String context = "../../.." + request.getContextPath() + "/";

    String errorMessage = (Language == "English" ? "Select Referrer!" : "সুপারিশকারী নির্বাচন করুন!");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title prp-page-title">
                            <img src="<%=context_folder%>/assets/images/menu_icons/1244000@2x.png" alt="prp">
                            <%=formTitle%>
                        </h3>
                    </div>
                </div>
                <form class="form-horizontal"
                      action="Gate_passServlet?actionType=addGatePassAtGate&isPermanentTable=<%=isPermanentTable%>"
                      id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
                      onsubmit="return PreprocessBeforeSubmiting(0,'add')">
                    <div class="kt-portlet__body form-body">
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                <div class="onlyborder">
                                    <div class="row mx-2 mx-md-0">
                                        <div class="col-md-8 offset-md-1">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="sub_title_top">
                                                        <div class="sub_title">
                                                            <h4 style="background: white">
                                                                <%=formTitle%>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                    <input type='hidden' class='form-control' name='id'
                                                           id='id_hidden_<%=i%>' value='<%=gate_passDTO.id%>'
                                                           tag='pb_html'/>
                                                    <input type='hidden' class='form-control' name='id'
                                                           id='gatePassTypeId'
                                                           value='1' tag='pb_html'/>
                                                    <div class="form-group row">
                                                        <label class="col-md-4 col-form-label text-md-right">
                                                            <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_MOBILENUMBER, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-8">
                                                            <div class="input-group" id='mobileNumber_div_<%=i%>'>
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" id="basic-addon3"><%=Language.equalsIgnoreCase("English") ? "+88" : "+৮৮"%></span>
                                                                </div>
                                                                <input type='text'
                                                                       class='form-control englishDigitOrCharOnly'
                                                                       name='mobileNumber'
                                                                       id='mobileNumber'
                                                                       placeholder='<%=Language.equalsIgnoreCase("English") ? "enter your 11 digit mobile number, ex: 01710101010" : "১১ ডিজিটের মোবাইল নাম্বারটি দিন, উদাহরণঃ 01710101010"%>'
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-4 col-form-label text-md-right">
                                                            <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_CREDENTIALTYPE, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-8">
                                                            <div id='credentialType_div_<%=i%>'>
                                                                <select class='form-control' name='credentialType'
                                                                        id='credentialType'
                                                                        tag='pb_html'>
                                                                    <%=CatRepository.getInstance().buildOptions("credential", Language, null)%>
                                                                </select>
                                                            </div>
                                                            <div id='credentialNo_div_<%=i%>'>
                                                                <input type='text' class='form-control englishDigitOrCharOnly'
                                                                       name='credentialNo' id='credentialNo'
                                                                       placeholder='<%=Language.equalsIgnoreCase("English") ? "enter credential number" : "পরিচয়পত্রের নাম্বার প্রবেশ করান"%>'
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-4 col-form-label text-md-right">
                                                            <%=LM.getText(LC.GATE_PASS_ADD_REFERRERID, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-8">
                                                            <div class="" id='select-referrer-div'>
                                                                <input type='hidden' class='form-control'
                                                                       id="referrer-id" name="referrerId"
                                                                       tag='pb_html'/>
                                                                <button type="button"
                                                                        class="btn btn-primary form-control rounded"
                                                                        id="tagEmp_modal_button">
                                                                    <%=LM.getText(LC.GATE_PASS_ADD_SELECT_REFERRER, loginDTO)%>
                                                                </button>
                                                                <div class="error-alert" id="referrer_error"
                                                                     style="display: none">
                                                                    <%=errorMessage%>
                                                                </div>
                                                                <table class="table table-bordered table-striped">
                                                                    <thead></thead>
                                                                    <tbody id="tagged_emp_table" class="rounded">
                                                                    <tr style="display: none;" class="selected_employee_row">
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td>
                                                                            <button type="button"
                                                                                    class="btn btn-sm delete-trash-btn"
                                                                                    onclick="remove_containing_row(this,'tagged_emp_table');">
                                                                                <i class="fa fa-trash"></i>
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-10 mt-3">
                                <div class="form-actions text-right">
                                    <button class="btn border-0 btn-primary shadow btn-border-radius" style="border-radius: 8px" type="submit">
                                        <%=Language.equalsIgnoreCase("English") ? "Submit" : "যাচাই করুন"%>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<jsp:include page="../gate_pass/gatePassEmployeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script type="text/javascript">
    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button

    // map to store and send added employee data
    added_employee_info_map = new Map();

    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_emp_table', {
                info_map: added_employee_info_map,
                isSingleEntry: true,
                callBackFunction: function (empInfo) {

                    document.getElementById('referrer-id').value = empInfo.employeeRecordId;
                    console.log('callBackFunction called and referrer Id is being populated!!');
                    $('#referrer-id-error').hide();
                }
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagEmp_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_emp_table';
        $('#gate_pass_search_emp_modal').modal();
    });


    function remove_containing_row(button, table_name) {
        let containing_row = button.parentNode.parentNode;
        let containing_table = containing_row.parentNode;
        containing_table.deleteRow(containing_row.rowIndex);

        // button id = "<employee record id>_button"
        let td_button = button.parentNode;
        let employee_record_id = td_button.id.split("_")[0];

        let added_info_map = table_name_to_collcetion_map.get(table_name).info_map;
        console.log("delete (employee_record_id)");
        added_info_map.delete(employee_record_id);
        console.log(added_info_map);

        $('#referrer-id').val('');
    }
</script>






