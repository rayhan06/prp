<%@page pageEncoding="UTF-8" %>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%
String url = "Gate_passServlet?actionType=parliamentAreaSecuritySearch";
String navigator = SessionConstants.NAV_GATE_PASS;
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.APPOINTMENT_EDIT_LANGUAGE, loginDTO);

String pageno = "";

RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();
boolean isPermanentTable = rn.m_isPermanentTable;

System.out.println("rn " + rn);

String action = url;
String context = "../../.." + request.getContextPath() + "/";
String link = context + url;
String concat = "?";
if (url.contains("?")) {
	concat = "&";
}
int pagination_number = 0;
%>

<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">
		<i class="fa fa-search fa-2x" style="color:#0098bf !important;"></i>&nbsp;
		<h3 class="kt-subheader__title">
			<%=Language.equalsIgnoreCase("English") ? "SEARCH GATE PASS (Parliament Area Security Service)" : "গেইট পাস খুঁজুন (সংসদ এলাকার নিরাপত্তা সেবা)"%>
		</h3>
	</div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content" style="background: white">
	<div class="row shadow-none border-0">
		<div class="col-lg-12">
			<jsp:include page="./parliament_area_securitySearchNav.jsp" flush="true">
				<jsp:param name="url" value="<%=url%>" />
				<jsp:param name="navigator" value="<%=navigator%>" />
				<jsp:param name="pageName" value="<%=LM.getText(LC.GATE_PASS_SEARCH_GATE_PASS_SEARCH_FORMNAME, loginDTO)%>" />
			</jsp:include>
			<div id="horizontal-line" style="height: 1px; background: #ecf0f5"></div>
			<div class="kt-portlet shadow-none" id="search-result-div" style="display: none">
				<div class="kt-portlet__body">
					<form action="Gate_passServlet?isPermanentTable=<%=isPermanentTable%>&actionType=delete" method="POST" id="tableForm" enctype = "multipart/form-data">
						<jsp:include page="parliament_area_securitySearchForm.jsp" flush="true">
							<jsp:param name="pageName" value="<%=LM.getText(LC.GATE_PASS_SEARCH_GATE_PASS_SEARCH_FORMNAME, loginDTO)%>" />
						</jsp:include>
					</form>
				</div>
			</div>
		</div>
	</div>
	<% pagination_number = 1;%>
	<%@include file="../common/pagination_with_go2.jsp" %>
</div>

<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>

<style>
	.my-4 {
		display: none;
	}
	#horizontal-line {
		display: none;
	}
</style>


