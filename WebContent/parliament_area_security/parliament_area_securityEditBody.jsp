<%@page pageEncoding="UTF-8" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="gate_pass.*" %>
<%@page import="java.util.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="approval_execution_table.*" %>
<%@ page import="approval_path.*" %>
<%@ page import="user.*" %>
<%@page import="workflow.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="parliament_gate.Parliament_gateRepository" %>
<%@ page import="gate_pass_type.Gate_pass_typeRepository" %>
<%@ page import="gate_pass_sub_type.Gate_pass_sub_typeRepository" %>
<%@ page import="com.google.gson.JsonParser" %>
<%@ page import="com.google.gson.JsonArray" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = LM.getText(LC.GATE_PASS_EDIT_LANGUAGE, loginDTO);
%>


<div class="box box-primary">
    <div class="box-header with-border">
        <h2 class="box-title"><i class="fa fa-gift"></i>
            <%=Language.equalsIgnoreCase("English") ? "Parliament Area Security Service" : "সংসদ এলাকার নিরাপত্তা সেবা"%>
        </h2>
    </div>
    <div class="box-body" id="parliament-area-security-div">
        <form class="form-horizontal"
              action="Gate_passServlet?actionType=parliamentAreaSecurityView"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting()">
            <div class="form-group row">
                <label class="col-md-3 control-label">
                    <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_MOBILENUMBER, loginDTO)%>
                </label>
                <div class="col-md-6 " id='gatePassMobileNo_div'>

                    <input type='text' class='form-control englishDigitOnly' name='mobileNumber' id='mobileNumber'
                           placeholder='<%=Language.equalsIgnoreCase("English") ? "enter your 11 digit mobile number, ex: 01710101010" :
                               "১১ ডিজিটের মোবাইল নাম্বারটি দিন, উদাহরণঃ 01710101010"%>' tag='pb_html'/>
                </div>


                <div class="form-actions text-center">
                    <button class="btn btn-success rounded" type="submit" id="submit_button">

                        <%=LM.getText(LC.GATE_PASS_SEARCH_GATE_PASS_SEARCH_BUTTON, loginDTO)%>

                    </button>
                </div>

            </div>

        </form>

    </div>

</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">
    function PreprocessBeforeSubmiting() {
        let doSubmit = true;
        const mobileNo = $('#mobileNumber').val();
        let url = "Gate_passServlet?actionType=visitorPassLookup" + "&mobileNumber=88" + mobileNo;

        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (data) {
                let count = parseInt(data);
                if (count == 0) {
                    Swal.fire({
                        position: 'center',
                        type: 'success',
                        background: '#7FFFD4',
                        title: '<%=Language.equalsIgnoreCase("English") ? "No gate pass entry for this phone number!" : "এই ফোন নম্বর এর জন্য কোন গেট পাস এন্ট্রি নেই!"%>',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    doSubmit = false;
                } else {
                    // $('#mobileNumber').val('88'+mobileNo);
                    doSubmit = true;
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
        return doSubmit;
    }

    $('#mobileNumber').on('keyup', () => {
        const str = $('#mobileNumber').val();
        if (str.length > 11)
            $('#mobileNumber').val(str.substr(0, 11));
    });

    // $('#bigform').on('submit', () => {
    //
    // });

    $(".englishDigitOnly").keypress(function (event) {
        const ew = event.which;
        if (ew == 32)
            return true;
        const charStr = String.fromCharCode(ew);

        if (charStr == "0") {
            return true;
        } else if (charStr == "1") {
            return true;
        } else if (charStr == "2") {
            return true;
        } else if (charStr == "3") {
            return true;
        } else if (charStr == "4") {
            return true;
        } else if (charStr == "5") {
            return true;
        } else if (charStr == "6") {
            return true;
        } else if (charStr == "7") {
            return true;
        } else if (charStr == "8") {
            return true;
        } else if (charStr == "9") {
            return true;
        }
        return false;
    });

</script>






