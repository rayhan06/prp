<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="org.apache.commons.codec.language.bm.Lang" %>


<%
    System.out.println("Inside nav.jsp");
    String url = request.getParameter("url");
    String navigator = request.getParameter("navigator");
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Parliament Area Security Search";
    String pageno = "";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String[][] searchFieldInfo = rn.getSearchFieldInfo();
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";
    int row = 0;

    String Language = LM.getText(LC.GATE_PASS_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    int pagination_number = 0;
    boolean isPermanentTable = rn.m_isPermanentTable;
    System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
%>


<!-- search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__body">
        <!-- BEGIN FORM-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group row">
                        <label class="col-md-3 col-xl-2 col-form-label">
                            <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_MOBILENUMBER, loginDTO)%>
                        </label>
                        <div class="col-md-9 col-xl-10">
                            <input type="text" class="form-control englishDigitOnly" id="mobile_number"
                                   name="mobile_number" oninput='mobileNumberValidation()'
                                   placeholder='<%=Language.equalsIgnoreCase("English") ? "Enter 11 digit number, Ex: 017XXXXXXXX" : "১১ ডিজিটের মোবাইল নাম্বার দিন, উদাহরণঃ 017XXXXXXXX"%>'>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group row">
                        <label class="col-md-5 col-form-label">
                            <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_CREDENTIALNO, loginDTO)%>
                        </label>
                        <div class="col-md-7">
                            <input type="text" class="form-control" id="credential_no" name="credential_no"
                                   oninput='mobileNumberValidation()'
                                   placeholder='<%=Language.equalsIgnoreCase("English") ? "Enter credential number" : "পরিচয়পত্রের নাম্বার দিন"%>'>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="text-right">
                        <button type="submit" class="btn shadow green-meadow btn-outline sbold uppercase advanceseach"
                                onclick="allfield_changed('',0)"
                                style="background-color: #00a1d4; color: white; border-radius: 6px!important; border-radius: 8px;">
                            <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- END FORM-->
    </div>
</div>


<%@include file="../common/pagination_with_go2.jsp" %>

<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">

    $(document).ready(function () {
        const curDate = new Date();
        let maxDate = new Date();
        maxDate.setFullYear(curDate.getFullYear() + 2);
        curDate.setFullYear(curDate.getFullYear() - 1);
        setMinDateByTimestampAndId('visiting_date', curDate.getTime());
        setMaxDateByTimestampAndId('visiting_date', maxDate.getTime());
    });

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {

        if ($('#search-result-div').is(":hidden")) {
            $('#search-result-div').show();
            $('.my-4').show();
            $('#horizontal-line').show();
        }

        var params = 'AnyField=';

        params += '&credential_no=' + $('#credential_no').val();
        params += '&mobile_number=88' + $('#mobile_number').val();

        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

    function mobileNumberValidation() {
        let mobile_number = $('#mobile_number').val();

        if (mobile_number.length === 12) {
            $('#mobile_number').val(mobile_number.substring(0, 11));
        }
    }

    $(".englishDigitOnly").keypress(function (event) {
        var ew = event.which;
        if (ew == 32)
            return true;
        var charStr = String.fromCharCode(ew);

        if (charStr == "0") {
            return true;
        } else if (charStr == "1") {
            return true;
        } else if (charStr == "2") {
            return true;
        } else if (charStr == "3") {
            return true;
        } else if (charStr == "4") {
            return true;
        } else if (charStr == "5") {
            return true;
        } else if (charStr == "6") {
            return true;
        } else if (charStr == "7") {
            return true;
        } else if (charStr == "8") {
            return true;
        } else if (charStr == "9") {
            return true;
        }
        return false;
    });

</script>

