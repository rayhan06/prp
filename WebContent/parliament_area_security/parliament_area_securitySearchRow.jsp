<%@page pageEncoding="UTF-8" %>

<%@page import="gate_pass.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="approval_execution_table.*" %>
<%@ page import="approval_path.*" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.GATE_PASS_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_GATE_PASS;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Gate_pass_searchModel gate_passDTO = (Gate_pass_searchModel) request.getAttribute("gate_passDTO");
    CommonDTO commonDTO = gate_passDTO;
    String servletName = "Gate_passServlet";

    Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
    ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
    Approval_execution_tableDTO approval_execution_tableDTO = null;
    String Message = "Done";
    approval_execution_tableDTO = (Approval_execution_tableDTO) approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("gate_pass", gate_passDTO.iD);

    System.out.println("gate_passDTO = " + gate_passDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Gate_passDAO gate_passDAO = (Gate_passDAO) request.getAttribute("gate_passDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<%--<td id='gatePassType'>--%>
<%--    <%=Language.equalsIgnoreCase("english") ? (gate_passDTO.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ?--%>
<%--            gate_passDTO.gatePassTypeEng + " (" + gate_passDTO.gatePassSubTypeEng + ")" : gate_passDTO.gatePassTypeEng) : (gate_passDTO.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ?--%>
<%--            gate_passDTO.gatePassTypeBng + " (" + gate_passDTO.gatePassSubTypeBng + ")" : gate_passDTO.gatePassTypeBng) %>--%>
<%--</td>--%>

<td id='visitingDate'>
    <%
        String today;
        if (gate_passDTO.gatePassSubTypeEng.equalsIgnoreCase("official")) {
            today = simpleDateFormat.format(new Date());
    %>
    <%="<b>" + Utils.getDigits(today, Language) + "</b>"%>
    <%
    } else {
    %>
    <%="<b>" + Utils.getDigits(simpleDateFormat.format(new Date(gate_passDTO.visitingDate)), Language) + "</b>"%>
    <%
        }
    %>
</td>

<td id='gatePassVisitorName'>
    <%="<em><b>" + gate_passDTO.name + "</b></em>"%>
</td>

<td id='gatePassReferrerName'>
    <%
        value = gate_passDTO.mobileNumber + "";
    %>

    <%=Utils.getDigits(value, Language)%>
</td>

<%--<td id='gatePassVisitorAddress'>--%>
<%--    <%--%>
<%--        String address = gate_passDTO.currentAddress.addressText;--%>
<%--        if (Language.equalsIgnoreCase("English")) {--%>
<%--            address += gate_passDTO.currentAddress.thanaEng.equals("") ? "": ", "+gate_passDTO.currentAddress.thanaEng;--%>
<%--            address += gate_passDTO.currentAddress.districtEng.equals("") ? "": ", "+gate_passDTO.currentAddress.districtEng;--%>
<%--        } else {--%>
<%--            address += gate_passDTO.currentAddress.thanaBng.equals("") ? "": ", "+gate_passDTO.currentAddress.thanaBng;--%>
<%--            address += gate_passDTO.currentAddress.districtBng.equals("") ? "": ", "+gate_passDTO.currentAddress.districtBng;--%>
<%--        }--%>
<%--    %>--%>
<%--    <%=address%>--%>
<%--</td>--%>

<td id='gatePassVisitorCredential'>
    <%=Language.equalsIgnoreCase("english") ? gate_passDTO.credentialTypeEng + " - " : gate_passDTO.credentialTypeBng + " - " %>
    <%
        value = gate_passDTO.credentialNo + "";
    %>

    <%=Utils.getDigits(value, Language)%>

</td>

<td id='gatePassAprStat'>
    <%
        if (gate_passDTO.gatePassTypeEng.equalsIgnoreCase("parliament area pass")) {
            if (gate_passDTO.firstLayerApprovalStatus == Gate_passDAO.approvalStatusPending) {
    %>
    <span class="text-primary font-italic font-weight-bold">
            <%=Language.equalsIgnoreCase("English") ? "Pending" : "অনিষ্পন্ন"%>
        </span>
    <%
    } else if (gate_passDTO.firstLayerApprovalStatus == Gate_passDAO.approvalStatusApproved) {

    %>
    <span class="text-success font-italic font-weight-bold">
                <%=Language.equalsIgnoreCase("English") ? "Approved" : "অনুমোদিত"%>
        </span>
    <%

    } else if (gate_passDTO.firstLayerApprovalStatus == Gate_passDAO.approvalStatusCancelled) {
    %>
    <span class="text-danger font-italic font-weight-bold">
                    <%=Language.equalsIgnoreCase("English") ? "Dismissed" : "অননুমোদিত"%>
        </span>
    <%
        }
    } else {
        if (gate_passDTO.firstLayerApprovalStatus == Gate_passDAO.approvalStatusPending) {
    %>
    <span class="text-primary font-italic font-weight-bold">
                <%=Language.equalsIgnoreCase("English") ? "Pending" : "অনিষ্পন্ন"%>
        </span>
    <%
    } else if (gate_passDTO.firstLayerApprovalStatus == Gate_passDAO.approvalStatusCancelled) {
    %>
    <span class="text-danger font-italic font-weight-bold">
                    <%=Language.equalsIgnoreCase("English") ? "Dismissed" : "অননুমোদিত"%>
        </span>
    <%
    } else if (gate_passDTO.firstLayerApprovalStatus == Gate_passDAO.approvalStatusApproved) {
        if (gate_passDTO.secondLayerApprovalStatus == Gate_passDAO.approvalStatusPending) {
    %>
    <span class="text-primary font-italic font-weight-bold">
                        <%=Language.equalsIgnoreCase("English") ? "Pending" : "অনিষ্পন্ন"%>
        </span>
    <%
    } else if (gate_passDTO.secondLayerApprovalStatus == Gate_passDAO.approvalStatusCancelled) {
    %>
    <span class="text-danger font-italic font-weight-bold">
                        <%=Language.equalsIgnoreCase("English") ? "Dismissed" : "অননুমোদিত"%>
        </span>
    <%
    } else if (gate_passDTO.secondLayerApprovalStatus == Gate_passDAO.approvalStatusProcessing) {
    %>
    <span class="text-info font-italic font-weight-bold">
                            <%=Language.equalsIgnoreCase("English") ? "Processing" : "প্রক্রিয়াধীন"%>
        </span>
    <%
    } else if (gate_passDTO.secondLayerApprovalStatus == Gate_passDAO.approvalStatusApproved) {
    %>
    <span class="text-success  font-italic font-weight-bold">
                        <%=Language.equalsIgnoreCase("English") ? "Approved" : "অনুমোদিত"%>
        </span>
    <%
                }
            }
        }
    %>

</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Gate_passServlet?actionType=parliamentAreaSecurityView&ID=<%=gate_passDTO.gate_pass_id%>'"
    >
        <i class="fa fa-eye"></i>
    </button>
</td>


<style>
    td {
        text-align: center;
    }
</style>

