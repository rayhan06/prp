<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="cash_management.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>


<%
    String servletName = "Cash_managementServlet";
    String ID = request.getParameter("ID");
    Cash_managementDTO cash_managementDTO = Cash_managementDAO.getInstance().getDTOFromID(Long.parseLong(ID));
    CommonDTO commonDTO = cash_managementDTO;
%>
<%@include file="../pb/viewInitializer.jsp" %>

<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.CASH_MANAGEMENT_ADD_CASH_MANAGEMENT_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-md-10 offset-md-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.CASH_MANAGEMENT_ADD_CASH_MANAGEMENT_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.CASH_MANAGEMENT_ADD_BILLNUMBER, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=Utils.getDigits(cash_managementDTO.billRegisterId, Language)%>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.CASH_MANAGEMENT_ADD_BILLDESCRIPTION, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=cash_managementDTO.billDescription%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.CASH_MANAGEMENT_ADD_BILLAMOUNT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=Utils.getDigits(cash_managementDTO.billAmount, Language)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.CASH_MANAGEMENT_ADD_PERMANENTADVANCEDEXCLUDED, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=Utils.getDigits(cash_managementDTO.permanentAdvancedExcluded, Language)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.CASH_MANAGEMENT_ADD_TOBEGIVENADVANCEDEXCLUDED, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=Utils.getDigits(cash_managementDTO.toBeGivenAdvancedExcluded, Language)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.CASH_MANAGEMENT_ADD_MISCELLANEOUS, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=Utils.getDigits(cash_managementDTO.miscellaneous, Language)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.CASH_MANAGEMENT_ADD_CASHSTATUSCAT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=CatRepository.getInstance().getText(Language, "cash_status", cash_managementDTO.cashStatusCat)%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>