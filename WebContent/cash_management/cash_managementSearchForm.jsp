<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="cash_management.*" %>
<%@ page import="util.*" %>
<%@ page language="java" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="card_info.CardStatusEnum" %>
<%@ page import="finance.CashStatusEnum" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="finance.CashTypeEnum" %>
<%@ page import="java.util.*" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="util.StringUtils" %>
<%@ page import="static util.StringUtils.convertBanglaIfLanguageIsBangla" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page pageEncoding="UTF-8" %>


<%
    String navigator2 = "navCASH_MANAGEMENT";
    String servletName = "Cash_managementServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>

<style>
    th {
        text-align: center;
    }

    .separator-right {
        border-right: 3px solid black !important;
    }
</style>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th colspan="9" class="separator-right">
                <%=CashStatusEnum.ACQUISITION.getText(Language)%>
            </th>
            <th colspan="6">
                <%=CashStatusEnum.PAYMENT.getText(Language)%>
            </th>
        </tr>
        <tr>
            <th rowspan="2">
                <%=getDataByLanguage(Language, "ক্রমিক নং", "Serial No.")%>
            </th>
            <th rowspan="2">
                <%=LM.getText(LC.CASH_MANAGEMENT_ADD_BILLNUMBER, loginDTO)%>
            </th>
            <th rowspan="2">
                <%=LM.getText(LC.CASH_MANAGEMENT_ADD_BILLDESCRIPTION, loginDTO)%>
            </th>
            <th rowspan="2">
                <%=CashTypeEnum.SALARY.getText(Language)%>
            </th>
            <th rowspan="2">
                <%=CashTypeEnum.ALLOWANCE.getText(Language)%>
            </th>
            <th colspan="2">
                <%=getDataByLanguage(Language, "আনুষাঙ্গিক খরচাদি", "Accessories Expenses")%>
            </th>
            <th rowspan="2">
                <%=CashTypeEnum.OTHERS.getText(Language)%>
            </th>
            <th rowspan="2" class="separator-right">
                <%=getDataByLanguage(Language, "মোট", "Total")%>
            </th>

            <th rowspan="2">
                <%=CashTypeEnum.SALARY.getText(Language)%>
            </th>
            <th rowspan="2">
                <%=CashTypeEnum.ALLOWANCE.getText(Language)%>
            </th>
            <th colspan="2">
                <%=getDataByLanguage(Language, "আনুষাঙ্গিক খরচাদি", "Accessories Expenses")%>
            </th>
            <th rowspan="2">
                <%=CashTypeEnum.OTHERS.getText(Language)%>
            </th>
            <th rowspan="2">
                <%=getDataByLanguage(Language, "মোট", "Total")%>
            </th>
        </tr>
        <tr>
            <th>
                <%=getDataByLanguage(Language, "স্থায়ী অগ্রিম", "Permanent Advance")%>
            </th>
            <th>
                <%=getDataByLanguage(Language, "অগ্রিম প্রদান", "Advance Payment")%>
            </th>
            <th>
                <%=getDataByLanguage(Language, "স্থায়ী অগ্রিমের বহির্ভূত কর", "Permanent Advance Tax Excluded")%>
            </th>
            <th>
                <%=getDataByLanguage(Language, "প্রদানের জন্য উত্তোলিত অর্থের বহির্ভূত", "Excluding raised for Payment")%>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td colspan="3" class="text-right">
                <%=getDataByLanguage(Language, "পূর্বের জের=", "Previous Total=")%>
            </td>
            <td></td>
            <td></td>
            <td class="text-right">
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        convertBanglaIfLanguageIsBangla(Language, String.format(
                                "%d",
                                Cash_managementServlet.PERMANENT_ADVANCE_AMOUNT
                        ))
                )%>
            </td>
            <td></td>
            <td></td>
            <td class="separator-right text-right">
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        convertBanglaIfLanguageIsBangla(Language, String.format(
                                "%d",
                                Cash_managementServlet.PERMANENT_ADVANCE_AMOUNT
                        ))
                )%>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Cash_managementDTO> cashManagementDTOs = (List<Cash_managementDTO>) recordNavigator.list;
            TreeMap<Long, List<Cash_managementDTO>> groupedAndSortedByChequeRegisterId
                    = cashManagementDTOs.stream()
                                        .collect(Collectors.groupingBy(
                                                cashManagementDTO -> cashManagementDTO.chequeRegisterId,
                                                TreeMap::new,
                                                Collectors.toList()
                                        ));
            int serialNo = 0;
            long [] salaryTotal = new long[] {0, 0};
            long [] allowanceTotal = new long[] {0, 0};
            long [] othersTotal = new long[] {0, 0};
            long [] total = new long[] {0, 0};
            for (Map.Entry<Long, List<Cash_managementDTO>> entry : groupedAndSortedByChequeRegisterId.entrySet()) {
                Map<Integer, Cash_managementDTO> groupedByCashStatus =
                        entry.getValue()
                             .stream()
                             .collect(Collectors.toMap(dto -> dto.cashStatusCat, dto -> dto, (dto1, dto2) -> dto1));
                Cash_managementDTO acquisitionDTO = groupedByCashStatus.getOrDefault(CashStatusEnum.ACQUISITION.getValue(), new Cash_managementDTO());
                salaryTotal[0] += acquisitionDTO.salaryAmount;
                allowanceTotal[0] += acquisitionDTO.allowanceAmount;
                othersTotal[0] += acquisitionDTO.miscellaneous;
                total[0] += (acquisitionDTO.salaryAmount + acquisitionDTO.allowanceAmount + acquisitionDTO.miscellaneous);

                Cash_managementDTO paymentDTO = groupedByCashStatus.getOrDefault(CashStatusEnum.PAYMENT.getValue(), new Cash_managementDTO());
                salaryTotal[1] += paymentDTO.salaryAmount;
                allowanceTotal[1] += paymentDTO.allowanceAmount;
                othersTotal[1] += paymentDTO.miscellaneous;
                total[1] += (paymentDTO.salaryAmount + paymentDTO.allowanceAmount + paymentDTO.miscellaneous);
        %>
        <tr>
            <td class="text-center">
                <%=convertBanglaIfLanguageIsBangla(Language, String.format("%d", ++serialNo))%>
            </td>
            <td class="text-center">
                <%=Utils.getDigits(acquisitionDTO.billRegisterId, Language)%>
            </td>
            <td>
                <%=acquisitionDTO.billDescription%>
            </td>
            <td class="text-right">
                <%=acquisitionDTO.getFormattedSalaryAmount(Language)%>
            </td>
            <td class="text-right">
                <%=acquisitionDTO.getFormattedAllowanceAmount(Language)%>
            </td>
            <td></td>
            <td></td>
            <td>
                <%=acquisitionDTO.getFormattedOthersAmount(Language)%>
            </td>
            <td class="separator-right text-right">
                <%=acquisitionDTO.getFormattedTotalAmount(Language)%>
            </td>
            <td class="text-right">
                <%=paymentDTO.getFormattedSalaryAmount(Language)%>
            </td>
            <td class="text-right">
                <%=paymentDTO.getFormattedAllowanceAmount(Language)%>
            </td>
            <td></td>
            <td></td>
            <td>
                <%=paymentDTO.getFormattedOthersAmount(Language)%>
            </td>
            <td class="text-right">
                <%=paymentDTO.getFormattedTotalAmount(Language)%>
            </td>
        </tr>
        <% } %>
        </tbody>

        <tfoot>
        <tr>
            <%--1--%>
            <td></td>
            <%--2--%>
            <td></td>
            <%--3--%>
            <td class="text-right">
                <%=getDataByLanguage(Language, "মোট জের=", "Total=")%>
            </td>
            <%--4--%>
            <td></td>
            <%--5--%>
            <td></td>
            <%--6--%>
            <td></td>
            <%--7--%>
            <td></td>
            <%--8--%>
            <td></td>
            <%--9--%>
            <td class="separator-right"></td>
            <%--PAYMENT RFECORD--%>
            <%--14--%>
            <td class="text-right">
                <%=getDataByLanguage(Language, "মোট দেয়=", "Total Given =")%>&nbsp;
                <%--SALARY TOTAL--%>
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        convertBanglaIfLanguageIsBangla(Language, String.format(
                                "%d",
                                salaryTotal[1]
                        ))
                )%>
            </td>
            <%--15--%>
            <td class="text-right">
                <%--ALLOWANCE AMOUNT TOTAL--%>
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        convertBanglaIfLanguageIsBangla(Language, String.format(
                                "%d",
                                allowanceTotal[1]
                        ))
                )%>
            </td>
            <%--16--%>
            <td></td>
            <%--17--%>
            <td></td>
            <%--18--%>
            <td class="text-right">
                <%--OTHERS AMOUNT TOTAL--%>
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        convertBanglaIfLanguageIsBangla(Language, String.format(
                                "%d",
                                othersTotal[1]
                        ))
                )%>
            </td>
            <%--19--%>
            <td class="text-right">
                <%--TOTAL--%>
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        convertBanglaIfLanguageIsBangla(Language, String.format(
                                "%d",
                                total[1]
                        ))
                )%>
            </td>
        </tr>

        <tr>
            <%--1--%>
            <td></td>
            <%--2--%>
            <td></td>
            <%--3--%>
            <td></td>
            <%--4--%>
            <td class="text-right">
                <%--SALARY TOTAL--%>
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        convertBanglaIfLanguageIsBangla(Language, String.format(
                                "%d",
                                salaryTotal[0]
                        ))
                )%>
            </td>
            <%--5--%>
            <td class="text-right">
                <%--ALLOWANCE TOTAL--%>
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        convertBanglaIfLanguageIsBangla(Language, String.format(
                                "%d",
                                allowanceTotal[0]
                        ))
                )%>
            </td>
            <%--6--%>
            <td class="text-right">
                <%--ADVANCE AMOUNT TOTAL--%>
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        convertBanglaIfLanguageIsBangla(Language, String.format(
                                "%d",
                                Cash_managementServlet.PERMANENT_ADVANCE_AMOUNT
                        ))
                )%>
            </td>
            <%--7--%>
            <td></td>
            <%--8--%>
            <td class="text-right">
                <%--OTHER AMOUNT TOTAL--%>
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        convertBanglaIfLanguageIsBangla(Language, String.format(
                                "%d",
                                othersTotal[0]
                        ))
                )%>
            </td>
            <%--9--%>
            <td class="separator-right text-right">
                <%--TOTAL--%>
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        convertBanglaIfLanguageIsBangla(Language, String.format(
                                "%d",
                                total[0] + Cash_managementServlet.PERMANENT_ADVANCE_AMOUNT
                        ))
                )%>
            </td>
            <%--PAYMENT RFECORD--%>
            <%--14--%>
            <td class="text-right">
                <%=getDataByLanguage(Language, "সমাপ্তির জেট=", "Ending Total =")%>&nbsp;
                <%--SALARY TOTAL--%>
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        convertBanglaIfLanguageIsBangla(Language, String.format(
                                "%d",
                                0
                        ))
                )%>
            </td>
            <%--15--%>
            <td class="text-right">
                <%--ALLOWANCE AMOUNT TOTAL--%>
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        convertBanglaIfLanguageIsBangla(Language, String.format(
                                "%d",
                                0
                        ))
                )%>
            </td>
            <%--16--%>
            <td class="text-right">
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        convertBanglaIfLanguageIsBangla(Language, String.format(
                                "%d",
                                Cash_managementServlet.PERMANENT_ADVANCE_AMOUNT
                        ))
                )%>
            </td>
            <%--17--%>
            <td></td>
            <%--18--%>
            <td class="text-right">
                <%--OTHERS AMOUNT TOTAL--%>
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        convertBanglaIfLanguageIsBangla(Language, String.format(
                                "%d",
                                0
                        ))
                )%>
            </td>
            <%--19--%>
            <td class="text-right">
                <%--TOTAL--%>
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        convertBanglaIfLanguageIsBangla(Language, String.format(
                                "%d",
                                Cash_managementServlet.PERMANENT_ADVANCE_AMOUNT
                        ))
                )%>
            </td>
        </tr>

        <tr>
            <%--1--%>
            <td></td>
            <%--2--%>
            <td></td>
            <%--3--%>
            <td></td>
            <%--4--%>
            <td></td>
            <%--5--%>
            <td></td>
            <%--6--%>
            <td></td>
            <%--7--%>
            <td></td>
            <%--8--%>
            <td></td>
            <%--9--%>
            <td class="separator-right"></td>
            <%--PAYMENT RFECORD--%>
            <%--14--%>
            <td class="text-right">
                <%=getDataByLanguage(Language, "নীট জেট=", "Net Total =")%>&nbsp;
                <%--SALARY TOTAL--%>
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        convertBanglaIfLanguageIsBangla(Language, String.format(
                                "%d",
                                salaryTotal[1]
                        ))
                )%>
            </td>
            <%--15--%>
            <td class="text-right">
                <%--ALLOWANCE AMOUNT TOTAL--%>
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        convertBanglaIfLanguageIsBangla(Language, String.format(
                                "%d",
                                allowanceTotal[1]
                        ))
                )%>
            </td>
            <%--16--%>
            <td class="text-right">
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        convertBanglaIfLanguageIsBangla(Language, String.format(
                                "%d",
                                Cash_managementServlet.PERMANENT_ADVANCE_AMOUNT
                        ))
                )%>
            </td>
            <%--17--%>
            <td></td>
            <%--18--%>
            <td class="text-right">
                <%--OTHERS AMOUNT TOTAL--%>
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        convertBanglaIfLanguageIsBangla(Language, String.format(
                                "%d",
                                othersTotal[1]
                        ))
                )%>
            </td>
            <%--19--%>
            <td class="text-right">
                <%--TOTAL--%>
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        convertBanglaIfLanguageIsBangla(Language, String.format(
                                "%d",
                                total[1] + Cash_managementServlet.PERMANENT_ADVANCE_AMOUNT
                        ))
                )%>
            </td>
        </tr>
        </tfoot>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>