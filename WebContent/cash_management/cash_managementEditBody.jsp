<%@page import="cash_management.Cash_managementDAO" %>
<%@page import="cash_management.Cash_managementDTO" %>
<%@page import="cheque_register.Cheque_registerDTO" %>

<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="login.LoginDTO" %>
<%@page import="pb.CatRepository" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="cheque_register.Cheque_registerDAO" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    String context = request.getContextPath() + "/";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    Cash_managementDTO cash_managementDTO;
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        cash_managementDTO = new Cash_managementDTO();
        ID = "0";
    } else {
        cash_managementDTO = Cash_managementDAO.getInstance().getDTOFromID(Long.parseLong(ID));
    }
    Cheque_registerDTO cheque_registerDTO;
    if (request.getParameter("chequeId") != null) {
        cheque_registerDTO = Cheque_registerDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("chequeId")));
    } else {
        cheque_registerDTO = new Cheque_registerDTO();
    }
    String actionName = request.getParameter("actionType").equals("edit") ? "edit" : "add";
    int i = 0;
    String Language = LM.getText(LC.CASH_MANAGEMENT_EDIT_LANGUAGE, loginDTO);
    String formTitle = LM.getText(LC.CASH_MANAGEMENT_ADD_CASH_MANAGEMENT_ADD_FORMNAME, loginDTO);
    String servletName = "Cash_managementServlet";
    String tableName = "cash_management";
    String value = "";
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" action="Cash_managementServlet?actionType=ajax_<%=actionName%>&isPermanentTable=true&id=<%=ID%>"
              id="bigform" name="bigform" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=cash_managementDTO.iD%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='chequeRegisterId'
                                           id='chequeRegisterId_hidden_<%=i%>'
                                           value='<%=actionName.equalsIgnoreCase("edit")?cash_managementDTO.chequeRegisterId:cheque_registerDTO.iD%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='billNumber'
                                           id='billNumber_hidden_<%=i%>'
                                           value='<%=actionName.equalsIgnoreCase("edit")?cash_managementDTO.billRegisterId:cheque_registerDTO.billRegisterId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='billDescription'
                                           id='billDescription_hidden_<%=i%>'
                                           value='<%=actionName.equalsIgnoreCase("edit")?cash_managementDTO.billDescription:cheque_registerDTO.billDescription%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='billAmount'
                                           id='billAmount_hidden_<%=i%>'
                                           value='<%=actionName.equalsIgnoreCase("edit")?cash_managementDTO.billAmount:cheque_registerDTO.billAmount%>'
                                           tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.CASH_MANAGEMENT_ADD_BILLNUMBER, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <label class="col-form-label form-control">
                                                <%=actionName.equalsIgnoreCase("edit") ? cash_managementDTO.billRegisterId : cheque_registerDTO.billRegisterId%>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.CASH_MANAGEMENT_ADD_BILLDESCRIPTION, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                              <textarea type='text' class='form-control' name='billDescription'
                                                        id='billDescription_text_<%=i%>' rows="4" readonly
                                                        style="text-align: left;resize: none; width: 100%">    <%=actionName.equalsIgnoreCase("edit") ? cash_managementDTO.billDescription : cheque_registerDTO.billDescription%></textarea>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.CASH_MANAGEMENT_ADD_BILLAMOUNT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <label class="col-form-label form-control">
                                                <%=actionName.equalsIgnoreCase("edit") ? cash_managementDTO.billAmount : cheque_registerDTO.billAmount%>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.CASH_MANAGEMENT_ADD_PERMANENTADVANCEDEXCLUDED, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='permanentAdvancedExcluded'
                                                   id='permanentAdvancedExcluded_text_<%=i%>'
                                                   value='<%=cash_managementDTO.permanentAdvancedExcluded%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.CASH_MANAGEMENT_ADD_TOBEGIVENADVANCEDEXCLUDED, loginDTO)%>
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='toBeGivenAdvancedExcluded'
                                                   id='toBeGivenAdvancedExcluded_text_<%=i%>'
                                                   value='<%=cash_managementDTO.toBeGivenAdvancedExcluded%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.CASH_MANAGEMENT_ADD_MISCELLANEOUS, loginDTO)%>
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='miscellaneous'
                                                   id='miscellaneous_text_<%=i%>'
                                                   value='<%=cash_managementDTO.miscellaneous%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.CASH_MANAGEMENT_ADD_CASHSTATUSCAT, loginDTO)%>
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='cashStatusCat'
                                                    id='cashStatusCat_category_<%=i%>' tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("cash_status", Language, cash_managementDTO.cashStatusCat)%>
                                            </select>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.CASH_MANAGEMENT_ADD_CASH_MANAGEMENT_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="button" onclick="submitCashForm()">
                                <%=LM.getText(LC.CASH_MANAGEMENT_ADD_CASH_MANAGEMENT_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<script type="text/javascript">
    const bigForm = $('#bigform');
    const isLangEng = '<%=HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng%>';

    function PreprocessBeforeSubmiting(row, validate) {
    }

    function submitCashForm() {
        if (bigForm.valid()) {
            submitAjaxForm();
        }
    }

    function keyDownEvent(e) {
        console.log(e);
        let isvalid = inputValidationForIntValue(e, $(this), <%=actionName.equalsIgnoreCase("edit") ? cash_managementDTO.billAmount : cheque_registerDTO.billAmount%>);
        return true == isvalid;
    }

    function buttonStateChange(value) {
        $(':button').prop('disabled', value);
    }

    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Cash_managementServlet");
    }

    function init(row) {
        select2SingleSelector("#cashStatusCat_category_0", '<%=Language%>');
        document.getElementById('permanentAdvancedExcluded_text_0').onkeydown = keyDownEvent;
        document.getElementById('toBeGivenAdvancedExcluded_text_0').onkeydown = keyDownEvent;
        document.getElementById('miscellaneous_text_0').onkeydown = keyDownEvent;
        $.validator.addMethod('cashStatusSelection', function (value, element) {
            return value != 0;
        });
        bigForm.validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                cashStatusCat: {
                    required: true,
                    cashStatusSelection: true
                },
                permanentAdvancedExcluded: {
                    required: true,
                },
                toBeGivenAdvancedExcluded: {
                    required: true,
                },
                miscellaneous: {
                    required: true,
                },
            },

            messages: {
                cashStatusCat: isLangEng ? "Please Select Cash Status" : "অনুগ্রহ করে ক্যাশ অবস্থা নির্বাচন করুন",
                permanentAdvancedExcluded: isLangEng ? "Please Enter Cash Amount" : "অনুগ্রহ করে ক্যাশের পরিমাণ প্রবেশ করুন",
                toBeGivenAdvancedExcluded: isLangEng ? "Please Enter Cash Amount" : "অনুগ্রহ করে ক্যাশের পরিমাণ প্রবেশ করুন",
                miscellaneous: isLangEng ? "Please Enter Cash Amount" : "অনুগ্রহ করে ক্যাশের পরিমাণ প্রবেশ করুন"
            }
        });
    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        // CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });
</script>