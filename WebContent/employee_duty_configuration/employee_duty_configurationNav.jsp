<%@page import="language.LC"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="language.LM"%>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator"%>
<%@ page import="java.util.Arrays"%>
<%@ page import="searchform.SearchForm"%>
<%@ page import="pb.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page contentType="text/html;charset=utf-8" %>


<%
	System.out.println("Inside nav.jsp");
	String url = "Employee_duty_configurationServlet?actionType=search";	
%>
<%@include file="../pb/navInitializer.jsp"%>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
				<input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text' class='form-control border-0'
					   onKeyUp='allfield_changed(false)' id='anyfield'  name='anyfield'
				>
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-group">
                <div class="tooltip tooltip-portlet tooltip bs-tooltip-top" role="tooltip" id="tooltip_p6zf7aqcpv"
                     aria-hidden="true" x-placement="top"
                     style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">
                    <div class="tooltip-arrow arrow" style="left: 34px;"></div>
                    <div class="tooltip-inner">Collapse</div>
                </div>
            </div>
        </div>
    </div>        
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">

			<div class="row">
				<div class="col-6" style = "display:none">
					<div class="form-group row">
						<label class="col-3 col-form-label"><%=LM.getText(LC.EMPLOYEE_DUTY_CONFIGURATION_ADD_DUTYCAT, loginDTO)%></label>
						<div class="col-9">
							<select class='form-control'  name='duty_cat' id = 'duty_cat' onSelect='setSearchChanged()'>
								<%										
									Options = CatRepository.getInstance().buildOptions("duty", Language, -1);
								%>
								<%=Options%>
							</select>

						</div>
					</div>
				</div>
				<div class="col-6">
                    <div class="form-group row">
                        <label class="col-3 col-form-label"><%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.EMPLOYEE_DUTY_CONFIGURATION_ADD_STARTDATE, loginDTO)%></label>					
						<div class="col-9">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="start_date_start_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="start_date_start" name="start_date_start">

						</div>
					</div>
				</div>			

				<div class="col-6">
					<div class="form-group row">
						<label class="col-3 col-form-label"><%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.EMPLOYEE_DUTY_CONFIGURATION_ADD_STARTDATE, loginDTO)%></label>
						<div class="col-9">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="start_date_end_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="start_date_end" name="start_date_end">

						</div>
					</div>
				</div>
				<div class="col-6">
                    <div class="form-group row">
                        <label class="col-3 col-form-label"><%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.EMPLOYEE_DUTY_CONFIGURATION_ADD_ENDDATE, loginDTO)%></label>					
						<div class="col-9">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="end_date_start_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="end_date_start" name="end_date_start">

						</div>
					</div>
				</div>			

				<div class="col-6">
					<div class="form-group row">
						<label class="col-3 col-form-label"><%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.EMPLOYEE_DUTY_CONFIGURATION_ADD_ENDDATE, loginDTO)%></label>
						<div class="col-9">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="end_date_end_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="end_date_end" name="end_date_end">

						</div>
					</div>
				</div>
				<div class="col-6">
                    <div class="form-group row">
                        <label class="col-3 col-form-label"><%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.EMPLOYEE_DUTY_CONFIGURATION_ADD_INSERTIONDATE, loginDTO)%></label>					
						<div class="col-9">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="insertion_date_start_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="insertion_date_start" name="insertion_date_start">

						</div>
					</div>
				</div>			

				<div class="col-6">
					<div class="form-group row">
						<label class="col-3 col-form-label"><%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.EMPLOYEE_DUTY_CONFIGURATION_ADD_INSERTIONDATE, loginDTO)%></label>
						<div class="col-9">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="insertion_date_end_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="insertion_date_end" name="insertion_date_end">

						</div>
					</div>
				</div>
		</div>
		<div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed(true)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp"%>


<template id = "loader">
<div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
</div>
</template>

<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>
<script type="text/javascript">

	function getParams()
	{
		var params = 'AnyField=' + document.getElementById('anyfield').value;

		if($("#duty_cat").val() != -1 && $("#duty_cat").val() != "")
		{
			params +=  '&duty_cat='+ $("#duty_cat").val();
		}
		$("#start_date_start").val(getDateStringById('start_date_start_js', 'DD/MM/YYYY'));
		params +=  '&start_date_start='+ getBDFormattedDate('start_date_start');
		$("#start_date_end").val(getDateStringById('start_date_end_js', 'DD/MM/YYYY'));
		params +=  '&start_date_end='+ getBDFormattedDate('start_date_end');		
		$("#end_date_start").val(getDateStringById('end_date_start_js', 'DD/MM/YYYY'));
		params +=  '&end_date_start='+ getBDFormattedDate('end_date_start');
		$("#end_date_end").val(getDateStringById('end_date_end_js', 'DD/MM/YYYY'));
		params +=  '&end_date_end='+ getBDFormattedDate('end_date_end');		
		$("#insertion_date_start").val(getDateStringById('insertion_date_start_js', 'DD/MM/YYYY'));
		params +=  '&insertion_date_start='+ getBDFormattedDate('insertion_date_start');
		$("#insertion_date_end").val(getDateStringById('insertion_date_end_js', 'DD/MM/YYYY'));
		params +=  '&insertion_date_end='+ getBDFormattedDate('insertion_date_end');		
		
		params +=  '&search=true';
		
		var extraParams = document.getElementsByName('extraParam');
		extraParams.forEach((param) => {
			params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

		var pageNo = document.getElementsByName('pageno')[0].value;
		var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

		var totalRecords = 0;
		var lastSearchTime = 0;
		if(document.getElementById('hidden_totalrecords'))
		{
			totalRecords = document.getElementById('hidden_totalrecords').value;
			lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
		}

		params += '&pageno=' + pageNo;
		params += '&RECORDS_PER_PAGE=' + rpp;
		params += '&TotalRecords=' + totalRecords;
		params += '&lastSearchTime=' + lastSearchTime;
		
		return params;
	}

	function allfield_changed(setPushState)
	{
		var params = getParams();
		navsubmit(params, "Employee_duty_configurationServlet", setPushState);
	}
	
	function resetFields()
	{
		$("#anyfield").val("");
		$("#duty_cat").val("-1");
		resetDateById('start_date_start_js');
        resetDateById('start_date_end_js');		
		resetDateById('end_date_start_js');
        resetDateById('end_date_end_js');		
		resetDateById('insertion_date_start_js');
        resetDateById('insertion_date_end_js');		
	}
	
	 window.addEventListener('popstate',e=>{
		if(e.state){
			let params = e.state;
			navsubmit(params, "Test_moduleServlet", false);
			resetFields();
			let arr = params.split('&');
			arr.forEach(e=>{
				let item = e.split('=');
				if(item.length === 2){
					//console.log("param " + item[0] + " = " + item[1]);
					switch (item[0]){
						case 'anyfield':	                        	
							$("#anyfield").val(item[1]);
							break;
						case 'AnyField':
							$("#anyfield").val(item[1]);
							break;
						case 'duty_cat':
							$("#duty_cat").val(item[1]);
							break;
						case 'start_date_start':
							setDateByTimestampAndId('start_date_start_js',item[1]);
							break;
						case 'start_date_end':
							setDateByTimestampAndId('start_date_end_js',item[1]);
							break;
						case 'end_date_start':
							setDateByTimestampAndId('end_date_start_js',item[1]);
							break;
						case 'end_date_end':
							setDateByTimestampAndId('end_date_end_js',item[1]);
							break;
						case 'insertion_date_start':
							setDateByTimestampAndId('insertion_date_start_js',item[1]);
							break;
						case 'insertion_date_end':
							setDateByTimestampAndId('insertion_date_end_js',item[1]);
							break;
					}
				}
			});
		}else{
			navsubmit(null, "Test_moduleServlet", false);
		}
	});
	
	window.onbeforeunload = function(){
		 var params = getParams();
		 history.pushState(params,'', 'Employee_duty_configurationServlet?actionType=search&'+params);
	};
	
	 $(document).ready(() => {
        readyInit('Employee_duty_configurationServlet');
    });

</script>

