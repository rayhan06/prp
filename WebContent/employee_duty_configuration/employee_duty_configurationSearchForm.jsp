
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="employee_duty_configuration.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
String navigator2 = "navEMPLOYEE_DUTY_CONFIGURATION";
String servletName = "Employee_duty_configurationServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.EMPLOYEE_DUTY_CONFIGURATION_ADD_ISOFFICE, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_NAME, loginDTO)%></th>

								<th><%=LM.getText(LC.EMPLOYEE_DUTY_CONFIGURATION_ADD_STARTDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.EMPLOYEE_DUTY_CONFIGURATION_ADD_ENDDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
							
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Employee_duty_configurationDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Employee_duty_configurationDTO employee_duty_configurationDTO = (Employee_duty_configurationDTO) data.get(i);
																																
											
											%>
											<tr>
								
		
											<td>
											<%=Utils.getYesNo(employee_duty_configurationDTO.isOffice, Language)%>
											</td>
		
											<td>
											<%=employee_duty_configurationDTO.isOffice?WorkflowController.getOfficeUnitName(employee_duty_configurationDTO.officeId, Language):WorkflowController.getNameFromOrganogramId(employee_duty_configurationDTO.employeeId, Language)%>
											</td>
		
											
		
											<td>
											<%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, employee_duty_configurationDTO.startDate), Language)%>
											</td>
		
											<td>
											<%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, employee_duty_configurationDTO.endDate), Language)%>
											</td>
		
		
		
		
		
		
		
	
											<%CommonDTO commonDTO = employee_duty_configurationDTO; %>
											<%@page import="sessionmanager.SessionConstants" %>
											<td>
											    <button
											            type="button"
											            class="btn-sm border-0 shadow bg-light btn-border-radius"
											            style="color: #ff6b6b;"
											            onclick="location.href='<%=servletName%>?actionType=view&ID=<%=commonDTO.iD%>'"
											    >
											        <i class="fa fa-eye"></i>
											    </button>
											</td>
											
																													
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			