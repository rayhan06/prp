

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="employee_duty_configuration.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Employee_duty_configurationServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Employee_duty_configurationDTO employee_duty_configurationDTO = Employee_duty_configurationDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = employee_duty_configurationDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.EMPLOYEE_DUTY_CONFIGURATION_ADD_EMPLOYEE_DUTY_CONFIGURATION_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.EMPLOYEE_DUTY_CONFIGURATION_ADD_EMPLOYEE_DUTY_CONFIGURATION_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.EMPLOYEE_DUTY_CONFIGURATION_ADD_ISOFFICE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getYesNo(employee_duty_configurationDTO.isOffice, Language)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.HM_NAME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=employee_duty_configurationDTO.isOffice?WorkflowController.getOfficeUnitName(employee_duty_configurationDTO.officeId, Language):WorkflowController.getNameFromOrganogramId(employee_duty_configurationDTO.employeeId, Language)%>
                                    </div>
                                </div>
			
								
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.EMPLOYEE_DUTY_CONFIGURATION_ADD_STARTDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, employee_duty_configurationDTO.startDate), Language)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.EMPLOYEE_DUTY_CONFIGURATION_ADD_ENDDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, employee_duty_configurationDTO.endDate), Language)%>
                                    </div>
                                </div>
                                
                                <div class="form-group row d-flex valign-items-top">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.HM_DATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                   		<div class="row">
	                                    <%
				                        	EmployeeDutyConfigurationDetailsDAO employeeDutyConfigurationDetailsDAO = EmployeeDutyConfigurationDetailsDAO.getInstance();
				                         	List<EmployeeDutyConfigurationDetailsDTO> employeeDutyConfigurationDetailsDTOs = 
				                         			employeeDutyConfigurationDetailsDAO.getAll(employee_duty_configurationDTO.isOffice, 
				                         					employee_duty_configurationDTO.officeId, 
				                         					employee_duty_configurationDTO.employeeId, 
				                         					employee_duty_configurationDTO.startDate, 
				                         					employee_duty_configurationDTO.endDate);
				                         	
				                         	for(EmployeeDutyConfigurationDetailsDTO employeeDutyConfigurationDetailsDTO: employeeDutyConfigurationDetailsDTOs)
				                         	{
				                         		%>
				                         			<div class="col-4">
															<%=Utils.getDigits(util.StringUtils.getFormattedDate(Language,employeeDutyConfigurationDetailsDTO.dutyOn), Language)%>
													</div>
				                         		<%
				                         		
				                         	}
				                         	
				                        %>
				                        </div>
				                    </div>
                                </div>
                                
                                <%
                                if(employee_duty_configurationDTO.isOffice)
                                {
                                	%>
                               <div class="form-group row d-flex valign-items-top">
                                    <label class="col-4 col-form-label text-right">
                                        <%=Language.equalsIgnoreCase("english")?"Applied to Employees":"নিম্নোক্ত কর্মকর্তাদের জন্য প্রযোজ্য"%>
                                    </label>
                                    <div class="col-8">
										<%
										if(employee_duty_configurationDTO.employees != null && !employee_duty_configurationDTO.employees.equalsIgnoreCase(""))
										{
											String []employees = employee_duty_configurationDTO.employees.split(", ");
											int j = 1;
											for(String sEmployee: employees)
											{
												long employee = Long.parseLong(sEmployee);
												%>
														<%=Utils.getDigits(j, Language)%>.
														<%=WorkflowController.getNameFromOrganogramId(employee, Language)%>, 
														<%=WorkflowController.getOrganogramName(employee, Language)%>, 
														<%=WorkflowController.getOfficeNameFromOrganogramId(employee, Language)%>
														<br>
												<%
												j ++;
											}
										}
										%>
                                    </div>
                                </div>
                                	<%
                                }
                                %>
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>