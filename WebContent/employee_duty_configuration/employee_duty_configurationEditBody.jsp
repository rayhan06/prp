<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="employee_duty_configuration.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>
<%@page import="java.util.Calendar" %>
<%@page import="pbReport.DateUtils" %>

<%
Employee_duty_configurationDTO employee_duty_configurationDTO = new Employee_duty_configurationDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	employee_duty_configurationDTO = Employee_duty_configurationDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = employee_duty_configurationDTO;
String tableName = "employee_duty_configuration";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.EMPLOYEE_DUTY_CONFIGURATION_ADD_EMPLOYEE_DUTY_CONFIGURATION_ADD_FORMNAME, loginDTO);
String servletName = "Employee_duty_configurationServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
         <jsp:include page="../employee_assign/employeeSearchModal.jsp">
             <jsp:param name="isHierarchyNeeded" value="false"/>
         </jsp:include>
        <form class="form-horizontal"
              action="Employee_duty_configurationServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-11">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=employee_duty_configurationDTO.iD%>' tag='pb_html'/>
														<input type='hidden' class='form-control-sm' name='isOffice' id = 'isOffice' value='false'
   																tag='pb_html'>
													
                                                      
                                                      <div class="form-group row">
				                                            <label class="col-4 col-form-label text-right">
				                                                <%=Language.equalsIgnoreCase("english")?"Employee/Office":"কর্মকর্তা/অফিস"%>	</label>
				                                            </label>
				                                            <div class="col-8">
				                                                <button type="button"
				                                                        class="btn text-white shadow form-control btn-border-radius"
				                                                        style="background-color: #4a87e2"
				                                                        onclick="addEmployee()"
				                                                        id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
				                                                </button>
				                                                <table class="table table-bordered table-striped"
				                                                       >
				                                                    <tbody id="employeeToSet"></tbody>
				                                                </table>
				                                            </div>
				                                        </div>									
														<input type='hidden' class='form-control'  name='employeeId' id = 'employeeId' value='-1' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='officeId' id = 'officeId' value='-1' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='startDate' id = 'startDate' value='' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='endDate' id = 'endDate' value='' tag='pb_html'/>
														
														<input type='hidden' class='form-control'  name='officeUnitType' id = 'officeUnitType' value='<%=employee_duty_configurationDTO.officeId%>' tag='pb_html'/>
																			
																					
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
																<%=LM.getText(LC.HM_YEAR, loginDTO)%>														</label>
                                                            <div class="col-8">
																<select class='form-control' name='year' id='dYear' onchange="processYMAndSubmit();">
												                    <option value=""><%=LM.getText(LC.HM_SELECT, loginDTO)%>
												                    </option>
												                    <%
												                        Calendar calendar = Calendar.getInstance();
												                        int currentYear = calendar.get(Calendar.YEAR);
												                        for (int j = currentYear; j >= currentYear - 10; j--) {
												                    %>
												                    <option value="<%=j%>"><%=Utils.getDigits(j, Language)%>
												                    </option>
												                    <%
												                        }
												                    %>
												                </select>
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.HM_MONTH, loginDTO)%>															</label>
                                                            <div class="col-8">
																<select class='form-control' name='month' id='dMonth' onchange="processYMAndSubmit();">
													                <option value=""><%=LM.getText(LC.HM_SELECT, loginDTO)%>
													                </option>
													                <%
													                    for (int j = 0; j < 12; j++) {
													                %>
													                <option value="<%=j%>"><%=DateUtils.getMonthName(j, Language)%>
													                </option>
													                <%
													                    }
													                %>
													            </select>		
															</div>
                                                      </div>
                                                      
                                                      <div class="form-group row" id = "cbDiv" style = "display:none">
                                                            <label class="col-2 col-form-label text-right">
															<%=Language.equalsIgnoreCase("english")?"Holidays":"ছুটির দিন"%>	</label>
                                                            <div class="col-1">
																<input type='checkbox' class='form-control-sm' name='holidays' id = 'holidays' value='true' onchange = "checkAll('holidays')"
   																tag='pb_html'>
															</div>
                                                      
                                                            <label class="col-2 col-form-label text-right">
															<%=Language.equalsIgnoreCase("english")?"Saturdays":"শনিবার"%>	</label>
                                                            <div class="col-1">
																<input type='checkbox' class='form-control-sm' name='saturdays' id = 'saturdays' value='true' onchange = "checkAll('saturdays')"
   																tag='pb_html'>
															</div>
                                                      
                                                            <label class="col-2 col-form-label text-right">
															<%=Language.equalsIgnoreCase("english")?"Fridays":"শুক্রবার"%>	</label>
                                                            <div class="col-1">
																<input type='checkbox' class='form-control-sm' name='fridays' id = 'fridays' value='true' onchange = "checkAll('fridays')"
   																tag='pb_html'>
															</div>
                                                      
                                                            <label class="col-2 col-form-label text-right">
															<%=Language.equalsIgnoreCase("english")?"Everyday":"প্রতিদিন"%>	</label>
                                                            <div class="col-1">
																<input type='checkbox' class='form-control-sm' name='everyday' id = 'everyday' value='true' onchange = "checkAll('everyday')"
   																tag='pb_html'>
															</div>
                                                      </div>
                                                      
                                                      <div class="kt-portlet__body form-body" id = "dnaDiv" style = "display:none">
										                <div id="dncalendar-container"></div>
										              </div>									
					
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
                 <div class="form-actions text-right mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.EMPLOYEE_DUTY_CONFIGURATION_ADD_EMPLOYEE_DUTY_CONFIGURATION_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.EMPLOYEE_DUTY_CONFIGURATION_ADD_EMPLOYEE_DUTY_CONFIGURATION_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>				

<%@include file="/dncalendar/dncalendarJs.jsp" %>
<%@include file="/dncalendar/dncalendarCss.jsp" %>
<script type="text/javascript">

var daysAdded = [];
var dutyDates;
function addToDaysAdded(date)
{
	if(daysAdded.indexOf(date) === -1)
	{
		console.log("adding = " + date);
		daysAdded.push(date);
	}
}

function removeFromDaysAdded(date)
{
	const index = daysAdded.indexOf(date);
	if (index > -1) {
		console.log("removing = " + date);
		daysAdded.splice(index, 1); // 2nd parameter means remove one item only
	}
}

function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);
	if($("#employeeId").val() == -1 && $("#officeId").val() == -1)
	{
		toastr.error("No employee or office selected");
		return false;
	}
	if($("#startDate").val() == '' || $("#endDate").val() == '')
	{
		toastr.error("No Year or Month chosen");
		return false;
	}
	submit();
	return false;
}

function submit() {
    buttonStateChange(true);
    console.log("submitting");
    var form = $("#bigform");
    var dates = "";
    for (let i = 0; i < daysAdded.length; i++) 
	{
    	dates += "&dutyOn=" + daysAdded[i];
	}
    console.log("dates = " + dates);
    $.ajax({
        type: "POST",
        url: form.attr('action'),
        data: form.serialize() + dates,
        dataType: 'JSON',
        success: function (response) {
            if (response.responseCode === 0) {
                console.log("Failed");
                showToastSticky(response.msg, response.msg);
                buttonStateChange(false);
            } else if (response.responseCode === 200) {
                console.log("Successfully added");
                window.location.replace(getContextPath() + response.msg);
            } else {
                console.log("Error: " + response.responseCode);
                buttonStateChange(false);
            }
        }
        ,
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                + ", Message: " + errorThrown);
            buttonStateChange(false);
        }
    });
}


function dutyChanged(duty)
{
	if(duty =='1')
	{
		var elem = $( "td[data-year='2022'][data-month ='4'][data-date ='12']" );
		toggleWorkingDay('12-4-2022', elem);
	}
	else if(duty =='2')
	{
		my_calendar.build(1, 5, 2022);
		/*my_calendar.update({
		    minDate: "2022-05-01",
		    maxDate: "2022-05-31"
		});*/
		
	}
	
	
}

function getElemFromDate(date)
{
	var splitted = date.split("-");
	var workingDayElem = $( "td[data-year='" + parseInt(splitted[0]) + "'][data-month ='" + parseInt(splitted[1]) + "'][data-date ='" + parseInt(splitted[2]) + "']" );
	return workingDayElem;
}

function addWorkingDay(date)
{
	console.log("addWorkingDay = " + date);
	var workingDayElem = getElemFromDate(date);
	if (daysAdded.indexOf(date) === -1) 
    {
		workingDayElem.addClass("default-date");
		addToDaysAdded(date);
    } 
}

function removeWorkingDay(date)
{
	var workingDayElem = getElemFromDate(date);
	
	workingDayElem.removeClass("default-date");
	removeFromDaysAdded(date);
     
}
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function normalizeDate(date)
{
	
	var splitted = date.split("-");
	var day = parseInt(splitted[2]) > 9? splitted[2]: "0" + splitted[2];
	var month = parseInt(splitted[1]) > 9? splitted[1]: "0" + splitted[1];
	var year = splitted[0];
	return (year + "-" + month + "-" + day);
}

var clickingTime = 0;

function toggleWorkingDay(date, workingDayElem) {
	const d = new Date();
	let time = d.getTime();
	
	if(time - clickingTime < 100)
	{
		return;
	}
	clickingTime = time;
	console.log("date = " + date);
	
	console.log(workingDayElem.attr('data-date'));
	var normalizedDate = normalizeDate(date);
	if (daysAdded.indexOf(normalizedDate) === -1) 
	{
		console.log("adding date " + normalizedDate);
	 	workingDayElem.addClass("default-date");
	 	addToDaysAdded(normalizedDate);
	} 
	else
	{
		console.log("removing date" + normalizedDate);
	 	workingDayElem.removeClass("default-date");
	 	removeFromDaysAdded(normalizedDate);
	}

}

function addDaysFromArray(arr)
{
	for (let i = 0; i < arr.length; i++) 
	{
		addWorkingDay(arr[i]);
	}
}

function removeDaysFromArray(arr)
{
	for (let i = 0; i < arr.length; i++) 
	{
		removeWorkingDay(arr[i]);
	}
}

function checkAll(type)
{
	var cb = $("#" + type);
	if(cb.prop("checked"))
	{
		if(type == "holidays")
		{
			addDaysFromArray(dutyDates.holidays);
		}
		else if(type == "saturdays")
		{
			addDaysFromArray(dutyDates.saturdays);
		}
		else if(type == "fridays")
		{
			addDaysFromArray(dutyDates.fridays);
		}
		else if(type == "everyday")
		{
			addDaysFromArray(dutyDates.everyday);
		}
	}
	else
	{
		if(type == "holidays")
		{
			removeDaysFromArray(dutyDates.holidays);
		}
		else if(type == "saturdays")
		{
			removeDaysFromArray(dutyDates.saturdays);
		}
		else if(type == "fridays")
		{
			removeDaysFromArray(dutyDates.fridays);
		}
		else if(type == "everyday")
		{
			removeDaysFromArray(dutyDates.everyday);
		}
	}
}

function processYMAndSubmit() {
	var startDate = 0;
    var endDate = 91605117600000;
    if ($("#dYear").val() != "" && $("#dMonth").val() != "") {
    	
        var year = parseInt($("#dYear").val());
        var month = parseInt($("#dMonth").val());
        var day = 1;
        console.log("year = " + year + " month = " + month + " day = " + day);
        startDate = new Date(year, month, day).getTime();

        month++;
        if (month == 12) {
            month = 0;
            year++;
        }
        endDate = new Date(year, month, day).getTime();
        
        console.log("startDate = " + startDate);
        console.log("endDate = " + endDate);
        
        $("#startDate").val(startDate);
        $("#endDate").val(endDate);
        
        my_calendar = $("#dncalendar-container").dnCalendar({

    	    showNotes: false,
    	    dayClick: function (date, view) {
    			console.log("dayclick called");
    	        toggleWorkingDay(date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate(),  $(this));
    	    },

    	});
        
        my_calendar.build(1, month, year);
        
        var employeeId =  $("#employeeId").val();
        var officeId =  $("#officeId").val();
        var isOffice = $("#isOffice").val();
        if(isOffice)
       	{
        	fetchCalendars(isOffice, officeId, startDate, endDate);
       	}
        else
        {
        	fetchCalendars(isOffice, employeeId, startDate, endDate);
        }
        
    }
    else
    {
    	$("#cbDiv").css("display", "none");
    	$("#dnaDiv").css("display", "none");
    }

    
    //ajaxSubmit();
}


function fetchCalendars(isOffice, id, startDate, endDate)
{
	console.log("fetchCalendars called");
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
        	dutyDates = JSON.parse(this.responseText);
        	daysAdded = [];
        	addDaysFromArray(dutyDates.workingdays);
        	$("#cbDiv").removeAttr("style");
        	$("#dnaDiv").removeAttr("style");

        } else if (this.readyState == 4 && this.status != 200) {
            alert('failed ' + this.status);
        }
    };
    var url = "Employee_duty_configurationServlet?actionType=getByEmpOrOffice&isOffice=" + isOffice + "&id="
        + id + "&startDate=" + startDate + "&endDate=" + endDate;
    xhttp.open("GET", url, false);
    xhttp.send();
}

function patient_inputted(userName, orgId) {
    console.log("patient_inputted " + userName);
    $("#employeeId").val(orgId);
    $("#officeId").val(-1);
    $("#isOffice").val('false');
    processYMAndSubmit();
}

function office_inputted(officeId)
{
	 $("#employeeId").val(-1);
	 $("#officeId").val(officeId);
	 $("#isOffice").val('true');
	 processYMAndSubmit();
}



function init(row)
{

	
	
}



var my_calendar;

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
	
	my_calendar = $("#dncalendar-container").dnCalendar({

	    showNotes: false,
	    dayClick: function (date, view) {
			console.log("dayclick called");
	        toggleWorkingDay(date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate(),  $(this));
	    },

	});

	// init calendar
	my_calendar.build();

	// update calendar
	my_calendar.update({
	    minDate: "2000-01-01"
	});
	
	$( "td[data-year='2022'][data-month ='4'][data-date ='13']" ).removeClass('today-date');

});	



</script>






