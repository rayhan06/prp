<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="employee_offices.*" %>
<%@ page import="pb.*" %>
<%@page import="workflow.WorkflowController"%>
<%@ page import="java.util.*" %>
<%@page contentType="text/html;charset=utf-8" %>
<%@ page import="pb.*" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.RECEPTIONIST_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row">
    <div class="col-12">
		<div  class="search-criteria-div" style = "display: none;">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.RECEPTIONIST_REPORT_WHERE_INSERTEDBYROLEID, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<input class='form-control'  name='insertedByRoleId' id = 'insertedByRoleId' value=""/>							
				</div>
			</div>
		</div>
		
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.RECEPTIONIST_REPORT_SELECT_INSERTEDBYUSERNAME, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select  class='form-control' name='inserted_by_user_name'
                          id='inserted_by_user_name' 
                          tag='pb_html' >
                          <option value=""><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
                          <%
                          Set<Long> emps = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.MEDICAL_RECEPTIONIST_ROLE);
                          emps.addAll(EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.MEDICAL_ADMIN_ROLE));
                          //emps.addAll(EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.ADMIN_ROLE));
                          for(Long em: emps)
                          {
                          %>
                          <option value = "<%=WorkflowController.getUserNameFromOrganogramId(em)%>">
                          <%=WorkflowController.getNameFromOrganogramId(em, Language)%>
                          </option>
                          <%
                          }
                          %>
                    </select>							
				</div>
			</div>
		</div>
        <%@include file="../pbreport/yearmonth.jsp"%>
        <%@include file="../pbreport/calendar.jsp"%>
    </div>
</div>
<script type="text/javascript">
var isVisible = [true, true, true, false];
function init()
{
	addables = [0, 0, 1];
	dateTimeInit($("#Language").val());
    $("#search_by_date").prop('checked', true);
    $("#search_by_date").trigger("change");
    setDateByStringAndId('startDate_js', '<%=datestr%>');
    setDateByStringAndId('endDate_js', '<%=datestr%>');
    add1WithEnd = false;
    processNewCalendarDateAndSubmit();
}
function PreprocessBeforeSubmiting()
{
}

function getLink(list)
{
	var startDate = $("#startDate").val();
	var endDate = $("#endDate").val();
	var erId = list[3];

	return "AppointmentServlet?actionType=getReceptionsAppointments&startDate=" + startDate + "&endDate=" + endDate + "&erId=" + erId;
}
</script>