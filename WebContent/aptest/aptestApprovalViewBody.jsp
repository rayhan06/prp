

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="aptest.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@ page import="approval_execution_table.*"%>
<%@ page import="approval_path.*"%>
<%@page import="user.*"%>
<%@page import="workflow.*"%>
<%@page import="util.*"%>



<%
	String servletName = "AptestServlet";
	String tableName = "aptest";
	String ID = request.getParameter("ID");
	AptestDAO aptestDAO = new AptestDAO("aptest");
	long id = Long.parseLong(ID);
	AptestDTO aptestDTO = (AptestDTO)aptestDAO.getDTOByID(id);
	CommonDTO commonDTO = aptestDTO;
%>
<%@include file="../approval_path/approvalViewBodyInitializer.jsp"%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.APTEST_ADD_APTEST_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.APTEST_ADD_APTEST_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>

			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.APTEST_ADD_NAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = aptestDTO.nameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.APTEST_ADD_NAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = aptestDTO.nameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.APTEST_ADD_DESCRIPTION, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = aptestDTO.description + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
			
			
			
		

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.HM_INITIATOR_REMARKS, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = approval_execution_table_initiationDTO.remarks + "";
                                        %>
                                        <%=value%>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.HM_INITIATOR_FILES, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            {
                                                List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(approval_execution_table_initiationDTO.fileDropzone);
                                        %>
											<%@include file="../pb/dropzoneViewer.jsp"%>
                                        <%
                                            }
                                        %>
                                    </div>
                                </div>
							</div>
                        </div>
                    </div>
                </div>
            </div>				

            <%@include file="../approval_path/approvalForms.jsp"%>
            <div class="row mt-5">
                <div class="col-12">
                    <%@include file="../approval_path/approvalButtons.jsp"%> 
                </div>
            </div>
        </div>
    </div>
</div>
        
        
<script type="text/javascript">
$(document).ready(function()
{
	console.log("using ckEditor");
     CKEDITOR.replaceAll();
});
 </script>