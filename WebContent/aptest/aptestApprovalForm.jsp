
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="aptest.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>


<%@ page import="util.*"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
String navigator2 = "navAPTEST";
%>
<%@include file="../pb/searchInitializer.jsp"%>
<%
Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
String servletName = "AptestServlet";
%>				
		
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.APTEST_ADD_NAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.APTEST_ADD_NAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.APTEST_ADD_DESCRIPTION, loginDTO)%></th>

								
								<th><%=LM.getText(LC.HM_APPROVAL_PATH, loginDTO)%></th>
								
								<th><%=LM.getText(LC.HM_INITIATED_BY, loginDTO)%></th>
							
								<th><%=LM.getText(LC.HM_DATE_OF_INITIATION, loginDTO)%></th>

								<th><%=LM.getText(LC.HM_ASSIGNED_TO, loginDTO)%></th>
								
								<th><%=LM.getText(LC.HM_ASSIGNMENT_DATE, loginDTO)%></th>
								
								<th><%=LM.getText(LC.HM_DUE_DATE, loginDTO)%></th>
								
								<th><%=LM.getText(LC.HM_STATUS, loginDTO)%></th>

								<th><%=LM.getText(LC.HM_WORKFLOW_ACTION, loginDTO)%></th>

								

								<th><%=LM.getText(LC.HM_HISTORY, loginDTO)%></th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList) session.getAttribute("viewAPTEST");

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											AptestDTO aptestDTO = (AptestDTO) data.get(i);
																																
											
											%>
											<tr id = 'tr_<%=i%>'>
											
											
		
											
											<td>
											<%
											value = aptestDTO.nameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td>
											<%
											value = aptestDTO.nameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td>
											<%
											value = aptestDTO.description + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
		
											
		
											
		
	
											<%CommonDTO commonDTO = aptestDTO; %>
		
											<%@include file="../approval_path/approvalApTd.jsp"%>

											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>
						</tbody>
					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />
<input type="hidden" id="ViewAll" value="<%=ViewAll%>" />
