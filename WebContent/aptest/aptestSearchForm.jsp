<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="aptest.*" %>
<%@ page import="util.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="pb.*" %>
<%@ page import="approval_execution_table.*" %>


<%
    String navigator2 = "navAPTEST";
    String servletName = "AptestServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>
<%
    Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
%>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.APTEST_ADD_NAMEEN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.APTEST_ADD_NAMEBN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.APTEST_ADD_DESCRIPTION, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.APTEST_SEARCH_APTEST_EDIT_BUTTON, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_HISTORY, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_SEND_TO_APPROVAL_PATH, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span>All</span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute("viewAPTEST");

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        AptestDTO aptestDTO = (AptestDTO) data.get(i);


        %>
        <tr>


            <td>
                <%
                    value = aptestDTO.nameEn + "";
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>

            <td>
                <%
                    value = aptestDTO.nameBn + "";
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>

            <td>
                <%
                    value = aptestDTO.description + "";
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>


            <%CommonDTO commonDTO = aptestDTO; %>
            <%@include file="../pb/searchAndViewButton.jsp" %>


            <%@include file="../approval_path/approvalTd.jsp" %>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			