<%@page import="util.CommonConstant" %>
<%@page import="org.apache.tomcat.util.bcel.classfile.ConstantLong" %>
<%@page import="user.UserRepository" %>
<%@page import="user.UserDTO" %>
<%@page import="util.ServletConstant" %>
<%@page import="util.ActionTypeConstant" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="util.JSPConstant" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="permission.ColumnRepository" %>
<%@page import="util.CollectionUtils" %>
<%@page import="role.PermissionRepository" %>
<%@page import="role.*" %>
<%@page import="permission.MenuRepository" %>
<%@page import="permission.*" %>
<%@page import="role.PermissionRepository" %>
<%@page import="config.GlobalConfigurationRepository" %>
<%@page import="config.GlobalConfigDTO" %>
<%@page import="java.util.*" %>
<%!
    private boolean isAnsestorByRootAndNode(MenuDTO root, MenuDTO node) {
        if (node == null) {
            return false;
        }
        if (node.menuID == root.menuID) {
            return true;
        }
        if (node.parentMenuID == -1) {
            return false;
        }
        MenuDTO parentMenu = MenuRepository.getInstance().getMenuDTOByMenuID(node.parentMenuID);
        return isAnsestorByRootAndNode(root, parentMenu);
    }


    private List<MenuDTO> alignMenuNames(MenuDTO menuDTO, String prefix) {

        List<MenuDTO> resultList = new ArrayList<MenuDTO>();
        resultList.add(menuDTO);

        menuDTO.menuName = prefix + menuDTO.menuName;
        menuDTO.menuNameBangla = prefix + menuDTO.menuNameBangla;
        prefix = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + prefix;


        if (menuDTO.getChildMenuList() != null) {
            for (MenuDTO childMenuDTO : menuDTO.getChildMenuList()) {
                resultList.addAll(alignMenuNames(childMenuDTO, prefix));
            }
        }


        return resultList;

    }
%>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    System.out.println("userDTO " + userDTO);
    List<MenuDTO> rootMenuList = new ArrayList<MenuDTO>(MenuRepository.getInstance().getRootMenuList());

    List<MenuDTO> allMenuList = new ArrayList<MenuDTO>();
    for (MenuDTO menu : rootMenuList) {
        allMenuList.addAll(alignMenuNames(menu, menu.isVisible ? "|--->" : ""));
    }

    RoleDTO roleDTO = (RoleDTO) request.getAttribute(ServletConstant.ROLE_DTO);
    String action = JSPConstant.ROLE_SERVLET + "?" + ActionTypeConstant.ACTION_TYPE + "=";
    String formTitle = "";
    long roleID = 0;
    if (roleDTO == null) {
        formTitle = LM.getText(LC.ROLE_ADD_ROLL_ADD, loginDTO);
        action = action + ActionTypeConstant.ROLE_ADD;
        roleDTO = (RoleDTO) request.getSession().getAttribute(ServletConstant.ROLE_DTO);
        if (roleDTO == null) roleDTO = new RoleDTO();
        else request.getSession().removeAttribute(ServletConstant.ROLE_DTO);

    } else {
        formTitle = LM.getText(LC.ROLE_ADD_ROLE_EDIT, loginDTO);
        action = action + ActionTypeConstant.ROLE_EDIT;
        roleID = roleDTO.ID;
    }
    String fieldError = "";
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-cogs"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form role="form" action="<%=action%>" class="form-horizontal" method="post">
            <div class="kt-portlet__body">
                <div class="form-body row">
                    <jsp:include page='../common/flushActionStatus.jsp'/>
                    <div class="col-md-6 form-group">
                        <div class="row">
                            <label class="col-md-3  col-form-label">
                                <%=LM.getText(LC.ROLE_ADD_ROLE_NAME, loginDTO) %>
                                <span class="required"> * </span></label>
                            <div class="col-md-9 ">
                                <input type="text" name="roleName" class="form-control" value="<%=roleDTO.roleName%>"/>
                                <input type="hidden" name="parentRoleID"/>
                                <input type="hidden" name="ID" value="<%=request.getParameter("ID")%>"/>
                            </div>
                            <%
                                fieldError = (String) request.getSession().getAttribute(ServletConstant.ROLE_NAME);
                                if (fieldError != null) {
                                    request.getSession().removeAttribute(ServletConstant.ROLE_NAME);
                            %>
                            <label><span class="label label-danger" id="rolenameError"><%=fieldError%></span></label>
                            <%}%>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <div class=" row">
                            <label class="col-md-3  col-form-label">
                                Description
                            </label>
                            <div class="col-md-9 ">
                                <input type="text" name="description" class="form-control"
                                       value="<%=roleDTO.description%>"/>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <input type="hidden" name="roleID" value="<%=roleID%>"/>
                        <%
                            int orderIndex = 0;

                            for (int isAPI = 0; isAPI <= 1; isAPI++) {
                        %>
                        <table id="tableData" class="table table-bordered table-striped text-nowrap">
                            <thead>
                            <tr>
                                <th>
                                    <%=(isAPI == 0 ? LM.getText(LC.ROLE_ADD_MENU, loginDTO) : LM.getText(LC.ROLE_ADD_API, loginDTO))%>
                                </th>
                                <th>
                                    <%=LM.getText(LC.ROLE_ADD_PERMISSION, loginDTO) %>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <%
                                for (MenuDTO menuDTO : allMenuList) {
                                    if (menuDTO.isAPI != (isAPI == 1)) {
                                        continue;
                                    }


                                    String menuName = "";
                                    if (userDTO.languageID == CommonConstant.Language_ID_Bangla) {
                                        menuName = menuDTO.menuNameBangla.replace(">", ">" + "<i class=\"" + menuDTO.icon + "\"></i> ");
                                    } else {
                                        menuName = menuDTO.menuName.replace(">", ">" + "<i class=\"" + menuDTO.icon + "\"></i> ");
                                    }
                                    ++orderIndex;
                            %>
                            <tr>
                                <td>
                                    <%=menuName%>
                                </td>
                                <td><input
                                        id="<%=menuDTO.menuID%>" <%=(PermissionRepository.checkPermissionByRoleIDAndMenuID(roleID, menuDTO.menuID)?"checked":"")%>
                                        type="checkbox" name="menuID" value="<%=menuDTO.menuID%>"
                                        onchange="checkMenuNode(this);"

                                        subtree-size="<%=MenuRepository.getInstance().getSubtreeMenuIDListByMenuID(menuDTO.menuID).size()%>"
                                        parent-id="<%=menuDTO.parentMenuID%>"
                                ></td>
                            </tr>

                            <%
                                }
                            %>
                            </tbody>
                        </table>
                        <% }
                        %>
                        <table id="tableData" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.ROLE_ADD_COLUMN, loginDTO) %>
                                </th>
                                <th><%=LM.getText(LC.ROLE_ADD_PERMISSION, loginDTO) %>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <%
                                List<ColumnDTO> columnList = ColumnRepository.getInstance().getAllColumnList();

                                for (ColumnDTO columnDTO : columnList) {
                                    boolean hasColumnPermission = PermissionRepository.checkPermissionByRoleIDAndColumnID(roleID, columnDTO.columnID);
                            %>
                            <tr>
                                <td>
                                    <%=columnDTO.columnName%>
                                </td>
                                <td><input id="<%=columnDTO.columnID%>"
                                    <%=(hasColumnPermission ? "checked" : "")%> type="checkbox"
                                           name="columnID" value="<%=columnDTO.columnID%>"></td>
                            </tr>
                            <%
                                }
                            %>
                            </tbody>
                        </table>
                    </div>
                    <%if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ROLE_EDIT)) { %>
                    <div class="form-actions text-right ml-auto mt-3">
                        <a class="btn btn-sm cancel-btn text-light shadow btn-border-radius"
                           href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.ROLE_ADD_CANCEL, loginDTO)%>
                        </a>
                        <button class="btn btn-sm  submit-btn text-light shadow btn-border-radius ml-2"
                                type="submit"><%=LM.getText(LC.ROLE_ADD_SUBMIT, loginDTO)%>
                        </button>
                    </div>
                    <%} %>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">

    function checkMenuNode(element) {
        //debugger;
        if (element.checked == true) {
            checkUpToRoot(element);
        }
        var descendentIDs = $(element).attr("descendentids");
        var span = element.parentElement;
        var div = span.parentElement;
        var td = div.parentElement;
        var tr = td.parentElement;
        var subtreeSize = $(element).attr("subtree-size");
        console.log("subtree size = " + subtreeSize);
        for (var i = 1; i < subtreeSize; i++) {
            tr = tr.nextElementSibling;
            var checkbox = tr.children[1].children[0].children[0].children[0];
            var spn = checkbox.parentElement;
            if ($(span).attr("class") == "checked") {
                $(spn).addClass("checked");
                checkbox.checked = true;
            } else {
                $(spn).removeClass("checked");
                checkbox.checked = false;
            }
        }
    }

    function checkUpToRoot(element) {
        var parentID = $(element).attr("parent-id");
        if (parentID == -1) {
            return;
        }
        var currentCheckBox = element;
        while ($(currentCheckBox).attr("id") != parentID) {
            var span = currentCheckBox.parentElement;
            var div = span.parentElement;
            var td = div.parentElement;
            var tr = td.parentElement;
            var prevTr = tr.previousElementSibling;
            var prevTd = prevTr.children[1];
            var prevDiv = prevTd.children[0];
            var prevSpan = prevDiv.children[0];
            var prevCheckbox = prevSpan.children[0];

            currentCheckBox = prevCheckbox;
        }
        $(currentCheckBox.parentElement).addClass("checked");
        currentCheckBox.checked = true;
        checkUpToRoot(currentCheckBox);
    }
</script>
