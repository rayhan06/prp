<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="office_shift.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="office_units.Office_unitsRepository" %>

<%
    Office_shiftDTO office_shiftDTO;
    office_shiftDTO = (Office_shiftDTO) request.getAttribute("office_shiftDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (office_shiftDTO == null) {
        office_shiftDTO = new Office_shiftDTO();

    }
    System.out.println("office_shiftDTO = " + office_shiftDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.OFFICE_SHIFT_ADD_OFFICE_SHIFT_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
%>

<%
    String Language = LM.getText(LC.OFFICE_SHIFT_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>


<%--
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">
		<h3 class="kt-subheader__title"> Asset Management </h3>
	</div>
</div>

<!-- end:: Subheader -->--%>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">

        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i> &nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal kt-form"
              action="Office_shiftServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">

            <input type='hidden' class='form-control' name='id' id='id_hidden_<%=i%>'
                   value='<%=office_shiftDTO.id%>' tag='pb_html'/>

            <div class="kt-portlet__body form-body mt-5">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: white"><%=formTitle%>
                                            </h4>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.OFFICE_SHIFT_ADD_OFFICEUNITID, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='officeUnitId_div_<%=i%>'>
                                                    <select class='form-control' name='officeUnitId'
                                                            id='officeUnitId_select2_<%=i%>' tag='pb_html'>
                                                        <%=Office_unitsRepository.getInstance().buildOptions(Language, office_shiftDTO.officeUnitId)%>
                                                    </select>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.OFFICE_SHIFT_ADD_NAMEEN, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='nameEn_div_<%=i%>'>
                                                    <input type='text' class='form-control rounded'
                                                           name='nameEn' id='nameEn_text_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + office_shiftDTO.nameEn + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.OFFICE_SHIFT_ADD_NAMEBN, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='nameBn_div_<%=i%>'>
                                                    <input type='text' class='form-control rounded'
                                                           name='nameBn' id='nameBn_text_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + office_shiftDTO.nameBn + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <input type='hidden' class='form-control' name='insertionDate'
                                               id='insertionDate_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + office_shiftDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>


                                        <input type='hidden' class='form-control' name='insertedBy'
                                               id='insertedBy_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + office_shiftDTO.insertedBy + "'"):("'" + "" + "'")%> tag='pb_html'/>


                                        <input type='hidden' class='form-control' name='modifiedBy'
                                               id='modifiedBy_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + office_shiftDTO.modifiedBy + "'"):("'" + "" + "'")%> tag='pb_html'/>


                                        <input type='hidden' class='form-control' name='isDeleted'
                                               id='isDeleted_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + office_shiftDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>


                                        <input type='hidden' class='form-control'
                                               name='lastModificationTime'
                                               id='lastModificationDate_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + office_shiftDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>


                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 mt-3 text-right">
                        <a class="btn btn-sm cancel-btn text-white shadow"
                           href="<%=request.getHeader("referer")%>">
                            <%=LM.getText(LC.OFFICE_SHIFT_ADD_OFFICE_SHIFT_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button class="btn btn-sm submit-btn text-white shadow ml-2" type="submit">
                            <%=LM.getText(LC.OFFICE_SHIFT_ADD_OFFICE_SHIFT_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


    $(document).ready(function () {

        dateTimeInit("<%=Language%>");

        $.validator.addMethod('officeUnitValidator', function (value, element) {
            return value != 0;

        });
        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                nameEn: "required",
                nameBn: "required",
                officeUnitId: {
                    required: true,
                    officeUnitValidator: true
                }
            },
            messages: {
                nameEn: '<%=Language.equalsIgnoreCase("English") ? "Kindly enter shift name in English!" : "অনুগ্রহ করে ইংরেজিতে শিফট এর নাম লিখুন!"%>',
                nameBn: '<%=Language.equalsIgnoreCase("English") ? "Kindly enter shift name in Bangla!" : "অনুগ্রহ করে বাংলায় শিফট এর নাম লিখুন!"%>',
                officeUnitId: '<%=Language.equalsIgnoreCase("English") ? "Kindly select office unit!" : "অনুগ্রহ করে অফিস ইউনিট সিলেক্ট করুন!"%>'
            }
        });
    });

    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Office_shiftServlet");
    }

    function init(row) {

        $("#officeUnitId_select2_" + row).select2({
            dropdownAutoWidth: true
        });


    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;


</script>