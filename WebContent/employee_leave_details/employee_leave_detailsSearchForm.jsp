<%@page import="sessionmanager.SessionConstants" %>
<%@page import="employee_leave_details.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@ page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>

<%
    String navigator2 = "";
    String servletName = "Employee_leave_detailsServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>

<%
    List<Employee_leave_detailsDTO> data = (List<Employee_leave_detailsDTO>) rn2.list;
    if (data != null && data.size() > 0) {
%>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_EMPLOYEERECORDSID, userDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LEAVETYPECAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LEAVESTARTDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LEAVEENDDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_JOININGDATEAFTERLEAVE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LEAVERELIEVERID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_CONTACTNUMBERDURINGLEAVE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_SEARCH_EMPLOYEE_LEAVE_DETAILS_EDIT_BUTTON, loginDTO)%>
            </th>
<%--            <th><%=LM.getText(LC.HM_SEND_TO_APPROVAL_PATH, loginDTO)%>--%>
<%--            </th>--%>
            <th class="text-center">
                <span><%="English".equalsIgnoreCase(Language) ? "All" : "সকল"%></span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            for (Employee_leave_detailsDTO employee_leave_detailsDTO : data) {
        %>
        <tr>
            <%@include file="employee_leave_detailsSearchRow.jsp" %>
        </tr>
        <% } %>
        </tbody>
    </table>
</div>
<%
} else {
%>
<label style="width: 100%;text-align: center;font-size: larger;font-weight: bold;color: red">
    <%=isLanguageEnglish ? "No information is found" : "কোন তথ্য পাওয়া যায় নি"%>
</label>
<%
    }
%>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>