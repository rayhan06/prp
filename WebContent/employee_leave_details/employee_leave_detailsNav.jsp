<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="java.util.*" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>

<%@page contentType="text/html;charset=utf-8" %>


<%
    String url = "Employee_leave_detailsServlet?actionType=search";
	int year = Calendar.getInstance().get(Calendar.YEAR);
%>
<%@include file="../pb/navInitializer.jsp" %>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6" >
                    <div class="form-group row">
                        <label
                                class="col-md-3 col-form-label"><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_SEARCH_LEAVETYPECAT, loginDTO)%>
                        </label>

                        <div class="col-md-9">
                            <select class='form-control' name='leave_type_cat' id='leave_type_cat'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=CatRepository.getInstance().buildOptions("leave_type", Language, -2)%>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6" >
                    <div class="form-group row">
                        <label
                                class="col-md-3 col-form-label"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
                        </label>

                        <div class="col-md-9">
                            <button type="button"
                                   class="btn text-white shadow form-control btn-border-radius"
                                   style="background-color: #4a87e2"
                                   onclick="addEmployee()"
                                   id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
                           	</button>
                            <table class="table table-bordered table-striped" >
                                <tbody id="employeeToSet"></tbody>
                            </table>
                            
                            <input type = "hidden" value = '' name = 'employee_records_id' id = 'employee_records_id' />
                        </div>
                    </div>
                </div>
                <div class="col-md-6" >
                    <div class="form-group row">
                        <label
                                class="col-md-3 col-form-label"><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_SEARCH_LEAVESTARTDATE, loginDTO)%>
                        </label>

                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="leave_start_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                                <jsp:param name="END_YEAR"
                                           value="<%=year + 10%>"/>
                            </jsp:include>
                            <input type='hidden'
                                   class='form-control'
                                   id='leave_start_date'
                                   name='leave_start_date'
                                   value='' tag='pb_html'/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" >
                    <div class="form-group row">
                        <label
                                class="col-md-3 col-form-label"><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_SEARCH_LEAVEENDDATE, loginDTO)%>
                        </label>

                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="leave_end_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                                 <jsp:param name="END_YEAR"
                                           value="<%=year + 10%>"/>
                            </jsp:include>

                            <input type='hidden'
                                   class='form-control'
                                   id='leave_end_date'
                                   name='leave_end_date'
                                   value='' tag='pb_html'/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit" class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="padding-right: 20px; padding-bottom: 20px">
	<button type="button"
	        class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
	        onclick="allfield_changed('',0, 1)"
	        style="background-color: #a8b194; float: right;">
	    <%=LM.getText(LC.HM_EXCEL, loginDTO)%>
	</button>
</div>
<!-- End: search control -->
<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">
    $(document).ready(() => {
        select2SingleSelector('#leave_type_cat', '<%=Language%>');
    });

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number, type) {
        $("#leave_start_date").val(getDateStringById("leave_start_date_js"));
        $("#leave_end_date").val(getDateStringById("leave_end_date_js"));
        var params = 'AnyField=' + document.getElementById('anyfield').value;

        if ($("#leave_type_cat").val()) {
            params += '&leave_type_cat=' + $("#leave_type_cat").val();
        }
        
        if ($("#employee_records_id").val() != '') 
        {
            params += '&employee_records_id=' + $("#employee_records_id").val();
        }
        
        
        var date_elements = document.getElementById('leave_start_date').value.split("/");
        var date = new Date(date_elements[2], date_elements[1] - 1, date_elements[0]);
        if (date_elements != "") {
            params += '&leave_start_date=' + date.getTime();
        }
        date_elements = document.getElementById('leave_end_date').value.split("/");
        date = new Date(date_elements[2], date_elements[1] - 1, date_elements[0]);
        if (date_elements != "") {
            params += '&leave_end_date=' + date.getTime();
        }
        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        if(type == 0 || typeof(type) == "undefined")
		{
        	dosubmit(params);
		}
		else
		{
			var url =  "Employee_leave_detailsServlet?actionType=xl&" + params;
			window.location.href = url;
		}

    }

    $(function () {

        $("#leave_start_date").datepicker({
            dateFormat: 'dd/mm/yy',
            yearRange: '1900:2100',
            changeYear: true,
            changeMonth: true,
            buttonText: "<i class='fa fa-calendar'></i>",
            onSelect: function (dateText) {
                $("#leave-end-date").datepicker('option', 'minDate', dateText);
            }
        });
        $("#leave_end_date").datepicker({
            dateFormat: 'dd/mm/yy',
            yearRange: '1900:2100',
            changeYear: true,
            changeMonth: true,
            buttonText: "<i class='fa fa-calendar'></i>"
        });


        $("#leave-start-date").datepicker('option', 'minDate', new Date());
        $("#leave_end_date").datepicker('option', 'minDate', new Date());


        $("#leave-start-date").val('');
        $("#leave_end_date").val('');

    });
    
    function patient_inputted(userName, orgId, employee_records_id) {
        console.log("patient_inputted " + userName);
        $("#employee_records_id").val(employee_records_id);
    }
</script>

