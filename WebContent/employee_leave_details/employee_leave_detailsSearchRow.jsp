<%@page import="workflow.WorkflowController"%>
<%@ page import="approval_execution_table.*" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<td>
    <%=Employee_recordsRepository.getInstance().getEmployeeName(employee_leave_detailsDTO.employeeRecordsId, Language) %>
    <br>
    <%=WorkflowController.getUserNameFromErId(employee_leave_detailsDTO.employeeRecordsId, Language) %>
</td>

<td>
    <%=CatRepository.getInstance().getText(Language, "leave_type", employee_leave_detailsDTO.leaveTypeCat)%>
</td>


<td>
    <%=StringUtils.getFormattedDate(Language, employee_leave_detailsDTO.leaveStartDate)%>
</td>


<td>
    <%=StringUtils.getFormattedDate(Language, employee_leave_detailsDTO.leaveEndDate)%>


</td>

<td>
    <%=StringUtils.getFormattedDate(Language, employee_leave_detailsDTO.joiningDateAfterLeave)%>


</td>

<td>
    <%=Employee_recordsRepository.getInstance().getEmployeeName(employee_leave_detailsDTO.leaveRelieverId,Language)%>
    <br>
    <%=WorkflowController.getUserNameFromErId(employee_leave_detailsDTO.leaveRelieverId, Language) %>
</td>


<td>
    <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(employee_leave_detailsDTO.contactNumberDuringLeave))%>
</td>

<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Employee_leave_detailsServlet?actionType=view&ID=<%=employee_leave_detailsDTO.iD%>&empId=<%=employee_leave_detailsDTO.employeeRecordsId%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Employee_leave_detailsServlet?actionType=getEditPage&ID=<%=employee_leave_detailsDTO.iD%>&empId=<%=employee_leave_detailsDTO.employeeRecordsId%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<%--<td>--%>
<%--    <%--%>
<%--        if (employee_leave_detailsDTO.jobCat == SessionConstants.DEFAULT_JOB_CAT) {--%>
<%--    %>--%>
<%--    <button type="button" class="btn btn-sm btn-success shadow btn-border-radius" data-toggle="modal" data-target="#sendToApprovalPathModal">--%>
<%--        <%=LM.getText(LC.HM_SEND_TO_APPROVAL_PATH, loginDTO)%>--%>
<%--    </button>--%>
<%--    <%@include file="../inbox_internal/sendToApprovalPathModal.jsp" %>--%>
<%--    <%--%>
<%--    } else {--%>
<%--    %>--%>
<%--    <%=LM.getText(LC.HM_NO_ACTION_IS_REQUIRED, loginDTO)%>--%>
<%--    <%--%>
<%--        }--%>
<%--    %>--%>
<%--</td>--%>

<td class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=employee_leave_detailsDTO.iD%>'/></span>
    </div>
</td>