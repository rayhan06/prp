<%@ page import="employee_leave_details.EmpLeaveDetails" %>
<%@ page import="java.util.List" %>

<input type="hidden" data-ajax-marker="true">
<table class="table table-striped table-bordered" style="font-size: 14px">
    <thead>
    <tr>
        <th><b><%= LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_LEAVETYPECAT, loginDTO) %>
        </b></th>
        <th><b><%= LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_LEAVESTARTDATE, loginDTO) %>
        </b></th>
        <th><b><%= LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_LEAVEENDDATE, loginDTO) %>
        </b></th>
        <th><b><%= LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_JOININGDATEAFTERLEAVE, loginDTO) %>
        </b></th>
        <th><b><%= LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_LEAVERELIEVERID, loginDTO) %>
        </b></th>
        <th><b><%= LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_CONTACTADDRESS, loginDTO) %>
        </b></th>
<%--        <th><b><%= LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_ISAPPROVED, loginDTO) %>--%>
<%--        </b></th>--%>
<%--        <th><b><%= LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_APPROVALDATE, loginDTO) %>--%>
<%--        </b></th>--%>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <%
        List<EmpLeaveDetails> savedEmpLeaveDetailsList = (List<EmpLeaveDetails>) request.getAttribute("savedEmpLeaveDetailsList");
        int leaveDetailsIndex = 0;
        if (savedEmpLeaveDetailsList != null && savedEmpLeaveDetailsList.size() > 0) {
            for (EmpLeaveDetails leaveDetailsItem : savedEmpLeaveDetailsList) {
                ++leaveDetailsIndex;
    %>
    <tr>
        <%@include file="/employee_leave_details/employee_leave_details_view_item.jsp" %>
    </tr>
    <%
            }
        }
    %>
    </tbody>
</table>


<div class="row">
    <div class="col-12 mt-3 text-right">
        <button class="btn btn-gray m-t-10 rounded-pill"
                onclick="location.href='<%=request.getContextPath()%>/Employee_leave_detailsServlet?actionType=getAddPage&tab=6&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
            <i class="fa fa-plus"></i>&nbsp;<%= LM.getText(LC.HR_MANAGEMENT_OTHERS_ADD_LEAVE_DETAILS, loginDTO) %>
        </button>
    </div>
</div>


