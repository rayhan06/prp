<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="employee_leave_details.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="files.*" %>
<%@ page import="geolocation.*" %>
<%@ page import="util.StringUtils" %>
<%@ page import="employee_records.Employee_recordsRepository" %>

<%
    String context = "../../.." + request.getContextPath() + "/assets/";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String value;
    String Language = LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LANGUAGE, loginDTO);
    boolean isLangEng = "English".equalsIgnoreCase(Language);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    EmpLeaveDetails empLeaveDetails = Employee_leave_detailsDAO.getInstance().getEmpLeaveDetails(id);
    FilesDAO filesDAO = new FilesDAO();
%>

<script src="<%=context%>scripts/util1.js"></script>
<style>
    .my-table {
        @extend .table;
        border-bottom: 0 !important;
    }

    .blank_row {
        height: 10px !important;
        background-color: #FFFFFF;
    }

    .double_blank_row {
        height: 20px !important;
        background-color: #FFFFFF;
    }

    .right-border {
        border-right: 1px solid black;
    }

    .top-border {
        border-top: 1px solid black;
    }

    .bottom-border {
        border-bottom: 1px solid black;
    }

    .top-table {
        min-height: 200px
    }

    .top-table tr th {
        border-bottom: 1px solid black;
        border-left: 1px solid black;
        border-top: 1px solid black;
    }

    .top-table tr td {
        border-bottom: 1px solid black;
        border-left: 1px solid black;
        padding-left: 5px;
        vertical-align: top;
    }
</style>

<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>
<div class="modal-content viewmodal" style="display: none">
    <div class="modal-body container">
        <div class="row div_border office-div" id="first-second-class-emp-detail-view">
            <div class="col-md-12">
                <div id="first-second-class-emp">
                    <table style="width: 100%;">
                        <tr class="double_blank_row"></tr>
                        <tr>
                            <td align="center"><b>Bangladesh Form No-2395</b></td>
                        </tr>
                        <tr class="double_blank_row"></tr>
                    </table>
                    <table>
                        <tr>
                            <td style="text-align: justify;"><span style="padding-left: 30px;">Application for leave of absence under Rule 3 of the Prescribed leave Rules,
		                        1959 of the Civil Service Regulations Rules........................................................................................
		                    of the Fundamental Rules.</span></td>
                        </tr>
                        <tr class="double_blank_row"></tr>
                    </table>
                    <table class="top-table">
                        <thead>
                        <tr>
                            <th style="width: 15%; vertical-align: top; text-align: center;">Name and Appointment of
                                Officer
                            </th>
                            <th style="width: 10%; vertical-align: top; text-align: center">Present basic pay</th>
                            <th style="width: 30%; vertical-align: top; text-align: center">Last leave enjoyed</th>
                            <th style="width: 30%; vertical-align: top; text-align: center">Period and nature of leave
                                applied for & for what reason
                            </th>
                            <th style="width: 15%; vertical-align: top; text-align: center; border-right: 1px solid black;">
                                Remarks by Controlling officer
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><%=empLeaveDetails.empNameEng%>
                            </td>
                            <td><%=empLeaveDetails.basicPayInEng%>
                            </td>
                            <td>
                                <%
                                    if (!empLeaveDetails.lastLeaveDetails.leaveTypeEng.equals("")) {
                                        value = "Type of leave : ".concat(empLeaveDetails.lastLeaveDetails.leaveTypeEng).concat("<br>")
                                                .concat("Day count of leave : ").concat(empLeaveDetails.lastLeaveDetails.leaveCountDaysEng).concat("<br>")
                                                .concat("Joining date after leave : ").concat(empLeaveDetails.lastLeaveDetails.joiningDateAfterLeaveEng);
                                    } else {
                                        value = "";
                                    }
                                %>
                                <%=value%>
                            </td>
                            <td>
                                <% value = "Type of leave : ".concat(empLeaveDetails.leaveTypeEng).concat("<br>")
                                        .concat("Day count of leave : ").concat(empLeaveDetails.leaveCountDaysEng).concat("<br>")
                                        .concat("Joining date after leave : ").concat(empLeaveDetails.joiningDateAfterLeaveEng).concat("<br>")
                                        .concat("Reason of leave : " + empLeaveDetails.comments);
                                %>
                                <%=value%>
                            </td>
                            <td style="border-right: 1px solid black;">No remarks</td>
                        </tr>
                        </tbody>
                    </table>
                    <table class="my-table">
                        <tr class="double_blank_row"></tr>
                        <tr>
                            <td colspan="2" style="text-align: justify;">N.B. The article of the Civil Service
                                Regulations or of the Fundamental Rules under which leave is claimed should
                                always be noted in column 6.3
                            </td>
                        </tr>
                        <tr class="double_blank_row"></tr>
                        <tr>
                            <td colspan="2">
		                        <span style="padding-left: 30px;text-align: justify;"> Any application to prefix or affix authorised holidays to leave under article 320 C.S.R or Rule 68 or the Fundamental Rules should be made to the authority sanctioning the leave at the time when the application for leave is submitted.
		                        </span>
                            </td>
                        </tr>
                        <tr class="double_blank_row"></tr>
                        <tr class="double_blank_row"></tr>
                        <tr class="double_blank_row"></tr>
                        <tr>
                            <td style="width: 60%;"></td>
                            <td style="width: 40%; vertical-align: top; text-align: center;" class="top-border">
                                Signature of application
                            </td>
                        </tr>
                        <tr class="double_blank_row"></tr>
                        <tr>
                            <td colspan="2">The.....................................</td>
                        </tr>
                    </table>
                    <hr style="border-color: black;">

                    <table>
                        <tr class="double_blank_row"></tr>
                        <tr>
                            <td style="width: 50%; padding-right: 10px;" colspan="2" class="right-border">
                                No.............................................date.................
                            </td>
                            <td style="width: 50%;padding-left: 5px;padding-left: 10px;" colspan="2">
                                No.............................................date.................
                            </td>
                        </tr>

                        <tr class="double_blank_row">
                            <td style="width: 50%;" colspan="2" class="right-border"></td>
                            <td style="width: 50%;" colspan="2"></td>
                        </tr>

                        <tr>
                            <td style="width: 50%; text-align: justify;padding-right: 10px;" colspan="2"
                                class="right-border">
                                Report of the Chief Accounts Officer, Bangladesh Parliament Secretariat, Segunbagicha,
                                Dhaka.
                            </td>
                            <td style="width: 50%; padding-left: 5px;text-align: justify;padding-left: 10px;"
                                colspan="2">
                                Forward to the Chief Accounts Office, Bangladesh Parliament Secretariat, for submission
                                to Goverment with the usual report.
                            </td>
                        </tr>
                        <tr class="double_blank_row">
                            <td style="width: 50%;" colspan="2" class="right-border"></td>
                            <td style="width: 50%;" colspan="2"></td>
                        </tr>
                        <tr class="double_blank_row">
                            <td style="width: 50%;" colspan="2" class="right-border"></td>
                            <td style="width: 50%;" colspan="2"></td>
                        </tr>
                        <tr class="double_blank_row">
                            <td style="width: 50%" colspan="2" class="right-border"></td>
                            <td style="width: 50%;" colspan="2"></td>
                        </tr>
                        <tr class="double_blank_row">
                            <td style="width: 50%;" colspan="2" class="right-border"></td>
                            <td style="width: 50%;" colspan="2"></td>
                        </tr>
                        <tr>
                            <td style="width: 30%; " class="bottom-border"></td>
                            <td style="width: 20%; " class="right-border"></td>
                            <td style="width: 20%; "></td>
                            <td style="width: 30%; " class="bottom-border"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;width: 30%; vertical-align: top;">Superintendent/A.A.O</td>
                            <td style="width: 20%" class="right-border"></td>
                            <td style="width: 20%;"></td>
                            <td style="text-align: center;width: 30%; vertical-align: top">Controlling Officer</td>
                        </tr>
                        <tr class="double_blank_row">
                            <td style="width: 50%" colspan="2" class="right-border"></td>
                            <td style="width: 50%;" colspan="2"></td>
                        </tr>
                        <tr class="double_blank_row">
                            <td style="width: 50%;" colspan="2" class="right-border"></td>
                            <td style="width: 50%;" colspan="2"></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-content viewmodal" style="display:none">
    <div class="modal-body container">
        <div class="row div_border office-div" id="detail-view">
            <div class="col-md-12">
                <table class="my-table" style="width: 100%">
                    <tr>
                        <td align="center">বাংলাদের জাতীয় সংসদ সচিবালয়</td>
                    </tr>
                    <tr>
                        <td align="center"><u>মানব সম্পদ শাখা-২</u></td>
                    </tr>
                    <tr class="blank_row"></tr>
                    <tr class="blank_row"></tr>
                    <tr>
                        <td align="center"><u>তৃতীয় শ্রেনীর কর্মচারীর অর্জিত ছুটি মঞ্জুরীর আবেদন পত্র</u></td>
                    </tr>
                    <tr class="blank_row"></tr>
                    <tr>
                        <td><span style="padding-left: 50px"> বিঃদ্রঃ ক্রমিক নং-১ হতে ৯ পর্যন্ত আবেদনকারী কর্তৃক পূরন করিতে হইবে। আবেদনকারীকে ১০নং ক্রমিকে অঙ্গীকারনামা অব্যশই পূরন করতে হবে।   </span>
                        </td>
                    </tr>
                    <tr class="blank_row"></tr>
                </table>
                <table class="my-table">
                    <tr>
                        <td style="width: 50px;margin-right: 30px;">১ ।</td>
                        <td style="width: 40%;">আবেদনকারীর নাম</td>
                        <td style="width: 40px;margin-right: 30px;vertical-align: bottom;">:</td>
                        <td style="vertical-align: bottom;"><%=empLeaveDetails.empNameBng%>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50px;margin-right: 30px;">২ ।</td>
                        <td style="width: 40%;">পদবী</td>
                        <td style="width: 40px;margin-right: 30px;vertical-align: bottom;">:</td>
                        <td><%=empLeaveDetails.designationBng%>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50px;margin-right: 30px;">৩ ।</td>
                        <td style="width: 40%;">ছুটি বিধি প্রযোজ্য</td>
                        <td style="width: 40px;margin-right: 30px;vertical-align: bottom;">:</td>
                        <td style="vertical-align: bottom;"></td>
                    </tr>
                    <tr>
                        <td style="width: 50px;margin-right: 30px;">৪ ।</td>
                        <td style="width: 40%;">মূল বেতন</td>
                        <td style="width: 40px;margin-right: 30px;vertical-align: bottom;">:</td>
                        <td style="vertical-align: bottom;"><%=empLeaveDetails.basicPayInBng%>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50px;margin-right: 30px; vertical-align: top;">৫ ।</td>
                        <td style="width: 40%;">বাড়ী ভাড়া, যাতায়াত ভাড়া, চিকিৎসা ভাতা, মহার্ঘ ভাতা এবং অন্যান্য ভাতা
                        </td>
                        <td style="width: 40px;margin-right: 30px;vertical-align: bottom;">:</td>
                        <td style="vertical-align: bottom;"></td>
                    </tr>
                    <tr>
                        <td style="width: 50px;margin-right: 30px;">৬ ।</td>
                        <td style="width: 40%;">আবেদনকৃত ছুটির বিবরন</td>
                        <td style="width: 40px;margin-right: 30px;vertical-align: bottom;"></td>
                        <td style="vertical-align: bottom;"></td>
                    </tr>
                    <tr>
                        <td style="width: 50px;margin-right: 30px;"></td>
                        <td style="width: 40%;">(ক)<span style="padding-left: 20px;">কি ধরনের ছুটি</span></td>
                        <td style="width: 40px;margin-right: 30px;vertical-align: bottom;">:</td>
                        <td style="vertical-align: bottom;">
                            <%=empLeaveDetails.leaveTypeBan %>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50px;margin-right: 30px;"></td>
                        <td style="width: 40%;">(খ) <span style="padding-left: 20px;">কত তারিখ হতে</span></td>
                        <td style="width: 40px;margin-right: 30px;vertical-align: bottom;">:</td>
                        <td style="vertical-align: bottom;" id="leave-start-date">
                                <%=empLeaveDetails.leaveStartDateBan %>
                        <td>
                    </tr>
                    <tr>
                        <td style="width: 50px;margin-right: 30px;"></td>
                        <td style="width: 40%;">(গ) <span style="padding-left: 20px;">মোট কত দিন</span></td>
                        <td style="width: 40px;margin-right: 30px;vertical-align: bottom;">:</td>
                        <td style="vertical-align: bottom;" id="leave-day-count">
                                <%=empLeaveDetails.leaveCountDaysBan %>
                        <td>
                    </tr>
                    <tr>
                        <td style="width: 50px;margin-right: 30px;">৭ ।</td>
                        <td style="width: 40%;">কি কারনে ছুটি প্রয়োজন</td>
                        <td style="width: 40px;margin-right: 30px;vertical-align: bottom;">:</td>
                        <td style="vertical-align: bottom;">
                            <%=empLeaveDetails.comments%>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50px;margin-right: 30px;">৮ ।</td>
                        <td style="width: 40%;">গত ছুটি ভোগের বিবরন</td>
                        <td style="width: 40px;margin-right: 30px;vertical-align: bottom;">:</td>
                        <td style="vertical-align: bottom;"></td>
                    </tr>
                    <tr>
                        <td style="width: 50px;margin-right: 30px;"></td>
                        <td style="width: 40%;">(ক) <span style="padding-left: 20px;">কতদিন ছুটি ভোগ করেছেন</span></td>
                        <td style="width: 40px;margin-right: 30px;vertical-align: bottom;">:</td>
                        <td style="vertical-align: bottom;" id="count-last-leave-day">
                                <%=empLeaveDetails.lastLeaveDetails.leaveCountDaysBan %>
                        <td>
                    </tr>
                    <tr>
                        <td style="width: 50px;margin-right: 30px;"></td>
                        <td style="width: 40%;">(খ) <span style="padding-left: 20px;">কি ধরনের ছুটি</span></td>
                        <td style="width: 40px;margin-right: 30px;vertical-align: bottom;">:</td>
                        <td style="vertical-align: bottom;" id="last-leave-type">
                                <%=empLeaveDetails.lastLeaveDetails.leaveTypeBan %>
                        <td>
                    </tr>
                    <tr>
                        <td style="width: 50px;margin-right: 30px;"></td>
                        <td style="width: 40%;">(গ) <span style="padding-left: 20px;">কত তারিখ থেকে ছুটি শেষ করে কাজে যোগদান করেছেন</span>
                        </td>
                        <td style="width: 40px;margin-right: 30px;vertical-align: bottom;">:</td>
                        <td style="vertical-align: bottom;"
                            id="joining-date-after-last-leave"><%=empLeaveDetails.lastLeaveDetails.joiningDateAfterLeaveBan %>
                        </td>
                    </tr>
                    <tr></tr>
                    <tr>
                        <td style="width: 50px;margin-right: 30px;vertical-align: top;">৯ ।</td>
                        <td style="width: 40%;">ছুটিকালীন ঠিকানা<br>(এক বা একাধিক)</td>
                        <td style="width: 40px;margin-right: 30px;vertical-align: bottom;">:</td>
                        <td style="vertical-align: bottom;"><%=GeoLocationUtils.getGeoLocationString(empLeaveDetails.contactAddress, "Bangla") %>
                        </td>
                    </tr>
                </table>
                <br>
                <div style="text-align: center;"><u>আবেদনকারীর অঙ্গীকারনামা</u></div>
                <br>
                <table class="my-table">
                    <tr>
                        <td style="width: 50px;margin-right: 30px;vertical-align: top;">১০ ।</td>
                        <td>আমি এই মর্মে অঙ্গীকার করছি যে, আমার ছুটি থাকার কারনে অথবা কোন কারনে অফিসে অনুপস্থিত থাকার
                            দায়ে বেতন/ভাতা
                            অথবা অন্য কোন সরকারি অর্থ আমার নিকট পাওনা থাকলে আমি তা কতৃপক্ষকে ফেরত দিতে বাধ্য থাকব ।
                        </td>
                    </tr>
                </table>
                <table class="my-table">
                    <tr>
                        <td style="width : 60%;"></td>
                        <td style="width: 20%;text-align: right;">আবেদনকারীর স্বাক্ষর :</td>
                        <td>Erfan</td>
                    </tr>
                    <tr>
                        <td style="width : 60%"></td>
                        <td style="width: 20%;text-align: right;">নাম :</td>
                        <td>মোঃ ইরফান হোসেন</td>
                    </tr>
                    <tr>
                        <td style="width : 60%"></td>
                        <td style="width: 20%;text-align: right;">পদবী :</td>
                        <td>বেকার</td>
                    </tr>
                    <tr>
                        <td style="width : 60%"></td>
                        <td style="width: 20%;text-align: right;">কার্যালয়/শাখা/ইউনিট :</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="width : 60%"></td>
                        <td style="width: 20%;text-align: right;">তারিখ :</td>
                        <td id="sign-date"></td>
                    </tr>
                </table>
                <br>
                <div style="text-align: center;">আবেদনকারীর কর্মরত অফিস/শাখা/ইউনিটের সংশ্লিষ্ট কর্মকর্তার মন্তব্য এবং
                    সুপারিশ
                </div>
                <br>
                <div style="text-align: center;"><u><b>কর্মকর্তার স্বাক্ষর ও সীলমোহর</b></u></div>
            </div>

        </div>
    </div>
</div>

<div class="modal-content viewmodal">
    <div class="modal-header">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-9 col-sm-12">
                    <h5 class="modal-title">

                    </h5>
                </div>

                <div class="col-md-3 col-sm-12">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-success app_register"
                               data-id="419637"> Register </a>
                        </div>
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-danger app_reject"
                               data-id="419637"> Reject </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-10"></div>
                <%
                    if (empLeaveDetails.empClass == 1 || empLeaveDetails.empClass == 2) {
                %>
                <div class="col-2">
                    <button type="button" class="btn btn-primary" id="first-second-cls-download-btn"
                            style="border-radius: 15px"><%=isLangEng ? "Download" : "ডাউনলোড"%>
                    </button>
                </div>
                <%}%>
                <%
                    if (empLeaveDetails.empClass == 3) {
                %>
                <div class="col-2">
                    <button type="button" class="btn btn-primary" id="file-download-btn" style="border-radius: 15px">
                        <%=isLangEng ? "Download" : "ডাউনলোড"%>
                    </button>
                </div>
                <%}%>
            </div>
        </div>
    </div>
    <div class="kt-portlet">
        <div class="kt-portlet__body form-body">
            <h5 class="table-title"><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_SEARCH_ANYFIELD, loginDTO)%>
            </h5>
            <div class="table-responsive">
                <table class="table table-bordered table-striped text-nowrap">
                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_EMPLOYEERECORDSID, loginDTO)%>
                            </b></td>
                        <td><%=Employee_recordsRepository.getInstance().getEmployeeName(empLeaveDetails.employeeRecordsId, Language)%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LEAVETYPECAT, loginDTO)%>
                        </b></td>
                        <td><%=isLangEng ? empLeaveDetails.leaveTypeEng : empLeaveDetails.leaveTypeBan%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LEAVESTARTDATE, loginDTO)%>
                            </b></td>
                        <td><%=isLangEng ? empLeaveDetails.leaveStartDateEng : empLeaveDetails.leaveStartDateBan%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LEAVEENDDATE, loginDTO)%>
                        </b></td>
                        <td><%=isLangEng ? empLeaveDetails.leaveEndDateEng : empLeaveDetails.leaveEndDateBan%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_JOININGDATEAFTERLEAVE, loginDTO)%>
                            </b></td>
                        <td><%=isLangEng ? empLeaveDetails.joiningDateAfterLeaveEng : empLeaveDetails.joiningDateAfterLeaveBan%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_COMMENTS, loginDTO)%>
                        </b></td>
                        <td><%=empLeaveDetails.comments%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LEAVERELIEVERID, loginDTO)%>
                            </b></td>
                        <td><%=isLangEng ? empLeaveDetails.leaveRelieverName : empLeaveDetails.leaveRelieverNameBn%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_CONTACTNUMBERDURINGLEAVE, loginDTO)%>
                            </b></td>
                        <td><%=Utils.getDigits(empLeaveDetails.contactNumberDuringLeave + "", Language)%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_CONTACTADDRESS, loginDTO)%>
                            </b></td>
                        <td><%=GeoLocationUtils.getGeoLocationString(empLeaveDetails.contactAddress, Language)%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_ISAPPROVED, loginDTO)%>
                        </b></td>
                        <td><%=StringUtils.getYesNo(Language, empLeaveDetails.isApproved)%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_APPROVALDATE, loginDTO)%>
                        </b></td>
                        <td><%=isLangEng ? empLeaveDetails.approvalDateEng : empLeaveDetails.approvalDateBan%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_FILESDROPZONE, loginDTO)%>
                        </b></td>
                        <td>
                            <% {
                                List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(empLeaveDetails.filesDropzone);
                            %>
                            <table>
                                <tr>
                                    <%
                                        if (FilesDTOList != null) {
                                            for (int j = 0; j < FilesDTOList.size(); j++) {
                                                FilesDTO filesDTO = FilesDTOList.get(j);
                                                byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                    %>
                                    <td>
                                        <%
                                            if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                        %>
                                        <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                             style='width:100px'/>
                                        <% }%>
                                        <a href='Employee_leave_detailsServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                           download><%=filesDTO.fileTitle%>
                                        </a>
                                    </td>
                                    <% }
                                    } %>
                                </tr>
                            </table>
                            <%}%>
                        </td>
                    </tr>
                </table>
            </div>
            <h5 class="table-title mt-5"><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_DESCRIPTIONOFLASTLEAVE, loginDTO)%>
            </h5>
            <div class="table-responsive">
                <table class="table table-bordered table-striped text-nowrap">
                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LASTDAYSOFLEAVE, loginDTO)%>
                            </b></td>
                        <td><%=isLangEng ? empLeaveDetails.lastLeaveDetails.leaveCountDaysEng : empLeaveDetails.lastLeaveDetails.leaveCountDaysBan%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LASTTYPEOFLEAVE, loginDTO)%>
                            </b></td>
                        <td><%=isLangEng ? empLeaveDetails.lastLeaveDetails.leaveTypeEng : empLeaveDetails.lastLeaveDetails.leaveTypeBan%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LASTJOININGDATEAFTERLEAVE, loginDTO)%>
                            </b></td>
                        <td><%=isLangEng ? empLeaveDetails.lastLeaveDetails.joiningDateAfterLeaveEng : empLeaveDetails.lastLeaveDetails.joiningDateAfterLeaveBan%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

        $('#file-download-btn').click(function () {
            downloadPdf('leave_detail.pdf', "detail-view");
        });
        $('#first-second-cls-download-btn').click(function () {
            downloadPdf('leave_detail.pdf', "first-second-class-emp-detail-view");
        });
    });
</script>