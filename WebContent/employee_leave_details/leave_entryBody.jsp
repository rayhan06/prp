<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="user.UserDTO" %>
<%@page import="user.UserRepository" %>
<%@page import="workflow.*" %>
<%@page import="employee_offices.*" %>
<%@ page import="pb.*" %>
<%@ page import="common.*" %>

<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
String Language = LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LANGUAGE, loginDTO);
boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
String actionName = "addEmployee";
String formTitle = LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_EMPLOYEE_LEAVE_DETAILS_ADD_FORMNAME, loginDTO);
String errorLevelEng = "", errorLevelBng = "";
if(userDTO.roleID == SessionConstants.HR_1_ROLE)
{
	errorLevelEng = "1st class";
	errorLevelBng = "প্রথম শ্রেণীর";
}
else if(userDTO.roleID == SessionConstants.HR_1_ROLE)
{
	errorLevelEng = "2nd class";
	errorLevelBng = "দ্বিতীয় শ্রেণীর";
}
else if(userDTO.roleID == SessionConstants.HR_1_ROLE)
{
	errorLevelEng = "3rd and 4th class";
	errorLevelBng = "তৃতীয় ও চতুর্থ শ্রেণীর";
}
String errorString = "You can only select " + errorLevelEng + " employees.";
if(!isLanguageEnglish)
{
	errorString = "আপনি কেবলমাত্র " + errorLevelBng + " কর্মকর্তা/কর্মচারীদের ছুটি এন্ট্রি দিতে পারেন।";
}
boolean isPrivilleged = Utils.isPrivillegedHRUser(userDTO);
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<div class="kt-content  kt-grid__item kt-grid__item--fluid"
	id="kt_content" style="padding: 0px !important; margin-bottom: -18px">
	<div class="kt-portlet">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title prp-page-title">
					<i class="fa fa-gift"></i>&nbsp;
					<%=formTitle%>
				</h3>
			</div>
		</div>
		
			<div class="kt-portlet__body form-body">
				<div class="row">
					<div class="col-md-10 offset-md-1">
						<div class="onlyborder">
							<div class="row">
								<div class="col-md-10 offset-md-1">
									<div class="sub_title_top">
										<div class="sub_title">
											<h4 style="background: white"><%=formTitle%>
											</h4>
										</div>
										
										<input type = "hidden" value = '-1' name = 'empId' id = 'empId' />
										
										<div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                            	<%
                                            	if (
                                            			isPrivilleged
                                            			|| userDTO.roleID == SessionConstants.HR_1_ROLE
                                            			|| userDTO.roleID == SessionConstants.HR_2_ROLE
                                            			|| userDTO.roleID == SessionConstants.HR_3_ROLE
                                            		)
                                            	{
                                            		%>
                                            		<button type="button"
                                                        class="btn text-white shadow form-control btn-border-radius"
                                                        style="background-color: #4a87e2"
                                                        onclick="addEmployee()"
                                                        id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
                                                	</button>
	                                                <table class="table table-bordered table-striped" >
	                                                    <tbody id="employeeToSet"></tbody>
	                                                </table>
                                            		<%
                                            	}
                                            	else
                                            	{
                                            	%>
                                            	<select  class='form-control' name='emp_selected'
						                          id='emp_selected' onchange = 'setEmId(this.value)'
						                          tag='pb_html' >
						                          <option value=""><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
						                          <%
						                          Set<Long> emps = EmployeeOfficeRepository.getInstance().getByOffice(WorkflowController.getOfficeIdFromOrganogramId(userDTO.organogramID));
						                          
						                          //emps.addAll(EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.ADMIN_ROLE));
						                          for(Long em: emps)
						                          {
						                          %>
						                          <option value = "<%=WorkflowController.getEmployeeRecordIDFromOrganogramID(em)%>">
						                          <%=WorkflowController.getNameFromOrganogramId(em, Language)%>, 
						                          <%=WorkflowController.getOrganogramName(em, Language)%>
						                          </option>
						                          <%
						                          }
						                          %>
						                    	</select>
						                    	<%
                                            	}
						                    	%>
						                    	<span  style = "color:red" >
						                    		<h4 id = "errorDiv">
						                    		</h4>
                                        		</span>
                                            </div>
                                            
                                        </div>
                                        
                                        
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		
	</div>
</div>


<script type="text/javascript">
	function setEmId(value)
	{
		$("#empId").val(value);
		location.href = 'Employee_leave_detailsServlet?actionType=getAddPage&empId=' + value;
	}
	
	function patient_inputted(userName, orgId, empId) {
        console.log("patient_inputted " + userName);
        var errorDiv = $("#errorDiv");
        errorDiv.html("");
        <%
        if(userDTO.roleID == SessionConstants.HR_1_ROLE)
        {
        	%>
        	if(!userName.startsWith("1"))
        	{
        		errorDiv.html("<%=errorString%>");
        		return;
        	}
        	<%
        }
        else if(userDTO.roleID == SessionConstants.HR_2_ROLE)
        {
        	%>
        	if(!userName.startsWith("2"))
        	{
        		errorDiv.html("<%=errorString%>");
        		return;
        	}
        	<%
        }
        else if(userDTO.roleID == SessionConstants.HR_3_ROLE)
        {
        	%>
        	if(!userName.startsWith("3") && !userName.startsWith("4"))
        	{
        		errorDiv.html("<%=errorString%>");
        		return;
        	}
        	<%
        }
        %>
        setEmId(empId);
    }
</script>
