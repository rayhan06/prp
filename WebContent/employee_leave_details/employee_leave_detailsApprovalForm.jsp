
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="employee_leave_details.*"%>
<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}

String value = "";
String Language = LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LANGUAGE, loginDTO);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


Employee_leave_detailsDAO employee_leave_detailsDAO = (Employee_leave_detailsDAO)request.getAttribute("employee_leave_detailsDAO");
String ViewAll;
if(request.getParameter("ViewAll") != null)
{
	ViewAll= request.getParameter("ViewAll");
}
else
{
	ViewAll= "0";
}

String navigator2 = SessionConstants.NAV_EMPLOYEE_LEAVE_DETAILS;
System.out.println("navigator2 = " + navigator2);
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
System.out.println("rn2 = " + rn2);
String pageno2 = ( rn2 == null ) ? "1" : "" + rn2.getCurrentPageNo();
String totalpage2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalPages();
String totalRecords2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalRecords();
String lastSearchTime = ( rn2 == null ) ? "0" : "" + rn2.getSearchTime();
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

String successMessageForwarded = "Forwarded to your Senior Office";
String successMessageApproved = "Approval Done";
%>				
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_EMPLOYEERECORDSID, loginDTO)%></th>
								<th><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LEAVETYPECAT, loginDTO)%></th>
								<th><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LEAVESTARTDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LEAVEENDDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_COMMENTS, loginDTO)%></th>
								<th><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LEAVERELIEVERID, loginDTO)%></th>
								<th><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_CONTACTNUMBERDURINGLEAVE, loginDTO)%></th>
								<th><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_CONTACTADDRESS, loginDTO)%></th>
								<th><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_JOBCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_ISAPPROVED, loginDTO)%></th>
								<th><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_APPROVALDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_FILESDROPZONE, loginDTO)%></th>

								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>
								
								<th><%=LM.getText(LC.HM_APPROVAL_PATH, loginDTO)%></th>
								
								<th><%=LM.getText(LC.HM_INITIATED_BY, loginDTO)%></th
								
								<th><%=LM.getText(LC.HM_DATE_OF_INITIATION, loginDTO)%></th>
								
								<th><%=LM.getText(LC.HM_ASSIGNED_TO, loginDTO)%></th>
								
								<th><%=LM.getText(LC.HM_ASSIGNMENT_DATE, loginDTO)%></th>
								
								<th><%=LM.getText(LC.HM_DUE_DATE, loginDTO)%></th>
								
								<th><%=LM.getText(LC.HM_STATUS, loginDTO)%></th>

								<th><%=LM.getText(LC.HM_WORKFLOW_ACTION, loginDTO)%></th>
								
								<th><%=LM.getText(LC.HM_VALIDATE, loginDTO)%></th>
								

								<th><%=LM.getText(LC.HM_HISTORY, loginDTO)%></th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_EMPLOYEE_LEAVE_DETAILS);

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Employee_leave_detailsDTO employee_leave_detailsDTO = (Employee_leave_detailsDTO) data.get(i);
																																
											
											%>
											<tr id = 'tr_<%=i%>'>
											<%
											
								%>
											
		
								<%  								
								    request.setAttribute("employee_leave_detailsDTO",employee_leave_detailsDTO);
								%>  
								
								 <jsp:include page="./employee_leave_detailsApprovalRow.jsp">
								 		<jsp:param name="pageName" value="searchrow" />
								 		<jsp:param name="rownum" value="<%=i%>" />
								 </jsp:include>			

								
								<%

											%>
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />
<input type="hidden" id="ViewAll" value="<%=ViewAll%>" />
