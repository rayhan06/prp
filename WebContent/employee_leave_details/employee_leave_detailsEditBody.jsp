<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="employee_leave_details.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="user.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="employee_records.*" %>
<%@ page import="java.util.concurrent.TimeUnit" %>
<%@ page import="util.StringUtils" %>
<%@ page import="leave_reliever_mapping.Leave_reliever_mappingDTO" %>
<%@ page import="leave_reliever_mapping.Leave_reliever_mappingDAO" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="leave_reliever_mapping.Leave_reliever_mappingRepository" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="common.BaseServlet" %>

<%
    Employee_leave_detailsDTO employee_leave_detailsDTO, lastEmployeeLeaveDetailsDTO = null;
    employee_leave_detailsDTO = (Employee_leave_detailsDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    System.out.println("userDTO = " + userDTO);

    if (employee_leave_detailsDTO == null) {
        employee_leave_detailsDTO = new Employee_leave_detailsDTO();
    }
    System.out.println("employee_leave_detailsDTO = " + employee_leave_detailsDTO);
    String actionName;
    String empId = request.getParameter("empId");
    if (empId == null) {
        empId = String.valueOf(request.getAttribute("empId"));
        if (empId.equals("null") && userDTO != null) 
        {
            empId = String.valueOf(userDTO.employee_record_id);
            System.out.println("empId now 1 = " + empId);
        } else {
            empId = "0";
            System.out.println("empId now 2 = " + empId);
        }

    }
    System.out.println("actionType = " + request.getParameter("actionType"));
    System.out.println("empId = " + empId);
    Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().getById(Long.parseLong(empId));
    Employee_leave_detailsDAO employeeLeaveDetailsDAO = Employee_leave_detailsDAO.getInstance();
    EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(Long.parseLong(empId));

    Leave_reliever_mappingDTO leave_reliever_mappingDTO;
    EmployeeFlatInfoDTO leaveReliever1 = null;
    EmployeeFlatInfoDTO leaveReliever2 = null;
    List<Employee_leave_detailsDTO> leaveDetailsReliever1 = null;
    List<Employee_leave_detailsDTO> leaveDetailsReliever2 = null;
    List<Leave_reliever_mappingDTO> leave_reliever_mappingDTOS = null;
    OfficeUnitOrganograms officeUnitOrganograms = null;
    Office_unitsDTO officeUnitsDTO = null;
    if (employeeOfficeDTO != null) {
        officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
        officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
        leave_reliever_mappingDTOS = Leave_reliever_mappingRepository.getInstance().getLeave_reliever_mappingDTOByorganogram_id(officeUnitOrganograms.id);
    }

    if (leave_reliever_mappingDTOS != null && leave_reliever_mappingDTOS.size() > 0) {
        leave_reliever_mappingDTO = leave_reliever_mappingDTOS.get(0);
        leaveReliever1 = Leave_reliever_mappingDAO.buildFlatDTO(leave_reliever_mappingDTO.leaveReliever1);
        leaveReliever2 = Leave_reliever_mappingDAO.buildFlatDTO(leave_reliever_mappingDTO.leaveReliever2);
        Employee_leave_detailsDAO employee_leave_detailsDAO = Employee_leave_detailsDAO.getInstance();
        leaveDetailsReliever1 = employee_leave_detailsDAO.getLeaveDetaiForReleiver(leaveReliever1.employeeRecordsId);
        leaveDetailsReliever2 = employee_leave_detailsDAO.getLeaveDetaiForReleiver(leaveReliever2.employeeRecordsId);
    }
    
    System.out.println("employee_recordsDTO = " + employee_recordsDTO);
    System.out.println("employee_leave_detailsDTO = " + employee_leave_detailsDTO);

    employee_leave_detailsDTO.contactNumberDuringLeave = employee_recordsDTO.personalMobile;

    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
        lastEmployeeLeaveDetailsDTO = employeeLeaveDetailsDAO.getLastLeaveDTO(Long.parseLong(empId));
        if (lastEmployeeLeaveDetailsDTO != null) {
            employee_leave_detailsDTO.lastLeaveId = lastEmployeeLeaveDetailsDTO.iD;
        }
    } else {
        actionName = "edit";
        if (employee_leave_detailsDTO.lastLeaveId != -1) {
            lastEmployeeLeaveDetailsDTO = (Employee_leave_detailsDTO) employeeLeaveDetailsDAO.getDTOByID(employee_leave_detailsDTO.lastLeaveId);
        }

    }
    long countOfDaysLastLeave = 0;
    if (lastEmployeeLeaveDetailsDTO != null) {
        Date startDate = new Date(lastEmployeeLeaveDetailsDTO.leaveStartDate);
        Date endDate = new Date(lastEmployeeLeaveDetailsDTO.leaveEndDate);
        long diffInMillies = Math.abs(endDate.getTime() - startDate.getTime());
        countOfDaysLastLeave = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS) + 1;
    }

    String formTitle;
    if (actionName.equals("edit")) {
        formTitle = LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_EMPLOYEE_LEAVE_DETAILS_EDIT_FORMNAME, loginDTO);
    } else {
        formTitle = LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_EMPLOYEE_LEAVE_DETAILS_ADD_FORMNAME, loginDTO);
    }

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    int childTableStartingID = 1;

    long ColumnID;
    FilesDAO filesDAO = new FilesDAO();
    boolean canValidate = false;
    String url = "Employee_leave_detailsServlet?actionType=" + "ajax_" + actionName + "&isPermanentTable=true&empId=" + empId + "&iD=" + ID;
    if (request.getParameter("tab") != null) {
        url += "&tab=" + request.getParameter("tab") + "&userId=" + request.getParameter("userId");
    }

    String Language = LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    int year = Calendar.getInstance().get(Calendar.YEAR);
%>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <form class="form-horizontal kt-form" action="<%=url%>"
              id="emp_leave_details_form" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-lg-12 px-5">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD'
                                           id='iD_hidden_<%=i%>'
                                           value='<%=employee_leave_detailsDTO.iD%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='employeeRecordsId'
                                           id='employeeRecordsId_text'
                                           value='<%=empId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='jobCat'
                                           id='jobCat_select'
                                           value='<%=employee_leave_detailsDTO.jobCat%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastLeaveId'
                                           id='last_leave_id'
                                           value='<%=employee_leave_detailsDTO.lastLeaveId%>'
                                           tag='pb_html'/>
                                    <div class="card my-4">
                                        <div class="card-header">
                                            <h3 style="margin-left: 1%;"><%=isLanguageEnglish ? "Employee Details" : "কর্মচারী তথ্য"%>
                                            </h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped text-nowrap">
                                                    <tr>

                                                        <td style="width:30%">
                                                            <b><%=isLanguageEnglish ? "Employee Name" : "কর্মচারীর নাম"%>
                                                            </b></td>
                                                        <td><%=isLanguageEnglish ? employee_recordsDTO.nameEng : employee_recordsDTO.nameBng%>
                                                        </td>
                                                        <td style="width:30%">
                                                            <b><%=isLanguageEnglish ? "Phone Number" : "ফোন নাম্বার"%>
                                                            </b></td>
                                                        <td><%=StringUtils.convertBanglaIfLanguageIsBangla(Language, employee_recordsDTO.personalMobile != null ? employee_recordsDTO.personalMobile : "")%>
                                                        </td>

                                                    </tr>
                                                    <tr>

                                                        <td style="width:30%">
                                                            <b><%=isLanguageEnglish ? "Designation" : "পদবি"%>
                                                            </b></td>
                                                        <td><%=officeUnitOrganograms != null ? (isLanguageEnglish ? officeUnitOrganograms.designation_eng : officeUnitOrganograms.designation_bng) : ""%>
                                                        </td>
                                                        <td style="width:30%">
                                                            <b><%=isLanguageEnglish ? "Office" : "অফিস"%>
                                                            </b></td>
                                                        <td><%=employeeOfficeDTO != null ? (isLanguageEnglish ?
                                                                (officeUnitsDTO.unitNameEng == null ? "" : officeUnitsDTO.unitNameEng) :
                                                                (officeUnitsDTO.unitNameBng == null ? "" : officeUnitsDTO.unitNameBng)) : ""%>
                                                        </td>

                                                    </tr>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card my-4">
                                        <div class="card-header">
                                            <h3 style="margin-left: 1%;">
                                                <%=isLanguageEnglish ? "Leave Substitute Details" : "ছুটি চলাকালীন বিকল্প কর্মকর্তা তথ্য"%>
                                            </h3>
                                        </div>
                                        <div class="card-body">
                                            <%if (leave_reliever_mappingDTOS == null || leave_reliever_mappingDTOS.size() == 0) {%>
                                            <div class="form-group ">
                                                <div class="col-md-12">
                                                    <span class="text-nowrap" style="color: red;font-weight: bolder">
                                                        <%=isLanguageEnglish ? "No leave substitute found!" : "কোনো ছুটি চলাকালীন বিকল্প কর্মকর্তা পাওয়া যায় নি!"%>
                                                    </span>
                                                </div>


                                            </div>
                                            <%} else {%>
                                            <div class="form-group">
                                                <div id="leaveRelieverId1_div">
                                                    <%if (leaveReliever1 != null) {%>
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped text-nowrap">
                                                            <thead>
                                                            <th style="width:30%">
                                                                <%=LM.getText(LC.LEAVE_RELIEVER_MAPPING_SEARCH_LEAVERELIEVER1, loginDTO)%>
                                                            </th>
                                                            <th style="width:30%"><%=isLanguageEnglish ? leaveReliever1.employeeNameEn : leaveReliever1.employeeNameBn%>
                                                            </th>
                                                            <th style="width:40%"><%=isLanguageEnglish ? (leaveReliever1.organogramNameEn + ", " + leaveReliever1.officeNameEn)
                                                                    : (leaveReliever1.organogramNameBn + ", " + leaveReliever1.officeNameBn)%>
                                                            </th>
                                                            </thead>

                                                            <tbody>
                                                            <%
                                                                int index = 0;
                                                                if (leaveDetailsReliever1 != null) {
                                                                    for (Employee_leave_detailsDTO employeeLeaveDetailsDTO : leaveDetailsReliever1) {
                                                                        index++;
                                                            %>
                                                            <tr>
                                                                <td style="width:30%"><%=isLanguageEnglish ? "Leave " : "ছুটি "%>
                                                                    <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(index))%>
                                                                </td>
                                                                <td style="width:30%">
                                                                    <%=StringUtils.getFormattedDate(Language, employeeLeaveDetailsDTO.leaveStartDate)%>
                                                                </td>
                                                                <td style="width:40%">
                                                                    <%=StringUtils.getFormattedDate(Language, employeeLeaveDetailsDTO.leaveEndDate)%>
                                                                </td>


                                                            </tr>
                                                            <%
                                                                    }
                                                                }
                                                            %>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <%}%>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div id="leaveRelieverId2_div">
                                                    <%if (leaveReliever2 != null) {%>
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped text-nowrap">
                                                            <thead>
                                                            <th style="width:30%">
                                                                <%=LM.getText(LC.LEAVE_RELIEVER_MAPPING_SEARCH_LEAVERELIEVER2, loginDTO)%>
                                                            </th>
                                                            <th style="width:30%"><%=isLanguageEnglish ? leaveReliever2.employeeNameEn : leaveReliever2.employeeNameBn%>
                                                            </th>
                                                            <th style="width:40%"><%=isLanguageEnglish ? (leaveReliever2.organogramNameEn + ", " + leaveReliever2.officeNameEn)
                                                                    : (leaveReliever2.organogramNameBn + ", " + leaveReliever2.officeNameBn)%>
                                                            </th>
                                                            </thead>

                                                            <tbody>
                                                            <%
                                                                int index = 0;
                                                                if (leaveDetailsReliever2 != null) {
                                                                    for (Employee_leave_detailsDTO employeeLeaveDetailsDTO : leaveDetailsReliever2) {
                                                                        index++;

                                                            %>
                                                            <tr>
                                                                <td style="width:30%">
                                                                    <%=isLanguageEnglish ? "Leave " : "ছুটি "%>
                                                                    <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(index))%>
                                                                </td>
                                                                <td style="width:30%">
                                                                    <%=StringUtils.getFormattedDate(Language, employeeLeaveDetailsDTO.leaveStartDate)%>
                                                                </td>
                                                                <td style="width:40%">
                                                                    <%=StringUtils.getFormattedDate(Language, employeeLeaveDetailsDTO.leaveEndDate)%>
                                                                </td>


                                                            </tr>
                                                            <%
                                                                    }
                                                                }
                                                            %>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <%}%>
                                                </div>
                                            </div>
                                            <%}%></div>
                                    </div>
                                    <div class="card my-4">
                                        <div class="card-header">
                                            <h3 style="margin-left: 1%;"><%=isLanguageEnglish ? "Leave Date Details" : "ছুটির তারিখের তথ্য"%>
                                            </h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LEAVETYPECAT, loginDTO)) : (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_LEAVETYPECAT, loginDTO))%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9 " id='leaveTypeCat_div_<%=i%>'>
                                                    <select class='form-control' name='leaveTypeCat'
                                                            id='leaveTypeCat_category'
                                                            tag='pb_html'>
                                                        <%=CatRepository.getInstance().buildOptions("leave_type", Language, employee_leave_detailsDTO.leaveTypeCat)%>
                                                    </select>

                                                </div>

                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LEAVESTARTDATE, loginDTO)) : (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_LEAVESTARTDATE, loginDTO))%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9 " id='leaveStartDate_div_<%=i%>'>

                                                    <jsp:include page="/date/date.jsp">
                                                        <jsp:param name="DATE_ID"
                                                                   value="leave-start-date-js"></jsp:param>
                                                        <jsp:param name="LANGUAGE"
                                                                   value="<%=Language%>"></jsp:param>
                                                        <jsp:param name="END_YEAR"
                                                                   value="<%=year + 10%>"/>
                                                    </jsp:include>


                                                    <input type='hidden' class='form-control'
                                                           id='leave-start-date' name='leaveStartDate'
                                                           value=''
                                                           tag='pb_html'/>
                                                </div>

                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LEAVEENDDATE, loginDTO)) : (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_LEAVEENDDATE, loginDTO))%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9 " id='leaveEndDate_div_<%=i%>'>

                                                    <jsp:include page="/date/date.jsp">
                                                        <jsp:param name="DATE_ID"
                                                                   value="leave-end-date-js"></jsp:param>
                                                        <jsp:param name="LANGUAGE"
                                                                   value="<%=Language%>"></jsp:param>
                                                        <jsp:param name="END_YEAR"
                                                                   value="<%=year + 10%>"/>
                                                    </jsp:include>
                                                    <input type='hidden' class='form-control'
                                                           id='leave-end-date' name='leaveEndDate'
                                                           value=''
                                                           tag='pb_html'/>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_JOININGDATEAFTERLEAVE, loginDTO)) : (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_JOININGDATEAFTERLEAVE, loginDTO))%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9 "
                                                     id='joiningDateAfterLeave_div_<%=i%>'>
                                                    <jsp:include page="/date/date.jsp">
                                                        <jsp:param name="DATE_ID"
                                                                   value="joining-date-after-leave-js"></jsp:param>
                                                        <jsp:param name="LANGUAGE"
                                                                   value="<%=Language%>"></jsp:param>
                                                        <jsp:param name="END_YEAR"
                                                                   value="<%=year + 10%>"/>
                                                    </jsp:include>
                                                    <input type='hidden' class='form-control'
                                                           id='joining-date-after-leave'
                                                           name='joiningDateAfterLeave' value=''
                                                           tag='pb_html'/>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LEAVERELIEVERID, loginDTO)) : (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_LEAVERELIEVERID, loginDTO))%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <label class="col-md-9 col-form-label text-md-left"
                                                       id='setLeaveReliever'></label>
                                            </div>
                                            <input type='hidden' class='form-control'
                                                   name='leaveRelieverId'
                                                   id='leaveRelieverId'
                                                   value='<%=employee_leave_detailsDTO.leaveRelieverId%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="card my-4">
                                        <div class="card-header">
                                            <h3 style="margin-left: 1%;"><%=isLanguageEnglish ? "Other Information" : "অন্যান্য তথ্য"%>
                                            </h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_COMMENTS, loginDTO)) : (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_COMMENTS, loginDTO))%>
                                                </label>

                                                <div class="col-md-9 " id='comments_div_<%=i%>'>
                                                                    <textarea type='text' class='form-control'
                                                                              name='comments' id='comments_text_<%=i%>'
                                                                              rows="4" maxlength="1024"
                                                                              style="text-align: left;resize: none"><%=employee_leave_detailsDTO.comments == null ? "" : employee_leave_detailsDTO.comments.trim()%></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_CONTACTNUMBERDURINGLEAVE, loginDTO)) : (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_CONTACTNUMBERDURINGLEAVE, loginDTO))%>
                                                </label>

                                                <div class="col-md-9 " id='contactNumberDuringLeave_div_<%=i%>'>
                                                    <div class="input-group mb-2">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">+88</div>
                                                        </div>
                                                        <input type='text' class='digitOnly form-control rounded'
                                                               name='contactNumberDuringLeave' maxlength="11"
                                                               id='contactNumberDuringLeave_text_<%=i%>'
                                                               value='<%=employee_leave_detailsDTO.contactNumberDuringLeave ==null || employee_leave_detailsDTO.contactNumberDuringLeave.length() == 0?"":
                                                                   (employee_leave_detailsDTO.contactNumberDuringLeave.startsWith("88") ? employee_leave_detailsDTO.contactNumberDuringLeave.substring(2) : employee_leave_detailsDTO.contactNumberDuringLeave)%>'
                                                               placeholder='<%=LM.getText(LC.GLOBAL_MOBILE_PLACE_HOLDER, loginDTO)%>'
                                                               title='<%=isLanguageEnglish?"Mobile number must start with 01, then contain 9 digits"
                                                               :"মোবাইল নাম্বার 01 দিয়ে শুরু হবে, তারপর ৯টি সংখ্যা হবে"%>'>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_CONTACTADDRESS, loginDTO)) : (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_CONTACTADDRESS, loginDTO))%>
                                                </label>

                                                <div class="col-md-9">
                                                    <jsp:include page="/geolocation/geoLocation.jsp">
                                                        <jsp:param name="GEOLOCATION_ID"
                                                                   value="contactAddress_js"></jsp:param>
                                                        <jsp:param name="LANGUAGE"
                                                                   value="<%=Language%>"></jsp:param>
                                                    </jsp:include>
                                                    <input type="hidden" id="contactAddress"
                                                           name="contactAddress">
                                                </div>


                                            </div>

                                        </div>
                                    </div>
                                    <div class="card my-4">
                                        <div class="card-header">
                                            <h3 style="margin-left: 1%;"><%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_DESCRIPTIONOFLASTLEAVE, loginDTO)) : (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_DESCRIPTIONOFLASTLEAVE, loginDTO))%>
                                            </h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LASTDAYSOFLEAVE, loginDTO)) : (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_LASTDAYSOFLEAVE, loginDTO))%>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type='text' class='form-control'
                                                           value='<%=countOfDaysLastLeave == 0 ? "":StringUtils.convertBanglaIfLanguageIsBangla(Language,String.valueOf(countOfDaysLastLeave))%>'
                                                           tag='pb_html'
                                                           disabled="disabled"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LASTTYPEOFLEAVE, loginDTO)) : (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_LASTTYPEOFLEAVE, loginDTO))%>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type='text' class='form-control'
                                                           tag='pb_html'
                                                           disabled="disabled"
                                                           value='<%=lastEmployeeLeaveDetailsDTO==null ? "" : CatRepository.getInstance().getText(Language,"leave_type",lastEmployeeLeaveDetailsDTO.leaveTypeCat)%>'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LASTJOININGDATEAFTERLEAVE, loginDTO)) : (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_LASTJOININGDATEAFTERLEAVE, loginDTO))%>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type='text' class='form-control'
                                                           tag='pb_html'
                                                           disabled="disabled"
                                                           value='<%=lastEmployeeLeaveDetailsDTO == null ? "": StringUtils.getFormattedDate(Language,lastEmployeeLeaveDetailsDTO.joiningDateAfterLeave)%>'/>
                                                </div>
                                            </div>


                                            <input type='hidden' class='form-control' name='isApproved'
                                                   id='isApproved_checkbox_<%=i%>'
                                                   value='true' <%=(actionName.equals("edit") && String.valueOf(employee_leave_detailsDTO.isApproved).equals("true"))?("checked"):""%>
                                                   tag='pb_html'><br>
                                            <input type='hidden' class='form-control'
                                                   name='approvalDate'
                                                   id='approvalDate_hidden_<%=i%>'
                                                   value=<%=actionName.equals("edit")?("'" + employee_leave_detailsDTO.approvalDate + "'"):("''")%> tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_FILESDROPZONE, loginDTO)) : (LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_ADD_FILESDROPZONE, loginDTO))%>
                                        </label>
                                        <div class="col-md-9 " id='filesDropzone_div_<%=i%>'>
                                            <%
                                                if (actionName.equals("edit")) {
                                                    List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(employee_leave_detailsDTO.filesDropzone);
                                            %>
                                            <table>
                                                <tr>
                                                    <%
                                                        if (filesDropzoneDTOList != null) {
                                                            for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                                                FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                                                byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                    %>
                                                    <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                                        <%
                                                            if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                        %>
                                                        <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)
						%>' style='width:100px'/>
                                                        <%
                                                            }
                                                        %>
                                                        <a href='Employee_leave_detailsServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                           download><%=filesDTO.fileTitle%>
                                                        </a>
                                                        <a class='btn btn-danger'
                                                           onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>
                                                    </td>
                                                    <%
                                                            }
                                                        }
                                                    %>
                                                </tr>
                                            </table>
                                            <%
                                                }
                                            %>

                                            <%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>
                                            <div class="dropzone"
                                                 action="Employee_leave_detailsServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?employee_leave_detailsDTO.filesDropzone:ColumnID%>">
                                                <input type='file' style="display:none"
                                                       name='filesDropzoneFile'
                                                       id='filesDropzone_dropzone_File_<%=i%>'
                                                       tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='filesDropzoneFilesToDelete'
                                                   id='filesDropzoneFilesToDelete_<%=i%>'
                                                   value='' tag='pb_html'/>
                                            <input type='hidden' name='filesDropzone'
                                                   id='filesDropzone_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=actionName.equals("edit")?employee_leave_detailsDTO.filesDropzone:ColumnID%>'/>


                                        </div>
                                    </div>
                                    <%
                                        if (canValidate) {
                                    %>
                                    <div class="row div_border attachement-div">
                                        <div class="col-md-12">
                                            <h5><%=LM.getText(LC.HM_ATTACHMENTS, loginDTO)%>
                                            </h5>
                                            <%
                                                ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                            %>
                                            <div class="dropzone"
                                                 action="Approval_execution_tableServlet?pageType=add&actionType=UploadFilesFromDropZone&columnName=approval_attached_fileDropzone&ColumnID=<%=ColumnID%>">
                                                <input type='file' style="display: none"
                                                       name='approval_attached_fileDropzoneFile'
                                                       id='approval_attached_fileDropzone_dropzone_File_<%=i%>'
                                                       tag='pb_html'/>
                                            </div>
                                            <input type='hidden'
                                                   name='approval_attached_fileDropzoneFilesToDelete'
                                                   id='approval_attached_fileDropzoneFilesToDelete_<%=i%>'
                                                   value=''
                                                   tag='pb_html'/> <input type='hidden'
                                                                          name='approval_attached_fileDropzone'
                                                                          id='approval_attached_fileDropzone_dropzone_<%=i%>'
                                                                          tag='pb_html'
                                                                          value='<%=ColumnID%>'/>
                                        </div>

                                        <div class="col-md-12">
                                            <h5><%=LM.getText(LC.HM_REMARKS, loginDTO)%>
                                            </h5>

                                            <textarea class='form-control' name='remarks'
                                                      id='<%=i%>_remarks' tag='pb_html'></textarea>
                                        </div>
                                    </div>
                                    <%
                                        }
                                    %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-3 text-right">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button">
                        <%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_EMPLOYEE_LEAVE_DETAILS_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <%--                    <%--%>
                    <%--                        if (leave_reliever_mappingDTOS != null && leave_reliever_mappingDTOS.size() > 0) {--%>
                    <%--                    %>--%>
                    <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                            type="button" onclick="submitForm()">
                        <%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_EMPLOYEE_LEAVE_DETAILS_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                    <%--                    <%--%>
                    <%--                        }--%>
                    <%--                    %>--%>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script type="text/javascript">
    const leaveForm = $('#emp_leave_details_form');
    const isEnglish = <%=isLanguageEnglish%>
        let
    reliever1 = new Map(
        <%if(leaveDetailsReliever1 != null){%>
        <%=leaveDetailsReliever1.stream()
                        .map(detailsDTO -> "['" + detailsDTO.iD + "'," + detailsDTO.getIdsStrInJsonString() + "]")
                        .collect(Collectors.joining(",","[","]"))
        %>
        <%}%>
    );

    let reliever2 = new Map(
        <%if(leaveDetailsReliever2 != null){%>
        <%=leaveDetailsReliever2.stream()
                        .map(detailsDTO -> "['" + detailsDTO.iD + "'," + detailsDTO.getIdsStrInJsonString() + "]")
                        .collect(Collectors.joining(",","[","]"))
        %>
        <%}%>
    );

    let leaveReleiver1Id = <%=leaveReliever1!=null?leaveReliever1.employeeRecordsId:0%>;
    let leaveReleiver2Id = <%=leaveReliever2!=null?leaveReliever2.employeeRecordsId:0%>;

    $(document).ready(function () {
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
        setGeoLocation('<%=employee_leave_detailsDTO.contactAddress%>', 'contactAddress_js');
        select2SingleSelector("#leaveTypeCat_category", '<%=Language%>');
        select2SingleSelector("#contactAddress_geoSelectField_0", '<%=Language%>');
        init(row);
        dateTimeInit("<%=Language%>");
        initValidation();
        let action = '<%=actionName%>';
        if (action === "edit") {
            let reliever = <%=employee_leave_detailsDTO.leaveRelieverId%>;
            if (leaveReleiver1Id === reliever && reliever !== 0) {
                if (isEnglish) {
                    $('#setLeaveReliever').text('Duty on leave reliever 1');
                } else {
                    $('#setLeaveReliever').text('প্রথম অধিপ্রাপ্ত কর্মকর্তার উপর দায়িত্ব অর্পিত');
                }
            } else if (leaveReleiver2Id === reliever && reliever !== 0) {
                if (isEnglish) {
                    $('#setLeaveReliever').text('Duty on leave reliever 2');
                } else {

                    $('#setLeaveReliever').text('দ্বিতীয় অধিপ্রাপ্ত কর্মকর্তার উপর দায়িত্ব অর্পিত');
                }
            } else {
                <%
                if (leave_reliever_mappingDTOS != null && leave_reliever_mappingDTOS.size() > 0) {
            %>
                if (isEnglish) {
                    $('#setLeaveReliever').text('Select leave date.');
                } else {
                    $('#setLeaveReliever').text('ছুটির তারিখ বাছাই করুন');
                }
                <%
                }else{
                %>
                if (isEnglish) {
                    $('#setLeaveReliever').text('Please add leave relieve');
                } else {
                    $('#setLeaveReliever').text('অধিপ্রাপ্ত কর্মকর্তা যুক্ত করুন');
                }
                <%
                }
                %>
            }
        } else {
            <%
                if (leave_reliever_mappingDTOS != null && leave_reliever_mappingDTOS.size() > 0) {
            %>
            if (isEnglish) {
                $('#setLeaveReliever').text('Select leave date.');
            } else {
                $('#setLeaveReliever').text('ছুটির তারিখ বাছাই করুন');
            }
            <%
            }else{
            %>
            if (isEnglish) {
                $('#setLeaveReliever').text('Please add leave relieve');
            } else {
                $('#setLeaveReliever').text('অধিপ্রাপ্ত কর্মকর্তা যুক্ত করুন');
            }
            <%
            }
            %>


        }
    });

    function initValidation() {
      
        $.validator.addMethod('leaveTypeCatSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('jobCatSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('startDateSelection', function (value, element) {
            if (value == "") {
                return false;
            }
        });
        $.validator.addMethod('endDateSelection', function (value, element) {
            if (value == "") {
                return false;
            }
            let sd = $('#leaveStartDate_date').val();
            // let today = new Date();
            // today.setHours(0, 0, 0, 0);
            let ed = convertToDate(value);
            // if (sd == "") {
            //     return ed >= today;
            // } else {
            sd = convertToDate(sd);
            // return sd >= today && ed >= sd;
            return ed >= sd;
            // }
        });
        $.validator.addMethod('joiningDateSelection', function (value, element) {
            if (value == "") {
                return false;
            }
            let ed = $('#leaveEndDate_date').val();
            // let today = new Date();
            // today.setHours(0, 0, 0, 0);
            let jd = convertToDate(value);
            // if (ed == "") {
            //     return jd > today;
            // } else {
            ed = convertToDate(ed);
            // return jd > today && jd > ed;
            return jd > ed;
            // }
        });
        $("#emp_leave_details_form").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                leaveTypeCat: {
                    required: true,
                    leaveTypeCatSelection: true
                },
                employeeRecordsId: {
                    required: true,
                }
            },

            messages: {
                leaveTypeCat: "<%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_EMPLOYEE_LEAVE_DETAILS_PLEASE_SELECT_A_LEAVE_TYPE, userDTO)%>",
                employeeRecordsId: "<%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_EMPLOYEE_LEAVE_DETAILS_PLEASE_ENTER_EMPLOYEE_ID, userDTO)%>"
            }
        });
    }

    function processDate(date) {
        var parts = date.split("/");
        var date = new Date(parts[2] + "/" + parts[1] + "/" + parts[0]);
        return date.getTime();
    }

    function PreprocessBeforeSubmiting(row, validate) {
        $('#contactAddress').val(getGeoLocation('contactAddress_js'));
        $("#emp_leave_details_form").validate();
        preprocessCheckBoxBeforeSubmitting('isApproved', row);

        $('#leave-start-date').val(getDateStringById('leave-start-date-js'));
        $('#leave-end-date').val(getDateStringById('leave-end-date-js'));
        $('#joining-date-after-leave').val(getDateStringById('joining-date-after-leave-js'));
        const jQueryValid = leaveForm.valid();
        const leaveStartValid = dateValidator('leave-start-date-js', true, {
            'errorEn': 'Enter valid leave start date!',
            'errorBn': 'ছুটি শুরুর তারিখ প্রবেশ করান!'
        });
        const leaveEndValid = dateValidator('leave-end-date-js', true, {
            'errorEn': 'Enter valid leave end date!',
            'errorBn': 'ছুটি শেষ হওয়ার তারিখ প্রবেশ করান!'
        });
        const joiningDateValid = dateValidator('joining-date-after-leave-js', true, {
            'errorEn': 'Enter valid joining date after leave!',
            'errorBn': 'ছুটি শেষে যোগদানের তারিখ প্রবেশ করান!'
        });

        if ($('#leaveRelieverId').val() == 0 && processDate($('#leave-start-date').val()) >= new Date().getTime()) {
            /*if (isEnglish) {
                $('#setLeaveReliever').text('Please add leave relieve');
            } else {
                $('#setLeaveReliever').text('অধিপ্রাপ্ত কর্মকর্তা যুক্ত করুন');
            }

            return false;*/
        }
        const contactAddressValid = true;

        return jQueryValid && leaveStartValid &&
            leaveEndValid &&
            joiningDateValid && contactAddressValid;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Employee_leave_detailsServlet");
    }

    function init(row) {
        initGeoLocation('contactAddress_geoSelectField_', row, "Employee_leave_detailsServlet");

    }

    var row = 0;

    var child_table_extra_id = <%=childTableStartingID%>;


    function convertToDate(dateString) {
        const dateParts = dateString.split("/");
        return new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
    }


    function getReliever() {
        if (getDateStringById('leave-start-date-js') === '' || getDateStringById('leave-end-date-js') === '') {
            return;
        }
        let startDate = convertToDate(getDateStringById('leave-start-date-js')).getTime();
        let endDate = convertToDate(getDateStringById('leave-end-date-js')).getTime();


        let iterator = reliever1.values();
        let overlap = false;
        for (const itItem of iterator) {
            let currentStartTime = parseInt(itItem.leaveStartDate)
            let currentEndDate = parseInt(itItem.leaveEndDate)
            console.log(currentStartTime + ' ' + currentStartTime + ' ' + startDate + ' ' + endDate)

            if ((currentEndDate >= startDate) && (currentStartTime <= endDate)) {
                overlap = true;
                break;
            }
        }
        if (overlap === false && startDate >= new Date().getTime()) {
            <%if (leave_reliever_mappingDTOS == null || leave_reliever_mappingDTOS.size() == 0) {%>
            if (isEnglish) {
                $('#setLeaveReliever').text('Please add leave relieve');
            } else {
                $('#setLeaveReliever').text('অধিপ্রাপ্ত কর্মকর্তা যুক্ত করুন');
            }
            return;
            <%}%>

            $('#leaveRelieverId').val(leaveReleiver1Id);
            if (isEnglish) {
                $('#setLeaveReliever').text('Duty on leave reliever 1');
            } else {
                $('#setLeaveReliever').text('প্রথম অধিপ্রাপ্ত কর্মকর্তার উপর দায়িত্ব অর্পিত');
            }

            return;
        }
        iterator = reliever2.values();
        overlap = false;
        for (const itItem of iterator) {
            let currentStartTime = parseInt(itItem.leaveStartDate)
            let currentEndDate = parseInt(itItem.leaveEndDate)

            if ((currentEndDate >= startDate) && (currentStartTime <= endDate)) {
                overlap = true;
                break;
            }
        }
        if (overlap === false && startDate >= new Date().getTime()) {
            <%if (leave_reliever_mappingDTOS == null || leave_reliever_mappingDTOS.size() == 0) {%>
            if (isEnglish) {
                $('#setLeaveReliever').text('Please add leave relieve');
            } else {
                $('#setLeaveReliever').text('অধিপ্রাপ্ত কর্মকর্তা যুক্ত করুন');
            }
            return;
            <%}%>
            $('#leaveRelieverId').val(leaveReleiver2Id);
            if (isEnglish) {
                $('#setLeaveReliever').text('Duty on leave reliever 2');
            } else {
                $('#setLeaveReliever').text('দ্বিতীয় অধিপ্রাপ্ত কর্মকর্তার উপর দায়িত্ব অর্পিত');
            }
            return;
        }
        $('#leaveRelieverId').val(0);
        <%
            if (leave_reliever_mappingDTOS != null && leave_reliever_mappingDTOS.size() > 0) {
        %>
        if (isEnglish) {
            $('#setLeaveReliever').text('No leave reliever is free. select another date.');
        } else {
            $('#setLeaveReliever').text('কোন অধিপ্রাপ্ত কর্মকর্তা ফ্রি নেই। অন্য তারিখ বাছাই করুন।');
        }
        <%
        }else{
        %>
        if (isEnglish) {
            $('#setLeaveReliever').text('Please add leave relieve');
        } else {
            $('#setLeaveReliever').text('অধিপ্রাপ্ত কর্মকর্তা যুক্ত করুন');
        }
        <%}%>
    }

    function submitForm() {
        if (PreprocessBeforeSubmiting(0, '<%=actionName%>')) {
            submitAjaxForm('emp_leave_details_form');
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    $(function () {

        $('#leave-start-date-js').on('datepicker.change', () => {
            setMinDateById('leave-end-date-js', getDateStringById('leave-start-date-js'));
            getReliever();
        });

        $('#leave-end-date-js').on('datepicker.change', () => {
            setMinDateById('joining-date-after-leave-js', getDateStringById('leave-end-date-js'));
            getReliever();
        });

        <%if(actionName.equals("edit")) {%>
        setDateByTimestampAndId('leave-start-date-js', <%=employee_leave_detailsDTO.leaveStartDate%>);
        setDateByTimestampAndId('leave-end-date-js', <%=employee_leave_detailsDTO.leaveEndDate%>);
        setDateByTimestampAndId('joining-date-after-leave-js', <%=employee_leave_detailsDTO.joiningDateAfterLeave%>);
        <%}%>
    });
</script>
<style>
    .leave-button-cross-picker {
        height: 34px;
        width: 41px;
        border: 1px solid #c2cad8;
    }

    .input-width92 {
        width: 92%;
        float: left;
    }
</style>





