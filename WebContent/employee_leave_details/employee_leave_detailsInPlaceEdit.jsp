<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="employee_leave_details.Employee_leave_detailsDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>
<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>

<%
Employee_leave_detailsDTO employee_leave_detailsDTO = (Employee_leave_detailsDTO)request.getAttribute("employee_leave_detailsDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(employee_leave_detailsDTO == null)
{
	employee_leave_detailsDTO = new Employee_leave_detailsDTO();
	
}
System.out.println("employee_leave_detailsDTO = " + employee_leave_detailsDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

long ColumnID;
FilesDAO filesDAO = new FilesDAO();
%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=employee_leave_detailsDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeRecordsId'>")%>
			
	
	<div class="form-inline" id = 'employeeRecordsId_div_<%=i%>'>
		<input type='text' class='form-control'  name='employeeRecordsId' id = 'employeeRecordsId_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + employee_leave_detailsDTO.employeeRecordsId + "'"):("''")%> <% 
	if(!actionName.equals("edit"))
	{
%>
		required="required"
<%
	}
%>
  tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_leaveTypeCat'>")%>
			
	
	<div class="form-inline" id = 'leaveTypeCat_div_<%=i%>'>
		<select class='form-control'  name='leaveTypeCat' id = 'leaveTypeCat_category_<%=i%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "leave_type", employee_leave_detailsDTO.leaveTypeCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "leave_type", -1);			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_leaveStartDate'>")%>
			
	
	<div class="form-inline" id = 'leaveStartDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'leaveStartDate_date_<%=i%>' name='leaveStartDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_leaveStartDate = dateFormat.format(new Date(employee_leave_detailsDTO.leaveStartDate));
	%>
	'<%=formatted_leaveStartDate%>'
	<%
}
else
{
	%>
	''
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_leaveEndDate'>")%>
			
	
	<div class="form-inline" id = 'leaveEndDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'leaveEndDate_date_<%=i%>' name='leaveEndDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_leaveEndDate = dateFormat.format(new Date(employee_leave_detailsDTO.leaveEndDate));
	%>
	'<%=formatted_leaveEndDate%>'
	<%
}
else
{
	%>
	''
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_comments'>")%>
			
	
	<div class="form-inline" id = 'comments_div_<%=i%>'>
		<input type='text' class='form-control'  name='comments' id = 'comments_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + employee_leave_detailsDTO.comments + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_leaveRelieverId'>")%>
			

		<input type='hidden' class='form-control'  name='leaveRelieverId' id = 'leaveRelieverId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + employee_leave_detailsDTO.leaveRelieverId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_contactNumberDuringLeave'>")%>
			
	
	<div class="form-inline" id = 'contactNumberDuringLeave_div_<%=i%>'>
		<input type='text' class='form-control'  name='contactNumberDuringLeave' id = 'contactNumberDuringLeave_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + employee_leave_detailsDTO.contactNumberDuringLeave + "'"):("'" + "" + "'")%> <% 
	if(!actionName.equals("edit"))
	{
%>
		required="required"  pattern="880[0-9]{10}" title="contactNumberDuringLeave must start with 880, then contain 10 digits"
<%
	}
%>
  tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_contactAddress'>")%>
			
	
	<div class="form-inline" id = 'contactAddress_div_<%=i%>'>
		<div id ='contactAddress_geoDIV_<%=i%>' tag='pb_html'>
			<select class='form-control' name='contactAddress_active' id = 'contactAddress_geoSelectField_<%=i%>' onChange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'contactAddress', this.getAttribute('row'))"  tag='pb_html' row = '<%=i%>'></select>
		</div>
		<input type='text' class='form-control' name='contactAddress_text' id = 'contactAddress_geoTextField_<%=i%>' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(employee_leave_detailsDTO.contactAddress)  + "'"):("'" + "" + "'")%> placeholder='Road Number, House Number etc' tag='pb_html'>
		<input type='hidden' class='form-control'  name='contactAddress' id = 'contactAddress_geolocation_<%=i%>' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseID(employee_leave_detailsDTO.contactAddress)  + "'"):("'" + "1" + "'")%> tag='pb_html'>
		<%
		if(actionName.equals("edit"))
		{
		%>
		<label class="control-label"><%=GeoLocationDAO2.parseEnText(employee_leave_detailsDTO.contactAddress) + "," + GeoLocationDAO2.parseDetails(employee_leave_detailsDTO.contactAddress)%></label>
		<%
		}
		%>
			
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_jobCat'>")%>
			
	
	<div class="form-inline" id = 'jobCat_div_<%=i%>'>
		<select class='form-control'  name='jobCat' id = 'jobCat_select_<%=i%>'   tag='pb_html'>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isApproved'>")%>
			
	
	<div class="form-inline" id = 'isApproved_div_<%=i%>'>
		<input type='checkbox' class='form-control'  name='isApproved' id = 'isApproved_checkbox_<%=i%>' value='true' <%=(actionName.equals("edit") && String.valueOf(employee_leave_detailsDTO.isApproved).equals("true"))?("checked"):""%>   tag='pb_html'><br>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_approvalDate'>")%>
			
	
	<div class="form-inline" id = 'approvalDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'approvalDate_date_<%=i%>' name='approvalDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_approvalDate = dateFormat.format(new Date(employee_leave_detailsDTO.approvalDate));
	%>
	'<%=formatted_approvalDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_filesDropzone'>")%>
			
	
	<div class="form-inline" id = 'filesDropzone_div_<%=i%>'>
		<%
		if(actionName.equals("edit"))
		{
			List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(employee_leave_detailsDTO.filesDropzone);
			%>			
			<table>
				<tr>
			<%
			if(filesDropzoneDTOList != null)
			{
				for(int j = 0; j < filesDropzoneDTOList.size(); j ++)
				{
					FilesDTO filesDTO = filesDropzoneDTOList.get(j);
					byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
					%>
					<td id = 'filesDropzone_td_<%=filesDTO.iD%>'>
					<%
					if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)
					{
						%>
						<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64) 
						%>' style='width:100px' />
						<%
					}
					%>
					<a href = 'Employee_leave_detailsServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>
					<a class='btn btn-danger' onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>		
					</td>
					<%
				}
			}
			%>
			</tr>
			</table>
			<%
		}
		%>
		
		<%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>			
		<div class="dropzone" action="Employee_leave_detailsServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?employee_leave_detailsDTO.filesDropzone:ColumnID%>">
			<input type='file' style="display:none"  name='filesDropzoneFile' id = 'filesDropzone_dropzone_File_<%=i%>'  tag='pb_html'/>			
		</div>								
		<input type='hidden'  name='filesDropzoneFilesToDelete' id = 'filesDropzoneFilesToDelete_<%=i%>' value=''  tag='pb_html'/>
		<input type='hidden' name='filesDropzone' id = 'filesDropzone_dropzone_<%=i%>'  tag='pb_html' value='<%=actionName.equals("edit")?employee_leave_detailsDTO.filesDropzone:ColumnID%>'/>		


	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=employee_leave_detailsDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value='<%=employee_leave_detailsDTO.insertedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=employee_leave_detailsDTO.modifiedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + employee_leave_detailsDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=employee_leave_detailsDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Employee_leave_detailsServlet?actionType=view&ID=<%=employee_leave_detailsDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Employee_leave_detailsServlet?actionType=view&modal=1&ID=<%=employee_leave_detailsDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	