<%@page pageEncoding="UTF-8" %>

<%@page import="employee_leave_details.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="files.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@ page import="approval_execution_table.*"%>
<%@ page import="approval_path.*"%>
<%@ page import="workflow.*"%>
<%@page import="dbm.*" %>
<%@page import="holidays.*" %>
<%@page import="approval_summary.*" %>

<%@page import="geolocation.GeoLocationDAO2"%>

<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_EDIT_LANGUAGE, loginDTO);

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_EMPLOYEE_LEAVE_DETAILS;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Employee_leave_detailsDTO employee_leave_detailsDTO = (Employee_leave_detailsDTO)request.getAttribute("employee_leave_detailsDTO");

Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
Approval_summaryDAO approval_summaryDAO = new Approval_summaryDAO();
ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
Approval_execution_tableDTO approval_execution_tableDTO = null;
ApprovalPathDetailsDTO approvalPathDetailsDTO =  null;
boolean canApprove = false, canValidate = false, isInitiator = false, canTerminate = false;
boolean isInPreviousOffice = false;
String Message = "Done";

approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("employee_leave_details", employee_leave_detailsDTO.iD);
Approval_summaryDTO approval_summaryDTO = approval_summaryDAO.getDTOByTableNameAndTableID("employee_leave_details", approval_execution_tableDTO.previousRowId);
System.out.println("approval_execution_tableDTO = " + approval_execution_tableDTO);
approvalPathDetailsDTO = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
if(approvalPathDetailsDTO!= null && approvalPathDetailsDTO.organogramId == userDTO.organogramID)
{
	canApprove = true;
	if(approvalPathDetailsDTO.approvalRoleCat == SessionConstants.VALIDATOR)
	{
		canValidate = true;
	}
}

isInitiator = WorkflowController.isInitiator(tableName, approval_execution_tableDTO.previousRowId, userDTO.organogramID);

canTerminate = isInitiator && employee_leave_detailsDTO.isDeleted == 2;

Approval_pathDAO approval_pathDAO = new Approval_pathDAO();
Approval_pathDTO approval_pathDTO = approval_pathDAO.getDTOByID(approval_execution_tableDTO.approvalPathId);
	

System.out.println("employee_leave_detailsDTO = " + employee_leave_detailsDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Employee_leave_detailsDAO employee_leave_detailsDAO = (Employee_leave_detailsDAO)request.getAttribute("employee_leave_detailsDAO");

FilesDAO filesDAO = new FilesDAO();

SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

boolean isOverDue = false;

Date fromDate = new Date(approval_execution_tableDTO.lastModificationTime);
Date dueDate = null;
if(fromDate != null && approvalPathDetailsDTO!= null)
{
	dueDate = CalenderUtil.getDateAfter(fromDate, approvalPathDetailsDTO.daysRequired);
	long today = System.currentTimeMillis();
	boolean timeOver = today > dueDate.getTime();
	isOverDue  =  (approval_execution_tableDTO.approvalStatusCat == Approval_execution_tableDTO.PENDING) && timeOver;
	
	System.out.println("time dif = " + (today - dueDate.getTime()));
}
String formatted_dueDate;

System.out.println("i = " + i  + " OVERDUE = " + isOverDue);

if(isOverDue)
{
%>
<style>
#tr_<%=i%> {
  color: red;
}
</style>
<%
}
%>

											
		
											
											<td id = '<%=i%>_employeeRecordsId'>
											<%
											value = employee_leave_detailsDTO.employeeRecordsId + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_leaveTypeCat'>
											<%
											value = employee_leave_detailsDTO.leaveTypeCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "leave_type", employee_leave_detailsDTO.leaveTypeCat);
											%>																						
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_leaveStartDate'>
											<%
											value = employee_leave_detailsDTO.leaveStartDate + "";
											%>
											<%
											String formatted_leaveStartDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_leaveStartDate%>
				
			
											</td>
		
											
											<td id = '<%=i%>_leaveEndDate'>
											<%
											value = employee_leave_detailsDTO.leaveEndDate + "";
											%>
											<%
											String formatted_leaveEndDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_leaveEndDate%>
				
			
											</td>
		
											
											<td id = '<%=i%>_comments'>
											<%
											value = employee_leave_detailsDTO.comments + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_leaveRelieverId'>
											<%
											value = employee_leave_detailsDTO.leaveRelieverId + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_contactNumberDuringLeave'>
											<%
											value = employee_leave_detailsDTO.contactNumberDuringLeave + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_contactAddress'>
											<%
											value = employee_leave_detailsDTO.contactAddress + "";
											%>
											<%=GeoLocationDAO2.parseEnText(value)%>
											<%
											{
												String addressdetails = GeoLocationDAO2.parseDetails(value);
												if(!addressdetails.equals(""))
												{
												%>
													, <%=addressdetails%>
												<%
												}
											}
											%>

				
			
											</td>
		
											
											<td id = '<%=i%>_jobCat'>
											<%
											value = employee_leave_detailsDTO.jobCat + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_isApproved'>
											<%
											value = employee_leave_detailsDTO.isApproved + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_approvalDate'>
											<%
											value = employee_leave_detailsDTO.approvalDate + "";
											%>
											<%
											String formatted_approvalDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_approvalDate%>
				
			
											</td>
		
											
											<td id = '<%=i%>_filesDropzone'>
											<%
											value = employee_leave_detailsDTO.filesDropzone + "";
											%>
											<%
											{
												List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(employee_leave_detailsDTO.filesDropzone);
												%>
												<table>
												<tr>
												<%
												if(FilesDTOList != null)
												{
													for(int j = 0; j < FilesDTOList.size(); j ++)
													{
														FilesDTO filesDTO = FilesDTOList.get(j);
														byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
														%>
														<td>
														<%
														if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)
														{
															%>
															<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px' />
															<%
														}
														%>
														<a href = 'Employee_leave_detailsServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>
														</td>
													<%
													}
												}
												%>
												</tr>
												</table>
											<%												
											}
											%>
				
			
											</td>
		
											
		
											
		
											
		
											
		
											
		
	

<td>
											<a href='Employee_leave_detailsServlet?actionType=view&ID=<%=employee_leave_detailsDTO.iD%>'&isPermanentTable=false>View</a>
											
											<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
											
											<div class='modal fade' id='viedFileModal_<%=i%>'>
											  <div class='modal-dialog modal-lg' role='document'>
											    <div class='modal-content'>
											      <div class='modal-body'>
											        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
											          <span aria-hidden='true'>&times;</span>
											        </button>											        
											        
											        <object type='text/html' data='Employee_leave_detailsServlet?actionType=view&modal=1&ID=<%=employee_leave_detailsDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
											        
											      </div>
											    </div>
											  </div>
											</div>
											</td>
											
											<td>
												<%
												value = approval_pathDTO.nameEn;
												%>											
															
												<%=value%>
											</td>
											
											<td>
												<%
												value = WorkflowController.getNameFromUserId(approval_summaryDTO.initiator, Language);
												%>											
															
												<%=value%>
											</td>
											
											<td id = '<%=i%>_dateOfInitiation'>
												<%
												value = approval_summaryDTO.dateOfInitiation + "";
												%>
												<%

												String formatted_dateOfInitiation = simpleDateFormat.format(new Date(Long.parseLong(value)));
												%>
												<%=formatted_dateOfInitiation%>
				
			
											</td>
											
											
											<td>
												<%
												value = WorkflowController.getNameFromOrganogramId(approval_summaryDTO.assignedTo, Language);
												%>											
															
												<%=value%>
											</td>
											
											<td >
												<%
												String formatted_dateOfAssignment = simpleDateFormat.format(new Date(approval_execution_tableDTO.lastModificationTime));
												%>
												<%=formatted_dateOfAssignment%>
				
			
											</td>
											
											<td>
											
												<%
												
												
												if(dueDate!= null)
												{
													formatted_dueDate = simpleDateFormat.format(dueDate);
												}
												else
												{
													formatted_dueDate = "";
												}
												%>
												<%=formatted_dueDate%>
				
			
											</td>
		
											
											
											<td>
												<%
												if(isOverDue)
												{
													value = "OVERDUE";
												}
												else
												{
													value = CatDAO.getName(Language, "approval_status", approval_summaryDTO.approvalStatusCat);
												}
												
												%>											
															
												<%=value%>
											</td>
	
											<td>
											<%
											if(canApprove || canTerminate)
											{
												%>
												<a href='Employee_leave_detailsServlet?actionType=view&ID=<%=employee_leave_detailsDTO.iD%>&isPermanentTable=<%=isPermanentTable%>'>View</a>
												
												<%
											}
											else
											{
											
											 	%>
											 	<%=LM.getText(LC.HM_NO_ACTION_IS_REQUIRED, loginDTO)%>.
											 	<%
											}
											 %>																						
											</td>
											
											<td>
											<%
											if(canValidate)
											{
												%>
												<a href='Employee_leave_detailsServlet?actionType=getEditPage&ID=<%=employee_leave_detailsDTO.iD%>&isPermanentTable=<%=isPermanentTable%>'>View</a>
												
												<%
											}
											else
											{
											
											 	%>
											 	<%=LM.getText(LC.HM_NO_ACTION_IS_REQUIRED, loginDTO)%>.
											 	<%
											}
											 %>																						
											</td>
											
											
											<td>
												<a href="Approval_execution_tableServlet?actionType=search&tableName=employee_leave_details&previousRowId=<%=approval_execution_tableDTO.previousRowId%>"  ><%=LM.getText(LC.HM_HISTORY, loginDTO)%></a>
											</td>
											

