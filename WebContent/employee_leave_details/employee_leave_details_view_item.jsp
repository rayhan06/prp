<%@ page import="util.StringUtils" %>
<%@ page import="geolocation.GeoLocationUtils" %>
<td style="vertical-align: middle;"><%=isLanguageEnglish ? leaveDetailsItem.leaveTypeEng : leaveDetailsItem.leaveTypeBan%>
</td>
<td style="vertical-align: middle;"><%=isLanguageEnglish ? leaveDetailsItem.leaveStartDateEng : leaveDetailsItem.leaveStartDateBan%>
</td>
<td style="vertical-align: middle;"><%=isLanguageEnglish ? leaveDetailsItem.leaveEndDateEng : leaveDetailsItem.leaveEndDateBan%>
</td>
<td style="vertical-align: middle;"><%=isLanguageEnglish ? leaveDetailsItem.joiningDateAfterLeaveEng : leaveDetailsItem.joiningDateAfterLeaveBan%>
</td>
<td style="vertical-align: middle;"><%=isLanguageEnglish ? leaveDetailsItem.leaveRelieverName : leaveDetailsItem.leaveRelieverNameBn%>
</td>
<td style="vertical-align: middle;"><%=GeoLocationUtils.getGeoLocationString(leaveDetailsItem.contactAddress, Language)%>
</td>

<%--<td style="vertical-align: middle;"><%=StringUtils.getYesNo(Language,leaveDetailsItem.isApproved)%>--%>
<%--</td>--%>
<%--<td style="vertical-align: middle;"><%=isLanguageEnglish ? leaveDetailsItem.approvalDateEng : leaveDetailsItem.approvalDateBan%>--%>
<%--</td>--%>


<td style="text-align: center; vertical-align: middle;">
    <form action="Employee_leave_detailsServlet?isPermanentTable=true&actionType=delete&tab=6&ID=<%=leaveDetailsItem.dto.iD%>"
          method="POST" id="tableForm_leave_details<%=leaveDetailsIndex%>" enctype="multipart/form-data">

        <div class="btn-group" role="group" aria-label="Basic example">
            <button class="btn-primary" title="Edit" type="button"
                    onclick="location.href='Employee_leave_detailsServlet?actionType=view&ID=<%=leaveDetailsItem.dto.iD%>&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
                <i
                        class="fa fa-eye"></i></button>&nbsp;
            <button class="btn-success" title="Edit" type="button"
                    onclick="location.href='<%=request.getContextPath()%>/Employee_leave_detailsServlet?actionType=getEditPage&tab=6&ID=<%=leaveDetailsItem.dto.iD%>&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
                <i
                        class="fa fa-edit"></i></button>&nbsp;
            <button class="btn-danger" title="Delete" type="button"
                    onclick="deleteItem('tableForm_leave_details',<%=leaveDetailsIndex%>)"><i
                    class="fa fa-trash"></i></button>
        </div>
    </form>
</td>


