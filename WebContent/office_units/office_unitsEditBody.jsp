<%@page import="login.LoginDTO" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="office_building.*" %>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    Office_unitsDTO officeUnitsDTO;
    String formTitle;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        officeUnitsDTO = (Office_unitsDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
        officeUnitsDTO.office_buildings = Office_buildingDAO.getInstance().getByOfficeId(officeUnitsDTO.iD);
        System.out.println("officeUnitsDTO.office_buildings = " + officeUnitsDTO.office_buildings.size());
        formTitle = LM.getText(LC.ADD_OFFICE_UNIT_EDIT_OFFICE_UNIT, loginDTO);
    } else {
        actionName = "add";
        officeUnitsDTO = new Office_unitsDTO();
        formTitle = LM.getText(LC.ADD_OFFICE_UNIT_ADD_OFFICE_UNIT, loginDTO);
    }

%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="add-office-unit-form">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.ADD_OFFICE_UNIT_SUPERIOR_OFFICE, loginDTO)%>
                                            <span>*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <%--Office Unit Modal Trigger Button--%>
                                            <button type="button"
                                                    class="btn btn-primary btn-block shadow btn-border-radius"
                                                    id="office_units_id_modal_button"
                                                    onclick="officeModalButtonClicked();">
                                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                                            </button>
                                            <span id="parent-office-error"
                                                  style="display: none;color: #fd397a;text-align: center;margin-top: 1%;font-size: smaller;font-weight: 400">
                                                <%=isLanguageEnglish ? "Please select superior office" : "উর্ধতন দপ্তর বাছাই করুন"%>
                                            </span>
                                            <%--Modal Included in employee_office_report_Body.jsp--%>
                                            <div class="input-group" id="office_units_id_div" style="display: none">
                                                <input type="hidden" name='parentUnitId' id='office_units_id_input'
                                                       value="">
                                                <button type="button"
                                                        class="btn btn-secondary btn-block shadow-sm btn-border-radius form-control"
                                                        id="office_units_id_text"
                                                        onclick="officeModalEditButtonClicked()">
                                                </button>
                                                <span class="input-group-btn"
                                                      style="width: 5%;padding-right: 5px;height: 40px" tag='pb_html'>
                                                    <button type="button" class="btn btn-outline-danger"
                                                            style="height: 40px;border-radius: 0"
                                                            onclick="crsBtnClicked('office_units_id');"
                                                            id='office_units_id_crs_btn' tag='pb_html'>X
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="unitNameEng">
                                            <%=LM.getText(LC.GLOBAL_NAME_ENGLISH, loginDTO)%>
                                            <span>*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' required class='form-control' name='unitNameEng'
                                                   id='unitNameEng'
                                                   value="<%=officeUnitsDTO.unitNameEng%>"/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="unitNameBng">
                                            <%=LM.getText(LC.GLOBAL_NAME_BANGLA, loginDTO)%>
                                            <span>*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' required class='form-control' name='unitNameBng'
                                                   id='unitNameBng'
                                                   value="<%=officeUnitsDTO.unitNameBng%>"/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="isAbstractOffice_checkbox">
                                            <%=LM.getText(LC.OFFICE_UNITS_ADD_ABSTRACT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='checkbox' class='form-control-sm'
                                                   name='isAbstractOffice'
                                                   id='isAbstractOffice_checkbox'
                                                   value='true' <%=(actionName.equals("edit") && String.valueOf(officeUnitsDTO.isAbstractOffice).equals("true"))?("checked"):""%>
                                                   tag='pb_html'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="budgetOfficeId">
                                            <%=isLanguageEnglish ? "Budget Office Code" : "বাজেট অফিস কোড"%>
                                        </label>
                                        <div class="col-md-9">
                                            <select id="budgetOfficeId" name='budgetOfficeId'
                                                    class='form-control rounded shadow-sm'>
                                                <%=Budget_officeRepository.getInstance().buildOptions(
                                                        Language,
                                                        officeUnitsDTO.budgetOfficeId,
                                                        true
                                                )%>
                                            </select>
                                        </div>
                                    </div>

                                  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.BUILDING_ADD_BUILDING_BLOCK, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
									<tr>
										<th><%=isLanguageEnglish?"Building":"ভবন"%></th>
										<th><%=isLanguageEnglish?"Block":"ব্লক"%></th>
										<th><%=isLanguageEnglish?"Level":"লেভেল"%></th>
										<th><%=isLanguageEnglish?"Room No":"রুম নম্বর"%></th>
										<th><%=LM.getText(LC.BUILDING_ADD_BUILDING_BLOCK_REMOVE, loginDTO)%></th>
									</tr>
								</thead>
								<tbody id="field-OfficeBuilding">
									<%
									if(actionName.equals("edit")){
										int index = -1;
										
										
										for(Office_buildingDTO officeBuildingDTO: officeUnitsDTO.office_buildings)
										{
											index++;
											
											System.out.println("index index = "+index);

								%>	
							
								<tr id = "OfficeBuilding_<%=index + 1%>">
														<td style="display: none;">

															<input type='hidden' class='form-control'  name='officeBuilding.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=officeBuildingDTO.iD%>' tag='pb_html'/>
															<input type='hidden' class='form-control'  name='officeBuilding.officeUnitsId' id = 'officeUnitsId_hidden_<%=childTableStartingID%>' value='<%=officeBuildingDTO.officeUnitsId%>' tag='pb_html'/>
	
														</td>
								
														<td>										
															<select class='form-control' 
				                                                    name='officeBuilding.buildingType'
				                                                    id='buildingType_type_<%=childTableStartingID%>'
				                                                    onchange="setBlockLevel(this.id)" tag='pb_html'>
				                                                <%
				                                                Options = CommonDAO.getOptions(Language, "building", officeBuildingDTO.buildingType);
				                                                %>
				                                                <%=Options%>
				                                            </select>
					
														</td>
														<td>										
					
															<select class='form-control' 
				                                                    name='officeBuilding.buildingBlockType'
				                                                    id='buildingBlockType_type_<%=childTableStartingID%>'
																	tag='pb_html'
				                                                >
				                                                <option value = '<%=officeBuildingDTO.buildingBlockType%>'></option>
				                                            </select>					
														</td>
														<td>										
					
															<select class='form-control' 
				                                                    name='officeBuilding.buildingLevel'
				                                                    id='buildingLevel_type_<%=childTableStartingID%>'
																	tag='pb_html'
				                                                >
				                                                <option value = '<%=officeBuildingDTO.buildingLevel%>'></option>
				                                            </select>					
														</td>
														
														<td>										
					
															<input type='text' class='form-control' 
															 name='officeBuilding.roomNo' 
															 id = 'roomNo_text_<%=childTableStartingID%>' 
															 value='<%=officeBuildingDTO.roomNo%>' tag='pb_html'/>
					
														</td>
														<td>
															<span id='chkEdit'>
																<input type='checkbox' name='checkbox' value='' deletecb='true'
																	   class="form-control-sm"/>
															</span>
														</td>
													</tr>								
													<%	
																childTableStartingID ++;
															}
														}
													%>						
						
								</tbody>
							</table>
						</div>
						
						<div class="form-group">
								<div class="col-xs-9 text-right">
									<button
											id="add-more-OfficeBuilding"
											name="add-moreOfficeBuilding"
											type="button"
											onclick="childAdded(event, 'OfficeBuilding')"
											class="btn btn-sm text-white add-btn shadow">
										<i class="fa fa-plus"></i>
										<%=LM.getText(LC.HM_ADD, loginDTO)%>
									</button>
									<button
											id="remove-OfficeBuilding"
											name="removeOfficeBuilding"
											type="button"
											onclick="childRemoved(event, 'OfficeBuilding')"
											class="btn btn-sm remove-btn shadow ml-2 pl-4">
										<i class="fa fa-trash"></i>
									</button>
								</div>
						</div>
						
						<%Office_buildingDTO officeBuildingDTO = new Office_buildingDTO();%>
					
							<template id="template-OfficeBuilding" >						
								<tr>
									<td style="display: none;">

															<input type='hidden' class='form-control'  name='officeBuilding.iD' id = 'iD_hidden_' value='<%=officeBuildingDTO.iD%>' tag='pb_html'/>
															<input type='hidden' class='form-control'  name='officeBuilding.officeUnitsId' id = 'officeUnitsId_hidden_' value='<%=officeBuildingDTO.officeUnitsId%>' tag='pb_html'/>
	
														</td>
								
														<td>										
															<select class='form-control' 
				                                                    name='officeBuilding.buildingType'
				                                                    id='buildingType_type_'
				                                                    onchange="setBlockLevel(this.id)" tag='pb_html'>
				                                                <%
				                                                Options = CommonDAO.getOptions(Language, "building", officeBuildingDTO.buildingType);
				                                                %>
				                                                <%=Options%>
				                                            </select>
					
														</td>
														<td>										
					
															<select class='form-control' 
				                                                    name='officeBuilding.buildingBlockType'
				                                                    id='buildingBlockType_type_'
																	tag='pb_html'
				                                                >
				                                                <option value = '<%=officeBuildingDTO.buildingBlockType%>'></option>
				                                            </select>					
														</td>
														<td>										
					
															<select class='form-control' 
				                                                    name='officeBuilding.buildingLevel'
				                                                    id='buildingLevel_type_'
																	tag='pb_html'
				                                                >
				                                                <option value = '<%=officeBuildingDTO.buildingLevel%>'></option>
				                                            </select>					
														</td>
														
														<td>										
					
															<input type='text' class='form-control' 
															 name='officeBuilding.roomNo' 
															 id = 'roomNo_text_' 
															 value='<%=officeBuildingDTO.roomNo%>' tag='pb_html'/>
					
														</td>
														<td>
																<span id='chkEdit'>
																	<input type='checkbox' name='checkbox' value='' deletecb='true'
																		   class="form-control-sm"/>
																</span>
														</td>
								</tr>								
						
							</template>
					</div>
				</div>
					
                <div class="row mt-4">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button type="button" id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                    onclick="location.href = '<%=request.getHeader("referer")%>'">
                                <%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_ECONOMIC_SUB_CODE_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button type="button" id="submit-btn"
                                    class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    onclick="submitOfficeForm()">
                                <%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_ECONOMIC_SUB_CODE_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">
    const form = $('#add-office-unit-form');
    const officeUnitSelector = $('#office_units_id_input');
    var child_table_extra_id = <%=childTableStartingID%>;
    const isLangEng = <%=isLanguageEnglish%>

        $(document).ready(() => {
            form.validate({
                errorClass: 'error is-invalid',
                validClass: 'is-valid',
                rules: {
                    unitNameEng: {
                        required: true
                    },
                    unitNameBng: {
                        required: true
                    }
                },
                messages: {
                    unitNameEng: isLangEng ? "Please write office unit name in english" : "অনুগ্রহ করে ইংরেজীতে দপ্তরের নাম লিখুন",
                    unitNameBng: isLangEng ? "Please write office unit name in bangla" : "অনুগ্রহ করে বাংলায় দপ্তরের নাম লিখুন"
                }
            });
            <%
            if(actionName.equals("edit")){
                Office_unitsDTO parentUnitDTO = Office_unitsRepository.getInstance()
                                                                      .getOffice_unitsDTOByID(officeUnitsDTO.parentUnitId);
                if(parentUnitDTO != null) {
            %>
            const officeUnitModel = {
                name: '<%=UtilCharacter.getDataByLanguage(Language, parentUnitDTO.unitNameBng, parentUnitDTO.unitNameEng)%>',
                id: <%=parentUnitDTO.iD%>
            };
            viewOfficeIdInInput(officeUnitModel);
            <%
                }
            }
            %>
            
            $("[name = 'officeBuilding.buildingType']").each(function() {
            	setBlockLevel($(this).attr('id'));
            });
        });

    function submitOfficeForm() {
        let validation = form.valid();
        const parentUnitId = officeUnitSelector.val();
        if (parentUnitId === '' || parentUnitId < 0) {
            $('#parent-office-error').show();
            validation = false;
        } else {
            $('#parent-office-error').hide();
        }
        if (validation) {
            buttonStateChange(true);
            let data = form.serialize();
            $.ajax({
                type: "POST",
                url: "Office_unitsServlet?actionType=ajax_<%=actionName%>&iD=<%=officeUnitsDTO.iD%>",
                data: data,
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        //window.location.replace(getContextPath() + response.msg);
						history.back();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                                 + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        }
    }

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
    }

    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }

        $('#parent-office-error').hide();
        $('#office_units_id_modal_button').hide();
        $('#office_units_id_div').show();

        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        officeUnitSelector.val(selectedOffice.id);
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function officeModalEditButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        officeSearchSetSelectedOfficeLayers($('#office_units_id_input').val());
        $('#search_office_modal').modal();
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }
    
    function setBlockLevel(buildingElemId)
    {
    	var rowId = buildingElemId.split("_")[2];
    	console.log("rowId = " + rowId);
    	var levelElemId = "buildingLevel_type_" + rowId;
    	var blockElemId = "buildingBlockType_type_" + rowId;
    	
    	var buildingId = $("#" + buildingElemId).val();
    	var levelId = $("#" + levelElemId).val();
    	var blockId = $("#" + blockElemId).val();
    	
    	console.log("blockId = " + blockId);
    	
    	var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
            	var buildingOptions = JSON.parse(this.responseText);
            	$("#" + levelElemId).html(buildingOptions.levels);
            	$("#" + blockElemId).html(buildingOptions.blocks);
            } else {
                //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
            }
        };

        xhttp.open("GET", "Office_unitsServlet?actionType=ajax_setBlockLevel&buildingId=" + buildingId 
        		+ "&blockId=" + blockId
        		+ "&levelId=" + levelId
        		, true);
        xhttp.send();
    }
    
    function processRowsWhileAdding(childName)
    {
    	
    }
</script>