<%@page import="office_building.Office_buildingRepository"%>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="office_units.*" %>
<%@ page import="java.util.List" %>
<%@page pageEncoding="UTF-8" %>
<%
    String navigator2 = "";
%>
<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><%=LM.getText(LC.ADD_OFFICE_UNIT_SUPERIOR_OFFICE, loginDTO)%></th>
            <th><%=LM.getText(LC.GLOBAL_OFFICE_NAME_ENGLISH, loginDTO)%></th>
            <th><%=LM.getText(LC.GLOBAL_OFFICE_NAME_BANGLA, loginDTO)%></th>
            <th><%=isLanguageEnglish?"Address":"ঠিকানা"%></th>
            <th><%=LM.getText(LC.OFFICE_UNIT_ORGANOGRAM_SEARCH_OFFICE_UNIT_ORGANOGRAM_EDIT_BUTTON, loginDTO)%></th>
            <th class="" style="width: 70px">
                <div class="text-center">
                    <span><%=isLanguageEnglish?"All":"সকল"%></span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Office_unitsDTO> data = (List<Office_unitsDTO>) rn2.list;

                if (data != null && data.size()>0) {
                    for (Office_unitsDTO officeUnitsDTO : data) {
        %>
        <tr>
            <td><%=Office_unitsRepository.getInstance().geText(Language,officeUnitsDTO.parentUnitId)%></td>
            <td><%=officeUnitsDTO.unitNameEng%></td>
            <td><%=officeUnitsDTO.unitNameBng%></td>
            <td><%=Office_buildingRepository.getInstance().getNames(officeUnitsDTO.iD, isLanguageEnglish)%></td>
            <td>
                <button type="button" class="btn-sm border-0 shadow btn-border-radius text-white" style="background-color: #ff6b6b;"
                        onclick="location.href='Office_unitsServlet?actionType=getEditPage&ID=<%=officeUnitsDTO.iD%>'">
                    <i class="fa fa-edit"></i>
                </button>
            </td>

            <td class="text-right">
                <div class='checker'>
                     <span class='chkEdit'><input type='checkbox' name='ID' value='<%=officeUnitsDTO.iD%>'/></span>
                </div>
            </td>
        </tr>
        <% } } %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>