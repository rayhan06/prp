<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%
    String url = "Office_unitsServlet?actionType=search";
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0,false)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>

    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 control-label text-md-right">
                            <%=LM.getText(LC.ADD_OFFICE_UNIT_SUPERIOR_OFFICE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                    id="office_units_id_modal_button"
                                    onclick="officeModalButtonClicked();">
                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                            </button>

                            <div class="input-group" id="office_units_id_div" style="display: none">
                                <input type="hidden" name='officeUnitId' id='office_units_id_input' value="">
                                <button type="button"
                                        class="btn btn-secondary btn-block shadow-sm btn-border-radius form-control"
                                        id="office_units_id_text" onclick="officeModalEditButtonClicked()">
                                </button>
                                <span class="input-group-btn" style="height: 40px" tag='pb_html'>
							<button type="button" class="btn btn-outline-danger" style="border-radius: 0;height: 40px"
                                    onclick="crsBtnClicked('office_units_id');"
                                    id='office_units_id_crs_btn' tag='pb_html'>
								x
							</button>
						</span>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="officeNameEng" class="col-md-3 col-form-label text-md-right">
                            <%=LM.getText(LC.GLOBAL_NAME_ENGLISH, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="officeNameEng" placeholder=""
                                   name="officeNameEng" onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="officeNameBng" class="col-md-3 col-form-label text-md-right">
                            <%=LM.getText(LC.GLOBAL_NAME_BANGLA, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="officeNameBng" placeholder=""
                                   name="officeNameBng" onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->
<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<%@include file="../common/pagination_with_go2.jsp" %>

<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>

<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>

<script>

    const anyFieldSelector = $('#anyfield');
    const officeNameEngSelector = $('#officeNameEng');
    const officeNameBngSelector = $('#officeNameBng');
    const officeUnitIdInputSelector = $('#office_units_id_input');
    const officeUnitIdTextSelector = $('#office_units_id_text');

    function resetInputs() {
        anyFieldSelector.val('');
        officeNameEngSelector.val('');
        officeNameBngSelector.val('');
        officeUnitIdInputSelector.val('');
        officeUnitIdTextSelector.html('');
    }

    $(document).ready(() => {
        console.log('document ready...');
        readyInit('Office_unitsServlet');
        let locationSearch = location.search;
        if(locationSearch.startsWith('?')){
            setUI(locationSearch.substring(1));
        }
    });

    function dosubmit(params, pushState = true) {
        console.log('dosubmit is called');
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (pushState) {
                    history.replaceState(params, '', 'Office_unitsServlet?actionType=search&' + params);
                }
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        let url = "Office_unitsServlet?actionType=search&isPermanentTable=true&ajax=true"
        if (params) {
            url += "&" + params;
        }
        xhttp.open("GET", url, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number, pushState = true) {
        let pageNo = document.getElementsByName('pageno')[0].value;
        let rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;
        let totalRecords = 0;
        let lastSearchTime = 0;
        let params = 'search=true';

        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }

        params += '&pageno=' + pageNo + '&RECORDS_PER_PAGE=' + rpp;

        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
            params += '&TotalRecords=' + totalRecords + '&lastSearchTime=' + lastSearchTime;
        }

        if(anyFieldSelector.val()){
            params += '&AnyField='+anyFieldSelector.val();
        }
        if(officeNameEngSelector.val()){
            params += '&officeNameEng='+officeNameEngSelector.val();
        }
        if(officeNameBngSelector.val()){
            params += '&officeNameBng='+officeNameBngSelector.val();
        }
        if(officeUnitIdInputSelector.val()){
            params += '&officeUnitId='+officeUnitIdInputSelector.val();
            params += '&officeUnitsIdText='+officeUnitIdTextSelector.html();
        }

        dosubmit(params, pushState);
    }

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
    }

    function viewOfficeIdInInput(selectedOffice) {
        console.log(selectedOffice);
        if (selectedOffice.id === '') return;

        $('#office_units_id_modal_button').hide();
        $('#office_units_id_div').show();

        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id_input').val(selectedOffice.id);
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function officeModalEditButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        officeSearchSetSelectedOfficeLayers($('#office_units_id_input').val());
        $('#search_office_modal').modal();
    }

    window.addEventListener('popstate', e => {
        console.log('popstate');
        if (e.state) {
            let params = setUI(e.state);
            dosubmit(params, false);
        } else {
            dosubmit(null, false);
            resetInputs();
            resetPaginationFields();
        }
    });

    function setUI(queryParams){
        let paramMap = actualBuildParamMap(queryParams);
        let params = 'actionType=search';
        paramMap.forEach((value, key) => {
            if (key !== 'actionType') {
                params += "&" + key + "=" + value;
            }
        });
        resetInputs();
        let selectedOffice = {};
        paramMap.forEach((value, key) => {
            console.log('key = '+key+" value = "+value)
            switch (key) {
                case "AnyField":
                    anyFieldSelector.val(value);
                    break;
                case "officeNameEng":
                    officeNameEngSelector.val(value);
                    break;
                case "officeNameBng":
                    officeNameBngSelector.val(value);
                    break;
                case 'officeUnitId':
                    officeUnitIdInputSelector.val(value);
                    selectedOffice['id'] = value;
                    break;
                case 'officeUnitsIdText':
                    value = decodeURI(value);
                    officeUnitIdTextSelector.val(value);
                    selectedOffice['name'] = value;
                    break;
                default:
                    setPaginationFields([key, value]);
            }
        });
        if(selectedOffice.id && selectedOffice.name){
            viewOfficeIdInInput(selectedOffice);
        }
        return params;
    }
</script>