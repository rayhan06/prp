

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="am_minister_hostel_unit.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>
<%@ page import="am_minister_hostel_block.AmMinisterHostelSideDTO" %>
<%@ page import="am_minister_hostel_block.AmMinisterHostelSideDAO" %>
<%@ page import="am_minister_hostel_block.AmMinisterHostelSideRepository" %>


<%
    String servletName = "Am_minister_hostel_unitServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Am_minister_hostel_unitDTO am_minister_hostel_unitDTO = Am_minister_hostel_unitRepository.getInstance().getAm_minister_hostel_unitDTOByID(id);
    CommonDTO commonDTO = am_minister_hostel_unitDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.AM_MINISTER_HOSTEL_UNIT_ADD_AM_MINISTER_HOSTEL_UNIT_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-md-8 offset-md-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.AM_MINISTER_HOSTEL_UNIT_ADD_AM_MINISTER_HOSTEL_UNIT_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_MINISTER_HOSTEL_UNIT_ADD_AMMINISTERHOSTELBLOCKTYPE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = am_minister_hostel_unitDTO.amMinisterHostelBlockId + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "am_minister_hostel_block", "block_no", "id");
											%>
				
											<%=value%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_MINISTER_HOSTEL_UNIT_ADD_AMMINISTERHOSTELSIDEID, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = am_minister_hostel_unitDTO.amMinisterHostelSideId + "";
											%>
                                        <%
                                            AmMinisterHostelSideDTO amMinisterHostelSideDTO = AmMinisterHostelSideRepository.getInstance().getAmMinisterHostelSideDTOByID(am_minister_hostel_unitDTO.amMinisterHostelSideId);
                                            value = CatRepository.getInstance().getText(Language, "am_minister_hostel_side", amMinisterHostelSideDTO.ministerHostelSideCat);
                                        %>
				
											<%=value%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_MINISTER_HOSTEL_UNIT_ADD_UNITNUMBER, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = am_minister_hostel_unitDTO.unitNumber + "";
											%>
				
											<%=value%>
				
			
                                    </div>
                                </div>
			
			
			
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>