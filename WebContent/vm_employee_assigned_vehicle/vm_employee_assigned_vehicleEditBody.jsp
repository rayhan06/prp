<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_employee_assigned_vehicle.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>

<%
    Vm_employee_assigned_vehicleDTO vm_employee_assigned_vehicleDTO = new Vm_employee_assigned_vehicleDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        vm_employee_assigned_vehicleDTO = Vm_employee_assigned_vehicleDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = vm_employee_assigned_vehicleDTO;
    String tableName = "vm_employee_assigned_vehicle";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%@ include file="../recruitment_seat_plan/manualLoader.jsp" %>
<%
    String formTitle = LM.getText(LC.VM_EMPLOYEE_ASSIGNED_VEHICLE_ADD_VM_EMPLOYEE_ASSIGNED_VEHICLE_ADD_FORMNAME, loginDTO);
    String servletName = "Vm_employee_assigned_vehicleServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-10 offset-1">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=vm_employee_assigned_vehicleDTO.iD%>' tag='pb_html'/>


                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-right">
                                            <%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_EMPLOYEERECORDSID, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <!-- Button trigger modal -->
                                            <button type="button"
                                                    class="btn btn-primary btn-block shadow btn-border-radius"
                                                    id="employeeRecordId_modal_button"
                                                    onclick="employeeRecordIdModalBtnClicked();">
                                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                                            </button>

                                            <div class="input-group" id="employeeRecordId_div"
                                                 style="display: none">
                                                <input type="hidden" name='employeeRecordId'
                                                       id='employeeRecordId_input'
                                                       value="">
                                                <button type="button" class="btn btn-secondary form-control"
                                                        disabled
                                                        id="employeeRecordId_text"></button>
                                                <span class="input-group-btn" style="width: 5%" tag='pb_html'>
														<button type="button" class="btn btn-outline-danger"
                                                                onclick="crsBtnClicked('employeeRecordId');"
                                                                id='employeeRecordId_crs_btn' tag='pb_html'>
															x
														</button>
													</span>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_EMPLOYEE_ASSIGNED_VEHICLE_ADD_ASSIGNMENTDATE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%value = "assignmentDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='assignmentDate' id='assignmentDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(vm_employee_assigned_vehicleDTO.assignmentDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_EMPLOYEE_ASSIGNED_VEHICLE_ADD_VEHICLEDESCRIPTION, loginDTO)%>
                                        </label>
                                        <div class="col-8">

                                            <textarea type='text' class='form-control'
                                                      name='vehicleDescription' id='vehicleDescription_text_<%=i%>'
                                                      tag='pb_html'/>
                                            <%=actionName.equals("ajax_edit") ? (vm_employee_assigned_vehicleDTO.vehicleDescription) : ""%>
                                            </textarea>
                                        </div>
                                    </div>
                                    

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.VM_EMPLOYEE_ASSIGNED_VEHICLE_ADD_VM_EMPLOYEE_ASSIGNED_VEHICLE_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button" onclick="submitForm()">
                                <%=LM.getText(LC.RECRUITMENT_EXAM_QUESTIONS_ADD_RECRUITMENT_EXAM_QUESTIONS_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">

    //let language = '<%=Language%>';
    const fullPageLoader = $('#full-page-loader');
    const vehicleForm = $("#bigform");

    function PreprocessBeforeSubmiting(row) {
        preprocessDateBeforeSubmitting('assignmentDate', row);
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }

        return true;
    }


    function submitForm() {
        buttonStateChange(true);
        if (PreprocessBeforeSubmiting(0)) {
            fullPageLoader.show();
            let url = "<%=servletName%>?actionType=<%=actionName%>";
            $.ajax({
                type: "POST",
                url: url,
                data: vehicleForm.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        fullPageLoader.hide();
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        fullPageLoader.hide();
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    fullPageLoader.hide();
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        } else {
            buttonStateChange(false);
            fullPageLoader.hide();
        }
    }
    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function init(row) {

        setDateByStringAndId('assignmentDate_js_' + row, $('#assignmentDate_date_' + row).val());


    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        <%if(vm_employee_assigned_vehicleDTO.employeeEmpId != -1) {%>
        const employeeModel = JSON.parse('<%=EmployeeSearchModalUtil.getEmployeeDefaultSearchModelJson(vm_employee_assigned_vehicleDTO.employeeEmpId)%>')
        employeeRecordIdInInput(employeeModel);
        <%}%>
        fullPageLoader.hide();
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    modal_button_dest_table = 'none';

    function employeeRecordIdModalBtnClicked() {
        modal_button_dest_table = 'employeeRecordId';
        $('#search_emp_modal').modal();
    }

    function crsBtnClicked(fieldName) {
        //displayClass('none');
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
        let accordions = document.querySelectorAll('.accordion-populated');
        [...accordions].forEach(accordion => {
            accordion.innerHTML = '';
        });
    }

    function employeeRecordIdInInput(empInfo) {


        $('#employeeRecordId_modal_button').hide();
        $('#employeeRecordId_div').show();

        let language = '<%=Language.toLowerCase()%>'
        let designation;
        if (language === 'english') {
            designation = empInfo.employeeNameEn + ' (' + empInfo.organogramNameEn + ', ' + empInfo.officeUnitNameEn + ')';
        } else {
            designation = empInfo.employeeNameBn + ' (' + empInfo.organogramNameBn + ', ' + empInfo.officeUnitNameBn + ')';
        }
        document.getElementById('employeeRecordId_text').innerHTML = designation;
        //$('#employeeRecordId_input').val(empInfo.employeeRecordId);
        $('#employeeRecordId_input').val(empInfo.employeeRecordId);
    }

    table_name_to_collcetion_map = new Map([
        ['employeeRecordId', {
            isSingleEntry: true,
            callBackFunction: employeeRecordIdInInput
        }]
    ]);


</script>






