<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_employee_assigned_vehicle.*" %>
<%@ page import="util.*" %>
<%@page pageEncoding="UTF-8" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>


<%
    String navigator2 = "navVM_EMPLOYEE_ASSIGNED_VEHICLE";
    String servletName = "Vm_employee_assigned_vehicleServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><%=UtilCharacter.getDataByLanguage(Language,"কর্মকর্তার আইডি","Employee Id")%></th>
            <th><%=LM.getText(LC.VM_FUEL_REQUEST_REPORT_WHERE_REQUESTEREMPID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_OFFICE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_EMPLOYEE_ASSIGNED_VEHICLE_ADD_ASSIGNMENTDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_EMPLOYEE_ASSIGNED_VEHICLE_SEARCH_VM_EMPLOYEE_ASSIGNED_VEHICLE_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span>All</span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<Vm_employee_assigned_vehicleDTO>) rn2.list;

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Vm_employee_assigned_vehicleDTO vm_employee_assigned_vehicleDTO = (Vm_employee_assigned_vehicleDTO) data.get(i);


        %>
        <tr>

            <td>
                <%
                    value = "";
                    Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().getById(vm_employee_assigned_vehicleDTO.employeeEmpId);
                    if(employee_recordsDTO != null){
                        value = employee_recordsDTO.employeeNumber;
                    }
                %>
                <%=Utils.getDigits(value,Language)%>
            </td>


            <td>
                <%=UtilCharacter.getDataByLanguage(Language, vm_employee_assigned_vehicleDTO.employeeNameBn, vm_employee_assigned_vehicleDTO.employeeNameEn)%>
            </td>

            <td>
                <%=UtilCharacter.getDataByLanguage(Language, vm_employee_assigned_vehicleDTO.employeeOfficeUnitOrgNameBn, vm_employee_assigned_vehicleDTO.employeeOfficeUnitOrgNameEn)%>
            </td>

            <td>
                <%=UtilCharacter.getDataByLanguage(Language, vm_employee_assigned_vehicleDTO.employeeOfficeUnitNameBn, vm_employee_assigned_vehicleDTO.employeeOfficeUnitNameEn)%>
            </td>


            <td>
                <%
                    value = vm_employee_assigned_vehicleDTO.assignmentDate + "";
                %>
                <%
                    String formatted_assignmentDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                %>
                <%=Utils.getDigits(formatted_assignmentDate, Language)%>


            </td>


            <%CommonDTO commonDTO = vm_employee_assigned_vehicleDTO; %>
            <%@include file="../pb/searchAndViewButton.jsp" %>

            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID'
                                                 value='<%=vm_employee_assigned_vehicleDTO.iD%>'/></span>
                </div>
            </td>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			