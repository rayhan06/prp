<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_employee_assigned_vehicle.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>


<%
    String servletName = "Vm_employee_assigned_vehicleServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Vm_employee_assigned_vehicleDTO vm_employee_assigned_vehicleDTO = Vm_employee_assigned_vehicleDAO.getInstance().getDTOByID(id);
    CommonDTO commonDTO = vm_employee_assigned_vehicleDTO;
%>
<%@include file="../pb/viewInitializer.jsp" %>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.VM_EMPLOYEE_ASSIGNED_VEHICLE_ADD_VM_EMPLOYEE_ASSIGNED_VEHICLE_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.VM_EMPLOYEE_ASSIGNED_VEHICLE_ADD_VM_EMPLOYEE_ASSIGNED_VEHICLE_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>


                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.VM_FUEL_REQUEST_REPORT_WHERE_REQUESTEREMPID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=UtilCharacter.getDataByLanguage(Language, vm_employee_assigned_vehicleDTO.employeeNameBn, vm_employee_assigned_vehicleDTO.employeeNameEn)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=UtilCharacter.getDataByLanguage(Language, vm_employee_assigned_vehicleDTO.employeeOfficeUnitOrgNameBn, vm_employee_assigned_vehicleDTO.employeeOfficeUnitOrgNameEn)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_OFFICE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=UtilCharacter.getDataByLanguage(Language, vm_employee_assigned_vehicleDTO.employeeOfficeUnitNameBn, vm_employee_assigned_vehicleDTO.employeeOfficeUnitNameEn)%>


                                    </div>
                                </div>


                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.VM_EMPLOYEE_ASSIGNED_VEHICLE_ADD_ASSIGNMENTDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = vm_employee_assigned_vehicleDTO.assignmentDate + "";
                                        %>
                                        <%
                                            String formatted_assignmentDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                        %>
                                        <%=Utils.getDigits(formatted_assignmentDate, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.VM_EMPLOYEE_ASSIGNED_VEHICLE_ADD_VEHICLEDESCRIPTION, loginDTO)%>
                                    </label>
                                    <div class="col-8">

                                        <%=Utils.getDigits(vm_employee_assigned_vehicleDTO.vehicleDescription, Language)%>


                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>