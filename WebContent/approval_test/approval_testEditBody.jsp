<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="approval_test.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@ page import="approval_execution_table.*"%>
<%@ page import="approval_path.*"%>
<%@ page import="user.*"%>

<%@page import="workflow.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
Approval_testDTO approval_testDTO;
approval_testDTO = (Approval_testDTO)request.getAttribute("approval_testDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
if(approval_testDTO == null)
{
	approval_testDTO = new Approval_testDTO();
	
}
System.out.println("approval_testDTO = " + approval_testDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = LM.getText(LC.APPROVAL_TEST_ADD_APPROVAL_TEST_ADD_FORMNAME, loginDTO);
String servletName = "Approval_testServlet";
String fileColumnName = "";

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

long ColumnID = -1;
FilesDAO filesDAO = new FilesDAO();
boolean isPermanentTable = true;
if(request.getParameter("isPermanentTable") != null)
{
	isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
}

Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
Approval_execution_tableDTO approval_execution_tableDTO = null;
Approval_execution_tableDTO approval_execution_table_initiationDTO = null;
ApprovalPathDetailsDTO approvalPathDetailsDTO =  null;

String tableName = "approval_test";

boolean canApprove = false, canValidate = false, isInitiator = false, canTerminate = false;

if(!isPermanentTable)
{
	approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("approval_test", approval_testDTO.iD);
	System.out.println("approval_execution_tableDTO = " + approval_execution_tableDTO);
	approvalPathDetailsDTO = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
	approval_execution_table_initiationDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getInitiationDTOByUpdatedRowId("approval_test", approval_testDTO.iD);
	if(approvalPathDetailsDTO!= null && approvalPathDetailsDTO.organogramId == userDTO.organogramID)
	{
		canApprove = true;
		if(approvalPathDetailsDTO.approvalRoleCat == SessionConstants.VALIDATOR)
		{
			canValidate = true;
		}
	}
	
	isInitiator = WorkflowController.isInitiator(tableName, approval_execution_tableDTO.previousRowId, userDTO.organogramID);
	
	canTerminate = isInitiator && approval_testDTO.isDeleted == 2;
}
String Language = LM.getText(LC.APPROVAL_TEST_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Approval_testServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-11">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=approval_testDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.APPROVAL_TEST_ADD_DESCRIPTION, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='description' id = 'description_text_<%=i%>' value='<%=approval_testDTO.description%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='jobCat' id = 'jobCat_hidden_<%=i%>' value='<%=approval_testDTO.jobCat%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=approval_testDTO.isDeleted%>' tag='pb_html'/>
											
														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=approval_testDTO.lastModificationTime%>' tag='pb_html'/>
					
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
               <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.APPROVAL_TEST_ADD_APPROVAL_TEST_BABY, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
									<tr>
										<th><%=LM.getText(LC.APPROVAL_TEST_ADD_APPROVAL_TEST_BABY_INSCRIPTION, loginDTO)%></th>
										<th><%=LM.getText(LC.APPROVAL_TEST_ADD_APPROVAL_TEST_BABY_ANUMBER, loginDTO)%></th>
										<th><%=LM.getText(LC.APPROVAL_TEST_ADD_APPROVAL_TEST_BABY_REMOVE, loginDTO)%></th>
									</tr>
								</thead>
							<tbody id="field-ApprovalTestBaby">
						
						
								<%
									if(actionName.equals("edit")){
										int index = -1;
										
										
										for(ApprovalTestBabyDTO approvalTestBabyDTO: approval_testDTO.approvalTestBabyDTOList)
										{
											index++;
											
											System.out.println("index index = "+index);

								%>	
							
								<tr id = "ApprovalTestBaby_<%=index + 1%>">
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='approvalTestBaby.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=approvalTestBabyDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='approvalTestBaby.approvalTestId' id = 'approvalTestId_hidden_<%=childTableStartingID%>' value='<%=approvalTestBabyDTO.approvalTestId%>' tag='pb_html'/>
									</td>
									<td>										





																<input type='text' class='form-control'  name='approvalTestBaby.inscription' id = 'inscription_text_<%=childTableStartingID%>' value='<%=approvalTestBabyDTO.inscription%>'   tag='pb_html'/>					
									</td>
									<td>										





																<%
																	value = "";
																	if(approvalTestBabyDTO.aNumber != -1)
																	{
																	value = approvalTestBabyDTO.aNumber + "";
																	}
																%>		
																<input type='number' class='form-control'  name='approvalTestBaby.aNumber' id = 'aNumber_number_<%=childTableStartingID%>' value='<%=value%>'  tag='pb_html'>		
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='approvalTestBaby.isDeleted' id = 'isDeleted_hidden_<%=childTableStartingID%>' value= '<%=approvalTestBabyDTO.isDeleted%>' tag='pb_html'/>
											
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='approvalTestBaby.lastModificationTime' id = 'lastModificationTime_hidden_<%=childTableStartingID%>' value='<%=approvalTestBabyDTO.lastModificationTime%>' tag='pb_html'/>
									</td>
									<td>
										<span id='chkEdit'>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
												   class="form-control-sm"/>
										</span>
									</td>
								</tr>								
								<%	
											childTableStartingID ++;
										}
									}
								%>						
						
								</tbody>
							</table>
						</div>
						<div class="form-group">
								<div class="col-xs-9 text-right">
									<button
											id="add-more-ApprovalTestBaby"
											name="add-moreApprovalTestBaby"
											type="button"
											class="btn btn-sm text-white add-btn shadow">
										<i class="fa fa-plus"></i>
										<%=LM.getText(LC.HM_ADD, loginDTO)%>
									</button>
									<button
											id="remove-ApprovalTestBaby"
											name="removeApprovalTestBaby"
											type="button"
											class="btn btn-sm remove-btn shadow ml-2 pl-4">
										<i class="fa fa-trash"></i>
									</button>
								</div>
							</div>
					
							<%ApprovalTestBabyDTO approvalTestBabyDTO = new ApprovalTestBabyDTO();%>
					
							<template id="template-ApprovalTestBaby" >						
								<tr>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='approvalTestBaby.iD' id = 'iD_hidden_' value='<%=approvalTestBabyDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='approvalTestBaby.approvalTestId' id = 'approvalTestId_hidden_' value='<%=approvalTestBabyDTO.approvalTestId%>' tag='pb_html'/>
									</td>
									<td>





																<input type='text' class='form-control'  name='approvalTestBaby.inscription' id = 'inscription_text_' value='<%=approvalTestBabyDTO.inscription%>'   tag='pb_html'/>					
									</td>
									<td>





																<%
																	value = "";
																	if(approvalTestBabyDTO.aNumber != -1)
																	{
																	value = approvalTestBabyDTO.aNumber + "";
																	}
																%>		
																<input type='number' class='form-control'  name='approvalTestBaby.aNumber' id = 'aNumber_number_' value='<%=value%>'  tag='pb_html'>		
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='approvalTestBaby.isDeleted' id = 'isDeleted_hidden_' value= '<%=approvalTestBabyDTO.isDeleted%>' tag='pb_html'/>
											
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='approvalTestBaby.lastModificationTime' id = 'lastModificationTime_hidden_' value='<%=approvalTestBabyDTO.lastModificationTime%>' tag='pb_html'/>
									</td>
									<td>
											<span id='chkEdit'>
												<input type='checkbox' name='checkbox' value='' deletecb='true'
													   class="form-control-sm"/>
											</span>
									</td>
								</tr>								
						
							</template>
                        </div>
                    </div>               
				<div class="">
					<%
						if (canValidate) {
					%>
					<%@include file="../approval_path/validateDiv.jsp" %>
					<%
						}
					%>
				</div>
                <div class="form-actions text-right mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.APPROVAL_TEST_ADD_APPROVAL_TEST_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.APPROVAL_TEST_ADD_APPROVAL_TEST_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>				

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, validate)
{

	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	submitAddForm();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Approval_testServlet");	
}

function init(row)
{


	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;

	$("#add-more-ApprovalTestBaby").click(
        function(e) 
		{
            e.preventDefault();
            var t = $("#template-ApprovalTestBaby");

            $("#field-ApprovalTestBaby").append(t.html());
			SetCheckBoxValues("field-ApprovalTestBaby");
			
			var tr = $("#field-ApprovalTestBaby").find("tr:last-child");
			
			tr.attr("id","ApprovalTestBaby_" + child_table_extra_id);
			
			tr.find("[tag='pb_html']").each(function( index ) 
			{
				var prev_id = $( this ).attr('id');
				$( this ).attr('id', prev_id + child_table_extra_id);
				console.log( index + ": " + $( this ).attr('id') );
			});
			

			child_table_extra_id ++;

        });

    
      $("#remove-ApprovalTestBaby").click(function(e){    	
	    var tablename = 'field-ApprovalTestBaby';
	    var i = 0;
		console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
		var element = document.getElementById(tablename);

		var j = 0;
		for(i = document.getElementById(tablename).childNodes.length - 1; i >= 0 ; i --)
		{
			var tr = document.getElementById(tablename).childNodes[i];
			if(tr.nodeType === Node.ELEMENT_NODE)
			{
				console.log("tr.childNodes.length= " + tr.childNodes.length);
				var checkbox = tr.querySelector('input[deletecb="true"]');				
				if(checkbox.checked == true)
				{
					tr.remove();
				}
				j ++;
			}
			
		}    	
    });


</script>






