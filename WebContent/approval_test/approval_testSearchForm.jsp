
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="approval_test.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String value = "";
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
String Language = (userDTO.languageID == SessionConstants.BANGLA)? "Bangla":"English";
String navigator2 = "navAPPROVAL_TEST";
System.out.println("navigator2 = " + navigator2);
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
System.out.println("rn2 = " + rn2);
String pageno2 = ( rn2 == null ) ? "1" : "" + rn2.getCurrentPageNo();
String totalpage2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalPages();
String totalRecords2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalRecords();
String lastSearchTime = ( rn2 == null ) ? "0" : "" + rn2.getSearchTime();
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;
String ajax = request.getParameter("ajax");
boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
			
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.APPROVAL_TEST_ADD_DESCRIPTION, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>
								<th><%=LM.getText(LC.APPROVAL_TEST_SEARCH_APPROVAL_TEST_EDIT_BUTTON, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_HISTORY, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_SEND_TO_APPROVAL_PATH, loginDTO)%></th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList) session.getAttribute("viewAPPROVAL_TEST");

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Approval_testDTO approval_testDTO = (Approval_testDTO) data.get(i);
																																
											
											%>
											<tr>
								
		
											<td>
											<%
											value = approval_testDTO.description + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
		
	

											<td>
												<button
														type="button"
														class="btn-sm border-0 shadow bg-light btn-border-radius"
														style="color: #ff6b6b;"
														onclick="location.href='Approval_testServlet?actionType=view&ID=<%=approval_testDTO.iD%>'"
												>
													<i class="fa fa-eye"></i>
												</button>												
											</td>
	
											<td>
												<button
														type="button"
														class="btn-sm border-0 shadow btn-border-radius text-white"
														style="background-color: #ff6b6b;"
														onclick="location.href='Approval_testServlet?actionType=getEditPage&ID=<%=approval_testDTO.iD%>'"
												>
													<i class="fa fa-edit"></i>
												</button>																			
											</td>											
											
											
											<td>
											<%
												if(approval_testDTO.isDeleted == 0 && approval_execution_tableDTO != null)
												{
											%>
												<button
												class="btn btn-primary shadow border-0 btn-border-radius"
												style="background-color: #0098bf; border-radius: 6px"
												onclick="location.href='Approval_execution_tableServlet?actionType=search&tableName=approval_test&previousRowId=<%=approval_execution_tableDTO.previousRowId%>'"
												>
													<i class="fa fa-history"></i>
													<%=LM.getText(LC.HM_HISTORY, loginDTO)%>
												</button>
											<%
												}
												else
												{
											%>
												<%=LM.getText(LC.HM_NO_HISTORY_IS_AVAILABLE, loginDTO)%>
											<%
												}
											%>
											</td>
											
											<td>
											<%
											if(approval_testDTO.jobCat == SessionConstants.DEFAULT_JOB_CAT)
											{
											%>
												<button type="button" class="btn submit-btn text-white shadow btn-border-radius" data-toggle="modal" data-target="#sendToApprovalPathModal" >
													<%=LM.getText(LC.HM_SEND_TO_APPROVAL_PATH, loginDTO)%>
												</button>
												<%@include file="../inbox_internal/sendToApprovalPathModal.jsp"%>
											<%
											}
											else
											{
											%>
											<%=LM.getText(LC.HM_NO_ACTION_IS_REQUIRED, loginDTO)%>
											<%
											}
											%>
											</td>
											<td class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=approval_testDTO.iD%>'/></span>
												</div>
											</td>
																						
											
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			