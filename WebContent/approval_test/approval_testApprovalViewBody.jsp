

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="approval_test.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@ page import="approval_execution_table.*"%>
<%@ page import="approval_path.*"%>
<%@page import="user.*"%>
<%@page import="workflow.*"%>
<%@ page import="util.CommonConstant" %>



<%
	String servletName = "Approval_testServlet";
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String Language = LM.getText(LC.APPROVAL_TEST_EDIT_LANGUAGE, loginDTO);

	UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

	String actionName = "edit";
	String failureMessage = (String)request.getAttribute("failureMessage");
	if(failureMessage == null || failureMessage.isEmpty())
	{
		failureMessage = "";
	}
	out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
	String value = "";


	String ID = request.getParameter("ID");
	boolean isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));

	if(ID == null || ID.isEmpty())
	{
		ID = "0";
	}
	long id = Long.parseLong(ID);
	System.out.println("ID = " + ID + " isPermanentTable = " + isPermanentTable);
	Approval_testDAO approval_testDAO = new Approval_testDAO("approval_test");
	Approval_testDTO approval_testDTO = approval_testDAO.getDTOByID(id);
	String Value = "";
	int i = 0;
	FilesDAO filesDAO = new FilesDAO();
	
	String tableName = "approval_test";

	Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
	ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
	Approval_execution_tableDTO approval_execution_tableDTO = null;
	Approval_execution_tableDTO approval_execution_table_initiationDTO = null;
	ApprovalPathDetailsDTO approvalPathDetailsDTO =  null;

	boolean canApprove = false, canValidate = false, isInitiator = false, canTerminate = false;
	boolean isInPreviousOffice = false;
	String Message = "Done";

	if(!isPermanentTable)
	{
		approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("approval_test", approval_testDTO.iD);
		System.out.println("approval_execution_tableDTO = " + approval_execution_tableDTO);
		approvalPathDetailsDTO = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
		approval_execution_table_initiationDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getInitiationDTOByPreviousRowId("approval_test", approval_execution_tableDTO.previousRowId);
		if(approvalPathDetailsDTO!= null && approvalPathDetailsDTO.organogramId == userDTO.organogramID 
				&& approval_execution_tableDTO.iD == approval_execution_tableDAO.getMostRecentExecutionIDByPreviousRowID("approval_test", approval_execution_tableDTO.previousRowId))
		{
			canApprove = true;
			if(approvalPathDetailsDTO.approvalRoleCat == SessionConstants.VALIDATOR)
			{
				canValidate = true;
			}
		}
		
		isInitiator = WorkflowController.isInitiator(tableName, approval_execution_tableDTO.previousRowId, userDTO.organogramID);
		
		canTerminate = isInitiator && approval_testDTO.isDeleted == 2;
	}	
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.APPROVAL_TEST_ADD_APPROVAL_TEST_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.APPROVAL_TEST_ADD_APPROVAL_TEST_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>

			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.APPROVAL_TEST_ADD_DESCRIPTION, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = approval_testDTO.description + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
			
			
			
		

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.HM_INITIATOR_REMARKS, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = approval_execution_table_initiationDTO.remarks + "";
                                        %>
                                        <%=value%>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.HM_INITIATOR_FILES, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            {
                                                List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(approval_execution_table_initiationDTO.fileDropzone);
                                        %>
                                        <table>
                                            <tr>
                                                <%
                                                    if (FilesDTOList != null) {
                                                        for (int j = 0; j < FilesDTOList.size(); j++) {
                                                            FilesDTO filesDTO = FilesDTOList.get(j);
                                                            byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                %>
                                                <td>
                                                    <%
                                                        if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                    %>
                                                    <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                         style='width:100px'/>
                                                    <%
                                                        }
                                                    %>
                                                    <a href='Approval_execution_tableServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                       download><%=filesDTO.fileTitle%>
                                                    </a>
                                                </td>
                                                <%
                                                        }
                                                    }
                                                %>
                                            </tr>
                                        </table>
                                        <%
                                            }
                                        %>
                                    </div>
                                </div>
							</div>
                        </div>
                    </div>
                </div>
            </div>				

             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%=LM.getText(LC.APPROVAL_TEST_ADD_APPROVAL_TEST_BABY, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.APPROVAL_TEST_ADD_APPROVAL_TEST_BABY_INSCRIPTION, loginDTO)%></th>
								<th><%=LM.getText(LC.APPROVAL_TEST_ADD_APPROVAL_TEST_BABY_ANUMBER, loginDTO)%></th>
							</tr>
							<%
                        	ApprovalTestBabyDAO approvalTestBabyDAO = new ApprovalTestBabyDAO();
                         	List<ApprovalTestBabyDTO> approvalTestBabyDTOs = approvalTestBabyDAO.getApprovalTestBabyDTOListByApprovalTestID(approval_testDTO.iD);
                         	
                         	for(ApprovalTestBabyDTO approvalTestBabyDTO: approvalTestBabyDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%
											value = approvalTestBabyDTO.inscription + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = approvalTestBabyDTO.aNumber + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
            <%if (!isPermanentTable && (canApprove || canTerminate)) {%>
            <div class="row div_border attachement-div mt-5">
                <div class="col-md-12">
                    <h5>
                        <%=LM.getText(LC.HM_ATTACHMENTS, loginDTO)%>
                    </h5>
                    <%
                        long ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                    %>
                    <div class="dropzone"
                         action="Approval_execution_tableServlet?pageType=add&actionType=UploadFilesFromDropZone&columnName=approval_attached_fileDropzone&ColumnID=<%=ColumnID%>">
                        <input type='file' style="display: none"
                               name='approval_attached_fileDropzoneFile'
                               id='approval_attached_fileDropzone_dropzone_File_<%=i%>'
                               tag='pb_html'/>
                    </div>
                    <input type='hidden'
                           name='approval_attached_fileDropzoneFilesToDelete'
                           id='approval_attached_fileDropzoneFilesToDelete_<%=i%>' value=''
                           tag='pb_html'/>
                    <input type='hidden'
                           name='approval_attached_fileDropzone'
                           id='approval_attached_fileDropzone_dropzone_<%=i%>' tag='pb_html'
                           value='<%=ColumnID%>'/>
                </div>
                <div class="col-md-12 mt-5">
                    <h5>
                        <%=LM.getText(LC.HM_REMARKS, loginDTO)%>
                    </h5>
                    <textarea class='form-control' name='remarks' id='<%=i%>_remarks' tag='pb_html'></textarea>
                </div>
            </div>
            <%}%>
            <div class="row mt-5">
                <div class="col-12">
                    <div class="form-actions text-right">
                        <button type="button" class="btn btn-info shadow btn-border-radius" id='printer'
                                onclick="printAnyDiv('modalbody')">Print
                        </button>
                        <%
                            if (canApprove) {
                        %>
                        <button type="submit" class="btn btn-success shadow btn-border-radius"
                                id='approve_<%=id%>' data-toggle="modal"
                                data-target="#approvalModal"><%=LM.getText(LC.HM_APPROVE, loginDTO)%>
                        </button>
                        <%@include file="../approval_path/approvalModal.jsp" %>
                        <button type="submit" class="btn btn-danger shadow btn-border-radius"
                                id='reject_<%=id%>' data-toggle="modal"
                                data-target="#rejectionModal"><%=LM.getText(LC.HM_REJECT, loginDTO)%>
                        </button>
                        <%@include file="../approval_path/rejectionModal.jsp" %>
                        <%
                            }
                            if (canTerminate) {
                        %>
                        <button type="submit" class="btn btn-success shadow btn-border-radius"
                                id='approve_<%=id%>'
                                onclick='approve("<%=approval_execution_tableDTO.updatedRowId%>", "<%=Message%>", <%=i%>, "Approval_testServlet", 2, true, true)'><%=LM.getText(LC.HM_TERMINATE, loginDTO)%>
                        </button>
                        <%
                            }
                            if (canValidate) {
                        %>
                        <a type="submit" class="btn btn-success shadow btn-border-radius" id='validate_<%=id%>'
                           href='Approval_testServlet?actionType=getEditPage&ID=<%=id%>&isPermanentTable=<%=isPermanentTable%>'><%=LM.getText(LC.HM_VALIDATE, loginDTO)%>
                        </a>
                        <%
                            }
                        %>
                        <button type="submit" class="btn btn-success shadow btn-border-radius"
                                id='history_<%=id%>' data-toggle="modal"
                                data-target="#historyModal"><%=LM.getText(LC.HM_HISTORY, loginDTO)%>
                        </button>
                        <%@include file="../approval_path/historyModal.jsp" %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        
        
<script type="text/javascript">
$(document).ready(function()
{
	console.log("using ckEditor");
     CKEDITOR.replaceAll();
});
 </script>