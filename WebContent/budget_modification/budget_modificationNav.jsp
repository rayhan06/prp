<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoRepository" %>
<%@ page import="budget_modification.Budget_modificationType" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="budget_operation.Budget_operationRepository" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    System.out.println("Inside nav.jsp");
    String url = "Budget_modificationServlet?actionType=search";
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1">
    <div class="kt-portlet__body">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label" for="budget_selection_info_id">
                            <%=LM.getText(LC.CODE_SELECTION_SEARCH_ECONOMIC_YEAR, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control'
                                    name='budget_selection_info_id'
                                    id='budget_selection_info_id'
                            >
                                <%=BudgetSelectionInfoRepository.getInstance().buildOptionForSubmittedEconomicYears(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label" for="modification_type">
                            <%=isLanguageEnglishNav? "Modification Type" : "পরিবর্ধনের ধরণ"%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control'
                                    name='modification_type'
                                    id='modification_type'
                            >
                                <%=Budget_modificationType.buildOptions(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label" for="budget_office_id">
                            <%=LM.getText(LC.BUDGET_OFFICE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control'
                                    name='budget_office_id'
                                    id='budget_office_id'
                            >
                                <%=Budget_officeRepository.getInstance().buildOptions(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label" for="budget_operation_id">
                            <%=LM.getText(LC.CODE_SELECTION_SEARCH_BUDGET_OPEATION_CODE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control'
                                    name='budget_operation_id'
                                    id='budget_operation_id'
                            >
                                <%=Budget_operationRepository.getInstance().buildOperationCodes(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->

<%@include file="../common/pagination_with_go2.jsp" %>

<template id="loader">
    <div class="modal-body">
        <div class="search-loader-container-circle ">
            <div class="search-loader-circle"></div>
        </div>
    </div>
</template>


<script type="text/javascript">

    $(document).ready(() => {
        select2SingleSelector('#budget_selection_info_id', '<%=Language%>');
        select2SingleSelector('#budget_office_id', '<%=Language%>');
        select2SingleSelector('#budget_operation_id', '<%=Language%>');
        select2SingleSelector('#modification_type', '<%=Language%>');
    });

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                setTimeout(() => {
                    document.getElementById('tableForm').innerHTML = this.responseText ;
                    setPageNo();
                    searchChanged = 0;
                }, 200);
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "<%=url%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        let params = '&modification_type=' + $('#modification_type').val();
        params += '&budget_selection_info_id=' + $('#budget_selection_info_id').val();
        params += '&budget_office_id=' + $('#budget_office_id').val();
        params += '&budget_operation_id=' + $('#budget_operation_id').val();

        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }
</script>