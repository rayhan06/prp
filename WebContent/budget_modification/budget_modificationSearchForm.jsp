<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="overtime_allowance.*" %>
<%@ page import="util.*" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="overtime_bill.Overtime_billDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="util.StringUtils" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="overtime_bill.Overtime_billStatus" %>
<%@ page import="budget_modification.Budget_modificationDTO" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoRepository" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="budget_operation.Budget_operationRepository" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@page pageEncoding="UTF-8" %>

<%
    String navigator2 = "navOVERTIME_ALLOWANCE";
    String servletName = "Budget_modificationServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead class="text-center">
        <tr>
            <th>
                <%=isLanguageEnglish? "Economic Year" : "অর্থ বছর"%>
            </th>
            <th>
                <%=isLanguageEnglish? "Operation Code" : "অপারেশন কোড"%>
            </th>
            <th>
                <%=isLanguageEnglish? "Office" : "দপ্তর"%>
            </th>
            <th>
                <%=isLanguageEnglish? "Modification Type" : "পরিবর্ধনের ধরণ"%>
            </th>
            <th>
                <%=isLanguageEnglish? "View Details" : "বিস্তারিত দেখুন"%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Budget_modificationDTO> data = (List<Budget_modificationDTO>) recordNavigator.list;
            try {
                if (data != null) {
                    for (Budget_modificationDTO budgetModificationDTO :  data) {
        %>
                        <tr>
                            <td>
                                <%=BudgetSelectionInfoRepository.getInstance().getEconomicYearById(
                                        Language,
                                        budgetModificationDTO.budgetSelectionInfoId
                                )%>
                            </td>
                            <td>
                                <%=Budget_operationRepository.getInstance().getCode(
                                        Language,
                                        budgetModificationDTO.budgetOperationId
                                )%>
                            </td>
                            <td>
                                <%=Budget_officeRepository.getInstance().getText(
                                        budgetModificationDTO.budgetOfficeId,
                                        Language
                                )%>
                            </td>
                            <td>
                                <%=budgetModificationDTO.modificationType.getName(isLanguageEnglish)%>
                            </td>
                            <td>
                                <button
                                        type="button"
                                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                                        style="color: #ff6b6b;"
                                        onclick="location.href='<%=servletName%>?actionType=view&ID=<%=budgetModificationDTO.iD%>'"
                                >
                                    <i class="fa fa-eye"></i>
                                </button>
                            </td>
                        </tr>
        <%
                    }
                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>