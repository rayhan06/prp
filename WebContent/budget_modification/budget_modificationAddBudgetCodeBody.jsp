<%@page pageEncoding="UTF-8" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoRepository" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@ page import="dbm.DBMW" %>
<%@ page import="economic_group.EconomicGroupRepository" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String formTitle = isLanguageEnglish ? "Budget Modification (Add Budget Code)" : "বাজেট পরিবর্ধন (বাজেট কোড সংযুক্তি)";

    String servletName = "Budget_modificationServlet";
    String actionName = "add";
    int i = 0;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal" id="add-budget-code-form">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <%--form data goes here--%>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="budgetSelectionInfoId">
                                            <%=LM.getText(LC.CODE_SELECTION_SEARCH_ECONOMIC_YEAR, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control'
                                                    name='budgetSelectionInfoId'
                                                    id='budgetSelectionInfoId'
                                                    onSelect='setSearchChanged()' style="width: 100%">
                                                <%=BudgetSelectionInfoRepository.getInstance().buildEconomicYears(Language, null)%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="budgetInstitutionalGroup">
                                            <%=LM.getText(LC.BUDGET_INSTITUTIONAL_GROUP, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select id="budgetInstitutionalGroup"
                                                    name="budgetInstitutionalGroupId"
                                                    class='form-control rounded shadow-sm w-100'
                                                    onchange="institutionalGroupChanged(this);"
                                            >
                                                <%=Budget_institutional_groupRepository.getInstance().buildOptions(Language, 0L, false)%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="budgetCat">
                                            <%=LM.getText(LC.BUDGET_BUDGET_TYPE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select id="budgetCat"
                                                    name="budgetCat"
                                                    class='form-control rounded shadow-sm w-100'
                                                    onchange="budgetCatChanged(this);"
                                            >
                                                <%--Dynamically Added with AJAX--%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="budgetOffice">
                                            <%=LM.getText(LC.BUDGET_OFFICE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select id="budgetOffice"
                                                    name="budgetOfficeId"
                                                    class='form-control rounded shadow-sm w-100'
                                                    onchange="budgetOfficeChanged(this);">
                                                <%--Dynamically Added with AJAX--%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="budgetMappingId">
                                            <%=LM.getText(LC.BUDGET_OPERATION_CODE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select id="budgetMappingId"
                                                    name="budgetMappingId"
                                                    class='form-control rounded shadow-sm w-100'
                                            >
                                                <%--Dynamically Added with AJAX--%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="economicGroupId">
                                            <%=isLanguageEnglish ? "Economic Group" : "অর্থনৈতিক গ্রুপ"%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control'
                                                    name="economicGroupId"
                                                    id='economicGroupId'
                                                    onchange="economicGroupChanged(this);">
                                                <%=EconomicGroupRepository.getInstance().buildOptionWithCode(Language, null)%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="economicSubCodeId">
                                            <%=isLanguageEnglish ? "Economic Code (4 Digits)" : "অর্থনৈতিক কোড (৪ অংকের)"%>
                                        </label>
                                        <div class="col-md-8">
                                            <select id="economicCodeId"
                                                    name="economicCodeId"
                                                    class='form-control rounded shadow-sm w-100'
                                                    onchange="economicCodeChanged(this);"
                                            >
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="economicSubCodeId">
                                            <%=isLanguageEnglish ? "Economic Code (7 Digits)" : "অর্থনৈতিক কোড (৭ অংকের)"%>
                                        </label>
                                        <div class="col-md-8">
                                            <select id="economicSubCodeId"
                                                    name="economicSubCodeId"
                                                    class='form-control rounded shadow-sm w-100'
                                            >
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=isLanguageEnglish ? "Documents" : "নথিপত্র"%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                String fileColumnName = "fileDropzone";
                                                long ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                            %>
                                            <div class="dropzone"
                                                 action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=ColumnID%>">
                                                <input type='file' style="display:none" name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value='' tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=ColumnID%>'/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button type="button" id="cancel-btn"
                                    class="btn-sm shadow text-white border-0 cancel-btn"
                                    onclick="location.href = '<%=request.getHeader("referer")%>'">
                                <%=isLanguageEnglish ? "Cancel" : "বাতিল করুন"%>
                            </button>
                            <button type="button" id="submit-btn"
                                    class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    onclick="submitOtBillSubmissionConfigForm()">
                                <%=isLanguageEnglish ? "Submit" : "জমাদিন"%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    const form = $('#add-budget-code-form');

    const budgetInstitutionalGroup = document.getElementById('budgetInstitutionalGroup');
    const budgetCat = document.getElementById('budgetCat');
    const budgetOffice = document.getElementById('budgetOffice');
    const budgetMappingId = document.getElementById('budgetMappingId');
    const economicCodeId = document.getElementById('economicCodeId');
    const economicSubCodeId = document.getElementById('economicSubCodeId');

    function clearSelects(startIndex) {
        const selectElements = [budgetCat, budgetOffice, budgetMappingId];
        for (let i = startIndex; i < selectElements.length; i++) {
            selectElements[i].innerHTML = '';
        }
    }

    async function institutionalGroupChanged(selectElement) {
        const selectedInstitutionalGroupId = selectElement.value;
        clearSelects(0);
        if (selectedInstitutionalGroupId === '') return;

        const url = 'Budget_mappingServlet?actionType=getBudgetCatList&budget_instituitional_group_id='
                    + selectedInstitutionalGroupId;
        const response = await fetch(url);
        budgetCat.innerHTML = await response.text();
    }

    async function budgetCatChanged(selectElement) {
        const selectedBudgetCat = selectElement.value;
        clearSelects(1);
        if (selectedBudgetCat === '') return;

        const selectedInstitutionalGroup = budgetInstitutionalGroup.value;
        const url = 'Budget_mappingServlet?actionType=getBudgetOfficeList&&withCode=true&budget_instituitional_group_id='
                    + selectedInstitutionalGroup + '&budget_cat=' + selectedBudgetCat;
        const response = await fetch(url);
        budgetOffice.innerHTML = await response.text();
    }

    async function budgetOfficeChanged(selectElement) {
        const selectedBudgetOffice = selectElement.value;
        clearSelects(2);
        if (selectedBudgetOffice === '') return;

        const selectedInstitutionalGroup = budgetInstitutionalGroup.value;
        const selectedBudgetCat = budgetCat.value;

        const url = 'Budget_mappingServlet?actionType=getOperationCodeList&withCode=true&budget_instituitional_group_id='
                    + selectedInstitutionalGroup + '&budget_cat=' + selectedBudgetCat
                    + '&budget_office_id=' + selectedBudgetOffice;
        const response = await fetch(url);
        budgetMappingId.innerHTML = await response.text();
    }

    async function economicGroupChanged(selectElement) {
        const selectedEconomicGroup = selectElement.value;
        economicCodeId.innerHTML = '';
        economicSubCodeId.innerHTML = '';
        if (selectedEconomicGroup === '') return;
        const url = 'Budget_mappingServlet?actionType=buildEconomicCode&economic_group_type=' + selectedEconomicGroup;
        const data = await fetch(url);
        economicCodeId.innerHTML = await data.text();
    }

    async function economicCodeChanged(selectElement) {
        const selectedEconomicCode = selectElement.value;
        economicSubCodeId.innerHTML = '';
        if (selectedEconomicCode === '') return;
        const url = 'Budget_mappingServlet?actionType=getEconomicSubCodeOption'
                    + '&economicCode=' + selectedEconomicCode;
        const response = await fetch(url);
        economicSubCodeId.innerHTML = await response.text();
    }


    function submitOtBillSubmissionConfigForm() {
        submitAjaxByData(form.serialize(), "Budget_modificationServlet?actionType=ajax_addBudgetCode");
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }
</script>