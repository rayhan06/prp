<%@page import="login.LoginDTO" %>
<%@page import="java.util.*" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="login.LoginDTO" %>
<%@page import="files.*" %>
<%@page import="common.BaseServlet" %>
<%@page import="budget_modification.Budget_modificationDTO" %>
<%@page import="util.HttpRequestUtils" %>
<%@page import="budget_modification.Budget_modificationType" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoRepository" %>
<%@ page import="budget_operation.Budget_operationRepository" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@ page import="language.LM" %>
<%@ page import="language.LC" %>
<%@ page import="budget.BudgetUtils" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;

    Budget_modificationDTO budgetModificationDTO = (Budget_modificationDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    FilesDAO filesDAO = new FilesDAO();
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>
                    <%=isLangEn ? "Budget Modification Details" : "বাজেট পরিবর্ধনের বিস্তারিত"%>
                </h3>
            </div>
        </div>

        <div class="kt-portlet__body form-body">
            <div class="table-responsive">
                <%if(budgetModificationDTO.modificationType == Budget_modificationType.BUDGET_AMOUNT_CHANGE){%>
                <h5 class="text-right">(<%=LM.getText(LC.BUDGET_MONEY_INPUT_UNIT_HEADER, loginDTO)%>)</h5>
                <%}%>
                <table class="table table-bordered table-striped text-nowrap">
                    <tr>
                        <td style="width:30%">
                            <b><%=isLangEn? "Economic Year" : "অর্থ বছর"%></b>
                        </td>
                        <td>
                            <%=BudgetSelectionInfoRepository.getInstance().getEconomicYearById(
                                    Language,
                                    budgetModificationDTO.budgetSelectionInfoId
                            )%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:30%">
                            <b><%=isLangEn? "Budget Operation Code" : "বাজেট অপারেশন কোড"%></b>
                        </td>
                        <td>
                            <%=Budget_operationRepository.getInstance().getTextById(
                                    Language,
                                    budgetModificationDTO.budgetOperationId
                            )%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:30%">
                            <b><%=isLangEn? "Office" : "দপ্তর"%></b>
                        </td>
                        <td>
                            <%=Budget_officeRepository.getInstance().getText(
                                    budgetModificationDTO.budgetOfficeId,
                                    true,
                                    Language
                            )%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:30%">
                            <b><%=isLangEn? "Modification Type" : "পরিবর্ধনের ধরণ"%></b>
                        </td>
                        <td>
                            <%=budgetModificationDTO.modificationType.getName(isLangEn)%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:30%">
                            <b><%=isLangEn? "Economic Code" : "অর্থনৈতিক কোড"%></b>
                        </td>
                        <td>
                            <%=Economic_sub_codeRepository.getInstance().getText(Language, budgetModificationDTO.economicSubCodeId)%>
                        </td>
                    </tr>

                    <%if(budgetModificationDTO.modificationType == Budget_modificationType.BUDGET_AMOUNT_CHANGE){%>
                    <tr>
                        <td style="width:30%">
                            <b><%=isLangEn? "Before Modification Amount" : "পরিবর্ধনের আগের পরিমাণ"%></b>
                        </td>
                        <td>
                            <%=BudgetUtils.getFormattedAmount(Language, budgetModificationDTO.oldAmount)%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:30%">
                            <b><%=isLangEn? "After Modification Amount" : "পরিবর্ধনের পরের পরিমাণ"%></b>
                        </td>
                        <td>
                            <%=BudgetUtils.getFormattedAmount(Language, budgetModificationDTO.newAmount)%>
                        </td>
                    </tr>
                    <%}%>


                    <tr>
                        <td style="width:30%">
                            <b><%=isLangEn? "Documents" : "নথিপত্র"%></b>
                        </td>
                        <td>
                            <%
                                List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(budgetModificationDTO.fileDropzone);
                            %>
                            <table>
                                <tr>
                                    <%
                                        if (FilesDTOList != null) {
                                            for (FilesDTO filesDTO : FilesDTOList) {
                                                byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                    %>
                                    <td>
                                        <%if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {%>
                                        <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                             style='width:100px'/>
                                        <%}%>
                                        <a href='Budget_modificationServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                           download>
                                            <%=filesDTO.fileTitle%>
                                        </a>
                                    </td>
                                    <%
                                            }
                                        }
                                    %>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>