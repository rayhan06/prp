<%@page pageEncoding="UTF-8" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoRepository" %>
<%@ page import="dbm.DBMW" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="budget_modification.BudgetAmountModificationModel" %>

<%
    String context = request.getContextPath() + "/";
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String formTitle = isLanguageEnglish ? "Budget Modification (Change Amount)" : "বাজেট পরিবর্ধন (পরিমাণ পরিবর্তন)";

    String servletName = "Budget_modificationServlet";
    String actionName = "add";
    int i = 0;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal" id="add-budget-code-form">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <%--form data goes here--%>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="budgetSelectionInfoId">
                                            <%=LM.getText(LC.CODE_SELECTION_SEARCH_ECONOMIC_YEAR, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control'
                                                    name='budgetSelectionInfoId'
                                                    id='budgetSelectionInfoId'
                                                    onSelect='setSearchChanged()' style="width: 100%">
                                                <%=BudgetSelectionInfoRepository.getInstance().buildEconomicYears(Language, null)%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="budgetInstitutionalGroup">
                                            <%=LM.getText(LC.BUDGET_INSTITUTIONAL_GROUP, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select id="budgetInstitutionalGroup"
                                                    name="budgetInstitutionalGroupId"
                                                    class='form-control rounded shadow-sm w-100'
                                                    onchange="institutionalGroupChanged(this);"
                                            >
                                                <%=Budget_institutional_groupRepository.getInstance().buildOptions(Language, 0L, false)%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="budgetCat">
                                            <%=LM.getText(LC.BUDGET_BUDGET_TYPE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select id="budgetCat"
                                                    name="budgetCat"
                                                    class='form-control rounded shadow-sm w-100'
                                                    onchange="budgetCatChanged(this);"
                                            >
                                                <%--Dynamically Added with AJAX--%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="budgetOffice">
                                            <%=LM.getText(LC.BUDGET_OFFICE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select id="budgetOffice"
                                                    name="budgetOfficeId"
                                                    class='form-control rounded shadow-sm w-100'
                                                    onchange="budgetOfficeChanged(this);">
                                                <%--Dynamically Added with AJAX--%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="budgetMappingId">
                                            <%=LM.getText(LC.BUDGET_OPERATION_CODE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select id="budgetMappingId"
                                                    name="budgetMappingId"
                                                    class='form-control rounded shadow-sm w-100'
                                                    onchange="budgetMappingChanged(this);"
                                            >
                                                <%--Dynamically Added with AJAX--%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="economicGroupId">
                                            <%=isLanguageEnglish ? "Economic Group" : "অর্থনৈতিক গ্রুপ"%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control'
                                                    name="economicGroupId"
                                                    id='economicGroupId'
                                                    onchange="economicGroupChanged(this);">
                                                <%--Dynamically Added with AJAX--%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="economicSubCodeId">
                                            <%=isLanguageEnglish ? "Economic Code (4 Digits)" : "অর্থনৈতিক কোড (৪ অংকের)"%>
                                        </label>
                                        <div class="col-md-8">
                                            <select id="economicCodeId"
                                                    name="economicCodeId"
                                                    class='form-control rounded shadow-sm w-100'
                                                    onchange="economicCodeChanged(this);"
                                            >
                                                <%--Dynamically Added with AJAX--%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="economicSubCodeId">
                                            <%=isLanguageEnglish ? "Economic Code (7 Digits)" : "অর্থনৈতিক কোড (৭ অংকের)"%>
                                        </label>
                                        <div class="col-md-8">
                                            <select id="economicSubCodeId"
                                                    name="economicSubCodeId"
                                                    class='form-control rounded shadow-sm w-100'
                                                    onchange="economicSubCodeChanged(this);"
                                            >
                                                <%--Dynamically Added with AJAX--%>
                                            </select>
                                        </div>
                                    </div>

                                    <input type="hidden" id="budgetDtoId" name="budgetDtoId">

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="finalAmount">
                                            <%=isLanguageEnglish ? "Allocation Amount in Budget" : "বাজেটে বরাদ্দের পরিমাণ"%>
                                        </label>
                                        <div class="col-md-8">
                                            <input class="form-control" type="text" disabled
                                                   data-validate-budget-amount
                                                   id="finalAmount" name="finalAmount" value="0"
                                            >
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="revisedFinalAmount">
                                            <%=isLanguageEnglish ? "Allocation Amount in Revised Budget" : "সংশোধিত বাজেটে বরাদ্দের পরিমাণ"%>
                                        </label>
                                        <div class="col-md-8">
                                            <input class="form-control" type="text" disabled
                                                   data-validate-budget-amount
                                                   id="revisedFinalAmount" name="revisedFinalAmount" value="0"
                                            >
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="initialExpenditureAmount">
                                            <%=isLanguageEnglish ? "Amount Already Spent" : "ইতোমধ্যে ব্যয়ের পরিমাণ"%>
                                        </label>
                                        <div class="col-md-8">
                                            <input class="form-control" type="text" disabled
                                                   data-validate-budget-amount
                                                   id="initialExpenditureAmount" name="initialExpenditureAmount"
                                                   value="0"
                                            >
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=isLanguageEnglish ? "Documents" : "নথিপত্র"%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                String fileColumnName = "fileDropzone";
                                                long ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                            %>
                                            <div class="dropzone"
                                                 action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=ColumnID%>">
                                                <input type='file' style="display:none" name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value='' tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=ColumnID%>'/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button type="button" id="cancel-btn"
                                    class="btn-sm shadow text-white border-0 cancel-btn"
                                    onclick="location.href = '<%=request.getHeader("referer")%>'">
                                <%=isLanguageEnglish ? "Cancel" : "বাতিল করুন"%>
                            </button>
                            <button type="button" id="submit-btn"
                                    class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    onclick="submitOtBillSubmissionConfigForm()">
                                <%=isLanguageEnglish ? "Submit" : "জমাদিন"%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script>
    const form = $('#add-budget-code-form');

    const budgetSelectionInfoId = document.getElementById('budgetSelectionInfoId');
    const budgetInstitutionalGroup = document.getElementById('budgetInstitutionalGroup');
    const budgetCat = document.getElementById('budgetCat');
    const budgetOffice = document.getElementById('budgetOffice');
    const budgetMappingId = document.getElementById('budgetMappingId');
    const economicGroupId = document.getElementById('economicGroupId');
    const economicCodeId = document.getElementById('economicCodeId');
    const economicSubCodeId = document.getElementById('economicSubCodeId');

    function validateFloatingPointInput(event) {
        const maxValue = <%=Integer.MAX_VALUE%>;
        return true === inputValidationForFloatValue(event, $(this), 3, maxValue);
    }

    $(document).ready(function () {
        document.querySelectorAll('input[data-validate-budget-amount]')
                .forEach(inputElement => inputElement.onkeydown = validateFloatingPointInput);
    })

    function clearSelects(startIndex) {
        const selectElements = [budgetCat, budgetOffice, budgetMappingId, economicGroupId, economicCodeId, economicSubCodeId];
        for (let i = startIndex; i < selectElements.length; i++) {
            selectElements[i].innerHTML = '';
        }
        const emptyBudgetAmountModificationModel = JSON.parse(
            '<%=new Gson().toJson(BudgetAmountModificationModel.getEmptyInstance())%>'
        );
        for (const key in emptyBudgetAmountModificationModel) {
            const htmlElement = document.getElementById(key);
            if (htmlElement) {
                htmlElement.value = emptyBudgetAmountModificationModel[key];
                htmlElement.disabled = true;
            }
        }
    }

    async function institutionalGroupChanged(selectElement) {
        const selectedInstitutionalGroupId = selectElement.value;
        clearSelects(0);
        if (selectedInstitutionalGroupId === '') return;

        const url = 'Budget_mappingServlet?actionType=getBudgetCatList&budget_instituitional_group_id='
                    + selectedInstitutionalGroupId;
        const response = await fetch(url);
        budgetCat.innerHTML = await response.text();
    }

    async function budgetCatChanged(selectElement) {
        const selectedBudgetCat = selectElement.value;
        clearSelects(1);
        if (selectedBudgetCat === '') return;

        const selectedInstitutionalGroup = budgetInstitutionalGroup.value;
        const url = 'Budget_mappingServlet?actionType=getBudgetOfficeList&&withCode=true&budget_instituitional_group_id='
                    + selectedInstitutionalGroup + '&budget_cat=' + selectedBudgetCat;
        const response = await fetch(url);
        budgetOffice.innerHTML = await response.text();
    }

    async function budgetOfficeChanged(selectElement) {
        const selectedBudgetOffice = selectElement.value;
        clearSelects(2);
        if (selectedBudgetOffice === '') return;

        const selectedInstitutionalGroup = budgetInstitutionalGroup.value;
        const selectedBudgetCat = budgetCat.value;

        const url = 'Budget_mappingServlet?actionType=getOperationCodeList&withCode=true&budget_instituitional_group_id='
                    + selectedInstitutionalGroup + '&budget_cat=' + selectedBudgetCat
                    + '&budget_office_id=' + selectedBudgetOffice;
        const response = await fetch(url);
        budgetMappingId.innerHTML = await response.text();
    }

    async function budgetMappingChanged(selectElement) {
        const selectedBudgetMappingId = selectElement.value;
        clearSelects(3);
        if (selectedBudgetMappingId === '') return;

        const url = 'BudgetServlet?actionType=ajax_buildEconomicGroupsWithBudgetMapping'
                    + '&budgetSelectionInfoId=' + budgetSelectionInfoId.value
                    + '&budgetMappingId=' + selectedBudgetMappingId;
        const data = await fetch(url);
        economicGroupId.innerHTML = await data.text();
    }

    async function economicGroupChanged(selectElement) {
        const selectedEconomicGroupId = selectElement.value;
        clearSelects(4);
        if (selectedEconomicGroupId === '') return;

        const url = 'BudgetServlet?actionType=ajax_buildEconomicCodesWithBudgetMapping'
                    + '&budgetSelectionInfoId=' + budgetSelectionInfoId.value
                    + '&budgetMappingId=' + budgetMappingId.value
                    + '&economicGroupId=' + selectedEconomicGroupId;
        const data = await fetch(url);
        economicCodeId.innerHTML = await data.text();
    }

    async function economicCodeChanged(selectElement) {
        const selectedEconomicCodeId = selectElement.value;
        clearSelects(5);
        if (selectedEconomicCodeId === '') return;

        const url = 'BudgetServlet?actionType=ajax_buildEconomicSubCodesWithBudgetMapping'
                    + '&budgetSelectionInfoId=' + budgetSelectionInfoId.value
                    + '&budgetMappingId=' + budgetMappingId.value
                    + '&economicCodeId=' + selectedEconomicCodeId;
        const response = await fetch(url);
        economicSubCodeId.innerHTML = await response.text();
    }

    function setBudgetAmountModificationModel(budgetAmountModificationModel) {
        console.log({budgetAmountModificationModel});
        for (const key in budgetAmountModificationModel) {
            const htmlElement = document.getElementById(key);
            if (htmlElement) {
                htmlElement.disabled = false;
                htmlElement.value = budgetAmountModificationModel[key];
            }
        }
    }

    async function economicSubCodeChanged(selectElement) {
        const selectedEconomicSubCodeId = selectElement.value;
        clearSelects(6);
        if (selectedEconomicSubCodeId === '') return;

        const url = 'Budget_modificationServlet?actionType=ajax_getBudgetAmountModificationModel'
                    + '&budgetSelectionInfoId=' + budgetSelectionInfoId.value
                    + '&budgetMappingId=' + budgetMappingId.value
                    + '&economicSubCodeId=' + selectedEconomicSubCodeId;
        let data;
        try {
            const response = await fetch(url);
            data = await response.json();
        } catch (error) {
            console.log(error);
        }
        if (!data.success) {
            $('#toast_message').css('background-color', '#ff6063');
            showToastSticky('কোন তথ্য পাওয়া যায়নি', 'No information found');
            return;
        }
        setBudgetAmountModificationModel(data.data);
    }


    function submitOtBillSubmissionConfigForm() {
        submitAjaxByData(form.serialize(), "Budget_modificationServlet?actionType=ajax_changeAmount");
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }
</script>