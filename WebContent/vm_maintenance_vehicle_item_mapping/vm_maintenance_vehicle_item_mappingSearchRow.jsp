<%@page pageEncoding="UTF-8" %>

<%@page import="vm_maintenance_vehicle_item_mapping.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.VM_MAINTENANCE_VEHICLE_ITEM_MAPPING_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_VM_MAINTENANCE_VEHICLE_ITEM_MAPPING;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Vm_maintenance_vehicle_item_mappingDTO vm_maintenance_vehicle_item_mappingDTO = (Vm_maintenance_vehicle_item_mappingDTO)request.getAttribute("vm_maintenance_vehicle_item_mappingDTO");
CommonDTO commonDTO = vm_maintenance_vehicle_item_mappingDTO;
String servletName = "Vm_maintenance_vehicle_item_mappingServlet";


System.out.println("vm_maintenance_vehicle_item_mappingDTO = " + vm_maintenance_vehicle_item_mappingDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Vm_maintenance_vehicle_item_mappingDAO vm_maintenance_vehicle_item_mappingDAO = (Vm_maintenance_vehicle_item_mappingDAO)request.getAttribute("vm_maintenance_vehicle_item_mappingDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
		
											<td id = '<%=i%>_vmId'>
											<%
											value = vm_maintenance_vehicle_item_mappingDTO.vmId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_partsId'>
											<%
											value = vm_maintenance_vehicle_item_mappingDTO.partsId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_date'>
											<%
											value = vm_maintenance_vehicle_item_mappingDTO.date + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
		
		
		
	

											<td>
												<button type="button" class="btn btn-sm border-0 shadow" style="background-color: #22ccc1; color: white; border-radius: 8px"
											            onclick="location.href='Vm_maintenance_vehicle_item_mappingServlet?actionType=view&ID=<%=vm_maintenance_vehicle_item_mappingDTO.iD%>'">
											        <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
											    </button>												
											</td>
	
											<td id = '<%=i%>_Edit'>																																	
	
												<button type="button" class="btn btn-sm border-0 shadow" style="background-color: #cc22c1; color: white; border-radius: 8px"
											            onclick="location.href='Vm_maintenance_vehicle_item_mappingServlet?actionType=getEditPage&ID=<%=vm_maintenance_vehicle_item_mappingDTO.iD%>'">
											        <%=LM.getText(LC.VM_MAINTENANCE_VEHICLE_ITEM_MAPPING_SEARCH_VM_MAINTENANCE_VEHICLE_ITEM_MAPPING_EDIT_BUTTON, loginDTO)%>
											    </button>
																				
											</td>											
											
											
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=vm_maintenance_vehicle_item_mappingDTO.iD%>'/></span>
												</div>
											</td>
																						
											

