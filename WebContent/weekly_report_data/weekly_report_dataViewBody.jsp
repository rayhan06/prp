

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="weekly_report_data.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="weekly_report_data.*"%>
<%@page import="report_heading.*"%>
<%@page import="report_sub_heading_topics.*"%>



<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.WEEKLY_REPORT_DATA_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
Weekly_report_dataDAO weekly_report_dataDAO = new Weekly_report_dataDAO("weekly_report_data");
Weekly_report_dataDTO weekly_report_dataDTO = (Weekly_report_dataDTO)weekly_report_dataDAO.getDTOByID(id);
Weekly_report_dataDTO weekly_report_dataDTOLastWeek = weekly_report_dataDAO.getlastWeekDTO(weekly_report_dataDTO);

if(weekly_report_dataDTOLastWeek == null)
{
	weekly_report_dataDTOLastWeek = new Weekly_report_dataDTO();
}
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

Report_headingDAO report_headingDAO = new Report_headingDAO();
ReportSubHeadingDAO reportSubHeadingDAO = new ReportSubHeadingDAO();
Report_sub_heading_topicsDAO report_sub_heading_topicsDAO = new Report_sub_heading_topicsDAO();
TopicDAO topicDAO = new TopicDAO();
WeeklyReportTopicDAO weeklyReportTopicDAO = new WeeklyReportTopicDAO();
DecimalFormat f = new DecimalFormat("##.00");
String context = "../../.."  + request.getContextPath() + "/";
List<Integer> headings = new ArrayList<Integer>();
List<Integer> subHeadings = new ArrayList<Integer>();

Calendar calendar = new GregorianCalendar();
%>


<div class="modal-content viewmodal">
            <div class="modal-header">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9 col-sm-12">
                            <h3 class="modal-title">Weekly Report Data Details</h3>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="modal-body container">
			
			<div class="row div_border office-div">

                    <div class="col-md-12">
                        <h5>Weekly Report Data</h5>
                    </div>
			



			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span>Section</span><span style="float:right;">:</span></b></label>
                        <label id="sectionCat">
						
											<%
											value = weekly_report_dataDTO.sectionCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "section", weekly_report_dataDTO.sectionCat);
											%>											
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span>Year</span><span style="float:right;">:</span></b></label>
                        <label id="yearNumber">
						
											<%
											value = weekly_report_dataDTO.yearNumber + "";
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span>Month</span><span style="float:right;">:</span></b></label>
                        <label id="monthNumber">
						
											<%
											calendar.set(Calendar.MONTH, weekly_report_dataDTO.monthNumber);
											value = new SimpleDateFormat("MMM").format(calendar.getTime());
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span>Week</span><span style="float:right;">:</span></b></label>
                        <label id="weekNumber">
						
											<%
											value = weekly_report_dataDTO.weekNumber + "";
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.WEEKLY_REPORT_DATA_EDIT_WEEKSTARTDATE, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="weekStartDate">
						
											<%
											value = weekly_report_dataDTO.weekStartDate + "";
											%>
											<%
											String formatted_weekStartDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_weekStartDate%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.WEEKLY_REPORT_DATA_EDIT_WEEKENDDATE, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="weekEndDate">
						
											<%
											value = weekly_report_dataDTO.weekEndDate + "";
											%>
											<%
											String formatted_weekEndDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_weekEndDate%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span>Chainage</span><span style="float:right;">:</span></b></label>
                        <label id="fromChainageKm">
						
											<%
											value = "CH " + weekly_report_dataDTO.fromChainageKm + "+" 
													+ weekly_report_dataDTO.fromChainageMeter + ".0 to "
													+ "CH " + weekly_report_dataDTO.toChainageKm + "+" 
													+ weekly_report_dataDTO.toChainageMeter + ".0"
													;
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>
			</div>	

                <div class="row div_border attachement-div">
                    <div class="col-md-12" id="reportdiv">
						<table class="table table-bordered table-striped" id="report">
							<caption><strong>Padma Bridge Rail Link Project</strong>
							<br>Contract No: BR/PBRLP/G2G/Contract/2016
							<br><%=CatDAO.getName(Language, "section", weekly_report_dataDTO.sectionCat)%>
							 <%="(Km " + weekly_report_dataDTO.fromChainageKm + "+" 
										+ weekly_report_dataDTO.fromChainageMeter + " to "
										+ "Km " + weekly_report_dataDTO.toChainageKm + "+" 
										+ weekly_report_dataDTO.toChainageMeter + ")"
										 %>
							</caption>
							<thead>
								<tr style="background-color:#CCC0D9;">
									<th>Item</th>
									<th>Description</th>
									<th>Unit</th>
									<th>Estimated Work Volume</th>
									<th>Total for the week ending: <%=simpleDateFormat.format(new Date(weekly_report_dataDTO.weekStartDate))%> to <%=simpleDateFormat.format(new Date(weekly_report_dataDTO.weekEndDate))%></th>
									<th>Total to previous week up to: <%=simpleDateFormat.format(new Date(weekly_report_dataDTOLastWeek.weekEndDate))%></th>
									<th>Total work up to date: <%=simpleDateFormat.format(new Date(weekly_report_dataDTO.weekEndDate))%></th>
									<th>% of work completed in last week: <%=simpleDateFormat.format(new Date(weekly_report_dataDTO.weekStartDate))%> to <%=simpleDateFormat.format(new Date(weekly_report_dataDTO.weekEndDate))%></th>
									<th>Cumulative Percentage up to date: <%=simpleDateFormat.format(new Date(weekly_report_dataDTOLastWeek.weekEndDate))%></th>
									<th><%=LM.getText(LC.HM_REMARKS, loginDTO )%></th>
								</tr>
							</thead>
							<tbody>
							<%
								int outerCount = 1;
								int colCount = 9;
								int rowCount = 1;
								List<Report_headingDTO> report_headingDTOs = report_headingDAO.getAllReport_heading(true);
								%>
								<tr style="visibility: hidden;"><td colspan="10"></td></tr>
								<%
								for(Report_headingDTO report_headingDTO: report_headingDTOs)
								{
									%>
									<tr tag="heading" style="background-color:#C5E0B3;">
									<td colspan="10"><h3><%=report_headingDTO.nameEn%></h3></td>
									
									</tr>
									<%
									headings.add(rowCount);
									rowCount++;
									List<ReportSubHeadingDTO> reportSubHeadingDTOs = reportSubHeadingDAO.getReportSubHeadingDTOListByReportHeadingID(report_headingDTO.iD);
									for(ReportSubHeadingDTO reportSubHeadingDTO: reportSubHeadingDTOs)
									{
										%>
										<tr tag="subheading"  style="background-color:#FABF8F">
										
										<%
										if(!reportSubHeadingDTO.nameEn.equalsIgnoreCase("default"))
										{
										%>
										<td colspan="10"><h4><%=reportSubHeadingDTO.nameEn%></h4></td>
										<%
										}
										%>
																				
										</tr>
										<%
										subHeadings.add(rowCount);
										rowCount++;
										Report_sub_heading_topicsDTO report_sub_heading_topicsDTO = (Report_sub_heading_topicsDTO)report_sub_heading_topicsDAO.getDTOByHeadingAndSubHeadingId(report_headingDTO.iD, reportSubHeadingDTO.iD);
										if(report_sub_heading_topicsDTO != null)
										{
											List<TopicDTO> topicDTOs = topicDAO.getTopicDTOListByReportSubHeadingTopicsID(report_sub_heading_topicsDTO.iD);
											if(topicDTOs!= null)
											{
												for(TopicDTO topicDTO : topicDTOs)
												{
													WeeklyReportTopicDTO weeklyReportTopicDTO = null;
													if(actionName.equals("edit"))
													{
														weeklyReportTopicDTO = weeklyReportTopicDAO.getDTOByWeeklyReportDataIDAndTopicId(weekly_report_dataDTO.iD, topicDTO.iD);
													}
													double oldSum = weekly_report_dataDAO.getOldSum(weekly_report_dataDTO, topicDTO.iD);
													double newSum = oldSum + weeklyReportTopicDTO.totalWork;
													double workPercentage = oldSum * 100 / topicDTO.estimatedWorkVolume;
													double cumulativePercentage =  newSum * 100 / topicDTO.estimatedWorkVolume;
													%>
													<tr>
													<td><%=outerCount%></td>
													<td><%=topicDTO.nameEn%></td>
													<td><%=topicDTO.unit%></td>
													<td><%=topicDTO.estimatedWorkVolume%></td>
													<td><%=weeklyReportTopicDTO.totalWork%></td>
													<td><%=oldSum%></td>
													<td><%=newSum%></td>
													<td><%=f.format(workPercentage)%></td>
													<td><%=f.format(cumulativePercentage)%></td>
													<td><%=weeklyReportTopicDTO.remarks%></td>
													</tr>
													<%
													outerCount ++;
													rowCount++;
												}
											}
										}
									}
								}
								%>
								</tbody>									
						</table>
						<p class="lead"><button id="excel" class="btn btn-info">TO EXCEL</button>  <button id="pdf" class="btn btn-danger">TO PDF</button></p>
						
                    </div>                    
                </div>
               


        </div>
        </div>
<a id="dlink"  style="display:none;"></a>


<script src="<%=context%>assets/scripts/tableHTMLExport.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.min.js"></script>
<script src="<%=context%>assets/scripts/jspdf.plugin.autotable.js"></script>
        
        
<script>

var tableToExcelBasic = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,',
        template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64 = function (s) {
            return window.btoa(unescape(encodeURIComponent(s)))
        }, format = function (s, c) {
            return s.replace(/{(\w+)}/g, function (m, p) {
                return c[p];
            })
        }
    return function (table, name, filename) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = {
            worksheet: name || 'Worksheet',
            table: table.innerHTML
        }
   document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();
    }
})()

	
  $('#excel').on('click',function(){
	  tableToExcelBasic('report', 'Monthly Report', 'Report.xls');  
  })
  $('#pdf').on('click',function(){
	  	var doc = new jsPDF('p','pt','A4',true);
	    
	    doc.autoTable
	    (
	    		{ 
	    			html: '#report',
	    			didParseCell: function (data) 
	    			{
						<%
						for(int heading : headings)
						{
						%>
							console.log("heading = " + <%=heading%>);
							if (data.row.index === <%=heading%>) 
		    		        {
		    		            data.cell.styles.fillColor = [197, 224, 179];
	    		        	}
						<%
						}
						%>
	    		        
						<%
						for(int subHeading : subHeadings)
						{
						%>
							console.log("subHeading = " + <%=subHeading%>);
							if (data.row.index === <%=subHeading%>) 
		    		        {
		    		            data.cell.styles.fillColor = [250, 191, 143];
	    		        	}
						<%
						}
						%>
	    		        
    		    	}
		    		
	    		}
	    );


	    doc.save('report.pdf');
	  
	 
  })
  
  
  </script>