
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="weekly_report_data.*"%>
<%@page import="report_heading.*"%>
<%@page import="report_sub_heading_topics.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>


<%
Weekly_report_dataDTO weekly_report_dataDTO;
weekly_report_dataDTO = (Weekly_report_dataDTO)request.getAttribute("weekly_report_dataDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(weekly_report_dataDTO == null)
{
	weekly_report_dataDTO = new Weekly_report_dataDTO();
	
}
System.out.println("weekly_report_dataDTO = " + weekly_report_dataDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.WEEKLY_REPORT_DATA_EDIT_WEEKLY_REPORT_DATA_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.WEEKLY_REPORT_DATA_ADD_WEEKLY_REPORT_DATA_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

boolean isPermanentTable = true;

Report_headingDAO report_headingDAO = new Report_headingDAO();
ReportSubHeadingDAO reportSubHeadingDAO = new ReportSubHeadingDAO();
Report_sub_heading_topicsDAO report_sub_heading_topicsDAO = new Report_sub_heading_topicsDAO();
TopicDAO topicDAO = new TopicDAO();
WeeklyReportTopicDAO weeklyReportTopicDAO = new WeeklyReportTopicDAO();
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="Weekly_report_dataServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
				
				
				




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.WEEKLY_REPORT_DATA_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=weekly_report_dataDTO.iD%>' tag='pb_html'/>
	
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.WEEKLY_REPORT_DATA_EDIT_SECTIONCAT, loginDTO)):(LM.getText(LC.WEEKLY_REPORT_DATA_ADD_SECTIONCAT, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'sectionCat_div_<%=i%>'>	
		<select class='form-control'  name='sectionCat' id = 'sectionCat_category_<%=i%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "section", weekly_report_dataDTO.sectionCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "section", -1);			
}
%>
<%=Options%>
		</select>
		
	</div>
</div>			
				
			
<input type='hidden' class='form-control'  name='yearNumber' id = 'yearNumber_number_<%=i%>' min='1900' max='2018' value=<%=actionName.equals("edit")?("'" + weekly_report_dataDTO.yearNumber + "'"):("'" + 1900 + "'")%>  tag='pb_html'>				
	
<input type='hidden' class='form-control'  name='monthNumber' id = 'monthNumber_number_<%=i%>' min='0' max='1000000' value=<%=actionName.equals("edit")?("'" + weekly_report_dataDTO.monthNumber + "'"):("'" + 0 + "'")%>  tag='pb_html'>		
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.WEEKLY_REPORT_DATA_EDIT_WEEKNUMBER, loginDTO)):(LM.getText(LC.WEEKLY_REPORT_DATA_ADD_WEEKNUMBER, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'weekNumber_div_<%=i%>'>	
		<input type='number' required class='form-control'  name='weekNumber' id = 'weekNumber_number_<%=i%>' min='0' max='1000000' value=<%=actionName.equals("edit")?("'" + weekly_report_dataDTO.weekNumber + "'"):("'" + 0 + "'")%>  tag='pb_html'>
						
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.WEEKLY_REPORT_DATA_EDIT_WEEKSTARTDATE, loginDTO)):(LM.getText(LC.WEEKLY_REPORT_DATA_ADD_WEEKSTARTDATE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'weekStartDate_div_<%=i%>'>	
		<input type='date' required class='form-control'  name='weekStartDate_Date_<%=i%>' id = 'weekStartDate_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_weekStartDate = new SimpleDateFormat("yyyy-MM-dd");
	String formatted_weekStartDate = format_weekStartDate.format(new Date(weekly_report_dataDTO.weekStartDate));
	%>
	'<%=formatted_weekStartDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
		<input type='hidden' class='form-control'  name='weekStartDate' id = 'weekStartDate_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + weekly_report_dataDTO.weekStartDate + "'"):("'" + "0" + "'")%>  tag='pb_html'>
		
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.WEEKLY_REPORT_DATA_EDIT_WEEKENDDATE, loginDTO)):(LM.getText(LC.WEEKLY_REPORT_DATA_ADD_WEEKENDDATE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'weekEndDate_div_<%=i%>'>	
		<input type='date' required class='form-control'  name='weekEndDate_Date_<%=i%>' id = 'weekEndDate_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_weekEndDate = new SimpleDateFormat("yyyy-MM-dd");
	String formatted_weekEndDate = format_weekEndDate.format(new Date(weekly_report_dataDTO.weekEndDate));
	%>
	'<%=formatted_weekEndDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
		<input type='hidden' class='form-control'  name='weekEndDate' id = 'weekEndDate_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + weekly_report_dataDTO.weekEndDate + "'"):("'" + "0" + "'")%>  tag='pb_html'>
		
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.WEEKLY_REPORT_DATA_EDIT_FROMCHAINAGEKM, loginDTO)):(LM.getText(LC.WEEKLY_REPORT_DATA_ADD_FROMCHAINAGEKM, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'fromChainageKm_div_<%=i%>'>	
		<input type='text' required class='form-control'  name='fromChainageKm' id = 'fromChainageKm_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + weekly_report_dataDTO.fromChainageKm + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.WEEKLY_REPORT_DATA_EDIT_FROMCHAINAGEMETER, loginDTO)):(LM.getText(LC.WEEKLY_REPORT_DATA_ADD_FROMCHAINAGEMETER, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'fromChainageMeter_div_<%=i%>'>	
		<input type='text' required class='form-control'  name='fromChainageMeter' id = 'fromChainageMeter_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + weekly_report_dataDTO.fromChainageMeter + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.WEEKLY_REPORT_DATA_EDIT_TOCHAINAGEKM, loginDTO)):(LM.getText(LC.WEEKLY_REPORT_DATA_ADD_TOCHAINAGEKM, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'toChainageKm_div_<%=i%>'>	
		<input type='text' required class='form-control'  name='toChainageKm' id = 'toChainageKm_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + weekly_report_dataDTO.toChainageKm + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.WEEKLY_REPORT_DATA_EDIT_TOCHAINAGEMETER, loginDTO)):(LM.getText(LC.WEEKLY_REPORT_DATA_ADD_TOCHAINAGEMETER, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'toChainageMeter_div_<%=i%>'>	
		<input type='text' required class='form-control'  name='toChainageMeter' id = 'toChainageMeter_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + weekly_report_dataDTO.toChainageMeter + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + weekly_report_dataDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + weekly_report_dataDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
					
	





				<div class="col-md-12" style="padding-top: 20px;">
					<legend class="text-left content_legend"><%=LM.getText(LC.WEEKLY_REPORT_DATA_ADD_WEEKLY_REPORT_TOPIC, loginDTO)%></legend>
				</div>

				<div class="col-md-12">

					<div class="table-responsive">
						<table class="table table-bordered table-striped">
								<thead>
									<tr style="background-color:#CCC0D9;">
										<th>Item</th>
										<th>Description</th>
										<th>Unit</th>
										<th>Estimated Work Volume</th>
										<th><%=(actionName.equals("edit"))?(LM.getText(LC.WEEKLY_REPORT_DATA_EDIT_WEEKLY_REPORT_TOPIC_TOTALWORK, loginDTO)):(LM.getText(LC.WEEKLY_REPORT_DATA_ADD_WEEKLY_REPORT_TOPIC_TOTALWORK, loginDTO))%></th>
										<th><%=(actionName.equals("edit"))?(LM.getText(LC.WEEKLY_REPORT_DATA_EDIT_WEEKLY_REPORT_TOPIC_REMARKS, loginDTO)):(LM.getText(LC.WEEKLY_REPORT_DATA_ADD_WEEKLY_REPORT_TOPIC_REMARKS, loginDTO))%></th>
										
									</tr>
								</thead>
								<tbody id="field-WeeklyReportTopic">
								<%
								int outerCount = 1;
								List<Report_headingDTO> report_headingDTOs = report_headingDAO.getAllReport_heading(true);
								for(Report_headingDTO report_headingDTO: report_headingDTOs)
								{
									%>
									<tr style="background-color:#C5E0B3;">
									<td colspan="6"><h3><%=report_headingDTO.nameEn%></h3></td>
									
									</tr>
									<%
									List<ReportSubHeadingDTO> reportSubHeadingDTOs = reportSubHeadingDAO.getReportSubHeadingDTOListByReportHeadingID(report_headingDTO.iD);
									for(ReportSubHeadingDTO reportSubHeadingDTO: reportSubHeadingDTOs)
									{
										%>
										<tr style="background-color:#FABF8F">
										<%
										if(!reportSubHeadingDTO.nameEn.equalsIgnoreCase("default"))
										{
										%>
										<td colspan="6"><h4><%=reportSubHeadingDTO.nameEn%></h4></td>
										<%
										}
										%>						
										</tr>
										<%
										Report_sub_heading_topicsDTO report_sub_heading_topicsDTO = (Report_sub_heading_topicsDTO)report_sub_heading_topicsDAO.getDTOByHeadingAndSubHeadingId(report_headingDTO.iD, reportSubHeadingDTO.iD);
										if(report_sub_heading_topicsDTO != null)
										{
											List<TopicDTO> topicDTOs = topicDAO.getTopicDTOListByReportSubHeadingTopicsID(report_sub_heading_topicsDTO.iD);
											if(topicDTOs!= null)
											{
												for(TopicDTO topicDTO : topicDTOs)
												{
													WeeklyReportTopicDTO weeklyReportTopicDTO = null;
													if(actionName.equals("edit"))
													{
														weeklyReportTopicDTO = weeklyReportTopicDAO.getDTOByWeeklyReportDataIDAndTopicId(weekly_report_dataDTO.iD, topicDTO.iD);
													}
													%>
													<tr>
													<td><%=outerCount%></td>
													<td><%=topicDTO.nameEn%></td>
													<td><%=topicDTO.unit%></td>
													<td><%=topicDTO.estimatedWorkVolume%></td>
													<td>
													 	<input type='number' class='form-control'  name='weeklyReportTopic.totalWork' value='<%=actionName.equals("edit")? weeklyReportTopicDTO.totalWork : 0.0%>'/>
													 	<input type='hidden' class='form-control'  name='weeklyReportTopic.topicType' value='<%=topicDTO.iD%>' />
													 	<input type='hidden' class='form-control'  name='weeklyReportTopic.iD' value='<%=actionName.equals("edit")? weeklyReportTopicDTO.iD: topicDTO.iD%>' />
													 	<input type='hidden' class='form-control'  name='weeklyReportTopic.weeklyReportDataId' value='<%=actionName.equals("edit")? weeklyReportTopicDTO.weeklyReportDataId: -1%>' />
													 </td>
													<td><input type='text' class='form-control'  name='weeklyReportTopic.remarks' />	</td>
													</tr>
													<%
													outerCount ++;
												}
											}
										}
									}
								}
								%>																		
								</tbody>
							</table>
						
						
						
					</div>

					
				</div>		

				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						%>
						<%=LM.getText(LC.WEEKLY_REPORT_DATA_EDIT_WEEKLY_REPORT_DATA_CANCEL_BUTTON, loginDTO)%>
						<%
					}
					else
					{
						%>
						<%=LM.getText(LC.WEEKLY_REPORT_DATA_ADD_WEEKLY_REPORT_DATA_CANCEL_BUTTON, loginDTO)%>
						<%
					}
					
					%>
					</a>
					<button class="btn btn-success" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						%>
						<%=LM.getText(LC.WEEKLY_REPORT_DATA_EDIT_WEEKLY_REPORT_DATA_SUBMIT_BUTTON, loginDTO)%>
						<%
					}
					else
					{
						%>
						<%=LM.getText(LC.WEEKLY_REPORT_DATA_ADD_WEEKLY_REPORT_DATA_SUBMIT_BUTTON, loginDTO)%>
						<%
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">




function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}

	preprocessDateBeforeSubmitting('weekStartDate', row);	
	preprocessDateBeforeSubmitting('weekEndDate', row);	

	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Weekly_report_dataServlet");	
}

function init(row)
{


	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	
}

var row = 0;
	
window.onload =function ()
{
	init(row);
	CKEDITOR.replaceAll();
}

</script>






