<%@page import="language.LC"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="language.LM"%>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator"%>
<%@ page import="java.util.Arrays"%>
<%@ page import="searchform.SearchForm"%>
<%@ page import="pb.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%
	System.out.println("Inside nav.jsp");
	String url = request.getParameter("url");
	String navigator = request.getParameter("navigator");
	String pageName = request.getParameter("pageName");
	if (pageName == null)
		pageName = "Search";
	String pageno = "";
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
	pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

	System.out.println("rn " + rn);

	String action = url;
	String context = "../../.." + request.getContextPath() + "/";
	String link = context + url;
	String concat = "?";
	if (url.contains("?")) {
		concat = "&";
	}
	String[][] searchFieldInfo = rn.getSearchFieldInfo();
	String totalPage = "1";
	if (rn != null)
		totalPage = rn.getTotalPages() + "";
	int row = 0;

	String Language = LM.getText(LC.WEEKLY_REPORT_DATA_EDIT_LANGUAGE, loginDTO);
	String Options;
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	Date date = new Date();
	String datestr = dateFormat.format(date);
	int pagination_number = 0;
	boolean isPermanentTable = rn.m_isPermanentTable;
	System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
%>






<!-- search control -->
<div class="portlet box portlet-btcl">
	<div class="portlet-title">
		<div class="caption" style="margin-top: 5px;"><i class="fa fa-search-plus"  style="margin-top:-3px"></i><%=pageName%></div>
		<p class="desktop-only" style="float:right; margin:10px 5px !important;">Advanced Search</p>
		<div class="tools">
			<a class="expand" href="javascript:" data-original-title="" title=""></a>
		</div>
		
		<div class="col-xs-12 col-sm-5 col-md-4" style="margin-top:10px">
		<%
			
			out.println("<input type='text' class='form-control' onKeyUp='allfield_changed(\"\",0)' id='anyfield'  name='"+  LM.getText(LC.WEEKLY_REPORT_DATA_SEARCH_ANYFIELD, loginDTO) +"' ");
			String value = (String)session.getAttribute(searchFieldInfo[searchFieldInfo.length - 1][1]);
			
			if( value != null)
			{
				out.println("value = '" + value + "'");
			}
			
			out.println ("/><br />");
		%> 
		</div>
		
		
		
	</div>
	<div 
	class="portlet-body form collapse"
	>
		<!-- BEGIN FORM-->
		<div class="container-fluid">
			<div class="row col-lg-offset-1">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right">Section</label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<select class='form-control'  name='section_cat' id = 'section_cat' onSelect='setSearchChanged()'>
<%
			
			Options = CatDAO.getOptions(Language, "section", -2);
			out.print(Options);
%>
</select>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right">Year</label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<select class="form-control" id="year_number" name="year_number" onSelect='setSearchChanged()'>
						<option value =''>---Select---</option>
						<%
							for(int j = 2016; j < 2030; j ++)
							{
								%>
								<option value ='<%=j%>'><%=j%></option>
								<%
							}
						%>
						</select>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right">Month</label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<select class="form-control" id="month_number" name="month_number" onSelect='setSearchChanged()'>
							<option value =''>---Select---</option>
							<%
							Calendar calendar = new GregorianCalendar();
							for(int j = 0; j < 12; j ++)
							{
								calendar.set(Calendar.MONTH, j);
								value = new SimpleDateFormat("MMM").format(calendar.getTime());
								%>
								<option value ='<%=j%>'><%=value%></option>
								<%
							}
							%>
						</select>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right">Week</label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<select  class="form-control" id="week_number"  name="week_number" onSelect='setSearchChanged()'>
						<option value =''>---Select---</option>
						<%
							for(int j = 0; j < 5; j ++)
							{
								%>
								<option value ='<%=j%>'><%=j%></option>
								<%
							}
						%>
						</select>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.WEEKLY_REPORT_DATA_SEARCH_WEEKSTARTDATE, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="date" class="form-control" id="week_start_date" placeholder="" name="week_start_date" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.WEEKLY_REPORT_DATA_SEARCH_WEEKENDDATE, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="date" class="form-control" id="week_end_date" placeholder="" name="week_end_date" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.WEEKLY_REPORT_DATA_SEARCH_FROMCHAINAGEKM, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="number" class="form-control" id="from_chainage_km" placeholder="" name="from_chainage_km" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.WEEKLY_REPORT_DATA_SEARCH_FROMCHAINAGEMETER, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="number" class="form-control" id="from_chainage_meter" placeholder="" name="from_chainage_meter" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.WEEKLY_REPORT_DATA_SEARCH_TOCHAINAGEKM, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="number" class="form-control" id="to_chainage_km" placeholder="" name="to_chainage_km" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.WEEKLY_REPORT_DATA_SEARCH_TOCHAINAGEMETER, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="number" class="form-control" id="to_chainage_meter" placeholder="" name="to_chainage_meter" onChange='setSearchChanged()'>
					</div>
				</div>

			</div>	


			<div class=clearfix></div>

			<div class="form-actions fluid" style="margin-top:10px">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-offset-3 col-xs-12 col-md-12  col-md-12 col-lg-9">							
							<div class="col-xs-4  col-sm-4  col-md-6">
								<input type="hidden" name="search" value="yes" />
								<!-- 				          	<input type="reset" class="btn  btn-sm btn btn-circle  grey-mint btn-outline sbold uppercase" value="Reset" > -->
								<input type="submit" onclick="allfield_changed('',0)"
									class="btn  btn-sm btn btn-circle btn-sm green-meadow btn-outline sbold uppercase advanceseach"
									value="<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>">
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<!-- END FORM-->
	</div>


<%@include file="../common/pagination_with_go2.jsp"%>


<template id = "loader">
<div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
</div>
</template>


<script type="text/javascript">

	function dosubmit(params)
	{
		document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
		//alert(params);
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) 
		    {
		    	document.getElementById('tableForm').innerHTML = this.responseText ;
				setPageNo();
				searchChanged = 0;
			}
		    else if(this.readyState == 4 && this.status != 200)
			{
				alert('failed ' + this.status);
			}
		  };
		  
		  xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
		  xhttp.send();
		
	}

	function allfield_changed(go, pagination_number)
	{
		var params = 'AnyField=' + document.getElementById('anyfield').value;
		<%
			for(int i = 0; i < searchFieldInfo.length - 1; i ++)
			{
				if(i != 4 && i != 5)
				{
					out.println("params += '&" +  searchFieldInfo[i][1] + "='+document.getElementById('" 
					+ searchFieldInfo[i][1] + "').value");
				}
				else
				{
					out.println("params += '&" +  searchFieldInfo[i][1] + "='+ getFormattedDate('" 
							+ searchFieldInfo[i][1] + "')"); 
				}
			}
		%>
		params +=  '&search=true&ajax=true';

		var pageNo = document.getElementsByName('pageno')[0].value;
		var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

		var totalRecords = 0;
		var lastSearchTime = 0;
		if(document.getElementById('hidden_totalrecords'))
		{
			totalRecords = document.getElementById('hidden_totalrecords').value;
			lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
		}


		if(go !== '' && searchChanged == 0)
		{
			console.log("go found");
			params += '&go=1';
			pageNo = document.getElementsByName('pageno')[pagination_number].value;
			rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
			setPageNoInAllFields(pageNo);
			setRPPInAllFields(rpp);
		}
		params += '&pageno=' + pageNo;
		params += '&RECORDS_PER_PAGE=' + rpp;
		params += '&TotalRecords=' + totalRecords;
		params += '&lastSearchTime=' + lastSearchTime;
		
		console.log(params);
		dosubmit(params);
	
	}

</script>

