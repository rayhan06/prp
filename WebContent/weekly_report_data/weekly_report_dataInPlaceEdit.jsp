<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="weekly_report_data.Weekly_report_dataDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>

<%
Weekly_report_dataDTO weekly_report_dataDTO = (Weekly_report_dataDTO)request.getAttribute("weekly_report_dataDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(weekly_report_dataDTO == null)
{
	weekly_report_dataDTO = new Weekly_report_dataDTO();
	
}
System.out.println("weekly_report_dataDTO = " + weekly_report_dataDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.WEEKLY_REPORT_DATA_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=weekly_report_dataDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_sectionCat'>")%>
			
	
	<div class="form-inline" id = 'sectionCat_div_<%=i%>'>
		<select class='form-control'  name='sectionCat' id = 'sectionCat_category_<%=i%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "section", weekly_report_dataDTO.sectionCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "section", -1);			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_yearNumber'>")%>
			
	
	<div class="form-inline" id = 'yearNumber_div_<%=i%>'>
		<input type='number' class='form-control'  name='yearNumber' id = 'yearNumber_number_<%=i%>' min='1900' max='2018' value=<%=actionName.equals("edit")?("'" + weekly_report_dataDTO.yearNumber + "'"):("'" + 1900 + "'")%>  tag='pb_html'>
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_monthNumber'>")%>
			
	
	<div class="form-inline" id = 'monthNumber_div_<%=i%>'>
		<input type='number' class='form-control'  name='monthNumber' id = 'monthNumber_number_<%=i%>' min='0' max='1000000' value=<%=actionName.equals("edit")?("'" + weekly_report_dataDTO.monthNumber + "'"):("'" + 0 + "'")%>  tag='pb_html'>
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_weekNumber'>")%>
			
	
	<div class="form-inline" id = 'weekNumber_div_<%=i%>'>
		<input type='number' class='form-control'  name='weekNumber' id = 'weekNumber_number_<%=i%>' min='0' max='1000000' value=<%=actionName.equals("edit")?("'" + weekly_report_dataDTO.weekNumber + "'"):("'" + 0 + "'")%>  tag='pb_html'>
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_weekStartDate'>")%>
			
	
	<div class="form-inline" id = 'weekStartDate_div_<%=i%>'>
		<input type='date' class='form-control'  name='weekStartDate_Date_<%=i%>' id = 'weekStartDate_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_weekStartDate = new SimpleDateFormat("dd-MM-yyyy");
	String formatted_weekStartDate = format_weekStartDate.format(new Date(weekly_report_dataDTO.weekStartDate));
	%>
	'<%=formatted_weekStartDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
		<input type='hidden' class='form-control'  name='weekStartDate' id = 'weekStartDate_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + weekly_report_dataDTO.weekStartDate + "'"):("'" + "0" + "'")%>  tag='pb_html'>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_weekEndDate'>")%>
			
	
	<div class="form-inline" id = 'weekEndDate_div_<%=i%>'>
		<input type='date' class='form-control'  name='weekEndDate_Date_<%=i%>' id = 'weekEndDate_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_weekEndDate = new SimpleDateFormat("dd-MM-yyyy");
	String formatted_weekEndDate = format_weekEndDate.format(new Date(weekly_report_dataDTO.weekEndDate));
	%>
	'<%=formatted_weekEndDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
		<input type='hidden' class='form-control'  name='weekEndDate' id = 'weekEndDate_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + weekly_report_dataDTO.weekEndDate + "'"):("'" + "0" + "'")%>  tag='pb_html'>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_fromChainageKm'>")%>
			
	
	<div class="form-inline" id = 'fromChainageKm_div_<%=i%>'>
		<input type='text' class='form-control'  name='fromChainageKm' id = 'fromChainageKm_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + weekly_report_dataDTO.fromChainageKm + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_fromChainageMeter'>")%>
			
	
	<div class="form-inline" id = 'fromChainageMeter_div_<%=i%>'>
		<input type='text' class='form-control'  name='fromChainageMeter' id = 'fromChainageMeter_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + weekly_report_dataDTO.fromChainageMeter + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_toChainageKm'>")%>
			
	
	<div class="form-inline" id = 'toChainageKm_div_<%=i%>'>
		<input type='text' class='form-control'  name='toChainageKm' id = 'toChainageKm_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + weekly_report_dataDTO.toChainageKm + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_toChainageMeter'>")%>
			
	
	<div class="form-inline" id = 'toChainageMeter_div_<%=i%>'>
		<input type='text' class='form-control'  name='toChainageMeter' id = 'toChainageMeter_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + weekly_report_dataDTO.toChainageMeter + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + weekly_report_dataDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=weekly_report_dataDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Weekly_report_dataServlet?actionType=view&ID=<%=weekly_report_dataDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Weekly_report_dataServlet?actionType=view&modal=1&ID=<%=weekly_report_dataDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	