<%@page pageEncoding="UTF-8" %>

<%@page import="weekly_report_data.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.WEEKLY_REPORT_DATA_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_WEEKLY_REPORT_DATA;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Weekly_report_dataDTO weekly_report_dataDTO = (Weekly_report_dataDTO)request.getAttribute("weekly_report_dataDTO");
CommonDTO commonDTO = weekly_report_dataDTO;
String servletName = "Weekly_report_dataServlet";


System.out.println("weekly_report_dataDTO = " + weekly_report_dataDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Weekly_report_dataDAO weekly_report_dataDAO = (Weekly_report_dataDAO)request.getAttribute("weekly_report_dataDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

Calendar calendar = new GregorianCalendar();

%>

											
		
											
											<td id = '<%=i%>_sectionCat'>
											<%
											value = weekly_report_dataDTO.sectionCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "section", weekly_report_dataDTO.sectionCat);
											%>											
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_yearNumber'>
											<%
											value = weekly_report_dataDTO.yearNumber + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_monthNumber'>
											<%
											calendar.set(Calendar.MONTH, weekly_report_dataDTO.monthNumber);
											value = new SimpleDateFormat("MMM").format(calendar.getTime());
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_weekNumber'>
											<%
											value = weekly_report_dataDTO.weekNumber + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_weekStartDate'>
											<%
											value = weekly_report_dataDTO.weekStartDate + "";
											%>
											<%
											String formatted_weekStartDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_weekStartDate%>
				
			
											</td>
		
											
											<td id = '<%=i%>_weekEndDate'>
											<%
											value = weekly_report_dataDTO.weekEndDate + "";
											%>
											<%
											String formatted_weekEndDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_weekEndDate%>
				
			
											</td>
		
											
											<td id = '<%=i%>_fromChainageKm'>
											<%
											value = "CH " + weekly_report_dataDTO.fromChainageKm + "+" 
											+ weekly_report_dataDTO.fromChainageMeter + ".0 to "
											+ "CH " + weekly_report_dataDTO.toChainageKm + "+" 
											+ weekly_report_dataDTO.toChainageMeter + ".0"
											;
											%>
														
											<%=value%>
				
			
											</td>
		
												
	

											<td>
												<a href='Weekly_report_dataServlet?actionType=view&ID=<%=weekly_report_dataDTO.iD%>'>View</a>																						
											</td>
											
											<td>
												<a href='Weekly_report_dataServlet?actionType=viewMonthlyReport&ID=<%=weekly_report_dataDTO.iD%>'>View</a>																						
											</td>
	
											<td id = '<%=i%>_Edit'>																																	
	
												<a href='Weekly_report_dataServlet?actionType=getEditPage&ID=<%=weekly_report_dataDTO.iD%>'><%=LM.getText(LC.WEEKLY_REPORT_DATA_SEARCH_WEEKLY_REPORT_DATA_EDIT_BUTTON, loginDTO)%></a>
																				
											</td>											
											
											
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span id='chkEdit' ><input type='checkbox' name='ID' value='<%=weekly_report_dataDTO.iD%>'/></span>
												</div>
											</td>
																						
											

