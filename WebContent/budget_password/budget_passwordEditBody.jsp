<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="budget_password.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="budget_office.Budget_officeDTO" %>
<%@ page import="budget.BudgetUtils" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoDTO" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoDAO" %>
<%@ page import="util.StringUtils" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.BUDGET_PASSWORD_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

    String formTitle = LM.getText(LC.BUDGET_PASSWORD_ADD_BUDGET_PASSWORD_ADD_FORMNAME, loginDTO);

    String economicYear = BudgetUtils.getEconomicYearForCodeSelection(System.currentTimeMillis());
    BudgetSelectionInfoDTO budgetSelectionInfoDTO = BudgetSelectionInfoDAO.getInstance().getDTOByEconomicYear(economicYear);

    String errorMessage = null;
    List<Budget_officeDTO> budget_officeDTOS;
    List<String> passwords;
    if (budgetSelectionInfoDTO == null || !budgetSelectionInfoDTO.isSubmitted) {
        errorMessage = "English".equalsIgnoreCase(Language) ?
                "Budget code selection process has not yet been completed for Economic Year ".concat(economicYear)
                : StringUtils.convertToBanNumber(economicYear).concat(" অর্থ বছরের বাজেটের কোড বাছাইকরণ প্রক্রিয়া এখনো সম্পন্ন হয়নি");

        budget_officeDTOS = new ArrayList<>();
        passwords = new ArrayList<>();
    } else {
        budget_officeDTOS = Budget_officeRepository.getInstance().getAllOfficeList();
        passwords = Budget_passwordDAO.getInstance().generatePasswords(budget_officeDTOS.size());
    }
    boolean isLangEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=LM.getText(LC.BUDGET_ECONOMIC_YEAR, loginDTO)%> <%=Utils.getDigits(economicYear, Language)%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="budgetForm" enctype="multipart/form-data"
            action="Budget_passwordServlet?actionType=ajax_add&isPermanentTable=true" method="post">
            <div class="kt-portlet__body form-body">
                <%
                    if (errorMessage != null) {
                %>
                <div class="my-4">
                    <h5 class="text-center" style="color: red">
                        <%=errorMessage%>
                    </h5>
                </div>
                <%
                } else {
                %>
                <div class="row">
                    <div class="col-12">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                        <div class="mt-4">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped text-nowrap">
                                                    <thead>
                                                    <tr>
                                                        <th>
                                                            <%=LM.getText(LC.LAYER_BASE_OFFICE_LIST_OFFICE_NAME, loginDTO)%>
                                                        </th>
                                                        <th colspan="2">
                                                            <%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_EMPLOYEERECORDSID, loginDTO)%>
                                                        </th>
                                                        <th>
                                                            <%=LM.getText(LC.EMPLOYEE_RECORD_PASSWORD, loginDTO)%>
                                                        </th>
                                                        <th>
                                                            <%=LM.getText(LC.BUDGET_PASSWORD_ADD_GENERATE_PASSWORD, loginDTO)%>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <%
                                                        for (int i = 0; i < budget_officeDTOS.size(); i++) {
                                                            Budget_officeDTO dto = budget_officeDTOS.get(i);
                                                    %>

                                                    <tr>
                                                        <td>
                                                            <%=isLangEnglish ? dto.nameEn : dto.nameBn%>
                                                        </td>

                                                        <td colspan="2" id="employee_record_id_modal_button_<%=i%>">
                                                            <button type="button"
                                                                    class="btn btn-primary btn-block shadow btn-border-radius"
                                                                    onclick="modalBtnClicked(<%=i%>);">
                                                                <%=LM.getText(LC.LANGUAGE_SELECT, userDTO)%>
                                                            </button>
                                                        </td>
                                                        <td style="display: none" class="select_employee_<%=i%>">
                                                            <div class="input-group" id="employee_record_id_div_<%=i%>">
                                                                <input type="hidden"
                                                                       name='officeUnitOrganogramId_<%=i%>'
                                                                       id='officeUnitOrganogramId_<%=i%>'>
                                                                <span id="employee_record_id_text_<%=i%>"></span>
                                                            </div>
                                                        </td>
                                                        <td style="display: none" class="select_employee_<%=i%>">
                                                            <button type="button" class="btn btn-danger"
                                                                    onclick="crsBtnClicked(<%=i%>);"
                                                                    id='employee_record_id_crs_btn_<%=i%>'>
                                                                x
                                                            </button>
                                                        </td>

                                                        <td>
                                                            <input type="text" readonly class="form-control"
                                                                   id='password_password_<%=i%>'
                                                                   name='password_<%=i%>' value="<%=passwords.get(i)%>">
                                                        </td>

                                                        <td>
                                                            <button class="btn-sm shadow text-white border-0 text-white btn-border-radius ml-2"
                                                                    id='generate_password_password_<%=i%>'
                                                                    style="background-color: #cc22c1;"
                                                                    onclick="generatePasswordForRow(<%=i%>)"
                                                                    type="button">
                                                                <%=LM.getText(LC.BUDGET_PASSWORD_ADD_GENERATE_PASSWORD, loginDTO)%>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <%}%>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-12">
                        <div class="form-actions text-right">
                            <button id="budget-cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.BUDGET_PASSWORD_ADD_BUDGET_PASSWORD_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                                    id="budget-submit-btn"
                                    onclick="confirmSubmit()">
                                <%=LM.getText(LC.BUDGET_PASSWORD_ADD_BUDGET_PASSWORD_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
                <%
                    }
                %>
            </div>
        </form>
    </div>
</div>

<%
    String modalTitle = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng ? "Find Designation" : "পদবী খুঁজুন";
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
    <jsp:param name="modalTitle" value="<%=modalTitle%>"/>
</jsp:include>

<script type="text/javascript">
    const isLangEng = '<%=HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng%>';
    const officeCount = <%=budget_officeDTOS.size()%>;
    const budgetForm = $('#budgetForm');
    const budgetSubmitBtn = $('#budget-submit-btn');
    const budgetCancelBtn = $('#budget-cancel-btn');

    function generatePasswordForRow(index) {
        $.ajax({
            url: "Budget_passwordServlet?actionType=ajax_regenerateSinglePassword",
            type: "POST",
            async: false,
            dataType: 'JSON',
            success: function (response) {
                if(response.responseCode === 200){
                    $("#password_password_" + index).val(response.msg);
                }else if (response.responseCode === 302){
                    window.location.replace(getContextPath() + response.msg);
                }
            },
            error: function (error) {
                $('#toast_message').css('background-color', '#ff6063');
                showToastSticky(error);
            }
        });
    }

    function crsBtnClicked(rowIndex) {
        $('.select_employee_' + rowIndex).hide();
        $('#employee_record_id_modal_button_' + rowIndex).show();
        $('#officeUnitOrganogramId_' + rowIndex).val('');
        $('#employee_record_id_text_' + rowIndex).text('');
    }

    function viewInfoAndSetInput(empInfo, rowIndex) {
        $('#employee_record_id_modal_button_' + rowIndex).hide();
        $('.select_employee_' + rowIndex).show();
        let employeeView;
        if (isLangEng) {
            employeeView = empInfo.employeeNameEn + '<br><b>' + empInfo.organogramNameEn + '</b><br>' + empInfo.officeUnitNameEn;
        } else {
            employeeView = empInfo.employeeNameBn + '<br><b>' + empInfo.organogramNameBn + '</b><br>' + empInfo.officeUnitNameBn;
        }
        $('#employee_record_id_text_' + rowIndex).html(employeeView);
        $('#officeUnitOrganogramId_' + rowIndex).val(empInfo.organogramId);
    }

    table_name_to_collcetion_map = new Map([
        <% for(int rowIndex = 0;rowIndex < budget_officeDTOS.size();rowIndex++){%>
        ['employeeRecordId_<%=rowIndex%>', {
            isSingleEntry: true,
            callBackFunction: function (empInfo) {
                viewInfoAndSetInput(empInfo, <%=rowIndex%>);
            }
        }],
        <%}%>
    ]);

    function modalBtnClicked(rowIndex) {
        modal_button_dest_table = 'employeeRecordId_' + rowIndex;
        $('#search_emp_modal').modal();
    }

    $(document).ready(function () {
        $("#budget-cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    function confirmSubmit() {
        buttonStateChange(true);
        for (let i = 0; i < officeCount; i++) {
            if (document.getElementById('employee_record_id_div_' + i) == null || !$('#officeUnitOrganogramId_' + i).val()) {
                $('#toast_message').css('background-color', '#ff6063');
                showToastSticky('অনুগ্রহ করে,সকল অফিসের জন্য কর্মকর্তা বাছাই করুন', 'Please select employee for all office');
                buttonStateChange(false);
                return false;
            }
        }
        messageDialog('<%=LM.getText(LC.BUDGET_PASSWORD_ADD_DO_YOU_WANT_TO_SAVE_THE_CHANGES, loginDTO)%>',
            "<%=LM.getText(LC.BUDGET_PASSWORD_ADD_YOU_WONT_BE_ABLE_TO_REVERT_THIS, loginDTO)%>", 'warning', true,
            '<%=LM.getText(LC.REPORT_ADD_REPORT_SUBMIT_BUTTON, loginDTO)%>',
            '<%=LM.getText(LC.REPORT_ADD_REPORT_CANCEL_BUTTON, loginDTO)%>',
            ()=>{
                submitAjaxForm("budgetForm");
            }, () => {
                buttonStateChange(false);
            }
        );
    }

    function buttonStateChange(value) {
        $(':button').prop('disabled', value);
    }
</script>