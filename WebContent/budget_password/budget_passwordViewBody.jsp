<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="budget_password.*" %>
<%@page import="java.util.*" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="budget_selection_info.BudgetSelectionInfoDTO" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="util.StringUtils" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.BUDGET_PASSWORD_EDIT_LANGUAGE, loginDTO);
    boolean isLangEng = "English".equalsIgnoreCase(Language);
    BudgetSelectionInfoDTO budgetSelectionInfoDTO = (BudgetSelectionInfoDTO) request.getAttribute("budgetSelectionInfoDTO");
    List<Budget_passwordDTO> budget_passwordDTOS = Budget_passwordDAO.getInstance().getDTOsByBudgetSelectionId(budgetSelectionInfoDTO.iD);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=LM.getText(LC.BUDGET_ECONOMIC_YEAR, loginDTO)%> <%=Utils.getDigits(budgetSelectionInfoDTO.economicYear, Language)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">
                    <div class="onlyborder">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=LM.getText(LC.BUDGET_PASSWORD_ADD_BUDGET_PASSWORD_ADD_FORMNAME, loginDTO)%></h4>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th><%=LM.getText(LC.LAYER_BASE_OFFICE_LIST_OFFICE_NAME, loginDTO)%></th>
                                                    <th><%=LM.getText(LC.ADD_EMPLOYEE_EMPLOYEE_INFORMATION, loginDTO)%></th>
                                                    <th><%=LM.getText(LC.EMPLOYEE_RECORD_PASSWORD, loginDTO)%></th>
                                                </tr>
                                                </thead>
                                                <%for (Budget_passwordDTO dto : budget_passwordDTOS) {%>
                                                    <tr>
                                                        <td>
                                                            <%=Budget_officeRepository.getInstance().getText(dto.budgetOfficeId, Language)%>
                                                        </td>
                                                        <td>
                                                            <%if(isLangEng){%>
                                                                <%=dto.userName%><br>
                                                                <%=dto.userNameEn%><br>
                                                                <b><%=dto.OfficeUnitOrganogramEng%></b><br>
                                                                <%=dto.OfficeUnitEng%><br>
                                                            <%} else {%>
                                                                <%=StringUtils.convertToBanNumber(dto.userName)%><br>
                                                                <%=dto.userNameBN%><br>
                                                                <b><%=dto.OfficeUnitOrganogramBng%></b><br>
                                                                <%=dto.OfficeUnitBng%><br>
                                                            <%}%>
                                                        </td>
                                                        <td>
                                                            <%=dto.password%>
                                                        </td>
                                                    </tr>
                                                <%}%>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>