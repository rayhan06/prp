<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="budget_password.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoDAO" %>
<%@ page import="budget_office.Budget_officeDAO" %>
<%@ page import="util.StringUtils" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoDTO" %>
<%@ page import="budget_office.Budget_officeDTO" %>
<%@ page import="common.BaseServlet" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String Language = LM.getText(LC.BUDGET_PASSWORD_EDIT_LANGUAGE, loginDTO);
    boolean isLangEng = "English".equalsIgnoreCase(Language);
    String navigator2 = SessionConstants.NAV_BUDGET_PASSWORD;
    RecordNavigator rn2 = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.BUDGET_ECONOMIC_YEAR, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.LAYER_BASE_OFFICE_LIST_OFFICE_NAME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.ADD_EMPLOYEE_EMPLOYEE_INFORMATION, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.GLOBAL_PASSWORD, loginDTO)%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Budget_passwordDTO> data = (List<Budget_passwordDTO>) rn2.list;
            if (data != null && data.size() > 0) {
                List<Long> selectionIds = new ArrayList<>();
                List<Long> officeIds = new ArrayList<>();
                for (Budget_passwordDTO budget_passwordDTO : data) {
                    selectionIds.add(budget_passwordDTO.budgetSelectionId);
                    officeIds.add(budget_passwordDTO.budgetOfficeId);
                }
                Map<Long, BudgetSelectionInfoDTO> mapBudgetSelectionInfoDTO = BudgetSelectionInfoDAO.getInstance().getDTOs(selectionIds)
                        .stream()
                        .collect(Collectors.toMap(e -> e.iD, e -> e, (e1, e2) -> e1));
                Map<Long, Budget_officeDTO> mapBudget_officeDTO = Budget_officeDAO.getInstance().getDTOs(officeIds)
                        .stream()
                        .collect(Collectors.toMap(e -> e.iD, e -> e, (e1, e2) -> e1));
                for (Budget_passwordDTO budget_passwordDTO : data) {
                    BudgetSelectionInfoDTO budgetSelectionInfoDTO = mapBudgetSelectionInfoDTO.get(budget_passwordDTO.budgetSelectionId);
                    Budget_officeDTO budget_officeDTO = mapBudget_officeDTO.get(budget_passwordDTO.budgetOfficeId);
        %>
        <tr>
            <td><%=isLangEng?budgetSelectionInfoDTO.economicYear: StringUtils.convertToBanNumber(budgetSelectionInfoDTO.economicYear)%></td>

            <td><%=isLangEng ? budget_officeDTO.nameEn : budget_officeDTO.nameBn%></td>

            <td>
                <%=isLangEng?budget_passwordDTO.userName+"<br>"+budget_passwordDTO.userNameEn+"<br><b>"+budget_passwordDTO.OfficeUnitOrganogramEng+"</b><br>"+budget_passwordDTO.OfficeUnitEng:
                        StringUtils.convertToBanNumber(budget_passwordDTO.userName)+"<br>"+budget_passwordDTO.userNameBN+"<br><b>"+budget_passwordDTO.OfficeUnitOrganogramBng+"</b><br>"+budget_passwordDTO.OfficeUnitBng%>
            </td>

            <td><%=budget_passwordDTO.password%></td>
        </tr>
        <% }
        } %>
        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>