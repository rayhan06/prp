<%@page import="image.ImageDAO" %>
<%@page import="util.CommonConstant" %>
<%@page import="org.apache.tomcat.util.bcel.classfile.ConstantLong" %>
<%@page import="user.UserRepository" %>
<%@page import="user.UserDTO" %>
<%@page import="util.ServletConstant" %>
<%@page import="util.ActionTypeConstant" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="util.JSPConstant" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="permission.ColumnRepository" %>
<%@page import="util.CollectionUtils" %>
<%@page import="role.PermissionRepository" %>
<%@page import="role.*" %>
<%@page import="permission.MenuRepository" %>
<%@page import="permission.*" %>
<%@page import="role.PermissionRepository" %>
<%@page import="config.GlobalConfigurationRepository" %>
<%@page import="image.*" %>
<%@page import="java.util.*" %>
<%
    String action = "ImageServlet?actionType=allImages";
    LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    Image Setting
                </h3>
            </div>
        </div>
        <form role="form" action="<%=action%>" class="form-horizontal"
              method="post" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=LM.getText(LC.IMAGE_ADD_NEW_IMAGE, loginDTO)%>
                                            </h4>
                                        </div>
                                    </div>
                                    <form role="form" action="ImageServlet?actionType=add" class="form-horizontal"
                                          method="post">
                                        <input type="hidden" name="menuID" value="0"/><br>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.IMAGE_FILE_NAME, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="fileName" name="fileName"
                                                       value="" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.IMAGE_DESCRIPTION, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="menuNameBangla"
                                                       name="description" value=""
                                                       required="required">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.IMAGE_FILE_PATH, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="hyperLink" name="filePath"
                                                       value="">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 form-actions text-right mt-3">
                        <button class="btn btn-success btn-sm shadow btn-border-radius"
                                type="submit"><%=LM.getText(LC.ROLE_ADD_SUBMIT, loginDTO)%>
                        </button>
                    </div>
                </div>
                <div class="form-body mt-5">
                    <jsp:include page='../common/flushActionStatus.jsp'/>
                    <div class="table-responsive">
                        <table id="tableData" class="table table-bordered table-striped">
                            <col width="5%">
                            <col width="20%">
                            <col width="10%">
                            <col width="10%">
                            <col width="10%">
                            <thead>
                            <tr>
                                <td>ID</td>
                                <td>Description</td>
                                <td>Image</td>
                                <td>preview</td>
                                <td>Upload</td>
                                <td>Delete</td>
                            </tr>
                            </thead>
                            <tbody>
                            <%
                                List<ImageDTO> imageDTOs = new ImageDAO().getAllImageDTOList();
                                int index = 0;
                                for (ImageDTO imageDTO : imageDTOs) {
                            %>
                            <tr>
                                <td><%=imageDTO.ID%>
                                </td>
                                <td><input type="text" name="URL" class="black-text" value="<%=imageDTO.URL%>"> <input
                                        type="hidden" name="ID" value="<%=imageDTO.ID%>"></td>
                                <td>
                                    <img src="<%=request.getContextPath()%>/images/<%=imageDTO.fileName %>" alt="hh"
                                         height="100"/>
                                </td>
                                <td><img id="<%=index%>" alt="image" height="100"/></td>
                                <td><input type="file" name="<%=imageDTO.fileName%>" accept="image/*"
                                           onchange="document.getElementById('<%=index%>').src=window.URL.createObjectURL(this.files[0])">
                                </td>
                                <td><input type="checkbox" name="deletedID" value="<%=imageDTO.ID%>"></td>
                            </tr>
                            <% index++;
                            }%>
                            </tbody>
                        </table>
                    </div>
                    <%if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.IMAGE_SETTING_UPDATE)) { %>
                    <div class="form-actions text-right mt-3">
                        <a class="btn btn-sm text-white shadow btn-border-radius cancel-btn"
                           href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.ROLE_ADD_CANCEL, loginDTO)%>
                        </a>
                        <button class="btn  btn-sm text-white shadow btn-border-radius submit-btn ml-2" type="submit"><%=LM.getText(LC.ROLE_ADD_SUBMIT, loginDTO)%>
                        </button>
                    </div>
                    <%} %>
                </div>
            </div>
        </form>
    </div>
</div>