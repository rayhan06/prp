<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="parliament_gallery.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    Parliament_galleryDTO parliament_galleryDTO;
    parliament_galleryDTO = (Parliament_galleryDTO) request.getAttribute("parliament_galleryDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (parliament_galleryDTO == null) {
        parliament_galleryDTO = new Parliament_galleryDTO();

    }
    System.out.println("parliament_galleryDTO = " + parliament_galleryDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.PARLIAMENT_GALLERY_ADD_PARLIAMENT_GALLERY_ADD_FORMNAME, loginDTO);
    String servletName = "Parliament_galleryServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.PARLIAMENT_GALLERY_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Parliament_galleryServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='ID' id='iD_hidden_<%=i%>'
                                           value='<%=parliament_galleryDTO.iD%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_GALLERY_ADD_GALLERYNO, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%
                                                value = "";
                                                if (parliament_galleryDTO.galleryNo != -1) {
                                                    value = parliament_galleryDTO.galleryNo + "";
                                                }
                                            %>
                                            <input type='number' class='form-control' name='galleryNo'
                                                   id='galleryNo_number_<%=i%>' value='<%=value%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_GALLERY_ADD_NAMEENG, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='nameEng'
                                                   id='nameEng_text_<%=i%>'
                                                   value='<%=parliament_galleryDTO.nameEng%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_GALLERY_ADD_NAMEBNG, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='nameBng'
                                                   id='nameBng_text_<%=i%>'
                                                   value='<%=parliament_galleryDTO.nameBng%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_GALLERY_ADD_TOTALSEAT, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%
                                                value = "";
                                                if (parliament_galleryDTO.totalSeat != -1) {
                                                    value = parliament_galleryDTO.totalSeat + "";
                                                }
                                            %>
                                            <input type='number' class='form-control' name='totalSeat'
                                                   id='totalSeat_number_<%=i%>' value='<%=value%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_GALLERY_ADD_LOCATIONENG, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='locationEng'
                                                   id='locationEng_text_<%=i%>'
                                                   value='<%=parliament_galleryDTO.locationEng%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_GALLERY_ADD_LOCATIONBNG, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='locationBng'
                                                   id='locationBng_text_<%=i%>'
                                                   value='<%=parliament_galleryDTO.locationBng%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='isActive'
                                           id='isActive_hidden_<%=i%>'
                                           value='<%=parliament_galleryDTO.isActive%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=parliament_galleryDTO.insertedByUserId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=parliament_galleryDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=parliament_galleryDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModifierUser'
                                           id='lastModifierUser_hidden_<%=i%>'
                                           value='<%=parliament_galleryDTO.lastModifierUser%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value='<%=parliament_galleryDTO.isDeleted%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=parliament_galleryDTO.searchColumn%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=parliament_galleryDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 form-actions text-right mt-3">
                        <a class="btn btn-sm cancel-btn text-white shadow btn-border-radius "
                           href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.PARLIAMENT_GALLERY_ADD_PARLIAMENT_GALLERY_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button class="btn  btn-sm submit-btn text-white shadow btn-border-radius ml-2"
                                type="submit"><%=LM.getText(LC.PARLIAMENT_GALLERY_ADD_PARLIAMENT_GALLERY_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Parliament_galleryServlet");
    }

    function init(row) {


    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






