<%@page pageEncoding="UTF-8" %>

<%@page import="parliament_gallery.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.PARLIAMENT_GALLERY_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_PARLIAMENT_GALLERY;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Parliament_galleryDTO parliament_galleryDTO = (Parliament_galleryDTO) request.getAttribute("parliament_galleryDTO");
    CommonDTO commonDTO = parliament_galleryDTO;
    String servletName = "Parliament_galleryServlet";


    System.out.println("parliament_galleryDTO = " + parliament_galleryDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Parliament_galleryDAO parliament_galleryDAO = (Parliament_galleryDAO) request.getAttribute("parliament_galleryDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_nameEng'>
    <%
        value = parliament_galleryDTO.nameEng + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_nameBng'>
    <%
        value = parliament_galleryDTO.nameBng + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_totalSeat'>
    <%
        value = parliament_galleryDTO.totalSeat + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Parliament_galleryServlet?actionType=view&ID=<%=parliament_galleryDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button
            type="button"
            class="btn-sm border-0 shadow text-white btn-border-radius"
            style="background-color: #ff6b6b;"
            onclick="location.href='Parliament_galleryServlet?actionType=getEditPage&ID=<%=parliament_galleryDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>

</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=parliament_galleryDTO.iD%>'/></span>
    </div>
</td>
																						
											

