<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@ page import="pb.*" %>
<%
    String Language = LM.getText(LC.GENDER_RATIO_REPORT_EDIT_LANGUAGE, loginDTO);
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
%>

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>

<div class="row">
    <div class="col-md-6 col-sm-12">
        <div id="officeUnitId_div" class="search-criteria-div">
            <div class="form-group row">
                <label class="col-3 control-label text-right">
                    <%=LM.getText(LC.GENDER_RATIO_REPORT_WHERE_OFFICEUNITID, loginDTO)%>
                </label>
                <div class="col-9">
                    <%--Office Unit Modal Trigger Button--%>
                    <button type="button" class="btn btn-primary form-control" id="office_units_id_modal_button"
                            onclick="officeModalButtonClicked();">
                        <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                    </button>

                    <div class="input-group" id="office_units_id_div" style="display: none">
                        <input type="hidden" name='officeUnitId' id='office_units_id_input' value="">
                        <button type="button" class="btn btn-secondary form-control" disabled
                                id="office_units_id_text"></button>
                        <span class="input-group-btn" style="width: 5%;padding-right: 5px;" tag='pb_html'>
                            <button type="button" class="btn btn-outline-danger"
                                    onclick="crsBtnClicked('office_units_id');"
                                    id='office_units_id_crs_btn' tag='pb_html'>
                                x
                            </button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div id="officeUnitOrganogramId_div" class="search-criteria-div">
            <div class="form-group row">
                <label class="col-3 control-label text-right">
                    <%=LM.getText(LC.GENDER_RATIO_REPORT_WHERE_OFFICEUNITORGANOGRAMID, loginDTO)%>
                </label>
                <div class="col-9">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary form-control" id="organogram_id_modal_button"
                            onclick="organogramIdModalBtnClicked();">
                        <%=LM.getText(LC.LANGUAGE_SELECT, userDTO)%>
                    </button>
                    <div class="input-group" id="organogram_id_div" style="display: none">
                        <input type="hidden" name='officeUnitOrganogramId' id='organogram_id_input' value="">
                        <button type="button" class="btn btn-secondary form-control" disabled
                                id="organogram_id_text"></button>
                        <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                            <button type="button" class="btn btn-outline-danger"
                                    onclick="crsBtnClicked('organogram_id');"
                                    id='organogram_id_crs_btn' tag='pb_html'>
                                x
                            </button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div id="employmentCat_div" class="search-criteria-div">
            <div class="form-group row">
                <label class="col-3 control-label text-right" for="employmentCat">
                    <%=LM.getText(LC.GENDER_RATIO_REPORT_WHERE_EMPLOYMENTCAT, loginDTO)%>
                </label>
                <div class="col-9">
                    <select class='form-control' name='employmentCat' id='employmentCat' onChange="ajaxSubmit();">
                        <%
                            Options = CatDAO.getOptions(Language, "employment", CatDTO.CATDEFAULT);
                        %>
                        <%=Options%>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div id="gender_div" class="search-criteria-div">
            <div class="form-group row">
                <label class="col-3 control-label text-right" for="gender">
                    <%=LM.getText(LC.GENDER_RATIO_REPORT_WHERE_GENDER, loginDTO)%>
                </label>
                <div class="col-9">

                    <select class='form-control' name='gender' id='gender' onChange="ajaxSubmit();">
                        <%
                            Options = CatDAO.getOptions(Language, "gender", CatDTO.CATDEFAULT);
                        %>
                        <%=Options%>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    function init() {
        select2SingleSelector('#employmentCat', '<%=Language%>');
        select2SingleSelector('#gender', '<%=Language%>');
        dateTimeInit($("#Language").val());
    }

    function PreprocessBeforeSubmiting() {
    }
</script>