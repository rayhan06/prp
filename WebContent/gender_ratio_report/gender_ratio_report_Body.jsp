<%@page import="util.ActionTypeConstant" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="centre.CentreDAO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String userfullName = userDTO.fullName;
    int userCentre = userDTO.centreType;
    CentreDAO centreDAO = new CentreDAO();
    String centreName = centreDAO.getCentreNameByCentreID(userCentre);
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title prp-page-title">
                            <i class="fa fa-cubes"></i><%=LM.getText(LC.GENDER_RATIO_REPORT_OTHER_REPORT_GENERATION, loginDTO)%>
                        </h3>
                    </div>
                </div>

                <form class="form-horizontal" id="ReportForm"
                      action="<%=request.getContextPath()%>/Gender_ratio_report_Servlet?actionType=<%=ActionTypeConstant.REPORT_RESULT%>">
                    <div class="kt-portlet__body form-body">
                        <div class="row">
                            <div class="offset-md-1 col-md-10">
                                <div class="onlyborder">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="sub_title_top">
                                                        <div class="sub_title">
                                                            <h4 style="background: white">
                                                                <%=LM.getText(LC.GENDER_RATIO_REPORT_OTHER_REPORT_GENERATION, loginDTO)%>
                                                            </h4>
                                                        </div>

                                                        <input type="hidden" id="countURL" name="countURL"
                                                               value="/Gender_ratio_report_Servlet?actionType=<%=ActionTypeConstant.REPORT_COUNT%>">

                                                        <div id="searchCriteria" hide="false" class="col-12">
                                                            <%@include file="gender_ratio_report_SearchDiv.jsp" %>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-5" id="report-div">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                                        <div class="kt-subheader__main">
                                            <h3 class="kt-subheader__title">
                                                <i class="icon-settings"></i>
                                                <span id="report-name" class="caption-subject bold uppercase">
													<%=LM.getText(LC.GENDER_RATIO_REPORT_OTHER_GENDER_RATIO_REPORT, loginDTO)%>
												</span>
                                            </h3>
                                        </div>
                                    </div>

                                    <div class="tools"></div>

                                    <div class="portlet-body">
                                        <div class="table" style="overflow: scroll;">
                                            <table class="table table-striped " id="reportTable"
                                                   style="width: 100%">

                                                <thead>
                                                <tr>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%@include file="../pbreport/pagination.jsp" %>
                </form>
            </div>
        </div>
    </div>
</div>


<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<%
    String modalTitle = Language.equalsIgnoreCase("English") ? "Find Designation" : "পদবী খুঁজুন";
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
    <jsp:param name="modalTitle" value="<%=modalTitle%>"/>
</jsp:include>

<script>
    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
        ajaxSubmit();
    }

    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        $('#office_units_id_modal_button').hide();
        $('#office_units_id_div').show();

        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id_input').val(selectedOffice.id);

        ajaxSubmit();
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    function officeModalButtonClicked() {
        console.log('Button Clicked!');
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function viewOgranogramIdInInput(empInfo) {
        $('#organogram_id_modal_button').hide();
        $('#organogram_id_div').show();

        let language = '<%=Language.toLowerCase()%>'
        let designation;
        if (language === 'english') {
            designation = empInfo.organogramNameEn + ', ' + empInfo.officeUnitNameEn;
        } else {
            designation = empInfo.organogramNameBn + ', ' + empInfo.officeUnitNameBn;
        }
        document.getElementById('organogram_id_text').innerHTML = designation;
        $('#organogram_id_input').val(empInfo.organogramId);

        ajaxSubmit();
    }


    table_name_to_collcetion_map = new Map([
        ['organogramId', {
            isSingleEntry: true,
            callBackFunction: viewOgranogramIdInInput
        }],
    ]);
    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    function organogramIdModalBtnClicked() {
        modal_button_dest_table = 'organogramId';
        $('#search_emp_modal').modal();
    }

</script>

