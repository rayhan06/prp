<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="recruitment_seat_plan.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameRepository" %>
<%@ page import="recruitment_exam_venue.Recruitment_exam_venueRepository" %>

<%
    Recruitment_seat_planDTO recruitment_seat_planDTO = new Recruitment_seat_planDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        recruitment_seat_planDTO = Recruitment_seat_planDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = recruitment_seat_planDTO;
    String tableName = "recruitment_seat_plan";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%@ include file="manualLoader.jsp" %>
<%
    String formTitle = LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_ADD_FORMNAME, loginDTO);
    String servletName = "Recruitment_seat_planServlet";
%>
<style>
    .hiddenTr {
        display: none;
    }

</style>
<script
        src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"
        integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA=="
        crossorigin="anonymous"
        referrerpolicy="no-referrer"
></script>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-11">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>


                                        <input type='hidden' class='form-control' name='iD' id='iD_hidden'
                                               value='<%=recruitment_seat_planDTO.iD%>' tag='pb_html'/>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=LM.getText(LC.RECRUITMENT_TEST_NAME_ADD_RECRUITMENT_TEST_NAME_ADD_FORMNAME, loginDTO)%><span
                                                    class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='recruitmentTestNameId'
                                                        id='recruitmentTestNameId'
                                                        onchange="onRecruitmentTestNameChange(this)" tag='pb_html'>
                                                    <%= Recruitment_test_nameRepository.getInstance().buildOptions(Language, recruitment_seat_planDTO.recruitmentTestNameId) %>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_JOBTITLE, loginDTO)%><span
                                                    class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='recruitmentJobDescriptionId'
                                                        id='recruitmentJobDescriptionId'
                                                        onchange="onRecruitmentJobDescriptionChange(this)"
                                                        tag='pb_html'>

                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=LM.getText(LC.CANDIDATE_LIST_LEVEL, loginDTO)%><span
                                                    class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='recruitmentJobSpecificExamTypeId'
                                                        id='recruitmentJobSpecificExamTypeId'
                                                        onchange="onRecruitmentJobSpecificExamTypeIdChange()"
                                                        tag='pb_html'>

                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_ADD_FORMNAME, loginDTO)%><span
                                                    class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='recruitmentExamVenueId'
                                                        id='recruitmentExamVenueId'
                                                        onchange="onRecruitmentExamVenueChange(this)" tag='pb_html'>
                                                    <%= Recruitment_exam_venueRepository.getInstance().buildOptions(Language, recruitment_seat_planDTO.recruitmentExamVenueId) %>
                                                </select>
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_EXAMDATE, loginDTO)%>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-8">
                                                <%value = "examDate_js_" + i;%>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='examDate' id='examDate_date_<%=i%>'
                                                       value='<%=dateFormat.format(new Date(recruitment_seat_planDTO.examDate))%>'
                                                       tag='pb_html'>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_EXAMTIME, loginDTO)%>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-8">
                                                <input type='text' class='form-control' name='examTime'
                                                       id='examTime_text_<%=i%>'
                                                       value='<%=recruitment_seat_planDTO.examTime%>' tag='pb_html'/>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD, loginDTO)%>
                        </h5>

                        <div class="mt-3 mb-3">
                            <h3>
                                <span><%=UtilCharacter.getDataByLanguage(Language, "শুরুর রোলঃ", "Start Roll: ")%></span>&nbsp;<span
                                    id="jobApplicantStartRollId"></span></h3>
                            <h3><span><%=UtilCharacter.getDataByLanguage(Language, "শেষ রোলঃ", "End Roll: ")%></span>&nbsp;<span
                                    id="jobApplicantEndRollId"></span></h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="seatPlanConfigurationTable">
                                <thead>
                                <tr>

                                    <th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD_FLOOR, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD_CAPACITY, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD_STARTROLL, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD_ISRANGEAUTOCOUNT, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD_ENDROLL, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD_TOTAL, loginDTO)%>
                                    </th>

                                </tr>
                                </thead>
                                <tbody id="appendTbody">


                                </tbody>
                                <tr class="hiddenTr">


                                    <input type='hidden' class='form-control examVenueItemIdCls'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control recruitmentSeatPlanChildIdCls'
                                           tag='pb_html'/>

                                    <td class="buildingFloorRoomTdCls">

                                    </td>
                                    <td class="capacityTdCls">

                                    </td>
                                    <td class="startRollTdCls">

                                    </td>
                                    <td class="isRangeTdCls">

                                    </td>
                                    <td class="endRollTdCls">

                                    </td>
                                    <td class="totalTdCls">

                                    </td>

                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="form-actions text-right mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                            onclick="submitForm()">
                        <%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>

    </div>
</div>

<script type="text/javascript">

    let language = '<%=Language%>';
    let selectorArr = ['recruitmentTestNameId', 'recruitmentJobDescriptionId', 'recruitmentJobSpecificExamTypeId','jobApplicantStartRollId','jobApplicantEndRollId'];
    const fullPageLoader = $('#full-page-loader');
    const recruitmentSeatPlanForm = $("#bigform");

    function submitForm() {
        buttonStateChange(true);
        setRangeHiddenValue();
        if (PreprocessBeforeSubmiting()) {
            fullPageLoader.show();
            let url = "<%=servletName%>?actionType=<%=actionName%>";
            $.ajax({
                type: "POST",
                url: url,
                data: recruitmentSeatPlanForm.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        fullPageLoader.hide();
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        fullPageLoader.hide();
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    fullPageLoader.hide();
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        } else {
            buttonStateChange(false);
            fullPageLoader.hide();
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function PreprocessBeforeSubmiting(row, action) {

        preprocessDateBeforeSubmitting('examDate', row);


        return true;
    }

    function resetSelectors(startIndex) {
        selectorArr.filter((item, index) => index >= startIndex).map(element => $("#" + element).empty());
    }


    function onRecruitmentTestNameChange(selectedVal) {
        resetSelectors(1);
        let recruitmentTestNameId = selectedVal.value;

        if (recruitmentTestNameId !== null && recruitmentTestNameId > 0 && recruitmentTestNameId !== "") {
            const url = 'Recruitment_job_descriptionServlet?actionType=getJobDescriptionByRecTestName&recruitmentTestNameId=' + recruitmentTestNameId +
                '&selectedId=' +<%=recruitment_seat_planDTO.recruitmentJobDescriptionId%>;

            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (response) {
                    document.getElementById('recruitmentJobDescriptionId').innerHTML = response;
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }

    function onRecruitmentJobDescriptionChange(selectedVal) {
        resetSelectors(2);
        let recruitmentTestNameId = document.getElementById('recruitmentTestNameId').value;
        let jobValue = selectedVal.value;

        if (recruitmentTestNameId !== null && recruitmentTestNameId > 0 && recruitmentTestNameId !== "") {
            const url = 'Recruitment_job_descriptionServlet?actionType=getExamTypesByTestNameAndJobId&recruitmentTestNameId=' + recruitmentTestNameId +
                '&jobId=' + jobValue + '&selectedId=' +<%=recruitment_seat_planDTO.recruitmentJobSpecificExamTypeId%>;

            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (response) {
                    document.getElementById('recruitmentJobSpecificExamTypeId').innerHTML = response;
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }

    function onRecruitmentJobSpecificExamTypeIdChange() {
        let recruitmentTestName = document.querySelector('#recruitmentTestNameId').value;
        let recruitmentJobDescription = document.querySelector('#recruitmentJobDescriptionId').value;
        let recruitmentJobSpecificExamType = document.querySelector('#recruitmentJobSpecificExamTypeId').value;
        let param2 = '&recruitmentTestName=' + recruitmentTestName;
        param2 += '&recruitmentJobDescription=' + recruitmentJobDescription;
        param2 += '&recruitmentJobSpecificExamType=' + recruitmentJobSpecificExamType;
        const getSpecificExamStartAndEndRollUrl = 'Recruitment_exam_venueServlet?actionType=getSpecificExamStartAndEndRoll' + param2;
        $.ajax({
            url: getSpecificExamStartAndEndRollUrl,
            type: "GET",
            async: false,
            success: function (response) {
                setStartRollAndEndRoll(response);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function onRecruitmentExamVenueChange(selectedVal) {
        let recruitmentTestName = document.querySelector('#recruitmentTestNameId').value;
        let recruitmentJobDescription = document.querySelector('#recruitmentJobDescriptionId').value;
        let recruitmentJobSpecificExamType = document.querySelector('#recruitmentJobSpecificExamTypeId').value;
        let recruitmentExamVenueId = document.querySelector('#recruitmentExamVenueId').value;
        let recruitmentSeatPlanId = document.querySelector('#iD_hidden').value;



        let actionType = '';
        if ('<%=actionName%>' === 'ajax_edit') {
            actionType = 'getExamVenueByPreviousConfiguration';
        } else {
            actionType = 'getExamVenueBuildingFloorRoomByVenueId';
        }

        let param = 'actionType=' + actionType;
        param += '&recruitmentTestName=' + recruitmentTestName;
        param += '&recruitmentJobDescription=' + recruitmentJobDescription;
        param += '&recruitmentJobSpecificExamType=' + recruitmentJobSpecificExamType;
        param += '&recruitmentExamVenueId=' + recruitmentExamVenueId;
        param += '&recruitmentSeatPlanId=' + recruitmentSeatPlanId;

        if (recruitmentExamVenueId !== null && recruitmentExamVenueId !== -1 && recruitmentExamVenueId !== "") {
            fullPageLoader.show();
            $("#appendTbody").empty();
            const url = 'Recruitment_exam_venueServlet?' + param;

            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (response) {
                    setSeatPlanTableData(response);
                    fullPageLoader.hide();
                },
                error: function (error) {
                    fullPageLoader.hide();
                    console.log(error);
                }
            });
        }
    }

    function setSeatPlanTableData(data) {

        data.forEach((item, index) => {
            let {
                iD,
                recruitmentExamVenueItemId,
                building,
                floor,
                roomNo,
                capacity,
                startRoll,
                endRoll,
                total,
                isRange
            } = item;

            let capacityClass = 'capacity-' + index;
            let startRollClass = 'start-roll-' + index;
            let endRollClass = 'end-roll-' + index;
            let totalClass = 'total-' + index;
            let isRangeClass = 'isRange-' + index;

            let startRollInputField = '<td><input onInput="startRollChange(' + index + ')" type="text" value="' + startRoll + '" name="recruitmentSeatPlanChild.startRoll"' +
                ' class="form-control ' + startRollClass + '" ></td>';
            let endRollInputField = '<td><input type="text" value="' + endRoll + '" name="recruitmentSeatPlanChild.endRoll" class="form-control ' + endRollClass + '""></td>';
            let totalInputField = '<td><input type="text" value="' + total + '" name="recruitmentSeatPlanChild.total" class="form-control ' + totalClass + '"></td>';

            let isRangeField = '';
            if (isRange) {
                isRangeField = '<td><input type="hidden" name="recruitmentSeatPlanChild.isRangeAutoHidden" value="1" class="hiddenIsRange"><input type="checkbox" class="form-control-sm setIsRange ' + isRangeClass + '" name="recruitmentSeatPlanChild.isRangeAutoCount" onclick="checkBoxClick(' + index + ')" checked></td>';
            } else {
                isRangeField = '<td><input type="hidden" name="recruitmentSeatPlanChild.isRangeAutoHidden" value="-1" class="hiddenIsRange"><input type="checkbox" class="form-control-sm setIsRange ' + isRangeClass + '" name="recruitmentSeatPlanChild.isRangeAutoCount" onclick="checkBoxClick(' + index + ')" ></td>';
            }


            let trContent = document.querySelector('.hiddenTr').cloneNode(true);
            trContent.classList.replace('hiddenTr', 'appendedTr');

            trContent.querySelector('.examVenueItemIdCls').value = recruitmentExamVenueItemId;
            trContent.querySelector('.examVenueItemIdCls').name = 'recruitmentSeatPlanChild.recruitmentExamVenueItemId';

            trContent.querySelector('.recruitmentSeatPlanChildIdCls').value = iD;
            trContent.querySelector('.recruitmentSeatPlanChildIdCls').name = 'recruitmentSeatPlanChild.iD';

            trContent.querySelector('.buildingFloorRoomTdCls').innerHTML = building + ', ' + floor + ', ' + roomNo;
            trContent.querySelector('.capacityTdCls').innerHTML = capacity;
            trContent.querySelector('.capacityTdCls').classList.add(capacityClass);
            trContent.querySelector('.startRollTdCls').innerHTML = startRollInputField;
            trContent.querySelector('.endRollTdCls').innerHTML = endRollInputField;
            trContent.querySelector('.totalTdCls').innerHTML = totalInputField;
            trContent.querySelector('.isRangeTdCls').innerHTML = isRangeField;

            $("#appendTbody").append(trContent);
        });

    }

    function startRollChange(index) {
        let isRangeElement = document.querySelector('.isRange-' + index);
        if (isRangeElement.checked) {
            setRangeValues(index);
        }

    }

    function setRangeValues(index) {
        let capacityElement = document.querySelector('.capacity-' + index);
        let startElement = document.querySelector('.start-roll-' + index);
        let endElement = document.querySelector('.end-roll-' + index);
        let totalElement = document.querySelector('.total-' + index);

        let capacityElementVal = capacityElement.innerText;
        let startElementVal = startElement.value;
        endElement.value = (+startElementVal) + (+capacityElementVal);
        totalElement.value = (+endElement.value) - (+startElementVal);
        if (startElementVal === '') {
            endElement.value = '';
            totalElement.value = '';
        }
        $(endElement).prop('readonly', true);
        $(totalElement).prop('readonly', true);
        //$(totalElement).prop('readonly', true);
    }

    function checkBoxClick(index) {
        let isRangeElement = document.querySelector('.isRange-' + index);
        let endElement = document.querySelector('.end-roll-' + index);
        let startElement = document.querySelector('.start-roll-' + index);
        let totalElement = document.querySelector('.total-' + index);
        if (isRangeElement.checked) {
            setRangeValues(index);
        } else {
            $(endElement).prop('readonly', false);
            $(totalElement).prop('readonly', false);
            startElement.value = '';
            endElement.value = '';
            totalElement.value = '';
        }
    }


    function init(row) {

        setDateByStringAndId('examDate_js_' + row, $('#examDate_date_' + row).val());
        select2SingleSelector('#recruitmentTestNameId', '<%=Language%>');
        select2SingleSelector('#recruitmentJobDescriptionId', '<%=Language%>');
        select2SingleSelector('#recruitmentJobSpecificExamTypeId', '<%=Language%>');
        select2SingleSelector('#recruitmentExamVenueId', '<%=Language%>');

        for (i = 1; i < child_table_extra_id; i++) {
            setDateByStringAndId('examDate_js_' + i, $('#examDate_date_' + i).val());
            setTimeById('examTime_js_' + i, $('#examTime_time_' + i).val(), true);
        }

    }

    var row = 0;

    $(document).ready(function () {
        init(row);
        let actionName = '<%=actionName%>';
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
        if (actionName === 'ajax_edit') {
            let recruitmentTestNameElement = document.querySelector('#recruitmentTestNameId');
            onRecruitmentTestNameChange(recruitmentTestNameElement);
            let recruitmentJobDescriptionElement = document.querySelector('#recruitmentJobDescriptionId');
            onRecruitmentJobDescriptionChange(recruitmentJobDescriptionElement);
            onRecruitmentJobSpecificExamTypeIdChange();
            let recruitmentExamVenueId = document.querySelector('#recruitmentExamVenueId');
            onRecruitmentExamVenueChange(recruitmentExamVenueId);
            readOnlyOnEdit();
        }

        // $("#seatPlanConfigurationTable").sortable({
        //     cursor: 'row-resize',
        //     placeholder: 'ui-state-highlight',
        //     opacity: '0.55',
        //     items: '.ui-sortable-handle'
        // }).disableSelection();

        $(function () {
            $("#seatPlanConfigurationTable tbody").sortable({
                axis: "y",
                containment: "parent",
                animation: 200,
            });
            //$("#seatPlanConfigurationTable tbody").sortable();
        });

        fullPageLoader.hide();
    });



    function readOnlyOnEdit() {
        let allInputArr = ['recruitmentTestNameId', 'recruitmentJobDescriptionId', 'recruitmentJobSpecificExamTypeId', 'recruitmentExamVenueId',];
        allInputArr.forEach((item) => {
            $('#' + item).prop('disabled', true);
        });

    }

    function setRangeHiddenValue() {
        let isRangeElement = document.querySelectorAll('.setIsRange');

        isRangeElement.forEach((item) => {
            let parent = item.parentNode;
            let hiddenIsRange = parent.querySelector('.hiddenIsRange');
            if (item.checked) {
                hiddenIsRange.value = "1";
            } else {
                hiddenIsRange.value = "-1";
            }
        });


    }

    function setStartRollAndEndRoll(response) {
        let {startRoll, endRoll} = response;
        console.log(startRoll)
        let startRollElem = document.querySelector('#jobApplicantStartRollId');
        let endRollElem = document.querySelector('#jobApplicantEndRollId');
        startRollElem.innerText = startRoll;
        endRollElem.innerText = endRoll;
    }

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






