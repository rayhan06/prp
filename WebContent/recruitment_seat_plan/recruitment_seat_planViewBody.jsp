

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="recruitment_seat_plan.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>
<%@ page import="recruitment_test_name.Recruitment_test_nameRepository" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameDTO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionRepository" %>
<%@ page import="recruitment_job_specific_exam_type.RecruitmentJobSpecificExamTypeDTO" %>
<%@ page import="recruitment_job_specific_exam_type.JobSpecificExamTypeRepository" %>
<%@ page import="recruitment_exam_venue.Recruitment_exam_venueDTO" %>
<%@ page import="recruitment_exam_venue.Recruitment_exam_venueRepository" %>

<%@include file="../pb/viewInitializer.jsp"%>
<%
String servletName = "Recruitment_seat_planServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Recruitment_seat_planDTO recruitment_seat_planDTO = Recruitment_seat_planDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = recruitment_seat_planDTO;

	String recruitmentTestName = "";
	String recruitmentJob = "";
	String recruitmentJobSpecificExam = "";
	String recruitmentExamVenue = "";
	Recruitment_test_nameDTO recruitment_test_nameDTO = Recruitment_test_nameRepository.getInstance()
			.getRecruitment_test_nameDTOByiD(recruitment_seat_planDTO.recruitmentTestNameId);
	Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = Recruitment_job_descriptionRepository.getInstance()
			.getRecruitment_job_descriptionDTOByID(recruitment_seat_planDTO.recruitmentJobDescriptionId);
	if (recruitment_job_descriptionDTO != null) {
		List<RecruitmentJobSpecificExamTypeDTO> recruitmentJobSpecificExamTypeDTOS = JobSpecificExamTypeRepository.getInstance()
				.getRecruitmentJobSpecificExamTypeDTOByjob_id(recruitment_job_descriptionDTO.iD);
		RecruitmentJobSpecificExamTypeDTO jobSpecificExamTypeDTO = recruitmentJobSpecificExamTypeDTOS.stream()
				.filter(e -> e.order == recruitment_seat_planDTO.recruitmentJobSpecificExamTypeId)
				.findFirst()
				.orElse(null);
		if(jobSpecificExamTypeDTO != null){
			recruitmentJobSpecificExam = CatRepository.getName(Language, "job_exam_type", jobSpecificExamTypeDTO.jobExamTypeCat);
		}
		else{
			recruitmentJobSpecificExam = "";
		}
	}
	Recruitment_exam_venueDTO recruitment_exam_venueDTO = Recruitment_exam_venueRepository.getInstance()
			.getRecruitment_exam_venueDTOByiD(recruitment_seat_planDTO.recruitmentExamVenueId);


	recruitmentTestName = recruitment_test_nameDTO != null ? isLanguageEnglish ? recruitment_test_nameDTO.nameEn : recruitment_test_nameDTO.nameBn : "";
	recruitmentJob = recruitment_job_descriptionDTO != null ? isLanguageEnglish ? recruitment_job_descriptionDTO.jobTitleEn
			: recruitment_job_descriptionDTO.jobTitleBn : "";
	recruitmentExamVenue = recruitment_exam_venueDTO != null ? isLanguageEnglish ? recruitment_exam_venueDTO.nameEn : recruitment_exam_venueDTO.nameBn : "";

%>



<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.RECRUITMENT_TEST_NAME_ADD_RECRUITMENT_TEST_NAME_ADD_FORMNAME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=recruitmentTestName%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENTJOBDESCRIPTIONID, loginDTO)%>
                                    </label>
                                    <div class="col-8">

			                               <%=recruitmentJob%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENTJOBSPECIFICEXAMTYPEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
										<%=recruitmentJobSpecificExam%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENTEXAMVENUEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
										<%=recruitmentExamVenue%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_EXAMDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = recruitment_seat_planDTO.examDate + "";
											%>
											<%
											String formatted_examDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_examDate, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_EXAMTIME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = recruitment_seat_planDTO.examTime + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
			
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD_EXAMDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD_EXAMTIME, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD_BUILDING, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD_FLOOR, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD_ROOMNO, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD_CAPACITY, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD_STARTROLL, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD_ENDROLL, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD_TOTAL, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD_ISRANGEAUTOCOUNT, loginDTO)%></th>
							</tr>
							<%
                        	RecruitmentSeatPlanChildDAO recruitmentSeatPlanChildDAO = RecruitmentSeatPlanChildDAO.getInstance();
                         	List<RecruitmentSeatPlanChildDTO> recruitmentSeatPlanChildDTOs = (List<RecruitmentSeatPlanChildDTO>)recruitmentSeatPlanChildDAO.getDTOsByParent("recruitment_seat_plan_id", recruitment_seat_planDTO.iD);
                         	
                         	for(RecruitmentSeatPlanChildDTO recruitmentSeatPlanChildDTO: recruitmentSeatPlanChildDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%
											value = recruitmentSeatPlanChildDTO.examDate + "";
											%>
											<%
											formatted_examDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_examDate, Language)%>
				
			
										</td>
										<td>
											<%
											value = recruitmentSeatPlanChildDTO.examTime + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = recruitmentSeatPlanChildDTO.building + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = recruitmentSeatPlanChildDTO.floor + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = recruitmentSeatPlanChildDTO.roomNo + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = recruitmentSeatPlanChildDTO.capacity + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = recruitmentSeatPlanChildDTO.startRoll + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = recruitmentSeatPlanChildDTO.endRoll + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = recruitmentSeatPlanChildDTO.total + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = recruitmentSeatPlanChildDTO.isRangeAutoCount + "";
											%>
				
											<%=Utils.getYesNo(value, Language)%>
				
			
										</td>

                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
        </div>
    </div>
</div>