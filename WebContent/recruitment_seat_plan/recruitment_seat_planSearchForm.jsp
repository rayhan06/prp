<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="recruitment_seat_plan.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameRepository" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameDTO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionRepository" %>
<%@ page import="recruitment_job_specific_exam_type.JobSpecificExamTypeDao" %>
<%@ page import="recruitment_job_specific_exam_type.RecruitmentJobSpecificExamTypeDTO" %>
<%@ page import="recruitment_job_specific_exam_type.JobSpecificExamTypeRepository" %>
<%@ page import="java.util.List" %>
<%@ page import="recruitment_exam_venue.Recruitment_exam_venueDTO" %>
<%@ page import="recruitment_exam_venue.Recruitment_exam_venueRepository" %>


<%
    String navigator2 = "navRECRUITMENT_SEAT_PLAN";
    String servletName = "Recruitment_seat_planServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><%=LM.getText(LC.RECRUITMENT_TEST_NAME_ADD_RECRUITMENT_TEST_NAME_ADD_FORMNAME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENTJOBDESCRIPTIONID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENTJOBSPECIFICEXAMTYPEID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENTEXAMVENUEID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_EXAMDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_EXAMTIME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_SEARCH_RECRUITMENT_SEAT_PLAN_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span>All</span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<Recruitment_seat_planDTO>) rn2.list;

            try {

                if (data != null) {
                    int size = data.size();
                    String recruitmentTestName = "";
                    String recruitmentJob = "";
                    String recruitmentJobSpecificExam = "";
                    String recruitmentExamVenue = "";
                    for (int i = 0; i < size; i++) {
                        Recruitment_seat_planDTO recruitment_seat_planDTO = (Recruitment_seat_planDTO) data.get(i);
                        Recruitment_test_nameDTO recruitment_test_nameDTO = Recruitment_test_nameRepository.getInstance()
                                .getRecruitment_test_nameDTOByiD(recruitment_seat_planDTO.recruitmentTestNameId);
                        Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = Recruitment_job_descriptionRepository.getInstance()
                                .getRecruitment_job_descriptionDTOByID(recruitment_seat_planDTO.recruitmentJobDescriptionId);
                        if (recruitment_job_descriptionDTO != null) {
                            List<RecruitmentJobSpecificExamTypeDTO> recruitmentJobSpecificExamTypeDTOS = JobSpecificExamTypeRepository.getInstance()
                                    .getRecruitmentJobSpecificExamTypeDTOByjob_id(recruitment_job_descriptionDTO.iD);
                            RecruitmentJobSpecificExamTypeDTO jobSpecificExamTypeDTO = recruitmentJobSpecificExamTypeDTOS.stream()
                                    .filter(e -> e.order == recruitment_seat_planDTO.recruitmentJobSpecificExamTypeId)
                                    .findFirst()
                                    .orElse(null);
                            if(jobSpecificExamTypeDTO != null){
                                recruitmentJobSpecificExam = CatRepository.getName(Language, "job_exam_type", jobSpecificExamTypeDTO.jobExamTypeCat);
                            }
                            else{
                                recruitmentJobSpecificExam = "";
                            }
                        }
                        Recruitment_exam_venueDTO recruitment_exam_venueDTO = Recruitment_exam_venueRepository.getInstance()
                                .getRecruitment_exam_venueDTOByiD(recruitment_seat_planDTO.recruitmentExamVenueId);


                        recruitmentTestName = recruitment_test_nameDTO != null ? isLanguageEnglish ? recruitment_test_nameDTO.nameEn : recruitment_test_nameDTO.nameBn : "";
                        recruitmentJob = recruitment_job_descriptionDTO != null ? isLanguageEnglish ? recruitment_job_descriptionDTO.jobTitleEn
                                : recruitment_job_descriptionDTO.jobTitleBn : "";
                        recruitmentExamVenue = recruitment_exam_venueDTO != null ? isLanguageEnglish ? recruitment_exam_venueDTO.nameEn : recruitment_exam_venueDTO.nameBn : "";


        %>
        <tr>


            <td>

                <%=recruitmentTestName%>

            </td>

            <td>

                <%=recruitmentJob%>

            </td>

            <td>

                <%=recruitmentJobSpecificExam%>

            </td>

            <td>

                <%=recruitmentExamVenue%>

            </td>

            <td>
                <%
                    value = recruitment_seat_planDTO.examDate + "";
                %>
                <%
                    String formatted_examDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                %>
                <%=Utils.getDigits(formatted_examDate, Language)%>


            </td>

            <td>
                <%
                    value = recruitment_seat_planDTO.examTime + "";
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>


            <%CommonDTO commonDTO = recruitment_seat_planDTO; %>
            <%@include file="../pb/searchAndViewButton.jsp" %>

            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID'
                                                 value='<%=recruitment_seat_planDTO.iD%>'/></span>
                </div>
            </td>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			