<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.TRANSACTION_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>
<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>
<div class="row mx-2">
    <div class="col-md-12">
        <%@include file="../pbreport/yearmonth.jsp" %>
    </div>
    <div class="col-md-12">
        <%@include file="../pbreport/calendar.jsp" %>
    </div>
    <div class="search-criteria-div col-md-12">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.HM_DRUGS, loginDTO)%>
            </label>
            <div class="col-md-9">
                 <div class="input-group">
			        <input
			          class="form-control py-2  border searchBox w-auto"
			          type="search"
			          placeholder="Search"
			          
                                              id='suggested_drug_0'
                                              tag='pb_html'
                                              onkeyup="getDrugs(this.id)"
                                              autocomplete="off"
			        />
			        
			      </div>
			      <div
			        id="searchSgtnSection_"
			        class="search-sgtn-section shadow-sm bg-white"
					tag='pb_html'
			      >
			        <div class="pt-3">
			          <ul class="px-0" style="list-style: none" id="drug_ul_0" tag='pb_html'>
			          </ul>
			        </div>
			      </div>
			      
			      <input name='formStr'
                                           id='formStr_0' type="hidden"
                                           tag='pb_html'
                                           value=''></input>
                                    
                                           
                   <input name='drug_information_id'
                             id='drugInformationType_select_0' type="hidden"
                             tag='pb_html'
                             value=''></input>
            </div>
        </div>
    </div>
    <div class="search-criteria-div col-md-12">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.TRANSACTION_REPORT_SELECT_TRANSACTIONTYPE, loginDTO)%>
            </label>
            <div class="col-md-9">
            	<select class='form-control' name='medical_transaction_cat' id='medical_transaction_cat'
                        tag='pb_html'>

                    <%
                        Options = CatDAO.getOptions(Language, "medical_transaction", -2);
                        out.print(Options);
                    %>
                </select>               
            </div>
        </div>
    </div>
    
     <div class="search-criteria-div col-md-12">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.HM_SELECT_EMPLOYEE, loginDTO)%>
            </label>
            <div class="col-md-9">
                <button type="button" class="btn submit-btn text-white shadow btn-border-radius btn-block"
                        onclick="addEmployee()"
                        id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
                </button>
                <table class="table table-bordered table-striped">
                    <tbody id="employeeToSet"></tbody>
                </table>
                
                <input class='form-control' type='hidden' name='userName' id='userName' value=''/>
            </div>
        </div>
    </div>
    
    <div class="search-criteria-div col-md-12">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.HM_USER_NAME, loginDTO)%>
            </label>
            <div class="col-md-9">              
                <input class='form-control' type='text' name='userNameRaw' id='userNameRaw' value='' onKeyUp="setEngUserName(this.value, 'userName')"/>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function init()
{
	dateTimeInit($("#Language").val());
    $("#search_by_date").prop('checked', true);
    $("#search_by_date").trigger("change");
    setDateByStringAndId('startDate_js', '<%=datestr%>');
    setDateByStringAndId('endDate_js', '<%=datestr%>');
    add1WithEnd = false;
    processNewCalendarDateAndSubmit();
    addables = [0, 0, 0, 1, 1, 0, 0];
}
function PreprocessBeforeSubmiting()
{
	if($("#suggested_drug_0").val() == "")
		{
		console.log("resetting");
			$("#drugInformationType_select_0").val("");
		}
}
function drugClicked(rowId, drugText, drugId, form, stock)
{
	console.log("drugClicked with " + rowId + ", drugId = " + drugId + ", form = " + form);
	$("#suggested_drug_" + rowId).val(drugText);
	$("#suggested_drug_" + rowId).attr("style", "width: " + ($("#suggested_drug_" + rowId).val().length + 1)*8 + "px !important");
	
	 $("#drug_modal_textdiv_" + rowId).html(stock);
     $("#totalCurrentStock_text_" + rowId).val(stock);

	$("#drugInformationType_select_" + rowId).val(drugId);
	$("#quantity_number_" + rowId).attr("max", stock);
	
	$("#drug_ul_" + rowId).html("");
	//$("#drug_ul_" + rowId).css("display", "none");
}

function patient_inputted(userName, orgId) {
    console.log("patient_inputted " + userName);
    $("#userName").val(userName);
    $("#userNameRaw").val(userName);
}
</script>