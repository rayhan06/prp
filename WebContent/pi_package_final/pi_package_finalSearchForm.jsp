<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="pi_package_final.*" %>
<%@ page import="util.*" %>

<%@ page import="java.util.ArrayList" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page pageEncoding="UTF-8" %>

<%
    String navigator2 = "navPI_PACKAGE_FINAL";
    String servletName = "Pi_package_finalServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><%=UtilCharacter.getDataByLanguage(Language, "প্যাকেজ নাম্বার ইংলিশ", "Package Number English")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "প্যাকেজ নাম্বার বাংলা", "Package Number Bangla")%>
            </th>
            <th><%=LM.getText(LC.PI_PACKAGE_NEW_ADD_PACKAGENAMEEN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_PACKAGE_NEW_ADD_PACKAGENAMEBN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_PACKAGE_NEW_SEARCH_PI_PACKAGE_NEW_EDIT_BUTTON, loginDTO)%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "ডিলিট করুন", "Delete")%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<Pi_package_finalDTO>) rn2.list;
            try {
                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Pi_package_finalDTO model = (Pi_package_finalDTO) data.get(i);
        %>
        <tr>
            <input type="hidden" id="packageId" value="<%=model.iD%>">
            <td>
                <%
                    value = model.packageNumberEn + "";
                %>
                <%=value%>
            </td>
            <td>
                <%
                    value = model.packageNumberBn + "";
                %>
                <%=value%>
            </td>
            <td>
                <%
                    value = model.packageNameEn + "";
                %>
                <%=value%>
            </td>
            <td>
                <%
                    value = model.packageNameBn + "";
                %>
                <%=value%>
            </td>

            <%CommonDTO commonDTO = model; %>
            <%@include file="../pb/searchAndViewButton.jsp" %>

            <td class="text-center">
                <button
                        type="button"
                        class="btn btn-sm remove-btn shadow ml-2 pl-4"
                        onclick="deletePackage(this)">
                    <i class="fa fa-trash"></i>
                </button>
            </td>
        </tr>
        <%
                    }
                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>

<script type="text/javascript">
    function deletePackage(element) {
        let tr = element.parentNode.parentNode;
        let packageId = tr.querySelector("#packageId").value;
        let url = "Pi_package_finalServlet?actionType=deletePackageWithValidation&packageId=" + packageId;
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                response = JSON.parse(response);
                if (response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                    buttonStateChange(false);
                } else if (response.responseCode === 200) {
                    tr.remove();
                    showToastSticky("ডিলিট সফল হয়েছে", "Delete Successful");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChange(false);
            }
        });
    }
</script>