

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="report_heading.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>




<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.REPORT_HEADING_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
Report_headingDAO report_headingDAO = new Report_headingDAO("report_heading");
Report_headingDTO report_headingDTO = (Report_headingDTO)report_headingDAO.getDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<div class="modal-content viewmodal">
            <div class="modal-header">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9 col-sm-12">
                            <h5 class="modal-title">Report Heading Details</h5>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="modal-body container">
			
			<div class="row div_border office-div">

                    <div class="col-md-12">
                        <h5>Report Heading</h5>
                    </div>
			



			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.REPORT_HEADING_EDIT_NAMEEN, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="nameEn">
						
											<%
											value = report_headingDTO.nameEn + "";
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.REPORT_HEADING_EDIT_NAMEBN, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="nameBn">
						
											<%
											value = report_headingDTO.nameBn + "";
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				


			
			
			
		


			</div>	

                <div class="row div_border attachement-div">
                    <div class="col-md-12">
                        <h5>Report Sub Heading</h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=(actionName.equals("edit"))?(LM.getText(LC.REPORT_HEADING_EDIT_REPORT_SUB_HEADING_NAMEEN, loginDTO)):(LM.getText(LC.REPORT_HEADING_ADD_REPORT_SUB_HEADING_NAMEEN, loginDTO))%></th>
								<th><%=(actionName.equals("edit"))?(LM.getText(LC.REPORT_HEADING_EDIT_REPORT_SUB_HEADING_NAMEBN, loginDTO)):(LM.getText(LC.REPORT_HEADING_ADD_REPORT_SUB_HEADING_NAMEBN, loginDTO))%></th>
							</tr>
							<%
                        	ReportSubHeadingDAO reportSubHeadingDAO = new ReportSubHeadingDAO();
                         	List<ReportSubHeadingDTO> reportSubHeadingDTOs = reportSubHeadingDAO.getReportSubHeadingDTOListByReportHeadingID(report_headingDTO.iD);
                         	
                         	for(ReportSubHeadingDTO reportSubHeadingDTO: reportSubHeadingDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%
											value = reportSubHeadingDTO.nameEn + "";
											%>
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = reportSubHeadingDTO.nameBn + "";
											%>
														
											<%=value%>
				
			
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
               


        </div>