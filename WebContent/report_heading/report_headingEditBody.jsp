
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="report_heading.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>


<%
Report_headingDTO report_headingDTO;
report_headingDTO = (Report_headingDTO)request.getAttribute("report_headingDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(report_headingDTO == null)
{
	report_headingDTO = new Report_headingDTO();
	
}
System.out.println("report_headingDTO = " + report_headingDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.REPORT_HEADING_EDIT_REPORT_HEADING_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.REPORT_HEADING_ADD_REPORT_HEADING_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

boolean isPermanentTable = true;
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="Report_headingServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
				
				
				




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.REPORT_HEADING_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=report_headingDTO.iD%>' tag='pb_html'/>
	
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.REPORT_HEADING_EDIT_NAMEEN, loginDTO)):(LM.getText(LC.REPORT_HEADING_ADD_NAMEEN, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'nameEn_div_<%=i%>'>	
		<input type='text' class='form-control'  name='nameEn' id = 'nameEn_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + report_headingDTO.nameEn + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.REPORT_HEADING_EDIT_NAMEBN, loginDTO)):(LM.getText(LC.REPORT_HEADING_ADD_NAMEBN, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'nameBn_div_<%=i%>'>	
		<input type='text' class='form-control'  name='nameBn' id = 'nameBn_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + report_headingDTO.nameBn + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + report_headingDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + report_headingDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
					
	





				<div class="col-md-12" style="padding-top: 20px;">
					<legend class="text-left content_legend"><%=LM.getText(LC.REPORT_HEADING_ADD_REPORT_SUB_HEADING, loginDTO)%></legend>
				</div>

				<div class="col-md-12">

					<div class="table-responsive">
						<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th><%=(actionName.equals("edit"))?(LM.getText(LC.REPORT_HEADING_EDIT_REPORT_SUB_HEADING_NAMEEN, loginDTO)):(LM.getText(LC.REPORT_HEADING_ADD_REPORT_SUB_HEADING_NAMEEN, loginDTO))%></th>
										<th><%=(actionName.equals("edit"))?(LM.getText(LC.REPORT_HEADING_EDIT_REPORT_SUB_HEADING_NAMEBN, loginDTO)):(LM.getText(LC.REPORT_HEADING_ADD_REPORT_SUB_HEADING_NAMEBN, loginDTO))%></th>
										<th><%=LM.getText(LC.REPORT_HEADING_ADD_REPORT_SUB_HEADING_REMOVE, loginDTO)%></th>
									</tr>
								</thead>
								<tbody id="field-ReportSubHeading">
						
						
<%
	if(actionName.equals("edit")){
		int index = -1;
		
		
		for(ReportSubHeadingDTO reportSubHeadingDTO: report_headingDTO.reportSubHeadingDTOList)
		{
			index++;
			
			System.out.println("index index = "+index);

%>	
							
									<tr>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='reportSubHeading.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=reportSubHeadingDTO.iD%>' tag='pb_html'/>
	
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='reportSubHeading.reportHeadingId' id = 'reportHeadingId_hidden_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + reportSubHeadingDTO.reportHeadingId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
										</td>
										<td>										











		<input type='text' class='form-control'  name='reportSubHeading.nameEn' id = 'nameEn_text_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + reportSubHeadingDTO.nameEn + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
										</td>
										<td>										











		<input type='text' class='form-control'  name='reportSubHeading.nameBn' id = 'nameBn_text_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + reportSubHeadingDTO.nameBn + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='reportSubHeading.isDeleted' id = 'isDeleted_hidden_<%=childTableStartingID%>' value= <%=actionName.equals("edit")?("'" + reportSubHeadingDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='reportSubHeading.lastModificationTime' id = 'lastModificationTime_hidden_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + reportSubHeadingDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
										</td>
										<td><div class='checker'><span id='chkEdit' ><input type='checkbox' id='reportSubHeading_cb_<%=index%>' name='checkbox' value=''/></span></div></td>
									</tr>								
<%	
			childTableStartingID ++;
		}
	}
%>						
						
								</tbody>
							</table>
						
						
						
					</div>
					<div class="form-group">
						<div class="col-xs-9 text-right">

							<button id="remove-ReportSubHeading" name="removeReportSubHeading" type="button"
									class="btn btn-danger remove-me1"><%=LM.getText(LC.REPORT_HEADING_ADD_REPORT_SUB_HEADING_REMOVE, loginDTO)%></button>

							<button id="add-more-ReportSubHeading" name="add-moreReportSubHeading" type="button"
									class="btn btn-primary"><%=LM.getText(LC.REPORT_HEADING_ADD_REPORT_SUB_HEADING_ADD_MORE, loginDTO)%></button>

						</div>
					</div>
					
					<%ReportSubHeadingDTO reportSubHeadingDTO = new ReportSubHeadingDTO();%>
					
					<template id="template-ReportSubHeading" >						
								<tr>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='reportSubHeading.iD' id = 'iD_hidden_' value='<%=reportSubHeadingDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='reportSubHeading.reportHeadingId' id = 'reportHeadingId_hidden_' value=<%=actionName.equals("edit")?("'" + reportSubHeadingDTO.reportHeadingId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
									</td>
									<td>











		<input type='text' class='form-control'  name='reportSubHeading.nameEn' id = 'nameEn_text_' value=<%=actionName.equals("edit")?("'" + reportSubHeadingDTO.nameEn + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
									</td>
									<td>











		<input type='text' class='form-control'  name='reportSubHeading.nameBn' id = 'nameBn_text_' value=<%=actionName.equals("edit")?("'" + reportSubHeadingDTO.nameBn + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='reportSubHeading.isDeleted' id = 'isDeleted_hidden_' value= <%=actionName.equals("edit")?("'" + reportSubHeadingDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='reportSubHeading.lastModificationTime' id = 'lastModificationTime_hidden_' value=<%=actionName.equals("edit")?("'" + reportSubHeadingDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
									</td>
									<td><div><span id='chkEdit' ><input type='checkbox' name='checkbox' value=''/></span></div></td>
								</tr>								
						
					</template>
				</div>		

				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						%>
						<%=LM.getText(LC.REPORT_HEADING_EDIT_REPORT_HEADING_CANCEL_BUTTON, loginDTO)%>
						<%
					}
					else
					{
						%>
						<%=LM.getText(LC.REPORT_HEADING_ADD_REPORT_HEADING_CANCEL_BUTTON, loginDTO)%>
						<%
					}
					
					%>
					</a>
					<button class="btn btn-success" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						%>
						<%=LM.getText(LC.REPORT_HEADING_EDIT_REPORT_HEADING_SUBMIT_BUTTON, loginDTO)%>
						<%
					}
					else
					{
						%>
						<%=LM.getText(LC.REPORT_HEADING_ADD_REPORT_HEADING_SUBMIT_BUTTON, loginDTO)%>
						<%
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">




function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}


	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Report_headingServlet");	
}

function init(row)
{


	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	
}

var row = 0;
	
window.onload =function ()
{
	init(row);
	CKEDITOR.replaceAll();
}

var child_table_extra_id = <%=childTableStartingID%>;

	$("#add-more-ReportSubHeading").click(
        function(e) 
		{
            e.preventDefault();
            var t = $("#template-ReportSubHeading");

            $("#field-ReportSubHeading").append(t.html());
			SetCheckBoxValues("field-ReportSubHeading");
			
			var tr = $("#field-ReportSubHeading").find("tr:last-child");
			
			tr.attr("id","ReportSubHeading_" + child_table_extra_id);
			
			tr.find("[tag='pb_html']").each(function( index ) 
			{
				var prev_id = $( this ).attr('id');
				$( this ).attr('id', prev_id + child_table_extra_id);
				console.log( index + ": " + $( this ).attr('id') );
			});
			
			
			child_table_extra_id ++;

        });

    
      $("#remove-ReportSubHeading").click(function(e){    	
	    var tablename = 'field-ReportSubHeading';
	    var i = 0;
		console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
		var element = document.getElementById(tablename);

		var j = 0;
		for(i = document.getElementById(tablename).childNodes.length - 1; i >= 0 ; i --)
		{
			var tr = document.getElementById(tablename).childNodes[i];
			if(tr.nodeType === Node.ELEMENT_NODE)
			{
				console.log("tr.childNodes.length= " + tr.childNodes.length);
				var checkbox = tr.querySelector('input[type="checkbox"]');				
				if(checkbox.checked == true)
				{
					tr.remove();
				}
				j ++;
			}
			
		}    	
    });


</script>






