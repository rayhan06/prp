<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="asset_manufacturer.*" %>
<%@page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>
<%@page language="java" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.*" %>
<%@page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="geolocation.*" %>


<%
    String servletName = "Asset_manufacturerServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.ASSET_MANUFACTURER_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Asset_manufacturerDAO asset_manufacturerDAO = new Asset_manufacturerDAO("asset_manufacturer");
    Asset_manufacturerDTO asset_manufacturerDTO = asset_manufacturerDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid p-0" id="kt_content">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=LM.getText(LC.ASSET_MANUFACTURER_ADD_ASSET_MANUFACTURER_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped text-nowrap">
                <tr>
                    <td><b><%=LM.getText(LC.ASSET_MANUFACTURER_ADD_NAMEEN, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = asset_manufacturerDTO.nameEn + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>
                    </td>

                </tr>

                <tr>
                    <td><b><%=LM.getText(LC.ASSET_MANUFACTURER_ADD_NAMEBN, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = asset_manufacturerDTO.nameBn + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>

                <tr>
                    <td>
                        <b><%=LM.getText(LC.ASSET_MANUFACTURER_ADD_COMPANYWEBSITE, loginDTO)%>
                        </b></td>
                    <td>

                        <%
                            value = asset_manufacturerDTO.companyWebsite + "";
                        %>
                        <a href='Asset_manufacturerServlet?actionType=getURL&URL=<%=value%>'>Link</a>


                    </td>

                </tr>

                <tr>
                    <td>
                        <b><%=LM.getText(LC.ASSET_MANUFACTURER_ADD_SUPPORTWEBSITE, loginDTO)%>
                        </b></td>
                    <td>

                        <%
                            value = asset_manufacturerDTO.supportWebsite + "";
                        %>
                        <a href='Asset_manufacturerServlet?actionType=getURL&URL=<%=value%>'>Link</a>


                    </td>

                </tr>


                <tr>
                    <td>
                        <b><%=LM.getText(LC.ASSET_MANUFACTURER_ADD_MANUFACTURERCONTACTNAME, loginDTO)%>
                        </b></td>
                    <td>

                        <%
                            value = asset_manufacturerDTO.manufacturerContactName + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>

                <tr>
                    <td>
                        <b><%=LM.getText(LC.ASSET_MANUFACTURER_ADD_SUPPORTCONTACT1, loginDTO)%>
                        </b></td>
                    <td>

                        <%
                            value = asset_manufacturerDTO.supportContact1 + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>

                <tr>
                    <td>
                        <b><%=LM.getText(LC.ASSET_MANUFACTURER_ADD_SUPPORTCONTACT2, loginDTO)%>
                        </b></td>
                    <td>

                        <%
                            value = asset_manufacturerDTO.supportContact2 + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>

                <tr>
                    <td>
                        <b><%=LM.getText(LC.ASSET_MANUFACTURER_ADD_SUPPORTCONTACT3, loginDTO)%>
                        </b></td>
                    <td>

                        <%
                            value = asset_manufacturerDTO.supportContact3 + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>

                <tr>
                    <td>
                        <b><%=LM.getText(LC.ASSET_MANUFACTURER_ADD_SUPPORTEMAIL, loginDTO)%>
                        </b></td>
                    <td>

                        <%
                            value = asset_manufacturerDTO.supportEmail + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>

                <tr>
                    <td>
                        <b><%=LM.getText(LC.ASSET_MANUFACTURER_ADD_COMPANYADDRESS, loginDTO)%>
                        </b></td>
                    <td>

                        <%
                            value = asset_manufacturerDTO.companyAddress + "";
                        %>
                        <%=GeoLocationDAO2.getAddressToShow(value, Language)%>

                    </td>

                </tr>

                <tr>
                    <td>
                        <b><%=LM.getText(LC.ASSET_MANUFACTURER_ADD_IMAGEDROPZONE, loginDTO)%>
                        </b></td>
                    <td>

                        <%value = asset_manufacturerDTO.imageDropzone + "";%>
                        <%List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(asset_manufacturerDTO.imageDropzone);%>
                        <%@include file="../pb/dropzoneViewer.jsp" %>

                    </td>

                </tr>

            </table>
            </div>
        </div>
    </div>
</div>