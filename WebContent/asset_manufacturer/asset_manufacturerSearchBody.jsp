<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="util.RecordNavigator" %>
<%@page language="java" %>
<%@page import="java.util.ArrayList" %>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%
    String url = "Asset_manufacturerServlet?actionType=search";
    String navigator = SessionConstants.NAV_ASSET_MANUFACTURER;
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.APPOINTMENT_EDIT_LANGUAGE, loginDTO);

    String pageno = "";

    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();
    boolean isPermanentTable = rn.m_isPermanentTable;

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    int pagination_number = 0;
%>

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title"><%=LM.getText(LC.ASSET_MANUFACTURER_SEARCH_ASSET_MANUFACTURER_SEARCH_FORMNAME, loginDTO)%></h3>
    </div>
</div>

<!-- end:: Subheader -->


<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

    <div class="row">
        <div class="col-lg-12">

            <jsp:include page="./asset_manufacturerNav.jsp" flush="true">
                <jsp:param name="url" value="<%=url%>"/>
                <jsp:param name="navigator" value="<%=navigator%>"/>
                <jsp:param name="pageName"
                           value="<%=LM.getText(LC.ASSET_MANUFACTURER_SEARCH_ASSET_MANUFACTURER_SEARCH_FORMNAME, loginDTO)%>"/>
            </jsp:include>

            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <form action="Asset_manufacturerServlet?isPermanentTable=<%=isPermanentTable%>&actionType=delete"
                          method="POST" id="tableForm" enctype="multipart/form-data">
                        <jsp:include page="asset_manufacturerSearchForm.jsp" flush="true">
                            <jsp:param name="pageName"
                                       value="<%=LM.getText(LC.ASSET_MANUFACTURER_SEARCH_ASSET_MANUFACTURER_SEARCH_FORMNAME, loginDTO)%>"/>
                        </jsp:include>
                    </form>

                    <% pagination_number = 1;%>
                    <%@include file="../common/pagination_with_go2.jsp" %>

                </div>
            </div>
        </div>
    </div>
</div>

<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        initDeleteCheckBoxes();
        dateTimeInit("<%=Language%>");
    });

</script>