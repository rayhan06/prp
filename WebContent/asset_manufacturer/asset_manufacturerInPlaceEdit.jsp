<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="asset_manufacturer.Asset_manufacturerDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>
<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>

<%
Asset_manufacturerDTO asset_manufacturerDTO = (Asset_manufacturerDTO)request.getAttribute("asset_manufacturerDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(asset_manufacturerDTO == null)
{
	asset_manufacturerDTO = new Asset_manufacturerDTO();
	
}
System.out.println("asset_manufacturerDTO = " + asset_manufacturerDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

long ColumnID;
FilesDAO filesDAO = new FilesDAO();
String servletName = "Asset_manufacturerServlet";
String fileColumnName = "";
%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.ASSET_MANUFACTURER_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=asset_manufacturerDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_nameEn'>")%>
			
	
	<div class="form-inline" id = 'nameEn_div_<%=i%>'>
		<input type='text' class='form-control'  name='nameEn' id = 'nameEn_text_<%=i%>' value='<%=asset_manufacturerDTO.nameEn%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_nameBn'>")%>
			
	
	<div class="form-inline" id = 'nameBn_div_<%=i%>'>
		<input type='text' class='form-control'  name='nameBn' id = 'nameBn_text_<%=i%>' value='<%=asset_manufacturerDTO.nameBn%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_companyWebsite'>")%>
			
	
	<div class="form-inline" id = 'companyWebsite_div_<%=i%>'>
		<input type='url' class='form-control'  name='companyWebsite' id = 'companyWebsite_url_<%=i%>' value='<%=asset_manufacturerDTO.companyWebsite%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_supportWebsite'>")%>
			
	
	<div class="form-inline" id = 'supportWebsite_div_<%=i%>'>
		<input type='url' class='form-control'  name='supportWebsite' id = 'supportWebsite_url_<%=i%>' value='<%=asset_manufacturerDTO.supportWebsite%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_manufacturerContactName'>")%>
			
	
	<div class="form-inline" id = 'manufacturerContactName_div_<%=i%>'>
		<input type='text' class='form-control'  name='manufacturerContactName' id = 'manufacturerContactName_text_<%=i%>' value='<%=asset_manufacturerDTO.manufacturerContactName%>' <% 
	if(!actionName.equals("edit"))
	{
%>
		required="required"  pattern="880[0-9]{10}" title="manufacturerContactName must start with 880, then contain 10 digits"
<%
	}
%>
  tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_supportContact1'>")%>
			
	
	<div class="form-inline" id = 'supportContact1_div_<%=i%>'>
		<input type='text' class='form-control'  name='supportContact1' id = 'supportContact1_text_<%=i%>' value='<%=asset_manufacturerDTO.supportContact1%>' <% 
	if(!actionName.equals("edit"))
	{
%>
		required="required"  pattern="880[0-9]{10}" title="supportContact1 must start with 880, then contain 10 digits"
<%
	}
%>
  tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_supportContact2'>")%>
			
	
	<div class="form-inline" id = 'supportContact2_div_<%=i%>'>
		<input type='text' class='form-control'  name='supportContact2' id = 'supportContact2_text_<%=i%>' value='<%=asset_manufacturerDTO.supportContact2%>' <% 
	if(!actionName.equals("edit"))
	{
%>
		required="required"  pattern="880[0-9]{10}" title="supportContact2 must start with 880, then contain 10 digits"
<%
	}
%>
  tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_supportContact3'>")%>
			
	
	<div class="form-inline" id = 'supportContact3_div_<%=i%>'>
		<input type='text' class='form-control'  name='supportContact3' id = 'supportContact3_text_<%=i%>' value='<%=asset_manufacturerDTO.supportContact3%>' <% 
	if(!actionName.equals("edit"))
	{
%>
		required="required"  pattern="880[0-9]{10}" title="supportContact3 must start with 880, then contain 10 digits"
<%
	}
%>
  tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_supportEmail'>")%>
			
	
	<div class="form-inline" id = 'supportEmail_div_<%=i%>'>
		<input type='text' class='form-control'  name='supportEmail' id = 'supportEmail_text_<%=i%>' value='<%=asset_manufacturerDTO.supportEmail%>' <% 
	if(!actionName.equals("edit"))
	{
%>
		required="required"  pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$" title="supportEmail must be a of valid email address format"
<%
	}
%>
  tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_companyAddress'>")%>
			
	
	<div class="form-inline" id = 'companyAddress_div_<%=i%>'>
		<div id ='companyAddress_geoDIV_<%=i%>' tag='pb_html'>
			<select class='form-control' name='companyAddress_active' id = 'companyAddress_geoSelectField_<%=i%>' onChange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'companyAddress', this.getAttribute('row'))"  tag='pb_html' row = '<%=i%>'></select>
		</div>
		<input type='text' class='form-control' name='companyAddress_text' id = 'companyAddress_geoTextField_<%=i%>' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(asset_manufacturerDTO.companyAddress)  + "'"):("'" + "" + "'")%>
 placeholder='Road Number, House Number etc' tag='pb_html'>
		<input type='hidden' class='form-control'  name='companyAddress' id = 'companyAddress_geolocation_<%=i%>' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseID(asset_manufacturerDTO.companyAddress)  + "'"):("'" + "1" + "'")%>
 tag='pb_html'>
		<%
		if(actionName.equals("edit"))
		{
		%>
		<label class="control-label"><%=GeoLocationDAO2.parseText(asset_manufacturerDTO.companyAddress, Language) + "," + GeoLocationDAO2.parseDetails(asset_manufacturerDTO.companyAddress)%></label>
		<%
		}
		%>
			
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_imageDropzone'>")%>
			
	
	<div class="form-inline" id = 'imageDropzone_div_<%=i%>'>
		<%
		fileColumnName = "imageDropzone";
		if(actionName.equals("edit"))
		{
			List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(asset_manufacturerDTO.imageDropzone);
			%>			
			<%@include file="../pb/dropzoneEditor.jsp"%>
			<%
		}
		else
		{
			ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
			asset_manufacturerDTO.imageDropzone = ColumnID;
		}
		%>
				
		<div class="dropzone" action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=asset_manufacturerDTO.imageDropzone%>">
			<input type='file' style="display:none"  name='<%=fileColumnName%>File' id = '<%=fileColumnName%>_dropzone_File_<%=i%>'  tag='pb_html'/>			
		</div>								
		<input type='hidden'  name='<%=fileColumnName%>FilesToDelete' id = '<%=fileColumnName%>FilesToDelete_<%=i%>' value=''  tag='pb_html'/>
		<input type='hidden' name='<%=fileColumnName%>' id = '<%=fileColumnName%>_dropzone_<%=i%>'  tag='pb_html' value='<%=asset_manufacturerDTO.imageDropzone%>'/>		
		


	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_searchColumn" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=asset_manufacturerDTO.searchColumn%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedByUserId" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=asset_manufacturerDTO.insertedByUserId%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedByOrganogramId" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value='<%=asset_manufacturerDTO.insertedByOrganogramId%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=asset_manufacturerDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModifierUser" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModifierUser' id = 'lastModifierUser_hidden_<%=i%>' value='<%=asset_manufacturerDTO.lastModifierUser%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=asset_manufacturerDTO.isDeleted%>' tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=asset_manufacturerDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Asset_manufacturerServlet?actionType=view&ID=<%=asset_manufacturerDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Asset_manufacturerServlet?actionType=view&modal=1&ID=<%=asset_manufacturerDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	