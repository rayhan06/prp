<%@page pageEncoding="UTF-8" %>

<%@page import="asset_manufacturer.*" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="files.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.ASSET_MANUFACTURER_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_ASSET_MANUFACTURER;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Asset_manufacturerDTO asset_manufacturerDTO = (Asset_manufacturerDTO) request.getAttribute("asset_manufacturerDTO");
    CommonDTO commonDTO = asset_manufacturerDTO;
    String servletName = "Asset_manufacturerServlet";


    System.out.println("asset_manufacturerDTO = " + asset_manufacturerDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Asset_manufacturerDAO asset_manufacturerDAO = (Asset_manufacturerDAO) request.getAttribute("asset_manufacturerDAO");

    FilesDAO filesDAO = new FilesDAO();

    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td id='<%=i%>_nameEn'>
    <%
        value = asset_manufacturerDTO.nameEn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_nameBn'>
    <%
        value = asset_manufacturerDTO.nameBn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_companyWebsite'>
    <%
        value = asset_manufacturerDTO.companyWebsite + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_supportWebsite'>
    <%
        value = asset_manufacturerDTO.supportWebsite + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_manufacturerContactName'>
    <%
        value = asset_manufacturerDTO.manufacturerContactName + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_supportContact1'>
    <%
        value = asset_manufacturerDTO.supportContact1 + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_supportContact2'>
    <%
        value = asset_manufacturerDTO.supportContact2 + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_supportContact3'>
    <%
        value = asset_manufacturerDTO.supportContact3 + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_supportEmail'>
    <%
        value = asset_manufacturerDTO.supportEmail + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_companyAddress'>
    <%
        value = asset_manufacturerDTO.companyAddress + "";
    %>
    <%=GeoLocationDAO2.getAddressToShow(value, Language)%>


</td>


<td>
	<button
			type="button"
			class="btn-sm border-0 shadow bg-light btn-border-radius"
			style="color: #ff6b6b;"
			onclick="location.href='Asset_manufacturerServlet?actionType=view&ID=<%=asset_manufacturerDTO.iD%>'">
		<i class="fa fa-eye"></i>
	</button>
</td>

<td id='<%=i%>_Edit'>
	<button
			class="btn-sm border-0 shadow btn-border-radius text-white"
			style="background-color: #ff6b6b;"
			onclick="location.href='Asset_manufacturerServlet?actionType=getEditPage&ID=<%=asset_manufacturerDTO.iD%>'">
		<i class="fa fa-edit"></i>
	</button>
</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'>
			<input type='checkbox' name='ID' value='<%=asset_manufacturerDTO.iD%>'/>
		</span>
    </div>
</td>
																						
											

