<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="asset_manufacturer.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<%@page import="pb.*" %>
<%
    Asset_manufacturerDTO asset_manufacturerDTO;
    asset_manufacturerDTO = (Asset_manufacturerDTO) request.getAttribute("asset_manufacturerDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (asset_manufacturerDTO == null) {
        asset_manufacturerDTO = new Asset_manufacturerDTO();
    }
    System.out.println("asset_manufacturerDTO = " + asset_manufacturerDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.ASSET_MANUFACTURER_ADD_ASSET_MANUFACTURER_ADD_FORMNAME, loginDTO);
    String servletName = "Asset_manufacturerServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID = -1;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;

    String Language = LM.getText(LC.ASSET_MANUFACTURER_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form"
              action="Asset_manufacturerServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <input type='hidden' class='form-control' name='iD'
                                           id='iD_hidden_<%=i%>' value='<%=asset_manufacturerDTO.iD%>'
                                           tag='pb_html'/>

                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4  col-form-label text-md-right">
                                            <%=LM.getText(LC.ASSET_MANUFACTURER_ADD_NAMEEN, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 ">
                                            <input type='text' class='form-control' name='nameEn'
                                                   id='nameEn_text_<%=i%>'
                                                   value='<%=asset_manufacturerDTO.nameEn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row">

                                        <label class="col-md-4  col-form-label text-md-right">
                                            <%=LM.getText(LC.ASSET_MANUFACTURER_ADD_NAMEBN, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 ">
                                            <input type='text' class='form-control' name='nameBn'
                                                   id='nameBn_text_<%=i%>'
                                                   value='<%=asset_manufacturerDTO.nameBn%>'
                                                   tag='pb_html'/>
                                        </div>

                                    </div>


                                    <div class="form-group row">

                                        <label class="col-md-4  col-form-label text-md-right">
                                            <%=LM.getText(LC.ASSET_MANUFACTURER_ADD_COMPANYWEBSITE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 ">
                                            <input type='text' class='form-control'
                                                   name='companyWebsite' id='companyWebsite_url_<%=i%>'
                                                   value='<%=asset_manufacturerDTO.companyWebsite%>'
                                                   tag='pb_html'/>
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label class="col-md-4  col-form-label text-md-right">
                                            <%=LM.getText(LC.ASSET_MANUFACTURER_ADD_SUPPORTWEBSITE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 ">
                                            <input type='text' class='form-control'
                                                   name='supportWebsite' id='supportWebsite_url_<%=i%>'
                                                   value='<%=asset_manufacturerDTO.supportWebsite%>'
                                                   tag='pb_html'/>
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label class="col-md-4  col-form-label text-md-right">
                                            <%=LM.getText(LC.ASSET_MANUFACTURER_ADD_MANUFACTURERCONTACTNAME, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 ">
                                            <input type='text' class='form-control'
                                                   name='manufacturerContactName'
                                                   id='manufacturerContactName_text_<%=i%>'
                                                   value='<%=asset_manufacturerDTO.manufacturerContactName%>'
                                                   tag='pb_html'/>
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label class="col-md-4  col-form-label text-md-right">
                                            <%=LM.getText(LC.ASSET_MANUFACTURER_ADD_SUPPORTCONTACT1, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 ">
                                            <input type='text' class='form-control'
                                                   name='supportContact1'
                                                   id='supportContact1_text_<%=i%>'
                                                   value='<%=asset_manufacturerDTO.supportContact1%>'
                                                    <%if (!actionName.equals("edit")) {%>
                                                   required="required" pattern="880[0-9]{10}"
                                                   title="supportContact1 must start with 880, then contain 10 digits"
                                                    <%}%> tag='pb_html'/>
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label class="col-md-4  col-form-label text-md-right">
                                            <%=LM.getText(LC.ASSET_MANUFACTURER_ADD_SUPPORTCONTACT2, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 ">
                                            <input type='text' class='form-control'
                                                   name='supportContact2'
                                                   id='supportContact2_text_<%=i%>'
                                                   value='<%=asset_manufacturerDTO.supportContact2%>'
                                                    <%if (!actionName.equals("edit")) {%>
                                                   pattern="880[0-9]{10}"
                                                   title="supportContact2 must start with 880, then contain 10 digits"
                                                    <%}%> tag='pb_html'/>
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label class="col-md-4  col-form-label text-md-right">
                                            <%=LM.getText(LC.ASSET_MANUFACTURER_ADD_SUPPORTCONTACT3, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 ">
                                            <input type='text' class='form-control'
                                                   name='supportContact3'
                                                   id='supportContact3_text_<%=i%>'
                                                   value='<%=asset_manufacturerDTO.supportContact3%>'
                                                    <%if (!actionName.equals("edit")) {%>
                                                   pattern="880[0-9]{10}"
                                                   title="supportContact3 must start with 880, then contain 10 digits"
                                                    <%}%> tag='pb_html'/>
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label class="col-md-4  col-form-label text-md-right">
                                            <%=LM.getText(LC.ASSET_MANUFACTURER_ADD_SUPPORTEMAIL, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 ">
                                            <input type='text' class='form-control' name='supportEmail'
                                                   id='supportEmail_text_<%=i%>'
                                                   value='<%=asset_manufacturerDTO.supportEmail%>'
                                                    <%if (!actionName.equals("edit")) {%>
                                                   pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$"
                                                   title="supportEmail must be a of valid email address format"
                                                    <%}%> tag='pb_html'/>
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label class="col-md-4  col-form-label text-md-right">
                                            <%=LM.getText(LC.ASSET_MANUFACTURER_ADD_COMPANYADDRESS, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 ">

                                            <div id='companyAddress_geoDIV_<%=i%>' tag='pb_html'>
                                                <select class='form-control'
                                                        name='companyAddress_active'
                                                        id='companyAddress_geoSelectField_<%=i%>'
                                                        onChange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'companyAddress', this.getAttribute('row'))"
                                                        tag='pb_html' row='<%=i%>'></select>
                                            </div>
                                            <input type='text' class='form-control'
                                                   name='companyAddress_text'
                                                   id='companyAddress_geoTextField_<%=i%>'
                                                   onkeypress="return (event.charCode != 36 && event.keyCode != 36)"
                                                   value='<%=actionName.equals("edit")?GeoLocationDAO2.parseDetails(asset_manufacturerDTO.companyAddress):""%>'
                                                   placeholder='Road Number, House Number etc'
                                                   tag='pb_html'>
                                            <input type='hidden' class='form-control'
                                                   name='companyAddress'
                                                   id='companyAddress_geolocation_<%=i%>'
                                                   value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseID(asset_manufacturerDTO.companyAddress)  + "'"):("'" + "1" + "'")%>
                                                           tag='pb_html'>
                                            <%if (actionName.equals("edit")) {%>
                                            ` <label
                                                class="control-label"><%=GeoLocationDAO2.parseText(asset_manufacturerDTO.companyAddress, Language) + "," + GeoLocationDAO2.parseDetails(asset_manufacturerDTO.companyAddress)%>
                                        </label>
                                            <%}%>

                                        </div>

                                    </div>


                                    <div class="form-group row">

                                        <label class="col-md-4  col-form-label text-md-right">
                                            <%=LM.getText(LC.ASSET_MANUFACTURER_ADD_IMAGEDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 ">

                                            <%
                                                fileColumnName = "imageDropzone";
                                                if (actionName.equals("edit")) {
                                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(asset_manufacturerDTO.imageDropzone);
                                            %>
                                            <%@include file="../pb/dropzoneEditor.jsp" %>
                                            <%
                                                } else {
                                                    ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                    asset_manufacturerDTO.imageDropzone = ColumnID;
                                                }
                                            %>

                                            <div class="dropzone"
                                                 action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=asset_manufacturerDTO.imageDropzone%>">
                                                <input type='file' style="display:none"
                                                       name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>'
                                                       tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value=''
                                                   tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>'
                                                   tag='pb_html'
                                                   value='<%=asset_manufacturerDTO.imageDropzone%>'/>

                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=asset_manufacturerDTO.searchColumn%>'
                                           tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=asset_manufacturerDTO.insertedByUserId%>'
                                           tag='pb_html'/>

                                    <input type='hidden' class='form-control'
                                           name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=asset_manufacturerDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=asset_manufacturerDTO.insertionDate%>'
                                           tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='lastModifierUser'
                                           id='lastModifierUser_hidden_<%=i%>'
                                           value='<%=asset_manufacturerDTO.lastModifierUser%>'
                                           tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value='<%=asset_manufacturerDTO.isDeleted%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control'
                                           name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=asset_manufacturerDTO.lastModificationTime%>'
                                           tag='pb_html'/>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 text-right mt-3">
                        <a class="btn btn-sm shadow cancel-btn text-light btn-border-radius" href="<%=request.getHeader("referer")%>">
                            <%=LM.getText(LC.ASSET_MANUFACTURER_ADD_ASSET_MANUFACTURER_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button class="btn  btn-sm shadow submit-btn text-light btn-border-radius ml-2" type="submit">
                            <%=LM.getText(LC.ASSET_MANUFACTURER_ADD_ASSET_MANUFACTURER_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<script type="text/javascript">

    function PreprocessBeforeSubmiting(row, validate) {

        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;
            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }
        }
        return preprocessGeolocationBeforeSubmitting('companyAddress', row, false);
        //return true;
    }

    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Asset_manufacturerServlet");
    }

    function init(row) {
        initGeoLocation('companyAddress_geoSelectField_', row, "Asset_manufacturerServlet");
    }

    var row = 0;

    $(document).ready(function () {

        init(row);
        dateTimeInit("<%=Language%>");
        CKEDITOR.replaceAll();
    });

    var child_table_extra_id = <%=childTableStartingID%>;

</script>






