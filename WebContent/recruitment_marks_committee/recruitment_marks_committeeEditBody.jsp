<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="recruitment_marks_committee.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="employee_assign.EmployeeSearchIds" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="recruitment_marks_committee_members.Recruitment_marks_committee_membersDTO" %>
<%@ page import="recruitment_marks_committee_members.Recruitment_marks_committee_membersDAO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDAO" %>
<%@ page import="recruitment_marks_committee_members.Recruitment_marks_committee_membersRepository" %>

<%
    Recruitment_marks_committeeDTO recruitment_marks_committeeDTO;
    recruitment_marks_committeeDTO = (Recruitment_marks_committeeDTO) request.getAttribute("recruitment_marks_committeeDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (recruitment_marks_committeeDTO == null) {
        recruitment_marks_committeeDTO = new Recruitment_marks_committeeDTO();

    }
    System.out.println("recruitment_marks_committeeDTO = " + recruitment_marks_committeeDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_ADD_RECRUITMENT_MARKS_COMMITTEE_ADD_FORMNAME, loginDTO);
    String servletName = "Recruitment_marks_committeeServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");


    List<Recruitment_marks_committee_membersDTO> membersDTOS = null;
    if (actionName.equalsIgnoreCase("edit")) {
        membersDTOS = Recruitment_marks_committee_membersRepository.getInstance()
                .getRecruitment_marks_committee_membersDTOByrecruitment_marks_committee_id(recruitment_marks_committeeDTO.iD);
//                new Recruitment_marks_committee_membersDAO().getAllRecruitment_marks_committee_membersByCommitteeId
//                (recruitment_marks_committeeDTO.iD);
    }
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Recruitment_marks_committeeServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="formSubmit()">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-9 offset-md-1">
                                    <div class="row mx-2 mx-md-0">
                                        <div class="col-lg-12">
                                            <div class="sub_title_top">
                                                <div class="sub_title">
                                                    <h4 style="background: white"><%=formTitle%>
                                                    </h4>
                                                </div>
                                            </div>
                                            <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                                   value='<%=recruitment_marks_committeeDTO.iD%>' tag='pb_html'/>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_ADD_NAME, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type='text' class='form-control' name='name'
                                                           id='name_text_<%=i%>'
                                                           value='<%=recruitment_marks_committeeDTO.name%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%--                                                            Members--%>
                                                    <%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_ADD_SEARCHCOLUMN, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type="hidden" class='form-control' name='memberId'
                                                           id='memberId'
                                                           value=''>
                                                    <button type="button"
                                                            class="btn btn-primary btn-block shadow btn-border-radius mb-3"
                                                            id="tag_member_modal_button">
                                                        <%=LM.getText(LC.HM_SELECT, loginDTO)%>
                                                    </button>
                                                    <table class="table-responsive">
                                                        <table class="table table-bordered table-striped">
                                                            <thead></thead>
                                                            <tbody id="tagged_member_table">
                                                            <tr style="display: none;">
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <button type="button"
                                                                            class="btn btn-danger btn-block"
                                                                            onclick="remove_containing_row(this,'tagged_member_table');">
                                                                        Remove
                                                                    </button>
                                                                </td>
                                                            </tr>

                                                            <% if (membersDTOS != null) { %>
                                                            <%for (Recruitment_marks_committee_membersDTO memberDTO : membersDTOS) {%>
                                                            <tr>
                                                                <td><%=Employee_recordsRepository.getInstance()
                                                                        .getById(memberDTO.employeeRecordId).employeeNumber%>
                                                                </td>
                                                                <td>
                                                                    <%=isLanguageEnglish ? (memberDTO.employeeRecordName)
                                                                            : (memberDTO.employeeRecordNameBn)%>
                                                                </td>

                                                                <%
                                                                    String postName = isLanguageEnglish ? (memberDTO.postName + ", " + memberDTO.unitName)
                                                                            : (memberDTO.postNameBn + ", " + memberDTO.unitNameBn);
                                                                %>

                                                                <td><%=postName%>
                                                                </td>
                                                                <td id='<%=memberDTO.employeeRecordId%>_td_button'>
                                                                    <button
                                                                            type="button"
                                                                            class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4 pb-2"
                                                                            onclick="remove_containing_row(this,'tagged_member_table');"
                                                                    >
                                                                        <i class="fa fa-trash"></i>
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                            <%}%>
                                                            <%}%>

                                                            </tbody>
                                                        </table>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_ADD_RECRUITMENTJOBDESCRIPTIONIDS, loginDTO)%>
                                                    <%--                                                            Jobs--%>
                                                </label>
                                                <div class="col-md-9">
                                                    <select multiple="multiple" class='form-control' name='jobs'
                                                            id='jobs' tag='pb_html'>
                                                        <%=new Recruitment_job_descriptionDAO().getBuildOptions(Language,recruitment_marks_committeeDTO.recruitmentJobDescriptionIds)%>
                                                        <%--								<option value='-1'><%=Language.equals("English")?"Select":"বাছাই করুন"%> </option>--%>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-3 col-form-label text-md-right">
                                                    <span style="font-family: 'Roboto', 'SolaimanLipi', sans-serif;">
                                                        <%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH_SELECT_JOBS, loginDTO)%>
                                                    </span>
                                                </div>
                                                <div class="col-md-9 d-flex align-items-center">
                                                    <input class="mr-2"
                                                           id="chkall"
                                                           type="checkbox">
                                                </div>
                                            </div>
                                            <%--                                                    <input id="chkall" type="checkbox" >Select All--%>


                                            <%--													<div class="form-group row">--%>
                                            <%--                                                            <label class="col-4 col-form-label text-md-right"><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_ADD_RECRUITMENTJOBDESCRIPTIONIDS, loginDTO)%></label>--%>
                                            <%--                                                            <div class="col-8">--%>
                                            <%--																<input type='text' class='form-control'  name='recruitmentJobDescriptionIds' id = 'recruitmentJobDescriptionIds_text_<%=i%>' value='<%=recruitment_marks_committeeDTO.recruitmentJobDescriptionIds%>' 																  tag='pb_html'/>					--%>
                                            <%--															</div>--%>
                                            <%--                                                      </div>									--%>
                                            <input type='hidden' class='form-control' name='searchColumn'
                                                   id='searchColumn_hidden_<%=i%>'
                                                   value='<%=recruitment_marks_committeeDTO.searchColumn%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertionDate'
                                                   id='insertionDate_hidden_<%=i%>'
                                                   value='<%=recruitment_marks_committeeDTO.insertionDate%>'
                                                   tag='pb_html'/>
                                            <%--													<div class="form-group row">--%>
                                            <%--                                                            <label class="col-4 col-form-label text-md-right"><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_ADD_INSERTEDBY, loginDTO)%></label>--%>
                                            <%--                                                            <div class="col-8">--%>
                                            <%--																<input type='text' class='form-control'  name='insertedBy' id = 'insertedBy_text_<%=i%>' value='<%=recruitment_marks_committeeDTO.insertedBy%>' 																  tag='pb_html'/>					--%>
                                            <%--															</div>--%>
                                            <%--                                                      </div>									--%>
                                            <%--													<div class="form-group row">--%>
                                            <%--                                                            <label class="col-4 col-form-label text-md-right"><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_ADD_MODIFIEDBY, loginDTO)%></label>--%>
                                            <%--                                                            <div class="col-8">--%>
                                            <%--																<input type='text' class='form-control'  name='modifiedBy' id = 'modifiedBy_text_<%=i%>' value='<%=recruitment_marks_committeeDTO.modifiedBy%>' 																  tag='pb_html'/>					--%>
                                            <%--															</div>--%>
                                            <%--                                                      </div>									--%>
                                            <input type='hidden' class='form-control' name='isDeleted'
                                                   id='isDeleted_hidden_<%=i%>'
                                                   value='<%=recruitment_marks_committeeDTO.isDeleted%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='lastModificationTime'
                                                   id='lastModificationTime_hidden_<%=i%>'
                                                   value='<%=recruitment_marks_committeeDTO.lastModificationTime%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                            <%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_ADD_RECRUITMENT_MARKS_COMMITTEE_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                            <%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_ADD_RECRUITMENT_MARKS_COMMITTEE_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">
    let lang = "<%=Language%>";

    function PreprocessBeforeSubmiting() {

        let data = added_member_info_map.keys().next();
        if (!(data && data.value)) {
            toastr.error("Please Select At least One Member");
            return false;
        }


        document.getElementById('memberId').value = JSON.stringify(
            Array.from(added_member_info_map.values()));

        processMultipleSelectBoxBeforeSubmit("jobs");
        return true;
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function formSubmit(){
        event.preventDefault();
        let form = $("#bigform");
        form.validate();
        let valid = form.valid() && PreprocessBeforeSubmiting();
        if(valid){
            buttonStateChange(true);
            let url = form.attr('action');
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        }
    }

    let row = 0;

    $("#chkall").click(function () {
        if ($("#chkall").is(':checked')) {
            $("#jobs > option").prop("selected", "selected");
            $("#jobs").trigger("change");
        } else {
            $('#jobs').val(null).trigger('change');
            // $("#jobs > option").removeAttr("selected");
            // $("#jobs").trigger("change");
        }
    });
    $(document).ready(function () {

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                name: "required",
            },
            messages: {
                name: "নাম দিন ",
            }
        });

        select2MultiSelector("#jobs", '<%=Language%>');
    });

    <%
    List<EmployeeSearchIds> addedOnulipiIdsList = null;
            if(membersDTOS != null){
                addedOnulipiIdsList = membersDTOS.stream()
                                                       .map(dto -> new EmployeeSearchIds(dto.employeeRecordId,dto.unitId,dto.postId))
                                                       .collect(Collectors.toList());
            }
    %>
    let added_member_info_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(addedOnulipiIdsList)%>);


    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_member_table', {
                info_map: added_member_info_map,
                isSingleEntry: false
            }],

        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    $('#tag_member_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_member_table';
        $('#search_emp_modal').modal();
    });

</script>






