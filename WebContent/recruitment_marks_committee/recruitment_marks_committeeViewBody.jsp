<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="recruitment_marks_committee.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="recruitment_marks_committee_members.Recruitment_marks_committee_membersDAO" %>

<%
    String servletName = "Recruitment_marks_committeeServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Recruitment_marks_committeeDTO recruitment_marks_committeeDTO = Recruitment_marks_committeeRepository.getInstance().
            getRecruitment_marks_committeeDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important; margin-bottom: -18px">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_ADD_RECRUITMENT_MARKS_COMMITTEE_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div>
                <h5 class="table-title">
                    <%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_ADD_RECRUITMENT_MARKS_COMMITTEE_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">
                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_ADD_NAME, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = recruitment_marks_committeeDTO.name + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_ADD_RECRUITMENTJOBDESCRIPTIONIDS, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = Recruitment_marks_committeeDAO.getJobDetails
                                            (recruitment_marks_committeeDTO, Language);
                                %>

                                <%=value%>


                            </td>

                        </tr>


                        <%--							<tr>--%>
                        <%--								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_ADD_INSERTEDBY, loginDTO)%></b></td>--%>
                        <%--								<td>--%>
                        <%--						--%>
                        <%--											<%--%>
                        <%--											value = recruitment_marks_committeeDTO.insertedBy + "";--%>
                        <%--											%>--%>
                        <%--														--%>
                        <%--											<%=Utils.getDigits(value, Language)%>--%>
                        <%--				--%>
                        <%--			--%>
                        <%--								</td>--%>
                        <%--						--%>
                        <%--							</tr>--%>

                        <%--				--%>


                        <%--			--%>

                        <%--							<tr>--%>
                        <%--								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_ADD_MODIFIEDBY, loginDTO)%></b></td>--%>
                        <%--								<td>--%>
                        <%--						--%>
                        <%--											<%--%>
                        <%--											value = recruitment_marks_committeeDTO.modifiedBy + "";--%>
                        <%--											%>--%>
                        <%--														--%>
                        <%--											<%=Utils.getDigits(value, Language)%>--%>
                        <%--				--%>
                        <%--			--%>
                        <%--								</td>--%>
                        <%--						--%>
                        <%--							</tr>--%>


                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- <div class="modal-content viewmodal"> -->
<%--<div class="menubottom">--%>
<%--            <div class="modal-header">--%>
<%--                <div class="col-md-12">--%>
<%--                    <div class="row">--%>
<%--                        <div class="col-md-9 col-sm-12">--%>
<%--                            <h5 class="modal-title"><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_ADD_RECRUITMENT_MARKS_COMMITTEE_ADD_FORMNAME, loginDTO)%></h5>--%>
<%--                        </div>--%>
<%--                        <div class="col-md-3 col-sm-12">--%>
<%--                            <div class="row">--%>
<%--                                <div class="col-md-6">--%>
<%--                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>--%>
<%--                                </div>--%>
<%--                                <div class="col-md-6">--%>
<%--                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </div>--%>


<%--            </div>--%>

<%--            <div class="modal-body container">--%>
<%--			--%>
<%--			<div class="row div_border office-div">--%>

<%--                    <div class="col-md-12">--%>
<%--                        <h3><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_ADD_RECRUITMENT_MARKS_COMMITTEE_ADD_FORMNAME, loginDTO)%></h3>--%>
<%--						<table class="table table-bordered table-striped">--%>
<%--									--%>

<%--							<tr>--%>
<%--								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_ADD_NAME, loginDTO)%></b></td>--%>
<%--								<td>--%>
<%--						--%>
<%--											<%--%>
<%--											value = recruitment_marks_committeeDTO.name + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--								</td>--%>
<%--						--%>
<%--							</tr>--%>

<%--				--%>


<%--			--%>

<%--							<tr>--%>
<%--								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_ADD_RECRUITMENTJOBDESCRIPTIONIDS, loginDTO)%></b></td>--%>
<%--								<td>--%>
<%--						--%>
<%--											<%--%>
<%--											value = Recruitment_marks_committeeDAO.getJobDetails--%>
<%--													(recruitment_marks_committeeDTO, Language);--%>
<%--											%>--%>
<%--														--%>
<%--											<%=value%>--%>
<%--				--%>
<%--			--%>
<%--								</td>--%>
<%--						--%>
<%--							</tr>--%>

<%--				--%>


<%--			--%>
<%--			--%>
<%--			--%>

<%--&lt;%&ndash;							<tr>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_ADD_INSERTEDBY, loginDTO)%></b></td>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;											&lt;%&ndash;%>--%>
<%--&lt;%&ndash;											value = recruitment_marks_committeeDTO.insertedBy + "";&ndash;%&gt;--%>
<%--&lt;%&ndash;											%>&ndash;%&gt;--%>
<%--&lt;%&ndash;														&ndash;%&gt;--%>
<%--&lt;%&ndash;											<%=Utils.getDigits(value, Language)%>&ndash;%&gt;--%>
<%--&lt;%&ndash;				&ndash;%&gt;--%>
<%--&lt;%&ndash;			&ndash;%&gt;--%>
<%--&lt;%&ndash;								</td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;							</tr>&ndash;%&gt;--%>

<%--&lt;%&ndash;				&ndash;%&gt;--%>


<%--&lt;%&ndash;			&ndash;%&gt;--%>

<%--&lt;%&ndash;							<tr>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_ADD_MODIFIEDBY, loginDTO)%></b></td>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;											&lt;%&ndash;%>--%>
<%--&lt;%&ndash;											value = recruitment_marks_committeeDTO.modifiedBy + "";&ndash;%&gt;--%>
<%--&lt;%&ndash;											%>&ndash;%&gt;--%>
<%--&lt;%&ndash;														&ndash;%&gt;--%>
<%--&lt;%&ndash;											<%=Utils.getDigits(value, Language)%>&ndash;%&gt;--%>
<%--&lt;%&ndash;				&ndash;%&gt;--%>
<%--&lt;%&ndash;			&ndash;%&gt;--%>
<%--&lt;%&ndash;								</td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;							</tr>&ndash;%&gt;--%>

<%--				--%>


<%--			--%>
<%--			--%>
<%--			--%>
<%--		--%>
<%--						</table>--%>
<%--                    </div>--%>
<%--			--%>


<%--			</div>	--%>

<%--               --%>


<%--        </div>--%>
<%--	</div>--%>