<%@page pageEncoding="UTF-8" %>

<%@page import="recruitment_marks_committee.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_RECRUITMENT_MARKS_COMMITTEE;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Recruitment_marks_committeeDTO recruitment_marks_committeeDTO = (Recruitment_marks_committeeDTO) request.getAttribute("recruitment_marks_committeeDTO");
    CommonDTO commonDTO = recruitment_marks_committeeDTO;
    String servletName = "Recruitment_marks_committeeServlet";


    System.out.println("recruitment_marks_committeeDTO = " + recruitment_marks_committeeDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Recruitment_marks_committeeDAO recruitment_marks_committeeDAO = (Recruitment_marks_committeeDAO) request.getAttribute("recruitment_marks_committeeDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td id='<%=i%>_name'>
    <%
        value = recruitment_marks_committeeDTO.name + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<%--<td id='<%=i%>_recruitmentJobDescriptionIds'>--%>
<%--    <%--%>
<%--        value = Recruitment_marks_committeeDAO.getJobDetails--%>
<%--                (recruitment_marks_committeeDTO, Language);--%>
<%--    %>--%>

<%--    <%=value%>--%>


<%--</td>--%>


<%--		--%>
<%--		--%>
<%--		--%>
<%--											<td id = '<%=i%>_insertedBy'>--%>
<%--											<%--%>
<%--											value = recruitment_marks_committeeDTO.insertedBy + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_modifiedBy'>--%>
<%--											<%--%>
<%--											value = recruitment_marks_committeeDTO.modifiedBy + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>


<%--											<td>--%>
<%--												<button class="btn btn-sm border-0 shadow" style="background-color: #22ccc1; color: white; border-radius: 8px"--%>
<%--											            >--%>
<%--											        <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>--%>
<%--											    </button>												--%>
<%--											</td>--%>


<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Recruitment_marks_committeeServlet?actionType=view&ID=<%=recruitment_marks_committeeDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<%--											<td id = '<%=i%>_Edit'>																																	--%>
<%--	--%>
<%--												<button class="btn btn-sm border-0 shadow" style="background-color: #cc22c1; color: white; border-radius: 8px"--%>
<%--											            onclick="edit('<%=recruitment_marks_committeeDTO.iD%>')">--%>
<%--											        <%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_SEARCH_RECRUITMENT_MARKS_COMMITTEE_EDIT_BUTTON, loginDTO)%>--%>
<%--											    </button>--%>
<%--																				--%>
<%--											</td>--%>

<td id='<%=i%>_Edit' class="text-center">
    <button class="btn-sm border-0 shadow btn-border-radius text-white"
            onclick="edit('<%=recruitment_marks_committeeDTO.iD%>')"
            style="background-color: #ff6b6b;"
    >
        <i class="fa fa-edit"></i>
    </button>
</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=recruitment_marks_committeeDTO.iD%>'/></span>
    </div>
</td>


<script>

    function edit(id) {
        event.preventDefault();
        window.location = 'Recruitment_marks_committeeServlet?actionType=getEditPage&ID=' + id;

    }

    function view(id) {
        event.preventDefault();
        window.location = 'Recruitment_marks_committeeServlet?actionType=view&ID=' + id;

    }
</script>
