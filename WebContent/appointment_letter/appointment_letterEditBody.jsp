<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="appointment_letter.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_assign.EmployeeSearchIds" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="appointment_letter_onulipi.Appointment_letter_onulipiDTO" %>
<%@ page import="appointment_letter_onulipi.Appointment_letter_onulipiDAO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDAO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="config.GlobalConfigurationRepository" %>
<%@ page import="config.GlobalConfigConstants" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionRepository" %>
<%@ page import="appointment_letter_onulipi.Appointment_letter_onulipiRepository" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameRepository" %>

<%
    Appointment_letterDTO appointment_letterDTO;
    appointment_letterDTO = (Appointment_letterDTO) request.getAttribute("appointment_letterDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (appointment_letterDTO == null) {
        appointment_letterDTO = new Appointment_letterDTO();

    }
    System.out.println("appointment_letterDTO = " + appointment_letterDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.APPOINTMENT_LETTER_ADD_APPOINTMENT_LETTER_ADD_FORMNAME, loginDTO);
    String servletName = "Appointment_letterServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;


    String Language = LM.getText(LC.APPOINTMENT_LETTER_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");

    List<Appointment_letter_onulipiDTO> onulipiDTOS = null;
    if (actionName.equalsIgnoreCase("edit")) {
        onulipiDTOS = Appointment_letter_onulipiRepository.getInstance().
                getAppointment_letter_onulipiDTOByappointment_letter_id(appointment_letterDTO.iD);
//                new Appointment_letter_onulipiDAO().getAllAppointment_letter_onulipiByAppointmentLetterId(appointment_letterDTO.iD);
    }


    String template = (GlobalConfigurationRepository.
            getGlobalConfigDTOByID(GlobalConfigConstants.APPOINTMENT_LETTER_RULES_TEMPLATE).value);
    int year = Calendar.getInstance().get(Calendar.YEAR);

%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important; margin-bottom: -18px">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal"
              action="Appointment_letterServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="formSubmit()">
            <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                   value='<%=appointment_letterDTO.iD%>' tag='pb_html'/>
            <input type='hidden' class='form-control' name='recruitmentJobDescriptionId'
                   id='recruitmentJobDescriptionId_hidden_<%=i%>'
                   value='<%=appointment_letterDTO.recruitmentJobDescriptionId%>' tag='pb_html'/>
            <input type='hidden' class='form-control' name='orderingEmployeeRecordId'
                   id='orderingEmployeeRecordId_hidden_<%=i%>'
                   value='<%=appointment_letterDTO.orderingEmployeeRecordId%>' tag='pb_html'/>
            <input type='hidden' class='form-control' name='orderingUnitId' id='orderingUnitId_hidden_<%=i%>'
                   value='<%=appointment_letterDTO.orderingUnitId%>' tag='pb_html'/>
            <input type='hidden' class='form-control' name='orderingPostId' id='orderingPostId_hidden_<%=i%>'
                   value='<%=appointment_letterDTO.orderingPostId%>' tag='pb_html'/>

            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label text-md-right">
                                            <%=LM.getText(LC.RECRUITMENT_TEST_NAME_ADD_RECRUITMENT_TEST_NAME_ADD_FORMNAME, loginDTO)%><span
                                                class="required">*</span>
                                        </label>
                                        <div class="col-md-10">
                                            <select class='form-control' name='recruitmentTestNameId'
                                                    id='recruitmentTestNameId'
                                                    onchange="onRecruitmentTestNameChange(this)" tag='pb_html'>
                                                <%= Recruitment_test_nameRepository.getInstance().buildOptions(Language, appointment_letterDTO.recruitmentTestNameId) %>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label text-md-right">
                                            <%=LM.getText(LC.APPOINTMENT_LETTER_ADD_RECRUITMENTJOBDESCRIPTIONID, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-10">

                                            <select onchange="" class='form-control'
                                                    name='recruitmentJobDescriptionType'
                                                    id='recruitmentJobDescriptionType_select_<%=i%>' tag='pb_html'>
<%--                                                <%--%>

<%--                                                    long internShipId = Long.parseLong(GlobalConfigurationRepository.getInstance().--%>
<%--                                                            getGlobalConfigDTOByID(GlobalConfigConstants.INTERNSHIP_ID).value);--%>

<%--//                                                    Recruitment_job_descriptionDAO recruitment_job_descriptionDAO = new Recruitment_job_descriptionDAO();--%>
<%--                                                    List<Recruitment_job_descriptionDTO> recruitment_job_descriptionDTOS =--%>
<%--                                                            Recruitment_job_descriptionRepository.getInstance().getRecruitment_job_descriptionList()--%>
<%--                                                            .stream().filter(j -> j.iD != internShipId).collect(Collectors.toList());--%>


<%--                                                    if (Language.equalsIgnoreCase("english")) { %>--%>
<%--                                                <option value="0">Please Select</option>--%>
<%--                                                <% } else { %>--%>
<%--                                                <option value="0">বাছাই করুন</option>--%>
<%--                                                <% }--%>

<%--                                                    for (int k = 0; k < recruitment_job_descriptionDTOS.size(); k++) {--%>
<%--                                                        Recruitment_job_descriptionDTO job_descriptionDTO = recruitment_job_descriptionDTOS.get(k);--%>
<%--                                                        String optionText = "";--%>
<%--                                                        long optionValue = job_descriptionDTO.iD;--%>
<%--                                                        if (Language.equalsIgnoreCase("english")) {--%>
<%--                                                            optionText = job_descriptionDTO.jobTitleEn;--%>
<%--                                                        } else {--%>
<%--                                                            optionText = job_descriptionDTO.jobTitleBn;--%>
<%--                                                        }--%>

<%--                                                        if (actionName.equals("edit")) {--%>
<%--                                                            if (optionValue == appointment_letterDTO.recruitmentJobDescriptionId) { %>--%>
<%--                                                <option value=<%=optionValue%> selected><%=optionText%>--%>
<%--                                                </option>--%>
<%--                                                <% } else { %>--%>
<%--                                                <option value= <%=optionValue%>><%=optionText%>--%>
<%--                                                </option>--%>
<%--                                                <% }--%>
<%--                                                } else { %>--%>
<%--                                                <option value= <%=optionValue%>><%=optionText%>--%>
<%--                                                </option>--%>
<%--                                                <%--%>
<%--                                                        }--%>

<%--                                                    }--%>
<%--                                                %>--%>
                                            </select>


                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label text-md-right">
                                            <%=LM.getText(LC.APPOINTMENT_LETTER_ONULIPI_SEARCH_JARI_DATE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-10">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="jari-date-js"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                <jsp:param name="END_YEAR" value="<%=year + 1%>"/>
                                            </jsp:include>

                                            <input type='hidden' class='form-control' id='jariDate' name='jariDate'
                                                   value='' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label text-md-right">
                                            <%=LM.getText(LC.APPOINTMENT_LETTER_ADD_CODE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-10">
                                            <input type='text' class='form-control' name='code'
                                                   id='code_text_<%=i%>' value='<%=appointment_letterDTO.code%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label text-md-right">
                                            <%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGEMPLOYEERECORDID, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-10">
                                            <input type="hidden" class='form-control' name='employeeRecordsId'
                                                   id='employeeRecordsId'
                                                   value=''>
                                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius mb-3"
                                                    id="tagEmp_modal_button">
                                                <%=LM.getText(LC.HM_SELECT, userDTO)%>
                                            </button>
                                            <table class="table table-bordered table-striped">
                                                <thead></thead>

                                                <tbody id="tagged_emp_table">
                                                <tr style="display: none;">
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        <button type="button" class="btn btn-danger btn-block"
                                                                onclick="remove_containing_row(this,'tagged_emp_table');">
                                                            Remove
                                                        </button>
                                                    </td>
                                                </tr>

                                                <% if (actionName.equals("edit")) { %>
                                                <tr>
                                                    <td><%=Employee_recordsRepository.getInstance()
                                                            .getById(appointment_letterDTO.orderingEmployeeRecordId).employeeNumber%>
                                                    </td>
                                                    <td>
                                                        <%=isLanguageEnglish ? (appointment_letterDTO.orderingEmployeeRecordName)
                                                                : (appointment_letterDTO.orderingEmployeeRecordNameBn)%>
                                                    </td>

                                                    <%
                                                        String postName = isLanguageEnglish ? (appointment_letterDTO.orderingPostName + ", " + appointment_letterDTO.orderingUnitName)
                                                                : (appointment_letterDTO.orderingPostNameBn + ", " + appointment_letterDTO.orderingUnitNameBn);
                                                    %>

                                                    <td><%=postName%>
                                                    </td>
                                                    <td id='<%=appointment_letterDTO.orderingEmployeeRecordId%>_td_button'>
<%--                                                        <button type="button" class="btn btn-danger btn-block"--%>
<%--                                                                onclick="remove_containing_row(this,'tagged_emp_table');">--%>
<%--                                                            Remove--%>
<%--                                                        </button>--%>
                                                        <button type="button"
                                                                class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4 pb-2"
                                                                onclick="remove_containing_row(this,'tagged_emp_table');">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <%}%>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label text-md-right">
                                            <%=LM.getText(LC.APPOINTMENT_LETTER_ONULIPI_ADD_APPOINTMENT_LETTER_ONULIPI_ADD_FORMNAME, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-10">
                                            <input type="hidden" class='form-control' name='onulipiId'
                                                   id='onulipiId'
                                                   value=''>
                                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius mb-3"
                                                    id="tag_onulipi_modal_button">
                                                <%=LM.getText(LC.HM_SELECT, userDTO)%>
                                            </button>
                                            <table class="table table-bordered table-striped">
                                                <thead></thead>

                                                <tbody id="tagged_onulipi_table">
                                                <tr style="display: none;">
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        <button type="button" class="btn btn-danger btn-block"
                                                                onclick="remove_containing_row(this,'tagged_onulipi_table');">
                                                            Remove
                                                        </button>
                                                    </td>
                                                </tr>

                                                <% if (onulipiDTOS != null) { %>
                                                <%for (Appointment_letter_onulipiDTO onulipiDTO : onulipiDTOS) {%>
                                                <tr>
                                                    <td><%=Employee_recordsRepository.getInstance()
                                                            .getById(onulipiDTO.employeeRecordId).employeeNumber%>
                                                    </td>
                                                    <td>
                                                        <%=isLanguageEnglish ? (onulipiDTO.employeeRecordName)
                                                                : (onulipiDTO.employeeRecordNameBn)%>
                                                    </td>

                                                    <%
                                                        String postName = isLanguageEnglish ? (onulipiDTO.postName + ", " + onulipiDTO.unitName)
                                                                : (onulipiDTO.postNameBn + ", " + onulipiDTO.unitNameBn);
                                                    %>

                                                    <td><%=postName%>
                                                    </td>
                                                    <td id='<%=onulipiDTO.employeeRecordId%>_td_button'>
<%--                                                        <button type="button" class="btn btn-danger btn-block"--%>
<%--                                                                onclick="remove_containing_row(this,'tagged_onulipi_table');">--%>
<%--                                                            Remove--%>
<%--                                                        </button>--%>
                                                        <button type="button"
                                                                class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4 pb-2"
                                                                onclick="remove_containing_row(this,'tagged_onulipi_table');">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <%}%>
                                                <%}%>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label text-md-right">
                                            <%=LM.getText(LC.APPOINTMENT_LETTER_ADD_RULES, loginDTO)%>
                                            <%--														<span class="required"> * </span>--%>
                                        </label>
                                        <div class="col-md-10">
														<textarea type='text' class='form-control'
                                                                  name='rules' id='rules_text_<%=i%>'
                                                                  tag='pb_html'/>
                                            <%=actionName.equals("edit") ? (appointment_letterDTO.rules) : template%>
                                            </textarea>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 text-right">
                        <div class="form-actions mt-3">
                            <a id="cancel-btn" class="btn btn-sm cancel-btn text-white shadow"
                               href="<%=request.getHeader("referer")%>">
                                <%=LM.getText(LC.APPOINTMENT_LETTER_ADD_APPOINTMENT_LETTER_CANCEL_BUTTON, loginDTO)%>
                            </a>
                            <button  id="submit-btn" class="btn btn-sm submit-btn text-white shadow ml-2" type="submit">
                                <%=LM.getText(LC.APPOINTMENT_LETTER_ADD_APPOINTMENT_LETTER_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">

    function onRecruitmentTestNameChange(selectedVal) {

        let recruitmentTestNameId = selectedVal.value;

        if (recruitmentTestNameId !== null && recruitmentTestNameId > 0 && recruitmentTestNameId !== "") {
            const url = 'Recruitment_job_descriptionServlet?actionType=getJobDescriptionByRecTestName&recruitmentTestNameId=' + recruitmentTestNameId +
                '&selectedId=' +<%=appointment_letterDTO.recruitmentJobDescriptionId%>;

            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (response) {
                    document.getElementById('recruitmentJobDescriptionType_select_0').innerHTML = response;
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }


    $(document).ready(function () {

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        dateTimeInit("<%=Language%>");

        $.validator.addMethod('jobSelector', function (value, element) {
            return value != 0;
        });


        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                recruitmentJobDescriptionType: {
                    required: true,
                    jobSelector: true,
                },
                code: "required",
            },
            messages: {
                code: "Please Give code ",
                recruitmentJobDescriptionType: "চাকরি নির্বাচন করুন ",
            }
        });

        init(row);
        CKEDITOR.replaceAll();
    });

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function formSubmit(){
        event.preventDefault();
        let form = $("#bigform");
        form.validate();
        let valid = form.valid() && PreprocessBeforeSubmiting();
        if(valid){
            buttonStateChange(true);
            let url = form.attr('action');
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        }
    }

    function PreprocessBeforeSubmiting() {
        let data = added_employee_info_map.keys().next();
        if (!(data && data.value)) {
            toastr.error("Please Select Person");
            return false;
        }
        document.getElementById('employeeRecordsId').value = JSON.stringify(
            Array.from(added_employee_info_map.values()));

        document.getElementById('onulipiId').value = JSON.stringify(
            Array.from(added_onulipi_info_map.values()));


        $('#jariDate').val(getDateStringById('jari-date-js'));

        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }


        return dateValidator('jari-date-js', true, {
            errorEn: "তারিখ নির্বাচন করুন",
            errorBn: "তারিখ নির্বাচন করুন",
        });
    }


    function init(row) {
        select2SingleSelector('#recruitmentTestNameId', '<%=Language%>');
        select2SingleSelector('#recruitmentJobDescriptionType_select_0', '<%=Language%>');
        <% if(actionName.equalsIgnoreCase("edit")){ %>
            onRecruitmentTestNameChange(document.getElementById('recruitmentTestNameId'));
            setDateByTimestampAndId('jari-date-js', <%=appointment_letterDTO.jari_date%>);

        <% } %>
    }

    var row = 0;

    var child_table_extra_id = <%=childTableStartingID%>;


    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button

    // map to store and send added employee data
    // map to store and send added employee data


    <%
    List<EmployeeSearchIds> addedEmpIdsList = null;
    if(actionName.equals("edit")) {
        addedEmpIdsList = Arrays.asList(new EmployeeSearchIds
        (appointment_letterDTO.orderingEmployeeRecordId,appointment_letterDTO.orderingUnitId,appointment_letterDTO.orderingPostId));
    }
    %>

    added_employee_info_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(addedEmpIdsList)%>);

    <%
    List<EmployeeSearchIds> addedOnulipiIdsList = null;
            if(onulipiDTOS != null){
                addedOnulipiIdsList = onulipiDTOS.stream()
                                                       .map(dto -> new EmployeeSearchIds(dto.employeeRecordId,dto.unitId,dto.postId))
                                                       .collect(Collectors.toList());
            }
    %>
    added_onulipi_info_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(addedOnulipiIdsList)%>);


    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_emp_table', {
                info_map: added_employee_info_map,
                isSingleEntry: true
            }],
            ['tagged_onulipi_table', {
                info_map: added_onulipi_info_map,
                isSingleEntry: false
            }],

        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagEmp_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_emp_table';
        $('#search_emp_modal').modal();
    });

    $('#tag_onulipi_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_onulipi_table';
        $('#search_emp_modal').modal();
    });


</script>

<style>
    .required {
        color: red;
    }
</style>






