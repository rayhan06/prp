<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="appointment_letter.Appointment_letterDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Appointment_letterDTO appointment_letterDTO = (Appointment_letterDTO)request.getAttribute("appointment_letterDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(appointment_letterDTO == null)
{
	appointment_letterDTO = new Appointment_letterDTO();
	
}
System.out.println("appointment_letterDTO = " + appointment_letterDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

String servletName = "Appointment_letterServlet";
String fileColumnName = "";
%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.APPOINTMENT_LETTER_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=appointment_letterDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_recruitmentJobDescriptionId'>")%>
			

		<input type='hidden' class='form-control'  name='recruitmentJobDescriptionId' id = 'recruitmentJobDescriptionId_hidden_<%=i%>' value='<%=appointment_letterDTO.recruitmentJobDescriptionId%>' tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_orderingEmployeeRecordId'>")%>
			

		<input type='hidden' class='form-control'  name='orderingEmployeeRecordId' id = 'orderingEmployeeRecordId_hidden_<%=i%>' value='<%=appointment_letterDTO.orderingEmployeeRecordId%>' tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_orderingUnitId'>")%>
			

		<input type='hidden' class='form-control'  name='orderingUnitId' id = 'orderingUnitId_hidden_<%=i%>' value='<%=appointment_letterDTO.orderingUnitId%>' tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_orderingPostId'>")%>
			

		<input type='hidden' class='form-control'  name='orderingPostId' id = 'orderingPostId_hidden_<%=i%>' value='<%=appointment_letterDTO.orderingPostId%>' tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_code'>")%>
			
	
	<div class="form-inline" id = 'code_div_<%=i%>'>
		<input type='text' class='form-control'  name='code' id = 'code_text_<%=i%>' value='<%=appointment_letterDTO.code%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_orderingEmployeeRecordName'>")%>
			
	
	<div class="form-inline" id = 'orderingEmployeeRecordName_div_<%=i%>'>
		<input type='text' class='form-control'  name='orderingEmployeeRecordName' id = 'orderingEmployeeRecordName_text_<%=i%>' value='<%=appointment_letterDTO.orderingEmployeeRecordName%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_orderingEmployeeRecordNameBn'>")%>
			
	
	<div class="form-inline" id = 'orderingEmployeeRecordNameBn_div_<%=i%>'>
		<input type='text' class='form-control'  name='orderingEmployeeRecordNameBn' id = 'orderingEmployeeRecordNameBn_text_<%=i%>' value='<%=appointment_letterDTO.orderingEmployeeRecordNameBn%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_orderingUnitName'>")%>
			
	
	<div class="form-inline" id = 'orderingUnitName_div_<%=i%>'>
		<input type='text' class='form-control'  name='orderingUnitName' id = 'orderingUnitName_text_<%=i%>' value='<%=appointment_letterDTO.orderingUnitName%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_orderingUnitNameBn'>")%>
			
	
	<div class="form-inline" id = 'orderingUnitNameBn_div_<%=i%>'>
		<input type='text' class='form-control'  name='orderingUnitNameBn' id = 'orderingUnitNameBn_text_<%=i%>' value='<%=appointment_letterDTO.orderingUnitNameBn%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_orderingPostName'>")%>
			
	
	<div class="form-inline" id = 'orderingPostName_div_<%=i%>'>
		<input type='text' class='form-control'  name='orderingPostName' id = 'orderingPostName_text_<%=i%>' value='<%=appointment_letterDTO.orderingPostName%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_orderingPostNameBn'>")%>
			
	
	<div class="form-inline" id = 'orderingPostNameBn_div_<%=i%>'>
		<input type='text' class='form-control'  name='orderingPostNameBn' id = 'orderingPostNameBn_text_<%=i%>' value='<%=appointment_letterDTO.orderingPostNameBn%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_rules'>")%>
			
	
	<div class="form-inline" id = 'rules_div_<%=i%>'>
		<input type='text' class='form-control'  name='rules' id = 'rules_text_<%=i%>' value='<%=appointment_letterDTO.rules%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_searchColumn" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=appointment_letterDTO.searchColumn%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=appointment_letterDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy'>")%>
			
	
	<div class="form-inline" id = 'insertedBy_div_<%=i%>'>
		<input type='text' class='form-control'  name='insertedBy' id = 'insertedBy_text_<%=i%>' value='<%=appointment_letterDTO.insertedBy%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy'>")%>
			
	
	<div class="form-inline" id = 'modifiedBy_div_<%=i%>'>
		<input type='text' class='form-control'  name='modifiedBy' id = 'modifiedBy_text_<%=i%>' value='<%=appointment_letterDTO.modifiedBy%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=appointment_letterDTO.isDeleted%>' tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=appointment_letterDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Appointment_letterServlet?actionType=view&ID=<%=appointment_letterDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Appointment_letterServlet?actionType=view&modal=1&ID=<%=appointment_letterDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	