﻿<%@ page import="job_applicant_application.Job_applicant_applicationDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>


<%
    List<Job_applicant_applicationDTO> job_applicant_applicationDTOS = (List<Job_applicant_applicationDTO>) request.getAttribute("job_applicant_applicationDTOS");
    if(job_applicant_applicationDTOS == null){
        job_applicant_applicationDTOS = new ArrayList<>();
    }
%>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <title>Form</title>
    <%--    <style>--%>

    <%--    html, body {--%>
    <%--        margin: 0;--%>
    <%--        padding: 0;--%>
    <%--    }--%>

    <%--    .container {--%>
    <%--        width: 1170px;--%>
    <%--        margin: 0 auto;--%>
    <%--    }--%>

    <%--    .col {--%>
    <%--        float: left;--%>
    <%--    }--%>

    <%--    .col-25 {--%>
    <%--        width: 25%;--%>
    <%--    }--%>

    <%--    .col-15 {--%>
    <%--        width: 15%;--%>
    <%--    }--%>

    <%--    .col-50 {--%>
    <%--        width: 50%;--%>
    <%--    }--%>

    <%--    .col-100 {--%>
    <%--        width: 100%;--%>
    <%--    }--%>

    <%--    .col-75 {--%>
    <%--        width: 75%;--%>
    <%--    }--%>

    <%--    .col-offset-75 {--%>
    <%--        margin-left: 75%;--%>
    <%--    }--%>

    <%--    .colr {--%>
    <%--        float: right;--%>
    <%--    }--%>

    <%--    .row:after {--%>
    <%--        content: "";--%>
    <%--        display: table;--%>
    <%--        clear: both;--%>
    <%--    }--%>

    <%--    .txt-center {--%>
    <%--        text-align: center;--%>
    <%--    }--%>

    <%--    .ul-text {--%>
    <%--        text-decoration: underline;--%>
    <%--    }--%>

    <%--    .title {--%>
    <%--        padding-top: 56px;--%>
    <%--    }--%>

    <%--    .admit {--%>
    <%--        margin-top: 0;--%>
    <%--    }--%>

    <%--    .txt-right {--%>
    <%--        text-align: right;--%>
    <%--    }--%>

    <%--    .note-row {--%>
    <%--        border: 1px solid #000;--%>
    <%--        padding: 10px;--%>
    <%--    }--%>

    <%--    .note-list {--%>
    <%--        list-style: none;--%>
    <%--    }--%>

    <%--    .note-list li {--%>
    <%--        margin-bottom: 5px;--%>
    <%--    }--%>

    <%--    .note-list li span {--%>
    <%--        margin-right: 10px;--%>
    <%--    }--%>

    <%--    .height-150 {--%>
    <%--        max-height: 150px;--%>
    <%--    }--%>


    <%--    .margin-top-percentage-05 {--%>
    <%--        margin-top: 3%;--%>
    <%--    }--%>


    <%--    @media screen {--%>
    <%--        html, body {--%>
    <%--            font-family: "kalpurushregular" !important;--%>
    <%--            font-size: 12px;--%>
    <%--            box-sizing: border-box;--%>
    <%--            width: 100%;--%>
    <%--            height: 98%;--%>
    <%--            margin: 0px;--%>
    <%--            padding: 0px;--%>
    <%--            font-weight: 300--%>
    <%--        }--%>

    <%--        .container {--%>
    <%--            width: 1170px;--%>
    <%--            margin: 0 auto--%>
    <%--        }--%>

    <%--        .col {--%>
    <%--            float: left--%>
    <%--        }--%>

    <%--        .col-25 {--%>
    <%--            width: 25%--%>
    <%--        }--%>

    <%--        .col-50 {--%>
    <%--            width: 50%--%>
    <%--        }--%>

    <%--        .col-75 {--%>
    <%--            width: 75%;--%>
    <%--        }--%>

    <%--        .col-100 {--%>
    <%--            width: 100%--%>
    <%--        }--%>

    <%--        .col-offset-75 {--%>
    <%--            margin-left: 75%--%>
    <%--        }--%>

    <%--        .colr {--%>
    <%--            float: right--%>
    <%--        }--%>

    <%--        .row:after {--%>
    <%--            content: "";--%>
    <%--            display: table;--%>
    <%--            clear: both--%>
    <%--        }--%>

    <%--        .txt-center {--%>
    <%--            text-align: center--%>
    <%--        }--%>

    <%--        .ul-text {--%>
    <%--            text-decoration: underline--%>
    <%--        }--%>

    <%--        .title {--%>
    <%--            padding-top: 56px--%>
    <%--        }--%>

    <%--        .admit {--%>
    <%--            margin-top: 0--%>
    <%--        }--%>

    <%--        .txt-right {--%>
    <%--            text-align: right--%>
    <%--        }--%>

    <%--        .note-row {--%>
    <%--            border: 1px solid #000;--%>
    <%--            padding: 10px--%>
    <%--        }--%>

    <%--        .note-list {--%>
    <%--            list-style: none--%>
    <%--        }--%>

    <%--        .note-list li {--%>
    <%--            margin-bottom: 5px--%>
    <%--        }--%>

    <%--        .note-list li span {--%>
    <%--            margin-right: 10px--%>
    <%--        }--%>

    <%--        .height-150 {--%>
    <%--            max-height: 150px;--%>
    <%--        }--%>
    <%--        /*.margin-top-percentage-05 {*/--%>
    <%--        /*    margin-top: 5%;*/--%>
    <%--        /*}*/--%>
    <%--    }--%>

    <%--    @media print {--%>
    <%--        @page {--%>
    <%--            size: auto;--%>
    <%--            margin: 0mm;--%>
    <%--        }--%>

    <%--        html, body {--%>
    <%--            font-family: "kalpurushregular" !important;--%>
    <%--            font-size: 12px;--%>
    <%--            box-sizing: border-box;--%>
    <%--            margin: 0px;--%>
    <%--            padding: 0px;--%>
    <%--            font-weight: 300--%>
    <%--        }--%>

    <%--        .pagebreak {--%>
    <%--            page-break-before: always;--%>
    <%--        }--%>

    <%--        .container {--%>
    <%--            width: 1170px;--%>
    <%--            margin: 0 auto--%>
    <%--        }--%>

    <%--        .col {--%>
    <%--            float: left--%>
    <%--        }--%>

    <%--        .col-25 {--%>
    <%--            width: 25%--%>
    <%--        }--%>

    <%--        .col-50 {--%>
    <%--            width: 50%--%>
    <%--        }--%>

    <%--        .col-75 {--%>
    <%--            width: 75%;--%>
    <%--        }--%>

    <%--        .col-100 {--%>
    <%--            width: 100%--%>
    <%--        }--%>

    <%--        .col-offset-75 {--%>
    <%--            margin-left: 75%--%>
    <%--        }--%>

    <%--        .colr {--%>
    <%--            float: right--%>
    <%--        }--%>

    <%--        .txt-center {--%>
    <%--            text-align: center--%>
    <%--        }--%>

    <%--        .ul-text {--%>
    <%--            text-decoration: underline--%>
    <%--        }--%>

    <%--        .title {--%>
    <%--            padding-top: 56px--%>
    <%--        }--%>

    <%--        .admit {--%>
    <%--            margin-top: 0--%>
    <%--        }--%>

    <%--        .txt-right {--%>
    <%--            text-align: right--%>
    <%--        }--%>

    <%--        .note-row {--%>
    <%--            border: 1px solid #000;--%>
    <%--            padding: 0.5em--%>
    <%--        }--%>

    <%--        .note-list {--%>
    <%--            list-style: none--%>
    <%--        }--%>

    <%--        .note-list li {--%>
    <%--            margin-bottom: 5px--%>
    <%--        }--%>

    <%--        .note-list li span {--%>
    <%--            margin-right: 10px--%>
    <%--        }--%>

    <%--        .height-150 {--%>
    <%--            max-height: 150px;--%>
    <%--        }--%>

    <%--        .margin-top-10 {--%>
    <%--            margin-top: 10px;--%>
    <%--        }--%>

    <%--        /*.margin-top-percentage-05 {*/--%>
    <%--        /*    margin-top: 5%;*/--%>
    <%--        /*}*/--%>
    <%--    }--%>
    <%--</style>--%>
</head>
<body id = "random">
<%
    try {

        if (job_applicant_applicationDTOS != null) {
            int size = job_applicant_applicationDTOS.size();
            System.out.println("data not null and size = " + size + " data = " + job_applicant_applicationDTOS);
            for (int i = 0; i < size; i++) {
%>


<%
//    request.setAttribute("parliament_applicantDTO", row);
    request.setAttribute("jobAppApp", job_applicant_applicationDTOS.get(i));
%>

<jsp:include page="./appointment_letter_print_row.jsp">
    <jsp:param name="rownum" value="<%=i%>"/>
</jsp:include>
<%
            }
        }
    } catch (Exception ex) {
        System.out.println("data not null and size = " + ex);
    }
%>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        // generate();
        // downloadPdf("admit_card.pdf", "random");
        printAnyDiv("random")
    });

    function printAnyDiv(divName)
    {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>
</body>

</html>