<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="appointment_letter.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page pageEncoding="UTF-8" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.APPOINTMENT_LETTER_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    Appointment_letterDAO appointment_letterDAO = (Appointment_letterDAO) request.getAttribute("appointment_letterDAO");


    String navigator2 = SessionConstants.NAV_APPOINTMENT_LETTER;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>



<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.RECRUITMENT_TEST_NAME_ADD_RECRUITMENT_TEST_NAME_ADD_FORMNAME, loginDTO)%></th>
            <th><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_RECRUITMENTJOBDESCRIPTIONID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGEMPLOYEERECORDID, loginDTO)%>
            </th>
            <%--								<th><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGUNITID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGPOSTID, loginDTO)%></th>--%>
            <th><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_CODE, loginDTO)%>
            </th>
            <%--								<th><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGEMPLOYEERECORDNAME, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGEMPLOYEERECORDNAMEBN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGUNITNAME, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGUNITNAMEBN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGPOSTNAME, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGPOSTNAMEBN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_RULES, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_INSERTEDBY, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_MODIFIEDBY, loginDTO)%></th>--%>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.APPOINTMENT_LETTER_SEARCH_APPOINTMENT_LETTER_EDIT_BUTTON, loginDTO)%>
            </th>
            <th></th>
			<th class="text-center">
                <span><%="English".equalsIgnoreCase(Language)?"All":"সকল"%></span>
				<div class="d-flex align-items-center justify-content-between">
					<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
						<i class="fa fa-trash"></i>&nbsp;
					</button>
					<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
				</div>
			</th>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_APPOINTMENT_LETTER);

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Appointment_letterDTO appointment_letterDTO = (Appointment_letterDTO) data.get(i);


        %>
        <tr id='tr_<%=i%>'>
            <%

            %>


            <%
                request.setAttribute("appointment_letterDTO", appointment_letterDTO);
            %>

            <jsp:include page="./appointment_letterSearchRow.jsp">
                <jsp:param name="pageName" value="searchrow"/>
                <jsp:param name="rownum" value="<%=i%>"/>
            </jsp:include>


            <%

            %>
        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			