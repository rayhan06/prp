﻿<%@ page import="login.LoginDTO" %>

<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDTO" %>
<%@ page import="appointment_letter_onulipi.Appointment_letter_onulipiDTO" %>
<%@ page import="appointment_letter.Appointment_letterDTO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionRepository" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.CatDAO" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="employee_pay_scale.Employee_pay_scaleDTO" %>
<%@ page import="employee_pay_scale.Employee_pay_scaleRepository" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="util.DateUtils" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        String Language = "bangla";


    Job_applicant_applicationDTO jobAppApp = (Job_applicant_applicationDTO) request.getAttribute("jobAppApp");

    Appointment_letterDTO appointment_letterDTO = (Appointment_letterDTO) request.getAttribute("appointment_letterDTO");

    Recruitment_job_descriptionDTO jobDescriptionDTO =
            (Recruitment_job_descriptionDTO) request.getAttribute("jobDescriptionDTO");


    List<Appointment_letter_onulipiDTO> onulipiDTOS = (List<Appointment_letter_onulipiDTO>) request.getAttribute("onulipiDTOS");
    if(onulipiDTOS == null){
        onulipiDTOS = new ArrayList<>();
    }

    Employee_recordsDTO employee_recordsDTO =
            Employee_recordsRepository.getInstance().getById(appointment_letterDTO.orderingEmployeeRecordId);
    if (employee_recordsDTO == null) {
        employee_recordsDTO = new Employee_recordsDTO();
    }



    SimpleDateFormat format_dateOfExam = new SimpleDateFormat("dd/MM/yyyy");
    String formatted_date = format_dateOfExam.format(new Date(appointment_letterDTO.jari_date));
    String day = UtilCharacter.convertNumberEnToBn(formatted_date.substring(0, 2));
    int monthIndex = Integer.parseInt(formatted_date.substring(3, 5)) - 1;
    String month = pbReport.DateUtils.monthBn[monthIndex];

    String year = UtilCharacter.convertNumberEnToBn(formatted_date.substring(6));
    StringBuilder banglaDate = new StringBuilder().append(day).append(" ").append(month).append(" ").append(year);

//    System.out.println("####");
//    System.out.println(year + " " + day);
//    System.out.println("####");

    StringBuilder mainText = new StringBuilder();
    mainText.append("বাংলাদেশ জাতীয় সংসদ সচিবালয়ের নিম্নবর্ণিত ");
    String grade = CatRepository.getName(Language, "job_grade", jobDescriptionDTO.jobGradeCat);
    mainText.append(" ").append(grade).append(" এর ");
    String postName = jobDescriptionDTO.jobTitleBn;
    mainText.append(postName).append(" পদে নিম্নবর্ণিত প্রার্থীকে তার নামের পাশের বর্ণিত পদ ও বেতনস্কেলে নিম্নোক্ত শর্ত সাপেক্ষে ");
    String empStatus = CatRepository.getName(Language, "employment_status", jobDescriptionDTO.employmentStatusCat);
    mainText.append(empStatus).append("ভাবে নিয়োগ প্রদান করা হলো:");


%>

<%--<%=jobAppApp.applicant_name_bn%>--%>

<div  style="page-break-before: always;">
    <div class="row">
        <div class="col-xs-2">

        </div>

        <div class="col-xs-8" style="text-align: center">
            <b><p > বাংলাদেশ জাতীয় সংসদ সচিবালয় </p></b>
            <p>মানব সম্পদ শাখা - N</p>
            <p class=""> www.parliament.gov.bd </p>
        </div>

        <div class="col-xs-2">

        </div>

    </div>

    <br>
    <br>

    <div class="row">
        <div class="col-xs-2">

        </div>

        <div class="col-xs-8" style="text-align: center">
            <b><p > নিয়োগপত্র </p></b>

        </div>

        <div class="col-xs-2">

        </div>

    </div>

    <br>
    <br>
    <div class="row">

        <div class="col-xs-6">
            নং - <%=appointment_letterDTO.code%>

        </div>

        <div class="col-xs-6">
            <p style="float: right">
                তারিখ  - <%=banglaDate%> খ্রিস্টাব্দ
            </p>


        </div>

    </div>

    <div class="row">
        <div class="col-xs-12">
            <%=mainText%>
        </div>
    </div>

    <br>
<%--    <div class="row">--%>
    <div class="table-responsive">
        <table id="tableData" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th style="width: 5%; vertical-align: middle ; text-align: center;">ক্রমিক নং</th>
                <th style="width: 15%; vertical-align: middle ; text-align: center;">প্রার্থীর রোল নাম্বার</th>
                <th style="width: 30%; vertical-align: middle ; text-align: center;">প্রার্থীর নাম, মাতা ও পিতার নাম </th>
                <th style="width: 15%; vertical-align: middle ; text-align: center;">নিজ জেলা</th>
                <th style="width: 20%; vertical-align: middle ; text-align: center;">পদের নাম </th>
                <th style="width: 15%; vertical-align: middle ; text-align: center;">বেতনস্কেল </th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <td>১</td>
                <td><%=UtilCharacter.convertNumberEnToBn(jobAppApp.rollNumber)%></td>
                <td>
                    <%=jobAppApp.applicant_name_bn%> <br>
                    মাতার নাম : <%=jobAppApp.mother_name%> <br>
                    পিতার নাম : <%=jobAppApp.father_name%>
                </td>
                <td>
                    <%=jobAppApp.district_name_bn%>
                </td>
                <td>
                    <%=jobDescriptionDTO.jobTitleBn%>
                </td>
                <td>
                    <%
                        String value = "";
                        Employee_pay_scaleDTO employee_pay_scaleDTO = Employee_pay_scaleRepository.getInstance().getEmployee_pay_scaleDTOByID(jobDescriptionDTO.employeePayScaleType);
                        if(employee_pay_scaleDTO != null){
                            value = CatRepository.getName(Language, "national_pay_scale_type", employee_pay_scaleDTO.nationalPayScaleCat);

                        }
                    %>

                    টা : <%=value%>

                </td>

            </tr>
            </tbody>

        </table>
    </div>

<%--    </div>--%>

    <br>

    <div class="row">
        <div class="col-xs-12">
            <b>নিয়োগের শর্তাবলী :</b> <br><br>
            <%=appointment_letterDTO.rules%>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-8">
        </div>
        <div class="col-xs-4">
            <p style="text-align: center; float: right">
                    <%
                    byte[] encodeBase64Photo = Base64.encodeBase64(employee_recordsDTO.signature);
                    String defaultPhoto = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAUDBAQEAwUEBAQFBQUGBwwIBwcHBw8LCwkMEQ8SEhEPERETFhwXExQaFRERG" +
                            "CEYGh0dHx8fExciJCIeJBweHx7/wAALCABkASwBAREA/8QAFQABAQAAAAAAAAAAAAAAAAAAAAj/xAAUEAEAAAAAAAAAAAAAAAAAAAAA/9oACAEBAAA" +
                            "/ALLAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" +
                            "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH/9k=";
                %>
                <img
                        src="data:image/jpg;base64,<%=employee_recordsDTO.signature != null ? new String(encodeBase64Photo) : (defaultPhoto)%>"
                        width="50%"
                        id="orderingEmployeeSignature"
                >
            </p>
            <p style="text-align: center; float: right">
                <%=appointment_letterDTO.orderingEmployeeRecordNameBn%> <br>
                <%=appointment_letterDTO.orderingPostNameBn%> <br>
                ফোন : <%=UtilCharacter.convertNumberEnToBn(appointment_letterDTO.ordering_mobile)%> <br>
                ইমেইল : <%=UtilCharacter.convertNumberEnToBn(appointment_letterDTO.ordering_email)%>
            </p>
        </div>
    </div>

    <br>

    <div class="row">

        <div class="col-xs-6">
            নং - <%=appointment_letterDTO.code%>

        </div>

        <div class="col-xs-6">
            <p style="float: right">
                তারিখ  - <%=banglaDate%> খ্রিস্টাব্দ
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            &nbsp;  অবগতি বা অনুলিপির জন্যে প্রেরণ করা হলো: <br> <br>

            <% for(int i = 0; i < onulipiDTOS.size(); i++){
                Appointment_letter_onulipiDTO onulipiDTO = onulipiDTOS.get(i);

            %>

            <%=UtilCharacter.convertNumberEnToBn((i+1) + ". ")%>
            <%=onulipiDTO.postNameBn%>, <%=onulipiDTO.unitNameBn%>
            <br>



            <% }  %>

        </div>
    </div>

    <div class="row">
        <div class="col-xs-8">
        </div>
        <div class="col-xs-4">
            <p style="text-align: center; float: right">
                <%=appointment_letterDTO.orderingEmployeeRecordNameBn%> <br>
                <%=appointment_letterDTO.orderingPostNameBn%>
            </p>
        </div>
    </div>

</div>