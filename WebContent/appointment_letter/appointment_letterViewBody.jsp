<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="appointment_letter.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDAO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionRepository" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>


<%
    String servletName = "Appointment_letterServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.APPOINTMENT_LETTER_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Appointment_letterDTO appointment_letterDTO = Appointment_letterRepository.getInstance().
            getAppointment_letterDTOByID(id);

    StringBuilder employeeName = new StringBuilder();
    if (Language.equalsIgnoreCase("english")) {
        employeeName.append(appointment_letterDTO.orderingEmployeeRecordName).append(", ").
                append(appointment_letterDTO.orderingPostName).append(", ").
                append(appointment_letterDTO.orderingPostName);
    } else {
        employeeName.append(appointment_letterDTO.orderingEmployeeRecordNameBn).append(", ").
                append(appointment_letterDTO.orderingPostNameBn).append(", ").
                append(appointment_letterDTO.orderingPostNameBn);
    }

    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important; margin-bottom: -18px">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.APPOINTMENT_LETTER_ADD_APPOINTMENT_LETTER_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div>
                <h5 class="table-title">
                    <%=LM.getText(LC.APPOINTMENT_LETTER_ADD_APPOINTMENT_LETTER_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_RECRUITMENTJOBDESCRIPTIONID, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    Recruitment_job_descriptionDTO jobDescriptionDTO = Recruitment_job_descriptionRepository.getInstance().
                                            getRecruitment_job_descriptionDTOByID
                                                    (appointment_letterDTO.recruitmentJobDescriptionId);
                                    value = jobDescriptionDTO.jobTitleBn;
                                %>


                                <%=value%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGEMPLOYEERECORDID, loginDTO)%>
                                </b></td>
                            <td>

                                <%=employeeName%>


                            </td>

                        </tr>

                        <%--				--%>


                        <%--			--%>

                        <%--							<tr>--%>
                        <%--								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGUNITID, loginDTO)%></b></td>--%>
                        <%--								<td>--%>
                        <%--						--%>
                        <%--											<%--%>
                        <%--											value = appointment_letterDTO.orderingUnitId + "";--%>
                        <%--											%>--%>
                        <%--														--%>
                        <%--											<%=Utils.getDigits(value, Language)%>--%>
                        <%--				--%>
                        <%--			--%>
                        <%--								</td>--%>
                        <%--						--%>
                        <%--							</tr>--%>

                        <%--				--%>


                        <%--			--%>

                        <%--							<tr>--%>
                        <%--								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGPOSTID, loginDTO)%></b></td>--%>
                        <%--								<td>--%>
                        <%--						--%>
                        <%--											<%--%>
                        <%--											value = appointment_letterDTO.orderingPostId + "";--%>
                        <%--											%>--%>
                        <%--														--%>
                        <%--											<%=Utils.getDigits(value, Language)%>--%>
                        <%--				--%>
                        <%--			--%>
                        <%--								</td>--%>
                        <%--						--%>
                        <%--							</tr>--%>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_CODE, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = appointment_letterDTO.code + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <%--							<tr>--%>
                        <%--								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGEMPLOYEERECORDNAME, loginDTO)%></b></td>--%>
                        <%--								<td>--%>
                        <%--						--%>
                        <%--											<%--%>
                        <%--											value = appointment_letterDTO.orderingEmployeeRecordName + "";--%>
                        <%--											%>--%>
                        <%--														--%>
                        <%--											<%=Utils.getDigits(value, Language)%>--%>
                        <%--				--%>
                        <%--			--%>
                        <%--								</td>--%>
                        <%--						--%>
                        <%--							</tr>--%>

                        <%--				--%>


                        <%--			--%>

                        <%--							<tr>--%>
                        <%--								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGEMPLOYEERECORDNAMEBN, loginDTO)%></b></td>--%>
                        <%--								<td>--%>
                        <%--						--%>
                        <%--											<%--%>
                        <%--											value = appointment_letterDTO.orderingEmployeeRecordNameBn + "";--%>
                        <%--											%>--%>
                        <%--														--%>
                        <%--											<%=Utils.getDigits(value, Language)%>--%>
                        <%--				--%>
                        <%--			--%>
                        <%--								</td>--%>
                        <%--						--%>
                        <%--							</tr>--%>

                        <%--				--%>


                        <%--			--%>

                        <%--							<tr>--%>
                        <%--								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGUNITNAME, loginDTO)%></b></td>--%>
                        <%--								<td>--%>
                        <%--						--%>
                        <%--											<%--%>
                        <%--											value = appointment_letterDTO.orderingUnitName + "";--%>
                        <%--											%>--%>
                        <%--														--%>
                        <%--											<%=Utils.getDigits(value, Language)%>--%>
                        <%--				--%>
                        <%--			--%>
                        <%--								</td>--%>
                        <%--						--%>
                        <%--							</tr>--%>

                        <%--				--%>


                        <%--			--%>

                        <%--							<tr>--%>
                        <%--								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGUNITNAMEBN, loginDTO)%></b></td>--%>
                        <%--								<td>--%>
                        <%--						--%>
                        <%--											<%--%>
                        <%--											value = appointment_letterDTO.orderingUnitNameBn + "";--%>
                        <%--											%>--%>
                        <%--														--%>
                        <%--											<%=Utils.getDigits(value, Language)%>--%>
                        <%--				--%>
                        <%--			--%>
                        <%--								</td>--%>
                        <%--						--%>
                        <%--							</tr>--%>

                        <%--				--%>


                        <%--			--%>

                        <%--							<tr>--%>
                        <%--								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGPOSTNAME, loginDTO)%></b></td>--%>
                        <%--								<td>--%>
                        <%--						--%>
                        <%--											<%--%>
                        <%--											value = appointment_letterDTO.orderingPostName + "";--%>
                        <%--											%>--%>
                        <%--														--%>
                        <%--											<%=Utils.getDigits(value, Language)%>--%>
                        <%--				--%>
                        <%--			--%>
                        <%--								</td>--%>
                        <%--						--%>
                        <%--							</tr>--%>

                        <%--				--%>


                        <%--			--%>

                        <%--							<tr>--%>
                        <%--								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGPOSTNAMEBN, loginDTO)%></b></td>--%>
                        <%--								<td>--%>
                        <%--						--%>
                        <%--											<%--%>
                        <%--											value = appointment_letterDTO.orderingPostNameBn + "";--%>
                        <%--											%>--%>
                        <%--														--%>
                        <%--											<%=Utils.getDigits(value, Language)%>--%>
                        <%--				--%>
                        <%--			--%>
                        <%--								</td>--%>
                        <%--						--%>
                        <%--							</tr>--%>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_RULES, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = appointment_letterDTO.rules + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <%--							<tr>--%>
                        <%--								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_INSERTEDBY, loginDTO)%></b></td>--%>
                        <%--								<td>--%>
                        <%--						--%>
                        <%--											<%--%>
                        <%--											value = appointment_letterDTO.insertedBy + "";--%>
                        <%--											%>--%>
                        <%--														--%>
                        <%--											<%=Utils.getDigits(value, Language)%>--%>
                        <%--				--%>
                        <%--			--%>
                        <%--								</td>--%>
                        <%--						--%>
                        <%--							</tr>--%>


                        <%--							<tr>--%>
                        <%--								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_MODIFIEDBY, loginDTO)%></b></td>--%>
                        <%--								<td>--%>
                        <%--						--%>
                        <%--											<%--%>
                        <%--											value = appointment_letterDTO.modifiedBy + "";--%>
                        <%--											%>--%>
                        <%--														--%>
                        <%--											<%=Utils.getDigits(value, Language)%>--%>
                        <%--				--%>
                        <%--			--%>
                        <%--								</td>--%>
                        <%--						--%>
                        <%--							</tr>--%>


                    </table>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- <div class="modal-content viewmodal"> -->
<%--<div class="menubottom">--%>
<%--            <div class="modal-header">--%>
<%--                <div class="col-md-12">--%>
<%--                    <div class="row">--%>
<%--                        <div class="col-md-9 col-sm-12">--%>
<%--                            <h5 class="modal-title"><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_APPOINTMENT_LETTER_ADD_FORMNAME, loginDTO)%></h5>--%>
<%--                        </div>--%>
<%--                        <div class="col-md-3 col-sm-12">--%>
<%--                            <div class="row">--%>
<%--                                <div class="col-md-6">--%>
<%--                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>--%>
<%--                                </div>--%>
<%--                                <div class="col-md-6">--%>
<%--                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </div>--%>


<%--            </div>--%>

<%--            <div class="modal-body container">--%>
<%--			--%>
<%--			<div class="row div_border office-div">--%>

<%--                    <div class="col-md-12">--%>
<%--                        <h3><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_APPOINTMENT_LETTER_ADD_FORMNAME, loginDTO)%></h3>--%>
<%--						<table class="table table-bordered table-striped">--%>
<%--									--%>

<%--							<tr>--%>
<%--								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_RECRUITMENTJOBDESCRIPTIONID, loginDTO)%></b></td>--%>
<%--								<td>--%>

<%--									<%--%>
<%--										Recruitment_job_descriptionDTO jobDescriptionDTO = Recruitment_job_descriptionRepository.getInstance().--%>
<%--												getRecruitment_job_descriptionDTOByID--%>
<%--														(appointment_letterDTO.recruitmentJobDescriptionId);--%>
<%--										value = jobDescriptionDTO.jobTitleBn;--%>
<%--									%>--%>

<%--														--%>
<%--											<%=value%>--%>
<%--				--%>
<%--			--%>
<%--								</td>--%>
<%--						--%>
<%--							</tr>--%>

<%--				--%>


<%--			--%>

<%--							<tr>--%>
<%--								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGEMPLOYEERECORDID, loginDTO)%></b></td>--%>
<%--								<td>--%>

<%--									<%=employeeName%>--%>


<%--								</td>--%>

<%--							</tr>--%>

<%--&lt;%&ndash;				&ndash;%&gt;--%>


<%--&lt;%&ndash;			&ndash;%&gt;--%>

<%--&lt;%&ndash;							<tr>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGUNITID, loginDTO)%></b></td>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;											&lt;%&ndash;%>--%>
<%--&lt;%&ndash;											value = appointment_letterDTO.orderingUnitId + "";&ndash;%&gt;--%>
<%--&lt;%&ndash;											%>&ndash;%&gt;--%>
<%--&lt;%&ndash;														&ndash;%&gt;--%>
<%--&lt;%&ndash;											<%=Utils.getDigits(value, Language)%>&ndash;%&gt;--%>
<%--&lt;%&ndash;				&ndash;%&gt;--%>
<%--&lt;%&ndash;			&ndash;%&gt;--%>
<%--&lt;%&ndash;								</td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;							</tr>&ndash;%&gt;--%>

<%--&lt;%&ndash;				&ndash;%&gt;--%>


<%--&lt;%&ndash;			&ndash;%&gt;--%>

<%--&lt;%&ndash;							<tr>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGPOSTID, loginDTO)%></b></td>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;											&lt;%&ndash;%>--%>
<%--&lt;%&ndash;											value = appointment_letterDTO.orderingPostId + "";&ndash;%&gt;--%>
<%--&lt;%&ndash;											%>&ndash;%&gt;--%>
<%--&lt;%&ndash;														&ndash;%&gt;--%>
<%--&lt;%&ndash;											<%=Utils.getDigits(value, Language)%>&ndash;%&gt;--%>
<%--&lt;%&ndash;				&ndash;%&gt;--%>
<%--&lt;%&ndash;			&ndash;%&gt;--%>
<%--&lt;%&ndash;								</td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;							</tr>&ndash;%&gt;--%>

<%--				--%>


<%--			--%>

<%--							<tr>--%>
<%--								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_CODE, loginDTO)%></b></td>--%>
<%--								<td>--%>
<%--						--%>
<%--											<%--%>
<%--											value = appointment_letterDTO.code + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--								</td>--%>
<%--						--%>
<%--							</tr>--%>

<%--				--%>


<%--			--%>

<%--&lt;%&ndash;							<tr>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGEMPLOYEERECORDNAME, loginDTO)%></b></td>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;											&lt;%&ndash;%>--%>
<%--&lt;%&ndash;											value = appointment_letterDTO.orderingEmployeeRecordName + "";&ndash;%&gt;--%>
<%--&lt;%&ndash;											%>&ndash;%&gt;--%>
<%--&lt;%&ndash;														&ndash;%&gt;--%>
<%--&lt;%&ndash;											<%=Utils.getDigits(value, Language)%>&ndash;%&gt;--%>
<%--&lt;%&ndash;				&ndash;%&gt;--%>
<%--&lt;%&ndash;			&ndash;%&gt;--%>
<%--&lt;%&ndash;								</td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;							</tr>&ndash;%&gt;--%>

<%--&lt;%&ndash;				&ndash;%&gt;--%>


<%--&lt;%&ndash;			&ndash;%&gt;--%>

<%--&lt;%&ndash;							<tr>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGEMPLOYEERECORDNAMEBN, loginDTO)%></b></td>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;											&lt;%&ndash;%>--%>
<%--&lt;%&ndash;											value = appointment_letterDTO.orderingEmployeeRecordNameBn + "";&ndash;%&gt;--%>
<%--&lt;%&ndash;											%>&ndash;%&gt;--%>
<%--&lt;%&ndash;														&ndash;%&gt;--%>
<%--&lt;%&ndash;											<%=Utils.getDigits(value, Language)%>&ndash;%&gt;--%>
<%--&lt;%&ndash;				&ndash;%&gt;--%>
<%--&lt;%&ndash;			&ndash;%&gt;--%>
<%--&lt;%&ndash;								</td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;							</tr>&ndash;%&gt;--%>

<%--&lt;%&ndash;				&ndash;%&gt;--%>


<%--&lt;%&ndash;			&ndash;%&gt;--%>

<%--&lt;%&ndash;							<tr>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGUNITNAME, loginDTO)%></b></td>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;											&lt;%&ndash;%>--%>
<%--&lt;%&ndash;											value = appointment_letterDTO.orderingUnitName + "";&ndash;%&gt;--%>
<%--&lt;%&ndash;											%>&ndash;%&gt;--%>
<%--&lt;%&ndash;														&ndash;%&gt;--%>
<%--&lt;%&ndash;											<%=Utils.getDigits(value, Language)%>&ndash;%&gt;--%>
<%--&lt;%&ndash;				&ndash;%&gt;--%>
<%--&lt;%&ndash;			&ndash;%&gt;--%>
<%--&lt;%&ndash;								</td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;							</tr>&ndash;%&gt;--%>

<%--&lt;%&ndash;				&ndash;%&gt;--%>


<%--&lt;%&ndash;			&ndash;%&gt;--%>

<%--&lt;%&ndash;							<tr>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGUNITNAMEBN, loginDTO)%></b></td>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;											&lt;%&ndash;%>--%>
<%--&lt;%&ndash;											value = appointment_letterDTO.orderingUnitNameBn + "";&ndash;%&gt;--%>
<%--&lt;%&ndash;											%>&ndash;%&gt;--%>
<%--&lt;%&ndash;														&ndash;%&gt;--%>
<%--&lt;%&ndash;											<%=Utils.getDigits(value, Language)%>&ndash;%&gt;--%>
<%--&lt;%&ndash;				&ndash;%&gt;--%>
<%--&lt;%&ndash;			&ndash;%&gt;--%>
<%--&lt;%&ndash;								</td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;							</tr>&ndash;%&gt;--%>

<%--&lt;%&ndash;				&ndash;%&gt;--%>


<%--&lt;%&ndash;			&ndash;%&gt;--%>

<%--&lt;%&ndash;							<tr>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGPOSTNAME, loginDTO)%></b></td>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;											&lt;%&ndash;%>--%>
<%--&lt;%&ndash;											value = appointment_letterDTO.orderingPostName + "";&ndash;%&gt;--%>
<%--&lt;%&ndash;											%>&ndash;%&gt;--%>
<%--&lt;%&ndash;														&ndash;%&gt;--%>
<%--&lt;%&ndash;											<%=Utils.getDigits(value, Language)%>&ndash;%&gt;--%>
<%--&lt;%&ndash;				&ndash;%&gt;--%>
<%--&lt;%&ndash;			&ndash;%&gt;--%>
<%--&lt;%&ndash;								</td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;							</tr>&ndash;%&gt;--%>

<%--&lt;%&ndash;				&ndash;%&gt;--%>


<%--&lt;%&ndash;			&ndash;%&gt;--%>

<%--&lt;%&ndash;							<tr>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_ORDERINGPOSTNAMEBN, loginDTO)%></b></td>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;											&lt;%&ndash;%>--%>
<%--&lt;%&ndash;											value = appointment_letterDTO.orderingPostNameBn + "";&ndash;%&gt;--%>
<%--&lt;%&ndash;											%>&ndash;%&gt;--%>
<%--&lt;%&ndash;														&ndash;%&gt;--%>
<%--&lt;%&ndash;											<%=Utils.getDigits(value, Language)%>&ndash;%&gt;--%>
<%--&lt;%&ndash;				&ndash;%&gt;--%>
<%--&lt;%&ndash;			&ndash;%&gt;--%>
<%--&lt;%&ndash;								</td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;							</tr>&ndash;%&gt;--%>

<%--				--%>


<%--			--%>

<%--							<tr>--%>
<%--								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_RULES, loginDTO)%></b></td>--%>
<%--								<td>--%>
<%--						--%>
<%--											<%--%>
<%--											value = appointment_letterDTO.rules + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--								</td>--%>
<%--						--%>
<%--							</tr>--%>

<%--				--%>


<%--			--%>
<%--			--%>
<%--			--%>

<%--&lt;%&ndash;							<tr>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_INSERTEDBY, loginDTO)%></b></td>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;											&lt;%&ndash;%>--%>
<%--&lt;%&ndash;											value = appointment_letterDTO.insertedBy + "";&ndash;%&gt;--%>
<%--&lt;%&ndash;											%>&ndash;%&gt;--%>
<%--&lt;%&ndash;														&ndash;%&gt;--%>
<%--&lt;%&ndash;											<%=Utils.getDigits(value, Language)%>&ndash;%&gt;--%>
<%--&lt;%&ndash;				&ndash;%&gt;--%>
<%--&lt;%&ndash;			&ndash;%&gt;--%>
<%--&lt;%&ndash;								</td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;							</tr>&ndash;%&gt;--%>

<%--				--%>


<%--			--%>

<%--&lt;%&ndash;							<tr>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td style="width:30%"><b><%=LM.getText(LC.APPOINTMENT_LETTER_ADD_MODIFIEDBY, loginDTO)%></b></td>&ndash;%&gt;--%>
<%--&lt;%&ndash;								<td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;											&lt;%&ndash;%>--%>
<%--&lt;%&ndash;											value = appointment_letterDTO.modifiedBy + "";&ndash;%&gt;--%>
<%--&lt;%&ndash;											%>&ndash;%&gt;--%>
<%--&lt;%&ndash;														&ndash;%&gt;--%>
<%--&lt;%&ndash;											<%=Utils.getDigits(value, Language)%>&ndash;%&gt;--%>
<%--&lt;%&ndash;				&ndash;%&gt;--%>
<%--&lt;%&ndash;			&ndash;%&gt;--%>
<%--&lt;%&ndash;								</td>&ndash;%&gt;--%>
<%--&lt;%&ndash;						&ndash;%&gt;--%>
<%--&lt;%&ndash;							</tr>&ndash;%&gt;--%>

<%--				--%>


<%--			--%>
<%--			--%>
<%--			--%>
<%--		--%>
<%--						</table>--%>
<%--                    </div>--%>
<%--			--%>


<%--			</div>	--%>

<%--               --%>


<%--        </div>--%>
<%--	</div>--%>