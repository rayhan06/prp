<%@page pageEncoding="UTF-8" %>

<%@page import="appointment_letter.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionRepository" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameRepository" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameDTO" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.APPOINTMENT_LETTER_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_APPOINTMENT_LETTER;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Appointment_letterDTO appointment_letterDTO = (Appointment_letterDTO) request.getAttribute("appointment_letterDTO");
    CommonDTO commonDTO = appointment_letterDTO;
    String servletName = "Appointment_letterServlet";


    System.out.println("appointment_letterDTO = " + appointment_letterDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Appointment_letterDAO appointment_letterDAO = (Appointment_letterDAO) request.getAttribute("appointment_letterDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    StringBuilder employeeName = new StringBuilder();
    if (Language.equalsIgnoreCase("english")) {
        employeeName.append(appointment_letterDTO.orderingEmployeeRecordName).append(", ").
                append(appointment_letterDTO.orderingPostName).append(", ").
                append(appointment_letterDTO.orderingPostName);
    } else {
        employeeName.append(appointment_letterDTO.orderingEmployeeRecordNameBn).append(", ").
                append(appointment_letterDTO.orderingPostNameBn).append(", ").
                append(appointment_letterDTO.orderingPostNameBn);
    }

%>

<td id='<%=i%>_recruitmentTestName'>
    <%

        Recruitment_test_nameDTO recruitment_test_nameDTO = Recruitment_test_nameRepository.getInstance().getRecruitment_test_nameDTOByiD(appointment_letterDTO.recruitmentTestNameId);
        if (appointment_letterDTO.recruitmentTestNameId != -1 && recruitment_test_nameDTO != null && Language.equalsIgnoreCase("english")) {
            value = recruitment_test_nameDTO.nameEn + "";
        } else if (appointment_letterDTO.recruitmentTestNameId != -1 && recruitment_test_nameDTO != null && Language.equalsIgnoreCase("bangla")) {
            value = recruitment_test_nameDTO.nameBn + "";
        }else{
            value = "";
        } %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_recruitmentJobDescriptionId' style="vertical-align: middle">
    <%
        Recruitment_job_descriptionDTO jobDescriptionDTO = Recruitment_job_descriptionRepository.getInstance().
                getRecruitment_job_descriptionDTOByID
                        (appointment_letterDTO.recruitmentJobDescriptionId);
        if (jobDescriptionDTO != null) {
            value = UtilCharacter.getDataByLanguage
                    (Language, jobDescriptionDTO.jobTitleBn, jobDescriptionDTO.jobTitleEn);
        } else {
            value = "";
        }

    %>

    <%=value%>


</td>

<td id='<%=i%>_orderingEmployeeRecordId' style="vertical-align: middle">
    <%--											<%--%>
    <%--											value = appointment_letterDTO.orderingEmployeeRecordId + "";--%>
    <%--											%>--%>

    <%=employeeName%>


</td>

<%--											<td id = '<%=i%>_orderingUnitId'>--%>
<%--											<%--%>
<%--											value = appointment_letterDTO.orderingUnitId + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_orderingPostId'>--%>
<%--											<%--%>
<%--											value = appointment_letterDTO.orderingPostId + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>

<td id='<%=i%>_code'>
    <%
        value = appointment_letterDTO.code + "";
    %>
    <%=Utils.getDigits(value, Language)%>
</td>
<%--		--%>
<%--											<td id = '<%=i%>_orderingEmployeeRecordName'>--%>
<%--											<%--%>
<%--											value = appointment_letterDTO.orderingEmployeeRecordName + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_orderingEmployeeRecordNameBn'>--%>
<%--											<%--%>
<%--											value = appointment_letterDTO.orderingEmployeeRecordNameBn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_orderingUnitName'>--%>
<%--											<%--%>
<%--											value = appointment_letterDTO.orderingUnitName + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_orderingUnitNameBn'>--%>
<%--											<%--%>
<%--											value = appointment_letterDTO.orderingUnitNameBn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_orderingPostName'>--%>
<%--											<%--%>
<%--											value = appointment_letterDTO.orderingPostName + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_orderingPostNameBn'>--%>
<%--											<%--%>
<%--											value = appointment_letterDTO.orderingPostNameBn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_rules'>--%>
<%--											<%--%>
<%--											value = appointment_letterDTO.rules + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--		--%>
<%--		--%>
<%--											<td id = '<%=i%>_insertedBy'>--%>
<%--											<%--%>
<%--											value = appointment_letterDTO.insertedBy + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_modifiedBy'>--%>
<%--											<%--%>
<%--											value = appointment_letterDTO.modifiedBy + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>


<td>
	<button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
			onclick="location.href='Appointment_letterServlet?actionType=view&ID=<%=appointment_letterDTO.iD%>'">
		<i class="fa fa-eye"></i>
	</button>
</td>

<td id='<%=i%>_Edit'>
    <button class="btn-sm border-0 shadow btn-border-radius text-white" style="background-color: #ff6b6b;" onclick="edit('<%=appointment_letterDTO.iD%>')">
        <i class="fa fa-edit"></i>
	</button>
</td>

<td id='<%=i%>_print' style="vertical-align: middle">
<%--    <a href="" onclick="return printA('<%=appointment_letterDTO.iD%>')">--%>
<%--        <%=LM.getText(LC.ADMIT_CARD_SEARCH_PRINT, loginDTO)%>--%>
<%--    </a>--%>
	<button
			class="btn btn-sm btn-primary shadow btn-border-radius pl-4 pb-2"
			onclick="return printA('<%=appointment_letterDTO.iD%>')"
	>
		<i class="fa fa-print"></i>
	</button>
</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'>
			<input type='checkbox' name='ID' value='<%=appointment_letterDTO.iD%>'/>
		</span>
    </div>
</td>


<%--<script>--%>

<%--</script>--%>