<%@page pageEncoding="UTF-8" %>

<%@page import="medical_reagent_name.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.MEDICAL_REAGENT_NAME_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_MEDICAL_REAGENT_NAME;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Medical_reagent_nameDTO medical_reagent_nameDTO = (Medical_reagent_nameDTO) request.getAttribute("medical_reagent_nameDTO");
    CommonDTO commonDTO = medical_reagent_nameDTO;
    String servletName = "Medical_reagent_nameServlet";


    System.out.println("medical_reagent_nameDTO = " + medical_reagent_nameDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Medical_reagent_nameDAO medical_reagent_nameDAO = (Medical_reagent_nameDAO) request.getAttribute("medical_reagent_nameDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td id='<%=i%>_nameEn' class="text-nowrap">
    <%
        value = medical_reagent_nameDTO.nameEn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_nameBn' class="text-nowrap">
    <%
        value = medical_reagent_nameDTO.nameBn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_currentStock' class="text-nowrap">
    <%
        value = medical_reagent_nameDTO.currentStock + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_stockAlertRequired' class="text-nowrap">
    <%
        value = medical_reagent_nameDTO.stockAlertRequired + "";
    %>

    <%=Utils.getYesNo(value, Language)%>


</td>

<td id='<%=i%>_stockAlertQuantity' class="text-nowrap">
    <%
        value = medical_reagent_nameDTO.stockAlertQuantity + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_departmentCat' class="text-nowrap">
    <%
        value = medical_reagent_nameDTO.departmentCat + "";
    %>
    <%
        value = CatDAO.getName(Language, "department", medical_reagent_nameDTO.departmentCat);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_quantityUnitCat' class="text-nowrap">
    <%
        value = medical_reagent_nameDTO.quantityUnitCat + "";
    %>
    <%
        value = CatDAO.getName(Language, "quantity_unit", medical_reagent_nameDTO.quantityUnitCat);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Medical_reagent_nameServlet?actionType=view&ID=<%=medical_reagent_nameDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Medical_reagent_nameServlet?actionType=getEditPage&ID=<%=medical_reagent_nameDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td class="text-right" id='<%=i%>_checkbox'>
	<div class='checker'>
        <span class='chkEdit'>
            <input type='checkbox' name='ID' value='<%=medical_reagent_nameDTO.iD%>'/>
        </span>
	</div>
</td>
																						
											

