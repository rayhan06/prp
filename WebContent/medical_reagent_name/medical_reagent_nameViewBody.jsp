<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="medical_reagent_name.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>


<%
    String servletName = "Medical_reagent_nameServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.MEDICAL_REAGENT_NAME_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Medical_reagent_nameDAO medical_reagent_nameDAO = new Medical_reagent_nameDAO("medical_reagent_name");
    Medical_reagent_nameDTO medical_reagent_nameDTO = medical_reagent_nameDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.MEDICAL_REAGENT_NAME_ADD_MEDICAL_REAGENT_NAME_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div>
                <h5 class="table-title">
                    <%=LM.getText(LC.MEDICAL_REAGENT_NAME_ADD_MEDICAL_REAGENT_NAME_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">


                        <tr>
                            <td R><b><%=LM.getText(LC.MEDICAL_REAGENT_NAME_ADD_NAMEEN, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = medical_reagent_nameDTO.nameEn + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.MEDICAL_REAGENT_NAME_ADD_NAMEBN, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = medical_reagent_nameDTO.nameBn + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.MEDICAL_REAGENT_NAME_ADD_CURRENTSTOCK, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = medical_reagent_nameDTO.currentStock + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.MEDICAL_REAGENT_NAME_ADD_STOCKALERTREQUIRED, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = medical_reagent_nameDTO.stockAlertRequired + "";
                                %>

                                <%=Utils.getYesNo(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.MEDICAL_REAGENT_NAME_ADD_STOCKALERTQUANTITY, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = medical_reagent_nameDTO.stockAlertQuantity + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.MEDICAL_REAGENT_NAME_ADD_DEPARTMENTCAT, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = medical_reagent_nameDTO.departmentCat + "";
                                %>
                                <%
                                    value = CatDAO.getName(Language, "department", medical_reagent_nameDTO.departmentCat);
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.MEDICAL_REAGENT_NAME_ADD_QUANTITYUNITCAT, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = medical_reagent_nameDTO.quantityUnitCat + "";
                                %>
                                <%
                                    value = CatDAO.getName(Language, "quantity_unit", medical_reagent_nameDTO.quantityUnitCat);
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                    </table>
                </div>
            </div>
        </div>
    </div>
</div>