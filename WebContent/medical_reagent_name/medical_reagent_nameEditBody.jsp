<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="medical_reagent_name.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    Medical_reagent_nameDTO medical_reagent_nameDTO;
    medical_reagent_nameDTO = (Medical_reagent_nameDTO) request.getAttribute("medical_reagent_nameDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (medical_reagent_nameDTO == null) {
        medical_reagent_nameDTO = new Medical_reagent_nameDTO();

    }
    System.out.println("medical_reagent_nameDTO = " + medical_reagent_nameDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.MEDICAL_REAGENT_NAME_ADD_MEDICAL_REAGENT_NAME_ADD_FORMNAME, loginDTO);
    String servletName = "Medical_reagent_nameServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.MEDICAL_REAGENT_NAME_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Medical_reagent_nameServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="sub_title_top">
                                                <div class="sub_title">
                                                    <h4 style="background: white"><%=formTitle%>
                                                    </h4>
                                                </div>
                                            </div>
                                            <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                                   value='<%=medical_reagent_nameDTO.iD%>' tag='pb_html'/>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MEDICAL_REAGENT_NAME_ADD_NAMEEN, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type='text' class='form-control' name='nameEn'
                                                           id='nameEn_text_<%=i%>'
                                                           value='<%=medical_reagent_nameDTO.nameEn%>' tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MEDICAL_REAGENT_NAME_ADD_NAMEBN, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type='text' class='form-control' name='nameBn'
                                                           id='nameBn_text_<%=i%>'
                                                           value='<%=medical_reagent_nameDTO.nameBn%>' tag='pb_html'/>
                                                </div>
                                            </div>

                                            <input type='hidden' class='form-control' name='quantityUnit'
                                                   id='quantityUnit_text_<%=i%>'
                                                   value='<%=medical_reagent_nameDTO.quantityUnit%>' tag='pb_html'/>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MEDICAL_REAGENT_NAME_ADD_CURRENTSTOCK, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <%
                                                        value = "";
                                                        if (medical_reagent_nameDTO.currentStock != -1) {
                                                            value = medical_reagent_nameDTO.currentStock + "";
                                                        }
                                                    %>
                                                    <input type='number' class='form-control' name='currentStock'
                                                           id='currentStock_number_<%=i%>' value='<%=value%>'
                                                           tag='pb_html'>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MEDICAL_REAGENT_NAME_ADD_STOCKALERTREQUIRED, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type='checkbox' class='form-control-sm mt-1'
                                                           name='stockAlertRequired'
                                                           id='stockAlertRequired_checkbox_<%=i%>'
                                                           value='true' <%=(String.valueOf(medical_reagent_nameDTO.stockAlertRequired).equals("true"))?("checked"):""%>
                                                           tag='pb_html'>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MEDICAL_REAGENT_NAME_ADD_STOCKALERTQUANTITY, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <%
                                                        value = "";
                                                        if (medical_reagent_nameDTO.stockAlertQuantity != -1) {
                                                            value = medical_reagent_nameDTO.stockAlertQuantity + "";
                                                        }
                                                    %>
                                                    <input type='number' class='form-control' name='stockAlertQuantity'
                                                           id='stockAlertQuantity_number_<%=i%>' value='<%=value%>'
                                                           tag='pb_html'>
                                                </div>
                                            </div>
                                            <input type='hidden' class='form-control' name='insertedByUserId'
                                                   id='insertedByUserId_hidden_<%=i%>'
                                                   value='<%=medical_reagent_nameDTO.insertedByUserId%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                                   id='insertedByOrganogramId_hidden_<%=i%>'
                                                   value='<%=medical_reagent_nameDTO.insertedByOrganogramId%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertionDate'
                                                   id='insertionDate_hidden_<%=i%>'
                                                   value='<%=medical_reagent_nameDTO.insertionDate%>' tag='pb_html'/>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MEDICAL_REAGENT_NAME_ADD_DEPARTMENTCAT, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <select class='form-control' name='departmentCat'
                                                            id='departmentCat_category_<%=i%>' tag='pb_html'>
                                                        <%
                                                            Options = CatDAO.getOptions(Language, "department", medical_reagent_nameDTO.departmentCat);
                                                        %>
                                                        <%=Options%>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MEDICAL_REAGENT_NAME_ADD_QUANTITYUNITCAT, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <select class='form-control' name='quantityUnitCat'
                                                            id='quantityUnitCat_category_<%=i%>' tag='pb_html'>
                                                        <%
                                                            Options = CatDAO.getOptions(Language, "quantity_unit", medical_reagent_nameDTO.quantityUnitCat);
                                                        %>
                                                        <%=Options%>
                                                    </select>

                                                </div>
                                            </div>
                                            <input type='hidden' class='form-control' name='searchColumn'
                                                   id='searchColumn_hidden_<%=i%>'
                                                   value='<%=medical_reagent_nameDTO.searchColumn%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='isDeleted'
                                                   id='isDeleted_hidden_<%=i%>'
                                                   value='<%=medical_reagent_nameDTO.isDeleted%>' tag='pb_html'/>

                                            <input type='hidden' class='form-control' name='lastModificationTime'
                                                   id='lastModificationTime_hidden_<%=i%>'
                                                   value='<%=medical_reagent_nameDTO.lastModificationTime%>'
                                                   tag='pb_html'/>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11">
                        <div class="form-actions text-right mt-3">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.MEDICAL_REAGENT_NAME_ADD_MEDICAL_REAGENT_NAME_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.MEDICAL_REAGENT_NAME_ADD_MEDICAL_REAGENT_NAME_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {


        preprocessCheckBoxBeforeSubmitting('stockAlertRequired', row);

        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Medical_reagent_nameServlet");
    }

    function init(row) {


    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






