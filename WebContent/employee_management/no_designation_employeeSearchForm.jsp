<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="employee_management.*" %>
<%@ page import="util.*" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.util.List" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="pb.Utils" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%
    String navigator2 = "navEMPLOYEE_MANAGEMENT";
    String servletName = "Employee_managementServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>

<%
    List<Employee_managementDTO> data = (List<Employee_managementDTO>) rn2.list;
    if (data != null && data.size() > 0) {
%>
<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=getDataByLanguage(Language, "ক্রমিক নম্বর", "Serial Number")%>
            </th>
            <th><%=LM.getText(LC.HM_USER_NAME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEENG, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO)%>
            </th>
            <th><%=getDataByLanguage(Language, "পার্লামেন্ট যোগদান তারিখ", "Parliament Joining Date")%>
            </th>
            <th><%=getDataByLanguage(Language, "নিয়োগ", "Assign")%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            int index = (rn2.getCurrentPageNo() - 1) * rn2.getPageSize() + 1;
            for (Employee_managementDTO employeeRecordsDTO : data) {
        %>
        <tr>
            <td>
                <%=Utils.getDigits(index, Language)%>
            </td>
            <td><%=isLanguageEnglish ? employeeRecordsDTO.userName : StringUtils.convertToBanNumber(employeeRecordsDTO.userName)%>
            </td>
            <td>
                <%=employeeRecordsDTO.nameEng == null ? "" : employeeRecordsDTO.nameEng%>
            </td>

            <td>
                <%=employeeRecordsDTO.nameBng == null ? "" : employeeRecordsDTO.nameBng%>
            </td>

            <td>
                <%=employeeRecordsDTO.mobileNumber == null ? "" :
                        isLanguageEnglish ? employeeRecordsDTO.mobileNumber
                                : StringUtils.convertToBanNumber(employeeRecordsDTO.mobileNumber)%>
            </td>

            <td>
                <%=StringUtils.getFormattedDate(Language, employeeRecordsDTO.joiningDate)%>
            </td>
            <td>
                <button type="button"
                        class="btn btn-sm btn-info btn-info text-white shadow btn-border-radius"
                        id="select_criteria_btn"
                        onclick="location.href='GenericTreeServlet?actionType=addEmployeeOfficeManagement&userName=<%=employeeRecordsDTO.userName%>'">
                    <%=getDataByLanguage(Language, "নিযুক্ত করুন", "Assign")%>
                </button>
            </td>

        </tr>
        <%
                index++;
            }
        %>

        </tbody>
    </table>
</div>
<%
} else {
%>
<label style="width: 100%;text-align: center;font-size: larger;font-weight: bold;color: red">
    <%=isLanguageEnglish ? "No information is found" : "কোন তথ্য পাওয়া যায় নি"%>
</label>
<%
    }
%>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>