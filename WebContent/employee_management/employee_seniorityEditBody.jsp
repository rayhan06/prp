<%@page pageEncoding="UTF-8" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="employee_seniority.Employee_seniorityDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="util.StringUtils" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="java.util.Collections" %>
<%@ page import="employee_seniority.Employee_seniorityDAO" %>

<%
    boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    String context = request.getContextPath() + "/";
    String formTitle = isLangEn ? "Employee Seniority Serial" : "কর্মকর্তা/কর্মচারী জ্যেষ্ঠতা ক্রম";
    List<Employee_seniorityDTO> employeeSeniorityDTOs = Employee_seniorityDAO.getInstance().getAllDTOs();
    Collections.sort(employeeSeniorityDTOs);
%>

<style>
    .loader-container-circle {
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .1);
        position: fixed;
        top: 0;
        left: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: 10;
        visibility: visible;
    }

    @media (min-width: 1015px) {
        .loader-container-circle {
            width: calc(100% + 260px);
            height: 100%;
            background-color: rgba(0, 0, 0, 0.1);
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            z-index: 3;
            visibility: visible;
        }
    }

    .loader-circle {
        width: 50px;
        height: 50px;
        border: 5px solid;
        color: #3498db;
        border-radius: 50%;
        border-top-color: transparent;
        animation: loader 1.2s linear infinite;
    }

    @keyframes loader {
        25% {
            color: #2ecc71;
        }
        50% {
            color: #f1c40f;
        }
        75% {
            color: #e74c3c;
        }
        to {
            transform: rotate(360deg);
        }
    }
</style>

<div class="loader-container-circle" id="full-page-loader">
    <div class="loader-circle"></div>
</div>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row">
                <div class="col">
                    <button type="button" class="btn btn-sm btn-success text-white shadow btn-border-radius"
                            onclick="reloadAllSenioritySerial()"
                    >
                        <%=isLangEn ? "Reload All" : "সব রিলোড করুন"%>
                    </button>
                </div>
                <div class="col text-right">
                    <button type="button" class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                            onclick="saveSenioritySerial()">
                        <%=isLangEn ? "Save Changed Values" : "পরিবর্তিত মান সেভ করুন"%>
                    </button>
                </div>
            </div>
            <div class="mt-3">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th><%=isLangEn ? "Employee" : "কর্মকর্তা/কর্মচারী"%>
                        </th>
                        <th><%=isLangEn ? "Class" : "ক্লাস"%>
                        </th>
                        <th><%=isLangEn ? "Designation Grade" : "পদবী গ্রেড"%>
                        </th>
                        <th><%=isLangEn ? "Salary Grade" : "বেতন গ্রেড"%>
                        </th>
                        <th><%=isLangEn ? "Basic Salary" : "মূল বেতন"%>
                        </th>
                        <th><%=isLangEn ? "Joining Date" : "যোগদানের তারিখ"%>
                        </th>
                        <th><%=isLangEn ? "Serial" : "ক্রম"%>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <%if (employeeSeniorityDTOs.isEmpty()) {%>
                    <tr>
                        <td colspan="100%" class="text-center">
                            <%=isLangEn ? "No Data Found" : "কোন তথ্য পাওয়া যায়নি"%>
                        </td>
                    </tr>
                    <%
                    } else {
                        for (Employee_seniorityDTO employeeSeniorityDTO : employeeSeniorityDTOs) {
                            Employee_recordsDTO employeeRecordsDTO =
                                    Employee_recordsRepository.getInstance().getById(employeeSeniorityDTO.employeeRecordId);
                            OfficeUnitOrganograms officeUnitOrganograms =
                                    OfficeUnitOrganogramsRepository.getInstance().getById(employeeSeniorityDTO.organogramId);
                            Office_unitsDTO officeUnitsDTO =
                                    Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeSeniorityDTO.officeUnitId);
                    %>
                    <tr>
                        <td>
                            <%if (employeeRecordsDTO != null) {%>
                            <%=isLangEn ? employeeRecordsDTO.nameEng : employeeRecordsDTO.nameBng%><br>
                            <%=StringUtils.convertBanglaIfLanguageIsBangla(language, employeeRecordsDTO.employeeNumber)%>
                            <br>
                            <%}%>
                            <%if (officeUnitOrganograms != null) {%>
                            <%=isLangEn ? officeUnitOrganograms.designation_eng : officeUnitOrganograms.designation_bng%>
                            <br>
                            <%}%>
                            <%if (officeUnitsDTO != null) {%>
                            <%=isLangEn ? officeUnitsDTO.unitNameEng : officeUnitsDTO.unitNameBng%>
                            <%}%>
                        </td>
                        <td>
                            <%=CatRepository.getInstance().getText(language, "employee_class", employeeSeniorityDTO.employeeClassCat)%>
                        </td>
                        <td>
                            <%=CatRepository.getInstance().getText(language, "job_grade_type", employeeSeniorityDTO.jobGradeTypeCat)%>
                        </td>
                        <td>
                            <%=CatRepository.getInstance().getText(language, "job_grade", employeeSeniorityDTO.salaryGradeType)%>
                        </td>
                        <td>
                            <%=BangladeshiNumberFormatter.getFormattedNumber(
                                    StringUtils.convertBanglaIfLanguageIsBangla(language, String.valueOf(employeeSeniorityDTO.salary))
                            )%>/-
                        </td>
                        <td>
                            <%=StringUtils.getFormattedDate(isLangEn, employeeSeniorityDTO.joiningDate)%>
                        </td>
                        <td>
                            <input type="text"
                                   class="form-control"
                                   name="serial"
                                   value="<%=employeeSeniorityDTO.serial%>"
                                   data-only-integer="true"
                                   data-id="<%=employeeSeniorityDTO.iD%>"
                                   onchange="recordChangedValue(this)"
                            >
                        </td>
                    </tr>
                    <%
                            }
                        }
                    %>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script>
    const fullPageLoader = $('#full-page-loader');

    function typeOnlyInteger(e) {
        return true === inputValidationForPositiveValue(e, $(this), <%=Integer.MAX_VALUE%>, 0);
    }

    $(() => {
        $('body').delegate('[data-only-integer="true"]', 'keydown', typeOnlyInteger);
        fullPageLoader.hide();
    });

    async function reloadAllSenioritySerial() {
        fullPageLoader.show();
        const url = "Employee_seniorityServlet?actionType=ajax_reloadAllSenioritySerial";
        try {
            const res = await fetch(url, {method: 'POST'});
            const resJson = await res.json();
            console.log(resJson);
            if (resJson.success === true) {
                location.reload();
                return;
            }
            $('#toast_message').css('background-color', '#ff6063');
            showToast(resJson.message, resJson.message);
        } catch (error) {
            console.log(error);
            $('#toast_message').css('background-color', '#ff6063');
            showToast('<%=isLangEn? "Failed to reload" : "রিলোড করতে ব্যর্থ হয়েছে"%>');
        }
        fullPageLoader.hide();
    }

    const savedSenioritySerial = {};

    async function saveSenioritySerial() {
        const senioritySerials = Object.values(savedSenioritySerial);
        if (senioritySerials.length === 0) {
            $('#toast_message').css('background-color', '#4A87E2');
            showToast('<%=isLangEn? "No data changed" : "কোন তথ্য পরিবর্তীত হয়নি"%>');
            return;
        }
        const data = {
            senioritySerials: JSON.stringify(senioritySerials)
        };

        fullPageLoader.show();
        $.ajax({
            type: "POST",
            url: "Employee_seniorityServlet?actionType=ajax_saveSenioritySerial",
            data: data,
            dataType: 'JSON',
            success: function (response) {
                fullPageLoader.hide();
                if (response.success === true) {
                    location.reload();
                    return;
                }
                $('#toast_message').css('background-color', '#ff6063');
                showToast(response.message, response.message);
            },
            error: function (jqXHR, textStatus, error) {
                fullPageLoader.hide();
                console.error(error);
                $('#toast_message').css('background-color', '#ff6063');
                showToast('<%=isLangEn? "Failed to save" : "সেভ করতে ব্যর্থ হয়েছে"%>');
            }
        });
    }

    function recordChangedValue(serialInput) {
        const id = serialInput.dataset.id;
        const serial = serialInput.value;
        savedSenioritySerial[id] = {id, serial};
    }
</script>