<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    String context = request.getContextPath() + "/";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.ALLOWANCE_CONFIGURE_EDIT_LANGUAGE, loginDTO);
    String formTitle = LM.getText(LC.GLOBAL_CHANGE_PASSWORD, loginDTO);
    String servletName = "Employee_password_changeServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" action="Employee_password_changeServlet?actionType=ajax_changePassword"
              id="bigform" name="bigform" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_EMPLOYEERECORDID, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <!-- Button trigger modal -->
                                            <button type="button"
                                                    class="btn btn-primary btn-block shadow btn-border-radius"
                                                    id="employee_record_id_modal_button"
                                                    onclick="employeeRecordIdModalBtnClicked();">
                                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                                            </button>
                                            <div class="input-group" id="employee_record_id_div"
                                                 style="display: none">
                                                <input type="hidden" name='employeeRecordId'
                                                       id='employee_record_id_input' value="">
                                                <button type="button" class="btn btn-secondary form-control"
                                                        disabled
                                                        id="employee_record_id_text"></button>
                                                <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                                                    <button type="button" class="btn btn-outline-danger"
                                                            onclick="crsBtnClicked('employee_record_id');"
                                                            id='employee_record_id_crs_btn' tag='pb_html'>
                                                        x
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="password_change_div" style="display: none">
                                        <div class="form-group row" id="password_div">
                                            <label class="col-md-3 col-form-label text-right">
                                                <%=LM.getText(LC.EMPLOYEE_RECORD_PASSWORD, loginDTO)%>
                                                <span
                                                        class="required"> * </span>
                                            </label>
                                            <div class="col-md-9">
                                                <div id='passwordDiv'>
                                                    <input autocomplete="new-password" type='password'
                                                           class='form-control rounded'
                                                           placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_PASSWORD, loginDTO)%>'
                                                           name='password'
                                                           id='password'
                                                           value=''/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row" id="confirm_password_div">
                                            <label class="col-md-3 col-form-label text-right">
                                                <%=LM.getText(LC.EMPLOYEE_RECORD_CONFIRM_PASSWORD, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9">
                                                <div id='confirmPasswordDiv'>
                                                    <input type='password' class='form-control rounded'
                                                           name='confirmPassword'
                                                           id='confirmPassword'
                                                           placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_PASSWORD_AGAIN, loginDTO)%>'
                                                           value=''/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button">
                                <%=LM.getText(LC.ALLOWANCE_CONFIGURE_ADD_ALLOWANCE_CONFIGURE_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="button" onclick="submitPasswordChangeForm()">
                                <%=LM.getText(LC.ALLOWANCE_CONFIGURE_ADD_ALLOWANCE_CONFIGURE_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script type="text/javascript">
    const bigForm = $('#bigform');
    const cancelBtn = $('#cancel-btn');
    const submitBtn = $('#submit-btn');
    const isLangEng = '<%=HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng%>';

    function PreprocessBeforeSubmiting(row, validate) {
    }

    function isEmployeeSelected() {
        if (document.getElementById('employee_record_id_input').value == '') {
            showError("ইউজার বাছাই করুন", "Please Select User");
            return false;
        }
        return true;
    }

    function isPasswordMatch() {
        if (document.getElementById('password').value != document.getElementById('confirmPassword').value) {
            showError("নিশ্চিত পাসওয়ার্ড পাসওয়ার্ডের সাথে মিলে নাই", "Confirm password does not match with password");
            return false;
        }
        return true;
    }

    function submitPasswordChangeForm() {
        if (isEmployeeSelected() && bigForm.valid() && isPasswordMatch()) {
            submitAjaxForm();
        }
    }

    function buttonStateChange(value) {
        cancelBtn.prop("disabled", value);
        submitBtn.prop("disabled", value);
    }

    function init(row) {
        bigForm.validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                password: {
                    required: true,
                },
                confirmPassword: {
                    required: true,
                },
            },

            messages: {
                password: isLangEng ? "Please enter password" : "পাসওয়ার্ড প্রবেশ করুন",
                confirmPassword: isLangEng ? "Please enter confirm password" : "পাসওয়ার্ড নিশ্চিত করুন",
            }
        });
    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
        $('#password_change_div').hide();
        $('#password').val('');
        $('#confirmPassword').val('');
    }

    function viewEmployeeRecordIdInInput(empInfo) {
        $('#employee_record_id_modal_button').hide();
        $('#employee_record_id_div').show();

        let language = '<%=Language.toLowerCase()%>'
        let employeeView;
        if (language === 'english') {
            employeeView = empInfo.employeeNameEn + ', ' + empInfo.organogramNameEn + ', ' + empInfo.officeUnitNameEn;
        } else {
            employeeView = empInfo.employeeNameBn + ', ' + empInfo.organogramNameBn + ', ' + empInfo.officeUnitNameBn;
        }
        document.getElementById('employee_record_id_text').innerHTML = employeeView;
        $('#employee_record_id_input').val(empInfo.employeeRecordId);
        $('#password_change_div').show();
    }


    table_name_to_collcetion_map = new Map([
        ['employeeRecordId', {
            isSingleEntry: true,
            callBackFunction: viewEmployeeRecordIdInInput
        }]
    ]);
    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    function employeeRecordIdModalBtnClicked() {
        modal_button_dest_table = 'employeeRecordId';
        $('#search_emp_modal').modal();
    }


</script>






