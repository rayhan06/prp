<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="other_office.Other_officeRepository" %>
<%@ page import="geolocation.GeoDistrictRepository" %>
<%@ page import="geolocation.GeoLocationRepository" %>
<%@ page import="employee_management.EmployeeOfficerCat" %>

<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.EMPLOYEE_MANAGEMENT_ADD_EMPLOYEE_MANAGEMENT_ADD_FORMNAME, loginDTO);
    String context = "../../.." + request.getContextPath() + "/";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Employee_managementServlet?actionType=<%=actionName%>&isPermanentTable=true" id="employee_form">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="row mx-2 mx-md-0">
                                        <%@include file="common_employee_mp.jsp" %>

                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right" for="homeDistrict_select">
                                                    <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_HOME_DISTRICT, loginDTO)%><span>*</span>
                                                </label>
                                                <div class="col-md-8">
                                                    <select class='form-control rounded' name='homeDistrict' required
                                                            id='homeDistrict_select'>
                                                        <%=GeoLocationRepository.getInstance().buildOptionByLevel(Language, 2, -1)%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"
                                                       for="provision_period">
                                                    <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_PROVISION_PERIOD, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control digitOnly'
                                                           name='provision_period' maxlength="3"
                                                           placeholder='<%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_PROVISION_IN_MONTHS, loginDTO)%>'
                                                           id='provision_period' tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"
                                                       for="employeeClassCat_category">
                                                    <%=LM.getText(LC.EMPLOYEE_EMPLOYEE_CLASS, loginDTO)%>
                                                    <span>*</span></label>
                                                <div class="col-md-8">
                                                    <select class='form-control' name='employeeClassCat' required
                                                            id='employeeClassCat_category' tag='pb_html'>
                                                        <%=CatRepository.getInstance().buildOptions("employee_class", Language, null)%>
                                                    </select>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"
                                                       for="employmentCat_category">
                                                    <%=LM.getText(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY, loginDTO)%>
                                                    <span>*</span></label>
                                                <div class="col-md-8">
                                                    <select class='form-control' name='employmentCat' required
                                                            id='employmentCat_category' tag='pb_html'>
                                                        <%=CatRepository.getInstance().buildOptions("employment", Language, null)%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"
                                                       for="empOfficerCat_category">
                                                    <%=LM.getText(LC.EMPLOYEE_OFFICER_TYPE, loginDTO)%>
                                                    <span>*</span></label>
                                                <div class="col-md-8">
                                                    <select class='form-control' name='empOfficerCat' required
                                                            id='empOfficerCat_category' tag='pb_html'>
                                                        <%=CatRepository.getInstance().buildOptions("emp_officer", Language, null)%>
                                                    </select>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-6 ">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right" for="jobQuota_select">
                                                    <%=isLanguageEnglish ? "Quota" : "কোটা"%>
                                                </label>
                                                <div class="col-md-8">
                                                    <select class='form-control rounded' name='jobQuota'
                                                            id='jobQuota_select'>
                                                        <%=CatRepository.getInstance().buildOptions("job_quota", Language, null)%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-6 otherDiv">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right">
                                                    <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_ISFULLYVACCINATED, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='checkbox' class='form-control-sm'
                                                           name='isFullyVaccinated'
                                                           id='isFullyVaccinated_checkbox'
                                                           value='true'
                                                           tag='pb_html'>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 otherDiv" style="display: none">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right">
                                                    <%=isLanguageEnglish ? "Current Office" : "বর্তমান অফিস"%>
                                                    <span>*</span>
                                                </label>
                                                <div class="col-md-8">
                                                    <select class='form-control' name='currentOfficeCat' onChange = 'getDept(this.value)'
                                                            id='currentOffice_category' tag='pb_html'>
                                                        <%=Other_officeRepository.getInstance().buildOptions(Language, null)%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-12 col-md-6" id ="otherOfficeDeptDiv" style="display: none">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right">
                                                    <%=isLanguageEnglish ? "Department" : "বিভাগ"%>
                                                    <span>*</span>
                                                </label>
                                                <div class="col-md-8">
                                                    <select class='form-control' name='otherOfficeDeptType'
                                                            id='otherOfficeDeptType' tag='pb_html'>
                                                        <option value = '-1'></option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 otherDiv" style="display: none">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right">
                                                    <%=isLanguageEnglish ? "Current Designation" : "বর্তমান পদবী"%>

                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control'
                                                           name='currentDesignation'
                                                           placeholder="<%=isLanguageEnglish?"Enter current designation":"বর্তমান পদবী লিখুন"%>"
                                                           id='currentDesignation' tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 otherDiv" style="display: none">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right">
                                                    <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_BLOODGROUP, loginDTO)%>

                                                </label>
                                                <div class="col-md-8">
                                                    <select class='form-control rounded' name='bloodGroup'
                                                            id='bloodGroup_select'>
                                                        <%=CatRepository.getInstance().buildOptions("blood_group", Language, -1)%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-6 otherDiv" style="display: none">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right">
                                                    <%=LM.getText(LC.GLOBAL_Grade, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <select class='form-control rounded' name='grade'
                                                            id='grade_select'>
                                                        <%=CatRepository.getInstance().buildOptions("job_grade", Language, -1)%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-12">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.EMPLOYEE_MANAGEMENT_ADD_EMPLOYEE_MANAGEMENT_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                                    onclick="createEmployee()">
                                <%=LM.getText(LC.EMPLOYEE_MANAGEMENT_ADD_EMPLOYEE_MANAGEMENT_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<%=context%>/assets/scripts/input_validation.js" type="text/javascript"></script>

<script type="text/javascript">
    const OFFICER_CAT_OTHERS_STR = '<%=EmployeeOfficerCat.OTHERS.getValue()%>';
    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';
    $(document).ready(() => {
        $("#employeeClassCat_category").select2({
            dropdownAutoWidth: true
        });
        $("#empOfficerCat_category").select2({
            dropdownAutoWidth: true
        });
        $("#employmentCat_category").select2({
            dropdownAutoWidth: true
        });
        $("#jobQuota_select").select2({
            dropdownAutoWidth: true
        });

        $("#currentOffice_category").select2({
            dropdownAutoWidth: true
        });
        $("#bloodGroup_select").select2({
            dropdownAutoWidth: true
        });
        $("#homeDistrict_select").select2({
            dropdownAutoWidth: true
        });
        $("#grade_select").select2({
            dropdownAutoWidth: true
        });

        $("#gradeCat").select2({
            dropdownAutoWidth: true
        });
        $("#employee_form").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            messages: {
                nameEng: '<%=isLanguageEnglish ? "Please enter name in english":"অনুগ্রহ করে লিইংরেজিতে নাম লিখুন"%>',
                nameBng: '<%=isLanguageEnglish ? "Please enter name in bangla":"অনুগ্রহ করে লিবাংলাতে নাম লিখুন"%>',
                empOfficerCat: '<%=isLanguageEnglish ?"Please Select Officer Type":"অনুগ্রহ করে লিঅফিসারের ধরন বাছাই করুন"%>',
                employeeClassCat: '<%=isLanguageEnglish ?"Please Select employee Class Type":"অনুগ্রহ করে লিকর্মচারীর শ্রেনী বাছাই করুন"%>',
                employmentCat: '<%=isLanguageEnglish ?"Please select employment type":"অনুগ্রহ করে লিকর্মকর্তার ধরন বাছাই করুন"%>',
                gradeCat: '<%=isLanguageEnglish ?"Please select gender":"অনুগ্রহ করে লিঙ্গ বাছাই করুন"%>',
                homeDistrict: '<%=isLanguageEnglish ?"Please select home district":"অনুগ্রহ করে স্থায়ী জেলা বাছাই করুন"%>',
                nid:'<%=isLanguageEnglish ?"Please enter nid number":"অনুগ্রহ করে জাতীয় পরিচয়পত্র নাম্বার লিখুন"%>'
            }
        });
    });

    function createEmployee() {
        $('#joining-date').val(getDateStringById('joining-date-js'));
        $('#date-of-birth').val(getDateStringById('date-of-birth-js'));
        let joinDateValid = dateValidator('joining-date-js', true, {
            'errorEn': 'Enter a valid joining date!',
            'errorBn': 'চাকরিতে যোগদানের তারিখ প্রবেশ করান!'
        });
        let dobValid = dateValidator('date-of-birth-js', true, {
            'errorEn': 'Enter a valid date of birth!',
            'errorBn': 'জন্মতারিখ প্রবেশ করান!'
        });

        let empOfficerCatVal = $('#empOfficerCat_category').val();
        let msg = null;
        if (empOfficerCatVal === OFFICER_CAT_OTHERS_STR) {
            if (document.getElementById('currentOffice_category').value === '') {
                msg = isLangEng ? "Current office not found" : "বর্তমান অফিস পাওয়া যায় নি";
            }
            if (document.getElementById('bloodGroup_select').value === '') {
                document.getElementById('bloodGroup_select').value = -1;
            }
            if (document.getElementById('grade_select').value === '') {
                document.getElementById('grade_select').value = -1;
            }
        } else {
            document.getElementById('bloodGroup_select').value = -1;
            document.getElementById('grade_select').value = -1;
        }

        if (msg) {
            $('#toast_message').css('background-color', '#ff6063');
            showToast(msg, msg);
            return;
        }

        document.getElementById('bloodGroup_select').value = -1;
        document.getElementById('grade_select').value = -1;

        if ($('#employee_form').valid() && joinDateValid && dobValid) {
            let title = '<%=isLanguageEnglish?"Do you want to submit?":"আপনি কি সাবমিট করতে চান?"%>'
            let okBtnText = '<%=isLanguageEnglish?"Submit":"সাবমিট"%>';
            let cancelBtnText = '<%=isLanguageEnglish?"Cancel":"বাতিল"%>';
            messageDialog(title, '', 'success', true, okBtnText, cancelBtnText, () => {
                submitAjaxForm('employee_form');
            });
        }
    }

    function showOrHideOtherDiv() {
        const empOfficerCat = $('#empOfficerCat_category').val();
        if (empOfficerCat && empOfficerCat === OFFICER_CAT_OTHERS_STR) {
            $('.otherDiv').show();
            $("#currentOffice_category").select2({
                dropdownAutoWidth: true
            });
            $("#bloodGroup_select").select2({
                dropdownAutoWidth: true
            });
            $("#grade_select").select2({
                dropdownAutoWidth: true
            });

        } else {
            $('.otherDiv').hide();

        }
    }

    $(document).ready(function () {
        document.getElementById('jobQuota_select').value = -1;
        showOrHideOtherDiv();
        $('#empOfficerCat_category').change(function () {
            showOrHideOtherDiv();
        });

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });
    
    function getDept(value)
    {
    	console.log("getDept = " + value);
    	if(value != -1)
   		{
   			$("#otherOfficeDeptDiv").removeAttr("style");
   			fillSelect("otherOfficeDeptType", "Other_office_departments_listServlet?actionType=getDept&otherOfficeId=" + value);
   		}
    	else
   		{
    		$("#otherOfficeDeptDiv").css("display", "none");
   		}
    }
</script>