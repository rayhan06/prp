<%@page import="model.employee_records"%>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="employee_management.*" %>
<%@ page import="util.*" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.util.List" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.Utils" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%
    String navigator2 = "navEMPLOYEE_MANAGEMENT";
    String servletName = "Employee_managementServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>

<%
    List<Employee_managementDTO> data = (List<Employee_managementDTO>) rn2.list;
    if (data != null && data.size() > 0) {
%>
<div class="mb-2">
    <a class="btn btn-sm text-right btn-success shadow btn-border-radius text-white mx-2"
       id='reportXlButton' onclick="getXl()"
    ><%=LM.getText(LC.HM_EXCEL, loginDTO)%>
    </a>
</div>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=isLanguageEnglish ? "Serial Number" : "ক্রমিক নম্বর"%>
            </th>
            <th><%=LM.getText(LC.HM_USER_NAME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEENG, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG, loginDTO)%></th>

            <th><%=LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_PHOTO, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO)%></th>

            <th><%=LM.getText(LC.EMPLOYEE_RECORDS_SEARCH_ALTERNATIVEMOBILE, loginDTO)%></th>

            <th><%=LM.getText(LC.EMPLOYEE_OFFICER_TYPE, loginDTO)%></th>

            <th><%=LM.getText(LC.EMPLOYEE_EMPLOYEE_CLASS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_STATUS, loginDTO)%>
            </th>
            <th><%=isLanguageEnglish ? "Created Date" : "তৈরীর তারিখ"%></th>
            <th><%=isLanguageEnglish ? "Activate/Deactivate" : "সক্রিয়/নিষ্ক্রিয় করুন"%>
            
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th>
                <div>
                    <button type="button" style="color: red;border: 0;background-color: transparent"
                            onclick="onDeleteEmployee()">
                        <i class="fa fa-trash"></i>
                    </button>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            int index = (rn2.getCurrentPageNo() - 1) * rn2.getPageSize() + 1;
            for (Employee_managementDTO employee_managementDTO : data) {
        %>
        <tr>
            <td>
                <%=Utils.getDigits(index, Language)%>
            </td>
            <td><%=isLanguageEnglish ? employee_managementDTO.userName : StringUtils.convertToBanNumber(employee_managementDTO.userName)%>
            </td>
            <td>
                <%=employee_managementDTO.nameEng == null ? "" : employee_managementDTO.nameEng%>
            </td>

            <td>
                <%=employee_managementDTO.nameBng == null ? "" : employee_managementDTO.nameBng%>
            </td>

            <td>
                <%
                    if (employee_managementDTO.photo != null) {
                %>
                <img width="75px" height="75px"
                     src='data:image/jpg;base64,<%=new String(Base64.encodeBase64(employee_managementDTO.photo))%>'
                     id="photo-img"

                >
                <%
                    }
                %>
            </td>

            <td>
                <%=employee_managementDTO.mobileNumber == null ? "" :
                        isLanguageEnglish ? employee_managementDTO.mobileNumber
                                : StringUtils.convertToBanNumber(employee_managementDTO.mobileNumber)%>
            </td>

            <td>
                <%=employee_managementDTO.alternativeMobileNo == null ? "" :
                        isLanguageEnglish ? employee_managementDTO.alternativeMobileNo
                                : StringUtils.convertToBanNumber(employee_managementDTO.alternativeMobileNo)%>
            </td>

            <td>
                <%=CatRepository.getInstance().getText(Language, "emp_officer", employee_managementDTO.empOfficerCat)%>
            </td>

            <td>
                <%=CatRepository.getInstance().getText(Language, "employee_class", employee_managementDTO.employeeClassCat)%>
            </td>

            <td>
                <%=CatRepository.getInstance().getText(Language, "employment", employee_managementDTO.employmentCat)%>
            </td>
            <td>
                <%=employee_managementDTO.status ? LM.getText(LC.GLOBAL_STATUS_ACTIVE, loginDTO) : LM.getText(LC.GLOBAL_STATUS_INACTIVE, loginDTO)%>
                <%=employee_managementDTO.deactivationReason.equalsIgnoreCase("")?"":"<br>" + LM.getText(LC.HM_REASON, loginDTO) + ": " +  employee_managementDTO.deactivationReason%>
            </td>
            <td>
                <%=StringUtils.getFormattedDate(isLanguageEnglish,employee_managementDTO.insertionDate)%>
            </td>
            
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #33cc6b;"
                        data-toggle="modal" data-target="#activationModal_<%=employee_managementDTO.iD%>"
                >
                    <i class="fa fa-edit">
                    	<%
                    	if(employee_managementDTO.status)
                   		{
                   			%>
                   			<%=isLanguageEnglish ? "Deactivate" : "নিষ্ক্রিয় করুন"%>
                   			<%
                   		}
                    	else
                    	{
                    		%>
                    		<%=isLanguageEnglish ? "Activate" : "সক্রিয়"%>
                   			<%
                    	}
                    	%>
            		</i>
                </button>
                
                <div id="activationModal_<%=employee_managementDTO.iD%>" class="modal fade" role="dialog">
				    <div class="container">
				        <div class="">
				            <div class="modal-dialog">
				
				                <!-- Modal content-->
				                <div class="modal-content">
				                	<div id = "modalDiv_<%=employee_managementDTO.iD%>"
							              >
					                    <div class="modal-header">
					                        <button type="button" class="close" data-dismiss="modal"></button>
					                        <h4 class="modal-title">
					                        </h4>
					                    </div>
					                    <div class="modal-body">
											
											<div class="form-group row">
				                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.HM_REASON, loginDTO)%>
				                                        </label>
				                                        <div class="col-md-9">
				                                            <input required type='text' class='form-control' name='deactivationReason'
			                                                   id='deactivationReason_<%=employee_managementDTO.iD%>'
			                                                   value='' tag='pb_html'/>
				                                        </div>
				                              </div>
				                              
				                         </div>
					                    <div class="modal-footer">
					                        <button type="button" class="btn btn-success" onclick="activateEmployee(<%=employee_managementDTO.iD%>, <%=!employee_managementDTO.status%>)";
					                                ><%=LM.getText(LC.GLOBAL_SUBMIT, loginDTO)%>
					                        </button>
					                    </div>
				                    
				                    </div>
				                </div>
				                
				            </div>
				        </div>
				    </div>
				</div>
            </td>
            
            <td>
                <button
                        type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
                        onclick="location.href='Employee_recordsServlet?actionType=viewMultiForm&tab=1&ID=<%=employee_managementDTO.iD%>&userId=<%=employee_managementDTO.userName%>'">
                    <i class="fa fa-eye"></i>
                </button>
            </td>

            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input class="checkbox" type='checkbox' name='ID'
                                                 value='<%=employee_managementDTO.iD%>'/></span>
                </div>
            </td>

        </tr>
        <%
                index++;
            }
        %>

        </tbody>
    </table>
</div>
<%
} else {
%>
<label style="width: 100%;text-align: center;font-size: larger;font-weight: bold;color: red">
    <%=isLanguageEnglish ? "No information is found" : "কোন তথ্য পাওয়া যায় নি"%>
</label>
<%
    }
%>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>

<script>
    function getXl() {
        var url = "Employee_managementServlet" + window.location.search + '&isXL=1';
        console.log(url)

        window.location.href = url;
    }

    function onDeleteEmployee() {
        const deleteItem = document.getElementsByClassName('checkbox');
        let items = [], ids = ('');
        for (let i = 0; i < deleteItem.length; i++) {
            if (deleteItem[i].checked) {
                items.push(deleteItem[i].value);
                ids += ('&ID=' + parseInt(deleteItem[i].value));
            }
        }


        if (items.length > 0) {
            let title = '<%=isLanguageEnglish?"Do you want to delete selected employee?":"আপনি কি নির্বাচনকৃত কর্মকর্তা ডিলিট করতে চান?"%>';
            let message = '<%=isLanguageEnglish? "You will not be able to revert this!":"আপনি এটি আর ফিরিয়ে আনতে পারবেন না!"%>';
            let okBtnText = '<%=isLanguageEnglish?"Submit":"সাবমিট"%>';
            let cancelBtnText = '<%=isLanguageEnglish?"Cancel":"বাতিল"%>';
            messageDialog(title, message, 'warning', true, okBtnText, cancelBtnText, () => {
                changeStatus(true);
                const xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState === 4 && this.status === 200) {
                        changeStatus(false);
                        if (this.responseText.includes('option')) {
                            location.reload();
                        } else {
                            showToastSticky('এরর রেসপন্স', "Error response");
                        }
                    } else if (this.readyState === 4 && this.status !== 200) {
                        showToastSticky(this.status, this.status);
                        changeStatus(false);
                    }
                };
                xhttp.open("POST", "Employee_managementServlet?actionType=delete" + ids, true);
                xhttp.send();
            });
        } else {
            let title = '<%=isLanguageEnglish?"Please select employee to delete":"অনুগ্রহ করে ডিলিট করার জন্য কর্মকর্তা নির্বাচন করুন"%>';
            let okBtnText = '<%=isLanguageEnglish?"Ok":"ওকে"%>';
            messageDialog(title, '', 'info', false, okBtnText, '');
        }
    }

    function changeStatus(flag) {
        $('.btn').prop('disabled', flag);
        $('.checkbox').prop('disabled', flag);
    }
</script>