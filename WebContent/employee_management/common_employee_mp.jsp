<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page pageEncoding="UTF-8" %>

<div class="col-sm-12 col-md-6">
    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right" for="nameEng_text">
            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEENG, loginDTO)%>
            <span>*</span></label>
        <div class="col-md-8">
            <input type='text' class='englishOnly form-control rounded'
                   name='nameEng'
                   placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_YOUR_NAME_IN_ENGLISH, loginDTO)%>'
                   id='nameEng_text' required tag='pb_html'/>
        </div>
    </div>
</div>
<div class="col-sm-12 col-md-6">
    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right" for="nameBng_text">
            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG, loginDTO)%>
            <span>*</span></label>
        <div class="col-md-8">
            <input type='text' class='form-control noEnglish' name='nameBng'
                   placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_YOUR_NAME_IN_BANGLA, loginDTO)%>'
                   id='nameBng_text' required tag='pb_html'/>
        </div>
    </div>
</div>
<div class="col-sm-12 col-md-6">
    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right">
            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_DATEOFBIRTH, loginDTO)%><span>*</span>
        </label>
        <div class="col-md-8">
            <jsp:include page="/date/date.jsp">
                <jsp:param name="DATE_ID" value="date-of-birth-js"/>
                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
            </jsp:include>
            <input type='hidden' class='form-control' id='date-of-birth'
                   name='dateOfBirth' tag='pb_html'/>
        </div>
    </div>
</div>
<div class="col-sm-12 col-md-6">
    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right" for="mobileNumber_text">
            <%=LM.getText(LC.EMPLOYEE_PERSONAL_MOBILE_NUMBER, loginDTO)%>
            <span>*</span></label>
        <div class="col-md-8">
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">+88</div>
                </div>
                <input type='text' class='digitOnly form-control rounded'
                       name='mobileNumber' maxlength="11"
                       id='mobileNumber_text' required
                       pattern="^(01[3-9]{1}[0-9]{8})"
                       placeholder='<%=LM.getText(LC.GLOBAL_MOBILE_PLACE_HOLDER, loginDTO)%>'
                       title='<%=isLanguageEnglish?"personal mobile number must start with 01, then contain 9 digits"
                                                           :"ব্যক্তিগত মোবাইল নাম্বার 01 দিয়ে শুরু হবে, তারপর ৯টি সংখ্যা হবে"%>'
                       tag='pb_html'/>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-12 col-md-6">
    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right" for="personalEml">
            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_PERSONALEMAIL, loginDTO)%>
        </label>
        <div class="col-md-8">
            <input type='text' class='form-control rounded' name='personalEml'
                   id='personalEml'
                   placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_YOUR_PERSONAL_EMAIL, loginDTO)%>'
                   />
        </div>
    </div>
</div>
<div class="col-sm-12 col-md-6">
    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right" for="nid_text_<%=i%>">
            <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_NID, loginDTO)%><span>*</span>
        </label>
        <div class="col-md-8">
            <div id='nid_div_<%=i%>'>
                <input maxlength="20" type='text' class='form-control rounded' name='nid' id='nid_text_<%=i%>' required
                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_NATIONAL_IDENTITY_NUMBER, loginDTO)%>'/>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-12 col-md-6">
    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right"
               for="genderCat">
            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_GENDER, loginDTO)%>
            <span>*</span></label>
        <div class="col-md-8">
            <select class='form-control' name='genderCat' required
                    id='genderCat' tag='pb_html'>
                <%=CatRepository.getInstance().buildOptions("gender", Language, null)%>
            </select>
        </div>
    </div>
</div>
<div class="col-sm-12 col-md-6">
    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right">
            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_JOININGDATE, loginDTO)%>
            <span>*</span></label>
        <div class="col-md-8">
            <jsp:include page="/date/date.jsp">
                <jsp:param name="DATE_ID"
                           value="joining-date-js"></jsp:param>
                <jsp:param name="LANGUAGE"
                           value="<%=Language%>"></jsp:param>
            </jsp:include>
            <input type='hidden' class='form-control' id='joining-date'
                   name='joiningDate' value=''
                   tag='pb_html'/>
        </div>
    </div>
</div>