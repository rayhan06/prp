<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page contentType="text/html;charset=utf-8" %>
<%@page pageEncoding="UTF-8" %>

<%
    System.out.println("Inside nav.jsp");
    String url = "Employee_managementServlet?actionType=vacantOfficer";
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0,false)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="name_eng">
                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEENG, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="name_eng"
                                   placeholder="<%=LM.getText(LC.GLOBAL_EMPLOYEE_NAME_ENG, loginDTO)%>"
                                   name="name_eng" onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="name_bng">
                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="name_bng"
                                   placeholder="<%=LM.getText(LC.GLOBAL_EMPLOYEE_NAME_BNG, loginDTO)%>"
                                   name="name_bng" onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="mobile_number">
                            <%=LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">+88</div>
                                </div>
                                <input type="text" class="form-control" id="mobile_number" name="mobile_number"
                                       maxlength="11"
                                       placeholder="<%=LM.getText(LC.GLOBAL_MOBILE_PLACE_HOLDER, loginDTO)%>"
                                       onChange='setSearchChanged()'>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="userName">
                            <%=LM.getText(LC.HM_USER_NAME, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="userName" name="userName"
                                   placeholder="<%=LM.getText(LC.HM_USER_NAME, loginDTO)%>"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=isLanguageEnglishNav ? "Parliament Joining Date (To)" : "পার্লামেন্ট যোগদান তারিখ (হতে)"%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="joining_date_js_start"/>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            </jsp:include>
                            <input type='hidden' id='joiningDateStart' value=''>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=isLanguageEnglishNav ? "Parliament Joining Date (From)" : "পার্লামেন্ট যোগদান তারিখ (পর্যন্ত)"%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="joining_date_js_end"/>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            </jsp:include>
                            <input type='hidden' id='joiningDateEnd' value=''>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <div class="search-loader-container-circle ">
            <div class="search-loader-circle"></div>
        </div>
    </div>
</template>

<script src="<%=context%>/assets/scripts/input_validation.js" type="text/javascript"></script>
<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>
<script type="text/javascript">
    const engNameSelector = $('#name_eng');
    const bngNameSelector = $('#name_bng');
    const mobileNumberSelector = $('#mobile_number');
    const userNameSelector = $('#userName');
    const anyFieldSelector = $('#anyfield');

    function resetInputs() {
        resetDateById('joining_date_js_start');
        resetDateById('joining_date_js_end');
        anyFieldSelector.val('');
        engNameSelector.val('');
        bngNameSelector.val('');
        mobileNumberSelector.val('');
        userNameSelector.val('');
    }

    window.addEventListener('popstate', e => {
        if (e.state) {
            let paramMap = actualBuildParamMap(e.state);
            let params = 'actionType=vacantOfficer';
            paramMap.forEach((value, key) => {
                if (key !== 'actionType') {
                    if (key === 'userName_1') {
                        params += '&userName=' + convertBanToEng(value);
                    } else if (key === 'mobile_number_1') {
                        params += '&mobile_number=88' + convertBanToEng(value);
                    } else if (key !== 'userName' && key !== 'mobile_number') {
                        params += "&" + key + "=" + value;
                    }
                }
            });
            dosubmit(params, false);
            resetInputs();
            paramMap.forEach((value, key) => {
                switch (key) {
                    case 'joiningDateStart':
                        setDateByTimestampAndId('joining_date_js_start', value);
                        break;
                    case 'joiningDateEnd':
                        setDateByTimestampAndId('joining_date_js_end', value);
                        break;
                    case "AnyField1":
                        anyFieldSelector.val(value);
                        break;
                    case "name_eng1":
                        engNameSelector.val(value);
                        break;
                    case "name_bng1":
                        bngNameSelector.val(value);
                        break;
                    case "userName_1":
                        userNameSelector.val(value);
                        break;
                    case "mobile_number_1":
                        mobileNumberSelector.val(value);
                        break;
                    default:
                        setPaginationFields([key, value]);
                }
            });
        } else {
            dosubmit(null, false);
            resetInputs();
            resetPaginationFields();
        }
    });

    function dosubmit(params, pushState = true) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (pushState) {
                    history.pushState(params, '', 'Employee_managementServlet?actionType=vacantOfficer&' + params);
                }
                setTimeout(() => {
                    document.getElementById('tableForm').innerHTML = this.responseText;
                    setPageNo();
                    searchChanged = 0;
                }, 500);
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("GET", "Employee_managementServlet?actionType=vacantOfficer&isPermanentTable=true&ajax=true&" + params, false);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number, pushState = true) {
        $('#joiningDateStart').val(getDateTimestampById('joining_date_js_start'));
        $('#joiningDateEnd').val(getDateTimestampById('joining_date_js_end'));
        let params = 'search=true';
        if (anyFieldSelector.val()) {
            params += '&AnyField1=' + anyFieldSelector.val();
        }
        if ($('#name_eng').val()) {
            params += '&name_eng1=' + $('#name_eng').val();
        }
        if ($('#name_bng').val()) {
            params += '&name_bng1=' + $('#name_bng').val();
        }
        if ($('#mobile_number').val()) {
            params += '&mobile_number_1=' + mobileNumberSelector.val();
            params += '&mobile_number1=88' + convertBanToEng($('#mobile_number').val());
        }
        if ($("#userName").val()) {
            params += '&userName_1=' + userNameSelector.val();
            params += '&userName1=' + convertBanToEng($("#userName").val());
        }
        params += '&joiningDateStart=' + $('#joiningDateStart').val();
        params += '&joiningDateEnd=' + $('#joiningDateEnd').val();


        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params, pushState);

    }

    function convertBanToEng(str) {
        if (str == null) {
            return '';
        }
        str = str.replaceAll("০", "0");
        str = str.replaceAll("১", "1");
        str = str.replaceAll("২", "2");
        str = str.replaceAll("৩", "3");
        str = str.replaceAll("৪", "4");
        str = str.replaceAll("৫", "5");
        str = str.replaceAll("৬", "6");
        str = str.replaceAll("৭", "7");
        str = str.replaceAll("৮", "8");
        str = str.replaceAll("৯", "9");
        return str;
    }

</script>

