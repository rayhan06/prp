<%@ page import="language.LM" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="language.LC" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page import="util.CommonConstant" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="grade_wise_pay_scale.*" %>
<%@ page import="pb.*" %>

<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    String context = "../../.." + request.getContextPath() + "/";
    String servletType = request.getParameter("servletType");
    LoginDTO loginDTO2 = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    LoginDTO loginDTO = loginDTO2;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLangEng = Language.equalsIgnoreCase("English");
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    int year = Calendar.getInstance().get(Calendar.YEAR);
    String userName = request.getParameter("userName");

%>

<input type='hidden' id='servletType' value='<%=servletType%>'/>
<jsp:include page="../employee_assign/officeMultiSelectTagsUtil.jsp"/>
<div class="kt-portlet kt-portlet--responsive-tablet-and-mobile py-3">
    <div class="kt-portlet__body">
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6" style = "display:none">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=LM.getText(LC.APPOINTMENT_ADD_PATIENTID, loginDTO2)%>
                        </label>
                        <div class="col-md-9">
                            <button type="button"
                                    class="btn btn-border-radius text-white shadow form-control"
                                    style="background-color: #4a87e2;"
                                    onclick="addEmployee()">
                                <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO2)%>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="phone">
                            <%=LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO2)%>
                        </label>
                        <div class="col-md-9">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">+88</div>
                                </div>
                                <input type="text" class="form-control" id="phone" name="phone"
                                       maxlength="11"
                                       placeholder="<%=LM.getText(LC.GLOBAL_MOBILE_PLACE_HOLDER, loginDTO2)%>"
                                       onChange='setSearchChanged()'>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="userName">
                            <%=LM.getText(LC.HM_USER_NAME, loginDTO2)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="userName" name="userName"
                                   placeholder="<%=LM.getText(LC.HM_USER_NAME, loginDTO2)%>"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6" >
                    <div class="form-group row">
		                <label class="col-md-3 col-form-label">
		                    <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_OFFICEUNITID, loginDTO)%>
		                </label>
		                <div class="col-md-9">
		                    <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
		                            id="office_units_id_modal_button"
		                            onclick="officeModalButtonClicked();">
		                        <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
		                    </button>
		                    <input type="hidden" name='officeUnitIds' id='officeUnitIds_input' value="">
		                </div>
		            </div>
                </div>
                
                <div class="col-md-6">
                    <div class="form-group row">
	                    <label class="col-md-3 col-form-label" for="onlySelectedOffice_checkbox">
	                        <%=isLangEng ? "Only Selected Office" : "শুধুমাত্র নির্বাচিত অফিস" %>
	                    </label>
	                    <div class="col-1" id='onlySelectedOffice'>
	                        <input type='checkbox' class='form-control-sm mt-1' name='onlySelectedOffice'
	                               id='onlySelectedOffice_checkbox'
	                               onchange="this.value = this.checked;" value='false'>
	                    </div>
	                    <div class="col-8"></div>
                	</div>
                </div>
                
                <div class="search-criteria-div col-12" id="selected-offices" style="display: none;">
		            <div class="form-group row">
		                <div class="offset-1 col-10 tag-container" id="selected-offices-tag-container">
		                    <div class="tag template-tag">
		                        <span class="tag-name"></span>
		                        <i class="fas fa-times-circle tag-remove-btn"></i>
		                    </div>
		                </div>
		            </div>
		        </div>
                
                
                <div class="col-md-12">
                    <input type="button" value="<%=(LM.getText(LC.GLOBAL_SEARCH, loginDTO2))%>"
                           class="btn btn-primary btn-hover-brand btn-border-radius shadow"
                           onclick="getEmployeeDetails()">
                </div>
            </div>
        </div>
        
             <div class="row mt-5" id="search_employee_info" style="display: none">
         
            <div class="col-md-12">
                <h5 class="caption-subject font-blue-madison table-title"><%=(LM.getText(LC.OFFICE_AND_EMPLOYEE_MANAGEMENT_CURRENT_DESIGNATION, loginDTO2))%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead class="text-nowrap">
                        <tr class="uppercase">
                        	<th><%=(LM.getText(LC.HM_NAME, loginDTO2))%>
                            </th>
                            <th><%=(LM.getText(LC.OFFICE_AND_EMPLOYEE_MANAGEMENT_DESIGNATION, loginDTO2))%>
                            </th>
                            
                            <th>
                                <%=isLanguageEnglish ? "Grade" : "গ্রেড"%>
                            </th>
                            <th>
                                <%=isLanguageEnglish ? "Pay scale" : "পে স্কেল"%>
                            </th>
                            <th>
                                <%=isLanguageEnglish ? "Salary Grade" : "বেতন গ্রেড"%>
                            </th>
                            <th ><%=(LM.getText(LC.GLOBAL_SUBMIT, loginDTO2))%>
                            </th>
                        </tr>
                        </thead>
                        <tbody id="search_employee_organogram">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
             <template id="template-OfficeLastDate">
            <tr>
                <input type="hidden" id="p_office_name_" value="" tag='pb_html'>
                <input type="hidden" id="employee_office_id_" value="" tag='pb_html'>
                <td id="p_ename_" tag='pb_html'>
                </td>
                <td id="p_user_designation_" tag='pb_html'>
                </td>
                
               
                <td id="p_grade_" tag='pb_html'>
                </td>
                <td id="p_grade_level_" tag='pb_html'>
                	<select class="form-control payScaleSelect2" class='form-control rounded' id="payScale_" name="payScale" tag='pb_html'>
	                    <%=Grade_wise_pay_scaleRepository.getInstance().buildOptions(Language,  -1L)%>
	                </select>
                </td>
                
                 <td id="p_salaryGrade_" tag='pb_html'>
                	<select class="form-control" class='form-control rounded' id="salaryGrade_" name="salaryGrade" tag='pb_html'>
	                    <%=CatRepository.getOptions(Language, "job_grade", 0)%>
	                </select>
                </td>
               
                <td id="submit_btn_div" tag='pb_html'>
                    <button class="btn btn-success" id="submit_btn_" tag='pb_html'>
                        <%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_SUBMIT, loginDTO2))%>
                    </button>
                </td>
            </tr>
        </template>
    </div>
</div>



<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script src="<%=context%>/assets/scripts/util1.js"></script>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
var office = 0;
var ministry_id = 0;
var office_layer = 0;
var office_origin = 0;
var selected_row_index = -1;
var user_info = {};
var user_organogram_info = [];
var selected_office = {};
var permitted_apps = [];
var non_permitted_apps = [];
var designation_id;

const isLangEng = <%=isLanguageEnglish%>;
var language = '<%=Language%>';

function getEmployeeDetails(paramUserName = null) {
    var userName = document.getElementById('userName').value;
    if (paramUserName != null) {
        userName = paramUserName;
    }
    var phone = document.getElementById('phone').value;
    
    var officeUnitIds = '';
    if($("#officeUnitIds_input").val() != '' && $("#officeUnitIds_input").val() != null)
   	{
    	officeUnitIds = JSON.parse($("#officeUnitIds_input").val());
   	}
    
    var onlySelectedOffice = $("#onlySelectedOffice_checkbox").val();

  
    $('#search_employee_info').show();
    document.getElementById('search_employee_organogram').innerHTML = '';
    $('#info_table_body').hide();
    user_info = {}
    name = '';
    user_organogram_info = [];
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText != '' && this.responseText.length > 0 && this.responseText != '[]') {
                try {
                    var list = JSON.parse(this.responseText);
                    let userName = '';
                    let mobileNumber = '';
                    for (var i = 0; i < list.length; i++) {
                        var data = list[i];
                        let office = '';
                        let post = '';
                        let inChargeLevel = '';
                        let grade = '';
                        let gradeLevel = '';
                        let start_date = '';
                        let attachmentFlag = data.attachmentFlag;
                        {
                            addRow(i);
                            if (isLangEng) {
                                if (data.unitNameEng) {
                                    office = data.unitNameEng;
                                }
                                if (data.designationEng) {
                                    post = data.designationEng;
                                }
                               
                                if (data.inChargeLabelEng) {
                                    inChargeLevel = data.inChargeLabelEng;
                                }
                                if (data.gradeEng) {
                                    grade = data.gradeEng;
                                }
                                if (data.gradeLevelEng) {
                                    gradeLevel = data.gradeLevelEng;
                                }
                            } else {
                                if (data.unitNameBng) {
                                    office = data.unitNameBng;
                                }
                                if (data.designationBng) {
                                    post = data.designationBng;
                                }
                               
                                if (data.inChargeLabelBng) {
                                    inChargeLevel = data.inChargeLabelBng;
                                }
                                if (data.gradeBng) {
                                    grade = data.gradeBng;
                                }
                                if (data.gradeLevelBng) {
                                    gradeLevel = data.gradeLevelBng;
                                }
                            }
                            document.getElementById("p_user_designation_" + i).innerHTML = post + '<br>' + office;

                            document.getElementById("p_office_name_" + i).value = office;
                            document.getElementById("employee_office_id_" + i).value = data.employee_office_id;
                            
                            
                            if (isLangEng)
                            {
                            	document.getElementById("p_ename_" + i).innerHTML = data.name_eng + ", " + data.employee_number;
                            }
                            else
                            {
                            	document.getElementById("p_ename_" + i).innerHTML = data.name_bng + ", " + convertToBanglaNumber(data.employee_number);
                            }
                            
                            document.getElementById("p_grade_" + i).innerHTML = grade;
                            //document.getElementById("p_grade_level_" + i).innerHTML = gradeLevel;
                            console.log("data.gradeTypeLevel = " + data.gradeTypeLevel);
                            $("#payScale_" + i).val(data.gradeTypeLevel);
                            
                            $("#salaryGrade_" + i).val(data.salary_grade_type);
                           	
                            

                            document.getElementById("submit_btn_" + i).setAttribute('onclick', 'setPayScale("' + i + '")');
                            
                           

                        }
                    }
                    
                    $(".payScaleSelect2").select2({
                    	 width: '200px',
                    	  
					});
                   
                } catch (e) {
                    console.log(e);
                    showToast('কোন তথ্য পাওয়া যায় নি', "Don't find any data");
                }
            } else {
                showToast('কোন তথ্য পাওয়া যায় নি', "Don't find any data");
            }
            
        } else if (this.readyState == 4 && this.status != 200) {
            alert('failed ' + this.status);
        }
    };
    xhttp.open("Get", "EmployeeAssignServlet?actionType=getEmployeeDetailsByUserNameOrNidOrOffice" +
        '&userName=' + userName +
        '&officeUnitIds=' + officeUnitIds +
        '&onlySelectedOffice=' + onlySelectedOffice +
        '&phone=' + phone, true);
    xhttp.send();
}

function addRow(child_table_id) {
    var t = $("#template-OfficeLastDate");
    $('#search_employee_organogram').append(t.html());
    var tr = $('#search_employee_organogram').find("tr:last-child");
    tr.find("[tag='pb_html']").each(function (index) {
        var prev_id = $(this).attr('id');
        $(this).attr('id', prev_id + child_table_id);
    });
}


function setPayScale(id) {
    var payScale = $("#payScale_" + id).val();
    var salaryGrade = $("#salaryGrade_" + id).val();
    var employee_office_id = $("#employee_office_id_" + id).val();
    let msg;
   
    console.log( 'payScale = ' + payScale );
    if (language === 'english') {
        msg = "Are you sure?";
    } else {
        msg = "আপনি কি নিশ্চিত?";
    }
    messageDialog(msg, '', 'warning', true, '<%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_YES_SUBMIT, loginDTO2))%>',
        '<%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_CANCEL, loginDTO2))%>', () => {
            var emp_office = user_organogram_info[id];
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    if (this.responseText != '') {
                        showToast(success_message_bng, success_message_eng);
                        
                    } else {
                        showToast(failed_message_bng, failed_message_eng);
                    }
                } else if (this.readyState == 4 && this.status != 200) {
                    alert('failed ' + this.status);
                }
            };
            console.log($('#lastWorkingDateId_' + id));

            xhttp.open("Post", "EmployeeAssignServlet?actionType=setPayScale" +
                '&employee_office_id=' + employee_office_id +
                '&salaryGrade=' + salaryGrade +
                '&payScale=' + payScale, true);
            xhttp.send();
        }, () => {

        });
}

function patient_inputted(userName, orgId) {
    console.log("patient_inputted " + userName);
    $("#userName").val(userName);
}


officeSelectModalUsage = 'none';
officeSelectModalOptionsMap = new Map([
    ['officeUnitId', {
        officeSelectedCallback: viewOfficeIdInTags,
        isMultiSelect: true,
        keepLastSelectState: true
    }]
]);

function officeModalButtonClicked() {
    officeSelectModalUsage = 'officeUnitId';
    $('#search_office_modal').modal();
}
</script>