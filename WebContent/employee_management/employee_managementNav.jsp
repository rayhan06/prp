<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@page contentType="text/html;charset=utf-8" %>

<%
    String url = "Employee_managementServlet?actionType=search";
    String empOfficerCatStr = request.getParameter("empOfficerCat");
    int empOfficerCat = empOfficerCatStr == null ? -1 : Integer.parseInt(empOfficerCatStr);

%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0,false)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="name_eng">
                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEENG, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="name_eng"
                                   placeholder="<%=LM.getText(LC.GLOBAL_EMPLOYEE_NAME_ENG, loginDTO)%>"
                                   name="name_eng" onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="name_bng">
                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="name_bng"
                                   placeholder="<%=LM.getText(LC.GLOBAL_EMPLOYEE_NAME_BNG, loginDTO)%>"
                                   name="name_bng" onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="mobile_number">
                            <%=LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">+88</div>
                                </div>
                                <input type="text" class="form-control" id="mobile_number" name="mobile_number"
                                       maxlength="11"
                                       placeholder="<%=LM.getText(LC.GLOBAL_MOBILE_PLACE_HOLDER, loginDTO)%>"
                                       onChange='setSearchChanged()'>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="userName">
                            <%=LM.getText(LC.HM_USER_NAME, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="userName" name="userName"
                                   placeholder="<%=LM.getText(LC.HM_USER_NAME, loginDTO)%>"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="status">
                            <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_STATUS, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='status' id='status' onSelect='setSearchChanged()'>
                                <option value=""><%=isLanguageEnglishNav ? Utils.selectEngText : Utils.selectBngText%>
                                </option>
                                <option value="1"><%=LM.getText(LC.GLOBAL_STATUS_ACTIVE, loginDTO)%>
                                </option>
                                <option value="0"><%=LM.getText(LC.GLOBAL_STATUS_INACTIVE, loginDTO)%>
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="employee_class_cat">
                            <%=LM.getText(LC.EMPLOYEE_EMPLOYEE_CLASS, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='employee_class_cat' id='employee_class_cat'
                                    onSelect='setSearchChanged()'>
                                <%=CatRepository.getInstance().buildOptions("employee_class", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="emp_officer_cat">
                            <%=LM.getText(LC.EMPLOYEE_OFFICER_TYPE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='emp_officer_cat' id='emp_officer_cat'
                                    onSelect='setSearchChanged()'>
                                <%=CatRepository.getInstance().buildOptions("emp_officer", Language, empOfficerCat)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="employment_cat">
                            <%=LM.getText(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='employment_cat' id='employment_cat'
                                    onSelect='setSearchChanged()'>
                                <%=CatRepository.getInstance().buildOptions("employment", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="emp_photo">
                            <%=LM.getText(LC.GLOBAL_WAS_PHOTO_UPLOADED, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='emp_photo' id='emp_photo'
                                    onSelect='setSearchChanged()'>
                                <%=CatRepository.getInstance().buildOptions("yes_no", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <div class="search-loader-container-circle ">
            <div class="search-loader-circle"></div>
        </div>
    </div>
</template>

<script src="<%=context%>/assets/scripts/input_validation.js" type="text/javascript"></script>
<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>
<script type="text/javascript">

    const engNameSelector = $('#name_eng');
    const bngNameSelector = $('#name_bng');
    const mobileNumberSelector = $('#mobile_number');
    const userNameSelector = $('#userName');
    const anyFieldSelector = $('#anyfield');
    const empOfficerCatSelector = $('#emp_officer_cat');
    const employeeClassCatSelector = $('#employee_class_cat');
    const employmentCatSelector = $('#employment_cat');
    const statusSelector = $('#status');
    const empPhotoSelector = $('#emp_photo');

    window.addEventListener('popstate', e => {
        if (e.state) {
            let paramMap = actualBuildParamMap(e.state);
            let params = 'actionType=search';
            paramMap.forEach((value, key) => {
                if (key !== 'actionType') {
                    if (key === 'userName_1') {
                        params += '&userName=' + convertBanToEng(value);
                    } else if (key === 'mobile_number_1') {
                        params += '&mobile_number=88' + convertBanToEng(value);
                    } else if (key !== 'userName' && key !== 'mobile_number') {
                        params += "&" + key + "=" + value;
                    }
                }
            });
            dosubmit(params, false);
            resetInputs();
            paramMap.forEach((value, key) => {
                switch (key) {
                    case "AnyField":
                        anyFieldSelector.val(value);
                        break;
                    case "name_eng":
                        engNameSelector.val(value);
                        break;
                    case "name_bng":
                        bngNameSelector.val(value);
                        break;
                    case "emp_officer_cat":
                        empOfficerCatSelector.select2("val", value);
                        break;
                    case "employee_class_cat":
                        employeeClassCatSelector.select2("val", value);
                        break;
                    case "employment_cat":
                        employmentCatSelector.select2("val", value);
                        break;
                    case "status":
                        statusSelector.select2("val", value);
                        break;
                    case "userName_1":
                        userNameSelector.val(value);
                        break;
                    case "mobile_number_1":
                        mobileNumberSelector.val(value);
                        break;
                    case "emp_photo":
                        empPhotoSelector.select2("val", value);
                        break;
                    default:
                        setPaginationFields([key, value]);
                }
            });
        } else {
            dosubmit(null, false);
            resetInputs();
            resetPaginationFields();
        }
    });

    $(document).ready(() => {
        readyInit('Employee_managementServlet');
        select2SingleSelector('#emp_officer_cat', '<%=Language%>');
        select2SingleSelector('#employee_class_cat', '<%=Language%>');
        select2SingleSelector('#employment_cat', '<%=Language%>');
        select2SingleSelector('#status', '<%=Language%>');
        select2SingleSelector('#emp_photo', '<%=Language%>');
        let empOfficerCatjs =<%=empOfficerCat%>;
        if (empOfficerCatjs == 3) {
            allfield_changed('',0);
        }
    });

    function resetInputs() {
        empOfficerCatSelector.select2("val", '-1');
        employeeClassCatSelector.select2("val", '-1');
        employmentCatSelector.select2("val", '-1');
        statusSelector.select2("val", '-1');
        anyFieldSelector.val('');
        engNameSelector.val('');
        bngNameSelector.val('');
        mobileNumberSelector.val('');
        userNameSelector.val('');
        empPhotoSelector.select2("val", '-1');
    }

    function dosubmit(params, pushState = true) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (pushState) {
                    history.pushState(params, '', 'Employee_managementServlet?actionType=search&' + params);
                }
                setTimeout(() => {
                    document.getElementById('tableForm').innerHTML = this.responseText;
                    setPageNo();
                    searchChanged = 0;
                }, 500);
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        let url = "Employee_managementServlet?actionType=search&isPermanentTable=true&ajax=true"
        if (params) {
            url += "&" + params;
        }
        xhttp.open("GET", url, false);
        xhttp.send();
    }

    function allfield_changed(go, pagination_number, pushState = true) {
        let params = 'search=true';
        if (anyFieldSelector.val()) {
            params += '&AnyField=' + anyFieldSelector.val();
        }
        if (engNameSelector.val()) {
            params += '&name_eng=' + engNameSelector.val();
        }
        if (bngNameSelector.val()) {
            params += '&name_bng=' + bngNameSelector.val();
        }

        if (empOfficerCatSelector.val()) {
            params += '&emp_officer_cat=' + empOfficerCatSelector.val();
        }
        if (employeeClassCatSelector.val()) {
            params += '&employee_class_cat=' + employeeClassCatSelector.val();
        }
        if (employmentCatSelector.val()) {
            params += '&employment_cat=' + employmentCatSelector.val();
        }
        if (statusSelector.val()) {
            params += '&status=' + statusSelector.val();
        }
        if (empPhotoSelector.val()) {
            params += '&emp_photo=' + empPhotoSelector.val();
        }

        let extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        let pageNo = document.getElementsByName('pageno')[0].value;
        let rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        let totalRecords = 0;
        let lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        if (userNameSelector.val()) {
            params += '&userName_1=' + userNameSelector.val();
            params += '&userName=' + convertBanToEng(userNameSelector.val());
        }
        if (mobileNumberSelector.val()) {
            params += '&mobile_number_1=' + mobileNumberSelector.val();
            params += '&mobile_number=88' + convertBanToEng(mobileNumberSelector.val());
        }
        dosubmit(params, pushState);
    }

    function convertBanToEng(str) {
        if (str == null) {
            return '';
        }
        str = str.replaceAll("০", "0");
        str = str.replaceAll("১", "1");
        str = str.replaceAll("২", "2");
        str = str.replaceAll("৩", "3");
        str = str.replaceAll("৪", "4");
        str = str.replaceAll("৫", "5");
        str = str.replaceAll("৬", "6");
        str = str.replaceAll("৭", "7");
        str = str.replaceAll("৮", "8");
        str = str.replaceAll("৯", "9");
        return str;
    }

</script>

