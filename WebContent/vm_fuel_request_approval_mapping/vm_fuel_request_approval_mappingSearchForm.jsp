<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_fuel_request_approval_mapping.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="vm_fuel_request.Vm_fuel_requestApprovalDTO" %>
<%@ page import="vm_fuel_request.Vm_fuel_requestApprovalRepository" %>
<%@ page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String value = "";
    String Language = LM.getText(LC.CARD_APPROVAL_MAPPING_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = "English".equalsIgnoreCase(Language);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String navigator2 = SessionConstants.NAV_CARD_APPROVAL_MAPPING;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
    Map<Long, Vm_fuel_requestApprovalDTO> mapByCardApprovalDTOId;
    long validationStartDate=SessionConstants.MIN_DATE;
    long validationEndDate=SessionConstants.MIN_DATE;
    long cardInfoId=-1;
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<%
    if (!hasAjax) {
        Enumeration<String> parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {

            String paramName = parameterNames.nextElement();

            if (!paramName.equalsIgnoreCase("actionType")) {
                String[] paramValues = request.getParameterValues(paramName);
                for (int i = 0; i < paramValues.length; i++) {
                    String paramValue = paramValues[i];

%>
    
<% } } } } %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th VM_FUEL_REQUEST_ADD_VEHICLEID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VEHICLEDRIVERASSIGNMENTID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPLICATIONDATE, loginDTO)%></th>
            <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_STATUS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_UNPAID_BILL, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_REQUISITION_APPROVER_SEARCH_VM_REQUISITION_APPROVER_EDIT_BUTTON,loginDTO)%></th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Vm_fuel_request_approval_mappingDTO> dataMappings = (List<Vm_fuel_request_approval_mappingDTO>) session.getAttribute(SessionConstants.VIEW_CARD_APPROVAL_MAPPING);
            List<Vm_fuel_requestDTO> data = new ArrayList<>();
            HashMap<Long, List<VmFuelVendorItemDTO>> vendorIdToItems = new HashMap<>();
            HashMap<Long, Double> vehicleToUnpaidPrice = new HashMap<>();
            StringBuilder filterBuilder = new StringBuilder();

            if (dataMappings != null && !dataMappings.isEmpty()) {

                dataMappings
                        .forEach(dataMapping -> {
                            filterBuilder.append("," + dataMapping.vm_fuel_requestId);
                        });

                filterBuilder.replace(0, 1, "(");
                filterBuilder.append(")");

                String filterFuelRequest = " iD in " + filterBuilder;
                data = new Vm_fuel_requestDAO().getDTOs(null, -1, -1, true, userDTO, filterFuelRequest, false);

                filterBuilder.delete(0, filterBuilder.length());

                data
                        .forEach(d -> {
                            filterBuilder.append("," + d.vendorId);
                        });

                if (!data.isEmpty()) {

                    filterBuilder.replace(0, 1, "(");
                    filterBuilder.append(")");

                    filterFuelRequest = " vm_fuel_vendor_id in " + filterBuilder;
                    List<VmFuelVendorItemDTO> vmFuelVendorItemDTOS = new VmFuelVendorItemDAO().getDTOs(null, -1, -1, true, userDTO, filterFuelRequest, false);

                    vmFuelVendorItemDTOS
                            .forEach(vmFuelVendorItemDTO -> {
                                List<VmFuelVendorItemDTO> existingItems = vendorIdToItems.getOrDefault(vmFuelVendorItemDTO.vmFuelVendorId, new ArrayList<>());
                                existingItems.add(vmFuelVendorItemDTO);
                                vendorIdToItems.put(vmFuelVendorItemDTO.vmFuelVendorId, existingItems);
                            });

                    data
                            .stream()
                            .filter(d -> d.status == ApprovalStatus.SATISFIED.getValue())
                            .forEach(d -> {
                                List<VmFuelVendorItemDTO> vendorItemDTOS = vendorIdToItems.getOrDefault(d.vendorId, new ArrayList<>());

                                HashMap<Integer, Double> fuelUnitPriceMap = new HashMap<>();
                                vendorItemDTOS
                                        .forEach(vendorItemDTO -> {
                                            fuelUnitPriceMap.put(vendorItemDTO.vehicleFuelCat, vendorItemDTO.price);
                                        });

                                List<VmFuelRequestItemDTO> vmFuelRequestItemDTOS = d.vmFuelRequestItemDTOList;

                                double totalPriceAtomic = 0.0;
                                for (VmFuelRequestItemDTO vmFuelRequestItemDTO: vmFuelRequestItemDTOS) {
                                    double price = fuelUnitPriceMap.getOrDefault(vmFuelRequestItemDTO.vehicleFuelCat, 0.0) * vmFuelRequestItemDTO.requestedAmount;
                                    totalPriceAtomic = (totalPriceAtomic + price);
                                }

                                double totalPrice = vehicleToUnpaidPrice.getOrDefault(d.vehicleId, 0.0).doubleValue() + totalPriceAtomic;
                                vehicleToUnpaidPrice.put(d.vehicleId, totalPrice);
                            });

                }

            }

                if (data != null && data.size()>0) {
                    for (int i = 0; i < data.size(); i++) {
                        Vm_fuel_requestDTO vm_fuel_requestDTO = data.get(i);
        %>
        <tr id='tr_<%=i%>'>
            <%@include file="vm_fuel_request_approval_mappingSearchRow.jsp"%>
        </tr>
        <%}}%>
        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>

			