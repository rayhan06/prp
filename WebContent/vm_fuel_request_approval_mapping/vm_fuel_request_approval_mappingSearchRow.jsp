<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.StringUtils" %>
<%@ page import="vm_fuel_request.*" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleDAO" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="vm_fuel_vendor.Vm_fuel_vendorDTO" %>
<%@ page import="vm_fuel_vendor.Vm_fuel_vendorDAO" %>
<%@ page import="vm_fuel_vendor.VmFuelVendorItemDTO" %>
<%@ page import="vm_fuel_vendor.VmFuelVendorItemDAO" %>
<%@ page import="java.util.concurrent.atomic.AtomicReference" %>
<%@ page import="javax.rmi.CORBA.Util" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>

<%
    Employee_recordsDTO driver = Employee_recordsRepository.getInstance().getById(vm_fuel_requestDTO.vehicleDriverAssignmentId);
    String driverName = Language.equals("English") ? driver.nameEng : driver.nameBng;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>
<td id='<%=i%>_cardInfoId'>
        <%
        Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().
            getVm_vehicleDTOByID(vm_fuel_requestDTO.vehicleId);
        String brand = "";
        String seats = "";
        String option = "";
        String Options = "";

        if(vm_vehicleDTO !=null){
             brand = CatRepository.getName(Language, "vehicle_brand", vm_vehicleDTO.vehicleBrandCat);
             seats = Language.equals("English") ? "Number of seats: " : "আসন সংখ্যা: ";
             option = vm_vehicleDTO.regNo + " " + brand + " " + vm_vehicleDTO.modelNo;
             Options = Utils.getDigits(option + "(" + seats + vm_vehicleDTO.numberOfSeats + ")", Language);
        }
    %>
        <%=Options%>

<td id='<%=i%>_cardEmployeeId'>
    <%=driverName%>
</td>
<td id='<%=i%>_cardEmployeeImagesId'>
    <%
        value = vm_fuel_requestDTO.applicationDate + "";
    %>
    <%
        if (vm_fuel_requestDTO.applicationDate != -1 && vm_fuel_requestDTO.applicationDate != 0) {
            String formatted_lastFuelDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>

    <%=Utils.getDigits(formatted_lastFuelDate, Language)%>
    <%
        }
    %>
</td>

<td id='<%=i%>_cardApprovalStatusCat'>
    <span class="font-weight-bold"
          style="color: <%=ApprovalStatus.getColor(vm_fuel_requestDTO.status)%>;">
        <%=CatRepository.getInstance().getText(Language, "vm_fuel_request_approval_status", vm_fuel_requestDTO.status)%>
    </span>
</td>

<td id='<%=i%>_approverId'>
    <%
        if (vm_fuel_requestDTO.vendorId != -1 && vm_fuel_requestDTO.status == ApprovalStatus.SATISFIED.getValue()) {
            List<VmFuelVendorItemDTO> vendorItemDTOS = vendorIdToItems.getOrDefault(vm_fuel_requestDTO.vendorId, new ArrayList<>());

            HashMap<Integer, Double> fuelUnitPriceMap = new HashMap<>();
            vendorItemDTOS
                    .forEach(vendorItemDTO -> {
                        fuelUnitPriceMap.put(vendorItemDTO.vehicleFuelCat, vendorItemDTO.price);
                    });

            List<VmFuelRequestItemDTO> vmFuelRequestItemDTOS = vm_fuel_requestDTO.vmFuelRequestItemDTOList;

            double totalPriceAtomic = 0.0;
            for (VmFuelRequestItemDTO vmFuelRequestItemDTO : vmFuelRequestItemDTOS) {
                double price = fuelUnitPriceMap.getOrDefault(vmFuelRequestItemDTO.vehicleFuelCat, 0.0) * vmFuelRequestItemDTO.requestedAmount;
                totalPriceAtomic += price;
            }

            double totalPrice = 0.0;
            if (vehicleToUnpaidPrice.containsKey(vm_fuel_requestDTO.vehicleId)) {
                totalPrice = vehicleToUnpaidPrice.get(vm_fuel_requestDTO.vehicleId) - totalPriceAtomic;
            }
    %>
    <%=Utils.getDigits((int) (Math.round(totalPrice * 1.075 * 100)) / 100.0, Language)%>
    <%
        }
    %>
</td>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="event.preventDefault();location.href='Vm_fuel_request_approval_mappingServlet?actionType=getApprovalPage&cardInfoId=<%=vm_fuel_requestDTO.iD%>&language=<%=Language%>'"
    >
        <i class="fa fa-eye"></i>
    </button>
</td>

<td>
    <%
        if (vm_fuel_requestDTO.status == Vm_fuel_requestStatusEnum.APPROVED.getValue()) {
    %>
    <button class="btn btn-sm border-0 shadow" style="background-color: #5867dd; color: white; border-radius: 8px"
            onclick="event.preventDefault();location.href='Vm_fuel_request_approval_mappingServlet?actionType=getPaymentPage&cardInfoId=<%=vm_fuel_requestDTO.iD%>&language=<%=Language%>'">
        <%=LM.getText(LC.VM_REQUISITION_ADD_ACCEPT_PAYMENT, loginDTO)%>
    </button>
    <%
        }
    %>
</td>
																						
											

