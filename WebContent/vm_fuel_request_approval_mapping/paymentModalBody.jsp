<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="dbm.DBMW" %>

<%
    String checkboxHtml = request.getParameter("checkboxHtml");
%>

<div id="drop_down" class="row">
    <div class="col-md-12">
        <div id="ajax-content">
            <form class="kt-form"
                  id="bigformPayment" name="bigformPayment"
                  action="Vm_fuel_request_approval_mappingServlet?actionType=paidVm_fuel_request"
                  method="POST" enctype="multipart/form-data"
            >
                <div class="kt-form">
                    <div class="form-group row">
                        <label class="col-2 col-form-label">
                            <%=LM.getText(LC.VM_FUEL_REQUEST_ADD_SAROKNUMBER, loginDTO)%>
                        </label>
                        <div class="col-10">
                            <input type='text' class='form-control' name='sarokNumber' id='sarokNumber_text_0'
                                   tag='pb_html'/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">
                            <%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPLICATIONDATE, loginDTO)%>
                        </label>
                        <div class="col-10">
                            <%=checkboxHtml%>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">
                            <%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTREMARKS, loginDTO)%>
                        </label>
                        <div class="col-10">
                            <input type='text' class='form-control' name='paymentRemarks' id='paymentRemarks_text_0'
                                   tag='pb_html'/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">
                            <%=LM.getText(LC.VM_FUEL_REQUEST_ADD_FILESDROPZONE, loginDTO)%>
                        </label>
                        <div class="col-10">
                            <%
                                String fileColumnName = "filesDropzone";
                                long ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                            %>

                            <div class="dropzone"
                                 action="Vm_fuel_requestServlet?pageType=add&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=ColumnID%>">
                                <input type='file' style="display:none" name='<%=fileColumnName%>File'
                                       id='<%=fileColumnName%>_dropzone_File_0' tag='pb_html'/>
                            </div>
                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                   id='<%=fileColumnName%>FilesToDelete_0' value='' tag='pb_html'/>
                            <input type='hidden' name='<%=fileColumnName%>' id='<%=fileColumnName%>_dropzone_0'
                                   tag='pb_html' value='<%=ColumnID%>'/>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <button type="button" class="btn btn-sm cancel-btn shadow btn-border-radius text-white " data-dismiss="modal">
                        <%=LM.getText(LC.CLOSE_CLOSE, loginDTO)%>
                    </button>
                    <button class="btn btn-sm submit-btn shadow btn-border-radius text-white ml-2"
                        onclick="event.preventDefault();submitBigForm()"><%=LM.getText(LC.VM_REQUISITION_ADD_RECEIVE_VEHICLE_EDIT_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>