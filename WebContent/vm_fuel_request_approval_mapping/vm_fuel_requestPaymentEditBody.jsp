<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_fuel_request.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="vm_fuel_vendor.Vm_fuel_vendorDAO" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_assign.EmployeeSearchModel" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="employee_assign.EmployeeSearchIds" %>
<%@ page import="vm_vehicle.Vm_vehicleDAO" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="javax.rmi.CORBA.Util" %>
<%@ page import="org.apache.commons.codec.language.bm.Lang" %>
<%@ page import="vm_fuel_vendor.VmFuelVendorItemDTO" %>
<%@ page import="vm_fuel_vendor.VmFuelVendorItemDAO" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>


<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid p-0" id="kt_content" style="background: white">
    <div class=" shadow-none border-0">
        <div class="">
            <%
                Vm_fuel_requestDTO vm_fuel_requestDTO;
                vm_fuel_requestDTO = (Vm_fuel_requestDTO) request.getAttribute("vm_fuel_requestDTO");
                LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
                UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

                String filter = " status = 3 AND vehicle_id = " + vm_fuel_requestDTO.vehicleId + " ";
                List<Vm_fuel_requestDTO> unpaids = Vm_fuel_requestRepository.getInstance().getVm_fuel_requestDTOByvehicle_idAndStatus(vm_fuel_requestDTO.vehicleId, CommonApprovalStatus.SATISFIED.getValue());

                HashMap<Long, List<VmFuelVendorItemDTO>> vendorIdToItems = new HashMap<>();
                StringBuilder filterBuilder = new StringBuilder();

                String Language = LM.getText(LC.VM_FUEL_REQUEST_EDIT_LANGUAGE, loginDTO);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                String checkBox = "<div><input type=\"checkbox\" name=\"isPaid\" value=\"isPaidValue\"/><label>&nbsp;&nbsp;&nbsp;date</label></div>";

                StringBuilder checkboxHtml = new StringBuilder();

                unpaids
                        .forEach(dataMapping -> {
                            filterBuilder.append("," + dataMapping.vendorId);
                        });

                if (!unpaids.isEmpty()) {

                    filterBuilder.replace(0, 1, "(");
                    filterBuilder.append(")");

                    String filterFuelRequest = " vm_fuel_vendor_id in " + filterBuilder;
                    List<VmFuelVendorItemDTO> vmFuelVendorItemDTOS = new VmFuelVendorItemDAO().getDTOs(null, -1, -1, true, userDTO, filterFuelRequest, false);

                    vmFuelVendorItemDTOS
                            .forEach(vmFuelVendorItemDTO -> {
                                List<VmFuelVendorItemDTO> existingItems = vendorIdToItems.getOrDefault(vmFuelVendorItemDTO.vmFuelVendorId, new ArrayList<>());
                                existingItems.add(vmFuelVendorItemDTO);
                                vendorIdToItems.put(vmFuelVendorItemDTO.vmFuelVendorId, existingItems);
                            });
                }

                Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vm_fuel_requestDTO.vehicleId);
                String brand = CatRepository.getName(Language, "vehicle_brand", vm_vehicleDTO.vehicleBrandCat);
                String seats = Language.equals("English") ? "Number of seats: " : "আসন সংখ্যা: ";
                String option = vm_vehicleDTO.regNo + " " + brand + " " + vm_vehicleDTO.modelNo;

                String vehicleName = Utils.getDigits(option + "(" + seats + vm_vehicleDTO.numberOfSeats + ")", Language);

                String modalTitle = LM.getText(LC.VM_FUEL_REQUEST_ADD_VEHICLE_PAYMENT_DESCRIPTION, loginDTO) + vehicleName;

                double totalUnPaidBill = 0.0;


                for (Vm_fuel_requestDTO unpaid : unpaids) {

                    request.setAttribute("vm_fuel_requestDTO", unpaid);
                    request.setAttribute("vmFuelVendorItemDTOS", vendorIdToItems.get(unpaid.vendorId));

                    HashMap<Integer, Double> fuelUnitPriceMap = new HashMap<>();
                    vendorIdToItems.get(unpaid.vendorId)
                            .forEach(vendorItemDTO -> {
                                fuelUnitPriceMap.put(vendorItemDTO.vehicleFuelCat, vendorItemDTO.price);
                            });

                    double totalPriceAtomic = 0.0;
                    for (VmFuelRequestItemDTO vmFuelRequestItemDTO : unpaid.vmFuelRequestItemDTOList) {
                        double price = fuelUnitPriceMap.getOrDefault(vmFuelRequestItemDTO.vehicleFuelCat, 0.0) * vmFuelRequestItemDTO.requestedAmount;
                        totalPriceAtomic += price * 1.075;
                    }

                    totalUnPaidBill += totalPriceAtomic;

                    totalPriceAtomic = (int) (Math.round(totalPriceAtomic * 100)) / 100.0;
                    totalUnPaidBill = (int) (Math.round(totalUnPaidBill * 100)) / 100.0;

                    checkboxHtml.append(checkBox.replaceAll("isPaidValue", String.valueOf(unpaid.iD))
                            .replaceAll("date",
                                    Utils.getDigits(simpleDateFormat.format(new Date(unpaid.applicationDate)), Language) +
                                            " (" + Utils.getDigits(totalPriceAtomic, Language) + (Language.equals("English") ? " Taka" : " টাকা") + ")"));

            %>
            <jsp:include page="./vm_fuel_requestPaymentEditBodyEachUnpaid.jsp">
                <jsp:param name="pageName" value="searchrow"/>
                <jsp:param name="totalPriceAtomic" value="<%=totalPriceAtomic%>"/>
                <jsp:param name="vehicleName" value="<%=vehicleName%>"/>
            </jsp:include>
            <%
                }
            %>
            <div class="kt-portlet shadow-none">
                <div class="kt-portlet__body py-0 mb-5">
                    <div class="d-flex justify-content-between align-items-center">
                        <h5 class="table-title">
                            <%=LM.getText(LC.VM_FUEL_REQUEST_ADD_TOTAL_UNPAID_BILL, loginDTO)%><%=Utils.getDigits(totalUnPaidBill, Language) + (Language.equals("English") ? " Taka" : " টাকা")%>
                        </h5>
                        <div class="form-actions">
                            <button class="btn btn-success btn-sm shadow btn-border-radius"
                                    onclick="openPaymentModal()"><%=LM.getText(LC.PUBLIC_VIEW_PAYMENT, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="paymentModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
    <jsp:param name="modalTitle" value="<%=modalTitle%>"/>
    <jsp:param name="checkboxHtml" value="<%=checkboxHtml%>"/>
</jsp:include>


<script type="text/javascript">

    function openPaymentModal() {
        $('#payment_modal').modal();
    }


</script>


