<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_fuel_request.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="vm_fuel_vendor.Vm_fuel_vendorDAO" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_assign.EmployeeSearchModel" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="employee_assign.EmployeeSearchIds" %>
<%@ page import="vm_vehicle.Vm_vehicleDAO" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>

<%
    Vm_fuel_requestDTO vm_fuel_requestDTO;
    vm_fuel_requestDTO = (Vm_fuel_requestDTO) request.getAttribute("vm_fuel_requestDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    EmployeeSearchModel own = new Gson().fromJson(EmployeeSearchModalUtil.getEmployeeSearchModelJson(userDTO.employee_record_id, userDTO.unitID, userDTO.organogramID), EmployeeSearchModel.class);
    if (vm_fuel_requestDTO == null) {
        vm_fuel_requestDTO = new Vm_fuel_requestDTO();

    }
    System.out.println("vm_fuel_requestDTO = " + vm_fuel_requestDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ADD_FORMNAME, loginDTO);
    String servletName = "Vm_fuel_requestServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID = -1;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_FUEL_REQUEST_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    Employee_recordsDTO driver = Employee_recordsRepository.getInstance().getById(vm_fuel_requestDTO.vehicleDriverAssignmentId);
    String driverName = Language.equals("English") ? driver.nameEng : driver.nameBng;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    List<CategoryLanguageModel> fuelList = CatRepository.getInstance().getCategoryLanguageModelList("vehicle_fuel");
    HashMap<Integer, String> fuelNameMap = new HashMap<>();
    fuelList
            .forEach(fuel -> {
                fuelNameMap.put(fuel.categoryValue, Language.equals("English") ? fuel.englishText : fuel.banglaText);
            });
    String disabled = vm_fuel_requestDTO.status == Vm_fuel_requestStatusEnum.WAITING_FOR_APPROVAL.getValue() ? "" : "disabled";


    Calendar c = Calendar.getInstance();
    c.setTime(new Date());
//	c.add(Calendar.YEAR, i);
    Date today = c.getTime();
    SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
    String todayString = sf.format(today);
    int currentYear = Integer.parseInt(todayString.substring(6));
    int previousYear = currentYear - 1;
    int nextYear = currentYear + 1;

    String firstYear = "";
    String secondYear = "";

    String firstDateString = "01/07/";
    String secondDateString = "30/06/";

    int month = Integer.parseInt(todayString.substring(3, 5));
    firstYear = month > 6 ? String.valueOf(currentYear) : String.valueOf(previousYear);
    secondYear = month > 6 ? String.valueOf(nextYear) : String.valueOf(currentYear);

    firstDateString += firstYear;
    secondDateString += secondYear;

    long firstDate = sf.parse(firstDateString).getTime();
    long secondDate = sf.parse(secondDateString).getTime();

    HashMap<Integer, Long> currentFiscalFuelUseMap = new HashMap<>();

    String filterCurrentFiscal = " application_date >= " + firstDate +
            " AND application_date <= " + secondDate +
            " AND vehicle_id = " + vm_fuel_requestDTO.vehicleId +
            " AND isDeleted = 0 " +
            "AND status != " + CommonApprovalStatus.PENDING.getValue() + " " +
            "AND status != " + CommonApprovalStatus.CANCELLED.getValue() + " " +
            "AND status != " + CommonApprovalStatus.DISSATISFIED.getValue() + " ";
    List<Vm_fuel_requestDTO> fuelRequestsInurrentFiscal = Vm_fuel_requestRepository.getInstance().getVm_fuel_requestDTOByvehicle_idAndFiscal(vm_fuel_requestDTO.vehicleId, firstDate, secondDate);

    StringBuilder filterBuilder = new StringBuilder();

    fuelRequestsInurrentFiscal
            .forEach(fuel -> {
                filterBuilder.append("," + fuel.iD);
            });

    if (!fuelRequestsInurrentFiscal.isEmpty()) {

        filterBuilder.replace(0, 1, "(");
        filterBuilder.append(")");

        filterCurrentFiscal = " vm_fuel_request_id in " + filterBuilder;
        List<VmFuelRequestItemDTO> fuelItemsInurrentFiscal = new VmFuelRequestItemDAO().getDTOs(null, -1, -1, true, userDTO, filterCurrentFiscal, false);


        fuelItemsInurrentFiscal
                .forEach(fuelItem -> {
                    long fuelAmount = currentFiscalFuelUseMap.getOrDefault(fuelItem.vehicleFuelCat, Long.valueOf(0)) + fuelItem.requestedAmount;
                    currentFiscalFuelUseMap.put(fuelItem.vehicleFuelCat, fuelAmount);
                });


    }

    Fiscal_yearDTO currentFiscalYearDTO = new Fiscal_yearDAO().getFiscalYearBYDateLong(new Date().getTime());
    String currentFiscalYear = "(" + (Language.equals("English") ? currentFiscalYearDTO.nameEn : currentFiscalYearDTO.nameBn) + ")";


    VmFuelRequestItemDAO vmFuelRequestItemDAO = new VmFuelRequestItemDAO();
    HashMap<Integer, Long> previousAmountMap = new HashMap<>();
    HashMap<Integer, Long> previousDateMap = new HashMap<>();

    for (int fuelCat = 1; fuelCat < 13; fuelCat++) {
        String sql = "SELECT vehicle_fuel_cat, vm_fuel_request_id, requested_amount, vfr.application_date AS application_date FROM vm_fuel_request_item item " +
                "join vm_fuel_request vfr " +
                "on vfr.ID  = item.vm_fuel_request_id " +
                "where vfr.vehicle_id = " + vm_fuel_requestDTO.vehicleId + " " +
                "AND vehicle_fuel_cat  = " + fuelCat + " " +
                "AND requested_amount  > 0 " +
                "AND vfr.isDeleted = 0 " +
                "AND item.vm_fuel_request_id != " + vm_fuel_requestDTO.iD + " " +
                "AND vfr.application_date <= " + vm_fuel_requestDTO.applicationDate + " " +
                "AND vfr.status != " + CommonApprovalStatus.PENDING.getValue() + " " +
                "AND vfr.status != " + CommonApprovalStatus.CANCELLED.getValue() + " " +
                "AND vfr.status != " + CommonApprovalStatus.DISSATISFIED.getValue() + " " +
                "order by vfr.application_date DESC " +
                "limit 1 offset 0;";

        List<VmFuelRequestItemDTO> items = vmFuelRequestItemDAO.getDTOSBySQL(sql);

        if (!items.isEmpty()) {
            VmFuelRequestItemDTO item = items.get(0);
            previousAmountMap.put(item.vehicleFuelCat, item.requestedAmount);
            previousDateMap.put(item.vehicleFuelCat, item.lastModificationTime);
        }
    }


%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Vm_fuel_request_approval_mappingServlet?actionType=approvedVm_fuel_request"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="sub_title_top">
                                                <div class="sub_title">
                                                    <h4 style="background: white"><%=formTitle%>
                                                    </h4>
                                                </div>
                                            </div>


                                            <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                                   value='<%=vm_fuel_requestDTO.iD%>' tag='pb_html'/>

                                            <input type='hidden' class='form-control' name='insertedByUserId'
                                                   id='insertedByUserId_hidden_<%=i%>'
                                                   value='<%=vm_fuel_requestDTO.insertedByUserId%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                                   id='insertedByOrganogramId_hidden_<%=i%>'
                                                   value='<%=vm_fuel_requestDTO.insertedByOrganogramId%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertionDate'
                                                   id='insertionDate_hidden_<%=i%>'
                                                   value='<%=vm_fuel_requestDTO.insertionDate%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='lastModificationTime'
                                                   id='lastModificationTime_hidden_<%=i%>'
                                                   value='<%=vm_fuel_requestDTO.lastModificationTime%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='searchColumn'
                                                   id='searchColumn_hidden_<%=i%>'
                                                   value='<%=vm_fuel_requestDTO.searchColumn%>' tag='pb_html'/>

                                            <input type="hidden" name="cardInfoId" id="cardInfoId"
                                                   value='<%=vm_fuel_requestDTO.iD%>'>

                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VEHICLETYPE, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <%
                                                        Options = CatRepository.getName(Language, "vehicle_type", vm_fuel_requestDTO.vehicleType);
                                                    %>
                                                    <input readonly type='text' class='form-control'
                                                           value='<%=Options%>' tag='pb_html'/>

                                                </div>
                                            </div>
                                            <div class="form-group row" id="vehicleIdDiv">
                                                <label class="col-md-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VEHICLEID, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <%
                                                        Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance()
                                                                .getVm_vehicleDTOByID(vm_fuel_requestDTO.vehicleId);
                                                        String brand = "";
                                                        String seats = "";
                                                        String option = "";
                                                        if(vm_vehicleDTO != null){
                                                            brand = CatRepository.getName(Language, "vehicle_brand", vm_vehicleDTO.vehicleBrandCat);
                                                            seats = Language.equals("English") ? "Number of seats: " : "আসন সংখ্যা: ";
                                                            option = vm_vehicleDTO.regNo + " " + brand + " " + vm_vehicleDTO.modelNo;

                                                            Options = Utils.getDigits(option + "(" + seats + vm_vehicleDTO.numberOfSeats + ")", Language);
                                                        }
                                                    %>
                                                    <input readonly type='text' class='form-control'
                                                           value='<%=Options%>' tag='pb_html'/>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VEHICLEDRIVERASSIGNMENTID, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input readonly type='text' class='form-control'
                                                           value='<%=driverName%>' tag='pb_html'/>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPLICATIONDATE, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <%
                                                        value = simpleDateFormat.format(new Date(vm_fuel_requestDTO.applicationDate));
                                                    %>
                                                    <input readonly type='text' class='form-control' value='<%=value%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VENDORID, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <select <%=disabled%> class='form-control' name='vendorId'
                                                                          id='vendorId_select_<%=i%>' tag='pb_html'>
                                                        <%
                                                            Options = new Vm_fuel_vendorDAO().getOptions(Language, userDTO, vm_fuel_requestDTO.vendorId);
                                                        %>
                                                        <%=Options%>
                                                    </select>

                                                </div>
                                            </div>
                                            <%
                                                if (vm_fuel_requestDTO.status == ApprovalStatus.PAID.getValue()) {
                                            %>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_SAROKNUMBER, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <%
                                                        value = vm_fuel_requestDTO.sarokNumber + "";
                                                    %>
                                                    <input readonly type='text' class='form-control' value='<%=value%>'
                                                           tag='pb_html'/>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_LASTMODIFICATIONTIME, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <%
                                                        value = simpleDateFormat.format(new Date(vm_fuel_requestDTO.lastModificationTime));
                                                    %>
                                                    <input readonly type='text' class='form-control' value='<%=value%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTREMARKS, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <%
                                                        value = vm_fuel_requestDTO.paymentRemarks + "";
                                                    %>
                                                    <input readonly type='text' class='form-control' value='<%=value%>'
                                                           tag='pb_html'/>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_FILESDROPZONE, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <%
                                                        {
                                                            List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(vm_fuel_requestDTO.filesDropzone);
                                                    %>
                                                    <%@include file="../pb/dropzoneViewer.jsp" %>
                                                    <%
                                                        }
                                                    %>

                                                </div>
                                            </div>
                                            <%
                                                }
                                            %>

                                            <input type='hidden' class='form-control' name='isDeleted'
                                                   id='isDeleted_hidden_<%=i%>'
                                                   value='<%=vm_fuel_requestDTO.isDeleted%>' tag='pb_html'/>
                                            <input type='text' style='display:none' class='form-control'
                                                   name='requesterEmpId' id='requesterEmpId_hidden_<%=i%>'
                                                   value='<%=vm_fuel_requestDTO.requesterEmpId%>' tag='pb_html'/>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5">
                    <h5 class="table-title">
                        <%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM_VEHICLEFUELCAT, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM_REQUESTEDAMOUNT, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM_LASTFUELDATE, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM_LASTFUELAMOUNT, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_TOTAL_USED_IN_FISCAL_YEAR, loginDTO).replaceAll("currFis", currentFiscalYear)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="field-VmFuelRequestItem">


                            <%
                                if (true) {
                                    int index = -1;


                                    for (VmFuelRequestItemDTO vmFuelRequestItemDTO : vm_fuel_requestDTO.vmFuelRequestItemDTOList) {
                                        index++;

                                        System.out.println("index index = " + index);

                            %>

                            <tr id="VmFuelRequestItem_<%=index + 1%>">
                                <td>


                                    <input type='hidden' class='form-control' name='vmFuelRequestItem.iD'
                                           id='iD_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.iD%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.insertedByUserId'
                                           id='insertedByUserId_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.insertedByUserId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.insertionDate'
                                           id='insertionDate_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.lastFuelAmount'
                                           id='lastFuelAmount_text_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.lastFuelAmount%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.lastFuelDate'
                                           id='lastFuelDate_date_<%=childTableStartingID%>'
                                           value='<%=dateFormat.format(new Date(vmFuelRequestItemDTO.lastFuelDate))%>'
                                           tag='pb_html'>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.lastModificationTime'
                                           id='lastModificationTime_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.searchColumn'
                                           id='searchColumn_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.searchColumn%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.vmFuelRequestId'
                                           id='vmFuelRequestId_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.vmFuelRequestId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='vmFuelRequestItem.isDeleted'
                                           id='isDeleted_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.isDeleted%>' tag='pb_html'/>


                                    <input type='hidden' name='vmFuelRequestItem.vehicleFuelCat'
                                           id='vehicleFuelCat_text_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.vehicleFuelCat%>' tag='pb_html'/>
                                    <input readonly type='text' class='form-control'
                                           value='<%=fuelNameMap.getOrDefault(vmFuelRequestItemDTO.vehicleFuelCat, "")%>'
                                           tag='pb_html'/>


                                </td>
                                <td>


                                    <input readonly type='text' class='form-control'
                                           name='vmFuelRequestItem.requestedAmount'
                                           id='requestedAmount_text_<%=childTableStartingID%>'
                                           value='<%=Utils.getDigits(vmFuelRequestItemDTO.requestedAmount, Language)%>'
                                           tag='pb_html'/>
                                </td>
                                <td>

                                    <%
                                        long lastFuelDate = previousDateMap.getOrDefault(vmFuelRequestItemDTO.vehicleFuelCat, (long) -1);
                                        value = lastFuelDate + "";
                                    %>
                                    <%
                                        if (lastFuelDate != -1 && lastFuelDate != 0) {
                                            String formatted_lastFuelDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                    %>

                                    <input readonly type='text' class='form-control'
                                           name='vmFuelRequestItem.requestedAmount'
                                           id='lastDate_text_<%=childTableStartingID%>'
                                           value='<%=Utils.getDigits(formatted_lastFuelDate, Language)%>'
                                           tag='pb_html'/>
                                    <%
                                        }
                                    %>
                                </td>
                                <td>

                                    <input readonly type='text' class='form-control'
                                           name='vmFuelRequestItem.requestedAmount'
                                           id='lastAmount_text_<%=childTableStartingID%>'
                                           value='<%=Utils.getDigits(previousAmountMap.getOrDefault(vmFuelRequestItemDTO.vehicleFuelCat, Long.valueOf(0)), Language)%>'
                                           tag='pb_html'/>

                                </td>
                                <td>

                                    <input readonly type='text' class='form-control'
                                           name='vmFuelRequestItem.requestedAmount'
                                           id='total_text_<%=childTableStartingID%>'
                                           value='<%=Utils.getDigits(currentFiscalFuelUseMap.getOrDefault(vmFuelRequestItemDTO.vehicleFuelCat, Long.valueOf(0)), Language)%>'
                                           tag='pb_html'/>

                                </td>

                            </tr>
                            <%
                                        childTableStartingID++;
                                    }
                                }
                            %>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="text-right mt-3">
                    <%
                        if (vm_fuel_requestDTO.status == Vm_fuel_requestStatusEnum.WAITING_FOR_APPROVAL.getValue()) {
                    %>
                    <div class="form-actions text-right mb-5">
                        <a class="btn btn-sm cancel-btn text-white shadow btn-border-radius"
                           href="Vm_fuel_request_approval_mappingServlet?actionType=rejectVm_fuel_request&cardInfoId=<%=vm_fuel_requestDTO.iD%>"><%=LM.getText(LC.CARD_APPROVAL_MAPPING_DO_NOT_APPOVE, loginDTO)%>
                        </a>
                        <button class="btn  btn-sm submit-btn text-white shadow btn-border-radius ml-2"
                                type="submit"><%=LM.getText(LC.CARD_APPROVAL_MAPPING_APPROVAL, loginDTO)%>
                        </button>
                    </div>
                    <%
                        }
                    %>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    function loadVehicleList() {

        let url = "Vm_vehicleServlet?actionType=getAllByVehicleType&ID=" + '<%=vm_fuel_requestDTO.vehicleType%>' + "&vehicleId=" + '<%=vm_fuel_requestDTO.vehicleId%>';
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                // const response = JSON.parse(fetchedData);
                $("#vehicleId_select_0").html(fetchedData);
            },
            error: function (error) {
                console.log(error);
            }
        });

    }

    function PreprocessBeforeSubmiting(row, validate) {

        preprocessDateBeforeSubmitting('applicationDate', row);

        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Vm_fuel_requestServlet");
    }

    function init(row) {

        setDateByStringAndId('applicationDate_js_' + row, $('#applicationDate_date_' + row).val());
        loadVehicleList();
    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>


<style>
    .required {
        color: red;
    }
</style>






