<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LM" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="pb.*" %>


<%
    String url = request.getParameter("url");
    String navigator = request.getParameter("navigator");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    String context = "../../.." + request.getContextPath() + "/";
    String Language = LM.getText(LC.CARD_APPROVAL_MAPPING_EDIT_LANGUAGE, loginDTO);
    int pagination_number = 0;
%>


<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1" style="">
    <div class="kt-portlet__body">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label" for="vm_fuel_request_approval_status_cat">
                            <%=LM.getText(LC.VM_FUEL_REQUEST_ADD_STATUS, loginDTO)%></label>
                        <div class="col-md-10">
                            <select class='form-control' name='vm_fuel_request_approval_status_cat' id='vm_fuel_request_approval_status_cat'
                                    onSelect='setSearchChanged()'>
                                <%=CatRepository.getInstance().buildOptions("vm_fuel_request_approval_status", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ml-auto">
            <input type="hidden" name="search" value="yes"/>
            <button type="button" class="btn shadow " onclick="allfield_changed('',0)"
                    style="background-color: #00a1d4; color: white; border-radius: 6px!important; border-radius: 8px; margin-right: 8px">
                <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
            </button>
        </div>
    </div>
</div>


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script>
    const cardApprovalStatusCatSelector = $("#vm_fuel_request_approval_status_cat");
    $(document).ready(() => {
        select2SingleSelector('#vm_fuel_request_approval_status_cat', '<%=Language%>')
    });

	function dosubmit(params)
	{
		document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState === 4 && this.status === 200)
		    {
		    	document.getElementById('tableForm').innerHTML = this.responseText ;
				setPageNo();
				searchChanged = 0;
			}
		    else if(this.readyState === 4 && this.status !== 200)
			{
				alert('failed ' + this.status);
			}
		  };
		  
		  xhttp.open("Get", "<%=url%>&isPermanentTable=true>&" + params, true);
		  xhttp.send();
		
	}

    function allfield_changed(go, pagination_number) {
        var params = 'AnyField=' + "";

        if (cardApprovalStatusCatSelector.val()) {
            params += '&vm_fuel_request_approval_status_cat=' + cardApprovalStatusCatSelector.val();
        }

        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged === 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);
    }

</script>