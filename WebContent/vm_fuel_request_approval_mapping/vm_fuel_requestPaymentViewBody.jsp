<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_fuel_request.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>


<%

    String servletName = "Vm_fuel_requestServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.VM_FUEL_REQUEST_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Vm_fuel_requestDAO vm_fuel_requestDAO = new Vm_fuel_requestDAO("vm_fuel_request");
    Vm_fuel_requestDTO vm_fuel_requestDTO = Vm_fuel_requestRepository.getInstance().getVm_fuel_requestDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");


    VmFuelRequestItemDAO vmFuelRequestItemDAO = new VmFuelRequestItemDAO();
    HashMap<Integer, Long> previousAmountMap = new HashMap<>();
    HashMap<Integer, Long> previousDateMap = new HashMap<>();

    for (int fuelCat = 1; fuelCat < 13; fuelCat++) {
        String sql = "SELECT vehicle_fuel_cat, vm_fuel_request_id, requested_amount, vfr.application_date AS application_date FROM vm_fuel_request_item item " +
                "join vm_fuel_request vfr " +
                "on vfr.ID  = item.vm_fuel_request_id " +
                "where vfr.vehicle_id = " + vm_fuel_requestDTO.vehicleId + " " +
                "AND vehicle_fuel_cat  = " + fuelCat + " " +
                "AND requested_amount  > 0 " +
                "AND vfr.isDeleted = 0 " +
                "AND item.vm_fuel_request_id != " + vm_fuel_requestDTO.iD + " " +
                "AND vfr.application_date <= " + vm_fuel_requestDTO.applicationDate + " " +
                "AND vfr.status != " + CommonApprovalStatus.PENDING.getValue() + " " +
                "AND vfr.status != " + CommonApprovalStatus.CANCELLED.getValue() + " " +
                "AND vfr.status != " + CommonApprovalStatus.DISSATISFIED.getValue() + " " +
                "order by vfr.application_date DESC " +
                "limit 1 offset 0;";

        List<VmFuelRequestItemDTO> items = vmFuelRequestItemDAO.getDTOSBySQL(sql);

        if (!items.isEmpty()) {
            VmFuelRequestItemDTO item = items.get(0);
            previousAmountMap.put(item.vehicleFuelCat, item.requestedAmount);
            previousDateMap.put(item.vehicleFuelCat, item.lastModificationTime);
        }
    }



%>


<!-- <div class="modal-content viewmodal"> -->
<div class="menubottom">
    <div class="modal-header">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-9 col-sm-12">
                    <h5 class="modal-title"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ADD_FORMNAME, loginDTO)%>
                    </h5>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-success app_register"
                               data-id="419637"> Register </a>
                        </div>
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-danger app_reject"
                               data-id="419637"> Reject </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <div class="modal-body container">

        <div class="row div_border office-div">

            <div class="col-md-12">
                <h3><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ADD_FORMNAME, loginDTO)%>
                </h3>
                <table class="table table-bordered table-striped">


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VEHICLETYPE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.vehicleType + "";
                            %>
                            <%
                                //											value = CommonDAO.getName(Integer.parseInt(value), "vehicle", Language.equals("English")?"name_en":"name_bn", "id");
                                value = CatRepository.getName(Language, "vehicle_type", vm_fuel_requestDTO.vehicleType);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VEHICLEID, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.vehicleId + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPLICATIONDATE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.applicationDate + "";
                            %>
                            <%
                                String formatted_applicationDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                            %>
                            <%=Utils.getDigits(formatted_applicationDate, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VEHICLEDRIVERASSIGNMENTID, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.vehicleDriverAssignmentId + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_LASTFUELDATE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.lastFuelDate + "";
                            %>
                            <%
                                String formatted_lastFuelDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                            %>
                            <%=Utils.getDigits(formatted_lastFuelDate, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_STATUS, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.status + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_FISCALYEARID, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.fiscalYearId + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVEDDATE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.approvedDate + "";
                            %>
                            <%
                                String formatted_approvedDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                            %>
                            <%=Utils.getDigits(formatted_approvedDate, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VENDORID, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.vendorId + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_SAROKNUMBER, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.sarokNumber + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_FILESDROPZONE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.filesDropzone + "";
                            %>
                            <%
                                {
                                    List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(vm_fuel_requestDTO.filesDropzone);
                            %>
                            <%@include file="../pb/dropzoneViewer.jsp" %>
                            <%
                                }
                            %>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTREMARKS, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.paymentRemarks + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTERORGID, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.requesterOrgId + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTEROFFICEID, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.requesterOfficeId + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTEROFFICEUNITID, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.requesterOfficeUnitId + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTEREMPID, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.requesterEmpId + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTERPHONENUM, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.requesterPhoneNum + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTERNAMEEN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.requesterNameEn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTERNAMEBN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.requesterNameBn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTEROFFICENAMEEN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.requesterOfficeNameEn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTEROFFICENAMEBN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.requesterOfficeNameBn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTEROFFICEUNITNAMEEN, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.requesterOfficeUnitNameEn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTEROFFICEUNITNAMEBN, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.requesterOfficeUnitNameBn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTEROFFICEUNITORGNAMEEN, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.requesterOfficeUnitOrgNameEn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTEROFFICEUNITORGNAMEBN, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.requesterOfficeUnitOrgNameBn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVERORGID, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.approverOrgId + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVEROFFICEID, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.approverOfficeId + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVEROFFICEUNITID, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.approverOfficeUnitId + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVEREMPID, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.approverEmpId + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVERPHONENUM, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.approverPhoneNum + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVERNAMEEN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.approverNameEn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVERNAMEBN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.approverNameBn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVEROFFICENAMEEN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.approverOfficeNameEn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVEROFFICENAMEBN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.approverOfficeNameBn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVEROFFICEUNITNAMEEN, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.approverOfficeUnitNameEn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVEROFFICEUNITNAMEBN, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.approverOfficeUnitNameBn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVEROFFICEUNITORGNAMEEN, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.approverOfficeUnitOrgNameEn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVEROFFICEUNITORGNAMEBN, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.approverOfficeUnitOrgNameBn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVERORGID, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.paymentReceiverOrgId + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVEROFFICEID, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.paymentReceiverOfficeId + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVEROFFICEUNITID, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.paymentReceiverOfficeUnitId + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVEREMPID, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.paymentReceiverEmpId + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVERPHONENUM, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.paymentReceiverPhoneNum + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVERNAMEEN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.paymentReceiverNameEn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVERNAMEBN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.paymentReceiverNameBn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVEROFFICENAMEEN, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.paymentReceiverOfficeNameEn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVEROFFICENAMEBN, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.paymentReceiverOfficeNameBn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVEROFFICEUNITNAMEEN, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.paymentReceiverOfficeUnitNameEn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVEROFFICEUNITNAMEBN, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.paymentReceiverOfficeUnitNameBn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVEROFFICEUNITORGNAMEEN, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameEn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVEROFFICEUNITORGNAMEBN, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameBn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                </table>
            </div>


        </div>

        <div class="row div_border attachement-div">
            <div class="col-md-12">
                <h5><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM, loginDTO)%>
                </h5>
                <table class="table table-bordered table-striped">
                    <tr>
                        <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM_VEHICLEFUELCAT, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM_REQUESTEDAMOUNT, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM_LASTFUELAMOUNT, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM_LASTFUELDATE, loginDTO)%>
                        </th>
                    </tr>
                    <%
                        List<VmFuelRequestItemDTO> vmFuelRequestItemDTOs = VmFuelRequestItemRepository.getInstance().getVmFuelRequestItemDTOByvmFuelRequestId(vm_fuel_requestDTO.iD);

                        for (VmFuelRequestItemDTO vmFuelRequestItemDTO : vmFuelRequestItemDTOs) {
                    %>
                    <tr>
                        <td>
                            <%
                                value = vmFuelRequestItemDTO.vehicleFuelCat + "";
                            %>
                            <%
                                value = CatRepository.getInstance().getText(Language, "vehicle_fuel", vmFuelRequestItemDTO.vehicleFuelCat);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>
                        <td>
                            <%
                                value = vmFuelRequestItemDTO.requestedAmount + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>
                        <td>
                            <%
                                value = vmFuelRequestItemDTO.lastFuelAmount + "";
                            %>

                            <%=Utils.getDigits(previousAmountMap.getOrDefault(vmFuelRequestItemDTO.vehicleFuelCat, Long.valueOf(0)), Language)%>


                        </td>
                        <td>
                            <%
                                long lastFuelDate = previousDateMap.getOrDefault(vmFuelRequestItemDTO.vehicleFuelCat, (long) -1);
                                value = lastFuelDate + "";
                            %>
                            <%
                                if (lastFuelDate != -1 && lastFuelDate != 0) {
                                    formatted_lastFuelDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                            %>
                            <%=Utils.getDigits(formatted_lastFuelDate, Language)%>
                            <%
                                }
                            %>

                        </td>
                    </tr>
                    <%

                        }

                    %>
                </table>
            </div>
        </div>


    </div>
</div>