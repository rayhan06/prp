<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="parliament_visit_cost.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    Parliament_visit_costDTO parliament_visit_costDTO;
    parliament_visit_costDTO = (Parliament_visit_costDTO) request.getAttribute("parliament_visit_costDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (parliament_visit_costDTO == null) {
        parliament_visit_costDTO = new Parliament_visit_costDTO();

    }
    System.out.println("parliament_visit_costDTO = " + parliament_visit_costDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.PARLIAMENT_VISIT_COST_ADD_PARLIAMENT_VISIT_COST_ADD_FORMNAME, loginDTO);
    String servletName = "Parliament_visit_costServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.PARLIAMENT_VISIT_COST_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Parliament_visit_costServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data">
            <%--              onsubmit="PreprocessBeforeSubmiting(0,'<%=actionName%>')">--%>
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=parliament_visit_costDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.PARLIAMENT_VISIT_COST_ADD_TYPEOFVISITOR, loginDTO)%>
                                        </label>
                                        <div class="col-4 d-flex align-items-center">
                                            <input type='checkbox' class='form-control-sm visitor-checkbox'
                                                   name='typeOfVisitor1'
                                                   id='typeOfVisitor1' value="true"
                                                <%=parliament_visit_costDTO.typeOfVisitor == 1?("checked"):""%>
                                                   tag='pb_html'>
                                            <label class="form-check-label ml-3"
                                                   for="typeOfVisitor1"
                                                   style="font-size:small;font-weight: bolder;">
                                                <%=Language.equalsIgnoreCase("English") ? "Domestic" : "স্থানীও"%>
                                            </label>
                                        </div>
                                        <div class="col-5 d-flex align-items-center">
                                            <input type='checkbox' class='form-control-sm  visitor-checkbox'
                                                   name='typeOfVisitor2'
                                                   id='typeOfVisitor2' value="false"
                                                <%=parliament_visit_costDTO.typeOfVisitor == 1?"":("checked")%>
                                                   tag='pb_html'>
                                            <label class="form-check-label ml-3"
                                                   for="typeOfVisitor1"
                                                   style="font-size:small;font-weight: bolder;">
                                                <%=Language.equalsIgnoreCase("English") ? "Foreigner" : "বিদেশী"%>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_VISIT_COST_ADD_COSTPERPERSON, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%
                                                value = "";
                                                if (parliament_visit_costDTO.costPerPerson != -1) {
                                                    value = parliament_visit_costDTO.costPerPerson + "";
                                                }
                                            %>
                                            <input type='number' class='form-control' name='costPerPerson'
                                                   id='costPerPerson_number_<%=i%>' value='<%=value%>' tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_VISIT_COST_ADD_STARTDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%value = "startDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='startDate' id='startDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(parliament_visit_costDTO.startDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_VISIT_COST_ADD_ENDDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%value = "endDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='endDate' id='endDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(parliament_visit_costDTO.endDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.PARLIAMENT_VISIT_COST_ADD_ISACTIVE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='checkbox' class='form-control-sm' name='isActive'
                                                   id='isActive_checkbox_<%=i%>' value='true'
                                                <%=parliament_visit_costDTO.isActive == 1 ?("checked"):""%>
                                                   tag='pb_html'>
                                        </div>
                                    </div>

                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=parliament_visit_costDTO.insertedByUserId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=parliament_visit_costDTO.insertedByOrganogramId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=parliament_visit_costDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModifierUser'
                                           id='lastModifierUser_hidden_<%=i%>'
                                           value='<%=parliament_visit_costDTO.lastModifierUser%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>' value='<%=parliament_visit_costDTO.isDeleted%>'
                                           tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=parliament_visit_costDTO.lastModificationTime%>' tag='pb_html'/>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button type="button" id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.PARLIAMENT_VISIT_COST_ADD_PARLIAMENT_VISIT_COST_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.PARLIAMENT_VISIT_COST_ADD_PARLIAMENT_VISIT_COST_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    // function PreprocessBeforeSubmiting(row, validate) {
    //
    //     // preprocessCheckBoxBeforeSubmitting('typeOfVisitor1', row);
    //     preprocessDateBeforeSubmitting('startDate', row);
    //     preprocessDateBeforeSubmitting('endDate', row);
    //     // preprocessCheckBoxBeforeSubmitting('isActive', row);
    //
    //     return true;
    // }

    $('#bigform').on('submit', () => {
        preprocessDateBeforeSubmitting('startDate', row);
        preprocessDateBeforeSubmitting('endDate', row);
    });

    $(".visitor-checkbox").on('click', function () {
        var $box = $(this);
        if ($box.is(":checked")) {
            $('.visitor-checkbox').prop("checked", false);
            $box.prop("checked", true);
        } else {
            $box.prop("checked", false);
        }
    });


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Parliament_visit_costServlet");
    }

    $('#startDate_js_0').on('datepicker.change', () => {
        setMinDateByTimestampAndId('endDate_js_0', getDateTimestampById('startDate_js_0'));
    });

    function init(row) {
        let date = new Date();
        resetMaxMinYearById('startDate_js_' + row, date.getFullYear() + 5, date.getFullYear() - 2);
        resetMaxMinYearById('endDate_js_' + row, date.getFullYear() + 5, date.getFullYear() - 2);
        setDateByStringAndId('startDate_js_' + row, $('#startDate_date_' + row).val());
        setDateByStringAndId('endDate_js_' + row, $('#endDate_date_' + row).val());
        setMinDateByTimestampAndId('endDate_js_0', getDateTimestampById('startDate_js_0'));
    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






