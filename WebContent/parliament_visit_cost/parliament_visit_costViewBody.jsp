<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="parliament_visit_cost.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>

<%
    String servletName = "Parliament_visit_costServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.PARLIAMENT_VISIT_COST_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Parliament_visit_costDAO parliament_visit_costDAO = new Parliament_visit_costDAO("parliament_visit_cost");
    Parliament_visit_costDTO parliament_visit_costDTO = parliament_visit_costDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PARLIAMENT_VISIT_COST_ADD_PARLIAMENT_VISIT_COST_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <h5 class="table-title">
                <%=LM.getText(LC.PARLIAMENT_VISIT_COST_ADD_PARLIAMENT_VISIT_COST_ADD_FORMNAME, loginDTO)%>
            </h5>
            <table class="table table-bordered table-striped">
                <tr>
                    <td
                    ><b>
                        <%=LM.getText(LC.PARLIAMENT_VISIT_COST_ADD_TYPEOFVISITOR, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            if (Language.equals("English"))
                                value = parliament_visit_costDTO.typeOfVisitor == 1 ? "Domestic" : "Foreigner";
                            else
                                value = parliament_visit_costDTO.typeOfVisitor == 1 ? "স্থানীও" : "বিদেশী";
                        %>
                        <%=value%>


                    </td>

                </tr>


                <tr>
                    <td><b><%=LM.getText(LC.PARLIAMENT_VISIT_COST_ADD_COSTPERPERSON, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = parliament_visit_costDTO.costPerPerson + "";
                        %>
                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>


                <tr>
                    <td><b><%=LM.getText(LC.PARLIAMENT_VISIT_COST_ADD_STARTDATE, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = parliament_visit_costDTO.startDate + "";
                        %>
                        <%
                            String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                        %>
                        <%=Utils.getDigits(formatted_startDate, Language)%>


                    </td>

                </tr>


                <tr>
                    <td><b><%=LM.getText(LC.PARLIAMENT_VISIT_COST_ADD_ENDDATE, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = parliament_visit_costDTO.endDate + "";
                        %>
                        <%
                            String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                        %>
                        <%=Utils.getDigits(formatted_endDate, Language)%>


                    </td>

                </tr>


            </table>
        </div>
    </div>
</div>