<%@page pageEncoding="UTF-8" %>

<%@page import="parliament_visit_cost.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.PARLIAMENT_VISIT_COST_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_PARLIAMENT_VISIT_COST;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Parliament_visit_costDTO parliament_visit_costDTO = (Parliament_visit_costDTO) request.getAttribute("parliament_visit_costDTO");
    CommonDTO commonDTO = parliament_visit_costDTO;
    String servletName = "Parliament_visit_costServlet";


    System.out.println("parliament_visit_costDTO = " + parliament_visit_costDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Parliament_visit_costDAO parliament_visit_costDAO = (Parliament_visit_costDAO) request.getAttribute("parliament_visit_costDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td id='<%=i%>_typeOfVisitor'>

    <%
        if (Language.equals("English"))
            value = parliament_visit_costDTO.typeOfVisitor == 1 ? "Domestic" : "Foreigner";
        else
            value = parliament_visit_costDTO.typeOfVisitor == 1 ? "স্থানীও" : "বিদেশী";
    %>

    <%=value%>


</td>

<td>

    <%
        value = parliament_visit_costDTO.costPerPerson + "";
    %>
    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_startDate'>
    <%
        value = parliament_visit_costDTO.startDate + "";
    %>
    <%
        String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_startDate, Language)%>


</td>

<td id='<%=i%>_endDate'>
    <%
        value = parliament_visit_costDTO.endDate + "";
    %>
    <%
        String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_endDate, Language)%>


</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Parliament_visit_costServlet?actionType=view&ID=<%=parliament_visit_costDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Parliament_visit_costServlet?actionType=getEditPage&ID=<%=parliament_visit_costDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=parliament_visit_costDTO.iD%>'/></span>
    </div>
</td>
																						
											

