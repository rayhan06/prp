<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_maintenance.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.UtilCharacter" %>
<%@page pageEncoding="UTF-8" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.VM_MAINTENANCE_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    Vm_maintenanceDAO vm_maintenanceDAO = (Vm_maintenanceDAO) request.getAttribute("vm_maintenanceDAO");


    String navigator2 = SessionConstants.NAV_VM_MAINTENANCE;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<%

    if (hasAjax == false) {
        Enumeration<String> parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {

            String paramName = parameterNames.nextElement();

            if (!paramName.equalsIgnoreCase("actionType")) {
                String[] paramValues = request.getParameterValues(paramName);
                for (int i = 0; i < paramValues.length; i++) {
                    String paramValue = paramValues[i];

%>

<%

                }
            }


        }
    }

%>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLEID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLEDRIVERASSIGNMENTID, loginDTO)%>
            </th>
            <%--								<th><%=LM.getText(LC.VM_MAINTENANCE_ADD_LASTMAINTENANCEDATE, loginDTO)%></th>--%>
            <th><%=LM.getText(LC.VM_MAINTENANCE_ADD_APPLICATIONDATE, loginDTO)%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "অনুমোদনের তারিখ", "Approval Date")%>
            </th>
            <th><%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLEOFFICENAME, loginDTO)%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "মোট বিল", "Total Bill")%>
            </th>
            <%--								<th></th>--%>

            <%--								<th><%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLETYPECAT, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_MAINTENANCE_ADD_STATUS, loginDTO)%></th>--%>

            <%--								<th><%=LM.getText(LC.VM_MAINTENANCE_ADD_REQUESTERPOSTID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_MAINTENANCE_ADD_REQUESTERUNITID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_MAINTENANCE_ADD_REQUESTEREMPLOYEERECORDID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLEOFFICENAMEBN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_MAINTENANCE_ADD_ISCHECKED, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_MAINTENANCE_ADD_MECHANICREMARKS, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_MAINTENANCE_ADD_MECHANICAPPROVEDATE, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_MAINTENANCE_ADD_AOAPPROVEDATE, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_MAINTENANCE_ADD_VATPERCENTAGE, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_MAINTENANCE_ADD_TOTALWITHOUTVAT, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_MAINTENANCE_ADD_TOTALWITHVAT, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_MAINTENANCE_ADD_TOTALVAT, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_MAINTENANCE_ADD_PAYMENTRECEIVEDDATE, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_MAINTENANCE_ADD_FISCALYEARID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_MAINTENANCE_ADD_SAROKNUMBER, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_MAINTENANCE_ADD_PAYMENTREMARKS, loginDTO)%></th>--%>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th></th>
            <%--								<th><%=LM.getText(LC.VM_MAINTENANCE_SEARCH_VM_MAINTENANCE_EDIT_BUTTON, loginDTO)%></th>--%>
            <%--								<th class="">--%>
            <%--									<span class="ml-4">All</span>--%>
            <%--									<div class="d-flex align-items-center mr-3">--%>
            <%--										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">--%>
            <%--											<i class="fa fa-trash"></i>&nbsp;--%>
            <%--										</button>--%>
            <%--										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>--%>
            <%--									</div>--%>
            <%--								</th>--%>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_VM_MAINTENANCE);

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Vm_maintenanceDTO vm_maintenanceDTO = (Vm_maintenanceDTO) data.get(i);


        %>
        <tr id='tr_<%=i%>'>
            <%

            %>


            <%
                request.setAttribute("vm_maintenanceDTO", vm_maintenanceDTO);
            %>

            <jsp:include page="vm_maintenanceSearchRow.jsp">
                <jsp:param name="pageName" value="searchrow"/>
                <jsp:param name="rownum" value="<%=i%>"/>
            </jsp:include>


            <%

            %>
        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			