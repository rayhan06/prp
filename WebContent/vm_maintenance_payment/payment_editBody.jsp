<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_maintenance.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>

<%
    //    Vm_maintenanceDTO vm_maintenanceDTO;
//    vm_maintenanceDTO = (Vm_maintenanceDTO)request.getAttribute("vm_maintenanceDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
//    if(vm_maintenanceDTO == null)
//    {
//        vm_maintenanceDTO = new Vm_maintenanceDTO();
//
//    }
//    System.out.println("vm_maintenanceDTO = " + vm_maintenanceDTO);
    long ID = Long.parseLong(request.getParameter("ID"));

    Vm_maintenanceDTO vm_maintenanceDTO = Vm_maintenanceRepository.getInstance().getVm_maintenanceDTOByID(ID);

    Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vm_maintenanceDTO.vehicleId);
    String regValue = "";
    if (vm_vehicleDTO != null) {
        regValue = vm_vehicleDTO.regNo;
    }

    String engFormName = "Payment of Vehicle No " + regValue;
    String bngFormName = "গাড়ি নম্বর " + regValue + " এর পেমেন্ট";

    String actionName = "add";
    System.out.println("actionType = " + request.getParameter("actionType"));
//    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
//    {
//        actionName = "add";
//    }
//    else
//    {
//        actionName = "edit";
//    }
    String formTitle = LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ADD_FORMNAME, loginDTO);
    String servletName = "Vm_maintenanceServlet";
    String fileColumnName = "";

//    String ID = request.getParameter("ID");
//    if(ID == null || ID.isEmpty())
//    {
//        ID = "0";
//    }
//    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID = -1;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_MAINTENANCE_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=UtilCharacter.getDataByLanguage(Language, "পেমেন্ট", "Payment")%>

                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Vm_maintenanceServlet?actionType=savePayment&ID=<%=vm_maintenanceDTO.iD%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="formSubmit()">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=UtilCharacter.getDataByLanguage(Language, bngFormName, engFormName)%>
                                            </h4>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_MAINTENANCE_ADD_SAROKNUMBER, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='sarokNumber'
                                                       id='sarokNumber_text_<%=i%>'
                                                       value='<%=vm_maintenanceDTO.sarokNumber%>' tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=UtilCharacter.getDataByLanguage(Language, "মন্তব্য", "Remarks")%>
                                            </label>
                                            <div class="col-md-8">
                                                <textarea class='form-control' name='paymentRemarks'
                                                          id='paymentRemarksId'>

                                                </textarea>
                                            </div>

                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_MAINTENANCE_ADD_FILESDROPZONE, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <%
                                                    fileColumnName = "filesDropzone";
                                                    if (actionName.equals("edit")) {
                                                        List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(vm_maintenanceDTO.filesDropzone);
                                                %>
                                                <%@include file="../pb/dropzoneEditor.jsp" %>
                                                <%
                                                    } else {
                                                        ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                        vm_maintenanceDTO.filesDropzone = ColumnID;
                                                    }
                                                %>

                                                <div class="dropzone"
                                                     action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=vm_maintenanceDTO.filesDropzone%>">
                                                    <input type='file' style="display:none"
                                                           name='<%=fileColumnName%>File'
                                                           id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                                </div>
                                                <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                       id='<%=fileColumnName%>FilesToDelete_<%=i%>' value=''
                                                       tag='pb_html'/>
                                                <input type='hidden' name='<%=fileColumnName%>'
                                                       id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                       value='<%=vm_maintenanceDTO.filesDropzone%>'/>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 form-actions text-right mt-3">
                        <%--                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">--%>
                        <%--                        <%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_CANCEL_BUTTON, loginDTO)%>--%>
                        <%--                    </button>--%>
                        <button id = "submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                            <%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    let language = '<%=Language%>';
    $(document).ready(function () {
        CKEDITOR.replaceAll();

        let errSarok;
        if (language == 'english') {
            errSarok = 'Sarok Number Required';
        } else {
            errSarok = 'স্মারক নম্বর দিন';
        }

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                sarokNumber: {
                    required: true,
                },
            },
            messages: {
                sarokNumber: errSarok,
            }
        });
    });

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
    }

    function processBeforeSubmit(){
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
    }

    function formSubmit(){
        event.preventDefault();
        processBeforeSubmit();
        let form = $("#bigform");
        form.validate();
        let valid = form.valid();

        if(valid){
            buttonStateChange(true);
            let url = form.attr('action');
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });

        }
    }
</script>