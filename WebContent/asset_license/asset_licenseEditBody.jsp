<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="asset_license.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    Asset_licenseDTO asset_licenseDTO;
    asset_licenseDTO = (Asset_licenseDTO) request.getAttribute("asset_licenseDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (asset_licenseDTO == null) {
        asset_licenseDTO = new Asset_licenseDTO();

    }
    System.out.println("asset_licenseDTO = " + asset_licenseDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.ASSET_LICENSE_ADD_ASSET_LICENSE_ADD_FORMNAME, loginDTO);
    String servletName = "Asset_licenseServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.ASSET_LICENSE_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    int year = Calendar.getInstance().get(Calendar.YEAR);
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Asset_licenseServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input
                                            type='hidden' class='form-control' name='iD'
                                            id='iD_hidden_<%=i%>' value='<%=asset_licenseDTO.iD%>'
                                            tag='pb_html'
                                    />
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.ASSET_LICENSE_ADD_SOFTWARECAT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='softwareCat'
                                                    id='softwareCat'
                                                    onchange="getSoftwareSt(this.value, 'softwareSt')"
                                                    tag='pb_html'>
                                                <%
                                                    Options = CommonDAO.getOptionsWithWhere(Language, "asset_category", asset_licenseDTO.softwareCat, " asset_type_type = " + SessionConstants.ASSET_TYPE_LICENCE);
                                                %>
                                                <%=Options%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_LICENSE_ADD_SOFTWARENAME, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='softwareSt'
                                                    id='softwareSt' tag='pb_html'>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.HM_STATUS, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='licenseStatus'
                                                    id='licenseStatus_select_<%=i%>' tag='pb_html'>
                                                <%
                                                    Options = CatRepository.getInstance().buildOptions("asset_status", Language, 1);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_LICENSE_ADD_ASSETMANUFACTURERID, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='assetManufacturerId'
                                                    id='assetManufacturerId_hidden_<%=i%>'
                                                    tag='pb_html'>
                                                <%
                                                    Options = CommonDAO.getOptions(Language, "asset_manufacturer", asset_licenseDTO.assetManufacturerId);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_LICENSE_ADD_ASSETSUPPLIERID, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='assetSupplierId'
                                                    id='assetSupplierId_hidden_<%=i%>' tag='pb_html'>
                                                <%
                                                    Options = CommonDAO.getOptions(Language, "asset_supplier", asset_licenseDTO.assetSupplierId);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>

                                    <input type='hidden' class='form-control' name='numberOfLicense'
                                           id='numberOfLicense_number_<%=i%>' value='<%=value%>'
                                           tag='pb_html'>


                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.EMPLOYEE_LICENSE_KEY_ADD_ASSETLICENSEKEYID, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                                            <textarea class='form-control' name='licenseKey'
                                                                      id='licenseKeyId' tag='pb_html'></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_LICENSE_ADD_LICENSEDTONAME, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control'
                                                   name='licensedToName' id='licensedToName_text_<%=i%>'
                                                   value='<%=asset_licenseDTO.licensedToName%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_LICENSE_ADD_LICENSEDTOEMAIL, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control'
                                                   name='licensedToEmail'
                                                   id='licensedToEmail_text_<%=i%>'
                                                   value='<%=asset_licenseDTO.licensedToEmail%>' <%
                                                if (!actionName.equals("edit")) {
                                            %>
                                                   required="required"
                                                   pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$"
                                                   title="licensedToEmail must be a of valid email address format"
                                                    <%
                                                        }
                                                    %>
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_LICENSE_ADD_ISREASSIGNABLE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='checkbox' class='form-control-sm'
                                                   name='isReassignable'
                                                   id='isReassignable_checkbox_<%=i%>'
                                                   value='true'                                                                <%=(String.valueOf(asset_licenseDTO.isReassignable).equals("true"))?("checked"):""%>
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_LICENSE_ADD_EXPIRATIONDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%value = "expirationDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID"
                                                           value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                                <jsp:param name="END_YEAR" value="<%=year + 100%>"/>
                                            </jsp:include>
                                            <input type='hidden' name='expirationDate'
                                                   id='expirationDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(asset_licenseDTO.expirationDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_LICENSE_ADD_TERMINATIONDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%value = "terminationDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID"
                                                           value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                                <jsp:param name="END_YEAR" value="<%=year + 100%>"/>
                                            </jsp:include>
                                            <input type='hidden' name='terminationDate'
                                                   id='terminationDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(asset_licenseDTO.terminationDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_LICENSE_ADD_PURCHASEDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%value = "purchaseDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID"
                                                           value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='purchaseDate'
                                                   id='purchaseDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(asset_licenseDTO.purchaseDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='assetSupplierId'
                                           id='assetSupplierId_hidden_<%=i%>'
                                           value='<%=asset_licenseDTO.assetSupplierId%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_LICENSE_ADD_ORDERNUMBER, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='orderNumber'
                                                   id='orderNumber_text_<%=i%>'
                                                   value='<%=asset_licenseDTO.orderNumber%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_LICENSE_ADD_PURCHASECOST, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                value = "";
                                                if (asset_licenseDTO.purchaseCost != -1) {
                                                    value = asset_licenseDTO.purchaseCost + "";
                                                }
                                            %>
                                            <input type='number' class='form-control'
                                                   name='purchaseCost' id='purchaseCost_number_<%=i%>'
                                                   value='<%=value%>' tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_LICENSE_ADD_ISDEPRECIATIONAPPLICABLE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='checkbox' class='form-control-sm mt-1'
                                                   name='isDepreciationApplicable'
                                                   id='isDepreciationApplicable_checkbox_<%=i%>'
                                                   value='true'      <%=(String.valueOf(asset_licenseDTO.isDepreciationApplicable).equals("true"))?("checked"):""%>
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_LICENSE_ADD_DESCRIPTION, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='description'
                                                   id='description_text_<%=i%>'
                                                   value='<%=asset_licenseDTO.description%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=asset_licenseDTO.insertedByUserId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=asset_licenseDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=asset_licenseDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModifierUser'
                                           id='lastModifierUser_hidden_<%=i%>'
                                           value='<%=asset_licenseDTO.lastModifierUser%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=asset_licenseDTO.searchColumn%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.HM_STATUS, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                value = "";
                                                if (asset_licenseDTO.status != -1) {
                                                    value = asset_licenseDTO.status + "";
                                                }
                                            %>
                                            <input type='number' class='form-control' name='status'
                                                   id='status_number_<%=i%>' value='<%=value%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value='<%=asset_licenseDTO.isDeleted%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control'
                                           name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=asset_licenseDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 form-actions text-right mt-3">
                        <a class="btn btn-sm cancel-btn shadow btn-border-radius text-light"
                           href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.ASSET_LICENSE_ADD_ASSET_LICENSE_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button class="btn btn-sm submit-btn shadow btn-border-radius text-light ml-2"
                                type="submit"><%=LM.getText(LC.ASSET_LICENSE_ADD_ASSET_LICENSE_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {


        preprocessCheckBoxBeforeSubmitting('isReassignable', row);
        preprocessDateBeforeSubmitting('expirationDate', row);
        preprocessDateBeforeSubmitting('terminationDate', row);
        preprocessDateBeforeSubmitting('purchaseDate', row);
        preprocessCheckBoxBeforeSubmitting('isDepreciationApplicable', row);

        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Asset_licenseServlet");
    }

    function init(row) {

        setDateByStringAndId('expirationDate_js_' + row, $('#expirationDate_date_' + row).val());
        setDateByStringAndId('terminationDate_js_' + row, $('#terminationDate_date_' + row).val());
        setDateByStringAndId('purchaseDate_js_' + row, $('#purchaseDate_date_' + row).val());

        $.validator.addMethod('licenseKeyMatch', function (value, element) {
            return checkProductKey();
        });
        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                licenseKey: {
                    required: true,
                    licenseKeyMatch: true
                }
            },
            messages: {
                licenseKey: 'number of license does not match'
            }
        });

    }

    var row = 0;
    $(document).ready(function () {
        init(row);
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    function getSoftwareSt(value, dropdown) {
        console.log("getting st, val = " + value);
        fillDropDown(value, dropdown, "Software_typeServlet?actionType=getSoftwareSt&cat=" + value + "&language=<%=Language%>");
    }

</script>






