<%@page pageEncoding="UTF-8" %>

<%@page import="asset_license.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="util.StringUtils" %>
<%@ page import="asset_license_key.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.ASSET_LICENSE_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_ASSET_LICENSE;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Asset_licenseDTO asset_licenseDTO = (Asset_licenseDTO) request.getAttribute("asset_licenseDTO");
    CommonDTO commonDTO = asset_licenseDTO;
    String servletName = "Asset_licenseServlet";


    System.out.println("asset_licenseDTO = " + asset_licenseDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Asset_licenseDAO asset_licenseDAO = (Asset_licenseDAO) request.getAttribute("asset_licenseDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
<td id='<%=i%>_softwareCat'>
    <%
        value = asset_licenseDTO.softwareCat + "";
    %>
    <%
        value = CommonDAO.getName(Language, "asset_category", asset_licenseDTO.softwareCat);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>
<td id='<%=i%>_softwareName'>
    <%
        value = asset_licenseDTO.softwareName + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_numberOfLicense'>
    <%
        Asset_license_keyDAO asset_license_keyDAO = new Asset_license_keyDAO("asset_license_key");

        value = asset_license_keyDAO.getDTOsLicenseCount(asset_licenseDTO.iD, "") + "";
    %>
    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_numberOfAvailableLicense'>

    <%
        value = asset_license_keyDAO.getDTOsLicenseCount(asset_licenseDTO.iD, "is_used = 0") + "";
    %>
    <%=Utils.getDigits(value, Language)%>

</td>

<td id='<%=i%>_assetManufacturerId'>


    <%=CommonDAO.getName(Language, "asset_manufacturer", asset_licenseDTO.assetManufacturerId)%>


</td>


<td id='<%=i%>_expirationDate'>
    <%
        value = asset_licenseDTO.expirationDate + "";
    %>
    <%
        String formatted_expirationDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_expirationDate, Language)%>


</td>


<td id='<%=i%>_purchaseDate'>
    <%
        value = CatRepository.getInstance().getText(Language, "asset_status", asset_licenseDTO.status);
    %>
    <%=value%>


</td>


<td>
	<button
			type="button"
			class="btn-sm border-0 shadow bg-light btn-border-radius"
			style="color: #ff6b6b;"
			onclick="location.href='Asset_licenseServlet?actionType=view&ID=<%=asset_licenseDTO.iD%>'">
		<i class="fa fa-eye"></i>
	</button>
</td>

<td id='<%=i%>_Edit'>
	<button
			class="btn-sm border-0 shadow btn-border-radius text-white"
			style="background-color: #ff6b6b;"
			onclick="location.href='Asset_licenseServlet?actionType=getEditPage&ID=<%=asset_licenseDTO.iD%>'">
		<i class="fa fa-edit"></i>
	</button>
</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=asset_licenseDTO.iD%>'/></span>
    </div>
</td>
																						
											

