<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="language.LM" %>
<%@ page import="language.LC" %>
<%@ page import="asset_license.Asset_licenseDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_records.Employee_recordsDAO" %>
<%@ page import="asset_license_key.Asset_license_keyRepository" %>
<%@ page import="asset_license.Asset_licenseDAO" %>
<%@ page import="pb.CatDAO" %>
<%@ page import="pb.CommonDAO" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="pb.Utils" %>
<%@ page import="util.StringUtils" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.ASSET_LICENSE_EDIT_LANGUAGE, loginDTO);
    Asset_licenseDTO asset_licenseDTO = (Asset_licenseDTO) request.getAttribute("asset_licenseDTO");
    String assetLicenseId = request.getParameter("ID");
    int assignOrRevoke = (int) request.getAttribute("assignOrRevoke");
    String action = "";
    if(assignOrRevoke == 1) action = "assignLicenseKey";
    else action = "revokeLicenseKey";
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    boolean isLanguageEnglish = Language.equals("English");
%>


<div class="box box-primary">
    <div class="box-body">
        <form class="form-horizontal" action="Asset_licenseServlet?actionType=<%=action%>"
              id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data" onsubmit="return PreprocessBeforeSubmiting()">
            <div class="form-body">

                <input type='hidden' class='form-control'  name='assetLicenseId' id = 'assetLicenseId' value='<%=assetLicenseId%>' tag='pb_html'/>

                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.ASSET_LICENSE_ADD_SOFTWARENAME, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id = 'softwareName_div' style="padding-top: 7px; padding-left: 20px;">
                        <%=asset_licenseDTO.softwareName%>
                    </div>
                </div>

                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.ASSET_LICENSE_ADD_SOFTWARECAT, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " style="padding-top: 7px; padding-left: 20px;">
                        <%=CatDAO.getName(Language, "software", asset_licenseDTO.softwareCat)%>
                    </div>
                </div>

                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.ASSET_LICENSE_ADD_ASSETMANUFACTURERID, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " style="padding-top: 7px; padding-left: 20px;">
                        <%=CommonDAO.getName(Language, "asset_manufacturer", asset_licenseDTO.assetManufacturerId)%>
                    </div>
                </div>

                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.ASSET_LICENSE_ADD_EXPIRATIONDATE, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " style="padding-top: 7px; padding-left: 20px;">
                        <%
                            String value = asset_licenseDTO.expirationDate + "";
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        %>
                        <%
                            String formatted_expirationDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                        %>
                        <%=Utils.getDigits(formatted_expirationDate, Language)%>
                    </div>
                </div>

                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.ASSET_LICENSE_ADD_PURCHASEDATE, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " style="padding-top: 7px; padding-left: 20px;">
                        <%
                            value = asset_licenseDTO.purchaseDate + "";
                        %>
                        <%
                            String formatted_purchaseDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                        %>
                        <%=Utils.getDigits(formatted_purchaseDate, Language)%>

                    </div>
                </div>

                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.ASSET_LICENSE_ADD_ISREASSIGNABLE, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " style="padding-top: 7px; padding-left: 20px;">
                        <%=StringUtils.getYesNo(Language, asset_licenseDTO.isReassignable)%>
                    </div>
                </div>

                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.OFFICE_AND_EMPLOYEE_MANAGEMENT_EMPLOYEE_NAME, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 ">

                            <% if(action.equals("assignLicenseKey")) { %>
                        <input type='hidden' class='form-control'  name='employeeRecordsId' id = 'employeeRecordsId' tag='pb_html'/>
                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius" id="tagEmp_modal_button">
                                <%=LM.getText(LC.HM_SELECT, userDTO)%>
                            </button>
                            <table class="table table-bordered table-striped">
                                <thead></thead>

                                <tbody id="tagged_emp_table">
                                <tr style="display: none;">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <button type="button" class="btn btn-danger btn-block"
                                                onclick="remove_containing_row(this,'tagged_emp_table');">
                                            Remove
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                            <% } else
                            { %>
                        <select class='form-control'  name='employeeRecordsId' id = 'employeeRecordsId'   tag='pb_html'>
                            <%=new Asset_licenseDAO().buildOptionsAssignedEmployee(asset_licenseDTO.iD, Language)%>
                        </select>
                            <% }
                            %>



                    </div>
                </div>
                <% if(action.equals("assignLicenseKey")) { %>
                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.ASSET_LICENSE_KEY_ADD_PRODUCTKEY, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 ">
                        <select class='form-control'  name='licenseKeyId' id = 'licenseKeyId'   tag='pb_html'>
                            <%=Asset_license_keyRepository.getInstance().buildOptionsKey(asset_licenseDTO.iD, Language)%>
                        </select>

                    </div>
                </div>
                <%
                }
                %>

                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.ASSET_LICENSE_ADD_DESCRIPTION, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 ">
                        <input type='text' class='form-control'  name='remarks' id = 'remarks' tag='pb_html'/>
                    </div>
                </div>




                <div class="form-actions text-center">
                    <button class="btn btn-success" type="submit">

                        <%=LM.getText(LC.ASSET_LICENSE_ADD_ASSET_LICENSE_SUBMIT_BUTTON, loginDTO)%>

                    </button>
                </div>

            </div>

        </form>

    </div>
</div>
<jsp:include page="../employee_assign/employeeSearchModal.jsp" >
    <jsp:param name="isHierarchyNeeded" value="false" />
</jsp:include>
<script type="text/javascript">


    $(document).ready( function(){

    });

    function PreprocessBeforeSubmiting() {
        document.getElementById('employeeRecordsId').value = +added_employee_info_map.keys().next().value;
        return true;
    }


    function init(row)
    {



    }

    var row = 0;

    window.onload =function ()
    {
        init(row);
        CKEDITOR.replaceAll();
    }


    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button

    // map to store and send added employee data
    added_employee_info_map = new Map();

    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_emp_table', {
                info_map: added_employee_info_map,
                isSingleEntry: true
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagEmp_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_emp_table';
        $('#search_emp_modal').modal();
    });
</script>






