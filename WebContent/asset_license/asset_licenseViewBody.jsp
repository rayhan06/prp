<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="asset_license.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="asset_license_key.*" %>

<%
    String servletName = "Asset_licenseServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.ASSET_LICENSE_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Asset_licenseDAO asset_licenseDAO = new Asset_licenseDAO("asset_license");
    Asset_licenseDTO asset_licenseDTO = asset_licenseDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    Asset_license_keyDAO asset_license_keyDAO = new Asset_license_keyDAO("asset_license_key");
    List<Asset_license_keyDTO> asset_license_keyDTOs = asset_license_keyDAO.getDTOsByLicenseId(asset_licenseDTO.iD);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.ASSET_LICENSE_ADD_ASSET_LICENSE_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div>
                <h5 class="table-title">
                    <%=LM.getText(LC.ASSET_LICENSE_ADD_ASSET_LICENSE_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">
                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.ASSET_LICENSE_ADD_SOFTWARECAT, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = asset_licenseDTO.softwareCat + "";
                            %>
                            <%
                                value = CommonDAO.getName(Language, "asset_category", asset_licenseDTO.softwareCat);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.ASSET_LICENSE_ADD_SOFTWARENAME, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = asset_licenseDTO.softwareName + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.ASSET_LICENSE_ADD_NUMBEROFLICENSE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = asset_license_keyDTOs.size() + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.ASSET_LICENSE_ADD_ASSETMANUFACTURERID, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = CommonDAO.getName(Language, "asset_manufacturer", asset_licenseDTO.assetManufacturerId);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.ASSET_LICENSE_ADD_LICENSEDTONAME, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = asset_licenseDTO.licensedToName + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.ASSET_LICENSE_ADD_LICENSEDTOEMAIL, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = asset_licenseDTO.licensedToEmail + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.ASSET_LICENSE_ADD_ISREASSIGNABLE, loginDTO)%>
                        </b></td>
                        <td>


                            <%=Utils.getYesNo(asset_licenseDTO.isReassignable, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.ASSET_LICENSE_ADD_EXPIRATIONDATE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = asset_licenseDTO.expirationDate + "";
                            %>
                            <%
                                String formatted_expirationDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                            %>
                            <%=Utils.getDigits(formatted_expirationDate, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.ASSET_LICENSE_ADD_TERMINATIONDATE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = asset_licenseDTO.terminationDate + "";
                            %>
                            <%
                                String formatted_terminationDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                            %>
                            <%=Utils.getDigits(formatted_terminationDate, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.ASSET_LICENSE_ADD_PURCHASEDATE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = asset_licenseDTO.purchaseDate + "";
                            %>
                            <%
                                String formatted_purchaseDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                            %>
                            <%=Utils.getDigits(formatted_purchaseDate, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.ASSET_LICENSE_ADD_ASSETSUPPLIERID, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = CommonDAO.getName(Language, "asset_supplier", asset_licenseDTO.assetSupplierId);

                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.ASSET_LICENSE_ADD_ORDERNUMBER, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = asset_licenseDTO.orderNumber + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.ASSET_LICENSE_ADD_PURCHASECOST, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                if (asset_licenseDTO.purchaseCost > 0) {
                                    value = asset_licenseDTO.purchaseCost + "";
                                } else {
                                    value = "";
                                }


                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.ASSET_LICENSE_ADD_ISDEPRECIATIONAPPLICABLE, loginDTO)%>
                            </b></td>
                        <td>


                            <%=Utils.getYesNo(asset_licenseDTO.isDepreciationApplicable, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.ASSET_LICENSE_ADD_DESCRIPTION, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = asset_licenseDTO.description + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.HM_STATUS, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = CatDAO.getName(Language, "asset_status", asset_licenseDTO.status);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                </table>
                </div>
                <h5 class="table-title mt-5">
                    <%=LM.getText(LC.ASSET_LICENSE_KEY_ADD_ASSET_LICENSE_KEY_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">
                    <thead>
                    <tr>
                        <td><b><%=LM.getText(LC.ASSET_LICENSE_KEY_ADD_PRODUCTKEY, loginDTO)%>
                        </b></td>

                        <td><b><%=LM.getText(LC.HM_ASSIGNED_TO, loginDTO)%>
                        </b></td>
                        <td><b><%=LM.getText(LC.HM_ASSIGN, loginDTO)%>
                        </b></td>
                    </tr>
                    </thead>
                    <tbody>
                    <%
                        for (Asset_license_keyDTO asset_license_keyDTO : asset_license_keyDTOs) {
                    %>
                    <tr>
                        <td><%=asset_license_keyDTO.productKey%>
                        </td>

                        <td><%=WorkflowController.getNameFromOrganogramId(asset_license_keyDTO.employeeRecordsId, Language)%>
                        </td>
                        <td>
                            <%
                                if (asset_license_keyDTO.isUsed) {
                            %>
                            <button type="button" class="btn btn-sm border-0 shadow"
                                    style="background-color: #773351; color: white; border-radius: 8px"
                                    onclick="location.href='Assign_assetServlet?actionType=getRecentPage&organogramId=<%=asset_license_keyDTO.employeeRecordsId%>'">
                                <%=LM.getText(LC.HM_REASSIGN, loginDTO)%>
                            </button>
                            <%
                            } else {
                            %>
                            <button type="button" class="btn btn-sm border-0 shadow"
                                    style="background-color: #cc3341; color: white; border-radius: 8px"
                                    onclick="location.href='Assign_assetServlet?actionType=getAddPage&asset_category_type=<%=asset_licenseDTO.softwareCat%>&asset_license_key_id=<%=asset_license_keyDTO.iD%>&asset_license_id=<%=asset_licenseDTO.iD%>&key=<%=asset_license_keyDTO.productKey%>'">
                                <%=LM.getText(LC.HM_ASSIGN, loginDTO)%>
                            </button>
                            <%
                                }
                            %>
                        </td>
                    </tr>

                    <%
                        }
                    %>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
