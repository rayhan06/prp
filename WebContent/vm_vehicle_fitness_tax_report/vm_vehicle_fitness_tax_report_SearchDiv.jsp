<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VM_VEHICLE_FITNESS_TAX_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row mx-2">
    <div class="col-12">
<%--        <%@include file="../pbreport/yearmonth.jsp"%>--%>
<%--        <%@include file="calendar.jsp"%>--%>
		<div id="visitDate" class="search-criteria-div" style="display: none">
			<div class="form-group row">
				<label class="col-md-3 col-form-label text-md-right">
					<%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.HM_DATE, loginDTO)%>
				</label>
				<div class="col-md-9">
					<jsp:include page="/date/date.jsp">
						<jsp:param name="DATE_ID" value="startDate_js"></jsp:param>
						<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
					</jsp:include>
					<input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
						   data-label="Document Date" id='startDate' name='startDate' value=""
						   tag='pb_html'
					/>
				</div>
			</div>
		</div>
		<div id="visitDate_3" class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 col-form-label text-md-right">
					<%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.HM_DATE, loginDTO)%>
				</label>
				<div class="col-md-9">
					<jsp:include page="/date/date.jsp">
						<jsp:param name="DATE_ID" value="endDate_js"></jsp:param>
						<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
					</jsp:include>
					<input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
						   data-label="Document Date" id='endDate' name='endDate' value=""
						   tag='pb_html'
					/>
				</div>
			</div>
		</div>

		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-md-right">
					<%=LM.getText(LC.VM_VEHICLE_FITNESS_TAX_REPORT_WHERE_VEHICLETYPECAT, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='vehicleTypeCat' id = 'vehicleTypeCat' >		
						<%		
						Options = CatDAO.getOptions(Language, "vehicle_type", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>


		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-md-right">
					<%=LM.getText(LC.REPORT_SEARCH_REPORT_SEARCH_FORMNAME, loginDTO)%>
				</label>
				<div class="col-sm-9">
<%--					<select class='form-control'  name='fitnessTaxTokenTypeCat' id = 'fitnessTaxTokenTypeCat' onchange="reportTypeChanged(this)" >--%>
						<select class='form-control'  name='fitnessTaxTokenTypeCat' id = 'fitnessTaxTokenTypeCat'  >
						<%
							Options = CatDAO.getOptions(Language, "vm_fitness_tax_token", CatDTO.CATDEFAULT);
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>


		<div  class="search-criteria-div" style = "display: none;">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-md-right">
					<%=LM.getText(LC.VM_VEHICLE_FITNESS_TAX_REPORT_WHERE_ISDELETED, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<input class='form-control'  name='isDeleted' id = 'isDeleted' value=""/>							
				</div>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">

$(document).ready(() => {
	showFooter = false;
});
function reportTypeChanged(selectedVal){
	
	if(selectedVal.value!=""){
		let colNum = parseInt(selectedVal.value)+2;
		let hiddenColNum = 5-parseInt(selectedVal.value);

		let visibleClassName =  "col_"+colNum;
		let hiddenClassName =  "col_"+hiddenColNum;

		$('.'+visibleClassName).show();
		$('.'+hiddenClassName).hide();
	}
	else{
		let colNum = 3;
		let hiddenColNum = 4;

		let visibleClassName =  "col_"+colNum;
		let hiddenClassName =  "col_"+hiddenColNum;

		$('.'+visibleClassName).show();
		$('.'+hiddenClassName).show();
	}




	// let x = document.getElementsByClassName(className);
	// x[0].style.visibility = 'hidden';;
}
function init()
{
    dateTimeInit($("#Language").val());
}
function PreprocessBeforeSubmiting()
{
}
</script>