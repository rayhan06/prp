<div id="calendardiv" style="display:none">
    <div id="visitDate" class="search-criteria-div" style="display: none">
        <div class="form-group row">
            <label class="col-3 col-form-label text-right">
                <%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.HM_DATE, loginDTO)%>
            </label>
            <div class="col-9">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="startDate_js"></jsp:param>
                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                </jsp:include>
                <input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
                       data-label="Document Date" id='startDate' name='startDate' value=""
                       tag='pb_html'
                />
            </div>
        </div>
    </div>
    <div id="visitDate_3" class="search-criteria-div">
        <div class="form-group row">
            <label class="col-3 col-form-label text-right">
                <%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.HM_DATE, loginDTO)%>
            </label>
            <div class="col-9">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="endDate_js"></jsp:param>
                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                </jsp:include>
                <input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
                       data-label="Document Date" id='endDate' name='endDate' value=""
                       tag='pb_html'
                />
            </div>
        </div>
    </div>
</div>