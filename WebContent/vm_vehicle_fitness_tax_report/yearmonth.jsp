<%@page import="java.util.Calendar" %>
<%@page import="pbReport.DateUtils" %>
<div id="datetype" class="search-criteria-div">
    <div class="form-group row">
        <label class="col-3 col-form-label text-right">
            <%=LM.getText(LC.HM_SEARCH_BY_DATE, loginDTO)%>
        </label>
        <div class="col-9">
            <input
                    type='checkbox'
                    class='form-control-sm mt-1'
                    id='search_by_date'
                    name='search_by_date' value=""
                   onchange="searchByDateChanged()"
                   tag='pb_html'
            />
        </div>
    </div>
</div>
<div id="ymdiv">
    <div id="year" class="search-criteria-div">
        <div class="form-group row">
            <label class="col-3 col-form-label text-right">
                <%=LM.getText(LC.HM_YEAR, loginDTO)%>
            </label>
            <div class="col-9">
                <select class='form-control' name='year' id='dYear' onchange="processYMAndSubmit();">
                    <option value=""><%=LM.getText(LC.HM_SELECT, loginDTO)%>
                    </option>
                    <%
                        Calendar calendar = Calendar.getInstance();
                        int currentYear = calendar.get(Calendar.YEAR);
                        for (int j = currentYear; j >= currentYear - 10; j--) {
                    %>
                    <option value="<%=j%>"><%=Utils.getDigits(j, Language)%>
                    </option>
                    <%
                        }
                    %>
                </select>
            </div>
        </div>
    </div>
</div>
<div id="month" class="search-criteria-div">
    <div class="form-group row">
        <label class="col-3 col-form-label text-right">
            <%=LM.getText(LC.HM_MONTH, loginDTO)%>
        </label>
        <div class="col-9">
            <select class='form-control' name='month' id='dMonth' onchange="processYMAndSubmit();">
                <option value=""><%=LM.getText(LC.HM_SELECT, loginDTO)%>
                </option>
                <%
                    for (int j = 0; j < 12; j++) {
                %>
                <option value="<%=j%>"><%=DateUtils.getMonthName(j, Language)%>
                </option>
                <%
                    }
                %>
            </select>
        </div>
    </div>
</div>