<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="ot_employee_type.OT_employee_typeRepository" %>
<%@ page import="ot_bill_submission_config.OT_bill_submission_configDAO" %>
<%@ page import="ot_bill_type_config.OT_bill_type_configRepository" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    System.out.println("Inside nav.jsp");
    String url = "Overtime_bill_financeServlet?actionType=search";
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1">
    <div class="kt-portlet__body">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="budgetMappingId">
                            <%=LM.getText(LC.BUDGET_OPERATION_CODE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select id="budgetMappingId" name='budgetMappingId'
                                    class='form-control rounded shadow-sm'
                                    onchange="budgetMappingIdChanged(this);">
                                <%=OT_bill_type_configRepository.getInstance().buildBudgetMappingDropDown(
                                        Language,
                                        null,
                                        true
                                )%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="economicSubCodeId">
                            <%=isLanguageEnglishNav ? "Economic Code" : "অর্থনৈতিক কোড"%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control rounded shadow-sm'
                                    name='economicSubCodeId' id='economicSubCodeId'
                                    onchange="economicSubCodeIdChanged()">
                                <%--Dynamically added by ajax on budget office change--%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="otBillSubmissionConfigId">
                            <%=isLanguageEnglishNav ? "Bill Date" : "বিলের তারিখ"%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control rounded shadow-sm'
                                    name="otBillSubmissionConfigId"
                                    id="otBillSubmissionConfigId"
                            >
                                <%=OT_bill_submission_configDAO.getInstance().buildOptions(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>

<template id="loader">
    <div class="modal-body">
        <div class="search-loader-container-circle ">
            <div class="search-loader-circle"></div>
        </div>
    </div>
</template>


<script type="text/javascript">
    const $economicSubCodeIdSelect = $('#economicSubCodeId');
    const $budgetMappingIdSelect = $('#budgetMappingId');

    $(document).ready(() => {
        select2SingleSelector('#budgetOfficeId', '<%=Language%>');
    });

    async function budgetMappingIdChanged(selectElem) {
        $economicSubCodeIdSelect.html('');
        const budgetMappingId = selectElem.value;
        if (budgetMappingId === '') {
            return;
        }
        const url = 'OT_bill_type_configServlet?actionType=ajax_getEconomicSubCodeDropDown'
                    + '&budgetMappingId=' + budgetMappingId;
        const response = await fetch(url);
        $economicSubCodeIdSelect.html(await response.text());
    }

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                setTimeout(() => {
                    document.getElementById('tableForm').innerHTML = this.responseText;
                    setPageNo();
                    searchChanged = 0;
                }, 200);
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "<%=url%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        let params = 'otBillSubmissionConfigId=' + $('#otBillSubmissionConfigId').val();
        params += '&budgetMappingId=' + $budgetMappingIdSelect.val();
        if($economicSubCodeIdSelect.val()) {
            params += '&economicSubCodeId=' + $economicSubCodeIdSelect.val();
        }
        params += '&search=true&ajax=true';
        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }
</script>