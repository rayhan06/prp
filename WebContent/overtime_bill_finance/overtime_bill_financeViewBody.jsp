<%@ page import="java.util.*" %>
<%@page import="util.*" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="overtime_bill.*" %>
<%@ page import="language.LM" %>
<%@ page import="language.LC" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="overtime_bill_finance.Overtime_bill_financeDTO" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLangEn = "english".equalsIgnoreCase(Language);
    String formTitle = LM.getText(LC.OVERTIME_ALLOWANCE_ADD_OVERTIME_ALLOWANCE_ADD_FORMNAME, loginDTO);

    Overtime_bill_financeDTO overtimeBillFinanceDTO = (Overtime_bill_financeDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);

    String formattedTotalBill = BangladeshiNumberFormatter.getFormattedNumber(
            StringUtils.convertBanglaIfLanguageIsBangla(
                    Language,
                    String.format("%d", overtimeBillFinanceDTO.totalBillAmount)
            )
    );

    List<Overtime_billSummaryModel> overtimeBillSummaryModels =
            Overtime_billDAO.getInstance()
                            .findByOvertimeBillFinanceId(overtimeBillFinanceDTO.iD)
                            .stream()
                            .sorted(Comparator.comparingInt(billDTO -> billDTO.financeSerialNumber))
                            .map(overtimeBillDTO -> new Overtime_billSummaryModel(overtimeBillDTO, Language))
                            .collect(Collectors.toList());

    String forwardingViewUrl = "Overtime_allowanceServlet?actionType=prepareBill&overtimeBillFinanceId=" + overtimeBillFinanceDTO.iD;
    String bankStatementViewUrl = "Overtime_allowanceServlet?actionType=prepareBankStatement&overtimeBillFinanceId=" + overtimeBillFinanceDTO.iD;
    String overtimeBillViewUrlFormat = "Overtime_billServlet?actionType=view&ID=%s";
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal" id="overtime-allowance-form" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12 row mt-2">
                        <div class="col-lg-4 col-md-6 form-group">
                            <label class="h5">
                                <%=LM.getText(LC.BUDGET_OPERATION_CODE, loginDTO)%>
                            </label>
                            <div class='form-control rounded shadow-sm'>
                                <%=Budget_mappingRepository.getInstance().getOperationText(
                                        Language,
                                        overtimeBillFinanceDTO.budgetMappingId
                                )%>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 form-group">
                            <label class="h5">
                                <%=isLangEn ? "Economic Code" : "অর্থনৈতিক কোড"%>
                            </label>
                            <div class='form-control rounded shadow-sm'>
                                <%=Economic_sub_codeRepository.getInstance().getText(
                                        Language,
                                        overtimeBillFinanceDTO.economicSubCodeId
                                )%>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 form-group">
                            <label class="h5">
                                <%=isLangEn ? "Bill Date" : "বিলের তারিখ"%>
                            </label>
                            <div class='form-control rounded shadow-sm'>
                                <%=overtimeBillFinanceDTO.getBillDateText(isLangEn)%>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" id="action-btn-div">
                    <div class="col-12 mt-3 text-right">
                        <a class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                           href="<%=bankStatementViewUrl%>"
                        >
                            <%=isLangEn ? "See Bank Statement" : "ব্যাংক স্টেটমেন্ট দেখুন"%>
                        </a>
                        <a class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                           href="<%=forwardingViewUrl%>"
                        >
                            <%=isLangEn ? "See Forwarding" : "ফরওয়ার্ডিং দেখুন"%>
                        </a>
                    </div>
                </div>

                <div class="mt-4 table-responsive">
                    <table id="overtime-allowance-table" class="table table-bordered table-striped text-nowrap">
                        <thead>
                        <tr class="text-center">
                            <th><%=isLangEn ? "Serial No" : "ক্রমিক নং"%>
                            </th>
                            <th><%=isLangEn ? "Bill Description" : "বিলের বিবরণ"%>
                            </th>
                            <th><%=isLangEn ? "Office/Section" : "কার্যালয়/শাখা"%>
                            </th>
                            <th><%=isLangEn ? "Bill Amount" : "টাকার পরিমাণ"%>
                            </th>
                            <th><%=isLangEn ? "View Details" : "বিস্তারিত দেখুন"%>
                            </th>
                        </tr>
                        </thead>
                        <tbody class="main-tbody">
                        <%for (Overtime_billSummaryModel overtimeBillSummaryModel : overtimeBillSummaryModels) {%>
                        <tr class="template-row">
                            <td class="text-center">
                                <%=StringUtils.convertBanglaIfLanguageIsBangla(
                                        Language,
                                        String.format("%d", overtimeBillSummaryModel.financeSerialNumber)
                                )%>
                            </td>
                            <td>
                                <%=overtimeBillSummaryModel.getBillDescriptionHtml()%>
                            </td>
                            <td class="row-data-officeName text-center">
                                <%=overtimeBillSummaryModel.officeName%>
                            </td>
                            <td class="row-data-billAmount text-right">
                                <%=overtimeBillSummaryModel.billAmount%>
                            </td>
                            <td class="details-button text-center">
                                <a class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
                                   href="<%=String.format(overtimeBillViewUrlFormat, overtimeBillSummaryModel.overtimeBillId)%>"
                                >
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>
                        </tr>
                        <%}%>
                        <tr>
                            <td colspan="3" class="text-right" style="font-weight: bold">
                                <%=isLangEn ? "Total" : "মোট"%>
                            </td>
                            <td class="text-right">
                                <%=formattedTotalBill%>
                            </td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
</div>