<%@page pageEncoding="UTF-8" %>

<%@page import="vm_vehicle_parts.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VM_VEHICLE_PARTS_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_VM_VEHICLE_PARTS;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Vm_vehicle_partsDTO vm_vehicle_partsDTO = (Vm_vehicle_partsDTO) request.getAttribute("vm_vehicle_partsDTO");
    CommonDTO commonDTO = vm_vehicle_partsDTO;
    String servletName = "Vm_vehicle_partsServlet";


    System.out.println("vm_vehicle_partsDTO = " + vm_vehicle_partsDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Vm_vehicle_partsDAO vm_vehicle_partsDAO = (Vm_vehicle_partsDAO) request.getAttribute("vm_vehicle_partsDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td id='<%=i%>_nameBn'>
    <%
        value = vm_vehicle_partsDTO.nameBn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_nameEn'>
    <%
        value = vm_vehicle_partsDTO.nameEn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td>
	<button
			type="button"
			class="btn-sm border-0 shadow bg-light btn-border-radius"
			style="color: #ff6b6b;"
			onclick="location.href='Vm_vehicle_partsServlet?actionType=view&ID=<%=vm_vehicle_partsDTO.iD%>'">
		<i class="fa fa-eye"></i>
	</button>
</td>

<td id='<%=i%>_Edit'>
	<button
			type="button"
			class="btn-sm border-0 shadow btn-border-radius text-white"
			style="background-color: #ff6b6b;"
			onclick="location.href='Vm_vehicle_partsServlet?actionType=getEditPage&ID=<%=vm_vehicle_partsDTO.iD%>'">
		<i class="fa fa-edit"></i>
	</button>
</td>


<%--											<td id='<%=i%>_checkbox'>--%>
<%--												<div class='checker'>--%>
<%--													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=vm_vehicle_partsDTO.iD%>'/></span>--%>
<%--												</div>--%>
<%--											</td>--%>
																						
											

