<%@page import="permission.MenuConstants" %>
<%@page import="permission.MenuUtil" %>
<%@page import="permission.MenuRepository" %>
<%@page import="permission.MenuDTO" %>
<%@page import="role.PermissionRepository" %>
<%@page import="config.GlobalConfigurationRepository" %>
<%@page import="config.GlobalConfigDTO" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="sessionmanager.SessionConstants" %>
<%

    MenuUtil menuUtil = new MenuUtil();

    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    List<MenuDTO> allMenuList = menuUtil.getAlignMenuListAll();

    int limit = allMenuList.size();
    if (request.getParameter("limit") != null) {
        limit = Integer.parseInt(request.getParameter("limit"));
    }

    int offset = 0;
    if (request.getParameter("offset") != null) {
        offset = Integer.parseInt(request.getParameter("offset"));
    }

    String like = "";
    if (request.getParameter("like") != null) {
        like = request.getParameter("like");
    }

    String constantlike = "";
    if (request.getParameter("constantlike") != null) {
        like = request.getParameter("constantlike");
    }
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-cogs"></i>&nbsp;
                    <%=LM.getText(LC.MENU_CONFIG_MENU_SETTINGS, loginDTO)%>
                </h3>
            </div>
        </div>
        <form role="form" action="MenuConfigurationServlet?actionType=edit" class="form-horizontal" method="post">
            <div class="kt-portlet__body form-body">
                <div class="form-body">
                    <div class="table-responsive">
                        <%
                            for (int isAPI = 0; isAPI < 2; isAPI++) {


                                int orderIndex = 0;

                        %>
                        <table id="tableData" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <td>ID</td>
                                <td><%=(isAPI == 1 ? "API " : "Menu") %>
                                </td>
                                <td>Menu Name Bangla</td>
                                <%
                                    if (isAPI == 0) {
                                %>
                                <td>Parent Menu</td>
                                <%} %>
                                <td>HyperLink</td>
                                <td>Request</td>
                                <%if (isAPI == 0) {%>
                                <td>Selected Menu</td>
                                <%} %>
                                <td>isVisible</td>
                                <td>isAPI</td>
                                <td>Delete</td>
                                <td>Order</td>
                                <%if (isAPI == 0) {%>
                                <td>Icon</td>
                                <%}%>
                                <td>Constant Name</td>
                            </tr>
                            </thead>
                            <tbody>
                            <%
                                int index = 0;
                                for (MenuDTO menuDTO : allMenuList) {
                                    if (menuDTO.isAPI != (isAPI == 1)) {
                                        continue;
                                    }

                                    if (index > limit + offset) {
                                        break;
                                    }
                                    if (index < offset) {
                                        ++orderIndex;
                                        continue;
                                    }

                                    menuDTO.menuName = menuDTO.menuName.replace(">", ">" + "<i class=\"" + menuDTO.icon + "\"></i> ");
                                    if (!like.equalsIgnoreCase("")) {
                                        if (!menuDTO.menuName.toLowerCase().contains(like.toLowerCase())) {
                                            ++orderIndex;
                                            continue;
                                        }
                                    }

                                    if (!constantlike.equalsIgnoreCase("")) {
                                        if (!menuDTO.constant.toLowerCase().contains(constantlike.toLowerCase())) {
                                            ++orderIndex;
                                            continue;
                                        }
                                    }

                                    index++;

                                    MenuDTO menuDTOFromRepo = MenuRepository.getInstance().getMenuDTOByMenuID(menuDTO.menuID);

                                    ++orderIndex;
                                    String menuName = MenuRepository.getInstance().getMenuDTOByMenuID(menuDTO.menuID).menuName;
                            %>
                            <tr>
                                <td><%=menuDTO.menuID %>
                                </td>
                                <td class="menu-style"><%=menuDTO.menuName.replace(menuDTOFromRepo.menuName, "")%><input
                                        type="hidden" name="menuID" value="<%=menuDTO.menuID%>"/>
                                    <input type="text" size="<%=menuName.length()%>" name="menuName" class="black-text"
                                           value="<%= menuName%>" required="required">
                                </td>
                                <td>
                                    <input type="text"
                                           size="<%=menuDTO.menuNameBangla!=null?menuDTO.menuNameBangla.length():0%>"
                                           name="menuNameBangla" class="black-text" value="<%= menuDTO.menuNameBangla%>"
                                           required="required">
                                </td>
                                <td width="2%" <%=(isAPI == 1 ? "class=\"invisible\" " : "")%>       >
                                    <select name="parentMenuID" class="black-text">
                                        <option value="-1">No Parent</option>
                                        <%

                                            for (MenuDTO dropDownMenu : allMenuList) {

                                                if (dropDownMenu.isAPI == true) {
                                                    continue;
                                                }

                                                if (menuUtil.isAncestorByRootAndNode(menuDTO, dropDownMenu)) {
                                                    continue;
                                                }

                                        %>
                                        <option  <%=(dropDownMenu.menuID == menuDTO.parentMenuID ? "Selected" : "")%>
                                                value="<%=dropDownMenu.menuID%>"><%=dropDownMenu.menuName %>
                                        </option>
                                        <%
                                            }

                                        %>
                                    </select>
                                </td>
                                <td><input size="<%= (""+menuDTO.hyperLink).length()%>" type="text" class="black-text"
                                           name="hyperLink" value="<%=menuDTO.hyperLink%>"></td>
                                <td>
                                    <select name="requestMethodType" class="black-text">
                                        <option  <%=(menuDTO.requestMethodType == 1 ? "Selected" : "") %> value="1">GET
                                        </option>
                                        <option <%=(menuDTO.requestMethodType == 2 ? "Selected" : "") %> value=2>POST
                                        </option>
                                    </select>
                                </td>
                                <td  <%=(isAPI == 1 ? "class=\"invisible\" " : "") %>   >
                                    <select name="selectedMenuID" class="black-text">
                                        <option value="-1">Select Menu</option>

                                        <%

                                            for (MenuDTO dropDownMenu : allMenuList) {

                                                if (dropDownMenu.isAPI == true) {
                                                    continue;
                                                }

                                                if (dropDownMenu.isVisible == false) {
                                                    continue;
                                                }


                                        %>
                                        <option  <%=(dropDownMenu.menuID == menuDTO.selectedMenuID ? "Selected" : "")%>
                                                value="<%=dropDownMenu.menuID %>"><%=dropDownMenu.menuName %>
                                        </option>
                                        <%
                                            }

                                        %>
                                    </select>
                                </td>
                                <td>
                                    <input <%= menuDTO.isVisible?"checked":""%> type="checkbox" name="isVisible"
                                                                                value="<%=menuDTO.menuID %>">
                                </td>
                                <td>
                                    <input <%= menuDTO.isAPI?"checked":""%> type="checkbox" name="isAPI"
                                                                            value="<%=menuDTO.menuID %>">
                                </td>
                                <td>
                                    <input type="checkbox" name="isDeleted" value="<%=menuDTO.menuID %>">
                                </td>
                                <td width="3%">
                                    <input class="form-control" type="text" name="orderIndex" value="<%=orderIndex%>">
                                </td>
                                <td  <%=(isAPI == 1 ? "class=\"invisible\" " : "") %>   >
                                    <input type="text" name="icon" class="black-text" value="<%=menuDTO.icon%>">
                                </td>
                                <td>
                                    <input type="text" name="menuConstantName" class="black-text"
                                           value="<%=menuDTO.constant%>" required="required">
                                </td>
                            </tr>
                            <%}%>
                            </tbody>
                        </table>
                        <%} %>
                    </div>
                    <%if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MENU_SETTINGS_UPDATE)) { %>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-12 text-right mt-3">
                                <input type="submit" class="btn btn-success btn-sm shadow btn-border-radius" value="Update">
                            </div>
                        </div>
                    </div>
                    <% }%>
                </div>
            </div>
        </form>
    </div>
</div>


<%if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MENU_ADD)) { %>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet mt-5">
        <form role="form" action="MenuConfigurationServlet?actionType=add" class="form-horizontal" method="post">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=LM.getText(LC.MENU_CONFIG_ADD_NEW_MENU, loginDTO)%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div>
                                        <input type="hidden" name="menuID" value="0"/><br>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MENU_CONFIG_MENU_NAME, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="menuName" name="menuName"
                                                       value="" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MENU_CONFIG_MENU_NAME_BANGLA, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="menuNameBangla"
                                                       name="menuNameBangla" value=""
                                                       required="required">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MENU_CONFIG_PARENT_MENU, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                <select name="parentMenuID" class="black-text form-control">
                                                    <option value="-1">No Parent</option>
                                                    <%

                                                        for (MenuDTO dropDownMenu : allMenuList) {

                                                            if (dropDownMenu.isAPI == true) {
                                                                continue;
                                                            }


                                                    %>
                                                    <option value="<%=dropDownMenu.menuID%>"><%=dropDownMenu.menuName %>
                                                    </option>
                                                    <%
                                                        }

                                                    %>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MENU_CONFIG_HYPERLINK, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="hyperLink" name="hyperLink"
                                                       value="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MENU_CONFIG_REQUEST_METHOD_TYPE, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                <select name="requestMethodType" class="black-text form-control">
                                                    <option value="1">GET</option>
                                                    <option value=2>POST</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MENU_CONFIG_SELECTED_MENU, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                <select name="selectedMenuID" class="black-text form-control">
                                                    <option value="-1">Select Menu</option>
                                                    <%

                                                        for (MenuDTO dropDownMenu : allMenuList) {

                                                            if (dropDownMenu.isAPI == true) {
                                                                continue;
                                                            }
                                                            if (dropDownMenu.isVisible == false) {
                                                                continue;
                                                            }
                                                    %>
                                                    <option value="<%=dropDownMenu.menuID %>"><%=dropDownMenu.menuName %>
                                                    </option>

                                                    <%
                                                        }

                                                    %>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MENU_CONFIG_VISIBILITY, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                <input type="checkbox" class="form-control-sm mt-1" id="isVisible"
                                                       name="isVisible">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MENU_CONFIG_IS_API, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                <input type="checkbox" class="form-control-sm mt-1" id="isAPI" name="isAPI">
                                            </div>
                                        </div>
                                        <input type="hidden" name="isDeleted" value="0"> <br>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MENU_CONFIG_ORDER_INDEX, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="orderIndex"
                                                       name="orderIndex" value="0">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MENU_CONFIG_ICON, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="icon" name="icon" value="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MENU_CONFIG_CONSTANT_NAME, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="menuConstantName" value=""
                                                       required="required">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 ">
                        <div class="text-right">
                            <button class="btn btn-success btn-sm shadow btn-border-radius"
                                    type="submit"><%=LM.getText(LC.GLOBAL_SUBMIT, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<%} %>








