<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="xray_report.*" %>
<%@page import="java.util.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.XRAY_TYPE_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row">
    <div class="col-12">
		
		<div  class="search-criteria-div" style = "display: none;">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.HM_X_RAY_TYPE, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select name='prescriptionLabTest.xRayReportId' style = "display:none"
                                                    id='xRayReportId_select_' tag='pb_html' onchange = "setDesc(this.id)" style="width:400px">
                          <option value = ""><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
                          <%
                          List<Xray_reportDTO> xray_reportDTOs = Xray_reportRepository.getInstance().getXray_reportList();
                          for(Xray_reportDTO xray_reportDTO: xray_reportDTOs)
                          {
                        	  %>
                        	  <option value = "<%=xray_reportDTO.iD%>" ><%=xray_reportDTO.comment%></option>
                        	  <%
                          }
                          %>
                          
                      </select>					
				</div>
			</div>
		</div>
	
        <%@include file="../pbreport/yearmonth.jsp"%>
        <%@include file="../pbreport/calendar.jsp"%>
		<div  class="search-criteria-div" style = "display: none;">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.XRAY_TYPE_REPORT_WHERE_XRAYREPORTID_5, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<input class='form-control'  name='xrayReportId_5' id = 'xrayReportId_5' value=""/>							
				</div>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">
function init()
{
    dateTimeInit($("#Language").val());
    $("#search_by_date").prop('checked', true);
    $("#search_by_date").trigger("change");
    setDateByStringAndId('startDate_js', '<%=datestr%>');
    setDateByStringAndId('endDate_js', '<%=datestr%>');
    add1WithEnd = true;
    processNewCalendarDateAndSubmit();
}
function PreprocessBeforeSubmiting()
{
}
</script>