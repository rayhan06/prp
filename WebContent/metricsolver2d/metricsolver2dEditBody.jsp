<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="metricsolver2d.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
Metricsolver2dDTO metricsolver2dDTO = new Metricsolver2dDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	metricsolver2dDTO = Metricsolver2dDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = metricsolver2dDTO;
String tableName = "metricsolver2d";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = "METRIC SOLVER";
String servletName = "Metricsolver2dServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Metricsolver2dServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=metricsolver2dDTO.iD%>' tag='pb_html'/>
														
														<div class="form-group row">
                                                            <label class="col-2">
															x
															</label>
                                                            <div class="col-4">
																	
																<input type='number' step = 'any' class='form-control'  name='x' id = 'x_number_<%=i%>' value='<%=metricsolver2dDTO.x%>'  tag='pb_html'>		
															</div>
                                                      
                                                            <label class="col-2">
															t
															</label>
                                                            <div class="col-4">
																	
																<input type='number' step = 'any' class='form-control'  name='t' id = 't_number_<%=i%>' value='<%=metricsolver2dDTO.t%>'  tag='pb_html'>		
															</div>
                                                      </div>									
																
	
													<div class="form-group row">
                                                            <label class="col-2">
															g<sub>tt</sub>
															</label>
                                                            <div class="col-4">
																<input type='text' class='form-control'  name='gtt' id = 'gtt_text_<%=i%>' value='<%=metricsolver2dDTO.gtt%>'   tag='pb_html'/>					
															</div>
                                                      
                                                            <label class="col-2">
															g<sub>xx</sub>
															</label>
															<div class="col-4">
																<input type='text' class='form-control'  name='gxx' id = 'gxx_text_<%=i%>' value='<%=metricsolver2dDTO.gxx%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-2">
															g<sub>tt</sub> (Numeric)
															</label>
                                                            <div class="col-4">
																		
																<input type='number' step = 'any' class='form-control'  name='gttNum' id = 'gttNum_number_<%=i%>' value='<%=metricsolver2dDTO.gttNum%>'  tag='pb_html'>		
															</div>
                                                      
                                                            <label class="col-2">
															g<sub>xx</sub> (Numeric)														</label>
                                                            <div class="col-4">
																		
																<input type='number' step = 'any' class='form-control'  name='gxxNum' id = 'gxxNum_number_<%=i%>' value='<%=metricsolver2dDTO.gxxNum%>'  tag='pb_html'>		
															</div>
                                                      </div>
                                                      	<h5>Diff Metrics:</h5>									
													<div class="form-group row">
                                                            <label class="col-1">
															 ∂<sub>t</sub> g<sub>tt</sub>														
															</label>
                                                            <div class="col-2">
																<input type='text' class='form-control'  name='dtGtt' id = 'dtGtt_text_<%=i%>' value='<%=metricsolver2dDTO.dtGtt%>'   tag='pb_html'/>					
															</div>
                                                      
                                                            <label class="col-1">
															∂<sub>x</sub> g<sub>tt</sub>
															</label>
                                                            <div class="col-2">
																<input type='text' class='form-control'  name='dxGtt' id = 'dxGtt_text_<%=i%>' value='<%=metricsolver2dDTO.dxGtt%>'   tag='pb_html'/>					
															</div>
                                                      
                                                            <label class="col-1">
															∂<sub>t</sub> g<sub>xx</sub>															
															</label>
                                                            <div class="col-2">
																<input type='text' class='form-control'  name='dtGxx' id = 'dtGxx_text_<%=i%>' value='<%=metricsolver2dDTO.dtGxx%>'   tag='pb_html'/>					
															</div>
                                                      
                                                            <label class="col-1">
															∂<sub>x</sub> g<sub>xx</sub>															
															</label>
                                                            <div class="col-2">
																<input type='text' class='form-control'  name='dxGxx' id = 'dxGxx_text_<%=i%>' value='<%=metricsolver2dDTO.dxGxx%>'   tag='pb_html'/>					
															</div>
                                                      </div>	
                                                      <h5>Numeric Values of Diffs:</h5>	
                                                      
                                                      <div class="form-group row">
                                                            <label class="col-1">
															 ∂<sub>t</sub> g<sub>tt</sub>														
															</label>
                                                            <div class="col-2">
																<input type='text' class='form-control'  name='dtGttNum' value='<%=metricsolver2dDTO.dtGttNum%>'   tag='pb_html'/>					
															</div>
                                                      
                                                            <label class="col-1">
															∂<sub>x</sub> g<sub>tt</sub>
															</label>
                                                            <div class="col-2">
																<input type='text' class='form-control'  name='dxGttNum' value='<%=metricsolver2dDTO.dxGttNum%>'   tag='pb_html'/>					
															</div>
                                                      
                                                            <label class="col-1">
															∂<sub>t</sub> g<sub>xx</sub>															
															</label>
                                                            <div class="col-2">
																<input type='text' class='form-control'  name='dtGxxNum' value='<%=metricsolver2dDTO.dtGxxNum%>'   tag='pb_html'/>					
															</div>
                                                      
                                                            <label class="col-1">
															∂<sub>x</sub> g<sub>xx</sub>															
															</label>
                                                            <div class="col-2">
																<input type='text' class='form-control'  name='dxGxxNum'  value='<%=metricsolver2dDTO.dxGxxNum%>'   tag='pb_html'/>					
															</div>
                                                      </div>							
																				
																
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                CANCEL
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                SUBMIT
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);

	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Metricsolver2dServlet");	
}

function init(row)
{


	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	


</script>






