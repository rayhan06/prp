
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="metricsolver2d.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
String navigator2 = "navMETRICSOLVER2D";
String servletName = "Metricsolver2dServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>x, t</th>
								<th>Equation</th>
								<th>Metrics</th>
								
								<th>Diff Metrics</th>
								
								<th>Gammas</th>
								
								<th>Nonzero Riemanns</th>

								
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
								<th>Edit</th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Metricsolver2dDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Metricsolver2dDTO metricsolver2dDTO = (Metricsolver2dDTO) data.get(i);
																																
											
											%>
											<tr>
											<td>
											<%=Utils.getDigits(String.format("%.1f", metricsolver2dDTO.x), Language)%>
											,
											<%=Utils.getDigits(String.format("%.1f", metricsolver2dDTO.t), Language)%>
											</td>
		
											<td>
											<%=Metricsolver2dDAO.getInstance().getEquation(metricsolver2dDTO)%>
											</td>
		
											<td>
											<%
											String metrics = Metricsolver2dDAO.getInstance().addSup(metricsolver2dDTO.gtt) + ", 0, 0, " 
											+ Metricsolver2dDAO.getInstance().addSup(metricsolver2dDTO.gxx);
											%>
											<%=Metricsolver2dDAO.getInstance().getTable2D(metrics)%>
											<br>
											<%
											String numMetrics = metricsolver2dDTO.gttNum + ", 0, 0, " + metricsolver2dDTO.gxxNum;
											%>
											<%=Metricsolver2dDAO.getInstance().getTable2D(numMetrics)%>
											</td>
											
											<td>
											<%
											String diffMetrics = metricsolver2dDTO.dtGtt + ", " 
											+ metricsolver2dDTO.dxGtt + ", " 
											+ metricsolver2dDTO.dtGxx + ", " 
											+ metricsolver2dDTO.dxGxx 
											;
											%>
											<%=Metricsolver2dDAO.getInstance().getTable2D(diffMetrics)%>
											
											<br>
											
											<%
											String numDiffMetrics = metricsolver2dDTO.dtGttNum + ", " 
											+ metricsolver2dDTO.dxGttNum + ", " 
											+ metricsolver2dDTO.dtGxxNum + ", " 
											+ metricsolver2dDTO.dxGxxNum 
											;
											%>
											<%=Metricsolver2dDAO.getInstance().getTable2D(numDiffMetrics)%>
											</td>
											
											<td>
											<%=Metricsolver2dDAO.getInstance().getGamma(metricsolver2dDTO.prettyGamma)%>
											<br>
											<%=Metricsolver2dDAO.getInstance().getGamma(metricsolver2dDTO.gammas)%>
											</td>
											
											<td>
											<%=metricsolver2dDTO.nonZeroRiemannCount < 0? "Undefined": metricsolver2dDTO.nonZeroRiemannCount%>
											</td>
		
											
		
		
		
		
	
											<%CommonDTO commonDTO = metricsolver2dDTO; %>
											<%@include file="../pb/searchAndViewButton.jsp"%>											
																						
											<td class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=metricsolver2dDTO.iD%>'/></span>
												</div>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									e.printStackTrace();
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			