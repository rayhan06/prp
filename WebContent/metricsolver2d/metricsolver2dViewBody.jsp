

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="metricsolver2d.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Metricsolver2dServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Metricsolver2dDTO metricsolver2dDTO = Metricsolver2dDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = metricsolver2dDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>



<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    Metric Solver
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body ">
            <div class="row mb-4">
                <div class="col-12">
                    <div class="onlyborder">
                        <div class="row ">
                        	
                            <div class="col-10">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            Metric Solver
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex ">
                                    <label class="col-2 col-form-label text-right">
                                        x, t
                                    </label>
                                    <div class="col-10">
										<%=Utils.getDigits(String.format("%.1f", metricsolver2dDTO.x), Language)%>
											,
											<%=Utils.getDigits(String.format("%.1f", metricsolver2dDTO.t), Language)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex ">
                                    <label class="col-2 col-form-label text-right">
                                        Equation
                                    </label>
                                    <div class="col-10">
										<%=Metricsolver2dDAO.getInstance().getEquation(metricsolver2dDTO)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex ">
                                    <label class="col-2 col-form-label text-right">
                                        Metrics
                                    </label>
                                    <div class="col-10">
											<%
											String metrics = Metricsolver2dDAO.getInstance().addSup(metricsolver2dDTO.gtt) + ", 0, 0, " 
											+ Metricsolver2dDAO.getInstance().addSup(metricsolver2dDTO.gxx);
											%>
											<%=Metricsolver2dDAO.getInstance().getTable2D(metrics)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex ">
                                    <label class="col-2 col-form-label text-right">
                                        Numetric Metrics
                                    </label>
                                    <div class="col-10">
											<%
											String numMetrics = metricsolver2dDTO.gttNum + ", 0, 0, " + metricsolver2dDTO.gxxNum;
											%>
											<%=Metricsolver2dDAO.getInstance().getTable2D(numMetrics)%>
											</td>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex ">
                                    <label class="col-2 col-form-label text-right">
                                        Diff Metrics
                                    </label>
                                    <div class="col-10">
										<%
											String diffMetrics = metricsolver2dDTO.dtGtt + ", " 
											+ metricsolver2dDTO.dxGtt + ", " 
											+ metricsolver2dDTO.dtGxx + ", " 
											+ metricsolver2dDTO.dxGxx 
											;
											%>
											<%=Metricsolver2dDAO.getInstance().getTable2D(diffMetrics)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex ">
                                    <label class="col-2 col-form-label text-right">
                                        Numeric Diff Metrics
                                    </label>
                                    <div class="col-10">
										<%
											String numDiffMetrics = metricsolver2dDTO.dtGttNum + ", " 
											+ metricsolver2dDTO.dxGttNum + ", " 
											+ metricsolver2dDTO.dtGxxNum + ", " 
											+ metricsolver2dDTO.dxGxxNum 
											;
											%>
											<%=Metricsolver2dDAO.getInstance().getTable2D(numDiffMetrics)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex ">
                                    <label class="col-2 col-form-label text-right">
                                        Gamma
                                    </label>
                                    <div class="col-10">
											<%=Metricsolver2dDAO.getInstance().getGamma(metricsolver2dDTO.sGamma)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex ">
                                    <label class="col-2 col-form-label text-right">
                                        Numeric Gamma
                                    </label>
                                    <div class="col-10">
											<%=Metricsolver2dDAO.getInstance().getGamma(metricsolver2dDTO.gammas)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex ">
                                    <label class="col-2 col-form-label text-right">
                                        Pretty Gamma
                                    </label>
                                    <div class="col-10">
										<%=Metricsolver2dDAO.getInstance().getGamma(metricsolver2dDTO.prettyGamma)%>
                                    </div>
                                </div>
                                
                                <div class="form-group row d-flex ">
                                    <label class="col-2 col-form-label text-right">
                                        Numeric Riemann
                                    </label>
                                    <div class="col-10">
										<%=Metricsolver2dDAO.getInstance().getRiemann(metricsolver2dDTO.riemanns)%>
                                    </div>
                                </div>
                                
                                <div class="form-group row d-flex table-responsive">
                                    <label class="col-2 col-form-label text-right">
                                        Edit Gamma
                                    </label>
                                    <div class="col-10">
                                    	<form class="form-horizontal"
								              action="Metricsolver2dServlet?actionType=prettyfyGamma"
								              method="POST" enctype="multipart/form-data"
								              >
								              <input type='hidden' class='form-control'  name='iD'  value='<%=metricsolver2dDTO.iD%>' tag='pb_html'/>
										<%=Metricsolver2dDAO.getInstance().getGammaEditable(metricsolver2dDTO.prettyGamma, metricsolver2dDTO.diffGamma, metricsolver2dDTO.diffGammaNum)%>
										<br>
										<button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
			                                Submit
			                            </button>
										</form>
                                    </div>
                                </div>
			
							
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>