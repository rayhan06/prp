<%@page pageEncoding="UTF-8" %>

<%@page import="asset_license_key.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.ASSET_LICENSE_KEY_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_ASSET_LICENSE_KEY;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Asset_license_keyDTO asset_license_keyDTO = (Asset_license_keyDTO)request.getAttribute("asset_license_keyDTO");
CommonDTO commonDTO = asset_license_keyDTO;
String servletName = "Asset_license_keyServlet";


System.out.println("asset_license_keyDTO = " + asset_license_keyDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Asset_license_keyDAO asset_license_keyDAO = (Asset_license_keyDAO)request.getAttribute("asset_license_keyDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
		
											<td id = '<%=i%>_assetLicenseId'>
											<%
											value = asset_license_keyDTO.assetLicenseId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_productKey'>
											<%
											value = asset_license_keyDTO.productKey + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
		
		
		
		
	

											<td>
												<a href='Asset_license_keyServlet?actionType=view&ID=<%=asset_license_keyDTO.iD%>'><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></a>
										
											</td>
	
											<td id = '<%=i%>_Edit'>																																	
	
												<a href='Asset_license_keyServlet?actionType=getEditPage&ID=<%=asset_license_keyDTO.iD%>'><%=LM.getText(LC.ASSET_LICENSE_KEY_SEARCH_ASSET_LICENSE_KEY_EDIT_BUTTON, loginDTO)%></a>
																				
											</td>											
											
											
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=asset_license_keyDTO.iD%>'/></span>
												</div>
											</td>
																						
											

