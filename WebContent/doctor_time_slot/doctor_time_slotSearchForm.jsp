<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="doctor_time_slot.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.DOCTOR_TIME_SLOT_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    Doctor_time_slotDAO doctor_time_slotDAO = (Doctor_time_slotDAO) request.getAttribute("doctor_time_slotDAO");


    String navigator2 = SessionConstants.NAV_DOCTOR_TIME_SLOT;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>




<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th class=""><%=LM.getText(LC.HM_DOCTOR, loginDTO)%>
            </th>
            <th class=""><%=LM.getText(LC.HM_SPECIALITY, loginDTO)%>
            </th>
            
            <th class=""><%=LM.getText(LC.HM_DEPARTMENT, loginDTO)%>
            </th>
           
            <th class=""><%=LM.getText(LC.HM_TIME_PER_SLOT, loginDTO)%>
            </th>
            <th class=""><%=LM.getText(LC.HM_QUALIFICATIONS, loginDTO)%>
            </th>
            <th class=""><%=LM.getText(LC.HM_IS_AVAILABLE, loginDTO)%>
            </th>
            <th class=""><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th class=""><%=LM.getText(LC.DOCTOR_TIME_SLOT_SEARCH_DOCTOR_TIME_SLOT_EDIT_BUTTON, loginDTO)%>
            </th>
         

        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_DOCTOR_TIME_SLOT);

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Doctor_time_slotDTO doctor_time_slotDTO = (Doctor_time_slotDTO) data.get(i);


        %>
        <%

            UserDTO drDTO = UserRepository.getUserDTOByOrganogramID(doctor_time_slotDTO.doctorId);
        	if(drDTO == null)
        	{
        		continue;
        	}
            
            %>
        <tr id='tr_<%=i%>'>
            


            <%
                request.setAttribute("doctor_time_slotDTO", doctor_time_slotDTO);
            %>

            <jsp:include page="./doctor_time_slotSearchRow.jsp">
                <jsp:param name="pageName" value="searchrow"/>
                <jsp:param name="rownum" value="<%=i%>"/>
            </jsp:include>


            <%

            %>
        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			