<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="doctor_time_slot.*" %>
<%@ page import="util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="appointment.*" %>
<%@page import="shift_slot.*"%>

<%
    String servletName = "Doctor_time_slotServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.DOCTOR_TIME_SLOT_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Doctor_time_slotDAO doctor_time_slotDAO = new Doctor_time_slotDAO("doctor_time_slot");
    Doctor_time_slotDTO doctor_time_slotDTO = doctor_time_slotDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    AppointmentDAO appointmentDAO = new AppointmentDAO();
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.DOCTOR_TIME_SLOT_ADD_DOCTOR_TIME_SLOT_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <h5 class="table-title">
                <%=LM.getText(LC.DOCTOR_TIME_SLOT_ADD_DOCTOR_TIME_SLOT_ADD_FORMNAME, loginDTO)%>
            </h5>
            <div class="table-responsive">
                <table class="table table-bordered table-striped text-nowrap">
                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.HM_DOCTOR, loginDTO)%>
                        </b></td>
                        <td>

                            <%
						        value = WorkflowController.getNameFromOrganogramId(doctor_time_slotDTO.doctorId, Language);
						    %>
						
						    <%=value%>, 
							<%
						        value = doctor_time_slotDTO.deptCat + "";
						    %>
						    <%
						        value = CatDAO.getName(Language, "department", doctor_time_slotDTO.deptCat);
						    %>

    						<%=value%>


                        </td>

                    </tr>

			

                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.HM_IS_AVAILABLE, loginDTO)%>
                        </b></td>
                        <td>

                            <%=Utils.getYesNo(doctor_time_slotDTO.isAvailable, Language)%>


                        </td>

                    </tr>
                    
                    <%
                    if(!doctor_time_slotDTO.isAvailable)
                    {
                    %>

                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.HM_UNAVAILABILITY_REASON, loginDTO)%>
                        </b></td>
                        <td>

                            <%=doctor_time_slotDTO.unavailabilityReason != null? doctor_time_slotDTO.unavailabilityReason: ""%>


                        </td>

                    </tr>
                    <%
                    }
                    %>


                </table>
            </div>
            
             
           <%
			if(appointmentDAO.isDoctor(doctor_time_slotDTO.doctorId))
			{
			%>
              <div class="form-group row">
                <div class="table-responsive">
				    <table id="tableData" class="table table-bordered table-striped text-nowrap">
				        <thead>
				        	<tr>
				        		<th></th>
					        	<%
					        	List<Shift_slotDTO> shift_slotDTOs = Shift_slotRepository.getInstance().getShift_slotList();
					        	for(Shift_slotDTO shift_slotDTO: shift_slotDTOs)
								{
					        		%>
					        		<th><%=Utils.getDigits(TimeFormat.getInAmPmFormat(shift_slotDTO.startTime), Language) + " - " 
				                    		+ Utils.getDigits(TimeFormat.getInAmPmFormat(shift_slotDTO.endTime), Language)%></th>
					        		<%
								}
					        	
					        	%>
				        	</tr>
				        </thead>
				        <tbody>
				        	<%
				        	for(int day = 0; day < 7; day ++)
				        	{
				        		%>
				        		<tr>
				        			<td><%=CatDAO.getName(Language, "day", day)%></td>
				        			<%
				        			for(Shift_slotDTO shift_slotDTO: shift_slotDTOs)
									{
				        				Doctor_time_slotDTO dtDTO = doctor_time_slotDAO.getSlotDTOByDoctorShiftDay(doctor_time_slotDTO.doctorId, shift_slotDTO.shiftCat, day);
						        		%>
						        		<td>
						        		<input type='checkbox' onclick="return false;" class='form-control-sm' name='<%=day%>_<%=shift_slotDTO.shiftCat%>'  value='true' <%=dtDTO!=null?"checked":""%> >
						        		<%
						        		if(dtDTO!=null && dtDTO.weekList.get(0) != 0)
						        		{
						        			%>
						        			<br><%=Language.equalsIgnoreCase("english")?"Weeks: ":"সপ্তাহসমূহ " %>

						        			<%
						        			int j = 0;
						        			for(int week: dtDTO.weekList)
						        			{
						        				if(j != 0)
						        				{
						        					%>
						        					, 
						        					<%
						        				}
						        				%>
						        				<%=CatDAO.getName(Language, "week", week)%>
						        				<%
						        				j++;
						        			}
						        			
						        		}
						        		%>
						        		</td>
						        		<%
									}
				        			 %>
				        		</tr>
				        		<%
				        	}
				        	%>
		                    
		                </tbody>
                	</table>
                </div>
             </div>
               <%
			}
               %>
        </div>
    </div>
</div>