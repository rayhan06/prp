<%@page import="appointment.AppointmentDAO"%>
<%@page pageEncoding="UTF-8" %>

<%@page import="doctor_time_slot.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.DOCTOR_TIME_SLOT_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_DOCTOR_TIME_SLOT;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Doctor_time_slotDTO doctor_time_slotDTO = (Doctor_time_slotDTO) request.getAttribute("doctor_time_slotDTO");
    CommonDTO commonDTO = doctor_time_slotDTO;
    String servletName = "Doctor_time_slotServlet";


    System.out.println("doctor_time_slotDTO = " + doctor_time_slotDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Doctor_time_slotDAO doctor_time_slotDAO = (Doctor_time_slotDAO) request.getAttribute("doctor_time_slotDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    
    AppointmentDAO appointmentDAO = new AppointmentDAO();

%>


<td>
    <%
        value = WorkflowController.getNameFromOrganogramId(doctor_time_slotDTO.doctorId, Language);
    %>
    <b><%=value%></b>
</td>
<td>

    <%
        value = CatDAO.getName(Language, "department", doctor_time_slotDTO.deptCat);
    %>
    <%=value%>
</td>
<td>
    
    <%=CatDAO.getName(Language, "medical_dept", doctor_time_slotDTO.medicalDeptCat)%>

</td>



<td id='<%=i%>_durationPerPatient' class="text-nowrap">
<%
if(appointmentDAO.isDoctor(doctor_time_slotDTO.doctorId))
{
%>
    <%
        value = doctor_time_slotDTO.durationPerPatient + "";
    %>

    <%=Utils.getDigits(value, Language)%>
<%
}%>

</td>

<td class="text-nowrap">

    <%
        value = doctor_time_slotDTO.qualifications + "";
    %>

    <%=value%>


</td>

<td>

    <%=Utils.getYesNo(doctor_time_slotDTO.isAvailable, Language)%>


</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Doctor_time_slotServlet?actionType=view&ID=<%=doctor_time_slotDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Doctor_time_slotServlet?actionType=addSlots&doctorId=<%=doctor_time_slotDTO.doctorId%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

									

