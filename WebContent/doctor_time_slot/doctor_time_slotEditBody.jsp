<%@page import="shift_slot.*"%>
<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="doctor_time_slot.*" %>
<%@page import="java.util.*" %>
<%@page import="appointment.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="util.TimeFormat" %>

<%@ page import="pb.*" %>

<%
    Doctor_time_slotDTO doctor_time_slotDTO;
    doctor_time_slotDTO = (Doctor_time_slotDTO) request.getAttribute("doctor_time_slotDTO");
    Doctor_time_slotDAO doctor_time_slotDAO = new Doctor_time_slotDAO();
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (doctor_time_slotDTO == null) {
        doctor_time_slotDTO = new Doctor_time_slotDTO();

    }
    System.out.println("doctor_time_slotDTO = " + doctor_time_slotDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.DOCTOR_TIME_SLOT_ADD_DOCTOR_TIME_SLOT_ADD_FORMNAME, loginDTO);
    String servletName = "Doctor_time_slotServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.DOCTOR_TIME_SLOT_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    AppointmentDAO appointmentDAO = new AppointmentDAO();
    List<CatDTO> weekCatDTOs = CatRepository.getDTOs("week");
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Doctor_time_slotServlet?actionType=massEdit&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=doctor_time_slotDTO.iD%>' tag='pb_html'/>
                                           
                                     <input type='hidden' class='form-control' name='doctorId' id='doctorId'
                                           value='<%=doctor_time_slotDTO.doctorId%>' tag='pb_html'/>
                                           
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                        </label>
                                        <div class="col-md-8">
                                           
                                            <b><%=WorkflowController.getNameFromOrganogramId(doctor_time_slotDTO.doctorId, Language)%></b>
                                            

                                        </div>
                                    </div>
                                           
                                      <div class="form-group row" style = "display:none">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.HM_SHIFT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='shiftCat' onchange="redirectIfAvailable()"
                                                    id='shiftCat_category_<%=i%>' tag='pb_html'>
                                                <%
                                                    Options = CatDAO.getOptions(Language, "shift", doctor_time_slotDTO.shiftCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.HM_SPECIALITY, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='deptCat'
                                                    id='deptCat_category_<%=i%>' tag='pb_html'>
                                                <%
                                                    Options = CatDAO.getOptions(Language, "department", doctor_time_slotDTO.deptCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.HM_DEPARTMENT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='medicalDeptCat'
                                                    id='medicalDeptCat_category_<%=i%>' tag='pb_html'>
                                                <%
                                                	Options = CatRepository.getOptions( Language, "medical_dept", doctor_time_slotDTO.medicalDeptCat, CatRepository.SORT_BY_NAMEEN);
                                                %>
                                                <option value = "-1" <%=doctor_time_slotDTO.medicalDeptCat == -1?"Selected":""%> >
                                                <%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    
                                  
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.HM_TIME_PER_SLOT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='durationPerPatient'
                                                    id='durationPerPatient_<%=i%>' tag='pb_html'>
                                                <% for (int min = 5; min <= 60; min += 5) {%>
                                                <option <%=((actionName.equals("edit") && doctor_time_slotDTO.durationPerPatient == min) ? "selected" : "")%>
                                                        value="<%=min%>"><%=min%>
                                                </option>
                                                <%}%>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.HM_IS_AVAILABLE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='checkbox' class='form-control-sm' name='isAvailable' id = 'isAvailable' value='true' <%=doctor_time_slotDTO.isAvailable?"checked":"" %>>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.HM_UNAVAILABILITY_REASON, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <textarea class='form-control' name='unavailabilityReason' id = 'unavailabilityReason'><%=doctor_time_slotDTO.unavailabilityReason != null? doctor_time_slotDTO.unavailabilityReason:""%></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.HM_QUALIFICATIONS, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <textarea class='form-control' name='qualifications' id = 'qualifications'><%=doctor_time_slotDTO.qualifications != null? doctor_time_slotDTO.qualifications:""%></textarea>
                                        </div>
                                    </div>
                                    
                                    <%
                                    System.out.println("appointmentDAO.isDoctor(doctor_time_slotDTO.doctorId) = " + appointmentDAO.isDoctor(doctor_time_slotDTO.doctorId));
									if(appointmentDAO.isDoctor(doctor_time_slotDTO.doctorId))
									{
									%>
                                    <div class="form-group row">
						                <div class="table-responsive">
										    <table id="tableData" class="table table-bordered table-striped text-nowrap">
										        <thead>
										        	<tr>
										        		<th></th>
											        	<%
											        	List<Shift_slotDTO> shift_slotDTOs = Shift_slotRepository.getInstance().getShift_slotList();
											        	for(Shift_slotDTO shift_slotDTO: shift_slotDTOs)
														{
											        		%>
											        		<th><%=Utils.getDigits(TimeFormat.getInAmPmFormat(shift_slotDTO.startTime), Language) + " - " 
				                    							+ Utils.getDigits(TimeFormat.getInAmPmFormat(shift_slotDTO.endTime), Language)%></th>
											        		<%
														}
											        	
											        	%>
										        	</tr>
										        </thead>
										        <tbody>
										        	<%
										        	for(int day = 0; day < 7; day ++)
										        	{
										        		%>
										        		<tr>
										        			<td><%=CatDAO.getName(Language, "day", day)%></td>
										        			<%
										        			for(Shift_slotDTO shift_slotDTO: shift_slotDTOs)
															{
										        				Doctor_time_slotDTO dtDTO = doctor_time_slotDAO.getSlotDTOByDoctorShiftDay(doctor_time_slotDTO.doctorId, shift_slotDTO.shiftCat, day);
												        		%>
												        		<td>
												        			<div class = "row">
												        				<div class = "col-2">
													        				<input type='checkbox' class='form-control-sm' name='<%=day%>_<%=shift_slotDTO.shiftCat%>'  value='true' <%=dtDTO!=null?"checked":""%> >
													        			</div>
													        			<div class = "col-10">
															        		<select class='form-control weekSelect' name = 'weekCats_<%=day%>_<%=shift_slotDTO.shiftCat%>' multiple='multiple'>
															        		<%
															        		if(dtDTO == null)
															        		{
															        			dtDTO = new Doctor_time_slotDTO();
															        		}
															        		for(CatDTO weekCatDTO: weekCatDTOs)
															        		{
															        			
															        			%>
															        			<option value = "<%=weekCatDTO.value%>" <%=dtDTO.weekList.contains(weekCatDTO.value)?"selected":""%>>
															        				<%=Language.equalsIgnoreCase("english")?weekCatDTO.nameEn:weekCatDTO.nameBn%>
															        			</option>
															        			<%
															        		}
															        		%>
															        		</select>
														        		</div>
													        		</div>
												        		</td>
												        		<%
															}
										        			 %>
										        		</tr>
										        		<%
										        	}
										        	%>
								                    
								                </tbody>
						                	</table>
						                </div>
					                </div>
					                <%
									}
					                %>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-actions text-right mt-3">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.DOCTOR_TIME_SLOT_ADD_DOCTOR_TIME_SLOT_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.DOCTOR_TIME_SLOT_ADD_DOCTOR_TIME_SLOT_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {



        return true;
    }
    
    function disableSlotIfNecesary()
    {
    	var drElemId  = "doctorId";
    	var role = $("#" + drElemId + " option:selected").attr("role");
			
		if(role == <%=SessionConstants.DOCTOR_ROLE%>)
       	{
       		$("#slotDiv").css("display", "block");
       	
       	}
        else
        {
       		$("#slotDiv").css("display", "none");	
        }
    }
    

    function init(row) {


        disableSlotIfNecesary();
        $(".weekSelect").select2({
            dropdownAutoWidth: true,
            theme: "classic"
        });
    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        //CKEDITOR.replaceAll();

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






