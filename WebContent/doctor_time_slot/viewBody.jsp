<%@page import="user.UserRepository"%>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="doctor_time_slot.*" %>
<%@ page import="util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>

<%@page import="appointment.*" %>
<%@page import="shift_slot.*"%>

<%
    String servletName = "Doctor_time_slotServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
  
    String value = "";
    String Language = LM.getText(LC.DOCTOR_TIME_SLOT_EDIT_LANGUAGE, loginDTO);


    Doctor_time_slotDAO doctor_time_slotDAO = new Doctor_time_slotDAO("doctor_time_slot");
    List<Doctor_time_slotDTO> doctor_time_slotDTOs = Doctor_time_slotRepository.getInstance().getDoctor_time_slotList();

    String Value = "";
    int i = 0;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    AppointmentDAO appointmentDAO = new AppointmentDAO();
%>
<div class="ml-auto mr-5 mt-4">
    
    <button type="button" class="btn" id='printer2'
            onclick="printAnyDiv('modalbody')">
        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
  
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet" >
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.DOCTOR_TIME_SLOT_ADD_DOCTOR_TIME_SLOT_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
           

            
           
              <div class="form-group row">
                <div class="table-responsive" id = 'modalbody'>
                	 <h5 class="table-title">
		                <%=LM.getText(LC.DOCTOR_TIME_SLOT_ADD_DOCTOR_TIME_SLOT_ADD_FORMNAME, loginDTO)%>
		            </h5>
				    <table id="tableData" class="table table-bordered table-striped text-nowrap" >
				        <thead>
				        	<tr>
				        		<th></th>
					        	<%
					        	List<Shift_slotDTO> shift_slotDTOs = Shift_slotRepository.getInstance().getShift_slotList();
					        	for(Shift_slotDTO shift_slotDTO: shift_slotDTOs)
								{
					        		%>
					        		<th><%=Utils.getDigits(TimeFormat.getInAmPmFormat(shift_slotDTO.startTime), Language) + " - " 
				                    		+ Utils.getDigits(TimeFormat.getInAmPmFormat(shift_slotDTO.endTime), Language)%></th>
					        		<%
								}
					        	
					        	%>
				        	</tr>
				        </thead>
				        <tbody>
				        	<%
				        	for(int day = 0; day < 7; day ++)
				        	{
				        		%>
				        		<tr>
				        			<td><%=CatDAO.getName(Language, "day", day)%></td>
				        			<%
				        			for(Shift_slotDTO shift_slotDTO: shift_slotDTOs)
									{
				        										        		%>
						        		<td>
						        		<%
						        		for(Doctor_time_slotDTO doctor_time_slotDTO: doctor_time_slotDTOs)
						        		{
						        			if(doctor_time_slotDTO.dayCat == day && doctor_time_slotDTO.shiftCat ==  shift_slotDTO.shiftCat)
						        			{
						        				UserDTO drDTO = UserRepository.getUserDTOByOrganogramID(doctor_time_slotDTO.doctorId);
						        				if(drDTO == null)
						        				{
						        					continue;
						        				}
						        				%>
						        				<%=WorkflowController.getNameFromOrganogramId(doctor_time_slotDTO.doctorId, Language) %>
						        				<%
						        				if(doctor_time_slotDTO.weekList.get(0) != null && doctor_time_slotDTO.weekList.get(0) != 0)
						        				{
						        					%>
						        					 , <b><%=Language.equalsIgnoreCase("english")?"Weeks: ":"সপ্তাহসমূহ " %>
						        					 </b>
						        					 <%
						        					 	int j = 0;
									        			for(int week: doctor_time_slotDTO.weekList)
									        			{
									        				if(j != 0)
									        				{
									        					%>
									        					, 
									        					<%
									        				}
									        				%>
									        				<%=CatDAO.getName(Language, "week", week)%>
									        				<%
									        				j++;
									        			}
						        					 %>
						        					 
						        					<%
						        				}
						        				%>
						        				<br>
						        				<%
						        			}
						        		}
						        		%>
						        		
						        		</td>
						        		<%
									}
				        			 %>
				        		</tr>
				        		<%
				        	}
				        	%>
		                    
		                </tbody>
                	</table>
                </div>
             </div>
             
        </div>
    </div>
</div>