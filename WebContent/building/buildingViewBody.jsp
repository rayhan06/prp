

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="building.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "BuildingServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
BuildingDTO buildingDTO = BuildingDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = buildingDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.BUILDING_ADD_BUILDING_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.BUILDING_ADD_BUILDING_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.BUILDING_ADD_NAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=buildingDTO.nameEn%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.BUILDING_ADD_NAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=buildingDTO.nameBn%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.BUILDING_ADD_MINIMUMLEVEL, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(buildingDTO.minimumLevel, Language)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.BUILDING_ADD_MAXIMUMLEVEL, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(buildingDTO.maximumLevel, Language)%>
                                    </div>
                                </div>
			
			
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%=LM.getText(LC.BUILDING_ADD_BUILDING_BLOCK, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.BUILDING_ADD_BUILDING_BLOCK_NAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.BUILDING_ADD_BUILDING_BLOCK_NAMEBN, loginDTO)%></th>
								<th><%=isLanguageEnglish?"Order":"ক্রম"%></th>
							</tr>
							<%
                        	BuildingBlockDAO buildingBlockDAO = BuildingBlockDAO.getInstance();
                         	List<BuildingBlockDTO> buildingBlockDTOs = (List<BuildingBlockDTO>)buildingBlockDAO.getDTOsByParent("building_id", buildingDTO.iD);
                         	
                         	for(BuildingBlockDTO buildingBlockDTO: buildingBlockDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%=buildingBlockDTO.nameEn%>
										</td>
										<td>
											<%=buildingBlockDTO.nameBn%>
										</td>
										<td>
											<%=buildingBlockDTO.ordering%>
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
        </div>
    </div>
</div>