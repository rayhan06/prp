<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="fund_management.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>
<%@ page import="util.StringUtils" %>
<%@ page import="static util.StringUtils.convertBanglaIfLanguageIsBangla" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="fund_management_question.FundManagementQuestionRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="static util.StringUtils.convertBanglaIfLanguageIsBangla" %>
<%@ page import="static util.UtilCharacter.*" %>
<%@ page import="card_approval_mapping.Card_approval_mappingDTO" %>
<%@ page import="card_approval_mapping.Card_approval_mappingDAO" %>
<%@ page import="fund_approval_mapping.Fund_approval_mappingDTO" %>
<%@ page import="fund_approval_mapping.Fund_approval_mappingDAO" %>
<%@ page import="card_info.CardApprovalDTO" %>
<%@ page import="card_info.CardApprovalRepository" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>


<%
    Fund_managementDTO fund_managementDTO = (Fund_managementDTO) request.getAttribute("fund_managementDTO");
    Fund_approval_mappingDTO approverFundManagementDTO = (Fund_approval_mappingDTO) request.getAttribute("approverFundManagementDTO");

    Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(fund_managementDTO.employeeRecordsId);
    String nid = employeeRecordsDTO == null ? "" : employeeRecordsDTO.nid;
%>

<%@include file="../pb/viewInitializer.jsp" %>

<%
    String fundType =
            "<b>".concat(CatRepository.getName(Language, "fund_type", fund_managementDTO.fundTypeCat)).concat("</b>");

    String bodyTextBn =
            "আমার সাধারণ %s তহবিলের সিজিএস/সিএও/প্রশাসন/জাসস <b>%s</b> নং হিসাব গচ্ছিত অংক থেকে আমি <b>%s</b> টাকা অগ্রিম গ্রহণের জন্য আবেদন করছি।"
                    .concat(" আমি নিম্নবর্ণিত তথসমূহের সঠিক ও যথাযথ জবাব প্রদান করছি।");

    bodyTextBn = String.format(
            bodyTextBn,
            fundType,
            fund_managementDTO.cgsNumber,
            StringUtils.convertToBanNumber(String.valueOf(fund_managementDTO.advanceAmount))
    );

    String bodyTextEn =
            "I am applying to take advance of <b>%d</b> Taka from my General %s Fund deposited under CSS/SAO/administration/JSS No. "
                    .concat("<b>%s</b>. I am giving the correct and appropriate answer to the following information.");
    bodyTextEn = String.format(
            bodyTextEn,
            fund_managementDTO.advanceAmount,
            fundType,
            fund_managementDTO.cgsNumber
    );
    List<String> fundManagementQuestions =
            FundManagementQuestionRepository.getInstance()
                                            .getNameDTOList()
                                            .stream()
                                            .map(nameDTO -> nameDTO.getText(Language))
                                            .collect(Collectors.toList());
    int quesNumber = 0;


    List<Fund_approval_mappingDTO> fundApprovalMappingDTOs =
            Fund_approval_mappingDAO.getInstance().getAllApprovalDTOByFundIdNotPending(fund_managementDTO.iD);


    List<Long> cardApprovalIds = fundApprovalMappingDTOs.stream()
                                                        .map(e->e.cardApprovalId)
                                                        .collect(Collectors.toList());
    Map<Long,CardApprovalDTO> mapByCardApprovalDTOId = CardApprovalRepository.getInstance().getByIds(cardApprovalIds)
                                                                             .stream()
                                                                             .collect(Collectors.toMap(e->e.iD,e->e,(e1,e2)->e1));
%>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    @media print {
        .page-break {
            page-break-after: always;
        }
    }

    .view-top-section {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        margin: 2.5rem 0;
    }

    input[readonly],
    textarea[readonly] {
        background-color: rgba(231, 231, 231, .5) !important;
        font-weight: bold;
        font-size: 1.1rem;
    }

    .view-application-text {
        margin-bottom: 3rem;
        font-size: 1.2rem;
    }
</style>

<div class="kt-content" id="kt_content">
    <div class="row">
        <div class="kt-portlet">

            <div class="ml-auto m-4">
                <button type="button" class="btn" id='printer' onclick="printDiv('fund-application')">
                    <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
                </button>
            </div>

            <div class="kt-portlet__body page-bg" id="fund-application">
                <div class="view-top-section">
                    <div>
                        <%=LM.getText(LC.HM_DATE, loginDTO)%>
                        :
                        <%=convertBanglaIfLanguageIsBangla(
                                Language,
                                new SimpleDateFormat("dd/MM/yyyy").format(new Date()))%>
                    </div>
                    <div>
                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NID, loginDTO)%>
                        :
                        <%=convertBanglaIfLanguageIsBangla(Language, nid)%>
                    </div>
                </div>

                <%--Application Body--%>
                <div class="mb-4">
                    <div class="text-center mb-4">
                        <h4>
                            <%=String.format(LM.getText(LC.FUND_MANAGEMENT_VIEW_HEADING, loginDTO), fundType)%>
                        </h4>
                    </div>

                    <div class="view-application-text">
                        <%=getDataByLanguage(Language, bodyTextBn, bodyTextEn)%>
                    </div>

                    <div class="container view-application-text">
                        <div class="col-6 offset-6">
                            <div class="row">
                                <div class="col-6 text-right">
                                    <%=LM.getText(LC.CARD_INFO_ADD_APPLICANT_NAME, loginDTO)%>
                                </div>
                                <div class="col-6 text-right">
                                    <%=getDataByLanguage(Language, fund_managementDTO.employeeNameBn, fund_managementDTO.employeeNameEn)%>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6 text-right">
                                    <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_POST, loginDTO)%>
                                </div>
                                <div class="col-6 text-right">
                                    <%=getDataByLanguage(Language, fund_managementDTO.organogramNameBn, fund_managementDTO.organogramNameEn)%>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6 text-right">
                                    <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_OFFICE, loginDTO)%>
                                </div>
                                <div class="col-6 text-right">
                                    <%=getDataByLanguage(Language, fund_managementDTO.officeNameBn, fund_managementDTO.officeNameEn)%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="row mt-3 mx-0">
                        <label class="col-4 col-form-label text-right">
                            <%=fundManagementQuestions.get(quesNumber++)%>
                        </label>
                        <div class="col-8">
                            <%
                                value = convertBanglaIfLanguageIsBangla(
                                        Language,
                                        String.valueOf(fund_managementDTO.q1Response)
                                );
                            %>
                            <input type='text' class='form-control' readonly
                                   value='<%=value%>'>
                        </div>
                    </div>

                    <div class="row mt-3 mx-0">
                        <label class="col-4 col-form-label text-right">
                            <%=fundManagementQuestions.get(quesNumber++)%>
                        </label>
                        <div class="col-8">
                            <textarea class='form-control' readonly><%=fund_managementDTO.q2Response%></textarea>
                        </div>
                    </div>

                    <div class="row mt-3 mx-0">
                        <label class="col-4 col-form-label text-right">
                            <%=fundManagementQuestions.get(quesNumber++)%>
                        </label>
                        <div class="col-8">
                            <%
                                value = convertBanglaIfLanguageIsBangla(
                                        Language,
                                        String.valueOf(fund_managementDTO.q3Response)
                                );
                            %>
                            <input type='text' class='form-control' readonly
                                   value='<%=value%>'>
                        </div>
                    </div>

                    <div class="row mt-3 mx-0">
                        <label class="col-4 col-form-label text-right" for="q4Response">
                            <%=fundManagementQuestions.get(quesNumber++)%>
                        </label>
                        <div class="col-8">
                            <input type='text' class='form-control' id="q4Response" readonly
                                   value='<%=fund_managementDTO.q4Response%>'>
                        </div>
                    </div>

                    <div class="row mt-3 mx-0">
                        <label class="col-4 col-form-label text-right" for="q5Response">
                            <%=fundManagementQuestions.get(quesNumber++)%>
                        </label>
                        <div class="col-8">
                            <input type='text' class='form-control' id="q5Response" readonly
                                   value='<%=fund_managementDTO.q5Response%>'>
                        </div>
                    </div>

                    <div class="row mt-3 mx-0">
                        <label class="col-4 col-form-label text-right">
                            <%=fundManagementQuestions.get(quesNumber++)%>
                        </label>
                        <div class="col-8 row">
                            <div class="col-12 col-md-6">
                                <%
                                    value = convertBanglaIfLanguageIsBangla(
                                            Language,
                                            String.valueOf(fund_managementDTO.q6aResponse)
                                    );
                                %>
                                <input type='text' class='form-control'
                                       value='<%=value%> <%=getDataByLanguage(Language, "  টাকা মাসে", "  Taka monthly")%>'
                                       readonly>
                            </div>
                            <div class="col-12 col-md-6">
                                <%
                                    value = convertBanglaIfLanguageIsBangla(
                                            Language,
                                            String.valueOf(fund_managementDTO.q6bResponse)
                                    );
                                %>
                                <input type='text' class='form-control'
                                       value='<%=value%> <%=getDataByLanguage(Language, "  কিস্তি", "  installments")%>'
                                       readonly>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3 mx-0">
                        <label class="col-4 col-form-label text-right">
                            <%=fundManagementQuestions.get(quesNumber++)%>
                        </label>
                        <div class="col-8 row">
                            <div class="col-12 col-md-6">
                                <%
                                    value = convertBanglaIfLanguageIsBangla(
                                            Language,
                                            String.valueOf(fund_managementDTO.q7aResponse)
                                    );
                                %>
                                <input type='text' class='form-control'
                                       value='<%=value%> <%=getDataByLanguage(Language, "  টাকা মাসে", "  Taka monthly")%>'
                                       readonly>
                            </div>
                            <div class="col-12 col-md-6">
                                <%
                                    value = convertBanglaIfLanguageIsBangla(
                                            Language,
                                            String.valueOf(fund_managementDTO.q7bResponse)
                                    );
                                %>
                                <input type='text' class='form-control'
                                       value='<%=value%> <%=getDataByLanguage(Language, " কিস্তি", "  installments")%>'
                                       readonly>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3 mx-0">
                        <label class="col-4 col-form-label text-right" for="q8Response">
                            <%=fundManagementQuestions.get(quesNumber++)%>
                        </label>
                        <div class="col-8">
                            <input type='text' class='form-control' id="q8Response" readonly
                                   value='<%=fund_managementDTO.q8Response%>'>
                        </div>
                    </div>

                    <div class="row mt-3 mx-0">
                        <label class="col-4 col-form-label text-right" for="q9Response">
                            <%=fundManagementQuestions.get(quesNumber++)%>
                        </label>
                        <div class="col-8">
                            <%
                                value = convertBanglaIfLanguageIsBangla(
                                        Language,
                                        String.valueOf(fund_managementDTO.q9Response)
                                );
                            %>
                            <input type='text' class='form-control' id="q9Response" readonly
                                   value='<%=value%>'>
                        </div>
                    </div>
                </div>

                <%if (approverFundManagementDTO.fundApplicationStatusCat == FundApplicationStatus.WAITING_FOR_APPROVAL.getValue()) {%>
                    <div class="row my-5" align="right">
                        <div class="col-12">
                            <button type="button" class="btn btn-danger shadow ml-2" style="border-radius: 8px;" id="reject_btn"
                                    onclick="rejectApplication()">
                                <%=LM.getText(LC.CARD_APPROVAL_MAPPING_DO_NOT_APPOVE, loginDTO)%>
                            </button>

                            <button type="button" class="btn btn-success shadow ml-2" style="border-radius: 8px;" id="approve_btn"
                                    onclick="approveApplication()"><%=LM.getText(LC.CARD_APPROVAL_MAPPING_APPROVAL, loginDTO)%>
                            </button>
                        </div>
                    </div>
                <%}%>

                <div class="container mt-5">
                    <h3><%=LM.getText(LC.CARD_INFO_REQUESTER_INFORMATION, loginDTO)%>
                    </h3>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th><%=LM.getText(LC.CARD_INFO_ADD_APPLICANT_NAME, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.CARD_INFO_DESIGNATION_OFFICE, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.CARD_INFO_REQUEST_CREATE_DATE_TIME, loginDTO)%>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <%=getDataByLanguage(Language, fund_managementDTO.employeeNameBn, fund_managementDTO.employeeNameEn)%>
                            </td>
                            <td>
                                <b>
                                    <%=getDataByLanguage(Language, fund_managementDTO.organogramNameBn, fund_managementDTO.organogramNameEn)%>
                                </b>
                                <br>
                                <%=getDataByLanguage(Language, fund_managementDTO.officeNameBn, fund_managementDTO.officeNameEn)%>
                            </td>
                            <td>
                                <%=StringUtils.convertToDateAndTime(isLanguageEnglish, fund_managementDTO.insertionDate)%>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="container">
                    <h3><%=LM.getText(LC.CARD_INFO_APPROVAL_INFORMATION, loginDTO)%>
                    </h3>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th><%=LM.getText(LC.CARD_INFO_APPROVER_OFFICE_INFORMATION, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.CARD_INFO_APPROVAL_STATUS, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.CARD_INFO_APPROVAL_DATE_TIME, loginDTO)%>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <%
                            for (Fund_approval_mappingDTO approvalMappingDTO : fundApprovalMappingDTOs) {
                                CardApprovalDTO cardApprovalDTO = mapByCardApprovalDTOId.get(approvalMappingDTO.cardApprovalId);
                        %>
                        <tr>
                            <td>
                                <b>
                                    <%=isLanguageEnglish ? cardApprovalDTO.officeUnitEng : cardApprovalDTO.officeUnitBng%>
                                </b>
                            </td>
                            <td>
                                <span class="btn btn-sm border-0 shadow"
                                      style="background-color: <%=FundApplicationStatus.getColor(approvalMappingDTO.fundApplicationStatusCat)%>; color: white; border-radius: 8px;cursor: text">
                                    <%=CatRepository.getInstance().getText(Language, "fund_application_status", approvalMappingDTO.fundApplicationStatusCat)%>
                                </span>
                                <%if(approvalMappingDTO.fundApplicationStatusCat == FundApplicationStatus.REJECTED.getValue()){%>
                                    <div class="container mt-3">
                                        <b><%=LM.getText(LC.BUDGET_COMMENT, loginDTO)%></b>
                                        <br>
                                        <%=approvalMappingDTO.comment%>
                                    </div>
                                <%}%>
                            </td>
                            <td>
                                <%if(approvalMappingDTO.fundApplicationStatusCat != FundApplicationStatus.WAITING_FOR_APPROVAL.getValue()){%>
                                    <%=StringUtils.convertToDateAndTime(isLanguageEnglish, approvalMappingDTO.lastModificationTime)%>
                                <%}%>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display: none">
    <form action="Fund_approval_mappingServlet?actionType=rejectApplication"
          id="rejectForm" name="rejectForm" method="POST"
          enctype="multipart/form-data">
        <input type="hidden" name="fundManagementId" value="<%=fund_managementDTO.iD%>">
        <input type="hidden" name="reject_reason" id="reject_reason">
    </form>
</div>

<div style="display: none">
    <form action="Fund_approval_mappingServlet?actionType=approveApplication"
          id="approveForm" name="approveForm" method="POST"
          enctype="multipart/form-data">
        <input type="hidden" name="fundManagementId" value="<%=fund_managementDTO.iD%>">
    </form>
</div>

<script>
    function rejectApplication(){
        let msg = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_REASON_FOR_DISSATISFACTION, loginDTO)%>';
        let placeHolder = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_TYPE_DISSATISFACTION_REASON_HERE, loginDTO)%>';
        let confirmButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_YES_DISSATISFIED, loginDTO)%>';
        let cancelButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARD_APPROVAL_MAPPING_CANCEL_BUTTON, loginDTO)%>';
        let emptyReasonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_PLEASE_WRITE_DISSATISFACTION_REASON, loginDTO)%>';
        dialogMessageWithTextBox(msg,placeHolder,confirmButtonText,cancelButtonText,emptyReasonText,(reason)=>{
            $('#reject_btn').prop("disabled",true);
            $('#reject_reason').val(reason);
            $('#rejectForm').submit();
        },()=>{});
    }

    function approveApplication(){
        let msg = '<%=getDataByLanguage(Language, "আপনি কি আবেদন অনমোদনের ব্যাপারে নিশ্চিত?", "Are you sure to approve application?")%>';
        let confirmButtonText = '<%=StringUtils.getYesNo(Language, true)%>';
        let cancelButtonText = '<%=StringUtils.getYesNo(Language, false)%>';
        messageDialog('',msg,'success',true,confirmButtonText,cancelButtonText,()=>{
            $('#approve_btn').prop("disabled",true);
            $('#reject_btn').prop("disabled",true);
            $('#approveForm').submit();
        },()=>{});
    }

    function printDiv(divId) {
        let printContents = document.getElementById(divId).innerHTML;
        let originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>
