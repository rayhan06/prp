<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="fund_approval_mapping.*" %>
<%@ page import="util.*" %>
<%@ page language="java" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="fund_management.Fund_managementRepository" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="java.util.*" %>
<%@ page import="fund_management.Fund_managementDTO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="fund_management.FundApplicationStatus" %>

<%
    String navigator2 = "navFUND_APPROVAL_MAPPING";
    String servletName = "Fund_approval_mappingServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th>
                <%=LM.getText(LC.FUND_MANAGEMENT_ADD_EMPLOYEERECORDSID, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.FUND_MANAGEMENT_ADD_FUNDTYPECAT, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.FUND_MANAGEMENT_ADD_CGSNUMBER, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.FUND_MANAGEMENT_ADD_ADVANCEAMOUNT, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.FUND_APPROVAL_MAPPING_ADD_FUNDAPPLICATIONSTATUSCAT, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Fund_approval_mappingDTO> data = (List<Fund_approval_mappingDTO>) recordNavigator.list;
            try {
                if (data != null) {
                    Set<Long> fundManagementIds = data.stream()
                                                      .map(fundApprovalMappingDTO -> fundApprovalMappingDTO.fundManagementId)
                                                      .collect(Collectors.toSet());
                    Map<Long, Fund_managementDTO> fundManagementDTOsById = Fund_managementRepository.getInstance().getDTOsGroupedById(fundManagementIds);
                    for (Fund_approval_mappingDTO fund_approval_mappingDTO : data) {
                        Fund_managementDTO fund_managementDTO = fundManagementDTOsById.get(fund_approval_mappingDTO.fundManagementId);
        %>
        <tr>
            <td>
                <%=getDataByLanguage(Language, fund_managementDTO.employeeNameBn, fund_managementDTO.employeeNameEn)%>
                <br>
                <b>
                    <%=getDataByLanguage(Language, fund_managementDTO.organogramNameBn, fund_managementDTO.organogramNameEn)%>
                </b>
                <br>
                <%=getDataByLanguage(Language, fund_managementDTO.officeNameBn, fund_managementDTO.officeNameEn)%>
            </td>

            <td>
                <%=CatRepository.getInstance().getText(Language, "fund_type", fund_managementDTO.fundTypeCat)%>
            </td>

            <td>
                <%=Utils.getDigits(fund_managementDTO.cgsNumber, Language)%>
            </td>

            <td>
                <%=Utils.getDigits(fund_managementDTO.advanceAmount, Language)%>
            </td>

            <td>
                <span class="btn btn-sm border-0 shadow"
                      style="background-color: <%=FundApplicationStatus.getColor(fund_approval_mappingDTO.fundApplicationStatusCat)%>; color: white; border-radius: 8px;cursor: text">
                        <%=CatRepository.getInstance().getText(
                                Language, "fund_application_status", fund_approval_mappingDTO.fundApplicationStatusCat
                        )%>
                </span>
            </td>

            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=getApprovalPage&fundManagementId=<%=fund_approval_mappingDTO.fundManagementId%>'"
                >
                    <i class="fa fa-eye"></i>
                </button>
            </td>

        </tr>
        <%
                    }
                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>