<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="vaccine_entry.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
Vaccine_entryDTO vaccine_entryDTO = new Vaccine_entryDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	vaccine_entryDTO = Vaccine_entryDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = vaccine_entryDTO;
String tableName = "vaccine_entry";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.VACCINE_ENTRY_ADD_VACCINE_ENTRY_ADD_FORMNAME, loginDTO);
String servletName = "Vaccine_entryServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Vaccine_entryServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=vaccine_entryDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.VACCINE_ENTRY_ADD_VACCINECOUNT, loginDTO)%>															</label>
                                                            <div class="col-8">
																		
																<input type='number' class='form-control'  name='vaccineCount' id = 'vaccineCount_number_<%=i%>'
																 value='<%=vaccine_entryDTO.vaccineCount%>' min = "1" required tag='pb_html'>		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.VACCINE_ENTRY_ADD_DESCRIPTION, loginDTO)%>															</label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='description' id = 'description_text_<%=i%>' value='<%=vaccine_entryDTO.description%>'   tag='pb_html'/>					
															</div>
                                                      </div>
                                                      
                                                      <div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.HM_DATE, loginDTO)%>															</label>
                                                            <div class="col-8">
																<jsp:include page="/date/date.jsp">
                                                            <jsp:param name="DATE_ID"
                                                                       value="entryDate_date_js"></jsp:param>
                                                            <jsp:param name="LANGUAGE"
                                                                       value="<%=Language%>"></jsp:param>
                                                        </jsp:include>

                                                        <input type='hidden'
                                                               class='form-control formRequired datepicker'
                                                               readonly="readonly" data-label="Document Date"
                                                               id='entryDate_date_<%=i%>' name='entryDate' value=<%
																	String formatted_entryDate = dateFormat.format(new Date(vaccine_entryDTO.entryDate));
																%>
                                                                       '<%=formatted_entryDate%>'/>
															</div>
                                                      </div>									
													<input type='hidden' class='form-control'  name='insertedByEmployeeId' id = 'insertedByEmployeeId_hidden_<%=i%>' value='<%=vaccine_entryDTO.insertedByEmployeeId%>' tag='pb_html'/>
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.VACCINE_ENTRY_ADD_VACCINE_ENTRY_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.VACCINE_ENTRY_ADD_VACCINE_ENTRY_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);
	
	var entryDate = getDateStringById('entryDate_date_js', 'DD/MM/YYYY');
    $("#entryDate_date_<%=i%>").val(entryDate);

	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Vaccine_entryServlet");	
}

function init(row)
{

	setDateByTimestampAndId('entryDate_date_js', '<%=vaccine_entryDTO.entryDate%>');
	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	


</script>






