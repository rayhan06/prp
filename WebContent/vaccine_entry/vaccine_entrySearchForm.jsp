
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="vaccine_entry.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
String navigator2 = "navVACCINE_ENTRY";
String servletName = "Vaccine_entryServlet";

%>
<%@include file="../pb/searchInitializer.jsp"%>		
<%
boolean isLangEng = Language.equalsIgnoreCase("english");
%>		
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_DATE, loginDTO)%></th>
								<th><%=LM.getText(LC.VACCINE_ENTRY_ADD_VACCINECOUNT, loginDTO)%></th>
								<th><%=LM.getText(LC.VACCINE_ENTRY_ADD_DESCRIPTION, loginDTO)%></th>						
								<th><%=LM.getText(LC.VACCINE_ENTRY_SEARCH_VACCINE_ENTRY_EDIT_BUTTON, loginDTO)%></th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Vaccine_entryDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Vaccine_entryDTO vaccine_entryDTO = (Vaccine_entryDTO) data.get(i);
																																
											
											%>
											<tr>
											
											<td>
											<%=WorkflowController.getNameFromEmployeeRecordID(vaccine_entryDTO.insertedByEmployeeId, isLangEng)%>
											</td>
											
											<td>
											<%=Utils.getDigits(simpleDateFormat.format(new Date(vaccine_entryDTO.entryDate)), Language)%>
											</td>
		
								
		
											<td>
											<%=Utils.getDigits(vaccine_entryDTO.vaccineCount, Language)%>
											</td>
		
											<td>
											<%=vaccine_entryDTO.description%>
											</td>
		
		
		
											
		
		
		
	
											<%CommonDTO commonDTO = vaccine_entryDTO; %>
											<td>
											    <%
											        if (commonDTO.jobCat == SessionConstants.DEFAULT_JOB_CAT) {
											    %>
											    <button
											            type="button"
											            class="btn-sm border-0 shadow btn-border-radius text-white"
											            style="background-color: #ff6b6b;"
											            onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=commonDTO.iD%>'"
											    >
											        <i class="fa fa-edit"></i>
											    </button>
											    <%
											        }
											    %>
											</td>										
																						
											<td class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=vaccine_entryDTO.iD%>'/></span>
												</div>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			