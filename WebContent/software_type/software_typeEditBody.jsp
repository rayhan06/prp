<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="software_type.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
Software_typeDTO software_typeDTO = new Software_typeDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	software_typeDTO = Software_typeDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = software_typeDTO;
String tableName = "software_type";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.SOFTWARE_TYPE_ADD_SOFTWARE_TYPE_ADD_FORMNAME, loginDTO);
String servletName = "Software_typeServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Software_typeServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=software_typeDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right">
															<%=LM.getText(LC.SOFTWARE_TYPE_ADD_SOFTWARECAT, loginDTO)%>	<%=LM.getText(LC.HM_TYPE, loginDTO)%>														</label>
                                                            <div class="col-md-8">
																<select class='form-control'  name='softwareCat' id = 'softwareCat_category_<%=i%>' onchange="getSubCat(this.value)"  tag='pb_html'>		
																<%
																	Options = CatDAO.getOptions( Language, "software", software_typeDTO.softwareCat);
																%>
																<%=Options%>
																</select>
	
                                                           
																<input type='text' class='form-control'  name='otherSoftware' value='' placeholder = "Type Software Name to add New Type"  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right">
															<%=LM.getText(LC.SOFTWARE_TYPE_ADD_SOFTWARESUBCAT, loginDTO)%> <%=LM.getText(LC.HM_TYPE, loginDTO)%>														</label>
                                                            <div class="col-md-8">
																<select class='form-control'  name='softwareSubCat' id = 'softwareSubCat'   tag='pb_html'>		
																<option value = "<%=software_typeDTO.softwareSubCat%>"></option>
																</select>
                                                            
																<input type='text' class='form-control'  name='otherSoftwareSub' placeholder = "Type Version if not found in the dropdown"  value=''   tag='pb_html'/>					
															</div>
                                                      </div>								
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right">
															<%=LM.getText(LC.SOFTWARE_TYPE_ADD_LOT, loginDTO)%>															</label>
                                                            <div class="col-md-8">
																<input type='text' class='form-control'  name='lot' id = 'lot_text_<%=i%>' value='<%=software_typeDTO.lot%>'   tag='pb_html'/>					
															</div>
                                                      </div>
                                                      <div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right">
															<%=LM.getText(LC.HM_PO_NUMBER, loginDTO)%></label>
                                                            <div class="col-md-8">
																<input type='text' class='form-control'  name='poNumber' id = 'poNumber_text_<%=i%>' value='<%=software_typeDTO.poNumber%>'   tag='pb_html'/>					
															</div>
                                                      </div>
                                                      <div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right">
															<%=LM.getText(LC.SOFTWARE_TYPE_ADD_PURCHASEDATE, loginDTO)%>															</label>
                                                            <div class="col-md-8">
																<%value = "purchaseDate_js_" + i;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='purchaseDate' id = 'purchaseDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(software_typeDTO.purchaseDate))%>' tag='pb_html'>
															</div>
                                                      </div>
                                                      <div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right">Has License</label>
                                                            <div class="col-md-8">
																<input type='checkbox' class='form-control-sm mt-1'  name='hasLicense'
																onchange="showHideFields()"
																id = 'hasLicense' <%=software_typeDTO.hasLicense?"checked":""%>   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row" style = "display:none" id = "eDateDiv">
                                                            <label class="col-md-4 col-form-label text-md-right">
															<%=LM.getText(LC.SOFTWARE_TYPE_ADD_EXPIRYDATE, loginDTO)%>															</label>
                                                            <div class="col-md-8">
																<%value = "expiryDate_js_" + i;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																	<jsp:param name="END_YEAR" value="2100"></jsp:param>
																</jsp:include>
																<input type='hidden' name='expiryDate' id = 'expiryDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(software_typeDTO.expiryDate))%>' tag='pb_html'>
															</div>
                                                      </div>									
													
                                                      
                                                      
                                                      
                                                      <div class="form-group row" style = "display:none" id = "quantityDiv">
                                                            <label class="col-md-4 col-form-label text-md-right">Quantity</label>
                                                            <div class="col-md-8">
																<input type='number' class='form-control'  
																name='quantity' id = 'quantity' 
																value='<%=software_typeDTO.quantity%>'   tag='pb_html'/>					
					
															</div>
                                                      </div>									
					
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
               <div class="mt-4" style = "display:none" id = "childDiv">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.SOFTWARE_TYPE_ADD_SOFTWARE_SUBTYPE, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
									<tr>
										<th><%=LM.getText(LC.SOFTWARE_TYPE_ADD_SOFTWARE_SUBTYPE_LICENSEKEY, loginDTO)%></th>
										<th><%=LM.getText(LC.SOFTWARE_TYPE_ADD_SOFTWARE_SUBTYPE_REMOVE, loginDTO)%></th>
									</tr>
								</thead>
							<tbody id="field-SoftwareSubtype">
						
						
								<%
									if(actionName.equals("ajax_edit")){
										int index = -1;
										
										
										for(SoftwareSubtypeDTO softwareSubtypeDTO: software_typeDTO.softwareSubtypeDTOList)
										{
											index++;
											
											System.out.println("index index = "+index);

								%>	
							
								<tr id = "SoftwareSubtype_<%=index + 1%>">
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='softwareSubtype.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=softwareSubtypeDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='softwareSubtype.softwareTypeId' id = 'softwareTypeId_hidden_<%=childTableStartingID%>' value='<%=softwareSubtypeDTO.softwareTypeId%>' tag='pb_html'/>
									</td>
									<td>										





																<input type='text' class='form-control'  name='softwareSubtype.licenseKey' id = 'licenseKey_text_<%=childTableStartingID%>' value='<%=softwareSubtypeDTO.licenseKey%>'   tag='pb_html'/>					
									</td>
									<td>
										<span id='chkEdit' <%=softwareSubtypeDTO.assignedToOrgId != -1? "style=display:none":"" %>>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
												   class="form-control-sm"/>
										</span>
									</td>
								</tr>								
								<%	
											childTableStartingID ++;
										}
									}
								%>						
						
								</tbody>
							</table>
						</div>
						<div class="form-group">
								<div class="col-xs-9 text-right">
									<button
											id="add-more-SoftwareSubtype"
											name="add-moreSoftwareSubtype"
											type="button"
											onclick="childAdded(event, 'SoftwareSubtype')"
											class="btn btn-sm text-white add-btn shadow">
										<i class="fa fa-plus"></i>
										<%=LM.getText(LC.HM_ADD, loginDTO)%>
									</button>
									<button
											id="remove-SoftwareSubtype"
											name="removeSoftwareSubtype"
											type="button"
											onclick="childRemoved(event, 'SoftwareSubtype')"
											class="btn btn-sm remove-btn shadow ml-2 pl-4">
										<i class="fa fa-trash"></i>
									</button>
								</div>
							</div>
					
							<%SoftwareSubtypeDTO softwareSubtypeDTO = new SoftwareSubtypeDTO();%>
					
							<template id="template-SoftwareSubtype" >						
								<tr>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='softwareSubtype.iD' id = 'iD_hidden_' value='<%=softwareSubtypeDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='softwareSubtype.softwareTypeId' id = 'softwareTypeId_hidden_' value='<%=softwareSubtypeDTO.softwareTypeId%>' tag='pb_html'/>
									</td>
									<td>





																<input type='text' class='form-control'  name='softwareSubtype.licenseKey' id = 'licenseKey_text_' value='<%=softwareSubtypeDTO.licenseKey%>'   tag='pb_html'/>					
									</td>
									<td>
											<span id='chkEdit'>
												<input type='checkbox' name='checkbox' value='' deletecb='true'
													   class="form-control-sm"/>
											</span>
									</td>
								</tr>								
						
							</template>
                        </div>
                    </div>
                <div class="form-actions text-center mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.SOFTWARE_TYPE_ADD_SOFTWARE_TYPE_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.SOFTWARE_TYPE_ADD_SOFTWARE_TYPE_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>				

<script type="text/javascript">

function showHideFields()
{
	if($("#hasLicense").prop("checked"))
	{
		$("#quantityDiv").css("display", "none");
		$("#childDiv").css("display", "block");
		$("#eDateDiv").removeAttr("style");
	}
	else
	{
		$("#quantityDiv").removeAttr("style");
		$("#childDiv").css("display", "none");
		$("#eDateDiv").css("display", "none");
	}
	
}

function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);
	preprocessDateBeforeSubmitting('expiryDate', row);
	preprocessDateBeforeSubmitting('purchaseDate', row);

	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Software_typeServlet");	
}

function init(row)
{
	showHideFields();
	setDateByStringAndId('expiryDate_js_' + row, $('#expiryDate_date_' + row).val());
	setDateByStringAndId('purchaseDate_js_' + row, $('#purchaseDate_date_' + row).val());
	getSubCat($("#softwareCat_category_0").val());

	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;

function processRowsWhileAdding(childName)
{
	if(childName == "SoftwareSubtype")
	{			
	}
}

function getSubCat(cat)
{
	var subCat = $("#softwareSubCat").val();
	if(subCat == "" || subCat == null)
	{
		subCat = "-1";
	}
	console.log("subCat = " + subCat + " cat = " + cat);
	var qs = "Software_typeServlet?actionType=getSubType&cat=" + cat + "&subCat=" + subCat;
	fillSelect("softwareSubCat", qs);
}
</script>






