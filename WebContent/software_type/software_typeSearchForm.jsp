
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="software_type.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
String navigator2 = "navSOFTWARE_TYPE";
String servletName = "Software_typeServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.SOFTWARE_TYPE_ADD_SOFTWARECAT, loginDTO)%> <%=LM.getText(LC.HM_TYPE, loginDTO)%></th>
								<th><%=LM.getText(LC.SOFTWARE_TYPE_ADD_SOFTWARESUBCAT, loginDTO)%> <%=LM.getText(LC.HM_TYPE, loginDTO)%></th>
								<th><%=LM.getText(LC.SOFTWARE_TYPE_ADD_LOT, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_PO_NUMBER, loginDTO)%></th>
								<th>Has License</th>
								<th><%=LM.getText(LC.SOFTWARE_TYPE_ADD_EXPIRYDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.SOFTWARE_TYPE_ADD_PURCHASEDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
								<th><%=LM.getText(LC.SOFTWARE_TYPE_SEARCH_SOFTWARE_TYPE_EDIT_BUTTON, loginDTO)%></th>
								<th>Finalize</th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Software_typeDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Software_typeDTO software_typeDTO = (Software_typeDTO) data.get(i);
																																
											
											%>
											<tr>
								
		
											<td>
											<%=CatRepository.getInstance().getText(Language, "software", software_typeDTO.softwareCat)%>
											</td>
		
											<td>
											<%=Software_typeDAO.getInstance().getName(software_typeDTO.softwareCat, software_typeDTO.softwareSubCat)%>
											</td>
		
											<td>
											<%=software_typeDTO.lot%>
											</td>
											
											<td>
											<%=software_typeDTO.poNumber%>
											</td>
											
											<td>
											<%=Utils.getYesNo(software_typeDTO.hasLicense, Language)%>
											</td>
		
											<td>
											<%
											if(software_typeDTO.hasLicense)
											{
											%>
											<%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, software_typeDTO.expiryDate), Language)%>
											<%
											}
											%>
											</td>
		
											<td>
											<%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, software_typeDTO.purchaseDate), Language)%>
											</td>
		
	
											<%CommonDTO commonDTO = software_typeDTO; %>
											<%@page import="sessionmanager.SessionConstants" %>
											<td>
											    <button
											            type="button"
											            class="btn-sm border-0 shadow bg-light btn-border-radius"
											            style="color: #ff6b6b;"
											            onclick="location.href='<%=servletName%>?actionType=view&ID=<%=commonDTO.iD%>'"
											    >
											        <i class="fa fa-eye"></i>
											    </button>
											</td>
											
											<td>
											    <%
											        if (!software_typeDTO.canBeAssigned) {
											    %>
											    <button
											            type="button"
											            class="btn-sm border-0 shadow btn-border-radius text-white"
											            style="background-color: #ff6b6b;"
											            onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=commonDTO.iD%>'"
											    >
											        <i class="fa fa-edit"></i>
											    </button>
											    <%
											        }
											    %>
											</td>
											
											<td>
											    <%
											        if (!software_typeDTO.canBeAssigned) {
											    %>
											    <button
											            type="button"
											            class="btn-sm border-0 shadow btn-border-radius text-white"
											            style="background-color: #cc9b6b;"
											            onclick="location.href='<%=servletName%>?actionType=finalize&id=<%=commonDTO.iD%>'"
											    >
											        <i class="fa fa-stamp"></i>
											    </button>
											    <%
											        }
											    %>
											</td>										
																						
											<td class="text-right">
											<%
											        if (!software_typeDTO.canBeAssigned) {
											    %>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=software_typeDTO.iD%>'/></span>
												</div>
												<%
											        }
												%>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			