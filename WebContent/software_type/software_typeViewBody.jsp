

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="software_type.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Software_typeServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Software_typeDTO software_typeDTO = Software_typeDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = software_typeDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>



<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.SOFTWARE_TYPE_ADD_SOFTWARE_TYPE_ADD_FORMNAME, loginDTO)%>
                </h3>
                <div class="ml-auto mr-3">
				    <button type="button" class="btn" 
				            onclick="location.href='Software_typeServlet?actionType=search'">
				        <i class="fa fa-search fa-2x" style="color: gray" aria-hidden="true"></i>
				    </button>			    			   
				</div>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-0">
                            <div class="col-md-8 offset-md-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.SOFTWARE_TYPE_ADD_SOFTWARE_TYPE_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-start">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.SOFTWARE_TYPE_ADD_SOFTWARECAT, loginDTO)%> <%=LM.getText(LC.HM_TYPE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8">
											<%=CatRepository.getInstance().getText(Language, "software", software_typeDTO.softwareCat)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-start">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.SOFTWARE_TYPE_ADD_SOFTWARESUBCAT, loginDTO)%> <%=LM.getText(LC.HM_TYPE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8">
											<%=Software_typeDAO.getInstance().getName(software_typeDTO.softwareCat, software_typeDTO.softwareSubCat)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-start">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.SOFTWARE_TYPE_ADD_LOT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8">
											<%=software_typeDTO.lot%>
                                    </div>
                                </div>
                                
                                <div class="form-group row d-flex align-items-start">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.HM_PO_NUMBER, loginDTO)%>
                                    </label>
                                    <div class="col-md-8">
											<%=software_typeDTO.poNumber%>
                                    </div>
                                </div>
                                
                                <div class="form-group row d-flex align-items-start">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        Has License
                                    </label>
                                    <div class="col-md-8">
											<%=Utils.getYesNo(software_typeDTO.hasLicense, Language)%>
                                    </div>
                                </div>
			
			
								<%
								if(software_typeDTO.hasLicense)
								{
								%>
								<div class="form-group row d-flex align-items-start">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.SOFTWARE_TYPE_ADD_EXPIRYDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8">
											<%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, software_typeDTO.expiryDate), Language)%>
                                    </div>
                                </div>
                                <%
								}
                                %>
			
								<div class="form-group row d-flex align-items-start">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.SOFTWARE_TYPE_ADD_PURCHASEDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8">
											<%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, software_typeDTO.purchaseDate), Language)%>
                                    </div>
                                </div>
			
			
			
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			


             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%=LM.getText(LC.SOFTWARE_TYPE_ADD_SOFTWARE_SUBTYPE, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
							<%
								if(software_typeDTO.hasLicense)
								{
								%>
								<th><%=LM.getText(LC.SOFTWARE_TYPE_ADD_SOFTWARE_SUBTYPE_LICENSEKEY, loginDTO)%></th>
								<%
								}
								%>
								<th><%=LM.getText(LC.HM_ASSIGNED_TO, loginDTO)%></th>
							</tr>
							<%
                        	SoftwareSubtypeDAO softwareSubtypeDAO = SoftwareSubtypeDAO.getInstance();
                         	List<SoftwareSubtypeDTO> softwareSubtypeDTOs = (List<SoftwareSubtypeDTO>)softwareSubtypeDAO.getDTOsByParent("software_type_id", software_typeDTO.iD);
                         	
                         	for(SoftwareSubtypeDTO softwareSubtypeDTO: softwareSubtypeDTOs)
                         	{
                         		%>
                         			<tr>
		                         		<%
										if(software_typeDTO.hasLicense)
										{
										%>
										<td>
											<%=softwareSubtypeDTO.licenseKey%>
										</td>
										<%
										}
										%>
										<td>
											<%
											String name = WorkflowController.getNameFromOrganogramId(softwareSubtypeDTO.assignedToOrgId, Language);
											if(!name.equalsIgnoreCase(""))
											{
											%>
											
												<%=WorkflowController.getNameFromOrganogramId(softwareSubtypeDTO.assignedToOrgId, Language)%>
												<%
											}
											else
											{
												%>
												N/A
												<%
											}
												%>
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
        </div>
    </div>
</div>