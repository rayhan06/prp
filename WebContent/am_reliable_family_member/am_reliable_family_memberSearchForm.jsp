
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="am_reliable_family_member.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
String navigator2 = "navAM_RELIABLE_FAMILY_MEMBER";
String servletName = "Am_reliable_family_memberServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.AM_RELIABLE_FAMILY_MEMBER_ADD_EMPLOYEERECORDSID, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_RELIABLE_FAMILY_MEMBER_ADD_EMPLOYEEFAMILYINFOID, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_RELIABLE_FAMILY_MEMBER_ADD_REMARKS, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
								<th><%=LM.getText(LC.AM_RELIABLE_FAMILY_MEMBER_SEARCH_AM_RELIABLE_FAMILY_MEMBER_EDIT_BUTTON, loginDTO)%></th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Am_reliable_family_memberDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Am_reliable_family_memberDTO am_reliable_family_memberDTO = (Am_reliable_family_memberDTO) data.get(i);
																																
											
											%>
											<tr>
								
		
											<td>
											<%
											value = am_reliable_family_memberDTO.employeeRecordsId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_reliable_family_memberDTO.employeeFamilyInfoId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_reliable_family_memberDTO.remarks + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
		
		
		
		
		
	
											<%CommonDTO commonDTO = am_reliable_family_memberDTO; %>
											<%@include file="../pb/searchAndViewButton.jsp"%>											
																						
											<td class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=am_reliable_family_memberDTO.iD%>'/></span>
												</div>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			