

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="am_minister_hostel_level.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>
<%@ page import="am_minister_hostel_block.AmMinisterHostelSideDTO" %>
<%@ page import="am_minister_hostel_unit.Am_minister_hostel_unitDAO" %>
<%@ page import="am_minister_hostel_unit.Am_minister_hostel_unitDTO" %>
<%@ page import="am_minister_hostel_block.AmMinisterHostelSideDAO" %>
<%@ page import="am_minister_hostel_block.AmMinisterHostelSideRepository" %>
<%@ page import="am_minister_hostel_unit.Am_minister_hostel_unitRepository" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>


<%
    String servletName = "Am_minister_hostel_levelServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Am_minister_hostel_levelDTO am_minister_hostel_levelDTO = Am_minister_hostel_levelDAO.getInstance().getDTOByID(id);
//            Am_minister_hostel_levelRepository.getInstance().getAm_minister_hostel_levelDTOByID(id);
    CommonDTO commonDTO = am_minister_hostel_levelDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.AM_MINISTER_HOSTEL_LEVEL_ADD_AM_MINISTER_HOSTEL_LEVEL_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-md-8 offset-md-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.AM_MINISTER_HOSTEL_LEVEL_ADD_AM_MINISTER_HOSTEL_LEVEL_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>




								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_MINISTER_HOSTEL_LEVEL_ADD_AMMINISTERHOSTELBLOCKTYPE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = am_minister_hostel_levelDTO.amMinisterHostelBlockId + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "am_minister_hostel_block", "block_no", "id");
											%>
				
											<%=value%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_MINISTER_HOSTEL_LEVEL_ADD_AMMINISTERHOSTELSIDEID, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = am_minister_hostel_levelDTO.amMinisterHostelSideId + "";
											%>
                                        <%
                                            AmMinisterHostelSideDTO amMinisterHostelSideDTO = AmMinisterHostelSideRepository.getInstance().getAmMinisterHostelSideDTOByID(am_minister_hostel_levelDTO.amMinisterHostelSideId);
                                            value = CatRepository.getInstance().getText(Language, "am_minister_hostel_side", amMinisterHostelSideDTO.ministerHostelSideCat);
                                        %>
				
											<%=value%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_MINISTER_HOSTEL_LEVEL_ADD_AMMINISTERHOSTELUNITID, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = am_minister_hostel_levelDTO.amMinisterHostelUnitId + "";
											%>
                                        <%
                                            Am_minister_hostel_unitDTO am_minister_hostel_unitDTO = Am_minister_hostel_unitRepository.getInstance().getAm_minister_hostel_unitDTOByID(am_minister_hostel_levelDTO.amMinisterHostelUnitId);
                                            value = am_minister_hostel_unitDTO.unitNumber;
                                        %>
				
											<%=value%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_MINISTER_HOSTEL_LEVEL_ADD_AMMINISTERHOSTELLEVELCAT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = am_minister_hostel_levelDTO.amMinisterHostelLevelCat + "";
											%>
											<%
											value = CatRepository.getInstance().getText(Language, "am_minister_hostel_level", am_minister_hostel_levelDTO.amMinisterHostelLevelCat);
											%>	
				
											<%=value%>
				
			
                                    </div>
                                </div>


                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=UtilCharacter.getDataByLanguage(Language, "অবস্থা", "Status")%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = CommonApprovalStatus.getText
                                                    (am_minister_hostel_levelDTO.status, Language);
                                        %>

                                        <%=value%>


                                    </div>
                                </div>

                                <% if(am_minister_hostel_levelDTO.status == CommonApprovalStatus.AVAILABLE.getValue()){ %>
                                    <div class="row  ">
                                        <div class="col-12 px-0 text-right mb-3">
                                            <button id="submit-btn"
                                                    class="btn-sm shadow text-white border-0 submit-btn"
                                                    onclick="makeNotAvailable()">
                                                <%=UtilCharacter.getDataByLanguage(Language,
                                                        "ব্যবহারযোগ্য নয়রূপে পরিবর্তন করুন", "Make Not Available")%>
                                            </button>

                                        </div>
                                    </div>
                                <% } %>
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>


<script>


    function notAvailable(remarks){
        $.ajax({
            type : "POST",
            url : "Am_minister_hostel_levelServlet?actionType=setNotAvailable&remarks=" + remarks + "&levelId=" + <%=am_minister_hostel_levelDTO.iD%>,
            dataType : 'JSON',
            success : function(response) {
                if(response.responseCode === 0){
                    $('#toast_message').css('background-color','#ff6063');
                    showToastSticky(response.msg,response.msg);
                }else if(response.responseCode === 200){
                    window.location.replace(getContextPath()+response.msg);
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
            }
        });
    }

    function makeNotAvailable(){
        event.preventDefault();
        let msg = '<%=UtilCharacter.getDataByLanguage(Language, "ব্যবহারযোগ্য নয়রূপে পরিবর্তন করুন", "Make Not Available")%>';
        let placeHolder = '<%=UtilCharacter.getDataByLanguage(Language, "ব্যবহারযোগ্য নয় করার কারণ লিখুন", "Write reason for Not Available")%>';
        let confirmButtonText = '<%=UtilCharacter.getDataByLanguage(Language, "ব্যবহারযোগ্য নয় করুন", "Make Not Available")%>';
        let cancelButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARD_APPROVAL_MAPPING_CANCEL_BUTTON, loginDTO)%>';
        dialogMessageWithTextBoxWithoutAnimation(msg,placeHolder,confirmButtonText,cancelButtonText,"",(reason)=>{
            notAvailable(reason);
        },()=>{});
    }
</script>