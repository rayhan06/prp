<%@page import="am_minister_hostel_level.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>

<%
    Am_minister_hostel_levelDTO am_minister_hostel_levelDTO = new Am_minister_hostel_levelDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        am_minister_hostel_levelDTO = Am_minister_hostel_levelDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = am_minister_hostel_levelDTO;
    String tableName = "am_minister_hostel_level";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.AM_MINISTER_HOSTEL_LEVEL_ADD_AM_MINISTER_HOSTEL_LEVEL_ADD_FORMNAME, loginDTO);
    String servletName = "Am_minister_hostel_levelServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Am_minister_hostel_levelServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=am_minister_hostel_levelDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_MINISTER_HOSTEL_LEVEL_ADD_AMMINISTERHOSTELBLOCKTYPE, loginDTO)%><span>*</span>
                                        </label>
                                        <div class="col-md-8">
                                            <select onchange="loadSideOptions('')" class='form-control'
                                                    name='amMinisterHostelBlockId'
                                                    id='amMinisterHostelBlockId_select_<%=i%>' tag='pb_html'>
                                                <%--																<%--%>
                                                <%--																	Options = CommonDAO.getOptions(Language, "am_minister_hostel_block", am_minister_hostel_levelDTO.amMinisterHostelBlockId);--%>
                                                <%--																%>--%>
                                                <%--																<%=Options%>--%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_MINISTER_HOSTEL_UNIT_ADD_AMMINISTERHOSTELSIDEID, loginDTO)%><span>*</span>
                                        </label>
                                        <div class="col-md-8">
                                            <select onchange="loadUnitOptions('')" class='form-control'
                                                    name='amMinisterHostelSideId'
                                                    id='amMinisterHostelSideId_select_<%=i%>' tag='pb_html'>

                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_MINISTER_HOSTEL_LEVEL_ADD_AMMINISTERHOSTELUNITID, loginDTO)%><span>*</span>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='amMinisterHostelUnitId'
                                                    id='amMinisterHostelUnitId_select_<%=i%>' tag='pb_html'>

                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_MINISTER_HOSTEL_LEVEL_ADD_AMMINISTERHOSTELLEVELCAT, loginDTO)%><span>*</span>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='amMinisterHostelLevelCat'
                                                    id='amMinisterHostelLevelCat_category_<%=i%>' tag='pb_html'>
                                                <%
                                                    Options = CatRepository.getInstance().buildOptions("am_minister_hostel_level", Language, am_minister_hostel_levelDTO.amMinisterHostelLevelCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=am_minister_hostel_levelDTO.searchColumn%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=am_minister_hostel_levelDTO.insertedByUserId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=am_minister_hostel_levelDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=am_minister_hostel_levelDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModifierUser'
                                           id='lastModifierUser_hidden_<%=i%>'
                                           value='<%=am_minister_hostel_levelDTO.lastModifierUser%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value='<%=am_minister_hostel_levelDTO.isDeleted%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=am_minister_hostel_levelDTO.lastModificationTime%>' tag='pb_html'/>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.AM_MINISTER_HOSTEL_LEVEL_ADD_AM_MINISTER_HOSTEL_LEVEL_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.AM_MINISTER_HOSTEL_LEVEL_ADD_AM_MINISTER_HOSTEL_LEVEL_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    let duplicatePackageErr;
    let requiredMsg, duplicateMsg;

    function noDuplicate() {

        var nameBns = document.getElementsByName("amMinisterHostelSide.ministerHostelSideCat");

        var noDuplicate = true;
        const concatedDataSet = new Set();

        for (var i = 0; i < nameBns.length; ++i) {
            var concatedData = nameBns[i].value;
            if (concatedDataSet.has(concatedData)) {
                nameBns[i].parentNode.childNodes.item(1).style.display = 'block';
                nameBns[i].parentNode.childNodes.item(1).innerText = duplicateMsg;

                noDuplicate = false;
            }
            concatedDataSet.add(concatedData);
        }

        return noDuplicate;

    }

    function processBlockResponse(data) {
        document.getElementById('amMinisterHostelBlockId_select_0').innerHTML = data;

        var actionName = '<%=actionName%>';
        if (actionName.includes('edit')) {
            loadSideOptions('&ID=' + '<%=am_minister_hostel_levelDTO.amMinisterHostelSideId%>');
        } else {
            loadSideOptions('');
        }
    }

    function processSideResponse(data) {
        document.getElementById('amMinisterHostelSideId_select_0').innerHTML = data;

        var actionName = '<%=actionName%>';
        if (actionName.includes('edit')) {
            loadUnitOptions('&ID=' + '<%=am_minister_hostel_levelDTO.amMinisterHostelUnitId%>');
        } else {
            loadUnitOptions('');
        }
    }

    function processUnitResponse(data) {
        document.getElementById('amMinisterHostelUnitId_select_0').innerHTML = data;
    }

    function loadUnitOptions(id) {
        var blockId = document.getElementById('amMinisterHostelSideId_select_0').value;
        if (blockId && blockId != undefined && blockId.toString().trim().length > 0 && parseInt(blockId.toString()) > 0) {
            var unitOptionsGetUrl = 'Am_minister_hostel_unitServlet?actionType=getUnitOptionsBySideId&sideId=' + blockId;
            unitOptionsGetUrl += id;
            ajaxGet(unitOptionsGetUrl, processUnitResponse, processUnitResponse);
        }
    }

    function loadSideOptions(id) {
        var blockId = document.getElementById('amMinisterHostelBlockId_select_0').value;
        if (blockId && blockId != undefined && blockId.toString().trim().length > 0 && parseInt(blockId.toString()) > 0) {
            var sideOptionsGetUrl = 'Am_minister_hostel_blockServlet?actionType=getSideOptionsByBlockId&blockId=' + blockId;
            sideOptionsGetUrl += id;
            ajaxGet(sideOptionsGetUrl, processSideResponse, processSideResponse);
        }
    }

    function loadBlockOptions(id) {
        var blockOptionsGetUrl = 'Am_minister_hostel_blockServlet?actionType=getBlockOptions';
        blockOptionsGetUrl += id;
        ajaxGet(blockOptionsGetUrl, processBlockResponse, processBlockResponse);
    }


    function ajaxGet(url, onSuccess, onError) {
        $.ajax({
            type: "GET",
            url: getContextPath() + url,
            dataType: "json",
            success: onSuccess,
            error: onError,
            complete: function () {
                // $.unblockUI();
            }
        });
    }


    function PreprocessBeforeSubmiting(row, validate) {

        let form = $("#bigform");
        form.validate();
        let valid = form.valid();
        if (valid) {
            submitMinisterHostelLevel();
        }
        return false;
    }

    function submitMinisterHostelLevel() {
        buttonStateChange(true);
        console.log("submitting");
        var form = $("#bigform");
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            dataType: 'JSON',
            success: function (response) {
                if (response.responseCode === 0) {
                    console.log("Failed");
                    showToastSticky(response.msg, response.msg);
                    buttonStateChange(false);
                } else if (response.responseCode === 200) {
                    showToastSticky("মিনিস্টার হোস্টেল লেভেল সেভ করা হয়েছে", "Minister hostel level saved");
                    setTimeout(() => {
                        window.location.replace(getContextPath() + response.msg);
                    }, 3000);
                } else {
                    console.log("Error: " + response.responseCode);
                    buttonStateChange(false);
                }
            }
            ,
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChange(false);
            }
        });
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Am_minister_hostel_levelServlet");
    }

    function init(row) {

        var actionName = '<%=actionName%>';
        if (actionName.includes('edit')) {
            loadBlockOptions('&ID=' + '<%=am_minister_hostel_levelDTO.amMinisterHostelBlockId%>');
        } else {
            loadBlockOptions('');
        }

        let lang = '<%=Language%>';

        if (lang == 'English') {
            requiredMsg = 'This is required';
            duplicateMsg = 'This is duplicated';

        } else {
            requiredMsg = '<%="এই তথ্যটি আবশ্যক"%>';
            duplicateMsg = '<%="পূর্বে ব্যবহৃত হয়েছে"%>';

        }


        $.validator.addMethod('validSelector', function (value, element) {
            return value && value != -1;
        });

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                amMinisterHostelBlockId: {
                    validSelector: true,
                },
                amMinisterHostelSideId: {
                    validSelector: true,
                },
                amMinisterHostelUnitId: {
                    validSelector: true,
                },
                amMinisterHostelLevelCat: {
                    validSelector: true,
                },

            },
            messages: {
                amMinisterHostelBlockId: requiredMsg,
                amMinisterHostelSideId: requiredMsg,
                amMinisterHostelUnitId: requiredMsg,
                amMinisterHostelLevelCat: requiredMsg,

            }
        });
    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






