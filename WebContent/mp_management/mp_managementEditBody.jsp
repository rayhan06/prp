<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="political_party.Political_partyRepository" %>

<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.MP_MANAGEMENT_ADD_MP_MANAGEMENT_ADD_FORMNAME, loginDTO);
    String servletName = "Mp_managementServlet";
    String selectOption = Utils.buildSelectOption(isLanguageEnglish);
    String context = "../../.." + request.getContextPath() + "/";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Mp_managementServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="mp_form" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmitting()">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="row mx-2 mx-md-0">
                                        <%@include file="../employee_management/common_employee_mp.jsp"%>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right" for="electionDetailsId">
                                                    <%=LM.getText(LC.EMPLOYEE_PARLIAMENT_NUMBER, loginDTO)%><span>*</span>
                                                </label>
                                                <div class="col-md-8">
                                                    <select class='form-control' name='electionDetailsId' required
                                                            id='electionDetailsId' tag='pb_html' onchange="changeElection()">
                                                        <%=Election_detailsRepository.getInstance().buildOptions(Language, null)%>
                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"
                                                       for="electionConstituencyId">
                                                    <%=LM.getText(LC.EMPLOYEE_ELECTION_CONSTITUENCY, loginDTO)%>
                                                    <span>*</span></label>
                                                <div class="col-md-8">
                                                    <select class='form-control' name='electionConstituencyId' required
                                                            id='electionConstituencyId' tag='pb_html'>
                                                        <%=selectOption%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"
                                                       for="politicalPartyId">
                                                    <%=LM.getText(LC.ELECTION_WISE_MP_POLITICAL_PARTY, loginDTO)%>
                                                    <span>*</span></label>
                                                <div class="col-md-8">
                                                    <select class='form-control' name='politicalPartyId' required
                                                            id='politicalPartyId' tag='pb_html'>
                                                        <%=Political_partyRepository.getInstance().buildOptions(Language, null)%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right" for="electedCount">
                                                    <%=LM.getText(LC.PALCEHOLDER_HOW_MANY_TIMES_ELECTED, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control digitOnly'
                                                           name='electedCount' maxlength="2"
                                                           placeholder='<%=LM.getText(LC.PALCEHOLDER_HOW_MANY_TIMES_ELECTED, loginDTO)%>'
                                                           id='electedCount' tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-12">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.MP_MANAGEMENT_ADD_MP_MANAGEMENT_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn btn-sm shadow text-white border-0 submit-btn ml-2" type="button" onclick="createMP()">
                                <%=LM.getText(LC.MP_MANAGEMENT_ADD_MP_MANAGEMENT_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<%=context%>/assets/scripts/input_validation.js" type="text/javascript"></script>

<script type="text/javascript">

    $(document).ready(function () {
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    function changeElection() {
        document.getElementById("electionConstituencyId").innerHTML = "<%=selectOption%>";
        let electionDetailsId = $('#electionDetailsId').val();
        if (!electionDetailsId) {
            return;
        }
        let url = "Election_constituencyServlet?actionType=buildElectionConstituency&election_id=" + electionDetailsId + "&language=<%=Language%>";
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                console.log(fetchedData);
                document.getElementById("electionConstituencyId").innerHTML = fetchedData;
            },
            error: function (error) {
                showToastSticky(error,error);
            }
        });
    }

    $(document).ready(() => {
        $("#mp_form").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            messages: {
                nameEng: '<%=isLanguageEnglish ? "Please enter name in english":"ইংরেজিতে নাম লিখুন"%>',
                nameBng: '<%=isLanguageEnglish ? "Please enter name in bangla":"বাংলাতে নাম লিখুন"%>',
                electionDetailsId: '<%=isLanguageEnglish ?"Please select parliament number" : "সংসদ নম্বর বাছাই করুন"%>',
                electionConstituencyId: '<%=isLanguageEnglish ?"Please select constituency" : "নির্বাচনী এলাকা বাছাই করুন"%>',
                politicalPartyId: '<%=isLanguageEnglish ?"Please select political party" : "রাজনৈতিক দল বাছাই করুন"%>'
            }
        });
    });

    function createMP() {
        $('#joining-date').val(getDateStringById('joining-date-js'));
        $('#date-of-birth').val(getDateStringById('date-of-birth-js'));
        let joinDateValid = dateValidator('joining-date-js', true, {
            'errorEn': 'Enter a valid joining date!',
            'errorBn': 'চাকরিতে যোগদানের তারিখ প্রবেশ করান!'
        });
        let dobValid = dateValidator('date-of-birth-js', true, {
            'errorEn': 'Enter a valid date of birth!',
            'errorBn': 'জন্মতারিখ প্রবেশ করান!'
        });
        if ($('#mp_form').valid() && joinDateValid && dobValid) {
            let title = '<%=isLanguageEnglish?"Do you want to submit?":"আপনি কি সাবমিট করতে চান?"%>'
            let okBtnText = '<%=isLanguageEnglish?"Submit":"সাবমিট"%>';
            let cancelBtnText = '<%=isLanguageEnglish?"Cancel":"বাতিল"%>';
            messageDialog(title,'','success',true,okBtnText,cancelBtnText,()=>{
                submitAjaxForm('mp_form');
            });
        }
    }

    function buttonStateChange(flag){
        $('.btn').prop('disabled',flag);
    }

</script>