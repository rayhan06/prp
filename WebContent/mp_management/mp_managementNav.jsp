<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="political_party.Political_partyRepository" %>
<%@page contentType="text/html;charset=utf-8" %>

<%
    String url = "Mp_managementServlet?actionType=search";
%>
<%@include file="../pb/navInitializer.jsp" %>
<%
    String selectOption = Utils.buildSelectOption(isLanguageEnglishNav);
%>
<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="userName">
                            <%=LM.getText(LC.HM_USER_NAME, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="userName" name="userName"
                                   placeholder="<%=LM.getText(LC.HM_USER_NAME, loginDTO)%>"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="mobile_number">
                            <%=LM.getText(LC.EMPLOYEE_PERSONAL_MOBILE_NUMBER, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="mobile_number" name="mobile_number" maxlength="11"
                                   placeholder="<%=LM.getText(LC.GLOBAL_MOBILE_PLACE_HOLDER, loginDTO)%>"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="name_eng">
                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEENG, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="name_eng" name="name_eng"
                                   placeholder="<%=LM.getText(LC.GLOBAL_MP_NAME_ENG, loginDTO)%>"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="name_bng">
                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="name_bng" name="name_bng"
                                   placeholder="<%=LM.getText(LC.GLOBAL_MP_NAME_BNG, loginDTO)%>"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="electionDetailsId">
                            <%=LM.getText(LC.EMPLOYEE_PARLIAMENT_NUMBER, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='electionDetailsId' id='electionDetailsId'
                                     onchange="changeElection(null)">
                                <%=Election_detailsRepository.getInstance().buildOptions(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="electionConstituencyId">
                            <%=LM.getText(LC.EMPLOYEE_ELECTION_CONSTITUENCY, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='electionConstituencyId' id='electionConstituencyId'>
                                <%=selectOption%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="politicalPartyId">
                            <%=LM.getText(LC.ELECTION_WISE_MP_POLITICAL_PARTY, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='politicalPartyId' id='politicalPartyId'>
                                <%=Political_partyRepository.getInstance().buildOptions(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="mp_status">
                            <%=LM.getText(LC.MP_REPORT_WHERE_MPSTATUSCAT, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='mpStatus' id='mp_status'
                                    onSelect='setSearchChanged()'>
                                <%=CatRepository.getInstance().buildOptions("mp_status", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>

<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>
<script src="<%=context%>assets/scripts/pb.js"></script>
<script type="text/javascript">
    const engNameSelector = $('#name_eng');
    const bngNameSelector = $('#name_bng');
    const mobileNumberSelector = $('#mobile_number');
    const userNameSelector = $('#userName');
    const anyFieldSelector = $('#anyfield');
    const mpStatusSelector = $('#mp_status');
    const electionDetailsSelector = $('#electionDetailsId');
    const electionConstituencySelector = $('#electionConstituencyId');
    const politicalPartySelector = $('#politicalPartyId');

    window.addEventListener('popstate',e=>{
        if(e.state){
            let paramMap = actualBuildParamMap(e.state);
            let params = 'actionType=search';
            paramMap.forEach((value, key) => {
                if(key!=='actionType'){
                    if(key === 'userName_1'){
                        params += '&userName='+convertBanToEng(value);
                    }else if(key === 'mobile_number_1'){
                        params += '&mobile_number=88'+convertBanToEng(value);
                    }else if (key !== 'userName' && key !== 'mobile_number'){
                        params +="&"+key+"="+value;
                    }
                }
            });
            doSubmit(params,false);
            resetInputs();
            paramMap.forEach((value,key)=>{
                switch (key){
                    case "AnyField":
                        anyFieldSelector.val(value);
                        break;
                    case "name_eng":
                        engNameSelector.val(value);
                        break;
                    case "name_bng":
                        bngNameSelector.val(value);
                        break;
                    case "electionDetailsId":
                        electionDetailsSelector.val(value);
                        select2SingleSelector('#electionDetailsId', '<%=Language%>');
                        changeElection(paramMap.get("electionConstituencyId"));
                        break;
                    case "politicalPartyId":
                        politicalPartySelector.select2("val", value);
                        break;
                    case "mp_status":
                        mpStatusSelector.select2("val", value);
                        break;
                    case "userName_1":
                        userNameSelector.val(value);
                        break;
                    case "mobile_number_1":
                        mobileNumberSelector.val(value);
                        break;
                    default:
                        setPaginationFields([key,value]);
                }
            });
        }else{
            doSubmit(null,false);
            resetInputs();
            resetPaginationFields();
        }
    });

    $(document).ready(()=>{
        readyInit('Mp_managementServlet');
        select2SingleSelector('#politicalPartyId', '<%=Language%>');
        select2SingleSelector('#electionDetailsId', '<%=Language%>');
        select2SingleSelector('#mp_status', '<%=Language%>');
        if(location.href.includes("electionDetailsId")){
            let paramMap = buildParamMap();
            changeElection(paramMap.get("electionConstituencyId"));
        }
        select2SingleSelector('#electionConstituencyId', '<%=Language%>');
    });
    function resetInputs(){
        electionDetailsSelector.select2("val", '-1');
        electionConstituencySelector.select2("val", '-1');
        politicalPartySelector.select2("val", '-1');
        mpStatusSelector.select2("val", '-1');
        anyFieldSelector.val('');
        engNameSelector.val('');
        bngNameSelector.val('');
        mobileNumberSelector.val('');
        userNameSelector.val('');
    }
    function doSubmit(params,pushState=true) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                if(pushState){
                    history.pushState(params,'','Mp_managementServlet?actionType=search&'+params);
                }
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        let url = "Mp_managementServlet?actionType=search&isPermanentTable=true&ajax=true"
        if(params){
            url += "&"+params;
        }
        xhttp.open("GET", url, true);
        xhttp.send();
    }

    function allfield_changed(go, pagination_number,pushState=true) {
        let params = 'search=true';
        if(anyFieldSelector.val()){
            params += '&AnyField=' + anyFieldSelector.val();
        }
        if(engNameSelector.val()){
            params += '&name_eng=' + engNameSelector.val();
        }
        if(bngNameSelector.val()){
            params += '&name_bng=' + bngNameSelector.val();
        }
        if(mobileNumberSelector.val()){
            params += '&mobile_number_1='+mobileNumberSelector.val();
            params += '&mobile_number=88' + convertToEnglishNumber(mobileNumberSelector.val());
        }
        if(userNameSelector.val()){
            params += '&userName_1='+userNameSelector.val();
            params += '&userName=' + convertToEnglishNumber(userNameSelector.val());
        }
        if(electionDetailsSelector.val()){
            params += '&electionDetailsId=' + electionDetailsSelector.val();
        }
        if(electionConstituencySelector.val()){
            params += '&electionConstituencyId=' + electionConstituencySelector.val();
        }
        if(politicalPartySelector.val()){
            params += '&politicalPartyId=' + politicalPartySelector.val();
        }
        if(mpStatusSelector.val()){
            params += '&mpStatus=' + mpStatusSelector.val();
        }

        let extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        let pageNo = document.getElementsByName('pageno')[0].value;
        let rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        let totalRecords = 0;
        let lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }

        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        doSubmit(params,pushState);
    }

    function changeElection(constituency) {
        document.getElementById("electionConstituencyId").innerHTML = "<%=selectOption%>";
        let electionDetailsId = electionDetailsSelector.val();
        if (!electionDetailsId) {
            return;
        }
        let url = "Election_constituencyServlet?actionType=buildElectionConstituency&election_id=" + electionDetailsId + "&language=<%=Language%>";
        if(constituency){
            url+="&constituency="+constituency;
        }
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                document.getElementById("electionConstituencyId").innerHTML = fetchedData;
            },
            error: function (error) {
                showToastSticky(error,error);
            }
        });
    }
</script>