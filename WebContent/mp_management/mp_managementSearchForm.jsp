<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="mp_management.*" %>
<%@ page import="java.util.ArrayList" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.StringUtils" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="election_constituency.Election_constituencyRepository" %>
<%@ page import="political_party.Political_partyRepository" %>
<%@ page import="java.util.List" %>
<%@ page import="pb.CatRepository" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="pb.Utils" %>
<%@ page import="java.util.Collection" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_records.Employee_recordsDTO" %>

<%
    String navigator2 = "navMP_MANAGEMENT";
    String servletName = "Mp_managementServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>

<%
    List<Mp_managementDTO> data = (List<Mp_managementDTO>) rn2.list;

    if (data != null && data.size() > 0) {
%>
<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=getDataByLanguage(Language, "ক্রমিক নম্বর", "Serial Number")%>
            </th>
            <th><%=LM.getText(LC.HM_USER_NAME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEENG, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_PHOTO, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_PERSONAL_MOBILE_NUMBER, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_PERSONALEMAIL, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_PARLIAMENT_NUMBER, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_ELECTION_CONSTITUENCY, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.ELECTION_WISE_MP_POLITICAL_PARTY, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.MP_REPORT_WHERE_MPSTATUSCAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th>
                <div>
                    <button type="button" style="color: red;border: 0;background-color: transparent"
                            onclick="onDeleteEmployee()">
                        <i class="fa fa-trash"></i>
                    </button>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            int index = (rn2.getCurrentPageNo() - 1) * rn2.getPageSize() + 1;
            for (Mp_managementDTO mp_managementDTO : data) {
        %>
        <tr>
            <td>
                <%=Utils.getDigits(index, Language)%>
            </td>
            <td><%=isLanguageEnglish ? mp_managementDTO.userName : StringUtils.convertToBanNumber(mp_managementDTO.userName)%>
            </td>
            <td>
                <%=mp_managementDTO.nameEng == null ? "" : mp_managementDTO.nameEng%>
            </td>

            <td>
                <%=mp_managementDTO.nameBng == null ? "" : mp_managementDTO.nameBng%>
            </td>

            <td>
                <%
                    if (mp_managementDTO.photo!=null) {
                %>
                <img width="75px" height="75px"
                     src='data:image/jpg;base64,<%=new String(Base64.encodeBase64(mp_managementDTO.photo))%>'
                     id="photo-img"

                >
                <%
                    }
                %>
            </td>

            <td>
                <%=mp_managementDTO.mobileNumber == null ? "" :
                        isLanguageEnglish ? mp_managementDTO.mobileNumber :
                                StringUtils.convertToBanNumber(mp_managementDTO.mobileNumber)%>
            </td>

            <td>
                <%=mp_managementDTO.personalEml == null ? "" : mp_managementDTO.personalEml%>
            </td>

            <td>
                <%=Election_detailsRepository.getInstance().getText(mp_managementDTO.electionDetailsId, isLanguageEnglish)%>
            </td>

            <td>
                <%=Election_constituencyRepository.getInstance().getText(mp_managementDTO.electionConstituencyId, isLanguageEnglish)%>
            </td>

            <td>
                <%=Political_partyRepository.getInstance().getText(mp_managementDTO.politicalPartyId, isLanguageEnglish)%>
            </td>

            <td>
                <%=CatRepository.getInstance().getText(Language, "mp_status", mp_managementDTO.mpStatus)%>
            </td>

            <td>
                <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
                        onclick="location.href='Employee_recordsServlet?actionType=viewMultiForm&tab=1&ID=<%=mp_managementDTO.iD%>&userId=<%=mp_managementDTO.userName%>'">
                    <i class="fa fa-eye"></i>
                </button>
            </td>

            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input class="checkbox" type='checkbox' name='ID'
                                                 value='<%=mp_managementDTO.iD%>'/></span>
                </div>
            </td>

        </tr>
        <%
                index++;
            } %>
        </tbody>
    </table>
</div>

<% } else {%>
<label style="width: 100%;text-align: center;font-size: larger;font-weight: bold;color: red">
    <%=isLanguageEnglish ? "No member of parliament information is found" : "কোন সংসদ সদস্য এর তথ্য পাওয়া যায় নি"%>
</label>
<%
    }
%>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>

<script>
    function onDeleteEmployee() {
        const deleteItem = document.getElementsByClassName('checkbox');
        let items = [], ids = ('');
        for (let i = 0; i < deleteItem.length; i++) {
            if (deleteItem[i].checked) {
                items.push(deleteItem[i].value);
                ids += ('&ID=' + parseInt(deleteItem[i].value));
            }
        }


        if (items.length > 0) {
            let title = '<%=isLanguageEnglish?"Do you want to delete selected member of parliament?":"আপনি কি নির্বাচনকৃত সংসদ সদস্য ডিলিট করতে চান?"%>';
            let message = '<%=isLanguageEnglish?"You won't be able to revert this!":"আপনি এটি আর ফিরিয়ে আনতে পারবেন না!"%>';
            let okBtnText = '<%=isLanguageEnglish?"Submit":"সাবমিট"%>';
            let cancelBtnText = '<%=isLanguageEnglish?"Cancel":"বাতিল"%>';
            messageDialog(title, message, 'warning', true, okBtnText, cancelBtnText, () => {
                changeStatus(true);
                const xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState === 4 && this.status === 200) {
                        changeStatus(false);
                        if (this.responseText.includes('option')) {
                            location.reload();
                        } else {
                            showToastSticky('এরর রেসপন্স', "Error response");
                        }
                    } else if (this.readyState === 4 && this.status !== 200) {
                        showToastSticky(this.status, this.status);
                        changeStatus(false);
                    }
                };
                xhttp.open("POST", "Election_wise_mpServlet?actionType=delete" + ids, true);
                xhttp.send();
            });
        } else {
            let title = '<%=isLanguageEnglish?"Please select member of parliament to delete":"অনুগ্রহ করে ডিলিট করার জন্য সংসদ সদস্য নির্বাচন করুন"%>';
            let okBtnText = '<%=isLanguageEnglish?"Ok":"ওকে"%>';
            messageDialog(title, '', 'info', false, okBtnText, '');
        }
    }

    function changeStatus(flag) {
        $('.btn').prop('disabled', flag);
        $('.checkbox').prop('disabled', flag);
    }
</script>