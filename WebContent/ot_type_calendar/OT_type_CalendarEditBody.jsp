<%@page pageEncoding="UTF-8" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="ot_type_calendar.OT_type_CalendarServlet" %>
<%@ page import="holidays.Holiday" %>
<%@ page import="language.LM" %>
<%@ page import="language.LC" %>
<%@ page import="election_details.Election_detailsRepository" %>

<%
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    String context = request.getContextPath() + "/";
    boolean isLanguageEnglish = "english".equalsIgnoreCase(Language);
    String formTitle = isLanguageEnglish ? "Parliament Session Work Calendar" : "সংসদ অধিবেশনের কাজের ক্যালেন্ডার";
    Holiday holiday = new Holiday();
%>

<style>
    .parliament-session-date {
        font-weight: bold;
        color: #e80808 !important;
        background-color: #ddd !important;;
    }

    .parliament-session-date .entry:after {
        content: 'ক দিন' !important;
        font-size: 12px;
        color: black;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <div class="kt-portlet__body form-body">
            <div class="row">
                <div class="col-sm-12 col-md-5 form-group">
                    <label class="h5" for="electionDetailsId">
                        <%=isLanguageEnglish ? "Parliament Number" : "সংসদ নম্বর"%>
                    </label>
                    <select id="electionDetailsId"
                            name='electionDetailsId'
                            class='form-control rounded shadow-sm'
                            onchange="electionDetailsChanged(this);"
                    >
                        <%=Election_detailsRepository.getInstance().buildOptions(Language, null)%>
                    </select>
                </div>
                <div class="col-sm-12 col-md-5 form-group">
                    <label class="h5" for="parliamentSessionId">
                        <%=isLanguageEnglish ? "Parliament Session" : "সংসদ অধিবেশন"%>
                    </label>
                    <select id="parliamentSessionId" name='parliamentSessionId'
                            class='form-control rounded shadow-sm'>
                        <%--Dynamically Added with AJAX--%>
                    </select>
                </div>
                <div class="col-sm-12 col-md-2 form-group">
                    <label class="h5">&nbsp;</label>
                    <button class="form-control btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                            type="button" onclick="location.href='Parliament_sessionServlet?actionType=getAddPage&data=otTypeCalendar'">
                        <%=isLanguageEnglish ? "Add Parliament Session" : "সংসদ অধিবেশন যোগ করুন"%>
                    </button>
                </div>
            </div>

            <div id="dn-calendar-container" class="mt-4"></div>
        </div>
    </div>
</div>

<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<%@include file="/dncalendar/dncalendarJs.jsp" %>
<%@include file="/dncalendar/dncalendarCss.jsp" %>

<script type="text/javascript">
    const parliamentSessionId = document.getElementById('parliamentSessionId');
    const electionDetailsId = document.getElementById('electionDetailsId');

    async function electionDetailsChanged(selectElement) {
        parliamentSessionId.innerHTML = '';
        let electionDetailsId = selectElement.value;
        if (electionDetailsId === -1 || electionDetailsId === "") {
            return;
        }
        const url = 'OT_type_CalendarServlet?actionType=ajax_getParliamentSession'
                    + '&electionDetailsId=' + electionDetailsId;
        const response = await fetch(url);
        parliamentSessionId.innerHTML = await response.text();
    }

    function typeOnlyInteger(e) {
        return true === inputValidationForPositiveValue(e, $(this), <%=Integer.MAX_VALUE%>, 0);
    }

    const parliamentNumberInput = $('#parliamentNumber');
    const parliamentSessionNumberInput = $('#parliamentSessionNumber');

    $(() => {
        const htmlBody = $('body');
        htmlBody.delegate('[data-only-integer="true"]', 'keydown', typeOnlyInteger);
    });

    const CREATED = '<%=OT_type_CalendarServlet.CalendarInsertionStatus.CREATED%>';
    const REMOVED = '<%=OT_type_CalendarServlet.CalendarInsertionStatus.REMOVED%>';

    async function addOtTypeCalendar(data, dateCellElement) {
        try {
            const backEndResponse = await fetch('OT_type_CalendarServlet?actionType=ajax_add', {
                method: 'post',
                body: new URLSearchParams(data)
            });
            const response = await backEndResponse.json();
            console.log(response);
            if (response.success) {
                switch (response.status) {
                    case CREATED:
                        dateCellElement.addClass('parliament-session-date');
                        specialDaysArray.push(response.createdNote);
                        myCalendar.update({
                            specialDays: specialDaysArray,
                        });
                        break;
                    case REMOVED:
                        dateCellElement.removeClass('parliament-session-date');
                        specialDaysArray = specialDaysArray.filter(note => note.date !== response.createdNote.date);
                        myCalendar.update({
                            specialDays: specialDaysArray,
                        });
                        break;
                }
            } else {
                $('#toast_message').css('background-color', '#ff6063');
                showToast(response.errorMessage, response.errorMessage);
            }
        } catch (error) {
            console.log(error);
            $('#toast_message').css('background-color', '#ff6063');
            showToast("সার্ভারে সমস্যা", "Server Error");
        }
    }

    function getFullDateFromDnCalendarDate(dnCalendarDate) {
        return dnCalendarDate.getDate() + "-" + (dnCalendarDate.getMonth() + 1) + "-" + dnCalendarDate.getFullYear();
    }

    let myCalendar;
    let specialDaysArray = <%=OT_type_CalendarServlet.getAllForDNCalendar()%>;
    $(document).ready(function () {
        myCalendar = $("#dn-calendar-container").dnCalendar({
            dataTitles: {defaultDate: 'default', today: 'Today'},
            notes: <%=holiday.getNotesForDNCalendar()%>,
            showNotes: true,
            dayClick: function (date, view) {
                const dateStr = getFullDateFromDnCalendarDate(date);
                const data = {
                    dateStr: dateStr,
                    electionDetailsId: electionDetailsId.value,
                    parliamentSessionId: parliamentSessionId.value
                };
                console.log(data);
                addOtTypeCalendar(data, $(this));
            },
            specialDays: specialDaysArray,
            specialDayCSSClass: 'parliament-session-date',
            holidays: <%=holiday.getHolidayForDNCalendar()%>
        });
        myCalendar.build();
        myCalendar.update({
            minDate: "2000-01-01"
        });
    });
</script>