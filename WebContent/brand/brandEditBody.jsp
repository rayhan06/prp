<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="brand.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
BrandDTO brandDTO;
brandDTO = (BrandDTO)request.getAttribute("brandDTO");
CommonDTO commonDTO = brandDTO;
if(brandDTO == null)
{
	brandDTO = new BrandDTO();
	
}
String tableName = "brand";
%>
<%@include file="../pb/addInitializer.jsp"%>
<%
String formTitle = LM.getText(LC.BRAND_ADD_BRAND_ADD_FORMNAME, loginDTO);
String servletName = "BrandServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="BrandServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=brandDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.BRAND_ADD_NAMEEN, loginDTO)%> *</label>
                                                            <div class="col-md-8">
																<input type='text' class='form-control'  name='nameEn' id = 'nameEn_text_<%=i%>' value='<%=brandDTO.nameEn%>'  required="required"  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.BRAND_ADD_NAMEBN, loginDTO)%></label>
                                                            <div class="col-md-8">
																<input type='text' class='form-control'  name='nameBn' id = 'nameBn_text_<%=i%>' value='<%=brandDTO.nameBn%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.BRAND_ADD_ASSETCATEGORYTYPE, loginDTO)%></label>
                                                            <div class="col-md-8">
																<select class='form-control'  name='assetCategoryType' id = 'assetCategoryType_select_<%=i%>'   tag='pb_html'>
																<%
																	Options = CommonDAO.getOptions(Language, "asset_category", brandDTO.assetCategoryType);
																%>
																<%=Options%>
																</select>
		
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=brandDTO.searchColumn%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=brandDTO.insertedByUserId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value='<%=brandDTO.insertedByOrganogramId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=brandDTO.insertionDate%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='lastModifierUser' id = 'lastModifierUser_hidden_<%=i%>' value='<%=brandDTO.lastModifierUser%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=brandDTO.isDeleted%>' tag='pb_html'/>
											
														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=brandDTO.lastModificationTime%>' tag='pb_html'/>
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.BRAND_ADD_BRAND_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.BRAND_ADD_BRAND_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, validate)
{

	submitAddForm();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "BrandServlet");	
}

function init(row)
{


	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;



</script>






