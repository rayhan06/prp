<%@page import="sessionmanager.SessionConstants" %>
<%@page import="employee_education_info.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="files.*" %>
<%@page import="java.time.LocalDate" %>
<%@ page import="pb.*" %>
<%@ page import="com.fasterxml.jackson.databind.ObjectMapper" %>
<%@ page import="education_level.Education_levelRepository" %>
<%@ page import="result_exam.Result_examRepository" %>
<%@ page import="board.BoardRepository" %>
<%@ page import="university_info.University_infoDAO" %>
<%@page import="dbm.*" %>
<%@ page import="common.BaseServlet" %>

<%@include file="../pb/addInitializer2.jsp" %>
<%
    String currentYear = String.valueOf(LocalDate.now().getYear());
    Long degreeExamType = null;

    Employee_education_infoDTO employee_education_infoDTO;
    String formTitle;
    if (isEditActionType) {
        formTitle = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_EMPLOYEE_EDUCATION_INFO_EDIT_FORMNAME, loginDTO);
        employee_education_infoDTO = (Employee_education_infoDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
        degreeExamType = employee_education_infoDTO.degreeExamType;
    } else {
        formTitle = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_EMPLOYEE_EDUCATION_INFO_ADD_FORMNAME, loginDTO);
        employee_education_infoDTO = new Employee_education_infoDTO();
    }

    List<CategoryLanguageModel> catDtos = CatRepository.getInstance().getCategoryLanguageModelList("grade_point");
    ObjectMapper objectMapper = new ObjectMapper();
    String gradePointCatDtosJson = objectMapper.writeValueAsString(catDtos);
    long empId;
    if (request.getParameter("empId") == null) {
        empId = userDTO.employee_record_id;
    } else {
        empId = Long.parseLong(request.getParameter("empId"));
    }

    String url = "Employee_education_infoServlet?actionType=" + actionName + "&isPermanentTable=true&empId=" + empId + "&iD=" + employee_education_infoDTO.iD;
    if (request.getParameter("tab") != null) {
        url += "&tab=" + request.getParameter("tab")+"&userId="+request.getParameter("userId");
    }
    if (employee_education_infoDTO.filesDropzone <= 0) {
        employee_education_infoDTO.filesDropzone = DBMW.getInstance().getNextSequenceId("fileid");
    }
%>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="emp_edu_form" name="bigform" enctype="multipart/form-data" action="<%=url%>">
            <div class="form-body">
                <div class="kt-portlet__body form-body">
                    <div class="row">
                        <div class="col-md-10 offset-md-1">
                            <div class="onlyborder">
                                <div class="row px-4 px-md-0">
                                    <div class="col-md-10 offset-md-1">
                                        <div class="sub_title_top">
                                            <div class="sub_title">
                                                <h4 style="background-color: #FFFFFF">
                                                    <%=formTitle%>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                                   for="educationLevelType_select">
                                                <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_EDUCATIONLEVELTYPE, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-8 col-xl-9 " id='educationLevelType_div_<%=i%>'>
                                                <select class='form-control' name='educationLevelType'
                                                        id='educationLevelType_select' tag='pb_html'>
                                                    <%=Education_levelRepository.getInstance().buildOptions(Language, employee_education_infoDTO.educationLevelType)%>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row" id='degreeExamType_div'>
                                            <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                                   for="degreeExamType_select">
                                                <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_DEGREEEXAMTYPE, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-8 col-xl-9">
                                                <select class='form-control' name='degreeExamType'
                                                        id='degreeExamType_select'
                                                        tag='pb_html'></select>
                                            </div>
                                        </div>
                                        <div class="form-group row" id='otherDegreeExam_div' style="display:none;">
                                            <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                                   for="otherDegreeExam_text"
                                                   id="otherDegreeExam_label">
                                                <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_OTHERDEGREEEXAM, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-xl-9 ">
                                                <input type='text' class='form-control rounded-lg'
                                                       name='otherDegreeExam' maxlength="128"
                                                       id='otherDegreeExam_text'
                                                       value='<%=employee_education_infoDTO.otherDegreeExam!=null?employee_education_infoDTO.otherDegreeExam:""%>'>
                                            </div>
                                        </div>
                                        <div class="form-group row" id="major_div">
                                            <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                                   for="major_text">
                                                <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_MAJOR, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-xl-9">
                                                <input type='text' class='form-control rounded-lg'
                                                       name='major'
                                                       id='major_text'
                                                       maxlength="64"
                                                       value='<%=employee_education_infoDTO.major!=null?employee_education_infoDTO.major:""%>'>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                                   for="resultExamType_select">
                                                <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_RESULTEXAMTYPE, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-xl-9 " id='resultExamType_div_<%=i%>'>
                                                <select class='form-control' name='resultExamType'
                                                        id='resultExamType_select'
                                                        tag='pb_html'>
                                                    <%=Result_examRepository.getInstance().buildOptions(Language, employee_education_infoDTO.resultExamType)%>
                                                </select>
                                            </div>
                                        </div>

                                        <div id="grade_div">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                                       for="gradePointCat_category">
                                                    <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_GRADEPOINTCAT, loginDTO)%>
                                                </label>
                                                <div class="col-md-8 col-xl-9 " id='gradePointCat_div'>
                                                    <select class='form-control' name='gradePointCat'
                                                            id='gradePointCat_category'
                                                            tag='pb_html'>
                                                        <%=CatRepository.getInstance().buildOptions("grade_point", Language, employee_education_infoDTO.gradePointCat)%>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                                       for="cgpaNumber_text">
                                                    <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_CGPANUMBER, loginDTO)%>
                                                </label>
                                                <div class="col-md-8 col-xl-9 " id='cgpaNumber_div'>
                                                    <input type='text'
                                                           class='form-control rounded-lg'
                                                           name='cgpaNumber'
                                                           id='cgpaNumber_text'
                                                           value='<%=employee_education_infoDTO.cgpaNumber!=null?employee_education_infoDTO.cgpaNumber:""%>'>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                                   for="yearOfPassingNumber_number">
                                                <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_YEAROFPASSINGNUMBER, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-xl-9 " id='yearOfPassingNumber_div'>
                                                <input type='text' class='form-control rounded-lg'
                                                       name='yearOfPassingNumber'
                                                       id='yearOfPassingNumber_number'
                                                       value='<%=employee_education_infoDTO.yearOfPassingNumber>0?employee_education_infoDTO.yearOfPassingNumber:""%>'>

                                            </div>
                                        </div>
                                        <div class="form-group row" id="boardType_div">
                                            <label class="col-md-4 col-xl-3 control-label text-md-right"
                                                   for="boardType_select">
                                                <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_BOARDTYPE, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-xl-9 " id='boardType_div_<%=i%>'>
                                                <select class='form-control' name='boardType'
                                                        id='boardType_select'
                                                        tag='pb_html'>
                                                    <%=BoardRepository.getInstance().buildOptions(Language, employee_education_infoDTO.boardType)%>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row" id="instituteType_div">
                                            <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                                   for="instituteType_select">
                                                <%=Language.equalsIgnoreCase("English") ? "University" : "বিশ্ববিদ্যালয়"%>
                                            </label>
                                            <div class="col-md-8 col-xl-9 " id='instituteType_div_<%=i%>'>
                                                <select class='form-control' name='instituteType'
                                                        id='instituteType_select'
                                                        tag='pb_html'>
                                                    <%String options = new University_infoDAO().getUniversityList(Language, employee_education_infoDTO.universityInfoType);%>
                                                    <%=options%>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row" id="instituteName_div">
                                            <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                                   for="institutionName_text">
                                                <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_INSTITUTIONNAME, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-xl-9 " id='institutionName_div_<%=i%>'>
                                                <input type='text' class='form-control rounded-lg'
                                                       name='institutionName'
                                                       id='institutionName_text'
                                                       maxlength="512"
                                                       value='<%=employee_education_infoDTO.institutionName!=null?employee_education_infoDTO.institutionName:""%>'>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                                   for="isForeign_checkbox_<%=i%>">
                                                <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_ISFOREIGN, loginDTO)%>
                                            </label>
                                            <div class="col-1 " id='isForeign_div_<%=i%>'>
                                                <input type='checkbox' class='form-control-sm mt-md-1 rounded-lg'
                                                       name='isForeign'
                                                       id='isForeign_checkbox_<%=i%>'
                                                       value='true' <%=(isEditActionType && String.valueOf(employee_education_infoDTO.isForeign).equals("true"))?("checked"):""%>
                                                       tag='pb_html'><br>
                                            </div>
                                        </div>
                                        <div class="form-group row" style="display: none" id='durationYears_div_<%=i%>'>
                                            <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                                   for="durationYears_number">
                                                <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_DURATIONYEARS, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-xl-9 ">
                                                <input type='text' class='form-control rounded-lg'
                                                       name='durationYears'
                                                       id='durationYears_number'
                                                       value='<%=employee_education_infoDTO.durationYears>0?employee_education_infoDTO.durationYears:""%>'>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                                   for="achievement_text_<%=i%>">
                                                <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_ACHIEVEMENT, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-xl-9 " id='achievement_div_<%=i%>'>
                                                  <textarea rows="4" style="resize: none"
                                                            class='form-control rounded-lg'
                                                            name='achievement'
                                                            id='achievement_text_<%=i%>' maxlength="1024"
                                                            tag='pb_html'><%=employee_education_infoDTO.achievement != null ? employee_education_infoDTO.achievement.trim() : ""%></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_FILESDROPZONE, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-xl-9 " id='filesDropzone_div_<%=i%>'>
                                                <%
                                                    if (isEditActionType && employee_education_infoDTO.filesDropzone > 0) {
                                                        List<FilesDTO> filesDropzoneDTOList = new FilesDAO().getMiniDTOsByFileID(employee_education_infoDTO.filesDropzone);
                                                %>
                                                <table>
                                                    <tr>
                                                        <%
                                                            if (filesDropzoneDTOList != null) {
                                                                for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                                                    FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                                                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                        %>
                                                        <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                                            <%
                                                                if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                            %>
                                                            <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px'/>
                                                            <%
                                                                }
                                                            %>
                                                            <a href='Employee_education_infoServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                               download><%=filesDTO.fileTitle%>
                                                            </a>
                                                            <a class='btn btn-danger rounded-lg'
                                                               onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>
                                                        </td>
                                                        <%
                                                                }
                                                            }
                                                        %>
                                                    </tr>
                                                </table>
                                                <%
                                                    }
                                                %>

                                                <div class="dropzone rounded-lg"
                                                     action="Employee_education_infoServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=employee_education_infoDTO.filesDropzone%>">
                                                    <input type='file' style="display:none"
                                                           name='filesDropzoneFile'
                                                           id='filesDropzone_dropzone_File_<%=i%>'
                                                           tag='pb_html'/>
                                                </div>
                                                <input type='hidden' name='filesDropzoneFilesToDelete'
                                                       id='filesDropzoneFilesToDelete_<%=i%>' value=''
                                                       tag='pb_html'/>
                                                <input type='hidden' name='filesDropzone'
                                                       id='filesDropzone_dropzone_<%=i%>'
                                                       tag='pb_html'
                                                       value='<%=employee_education_infoDTO.filesDropzone%>'>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11 mt-3 text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_EMPLOYEE_EDUCATION_INFO_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                                    onclick="submitForm()">
                                <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_EMPLOYEE_EDUCATION_INFO_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<%=contextOfPath%>assets/scripts/util1.js"></script>

<script type="text/javascript">
    const language = "<%=Language%>";
    const form = $("#emp_edu_form");
    const otherIdValue = 2147483647;
    <%=SessionConstants.OTHER_ID_VALUE%>
    var curYear = "<%=currentYear%>";
    let selectedGradeScale;
    const scale_cal_list =<%=gradePointCatDtosJson%>;
    let degreeExam = <%=degreeExamType%>;
    const durationYearsDiv = $('#durationYears_div_0');
    let durationYearsValidator = null;
    $(document).ready(function () {
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        initUi();
        select2SingleSelector("#educationLevelType_select", '<%=Language%>');
        select2SingleSelector("#degreeExamType_select", '<%=Language%>');
        select2SingleSelector("#resultExamType_select", '<%=Language%>');
        select2SingleSelector("#gradePointCat_category", '<%=Language%>');
        select2SingleSelector("#instituteType_select", '<%=Language%>');
        select2SingleSelector("#boardType_select", '<%=Language%>');

        $.validator.addMethod('educationLevelSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('degreeTypeSelection', function (value, element) {
            return value != 0;
        });

        form.validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                educationLevelType: {
                    required: true,
                    educationLevelSelection: true
                },
                degreeExamType: {
                    required: true,
                    degreeTypeSelection: true
                }
            },
            messages: {
                educationLevelType: "<%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_SELECT_EDUCATION_LEVEL, loginDTO)%>",
                degreeExamType: "<%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_SELECT_DEGREE_TYPE_EXAM, loginDTO)%>"
            }
        });

        $('#educationLevelType_select').on('select2:select', function (e) {
            switch ($(this).val()) {
                case '1':
                case '2':
                case '3':
                case '4':
                    durationYearsDiv.hide();
                    break;
                default:
                    durationYearsDiv.show();
            }
        });
        <%
            if(isEditActionType && employee_education_infoDTO.educationLevelType<=4){
        %>
        form.validate().settings.rules.durationYears.required = false;
        <%
            }
        %>
    });


    function degreeOtherExam() {
        if ($('#degreeExamType_select').val() == otherIdValue) {
            $('#otherDegreeExam_div').show();
        } else {
            $('#otherDegreeExam_div').hide();
            $('#otherDegreeExam_div').val('');
        }
    }

    function selectResultType() {
        if ($('#resultExamType_select').val() == '') {
            $('#grade_div').hide();
            $('#cgpaNumber_text').val("");
            $("#gradePointCat_category").val('');
        } else if ($('#resultExamType_select').val() == 4) {
            $('#grade_div').show();
        } else {
            $('#grade_div').hide();
            $('#cgpaNumber_text').val("");
            $("#gradePointCat_category").val('');
        }
    }

    function initUi() {
        $('#grade_div').hide();
        if (<%=isEditActionType%>) {
            degreeOtherExam();
            selectResultType();
        }
        showOrHideBoardType();
        showOrHideInstitutionType();
        $('#educationLevelType_select').change(function () {
            showOrHideBoardType();
            showOrHideInstitutionType();
        });

        if ($('#educationLevelType_select').val()) {
            fetchDegree($('#educationLevelType_select').val(), degreeExam);
        }
        $('#educationLevelType_select').change(function () {
            fetchDegree($(this).val(), null);
            showOrHideMajor();
        });
        $('#degreeExamType_select').change(function () {
            degreeOtherExam();
        });

        $('#resultExamType_select').change(function () {
            selectResultType();
        });

        $('#yearOfPassingNumber_number').keydown(function (e) {
            return inputValidationForIntValue(e, $(this), curYear);
        });

        $('#durationYears_number').keydown(function (e) {
            return inputValidationForIntValue(e, $(this), 9);
        });

        $('#gradePointCat_category').change(function () {
            $('#cgpaNumber_text').val("");
            selectedGradeScale = getSelectedGradeScaleEngText($("#gradePointCat_category").val());
            console.log("selectedGradeScale : " + selectedGradeScale);
        });

        $('#cgpaNumber_text').keydown(function (e) {
            return inputValidationForFloatValue(e, $(this), 2, selectedGradeScale);
        });
        showOrHideMajor();

        <%if(employee_education_infoDTO.educationLevelType>4){%>
        $('#durationYears_div_0').show();
        <%}%>
    }

    function showOrHideMajor() {
        let value = $('#educationLevelType_select').val();
        if (value > 2) {
            $('#major_div').show();
        } else {
            $('#major_div').hide();
            $('#major_text').val("");
        }
    }

    function showOrHideBoardType() {
        if ($('#educationLevelType_select').val() && $('#educationLevelType_select').val() <= 4) {
            $('#boardType_div').show();
            // $('#boardType_select').val(null);

        } else {
            $('#boardType_div').hide();
            $('#boardType_select').val('');

        }
    }

    function showOrHideInstitutionType() {
        if ($('#educationLevelType_select').val() && $('#educationLevelType_select').val() > 4) {
            $('#instituteType_div').show();

        } else {
            $('#instituteType_div').hide();
            $('#instituteType_select').val(null);
        }
    }

    function isFormValid() {
        preprocessCheckBoxBeforeSubmitting('isForeign', 0);
        return form.valid();
    }

    function fetchDegree(educationLevelId, selectedDegreeValue) {
        let url = "option_Servlet?actionType=ajax_type&type=degree&education_level_id=" + educationLevelId;
        if (selectedDegreeValue) {
            url += "&selectedValue=" + selectedDegreeValue;
        }
        $('#degreeExamType_select').html("");
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                document.getElementById("degreeExamType_select").innerHTML = fetchedData;
                degreeOtherExam();
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function getSelectedGradeScaleEngText(value) {
        for (let i in scale_cal_list) {
            if (scale_cal_list[i].categoryValue == value) {
                return scale_cal_list[i].englishText;
            }
        }
        return null;
    }

    function submitForm() {
        buttonStateChange(true);
        if (isFormValid()) {
            submitAjaxForm('emp_edu_form');
        } else {
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }
</script>