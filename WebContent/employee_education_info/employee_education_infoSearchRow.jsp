<%@page pageEncoding="UTF-8" %>
<%@ page import="education_level.Education_levelRepository" %>
<%@ page import="education_level.DegreeExamRepository" %>
<%@ page import="result_exam.Result_examRepository" %>
<%@ page import="board.BoardRepository" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%
    if (isAdmin) {
%>
<td>
    <%=Employee_recordsRepository.getInstance().getEmployeeName(employee_education_infoDTO.employeeRecordsId, Language)%>
</td>
<%
    }
%>
<td><%=Education_levelRepository.getInstance().getText(Language, employee_education_infoDTO.educationLevelType)%>
</td>

<td><%=DegreeExamRepository.getInstance().getText(Language, employee_education_infoDTO.degreeExamType, employee_education_infoDTO.educationLevelType)%>
</td>

<td><%=Result_examRepository.getInstance().getText(Language, employee_education_infoDTO.resultExamType)%>
</td>

<td><%=employee_education_infoDTO.yearOfPassingNumber%>
</td>

<td><%=BoardRepository.getInstance().getText(Language, employee_education_infoDTO.boardType)%>
</td>

<td><%=employee_education_infoDTO.institutionName != null ? employee_education_infoDTO.institutionName : ""%>
</td>

<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;max-height: 30px"
            onclick="location.href='Employee_education_infoServlet?actionType=view&ID=<%=employee_education_infoDTO.iD%>&empId=<%=employee_education_infoDTO.employeeRecordsId%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;max-height: 30px"
            onclick="location.href='Employee_education_infoServlet?actionType=getEditPage&ID=<%=employee_education_infoDTO.iD%>&empId=<%=employee_education_infoDTO.employeeRecordsId%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=employee_education_infoDTO.iD%>'/></span>
    </div>
</td>