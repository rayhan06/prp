<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="employee_education_info.*"%>
<%@page import="files.*"%>
<%@ page import="education_level.Education_levelRepository" %>
<%@ page import="education_level.DegreeExamRepository" %>
<%@ page import="result_exam.Result_examRepository" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="util.StringUtils" %>
<%@ page import="board.BoardRepository" %>
<%@ page import="java.util.List" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="university_info.University_infoDTO" %>
<%@ page import="university_info.University_infoDAO" %>

<%
	LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
	String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
	boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
	Employee_education_infoDTO employee_education_infoDTO = (Employee_education_infoDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
	<div class="kt-portlet">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title prp-page-title">
					<i class="fa fa-gift"></i><%=isLangEng ? "Education Info Details" : "শিক্ষার বিশদ তথ্য"%>
				</h3>
			</div>
		</div>
		<div class="kt-portlet__body form-body">
			<div class="row">
				<div class="col-md-8 offset-md-2">
					<div class="onlyborder">
						<div class="row">
							<div class="col-md-10 offset-md-1">
								<div class="sub_title_top">
									<div class="sub_title">
										<h4 style="background-color: white"><%=isLangEng?"Education Info":"শিক্ষার তথ্য"%></h4>
									</div>
								</div>
								<div class="table-responsive">
									<table class="table table-bordered table-striped text-nowrap">
										<tr>
											<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_EDUCATIONLEVELTYPE, loginDTO)%>
											</b></td>
											<td>
												<%=Education_levelRepository.getInstance().getText(Language,employee_education_infoDTO.educationLevelType)%>
											</td>
										</tr>

										<tr>
											<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_DEGREEEXAMTYPE, loginDTO)%>
											</b></td>
											<td>
												<%=DegreeExamRepository.getInstance().getText(Language,employee_education_infoDTO.degreeExamType,employee_education_infoDTO.educationLevelType)%>
											</td>
										</tr>


										<%if (employee_education_infoDTO.otherDegreeExam != null && employee_education_infoDTO.otherDegreeExam.trim().length() > 0) {%>
										<tr>
											<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_OTHERDEGREEEXAM, loginDTO)%>
											</b></td>
											<td>
												<%=employee_education_infoDTO.otherDegreeExam == null ? "" :employee_education_infoDTO.otherDegreeExam%>
											</td>
										</tr>
										<%} %>

										<%if (employee_education_infoDTO.major != null && employee_education_infoDTO.major.trim().length() > 0) {%>
										<tr>
											<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_MAJOR, loginDTO)%>
											</b></td>
											<td>
												<%=employee_education_infoDTO.major%>
											</td>
										</tr>
										<%}%>

										<tr>
											<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_RESULTEXAMTYPE, loginDTO)%>
											</b></td>
											<td>
												<%=Result_examRepository.getInstance().getText(Language,employee_education_infoDTO.resultExamType)%>
											</td>
										</tr>

										<%
											if(CatRepository.getInstance().getCategoryLanguageModel("grade_point", employee_education_infoDTO.gradePointCat)!=null){
										%>
										<tr>
											<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_GRADEPOINTCAT, loginDTO)%>
											</b></td>
											<td>
												<%=CatRepository.getInstance().getText(Language, "grade_point", employee_education_infoDTO.gradePointCat)%>
											</td>
										</tr>

										<tr>
											<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_CGPANUMBER, loginDTO)%>
											</b></td>
											<td>
												<%=isLangEng?employee_education_infoDTO.cgpaNumber: StringUtils.convertToBanNumber(String.valueOf(employee_education_infoDTO.cgpaNumber))%>
											</td>
										</tr>
										<%
											}
										%>

										<tr>
											<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_YEAROFPASSINGNUMBER, loginDTO)%>
											</b></td>
											<td>
												<%if(employee_education_infoDTO.yearOfPassingNumber > 0){%>
												<%=isLangEng?employee_education_infoDTO.yearOfPassingNumber : StringUtils.convertToBanNumber(String.valueOf(employee_education_infoDTO.yearOfPassingNumber))%>
												<%}%>
											</td>
										</tr>


										<%
											if(employee_education_infoDTO.educationLevelType<=4){
										%>
										<tr>
											<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_BOARDTYPE, loginDTO)%>
											</b></td>
											<td>
												<%=BoardRepository.getInstance().getText(Language,employee_education_infoDTO.boardType)%>
											</td>
										</tr>
										<%
											}else {
												University_infoDTO universityInfoDTO = new University_infoDAO().getDTOByID(employee_education_infoDTO.universityInfoType);
										%>
										<td style="width:30%"><b><%=isLangEng?"University":"বিশ্ববিদ্যালয়"%>
										</b></td>
										<td>
											<%=universityInfoDTO == null?"":(isLangEng?universityInfoDTO.universityNameEn:universityInfoDTO.universityNameBn)%>
										</td>
										<%
											}
										%>

										<tr>
											<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_INSTITUTIONNAME, loginDTO)%>
											</b></td>
											<td>
												<%=employee_education_infoDTO.institutionName%>
											</td>
										</tr>

										<tr>
											<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_ISFOREIGN, loginDTO)%>
											</b></td>
											<td>
												<%=StringUtils.getYesNo(Language,employee_education_infoDTO.isForeign)%>
											</td>
										</tr>

										<%
											if(employee_education_infoDTO.educationLevelType>4){
										%>
										<tr>
											<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_DURATIONYEARS, loginDTO)%>
											</b></td>
											<td>
												<%=isLangEng?employee_education_infoDTO.durationYears:StringUtils.convertToBanNumber(String.valueOf(employee_education_infoDTO.durationYears))%>
											</td>
										</tr>
										<%
											}
										%>


										<tr>
											<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_ACHIEVEMENT, loginDTO)%>
											</b></td>
											<td style="word-wrap: break-word">
												<%=employee_education_infoDTO.achievement%>
											</td>
										</tr>


										<tr>
											<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_FILESDROPZONE, loginDTO)%>
											</b></td>
											<td>
												<% {
													List<FilesDTO> FilesDTOList = new FilesDAO().getMiniDTOsByFileID(employee_education_infoDTO.filesDropzone);
												%>
												<table>
													<tr><%
														if (FilesDTOList != null) {
															for (int j = 0; j < FilesDTOList.size(); j++) {
																FilesDTO filesDTO = FilesDTOList.get(j);
																byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
													%>
														<td>
															<%
																if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
															%>
															<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
																 style='width:100px'/>
															<%
																}
															%>
															<a href='Employee_education_infoServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
															   download><%=filesDTO.fileTitle%>
															</a>
														</td>
														<%
																}
															}%>
													</tr>
												</table>
												<%}%>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>