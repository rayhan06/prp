<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="employee_education_info.*" %>
<%@ page import="java.util.List" %>
<%@ page import="common.RoleEnum" %>
<%@page pageEncoding="UTF-8" %>
<%
    String navigator2 = "";
    String servletName = "Employee_education_infoServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>

<%
    boolean isAdmin = userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId() || userDTO.roleID == RoleEnum.ADMIN.getRoleId();
    List<Employee_education_infoDTO> data = (List<Employee_education_infoDTO>) rn2.list;
    if (data != null && data.size() > 0) {
%>
<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <%
                if (isAdmin) {
            %>
            <th><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_EMPLOYEERECORDSID, loginDTO)%>
            </th>
            <%
                }
            %>
            <th><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_EDUCATIONLEVELTYPE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_DEGREEEXAMTYPE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_RESULTEXAMTYPE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_YEAROFPASSINGNUMBER, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_BOARDTYPE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_INSTITUTIONNAME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_SEARCH_EMPLOYEE_EDUCATION_INFO_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="text-center">
                <span><%="English".equalsIgnoreCase(Language) ? "All" : "সকল"%></span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            for (Employee_education_infoDTO employee_education_infoDTO : data) {
        %>
        <tr>
            <%@include file="employee_education_infoSearchRow.jsp" %>

        </tr>
        <% } %>
        </tbody>
    </table>
</div>
<%
} else {
%>
<label style="width: 100%;text-align: center;font-size: larger;font-weight: bold;color: red">
    <%=isLanguageEnglish ? "No information is found" : "কোন তথ্য পাওয়া যায় নি"%>
</label>
<%
    }
%>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>