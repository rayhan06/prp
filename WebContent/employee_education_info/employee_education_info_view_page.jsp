<%@ page import="employee_education_info.EmployeeEducationInfoDTOWithValue" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="university_info.University_infoDTO" %>
<%@ page import="java.util.Map" %>
<%@ page import="university_info.University_infoDAO" %>
<%@page contentType="text/html;charset=utf-8" %>

<input type="hidden" data-ajax-marker="true">

<table class="table table-striped table-bordered" style="font-size: 14px">
    <thead>
    <tr>
        <th style="text-align: left"><b><%= LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_EDUCATIONLEVELTYPE, loginDTO) %>
        </b></th>
        <th style="text-align: left"><b><%= LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_DEGREEEXAMTYPE, loginDTO) %>
        </b></th>
        <th style="text-align: left"><b><%= LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_MAJOR, loginDTO) %>
        </b></th>
        <th style="text-align: left"><b><%=isLanguageEnglish?"Board/University":"বোর্ড / বিশ্ববিদ্যালয়"%>
        </b></th>
        <th style="text-align: left"><b><%= LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_INSTITUTIONNAME, loginDTO) %>
        </b></th>
        <th style="text-align: left"><b><%= LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_YEAROFPASSINGNUMBER, loginDTO) %>
        </b></th>
        <th><b></b></th>
    </tr>
    </thead>
    <tbody>
    <%
        List<EmployeeEducationInfoDTOWithValue> savedEductionInfoList = (List<EmployeeEducationInfoDTOWithValue>) request.getAttribute("savedEductionInfoList");
        int x = 0;
        if (savedEductionInfoList != null && savedEductionInfoList.size() > 0) {
            List<Long> universityIds = savedEductionInfoList.stream()
                    .filter(e->e.dto.universityInfoType>0 || e.dto.universityInfoType == -100)
                    .map(e->e.dto.universityInfoType)
                    .collect(Collectors.toList());
            Map<Long, University_infoDTO> mapUniversityDTOById = new University_infoDAO().getByIds(universityIds)
                    .stream()
                    .collect(Collectors.toMap(e->e.iD,e->e));
            for (EmployeeEducationInfoDTOWithValue eduInfoItem : savedEductionInfoList) {
                ++x;
    %>
    <tr>
        <td style="text-align: left"><%=isLanguageEnglish ? eduInfoItem.educationLevelEng : eduInfoItem.educationLevelBan%></td>
        <td style="text-align: left"><%= eduInfoItem.degreeExamEng.equals("Other") ? eduInfoItem.dto.otherDegreeExam : isLanguageEnglish ? eduInfoItem.degreeExamEng : eduInfoItem.degreeExamBan%></td>
        <td style="text-align: left"><%= eduInfoItem.dto.major %></td>
        <%
            University_infoDTO university_infoDTO = mapUniversityDTOById.get(eduInfoItem.dto.universityInfoType);
        %>
        <td style="text-align: left"><%=university_infoDTO == null ?(isLanguageEnglish ? eduInfoItem.boardEng : eduInfoItem.boardBan):
                (isLanguageEnglish?university_infoDTO.universityNameEn:university_infoDTO.universityNameBn)%>
        </td>
        <td style="text-align: left"><%= eduInfoItem.dto.institutionName %></td>
        <td style="text-align: left">
            <%if(eduInfoItem.dto.yearOfPassingNumber > 0){%>
                <%=isLanguageEnglish ? eduInfoItem.dto.yearOfPassingNumber : eduInfoItem.passingYearBan%>
            <%}%>
        </td>

        <td style="text-align: center; vertical-align: middle;">
            <form action="Employee_education_infoServlet?isPermanentTable=true&actionType=delete&data=education&tab=2&ID=<%=eduInfoItem.dto.iD%>&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>" method="POST" id="tableForm_edu<%=x%>" enctype = "multipart/form-data">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button style="max-height: 30px" type="button" class="btn-primary" title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_VIEW_DETAILS, loginDTO) %>"
                            onclick="location.href='Employee_education_infoServlet?actionType=view&ID=<%=eduInfoItem.dto.iD%>&empId=<%=eduInfoItem.dto.employeeRecordsId%>&userId=<%=request.getParameter("userId")%>'">
                        <i class="fa fa-eye"></i>&nbsp;
                    </button>&nbsp;
                    <button style="max-height: 30px" type="button" class="btn-success" title="Edit"
                            onclick="location.href='<%=request.getContextPath()%>/Employee_education_infoServlet?actionType=getEditPage&data=education&tab=2&ID=<%=eduInfoItem.dto.iD%>&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'"><i class="fa fa-edit"></i></button>&nbsp;
                    <button style="max-height: 30px" type="button" class="btn-danger" title="Delete" onclick="deleteItem('tableForm_edu',<%=x%>)"><i class="fa fa-trash"></i></button>
                </div>
            </form>
        </td>
    </tr>
    <%
            }
        }
    %>
    </tbody>
</table>


<div class="row">
    <div class="col-12 text-right mt-3">
        <button class="btn btn-gray m-t-10 rounded-pill"
                onclick="location.href='<%=request.getContextPath()%>/Employee_education_infoServlet?actionType=getAddPage&tab=2&data=education&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
            <i class="fa fa-plus"></i>&nbsp;<%= LM.getText(LC.HR_MANAGEMENT_EDUCATION_AND_LINGO_ADD_EDUCATION, loginDTO) %>
        </button>
    </div>
</div>

