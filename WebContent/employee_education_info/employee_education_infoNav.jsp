<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="education_level.Education_levelRepository" %>
<%@ page import="result_exam.Result_examRepository" %>
<%@ page import="board.BoardRepository" %>
<%@page contentType="text/html;charset=utf-8" %>

<%
    String url = "Employee_education_infoServlet?actionType=search";
%>
<%@include file="../pb/navInitializer.jsp" %>
<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"
                               for="education_level_type"><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_SEARCH_EDUCATIONLEVELTYPE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='education_level_type' id='education_level_type'
                                    onchange="educationLevelChange()" onSelect='setSearchChanged()' style="width: 100%">
                                <%=Education_levelRepository.getInstance().buildOptions(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"
                               for="degree_exam_type"><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_SEARCH_DEGREEEXAMTYPE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='degree_exam_type' id='degree_exam_type'
                                    onSelect='setSearchChanged()' style="width: 100%">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"
                               for="result_exam_type"><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_SEARCH_RESULTEXAMTYPE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='result_exam_type' id='result_exam_type'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=Result_examRepository.getInstance().buildOptions(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"
                               for="board_type"><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_SEARCH_BOARDTYPE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='board_type' id='board_type'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=BoardRepository.getInstance().buildOptions(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->

<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>

<script type="text/javascript">
    $(document).ready(() => {
        select2SingleSelector('#education_level_type', '<%=Language%>');
        select2SingleSelector('#degree_exam_type', '<%=Language%>');
        select2SingleSelector('#result_exam_type', '<%=Language%>');
        select2SingleSelector('#board_type', '<%=Language%>');
    });

    function educationLevelChange() {
        console.log($("#education_level_type").val());
        document.getElementById("degree_exam_type").innerHTML = '';
        let educationLevelId = $('#education_level_type').val();
        if (!educationLevelId) {
            return;
        }
        let url = "option_Servlet?actionType=ajax_type&type=degree&education_level_id=" + educationLevelId;
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                console.log(fetchedData);
                document.getElementById("degree_exam_type").innerHTML = fetchedData;
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=true&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        var params = 'AnyField=' + document.getElementById('anyfield').value;

        if ($("#education_level_type").val()) {
            params += '&education_level_type=' + $("#education_level_type").val();
        }
        if ($("#degree_exam_type").val()) {
            params += '&degree_exam_type=' + $("#degree_exam_type").val();
        }
        if ($("#result_exam_type").val()) {
            params += '&result_exam_type=' + $("#result_exam_type").val();
        }
        if ($("#board_type").val()) {
            params += '&board_type=' + $("#board_type").val();
        }

        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

</script>

