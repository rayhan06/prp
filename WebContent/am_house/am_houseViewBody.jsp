

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="am_house.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>
<%@ page import="vm_requisition.CommonApprovalStatus" %>
<%@ page import="common.BaseServlet" %>


<%
    String servletName = "Am_houseServlet";
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }

    long id = Long.parseLong(ID);
    Am_houseDTO am_houseDTO = (Am_houseDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);

%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.AM_HOUSE_ADD_AM_HOUSE_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-md-10 offset-md-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.AM_HOUSE_ADD_AM_HOUSE_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>


			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_HOUSE_ADD_AMHOUSEOLDNEWCAT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">

											<%
											String value = CatRepository.getInstance().getText(Language, "am_house_old_new", am_houseDTO.amHouseOldNewCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_HOUSE_ADD_AMHOUSELOCATIONCAT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = CatRepository.getInstance().getText(Language, "am_house_location", am_houseDTO.amHouseLocationCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_HOUSE_ADD_AMHOUSECLASSCAT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = CatRepository.getInstance().getText(Language, "am_house_class", am_houseDTO.amHouseClassCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_HOUSE_ADD_HOUSENUMBER, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = am_houseDTO.houseNumber + "";
											%>
				
											<%=value%>
				
			
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=UtilCharacter.getDataByLanguage(Language, "অবস্থা", "Status")%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = CommonApprovalStatus.getText
                                                    (am_houseDTO.status, Language);
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <% if(am_houseDTO.status == CommonApprovalStatus.AVAILABLE.getValue()){ %>

                                    <div class="row  ">
                                        <div class="col-12 px-0 text-right mb-3">
                                            <button id="submit-btn"
                                                    class="btn-sm shadow text-white border-0 submit-btn"
                                                    onclick="makeNotAvailable()">
                                                <%=UtilCharacter.getDataByLanguage(Language,
                                                        "ব্যবহারযোগ্য নয়রূপে পরিবর্তন করুন", "Make Not Available")%>
                                            </button>

                                        </div>
                                    </div>

                                <% } %>
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>

<script>


    function notAvailable(remarks){
        $.ajax({
            type : "POST",
            url : "Am_houseServlet?actionType=setNotAvailable&remarks=" + remarks + "&houseId=" + <%=am_houseDTO.iD%>,
            dataType : 'JSON',
            success : function(response) {
                if(response.responseCode === 0){
                    $('#toast_message').css('background-color','#ff6063');
                    showToastSticky(response.msg,response.msg);
                }else if(response.responseCode === 200){
                    window.location.replace(getContextPath()+response.msg);
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
            }
        });
    }

    function makeNotAvailable(){
        event.preventDefault();
        let msg = '<%=UtilCharacter.getDataByLanguage(Language, "ব্যবহারযোগ্য নয়রূপে পরিবর্তন করুন", "Make Not Available")%>';
        let placeHolder = '<%=UtilCharacter.getDataByLanguage(Language, "ব্যবহারযোগ্য নয় করার কারণ লিখুন", "Write reason for Not Available")%>';
        let confirmButtonText = '<%=UtilCharacter.getDataByLanguage(Language, "ব্যবহারযোগ্য নয় করুন", "Make Not Available")%>';
        let cancelButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARD_APPROVAL_MAPPING_CANCEL_BUTTON, loginDTO)%>';
        dialogMessageWithTextBoxWithoutAnimation(msg,placeHolder,confirmButtonText,cancelButtonText,"",(reason)=>{
            notAvailable(reason);
        },()=>{});
    }
</script>