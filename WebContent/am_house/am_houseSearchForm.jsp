
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="am_house.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>
<%@page pageEncoding="UTF-8" %>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="java.util.List" %>
<%@ page import="am_house_type_sub_category.Am_house_type_sub_categoryRepository" %>
<%@ page import="am_house_type_sub_category.Am_house_type_sub_categoryDTO" %>


<%
	LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
	String value = "";
	String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
	UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;

	String navigator2 = "navAM_HOUSE";
	String servletName = "Am_houseServlet";

	RecordNavigator rn2 = (RecordNavigator) request.getAttribute("recordNavigator");
	System.out.println("rn2 = " + rn2);
	String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
	String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
	String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
	String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
	String tableName = rn2.m_tableName;

	String ajax = request.getParameter("ajax");
	boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped text-nowrap">
						<thead>
							<tr>
								<th><%=LM.getText(LC.AM_HOUSE_ADD_AMHOUSEOLDNEWCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ADD_AMHOUSELOCATIONCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ADD_AMHOUSECLASSCAT, loginDTO)%></th>
								<th><%=UtilCharacter.getDataByLanguage(Language, "বাসার উপশ্রেণী", "House Sub Class")%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ADD_HOUSENUMBER, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
								<th><%=LM.getText(LC.AM_HOUSE_SEARCH_AM_HOUSE_EDIT_BUTTON, loginDTO)%></th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								List<Am_houseDTO> data =  rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Am_houseDTO am_houseDTO = data.get(i);
																																
											
											%>
											<tr>
								
		
											<td>
											<%
											value = CatRepository.getInstance().getText(Language, "am_house_old_new", am_houseDTO.amHouseOldNewCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = CatRepository.getInstance().getText(Language, "am_house_location", am_houseDTO.amHouseLocationCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = CatRepository.getInstance().getText(Language, "am_house_class", am_houseDTO.amHouseClassCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>

											<td>
											<%
												Am_house_type_sub_categoryDTO dto = Am_house_type_sub_categoryRepository.getInstance()
													.getAm_house_type_sub_categoryDTOByiD(am_houseDTO.amHouseClassSubCat);
												value = dto == null ? "" :
														Language.equalsIgnoreCase("English") ?
																dto.amHouseSubCatNameEn : dto.amHouseSubCatNameBn;
											%>

											<%=value%>
											</td>
		
											<td>
											<%
											value = am_houseDTO.houseNumber + "";
											%>
				
											<%=value%>
				
			
											</td>
		
		
		
		
		
		
		
		
	
											<%CommonDTO commonDTO = am_houseDTO; %>
											<%@include file="../pb/searchAndViewButton.jsp"%>											
																						
											<td class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=am_houseDTO.iD%>'/></span>
												</div>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=true%>" />


			