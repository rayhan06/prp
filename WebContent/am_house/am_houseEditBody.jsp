<%@page import="login.LoginDTO" %>
<%@page import="am_house.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="am_house_type_sub_category.Am_house_type_sub_categoryRepository" %>

<%
    String servletName = "Am_houseServlet";
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    String actionName;
    Am_houseDTO am_houseDTO;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        am_houseDTO = (Am_houseDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    } else {
        actionName = "add";
        am_houseDTO = new Am_houseDTO();
    }
    String formTitle = LM.getText(LC.AM_HOUSE_ADD_AM_HOUSE_ADD_FORMNAME, loginDTO);
    int i = 0;

%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-8 offset-md-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=am_houseDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ADD_AMHOUSEOLDNEWCAT, loginDTO)%><span>*</span>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='amHouseOldNewCat'
                                                    id='amHouseOldNewCat_category_<%=i%>' tag='pb_html'>
                                                <%
                                                    String Options = CatRepository.getInstance().buildOptions("am_house_old_new", Language, am_houseDTO.amHouseOldNewCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ADD_AMHOUSELOCATIONCAT, loginDTO)%><span>*</span>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='amHouseLocationCat'
                                                    id='amHouseLocationCat_category_<%=i%>' tag='pb_html'>
                                                <%
                                                    Options = CatRepository.getInstance().buildOptions("am_house_location", Language, am_houseDTO.amHouseLocationCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ADD_AMHOUSECLASSCAT, loginDTO)%><span>*</span>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='amHouseClassCat' onchange="loadHouseSubClass()"
                                                    id='amHouseClassCat_category_<%=i%>' tag='pb_html'>
                                                <%
                                                    Options = CatRepository.getInstance().buildOptions("am_house_class", Language, am_houseDTO.amHouseClassCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=UtilCharacter.getDataByLanguage(Language, "বাসার উপশ্রেণী", "House Sub Class")%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='amHouseClassSubCat'
                                                    id='amHouseClassSubCat_category_<%=i%>' tag='pb_html'>
                                                <%
                                                    int defaultValue = -1;
                                                    if ("edit".equals(request.getParameter("actionType"))) {
                                                        defaultValue = am_houseDTO.amHouseClassSubCat;
                                                    }
                                                    Options = Am_house_type_sub_categoryRepository.getInstance().buildOptionsByHouseClass(Language,
                                                            am_houseDTO.amHouseClassCat, defaultValue);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ADD_HOUSENUMBER, loginDTO)%><span>*</span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='houseNumber'
                                                   id='houseNumber_text_<%=i%>' value='<%=am_houseDTO.houseNumber%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>' value='<%=am_houseDTO.insertedByUserId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=am_houseDTO.insertedByOrganogramId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='modifiedBy'
                                           id='modifiedBy_hidden_<%=i%>' value='<%=am_houseDTO.modifiedBy%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>' value='<%=am_houseDTO.insertionDate%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>' value='<%=am_houseDTO.searchColumn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>' value='<%=am_houseDTO.isDeleted%>'
                                           tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=am_houseDTO.lastModificationTime%>' tag='pb_html'/>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.AM_HOUSE_ADD_AM_HOUSE_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="submit" onclick="submitForm()">
                                <%=LM.getText(LC.AM_HOUSE_ADD_AM_HOUSE_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    const houseForm = $("#bigform");

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function PreprocessBeforeSubmiting() {
        houseForm.validate();
        return houseForm.valid();
    }

    function submitForm() {
        buttonStateChange(true);
        if (PreprocessBeforeSubmiting()) {
            $.ajax({
                type: "POST",
                url: "Am_houseServlet?actionType=ajax_<%=actionName%>",
                data: houseForm.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        showToastSticky("বাসা সেভ করা হয়েছে", "House saved");
                        setTimeout(() => {
                            window.location.replace(getContextPath() + response.msg);
                        }, 3000);

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        } else {
            buttonStateChange(false);
        }
    }

    function loadHouseSubClass(){
        let $house = $("#houseId");
        let $houseSubClass = $("#amHouseClassSubCat_category_<%=i%>")
        let houseClass = document.getElementById('amHouseClassCat_category_0').value;
        if (houseClass === '') {
            $house.html("");
            return;
        }

        let url = "Am_house_type_sub_categoryServlet?actionType=getHouseSubClass&houseClass=" + houseClass;
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                const response = JSON.parse(fetchedData);
                if (response && response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                } else if (response && response.responseCode === 200) {
                    $houseSubClass.html(response.msg);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
            }
        });
    }

    function convertToSelect2(){
        select2SingleSelector("#amHouseOldNewCat_category_<%=i%>", '<%=Language%>>');
        select2SingleSelector("#amHouseLocationCat_category_<%=i%>", '<%=Language%>>');
        select2SingleSelector("#amHouseClassCat_category_<%=i%>", '<%=Language%>>');
        select2SingleSelector("#amHouseClassSubCat_category_<%=i%>", '<%=Language%>>');
    }

    $(document).ready(function () {
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        $.validator.addMethod('validSelector', function (value, element) {
            return value && value !== -1 && value.toString().trim().length > 0;
        });

        let lang = '<%=Language%>';
        let divErr, locErr, classErr, houseNumberErr;
        if (lang.toUpperCase() === 'ENGLISH') {
            houseNumberErr = 'House Number Required';
            divErr = 'New House/ Old House Required';
            locErr = 'House Location Required';
            classErr = 'House Class Required';
        } else {
            houseNumberErr = 'বাসার নম্বর প্রয়োজনীয়';
            divErr = 'নতুন বাসা/পুরাতন বাসা প্রয়োজনীয়';
            locErr = 'বাসার অবস্থান প্রয়োজনীয়';
            classErr = 'বাসার শ্রেণী প্রয়োজনীয়';
        }

        houseForm.validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                houseNumber: {
                    required: true,
                },
                amHouseOldNewCat: {
                    validSelector: true,
                },
                amHouseLocationCat: {
                    validSelector: true,
                },
                amHouseClassCat: {
                    validSelector: true,
                },
            },
            messages: {
                houseNumber: houseNumberErr,
                amHouseOldNewCat: divErr,
                amHouseLocationCat: locErr,
                amHouseClassCat: classErr,
            }
        });

        convertToSelect2();
    });


</script>






