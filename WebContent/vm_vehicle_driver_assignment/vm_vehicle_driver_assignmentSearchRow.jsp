<%@page pageEncoding="UTF-8" %>

<%@page import="vm_vehicle_driver_assignment.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="io.jsonwebtoken.lang.Strings" %>
<%@ page import="java.util.stream.Collectors" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_VM_VEHICLE_DRIVER_ASSIGNMENT;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Vm_vehicle_driver_assignmentDTO vm_vehicle_driver_assignmentDTO = (Vm_vehicle_driver_assignmentDTO) request.getAttribute("vm_vehicle_driver_assignmentDTO");
    CommonDTO commonDTO = vm_vehicle_driver_assignmentDTO;
    String servletName = "Vm_vehicle_driver_assignmentServlet";


    System.out.println("vm_vehicle_driver_assignmentDTO = " + vm_vehicle_driver_assignmentDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Vm_vehicle_driver_assignmentDAO vm_vehicle_driver_assignmentDAO = (Vm_vehicle_driver_assignmentDAO) request.getAttribute("vm_vehicle_driver_assignmentDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td id='<%=i%>_vehicleTypeCat'>
    <%
        value = vm_vehicle_driver_assignmentDTO.vehicleTypeCat + "";
    %>
    <%
        //if(vm_vehicle_driver_assignmentDTO.vehicleTypeCatMultiple.contains(",")) {
            try {
                String[] types = vm_vehicle_driver_assignmentDTO.vehicleTypeCatMultiple.split(",");
                value = Arrays.stream(types)
                        .map(type -> CatRepository.getName(Language, "vehicle_type", Integer.parseInt(type.trim())))
                        .collect(Collectors.joining(", "));
            } catch (Exception e){
                System.out.println(e);
            }
        /*}
        else {
            value = CatRepository.getName(Language, "vehicle_type", vm_vehicle_driver_assignmentDTO.vehicleTypeCat);
        }*/
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_vehicleId'>
    <%
        String[] vehicles = vm_vehicle_driver_assignmentDTO.vehicleIdMultiple.split(",");
        List<Vm_vehicleDTO> vm_vehicleDTOs = Arrays.stream(vehicles)
                .map(vehicle -> Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(Long.parseLong(vehicle)))
                .collect(Collectors.toList());

        value = "";
        if (!vm_vehicleDTOs.isEmpty()) {
            value = vm_vehicleDTOs.stream().map(dto->dto.regNo)
                    .collect(Collectors.joining(", "));
        } %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_driverId'>
    <%
        //											value = vm_vehicle_driver_assignmentDTO.driverId + "";

    %>

    <%=UtilCharacter.getDataByLanguage(Language, vm_vehicle_driver_assignmentDTO.employeeRecordNameBn,
            vm_vehicle_driver_assignmentDTO.employeeRecordName)%>

</td>

<%--											<td id = '<%=i%>_employeeRecordName'>--%>
<%--											<%--%>
<%--											value = vm_vehicle_driver_assignmentDTO.employeeRecordName + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_employeeRecordNameBn'>--%>
<%--											<%--%>
<%--											value = vm_vehicle_driver_assignmentDTO.employeeRecordNameBn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_unitName'>--%>
<%--											<%--%>
<%--											value = vm_vehicle_driver_assignmentDTO.unitName + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_unitNameBn'>--%>
<%--											<%--%>
<%--											value = vm_vehicle_driver_assignmentDTO.unitNameBn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_postName'>--%>
<%--											<%--%>
<%--											value = vm_vehicle_driver_assignmentDTO.postName + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_postNameBn'>--%>
<%--											<%--%>
<%--											value = vm_vehicle_driver_assignmentDTO.postNameBn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_remarks'>--%>
<%--											<%--%>
<%--											value = vm_vehicle_driver_assignmentDTO.remarks + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="view('<%=vm_vehicle_driver_assignmentDTO.iD%>')">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="edit('<%=vm_vehicle_driver_assignmentDTO.iD%>')">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=vm_vehicle_driver_assignmentDTO.iD%>'/></span>
    </div>
</td>
																						
											

