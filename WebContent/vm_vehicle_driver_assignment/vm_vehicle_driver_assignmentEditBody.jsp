<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_vehicle_driver_assignment.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="employee_assign.EmployeeSearchIds" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="util.UtilCharacter" %>

<%
    Vm_vehicle_driver_assignmentDTO vm_vehicle_driver_assignmentDTO;
    vm_vehicle_driver_assignmentDTO = (Vm_vehicle_driver_assignmentDTO) request.getAttribute("vm_vehicle_driver_assignmentDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (vm_vehicle_driver_assignmentDTO == null) {
        vm_vehicle_driver_assignmentDTO = new Vm_vehicle_driver_assignmentDTO();

    }
    System.out.println("vm_vehicle_driver_assignmentDTO = " + vm_vehicle_driver_assignmentDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_ADD_VM_VEHICLE_DRIVER_ASSIGNMENT_ADD_FORMNAME, loginDTO);
    String servletName = "Vm_vehicle_driver_assignmentServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");


    int year = Calendar.getInstance().get(Calendar.YEAR);

%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Vm_vehicle_driver_assignmentServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="formSubmit()">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="sub_title_top">
                                                <div class="sub_title">
                                                    <h4 style="background: white"><%=formTitle%>
                                                    </h4>
                                                </div>
                                            </div>


                                            <input type='hidden' class='form-control' name='iD'
                                                   id='iD_hidden_<%=i%>'
                                                   value='<%=vm_vehicle_driver_assignmentDTO.iD%>'
                                                   tag='pb_html'/>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_ADD_VEHICLETYPECAT, loginDTO)%>

                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <select class='form-control' name='vehicleTypeCat'
                                                            id='vehicleTypeCat_category_<%=i%>' tag='pb_html'
                                                            onchange="loadVehicleList()">
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group row" id="vehicleIdDiv">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_ADD_VEHICLEID, loginDTO)%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <select class='form-control' name='vehicleId'
                                                            id='givenVehicleId_select_<%=i%>' tag='pb_html'>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=UtilCharacter.getDataByLanguage(Language, "শুরুর তারিখ", "Start Date")%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <jsp:include page="/date/date.jsp">
                                                        <jsp:param name="DATE_ID"
                                                                   value="start-date-js"></jsp:param>
                                                        <jsp:param name="LANGUAGE"
                                                                   value="<%=Language%>"></jsp:param>
                                                        <jsp:param name="END_YEAR" value="<%=year + 20%>"/>
                                                    </jsp:include>

                                                    <input type='hidden' class='form-control'
                                                           id='startDate_<%=i%>'
                                                           name='startDate' value='' tag='pb_html'/>

                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=UtilCharacter.getDataByLanguage(Language, "শেষের তারিখ", "End Date")%>
                                                </label>
                                                <div class="col-md-9">
                                                    <jsp:include page="/date/date.jsp">
                                                        <jsp:param name="DATE_ID"
                                                                   value="end-date-js"></jsp:param>
                                                        <jsp:param name="LANGUAGE"
                                                                   value="<%=Language%>"></jsp:param>
                                                        <jsp:param name="END_YEAR" value="<%=year + 20%>"/>
                                                    </jsp:include>

                                                    <input type='hidden' class='form-control'
                                                           id='endDate_<%=i%>'
                                                           name='endDate' value='' tag='pb_html'/>

                                                </div>
                                            </div>


                                            <%--														<input type='hidden' class='form-control'  name='vehicleId' id = 'vehicleId_hidden_<%=i%>' value='<%=vm_vehicle_driver_assignmentDTO.vehicleId%>' tag='pb_html'/>--%>
                                            <%--														<input type='hidden' class='form-control'  name='driverId' id = 'driverId_hidden_<%=i%>' value='<%=vm_vehicle_driver_assignmentDTO.driverId%>' tag='pb_html'/>--%>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_ADD_REMARKS, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type='text' class='form-control' name='remarks'
                                                           id='remarks_text_<%=i%>'
                                                           value='<%=vm_vehicle_driver_assignmentDTO.remarks%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_ADD_DRIVERID, loginDTO)%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type="hidden" class='form-control' name='driverId'
                                                           id='driverId'
                                                           value=''>
                                                    <button type="button"
                                                            class="btn btn-primary btn-block shadow btn-border-radius mb-3"
                                                            id="tagDriver_modal_button">
                                                        <%=LM.getText(LC.HM_SELECT, loginDTO)%>
                                                    </button>
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped text-nowrap">
                                                            <thead></thead>

                                                            <tbody id="tagged_driver_table">
                                                            <tr style="display: none;">
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <button type="button"
                                                                            class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4"
                                                                            style="padding-right: 14px"
                                                                            onclick="remove_containing_row(this,'tagged_driver_table');">
                                                                        <i class="fa fa-trash"></i>
                                                                    </button>
                                                                </td>
                                                            </tr>

                                                            <% if (actionName.equals("edit") && vm_vehicle_driver_assignmentDTO.driverId != -1) { %>
                                                            <tr>
                                                                <td><%=Employee_recordsRepository.getInstance()
                                                                        .getById(vm_vehicle_driver_assignmentDTO.driverId).employeeNumber%>
                                                                </td>
                                                                <td>
                                                                    <%=isLanguageEnglish ? (vm_vehicle_driver_assignmentDTO.employeeRecordName)
                                                                            : (vm_vehicle_driver_assignmentDTO.employeeRecordNameBn)%>
                                                                </td>

                                                                <%
                                                                    String postName = isLanguageEnglish ? (vm_vehicle_driver_assignmentDTO.postName + ", " + vm_vehicle_driver_assignmentDTO.unitName)
                                                                            : (vm_vehicle_driver_assignmentDTO.postNameBn + ", " + vm_vehicle_driver_assignmentDTO.unitNameBn);
                                                                %>

                                                                <td><%=postName%>
                                                                </td>
                                                                <td id='<%=vm_vehicle_driver_assignmentDTO.driverId%>_td_button'>
                                                                    <button type="button"
                                                                            class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4"
                                                                            style="padding-right: 14px"
                                                                            onclick="remove_containing_row(this,'tagged_driver_table');">
                                                                        <i class="fa fa-trash"></i>
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                            <%}%>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>


                                            <input type='hidden' class='form-control' name='insertedByUserId'
                                                   id='insertedByUserId_hidden_<%=i%>'
                                                   value='<%=vm_vehicle_driver_assignmentDTO.insertedByUserId%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control'
                                                   name='insertedByOrganogramId'
                                                   id='insertedByOrganogramId_hidden_<%=i%>'
                                                   value='<%=vm_vehicle_driver_assignmentDTO.insertedByOrganogramId%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertionDate'
                                                   id='insertionDate_hidden_<%=i%>'
                                                   value='<%=vm_vehicle_driver_assignmentDTO.insertionDate%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='isDeleted'
                                                   id='isDeleted_hidden_<%=i%>'
                                                   value='<%=vm_vehicle_driver_assignmentDTO.isDeleted%>'
                                                   tag='pb_html'/>

                                            <input type='hidden' class='form-control'
                                                   name='lastModificationTime'
                                                   id='lastModificationTime_hidden_<%=i%>'
                                                   value='<%=vm_vehicle_driver_assignmentDTO.lastModificationTime%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='searchColumn'
                                                   id='searchColumn_hidden_<%=i%>'
                                                   value='<%=vm_vehicle_driver_assignmentDTO.searchColumn%>'
                                                   tag='pb_html'/>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 form-actions text-right mt-3">
                        <a id="cancel-btn" class="btn btn-sm cancel-btn text-white shadow btn-border-radius"
                           href="<%=request.getHeader("referer")%>">
                            <%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_ADD_VM_VEHICLE_DRIVER_ASSIGNMENT_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button id="submit-btn" class="btn btn-sm submit-btn text-white shadow btn-border-radius ml-2"
                                type="submit"><%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_ADD_VM_VEHICLE_DRIVER_ASSIGNMENT_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../vm_requisition_pending/employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
    <jsp:param name="ID" value="<%=1%>"/>
    <jsp:param name="all" value="1"/>
</jsp:include>

<script type="text/javascript">
    let selectedVehicleTypeCat;
    let selectedVehicleTypeCatArr = [];
    let selectedVehicle;
    let selectedVehicleArr = [];

    function PreprocessBeforeSubmiting() {
        let data = added_driver_map.keys().next();
        if (!(data && data.value)) {
            toastr.error("Please Select Person");
            return false;
        }
        document.getElementById('driverId').value = JSON.stringify(
            Array.from(added_driver_map.values()));

        $('#startDate_0').val(getDateStringById('start-date-js'));
        $('#endDate_0').val(getDateStringById('end-date-js'));

        return dateValidator('start-date-js', true, {
            errorEn: "শুরুর তারিখ নির্বাচন করুন ",
            errorBn: "Select Start Date",
        });
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Vm_vehicle_driver_assignmentServlet");
    }

    function init(row) {


        if (actionName == 'edit') {
            loadVehicleList();
            setDateByTimestampAndId('start-date-js', <%=vm_vehicle_driver_assignmentDTO.start_time%>);
            setDateByTimestampAndId('end-date-js', <%=vm_vehicle_driver_assignmentDTO.end_time%>);
        } else {
            setDateByTimestampAndId('start-date-js', <%=System.currentTimeMillis()%>);
            setDateByTimestampAndId('end-date-js', <%=System.currentTimeMillis()%>);
        }

        $('#start-date-js').on('datepicker.change', () => {
            setMinDateById('end-date-js', getDateStringById('start-date-js'));
        });


    }

    var row = 0;
    let actionName = '<%=actionName%>';
    let vehicleId = '<%=vm_vehicle_driver_assignmentDTO.vehicleId%>';
    let language = '<%=Language%>';
    $(document).ready(function () {
        init(row);

        select2MultiSelector("#vehicleTypeCat_category_<%=i%>", '<%=Language%>');
        select2MultiSelector("#givenVehicleId_select_<%=i%>", '<%=Language%>');
        <% if(actionName.equalsIgnoreCase("edit")){ %>

        selectedVehicleTypeCat = '<%=vm_vehicle_driver_assignmentDTO.vehicleTypeCatMultiple%>';
        selectedVehicleTypeCatArr = selectedVehicleTypeCat.split(',').map(function (item) {
            return item.trim();
        });

        selectedVehicle = '<%=vm_vehicle_driver_assignmentDTO.vehicleIdMultiple%>';
        selectedVehicleArr = selectedVehicle.split(',').map(function (item) {
            return item.trim();
        });
        <% }%>
        generateVehicleTypeOptions(selectedVehicleTypeCatArr);
        loadVehicleList(selectedVehicleArr);

        $.validator.addMethod('validSelector', function (value, element) {
            return value && value != -1;
        });


        let errVehicleTypeCat;
        let errVehicle;

        if (language == 'english') {
            errVehicleTypeCat = 'Please select vehicle type';
            errVehicle = 'Please select vehicle';


        } else {
            errVehicleTypeCat = 'যানবাহনের ধরণ নির্বাচন করুন ';
            errVehicle = 'যানবাহন নির্বাচন করুন ';

        }

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                vehicleTypeCat: {
                    required: true,
                    validSelector: true,
                },
                vehicleId: {
                    required: true,
                    validSelector: true,
                },
                // startDate: "required",
            },
            messages: {
                vehicleTypeCat: errVehicleTypeCat,
                vehicleId: errVehicle,

            }
        });
    });

    function  getCommaSeparatedVal(selectedInputs){
        var temp = "";
        if(selectedInputs != null){
            selectedInputs.forEach(function(value, index, array){
                if(index > 0){
                    temp += ", ";
                }
                temp += value;
            });
            return temp;
        }
    }

    function processMultipleSelectBoxBeforeSubmit2(name)
    {

        $( "[name='" + name + "']" ).each(function( i )
        {
            var selectedInputs = $(this).val();
            let temp = getCommaSeparatedVal(selectedInputs);
            if(temp.includes(',')){
                $(this).append('<option value="' + temp + '"></option>');
            }
            $(this).val(temp);

        });
    }

    function generateVehicleTypeOptions(selectedVehicleTypeArr) {
        let url = "Vm_vehicle_driver_assignmentServlet?actionType=getVehicleTypes";
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                $('#vehicleTypeCat_category_<%=i%>').html("");
                let o;
                let str;

                $('#vehicleTypeCat_category_<%=i%>').append(o);
                const response = JSON.parse(fetchedData);
                if (response && response.length > 0) {
                    for (let x in response) {
                        if (language === 'English') {
                            str = response[x].englishText;
                        } else {
                            str = response[x].banglaText;
                        }

                        if (selectedVehicleTypeArr && selectedVehicleTypeArr.length > 0) {
                            if (selectedVehicleTypeArr.includes(response[x].value + '')) {
                                o = new Option(str, response[x].value, false, true);
                            } else {
                                o = new Option(str, response[x].value);
                            }
                        } else {
                            o = new Option(str, response[x].value);
                        }
                        $(o).html(str);
                        $('#vehicleTypeCat_category_<%=i%>').append(o);
                    }
                }

            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function formSubmit() {
        event.preventDefault();
        let isValid = PreprocessBeforeSubmiting() && $("#bigform").valid();
        if (isValid) {
            buttonStateChange(true);
            let form = $("#bigform");
            let url = form.attr('action');
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        }


    }

    function loadVehicleList(selectedVehicleArr) {

        let temp = '';
        $( "[name='vehicleTypeCat']" ).each(function( i )
        {
            temp = $(this).val();
        });
        let typeValue = temp != null ?temp.join(',') : "";
        let url = "Vm_vehicle_driver_assignmentServlet?actionType=getVehicleByType&type=" + typeValue;

        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                $('#givenVehicleId_select_<%=i%>').html("");
                let o;
                let str;

                $('#givenVehicleId_select_<%=i%>').append(o);
                const response = fetchedData !== "" ? JSON.parse(fetchedData) : null;
                if (response && response.length > 0) {
                    for (let x in response) {
                        if (language === 'English') {
                            str = response[x].englishText;
                        } else {
                            str = response[x].banglaText;
                        }
                        if (selectedVehicleArr && selectedVehicleArr.length > 0) {
                            if (selectedVehicleArr.includes(response[x].value + '')) {
                                o = new Option(str, response[x].value, false, true);
                            } else {
                                o = new Option(str, response[x].value);
                            }
                        } else {
                            o = new Option(str, response[x].value);
                        }
                        $(o).html(str);
                        $('#givenVehicleId_select_<%=i%>').append(o);
                    }
                }

            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button

    // map to store and send added employee data
    // map to store and send added employee data


    <%
    List<EmployeeSearchIds> addedEmpIdsList = null;
    if(actionName.equals("edit") && vm_vehicle_driver_assignmentDTO.driverId != -1) {
        addedEmpIdsList = Arrays.asList(new EmployeeSearchIds
        (vm_vehicle_driver_assignmentDTO.driverId ,vm_vehicle_driver_assignmentDTO.unit_id,vm_vehicle_driver_assignmentDTO.post_id));
    }
    %>

    added_driver_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(addedEmpIdsList)%>);


    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_driver_table', {
                info_map: added_driver_map,
                isSingleEntry: true
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagDriver_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_driver_table';
        $('#search_emp_modal').modal();
    });

</script>

<style>
    .required {
        color: red;
    }
</style>






