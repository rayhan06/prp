<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_vehicle_driver_assignment.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="java.util.stream.Collectors" %>


<%
    String servletName = "Vm_vehicle_driver_assignmentServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Vm_vehicle_driver_assignmentDTO vm_vehicle_driver_assignmentDTO = Vm_vehicle_driver_assignmentRepository.getInstance().
            getVm_vehicle_driver_assignmentDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important; margin-bottom: -18px">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_ADD_VM_VEHICLE_DRIVER_ASSIGNMENT_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>

        <div class="kt-portlet__body form-body">
            <div>
                <h5 class="table-title">
                    <%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_ADD_VM_VEHICLE_DRIVER_ASSIGNMENT_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_ADD_VEHICLETYPECAT, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicle_driver_assignmentDTO.vehicleTypeCat + "";
                                %>
                                <%
                                    if(vm_vehicle_driver_assignmentDTO.vehicleTypeCatMultiple.contains(",")) {
                                        try {
                                            String[] types = vm_vehicle_driver_assignmentDTO.vehicleTypeCatMultiple.split(",");
                                            value = Arrays.stream(types)
                                                    .map(type -> CatRepository.getName(Language, "vehicle_type", Integer.parseInt(type)))
                                                    .collect(Collectors.joining(", "));
                                        } catch (Exception e){
                                            System.out.println(e);
                                        }
                                    }
                                    else {
                                        value = CatRepository.getName(Language, "vehicle_type", vm_vehicle_driver_assignmentDTO.vehicleTypeCat);
                                    }
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_ADD_VEHICLEID, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    String[] vehicles = vm_vehicle_driver_assignmentDTO.vehicleIdMultiple.split(",");
                                    List<Vm_vehicleDTO> vm_vehicleDTOs = Arrays.stream(vehicles)
                                            .map(vehicle -> Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(Long.parseLong(vehicle)))
                                            .collect(Collectors.toList());

                                    value = "";
                                    if (vm_vehicleDTOs != null) {
                                        value = vm_vehicleDTOs.stream().map(dto->dto.regNo)
                                                .collect(Collectors.joining(", "));
                                    } %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_ADD_DRIVERID, loginDTO)%>
                                </b></td>
                            <td>

                                <%=UtilCharacter.getDataByLanguage(Language, vm_vehicle_driver_assignmentDTO.employeeRecordNameBn,
                                        vm_vehicle_driver_assignmentDTO.employeeRecordName)%>


                            </td>

                        </tr>

                        <tr>
                            <td style="width:30%">
                                <b><%=UtilCharacter.getDataByLanguage(Language, "শুরুর তারিখ", "Start Date")%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicle_driver_assignmentDTO.start_time + "";
                                %>
                                <%
                                    String formatted_dateOfExam = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_dateOfExam, Language)%>


                            </td>

                        </tr>

                        <tr>
                            <td style="width:30%">
                                <b><%=UtilCharacter.getDataByLanguage(Language, "শেষের তারিখ", "End Date")%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicle_driver_assignmentDTO.end_time + "";
                                %>
                                <%
                                    formatted_dateOfExam = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_dateOfExam, Language)%>


                            </td>

                        </tr>

                        <%--				--%>


                        <%--			--%>

                        <%--							<tr>--%>
                        <%--								<td style="width:30%"><b><%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_ADD_EMPLOYEERECORDNAME, loginDTO)%></b></td>--%>
                        <%--								<td>--%>
                        <%--						--%>
                        <%--											<%--%>
                        <%--											value = vm_vehicle_driver_assignmentDTO.employeeRecordName + "";--%>
                        <%--											%>--%>
                        <%--														--%>
                        <%--											<%=Utils.getDigits(value, Language)%>--%>
                        <%--				--%>
                        <%--			--%>
                        <%--								</td>--%>
                        <%--						--%>
                        <%--							</tr>--%>

                        <%--				--%>


                        <%--			--%>

                        <%--							<tr>--%>
                        <%--								<td style="width:30%"><b><%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_ADD_EMPLOYEERECORDNAMEBN, loginDTO)%></b></td>--%>
                        <%--								<td>--%>
                        <%--						--%>
                        <%--											<%--%>
                        <%--											value = vm_vehicle_driver_assignmentDTO.employeeRecordNameBn + "";--%>
                        <%--											%>--%>
                        <%--														--%>
                        <%--											<%=Utils.getDigits(value, Language)%>--%>
                        <%--				--%>
                        <%--			--%>
                        <%--								</td>--%>
                        <%--						--%>
                        <%--							</tr>--%>

                        <%--				--%>


                        <%--			--%>

                        <%--							<tr>--%>
                        <%--								<td style="width:30%"><b><%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_ADD_UNITNAME, loginDTO)%></b></td>--%>
                        <%--								<td>--%>
                        <%--						--%>
                        <%--											<%--%>
                        <%--											value = vm_vehicle_driver_assignmentDTO.unitName + "";--%>
                        <%--											%>--%>
                        <%--														--%>
                        <%--											<%=Utils.getDigits(value, Language)%>--%>
                        <%--				--%>
                        <%--			--%>
                        <%--								</td>--%>
                        <%--						--%>
                        <%--							</tr>--%>

                        <%--				--%>


                        <%--			--%>

                        <%--							<tr>--%>
                        <%--								<td style="width:30%"><b><%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_ADD_UNITNAMEBN, loginDTO)%></b></td>--%>
                        <%--								<td>--%>
                        <%--						--%>
                        <%--											<%--%>
                        <%--											value = vm_vehicle_driver_assignmentDTO.unitNameBn + "";--%>
                        <%--											%>--%>
                        <%--														--%>
                        <%--											<%=Utils.getDigits(value, Language)%>--%>
                        <%--				--%>
                        <%--			--%>
                        <%--								</td>--%>
                        <%--						--%>
                        <%--							</tr>--%>

                        <%--				--%>


                        <%--			--%>

                        <%--							<tr>--%>
                        <%--								<td style="width:30%"><b><%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_ADD_POSTNAME, loginDTO)%></b></td>--%>
                        <%--								<td>--%>
                        <%--						--%>
                        <%--											<%--%>
                        <%--											value = vm_vehicle_driver_assignmentDTO.postName + "";--%>
                        <%--											%>--%>
                        <%--														--%>
                        <%--											<%=Utils.getDigits(value, Language)%>--%>
                        <%--				--%>
                        <%--			--%>
                        <%--								</td>--%>
                        <%--						--%>
                        <%--							</tr>--%>

                        <%--				--%>


                        <%--			--%>

                        <%--							<tr>--%>
                        <%--								<td style="width:30%"><b><%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_ADD_POSTNAMEBN, loginDTO)%></b></td>--%>
                        <%--								<td>--%>
                        <%--						--%>
                        <%--											<%--%>
                        <%--											value = vm_vehicle_driver_assignmentDTO.postNameBn + "";--%>
                        <%--											%>--%>
                        <%--														--%>
                        <%--											<%=Utils.getDigits(value, Language)%>--%>
                        <%--				--%>
                        <%--			--%>
                        <%--								</td>--%>
                        <%--						--%>
                        <%--							</tr>--%>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_ADD_REMARKS, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicle_driver_assignmentDTO.remarks + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


