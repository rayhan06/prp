<%@page import="office_unit_organogram.*" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="java.util.List" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="java.util.function.Function" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="java.util.Map" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    List<Office_unit_organogramDTO> organogramDTOs = (List<Office_unit_organogramDTO>) request.getAttribute("organogramDTOs");
    if(organogramDTOs == null) organogramDTOs = new ArrayList<>();
    organogramDTOs.sort(Office_unit_organogramDTO.groupSameDesignations);
    Map<Long, Office_unitsDTO> officeDTOsById = Office_unitsRepository.getInstance()
                                                                      .getOffice_unitsList()
                                                                      .stream()
                                                                      .collect(Collectors.toMap(dto -> dto.iD, Function.identity()));
%>
<% if(organogramDTOs.isEmpty()) { %>
<tr>
    <td colspan="100%" class="text-center" style="font-weight: bold;">
        <%=getDataByLanguage(Language, "কোন তথ্য পাওয়া যায়নি!", "No data found!")%>
    </td>
</tr>
<% } %>

<%
    for (Office_unit_organogramDTO organogramDTO : organogramDTOs) {
        Office_unitsDTO officeUnitsDTO = officeDTOsById.getOrDefault(organogramDTO.officeUnitId, new Office_unitsDTO());
%>
<tr>
    <td>
        <%=getDataByLanguage(Language, organogramDTO.designationBng, organogramDTO.designationEng)%>
    </td>
    <td>
        <%=getDataByLanguage(Language, officeUnitsDTO.unitNameBng, officeUnitsDTO.unitNameEng)%>
    </td>
    <td>
        <input type='text' class='form-control' name='layer1_<%=organogramDTO.iD%>'
               data-only-integer="true" id='layer1_<%=organogramDTO.iD%>'
               value='<%=organogramDTO.layer1%>'>
    </td>
    <td>
        <input type='text' class='form-control' name='layer2_<%=organogramDTO.iD%>'
               data-only-integer="true" id='layer2_<%=organogramDTO.iD%>'
               value='<%=organogramDTO.layer2%>'>
    </td>
</tr>
<%}%>