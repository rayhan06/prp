<%@page import="office_unit_organogram.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="employee_assign.EmployeeSearchModel" %>
<%@ page import="designations.DesignationsRepository" %>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    Office_unit_organogramDTO office_unit_organogramDTO;
    if ("ajax_edit".equals(actionName)) {
        office_unit_organogramDTO = (Office_unit_organogramDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    } else {
        office_unit_organogramDTO = new Office_unit_organogramDTO();
    }
    String defaultSelectOption = Utils.buildSelectOption(isLanguageEnglish);
    String formTitle = LM.getText(LC.OFFICE_UNIT_ORGANOGRAM_ADD_OFFICE_UNIT_ORGANOGRAM_ADD_FORMNAME, loginDTO);
    long officeUnitId = Utils.parseOptionalLong(request.getParameter("officeUnitId"), 0L, "");
    System.out.println(officeUnitId);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="designationForm">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"
                                               for="officeUnitId_text_<%=i%>">
                                            <%=LM.getText(LC.HM_OFFICE, loginDTO)%> <span>*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <button type="button"
                                                    class="btn btn-block shadow btn-primary text-white btn-border-radius"
                                                    id="office_units_id_modal_button"
                                                    onclick="officeModalButtonClicked();">
                                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                                            </button>
                                            <div class="input-group" id="office_units_id_div" style="display: none">
                                                <input type="hidden" name='officeUnitId' id='office_units_id_input'
                                                       value="<%=officeUnitId%>">
                                                <button type="button"
                                                        class="btn btn-secondary btn-block shadow-sm btn-border-radius form-control"
                                                        id="office_units_id_text"
                                                        onclick="officeModalEditButtonClicked()">
                                                </button>
                                                <span class="input-group-btn"
                                                      style="width: 5%;padding-right: 5px;height: 40px" tag='pb_html'>
                                                    <button type="button" class="btn btn-outline-danger"
                                                            style="height: 40px;border-radius: 0"
                                                            onclick="crsBtnClicked('office_units_id');"
                                                            id='office_units_id_crs_btn' tag='pb_html'>
                                                        X
                                                    </button>
                                                </span>
                                            </div>
                                            <span id="office_error_id"
                                                  style="display: none;color: #fd397a;text-align: center;margin-left: 12px;margin-top: 1%;font-size: smaller">
                                                <%=isLanguageEnglish ? "Please select office" : "দপ্তর বাছাই করুন"%>
                                            </span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"
                                               for="officeUnitId_text_<%=i%>">
                                            <%=LM.getText(LC.HM_DESIGNATION, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                        	<select  class='form-control' name='designationsId'
                                                    id='designationsId' >
                                                    <%=DesignationsRepository.getInstance().buildOptions(Language,office_unit_organogramDTO.designationsId)%>
                                             </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="superior_designation"
                                               class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.GLOBAL_SUPERIOR_DESIGNATION, loginDTO)%>
                                            <span>*</span></label>
                                        <div class="col-md-9">
                                            <select required class='form-control' name='superiorDesignationId'
                                                    id='superior_designation'>
                                                <%
                                                    if (office_unit_organogramDTO.officeUnitId >= 0) {
                                                %>
                                                <%=OfficeUnitOrganogramsRepository.getInstance().buildOptionsForDesignationWithParentDesignation(Language, office_unit_organogramDTO.superiorDesignationId, office_unit_organogramDTO.officeUnitId)%>
                                                <%
                                                } else {
                                                %>
                                                <%=defaultSelectOption%>
                                                <%
                                                    }
                                                %>
                                            </select>
                                        </div>
                                    </div>
                                                                   
                                    
                                    <div class="form-group row">
                                        <label 
                                               class="col-md-3 col-form-label text-md-right"><%=Language.equalsIgnoreCase("english")?"Virtual Post":"ভার্চুয়াল পদ"%>
                                            </label>
                                        <div class="col-md-9">
                                            
                                            
                                            <input class='form-control-sm' type="checkbox"
                                               
                                                   name='isVirtual' id='isVirtual'
                                                   <%=office_unit_organogramDTO.isVirtual?"checked":""%>
                                            >

                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label  class="col-md-3 col-form-label text-md-right">
                                            <%=isLanguageEnglish? "Substitute" : "বিকল্প"%>
                                        </label>
                                        <div class="col-md-9">
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                                    id="employee_record_id_modal_button"
                                                    onclick="employeeRecordIdModalBtnClicked();">
                                                <%=LM.getText(LC.LANGUAGE_SELECT, userDTO)%>
                                            </button>
                                            <div class="input-group" id="employee_record_id_div" style="display: none">
                                                <input type="hidden" name='substituteOrganogramId' id='employee_record_id_input' value="">
                                                <button type="button" class="btn btn-secondary form-control" disabled
                                                        id="employee_record_id_text"></button>
                                                <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                                                    <button type="button" class="btn btn-outline-danger modalCross"
                                                            onclick="crsBtnClicked('employee_record_id');"
                                                            id='employee_record_id_crs_btn' tag='pb_html'>
                                                        x
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="briefDescription_eng" class="col-md-3 col-form-label text-md-right">
                                            <%=isLanguageEnglish ? "Brief Description(English)" : "সংক্ষিপ্ত বিবরণ(ইংরেজী)"%>
                                        </label>
                                        <div class="col-md-9">
                                            <input class='form-control' maxlength="128"
                                                   placeholder='<%=isLanguageEnglish?"Enter brief description in english":"ইংরেজীতে সংক্ষিপ্ত বিবরণ লিখুন"%>'
                                                   name='briefDescriptionEng' id='briefDescription_eng'
                                                   value='<%=office_unit_organogramDTO.briefDescriptionEng%>'
                                            >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="briefDescription_bng" class="col-md-3 col-form-label text-md-right">
                                            <%=isLanguageEnglish ? "Brief Description(Bangla)" : "সংক্ষিপ্ত বিবরণ(বাংলা)"%>
                                        </label>
                                        <div class="col-md-9">
                                            <input class='form-control' maxlength="512"
                                                   placeholder='<%=isLanguageEnglish?"Enter brief description in bangla":"বাংলায় সংক্ষিপ্ত বিবরণ লিখুন"%>'
                                                   name='briefDescriptionBng' id='briefDescription_bng'
                                                   value='<%=office_unit_organogramDTO.briefDescriptionBng%>'
                                            >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="layer1" class="col-md-3 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "১ম স্তরের ক্রম", "1st Layer Order")%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='layer1'
                                                   data-only-integer="true" id='layer1'
                                                   value='<%=office_unit_organogramDTO.layer1%>'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="layer2" class="col-md-3 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "২য় স্তরের ক্রম", "2nd Layer Order")%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='layer2'
                                                   data-only-integer="true" id='layer2'
                                                   value='<%=office_unit_organogramDTO.layer2%>'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-11">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" type="button" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.OFFICE_UNIT_ORGANOGRAM_ADD_OFFICE_UNIT_ORGANOGRAM_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn" type="button" onclick="submitDesignationForm()"
                                    class="btn-sm shadow text-white border-0 submit-btn ml-2">
                                <%=LM.getText(LC.OFFICE_UNIT_ORGANOGRAM_ADD_OFFICE_UNIT_ORGANOGRAM_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<%
    String modalTitle = Language.equalsIgnoreCase("English") ? "Find Substitute" : "বিকল্প খুঁজুন";
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
    <jsp:param name="modalTitle" value="<%=modalTitle%>"/>
</jsp:include>

<script src="<%=contextOfPath%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<script type="text/javascript">
    const form = $('#designationForm');
    const isLangEng = <%=isLanguageEnglish%>;
    const officeUnitSelector = $('#office_units_id_input');

    function typeOnlyInteger(e) {
        return true === inputValidationForPositiveValue(e, $(this), <%=Integer.MAX_VALUE%>, 0);
    }

    $(document).ready(function () {
        <%
        Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitId);
        %>

        let selectedOffice = {
            id: '',
            name: ''
        }
        <%
        if(office_unitsDTO!=null){
        %>
        selectedOffice.id = <%=office_unitsDTO.iD%>;
        selectedOffice.name = '<%=isLanguageEnglish?office_unitsDTO.unitNameEng:office_unitsDTO.unitNameBng%>';
        viewOfficeIdInInput(selectedOffice);
        <%
        }
        %>

        document.querySelectorAll('[data-only-integer="true"]')
            .forEach(inputField => inputField.onkeydown = typeOnlyInteger);

        select2SingleSelector("#superior_designation", "<%=Language%>");
        select2SingleSelector("#designationsId", "<%=Language%>");
        form.validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                superiorDesignationId: {
                    required: true
                }
            },
            messages: {
                superiorDesignationId: isLangEng ? "Please select superior designation" : "অনুগ্রহ করে ঊর্ধ্বতন পদবী বাছাই করুন",
               
              
            }
        });

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        <%
        if(actionName.equals("ajax_edit")){
                Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance()
                                                                       .getOffice_unitsDTOByID(office_unit_organogramDTO.officeUnitId);
                EmployeeSearchModel employeeSearchModel = EmployeeSearchModalUtil.getEmployeeSearchModelByOrganogramId(
                        office_unit_organogramDTO.substituteOrganogramId
                );
        %>
        const officeUnitModel = {
            name: '<%=UtilCharacter.getDataByLanguage(Language, officeUnitsDTO.unitNameBng, officeUnitsDTO.unitNameEng)%>',
            id: <%=office_unit_organogramDTO.officeUnitId%>
        };
        viewOffice(officeUnitModel);
        <%if(employeeSearchModel != null){%>
        const employeeSearchModel = JSON.parse('<%=employeeSearchModel.getJSON()%>');
        viewEmployeeRecordIdInInput(employeeSearchModel);
        <%}%>
        <%}%>
        

    });

    function submitDesignationForm() {
        let validation = form.valid();
        const officeUnitId = officeUnitSelector.val();
        if (officeUnitId === '' || officeUnitId < 0) {
            $('#office_error_id').show();
            validation = false;
        } else {
            $('#office_error_id').hide();
        }
        if (validation) {
            let data = {
                officeId: officeUnitId,
                superiorDesignationId: $('#superior_designation').val(),
                designationsId: $('#designationsId').val(),
                isVirtual: $('#isVirtual').prop("checked"),
                briefDescriptionEng: $('#briefDescription_eng').val(),
                briefDescriptionBng: $('#briefDescription_bng').val(),

                substituteOrganogramId: $('#employee_record_id_input').val(),
                layer1: $('#layer1').val(),
                layer2: $('#layer2').val()
            };
            submitAjaxByData(data,"Office_unit_organogramServlet?actionType=<%=actionName%>&iD=<%=office_unit_organogramDTO.iD%>");
        }
    }

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
        $('#superior_designation').html("<%=defaultSelectOption%>");
    }

    function viewOffice(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        $('#office_units_id_modal_button').hide();
        $('#office_units_id_div').show();
        $('#office_error_id').hide();

        console.log(selectedOffice.name + " selected");
        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        officeUnitSelector.val(selectedOffice.id);
        
    }

    function viewOfficeIdInInput(selectedOffice) {
        viewOffice(selectedOffice);
        fetchSuperiorDesignation(selectedOffice.id);
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function officeModalEditButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        officeSearchSetSelectedOfficeLayers($('#office_units_id_input').val());
        $('#search_office_modal').modal();
    }

    function convertToBanglaNumber(str) {
        str = String(str);
        str = str.replaceAll('0', '০');
        str = str.replaceAll('1', '১');
        str = str.replaceAll('2', '২');
        str = str.replaceAll('3', '৩');
        str = str.replaceAll('4', '৪');
        str = str.replaceAll('5', '৫');
        str = str.replaceAll('6', '৬');
        str = str.replaceAll('7', '৭');
        str = str.replaceAll('8', '৮');
        str = str.replaceAll('9', '৯');
        return str;
    }

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
    }

    function viewEmployeeRecordIdInInput(empInfo) {
        $('#employee_record_id_modal_button').hide();
        $('#employee_record_id_div').show();

        let language = '<%=Language.toLowerCase()%>'
        let employeeView;
        if (language === 'english') {
            employeeView = empInfo.employeeNameEn + ', ' + empInfo.organogramNameEn + ', ' + empInfo.officeUnitNameEn;
        } else {
            employeeView = empInfo.employeeNameBn + ', ' + empInfo.organogramNameBn + ', ' + empInfo.officeUnitNameBn;
        }
        document.getElementById('employee_record_id_text').innerHTML = employeeView;
        $('#employee_record_id_input').val(empInfo.organogramId);
    }

    table_name_to_collcetion_map = new Map([
        ['employeeRecordId', {
            isSingleEntry: true,
            callBackFunction: viewEmployeeRecordIdInInput
        }]
    ]);
    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    function employeeRecordIdModalBtnClicked() {
        modal_button_dest_table = 'employeeRecordId';
        $('#search_emp_modal').modal();
    }

    function PreprocessBeforeSubmitting() {
        submitAddForm2();
        return false;
    }

    function fetchSuperiorDesignation(officeUnitId) {
        let url = "option_Servlet?actionType=ajax_designation&officeUnitId=" + officeUnitId;
        $('#superior_designation').html("");
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                $('#superior_designation').html(fetchedData);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }
    
   
</script>