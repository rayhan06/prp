<%@ page import="util.HttpRequestUtils" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="pb.Utils" %>
<%@page pageEncoding="UTF-8" %>
<%
    String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    long officeUnitId = Utils.parseOptionalLong(
            request.getParameter("officeUnitId"),
            1L,
            null
    );
    String context = request.getContextPath() + "/";
%>

<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1">
    <div class="kt-portlet__body pb-0">
        <div class="row col-12 px-0">
            <div class="col-md-6 form-group">
                <label class="h5" for="officeUnitId">
                    <%=HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng ? "Office" : "দপ্তর"%>
                </label>
                <select id="officeUnitId" name="officeUnitId" class='form-control rounded shadow-sm w-100 action-btn'
                        onchange="officeUnitOrganogramClosuer.fetchOfficeUnitOrganogram();">
                    <%=Office_unitsRepository.getInstance().buildOptionsWithoutSelectOption(language)%>
                </select>
            </div>

            <div class="col-md-6 form-group text-right">
                <label class="h5">&nbsp;</label>
                <div class="form-control" style="border: none">
                    <button type="button" class="btn btn-sm btn-success text-white shadow btn-border-radius"
                            id="download-excel" onclick="downloadOrderingExcel()"
                    >
                        <i class="fas fa-file-excel"></i>&nbsp;<%=isLangEn ? "Download Excel" : "ডাউনলোড এক্সেল"%>
                    </button>

                    <button type="button" class="btn btn-sm btn-primary text-white shadow ml-2  btn-border-radius"
                            onclick="orderingExcelInput.click()"
                    >
                        <i class="fas fa-cloud-upload-alt"></i>&nbsp;<%=isLangEn ? "Upload Excel" : "আপলোড এক্সেল"%>
                    </button>
                    <input type="file" class="d-none hide-file-preview" id="ordering-excel" name="orderingExcel">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="kt-portlet__content" id="office_unit_organogram_div">

</div>

<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script>
    const officeUnitOrganogramClosuer = (function () {
        let orderList = [];

        function setButtonDisabledProp(value) {
            $('.action-btn').prop('disabled', value);
        }

        function setOrderList() {
            let value = $('#organogram_ids').html();
            orderList = value.split(",");
            console.log(orderList);
        }

        return {
            upCall: function (button) {
                let index = button.parentNode.parentNode.rowIndex;
                if (index > 1) {
                    index = index - 1;
                    let curValue = orderList[index];
                    orderList[index] = orderList[index - 1];
                    orderList[index - 1] = curValue;
                    let row = $(button).parents("tr:first");
                    row.insertBefore(row.prev());
                }
            },

            downCall: function (button) {
                let len = $('#tableData2')[0].rows.length;
                len = len - 1; // reduce title's row length
                let index = button.parentNode.parentNode.rowIndex;
                if (index < len) {
                    let row = $(button).parents("tr:first");
                    row.insertAfter(row.next());
                    let downValue = orderList[index];
                    orderList[index] = orderList[index - 1];
                    orderList[index - 1] = downValue;
                }
            },

            fetchOfficeUnitOrganogram: function () {
                let officeUnit = $('#officeUnitId').val();
                if (!officeUnit) {
                    officeUnit = 1;
                }
                $.ajax({
                    url: "Office_unit_organogramServlet?actionType=ajax_office_unit_organogram&office_unit_id=" + officeUnit,
                    type: "GET",
                    async: false,
                    success: function (fetchedData) {
                        document.getElementById("office_unit_organogram_div").innerHTML = fetchedData;
                        setOrderList();
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            },
            saveOrder: function () {
                let data = {
                    "officeUnitId": 1,
                    "orders": getOrderedOfficeUnitIds()
                }
                setButtonDisabledProp(true);
                $.ajax({
                    type: "POST",
                    url: "Office_unit_organogramServlet?actionType=ajax_save_order",
                    data: data,
                    dataType: 'JSON',
                    success: function (response) {
                        console.log(response);
                        if (response.responseCode === 200) {
                            showToastSticky(response.msg, response.msg);
                            setButtonDisabledProp(false);
                        } else if (response.responseCode === 0) {
                            $('#toast_message').css('background-color', '#ff6063');
                            showToastSticky(response.msg, response.msg);
                            setButtonDisabledProp(false);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                                     + ", Message: " + errorThrown);
                        setButtonDisabledProp(false);
                    }
                });
            },
        }
    })();

    function getOrderedOfficeUnitIds() {
        return Array.from(document.querySelectorAll('input[name="orderValue"]'))
                    .map(input => ({
                        officeUnitOrganogramId: input.dataset.officeUnitOrganogramId,
                        orderValue: parseInt(input.value)
                    })).sort((a, b) => a.orderValue - b.orderValue)
                    .map(organogramWIthOrder => organogramWIthOrder.officeUnitOrganogramId)
                    .join(",");
    }

    function typeOnlyInteger(e) {
        return true === inputValidationForPositiveValue(e, $(this), <%=Integer.MAX_VALUE%>, 0);
    }

    $(document).ready(() => {
        $('#officeUnitId').val('<%=officeUnitId%>');
        select2SingleSelector('#officeUnitId', '<%=language%>');
        officeUnitOrganogramClosuer.fetchOfficeUnitOrganogram();
        $('body').delegate('[data-only-integer="true"]', 'keydown', typeOnlyInteger);
    });

    function downloadOrderingExcel() {
        const officeUnitId = $('#officeUnitId').val();
        if (officeUnitId === '') {
            console.warn("officeUnitId is empty not downloading excel");
            return;
        }
        location.href = "Office_unit_organogramServlet?actionType=ajax_getOrderingExcel"
                        + "&officeUnitId=" + officeUnitId;
    }

    const orderingExcelInput = document.getElementById('ordering-excel');

    orderingExcelInput.onchange = async function () {
        if (this.files.length === 0) {
            console.warn('no files selected');
            return;
        }
        const formData = new FormData();
        formData.append('orderingExcel', this.files[0]);
        const res = await fetch("Office_unit_organogramServlet?actionType=ajax_saveOrderFromExcel", {
            method: 'POST',
            body: formData
        });
        const response = await res.json();
        if (response.responseCode === 200) {
            showToastSticky(response.msg, response.msg);
            setTimeout(() => {
                const officeUnitId = $('#officeUnitId').val();
                let url = 'Office_unit_organogramServlet?actionType=setOrdering';
                if(officeUnitId !== '') {
                    url += '&officeUnitId=' + officeUnitId;
                }
                location.href = url;
            }, 500);
        } else if (response.responseCode === 0) {
            $('#toast_message').css('background-color', '#ff6063');
            showToastSticky(response.msg, response.msg);
        }
    }
</script>