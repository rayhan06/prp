<%@page import="office_unit_organogram.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="java.util.List" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="java.util.function.Function" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="java.util.Map" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@include file="../pb/addInitializer2.jsp" %>

<%
    String formTitle = getDataByLanguage(Language, "পদবী ক্রম নির্ধারণ", "Set Designation Ordering");
    List<Office_unit_organogramDTO> organogramDTOs = Office_unit_organogramDAO.getInstance().getAllActiveDTOs();
    request.setAttribute("organogramDTOs", organogramDTOs);
    String context = request.getContextPath() + "/";
%>

<style>
    .table-sticky-header thead th {
        position: -webkit-sticky; /* for Safari */
        position: sticky;
        top: 0;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal" id="organogram-layer-from">
            <div class="kt-portlet__body form-body">
                <div class="row col-12">
                    <div class="col-md-6 form-group">
                        <label class="h5" for="officeUnitId">
                            <%=getDataByLanguage(Language, "দপ্তর", "Office")%>
                        </label>
                        <select id="officeUnitId" name="officeUnitId" class='form-control rounded shadow-sm w-100'
                                onchange="searchOrganograms();">
                            <%=Office_unitsRepository.getInstance().buildOptions(Language, null)%>
                        </select>
                    </div>
                    <div class="col-md-6 form-group">
                        <label class="h5" for="designationName">
                            <%=getDataByLanguage(Language, "পদবীর নাম", "Designation Name")%>
                        </label>
                        <select id="designationName" name="designationName" class='form-control rounded shadow-sm w-100'
                                onchange="searchOrganograms()">
                            <%=Employee_recordsRepository.getInstance().buildAllDesignationOption(Language, null)%>
                        </select>
                    </div>
                </div>

                <div style="height: 55vh !important; overflow-y: auto">
                    <table class="table table-bordered table-striped table-sticky-header" id="organogram-table">
                        <thead>
                        <tr>
                            <th><%=getDataByLanguage(Language, "পদবী", "Designation")%>
                            </th>
                            <th><%=getDataByLanguage(Language, "অফিস", "Office")%>
                            </th>
                            <th><%=getDataByLanguage(Language, "১ম স্তরের ক্রম", "1st Layer Order")%>
                            </th>
                            <th><%=getDataByLanguage(Language, "২য় স্তরের ক্রম", "2nd Layer Order")%>
                            </th>
                        </tr>
                        </thead>
                        <tbody class="main-tbody">
                        <jsp:include page="setOrderingTableRows.jsp" />
                        </tbody>
                        <tr class="loading-gif" style="display: none;">
                            <td class="text-center" colspan="100%">
                                <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
                                <span><%=getDataByLanguage(Language, "লোড হচ্ছে...", "Loading...")%></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="row mt-5">
                    <div class="col-md-12">
                        <div class="form-actions text-right">
                            <button id="submit-all-btn" type="button" onclick="submitForm();"
                                    class="btn-sm shadow text-white border-0 submit-btn ml-2">
                                <%=getDataByLanguage(Language, "সেভ করুন", "SAVE")%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script>
    function typeOnlyInteger(e) {
        return true === inputValidationForPositiveValue(e, $(this), <%=Integer.MAX_VALUE%>, 0);
    }

    $(() => {
        select2SingleSelector('#designationName', '<%=Language%>');
        select2SingleSelector('#officeUnitId', '<%=Language%>');
        $('body').delegate('[data-only-integer="true"]', 'keydown', typeOnlyInteger);
    });

    function showOrHideLoadingGif(tableId, toShow) {
        const loadingGif = $('#' + tableId + ' tr.loading-gif');
        if(toShow){
            document.querySelector('#' + tableId + ' tbody.main-tbody').innerHTML = '';
            document.querySelectorAll('#' + tableId + ' tfoot').forEach(tfoot => tfoot.remove());
            loadingGif.show();
        }
        else loadingGif.hide();
    }

    const organogramLayerFrom = $('#organogram-layer-from');

    const officeUnitIdSelect = $('#officeUnitId');
    const designationNameSelect = $('#designationName');
    const organogramTableTbody = document.querySelector('#organogram-table tbody.main-tbody');
    const organogramTableId = 'organogram-table';
    async function searchOrganograms() {
        const officeUnitId = officeUnitIdSelect.val();
        const designationName = designationNameSelect.val();

        const url = 'Office_unit_organogramServlet?actionType=ajax_getOrderingRows&officeUnitId='
                    + officeUnitId + '&designationName=' + designationName;

        showOrHideLoadingGif(organogramTableId, true);
        try {
            const res = await fetch(url);
            organogramTableTbody.innerHTML = await res.text();
        } catch (error) {
            console.log(error);
        }
        showOrHideLoadingGif(organogramTableId, false);
    }

    function submitForm() {
        setButtonDisableState(true);
        $.ajax({
            type: "POST",
            url: "Office_unit_organogramServlet?actionType=ajax_setOrdering",
            data: organogramLayerFrom.serialize(),
            dataType: 'JSON',
            success: function (response) {
                if (response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky('সেভ করতে ব্যর্থ হয়েছে!', 'Failed to Save!');
                } else if (response.responseCode === 200) {
                    $('#toast_message').css('background-color', '#04c73c');
                    showToast(response.msg, response.msg);
                }
                setButtonDisableState(false);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#toast_message').css('background-color', '#ff6063');
                showToastSticky('সেভ করতে ব্যর্থ হয়েছে!', 'Failed to Save!');
                setButtonDisableState(false);
            }
        });
    }

    function setButtonDisableState(value) {
        $('#submit-all-btn').prop('disabled', value);
    }
</script>