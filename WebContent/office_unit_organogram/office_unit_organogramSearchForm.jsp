<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="office_unit_organogram.*" %>
<%@ page import="util.*" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.util.List" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@page pageEncoding="UTF-8" %>

<%
    String navigator2 = "navOFFICE_UNIT_ORGANOGRAM";
%>
<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><%=LM.getText(LC.HM_OFFICE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.GLOBAL_SUPERIOR_DESIGNATION, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.GLOBAL_DESIGNATION_ENGLISH, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.GLOBAL_DESIGNATION_BANGLA, loginDTO)%>
            </th>
            <th style="max-width: 15%"><%=isLanguageEnglish ? "Brief Description" : "সংক্ষিপ্ত বিবরণ"%>
            </th>
            <th><%=isLanguageEnglish ? "1st Layer Order" : "১ম স্তরের ক্রম"%>
            </th>
            <th><%=isLanguageEnglish ? "2nd Layer Order" : "২য় স্তরের ক্রম"%>
            </th>
            <th><%=LM.getText(LC.HM_STATUS, loginDTO)%>
            </th>
            <th><%=isLanguageEnglish ? "Temporary Post" : "অস্থায়ী পদ"%>
            </th>
             <th><%=isLanguageEnglish ? "Virtual Post" : "ভার্চুয়াল পদ"%>
            </th>
            <th><%=LM.getText(LC.OFFICE_UNIT_ORGANOGRAM_SEARCH_OFFICE_UNIT_ORGANOGRAM_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span><%=isLanguageEnglish ? "All" : "সকল"%></span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Office_unit_organogramDTO> data = (List<Office_unit_organogramDTO>) rn2.list;
            if (data != null && data.size() > 0) {
                for (Office_unit_organogramDTO office_unit_organogramDTO : data) {
                    EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(office_unit_organogramDTO.id);
                    Employee_recordsDTO employeeRecordsDTO = employeeOfficeDTO != null ? Employee_recordsRepository.getInstance().getById(employeeOfficeDTO.employeeRecordId) : null;
                    String title = isLanguageEnglish?"No employee assigned":"কোনো কর্মকর্তা/কর্মচারী যুক্ত নেই";
                    if (employeeRecordsDTO != null) {
                        title = isLanguageEnglish ? employeeRecordsDTO.nameEng : employeeRecordsDTO.nameBng;
                    }
        %>
        <tr title="<%=title%>">
            <td>
                <%=Office_unitsRepository.getInstance().geText(Language, office_unit_organogramDTO.officeUnitId)%>
            </td>

            <td>
                <%=OfficeUnitOrganogramsRepository.getInstance().getDesignation(Language, office_unit_organogramDTO.superiorDesignationId)%>
            </td>

            <td>
                <%=office_unit_organogramDTO.designationEng%>
            </td>

            <td>
                <%=office_unit_organogramDTO.designationBng%>
            </td>

            <td style="max-width: 15%">
                <%=isLanguageEnglish ? office_unit_organogramDTO.briefDescriptionEng : office_unit_organogramDTO.briefDescriptionBng%>
            </td>

            <td>
                <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(office_unit_organogramDTO.layer1))%>
            </td>

            <td>
                <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(office_unit_organogramDTO.layer2))%>
            </td>
            
            <td>
            	<%=office_unit_organogramDTO.status == 1 ? LM.getText(LC.GLOBAL_STATUS_ACTIVE, loginDTO) : LM.getText(LC.GLOBAL_STATUS_INACTIVE, loginDTO)%>
            </td>
            
            <td>
            <%
            if(office_unit_organogramDTO.status == 1 && office_unit_organogramDTO.permanentOrganogramId == -1)
            {
            	%>
            	<button
                        type="button"
                        class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #33cc6b;"
                        data-toggle="modal" data-target="#creationModal_<%=office_unit_organogramDTO.iD%>"
                >
                    <i class="fa fa-edit">
                    	<%=isLanguageEnglish ? "Create" : "তৈরি করুন"%>
            		</i>
                </button>
                
                <div id="creationModal_<%=office_unit_organogramDTO.iD%>" class="modal fade" role="dialog">
				    <div class="container">
				        <div class="">
				            <div class="modal-dialog">
				
				                <!-- Modal content-->
				                <div class="modal-content">
				                	<div id = "modalDiv_<%=office_unit_organogramDTO.iD%>"
							              >
					                    <div class="modal-header">
					                        <button type="button" class="close" data-dismiss="modal"></button>
					                        <h4 class="modal-title">
					                        </h4>
					                    </div>
					                    <div class="modal-body">
											<input name = "ooid" value = "<%=office_unit_organogramDTO.iD%>" style = "display:none" />
											<div class="form-group row">
				                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.GLOBAL_DESIGNATION_ENGLISH, loginDTO)%>
				                                        </label>
				                                        <div class="col-md-9">
				                                            <input  type='text' class='form-control' name='designationEng'
			                                                   id='designationEng_<%=office_unit_organogramDTO.iD%>'
			                                                   value='' tag='pb_html'/>
				                                        </div>
				                              </div>
				                              <div class="form-group row">
				                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.GLOBAL_DESIGNATION_BANGLA, loginDTO)%>
				                                        </label>
				                                        <div class="col-md-9">
				                                            <input  type='text' class='form-control' name='designationBng'
			                                                   id='designationBng_<%=office_unit_organogramDTO.iD%>'
			                                                   value='' tag='pb_html'/>
				                                        </div>
				                              </div>
				                         </div>
					                    <div class="modal-footer">
					                        <button type="button" class="btn btn-success" onclick="createTemp(<%=office_unit_organogramDTO.iD%>)";
					                                ><%=LM.getText(LC.GLOBAL_SUBMIT, loginDTO)%>
					                        </button>
					                    </div>
				                    
				                    </div>
				                </div>
				                
				            </div>
				        </div>
				    </div>
			</div>
            	<%
            }
            else if(office_unit_organogramDTO.status == 1 && office_unit_organogramDTO.permanentOrganogramId != -1)
            {
            	%>
            	<button
                        type="button"
                        class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #ff6b6b;"
                        onclick="location.href='Office_unit_organogramServlet?actionType=revert&ID=<%=office_unit_organogramDTO.iD%>'"
                >
                    <i class="fa fa-edit">
                    <%=isLanguageEnglish ? "Revert" : "বাতিল করুন"%>
            		</i>
                </button>
            	<%
            }
            %>
            </td>
            
            <td>
                <%=Utils.getYesNo(office_unit_organogramDTO.isVirtual, Language)%>
            </td>

            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #ff6b6b;"
                        onclick="location.href='Office_unit_organogramServlet?actionType=getEditPage&ID=<%=office_unit_organogramDTO.iD%>'"
                >
                    <i class="fa fa-edit"></i>
                </button>
            </td>

            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID'
                                                 value='<%=office_unit_organogramDTO.iD%>'/></span>
                </div>
            </td>

        </tr>
        <% }
        } %>
        </tbody>

    </table>
</div>




<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>


