<%@page import="login.LoginDTO" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="java.util.List" %>
<%@ page import="office_unit_organogram.Office_unit_organogramDAO" %>
<%@ page import="office_unit_organogram.Office_unit_organogramDTO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="util.StringUtils" %>
<%
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    long officeUnitId = Long.parseLong(request.getParameter("office_unit_id"));
    List<Office_unit_organogramDTO> list = Office_unit_organogramDAO.getInstance().getByOfficeUnitId(officeUnitId);
    String ids = list.stream()
            .map(e -> String.valueOf(e.iD))
            .collect(Collectors.joining(","));
%>
<span id="organogram_ids" style="display: none"><%=ids%></span>
<div class="kt-content kt-grid__item kt-grid__item--fluid pt-0" id="kt_content" style="background: white">
    <div class="kt-portlet shadow-none">
        <div class="">
            <div id="tagged_questions">
                <div class="table-responsive">
                    <table id="tableData2" class="table table-bordered table-striped text-nowrap mb-0">
                        <thead>
                        <tr>
                            <th><%=LM.getText(LC.GLOBAL_DESIGNATION_ENGLISH, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.GLOBAL_DESIGNATION_BANGLA, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.GLOBAL_EMPLOYEE_MP, loginDTO)%>
                            </th>
                            <th><%=isLangEng ? "Order" : "ক্রম"%>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <%
                            for (Office_unit_organogramDTO dto : list) {
                                if (dto != null) {
                        %>
                        <tr>
                            <td title="<%=dto.briefDescriptionEng%>"><%=dto.designationEng%>
                            </td>
                            <td title="<%=dto.briefDescriptionBng%>"><%=dto.designationBng%>
                            </td>
                            <td><%
                                Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getDTOByOrganogramId(dto.iD);
                                if (employeeRecordsDTO != null) {
                            %>
                                <%=isLangEng ? "<b>" + employeeRecordsDTO.nameEng + "</b><br>" + employeeRecordsDTO.employeeNumber + "<br>" + employeeRecordsDTO.personalMobile :
                                        "<b>" + employeeRecordsDTO.nameBng + "</b><br>" + StringUtils.convertToBanNumber(employeeRecordsDTO.employeeNumber) + "<br>" + StringUtils.convertToBanNumber(employeeRecordsDTO.personalMobile)%>
                                <%
                                    }
                                %>
                            </td>
                            <td>
                                <input type="text"
                                       class="form-control"
                                       name="orderValue"
                                       value="<%=dto.orderValue%>"
                                       data-only-integer="true"
                                       data-office-unit-organogram-id="<%=dto.iD%>"
                                >
                                <%-- move up-down seems clumsy for user
                                <button class="btn btn-info action-btn shadow btn-border-radius py-3 pl-4"
                                        onclick="officeUnitOrganogramClosuer.upCall(this)"
                                        title="<%=LM.getText(LC.GLOBAL_UP,loginDTO)%>"><i
                                        class="fa fa-arrow-up"></i>
                                </button>
                                <button class="btn btn-primary action-btn shadow btn-border-radius py-3 pl-4"
                                        onclick="officeUnitOrganogramClosuer.downCall(this)"
                                        title="<%=LM.getText(LC.GLOBAL_DOWN,loginDTO)%>"><i
                                        class="fa fa-arrow-down"></i></button>
                                --%>
                            </td>
                        </tr>
                        <%
                                }
                            }
                        %>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-md-12 text-right">
                    <button class="action-btn btn submit-btn text-white shadow btn-border-radius"
                            onclick="officeUnitOrganogramClosuer.saveOrder()">
                        <%=isLangEng ? "Save" : "সেভ করুন"%>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>