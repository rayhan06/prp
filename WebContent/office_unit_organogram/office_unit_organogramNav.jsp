<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    String url = "Office_unit_organogramServlet?actionType=search";
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 control-label ">
                            <%=LM.getText(LC.HM_OFFICE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                    id="office_units_id_modal_button"
                                    onclick="officeModalButtonClicked();">
                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                            </button>

                            <div class="input-group" id="office_units_id_div" style="display: none">
                                <input type="hidden" name='officeUnitId' id='office_units_id_input' value="">
                                <button type="button"
                                        class="btn btn-secondary btn-block shadow-sm btn-border-radius form-control"
                                        id="office_units_id_text" onclick="officeModalEditButtonClicked()">
                                </button>
                                <span class="input-group-btn" style="height: 40px" tag='pb_html'>
							<button type="button" class="btn btn-outline-danger" style="border-radius: 0;height: 40px"
                                    onclick="crsBtnClicked('office_units_id');"
                                    id='office_units_id_crs_btn' tag='pb_html'>
								x
							</button>
						</span>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="designation_eng" class="col-md-3 col-form-label ">
                            <%=LM.getText(LC.GLOBAL_DESIGNATION_ENGLISH, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="designation_eng" placeholder=""
                                   name="designation_eng" onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="designation_bng" class="col-md-3 col-form-label ">
                            <%=LM.getText(LC.GLOBAL_DESIGNATION_BANGLA, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="designation_bng" placeholder=""
                                   name="designation_bng" onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>


<div style="padding-right: 20px; padding-bottom: 20px">
	<button type="button"
	        class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
	        onclick="allfield_changed('',0, 1)"
	        style="background-color: #a8b194; float: right;">
	    <%=LM.getText(LC.HM_EXCEL, loginDTO)%>
	</button>
</div>
<!-- End: search control -->
<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<%@include file="../common/pagination_with_go2.jsp" %>

<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">
    let OfficeUnitOrganogramsSelector = (function (){
        return {
            anyFieldSelector : $('#anyfield'),
            officeUnitSelector : $('#office_units_id_input'),
            designationEngSelector :$('#designation_eng'),
            designationBngSelector :$('#designation_bng')
        }
    })();
    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Office_unit_organogramServlet?actionType=search&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number, type) {
        let params = 'search=true&ajax=true';
        if(OfficeUnitOrganogramsSelector.anyFieldSelector.val()){
            params += '&AnyField='+OfficeUnitOrganogramsSelector.anyFieldSelector.val();
        }
        if(OfficeUnitOrganogramsSelector.designationEngSelector.val()){
            params += '&designation_eng=' + OfficeUnitOrganogramsSelector.designationEngSelector.val();
        }
        if(OfficeUnitOrganogramsSelector.designationBngSelector.val()){
            params += '&designation_bng=' + OfficeUnitOrganogramsSelector.designationBngSelector.val();
        }
        if(OfficeUnitOrganogramsSelector.officeUnitSelector.val()){
            params += '&officeUnitId=' + $('#office_units_id_input').val();
        }
        params += '&officeUnitId=' + OfficeUnitOrganogramsSelector.officeUnitSelector.val();
        /*if ($("#job_grade_type_cat").val() != -1 && $("#job_grade_type_cat").val() != "") {
            params += '&job_grade_type_cat=' + $("#job_grade_type_cat").val();
        }*/

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        let pageNo = document.getElementsByName('pageno')[0].value;
        let rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        let totalRecords = 0;
        let lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        
        
        if(type == 0 || typeof(type) == "undefined")
		{
        	dosubmit(params);
		}
		else
		{
			var url =  "Office_unit_organogramServlet?actionType=xl&" + params;
			window.location.href = url;
		}
        
    }

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
    }

    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') return;

        $('#office_units_id_modal_button').hide();
        $('#office_units_id_div').show();

        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id_input').val(selectedOffice.id);
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function officeModalEditButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        officeSearchSetSelectedOfficeLayers($('#office_units_id_input').val());
        $('#search_office_modal').modal();
    }
</script>