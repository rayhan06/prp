<%@page import="workflow.WorkflowController"%>
<%@page import="employee_offices.EmployeeOfficeRepository"%>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="budget_password.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>

<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="office_unit_organograms.*" %>
<%@ page import="office_units.*" %>
<%@ page import="util.StringUtils" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>


<%
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
	boolean isLangEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
	String formTitle = isLangEnglish?"Committee Config":"কমিটি কনফিগ";
    List<Office_unitsDTO> committees = Office_unitsRepository.getInstance().getChildList(SessionConstants.CHAIRMAN_COMMITTEE);
    String confirmationMessage = isLangEnglish?"Are you sure?":"আপনি কি নিশ্চিত?";
%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid"
	id="kt_content" style="padding: 0 !important;">
	<div class="kt-portlet">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title prp-page-title">
					<%=formTitle%>
					
				</h3>
			</div>
		</div>
		
			<div class="kt-portlet__body form-body">
				
				<div class="row">
					<div class="col-12">
						<div class="mt-2">
							<div>
								<table
										class="table table-bordered table-striped">
									<thead>
									<tr>
										<th>
											<%=LM.getText(LC.LAYER_BASE_OFFICE_LIST_OFFICE_NAME, userDTO)%>
										</th>
										<th >
											<%=LM.getText(LC.HM_DESIGNATION, userDTO)%>
										</th>
										<th>
											<%=isLangEnglish?"Selected Designation":"বাছাইকৃত পদ"%>
										</th>
										<th>
											<%=LM.getText(LC.GLOBAL_SUBMIT, userDTO)%>
										</th>
									</tr>
									</thead>
									<tbody>
									<%
										int i = 0;
										for(Office_unitsDTO committee: committees)
										{
									%>
									<tr>
										<td>
											<%=isLangEnglish ? committee.unitNameEng : committee.unitNameBng%>
										</td>

										<td id="employee_record_id_modal_button_<%=i%>">
											<div class = "row">
												<div class = "col-8">
													<button type="button"
															class="btn btn-primary btn-block shadow btn-border-radius"
															onclick="modalBtnClicked(<%=i%>);">
														<%=LM.getText(LC.LANGUAGE_SELECT, userDTO)%>
													</button>
												</div>
												<div class = "col-3">
													<button type="button" class="btn-sm border-0 btn-border-radius" style="color: #ff6b6b;"
															onclick="crsBtnClicked(<%=i%>);"
															id='employee_record_id_crs_btn_<%=i%>'>
														<i class="fa fa-trash"></i>

													</button>
												</div>
											</div>
										</td>
										<td>
											<div class = "row">
												<div id = "selected_designation_<%=i%>" >
													<%
														Committee_post_mapDTO committee_post_mapDTO = Committee_post_mapRepository.getInstance().getByCommittee(committee.iD);
														OfficeUnitOrganograms officeUnitOrganograms = null;
														EmployeeOfficeDTO employeeOfficeDTO = null;
														if(committee_post_mapDTO != null)
														{
															officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(committee_post_mapDTO.replacementPostId);

														}
														if(officeUnitOrganograms != null)
														{
															Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getDTOByOrganogramId(officeUnitOrganograms.id);
															employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(officeUnitOrganograms.id);
															String name;
															if(employeeRecordsDTO == null){
																name = "";
															}else{
																name = Employee_recordsRepository.getInstance().getEmployeeName(employeeRecordsDTO.iD, isLangEnglish);
															}
															if(name!=null && name.trim().length()>0)
															{
													%>
													<%=isLangEnglish?employeeRecordsDTO.employeeNumber:StringUtils.convertToBanNumber(employeeRecordsDTO.employeeNumber)%>,
													<%=name.trim()%>

													<br>
													<%
														}
													%>
													<b>
														<%=isLangEnglish?officeUnitOrganograms.designation_eng:officeUnitOrganograms.designation_bng%>
													</b>
													<%
														Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitOrganograms.office_unit_id);
														if(office_unitsDTO != null)
														{
													%>
													<br>
													<%=isLangEnglish?office_unitsDTO.unitNameEng:office_unitsDTO.unitNameBng%>
													<%
															}
													%>
													<%
														if(employeeOfficeDTO!=null){
													%>
													<br>
													<b><%=isLangEnglish?"Joining Date":"যোগদানের তারিখ"%> :</b>
													<%=StringUtils.getFormattedDate(isLangEnglish,employeeOfficeDTO.joiningDate)%>
													<%
															}
														}
													%>
												</div>

											</div>
											<input type = "hidden" id = "selected_org_<%=i%>" value = '<%=officeUnitOrganograms != null?officeUnitOrganograms.id:-1%>'/>
											<input type = "hidden" id = "committee_<%=i%>" value = '<%=committee.iD%>'/>
										</td>
										<td>
											<button type="button" class="btn btn-submit btn-block shadow btn-border-radius"
													onclick="submitCommitteeMap(<%=i%>);"
											>
												<i class="fa fa-check"></i>
											</button>
										</td>
									</tr>
									<%
											i++;
										}
									%>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		
	</div>
</div>


<%
    String modalTitle = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng ? "Find Designation" : "পদবী খুঁজুন";
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
    <jsp:param name="modalTitle" value="<%=modalTitle%>"/>
</jsp:include>


<script type="text/javascript">
    const isLangEng = <%=isLangEnglish%>;
    

    function crsBtnClicked(rowIndex) {
       
        $('#selected_org_' + rowIndex).val('-1');
        $('#selected_designation_' + rowIndex).html('');
    }
    
    function viewInfoAndSetInput(empInfo, rowIndex) {
        let employeeView;
        if (isLangEng) {
            employeeView = empInfo.employeeNameEn + '<br><b>' + empInfo.organogramNameEn + '</b><br>' + empInfo.officeUnitNameEn;
        } else {
            employeeView = empInfo.employeeNameBn + '<br><b>' + empInfo.organogramNameBn + '</b><br>' + empInfo.officeUnitNameBn;
        }
        $('#selected_designation_' + rowIndex).html(employeeView);
        $('#selected_org_' + rowIndex).val(empInfo.organogramId);
    }

    table_name_to_collcetion_map = new Map([
        <% for(int rowIndex = 0;rowIndex < committees.size();rowIndex++){%>
        ['employeeRecordId_<%=rowIndex%>', {
            isSingleEntry: true,
            callBackFunction: function (empInfo) {
                viewInfoAndSetInput(empInfo, <%=rowIndex%>);
            }
        }],
        <%}%>
    ]);

    function modalBtnClicked(rowIndex) {
        modal_button_dest_table = 'employeeRecordId_' + rowIndex;
        $('#search_emp_modal').modal();
    }
    
    function submitCommitteeMap(rowIndex)
    {
    	if(!confirm("<%=confirmationMessage%>"))
   		{
   			return;
   		}
    	console.log("submitCommitteeMap called");
    	var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
            	<%
            	if(isLangEnglish)
            	{
            		%>
            	
	            	if(this.responseText == "Success")
	           		{          		
	            		toastr.success("Success");
	           		}
	            	else
	            	{
	            		toastr.error("Failure");
	            	}
	           	<%
            	}
            	else
            	{
            		%>
            		if(this.responseText == "Success")
	           		{          		
	            		toastr.success("সফলতা");
	           		}
	            	else
	            	{
	            		toastr.error("ব্যর্থতা");
	            	}
            		<%
            	}
	           	%>
            } 
            else if (this.readyState == 4 && this.status != 200) 
            {
            	toastr.error("Failure");
            }
        };
        var url = "Office_unit_organogramServlet?actionType=ajax_config&committeeId=" + $("#committee_" + rowIndex).val()
        		+ "&selectedOrg=" + $("#selected_org_" + rowIndex).val() ;
        xhttp.open("POST", url, false);
        xhttp.send();
    }

</script>
