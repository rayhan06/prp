<%@ page import="election_wise_mp.Election_wise_mpInfoModel" %>
<%@ page import="java.util.List" %>
<%@ page import="election_wise_mp.Election_wise_mpTrainingReportModel" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Collections" %>
<%@ page import="geolocation.GeoCountryRepository" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="workflow.*" %>
<%@ page contentType="text/html;charset=utf-8" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="language.LM" %>
<%@ page import="language.LC" %>

<%
	boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
	String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    List<Long> employeeIds =
            (List<Long>) request.getAttribute("employeeIds");

    List<Election_wise_mpTrainingReportModel> employeeTrainingReportModels =
            (List<Election_wise_mpTrainingReportModel>) request.getAttribute("employeeTrainingReportModels");

    Map<Long, List<Election_wise_mpTrainingReportModel>> mpTrainingReportModelsByElectionWiseMpId =
    		employeeTrainingReportModels.stream()
                                  .collect(Collectors.groupingBy(
                                          mpTrainingReportModel -> mpTrainingReportModel.employeeRecordsId
                                  ));
%>

<thead>
<tr>
	<th class="text-center"><%=isLangEng?"Name":"নাম"%></th>
    <th class="text-center"><%=isLangEng?"Designation":"পদবী"%></th>
    <th class="text-center"><%=isLangEng?"Office":"অফিস"%></th>
    <th class="text-center"><%=isLangEng?"Training":"প্রশিক্ষণ"%></th>
    <th class="text-center"><%=isLangEng?"Country":"দেশ"%></th>
    <th class="text-center"><%=isLangEng?"Timeline":"সময়কাল"%></th>
</tr>
</thead>

<tbody>
<%if (employeeIds.isEmpty()) {%>
<tr>
    <td class="text-center" colspan="100%">
        ভ্রমণ বিষয়ক কোনো তথ্য পাওয়া যায় নি
    </td>
</tr>
<%
} else {
    for (Long erId : employeeIds) {
        List<Election_wise_mpTrainingReportModel> reportModels =
                mpTrainingReportModelsByElectionWiseMpId.getOrDefault(erId, Collections.emptyList());
        int rowSpan = reportModels.isEmpty() ? 1 : reportModels.size();
%>
<tr>
    <td class="text-center" rowspan="<%=rowSpan%>">
        <%=WorkflowController.getNameFromEmployeeRecordID(erId, isLangEng)%>
        <br>
        <%=WorkflowController.getUserNameFromErId(erId, isLangEng)%>
        <%
        long org = WorkflowController.getOrganogramIDFromErID(erId);
        %>
    </td>
    <td class="text-center" rowspan="<%=rowSpan%>">
        <%=WorkflowController.getOrganogramName(org, isLangEng)%>
    </td>
    <td class="text-center" rowspan="<%=rowSpan%>">
        <%=WorkflowController.getOfficeNameFromOrganogramId(org, isLangEng)%>
    </td>
    <%if (reportModels.isEmpty()) {%>
    <td class="text-center" colspan="3">
        ভ্রমণ বিষয়ক কোনো তথ্য পাওয়া যায় নি
    </td>
    <%
    } else {
        Election_wise_mpTrainingReportModel firstReportModel = reportModels.get(0);
    %>
    <td>
        <%=isLangEng?firstReportModel.trainingNameEn: firstReportModel.trainingNameBn%>
    </td>
    <td>
        <%=GeoCountryRepository.getInstance().getText(isLangEng, firstReportModel.country)%>
    </td>
    <td>
        <%=DateUtils.getDateInWord(Language, firstReportModel.startDate)%>
        -
        <%=DateUtils.getDateInWord(Language, firstReportModel.endDate)%>
    </td>
    <%}%>
</tr>

<%
    for (int i = 1; i < reportModels.size(); ++i) {
        Election_wise_mpTrainingReportModel reportModel = reportModels.get(i);
%>
<tr>
    <td>
        <%=isLangEng?reportModel.trainingNameEn:reportModel.trainingNameBn%>
    </td>
    <td>
        <%=GeoCountryRepository.getInstance().getText(isLangEng, reportModel.country)%>
    </td>
    <td>
        <%=DateUtils.getDateInWord(Language, reportModel.startDate)%>
        -
        <%=DateUtils.getDateInWord(Language, reportModel.endDate)%>
    </td>
</tr>
<%}%>

<%
        }
    }
%>
</tbody>