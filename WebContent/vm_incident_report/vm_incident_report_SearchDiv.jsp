<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VM_INCIDENT_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row mx-2">
    <div class="col-12">
        <%@include file="../pbreport/yearmonth.jsp"%>
        <%@include file="../pbreport/calendar.jsp"%>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.VM_INCIDENT_REPORT_WHERE_INCIDENTCAT, loginDTO)%>
				</label>
				<div class="col-md-9">
					<select class='form-control'  name='incidentCat' id = 'incidentCat' >		
						<%		
						Options = CatDAO.getOptions(Language, "incident", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.VM_INCIDENT_REPORT_WHERE_VEHICLETYPECAT, loginDTO)%>
				</label>
				<div class="col-md-9">
					<select class='form-control'  name='vehicleTypeCat' id = 'vehicleTypeCat' >		
						<%		
						Options = CatDAO.getOptions(Language, "vehicle_type", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div" style = "display: none;">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.VM_INCIDENT_REPORT_WHERE_VEHICLEID, loginDTO)%>
				</label>
				<div class="col-md-9">
					<input class='form-control'  name='vehicleId' id = 'vehicleId' value=""/>							
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.VM_INCIDENT_REPORT_WHERE_REGNO, loginDTO)%>
				</label>
				<div class="col-md-9">
					<input class='form-control'  name='regNo' id = 'regNo' value=""/>							
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.VM_INCIDENT_REPORT_WHERE_DRIVERPHONENUM, loginDTO)%>
				</label>
				<div class="col-md-9">
					<input class='form-control'  name='driverPhoneNum' id = 'driverPhoneNum' value=""/>							
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.VM_INCIDENT_REPORT_WHERE_DRIVERNAMEEN, loginDTO)%>
				</label>
				<div class="col-md-9">
					<input class='form-control'  name='driverNameEn' id = 'driverNameEn' value=""/>							
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.VM_INCIDENT_REPORT_WHERE_DRIVERNAMEBN, loginDTO)%>
				</label>
				<div class="col-md-9">
					<input class='form-control'  name='driverNameBn' id = 'driverNameBn' value=""/>							
				</div>
			</div>
		</div>
		<div  class="search-criteria-div" style = "display: none;">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.VM_INCIDENT_REPORT_WHERE_ISDELETED, loginDTO)%>
				</label>
				<div class="col-md-9">
					<input class='form-control'  name='isDeleted' id = 'isDeleted' value=""/>							
				</div>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">

$(document).ready(() => {
	showFooter = false;
});

function init()
{
    dateTimeInit($("#Language").val());
}
function PreprocessBeforeSubmiting()
{
}
</script>