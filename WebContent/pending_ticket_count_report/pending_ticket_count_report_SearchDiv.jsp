<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.*"%>
<%@ page import="pb.*" %>
<%@page import="workflow.WorkflowController"%>
<%@ page import="employee_offices.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>

<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.PENDING_TICKET_COUNT_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row">
    <div class="col-12">
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=Language.equalsIgnoreCase("english")?"Issue Type":"সমস্যার ধরণ"%>

				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='ticketIssuesType' id = 'ticketIssuesType' >		
						<%		
						Options = CommonDAO.getOptions(Language, "ticket_issues", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=Language.equalsIgnoreCase("english")?"Assigned Engineer":"দায়িত্বপ্রাপ্ত ইঞ্জিনিয়ার"%>
				</label>
				<div class="col-sm-9">
					<select name='currentAssignedOrganogramId' id = 'currentAssignedOrganogramId'	class='form-control'>
						<option value = ""></option>
						 <%
						     Set<Long> engineersAndAdmins = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.TICKET_ADMIN_ROLE);
						
							Set<Long> ticketAdmins = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.SUPPORT_ENGINEER_ROLE);
							engineersAndAdmins.addAll(ticketAdmins);
						
						 
                            for(Long employee: engineersAndAdmins)
                            {
                            %>
                            <option value = "<%=employee%>" >
                             <%=WorkflowController.getNameFromOrganogramId(employee, Language)%>
                            </option>
                            <%
                            }
                            %>
						</select>						
				</div>
			</div>
		</div>
        
    </div>
</div>
<script type="text/javascript">
function init()
{

}
function PreprocessBeforeSubmiting()
{
}
</script>