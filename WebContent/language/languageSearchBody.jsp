<%@page import="language.LC" %>
<%@page import="permission.MenuConstants" %>
<%@page import="role.PermissionRepository" %>
<%@page import="user.UserRepository" %>
<%@page import="user.UserDTO" %>
<%@page import="login.LoginDTO" %>
<%@page import="util.ActionTypeConstant" %>
<%@page import="util.JSPConstant" %>
<%@page import="util.ServletConstant" %>
<%@page import="java.util.List" %>
<%@page import="permission.MenuDTO" %>
<%@page import="permission.MenuRepository" %>
<%@page import="permission.MenuUtil" %>
<%@page import="language.LM" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageDTO" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%!
    MenuUtil menuUtil = new MenuUtil();
%>
<%
    String url = "LanguageServlet?actionType=search";
    String servletName = "LanguageServlet";
    String navigator = SessionConstants.NAV_LANGUAGE;
    String pageName = "Language Search";
%>

<%
    String context = "../../.." + request.getContextPath() + "/";
    String action = JSPConstant.LANGUAGE_SERVLET + "?" + ActionTypeConstant.ACTION_TYPE + "=" + ActionTypeConstant.LANGUAGE_EDIT;
    LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
    UserDTO loginUserDTO = UserRepository.getUserDTOByUserID(loginDTO);
%>


<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid p-0" id="kt_content" style="background: white">
    <div class="shadow-none border-0">
        <jsp:include page="../includes/nav.jsp" flush="true">
            <jsp:param name="url" value="<%=url%>"/>
            <jsp:param name="navigator" value="<%=navigator%>"/>
            <jsp:param name="pageName" value="<%=pageName%>"/>
        </jsp:include>
        <div style="height: 1px; background: #ecf0f5"></div>
        <div class="kt-portlet shadow-none">
            <div class="kt-portlet__body">
                <form action="<%=action%>" method="POST" id="tableForm">
                    <input type="hidden" name="backLinkEnabled" value="<%=request.getParameter("backLinkEnabled")%>"/>
                    <jsp:include page='../common/flushActionStatus.jsp'/>
                    <div class="table-responsive">
                        <table id="tableData" class="table table-bordered table-striped text-nowrap">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Menu Name</th>
                                <th>English Text</th>
                                <th>Bangla Text</th>
                                <th>ConstantPrefix</th>
                                <th>Constant</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <%
                                ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_LANGUAGE);
                                if (data != null) {
                                    int size = data.size();
                                    for (int i = 0; i < size; i++) {
                                        LanguageTextDTO row = (LanguageTextDTO) data.get(i);
                            %>
                            <tr>
                                <input type="hidden" name="ID" value="<%=row.ID%>"/>
                                <input type="hidden" name="menuID" value="<%=row.menuID%>"/>
                                <td><%=row.ID%>
                                </td>
                                <td><%=menuUtil.getAllAncestorMenus((int) row.menuID)%>
                                </td>
                                <td>
                                    <input type="text" class="form-control" id="languageTextEnglish"
                                           name="<%=ServletConstant.LANGUAGE_TEXT_ENGLISH%>"
                                           value="<%=row.languageTextEnglish%>">
                                </td>
                                <td>
                                    <input type="text" class="form-control" id="languageTextBangla"
                                           name="<%=ServletConstant.LANGUAGE_TEXT_BANGLA%>"
                                           value="<%=row.languageTextBangla%>">
                                </td>
                                <td>
                                    <input type="text" class="form-control" id="languageConstantPrefix"
                                           name="<%=ServletConstant.LANGUAGE_CONSTANT_PREFIX%>"
                                           value="<%=row.languageConstantPrefix%>">
                                </td>
                                <td>
                                    <input type="text" class="form-control" id="languageConstant"
                                           name="<%=ServletConstant.LANGUAGE_CONSTANT%>"
                                           value="<%=row.languageConstant%>">
                                </td>
                                <td><input type="checkbox" name="<%=ServletConstant.DELETE_ID%>" value="<%=row.ID%>"/>
                                </td>
                            </tr>
                            <%
                                }
                            %>
                            </tbody>
                            <%
                                }
                            %>
                        </table>
                    </div>
                    <%if (PermissionRepository.checkPermissionByRoleIDAndMenuID(loginUserDTO.roleID, MenuConstants.LANGUAGE_TEXT_EDIT)) { %>
                    <div class="row">
                        <div class="col-12 form-actions text-right mt-3">
                            <a class="btn btn-sm cancel-btn text-light shadow btn-border-radius ml-auto"
                               href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.THEME_SEARCH_THEME_CANCEL_BUTTON, loginDTO)%>
                            </a>
                            <button class="btn  btn-sm submit-btn text-light shadow btn-border-radius ml-2"
                                    type="submit"><%=LM.getText(LC.GLOBAL_SUBMIT, loginDTO)%>
                            </button>
                        </div>
                    </div>
                    <%} %>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->


<%
    String menuID = (String) request.getAttribute(ServletConstant.LANGUAGE_TEXT_MENU_SELECTED);
    if (menuID == null) menuID = "";
    else request.removeAttribute(ServletConstant.LANGUAGE_TEXT_MENU_SELECTED);

    String constantPrefiex = (String) request.getAttribute(ServletConstant.LANGUAGE_CONSTANT_PREFIX_SELECTED);
    if (constantPrefiex == null) constantPrefiex = "";
    else request.removeAttribute(ServletConstant.LANGUAGE_CONSTANT_PREFIX_SELECTED);

    List<MenuDTO> allMenuList = menuUtil.getAlignMenuListAllSeparated();
%>
<%if (PermissionRepository.checkPermissionByRoleIDAndMenuID(loginUserDTO.roleID, MenuConstants.LANGUAGE_TEXT_ADD)) { %>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <form action="<%=servletName%>?actionType=add" method="POST" class="form-horizontal">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=LM.getText(LC.LANGUAGE_SEARCH_NEW_TEXT, loginDTO)%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.LANGUAGE_SEARCH_MENU, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <select class="form-control select2" size="1" name="menuID"
                                                    id="languageAddMenu">
                                                <option value="" <%if (menuID.equals("")) {%> selected='selected' <%}%>>
                                                    All
                                                </option>
                                                <%for (MenuDTO menuDTO : allMenuList) {%>
                                                <option value="<%=menuDTO.menuID%>" <%if (menuID.equals("" + menuDTO.menuID)) {%>
                                                        selected <%}%>><%=menuDTO.menuName%>
                                                </option>
                                                <%}%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.LANGUAGE_SEARCH_ENGLISH_TEXT, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" id="l_languageTextEnglish"
                                                   name="languageTextEnglish"
                                                   value="" onkeyup="setConstant(this.value)">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.LANGUAGE_SEARCH_BANGLA_TEXT, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" id="l_languageTextBangla"
                                                   name="languageTextBangla"
                                                   value="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.LANGUAGE_SEARCH_CONSTANT_PREFIX, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" id="l_languageConstantPrefix"
                                                   name="languageConstantPrefix" value="<%=constantPrefiex%>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.LANGUAGE_SEARCH_CONSTANT, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" id="l_languageConstant"
                                                   name="languageConstant"
                                                   value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 text-right">
                        <button class="btn btn-sm submit-btn text-light shadow btn-border-radius" type="submit">
                            <%=LM.getText(LC.GLOBAL_SUBMIT, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<%} %>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        select2SingleSelector("#languageAddMenu", 'English');
        $('#tableForm').submit(function (e) {
            var currentForm = this;
            var selected = false;
            e.preventDefault();
            var set = $('#tableData').find('tbody > tr > td:last-child input[type="checkbox"]');
            $(set).each(function () {
                if ($(this).prop('checked')) {
                    selected = true;
                }
            });
            currentForm.submit();
        });
    })

    function setConstant(value) {
        value = value.toUpperCase();
        value = value.replaceAll(" ", "_");
        value = value.replaceAll("-", "_");
        value = value.replaceAll(".", "_");
        value = value.replaceAll(",", "_");
        value = value.replaceAll("&", "_");
        value = value.replaceAll("%", "_");
        $("#l_languageConstant").val(value);
    }

</script>


