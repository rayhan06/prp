<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="employee_provision.*" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.List" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String Language = LM.getText(LC.EMPLOYEE_PROVISION_EDIT_LANGUAGE, loginDTO);
    String navigator2 = SessionConstants.NAV_EMPLOYEE_PROVISION;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_EMPLOYEERECORDSID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_PROVISION_ADD_STARTTIME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_PROVISION_ADD_ENDTIME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_PROVISION_ADD_DURATION, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_PROVISION_ADD_ISDONE, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.EMPLOYEE_ACR_SEARCH_EMPLOYEE_ACR_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span>All</span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Employee_provisionDTO> data = (List<Employee_provisionDTO>) recordNavigator.list;
            if (data != null && data.size() > 0) {
                for (Employee_provisionDTO employee_provisionDTO : data) {
        %>
        <tr>
            <%@include file="employee_provisionSearchRow.jsp" %>
        </tr>
        <% }
        } %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>