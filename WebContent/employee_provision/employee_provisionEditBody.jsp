<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="employee_provision.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>

<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="files.FilesDTO" %>
<%@ page import="files.FilesDAO" %>
<%@ page import="dbm.DBMW" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    Employee_provisionDTO employee_provisionDTO;
    employee_provisionDTO = (Employee_provisionDTO) request.getAttribute("employee_provisionDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (employee_provisionDTO == null) {
        employee_provisionDTO = new Employee_provisionDTO();

    }
    System.out.println("employee_provisionDTO = " + employee_provisionDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.EMPLOYEE_PROVISION_ADD_EMPLOYEE_PROVISION_ADD_FORMNAME, loginDTO);
    String servletName = "Employee_provisionServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    } else {
        employee_provisionDTO = Employee_provisionDAO.getInstance().getDTOFromID(Long.parseLong(ID));
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    String Language = LM.getText(LC.EMPLOYEE_PROVISION_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    long ColumnID;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().getById(employee_provisionDTO.employeeRecordsId);
    EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employee_provisionDTO.employeeRecordsId);
    if (employeeOfficeDTO == null) employeeOfficeDTO = new EmployeeOfficeDTO();

    Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
    if (officeUnitsDTO == null) officeUnitsDTO = new Office_unitsDTO();

    OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
    if (officeUnitOrganograms == null) officeUnitOrganograms = new OfficeUnitOrganograms();

    String URL = "Employee_provisionServlet?actionType=ajax_" + actionName + "&isPermanentTable=true";
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form" id="employee_provision_form" name="bigform" action="<%=URL%>"
              enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=employee_provisionDTO.iD%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='employeeRecordsId'
                                           id='employeeRecordsId_hidden_<%=i%>'
                                           value='<%=employee_provisionDTO.employeeRecordsId%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_EMPLOYEERECORDID, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-form-label form-control">
                                            <%=isLanguageEnglish ? employee_recordsDTO.nameEng : employee_recordsDTO.nameBng%>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_OFFICEUNITID, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-form-label form-control">
                                            <%=isLanguageEnglish ? officeUnitsDTO.unitNameEng : officeUnitsDTO.unitNameBng%>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-form-label form-control">
                                            <%=isLanguageEnglish ? officeUnitOrganograms.designation_eng : officeUnitOrganograms.designation_bng%>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.EMPLOYEE_PROVISION_ADD_STARTTIME, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-form-label form-control">
                                            <%
                                                value = employee_provisionDTO.startTime + "";
                                                String startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                            %>

                                            <%=Utils.getDigits(startDate, Language)%>
                                            <input type='hidden' value="<%=employee_provisionDTO.startTime%>"
                                                   name='startTime' id='startTime' tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.EMPLOYEE_PROVISION_ADD_ENDTIME, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-form-label form-control">
                                            <%
                                                value = employee_provisionDTO.endTime + "";
                                                String endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                            %>

                                            <%=Utils.getDigits(endDate, Language)%>

                                            <input type='hidden' value="<%=employee_provisionDTO.endTime%>"
                                                   name='endTime' id='endTime' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_PROVISION_IN_MONTHS, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                value = "";
                                                if (employee_provisionDTO.duration != -1) {
                                                    value = employee_provisionDTO.duration + "";
                                                }
                                            %>
                                            <input type='number' class='form-control' name='duration'
                                                   id='duration_number_<%=i%>' value='<%=value%>' tag='pb_html'>
                                        </div>
                                    </div>
                                    <%--                                    <div class="form-group row">--%>
                                    <%--                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.EMPLOYEE_PROVISION_ADD_ISDONE, loginDTO)%>--%>
                                    <%--                                        </label>--%>
                                    <%--                                        <div class="col-md-8">--%>
                                    <%--                                            <input type='checkbox' class='form-control-sm' name='isDone'--%>
                                    <%--                                                   id='isDone_checkbox_<%=i%>'--%>
                                    <%--                                                   value=<%=(String.valueOf(employee_provisionDTO.isDone).equals("true"))?("checked"):""%>--%>
                                    <%--                                                   tag='pb_html'>--%>
                                    <%--                                        </div>--%>
                                    <%--                                    </div>--%>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.EMPLOYEE_PROVISION_ADD_ASSESMENTREPORTFILE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                if (actionName.equals("edit")) {
                                                    List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(employee_provisionDTO.assessmentReportFile);
                                            %>
                                            <table>
                                                <tr>
                                                    <%
                                                        if (filesDropzoneDTOList != null) {
                                                            for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                                                FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                                                byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                    %>
                                                    <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                                        <%
                                                            if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                        %>
                                                        <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                             style='width:100px'/>
                                                        <%
                                                            }
                                                        %>
                                                        <a href='Employee_provisionServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                           download><%=filesDTO.fileTitle%>
                                                        </a>
                                                        <a class='btn btn-danger'
                                                           onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete")'>x</a>
                                                    </td>
                                                    <%
                                                            }
                                                        }
                                                    %>
                                                </tr>
                                            </table>
                                            <%
                                                }
                                            %>

                                            <%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>
                                            <div class="dropzone"
                                                 action="Employee_provisionServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?employee_provisionDTO.assessmentReportFile:ColumnID%>">
                                                <input type='file' style="display:none"
                                                       name='filesDropzoneFile'
                                                       id='filesDropzone_dropzone_File'
                                                       tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='filesDropzoneFilesToDelete'
                                                   id='filesDropzoneFilesToDelete'
                                                   value='' tag='pb_html'/>
                                            <input type='hidden' name='assessmentReportFile'
                                                   id='assessmentReportFile' tag='pb_html'
                                                   value='<%=actionName.equals("edit")?employee_provisionDTO.assessmentReportFile:ColumnID%>'/>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                    type="button" onclick="location.href = '<%=request.getHeader("referer")%>'">
                                <%=LM.getText(LC.EMPLOYEE_PROVISION_ADD_EMPLOYEE_PROVISION_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" id="submit-btn"
                                    type="button" onclick="submitItem()">
                                <%=LM.getText(LC.EMPLOYEE_PROVISION_ADD_EMPLOYEE_PROVISION_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    let isEnglish = <%=isLanguageEnglish%>;
    let submitFunction = () => {
        if ($('#employee_provision_form').valid()) {
            submitAjaxForm('employee_provision_form');
        }
    }

    function submitItem() {
        if (isEnglish) {
            messageDialog('Do you want to submit?', "You won't be able to revert this!", 'success', true, 'Submit', 'Cancel', submitFunction);
        } else {
            messageDialog('সাবমিট করতে চান?', "সাবমিটের পর পরিবর্তনযোগ্য না!", 'success', true, 'সাবমিট', 'বাতিল', submitFunction);
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Employee_provisionServlet");
    }

    function init(row) {
        setDateByTimestampAndId('startTime_js', <%=employee_provisionDTO.startTime%>);
        setDateByTimestampAndId('endTime_js', <%=employee_provisionDTO.endTime%>);
    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });
</script>