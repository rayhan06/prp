<%@page pageEncoding="UTF-8" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="util.StringUtils" %>

<td>
    <%=Employee_recordsRepository.getInstance().getEmployeeName(employee_provisionDTO.employeeRecordsId, Language)%>
</td>
<td>
    <%=StringUtils.getFormattedDate(Language,employee_provisionDTO.startTime)%>
</td>

<td>
    <%=StringUtils.getFormattedDate(Language,employee_provisionDTO.endTime)%>
</td>

<td>
    <%=StringUtils.convertBanglaIfLanguageIsBangla(Language,String.valueOf(employee_provisionDTO.duration))%>
</td>

<td>
    <%=StringUtils.getYesNo(Language,employee_provisionDTO.isDone)%>
</td>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Employee_provisionServlet?actionType=view&ID=<%=employee_provisionDTO.iD%>'"
    >
        <i class="fa fa-eye"></i>
    </button>
</td>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Employee_provisionServlet?actionType=getEditPage&ID=<%=employee_provisionDTO.iD%>'"
    >
        <i class="fa fa-edit"></i>
    </button>
</td>

<td class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=employee_provisionDTO.iD%>'/></span>
    </div>
</td>