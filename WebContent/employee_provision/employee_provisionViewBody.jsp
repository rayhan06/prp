<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="employee_provision.*" %>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="files.*" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="util.StringUtils" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String Language = LM.getText(LC.EMPLOYEE_PROVISION_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Employee_provisionDAO employee_provisionDAO = Employee_provisionDAO.getInstance();
    Employee_provisionDTO employee_provisionDTO = employee_provisionDAO.getDTOByID(id);

    FilesDAO filesDAO = new FilesDAO();

    Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().getById(employee_provisionDTO.employeeRecordsId);
    EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employee_provisionDTO.employeeRecordsId);
    if (employeeOfficeDTO == null) employeeOfficeDTO = new EmployeeOfficeDTO();

    Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
    if (officeUnitsDTO == null) officeUnitsDTO = new Office_unitsDTO();

    OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
    if (officeUnitOrganograms == null) officeUnitOrganograms = new OfficeUnitOrganograms();

    boolean isLanguageEnglish = "English".equalsIgnoreCase(Language);
%>

<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.EMPLOYEE_PROVISION_ADD_EMPLOYEE_PROVISION_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="form-body">
                <h5 class="table-title">
                    <%=LM.getText(LC.EMPLOYEE_PROVISION_ADD_EMPLOYEE_PROVISION_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">
                        <tr>
                            <td>
                                <b><%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_EMPLOYEERECORDID, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%=isLanguageEnglish ? employee_recordsDTO.nameEng : employee_recordsDTO.nameBng%>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <b><%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_OFFICEUNITID, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%=isLanguageEnglish ? officeUnitsDTO.unitNameEng : officeUnitsDTO.unitNameBng%>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <b><%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%=isLanguageEnglish ? officeUnitOrganograms.designation_eng : officeUnitOrganograms.designation_bng%>
                            </td>
                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_PROVISION_ADD_STARTTIME, loginDTO)%>
                            </b></td>
                            <td>
                                <%=StringUtils.getFormattedDate(Language, employee_provisionDTO.startTime)%>
                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_PROVISION_ADD_ENDTIME, loginDTO)%>
                            </b></td>
                            <td>
                                <%=StringUtils.getFormattedDate(Language, employee_provisionDTO.endTime)%>
                            </td>
                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_PROVISION_ADD_DURATION, loginDTO)%>
                            </b></td>
                            <td>
                                <%=Utils.getDigits(String.valueOf(employee_provisionDTO.duration), Language)%>
                            </td>
                        </tr>

                        <tr>
                            <td class="data-view-heading">
                                <b>
                                    <%=LM.getText(LC.FILES_SEARCH_ANYFIELD, loginDTO)%>
                                </b>
                            </td>

                            <%
                                List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(employee_provisionDTO.assessmentReportFile);
                                if (filesDropzoneDTOList != null && filesDropzoneDTOList.size() >= 1) {
                                    for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                        FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                        byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                            %>
                            <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                <%
                                    if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                %>
                                <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                     style='width:100px'/>
                                <%
                                    }
                                %>
                                <a href='Employee_provisionServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                   download>
                                    <%=filesDTO.fileTitle%>
                                </a>
                            </td>
                            <%
                                }
                            } else {
                            %>
                            <td></td>
                            <%}%>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>