<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="ot_employee_type.OT_employee_typeRepository" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    System.out.println("Inside nav.jsp");
    String url = "Driver_ot_billServlet?actionType=search";
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1">
    <div class="kt-portlet__body">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_MONTHYEAR, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="monthYear_js"/>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                <jsp:param name="HIDE_DAY" value="true"/>
                            </jsp:include>
                            <input type='hidden' id='monthYearData' value='null'>
                        </div>
                    </div>
                </div>
                <%-- TODO not yet added in dto
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=isLanguageEnglishNav ? "Finance Serial Number" : "অর্থ শাখার ক্রমিক নং"%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="financeSerialNumber" id="financeSerialNumber">
                        </div>
                    </div>
                </div>
                --%>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="overtimeBillTypeCat">
                            <%=isLanguageEnglishNav ? "Bill Type" : "বিলের ধরণ"%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' style="width: 100%"
                                    name='overtimeBillTypeCat' id='overtimeBillTypeCat'
                            >
                                <%=CatRepository.getInstance().buildOptions(
                                        SessionConstants.OT_BILL_TYPE_CAT_DOMAIN_NAME,
                                        Language,
                                        null
                                )%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="isInPreview">
                            <%=isLanguageEnglishNav ? "Submission Status" : "জমা দেওয়ার অবস্থা"%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' style="width: 100%"
                                    name='isInPreview' id='isInPreview'
                            >
                                <option value=""><%=isLanguageEnglishNav ? "All" : "সব"%>
                                </option>
                                <option value="1"><%=isLanguageEnglishNav ? "Draft" : "খসড়া"%>
                                </option>
                                <option selected value="0"><%=isLanguageEnglishNav ? "Submitted" : "জমা দেওয়া"%>
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <a class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                       href="Driver_ot_billServlet?actionType=getAddPage"
                       style="background-color: #00a1d4;">
                        <%=isLanguageEnglishNav ? "Bill Submission" : "বিল জমাদান"%>
                    </a>
                </div>
                <div class="col-6 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <div class="search-loader-container-circle ">
            <div class="search-loader-circle"></div>
        </div>
    </div>
</template>


<script type="text/javascript">

    $(document).ready(() => {
    });

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                setTimeout(() => {
                    document.getElementById('tableForm').innerHTML = this.responseText;
                    setPageNo();
                    searchChanged = 0;
                }, 200);
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "<%=url%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        let params = '&monthYear=' + $('#monthYear').val();
        // params += '&financeSerialNumber=' + convertToEnglishNumber($('#financeSerialNumber').val());
        const monthYearData = JSON.parse($('#monthYearData').val());
        params += getMonthYearFromParam(monthYearData);
        params += '&overtimeBillTypeCat=' + $('#overtimeBillTypeCat').val();
        params += '&isInPreview=' + $('#isInPreview').val();


        params += '&search=true&ajax=true';
        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

    $('#monthYear_js').on('datepicker.change', (event, param) => {
        const isValidDate = dateValidator('monthYear_js', true);
        if (isValidDate) {
            $('#monthYearData').val(JSON.stringify(param.data));
        } else {
            $('#monthYearData').val('null');
        }
    });

    function stringIsBlank(string) {
        return string == null || string === '';
    }

    function getMonthYearFromParam(monthYearData) {
        if (monthYearData == null) {
            return '';
        }
        const invalidData = stringIsBlank(monthYearData.month)
                            || stringIsBlank(monthYearData.year);
        if (invalidData) return '';

        let month = +monthYearData.month;
        let year = +monthYearData.year;

        let paramString = '&monthYearFrom=' + new Date(year, month - 1, 1).getTime();

        let toTime = month === 11 // month is december
                     ? new Date(year + 1, 0, 1).getTime()
                     : new Date(year, month, 1).getTime();
        paramString += ('&monthYearTo=' + (toTime - 1));

        return paramString;
    }
</script>