<%@ page pageEncoding="UTF-8" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="driver_ot_bill.Driver_ot_billDTO" %>
<%@ page import="ot_bill_submission_config.OT_bill_submission_configDAO" %>
<%@ page import="driver_ot_bill.Driver_ot_billDAO" %>

<%
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String context = request.getContextPath() + "/";

    String formTitle = isLanguageEnglish ? "SUBMIT DIVER OVERTIME BILL" : "গাড়ীচালকদের ওভারটাইম বিল জমাদান";

    String actionName = "ajax_add";
    boolean isEditPage = false;
    Driver_ot_billDTO driverOtBillDTO = (Driver_ot_billDTO) request.getAttribute("driverOtBillDTO");
    if (driverOtBillDTO == null) {
        driverOtBillDTO = new Driver_ot_billDTO();
    } else {
        actionName = "ajax_edit";
        isEditPage = true;
    }
%>


<style>
    .loader-container-circle {
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .2);
        position: fixed;
        top: 0;
        left: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: 10;
        visibility: visible;
    }

    @media (min-width: 1015px) {
        .loader-container-circle {
            width: calc(100% + 260px);
            height: 100%;
            background-color: rgba(0, 0, 0, 0.1);
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            z-index: 3;
            visibility: visible;
        }
    }

    .loader-circle {
        width: 50px;
        height: 50px;
        border: 5px solid;
        color: #3498db;
        border-radius: 50%;
        border-top-color: transparent;
        animation: loader 1.2s linear infinite;
    }

    @keyframes loader {
        25% {
            color: #2ecc71;
        }
        50% {
            color: #f1c40f;
        }
        75% {
            color: #e74c3c;
        }
        to {
            transform: rotate(360deg);
        }
    }

    .no-data-found-div {
        text-align: center;
        background-color: #eee;
        padding: .875rem;
    }

    .template-div {
        display: none !important;
    }
</style>

<div class="loader-container-circle" id="full-page-loader">
    <div class="loader-circle"></div>
</div>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal" id="driver-ot-bill-form" enctype="multipart/form-data">

            <input type='hidden' id='id' name='id' value='<%=driverOtBillDTO.iD%>'>

            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label class="h5">
                            <%=isLanguageEnglish ? "Driver" : "গাড়ীচালক"%>
                        </label>
                        <div>
                            <%if (isEditPage) {%>
                            <div class="form-control">
                                <%=driverOtBillDTO.getEmployeeInfo(isLanguageEnglish)%>
                                <input type='hidden'
                                       id='employeeOfficesId' name='employeeOfficesId'
                                       value='<%=driverOtBillDTO.employeeOfficesId%>'
                                >
                            </div>
                            <%} else {%>
                            <select class='form-control rounded shadow-sm'
                                    name="employeeOfficesId"
                                    id="employeeOfficesId"
                                    onchange="loadData();"
                            >
                                <%=Driver_ot_billDAO.getInstance().buildOptionsDriverEmployeeOfficeIds(Language)%>
                            </select>
                            <%}%>
                        </div>
                    </div>

                    <div class="col-md-6 form-group">
                        <label class="h5">
                            <%=isLanguageEnglish ? "Bill Date" : "বিলের তারিখ"%>
                        </label>
                        <div>
                            <%if (isEditPage) {%>
                            <div class="form-control">
                                <%=driverOtBillDTO.getBillDateTypeText(isLanguageEnglish)%>
                                <input type='hidden'
                                       id='otBillSubmissionConfigId' name='otBillSubmissionConfigId'
                                       value='<%=driverOtBillDTO.otBillSubmissionConfigId%>'
                                >
                            </div>
                            <%} else {%>
                            <select class='form-control rounded shadow-sm'
                                    name="otBillSubmissionConfigId"
                                    id="otBillSubmissionConfigId"
                                    onchange="loadData();"
                            >
                                <%=OT_bill_submission_configDAO.getInstance().buildOptionsOfSubmittableDTOs(
                                        null,
                                        System.currentTimeMillis(),
                                        Language
                                )%>
                            </select>
                            <%}%>
                        </div>
                    </div>
                </div>

                <div id="driver-ot-bill-ajax-data-table-div" class="mt-4 w-100" style="overflow-x: scroll">
                    <%if (isEditPage) {%>
                    <jsp:include page="driver_ot_billTable.jsp"/>
                    <%} else {%>
                    <div class="no-data-found-div">
                        <%=isLanguageEnglish ? "No Data Found" : "কোন তথ্য পাওয়া যায়নি"%>
                    </div>
                    <%}%>
                </div>

                <div id="no-data-found-div" class="template-div no-data-found-div">
                    <%=isLanguageEnglish ? "No Data Found" : "কোন তথ্য পাওয়া যায়নি"%>
                </div>

                <div id="action-btn-div" class="row" style="display: none">
                    <div class="col-12 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button"
                                onclick="location.href = '<%=request.getHeader("referer")%>'">
                            <%=isLanguageEnglish ? "Cancel" : "বাতিল করুন"%>
                        </button>
                        <button id="preview-btn"
                                class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                                type="button" onclick="submitForm()">
                            <%=isLanguageEnglish ? "See Preview" : "প্রিভিউ দেখুন"%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script>
    const fullPageLoader = $('#full-page-loader');
    const actionBtnDiv = $('#action-btn-div');
    const ajaxDataTableDiv = document.getElementById('driver-ot-bill-ajax-data-table-div');
    const noDataFoundDiv = document.getElementById('no-data-found-div');
    const employeeOfficesIdInput = $('#employeeOfficesId');
    const otBillSubmissionConfigIdInput = $('#otBillSubmissionConfigId');

    $(document).ready(function () {
        <%if(!isEditPage){%>
        select2SingleSelector('#employeeOfficesId', '<%=Language%>');
        actionBtnDiv.hide();
        <%} else {%>
        actionBtnDiv.show();
        <%}%>
        fullPageLoader.hide();
    });

    function clearTable() {
        ajaxDataTableDiv.innerHTML = '';
        const noDataFoundDivClone = noDataFoundDiv.cloneNode(true);
        noDataFoundDivClone.classList.remove('template-div');
        ajaxDataTableDiv.append(noDataFoundDivClone);
    }

    async function loadData() {
        const employeeOfficesId = employeeOfficesIdInput.val();
        const otBillSubmissionConfigId = otBillSubmissionConfigIdInput.val();

        clearTable();
        if (employeeOfficesId === '' || otBillSubmissionConfigId === '') {
            console.warn('mandatory data missing. not doing ajax call');
            return;
        }
        fullPageLoader.show();

        const url = 'Driver_ot_billServlet?actionType=ajax_getDriverOtBillData'
                    + '&employeeOfficesId=' + employeeOfficesId
                    + '&otBillSubmissionConfigId=' + otBillSubmissionConfigId;

        const response = await fetch(url);
        ajaxDataTableDiv.innerHTML = await response.text();
        const isEditableInput = ajaxDataTableDiv.querySelector('input[name="isEditable"]');
        const isEditable = isEditableInput && (isEditableInput.value === 'true');
        if (isEditable) {
            actionBtnDiv.show();
        } else {
            actionBtnDiv.hide();
        }
        fullPageLoader.hide();
    }

    function getHours(startHour, endHour) {
        const isRangeInvalid = startHour < 0 || endHour < 0 || startHour > endHour;
        return isRangeInvalid ? 0 : endHour - startHour;
    }

    function calculateTotalHours(driverOtDetailModel) {
        driverOtDetailModel.totalOfficeHour = getHours(driverOtDetailModel.officeHourFrom, driverOtDetailModel.officeHourTo);
        driverOtDetailModel.totalExtraHourMorning = getHours(driverOtDetailModel.extraHourMorningFrom, driverOtDetailModel.extraHourMorningTo);
        driverOtDetailModel.totalExtraHourAfternoon = getHours(driverOtDetailModel.extraHourAfternoonFrom, driverOtDetailModel.extraHourAfternoonTo);
        driverOtDetailModel.totalExtraHour = driverOtDetailModel.totalExtraHourMorning + driverOtDetailModel.totalExtraHourAfternoon;
    }

    const selectElementFieldNames = [
        "officeHourFrom", "officeHourTo",
        "extraHourMorningFrom", "extraHourMorningTo",
        "extraHourAfternoonFrom", "extraHourAfternoonTo"
    ];

    function updateDriverOtDetailModel(driverOtDetailModel, rootTr) {
        for (const fieldName of selectElementFieldNames) {
            const selector = 'select[name="' + fieldName + '"]';
            const selectElement = rootTr.querySelector(selector);
            driverOtDetailModel[fieldName] = +selectElement.value;
        }
        calculateTotalHours(driverOtDetailModel);
    }

    const totalHourFieldNames = [
        "totalOfficeHour", "totalExtraHourMorning",
        "totalExtraHourAfternoon", "totalExtraHour"
    ];

    function showTotalHoursFromDriverOtDetailModel(driverOtDetailModel, rootTr) {
        for (const fieldName of totalHourFieldNames) {
            const selector = 'td[data-total-hours="' + fieldName + '"]';
            const trView = rootTr.querySelector(selector);
            trView.innerText = convertToBanglaNumIfBangla(String(driverOtDetailModel[fieldName]), '<%=Language%>');
        }
    }

    function driverOtDetailModelChanged(changedElement) {
        const nearestTr = changedElement.closest('tr.has-driver-ot-detail-model');
        const driverOtDetailModel = JSON.parse(nearestTr.dataset.model);
        updateDriverOtDetailModel(driverOtDetailModel, nearestTr);
        showTotalHoursFromDriverOtDetailModel(driverOtDetailModel, nearestTr);
        nearestTr.dataset.model = JSON.stringify(driverOtDetailModel);
    }

    function submitForm() {
        const employeeOfficesId = employeeOfficesIdInput.val();
        if (employeeOfficesId === '') {
            $('#toast_message').css('background-color', '#ff6063');
            showToast("গাড়ী চালক বাছাই করুন", "Select Driver");
            return;
        }
        const otBillSubmissionConfigId = otBillSubmissionConfigIdInput.val();
        if (otBillSubmissionConfigId === '') {
            $('#toast_message').css('background-color', '#ff6063');
            showToast("বিলের তারিখ বাছাই করুন", "Select Bill Date");
            return;
        }
        const driverOtDetailModels = Array.from(document.querySelectorAll('tr.has-driver-ot-detail-model'))
                                          .map(tableTr => JSON.parse(tableTr.dataset.model));
        if (driverOtDetailModels.length === 0) {
            $('#toast_message').css('background-color', '#ff6063');
            showToast("জমা দেবার মত কোন তথ্য নেই", "No Data To Submit");
            return;
        }
        const data = {
            id: '<%=isEditPage? driverOtBillDTO.iD : -1%>',
            employeeOfficesId: employeeOfficesId,
            otBillSubmissionConfigId: otBillSubmissionConfigId,
            driverOtDetailModels: JSON.stringify(driverOtDetailModels)
        };
        console.log({data});

        setButtonDisableState(true);
        fullPageLoader.show();
        $.ajax({
            type: "POST",
            url: "Driver_ot_billServlet?actionType=<%=actionName%>",
            data: data,
            dataType: 'JSON',
            success: function (response) {
                setButtonDisableState(false);
                fullPageLoader.hide();
                if (response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                } else if (response.responseCode === 200) {
                    window.location.replace(getContextPath() + response.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                setButtonDisableState(false);
                fullPageLoader.hide();
            }
        });
    }

    function setButtonDisableState(value) {
        $('#preview-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }
</script>