<%@page pageEncoding="UTF-8" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="driver_ot_bill.Driver_ot_bill_configDAO" %>
<%@ page import="driver_ot_bill.Driver_ot_bill_configRepository" %>
<%@ page import="driver_ot_bill.Driver_ot_bill_configDTO" %>
<%@ page import="util.DateTimeUtil" %>

<%
    String context = request.getContextPath() + "/";
    String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String formTitle = isLanguageEnglish ? "DRIVER OVERTIME BILL CONFIGURATION" : "ড্রাইভারদের অধিকাল ভাতার বিল কনফিগারেশন";

    Driver_ot_bill_configDTO driverOtBillConfigDTO = Driver_ot_bill_configDAO.getInstance().getDTOFromID(
            Driver_ot_bill_configRepository.configId
    );
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="driver-ot-bill-config-form">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <%--for data goes here--%>
                                    <input type="hidden" name="iD" value="<%=driverOtBillConfigDTO.iD%>">

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="ordinanceText">
                                            <%=isLanguageEnglish ? "Ordinance Description" : "আদেশের বিবরণ"%>
                                        </label>
                                        <div class="col-md-9">
                                            <textarea class="form-control"
                                                      name="ordinanceText"
                                                      id="ordinanceText"
                                                      style="resize: none"
                                                      rows="5"
                                                      maxlength="4096"
                                            ><%=driverOtBillConfigDTO.ordinanceText%></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=isLanguageEnglish ? "Office Time" : "অফিসের সময়"%>
                                        </label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div>
                                                        <label for="minOfficeHour">
                                                            <%=isLanguageEnglish ? "Morning" : "সকাল"%>
                                                        </label>
                                                    </div>
                                                    <div>
                                                        <select class='form-control rounded shadow-sm'
                                                                style="min-width: 8ch;"
                                                                name="minOfficeHour" id="minOfficeHour"
                                                        >
                                                            <%=DateTimeUtil.buildTimeDropDown(
                                                                    7, 12,
                                                                    driverOtBillConfigDTO.minOfficeHour,
                                                                    language,
                                                                    null,
                                                                    false
                                                            )%>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div>
                                                        <label for="maxOfficeHour">
                                                            <%=isLanguageEnglish ? "Afternoon" : "বিকাল"%>
                                                        </label>
                                                    </div>
                                                    <div>
                                                        <select class='form-control rounded shadow-sm'
                                                                style="min-width: 8ch;"
                                                                name="maxOfficeHour" id="maxOfficeHour"
                                                        >
                                                            <%=DateTimeUtil.buildTimeDropDown(
                                                                    12, 17,
                                                                    driverOtBillConfigDTO.maxOfficeHour,
                                                                    language,
                                                                    null,
                                                                    false
                                                            )%>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="minExtraHour">
                                            <%=isLanguageEnglish ? "Extra Hour Start (Morning)" : "অতিরিক্ত সময় শুরু (সকাল)"%>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control rounded shadow-sm' style="min-width: 8ch;"
                                                    name="minExtraHour" id="minExtraHour"
                                            >
                                                <%=DateTimeUtil.buildTimeDropDown(
                                                        5, 7,
                                                        driverOtBillConfigDTO.minExtraHour,
                                                        language,
                                                        null,
                                                        false
                                                )%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="maxExtraHour">
                                            <%=isLanguageEnglish ? "Extra Hour End (Afternoon/Night)" : "অতিরিক্ত সময় শেষ (বিকাল/রাত)"%>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control rounded shadow-sm' style="min-width: 8ch;"
                                                    name="maxExtraHour" id="maxExtraHour"
                                            >
                                                <%=DateTimeUtil.buildTimeDropDown(
                                                        12, 24,
                                                        driverOtBillConfigDTO.maxExtraHour,
                                                        language,
                                                        null,
                                                        false
                                                )%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="maxOvertimeHours">
                                            <%=isLanguageEnglish ? "Maximum number of hours of overtime paid" : "সর্বোচ্চ যত ঘন্টা অতিরিক্ত সময়ের টাকা পাবে"%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control'
                                                   id="maxOvertimeHours" name='maxOvertimeHours'
                                                   data-only-int="true"
                                                   value='<%=driverOtBillConfigDTO.maxOvertimeHours%>'>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-md-11">
                        <div class="form-actions text-right">
                            <button type="button" id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                    onclick="location.href = '<%=request.getHeader("referer")%>'">
                                <%=isLanguageEnglish ? "Cancel" : "বাতিল করুন"%>
                            </button>
                            <button type="button" id="submit-btn"
                                    class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    onclick="saveConfiguration()">
                                <%=isLanguageEnglish ? "Save" : "সেভ করুন"%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>


<script>
    function keyDownEvent(e) {
        return true === inputValidationForIntValue(e, $(this), <%=Integer.MAX_VALUE%>);
    }

    $(() => {
        document.querySelectorAll('input[data-only-int="true"]')
                .forEach(input => input.onkeydown = keyDownEvent);
    });

    const form = $('#driver-ot-bill-config-form');

    function saveConfiguration() {
        submitAjaxByData(form.serialize(), "Driver_ot_billServlet?actionType=ajax_saveConfig");
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }
</script>