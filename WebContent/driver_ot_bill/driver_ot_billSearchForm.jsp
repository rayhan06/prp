<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="overtime_allowance.*" %>
<%@ page import="util.*" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="overtime_bill.Overtime_billDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="util.StringUtils" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="overtime_bill.Overtime_billStatus" %>
<%@ page import="ot_employee_type.OT_employee_typeRepository" %>
<%@ page import="bill_approval_history.BillApprovalStatus" %>
<%@ page import="overtime_bill.Overtime_billServlet" %>
<%@ page import="driver_ot_bill.Driver_ot_billDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@page pageEncoding="UTF-8" %>

<%
    String navigator2 = "navOVERTIME_ALLOWANCE";
    String servletName = "Driver_ot_billServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead class="text-center">
        <tr>
            <th>
                <%=isLanguageEnglish ? "Bill Date (Type)" : "বিলের তারিখ (ধরন)"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Driver" : "গাড়িচালক"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Amount" : "পরিমাণ"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Preparer & Date" : "প্রস্তুতকারী ও সময়"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Approval Status" : "অনুমোদনের অবস্থা"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Action" : "কার্যকলাপ"%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Driver_ot_billDTO> data = (List<Driver_ot_billDTO>) recordNavigator.list;
            try {
                if (data != null) {
                    for (Driver_ot_billDTO driverOtBillDTO : data) {
                        BillApprovalStatus billApprovalStatus = BillApprovalStatus.getFromValue(driverOtBillDTO.approvalStatus);
        %>
        <tr>
            <td>
                <%=driverOtBillDTO.getBillDateTypeText(isLanguageEnglish)%>
            </td>
            <td>
                <%=Employee_recordsRepository.getInstance().getEmployeeName(driverOtBillDTO.employeeRecordId, isLanguageEnglish)%>
                <br>
                <%=OfficeUnitOrganogramsRepository.getInstance().getDesignation(Language, driverOtBillDTO.organogramId)%>
                <br>
                <%=Office_unitsRepository.getInstance().geText(Language, driverOtBillDTO.officeUnitId)%>
            </td>
            <td>
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        StringUtils.convertBanglaIfLanguageIsBangla(
                                Language,
                                String.format("%d", driverOtBillDTO.billAmount)
                        )
                )%>/-
            </td>
            <td>
                <%if (driverOtBillDTO.isInPreviewStage) {%>
                <div class="btn btn-sm border-0 shadow mt-2"
                     style="background-color: <%=billApprovalStatus.getColor()%>; color: white; border-radius: 8px;cursor: text">
                    <%=isLanguageEnglish ? "Draft" : "খসড়া"%>
                </div>
                <%} else {%>
                <%=driverOtBillDTO.preparedNameBn%><br>
                <%=StringUtils.getFormattedTime(false, driverOtBillDTO.preparedTime)%>
                <%}%>
            </td>
            <td>
                <%if (!driverOtBillDTO.isInPreviewStage) {%>
                <div class="btn btn-sm border-0 shadow mt-2"
                     style="background-color: <%=billApprovalStatus.getColor()%>; color: white; border-radius: 8px;cursor: text">
                    <%=billApprovalStatus.getName(isLanguageEnglish)%>
                </div>
                <%}%>
            </td>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=view&ID=<%=driverOtBillDTO.iD%>'"
                >
                    <i class="fa fa-eye"></i>
                </button>

                <%if (driverOtBillDTO.isEditable(userDTO, Language)) {%>
                <button
                        type="button"
                        class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=driverOtBillDTO.iD%>'"
                >
                    <i class="fa fa-edit"></i>
                </button>
                <%}%>
                <%if (driverOtBillDTO.isDeletableByUser(userDTO)) {%>
                <button type="button"
                        data-delete-btn
                        class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #ff6b6b;"
                        onclick="openDeleteWarning(<%=driverOtBillDTO.iD%>)"
                >
                    <i class="fa fa-trash"></i>
                </button>
                <%}%>
            </td>
        </tr>
        <%
                    }
                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<script>
    function openDeleteWarning(overtimeBillId) {
        const message = utilIsBangla ? "আপনি কি রেকর্ড মুছে ফেলার ব্যাপারে নিশ্চিত?"
                                     : "Are you sure you want to delete the record?";
        deleteDialog(
            message,
            () => deleteOvertimeBillDTO(overtimeBillId),
            () => {
            }
        );
    }

    async function deleteOvertimeBillDTO(id) {
        const url = '<%=servletName%>?actionType=ajax_deleteBill&ID=' + id;
        try {
            const res = await fetch(url, {method: 'POST'});
            const resJson = await res.json();
            console.log(resJson);
            if (resJson.success === true) {
                location.reload();
                return;
            }
            $('#toast_message').css('background-color', '#ff6063');
            showToast(resJson.message, resJson.message);
        } catch (error) {
            console.log(error);
            $('#toast_message').css('background-color', '#ff6063');
            showToast('<%=isLanguageEnglish? "Failed to delete" : "ডিলিট করতে ব্যর্থ হয়েছে"%>');
        }
    }
</script>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>