<%@ page pageEncoding="UTF-8" %>
<%@ page import="util.StringUtils" %>
<%@ page import="driver_ot_bill.DriverOtBillTableData" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="driver_ot_bill.DriverOtDetailModel" %>
<%@ page import="util.DateTimeUtil" %>
<%@ page import="driver_ot_bill.DriverOtDateType" %>
<%@ page import="com.google.gson.Gson" %>

<%
    String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    Gson gson = new Gson();
    DriverOtBillTableData driverOtBillTableData = (DriverOtBillTableData) request.getAttribute("driverOtBillTableData");
%>

<%if (driverOtBillTableData.errorMessage != null) {%>

<input type="hidden" name="isEditable" value="false">
<div id="no-data-found-div" class="no-data-found-div">
    <%=driverOtBillTableData.errorMessage%>
</div>

<%} else {%>

<input type="hidden" name="isEditable" value="<%=driverOtBillTableData.isEditable%>">

<style>
    #driver-ot-bill-table th {
        vertical-align: top;
        text-align: center;
    }

    #driver-ot-bill-table tbody td {
        vertical-align: middle;
        text-align: center;
    }

    .sticky-column {
        position: sticky;
        left: 0;
        background-color: white;
    }
</style>

<div class="table-responsive">
    <table class="table table-bordered table-bordered-custom text-nowrap" id="driver-ot-bill-table">
        <thead>
        <tr>
            <th class="sticky-column" rowspan="2">তারিখ</th>
            <th colspan="3">অফিস সময়</th>
            <th colspan="6">অতিরিক্ত সময়</th>
            <th>অতিরিক্ত মোট ঘন্টা</th>
        </tr>
        <tr>
            <th>সকাল</th>
            <th>বিকাল</th>
            <th>ঘন্টা</th>
            <th colspan="2">সকাল</th>
            <th>ঘন্টা</th>
            <th colspan="2">বিকাল</th>
            <th>ঘন্টা</th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        <%
            for (DriverOtDetailModel model : driverOtBillTableData.driverOtDetailModels) {
                String dayType = DriverOtDateType.getByValue(model.driverOtDateType).getBanglaText();
                if (!dayType.isEmpty()) {
                    dayType = String.format("(%s)", dayType);
                }
        %>
        <tr
                <%if (driverOtBillTableData.isEditable) {%>
                data-model='<%=gson.toJson(model)%>' class="has-driver-ot-detail-model"
                <%}%>
        >
            <td class="sticky-column">
                <%=StringUtils.getFormattedDate(isLangEn, model.driverOtDate)%> <%=dayType%>
            </td>

            <%--Office Hour--%>
            <td>
                <%if (driverOtBillTableData.isEditable) {%>
                <select class='form-control rounded shadow-sm' style="min-width: 8ch;"
                        name="officeHourFrom"
                        onchange="driverOtDetailModelChanged(this)"
                >
                    <%=DateTimeUtil.buildTimeDropDown(
                            driverOtBillTableData.minOfficeHour,
                            driverOtBillTableData.maxOfficeHour,
                            model.officeHourFrom,
                            language,
                            DriverOtDetailModel.nothingSelected,
                            false
                    )%>
                </select>
                <%} else {%>
                <%=StringUtils.convertBanglaIfLanguageIsBangla(
                        language,
                        String.valueOf(DateTimeUtil.getHourValue(model.officeHourFrom, false))
                )%>
                <%}%>
            </td>
            <td>
                <%if (driverOtBillTableData.isEditable) {%>
                <select class='form-control rounded shadow-sm' style="min-width: 8ch;"
                        name="officeHourTo"
                        onchange="driverOtDetailModelChanged(this)"
                >
                    <%=DateTimeUtil.buildTimeDropDown(
                            driverOtBillTableData.minOfficeHour,
                            driverOtBillTableData.maxOfficeHour,
                            model.officeHourTo,
                            language,
                            DriverOtDetailModel.nothingSelected,
                            false
                    )%>
                </select>
                <%} else {%>
                <%=StringUtils.convertBanglaIfLanguageIsBangla(
                        language,
                        String.valueOf(DateTimeUtil.getHourValue(model.officeHourTo, false))
                )%>
                <%}%>
            </td>
            <%--total Office Hour--%>
            <td data-total-hours="totalOfficeHour">
                <%=StringUtils.convertBanglaIfLanguageIsBangla(
                        language,
                        String.valueOf(model.totalOfficeHour)
                )%>
            </td>

            <%--Extra Hour Morning--%>
            <td>
                <%if (driverOtBillTableData.isEditable) {%>
                <select class='form-control rounded shadow-sm' style="min-width: 8ch;"
                        name="extraHourMorningFrom"
                        onchange="driverOtDetailModelChanged(this)"
                >
                    <%=DateTimeUtil.buildTimeDropDown(
                            driverOtBillTableData.minExtraHour,
                            DriverOtBillTableData.NOON,
                            model.extraHourMorningFrom,
                            language,
                            DriverOtDetailModel.nothingSelected,
                            false
                    )%>
                </select>
                <%} else {%>
                <%=StringUtils.convertBanglaIfLanguageIsBangla(
                        language,
                        String.valueOf(DateTimeUtil.getHourValue(model.extraHourMorningFrom, false))
                )%>
                <%}%>
            </td>
            <td>
                <%if (driverOtBillTableData.isEditable) {%>
                <select class='form-control rounded shadow-sm' style="min-width: 8ch;"
                        name="extraHourMorningTo"
                        onchange="driverOtDetailModelChanged(this)"
                >
                    <%=DateTimeUtil.buildTimeDropDown(
                            driverOtBillTableData.minExtraHour,
                            DriverOtBillTableData.NOON,
                            model.extraHourMorningTo,
                            language,
                            DriverOtDetailModel.nothingSelected,
                            false
                    )%>
                </select>
                <%} else {%>
                <%=StringUtils.convertBanglaIfLanguageIsBangla(
                        language,
                        String.valueOf(DateTimeUtil.getHourValue(model.extraHourMorningTo, false))
                )%>
                <%}%>
            </td>
            <%--total Extra Hour Morning--%>
            <td data-total-hours="totalExtraHourMorning">
                <%=StringUtils.convertBanglaIfLanguageIsBangla(
                        language,
                        String.valueOf(model.totalExtraHourMorning)
                )%>
            </td>

            <%--Extra Hour Afternon--%>
            <td>
                <%if (driverOtBillTableData.isEditable) {%>
                <select class='form-control rounded shadow-sm' style="min-width: 8ch;"
                        name="extraHourAfternoonFrom"
                        onchange="driverOtDetailModelChanged(this)"
                >
                    <%=DateTimeUtil.buildTimeDropDown(
                            DriverOtBillTableData.NOON,
                            driverOtBillTableData.maxExtraHour,
                            model.extraHourAfternoonFrom,
                            language,
                            DriverOtDetailModel.nothingSelected,
                            false
                    )%>
                </select>
                <%} else {%>
                <%=StringUtils.convertBanglaIfLanguageIsBangla(
                        language,
                        String.valueOf(DateTimeUtil.getHourValue(model.extraHourAfternoonFrom, false))
                )%>
                <%}%>
            </td>
            <td>
                <%if (driverOtBillTableData.isEditable) {%>
                <select class='form-control rounded shadow-sm' style="min-width: 8ch;"
                        name="extraHourAfternoonTo"
                        onchange="driverOtDetailModelChanged(this)"
                >
                    <%=DateTimeUtil.buildTimeDropDown(
                            DriverOtBillTableData.NOON,
                            driverOtBillTableData.maxExtraHour,
                            model.extraHourAfternoonTo,
                            language,
                            DriverOtDetailModel.nothingSelected,
                            false
                    )%>
                </select>
                <%} else {%>
                <%=StringUtils.convertBanglaIfLanguageIsBangla(
                        language,
                        String.valueOf(DateTimeUtil.getHourValue(model.extraHourAfternoonTo, false))
                )%>
                <%}%>
            </td>
            <%--total Extra Hour Afternoon--%>
            <td data-total-hours="totalExtraHourAfternoon">
                <%=StringUtils.convertBanglaIfLanguageIsBangla(
                        language,
                        String.valueOf(model.totalExtraHourAfternoon)
                )%>
            </td>

            <%--Total Extra Hour--%>
            <td data-total-hours="totalExtraHour">
                <%=StringUtils.convertBanglaIfLanguageIsBangla(
                        language,
                        String.valueOf(model.totalExtraHour)
                )%>
            </td>
        </tr>
        <%}%>
        </tbody>
    </table>
</div>
<%}%>
