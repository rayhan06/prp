<%@ page pageEncoding="UTF-8" %>
<%@ page import="driver_ot_bill.Driver_ot_billDTO" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="driver_ot_bill.Driver_ot_detailDAO" %>
<%@ page import="driver_ot_bill.DriverOtDetailModel" %>
<%@ page import="java.util.List" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="driver_ot_bill.DriverOtDateType" %>
<%@ page import="util.StringUtils" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="static driver_ot_bill.Driver_ot_billDTO.getRateStringForView" %>
<%@ page import="static util.StringUtils.convertToBanNumber" %>
<%@ page import="bill_approval_history.Bill_approval_historyDTO" %>
<%@ page import="java.util.Map" %>
<%@ page import="bill_approval_history.BillApprovalStatus" %>
<%@ page import="finance.FinanceUtil" %>
<%@ page import="java.util.TreeMap" %>
<%@ page import="bill_approval_history.Bill_approval_historyDAO" %>
<%@ page import="user.UserDTO" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberInWord" %>

<%
    boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;

    Driver_ot_billDTO driverOtBillDTO = (Driver_ot_billDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    List<DriverOtDetailModel> modelList = Driver_ot_detailDAO.getInstance().buildSortedModelFromBillDto(driverOtBillDTO);

    String totalExtraHourString;
    if (driverOtBillDTO.overtimeHours < driverOtBillDTO.maxOvertimeHours) {
        totalExtraHourString = "" + driverOtBillDTO.overtimeHours;
    } else {
        totalExtraHourString = String.format(
                "%d-%d=%d",
                driverOtBillDTO.overtimeHours,
                driverOtBillDTO.overtimeHours - driverOtBillDTO.maxOvertimeHours,
                driverOtBillDTO.maxOvertimeHours
        );
    }
    totalExtraHourString = convertToBanNumber(totalExtraHourString);

    String totalAmount = BangladeshiNumberFormatter.getFormattedNumber(convertToBanNumber(
            String.format("%d", driverOtBillDTO.billAmount))
    );
    String basicSalary = BangladeshiNumberFormatter.getFormattedNumber(convertToBanNumber(
            String.format("%d", driverOtBillDTO.basicSalary))
    );
    String overtimeDays = convertToBanNumber(String.format("%d", driverOtBillDTO.overtimeDays));
    String salaryPerDay = convertToBanNumber(getRateStringForView(driverOtBillDTO.getSalaryPerDay()));
    String maxTotalOfficeHours = convertToBanNumber(String.format("%d", driverOtBillDTO.getMaxTotalOfficeHours()));
    String hourlyRate = convertToBanNumber(getRateStringForView(driverOtBillDTO.getHourlyRate()));
    String convertedBillTotal = StringUtils.convertToBanNumber(String.format("%d", driverOtBillDTO.billAmount));
    String formattedBillTotal = BangladeshiNumberFormatter.getFormattedNumber(convertedBillTotal);

    TreeMap<Integer, Bill_approval_historyDTO> approvalHistoryBySortedLevel =
            Bill_approval_historyDAO.getInstance().getApprovalHistoryBySortedLevel(driverOtBillDTO, userDTO);
%>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    .page[data-size="A4-landscape"] {
        width: 297mm;
        height: 210mm;
        padding: .30in .40in;
        background: white;
        margin-bottom: 10px;
    }

    .page[data-size="A4-landscape-width"] {
        width: 297mm;
        padding: .30in .40in;
        background: white;
        margin-bottom: 10px;
    }

    .page {
        background: white;
        padding: .05in;
        page-break-after: always;
    }

    .table-bordered-custom th,
    .table-bordered-custom td {
        border: 1px solid #000;
        padding: 2px;
    }

    th {
        text-align: center;
    }

    .signature-image {
        width: 200px !important;
        height: 53px !important;
    }

    .signature-div {
        color: #a406dc;
        font-size: 10px !important;
    }

    .signature-div * {
        font-size: 10px !important;
    }

    @media print {
        @page {
            size: landscape;
            margin: .25in;
        }
    }

    div.bill-heading {
        font-weight: bold;
        font-size: 1.1rem;
    }

    td {
        white-space: nowrap;
    }

    .header-tbody td {
        text-align: center;
    }

    div.finance-serial-number {
        font-weight: bold;
        font-size: 1.8rem;
    }

    .signature-container-div {
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: space-between;
    }

    .no-top-border {
        border-top-color: white !important;
    }

    .no-bottom-border {
        border-bottom-color: white !important;
    }

    .align-top {
        vertical-align: top;
    }
</style>

<div class="loader-container-circle" id="full-page-loader">
    <div class="loader-circle"></div>
</div>

<div class="kt-content p-0" id="kt_content">
    <div class="">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title prp-page-title">
                        <%=isLangEn ? "Driver's Overtime Bill" : "গাড়ীচালকদের অধিকাল ভাতা"%>
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body page-bg" id="bill-div">

                <div class="ml-auto m-3">
                    <button type="button" class="btn" id='download-pdf'
                            onclick="printDivWithJqueryPrint('to-print-div');">
                        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                </div>

                <div style="margin: auto;">
                    <div class="container" id="to-print-div">
                        <%
                            int rowsPerPage = 18;
                            int index = 0, pageNumber = 1;
                            boolean isLastPage = false, isFirstPage = true;
                            while (index < modelList.size()) {
                        %>
                        <section class="page">
                            <div class="row ">
                                <div class="text-left col-4"></div>
                                <div class="col-4 text-center">
                                    -<%=convertToBanNumber(String.valueOf(pageNumber))%>-
                                </div>
                                <div class="col-4 text-right finance-serial-number">
                                    <%--financeSerialNumber--%>
                                </div>
                            </div>
                            <%if (isFirstPage) {%>
                            <div class="text-center">
                                <div class="bill-heading">বাংলাদেশ জাতীয় সংসদ সচিবালয়</div>
                                <div class="bill-heading">
                                    পরিবহন শাখা
                                </div>
                                <div class="bill-heading">
                                    গাড়ীচালকদের ওভারটাইম বিল
                                </div>
                            </div>
                            <p class="mt-2">
                                <%=driverOtBillDTO.ordinanceText%> <%=driverOtBillDTO.vehicleNumberBn%> নম্বর
                                <%=driverOtBillDTO.vehicleTypeBn%> এর চালক <%=driverOtBillDTO.employeeNameBn%>-এর
                                <%=driverOtBillDTO.getBillDateRangeText(false)%> তারিখের অতিরিক্ত খাটুনির বিল।
                            </p>
                            <%}%>

                            <div class="mt-3">
                                <table id="overtime-bill-table" class="table-bordered-custom w-100">
                                    <tbody class="header-tbody">
                                    <tr>
                                        <td rowspan="3">তারিখ</td>
                                        <td colspan="3">অফিস সময়</td>
                                        <td colspan="6">অতিরিক্ত সময়</td>
                                        <td rowspan="3">মোট টাকার পরিমাণ</td>
                                        <td rowspan="3">মন্তব্য</td>
                                    </tr>
                                    <tr>
                                        <td>হতে</td>
                                        <td>পর্যন্ত</td>
                                        <td rowspan="2">অফিস সময়ে<br>মোট ঘন্টা</td>
                                        <td colspan="4">অফিস সময়ের বাইরে অতিরিক্ত সময়</td>
                                        <td rowspan="2">অতিরিক্ত মোট ঘন্টা</td>
                                        <td class="text-center">অতিরিক্ত ঘন্টার হার</td>
                                    </tr>
                                    <tr>
                                        <td>সকাল</td>
                                        <td>বিকাল</td>
                                        <td colspan="2">সকাল</td>
                                        <td colspan="2">বিকাল</td>
                                        <td>
                                            মূলবেতন <%=basicSalary%>÷<%=overtimeDays%>=<br>
                                            <%=salaryPerDay%>÷<%=maxTotalOfficeHours%>=<%=hourlyRate%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>১</td>
                                        <td>২</td>
                                        <td>৩</td>
                                        <td>৪</td>
                                        <td colspan="2">৫</td>
                                        <td colspan="2">৬</td>
                                        <td>৭</td>
                                        <td>৮</td>
                                        <td>৯</td>
                                        <td>১০</td>
                                    </tr>
                                    </tbody>

                                    <tbody>
                                    <%
                                        int rowsInThisPage = 0;
                                        while (index < modelList.size() && rowsInThisPage < rowsPerPage) {
                                            isLastPage = (index == (modelList.size() - 1));
                                            boolean isLastRowOfPage = isLastPage || rowsInThisPage == (rowsPerPage - 1);
                                            boolean isFirstRowOfPage = rowsInThisPage == 0;
                                            rowsInThisPage++;
                                            DriverOtDetailModel model = modelList.get(index++);
                                            String dayType = DriverOtDateType.getByValue(model.driverOtDateType).getBanglaText();
                                            if (!dayType.isEmpty()) {
                                                dayType = String.format("(%s)", dayType);
                                            }
                                    %>
                                    <tr>
                                        <td>
                                            <%=StringUtils.getFormattedDate(isLangEn, model.driverOtDate)%> <%=dayType%>
                                        </td>
                                        <td class="text-center">
                                            <%=DriverOtDetailModel.getHourValueForView(model.officeHourFrom, "bangla")%>
                                        </td>
                                        <td class="text-center">
                                            <%=DriverOtDetailModel.getHourValueForView(model.officeHourTo, "bangla")%>
                                        </td>
                                        <td class="text-center">
                                            <%=DriverOtDetailModel.getHourValueForView(model.totalOfficeHour, "bangla")%>
                                        </td>
                                        <td class="text-center">
                                            <%=DriverOtDetailModel.getHourRangeForView(
                                                    model.extraHourMorningFrom, model.extraHourMorningTo,
                                                    "bangla"
                                            )%>
                                        </td>
                                        <td class="text-center">
                                            <%=DriverOtDetailModel.getHourValueForView(model.totalExtraHourMorning, "bangla")%>
                                        </td>
                                        <td class="text-center">
                                            <%=DriverOtDetailModel.getHourRangeForView(
                                                    model.extraHourAfternoonFrom, model.extraHourAfternoonTo,
                                                    "bangla"
                                            )%>
                                        </td>
                                        <td class="text-center">
                                            <%=DriverOtDetailModel.getHourValueForView(model.totalExtraHourAfternoon, "bangla")%>
                                        </td>
                                        <td class="text-center">
                                            <%=DriverOtDetailModel.getHourValueForView(model.totalExtraHour, "bangla")%>
                                        </td>
                                        <td class="no-top-border <%=isLastRowOfPage?"" : "no-bottom-border"%>"></td>
                                        <td class="no-top-border <%=isLastRowOfPage?"" : "no-bottom-border"%>"></td>
                                        <%if (isFirstRowOfPage) {%>
                                        <td rowspan="100%" class="align-top">
                                            প্রত্যয়ন করা যাচ্ছে যে<br>
                                            <%=driverOtBillDTO.vehicleNumberBn%> নম্বর <br>
                                            <%=driverOtBillDTO.vehicleTypeBn%> এর চালক <%=driverOtBillDTO.employeeNameBn%><br>
                                            <%=driverOtBillDTO.getBillDateRangeText(false)%> তারিখ পর্যন্ত<br>
                                            ডিউটি করেছেন বিধায় তার অতিরিক্ত<br>
                                            খাটুনি ভাতা প্রদান করা যেতে পারে<br>
                                            <%--prepared by--%>
                                            <%if (!driverOtBillDTO.isInPreviewStage) {%>
                                            <div class="text-center">
                                                <div class="signature-div">
                                                    <div>
                                                        <img class="signature-image"
                                                             src='<%=StringUtils.getBase64EncodedImageStr(driverOtBillDTO.preparedSignature)%>'
                                                             alt=""/>
                                                        <br>
                                                        <%=StringUtils.getFormattedTime(false, driverOtBillDTO.preparedTime)%>
                                                    </div>
                                                    <div>
                                                        <%=driverOtBillDTO.preparedNameBn%><br>
                                                        <%=driverOtBillDTO.preparedOrgNameBn%><br>
                                                        <%=driverOtBillDTO.preparedOfficeNameBn%><br>
                                                    </div>
                                                </div>
                                            </div>
                                            <%}%>
                                        </td>
                                        <%}%>
                                    </tr>
                                    <%}%>
                                    </tbody>
                                    <%if (isLastPage) {%>
                                    <tfoot>
                                    <tr>
                                        <td colspan="8" class="text-right">মোট</td>
                                        <td class="text-center">
                                            <%=totalExtraHourString%>
                                        </td>
                                        <td class="no-top-border"></td>
                                        <td class="text-center">
                                            <%=totalAmount%>
                                        </td>
                                        <td></td>
                                    </tr>
                                    </tfoot>
                                    <%}%>
                                </table>
                            </div>
                            <%if (isLastPage) {%>
                            <div class="text-center w-100 mt-2">
                                টাঃ=<%=formattedBillTotal%>/- (<%=BangladeshiNumberInWord.convertToWord(convertedBillTotal)%>)
                                <br>
                                টাকা পাশ করা হলো।
                            </div>
                            <%--Signature Div--%>
                            <div class="row col-12">
                                <div class="signature-container-div">
                                    <%--prepared by--%>
                                    <div class="text-center">
                                        <div class="signature-div">
                                            <%if (!driverOtBillDTO.isInPreviewStage) {%>
                                            <div>
                                                <img class="signature-image"
                                                     src='<%=StringUtils.getBase64EncodedImageStr(driverOtBillDTO.preparedSignature)%>'
                                                     alt=""/>
                                                <br>
                                                <%=StringUtils.getFormattedTime(false, driverOtBillDTO.preparedTime)%>
                                            </div>
                                            <div>
                                                <%=driverOtBillDTO.preparedNameBn%><br>
                                                <%=driverOtBillDTO.preparedOrgNameBn%><br>
                                                <%=driverOtBillDTO.preparedOfficeNameBn%><br>
                                            </div>
                                            <%}%>
                                        </div>
                                    </div>
                                    <%
                                        for (Map.Entry<Integer, Bill_approval_historyDTO> entry : approvalHistoryBySortedLevel.entrySet()) {
                                            Integer level = entry.getKey();
                                            Bill_approval_historyDTO approvalHistoryDTO = entry.getValue();
                                    %>
                                    <div class="text-center">
                                        <%if (approvalHistoryDTO != null) {%>
                                        <div class="signature-div">
                                            <%if (!driverOtBillDTO.isInPreviewStage) {%>
                                            <%if (approvalHistoryDTO.billApprovalStatus == BillApprovalStatus.APPROVED) {%>
                                            <div>
                                                <img class="signature-image"
                                                     src='<%=StringUtils.getBase64EncodedImageStr(approvalHistoryDTO.signature)%>'
                                                     alt=""/>
                                                <br>
                                                <%=StringUtils.getFormattedTime(false, approvalHistoryDTO.approvalTime)%>
                                            </div>
                                            <%} else if (approvalHistoryDTO.isThisUsersHistory(userDTO)) {%>
                                            <button id="layer1-approve-btn"
                                                    class="btn-sm shadow text-white border-0 submit-btn mt-3 btn-border-radius"
                                                    type="button"
                                                    onclick="approveApplication(<%=driverOtBillDTO.iD%>,<%=level%>)">
                                                <%=isLangEn ? "Approve" : "অনুমোদন করুন"%>
                                            </button>
                                            <%} else {%>
                                            <div class="btn btn-sm border-0 shadow mt-2"
                                                 style="background-color: #bb0a0a; color: white; border-radius: 8px;cursor: text">
                                                <%=isLangEn ? "Waiting for Approval" : "অনুমোদনের অপেক্ষায়"%>
                                            </div>
                                            <%}%>
                                            <%}%>
                                            <div>
                                                <%=approvalHistoryDTO.nameBn%><br>
                                                <%=approvalHistoryDTO.orgNameBn%><br>
                                                <%=approvalHistoryDTO.officeNameBn%><br>
                                            </div>
                                        </div>
                                        <%}%>
                                    </div>
                                    <%}%>
                                </div>
                            </div>

                            <div class="row col-12 mt-5">
                                <div class="col-4 text-center"></div>
                                <div class="col-4 text-center">
                                    <%=FinanceUtil.getFinance1HeadDesignation("Bangla")%><br>
                                    বাংলাদেশ জাতীয় সংসদ সচিবালয়
                                </div>
                                <div class="col-4 text-center"></div>
                            </div>
                            <%}%>
                        </section>
                        <%
                                isFirstPage = false;
                                ++pageNumber;
                            }
                        %>
                    </div>

                    <%if (driverOtBillDTO.isInPreviewStage) {%>
                    <%--Action Button Div--%>
                    <div id="action-btn-div" class="row">
                        <div class="col-12 mt-3 text-right">
                            <button id="edit-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button"
                                    onclick="location.href = 'Driver_ot_billServlet?actionType=getEditPage&ID=<%=driverOtBillDTO.iD%>'">
                                <%=isLangEn ? "Edit" : "এডিট করুন"%>
                            </button>
                            <button id="submit-btn"
                                    class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                                    type="button" onclick="submitDriverOvertimeBill()">
                                <%=isLangEn ? "Submit" : "জমা দিন"%>
                            </button>
                        </div>
                    </div>
                    <%}%>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="../utility/jquery_print.jsp"/>

<script>
    const fullPageLoader = $('#full-page-loader');


    function setButtonDisableState(value) {
        $('#submit-btn').prop('disabled', value);
        $('#edit-btn').prop('disabled', value);
    }

    function submitDriverOvertimeBill() {
        const data = {
            id: <%=driverOtBillDTO.iD%>
        };
        console.log({data});
        setButtonDisableState(true);
        fullPageLoader.show();
        $.ajax({
            type: "POST",
            url: "Driver_ot_billServlet?actionType=ajax_submit",
            data: data,
            dataType: 'JSON',
            success: function (response) {
                setButtonDisableState(false);
                fullPageLoader.hide();
                if (response.success) {
                    window.location.assign(getContextPath() + response.message);
                } else {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.message, response.message);
                }
            },
            error: function () {
                setButtonDisableState(false);
                fullPageLoader.hide();
                $('#toast_message').css('background-color', '#ff6063');
                showToastSticky("সাবমিট করতে ব্যর্থ হয়েছে", "Failed to submit");
            }
        });
    }

    function approveApplication(id, level) {
        setButtonDisableState(true);
        let msg = '<%=isLangEn? "Are you sure to approve application?" : "আপনি কি আবেদন অনুমোদনের ব্যাপারে নিশ্চিত?"%>';
        let confirmButtonText = '<%=StringUtils.getYesNo(Language, true)%>';
        let cancelButtonText = '<%=StringUtils.getYesNo(Language, false)%>';
        messageDialog('', msg, 'success', true, confirmButtonText, cancelButtonText,
            () => {
                approveDriverOtBill(id, level);
            }, () => {
                setButtonDisableState(false);
            }
        );
    }

    function approveDriverOtBill(id, level) {
        const data = {
            id: id, level: level
        };

        $.ajax({
            type: "POST",
            url: "Driver_ot_billServlet?actionType=ajax_approve",
            data: data,
            dataType: 'JSON',
            success: function (response) {
                setButtonDisableState(false);
                if (response.success) {
                    location.reload();
                } else {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToast(response.message, response.message);
                }
            },
            error: function (/*jqXHR, textStatus, errorThrown*/) {
                $('#toast_message').css('background-color', '#ff6063');
                showToast("সার্ভারে সমস্যা", "Server Error");
                setButtonDisableState(false);
            }
        });
    }
</script>
