<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="payroll_deduction_type.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="common.BaseServlet" %>

<%
    Payroll_deduction_typeDTO payroll_deduction_typeDTO;
    payroll_deduction_typeDTO = (Payroll_deduction_typeDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    CommonDTO commonDTO = payroll_deduction_typeDTO;
    if (payroll_deduction_typeDTO == null) {
        payroll_deduction_typeDTO = new Payroll_deduction_typeDTO();

    }
    String tableName = "payroll_deduction_type";
    String context = request.getContextPath() + "/";
%>
<%@include file="../pb/addInitializer.jsp" %>
<%
    String formTitle = LM.getText(LC.PAYROLL_DEDUCTION_TYPE_ADD_PAYROLL_DEDUCTION_TYPE_ADD_FORMNAME, loginDTO);
    String servletName = "Payroll_deduction_typeServlet";
    boolean isLangEng="English".equalsIgnoreCase(Language);
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              id="bigform" name="bigform" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=payroll_deduction_typeDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PAYROLL_DEDUCTION_TYPE_ADD_CODE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='code' id='code_text_<%=i%>'
                                                   value='<%=payroll_deduction_typeDTO.code%>'
                                                   placeholder="<%=isLangEng?"Enter Code":"কোড লিখুন"%>" tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PAYROLL_DEDUCTION_TYPE_ADD_DESCRIPTIONEN, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='englishOnly form-control rounded' name='descriptionEn' required="required"
                                                   id='descriptionEn_text_<%=i%>'
                                                   value='<%=payroll_deduction_typeDTO.descriptionEn%>'
                                                   placeholder="<%=isLangEng?"Type in English":"ইংরেজিতে লিখুন"%>" tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PAYROLL_DEDUCTION_TYPE_ADD_DESCRIPTIONBN, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='noEnglish form-control rounded'
                                                   required="required" name='descriptionBn'
                                                   id='descriptionBn_text_<%=i%>'
                                                   value='<%=payroll_deduction_typeDTO.descriptionBn%>'
                                                   placeholder="<%=isLangEng?"Type in Bangla":"বাংলাতে লিখুন"%>" tag='pb_html'/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button">
                                <%=LM.getText(LC.PAYROLL_DEDUCTION_TYPE_ADD_PAYROLL_DEDUCTION_TYPE_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button" onclick="submitPayrollDeductionTypeForm()">
                                <%=LM.getText(LC.PAYROLL_DEDUCTION_TYPE_ADD_PAYROLL_DEDUCTION_TYPE_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">

    const bigForm = $('#bigform');
    const cancelBtn = $('#cancel-btn');
    const submitBtn = $('#submit-btn');
    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';
    function submitPayrollDeductionTypeForm() {
        if (bigForm.valid()) {
            setButtonState(true);
            $.ajax({
                type: "POST",
                url: "Payroll_deduction_typeServlet?actionType=ajax_<%=actionName%>&isPermanentTable=true&id=<%=ID%>",
                data: bigForm.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        setButtonState(false);
                        console.log("response code 0");
                    } else if (response.responseCode === 200) {
                        console.log("repsonse code 200");
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    setButtonState(false);
                }
            });
        }
    }

    function setButtonState(value) {
        cancelBtn.prop("disabled", value);
        submitBtn.prop("disabled", value);
    }

    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Payroll_deduction_typeServlet");
    }

    function init(row) {
        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                code: {
                    required: true,
                },
                descriptionEn: {
                    required: true,
                },
                descriptionBn: {
                    required: true,
                },
            },

            messages: {
                code: isLangEng ? "Please Enter Code" : "অনুগ্রহ করে কোড প্রবেশ করুন",
                descriptionEn: isLangEng ? "Please Enter Description in English" : "অনুগ্রহ করে ইংরেজি বিবরণ প্রবেশ করুন",
                descriptionBn: isLangEng ? "Please Enter Description in Bangla" : "অনুগ্রহ করে বাংলা বিবরণ প্রবেশ করুন",
            }
        });

    }
    $(".noEnglish").keypress(function(event){
        var ew = event.which;
        if(ew == 32)
            return true;
        if(48 <= ew && ew <= 57)
            return false;
        if(65 <= ew && ew <= 90)
            return false;
        if(97 <= ew && ew <= 122)
            return false;
        return true;
    });

    $(".englishOnly").keypress(function(event){
        var ew = event.which;
        if(ew === 32)
            return true;
        if(48 <= ew && ew <= 57)
            return true;
        if(65 <= ew && ew <= 90)
            return true;
        if(97 <= ew && ew <= 122)
            return true;
        return "#,.;:(){}/-&|+-".includes(String.fromCharCode(ew));
    });

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });



</script>






