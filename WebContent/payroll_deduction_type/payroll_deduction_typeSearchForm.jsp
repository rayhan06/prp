<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="payroll_deduction_type.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>


<%
    String navigator2 = "navPAYROLL_DEDUCTION_TYPE";
    String servletName = "Payroll_deduction_typeServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.PAYROLL_DEDUCTION_TYPE_ADD_CODE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PAYROLL_DEDUCTION_TYPE_ADD_DESCRIPTIONEN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PAYROLL_DEDUCTION_TYPE_ADD_DESCRIPTIONBN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PAYROLL_DEDUCTION_TYPE_SEARCH_PAYROLL_DEDUCTION_TYPE_EDIT_BUTTON, loginDTO)%>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Payroll_deduction_typeDTO> data = (List<Payroll_deduction_typeDTO>) recordNavigator.list;

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Payroll_deduction_typeDTO payroll_deduction_typeDTO = data.get(i);


        %>
        <tr>


            <td>
                <%=Utils.getDigits(payroll_deduction_typeDTO.code, Language)%>
            </td>

            <td>
                <%=payroll_deduction_typeDTO.descriptionEn%>
            </td>

            <td>
                <%=payroll_deduction_typeDTO.descriptionBn%>
            </td>


            <%CommonDTO commonDTO = payroll_deduction_typeDTO; %>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=commonDTO.iD%>'"
                >
                    <i class="fa fa-edit"></i>
                </button>
            </td>
        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			