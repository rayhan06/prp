
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="employee_attendance.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@ page import="java.util.*" %>
<%@ page import="holiday.HolidayRepository" %>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
boolean showApproval = Employee_attendanceDAO.hasPermissionToSearchOfficeAndEmployee(userDTO);
String failureMessage = (String)request.getAttribute("failureMessage");
String successMessage = (String) request.getSession(true).getAttribute("success_message");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";
}


	String fromDate = (String)request.getAttribute("fromDate");
	String toDate = (String)request.getAttribute("toDate");
	String month = (String)request.getAttribute("month");
	String year = (String)request.getAttribute("year");
	String employeeRecordsId = (String)request.getAttribute("employeeRecordsId");
	String decentEmployeeRecordsId = (String) request.getAttribute("decentEmployeeRecordsId");
	String searchReportType = (String)request.getAttribute("searchReportType");
	String searchDate = (String)request.getAttribute("searchDate");
	String officeUnitsId = (String) request.getAttribute("officeUnitsId");

	boolean showEntry = true;
	boolean showOut = true;
	if(searchReportType != null && searchReportType.equals("1")) {
		showOut = false;
	} else if(searchReportType != null && searchReportType.equals("2")) {
		showEntry = false;
	}

String navigator2 = SessionConstants.NAV_EMPLOYEE_ATTENDANCE;
System.out.println("navigator2 = " + navigator2);
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
System.out.println("rn2 = " + rn2);
String pageno2 = ( rn2 == null ) ? "1" : "" + rn2.getCurrentPageNo();
String totalpage2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalPages();
String totalRecords2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalRecords();
String lastSearchTime = ( rn2 == null ) ? "0" : "" + rn2.getSearchTime();
boolean isPermanentTable = true;
String tableName = "";


String ajax = request.getParameter("ajax");
boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
    List<AttendanceReportDetails>  employeeAttendanceReportDetails = (List<AttendanceReportDetails>) request.getAttribute("employeeAttendanceReportDetails");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<%if(employeeAttendanceReportDetails!=null && employeeAttendanceReportDetails.size()>0) {%>
<a class="download-excel-link btn btn-sm btn-success shadow btn-border-radius mb-3 pull-right" href="Employee_attendanceServlet?actionType=downloadAttendanceReport&officeUnitsId=<%=officeUnitsId%>&searchDate=<%=searchDate%>&decentEmployeeRecordsId=<%=decentEmployeeRecordsId%>&searchReportType=<%=searchReportType%>&fromDate=<%=fromDate%>&toDate=<%=toDate%>&month=<%=month%>&year=<%=year%>&employeeRecordsId=<%=employeeRecordsId%>"><%=LM.getText(LC.ATTENDANCE_DOWNLOAD_EXCEL, loginDTO)%></a>
<%}%>
                <%if(searchReportType!=null && searchReportType.equals("4")) { %>
<div class="table-responsive">
	<table id="tableData2" class="table table-bordered table-striped text-nowrap">
		<thead>
		<tr>
			<th><%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_EMPLOYEERECORDSTYPE, loginDTO)%></th>
			<th><%=LM.getText(LC.HM_DESIGNATION, loginDTO)%></th>
			<th><%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_PRESENT, loginDTO)%></th>
			<th><%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_ABSENT, loginDTO)%></th>
			<%if(showApproval==true) { %>
			<%--<th>
				<div class="download-excel-link" style="text-align: center" onclick="openApprovalModal()" ><%=LM.getText(LC.EMPLOYEE_ATTENDANCE_SEARCH_APPROVE, loginDTO)%></div>
			</th>--%>
			<th class="">
				<button type="button" class="btn btn-sm border-0 shadow" style="background-color: #22ccc1; color: white; border-radius: 8px"
						onclick="openApprovalModal()">

					<%=LM.getText(LC.EMPLOYEE_ATTENDANCE_SEARCH_APPROVE, loginDTO)%>
				</button>
			</th>
			<% }%>

			<%--<th onclick="openApprovalModal()"><%=LM.getText(LC.EMPLOYEE_ATTENDANCE_SEARCH_APPROVE, loginDTO)%></th>--%>
			<%--<th><input type="submit" class="btn btn-sm btn-green"
					   value="<%=LM.getText(LC.EMPLOYEE_ATTENDANCE_SEARCH_APPROVE, loginDTO)%>"/></th>--%>
		</tr>
		</thead>
		<tbody>
		<%

			try
			{

				if (employeeAttendanceReportDetails != null)
				{
					int size = employeeAttendanceReportDetails.size();
					System.out.println("data not null and size = " + size + " data = " + employeeAttendanceReportDetails);
					for (int i = 0; i < size; i++)
					{
						AttendanceReportDetails attendanceReportDetails = employeeAttendanceReportDetails.get(i);


		%>
		<tr id = 'tr_<%=i%>'>
			<%

			%>


			<%
				request.setAttribute("attendanceReportDetails",attendanceReportDetails);
				request.setAttribute("month", month);
				request.setAttribute("year", year);
				request.setAttribute("showApproval",showApproval);

			%>

			<jsp:include page="./employee_attendanceOvertimeSearchRow.jsp">
				<jsp:param name="pageName" value="searchrow" />
				<jsp:param name="rownum" value="<%=i%>" />
			</jsp:include>


			<%

			%>
		</tr>
		<%
					}

					System.out.println("printing done");
				}
				else
				{
					System.out.println("data  null");
				}
			}
			catch(Exception e)
			{
				System.out.println("JSP exception " + e);
			}
		%>



		</tbody>

	</table>
</div>
                <%} else if(employeeAttendanceReportDetails.size()>0) { %>
<div class="table-responsive">
	<table id="tableData" class="table table-bordered table-striped text-nowrap">
		<thead>
		<tr>
			<th><%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_EMPLOYEERECORDSTYPE, loginDTO)%></th>
			<th><%=LM.getText(LC.HM_DATE, loginDTO)%></th>
			<%if(showEntry) {%>
			<th><%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_INTIME, loginDTO)%></th>
			<% } %>
			<%if(showOut) {%>
			<th><%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_OUTTIME, loginDTO)%></th>
			<% } %>
			<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>
			<% if(showApproval) { %>
			<th class="">
					<button type="button" class="btn btn-sm border-0 shadow" style="background-color: #68cc6c; color: white; border-radius: 8px" onclick="openLateApprovalModal()">
						<i class="fas fa-check"></i>&nbsp;
						<%=LM.getText(LC.EMPLOYEE_ATTENDANCE_SEARCH_APPROVE, loginDTO)%>
					</button>
			</th>
			<%--<th><input type="submit" class="btn btn-sm btn-green"
					   value="<%=LM.getText(LC.EMPLOYEE_ATTENDANCE_SEARCH_APPROVE, loginDTO)%>"/></th>--%>
			<% } %>

		</tr>
		</thead>
		<tbody>
		<%

			try
			{

				if (employeeAttendanceReportDetails != null)
				{
					int size = employeeAttendanceReportDetails.size();
					System.out.println("data not null and size = " + size + " data = " + employeeAttendanceReportDetails);
					for (int i = 0; i < size; i++)
					{
						AttendanceReportDetails attendanceReportDetails = employeeAttendanceReportDetails.get(i);


		%>
		<tr id = 'tr_<%=i%>'>
			<%

			%>


			<%
				request.setAttribute("attendanceReportDetails",attendanceReportDetails);
				request.setAttribute("showEntry",showEntry);
				request.setAttribute("showOut",showOut);
				request.setAttribute("showApproval",showApproval);

			%>

			<jsp:include page="./employee_attendanceSearchRow.jsp">
				<jsp:param name="pageName" value="searchrow" />
				<jsp:param name="rownum" value="<%=i%>" />
			</jsp:include>


			<%

			%>
		</tr>
		<%
					}

					System.out.println("printing done");
				}
				else
				{
					System.out.println("data  null");
				}
			}
			catch(Exception e)
			{
				System.out.println("JSP exception " + e);
			}
		%>



		</tbody>

	</table>
</div>
                <%} %>


<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />

<script>

	$(document).ready(function()  {
		<%if(successMessage!=null && !successMessage.isEmpty()) {%>
		      showToast('Employee attendance added successfully', 'Employee attendance added successfully');
		<%}%>

	});


</script>