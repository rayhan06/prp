<%@page pageEncoding="UTF-8" %>

<%@page import="employee_attendance.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="util.*" %>
<%@ page import="employee_records.Employee_recordsDAO" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EMPLOYEE_ATTENDANCE_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

    boolean isPermanentTable = true;
    System.out.println("isPermanentTable = " + isPermanentTable);
    AttendanceReportDetails attendanceReportDetails = (AttendanceReportDetails) request.getAttribute("attendanceReportDetails");
    String servletName = "Employee_attendanceServlet";
    boolean showEntry = (boolean) request.getAttribute("showEntry");
    boolean showOut = (boolean) request.getAttribute("showOut");
    boolean showApproval = (boolean) request.getAttribute("showApproval");
    int i = 0;
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_employeeRecordsType'>

    <%=Employee_recordsDAO.getEmployeeName(attendanceReportDetails.employeeRecordsId, Language)%>

</td>

<td id='<%=i%>_inTimestamp'>
    <%
        value = attendanceReportDetails.date + "";
        String formatted_inTimestamp = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_inTimestamp, Language)%>


</td>
<%
    if (attendanceReportDetails.isHoliday) {
        value = attendanceReportDetails.holidayNote;
        if (Language.equals("Bangla") && value.equals("Holiday"))
            value = "ছুটি";
    } else if (attendanceReportDetails.isOnLeave) {
        value = attendanceReportDetails.leaveNote;
        if (Language.equals("Bangla") && value.equals("On Leave"))
            value = "ছুটিতে";
    } else if (attendanceReportDetails.isAbsent) {
        value = (Language.equals("English")) ? "Absent" : "অনুপস্থিত";
    } else {
        value = attendanceReportDetails.inTime + "";
        value = TimeFormat.getInAmPmFormat(value);
    }

%>
<%if (showEntry) { %>
<td id='<%=i%>_inTime'
    style="text-align: center; background-color: <%=attendanceReportDetails.entryCellBackgroundColor%>">
    <%=Utils.getDigits(value, Language)%>
</td>
<% } %>

<%
    value = attendanceReportDetails.outTime + "";
    String formatted_outTimestamp = TimeFormat.getInAmPmFormat(value);
%>
<%if (showOut) { %>
<td id='<%=i%>_outTime'
    style="text-align: center; background-color: <%=attendanceReportDetails.outCellBackgroundColor%>">
    <%=Utils.getDigits(formatted_outTimestamp, Language)%>
</td>
<% } %>


<td>
    <%
        String attendanceStatus = "";
        if (attendanceReportDetails.isHoliday) attendanceStatus = "Holiday";
        else if (attendanceReportDetails.isOnLeave) attendanceStatus = "OnLeave";
        else if (attendanceReportDetails.isAbsent) attendanceStatus = "Absent";
    %>
<%--    <a href='Employee_attendanceServlet?actionType=view&employeeRecordsId=<%=attendanceReportDetails.employeeRecordsId%>&date=<%=attendanceReportDetails.date%>&attendanceStatus=<%=attendanceStatus%>'><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
    </a>--%>
    <button type="button" class="btn btn-sm border-0 shadow" style="background-color: #22ccc1; color: white; border-radius: 8px"
            onclick="location.href='Employee_attendanceServlet?actionType=view&employeeRecordsId=<%=attendanceReportDetails.employeeRecordsId%>&date=<%=attendanceReportDetails.date%>&attendanceStatus=<%=attendanceStatus%>'">
        <i class="fa fa-plus"></i>&nbsp;
        <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
    </button>

</td>
<%if(showApproval) { %>
<td id='<%=i%>_checkbox'>
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID'
                                     value='<%=attendanceReportDetails.employeeRecordsId%>_<%=attendanceReportDetails.date%>'/></span>
    </div>
</td>
<% } %>



