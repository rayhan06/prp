<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%
    String url = "Employee_attendanceServlet?actionType=search";
    String navigator = SessionConstants.NAV_EMPLOYEE_ATTENDANCE;
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.APPOINTMENT_EDIT_LANGUAGE, loginDTO);
    String pageno = "";

    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();
    boolean isPermanentTable = false;

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    int pagination_number = 0;
%>
<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">
            <i class="fa fa-gift" ></i>&nbsp;
            &nbsp; <%=LM.getText(LC.ATTENDANCE_EMPLOYEE_ATTENDANCE_SEARCH, loginDTO)%>
        </h3>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content" style="background: white">
    <div class="row shadow-none border-0">
        <div class="col-lg-12">
            <jsp:include page="./employee_attendanceNav.jsp" flush="true">
                <jsp:param name="url" value="<%=url%>"/>
                <jsp:param name="navigator" value="<%=navigator%>"/>
                <jsp:param name="pageName"
                           value="<%=LM.getText(LC.EMPLOYEE_ATTENDANCE_SEARCH_EMPLOYEE_ATTENDANCE_SEARCH_FORMNAME, loginDTO)%>"/>
            </jsp:include>
<%--            <div style="height: 1px; background: #ecf0f5"></div>--%>
            <div class="kt-portlet shadow-none">
                <div class="kt-portlet__body">
                    <form action="Employee_attendanceServlet?isPermanentTable=<%=isPermanentTable%>&actionType=approve" method="POST"
                          id="tableForm" enctype="multipart/form-data">
                        <jsp:include page="employee_attendanceSearchForm.jsp" flush="true">
                            <jsp:param name="pageName"
                                       value="<%=LM.getText(LC.EMPLOYEE_ATTENDANCE_SEARCH_EMPLOYEE_ATTENDANCE_SEARCH_FORMNAME, loginDTO)%>"/>
                        </jsp:include>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="attendance-approval-modal" style="display:none;">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h4 class="caption table-title">
                    <%=LM.getText(LC.ATTENDANCE_APPROVE_EMPLOYEE_ATTENDANCE, loginDTO)%>
                </h4>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body modal-xl">
                <form action="Employee_attendanceServlet?actionType=approveAttendance" method="POST"
                      id="tableForm2" enctype="multipart/form-data">

                    <div class="form-group row">
                        <label class="col-2 col-form-label">
                            <%=LM.getText(LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_ADD_COMMENTS, loginDTO)%>
                        </label>
                        <div class="col-10" >
                            <input type='text' class='form-control'  name='comment' id = 'comment' value=''   tag='pb_html'/>
                        </div>
                    </div>

                    <input type='hidden' class='form-control'  name='approvedIds' id='approvedIds' value= '' tag='pb_html'/>

                    <div class="row">
                        <div class="col-12 text-right">
                            <button type="submit" class="btn submit-btn shadow text-white btn-border-radius"><%=LM.getText(LC.EMPLOYEE_ATTENDANCE_SEARCH_APPROVE, loginDTO)%></button>
                        </div>
                    </div>
            </form>
            </div>


            <%--------------------------------FOOTER----------------------------------------------%>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="late-approval-modal" style="display:none;">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h4 class="caption table-title">
                    <%=LM.getText(LC.ATTENDANCE_APPROVE_EMPLOYEE_LATE_IN_AND_EARLY_OUT, loginDTO)%>
                </h4>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body modal-xl">
                <form action="Employee_attendanceServlet?actionType=approveLateInEarlyOut" method="POST"
                      id="lateApproveId" enctype="multipart/form-data">

                    <div class="form-group row">
                        <label class="col-2 col-form-label">
                            <%=LM.getText(LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_ADD_COMMENTS, loginDTO)%>
                        </label>
                        <div class="col-10" >
                            <input type='text' class='form-control'  name='comment' id = 'lateApproveComment' value=''   tag='pb_html'/>
                        </div>
                    </div>

                    <input type='hidden' class='form-control'  name='lateApprovedIds' id='lateApprovedIds' value= '' tag='pb_html'/>

                    <div class="row">
                        <div class="col-12 text-right">
                            <button type="submit" class="btn submit-btn shadow text-white btn-border-radius"><%=LM.getText(LC.EMPLOYEE_ATTENDANCE_SEARCH_APPROVE, loginDTO)%></button>
                        </div>
                    </div>
                </form>
            </div>


            <%--------------------------------FOOTER----------------------------------------------%>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $('#tableForm').submit(function (e) {
            var currentForm = this;
            e.preventDefault();
            currentForm.submit();
        });

        $(document).on( "click",'.chkEdit',function() {

            $(this).toggleClass("checked");

            var set = $('#tableData').find('input[name="ID"]');

            let selectedValues = '';

            $(set).each(function() {
                if($(this).prop('checked')){
                    selected=true;
                    selectedValues += this.value+";";
                }
            });
            document.getElementById('lateApprovedIds').value = selectedValues;

        });

        $(document).on( "click",'.chkEditOvertime',function(){

            $(this).toggleClass("checked");

            var set = $('#tableData2').find('input[name="ID"]');
            let selectedValues = '';
            $(set).each(function() {
                if($(this).prop('checked')){
                    selected=true;
                    selectedValues += this.value+";";
                }
            });
            document.getElementById('approvedIds').value = selectedValues;

        });

        dateTimeInit("<%=Language%>");
    });
    function openApprovalModal() {
        $('#attendance-approval-modal').modal();
    }

    function openLateApprovalModal() {
        $('#late-approval-modal').modal();
    }
</script>


