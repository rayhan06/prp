<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="employee_attendance.Employee_attendanceDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Employee_attendanceDTO employee_attendanceDTO = (Employee_attendanceDTO)request.getAttribute("employee_attendanceDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(employee_attendanceDTO == null)
{
	employee_attendanceDTO = new Employee_attendanceDTO();
	
}
System.out.println("employee_attendanceDTO = " + employee_attendanceDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.EMPLOYEE_ATTENDANCE_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>

			
<%=("<td id = '" + i + "_ID" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='ID' id = 'ID_hidden_<%=i%>' value='<%=employee_attendanceDTO.ID%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeRecordsType'>")%>
			
	
	<div class="form-inline" id = 'employeeRecordsType_div_<%=i%>'>
		<select class='form-control'  name='employeeRecordsType' id = 'employeeRecordsType_select2_<%=i%>'   tag='pb_html'>
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "employee_records", "employeeRecordsType_select2_" + i, "form-control", "employeeRecordsType", employee_attendanceDTO.employeeRecordsType + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "employee_records", "employeeRecordsType_select2_" + i, "form-control", "employeeRecordsType" );			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_attendanceDeviceType'>")%>
			
	
	<div class="form-inline" id = 'attendanceDeviceType_div_<%=i%>'>
		<select class='form-control'  name='attendanceDeviceType' id = 'attendanceDeviceType_select2_<%=i%>'   tag='pb_html'>
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "attendance_device", "attendanceDeviceType_select2_" + i, "form-control", "attendanceDeviceType", employee_attendanceDTO.attendanceDeviceType + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "attendance_device", "attendanceDeviceType_select2_" + i, "form-control", "attendanceDeviceType" );			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeAttendanceDeviceType'>")%>
			
	
	<div class="form-inline" id = 'employeeAttendanceDeviceType_div_<%=i%>'>
		<select class='form-control'  name='employeeAttendanceDeviceType' id = 'employeeAttendanceDeviceType_select2_<%=i%>'   tag='pb_html'>
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "employee_attendance_device", "employeeAttendanceDeviceType_select2_" + i, "form-control", "employeeAttendanceDeviceType", employee_attendanceDTO.employeeAttendanceDeviceType + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "employee_attendance_device", "employeeAttendanceDeviceType_select2_" + i, "form-control", "employeeAttendanceDeviceType" );			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_inTimestamp'>")%>
			
	
	<div class="form-inline" id = 'inTimestamp_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'inTimestamp_date_<%=i%>' name='inTimestamp' value=<%
if(actionName.equals("edit"))
{
	String formatted_inTimestamp = dateFormat.format(new Date(employee_attendanceDTO.inTimestamp));
	%>
	'<%=formatted_inTimestamp%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_inTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='inTime' id = 'inTime_time_<%=i%>' value='<%=employee_attendanceDTO.inTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_outTimestamp'>")%>
			
	
	<div class="form-inline" id = 'outTimestamp_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'outTimestamp_date_<%=i%>' name='outTimestamp' value=<%
if(actionName.equals("edit"))
{
	String formatted_outTimestamp = dateFormat.format(new Date(employee_attendanceDTO.outTimestamp));
	%>
	'<%=formatted_outTimestamp%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_outTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='outTime' id = 'outTime_time_<%=i%>' value='<%=employee_attendanceDTO.outTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isAbsent" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isAbsent' id = 'isAbsent_checkbox_<%=i%>' value='<%=employee_attendanceDTO.isAbsent%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isOvertime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isOvertime' id = 'isOvertime_checkbox_<%=i%>' value='<%=employee_attendanceDTO.isOvertime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isApproved" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isApproved' id = 'isApproved_checkbox_<%=i%>' value='<%=employee_attendanceDTO.isApproved%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=employee_attendanceDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value='<%=employee_attendanceDTO.insertedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=employee_attendanceDTO.modifiedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + employee_attendanceDTO.isDeleted + "'"):("'" + "false" + "'")%>
 tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=employee_attendanceDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Employee_attendanceServlet?actionType=view&ID=<%=employee_attendanceDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Employee_attendanceServlet?actionType=view&modal=1&ID=<%=employee_attendanceDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	