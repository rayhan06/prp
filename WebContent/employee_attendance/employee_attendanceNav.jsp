<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="mps_attendance.Mps_attendanceDAO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="employee_attendance.Employee_attendanceDAO" %>


<%
    System.out.println("Inside nav.jsp");
    String url = request.getParameter("url");
    String navigator = request.getParameter("navigator");
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";
    String pageno = "";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String[][] searchFieldInfo = null;
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";
    int row = 0;

    String Language = LM.getText(LC.EMPLOYEE_ATTENDANCE_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equals("English");
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    int pagination_number = 0;
    boolean isPermanentTable = false;
    System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
%>


<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label">
            <%--<div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
            &lt;%&ndash;	<%
                    out.println("<input placeholder='অনুসন্ধান করুন' autocomplete='off' type='text' class='form-control border-0' onKeyUp='allfield_changed(\"\",0)' id='anyfield'  name='" + LM.getText(LC.ASSET_MANUFACTURER_SEARCH_ANYFIELD, loginDTO) + "' ");
                    String value = (String) session.getAttribute(searchFieldInfo[searchFieldInfo.length - 1][1]);

                    if (value != null) {
                        out.println("value = '" + value + "'");
                    } else {
                        out.println("value=''");
                    }

                    out.println("/><br />");
                %>&ndash;%&gt;
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>--%>
        </div>
        <div class="kt-portlet__head-toolbar">
            <%--<div class="kt-portlet__head-group">
                <div class="tooltip tooltip-portlet tooltip bs-tooltip-top" role="tooltip" id="tooltip_p6zf7aqcpv"
                     aria-hidden="true" x-placement="top"
                     style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">
                    <div class="tooltip-arrow arrow" style="left: 34px;"></div>
                    <div class="tooltip-inner">Collapse</div>
                </div>
            </div>--%>
        </div>
    </div>
    <div class="kt-portlet__body">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <% if (Employee_attendanceDAO.hasPermissionToSearchOfficeAndEmployee(userDTO) == true) {%>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_EMPLOYEERECORDSTYPE, loginDTO)%>
                        </label>
                        <div class="col-md-8 col-xl-9">
                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius mb-3"
                                    id="tagEmp_modal_button">
                                <%=LM.getText(LC.HM_SELECT, userDTO)%>
                            </button>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped text-nowrap">
                                    <thead></thead>
                                    <tbody id="tagged_emp_table">
                                    <tr style="display: none;">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <button
                                                    class="btn btn-sm delete-trash-btn"
                                                    onclick="remove_containing_row(this,'tagged_emp_table');"
                                            >
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=LM.getText(LC.EMPLOYEE_DECENT, loginDTO)%> <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_EMPLOYEERECORDSTYPE, loginDTO)%>
                        </label>
                        <div class="col-md-8 col-xl-9">
                            <select class='form-control' name='decentEmployeeRecordsId' id='decentEmployeeRecordsId'
                                    tag='pb_html'>

                                <%=new Employee_attendanceDAO().buildOptionsDescentEmployee(Language, userDTO.employee_record_id)%>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=LM.getText(LC.PROMOTION_NEW_OFFICE_UNIT, loginDTO)%>
                        </label>
                        <div class="col-md-8 col-xl-9">
                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                    id="newOfficeUnit_modal_button">
                                <%=LM.getText(LC.PROMOTION_NEW_OFFICE_UNIT, loginDTO)%>
                            </button>
                            <%--<select class='form-control' name='office_units_id' id='office_units_id' onSelect='setSearchChanged()'>
                                <%=Office_unitsRepository.getInstance().buildOptions(Language, null)%>
                            </select>--%>
                            <div class="input-group" id="office_units_id_div" style="display: none">
                                <input type="hidden" id='office_units_id' name="office_units_id" value='' tag='pb_html'>
                                <div class="d-flex align-items-center justify-content-between w-100">
                                    <button type="button" class="btn btn-secondary shadow btn-border-radius btn-block text-dark"
                                            disabled id="office_units_id_text"></button>
                                    <button type="button" class="btn btn-danger shadow btn-border-radius"
                                            id='office_units_id_crs_btn' tag='pb_html'>
                                        X
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <% } %>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%= LM.getText(LC.MP_ENABLE_DATE_RANGE, loginDTO) %>
                        </label>
                        <div class="col-md-8 col-xl-9">
                            <input onchange="openDateRangeHideMonthYear()" type='checkbox' class='form-control-sm mt-1'
                                   name='isDateRangeEnable'
                                   id='isDateRangeEnable'
                                   value='true'
                                   tag='pb_html'>
                        </div>
                    </div>
                </div>
                <%--<div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">

                        </label>
                        <div class="col-md-8 col-xl-9">

                        </div>
                    </div>
                </div>--%>
                <div class="dateRange col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=LM.getText(LC.HM_FROM, loginDTO)%>
                        </label>
                        <div class="col-md-8 col-xl-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="in_timestamp_start_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                <jsp:param name="START_YEAR" value="1971"></jsp:param>
                            </jsp:include>
                        </div>
                    </div>
                </div>
                <div class="dateRange col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=LM.getText(LC.HM_TO, loginDTO)%>
                        </label>
                        <div class="col-md-8 col-xl-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="in_timestamp_end_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                <jsp:param name="START_YEAR" value="1971"></jsp:param>
                            </jsp:include>
                        </div>
                    </div>
                </div>
                <div class="monthYear col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=LM.getText(LC.FREQUENCY_MONTH, loginDTO)%>
                        </label>
                        <div class="col-md-8 col-xl-9">
                            <select onchange="onSelectMonthYear()" class='form-control' name='month' id='monthId'
                                    tag='pb_html'>
                                <%= new Mps_attendanceDAO().buildOptionMonth(Language)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="monthYear col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=LM.getText(LC.FREQUENCY_YEAR, loginDTO)%>
                        </label>
                        <div class="col-md-8 col-xl-9">
                            <select onchange="onSelectMonthYear()" class='form-control' name='year' id='yearId'
                                    tag='pb_html'>
                                <%= new Mps_attendanceDAO().buildOptionYear(Language)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=LM.getText(LC.ATTENDANCE_REPORT_TYPE, loginDTO)%>
                        </label>
                        <div class="col-md-8 col-xl-9">
                            <select onchange="onSelectReportType()" class='form-control' name='searchReportType'
                                    id='searchReportType' tag='pb_html'>
                                <%= new Employee_attendanceDAO().buildOptionReportType(Language)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=LM.getText(LC.HM_DATE, loginDTO)%>
                        </label>
                        <div class="col-md-8 col-xl-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="search_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                <jsp:param name="START_YEAR" value="1971"></jsp:param>
                            </jsp:include>
                            <input type='hidden' class='form-control' name='search_date' id='search_date' value=""
                                   tag='pb_html'/>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit" class="btn shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4; color: white; border-radius: 6px!important; border-radius: 8px;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">

    $('#in_timestamp_start_js').on('datepicker.change', () => {
        setMinDateById('in_timestamp_end_js', getDateStringById('in_timestamp_start_js'));
        setSearchChanged();
    });

    $('#in_timestamp_end_js').on('datepicker.change', () => {
        setSearchChanged();
    });

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                //setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        document.getElementById('search_date').value = getDateStringById('search_date_js');
        //var params = 'AnyField=' + document.getElementById('anyfield').value;
        var params = '';
        params += '&in_timestamp_start=' + getDateTimestampById('in_timestamp_start_js');
        params += '&in_timestamp_end=' + getDateTimestampById('in_timestamp_end_js');
        params += '&search_date=' + getBDFormattedDate('search_date');
        params += '&month=' + document.getElementById('monthId').value;
        params += '&year=' + document.getElementById('yearId').value;
        params += '&search_report_type=' + document.getElementById('searchReportType').value;
        if (document.getElementById('decentEmployeeRecordsId')) {
            params += '&decent_employee_records_id=' + document.getElementById('decentEmployeeRecordsId').value;
        }
        if (document.getElementById('office_units_id')) {
            params += '&office_units_id=' + document.getElementById('office_units_id').value;
        }
        // params +=  '&out_timestamp_start='+ getBDFormattedDate('out_timestamp_start');
        // params +=  '&out_timestamp_end='+ getBDFormattedDate('out_timestamp_end');
        let employeeRecordsId = +added_employee_info_map.keys().next().value;
        if (employeeRecordsId) {
            params += '&employee_records_id=' + employeeRecordsId;
        }
        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = 0;
        var rpp = 0;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

    $("#employeeOfficeID").change(function () {
        fetchEmployee($('#employeeOfficeID').val(), '<%=Language%>', '');
    });

    function fetchEmployee(officeUnitId, language, selectedId) {
        let url = "Employee_shiftServlet?actionType=getEmployeeName&office_unit_id=" + officeUnitId + "&language=" + language + "&selectedId=" + selectedId;
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                console.log(fetchedData);
                document.getElementById('employeeRecordsId').innerHTML = fetchedData;
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    $(function () {
        setMaxDateByTimestampAndId('in_timestamp_start_js', (new Date()).getTime());
        setMaxDateByTimestampAndId('in_timestamp_end_js', (new Date()).getTime());
        $(".dateRange").hide();
    });
    $(function (e) {

        $('div.portlet.box.portlet-btcl.light').hide();

    });
    window.onload = function () {

    }

    function openDateRangeHideMonthYear() {

        $(".dateRange").toggle();
        $(".monthYear").toggle();

        if($("#monthId").val()!='')
            $('#monthId').val('');

        if($("#yearId").val()!='')
            $('#yearId').val('');
        resetDateById("in_timestamp_start_js");
        resetDateById("in_timestamp_end_js");
    }

    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button

    // map to store and send added employee data
    added_employee_info_map = new Map();

    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_emp_table', {
                info_map: added_employee_info_map,
                isSingleEntry: true
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagEmp_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_emp_table';
        $('#search_emp_modal').modal();
    });


    function onSelectReportType() {
        let val = document.getElementById('searchReportType').value;
        if (val != 0 && val != 4) {
            $('.date-range').hide();
        } else {
            $('.date-range').show();
        }
        //	clearAll();
    }

    function clearAll() {

        resetDateById('in_timestamp_start_js');
        resetDateById('in_timestamp_end_js');

        resetDateById('search_date_js');

        $("#search_date").val('');

        if($("#monthId").val()!='')
            $('#monthId').val('');

        if($("#yearId").val()!='')
            $('#yearId').val('');
    }

    $('#search_date_js').on('datepicker.change', (event, param) => {
        clearAllExcept('date');
    });
    $('#in_timestamp_start_js').on('datepicker.change', (event, param) => {
        clearAllExcept('from_to_date');
    });
    $('#in_timestamp_end_js').on('datepicker.change', (event, param) => {
        clearAllExcept('from_to_date');
    });

    function onSelectMonthYear() {
        clearAllExcept('month-year');
    }

    function clearAllExcept(param) {

        if (!param || param != 'date') {
            resetDateById('search_date_js');
            $("#search_date").val('');
        }

        if (!param || param != 'month-year') {
            if($("#monthId").val()!='')
                $('#monthId').val('');

            if($("#yearId").val()!='')
                $('#yearId').val('');
        }

        if(!param || param != 'from_to_date') {
            resetDateById('in_timestamp_start_js');
            resetDateById('in_timestamp_end_js');
        }

    }

    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }

        console.log(selectedOffice);

        $('#newOfficeUnit_modal_button').hide();
        $('#office_units_id_div').show();

        $('#office_units_id_div').show();

        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id').val(selectedOffice.id);

        //fetchDesignation(selectedOffice.id);
    }

    $('#office_units_id_crs_btn').on('click', function () {
        $('#newOfficeUnit_modal_button').show();
        $('#office_units_id_div').hide();

        $('#office_units_id').val('');
        document.getElementById('office_units_id_text').innerHTML = '';
        document.getElementById('rank_cat').innerHTML = '';
    });

    // Office Select Modal
    // modal trigger button
    // this part is needed because if one may have more than one place to select office
    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['newOfficeUnit', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    $('#newOfficeUnit_modal_button').on('click', function () {
        officeSelectModalUsage = 'newOfficeUnit';
        $('#search_office_modal').modal();
    });
</script>

