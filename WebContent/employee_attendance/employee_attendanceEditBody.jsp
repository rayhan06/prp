<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="date.JSDateConfig" %>

<%@page import="employee_attendance.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="employee_records.Employee_recordsDAO" %>
<%@ page import="attendance_device.Attendance_deviceRepository" %>
<%@ page import="org.apache.commons.codec.language.bm.Lang" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="employee_shift.Employee_shiftDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>

<%
    Date todaydate = new Date();
    Employee_attendanceDTO employee_attendanceDTO;
    employee_attendanceDTO = (Employee_attendanceDTO) request.getAttribute("employee_attendanceDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (employee_attendanceDTO == null) {
        employee_attendanceDTO = new Employee_attendanceDTO();
    }
    System.out.println("employee_attendanceDTO = " + employee_attendanceDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_EMPLOYEE_ATTENDANCE_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

%>
<%
    String Language = LM.getText(LC.EMPLOYEE_ATTENDANCE_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    boolean isLanguageEnglish = Language.equals("English");
%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i> &nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal kt-form"
              action="Employee_attendanceServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">

            <input type='hidden' class='form-control' name='ID' id='ID_hidden_<%=i%>'
                   value=<%=actionName.equals("edit")?("'" + employee_attendanceDTO.ID + "'"):("'" + "0" + "'")%>
                           tag='pb_html'/>

            <div class="kt-portlet__body form-body mt-5">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row px-3 px-md-0">
                                        <label class="col-md-4 col-lg-3 text-md-right col-form-label">
                                            <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_EMPLOYEERECORDSTYPE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-lg-9">
                                            <div  id='employeeRecordsType_div_<%=i%>'>

                                                <input type='hidden' class='form-control'
                                                       name='employeeRecordsType' id='employeeRecordsType'
                                                       tag='pb_html'/>
                                                <button type="button"
                                                        class="btn btn-primary btn-block shadow btn-border-radius mb-3"
                                                        id="tagEmp_modal_button">
                                                    <%=LM.getText(LC.HM_SELECT, userDTO)%>
                                                </button>
                                                <table class="table table-bordered table-striped">
                                                    <thead></thead>

                                                    <tbody id="tagged_emp_table">
                                                    <tr style="display: none;">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <button type="button"
                                                                    class="btn btn-sm delete-trash-btn"
                                                                    onclick="remove_containing_row(this,'tagged_emp_table');">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row px-3 px-md-0">
                                        <label class="col-md-4 col-lg-3 text-md-right col-form-label">
                                            <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_ATTENDANCEDEVICETYPE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-lg-9">
                                            <div  id='attendanceDeviceType_div_<%=i%>'>
                                                <select class='form-control' name='attendanceDeviceType'
                                                        id='attendanceDeviceType' tag='pb_html'>
                                                    <%=Attendance_deviceRepository.getInstance().buildOptions(Language, employee_attendanceDTO.attendanceDeviceType)%>
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row px-3 px-md-0">
                                        <label class="col-md-4 col-lg-3 text-md-right col-form-label">
                                            <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_INTIMESTAMP, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-lg-9">
                                            <div  id='inTimestamp_div_<%=i%>' style="z-index:2">
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="jsDate"></jsp:param>
                                                    <jsp:param name="LANGUAGE"
                                                               value="<%=Language%>"></jsp:param>
                                                    <jsp:param name="START_YEAR" value="1971"></jsp:param>
                                                </jsp:include>

                                                <input type='hidden' class='form-control' name='inTimestamp'
                                                       id='inTimestamp'
                                                       value="" tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row px-3 px-md-0">
                                        <label class="col-md-4 col-lg-3 text-md-right col-form-label">
                                            <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_INTIME, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-lg-9">
                                            <div  id='inTime_div_<%=i%>' style="z-index:2">
                                                <jsp:include page="/time/time.jsp">
                                                    <jsp:param name="TIME_ID" value="jsTimeIn"></jsp:param>
                                                    <jsp:param name="LANGUAGE"
                                                               value="<%=Language%>"></jsp:param>
                                                    <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                                </jsp:include>

                                                <input type='hidden' class='form-control' name='inTime'
                                                       id='inTime' value='' tag='pb_html'/>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row px-3 px-md-0">
                                        <label class="col-md-4 col-lg-3 text-md-right col-form-label">
                                            <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_OUTTIMESTAMP, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-lg-9">
                                            <div  id='outTimestamp_div_<%=i%>' style="z-index:2">
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="jsDate2"></jsp:param>
                                                    <jsp:param name="LANGUAGE"
                                                               value="<%=Language%>"></jsp:param>
                                                    <jsp:param name="START_YEAR" value="1971"></jsp:param>
                                                </jsp:include>

                                                <input type='hidden' class='form-control' name='outTimestamp'
                                                       id='outTimestamp' value="" tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row px-3 px-md-0">
                                        <label class="col-md-4 col-lg-3 text-md-right col-form-label">
                                            <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_OUTTIME, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-lg-9">
                                            <div  id='outTime_div_<%=i%>' style="z-index:2">
                                                <jsp:include page="/time/time.jsp">
                                                    <jsp:param name="TIME_ID" value="jsTimeOut"></jsp:param>
                                                    <jsp:param name="LANGUAGE"
                                                               value="<%=Language%>"></jsp:param>
                                                    <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                                </jsp:include>

                                                <input type='hidden' class='form-control' name='outTime'
                                                       id='outTime' value='' tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row px-3 px-md-0">
                                        <label class="col-md-4 col-lg-3 text-md-right col-form-label">
                                            <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_ISOVERTIME, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-lg-9">
                                            <div  id='isOvertime_div_<%=i%>'>
                                                <input type='checkbox' class='form-control-sm mt-1'
                                                       name='isOvertime' id='isOvertime_checkbox_<%=i%>'
                                                       value='true' <%=(actionName.equals("edit") && employee_attendanceDTO.isOvertime == 1)?("checked"):""%>
                                                       tag='pb_html'><br>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row px-3 px-md-0">
                                        <label class="col-md-4 col-lg-3 text-md-right col-form-label">
                                            <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_ISAPPROVED, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-lg-9">
                                            <div  id='isApproved_div_<%=i%>'>
                                                <input type='checkbox' class='form-control-sm mt-1'
                                                       name='isApproved' id='isApproved_checkbox_<%=i%>'
                                                       value='true' <%=(actionName.equals("edit") && employee_attendanceDTO.isApproved==1)?("checked"):""%>
                                                       tag='pb_html'><br>
                                            </div>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + employee_attendanceDTO.insertionDate + "'"):("'" + "0" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedBy'
                                           id='insertedBy_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + employee_attendanceDTO.insertedBy + "'"):("'" + "" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='modifiedBy'
                                           id='modifiedBy_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + employee_attendanceDTO.modifiedBy + "'"):("'" + "" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + employee_attendanceDTO.isDeleted + "'"):("'" + "false" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + employee_attendanceDTO.lastModificationTime + "'"):("'" + "0" + "'")%>
                                                   tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-11 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                            <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_EMPLOYEE_ATTENDANCE_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                            <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_EMPLOYEE_ATTENDANCE_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">

    $('#jsDate').on('datepicker.change', (event, param) => {
        const inDate = getDateStringById('jsDate', 'DD/MM/YYYY');
        setMinDateById('jsDate2', inDate);
    });

    $('#jsTimeIn').on('timepicker.change', (event, param) => {
        const time = getTimeById('jsTimeIn');
        setMinTimeById('jsTimeOut', time);
    });

    $('form').on('submit', () => {
        document.getElementById('inTimestamp').value = getDateStringById('jsDate', 'DD/MM/YYYY');
        document.getElementById('outTimestamp').value = getDateStringById('jsDate2', 'DD/MM/YYYY');
        document.getElementById('inTime').value = getTimeById('jsTimeIn', true);
        document.getElementById('outTime').value = getTimeById('jsTimeOut', true);
        return dateValidator('jsDate', true) && timeNotEmptyValidator('jsTimeIn');
    });

    $(document).ready(function () {

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        <%--dateTimeInit("<%=Language%>");--%>
        setMaxDateByTimestampAndId('jsDate', '<%=todaydate.getTime()%>');
        setMaxDateByTimestampAndId('jsDate2', '<%=todaydate.getTime()%>');

        <%
        if(actionName.equals("edit")){
        %>
        setDateByTimestampAndId('jsDate', '<%=employee_attendanceDTO.inTimestamp%>');
        setDateByTimestampAndId('jsDate2', '<%=employee_attendanceDTO.outTimestamp%>');
        setTimeById('jsTimeIn', '<%=employee_attendanceDTO.inTime%>');
        setTimeById('jsTimeOut', '<%=employee_attendanceDTO.outTime%>');
        <%
        }
        %>


        $.validator.addMethod('employeeRecordsValidator', function (value, element) {
            return value != 0;
        });

        $.validator.addMethod('officeDeviceValidator', function (value, element) {
            return value != 0;
        });

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                inTime: "required",
                //outTime: "required",
                inTimestamp: "required",
                //outTimestamp: "required",
                employeeRecordsType: {
                    required: true,
                    employeeRecordsValidator: true
                },
                attendanceDeviceType: {
                    required: true,
                    officeDeviceValidator: true
                }

            },
            messages: {
                inTime: '<%=Language.equalsIgnoreCase("English") ? "Kindly enter in time!" : "অনুগ্রহ করে ঢোকার সময় দিন"%>',
                outTime: '<%=Language.equalsIgnoreCase("English") ? "Kindly enter end time!" : "অনুগ্রহ করে বাহির হবার সময় দিন"%>',
                inTimestamp: '<%=Language.equalsIgnoreCase("English") ? "Kindly enter in date!" : "অনুগ্রহ করে ঢোকার তারিখ দিন"%>',
                outTimestamp: '<%=Language.equalsIgnoreCase("English") ? "Kindly enter out date!" : "অনুগ্রহ করে বাহির হবার তারিখ দিন"%>',
                employeeRecordsType: '<%=Language.equalsIgnoreCase("English") ? "Kindly select employee!" : "অনুগ্রহ করে ইমপ্লয়ি সিলেক্ট করুন"%>',
                attendanceDeviceType: '<%=Language.equalsIgnoreCase("English") ? "Kindly select attendance device!" : "অনুগ্রহ করে উপস্থিতি যন্ত্র সিলেক্ট করুন"%>'
            }
        });

    });

    <%--$(function () {--%>
    <%--    $("#inTimestamp").datepicker({--%>
    <%--        dateFormat: 'dd/mm/yy',--%>
    <%--        yearRange: '1900:2100',--%>
    <%--        changeYear: true,--%>
    <%--        changeMonth: true,--%>
    <%--        buttonText: "<i class='fa fa-calendar'></i>",--%>
    <%--        onSelect: function (dateText) {--%>
    <%--            $("#outTimestamp").datepicker('option', 'minDate', dateText);--%>
    <%--        }--%>
    <%--    });--%>
    <%--    $("#outTimestamp").datepicker({--%>
    <%--        dateFormat: 'dd/mm/yy',--%>
    <%--        yearRange: '1900:2100',--%>
    <%--        changeYear: true,--%>
    <%--        changeMonth: true,--%>
    <%--        buttonText: "<i class='fa fa-calendar'></i>"--%>
    <%--    });--%>

    <%--    $('#in-timestamp-crs-but').click(() => {--%>
    <%--        $("#outTimestamp").datepicker('option', 'minDate', null);--%>
    <%--        $("#inTimestamp").val('');--%>
    <%--    });--%>
    <%--    $('#out-timestamp-crs-but').click(() => {--%>
    <%--        $("#outTimestamp").val('');--%>
    <%--    });--%>


    <%--    $("#inTimestamp").datepicker('option', 'maxDate', new Date());--%>
    <%--    $("#outTimestamp").datepicker('option', 'maxDate', new Date());--%>
    <%--    // $("#outTimestamp").val('');--%>
    <%--    <%if (actionName.equals("edit")) {--%>
    <%--        String inTimestamp = "";--%>
    <%--        if(employee_attendanceDTO.inTimestamp > SessionConstants.MIN_DATE) {--%>
    <%--            inTimestamp = dateFormat.format(new Date(employee_attendanceDTO.inTimestamp));--%>
    <%--        }--%>
    <%--        String outTimestamp = "";--%>
    <%--        if(employee_attendanceDTO.outTimestamp > SessionConstants.MIN_DATE) {--%>
    <%--            outTimestamp = dateFormat.format(new Date(employee_attendanceDTO.outTimestamp));--%>
    <%--        }--%>

    <%--    %>--%>
    <%--    $("#inTimestamp").val('<%=inTimestamp%>');--%>
    <%--    $("#outTimestamp").val('<%=outTimestamp%>');--%>
    <%--    <%}%>--%>

    <%--});--%>

    function PreprocessBeforeSubmiting(row, validate) {
        document.getElementById('employeeRecordsType').value = +added_employee_info_map.keys().next().value;
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }

        preprocessCheckBoxBeforeSubmitting('isAbsent', row);
        preprocessCheckBoxBeforeSubmitting('isOvertime', row);
        preprocessCheckBoxBeforeSubmitting('isApproved', row);

        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Employee_attendanceServlet");
    }

    function init(row) {

        $("#employeeOfficeID").select2({
            dropdownAutoWidth: true
        });

        $("#employeeRecordsType").select2({
            dropdownAutoWidth: true
        });

        $("#attendanceDeviceType").select2({
            dropdownAutoWidth: true
        });


    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;


    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button

    // map to store and send added employee data
    added_employee_info_map = new Map();

    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_emp_table', {
                info_map: added_employee_info_map,
                isSingleEntry: true
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagEmp_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_emp_table';
        $('#search_emp_modal').modal();
    });


    function remove_containing_row(button, table_name) {
        let containing_row = button.parentNode.parentNode;
        let containing_table = containing_row.parentNode;
        containing_table.deleteRow(containing_row.rowIndex);

        // button id = "<employee record id>_button"
        let td_button = button.parentNode;
        let employee_record_id = td_button.id.split("_")[0];

        let added_info_map = table_name_to_collcetion_map.get(table_name).info_map;
        console.log("delete (employee_record_id)");
        added_info_map.delete(employee_record_id);
        console.log(added_info_map);
    }

</script>






