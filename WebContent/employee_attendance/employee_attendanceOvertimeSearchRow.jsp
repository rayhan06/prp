<%@page pageEncoding="UTF-8" %>

<%@page import="employee_attendance.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="util.*" %>
<%@ page import="employee_records.Employee_recordsDAO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EMPLOYEE_ATTENDANCE_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

    boolean isPermanentTable = true;
    System.out.println("isPermanentTable = " + isPermanentTable);
    AttendanceReportDetails attendanceReportDetails = (AttendanceReportDetails) request.getAttribute("attendanceReportDetails");
    String month = (String)request.getAttribute("month");
    String year = (String)request.getAttribute("year");
    boolean showApproval = (boolean) request.getAttribute("showApproval");
    String servletName = "Employee_attendanceServlet";
    int i = 0;
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_employeeRecordsType'>

    <%=Employee_recordsDAO.getEmployeeName(attendanceReportDetails.employeeRecordsId, Language)%>

</td>


<td id='<%=i%>_designation'>
    <%
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(attendanceReportDetails.employeeRecordsId);
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
    %>

    <%="English".equalsIgnoreCase(Language)?officeUnitOrganograms.designation_eng : officeUnitOrganograms.designation_bng%>

</td>

<td id='<%=i%>_present_days'>

    <%=Utils.getDigits(attendanceReportDetails.presentDays, Language)%>

</td>

<td id='<%=i%>_absent_days'>

    <%=Utils.getDigits(attendanceReportDetails.absentDays, Language)%>

</td>
<%if(showApproval) {%>
<td id='<%=i%>_checkbox'>
    <div class='checker'>
        <span class='chkEditOvertime'><input type='checkbox' name='ID'
                                             value='<%=attendanceReportDetails.employeeRecordsId%>_<%=month%>_<%=year%>'/></span>
    </div>
</td>
<% } %>



