<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="employee_attendance.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="employee_records.Employee_recordsDAO" %>
<%@ page import="attendance_device.Attendance_deviceDAO" %>
<%@ page import="util.TimeFormat" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String value = "";
    String Language = LM.getText(LC.EMPLOYEE_ATTENDANCE_EDIT_LANGUAGE, loginDTO);

    String employeeRecordsIdStr = request.getParameter("employeeRecordsId");
    if (employeeRecordsIdStr == null || employeeRecordsIdStr.isEmpty()) {
        employeeRecordsIdStr = "0";
    }
    long employeeRecordsId = Long.parseLong(employeeRecordsIdStr);
    System.out.println("ID = " + employeeRecordsIdStr);

    String dateStr = request.getParameter("date");
    long date = Long.parseLong(dateStr);

    String attendanceStatus = request.getParameter("attendanceStatus");
    System.out.println(attendanceStatus);
    Employee_attendanceDAO employee_attendanceDAO = new Employee_attendanceDAO("employee_attendance");
    //Employee_attendanceDTO employee_attendanceDTO = (Employee_attendanceDTO) employee_attendanceDAO.getDTOByID(id);
    List<Employee_attendanceDTO> detailsDTOs = employee_attendanceDAO.getAttendanceDetailsByDate(employeeRecordsId, date);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<!-- <div class="modal-content viewmodal"> -->
<div class="menubottom">
    <div class="modal-header">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-9 col-sm-12">
                    <h5 class="modal-title"><%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_EMPLOYEE_ATTENDANCE_ADD_FORMNAME, loginDTO)%>
                    </h5>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-success app_register"
                               data-id="419637"> Register </a>
                        </div>
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-danger app_reject"
                               data-id="419637"> Reject </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <div class="modal-body container">

        <div class="row div_border office-div">

            <div class="col-md-12">
                <%if(detailsDTOs==null || detailsDTOs.size()==0) { %>
                <%if(attendanceStatus.equalsIgnoreCase("Holiday")) { %>
                <h4>It is holiday</h4>
                <%} else if(attendanceStatus.equalsIgnoreCase("OnLeave")) { %>
                <h4>Employee is on leave</h4>
                <%} else if(attendanceStatus.equalsIgnoreCase("Absent")) { %>
                <h4>Employee is absent</h4>
                <%}%>


                <% } else { %>
                <h3><%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_EMPLOYEE_ATTENDANCE_ADD_FORMNAME, loginDTO)%>
                </h3>
                <table class="table table-bordered table-striped">

                    <thead>
                        <th><%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_INTIMESTAMP, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_ATTENDANCEDEVICETYPE, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.EMPLOYEE_ATTENDANCE_ADD_INTIME, loginDTO)%>
                        </th>
                    </thead>
                    <tbody>
                    <%


                        for (int ii = 0; ii < detailsDTOs.size(); ii++) {
                            Employee_attendanceDTO employee_attendanceDTO = detailsDTOs.get(ii);


                    %>
                    <tr id='tr_<%=ii%>'>
                        <%
                            value = employee_attendanceDTO.inTimestamp + "";
                            String formatted_inTimestamp = simpleDateFormat.format(new Date(Long.parseLong(value)));
                        %>
                        <td>
                            <%=Utils.getDigits(formatted_inTimestamp, Language)%>
                        </td>

                        <td>
                            <%=Attendance_deviceDAO.getInstance().getDeviceName(employee_attendanceDTO.attendanceDeviceType,Language)%>
                        </td>
                        <%
                            value = employee_attendanceDTO.inTime + "";
                            formatted_inTimestamp = TimeFormat.getInAmPmFormat(value);
                        %>
                        <td>
                            <%=Utils.getDigits(formatted_inTimestamp, Language)%>
                        </td>


                    </tr>
                    <%
                        }

                        System.out.println("printing done");


                    %>


                    </tbody>


                </table>
                <% } %>
            </div>


        </div>


    </div>
</div>