<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="xray_report.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>

<%
    String servletName = "Xray_reportServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.XRAY_REPORT_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Xray_reportDAO xray_reportDAO = new Xray_reportDAO("xray_report");
    Xray_reportDTO xray_reportDTO = xray_reportDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.XRAY_REPORT_ADD_XRAY_REPORT_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div>
                <h5 class="table-title">
                    <%=LM.getText(LC.XRAY_REPORT_ADD_XRAY_REPORT_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.HM_NAME, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = xray_reportDTO.nameEn + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.HM_TITLE, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = xray_reportDTO.comment + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>

                    </table>
                </div>
            </div>
            <div>
                <h5 class="table-title mt-5">
                    <%=LM.getText(LC.XRAY_REPORT_ADD_XRAY_REPORT_DETAILS, loginDTO)%>
                </h5>
                <div>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th><%=LM.getText(LC.HM_NAME, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.HM_DETAILS, loginDTO)%>
                            </th>
                        </tr>
                        <%
                            XrayReportDetailsDAO xrayReportDetailsDAO = new XrayReportDetailsDAO();
                            List<XrayReportDetailsDTO> xrayReportDetailsDTOs = xrayReportDetailsDAO.getXrayReportDetailsDTOListByXrayReportID(xray_reportDTO.iD);

                            for (XrayReportDetailsDTO xrayReportDetailsDTO : xrayReportDetailsDTOs) {
                        %>
                        <tr>
                            <td>
                                <%
                                    value = xrayReportDetailsDTO.nameEn + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                            <td>
                                <%
                                    value = xrayReportDetailsDTO.comment + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>
                        </tr>
                        <%

                            }

                        %>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>