<%@page pageEncoding="UTF-8" %>

<%@page import="xray_report.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.XRAY_REPORT_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_XRAY_REPORT;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Xray_reportDTO xray_reportDTO = (Xray_reportDTO) request.getAttribute("xray_reportDTO");
    CommonDTO commonDTO = xray_reportDTO;
    String servletName = "Xray_reportServlet";


    System.out.println("xray_reportDTO = " + xray_reportDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Xray_reportDAO xray_reportDAO = (Xray_reportDAO) request.getAttribute("xray_reportDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td id='<%=i%>_nameEn' class="text-nowrap">
    <%
        value = xray_reportDTO.nameEn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_comment' class="text-nowrap">
    <%
        value = xray_reportDTO.comment + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Xray_reportServlet?actionType=view&ID=<%=xray_reportDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button class="btn-sm border-0 shadow btn-border-radius text-white" style="background-color: #ff6b6b;" type="button"
            onclick="location.href='Xray_reportServlet?actionType=getEditPage&ID=<%=xray_reportDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td class="text-right" id='<%=i%>_checkbox'>
	<div class='checker'>
        <span class='chkEdit'>
            <input type='checkbox' name='ID' value='<%=xray_reportDTO.iD%>'/>
        </span>
	</div>
</td>
																						
											

