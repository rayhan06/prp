<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="xray_report.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    Xray_reportDTO xray_reportDTO;
    xray_reportDTO = (Xray_reportDTO) request.getAttribute("xray_reportDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (xray_reportDTO == null) {
        xray_reportDTO = new Xray_reportDTO();

    }
    System.out.println("xray_reportDTO = " + xray_reportDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.XRAY_REPORT_ADD_XRAY_REPORT_ADD_FORMNAME, loginDTO);
    String servletName = "Xray_reportServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.XRAY_REPORT_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>



<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Xray_reportServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-8 offset-md-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=xray_reportDTO.iD%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='nameBn'
                                           id='nameBn_text_<%=i%>' value='<%=xray_reportDTO.nameBn%>'
                                           tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label text-md-right"><%=LM.getText(LC.HM_NAME, loginDTO)%>
                                        </label>
                                        <div class="col-md-10">
                                            <input type='text' class='form-control' name='nameEn'
                                                   id='nameEn_text_<%=i%>' value='<%=xray_reportDTO.nameEn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label text-md-right"><%=LM.getText(LC.HM_TITLE, loginDTO)%>
                                        </label>
                                        <div class="col-md-10">
                                            <input type='text' class='form-control' name='comment'
                                                   id='comment_text_<%=i%>' value='<%=xray_reportDTO.comment%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=xray_reportDTO.insertedByUserId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=xray_reportDTO.insertedByOrganogramId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=xray_reportDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModifierUser'
                                           id='lastModifierUser_hidden_<%=i%>'
                                           value='<%=xray_reportDTO.lastModifierUser%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>' value='<%=xray_reportDTO.isDeleted%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=xray_reportDTO.lastModificationTime%>' tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.LAB_TEST_ADD_LAB_TEST_LIST, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped text-nowrap">
                                <thead>
                                <tr>
                                    <th class="text-nowrap"><%=LM.getText(LC.HM_NAME, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.HM_DETAILS, loginDTO)%>
                                    </th>
                                    <th class="text-nowrap"><%=LM.getText(LC.XRAY_REPORT_ADD_XRAY_REPORT_DETAILS_REMOVE, loginDTO)%>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="field-XrayReportDetails">


                                <%
                                    if (actionName.equals("edit")) {
                                        int index = -1;


                                        for (XrayReportDetailsDTO xrayReportDetailsDTO : xray_reportDTO.xrayReportDetailsDTOList) {
                                            index++;

                                            System.out.println("index index = " + index);

                                %>

                                <tr id="XrayReportDetails_<%=index + 1%>">
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control w-auto' name='xrayReportDetails.iD'
                                               id='iD_hidden_<%=childTableStartingID%>'
                                               value='<%=xrayReportDetailsDTO.iD%>' tag='pb_html'/>

                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control w-auto'
                                               name='xrayReportDetails.xrayReportId'
                                               id='xrayReportId_hidden_<%=childTableStartingID%>'
                                               value='<%=xrayReportDetailsDTO.xrayReportId%>' tag='pb_html'/>
                                    </td>
                                    <td>


                                        <input type='text' class='form-control w-auto' name='xrayReportDetails.nameEn'
                                               id='nameEn_text_<%=childTableStartingID%>'
                                               value='<%=xrayReportDetailsDTO.nameEn%>' tag='pb_html'/>


                                        <input type='hidden' class='form-control w-auto' name='xrayReportDetails.nameBn'
                                               id='nameBn_text_<%=childTableStartingID%>'
                                               value='<%=xrayReportDetailsDTO.nameBn%>' tag='pb_html'/>


                                        <input type='hidden' class='form-control w-auto' name='xrayReportDetails.title'
                                               id='title_text_<%=childTableStartingID%>'
                                               value='<%=xrayReportDetailsDTO.title%>' tag='pb_html'/>
                                    </td>
                                    <td>


                                            <textarea class='form-control' name='xrayReportDetails.comment'
                                                      id='comment_text_<%=childTableStartingID%>'
                                                      tag='pb_html'><%=xrayReportDetailsDTO.comment%></textarea>
                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control' name='xrayReportDetails.isDeleted'
                                               id='isDeleted_hidden_<%=childTableStartingID%>'
                                               value='<%=xrayReportDetailsDTO.isDeleted%>' tag='pb_html'/>

                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='xrayReportDetails.lastModificationTime'
                                               id='lastModificationTime_hidden_<%=childTableStartingID%>'
                                               value='<%=xrayReportDetailsDTO.lastModificationTime%>'
                                               tag='pb_html'/>
                                    </td>
                                    <td>
                                        <div class='checker'><span id='chkEdit'><input type='checkbox'
                                                                                       id='xrayReportDetails_cb_<%=index%>'
                                                                                       name='checkbox'
                                                                                       value=''/></span></div>
                                    </td>
                                </tr>
                                <%
                                            childTableStartingID++;
                                        }
                                    }
                                %>

                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <div class="text-right mt-3">
                                <button
                                        id="add-more-XrayReportDetails"
                                        name="add-moreXrayReportDetails"
                                        type="button"
                                        class="btn btn-sm text-white add-btn shadow">
                                    <i class="fa fa-plus"></i>
                                    <%=LM.getText(LC.HM_ADD, loginDTO)%>
                                </button>
                                <button
                                        id="remove-XrayReportDetails"
                                        name="removeXrayReportDetails"
                                        type="button"
                                        class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        </div>
                        <%XrayReportDetailsDTO xrayReportDetailsDTO = new XrayReportDetailsDTO();%>
                        <template id="template-XrayReportDetails">
                            <tr>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='xrayReportDetails.iD'
                                           id='iD_hidden_' value='<%=xrayReportDetailsDTO.iD%>' tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='xrayReportDetails.xrayReportId'
                                           id='xrayReportId_hidden_' value='<%=xrayReportDetailsDTO.xrayReportId%>'
                                           tag='pb_html'/>
                                </td>
                                <td>


                                    <input type='text' class='form-control w-auto' name='xrayReportDetails.nameEn'
                                           id='nameEn_text_' value='<%=xrayReportDetailsDTO.nameEn%>'
                                           tag='pb_html'/>


                                    <input type='hidden' class='form-control w-auto' name='xrayReportDetails.nameBn'
                                           id='nameBn_text_' value='<%=xrayReportDetailsDTO.nameBn%>'
                                           tag='pb_html'/>


                                    <input type='hidden' class='form-control w-auto' name='xrayReportDetails.title'
                                           id='title_text_' value='<%=xrayReportDetailsDTO.title%>' tag='pb_html'/>
                                </td>
                                <td>


                                        <textarea class='form-control' name='xrayReportDetails.comment'
                                                  id='comment_text_'
                                                  tag='pb_html'><%=xrayReportDetailsDTO.comment%></textarea>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='xrayReportDetails.isDeleted'
                                           id='isDeleted_hidden_' value='<%=xrayReportDetailsDTO.isDeleted%>'
                                           tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='xrayReportDetails.lastModificationTime'
                                           id='lastModificationTime_hidden_'
                                           value='<%=xrayReportDetailsDTO.lastModificationTime%>' tag='pb_html'/>
                                </td>
                                <td>
                                    <div><span id='chkEdit'><input type='checkbox' name='checkbox' value=''/></span>
                                    </div>
                                </td>
                            </tr>

                        </template>
                    </div>
                </div>
                <div class="form-actions text-center mb-5 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn btn-border-radius">
                        <%=LM.getText(LC.XRAY_REPORT_ADD_XRAY_REPORT_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius" type="submit">
                        <%=LM.getText(LC.XRAY_REPORT_ADD_XRAY_REPORT_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    function PreprocessBeforeSubmiting(row, validate) {


        for (i = 1; i < child_table_extra_id; i++) {
        }
        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Xray_reportServlet");
    }

    function init(row) {


        for (i = 1; i < child_table_extra_id; i++) {
        }

    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-XrayReportDetails").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-XrayReportDetails");

            $("#field-XrayReportDetails").append(t.html());
            SetCheckBoxValues("field-XrayReportDetails");

            var tr = $("#field-XrayReportDetails").find("tr:last-child");

            tr.attr("id", "XrayReportDetails_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                console.log(index + ": " + $(this).attr('id'));
            });

            dateTimeInit("<%=Language%>");
            CKEDITOR.replace('comment_text_' + child_table_extra_id);
            child_table_extra_id++;

        });


    $("#remove-XrayReportDetails").click(function (e) {
        var tablename = 'field-XrayReportDetails';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[type="checkbox"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
    });


</script>






