<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.StringUtils" %>
<%@ page import="vm_fuel_request.*" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ page import="vm_route_travel.Vm_route_travelDTO" %>
<%@ page import="vm_route_travel.Vm_route_travelDAO" %>
<%@ page import="vm_route_travel_withdraw.Vm_route_travel_withdrawDAO" %>
<%@ page import="vm_route_travel_withdraw.Vm_route_travel_withdrawDTO" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="vm_route_travel_withdraw.Vm_route_travel_withdrawRepository" %>

<%
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

//	String routeTravelTableName = "vm_route_travel";
//	String routeTravelWithdrawalTableName = "vm_route_travel_withdraw";
//
//	Vm_route_travelDAO vm_route_travelDAO = new Vm_route_travelDAO(routeTravelTableName);
//	Vm_route_travelDTO vm_route_travelDTO = vm_route_travelDAO.getDTOByRequesterEmpID(vm_route_travel_withdraw_approval_mappingDTO.employeeRecordsId);
//
//	Vm_route_travel_withdrawDAO vm_route_travel_withdrawDAO= new Vm_route_travel_withdrawDAO(routeTravelWithdrawalTableName);
//	Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO = vm_route_travel_withdrawDAO.getDTOByRequesterEmpID(vm_route_travel_withdraw_approval_mappingDTO.employeeRecordsId);

    //Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO = new Vm_route_travel_withdrawDAO().getDTOByID(vm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawId);
    Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO = Vm_route_travel_withdrawRepository.getInstance().getVm_route_travel_withdrawDTOByid(vm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawId);
    Vm_route_travelDTO vm_route_travelDTO = null;
    if (vm_route_travel_withdrawDTO != null) {
        vm_route_travelDTO = new Vm_route_travelDAO().getSatisfiedDTOByRequesterEmpID(vm_route_travel_withdrawDTO.requesterEmpId);
    }

    //Vm_route_travelDTO vm_route_travelDTO = new Vm_route_travelDAO().getDTOByRequesterEmpID(vm_route_travel_withdrawDTO.requesterEmpId);

//	Vm_route_travelDTO vm_route_travelDTO = null;
//	if(vm_route_travel_withdrawDTO!=null){
//		vm_route_travelDTO = new Vm_route_travelDAO().getDTOByRequesterEmpID(vm_route_travel_withdrawDTO.requesterEmpId);
//	}

%>

<%


    if (vm_route_travelDTO != null && vm_route_travel_withdrawDTO != null) {

%>

<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>
<%--<td id='<%=i%>_cardInfoId'>--%>
<%--	<%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(vm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawId))%>--%>
<%--</td>--%>

<td id='<%=i%>_requesterOrgId'>
    <%
        value = Language.equals("English") ?
                vm_route_travel_withdrawDTO.requesterNameEn
                : vm_route_travel_withdrawDTO.requesterNameBn
        ;
    %>

    <%=value%>


</td>


<td id='<%=i%>_driverName'>
    <%=Language.equals("English") ? (vm_route_travel_withdrawDTO.requesterOfficeUnitOrgNameEn)
            : (vm_route_travel_withdrawDTO.requesterOfficeUnitOrgNameBn)%>


</td>

<td id='<%=i%>_startAndEndAddress'>
    <%=Language.equals("English") ? (vm_route_travel_withdrawDTO.requesterOfficeUnitNameEn)
            : (vm_route_travel_withdrawDTO.requesterOfficeUnitNameBn)%>


</td>

<td id='<%=i%>_endDate'>
    <%
        value = vm_route_travel_withdrawDTO.endDate + "";
    %>
    <%
        String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_startDate, Language)%>


</td>

<td id='<%=i%>_routeName'>
    <%
        value = vm_route_travelDTO.routeId + "";
        value = CommonDAO.getName(Language, "vm_route", vm_route_travelDTO.routeId);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>
<td id='<%=i%>_stoppagePoint'>
    <%
        value = vm_route_travelDTO.stoppageId + "";
        if (value.equals("-1")) {
            value = Language.equals("English") ? "Others" : "অন্যান্য";
            value += "(" + vm_route_travelDTO.requestedStoppageName + ")";
        } else {
            value = CommonDAO.getName(vm_route_travelDTO.stoppageId, "vm_route_stoppage", "stoppage_name", "ID");
        }
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_cardApprovalStatusCat'>
    <span class="font-weight-bold"
          style="color: <%=ApprovalStatus.getColor(vm_route_travel_withdraw_approval_mappingDTO.withdrawApprovalStatusCat)%>;">
        <%=CatRepository.getInstance().getText(Language, "card_approval_status", vm_route_travel_withdraw_approval_mappingDTO.withdrawApprovalStatusCat)%>
    </span>
</td>


<td>
	<button
			type="button"
			class="btn-sm border-0 shadow bg-light btn-border-radius"
			style="color: #ff6b6b;"
			onclick="location.href='Vm_route_travel_withdraw_approval_mappingServlet?actionType=getApprovalPage&cardInfoId=<%=vm_route_travel_withdraw_approval_mappingDTO.routeTravelWithdrawId%>&language=<%=Language%>'">
		<i class="fa fa-eye"></i>
	</button>
</td>


<%

    }


%>



																						
											

