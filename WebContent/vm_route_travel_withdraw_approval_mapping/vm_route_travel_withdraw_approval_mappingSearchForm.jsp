<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="util.RecordNavigator" %>
<%@page import="java.util.Enumeration" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="vm_route_travel_withdraw_approval.Vm_route_travel_withdraw_approvalDTO" %>
<%@ page import="vm_route_travel_withdraw_approval.Vm_route_travel_withdraw_approvalRepository" %>
<%@ page import="vm_route_travel_withdraw_approval_mapping.Vm_route_travel_withdraw_approval_mappingDTO" %>

<%@page pageEncoding="UTF-8" %>


<%
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String failureMessage = (String) request.getAttribute("failureMessage");
	if (failureMessage == null || failureMessage.isEmpty()) {
		failureMessage = "";
	}
	String value = "";
	String Language = LM.getText(LC.CARD_APPROVAL_MAPPING_EDIT_LANGUAGE, loginDTO);
	boolean isLanguageEnglish = "English".equalsIgnoreCase(Language);
	UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
	String navigator2 = SessionConstants.NAV_CARD_APPROVAL_MAPPING;
	System.out.println("navigator2 = " + navigator2);
	RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
	System.out.println("rn2 = " + rn2);
	String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
	String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
	String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
	String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
	String ajax = request.getParameter("ajax");
	boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
	Map<Long, Vm_route_travel_withdraw_approvalDTO> mapByCardApprovalDTOId;
	long validationStartDate=SessionConstants.MIN_DATE;
	long validationEndDate=SessionConstants.MIN_DATE;
	long cardInfoId=-1;
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<%
	if (!hasAjax) {
		Enumeration<String> parameterNames = request.getParameterNames();

		while (parameterNames.hasMoreElements()) {

			String paramName = parameterNames.nextElement();

			if (!paramName.equalsIgnoreCase("actionType")) {
				String[] paramValues = request.getParameterValues(paramName);
				for (int i = 0; i < paramValues.length; i++) {
					String paramValue = paramValues[i];

%>

<% } } } } %>


<div class="table-responsive">
	<table id="tableData" class="table table-bordered table-striped text-nowrap">
		<thead>
		<tr>
<%--			<th><%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARDINFOID, loginDTO)%>--%>
<%--			</th>--%>
<%--			<th><%=LM.getText(LC.CARD_APPROVAL_MAPPING_CARD_REQUESTER_EMPLOYEE, loginDTO)%>--%>
<%--			</th>--%>
<%--			<th><%=LM.getText(LC.CARD_INFO_ADD_CARDEMPLOYEEIMAGESID, loginDTO)%></th>--%>
<%--			<th><%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARDAPPROVALSTATUSCAT, loginDTO)%>--%>
<%--			</th>--%>
<%--			<th><%=LM.getText(LC.CARD_APPROVAL_MAPPING_CARD_APPROVER, loginDTO)%>--%>
<%--			</th>--%>
<%--			&lt;%&ndash;            <th><%=LM.getText(LC.CARD_APPROVAL_ACTION,loginDTO)%></th>&ndash;%&gt;--%>
<%--			<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>--%>
<%--			</th>--%>

	<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_APPLICANT_OFFICER_NAME, loginDTO)%></th>
	<th><%=LM.getText(LC.PROMOTION_HISTORY_SEARCH_RANKCAT, loginDTO)%></th>
	<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_OFFICE, loginDTO)%></th>
	<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_LAST_DATE_OF_TRAVEL, loginDTO)%></th>

	<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_ROUTE_NAME, loginDTO)%></th>
	<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_STOPPAGE_POINT, loginDTO)%></th>
	<th><%=LM.getText(LC.HM_UPDATE, loginDTO)%></th>
	<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>

		</tr>
		</thead>
		<tbody>
		<%
			List<Vm_route_travel_withdraw_approval_mappingDTO> data = (List<Vm_route_travel_withdraw_approval_mappingDTO>) session.getAttribute(SessionConstants.VIEW_CARD_APPROVAL_MAPPING);


			if (data != null && data.size()>0) {
				List<Long> cardEmployeeInfoIds = new ArrayList<>();
				List<Long> cardEmployeeOfficeInfoIds = new ArrayList<>();
				List<Long> cardApprovalIds = new ArrayList<>();
				List<Long> cardEmployeeImagesIds = new ArrayList<>();
				data.forEach(dto->{
					cardApprovalIds.add(dto.routeTravelWithdrawApprovalId);
				});
				List<Vm_route_travel_withdraw_approvalDTO> cardApprovalDTOList = Vm_route_travel_withdraw_approvalRepository.getInstance().getByIds(cardApprovalIds);
				mapByCardApprovalDTOId = cardApprovalDTOList.stream()
						.collect(Collectors.toMap(dto->dto.iD,dto->dto,(e1,e2)->e1));
				for (int i = 0; i < data.size(); i++) {
					Vm_route_travel_withdraw_approval_mappingDTO vm_route_travel_withdraw_approval_mappingDTO = data.get(i);
		%>
		<tr id='tr_<%=i%>'>
			<%@include file="vm_route_travel_withdraw_approval_mappingSearchRow.jsp"%>
		</tr>
		<%}}%>
		</tbody>

	</table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>


			