<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="vm_fuel_request.ApprovalStatus" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="task_type_approval_path.TaskTypeApprovalPathRepository" %>

<%@ page import="vm_fuel_request.Vm_fuel_requestRepository" %>
<%@ page import="vm_fuel_request.Vm_fuel_requestDAO" %>

<%@ page import="card_approval_mapping.CardApprovalModel" %>

<%@ page import="vm_route_travel_withdraw_approval_mapping.Vm_route_travel_withdraw_approval_mappingDTO" %>
<%@ page import="vm_route_travel_withdraw_approval_mapping.Vm_route_travel_withdraw_approval_mappingDAO" %>
<%@ page import="vm_route_travel_withdraw_approval_mapping.Vm_route_travel_withdraw_approvalModel" %>
<%@ page import="vm_route_travel_withdraw_approval.Vm_route_travel_withdraw_approvalDTO" %>
<%@ page import="vm_route_travel_withdraw_approval.Vm_route_travel_withdraw_approvalDAO" %>
<%@ page import="vm_route_travel_withdraw.Vm_route_travel_withdrawDTO" %>
<%@ page import="vm_route_travel_withdraw.Vm_route_travel_withdrawDAO" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="vm_route_travel.Vm_route_travelDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleDAO" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_route.*" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    long cardInfoId = Long.parseLong(request.getParameter("cardInfoId"));
    Vm_route_travel_withdraw_approval_mappingDTO cardApprovalMappingDTO = new Vm_route_travel_withdraw_approval_mappingDAO().getByVm_fuel_requestIdAndApproverEmployeeRecordId(cardInfoId, userDTO.employee_record_id);
    boolean hasNextApprover = TaskTypeApprovalPathRepository.getInstance().hasNextApprover(cardApprovalMappingDTO.taskTypeId, cardApprovalMappingDTO.sequence + 1);
    System.out.println("hasNextApprover : " + hasNextApprover);
    String formTitle = LM.getText(LC.CARD_INFO_ADD_CARD_INFO_ADD_FORMNAME, loginDTO);
    String Language = LM.getText(LC.CARD_INFO_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    String context = request.getContextPath() + "/";
    Vm_route_travel_withdraw_approvalModel approvalModel = (Vm_route_travel_withdraw_approvalModel) request.getAttribute("Vm_route_travel_withdraw_approvalModel");
    Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO = (Vm_route_travel_withdrawDTO) request.getAttribute("Vm_route_travel_withdrawDTO");
    Vm_route_travelDTO vm_route_travelDTO = (Vm_route_travelDTO) request.getAttribute("Vm_route_travelDTO");

    String routeTableName = "vm_route";
    Vm_routeDAO vm_routeDAO = new Vm_routeDAO(routeTableName);
    Vm_routeDTO vm_routeDTO = new Vm_routeDTO();
    vm_routeDTO = Vm_routeRepository.getInstance().getVm_routeDTOByID(vm_route_travelDTO.routeId);

    if (approvalModel == null)
        approvalModel = new Vm_route_travel_withdraw_approvalModel();
    //Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO = new Vm_route_travel_withdrawDAO().getDTOByID(approvalModel.cardInfoId);


    int sequence = approvalModel.cardApprovalMappingDTO.sequence;


    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
//    Long employeeRecordId = (Long) request.getAttribute("employeeRecordId");
//    boolean isAdmin = (boolean) request.getAttribute("isAdmin");
//    String paramEmployeeRecordId = request.getParameter("employeeRecordId");
%>
<style>
    .fly-in-from-down {
        animation: flyFromDown 1s ease-out;
    }

    @keyframes flyFromDown {
        0% {
            transform: translateY(200%);
        }
        100% {
            transform: translateY(0%);
        }
    }

    .top-section-font {
        color: #00a1d4;
        font-weight: 500;
    }

    .page-bg {
        background-color: #f9f9fb;
    }

    .form-group label {
        font-weight: 600 !important;
    }

    .form-group{
        margin-bottom: 1rem;
    }

</style>

<!-- begin:: Subheader -->
<div class="ml-4 mt-4">
    <div class="kt-subheader__main">
<%--        <h2 class="kt-subheader__title" style="color: #00a1d4;">--%>
<%--            <i class="far fa-address-card"></i> <%=formTitle%>--%>
<%--        </h2>--%>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content" id="kt_content">
    <div class="kt-portlet" style="background-color: #F2F2F2!important;">
        <div class="kt-portlet__body m-4 page-bg">
            <div class="row">
                <div class="col-12 row">
                    <div class="col-3"></div>
                    <div class="col-6 text-center">
                        <img width="20%"
                             src="<%=context%>assets/static/parliament_logo.png"
                             alt="logo"
                             class="logo-default"
                        />
                        <h2 class="text-center mt-3 top-section-font">
                            <%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
                        </h2>
                        <h4 class="text-center mt-2 top-section-font">
                            <%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                        </h4>
                    </div>
                    <%--                        <div class="col-3 text-right">--%>
                    <%--                            <img src='<%=approvalModel.photoSrc%>' alt="employee photo" width="45%" id="photo">--%>
                    <%--                        </div>--%>
                </div>
            </div>
            <div class="mt-5">
                <div class="form-group row">
                    <label class="col-2 col-form-label">
                        <%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_NAMEOFEMPLOYEE, loginDTO)%>

                    </label>
                    <div class="col-4 col-form-label ">

                        <div>

                            <%=isLanguageEnglish ? (vm_route_travel_withdrawDTO.requesterNameEn)
                                    : (vm_route_travel_withdrawDTO.requesterNameBn)%>


                        </div>

                    </div>

                    <label class="col-2 col-form-label">
                        <%=LM.getText(LC.UNIT_NAME_UPDATE_OFFICE, loginDTO)%>

                    </label>
                    <div class="col-4 col-form-label">

                        <div>

                            <%=isLanguageEnglish ? (vm_route_travel_withdrawDTO.requesterOfficeUnitNameEn)
                                    : (vm_route_travel_withdrawDTO.requesterOfficeUnitNameBn)%>


                        </div>

                    </div>

                </div>

                <div class="form-group row">
                    <label class="col-2 col-form-label">
                        <%=LM.getText(LC.PROMOTION_HISTORY_EDIT_RANKCAT, loginDTO)%>

                    </label>
                    <div class="col-4 col-form-label">

                        <div>

                            <%=isLanguageEnglish ? (vm_route_travel_withdrawDTO.requesterOfficeUnitOrgNameEn)
                                    : (vm_route_travel_withdrawDTO.requesterOfficeUnitOrgNameBn)%>


                        </div>

                    </div>

                    <label class="col-2 col-form-label">
                        <%=LM.getText(LC.VM_REQUISITION_ADD_FARE_AMOUNT, loginDTO)%>

                    </label>
                    <div class="col-4 col-form-label">

                        <%
                            VmRouteStoppageDAO vmRouteStoppageDAO = new VmRouteStoppageDAO();

                            VmRouteStoppageDTO vmRouteStoppageDTO = VmStoppageRepository.getInstance()
                                    .getVmRouteStoppageDTOByID(vm_route_travelDTO.stoppageId);
//                                    vmRouteStoppageDAO.getDTOByID(vm_route_travelDTO.stoppageId);
                        %>

                        <%=Utils.getDigits(vmRouteStoppageDTO.rent, Language)%>

                    </div>

                </div>

                <div class="form-group row">
                    <label class="col-2 col-form-label">
                        <%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_ROUTE_NAME, loginDTO)%>

                    </label>
                    <div class="col-4 col-form-label">

                        <div>

                            <%=isLanguageEnglish ? (vm_routeDTO.nameEn)
                                    : (vm_routeDTO.nameBn)%>


                        </div>

                    </div>

                    <label class="col-2 col-form-label">
                        <%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_STOPPAGE_POINT, loginDTO)%>

                    </label>
                    <div class="col-4 col-form-label">

                        <div>

                            <%
                                String value = vm_route_travelDTO.stoppageId + "";
                                if (value.equals("-1")) {
                                    value = Language.equals("English") ? "Others" : "অন্যান্য";
                                    value += "(" + vm_route_travelDTO.requestedStoppageName + ")";
                                } else {
                                    value = CommonDAO.getName(vm_route_travelDTO.stoppageId, "vm_route_stoppage", "stoppage_name", "ID");
                                }
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </div>

                    </div>

                </div>

                <div class="form-group row">
                    <label class="col-2 col-form-label">
                        <%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLE_NUMBER, loginDTO)%>

                    </label>
                    <div class="col-4 col-form-label">

                        <div>

                            <%

                                Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().
                                        getVm_vehicleDTOByID(vm_routeDTO.vehicleId);

                                if (vm_vehicleDTO != null) {
                                    value = vm_vehicleDTO.regNo + "";

                                } else {
                                    value = "";
                                }


                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </div>

                    </div>

                    <label class="col-2 col-form-label">
                        <%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_LAST_DATE_OF_TRAVEL, loginDTO)%>

                    </label>
                    <div class="col-4 col-form-label">

                        <div>


                            <%value = dateFormat.format(new Date(vm_route_travel_withdrawDTO.endDate));%>
                            <%=Utils.getDigits(value, Language)%>


                        </div>

                    </div>

                </div>


            </div>


            <%
                if (approvalModel.cardApprovalMappingDTO.withdrawApprovalStatusCat == ApprovalStatus.PENDING.getValue()) {
            %>
            <div class="row">
                <div class="col-12 mt-3 text-right">
                    <a class="btn btn-danger shadow btn-border-radius btn-sm"
                       href="Vm_route_travel_withdraw_approval_mappingServlet?actionType=rejectVm_withdrawal&cardInfoId=<%=cardInfoId%>"><%=LM.getText(LC.CARD_APPROVAL_MAPPING_DO_NOT_APPOVE, loginDTO)%>
                    </a>
                    <%
                        if (hasNextApprover) {
                    %>

                    <a class="btn btn-success shadow ml-2 btn-border-radius btn-sm"
                       href="Vm_route_travel_withdraw_approval_mappingServlet?actionType=approvedVm_withdrawal&cardInfoId=<%=cardInfoId%>"><%=LM.getText(LC.CARD_APPROVAL_MAPPING_APPROVAL, loginDTO)%>
                    </a>
                    <%
                        //                            }
                    } else {
                    %>
                    <button type="button" class="btn btn-success shadow ml-2 btn-border-radius btn-sm"
                            onclick="satisfiedClicked()"><%=LM.getText(LC.CARD_APPROVAL_MAPPING_APPROVAL, loginDTO)%>
                    </button>
                    <%
                        }
                    %>
                </div>
            </div>
            <%
                }
            %>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="approval_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h4 class="caption" style="color: #56b2cf; margin-left: 10px">
                    <%=LM.getText(LC.CARD_APPROVAL_MAPPING_APPROVAL, loginDTO)%>
                </h4>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body modal-lg">
                <div class="row my-5">
                    <div class="col-12">
                        <form class="form-horizontal text-right"
                              action="Vm_route_travel_withdraw_approval_mappingServlet?actionType=approvedVm_withdrawal"
                              id="permanentCardForm" name="permanentCardForm" method="POST"
                              enctype="multipart/form-data" onsubmit="return PreprocessBeforeSubmiting()">

                            <input type="hidden" name="cardInfoId" id="cardInfoId" value='<%=cardInfoId%>'>
                            <div class="row my-5" align="right">
                                <div class="col-12">
                                    <button type="button" class="btn btn-danger shadow" style="border-radius: 8px;"
                                            data-dismiss="modal"
                                    ><%=LM.getText(LC.CLOSE_CLOSE, loginDTO)%>
                                    </button>
                                    <button class="btn btn-success shadow ml-2" style="border-radius: 8px;"
                                            type="submit"><%=LM.getText(LC.CARD_INFO_ADD_CARD_INFO_SUBMIT_BUTTON, loginDTO)%>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <%--            &lt;%&ndash;------------------------------FOOTER--------------------------------------------&ndash;%&gt;--%>
            <%--            <div class="modal-footer border-0">--%>

            <%--            </div>--%>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        <%--setDateByTimestampAndId('validation-start-date-js', <%=approvalModel.validationStartDate%>);--%>
        <%--setDateByTimestampAndId('validation-end-date-js', <%=approvalModel.validationEndDate%>);--%>
        // $('#validation-start-date-js').on('datepicker.change', () => {
        //     setMinDateById('validation-end-date-js', getDateStringById('validation-start-date-js'));
        // });
    });
    $('#approval_modal').on('show.bs.modal', function () {

    });

    function openModal() {
        $("#approval_modal").modal();
    }

    function PreprocessBeforeSubmiting() {
        // $('#validation-start-date').val(getDateStringById('validation-start-date-js'));
        // $('#validation-end-date').val(getDateStringById('validation-end-date-js'));
        // console.log("validation-start-date : " + $('#validation-start-date').val());
        // console.log("validation-end-date : " + $('#validation-end-date').val());
        // return dateValidator('validation-start-date-js', true, {
        //     'errorEn': 'Enter Validation Start Date!',
        //     'errorBn': 'ভ্যালিডির শুরুর দিন প্রবেশ করুন!'
        // }) && dateValidator('validation-end-date-js', true, {
        //     'errorEn': 'Enter Validation End Date!',
        //     'errorBn': 'ভ্যালিডির শেষের দিন প্রবেশ করুন!'
        // });
    }

    <%--<div class="modal-header">--%>
    <%--    <h4 class="caption" style="color: #56b2cf; margin-left: 10px">--%>
    <%--        <%=LM.getText(LC.CARD_APPROVAL_MAPPING_APPROVAL, loginDTO)%>--%>
    <%--    </h4>--%>
    <%--</div>--%>

    <%--&lt;%&ndash;------------------------------BODY--------------------------------------------&ndash;%&gt;--%>
    <%--<div class="modal-body modal-lg">--%>
    <%--    <div class="row my-5">--%>
    <%--        <div class="col-12">--%>
    <%--            <form class="form-horizontal text-right"--%>
    <%--                  action="Vm_route_travel_withdraw_approval_mappingServlet?actionType=approvedVm_withdrawal"--%>
    <%--                  id="permanentCardForm" name="permanentCardForm" method="POST"--%>
    <%--                  enctype="multipart/form-data" onsubmit="return PreprocessBeforeSubmiting()">--%>

    <%--                <input type="hidden" name="cardInfoId" id="cardInfoId" value='<%=cardInfoId%>'>--%>
    <%--                    <div class="row my-5" align="right">--%>
    <%--                        <div class="col-12">--%>
    <%--                            <button type="button" class="btn btn-danger shadow" style="border-radius: 8px;"--%>
    <%--                                    data-dismiss="modal"--%>
    <%--                            ><%=LM.getText(LC.CLOSE_CLOSE, loginDTO)%>--%>
    <%--                            </button>--%>
    <%--                            <button class="btn btn-success shadow ml-2" style="border-radius: 8px;"--%>
    <%--                                    type="submit"><%=LM.getText(LC.CARD_INFO_ADD_CARD_INFO_SUBMIT_BUTTON, loginDTO)%>--%>
    <%--                            </button>--%>
    <%--                        </div>--%>
    <%--                    </div>--%>
    <%--            </form>--%>
    <%--        </div>--%>
    <%--    </div>--%>
    <%--</div>--%>

    const satisfiedClicked =  () => {
        Swal.fire({
            title: '<%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_ARE_YOU_SURE, loginDTO)%>',
            text: "<%=LM.getText(LC.BUDGET_PASSWORD_ADD_YOU_WONT_BE_ABLE_TO_REVERT_THIS, loginDTO)%>",
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_APPROVAL, loginDTO)%>',
            cancelButtonText: '<%=LM.getText(LC.OFFICES_ADD_OFFICES_CANCEL_BUTTON, loginDTO)%>'
        }).then((result) => {
            console.log(result);
            if (result.value) {
                document.forms.permanentCardForm.submit();
            }
        })
    }

</script>





