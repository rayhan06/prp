<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page contentType="text/html;charset=utf-8" %>

<%
    String url = "Employee_employmentServlet?actionType=search";
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>

    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="name_en"><%=LM.getText(LC.EMPLOYEE_EMPLOYMENT_SEARCH_NAMEEN, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="name_en" placeholder="" name="name_en"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="name_bn"><%=LM.getText(LC.EMPLOYEE_EMPLOYMENT_SEARCH_NAMEBN, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="name_bn" placeholder="" name="name_bn"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="description"><%=LM.getText(LC.EMPLOYEE_EMPLOYMENT_SEARCH_DESCRIPTION, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="description" placeholder="" name="description"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit" class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>

<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>
<script type="text/javascript">
    const anyFieldSelector = $('#anyfield');
    const nameEnSelector = $('#name_en');
    const nameBnSelector = $('#name_bn');
    const descriptionSelector = $('#description');
    window.addEventListener('popstate',e=>{
        if(e.state){
            let params = e.state;
            dosubmit(params,false);
            resetInputs();
            let arr = params.split('&');
            arr.forEach(e=>{
                let item = e.split('=');
                if(item.length === 2){
                    switch (item[0]){
                        case 'name_en':
                            nameEnSelector.val(item[1]);
                            break;
                        case 'name_bn':
                            nameBnSelector.val(item[1]);
                            break;
                        case 'description':
                            descriptionSelector.val(item[1]);
                            break;
                        case 'AnyField':
                            anyFieldSelector.val(item[1]);
                            break;
                        default:
                            setPaginationFields(item);
                    }
                }
            });
        }else{
            dosubmit(null,false);
            resetInputs();
            resetPaginationFields();
        }
    });

    function resetInputs(){
        anyFieldSelector.val('');
        nameBnSelector.val('');
        nameEnSelector.val('');
        descriptionSelector.val('');
    }

    $(document).ready(()=>{
        readyInit('Employee_employmentServlet');
    });

    function dosubmit(params,pushState=true) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if(pushState){
                    history.pushState(params,'','Employee_employmentServlet?actionType=search&'+params);
                }
                setTimeout(() => {
                    document.getElementById('tableForm').innerHTML = this.responseText ;
                    setPageNo();
                    searchChanged = 0;
                }, 500);
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        let url = "Employee_employmentServlet?actionType=search&isPermanentTable=true&ajax=true"
        if(params){
            url += "&"+params;
        }
        xhttp.open("GET", url, false);
        xhttp.send();
    }

    function allfield_changed(go, pagination_number,pushState=true) {
        let params = 'search=true';
        if(anyFieldSelector.val()){
            params += '&AnyField=' + anyFieldSelector.val();
        }
        if(nameEnSelector.val()){
            params += '&name_en=' + nameEnSelector.val();
        }
        if(nameBnSelector.val()){
            params += '&name_bn=' + nameBnSelector.val();
        }

        let extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        let pageNo = document.getElementsByName('pageno')[0].value;
        let rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        let totalRecords = 0;
        let lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        // dosubmit(params,pushState);
        searchNav.dosubmit('Employee_employmentServlet?actionType=search',params,pushState);
    }
</script>