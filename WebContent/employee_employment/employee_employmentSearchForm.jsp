<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="employee_employment.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>

<%@page pageEncoding="UTF-8" %>
<%
    String navigator2 = "navEMPLOYEE_EMPLOYMENT";
    String servletName = "Employee_employmentServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.EMPLOYEE_EMPLOYMENT_EDIT_NAMEEN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_EMPLOYMENT_EDIT_NAMEBN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_EMPLOYMENT_EDIT_DESCRIPTION, loginDTO)%>
            </th>
            <th style="width: 75px"><%=LM.getText(LC.EMPLOYEE_EMPLOYMENT_SEARCH_EMPLOYEE_EMPLOYMENT_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="text-center" style="width: 75px">
                <span><%=isLanguageEnglish ? "All" : "সকল"%></span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>

        <tbody class="text-nowrap">
        <%
            List<Employee_employmentDTO> data = (List<Employee_employmentDTO>) rn2.list;

            if (data != null && data.size() > 0) {
                for (Employee_employmentDTO employee_employmentDTO : data) {
        %>
        <tr>
            <td>
                <%=employee_employmentDTO.nameEn%>
            </td>
            <td>
                <%=employee_employmentDTO.nameBn%>
            </td>

            <td>
                <%=employee_employmentDTO.description%>
            </td>

            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #ff6b6b;"
                        onclick="location.href='Employee_employmentServlet?actionType=getEditPage&ID=<%=employee_employmentDTO.iD%>'">
                    <i class="fa fa-edit"></i>
                </button>
            </td>
            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID' value='<%=employee_employmentDTO.iD%>'/></span>
                </div>
            </td>
        </tr>
        <% }
        } %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>