<%@page import="login.LoginDTO" %>
<%@page import="employee_employment.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    Employee_employmentDTO employee_employmentDTO;
    String actionName,formTitle;
    if("edit".equals(request.getParameter("actionType"))){
        actionName = "edit";
        formTitle = LM.getText(LC.EMPLOYEE_EMPLOYMENT_EDIT_EMPLOYEE_EMPLOYMENT_EDIT_FORMNAME, loginDTO);
        employee_employmentDTO = (Employee_employmentDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    }else{
        actionName = "add";
        formTitle = LM.getText(LC.EMPLOYEE_EMPLOYMENT_ADD_EMPLOYEE_EMPLOYMENT_ADD_FORMNAME, loginDTO);
        employee_employmentDTO = new Employee_employmentDTO();
    }
    String contextOfPath = request.getContextPath() + "/";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form" id="bigform" name="bigform" enctype="multipart/form-data"
            action="Employee_employmentServlet?actionType=ajax_<%=actionName%>" method="POST">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD'
                                           id='iD_hidden_0'
                                           value='<%=employee_employmentDTO.iD%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="nameEn_text_0">
                                            <%=LM.getText(LC.EMPLOYEE_EMPLOYMENT_ADD_NAMEEN, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9" id='nameEn_div_0'>
                                            <input type='text' class='englishOnly form-control'
                                                   name='nameEn' id='nameEn_text_0'
                                                   required="required"
                                                   placeholder='<%=Language.equalsIgnoreCase("English")?"Write Name English":"নাম (ইংরেজি) লিখুন"%>'
                                                   value='<%=employee_employmentDTO.nameEn == null?"":employee_employmentDTO.nameEn%>'   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="nameBn_text_0">
                                            <%=LM.getText(LC.EMPLOYEE_EMPLOYMENT_ADD_NAMEBN, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9 " id='nameBn_div_0'>
                                            <input type='text' class='noEnglish form-control'
                                                   name='nameBn' id='nameBn_text_0'
                                                   required="required"
                                                   placeholder='<%=Language.equalsIgnoreCase("English")?"Write Name Bangla":"নাম (বাংলা) লিখুন"%>'
                                                   value='<%=employee_employmentDTO.nameBn == null?"":employee_employmentDTO.nameBn%>'   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="description_text_0">
                                            <%=LM.getText(LC.EMPLOYEE_EMPLOYMENT_ADD_DESCRIPTION, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9 " id='description_div_0'>
                                                <textarea type='text' class='form-control' rows="3"
                                                          name='description' style="resize: none"
                                                          id='description_text_0' required="required" maxlength="128"
                                                          placeholder='<%=isLangEng?"Write Brief Description":"সংক্ষিপ্ত বিবরণ লিখুন"%>'
                                                          style="text-align: left"><%=employee_employmentDTO.description == null ?"":employee_employmentDTO.description%></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                type="button" onclick="location.href = '<%=request.getHeader("referer")%>'">
                            <%=LM.getText(LC.EMPLOYEE_WORK_AREA_EDIT_EMPLOYEE_WORK_AREA_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button class="btn-sm shadow text-white border-0 submit-btn ml-2" id="submit-btn"
                                type="button" onclick="submitForm()">
                            <%=LM.getText(LC.EMPLOYEE_WORK_AREA_EDIT_EMPLOYEE_WORK_AREA_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    const form = $("#bigform");

    $(document).ready(function () {
        form.validate({
            nameEn: "required",
            nameBn: "required",
            description: "required",
            messages: {
                nameEn: '<%=isLangEng? "Please insert English Name" : "ইংরেজি নাম দিন"%>',
                nameBn: '<%=isLangEng? "Please insert Bangla Name" : "বাংলা নাম দিন"%>',
                description: '<%=isLangEng? "Please insert Description" : "বিবরণ দিন"%>'
            }
        });
    });

    function submitForm(){
        buttonStateChange(true);
        if (form.valid()) {
            submitAjaxForm('bigform');
        } else {
            buttonStateChange(false);
        }
        buttonStateChange(true);
    }

    function buttonStateChange(value){
        $('#submit-btn').prop('disabled',value);
        $('#cancel-btn').prop('disabled',value);
    }
</script>