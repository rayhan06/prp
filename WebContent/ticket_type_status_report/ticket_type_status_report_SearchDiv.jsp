<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="workflow.WorkflowController"%>
<%@ page import="pb.*" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);   
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.TICKET_TYPE_STATUS_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row">
    <div class="col-12">
        <%@include file="../pbreport/yearmonth.jsp"%>
        <%@include file="../pbreport/calendar.jsp"%>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.TICKET_TYPE_STATUS_REPORT_WHERE_TICKETISSUESTYPE, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='ticketIssuesType' id = 'ticketIssuesType' >		
						<%		
						Options = CommonDAO.getOptions(Language, "ticket_issues", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
	
    </div>
</div>
<script type="text/javascript">
function init()
{
    dateTimeInit($("#Language").val());
    $("#search_by_date").prop('checked', true);
    $("#search_by_date").trigger("change");
    setDateByStringAndId('startDate_js', '<%=datestr%>');
    setDateByStringAndId('endDate_js', '<%=datestr%>');
    add1WithEnd = true;
    processNewCalendarDateAndSubmit();
    console.log("date checked");
}
function PreprocessBeforeSubmiting()
{
}
</script>