		<div class="row div_border office-div">

                    <div class="col-md-12">
                        <h3 style="text-align: center;">
                        <h4><u>Physical Examination</u></h4>
						<table class="table table-bordered table-striped">
									

						


							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_QUANTITY, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.quantity + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>


							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_COLOR, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.color + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>
							

			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_REACTION, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.reaction + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_GRAVITY, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.gravity + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				
							</table>
							<h4><u>Chemical Examination</u></h4>
							<table class="table table-bordered table-striped">

			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_ALBUMIN, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.albumin + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_SUGAR, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.sugar + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_BILESALTS, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.bileSalts + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				
							</table>
							<h4><u>Microscopical Examination</u></h4>
							<table class="table table-bordered table-striped">

			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_PUSSCELLS, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.pussCells + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_EPCELLS, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.epCells + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_RBC, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.rbc + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_RBCCAST, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.rbcCast + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_CRYSTALS, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.crystals + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_OTHERTHINGS, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.otherThings + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			


			
			
			
		
						</table>
                    </div>
			






			</div>	
