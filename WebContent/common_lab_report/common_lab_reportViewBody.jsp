<%@page import="doctor_time_slot.*"%>
<%@page import="user.*"%>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="common_lab_report.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="lab_test.*" %>
<%@page import="urine_examination_report.*" %>
<%@ page import="java.awt.image.BufferedImage" %>
<%@ page import="javax.imageio.ImageIO" %>
<%@ page import="java.io.ByteArrayOutputStream" %>
<%@ page import="java.io.File" %>
<%@ page import="family.*" %>
<%@ page import="appointment.*" %>
<%@ page import="usg_report.*" %>
<%@page import="prescription_details.*" %>
<%@page import="org.apache.commons.lang3.StringEscapeUtils" %>

<style>

    .row.div_border.office-div table tbody tr td p {
        margin-left: 15px;
    }

    .table_border {
        display: block;
        float: left;
        width: 100%;
        margin-top: 20px;
        border: 1px solid #000;
        padding: 10px;
    }

    @media print {
        .print-btn-container {
            display: none !important;
        }
    }


</style>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);


	UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
	userDTO.languageID = SessionConstants.ENGLISH;
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = "english";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Common_lab_reportDAO common_lab_reportDAO = new Common_lab_reportDAO("common_lab_report");
    Common_lab_reportDTO common_lab_reportDTO = common_lab_reportDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    Lab_testDAO lab_testDAO = new Lab_testDAO();
    Lab_testDTO lab_testDTO = lab_testDAO.getDTOByID(common_lab_reportDTO.labTestId);

 

    boolean xRayOrUsg = lab_testDTO.iD == SessionConstants.LAB_TEST_XRAY
            || lab_testDTO.iD == SessionConstants.LAB_TEST_ULTRASONOGRAM;


    String context = request.getContextPath() + "/";


    Prescription_detailsDAO prescription_detailsDAO = new Prescription_detailsDAO();
    PrescriptionLabTestDAO prescriptionLabTestDAO = new PrescriptionLabTestDAO();
    AppointmentDAO appointmentDAO = new AppointmentDAO();

    LabTestListDAO labTestListDAO = new LabTestListDAO();


    PrescriptionLabTestDTO prescriptionLabTestDTO = prescriptionLabTestDAO.getDTOByID(common_lab_reportDTO.prescriptionLabTestId);
    Prescription_detailsDTO prescription_detailsDTO = prescription_detailsDAO.getDTOByID(prescriptionLabTestDTO.prescriptionDetailsId);
    AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(prescription_detailsDTO.appointmentId);

    Usg_reportDAO usg_reportDAO = new Usg_reportDAO();
    Usg_reportDTO usg_reportDTO = null;

    if (xRayOrUsg) {
        usg_reportDTO = usg_reportDAO.getDTOByCommonLabReportId(common_lab_reportDTO.iD);
        if (usg_reportDTO == null) {
            usg_reportDTO = new Usg_reportDTO();
        }
    }

	Language = "english";
	boolean isUrine = lab_testDTO.iD == SessionConstants.LAB_TEST_URINE;
%>
<jsp:include page="../utility/jquery_print.jsp"/>
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="d-flex align-items-center justify-content-end">
                
                <button type="button" class="btn" id='printer2'
                        onclick="printDivWithJqueryPrint('modalbody')">
                    <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
                </button>
                <button type="button" class="btn pr-0" id='cross'
                        onclick="location.href='Prescription_detailsServlet?actionType=search&addLabTest=1'">
                    <i class="fa fa-times fa-2x" style="color: gray" aria-hidden="true"></i>
                </button>
                <%
                if
                (
                	(
                		prescriptionLabTestDTO.approvalStatus == PrescriptionLabTestDTO.APPROVED
                		&&
                		(
                			userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE
	                        || userDTO.roleID == SessionConstants.ADMIN_ROLE
	                        ||userDTO.roleID == SessionConstants.PATHOLOGY_HEAD
                        )
                	)
                	||
                	(
                		prescriptionLabTestDTO.approvalStatus != PrescriptionLabTestDTO.APPROVED
                		&&
                		(
	                		userDTO.roleID == SessionConstants.PATHOLOGY_HEAD		
	                		|| userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE
	                        || userDTO.roleID == SessionConstants.ADMIN_ROLE
	                        ||userDTO.roleID == SessionConstants.LAB_TECHNITIAN_ROLE
                        )
                	)
                )
                
                {
                %>
                <button type="button" class="btn pr-0" id='cross'
                        onclick="location.href='Common_lab_reportServlet?actionType=getEditPage&ID=<%=common_lab_reportDTO.iD%>'">
                    <i class="fa fa-edit fa-2x" style="color: gray" aria-hidden="true"></i>
                </button>
                <%
                }
                %>
            </div>
        </div>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet">
        <div class="kt-portlet__body" id="modalbody">
			<div class="d-flex justify-content-between mt-3">
				<div class="d-flex align-items-center">
					<div>
						<img width="40%"
							src="<%=context%>assets/static/parliament_logo.png" alt="logo"
							class="logo-default" />
					</div>
					<div class="prescription-parliament-info2" style="color: #1389bc">
						<h3 class="text-left">
							<%=LM.getText(LC.HM_PARLIAMENT_MEDICAL_CENTRE, loginDTO)%>
						</h3>
						<h5 class="text-left">
							<%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
						</h5>
						<h6 class="text-left">
							<%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
						</h6>
						<h6 class="text-left">
							<%=LM.getText(LC.HM_PARLIAMENT_PHONE, loginDTO)%>
						</h6>
					</div>
				</div>

				<div class="text-center pt-2"
					style="height: 40px; width: 200px; border: 1px solid #D3D3D3; border-radius: 3px">
					<span class="lead font-weight-bold text-dark">Reference No :
					</span> <span class="lead font-weight-bold" style="color: #0a6aa1;">
						<%
							value = xRayOrUsg ? common_lab_reportDTO.iD + "" : common_lab_reportDTO.reference;
						%> <%=value%></span>
				</div>
			</div>
			<div class="row mt-5">
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap">
                            <tr>
                                <td><b><%=LM.getText(LC.COMMON_LAB_REPORT_EDIT_NAME, loginDTO)%>
                                </b></td>
                                <td>
                                    <%
                                    	value = common_lab_reportDTO.name + "";
                                    %>
                                    <%=value%>
                                </td>
                                <td >
                                    <b><%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%>
                                    </b>
                                </td>
                                <td>
                                    <%
                                        value = appointmentDTO.employeeUserName + "";
                                        if (value.equalsIgnoreCase("null") || value.equalsIgnoreCase(FamilyDTO.defaultUser)) {
                                            value = "";
                                        }
                                    %>
                                    <%=Utils.getDigits(value, Language)%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b><%=LM.getText(LC.HM_DATE_OF_BIRTH, loginDTO)%>
                                        , <%=LM.getText(LC.COMMON_LAB_REPORT_EDIT_AGE, loginDTO)%>
                                    </b>
                                </td>
                                <td >
                                    <%
                                        value = simpleDateFormat.format(prescription_detailsDTO.dateOfBirth);
                                    %>
                                    <%=value%>,
                                    <%
                                        value = TimeFormat.getAge(prescription_detailsDTO.dateOfBirth, Language);
                                    %>
                                    <%=value%>
                                </td>
                                <td >
                                    <b><%=LM.getText(LC.COMMON_LAB_REPORT_EDIT_SEX, loginDTO)%>
                                    </b>
                                </td>
                                <td>
                                    <%
                                        value = common_lab_reportDTO.sex + "";
                                    %>
                                    <%=value%>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <b><%=LM.getText(LC.HM_REFERRED_BY, loginDTO)%>
                                    </b>
                                </td>
                                <td >
                                    <%
                                        value = common_lab_reportDTO.referredBy + "";
                                    %>
                                    <%=value%>
                                </td>
                                <td >
                                    <b>
                                        <%=LM.getText(LC.HM_REPORT_DATE, loginDTO)%>
                                    </b>
                                </td>
                                <td>
                                    <%
                                        value = common_lab_reportDTO.insertionDate + "";
                                    %>
                                    <%
                                        String formatted_insertionDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                    %>
                                    <%=formatted_insertionDate%>
                                </td>
                            </tr>
                            <tr>
                                <%
                                    if (!xRayOrUsg) {
                                %>
                                <td >
                                    <b><%=LM.getText(LC.COMMON_LAB_REPORT_EDIT_SPECIMEN, loginDTO)%>
                                    </b>
                                </td>
                                <%
                                } else {
                                %>
                                <td ><b><%=lab_testDTO.nameEn%> Of</b></td>
                                <%
                                    }
                                %>
                                <td colspan="3">
                                    <%
                                        if (!xRayOrUsg) {

                                            value = lab_testDTO.specimen + "";

                                        } else if (usg_reportDTO != null) {
                                            if (lab_testDTO.iD == SessionConstants.LAB_TEST_ULTRASONOGRAM) {
                                                value = CommonDAO.getName(Language, "ultrasono_type", usg_reportDTO.ultrasonoTypeType);
                                            } else {
                                                value = CommonDAO.getName(Language, "xray_report", usg_reportDTO.xRayType);
                                            }
                                        }
                                    %>
                                    <%=value%>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12">
                    <%
                        if (!xRayOrUsg) {
                    %>
                    <h3 style="text-align: center;">
                        <u>
                            <%=prescriptionLabTestDTO.description == null ? "" : prescriptionLabTestDTO.description%>
                            <%=lab_testDTO.nameEn%>
                            Report
                        </u>
                    </h3>
                    <%
                    } else if (prescriptionLabTestDTO.labTestType == SessionConstants.LAB_TEST_XRAY) {
                    %>
                    <h3 style="text-align: center;">
                        <u>
                            <%=usg_reportDTO.title%>
                        </u>
                    </h3>
                    <%
                    } else {
                    %>
                    <h2 style="text-align: center;">
                        <u>
                            Part Scanned: <%=usg_reportDTO.title%>
                        </u>
                    </h2>
                    <h5 style="text-align: center;">Thank you for your kind referral</h5><br>
                    <%
                        }
                    %>
                    <%
                    if(xRayOrUsg)
                    {
                    %>
                    <div class="col-md-12">
                        <%=common_lab_reportDTO.comment == null ? "" : (common_lab_reportDTO.comment)%>
                    </div>
                    <%
                    }
                    %>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12">
                  
                    <%
                        CommonLabReportDetailsDAO commonLabReportDetailsDAO = new CommonLabReportDetailsDAO();
                        List<CommonLabReportDetailsDTO> commonLabReportDetailsDTOs = commonLabReportDetailsDAO.getCommonLabReportDetailsDTOListByCommonLabReportID(common_lab_reportDTO.iD);

                        if (commonLabReportDetailsDTOs != null && commonLabReportDetailsDTOs.size() > 0) 
                        {
                    %>
                    <div >
                    	<%
                    	if(common_lab_reportDTO.labTestId == SessionConstants.LAB_TEST_SERO)
                        {
                    		
                    		for (CommonLabReportDetailsDTO commonLabReportDetailsDTO : commonLabReportDetailsDTOs) 
                            {
                    			if(commonLabReportDetailsDTO.labTestListId == SessionConstants.LAB_TEST_LIST_BG)
                    			{
                    				%>
                    				<div style = "text-align: center;">
                    					<u><b> <%=commonLabReportDetailsDTO.testName%></b></u>
                    				</div>
                    				<br>
                    				<%
                    			}
                    			else if(commonLabReportDetailsDTO.labTestListId == SessionConstants.LAB_TEST_LIST_ABO)
                    			{
                    				%>
                    				<table class="table table-bordered ">
                    					<tr>
                    						<td style = "width:50%"> <b><%=commonLabReportDetailsDTO.testName%></b>
                    						</td>
                    						<td><b><%=commonLabReportDetailsDTO.testResult%></b>
                    						</td>
                    					</tr>
                    				
                    				<%
                    			}
                    			else if(commonLabReportDetailsDTO.labTestListId == SessionConstants.LAB_TEST_LIST_RH)
                    			{
                    				%>
                    				
                    					<tr>
                    						<td style = "width:50%"> <b><%=commonLabReportDetailsDTO.testName%></b>
                    						</td>
                    						<td><b><%=commonLabReportDetailsDTO.testResult%></b>
                    						</td>
                    					</tr>
                    				</table>
                    				<%
                    			}
                            }
                    		%>
                    		<br>
                    		<%
                        }
                    	%>
                    	
                        <table class="table table-bordered table-striped text-nowrap">
                        	<%
                        	if(!isUrine)
                        	{
                        	%>
                            <tr>
                            <%
                            if(common_lab_reportDTO.labTestId == SessionConstants.LAB_TEST_HEMATO)
                            {
                            	%>
                            	<th class="w-25">Parameter
                                </th>
                                <th class="w-25">Result
                                </th>
                                <th class="w-25">Reference Value
                                </th>
                            	<%
                            }
                            else
                            {
                            	%>
                            	<th class="w-25"><%=LM.getText(LC.COMMON_LAB_REPORT_ADD_COMMON_LAB_REPORT_DETAILS_TESTNAME, loginDTO)%>
                                </th>
                                <th class="w-25"><%=LM.getText(LC.COMMON_LAB_REPORT_ADD_COMMON_LAB_REPORT_DETAILS_TESTRESULT, loginDTO)%>
                                </th>
                                <th class="w-25"><%=LM.getText(LC.COMMON_LAB_REPORT_ADD_COMMON_LAB_REPORT_DETAILS_STANDARDVALUE, loginDTO)%>
                                </th>
                            	<%
                            }
                            %>
                                
                            </tr>
                            <%
                        	}
                            %>
                            <%
                            	boolean oddColumn = true;
                            	boolean trOpen = false;
                            	boolean tableExists  = true;
                                for (CommonLabReportDetailsDTO commonLabReportDetailsDTO : commonLabReportDetailsDTOs) 
                                {
                                	if(commonLabReportDetailsDTO.testResult.equalsIgnoreCase("")
                                	&& !commonLabReportDetailsDTO.isTitle
                                	&& !commonLabReportDetailsDTO.testName.equalsIgnoreCase("")
                                	&& !isUrine
                                	)
                                	{
                                		continue;
                                	}
                            	%>
	                            <%
	                            if(commonLabReportDetailsDTO.labTestListId == SessionConstants.LAB_TEST_LIST_BG
	                            || commonLabReportDetailsDTO.labTestListId == SessionConstants.LAB_TEST_LIST_ABO
	                            || commonLabReportDetailsDTO.labTestListId == SessionConstants.LAB_TEST_LIST_RH)
	                            {
	                            	continue;
	                            }
	                            %>
                            <%
                            if(commonLabReportDetailsDTO.testName.equalsIgnoreCase("") 
                        		&& commonLabReportDetailsDTO.testResult.equalsIgnoreCase("") && !isUrine)
							{
                            	if(trOpen)
                            	{
                            		trOpen = false;
                            		%>
                            		</tr>
                            		<% 
                            	}
                            	if(tableExists)
                            	{
									tableExists = false;
									%>									
									</table>
									<br>
									<%
                            	}
								continue;
							}
                            %>
                            
                            <%
                            if(commonLabReportDetailsDTO.isTitle 
                            		&& commonLabReportDetailsDTO.testResult.equalsIgnoreCase(""))
							{
                            	if(trOpen)
                            	{
                            		trOpen = false;
                            		%>
                            		</tr>
                            		<% 
                            	}
                            	if(tableExists)
                            	{
									tableExists = false;
									%>
									</table>
									<br>
									<%
                            	}
                            	%>
                            	<u><b> <%=commonLabReportDetailsDTO.testName%></b></u>
                            	<%
								continue;
							}
                            %>
                            
                            <%
                            if(!tableExists)
                            {
                            	%>
                            	<table class="table table-bordered table-striped text-nowrap">                            	
                            	<%
                            	tableExists = true;
                            }
                            %>
                            <%
                            if(!isUrine)
                            {
                            %>
                            <tr>
                                <td class="w-25">
                                    <%
                                        value = commonLabReportDetailsDTO.testName + "";
                                        if (commonLabReportDetailsDTO.isTitle) {
                                            value = "<b>" + value + "</b>";
                                        }
                                    %>

                                    <%=value%>
                                </td>
                                <td class="w-25">

                                    <%=commonLabReportDetailsDTO.testResult%> <%=commonLabReportDetailsDTO.unit%>
                                    
                                </td>
                                <td class="w-25">
                                  
                                    <%=commonLabReportDetailsDTO.standardValue%>
                                </td>
                                
                            </tr>
                            <%
                            }
                            else
                            {
                            %>
                            <%
                            if(oddColumn)
                            {
                            	if(trOpen)
                            	{
                            		%>
                            		</tr>
                            		<%
                            		trOpen = false;
                            	}
                            	%>
                            	<tr>
                            	<%
                            	trOpen = true;
                            	
                            }
                            oddColumn = !oddColumn;
                            %>
                             
                                <td class="w-25">
                                    <%
                                        value = commonLabReportDetailsDTO.testName + "";
                                        if (commonLabReportDetailsDTO.isTitle) {
                                            value = "<b>" + value + "</b>";
                                        }
                                    %>

                                    <%=value%>
                                </td>
                                <td class="w-25">

                                    <%=commonLabReportDetailsDTO.testResult%><%=commonLabReportDetailsDTO.unit%>
                                    
                                </td>
                               

                            	
                            <%
                            }
                            %>
                            <%

                                }

                            %>
                            <%
                            if(trOpen)
                            {
                            	%>
                            	</tr>
                            	<% 
                            	trOpen = false;
                            }
                            %>
                        </table>
                    </div>
                    <%
                    if(!common_lab_reportDTO.comment.equalsIgnoreCase(""))
                    {
                    %>
                    <div  class="row mt-5">
                    	<table class="table table-bordered table-striped text-nowrap">
                    		<tr>
                    			<td width = "20%"><b>Remarks:</b></td>
                    			<td><%=common_lab_reportDTO.comment%></td>
                    		</tr>
                    	</table>
                    </div>
                    <%
                    }
                    %>
                </div>
            </div>
            <div class="mt-5">
                <div class="">
                    <%
                        }
                    %>
                   
                   <%
                   if(prescriptionLabTestDTO.approvalStatus == PrescriptionLabTestDTO.APPROVED)
                   {
                   %>
                    <div class="d-flex justify-content-between div_border office-div">
                        
                            <div class="doctorsignature">
                                
								<div class="">
	                                <div class="text-center" >
	                                	<%
		                                	byte [] b64Lab = WorkflowController.getBase64SignatureFromUserName(prescriptionLabTestDTO.xRayUserName);
		                                    if (b64Lab != null) {
		                                %>
		                                <div class="">
		                                <img src='data:image/jpg;base64,<%=b64Lab != null ? new String(b64Lab) : ""%>' style="width:150px"
			                                     id="signature-img">
		                                </div>
		                                <%
		                                    }
		                                %>
	                                	<p class="mb-1" style="text-decoration: overline;">
	                            			<%=WorkflowController.getNameFromUserName(prescriptionLabTestDTO.xRayUserName, "English")%>
	                        			</p>
	                        			<%
	                        			UserDTO xRayUserDTO = UserRepository.getUserDTOByUserName(prescriptionLabTestDTO.xRayUserName);
	                        			if(xRayUserDTO != null)
	                        			{
	                        			%>
	                        			<small class="mb-2">	                        			
	                            			<%=WorkflowController.getOrganogramName(xRayUserDTO.organogramID, "English")%>
	                        			</small>
	                        			<br>
	                        			<%
	                        			}
	                        			%>
	                        			<small class="mb-2">
	                        				Medical Center
	                        			</small>
	                        			<br>
	                        			<small class="mb-2">
	                        				Bangladesh Parliament Secretariat
	                        			</small>
	                                </div>
                                </div>

                            </div>
                        

                            <div class="doctorsignature">                                
                                <div class="">
	                                <div class="text-center" >
	                                	<%
		                                	byte [] b64 = WorkflowController.getBase64SignatureFromUserName(prescriptionLabTestDTO.approverUserName);
		                                    if (b64 != null) {
		                                %>
		                                <div class="">
		                                <img src='data:image/jpg;base64,<%=b64 != null ? new String(b64) : ""%>' style="width:150px"
			                                     id="signature-img">
		                                </div>
		                                <%
		                                    }
		                                %>
	                                	<p class="mb-1" style="text-decoration: overline;">
	                            			<%=WorkflowController.getNameFromUserName(prescriptionLabTestDTO.approverUserName, "English")%>
	                        			</p>
	                        			<%
	                        			UserDTO approverUserDTO = UserRepository.getUserDTOByUserName(prescriptionLabTestDTO.approverUserName);
	                        			if(approverUserDTO != null)
	                        			{
	                        			%>
	                        			<small class="mb-2">	                        			
	                            			<%=WorkflowController.getOrganogramName(approverUserDTO.organogramID, "English")%>
	                        			</small>
	                        			<br>
	                        			<%
	                        			}
	                        			%>
	                        			<small class="mb-2">
	                        				Medical Center
	                        			</small>
	                        			<br>
	                        			<small class="mb-2">
	                        				Bangladesh Parliament Secretariat
	                        			</small>
	                                </div>
                                </div>

                            </div>
                        
                    </div>
                    <%
                   }
                   else if(prescriptionLabTestDTO.approvalStatus == PrescriptionLabTestDTO.PENDING)
                   {
                	  %>
                	  <div class="div_border office-div">
                	  	<h3>APPROVAL PENDING</h3>
                	  </div>
                	  <% 
                   }
                   else if(prescriptionLabTestDTO.approvalStatus == PrescriptionLabTestDTO.REJECTED)
                   {
                	  %>
                	  <div class="div_border office-div">
                	  	<h3>REJECTED</h3>
                	  </div>
                	  <% 
                   }                 
                    %>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>