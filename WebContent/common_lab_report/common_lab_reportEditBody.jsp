<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="common_lab_report.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import="util.TimeFormat"%>
<%@page import="prescription_details.*"%>
<%@page import="appointment.*"%>
<%@page import="lab_test.*"%>
<%@page import="pb.*"%>
<%@page import="urine_examination_report.*"%>
<%@page import="user.*"%>
<%@ page import="pb.*"%>

<%
Common_lab_reportDTO common_lab_reportDTO;
common_lab_reportDTO = (Common_lab_reportDTO)request.getAttribute("common_lab_reportDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.COMMON_LAB_REPORT_EDIT_LANGUAGE, loginDTO);
boolean isLangEng = Language.equalsIgnoreCase("english");
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}

if(common_lab_reportDTO == null)
{
	common_lab_reportDTO = new Common_lab_reportDTO();
	common_lab_reportDTO.prescriptionLabTestId = Long.parseLong(request.getParameter("prescription_lab_test_id"));
	common_lab_reportDTO.labTestId = Long.parseLong(request.getParameter("lab_test_id"));
	common_lab_reportDTO.labTestDetails = request.getParameter("lab_test_details");
}

Prescription_detailsDAO prescription_detailsDAO = new Prescription_detailsDAO();
PrescriptionLabTestDAO prescriptionLabTestDAO = new PrescriptionLabTestDAO();
AppointmentDAO appointmentDAO = new AppointmentDAO();
Lab_testDAO lab_testDAO = new Lab_testDAO();
LabTestListDAO labTestListDAO = new LabTestListDAO();


PrescriptionLabTestDTO prescriptionLabTestDTO = prescriptionLabTestDAO.getDTOByID(common_lab_reportDTO.prescriptionLabTestId);
Prescription_detailsDTO prescription_detailsDTO = prescription_detailsDAO.getDTOByID(prescriptionLabTestDTO.prescriptionDetailsId);
AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(prescription_detailsDTO.appointmentId);
Lab_testDTO lab_testDTO = lab_testDAO.getDTOByID(common_lab_reportDTO.labTestId);

boolean isXrayType = lab_testDTO.nameEn.toLowerCase().contains("xray")
        || lab_testDTO.nameEn.toLowerCase().contains("x-ray")
        || lab_testDTO.nameEn.toLowerCase().contains("ultrasonogram")
        || lab_testDTO.nameEn.toLowerCase().contains("pregnancy");

    if(actionName.equalsIgnoreCase("add"))
{
	common_lab_reportDTO.name = prescription_detailsDTO.name;
	common_lab_reportDTO.age = (int)prescription_detailsDTO.age;
	common_lab_reportDTO.sex = CatDAO.getName(Language, "gender", appointmentDTO.genderCat);
	common_lab_reportDTO.referredBy = WorkflowController.getNameFromOrganogramId(appointmentDTO.doctorId, Language);
	common_lab_reportDTO.specimen = lab_testDTO.specimen;

	if(isXrayType)
	{
		common_lab_reportDTO.specimen = prescriptionLabTestDTO.description;
	}




	common_lab_reportDTO.commonLabReportDetailsDTOList = new ArrayList<CommonLabReportDetailsDTO>();
	String[] labTestDetailsIdsStrs = common_lab_reportDTO.labTestDetails.split(", ");

	List<Long> ids = Utils.StringToArrayList(common_lab_reportDTO.labTestDetails);
	List<LabTestListDTO> labTestListDTOs = labTestListDAO.getLabTestListDTOListByLabTestID(common_lab_reportDTO.labTestId);
	
	if(labTestListDTOs != null)
	{
		boolean addNext = false;
		boolean addAll = ids.size() == 0;
        for(LabTestListDTO labTestListDTO: labTestListDTOs)
		{
			if(addAll || addNext || ids.contains(labTestListDTO.iD) 
					|| labTestListDTO.nameEn.equalsIgnoreCase("")
					|| labTestListDTO.isTitle)
			{
				CommonLabReportDetailsDTO commonLabReportDetailsDTO = new CommonLabReportDetailsDTO();
	
				commonLabReportDetailsDTO.set(labTestListDTO);
				common_lab_reportDTO.commonLabReportDetailsDTOList.add(commonLabReportDetailsDTO);
				
				if(labTestListDTO.isTitle)
				{
					addNext = true;
				}
				else if(labTestListDTO.nameEn.equalsIgnoreCase(""))
				{
					addNext = false;
				}
			}
		}
	}

	
}


System.out.println("common_lab_reportDTO = " + common_lab_reportDTO);
String formTitle = lab_testDTO.nameEn +  " Report";
String servletName = "Common_lab_reportServlet";
String fileColumnName = "";

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";
int childTableStartingID = 1;

boolean isPermanentTable = true;

String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
boolean isApproval = Boolean.parseBoolean(request.getParameter("isApproval"));

%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title prp-page-title">
                            <i class="fa fa-gift"></i>&nbsp;
                            <%=formTitle%>
                        </h3>
                    </div>
                </div>
                <form class="form-horizontal"
                      action="Common_lab_reportServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
                      id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
                      onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
                    <div class="kt-portlet__body form-body">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="onlyborder">
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="sub_title_top">
                                                        <div class="sub_title">
                                                            <h4 style="background: white"><%=formTitle%>
                                                            </h4>
                                                        </div>
                                                     </div>

														<input type='hidden' class='form-control'  name='approvalStatus' id = 'approvalStatus' value='<%=PrescriptionLabTestDTO.PENDING%>' tag='pb_html'/>

														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=common_lab_reportDTO.iD%>' tag='pb_html'/>



														<input type='hidden' class='form-control'  name='prescriptionLabTestId' id = 'prescriptionLabTestId_hidden_<%=i%>' value='<%=common_lab_reportDTO.prescriptionLabTestId%>' tag='pb_html'/>


														<input type='hidden' class='form-control'  name='labTestId' id = 'labTestId_hidden_<%=i%>' value='<%=common_lab_reportDTO.labTestId%>' tag='pb_html'/>

													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.COMMON_LAB_REPORT_ADD_NAME, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control' readonly name='name' id = 'name_text_<%=i%>' value='<%=common_lab_reportDTO.name%>'   tag='pb_html'/>				
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.COMMON_LAB_REPORT_ADD_AGE, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control' readonly name='age' id = 'age_text_<%=i%>' 
																value='<%=TimeFormat.getAge(prescription_detailsDTO.dateOfBirth, Language)%>'   tag='pb_html'/>
															</div>
                                                      </div>
                                                      <div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.COMMON_LAB_REPORT_ADD_SEX, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control' readonly name='sex' id = 'sex_text_<%=i%>' value='<%=common_lab_reportDTO.sex%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.COMMON_LAB_REPORT_ADD_REFERREDBY, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control' readonly name='referredBy' id = 'referredBy_text_<%=i%>' value='<%=common_lab_reportDTO.referredBy%>'   tag='pb_html'/>					
															</div>
                                                      </div>
                                                      
                                                      <div class="form-group row">
															<label class="col-4 col-form-label text-right">Comment</label>
															<div class="col-8">
																<textarea class='form-control' name='comment' 
																	id='comment_text_<%=i%>' tag='pb_html'><%=common_lab_reportDTO.comment%></textarea>
															</div>
														</div>
														
														<div class="form-group row">
															<label class="col-4 col-form-label text-right">Reference No.</label>
															<div class="col-8">
																<input type='text' class='form-control'  name='reference' 
																id = 'reference_text_<%=i%>' value='<%=common_lab_reportDTO.reference%>'   tag='pb_html'/>					

															</div>
														</div>
																						
																						
														<input type="hidden" class='form-control' id="specimen_<%=i%>" name="specimen" value="<%=common_lab_reportDTO.specimen%>"/>
														
																				
														<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=common_lab_reportDTO.searchColumn%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=common_lab_reportDTO.isDeleted%>' tag='pb_html'/>
											
														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=common_lab_reportDTO.lastModificationTime%>' tag='pb_html'/>
														
													
					
													</div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>                               
                          </div>
                       </div>
                      <%
					if(!common_lab_reportDTO.commonLabReportDetailsDTOList.isEmpty())
					{
					%>
					<div class="row">
                        <div class="col-lg-12">
                            <div class="kt-portlet shadow-none" style="margin-top: -20px">
                                <div class="kt-portlet__head border-bottom-0">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title prp-page-title"
                                            style="margin-bottom: -60px!important; font-size: 15px!important;">
                                            <%=LM.getText(LC.COMMON_LAB_REPORT_ADD_COMMON_LAB_REPORT_DETAILS, loginDTO)%>
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body form-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
											<thead>
												<tr>
													<th><%=LM.getText(LC.COMMON_LAB_REPORT_ADD_COMMON_LAB_REPORT_DETAILS_TESTNAME, loginDTO)%></th>
													<th><%=LM.getText(LC.COMMON_LAB_REPORT_ADD_COMMON_LAB_REPORT_DETAILS_TESTRESULT, loginDTO)%></th>
													<th><%=isLangEng?"Unit":"একক"%></th>												
													<th><%=LM.getText(LC.COMMON_LAB_REPORT_ADD_COMMON_LAB_REPORT_DETAILS_STANDARDVALUE, loginDTO)%></th>
													
												</tr>
											</thead>
											<tbody id="field-CommonLabReportDetails">
						
						
											<%
												//if(actionName.equals("edit"))
												{
													int index = -1;
													
													
													for(CommonLabReportDetailsDTO commonLabReportDetailsDTO: common_lab_reportDTO.commonLabReportDetailsDTOList)
													{
														index++;
														
														System.out.println("index index = "+index);

											%>	
							
											<tr id = "CommonLabReportDetails_<%=index + 1%>">
												<td style="display: none;">


														<input type='hidden' class='form-control'  name='commonLabReportDetails.isTitle' id = 'isTitle_hidden_<%=childTableStartingID%>' value='<%=commonLabReportDetailsDTO.isTitle%>' tag='pb_html'/>


														<input type='hidden' class='form-control'  name='commonLabReportDetails.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=commonLabReportDetailsDTO.iD%>' tag='pb_html'/>
	
												</td>
												<td style="display: none;">





														<input type='hidden' class='form-control'  name='commonLabReportDetails.commonLabReportId' id = 'commonLabReportId_hidden_<%=childTableStartingID%>' value='<%=commonLabReportDetailsDTO.commonLabReportId%>' tag='pb_html'/>
												</td>
												<td style="display: none;">





														<input type='hidden' class='form-control'  name='commonLabReportDetails.labTestListId' id = 'labTestListId_hidden_<%=childTableStartingID%>' value='<%=commonLabReportDetailsDTO.labTestListId%>' tag='pb_html'/>
												</td>
												<td>										



																<%
																if(commonLabReportDetailsDTO.isTitle)
																{
																	%>
																	<b><u><%=commonLabReportDetailsDTO.testName%></u></b>
																	<%
																}
																%>

																<input type='<%=(commonLabReportDetailsDTO.isTitle || commonLabReportDetailsDTO.testName.equalsIgnoreCase(""))?"hidden":"text" %>' class='form-control'  name='commonLabReportDetails.testName' id = 'testName_text_<%=childTableStartingID%>' value='<%=commonLabReportDetailsDTO.testName%>'   tag='pb_html'/>					
												</td>
												
												<td>										





																<input type='<%=(commonLabReportDetailsDTO.isTitle || commonLabReportDetailsDTO.testName.equalsIgnoreCase(""))?"hidden":"text" %>' class='form-control'
		 														name='commonLabReportDetails.testResult' id = 'testResult_text_<%=childTableStartingID%>' 
		 														value='<%=commonLabReportDetailsDTO.testResult%>'   tag='pb_html'/>					
												</td>
												
												<td>										





																<input type='<%=(commonLabReportDetailsDTO.isTitle || commonLabReportDetailsDTO.testName.equalsIgnoreCase(""))?"hidden":"text" %>' class='form-control'  name='commonLabReportDetails.unit' id = 'unit_text_<%=childTableStartingID%>' value='<%=commonLabReportDetailsDTO.unit%>'   tag='pb_html'/>					
												</td>
												
												<td>										





																<input type='<%=(commonLabReportDetailsDTO.isTitle || commonLabReportDetailsDTO.testName.equalsIgnoreCase(""))?"hidden":"text" %>' class='form-control'  name='commonLabReportDetails.standardValue' id = 'standardValue_text_<%=childTableStartingID%>' value='<%=commonLabReportDetailsDTO.standardValue%>'   tag='pb_html'/>					
												</td>
												
												<td style="display: none;">





														<input type='hidden' class='form-control'  name='commonLabReportDetails.isDeleted' id = 'isDeleted_hidden_<%=childTableStartingID%>' value= '<%=commonLabReportDetailsDTO.isDeleted%>' tag='pb_html'/>
											
												</td>
												<td style="display: none;">





														<input type='hidden' class='form-control'  name='commonLabReportDetails.lastModificationTime' id = 'lastModificationTime_hidden_<%=childTableStartingID%>' value='<%=commonLabReportDetailsDTO.lastModificationTime%>' tag='pb_html'/>
												</td>
											</tr>								
											<%	
														childTableStartingID ++;
													}
												}
											%>						
						
										</tbody>
									</table>
								</div>
								
								<%CommonLabReportDetailsDTO commonLabReportDetailsDTO = new CommonLabReportDetailsDTO();%>
								
					
							    </div>
                            </div>
                        </div>
                    </div>	
                      <%
					}
                      
                      %>
					
					<%
						if (prescriptionLabTestDTO.approvalStatus == PrescriptionLabTestDTO.PENDING
							&& userDTO.roleID == SessionConstants.PATHOLOGY_HEAD && isApproval) {
					%>
                    	  <div class="form-actions text-center mb-5">
	                        <button class="btn btn-danger" type="submit" id = "approve" onclick = "setApprovalStatus(<%=PrescriptionLabTestDTO.REJECTED%>)"><%=LM.getText(LC.HM_REJECT, loginDTO)%></button>									
							<button class="btn btn-success" type="submit" id = "reject" onclick = "setApprovalStatus(<%=PrescriptionLabTestDTO.APPROVED%>)"><%=LM.getText(LC.HM_APPROVE, loginDTO)%></button>
					   	  </div>
					   	  <%
                      }
                      else
                      {
                      %>
                       <div class="form-actions text-center mb-5">
	                        <a class="btn btn-danger" href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.COMMON_LAB_REPORT_ADD_COMMON_LAB_REPORT_CANCEL_BUTTON, loginDTO)%></a>										
							<button class="btn btn-success" id = "submit" type="submit"><%=LM.getText(LC.COMMON_LAB_REPORT_ADD_COMMON_LAB_REPORT_SUBMIT_BUTTON, loginDTO)%></button>
					   </div>
					   <%
                      }
					   %>
                   </form>
               </div>                      
          </div>
      </div>
 </div>

<script type="text/javascript">

function setApprovalStatus(status)
{
	$("#approvalStatus").val(status);
	console.log("set " + status);
}

function PreprocessBeforeSubmiting(row, validate)
{

	var id = $("input[type=submit]:focus" ).attr("id");
	console.log("id = " + id);


	for(i = 1; i < child_table_extra_id; i ++)
	{
		if(document.getElementById("isTitle_checkbox_" + i))
		{
			if(document.getElementById("isTitle_checkbox_" + i).getAttribute("processed") == null)
			{
				preprocessCheckBoxBeforeSubmitting('isTitle', i);		
				document.getElementById("isTitle_checkbox_" + i).setAttribute("processed","1");
			}
		}
	}
	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Common_lab_reportServlet");	
}

function init(row)
{


	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	
}

var row = 0;
$(document).ready(function(){
	init(row);

});	

var child_table_extra_id = <%=childTableStartingID%>;

	$("#add-more-CommonLabReportDetails").click(
        function(e) 
		{
            e.preventDefault();
            var t = $("#template-CommonLabReportDetails");

            $("#field-CommonLabReportDetails").append(t.html());
			SetCheckBoxValues("field-CommonLabReportDetails");
			
			var tr = $("#field-CommonLabReportDetails").find("tr:last-child");
			
			tr.attr("id","CommonLabReportDetails_" + child_table_extra_id);
			
			tr.find("[tag='pb_html']").each(function( index ) 
			{
				var prev_id = $( this ).attr('id');
				$( this ).attr('id', prev_id + child_table_extra_id);
				console.log( index + ": " + $( this ).attr('id') );
			});
			

			child_table_extra_id ++;

        });

    
      $("#remove-CommonLabReportDetails").click(function(e){    	
	    var tablename = 'field-CommonLabReportDetails';
	    var i = 0;
		console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
		var element = document.getElementById(tablename);

		var j = 0;
		for(i = document.getElementById(tablename).childNodes.length - 1; i >= 0 ; i --)
		{
			var tr = document.getElementById(tablename).childNodes[i];
			if(tr.nodeType === Node.ELEMENT_NODE)
			{
				console.log("tr.childNodes.length= " + tr.childNodes.length);
				var checkbox = tr.querySelector('input[type="checkbox"]');				
				if(checkbox.checked == true)
				{
					tr.remove();
				}
				j ++;
			}
			
		}    	
    });


</script>






