<%@page pageEncoding="UTF-8" %>

<%@page import="common_lab_report.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@page import="lab_test.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.COMMON_LAB_REPORT_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_COMMON_LAB_REPORT;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Common_lab_reportDTO common_lab_reportDTO = (Common_lab_reportDTO) request.getAttribute("common_lab_reportDTO");
    CommonDTO commonDTO = common_lab_reportDTO;
    String servletName = "Common_lab_reportServlet";


    System.out.println("common_lab_reportDTO = " + common_lab_reportDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Common_lab_reportDAO common_lab_reportDAO = (Common_lab_reportDAO) request.getAttribute("common_lab_reportDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    Lab_testDAO lab_testDAO = new Lab_testDAO();
    Lab_testDTO lab_testDTO = lab_testDAO.getDTOByID(common_lab_reportDTO.labTestId);


%>


<td id='<%=i%>_prescriptionLabTestId'>
    <%
        value = common_lab_reportDTO.prescriptionLabTestId + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_labTestId'>
    <%
        value = lab_testDTO.nameEn + " Report";
    %>

    <%=value%>


</td>


<td id='<%=i%>_name'>
    <%
        value = common_lab_reportDTO.name + "";
    %>

    <%=value%>


</td>


<td id='<%=i%>_age'>
    <%
        value = common_lab_reportDTO.age + "";
    %>

    <%=value%>


</td>


<td id='<%=i%>_sex'>
    <%
        value = common_lab_reportDTO.sex + "";
    %>

    <%=value%>


</td>


<td id='<%=i%>_referredBy'>
    <%
        value = common_lab_reportDTO.referredBy + "";
    %>

    <%=value%>


</td>


<td id='<%=i%>_insertionDate'>
    <%
        value = common_lab_reportDTO.insertionDate + "";
    %>
    <%
        String formatted_insertionDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>

    <%=Utils.getDigits(formatted_insertionDate, Language)%>


</td>


<td>
    <button type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Common_lab_reportServlet?actionType=view&ID=<%=common_lab_reportDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>
	
																						
											
											
																				
											

