<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    String servletType = request.getParameter("servletType");
    System.out.println(" servletType = " + servletType);
    String pageTitle = (String) request.getAttribute("pageTitle");
    String isSuccess = request.getParameter("success");
    System.out.println("isSuccess " + isSuccess);

    //LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    LoginDTO localLoginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
    request.setAttribute("actionType", "getAddPage");
%>

<input type='hidden' id='servletType' value='<%=servletType%>'/>
<div class="row" style="margin: 0px !important;">
    <div class="col-md-12" style="padding: 5px !important;">
        <div id="ajax-content">

            <div class="row portlet-title">
                <div class="caption" style="font-weight: bold; font-size: 18px">
                    <%=(LM.getText(LC.ADD_OFFICE_MAIN_LABEL, localLoginDTO))%>
                </div>
            </div>
            <br>

            <div class="row form-group form-horizontal">
                <%=(LM.getText(LC.ADD_OFFICE_MINISTRY_LABEL, localLoginDTO))%>
            </div>
            <div class="row form-group form-horizontal">
                <select class="form-control" id="select_ministry" style="width: 350px"
                        onchange="onOfficeMinistryClicked()">
                </select>
            </div>

            <div class="row">
                <div class="column">
                    <div class="form-group form-horizontal">
                        <div class="column">
                            <div class="portlet-window-body">
                                <div id="tree_view"
                                     class="jstree jstree-2 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
                                     role="tree" style="background: white" state="0">
                                    <ul class="jstree-container-ul jstree-children" style="background: white"></ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column" id="add_office" style="display: none">
                    <div class="portlet-window-body">
                        <div class="row">
                            <%@include file="dropDownNode.jsp" %>
                            <hr>
                        </div>

                        <div class="content">
                            <jsp:include page="../offices/officesEditBody.jsp">
                                <jsp:param name="actionType" value="getAddPage"/>
                            </jsp:include>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var global_selected_office_origin_id;


    function onOfficeMinistryClicked() {
        autoExpand = true;
        $("#tree_view").html('').attr("state", 0);

        getChildren(0, 0, "tree_view", "getOfficeOriginByMinistryId", null, $('#select_ministry').val(), "", "");
    }

    // function getParentOffices(ministryId) {
    //     const xhttp = new XMLHttpRequest();
    //     xhttp.onreadystatechange = function () {
    //         if (this.readyState == 4 && this.status == 200) {
    //             if (this.responseText != '') {
    //
    //                 const result = JSON.parse(this.responseText);
    //                 document.getElementById("parentOfficeId").innerHTML = '';
    //                 for (var i = 0; i < result.length; i++) {
    //                     const data = result[i];
    //
    //                     const option = document.createElement("option");
    //                     option.value = data.id;
    //                     option.text = data.office_name_bng;
    //
    //                     if (i == 0) {
    //                         const my_language = document.getElementById('my_language').value;
    //                         op = new Option(my_language == '2' ? "-- Please select one --" : "-- বাছাই করুন --", '0');
    //                         document.getElementById("parentOfficeId").add(op);
    //                     }
    //
    //                     document.getElementById("parentOfficeId").add(option);
    //                 }
    //             }
    //             else {
    //                 console.log("nul response");
    //             }
    //         }
    //         else if (this.readyState == 4 && this.status != 200) {
    //             alert('failed ' + this.status);
    //         }
    //     };
    //
    //     const requestmsg = "OfficesServlet" + "?actionType=getParentOffice&ministryId=" + ministryId + "&officeOriginId=" + global_selected_office_origin_id;
    //     xhttp.open("GET", requestmsg, true);
    //     xhttp.send();
    // }

    function getMinistryData() {
        $('#select_ministry').empty().append($('<option>', {
            value: 0,
            text: $('#my_language').val() == 1 ? '-- বাছাই করুন --' : '-- Please Select One --'
        }));

        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (this.responseText !== '') {

                    const result = JSON.parse(this.responseText);
                    for (let i = 0; i < result.length; i++) {
                        const data = result[i];
                        $('#select_ministry').append($('<option>', {
                            value: data.id,
                            text: $('#my_language').val() == 1 ? data.name_bng : data.name_eng
                        }));
                    }
                }
            }
        };

        const request = "OfficesServlet" + "?actionType=getMinistry";
        xhttp.open("GET", request, true);
        xhttp.send();
    }


    $(document).ready(function () {
        getMinistryData();

        const url_string = window.location.toString();
        const url = new URL(url_string);
        const c = url.searchParams.get("success");
        if (c !== undefined && c !== null && c == 'true') {
            showToast(success_message_bng, success_message_eng);
            let go_to = url_string.split("&success")[0];
            history.pushState({}, null, go_to);
        }
    });

</script>