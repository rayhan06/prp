<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%

	String servletType = request.getParameter("servletType");
	String pageTitle = request.getParameter("pageTitle");
	String Options;
	LoginDTO loginDTO2 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String Language2 = LM.getText(LC.CENTRE_EDIT_LANGUAGE, loginDTO2);
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	Date date = new Date();
	String datestr = dateFormat.format(date);
	
			
%>


<input type='hidden' id='servletType' value='<%=servletType%>' />
<div class="row" style="margin: 0px !important;">
	<div class="col-md-12" style="padding: 5px !important;">
		<div id="ajax-content">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-check-square-o"></i><%=pageTitle%>
					</div>

				</div>
				<form  action="Office_unit_organogramsServlet?actionType=MultiAssignApprovalPathToOrganograms" id="bigform" name="bigform"  method="POST">
					<div class="portlet-window-body">
						<div class="row">
							<select class='form-control col-md-3'  name='approval_path'>
								<%		
								Options = CommonDAO.getOptions(Language2, "select", "approval_path", "approval_path", "form-control", "approval_path");
								out.print(Options);
								%>
							</select>
							<hr>
						</div>
	
	
						<div class="row">
							
	
							<div class="portlet light col-md-5">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-check-square-o"></i>Select Designations
									</div>
								</div>
								<div class="portlet-window-body">
									<div id="office_unit_tree_panel"
										class="jstree jstree-2 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
										role="tree" state="0">
										<%@include file="treeNode.jsp"%>
									</div>
								</div>
							</div>
						</div>
						<div class="form-actions text-center">
							<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
								CANCEL
							</a>
							<button class="btn btn-success" type="submit">
								SUBMIT
							</button>
						</div>
					</div>
				</form>
			</div>


		</div>
	</div>
</div>



<script type="text/javascript">

function checkbox_toggeled(id, text, cb_id, name, parent_id)
{
	var cb = document.getElementById(cb_id);
	var parent = document.getElementById(parent_id);
	var child_cbs = parent.querySelectorAll('input[type = "checkbox"]');
	var treeCBChecked = document.getElementById("treeCBChecked_" + id);
	var i;
	if(cb.checked)
	{
		console.log('checked');
		treeCBChecked.value = id;
		for (i = 0; i < child_cbs.length; i++) 
		{
			child_cbs[i].checked = true;
			document.getElementById("treeCBChecked_" + child_cbs[i].value).value = child_cbs[i].value;
		}
	}
	else
	{
		console.log('unchecked');
		treeCBChecked.value = -1;
		for (i = 0; i < child_cbs.length; i++) 
		{
			child_cbs[i].checked = false;
			document.getElementById("treeCBChecked_" + child_cbs[i].value).value = -1;
		}
	}
}

function fillNextElementWithID(id, element)
{
	console.log("\n\nback in js, got element = " + element.getAttribute('id'));
	
}
</script>
