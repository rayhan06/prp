<%@ page import="language.LM" %>
<%@ page import="language.LC" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="login.LoginDTO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    String servletType = request.getParameter("servletType");
    System.out.println(" servletType = " + servletType);
    String pageTitle = (String) request.getAttribute("pageTitle");

    LoginDTO localLoginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);

    String context = "../../.." + request.getContextPath() + "/";
%>

<input type='hidden' id='servletType' value='<%=servletType%>'/>
<div class="row" style="margin: 0 !important;">
    <div class="col-md-12" style="padding: 5px !important;">
        <div id="ajax-content">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption" style="font-weight: bold">
                        <%=(LM.getText(LC.OFFICE_HEAD_LABEL, localLoginDTO))%>
                    </div>
                </div>
                <div class="portlet-window-body">
                    <div class="row">
                        <%@include file="dropDownNode.jsp" %>
                        <hr>
                    </div>
                    <div id="search_table" style="height: 100%" class="row">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="snackbar">Updated as Head</div>


<script type="text/javascript">

    // region Variable

    var employee_office_id = 0;

    var office = 0;
    var ministry_id = 0;
    var office_layer = 0;
    var office_origin = 0;
    var pageNumber = 10;

    var officeId = 0;
    var officeUnitId = 0;
    var user_info = {};

    // endregion Variable

    // region Update Office Head

    function submitHeadOfOffice() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText != '') {
                    showToast(success_message_bng, success_message_eng);
                }
                else {
                    document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
                }
            }
            else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Post", "OfficeHeadServlet?actionType=submit" +
            '&id=' + employee_office_id +
            '&officeId=' + officeId +
            '&officeUnitId=' + officeUnitId, true);
        xhttp.send();
    }

    function toggleCheckbox(id, _office, _officeUnitId) {
        employee_office_id = id;
        officeId = _office;
        officeUnitId = _officeUnitId;
        console.log('employee office id ', employee_office_id, ' ', 'office id ', officeId, ' ', 'office_unit_id ', officeUnitId);

        bootbox.confirm({
            message: getLanguageBaseText("আপনি কি, এই কর্মকর্তা/কর্মচারী কে অফিস প্রধান হিসেবে যুক্ত করতে চান", "Do you want to add this person as Office head"),
            buttons: {
                confirm: {
                    label: getLanguageBaseText('হ্যাঁ', 'Yes'),
                    className: 'btn-success'
                },
                cancel: {
                    label: getLanguageBaseText('না', 'No'),
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result == true) {
                    submitHeadOfOffice();
                }
            }
        });
    }

    // endregion pdate Office Head

    // Where tree node is end
    function fillNextElementWithID(id, element) {

        ministry_id = document.getElementById("select_office_ministries").value;
        office_layer = document.getElementById("select_office_layers").value;
        office_origin = document.getElementById("select_office_origins").value;
        office = document.getElementById("select_offices").value;

        console.log('ministry_id: ', ministry_id, ' office_layer: ', office_layer, ' office_origin: ', office_origin, ' office: ', office);
        console.log('________message______________ ', element.getId);
        if (element.getAttribute('id') === "node_div_4") {

            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    if (this.responseText !== '') {
                        document.getElementById("search_table").innerHTML = '';
                        document.getElementById("search_table").innerHTML = this.responseText;

                        $('#tableData').DataTable({
                            dom: 'B',
                            "ordering": false,
                            buttons: [
                                {
                                    extend: 'excel',
                                    charset: 'UTF-8'
                                }, {
                                    extend: 'print',
                                    charset: 'UTF-8'
                                }
                            ]
                        });
                        document.getElementById('tableData_wrapper').classList.add("pull-right");
                        document.getElementById('tableData').classList.remove("no-footer");
                    }
                    else {
                    }
                }
                else if (this.readyState === 4 && this.status !== 200) {
                    alert('failed ' + this.status);
                }
            };

            xhttp.open("Get", "OfficeHeadServlet?actionType=search" + '&ministry_id=' + ministry_id + '&office_layer=' + office_layer + '&office_origin=' + office_origin + '&office=' + office, true);
            xhttp.send();
        }
    }

    function init() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (this.responseText !== '') {
                    document.getElementById("search_table").innerHTML = '';
                    document.getElementById("search_table").innerHTML = this.responseText;
                }
                else {
                    document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
                }
            }
            else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "OfficeHeadServlet?actionType=search", true);
        xhttp.send();

        var xhttp0 = new XMLHttpRequest();
        xhttp0.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (this.responseText !== '') {
                    user_info = JSON.parse(this.responseText);
                }
                else {
                    document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
                }
            }
            else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp0.open("Get", "GeneralPurposeServlet?actionType=getUserInfo", true);
        xhttp0.send();
    }

    window.onload = function () {
        //init();
    };

    // region Next Data

    function next(id) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (this.responseText !== '') {
                    document.getElementById("search_table").innerHTML = '';
                    document.getElementById("search_table").innerHTML = this.responseText;
                }
                else {
                    document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
                }
            }
            else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "OfficeHeadServlet?actionType=search", true);
        xhttp.send();
    }

    // endregion Next Data

    // Not working, so skip
    function pageNumberChange(id) {
        var perPageElement = document.getElementById("pagenumber").value;

        ministry_id = document.getElementById("select_office_ministries").value;
        office_layer = document.getElementById("select_office_layers").value;
        office_origin = document.getElementById("select_office_origins").value;
        office = document.getElementById("select_offices").value;
        pageNumber = document.getElementById("pagenumber").value;

        console.log('ministry_id: ', ministry_id, ' office_layer: ', office_layer, ' office_origin: ', office_origin, ' office: ', office);

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText != '') {
                    document.getElementById("search_table").innerHTML = '';
                    document.getElementById("search_table").innerHTML = this.responseText;
                }
                else {
                    document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
                }
            }
            else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "OfficeAdminServlet?actionType=search" +
            '&ministry_id=' + ministry_id +
            '&office_layer=' + office_layer +
            '&office_origin=' + office_origin +
            '&office=' + office +
            '&page_number=' + pageNumber +
            '&id=' + id +
            '&RECORDS_PER_PAGE=' + perPageElement, true);
        xhttp.send();
    }

    // region Searching

    function dosubmit(params, id) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('search_table').innerHTML = this.responseText;
            }
            else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "OfficeHeadServlet?actionType=search" +
            '&ministry_id=' + ministry_id +
            '&office_layer=' + office_layer +
            '&office_origin=' + office_origin +
            '&office=' + office +
            '&page_number=' + pageNumber +
            '&' + params, true);

        xhttp.send();
    }

    function anyfield_changed() {
        ministry_id = document.getElementById("select_office_ministries").value;
        office_layer = document.getElementById("select_office_layers").value;
        office_origin = document.getElementById("select_office_origins").value;
        office = document.getElementById("select_offices").value;
        pageNumber = document.getElementById("pagenumber").value;

        var searchKey = document.getElementById('anyfield').value;
        if (searchKey === null || searchKey.length === 0) searchKey = "";

        var params = 'AnyField=' + document.getElementById('anyfield').value + '&search=true&ajax=true&RECORDS_PER_PAGE=' + document.getElementById('pagenumber').value;
        dosubmit(params, null);
    }

    function allfield_changed() {
        let params = 'AnyField=' + document.getElementById('anyfield').value;
        params += ('&name_eng=' + document.getElementById('name_eng').value);
        params += ('&name_bng=' + document.getElementById('name_bng').value);
        params += ('&personal_mobile=' + document.getElementById('personal_mobile').value);
        params += ('&personal_email=' + document.getElementById('personal_email').value);

        params += '&search=true&ajax=true&RECORDS_PER_PAGE=' + document.getElementById('pagenumber').value;
        dosubmit(params);
    }

    // endregion Searching

</script>

<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>

<style>
    #snackbar {
        visibility: hidden; /* Hidden by default. Visible on click */
        min-width: 500px; /* Set a default minimum width */
        margin-left: -125px; /* Divide value of min-width by 2 */
        background-color: #10ca08; /* Black background color */
        color: #fff; /* White text color */
        text-align: center; /* Centered text */
        border-radius: 2px; /* Rounded borders */
        padding: 16px; /* Padding */
        position: fixed; /* Sit on top of the screen */
        z-index: 1; /* Add a z-index if needed */
        left: 50%; /* Center the snackbar */
        bottom: 30px; /* 30px from the bottom */
    }

    /* Show the snackbar when clicking on a button (class added with JavaScript) */
    #snackbar.show {
        visibility: visible; /* Show the snackbar */
        /* Add animation: Take 0.5 seconds to fade in and out the snackbar.
        However, delay the fade out process for 2.5 seconds */
        -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }

    /* Animations to fade the snackbar in and out */
    @-webkit-keyframes fadein {
        from {
            bottom: 0;
            opacity: 0;
        }
        to {
            bottom: 30px;
            opacity: 1;
        }
    }

    @keyframes fadein {
        from {
            bottom: 0;
            opacity: 0;
        }
        to {
            bottom: 30px;
            opacity: 1;
        }
    }

    @-webkit-keyframes fadeout {
        from {
            bottom: 30px;
            opacity: 1;
        }
        to {
            bottom: 0;
            opacity: 0;
        }
    }

    @keyframes fadeout {
        from {
            bottom: 30px;
            opacity: 1;
        }
        to {
            bottom: 0;
            opacity: 0;
        }
    }
</style>
