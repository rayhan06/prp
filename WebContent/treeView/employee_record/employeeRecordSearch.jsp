<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="util.RecordNavigator" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    String servletType = request.getParameter("servletType");
    LoginDTO localLoginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
    String navigator = SessionConstants.NAV_EMPLOYEE_RECORDS;
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    int pagination_number = 0;
%>

<input type='hidden' id='servletType' value='<%=servletType%>'/>
<div id="ajax-content">
    <div class="kt-portlet kt-portlet--responsive-tablet-and-mobile py-3">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><%=(LM.getText(LC.SEARCH_EMPLOYEE_RECORD_TITLE, localLoginDTO))%>
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <a class="btn btn-primary btn-hover-brand btn-square"
                   href="Employee_recordsServlet?actionType=getAddPage">
                    <i class="fa fa-plus"></i><%=LM.getText(LC.GEO_UNIONS_ADD_GEO_UNIONS_ADD_BUTTON, localLoginDTO)%>
                </a>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="kt-form" style="display: none">
                <%@include file="../dropDownNode.jsp" %>
            </div>
            <div class="kt-form">
                <div id="otherSearchField">
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label for="user_name"><%=LM.getText(LC.USER_SEARCH_USER_NAME, loginDTO)%>
                            </label>
                            <input type="text" class="form-control" id="user_name" placeholder=""
                                   name="user_name">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label for="personal_mobile"><%=LM.getText(LC.EMPLOYEE_RECORDS_SEARCH_PERSONALMOBILE, loginDTO)%>
                            </label>
                            <input type="text" class="form-control" id="personal_mobile" placeholder=""
                                   name="personal_mobile">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label for="name_eng"><%=LM.getText(LC.EMPLOYEE_RECORDS_SEARCH_NAMEENG, loginDTO)%>
                            </label>
                            <input type="text" class="form-control" id="name_eng" placeholder="" name="name_eng">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label for="name_bng"><%=LM.getText(LC.EMPLOYEE_RECORDS_SEARCH_NAMEBNG, loginDTO)%>
                            </label>
                            <input type="text" class="form-control" id="name_bng" placeholder="" name="name_bng">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label for="nid"><%=LM.getText(LC.EMPLOYEE_RECORDS_SEARCH_NID, loginDTO)%>
                            </label>
                            <input type="text" class="form-control" id="nid" placeholder="" name="nid">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label for="personal_email"><%=LM.getText(LC.EMPLOYEE_RECORDS_SEARCH_PERSONALEMAIL, loginDTO)%>
                            </label>
                            <input type="text" class="form-control" id="personal_email" placeholder=""
                                   name="personal_email">
                        </div>
                    </div>

                    <div class="kt-form__actions pt-3">
                        <button class="btn btn-primary btn-hover-brand btn-square" id="searchFormSubmit"><%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>

            <%@include file="/common/pagination_with_go2.jsp"%>

            <template id = "loader">
                <div class="modal-body">
                    <img alt="" class="loading" src="<%=request.getContextPath()%>/assets/images/loading-spinner-grey.gif">
                    <span>Loading...</span>
                </div>
            </template>

            <div id="search_table" class="py-5"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function fixedToEditable(i, deletedStyle, id) {
        var host = 'Employee_recordsServlet?actionType=getEditPage&ID=' + id;
        window.location.replace(host);
    }

    var currentPageNo = <%=rn.getCurrentPageNo()%>;
    var pageNo = <%=rn.getCurrentPageNo()%>;

    function allfield_changed(){}

    $("#searchFormSubmit").click( function () {

        pageNo = 1;
        searchClick();
    });

    $(document).on( "click", "a.page-link[title='Previous']", function(){

        pageNo = currentPageNo-1;
        searchClick();
    } );

    $(document).on( "click", "a.page-link[title='Next']", function(){

        pageNo = currentPageNo+1;
        searchClick();
    } );

    $(document).on( "click", "a.page-link[title='Left']", function(){

        pageNo = 1;
        searchClick();
    } );

    $(document).on( "click", "a.page-link[title='Last']", function(){

        pageNo = <%=rn.getTotalPages()%>;
        searchClick();
    } );

    $(document).on( "click", "input.employee-record-search-submit", function () {

        searchClick();
    })

    function searchClick() {
        try {
            var ministry_id = document.getElementById('select_office_ministries').value;
            var layer_id = document.getElementById('select_office_layers') ? document.getElementById('select_office_layers').value : 0;
            var origin_id = document.getElementById('select_office_origins') ? document.getElementById('select_office_origins').value : 0;
            var office_id = document.getElementById('select_offices') ? document.getElementById('select_offices').value : 0;
            var name_en = document.getElementById('name_eng').value;
            var name_bn = document.getElementById('name_bng').value;
            var nid = document.getElementById('nid').value;
            var email = document.getElementById('personal_email').value;
            var phone0 = document.getElementById('personal_mobile').value;
            var user_name = document.getElementById('user_name').value;

            var recordsPerPage = document.getElementById( 'RECORDS_PER_PAGE' ).value;

            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    if (this.responseText.includes('option')) {
                        document.getElementById('search_table').innerHTML = this.responseText;
                    }
                    else {
                        console.log("got errror response for officer");
                        showToast("কোন তথ্য পাওয়া যায় নি", "No data found");
                    }
                }
                else if (this.readyState === 4 && this.status !== 200) {
                    alert('failed ' + this.status);
                }
            };
            var parm = '';
            parm += '&ministry_id=' + ministry_id;
            parm += '&layer_id=' + layer_id;
            parm += '&origin_id=' + origin_id;
            parm += '&office_id=' + office_id;
            parm += '&name_en=' + name_en;
            parm += '&name_bn=' + name_bn;
            parm += '&nid=' + nid;
            parm += '&email=' + email;
            parm += '&phone0=' + phone0;
            parm += '&user_name=' + user_name;
            parm += '&RECORDS_PER_PAGE=' + recordsPerPage;
            parm += '&pageno=' + pageNo;
            parm += '&search=';
            parm += '&go=';
            parm += '&TotalRecords=<%=rn.getTotalRecords()%>';
            xhttp.open("Get", "Employee_recordsServlet?actionType=search" + parm, true);
            xhttp.send()

            currentPageNo = pageNo;

        } catch (e) {

            showToast("কোন তথ্য পাওয়া যায় নি", "No data found");
        }
    }

    function onDeleteEmployee() {
        const deleteItem = document.getElementsByClassName('checkbox');
        let items = [], ids = ('');
        for (let i = 0; i < deleteItem.length; i++) {
            if (deleteItem[i].checked) {
                items.push(deleteItem[i].value);
                ids += ('&ID=' + parseInt(deleteItem[i].value));
            }
        }
        console.log(items);

        if (items.length > 0) {
            const xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    if (this.responseText.includes('option')) {
                        location.reload();
                    }
                    else {
                        console.log("got error response");
                    }
                }
                else if (this.readyState === 4 && this.status !== 200) {
                    alert('failed ' + this.status);
                }
            };
            xhttp.open("POST", "Employee_recordsServlet?actionType=delete" + ids, true);
            xhttp.send();
        }
    }

    $(document).ready(()=>{
        searchClick();
        const url_string = window.location.toString();
        const url = new URL(url_string);
        const c = url.searchParams.get("success");
        if (c !== undefined && c !== null && c === 'true') {
            showToast(success_message_bng, success_message_eng);
            let go_to = url_string.split("&success")[0];
            history.pushState({}, null, go_to);
        }
        console.log('location: ', url);
    });
</script>