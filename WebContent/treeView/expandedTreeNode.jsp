<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language="java"%>
<%@ page import="java.util.*"%>
<%@ page import="pb.*"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="treeView.*"%>
<%@page import="role.*"%>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.CENTRE_EDIT_LANGUAGE, loginDTO);
String actionType = request.getParameter("actionType");
String servletType2 = request.getParameter("servletType");

TreeDTO treeDTO = (TreeDTO)request.getAttribute("treeDTO");
int myLayer = (int)request.getAttribute("myLayer");
ArrayList<Integer> nodeIDs = (ArrayList<Integer>) request.getAttribute("nodeIDs");
int parentID = Integer.parseInt(request.getParameter("parentID"));

int maxLayer = treeDTO.parentChildMap.length;
String geoName = treeDTO.parentChildMap[myLayer];
String englishNameColumn = treeDTO.englishNameColumn[myLayer];
String banglaNameColumn = treeDTO.banglaNameColumn[myLayer];
String treeName = treeDTO.treeName;
int checkBoxType = treeDTO.checkBoxType;
int plusButtonType =  treeDTO.plusButtonType;
String showName = treeDTO.showNameMap[myLayer];


String nameText = "";
String Options = "";

ArrayList<RoleDTO> RoleDTOs = RoleDAO.getDTOs();
%>

<%
for(int i = 0; i < nodeIDs.size(); i ++)
{
	int id = nodeIDs.get(i);
	String text =  GenericTree.getName(geoName, id, Language, englishNameColumn, banglaNameColumn);	
	if(myLayer == 0)
	{
		text = "<strong>" + text + "</strong>";
	}
	
%>
	<tr>
			
			<%
	if(myLayer == maxLayer - 1)
	{
%>
		<td  width="30%" class="text-left unitName"><%=text%></td>
		<td  width="70%" class="text-left unitName">
			<input type='hidden' name = 'designation_id' value='<%=id%>'></input>
			<table class="table table-bordered table-hover">
				<tr>
				<%
					ArrayList<Integer> RoleIDS = RoleDAO.getOrganogramRoles(id);
					for(int j = 0; j < RoleDTOs.size(); j ++)
					{
						RoleDTO roleDTO = RoleDTOs.get(j);
						boolean check = RoleIDS.contains((int)roleDTO.ID);
					
				%>
					<td>
						<label class="control-label">
							<%=roleDTO.roleName%>
						</label>
						<input type='checkbox' name='role_cb' value='<%=id%>_<%=roleDTO.ID%>' <%=check?"checked":""%>/>
					</td>
					
				<%
					}
				%>
				</tr>
			</table>
		</td>
<% 
	}
	else
	{
%>
		<td  width="100%" class="text-left unitName"><%=text%></td>
<%
	}
%>
	</tr>
	<%
	if(myLayer < maxLayer - 1)
	{
	%>
	<tr>
	<td>
		<table class="table table-bordered table-hover"
									id="tblOfficeUnitOrganograms_<%=id%>">
									<tbody name="tbodytoload" id="tbodylOfficeUnitOrganograms_<%=id%>" data = "<%=id%>">

									</tbody>
		</table>
		</td>
	</tr>
	<%
	}	
	%>
<%
}
%>


		