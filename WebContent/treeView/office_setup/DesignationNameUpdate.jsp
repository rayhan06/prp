<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    String servletType = request.getParameter("servletType");
    String pageTitle = request.getParameter("pageTitle");
    LoginDTO loginDTO2 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO3 = UserRepository.getInstance().getUserDtoByUserId(loginDTO2.userID);
%>
<input type='hidden' id='servletType' value='<%=servletType%>'/>
<div class="kt-portlet">
    <div id="ajax-content">
        <div class="kt-portlet kt-portlet--responsive-tablet-and-mobile py-3">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M6,9 L6,15 C6,16.6568542 7.34314575,18 9,18 L15,18 L15,18.8181818 C15,20.2324881 14.2324881,21 12.8181818,21 L5.18181818,21 C3.76751186,21 3,20.2324881 3,18.8181818 L3,11.1818182 C3,9.76751186 3.76751186,9 5.18181818,9 L6,9 Z M17,16 L17,10 C17,8.34314575 15.6568542,7 14,7 L8,7 L8,6.18181818 C8,4.76751186 8.76751186,4 10.1818182,4 L17.8181818,4 C19.2324881,4 20,4.76751186 20,6.18181818 L20,13.8181818 C20,15.2324881 19.2324881,16 17.8181818,16 L17,16 Z"
                                      fill="#000000" fill-rule="nonzero"/>
                                <path d="M9.27272727,9 L13.7272727,9 C14.5522847,9 15,9.44771525 15,10.2727273 L15,14.7272727 C15,15.5522847 14.5522847,16 13.7272727,16 L9.27272727,16 C8.44771525,16 8,15.5522847 8,14.7272727 L8,10.2727273 C8,9.44771525 8.44771525,9 9.27272727,9 Z"
                                      fill="#000000" opacity="0.3"/>
                            </g>
                        </svg>
                        <%=(LM.getText(LC.DESIGNATION_NAME_UPDATE_TITLE, loginDTO2))%>
                    </h3>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div div="row">
                <%@include file="../dropDownNode.jsp" %>
            </div>
            <div class="clearfix ml-4">
                <div class="form-group row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                        <label><%=(LM.getText(LC.DESIGNATION_NAME_UPDATE_NAME_BANGLA_OR_ENGLISH, loginDTO2))%>
                        </label>
                        <input class="form-control" id="designation_bn_en">
                        <span style="min-width: 50px"></span>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                        <label><%=(LM.getText(LC.DESIGNATION_NAME_UPDATE_USER_NAME, loginDTO2))%>
                        </label>
                        <input class="form-control" id="user_name">
                        <span style="min-width: 50px"></span>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-2">
                        <label></label>
                        <input type="button" value="Submit"
                               class="btn btn-primary btn-hover-brand btn-square d-flex align-item-center mt-2"
                               onclick="onSearch()">
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead class="thead-light text-center">
                    <tr id="table_head">
                        <th><%=(LM.getText(LC.DESIGNATION_NAME_UPDATE_NAME, loginDTO2))%>
                        </th>
                        <th><%=(LM.getText(LC.DESIGNATION_NAME_UPDATE_DESIGNATION_BANGLA, loginDTO2))%>
                        </th>
                        <th><%=(LM.getText(LC.DESIGNATION_NAME_UPDATE_DESIGNATION_ENGLISH, loginDTO2))%>
                        </th>
                        <th><%=(LM.getText(LC.DESIGNATION_NAME_UPDATE_UNIT, loginDTO2))%>
                        </th>
                        <th><%=(LM.getText(LC.DESIGNATION_NAME_UPDATE_OFFICE, loginDTO2))%>
                        </th>
                        <th><%=(LM.getText(LC.PROTIKOLPO_SETUP_ACTIVITY, loginDTO2))%>
                        </th>
                    </tr>
                    </thead>
                    <tbody id="designation_table"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="edit_body" style="display: none">
    <div class="form-group">
        <input type="hidden" id="id">
        <label><%=(LM.getText(LC.DESIGNATION_NAME_UPDATE_DESIGNATION_BANGLA, loginDTO2))%>
        </label>
        <input class="form-control" id="designation_bng_edit" onchange="onBanglaEdit(this.value)">
    </div>
    <div class="form-group">
        <label><%=(LM.getText(LC.DESIGNATION_NAME_UPDATE_DESIGNATION_ENGLISH, loginDTO2))%>
        </label>
        <input class="form-control" id="designation_eng_edit" onchange="onEnglishEdit(this.value)"/>
    </div>
    <%--<button class="btn btn-primary btn-hover-brand btn-square" onclick="onEditDone(event)">--%>
    <%--Submit--%>
    <%--</button>--%>
    <div>
        <button class="btn btn-primary" onclick="onSubmitData(event)">
            <%=LM.getText(LC.OFFICES_EDIT_OFFICES_SUBMIT_BUTTON, loginDTO)%>
        </button>
        <button class="btn btn-danger" data-dismiss="modal">
            <%=LM.getText(LC.OFFICES_EDIT_OFFICES_CANCEL_BUTTON, loginDTO)%>
        </button>
    </div>
</div>

<script type="text/javascript">
    var edit_designation_name_bn;
    var edit_designation_name_en;

    function onBanglaEdit(valueBn) {
        document.getElementById('designation_bng_edit').setAttribute("value", valueBn);
    }

    function onEnglishEdit(valueEn) {
        document.getElementById('designation_eng_edit').setAttribute("value", valueEn);
    }

    function onSearch() {
        const office_id = document.getElementById('select_offices').value;
        const office_unit_id = document.getElementById('select_office_units').value;
        const designation_bn_en = document.getElementById('designation_bn_en').value;
        const user_name = document.getElementById('user_name').value;
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById('designation_table').innerHTML = '';
                JSON.parse(this.responseText).forEach(function (val) {
                    const edit_update = (val.unit_name_eng === null || val.unit_name_eng.length === 0) ? "<%=(LM.getText(LC.GLOBAL_ADD_NEW, loginDTO))%>" : "<%=(LM.getText(LC.STATUS_SEARCH_STATUS_EDIT_BUTTON, loginDTO))%>";
                    const html = "<tr>\n" +
                        "<td>" + val.name_bng + " (" + val.username + ")" + "</td>\n" +
                        "<td>" + val.designation_bng + "</td>\n" +
                        "<td>" + val.designation_eng + "</td>\n" +
                        "<td>" + val.unit_name_bng + "</td>\n" +
                        "<td>" + val.office_name_bng + "</td>\n" +
                        "<td>\n" +
                        "<button class=\"btn btn-primary btn-hover-brand btn-square\" onclick=\"onChangeDesignation(" + val.id + "," + "'" + val.designation_bng + "'" + "," + "'" + val.designation_eng + "'" + ")\">" + edit_update + "</button>\n" +
                        "</td>\n" +
                        "</tr>";
                    document.getElementById('designation_table').innerHTML += html;
                });
            }
        };
        xhttp.open("Get", "DesignationNameUpdateServlet?actionType=getDesignation&office_id=" + office_id + "&office_unit_id=" + office_unit_id
            + "&designation_bn_en=" + designation_bn_en + "&user_name=" + user_name, true);
        xhttp.send();
    }

    function onChangeDesignation(id, bn, en) {
        document.getElementById('id').value = id;
        document.getElementById('designation_bng_edit').setAttribute('value', bn);
        document.getElementById('designation_eng_edit').setAttribute('value', en);
        document.getElementById('designation_bng_edit').onchange();
        edit_designation_name_bn = document.getElementById('designation_bng_edit').value;
        edit_designation_name_en = document.getElementById('designation_eng_edit').value;
        bootbox.dialog({
            message: document.getElementById('edit_body').innerHTML
        });
    }

    function onSubmitData(e) {
        try {
            const id = document.getElementById("id").value;
            const bn = document.getElementById("designation_bng_edit").value;
            const en = document.getElementById("designation_eng_edit").value;

            if (isNullOrNegativeValue([id, bn, en])) throw error();

            const xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    onSearch();
                    showToast(success_message_bng, success_message_eng);
                    bootbox.hideAll();
                }
            };
            xhttp.open("Post", "DesignationNameUpdateServlet?actionType=updateDesignation&id=" + id + "&name_bn=" + bn
                + "&name_en=" + en, true);
            xhttp.send();
        } catch (e) {
            showError(fill_all_input_bng, fill_all_input_eng);
        }
    }

    function fillNextElementWithID(id, element) {
    }

    window.onload = function () {
    };
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>