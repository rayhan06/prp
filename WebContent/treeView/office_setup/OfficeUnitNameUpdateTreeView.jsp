<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<%
    String servletType = request.getParameter("servletType");
    String pageTitle = request.getParameter("pageTitle");
%>
<input type='hidden' id='servletType' value='<%=servletType%>' />
<div class="kt-portlet">
	<div id="ajax-content">
		<div class="kt-portlet kt-portlet--responsive-tablet-and-mobile py-3">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<svg xmlns="http://www.w3.org/2000/svg"
							xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
							height="24px" viewBox="0 0 24 24" version="1.1"
							class="kt-svg-icon">
                            <g stroke="none" stroke-width="1"
								fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24" />
                                <path
								d="M6,9 L6,15 C6,16.6568542 7.34314575,18 9,18 L15,18 L15,18.8181818 C15,20.2324881 14.2324881,21 12.8181818,21 L5.18181818,21 C3.76751186,21 3,20.2324881 3,18.8181818 L3,11.1818182 C3,9.76751186 3.76751186,9 5.18181818,9 L6,9 Z M17,16 L17,10 C17,8.34314575 15.6568542,7 14,7 L8,7 L8,6.18181818 C8,4.76751186 8.76751186,4 10.1818182,4 L17.8181818,4 C19.2324881,4 20,4.76751186 20,6.18181818 L20,13.8181818 C20,15.2324881 19.2324881,16 17.8181818,16 L17,16 Z"
								fill="#000000" fill-rule="nonzero" />
                                <path
								d="M9.27272727,9 L13.7272727,9 C14.5522847,9 15,9.44771525 15,10.2727273 L15,14.7272727 C15,15.5522847 14.5522847,16 13.7272727,16 L9.27272727,16 C8.44771525,16 8,15.5522847 8,14.7272727 L8,10.2727273 C8,9.44771525 8.44771525,9 9.27272727,9 Z"
								fill="#000000" opacity="0.3" />
                            </g>
                        </svg>
						<%-- <%=(LM.getText(LC.UNIT_NAME_UPDATE_TITLE, loginDTO))%> --%>
					</h3>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body">
			<div class="row">
				<%@include file="../dropDownNode.jsp"%>
			</div>
			<div class="kt-portlet__body">
			    <div class="row">
			    <div class="form-group col-md-3"></div>
			    <div class="form-group col-md-6">
					<select class="form-control" id="search_office_select" name="<%= LM.getText(LC.CENTRE_EDIT_LANGUAGE, loginDTO)%>" required="required" onchange="searchOffice('search_office_select')">

					</select>

				</div>
			    </div>
				<div id="office_unit_tree_panel"
					class="jstree jstree-1 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
					role="tree" state="0">
					<ul class="jstree-container-ul jstree-children"></ul>
				</div>
			</div>
		</div>
	</div>
</div>

<form id="edit_body" class="kt-form" style="display: none">
	<div class="form-group">
		<input type="hidden" id="id"> <label><%=(LM.getText(LC.UNIT_NAME_UPDATE_UNIT_NAME_BANGLA, loginDTO))%>
		</label> <input class='form-control' id="unit_bng_edit"
			onchange="onBanglaEdit(this.value)" required>
	</div>
	<div class="form-group">
		<label><%=(LM.getText(LC.UNIT_NAME_UPDATE_UNIT_NAME_ENGLISH, loginDTO))%>
		</label><input class='form-control' id="unit_eng_edit"
			onchange="onEnglishEdit(this.value)" required>
	</div>
	<div>
		<button class="btn btn-primary" onclick="onSubmitData(event)">
			<%=LM.getText(LC.OFFICES_EDIT_OFFICES_SUBMIT_BUTTON, loginDTO)%>
		</button>
		<button class="btn btn-danger" data-dismiss="modal">
			<%=LM.getText(LC.OFFICES_EDIT_OFFICES_CANCEL_BUTTON, loginDTO)%>
		</button>
	</div>
</form>

<script type="text/javascript">
    let edit_unit_name_bn;
    let edit_unit_name_en;
    var previousSearchedElementID;
    function onBanglaEdit(valueBn) {
        document.getElementById('unit_bng_edit').setAttribute("value", valueBn);
    }

    function onEnglishEdit(valueEn) {
        document.getElementById('unit_eng_edit').setAttribute("value", valueEn);
    }

    // ok
    function onChangeUnitAddOrEdit(id, bn, en) {
        bootbox.hideAll();

        document.getElementById('id').value = id;
        document.getElementById('unit_bng_edit').setAttribute('value', bn);
        document.getElementById('unit_eng_edit').setAttribute('value', en);
        edit_unit_name_bn = document.getElementById('unit_bng_edit').value;
        edit_unit_name_en = document.getElementById('unit_eng_edit').value;
        bootbox.dialog({
            message: document.getElementById('edit_body').innerHTML
        });
    }

    // ok
    function onSubmitData(e) {
        try {
            const id = document.getElementById("id").value;
            const bn = document.getElementById("unit_bng_edit").value;
            const en = document.getElementById("unit_eng_edit").value;

            if (isNullOrNegativeValue([id, bn, en])) throw error();

            const xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    document.getElementById('tree_node_' + id).innerHTML = global_my_language == '1' ? bn : en;
                    showToast(success_message_bng, success_message_eng);
                    bootbox.hideAll();
                }
            };
            xhttp.open("Post", "UnitNameUpdateServlet?actionType=updateUnitName&id=" + id + "&name_bn=" + bn
                + "&name_en=" + en, true);
            xhttp.send();
        } catch (e) {
            showError(fill_all_input_bng, fill_all_input_eng);
        }
    }

    // ok
    function editNode(table_name, id) {
        if (table_name === 'office_units') {
            const xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200 && this.responseText !== '') {
                    let value;
                    JSON.parse(this.responseText).forEach(function (val) {
                        value = val;
                    });
                    onChangeUnitAddOrEdit(value.id, value.unit_name_bng, value.unit_name_eng);
                }
            };
            xhttp.open("Get", "UnitNameUpdateServlet?actionType=getUnitModel&id=" + id, true);
            xhttp.send();
        }
    }

    function getAllOfficeUnits(office_id) {
            const xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                	var select = document.getElementById('search_office_select');
                	select.innerHTML = "";
                	console.log(select.name);
                    JSON.parse(this.responseText).forEach(function (val) {
                        var html = "";
                        if(select.name == "Bangla"){
                            html = "<option value=" + val.id +">"+ val.unit_name_bng +"</option>";
                        }else {
                            html = "<option value=" + val.id +">"+ val.unit_name_eng +"</option>";
                        }
                        document.getElementById('search_office_select').innerHTML += html;
                    })
                }
            };
            xhttp.open("Get", "UnitNameUpdateServlet?actionType=getUnits&officeId=" + office_id, true);
            xhttp.send();
        
    }
    
    function searchOffice(element_id, language) {
		var select = document.getElementById(element_id);
		if(select != null){
			var selectedOfficeUnit = select.options[select.selectedIndex].value;
			console.log(selectedOfficeUnit);
			if(previousSearchedElementID != null){
				document.getElementById(previousSearchedElementID).style.backgroundColor = "#FFFFFF";
			}
			previousSearchedElementID = "tree_node_" + selectedOfficeUnit;
			var node = document.getElementById(previousSearchedElementID);
			if(node != null){
				node.style.backgroundColor = "#FDFF47";
				var rect = node.getBoundingClientRect();
				window.scrollTo({
					  top: (rect.top-200),
					  left: 0,
					  behavior: 'smooth'
					});
			}
			
		}
	}
    
    // ok
    function fillNextElementWithID(id, element) {
        if (element.getAttribute('id') === "node_div_4") {
            autoExpand = true;
            const office_id = document.getElementById("select_offices").value;
            getAllOfficeUnits(office_id);
            document.getElementById("office_unit_tree_panel").innerHTML = "";
            document.getElementById("office_unit_tree_panel").setAttribute("state", 0);
            getChildren(0, 0, "office_unit_tree_panel", "officeUnitNameUpdateTreeViewTree", '', office_id, "", "");
        }
    }
</script>

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>