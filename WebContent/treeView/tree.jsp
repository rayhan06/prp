<%
    String treeBody = "../treeView/" + request.getParameter("treeBody");
    request.setAttribute("treeBodyPath", treeBody);
    System.out.println("TreeBody = " + treeBody);
    System.out.println("TreeType = " + request.getParameter("treeType"));
    String context = "../../.." + request.getContextPath() + "/";
%>
<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<jsp:include page="../common/layout.jsp" flush="true">
    <jsp:param name="title" value="View Tree"/>
    <jsp:param name="body" value="${treeBodyPath}"/>
</jsp:include>
<script type="text/javascript" src="<%=context%>treeView/tree.js"></script>


