<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import="role.*"%>
<%

	String servletType = request.getParameter("servletType");
	String pageTitle = request.getParameter("pageTitle");
	String Options;
	LoginDTO loginDTO2 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String Language2 = LM.getText(LC.CENTRE_EDIT_LANGUAGE, loginDTO2);
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	Date date = new Date();
	String datestr = dateFormat.format(date);
	
	ArrayList<RoleDTO> RoleDTOs = RoleDAO.getDTOs();
	
			
%>


<input type='hidden' id='servletType' value='<%=servletType%>' />
<div class="row" style="margin: 0px !important;">
	<div class="col-md-12" style="padding: 5px !important;">
		<div id="ajax-content">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-check-square-o"></i><%=pageTitle%>
					</div>

				</div>
				<form  action="Office_unit_organogramsServlet?actionType=MultiRole" id="bigform" name="bigform"  method="POST">
					<div class="portlet-window-body">
						<div class="row">
							<table class="table table-bordered table-hover">
								<tr>
								<%
									for(int j = 0; j < RoleDTOs.size(); j ++)
									{
										RoleDTO roleDTO = RoleDTOs.get(j);
									
								%>
									<td>
										<label class="control-label">
											<%=roleDTO.roleName%>:
										</label>
									</td>
									<td>
										<input type='checkbox' name='role_cb' value='<%=roleDTO.ID%>' />
									</td>
									
								<%
									}
								%>
								</tr>
							</table>
							<hr>
						</div>
	
	
						<div class="row">
							
	
							<div class="portlet light col-md-5">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-check-square-o"></i>Select Designations
									</div>
								</div>
								<div class="portlet-window-body">
									<div id="office_unit_tree_panel"
										class="jstree jstree-2 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
										role="tree" state="0">
										<%@include file="treeNode.jsp"%>
									</div>
								</div>
							</div>
						</div>
						<div class="form-actions text-center">
							<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
								CANCEL
							</a>
							<button class="btn btn-success" type="submit">
								SUBMIT
							</button>
						</div>
					</div>
				</form>
			</div>


		</div>
	</div>
</div>



<script type="text/javascript">

function checkbox_toggeled(id, text, cb_id, name, parent_id)
{
	fillChildren(id, text, cb_id, name, parent_id);
}

function fillNextElementWithID(id, element)
{
	console.log("\n\nback in js, got element = " + element.getAttribute('id'));
	
}
</script>
