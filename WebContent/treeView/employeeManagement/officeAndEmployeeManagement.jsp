<%@ page import="language.LM" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="language.LC" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="util.CommonConstant" %>
<%@ page import="util.HttpRequestUtils" %>

<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    String context = "../../.." + request.getContextPath() + "/";
    String servletType = request.getParameter("servletType");
    LoginDTO loginDTO2 = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language2 = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String today = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
    int year = Calendar.getInstance().get(Calendar.YEAR);
    String userName = request.getParameter("userName");

%>
<input type='hidden' id='servletType' value='<%=servletType%>'/>
<div class="kt-portlet kt-portlet--responsive-tablet-and-mobile py-3">
    <div class="kt-portlet__body">
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=LM.getText(LC.APPOINTMENT_ADD_PATIENTID, loginDTO2)%>
                        </label>
                        <div class="col-md-9">
                            <button type="button"
                                    class="btn btn-border-radius text-white shadow form-control"
                                    style="background-color: #4a87e2;"
                                    onclick="addEmployee()">
                                <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO2)%>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="phone">
                            <%=LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO2)%>
                        </label>
                        <div class="col-md-9">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">+88</div>
                                </div>
                                <input type="text" class="form-control" id="phone" name="phone"
                                       maxlength="11"
                                       placeholder="<%=LM.getText(LC.GLOBAL_MOBILE_PLACE_HOLDER, loginDTO2)%>"
                                       onChange='setSearchChanged()'>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="userName">
                            <%=LM.getText(LC.HM_USER_NAME, loginDTO2)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="userName" name="userName"
                                   placeholder="<%=LM.getText(LC.HM_USER_NAME, loginDTO2)%>"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <input type="button" value="<%=(LM.getText(LC.GLOBAL_SEARCH, loginDTO2))%>"
                           class="btn btn-primary btn-hover-brand btn-border-radius shadow"
                           onclick="getEmployeeDetails()">
                </div>
            </div>
        </div>

        <div class="row mt-5" id="search_employee_info" style="display: none">
            <div class="col-md-12">
                <div class="d-flex justify-content-between">
                    <h5 class="caption-subject font-blue-madison table-title"><%=(LM.getText(LC.OFFICE_AND_EMPLOYEE_MANAGEMENT_PERSONAL_INFORMATION, loginDTO2))%>
                    </h5>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">
                        <thead>
                        <tr>
                            <th><%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAME, loginDTO2))%>
                            </th>
                            <th><%=(LM.getText(LC.EMPLOYEE_RECORDS_EDIT_USERNAME, loginDTO2))%>
                            </th>
                            <th><%=(LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO2))%>
                            </th>
                        </tr>
                        </thead>
                        <tbody id="info_table_body">
                        <tr>
                            <td id="p_name"></td>
                            <td id="p_user_id"></td>
                            <td id="p_mobile"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-12">
                <h5 class="caption-subject font-blue-madison table-title"><%=(LM.getText(LC.OFFICE_AND_EMPLOYEE_MANAGEMENT_CURRENT_DESIGNATION, loginDTO2))%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead class="text-nowrap">
                        <tr class="uppercase">
                            <th><%=(LM.getText(LC.OFFICE_AND_EMPLOYEE_MANAGEMENT_DESIGNATION, loginDTO2))%>
                            </th>
                            <th><%=(LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_INCHARGELABEL, loginDTO2))%>
                            </th>
                            <th><%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_JOININGDATE, loginDTO2))%>
                            </th>
                            <th>
                                <%=isLanguageEnglish ? "Grade" : "গ্রেড"%>
                            </th>
                            <th>
                                <%=isLanguageEnglish ? "Pay scale" : "পে স্কেল"%>
                            </th>
                            <th>
                                <%=isLanguageEnglish ? "Salary Grade" : "বেতন গ্রেড"%>
                            </th>
                            <th colspan="2"><%=(LM.getText(LC.OFFICE_AND_EMPLOYEE_MANAGEMENT_ACTIVITY, loginDTO2))%>
                            </th>
                        </tr>
                        </thead>
                        <tbody id="search_employee_organogram">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <template id="template-OfficeLastDate">
            <tr>
                <input type="hidden" id="p_office_name_" value="" tag='pb_html'>
				<input type="hidden" id="p_designation_" value="" tag='pb_html'>
                <td id="p_user_designation_" tag='pb_html'>
                </td>
                <td id="p_incharge_level_" tag='pb_html'>
                </td>
                <td id="p_joining_date_" tag='pb_html'>
                </td>
                <td id="p_grade_" tag='pb_html'>
                </td>
                <td id="p_grade_level_" tag='pb_html'>
                </td>
                <td id="p_salary_grade_" tag='pb_html'>
                </td>
                <td id="lastWorkingDateDiv_" tag='pb_html' style="min-width: 200px;mix-width: 200px">
                    <%
                        String value = "date_js_";
                    %>
                    <jsp:include page="/date/date.jsp">
                        <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                        <jsp:param name="LANGUAGE" value="<%=Language2%>"></jsp:param>
                        <jsp:param name="END_YEAR" value="<%=year%>"></jsp:param>
                    </jsp:include>
                </td>
                <td id="submit_btn_div" tag='pb_html'>
                    <button class="btn btn-success" id="submit_btn_" tag='pb_html'>
                        <%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_SUBMIT, loginDTO2))%>
                    </button>
                </td>
            </tr>
        </template>
        <div id="drop_down" class="row">
            <div class="col-md-12">
                <div id="ajax-content">
                    <div class="col-md-12 px-0">
                        <h3 class="caption"><%=(LM.getText(LC.OFFICE_AND_EMPLOYEE_MANAGEMENT_TITLE, loginDTO2))%>
                        </h3>
                    </div>
                    <div class="form-group row">
                        <div class="form-group row col-12 mx-0 px-0">
                            <label class="col-md-1 col-form-label">
                                <%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITID, loginDTO2)%><span
                                    class="required">*</span>
                            </label>
                            <div class="col-lg-4 col-md-6 my-3 my-md-0">
                                <%--Office Unit Modal Trigger Button--%>
                                <button type="button"
                                        class="btn btn-block shadow btn-primary text-white btn-border-radius"
                                        id="office_units_id_modal_button" onclick="officeModalButtonClicked();">
                                    <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO2)%>
                                </button>

                                <div class="input-group" id="office_units_id_div" style="display: none">
                                    <input type="hidden" name='officeUnitId' id='office_units_id_input' value="">
                                    <button type="button"
                                            class="btn btn-secondary btn-block shadow-sm btn-border-radius form-control"
                                            id="office_units_id_text" onclick="officeModalEditButtonClicked()">
                                    </button>
                                    <span class="input-group-btn" style="width: 5%;padding-right: 5px;" tag='pb_html'>
                                        <button type="button" class="btn btn-outline-danger"
                                                onclick="crsBtnClicked('office_units_id');"
                                                id='office_units_id_crs_btn' tag='pb_html'>
                                            x
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-3">

                                <button type="button"
                                        class="btn shadow btn-primary text-white btn-border-radius text-nowrap"
                                        onclick="addDesignation()">
                                    <%=isLanguageEnglish ? "Add Designation" : "পদবী যোগ করুন" %>
                                </button>
                            </div>
                        </div>

                    </div>
                    <div style="display:none">
                        <%@include file="../dropDownNode.jsp" %>
                    </div>
                    <hr/>
                    <div class="table-responsive">
                        <table id="loadingTable" class="table table-bordered table-striped">
                            <%--don't put these tr inside tbody--%>
                            <tr class="loading-gif" style="display: none;">
                                <td class="text-center" colspan="100%">
                                    <img alt="" class="loading"
                                         src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
                                    <span><%=isLanguageEnglish ? "Loading..." : "লোড হচ্ছে..." %></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="" id="search_table">
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap" id="realTable"
                               style="display: none;">
                            <thead>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="6">
                                    <%=(LM.getText(LC.OFFICE_AND_EMPLOYEE_MANAGEMENT_UNIT_NAME, loginDTO2))%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="kt-radio kt-radio--bold kt-radio--brand">
                                        <input type="radio" disabled="disabled"
                                               style="font-size: 18px; font-weight: bold;">
                                        <span></span>
                                    </label>
                                </td>
                                <td>
                                    <%=(LM.getText(LC.OFFICE_AND_EMPLOYEE_MANAGEMENT_DESIGNATION_NAME, loginDTO2))%>
                                </td>
                                <td>
                                    <%=(LM.getText(LC.OFFICE_AND_EMPLOYEE_MANAGEMENT_EMPLOYEE_NAME, loginDTO2))%>
                                </td>
                                <td>
                                    <select class="form-control">
                                    </select>
                                </td>
                                <td>
                                    <button class="btn btn-primary btn-hover-brand btn-square">Ok</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="applist" style="display: none">
    <h3>Permitted App</h3>
    <div id='permitted_app' class="kt-radio-list"></div>
    <h3>Non Permitted App</h3>
    <div id='nonpermitted_app' class="kt-radio-list"></div>
    <button class="btn btn-primary btn-hover-brand btn-square mt-3" onclick="submitAppPermission()">Submit</button>
</div>
<jsp:include page="../../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<jsp:include page="../../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script src="<%=context%>/assets/scripts/util1.js"></script>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var office = 0;
    var ministry_id = 0;
    var office_layer = 0;
    var office_origin = 0;
    var selected_row_index = -1;
    var user_info = {};
    var user_organogram_info = [];
    var selected_office = {};
    var permitted_apps = [];
    var non_permitted_apps = [];
    var designation_id;
    const today = '<%=today%>';
    const isLangEng = <%=isLanguageEnglish%>;

    const submitBtn = '<%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_SUBMIT, loginDTO2))%>';
    let name = '';
    var lastSelectedOfficeId = '';

    function showPermittedAppSearchedEmployee() {
        showPermittedApp(user_info.iD, user_info.designation_id);
    }

    function patient_inputted(userName, orgId) {
        console.log("patient_inputted " + userName);
        if (isLangEng) {
            $("#userName").val(userName);
        } else {
            $("#userName").val(convertNumberToBangla(userName));
        }
        employee_inputted(userName);
    }

    function employee_inputted(value) {
        console.log('employee_inputted changed value: ' + value);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                fillUserInfo(this.responseText)
            } else {
                //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
            }
        };

        xhttp.open("POST", "FamilyServlet?actionType=getEmployeeInfo&userName=" + value + "&language=<%=Language%>", true);
        xhttp.send();
    }

    function fillUserInfo(responseText) {
        var userNameAndDetails = JSON.parse(responseText);
        console.log("name = " + userNameAndDetails.name);
        $("#phone").val(phoneNumberRemove88ConvertLanguage(userNameAndDetails.phone, '<%=Language%>'));

    }

    $(document).ready(() => {
        if (<%=userName!=null%>) {
            getEmployeeDetails('<%=userName%>');
            $("#search_box").hide();
        }
    });

    function addDesignation() {
        window.location.href = '<%=request.getContextPath()%>' + '/Office_unit_organogramServlet?actionType=getAddPage&officeUnitId=' + lastSelectedOfficeId;
    }

    function showPermittedApp(employee_record_id, _designation_id) {
        designation_id = _designation_id;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                permitted_apps = [];
                document.getElementById('permitted_app').innerHTML = '';
                JSON.parse(this.responseText).forEach(function (val) {
                    permitted_apps.push({text: val.applicationNameBng, value: val.menuId});
                });
                showUnpermittedApp(employee_record_id, designation_id);
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("Get", "EmployeeAssignServlet?actionType=getEmployeePermittedApp" +
            '&designation_id=' + designation_id, true);
        xhttp.send();
    }

    function showUnpermittedApp(employee_record_id, designation_id) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            document.getElementById('nonpermitted_app').innerHTML = '';
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText != '') {
                    non_permitted_apps = [];
                    JSON.parse(this.responseText).forEach(function (val) {
                        if (val.applicationNameBng === null || val.applicationNameBng === "") {
                            non_permitted_apps.push({text: "নাম পাওয়া যায় নি", value: val.menuId});
                        } else {
                            non_permitted_apps.push({text: val.applicationNameBng, value: val.menuId});
                        }
                        const radioHtml = "<label class='kt-radio kt-radio--brand' style='font-size: 1.3rem;'><input id='" + val.menuId + "'type='radio' name='nonpermitted' value='" + val.menuId + "'" + ">" + val.applicationNameBng + "<span></span></label>";
                        document.getElementById('nonpermitted_app').innerHTML += radioHtml;
                    });
                    permitted_apps.forEach(function (x) {
                        const radioHtml = "<label class='kt-radio kt-radio--brand' style='font-size: 1.3rem;><input id='" + x.value + "' type='radio' name='permitted' value='" + x.value + "'" + ">" + x.text + "<span></span></label>";
                        document.getElementById('permitted_app').innerHTML += radioHtml;
                    });
                    bootbox.dialog({
                        message: document.getElementById('applist').innerHTML
                    });
                } else {
                }
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("Get", "EmployeeAssignServlet?actionType=getEmployeeUnPermittedApp" +
            '&designation_id=' + designation_id, true);
        xhttp.send();
    }

    function submitAppPermission() {
        var r = document.querySelector('input[name="nonpermitted"]:checked').value;
        if (r === null || r === undefined) {
            bootbox.hideAll();
            return;
        }
        var t = [r];
        confirmPermission(t, designation_id);
    }

    function confirmPermission(result, designation_id) {
        var app_ids = '';
        for (var i = 0; i < result.length; i++) {
            app_ids += ('&app_id=' + result[i]);
        }
        console.log('confirm permission app ', app_ids, ' ', result, ' ', designation_id);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText != '') {
                    bootbox.hideAll();
                    bootbox.confirm("Success", function (val) {
                    });
                    //showToast(success_message_bng, success_message_eng);
                } else {
                }
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("Post", "EmployeeAssignServlet?actionType=assignAppPermission" +
            '&designation_id=' + designation_id + app_ids, true);
        xhttp.send();
    }

    function getEmployeeDetails(paramUserName = null) {
        var userName = document.getElementById('userName').value;
        if (paramUserName != null) {
            userName = paramUserName;
        }
        var phone = document.getElementById('phone').value;

        if (!userName && !phone) {
            showToast('নাম অথবা ফোন যে কোন একটি তথ্য দিন', 'Please provide name or phone');
            return;
        }
        $('#search_employee_info').show();
        document.getElementById('search_employee_organogram').innerHTML = '';
        $('#info_table_body').hide();
        user_info = {}
        name = '';
        user_organogram_info = [];
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText != '' && this.responseText.length > 0 && this.responseText != '[]') {
                    try {
                        var list = JSON.parse(this.responseText);
                        let userName = '';
                        let mobileNumber = '';
                        for (var i = 0; i < list.length; i++) {
                            var data = list[i];
                            user_info.id = data.id;
                            user_info.employee_office_id = data.employee_office_id;
                            if (language === 'english') {
                                if (data.username) {
                                    userName = data.username;
                                }
                                if (data.nameEng) {
                                    name = data.nameEng;
                                }
                                if (data.personalMobile) {
                                    mobileNumber = data.personalMobile;
                                }
                            } else {
                                if (data.username) {
                                    userName = convertNumberToBangla(data.username);
                                }
                                if (data.nameBng) {
                                    name = data.nameBng;
                                }
                                if (data.personalMobile) {
                                    mobileNumber = convertNumberToBangla(data.personalMobile);
                                }
                            }
                            user_info.nameEng = data.nameEng;
                            user_info.nameBng = data.nameBng;
                            user_info.personalEmail = data.personalEmail;
                            user_info.personalMobile = data.personalMobile;
                            user_info.designation_id = data.designation_id;
                            user_info.designation = data.designation;
                            user_info.unitNameEng = data.unitNameEng;
                            user_info.unitNameBng = data.unitNameBng;
                            user_info.officeNameEng = data.officeNameEng;
                            user_info.officeNameBng = data.officeNameBng;
                            user_info.joiningDate = data.joiningDate;
                            user_info.lastOfficeDate = data.lastOfficeDate;
                            user_organogram_info.push(data);
                            let office = '';
                            let post = '';
                            let inChargeLevel = '';
                            let grade = '';
                            let salaryGrade = '';
                            let gradeLevel = '';
                            let start_date = '';
                            let attachmentFlag = data.attachmentFlag;
                            if (user_info.lastOfficeDate == <%=SessionConstants.MIN_DATE%>) {
                                if (language === 'english') {
                                    if (data.unitNameEng) {
                                        office = data.unitNameEng;
                                    }
                                    if (data.designationEng) {
                                        post = data.designationEng;
                                    }
                                    if (data.joiningDateEng) {
                                        start_date = data.joiningDateEng;
                                    }
                                    if (data.inChargeLabelEng) {
                                        inChargeLevel = data.inChargeLabelEng;
                                    }
                                    if (data.gradeEng) {
                                        grade = data.gradeEng;
                                    }
                                    if (data.gradeLevelEng) {
                                        gradeLevel = data.gradeLevelEng;
                                    }
                                    if (data.salaryGradeTypeEng) {
                                    	salaryGrade = data.salaryGradeTypeEng;
                                    }
                                } else {
                                    if (data.unitNameBng) {
                                        office = data.unitNameBng;
                                    }
                                    if (data.designationBng) {
                                        post = data.designationBng;
                                    }
                                    if (data.joiningDateBng) {
                                        start_date = data.joiningDateBng;
                                    }
                                    if (data.inChargeLabelBng) {
                                        inChargeLevel = data.inChargeLabelBng;
                                    }
                                    if (data.gradeBng) {
                                        grade = data.gradeBng;
                                    }
                                    if (data.gradeLevelBng) {
                                        gradeLevel = data.gradeLevelBng;
                                    }
                                    if (data.salaryGradeTypeBng) {
                                    	salaryGrade = data.salaryGradeTypeBng;
                                    }
                                }
                                addRow(i, data);

                                document.getElementById("p_user_designation_" + i).innerHTML = post + '<br>' + office;
								document.getElementById("p_designation_" + i).value = post;
                                document.getElementById("p_office_name_" + i).value = office;
                                document.getElementById("p_incharge_level_" + i).innerHTML = inChargeLevel;
                                document.getElementById("p_joining_date_" + i).innerHTML = start_date;
                                document.getElementById("p_grade_" + i).innerHTML = grade;
                                document.getElementById("p_grade_level_" + i).innerHTML = gradeLevel;
                                document.getElementById("p_salary_grade_" + i).innerHTML = salaryGrade;

                                document.getElementById("submit_btn_" + i).setAttribute('onclick', 'setLastWorkingDate("' + i + '")');

                            }
                        }
                        if (name || userName || mobileNumber) {
                            $('#p_name').html(name);
                            $('#p_user_id').html(userName);
                            $('#p_mobile').html(mobileNumber);
                            $('#info_table_body').show();
                        }
                    } catch (e) {
                        console.log(e);
                        showToast('কোন তথ্য পাওয়া যায় নি', "Don't find any data");
                    }
                } else {
                    showToast('কোন তথ্য পাওয়া যায় নি', "Don't find any data");
                }
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("Get", "EmployeeAssignServlet?actionType=getEmployeeDetailsByUserName" +
            '&userName=' + userName +
            '&phone=' + phone, true);
        xhttp.send();
    }

    function addRow(child_table_id, data) {
        var t = $("#template-OfficeLastDate");
        $('#search_employee_organogram').append(t.html());
        var tr = $('#search_employee_organogram').find("tr:last-child");
        tr.find("[tag='pb_html']").each(function (index) {
            var prev_id = $(this).attr('id');
            $(this).attr('id', prev_id + child_table_id);
        });

        setMinDateByTimestampAndId("date_js_" + child_table_id, data.joiningDateLong);
        select2SingleSelector('#daySelectiondate_js_' + child_table_id, '<%=Language%>');
        select2SingleSelector('#monthSelectiondate_js_' + child_table_id, '<%=Language%>');
        select2SingleSelector('#yearSelectiondate_js_' + child_table_id, '<%=Language%>');


    }

    function setLastWorkingDate(id) {
        var lastDate = getDateStringById("date_js_" + id, "YYYY-MM-DD");
        const isValidDate = dateValidator("date_js_" + id, true);
        console.log(lastDate);
        if (!isValidDate) {
            showToast('ভ্যালিড তারিখ দিন', 'Please select valid date');
            return;
        }
        if (!lastDate || lastDate == "") {
            showToast('তারিখ দিন', 'Please select date');
            return;
        }
        let msg;
        let dateArr = lastDate.split("-");
        let name = $('#p_name').text();
        let designation = $('#p_designation_' + id).val();
        let ofc = $('#p_office_name_' + id).val();
        let date = dateArr[2] + "/" + dateArr[1] + "/" + dateArr[0];
        console.log(name + ' ' + designation + ' ' + ofc);
        if (language === 'english') {
            msg = "Do you to submit " + date + " as last office day for " + name + " from " +
                designation + " position of " + ofc + " office?";
        } else {
            msg = "আপনি কি " + name + " কে " + convertNumberToBangla(date) + " তারিখ হতে " + ofc + " এর " + designation + " পদ থেকে অব্যহতি দিতে চান?";
        }
        messageDialog(msg, '', 'warning', true, '<%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_YES_SUBMIT, loginDTO2))%>',
            '<%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_CANCEL, loginDTO2))%>', () => {
                var emp_office = user_organogram_info[id];
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        if (this.responseText != '') {
                            showToast(success_message_bng, success_message_eng);
                            // getEmployeeDetails();
                            let responseText = this.responseText.split("=");
                            let result = responseText[1];
                            if (result !== null && result === 'false') {
                                window.location.href = ("<%=request.getContextPath()%>" + "/Pi_returnServlet?actionType=movableProduct&employeeOfficeId=" + emp_office.employee_office_id);
                            }
                        } else {
                            showToast(failed_message_bng, failed_message_eng);
                        }
                    } else if (this.readyState == 4 && this.status != 200) {
                        alert('failed ' + this.status);
                    }
                };
                console.log($('#lastWorkingDateId_' + id));

                xhttp.open("Post", "EmployeeAssignServlet?actionType=addLastOfficeDate" +
                    '&employee_office_id=' + emp_office.employee_office_id +
                    '&last_office_date=' + lastDate, true);
                xhttp.send();
            }, () => {

            });
    }

    function setLastAttachmentDate(id) {
        let lastDate = getDateTimestampById("date_js_" + id);
        let lastDateStr = getDateStringById("date_js_" + id, "YYYY-MM-DD");
        const isValidDate = dateValidator("date_js_" + id, true);
        console.log(lastDate);
        if (!isValidDate) {
            showToast('ভ্যালিড তারিখ দিন', 'Please select valid date');
            return;
        }
        if (!lastDate || lastDate == "") {
            showToast('তারিখ দিন', 'Please select date');
            return;
        }
        let msg;
        let name = $('#p_name').text();
        let designation = $('#p_user_designation_' + id).text();
        let ofc = $('#p_office_name_' + id).val();
        let dateArr = lastDateStr.split("-");
        let date = dateArr[2] + "/" + dateArr[1] + "/" + dateArr[0];
        console.log(name + ' ' + designation + ' ' + ofc);
        if (language === 'english') {
            msg = "Do you to submit " + date + " as last attachment day for " + name + " from attachment of " + ofc + " office?";
        } else {
            msg = "আপনি কি " + name + " কে " + convertNumberToBangla(date) + " তারিখ হতে " + ofc + " এর সংযুক্তি থেকে অব্যহতি দিতে চান?";
        }
        messageDialog(msg, '', 'warning', true, '<%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_YES_SUBMIT, loginDTO2))%>',
            '<%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_CANCEL, loginDTO2))%>', () => {
                var emp_office = user_organogram_info[id];
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        if (this.responseText != '') {
                            showToast(success_message_bng, success_message_eng);
                            getEmployeeDetails();
                        } else {
                            showToast(failed_message_bng, failed_message_eng);
                        }
                    } else if (this.readyState == 4 && this.status != 200) {
                        alert('failed ' + this.status);
                    }
                };
                console.log($('#lastWorkingDateId_' + id));

                xhttp.open("Post", "EmployeeAssignServlet?actionType=setLastAttachmentDate" +
                    '&attachmentId=' + emp_office.attachmentId +
                    '&lastDate=' + lastDate, true);
                xhttp.send();
            }, () => {

            });
    }

    function assignEmployeeCheck(selectedRowIndex, employee_id, officeId, office_unit_id, organogram_id, designation_level, designation_sequence, employee_office_id) {
        console.log('data: ', selectedRowIndex, employee_id, ' ', officeId, ' ', office_unit_id, ' ', organogram_id, ' ', employee_office_id);
        selected_office.selectedRowIndex = selectedRowIndex;
        selected_office.employee_id = employee_id;
        selected_office.officeId = officeId;
        selected_office.office_unit_id = office_unit_id;
        selected_office.organogram_id = organogram_id;
        selected_office.designation_level = designation_level;
        selected_office.designation_sequence = designation_sequence;
        selected_office.employee_office_id = employee_office_id;
        selected_office.existing_employee_office_id = employee_office_id;
        console.log('office to assign ', selected_office);
        selected_row_index = selectedRowIndex;
        const incharge_label = document.getElementById('incharge_label_' + selected_row_index).value;
        selected_office.incharge_label = incharge_label;
        const grade_type_level = document.getElementById('grade_type_level_' + selected_row_index).value;
        selected_office.grade_type_level = grade_type_level;
        selected_office.designation_name = document.getElementById("" + organogram_id).innerText;
        console.log('incharge_level: ', incharge_label, " designation name: " + selected_office.designation_name);
        console.log('grade_type_level: ', grade_type_level);
    }

    function assignEmployeeOK(obj) {


        if (user_info == null || Object.keys(user_info).length === 0 && user_info.constructor === Object) {
            showToast('কর্মকর্তা সিলেক্ট করুন', 'Please select employee first');
            return;
        }

        if (selected_office == null || (Object.keys(selected_office).length === 0 && selected_office.constructor === Object)) {
            showToast('পদবি সিলেক্ট করুন', 'Please select designation first');
            return;
        }


        if (selected_office.selectedRowIndex != obj) {
            showToast('সঠিক পদবি সিলেক্ট করুন', 'Please select correct designation first');
            return;
        }
        
        console.log("$('#salaryGradeType_' + selected_row_index).val() = " + $('#salaryGradeType_' + selected_row_index).val() );

        var assignDate = getDateStringById("assign_date_js_" + selected_row_index, "YYYY-MM-DD");
        var assignDateLong = getDateTimestampById("assign_date_js_" + selected_row_index)
        const isValidDate = dateValidator("assign_date_js_" + selected_row_index, true, '');
        console.log(assignDate);
        if (!assignDate || assignDate == "") {
            showToast('তারিখ দিন', 'Please select date');
            return;
        }
        if (!isValidDate) {
            showToast('ভ্যালিড তারিখ দিন', 'Please select valid date');
            return;
        }
        console.log($('#incharge_label_' + selected_row_index));
        console.log(document.getElementById('incharge_label_' + selected_row_index).value);


        if (!document.getElementById('incharge_label_' + selected_row_index).value) {
            showToast('ইনচার্জ লেভেল বাচাই করুন', "Please Select Incharge label");
            return;
        }
        console.log($('#grade_type_level_' + selected_row_index));
        console.log(document.getElementById('grade_type_level_' + selected_row_index).value);
        // TODO: 07-11-21 made optional for now
        // if (document.getElementById('incharge_label_' + selected_row_index).value === 'Routine responsibility' && !document.getElementById('grade_type_level_' + selected_row_index).value) {
        //     showToast('গ্রেড লেভেল বাচাই করুন', "Please Select Grade Level label");
        //     return;
        // }


        let msg;
        console.log(document.getElementById('incharge_label_' + selected_row_index).innerText);
        if (language === 'english') {
            msg = "Do you want to assign " + name + " to " + $('#' + selected_office.organogram_id).text().trim() + " designation of " + $('#selected_office_unit').text().trim() +
                " office as " + $('#incharge_label_' + selected_row_index + " option:selected").text() + "?";
        } else {
            msg = "আপনি কি " + name + " কে " + $('#' + selected_office.organogram_id).text().trim() + " পদে " + $('#selected_office_unit').text().trim() + " এ " +
                $('#incharge_label_' + selected_row_index + " option:selected").text() + " হিসেবে নিযুক্ত করতে চান?";
        }
        messageDialog(msg, '', 'success', true, '<%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_YES_SUBMIT, loginDTO2))%>',
            '<%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_CANCEL, loginDTO2))%>', () => {
                try {
                    buttonStateChangeFunction(true);
                    const incharge_label = document.getElementById('incharge_label_' + selected_row_index).value;
                    selected_office.incharge_label = incharge_label;
                    const grade_type_level = document.getElementById('grade_type_level_' + selected_row_index).value;
                    selected_office.grade_type_level = grade_type_level;
                    var xhttp = new XMLHttpRequest();
                    xhttp.onreadystatechange = function () {
                        console.log("response : " + this.responseText);
                        if (this.readyState == 4 && this.status == 200) {
                            let response = JSON.parse(this.responseText);
                            if (response.responseCode === 0) {
                                showError(response.msg, response.msg);

                            } else {
                                showToast(success_message_bng, success_message_eng);
                                fillNextElementWithID(0, 0);
                                getEmployeeDetails();
                            }
                            selected_office = {};
                            buttonStateChangeFunction(false);
                        } else if (this.readyState == 4 && this.status != 200) {
                            alert('failed buttonStateChangeFunction' + this.status);
                            buttonStateChangeFunction(false);
                        }

                    };
                    xhttp.open("Post", "EmployeeAssignServlet?actionType=assignEmployee" +
                        '&employee_record_id=' + user_info.id +
                        '&office_id=<%=CommonConstant.DEFAULT_OFFICE_ID%>' +
                        '&office_unit_id=' + selected_office.office_unit_id +
                        '&office_unit_organogram_id=' + selected_office.organogram_id +
                        '&designation=' + selected_office.designation_name +
                        '&designation_level=' + selected_office.designation_level +
                        '&designation_level=' + selected_office.designation_sequence +
                        '&incharge_label=' + selected_office.incharge_label +
                        '&gradeTypeLevel=' + selected_office.grade_type_level +
                        '&existing_employee_office_id=' + selected_office.existing_employee_office_id +
                        '&salaryGradeType=' + $('#salaryGradeType_' + selected_row_index).val() +
                        '&assign_date=' + assignDateLong,
                        true);
                    xhttp.send();

                } catch (e) {
                    showError(fill_all_input_bng, fill_all_input_eng);
                }
            }, () => {
            });
    }

    // Where tree node is end
    function fillNextElementWithID(id, element) {
        showOrHideLoadingGif('loadingTable', true);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText != '') {
                    document.getElementById('search_table').innerHTML = '';
                    document.getElementById('search_table').innerHTML = this.responseText;
                } else {
                }
                showOrHideLoadingGif('loadingTable', false);
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
                showOrHideLoadingGif('loadingTable', false);
            }

        };
        xhttp.open("Get", "EmployeeAssignServlet?actionType=getEmployeeRecord" + '&officeId=' + lastSelectedOfficeId, true);
        xhttp.send();

    }

    $("#officesType_select2").change(function () {
        showOrHideLoadingGif('loadingTable', true);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText != '') {
                    document.getElementById('search_table').innerHTML = '';
                    document.getElementById('search_table').innerHTML = this.responseText;
                } else {
                }
                showOrHideLoadingGif('loadingTable', false);
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
                showOrHideLoadingGif('loadingTable', false);
            }

        };

        xhttp.open("Get", "EmployeeAssignServlet?actionType=getEmployeeRecord" + '&officeId=' + $(this).val(), true);
        xhttp.send();

    });

    /*function layerChange(layer) {
        let layerid = "#office_layer_" + layer;
        let nextLayer = layer + 1;
        for (let i = nextLayer; i < 10; i++) {
            document.getElementById("office_layer_div_" + i).style.display = 'none';
            document.getElementById("office_layer_" + i).innerHTML = '';
        }
        lastSelectedOfficeId = $(layerid).val();
        let url = "Office_unitsServlet?actionType=ajax_office&parent_id=" + lastSelectedOfficeId + "&language=" + language;
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                console.log(fetchedData);
                if (fetchedData) {
                    document.getElementById("office_layer_div_" + nextLayer).style.display = 'block';
                    document.getElementById("office_layer_" + nextLayer).innerHTML = fetchedData;
                }
                getPosts(lastSelectedOfficeId);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }*/

    function getPosts(officeId) {
        showOrHideLoadingGif('loadingTable', true);
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText != '') {
                    document.getElementById('search_table').innerHTML = '';
                    document.getElementById('search_table').innerHTML = this.responseText;
                    $('#search_table').find('select').each(function () {
                        select2SingleSelector('#' + this.id, '<%=Language2%>');
                    });
                } else {
                }
                showOrHideLoadingGif('loadingTable', false);
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
                showOrHideLoadingGif('loadingTable', false);
            }

        };

        xhttp.open("Get", "EmployeeAssignServlet?actionType=getEmployeeRecord" + '&officeId=' + officeId, true);
        xhttp.send();

    }

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
        document.getElementById('search_table').innerHTML = '';
        lastSelectedOfficeId = '';
    }

    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        $('#office_units_id_modal_button').hide();
        $('#office_units_id_div').show();

        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id_input').val(selectedOffice.id);
        getPosts(selectedOffice.id);
        lastSelectedOfficeId = selectedOffice.id;
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function officeModalEditButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        officeSearchSetSelectedOfficeLayers($('#office_units_id_input').val());
        $('#search_office_modal').modal();
    }

    function buttonStateChangeFunction(value) {
        $("button").prop('disabled', value);
        $("input").prop('disabled', value);
    }

    function showOrHideLoadingGif(tableId, toShow) {
        const loadingGif = $('#' + tableId + ' tr.loading-gif');
        if (toShow) {
            // document.querySelector('#' + tableId + ' tbody.main-tbody').innerHTML = '';
            // document.querySelectorAll('#' + tableId + ' tfoot').forEach(tfoot => tfoot.remove());
            loadingGif.show();
        } else loadingGif.hide();
    }


</script>