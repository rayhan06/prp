<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="pb.*"%>
<%@page import="OfficeUnitsEdms.*"%>
<%

	String servletType = request.getParameter("servletType");
	String pageTitle = request.getParameter("pageTitle");
	String Options;
	LoginDTO loginDTO2 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String Language2 = LM.getText(LC.CENTRE_EDIT_LANGUAGE, loginDTO2);
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	Date date = new Date();
	String datestr = dateFormat.format(date);
	

	
	String actionName = "add";
			
%>
<table id='tr-template' style='display: none'>
	<tr>
		<td class="deleterow" style="width: 30px; text-align: center;"><i
			class="fa fa-times"></i>
		</td>
		

		<td class="text-left">		
		</td>
		
		<td class="text-left">
			<label name="order_label">0</label>			
		</td>
		<td>
			<span class="up-down-link"> 
				<a class="up-link">
					<span>
						<i onclick="moveup(this)" class="fa fa-arrow-circle-up"></i>
					</span>
				</a>
				<a class="down-link">
					<span> 
						<i onclick="movedown(this)" class="fa fa-arrow-circle-down"></i>
					</span>
				</a>
				<input type = "hidden" name = "organogram_id_template" value =""></input>
			</span>
		</td>
		
		<td>
			<select class='form-control'  name='roleType_template'   >
			<%
				
				Options = CommonDAO.getOptions(Language2, "select", "approval_role", "roleType", "form-control", "approval_roleType" );			
	
				out.print(Options);
			%>
			</select>
		</td>
		<td>
			<input class='form-control' required type = "number" name = "days" value =""></input>
		</td>
	</tr>
</table>

<table id='tr-template-2' style='display: none'>
	<tr>
		<td class="deleterow" style="width: 30px; text-align: center;"><i
			class="fa fa-times"></i>
		</td>
		<td class="text-left">
				
		</td>
		
		
		
	</tr>
</table>

<input type='hidden' id='servletType' value='<%=servletType%>' />
<div class="row" style="margin: 0px !important;">
	<div class="col-md-12" style="padding: 5px !important;">
		<div id="ajax-content">
			<div class="portlet light">
				
				<div class="portlet-window-body">
					


					<div class="row">
						<div class="portlet light col-md-12">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-check-square-o"></i>Add Approval Path
								</div>
							</div>
							<div class="portlet-window-body">
								<form class="form-horizontal" action="Approval_pathServlet?actionType=add&officeId=<%=SessionConstants.OFFICE_ID%>" id="bigform" name="bigform"  method="POST" onsubmit="return PreprocessBeforeSubmiting()">
								
									<input type='hidden' class='form-control'  name='officeId' id = 'officeId'/>
										<label class="col-lg-3 control-label">Approval Path Name</label>
										<div class="form-group ">					
											<div class="col-lg-6 ">	
												<input type='text' required class='form-control'  name='nameEn' id = 'nameEn'/>					
											</div>
										</div>
										
										<label class="col-lg-3 control-label">Module</label>
										<div class="form-group ">					
											<div class="col-lg-6 ">	
												<select class='form-control'  name='moduleCat' id = 'moduleCat'   tag='pb_html'>
													<option value="-1" > All </option>
													<%
													int j = 0;
													for(String type: SessionConstants.DOCTYPES)
													{
														%>
														<option value="<%=j%>"><%=type%></option>
														<%
														j ++;
													}
													%>
													</select>				
											</div>
										</div>
										
										<label class="col-lg-3 control-label">Office</label>
										<div class="form-group ">					
											<div class="col-lg-6 ">	
												<select class='form-control formRequired' required data-label="Office"  name='officeUnitId' id = 'officeUnitId' onchange="officeChanged( this.value )"   tag='pb_html'>

													<option value="-1" selected="selected"> All Office </option>		
													<%
													List<OfficeUnitsEdmsDTO> officeUnitsEdmsDTOList = new OfficeUnitsEdmsDAO().getAll( true );
													for( OfficeUnitsEdmsDTO officeUnitsEdmsDTO: officeUnitsEdmsDTOList )
													{ %>

														<option value="<%=officeUnitsEdmsDTO.iD%>">
															<%=officeUnitsEdmsDTO.unitNameEng%>
														</option>

													<%}
													%>
												</select>				
											</div>
										</div>
									
									
									
									<div
										class="jstree jstree-1 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
										id="origin_unit_tree_panel" role="tree" state="0">
										
										<div class="row">
										<div class="col-md-6 col-lg-6">
										
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-check-square-o"></i>Employees In the Approval Path
											</div>
										</div>
										
										<table class="table table-bordered">
											<thead>
													<tr>
														<th>Cancel</th>														
														<th>Name and Designation</th>
														<th>Order</th>
														<th>Change Order</th>
														<th width="25%">Role</th>
														<th>Days Allowed</th>	
													</tr>
											</thead>
											<tbody id="ap_employees">
						
											
																							
											</tbody>
										</table>
										
										</div>
										
										<div class="col-md-6 col-lg-6">
										
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-check-square-o"></i>Expand and Select Employees
											</div>
										</div>
										<div class="portlet-window-body">
											<div id="office_unit_tree_panel"
												class=""
												role="tree" state="0">
												<%@include file="treeNode.jsp"%>
											</div>
										</div>
										
										</div>
										</div>
										
										<br/>
										
										<div class="row">
										
										<div class="portlet light col-md-6 col-lg-6">
										
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-check-square-o"></i>Employees To Notify After the final Step
											</div>
										</div>
	
										<div class="table-responsive">
											<table class="table table-bordered">
											<thead>
													<tr>
														<th>Cancel</th>
														<th>Name and Designation</th>	
													</tr>
											</thead>
												<tbody id="noti_employees">
	
	
	
												</tbody>
											</table>
										</div>
										
								
										<div class="form-actions text-center">
											<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
											Cancel
											</a>
											<button class="btn btn-success" type="submit">
											Submit
											</button>
										</div>
										
										
										</div>
										
										
										<div class=" portlet light col-md-6 col-lg-6">
										
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-check-square-o"></i>Employee to Notify after Final Approval Step
												</div>
											</div>
											<div class="portlet-window-body">
												<div id="office_unit_tree_panel2"
													class=""
													role="tree" state="0">
													
												</div>
											</div>
													
										</div>
									</div>	 <!--  row-->
										
										
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">

function fix_order_labels()
{
	var i = -1;
	jQuery("label[name='order_label']").each(function() {
		$( this ).html(i);
		console.log($( this ).html());
		i++;
	});
}

function PreprocessBeforeSubmiting()
{
	
	console.log("submitting ...... ");	
	
	
	if(employeeCount == 0)
	{
		toastr.error("At least one employee must be in the approval path.");
		return false;
	}
	else
	{
		return true;
	}
	
	
}

var employeeCount = 0;

function addEmployee(id, text, cb_id, name, tbodyID) {
    //if (document.getElementById("td_delete_" + id) == null) 
    {
        var tbody = document.getElementById(tbodyID);
        var template = document.getElementById('tr-template');
        var template_backup = template.innerHTML;


        var tr = template.rows[0];
        
        var tr_id = "tr_ap_selected_" + id;
        
        var i = tr.querySelector("i");
        i.setAttribute("onclick", "removeEmployeeById('" + tr_id + "', '" + cb_id + "')");

        var hiddeninput = tr.querySelector('input[type="hidden"]');
        hiddeninput.setAttribute("name", "organogram_id");
        hiddeninput.setAttribute("value", id);
        //hiddeninput.setAttribute("id", "organogram_id_hidden_" + id + "_" + cb_id);
        //console.log("organogram_id_hidden = " + document.getElementById("organogram_id_hidden_" + id).value);

        var select = tr.querySelector("select");
        select.setAttribute("name", "roleType");


        var td_delete = tr.cells[0];
        td_delete.setAttribute("id", "td_delete_" + id);


        var td_text = tr.cells[1];
        if (name !== "") {
            td_text.innerHTML = text + ", " + name;
        }
        else {
            td_text.innerHTML = text;
        }

        template.innerHTML = template_backup;

        
        var newRow = tbody.insertRow(tbody.rows.length);
        newRow.innerHTML = tr.outerHTML;
        newRow.id = tr_id;
        
        employeeCount ++;

    }

}

function removeEmployeeById(id, cb_id) {
	console.log("cb_id = " + cb_id);
	$('#' + cb_id).prop("checked", false);
	$('#' + id).remove();
    fix_order_labels();
    employeeCount --;
}


function addEmployeeToNotify(id, text, cb_id, name, tbodyID) {
    if (document.getElementById("td_delete_employee_" + id) == null) {
        var tbody = document.getElementById(tbodyID);
        var template = document.getElementById('tr-template-2');
        var template_backup = template.innerHTML;


        var tr = template.rows[0];
        var tr_backup = tr;
        var tr_id = "tr_noti_selected_" + id;
        tr.setAttribute("id", tr_id);
        var i = tr.querySelector("i");
        i.setAttribute("onclick", "removeEmployeeById('" + tr_id + "', '" + cb_id + "')");

       
		
        var hiddenInput = "<input type ='hidden' name='employee_to_notify' value='" + id + "' />";
      


        var td_delete = tr.cells[0];
        td_delete.setAttribute("id", "td_delete_employee_" + id);


        var td_text = tr.cells[1];
        if (name !== "") {
            td_text.innerHTML = name + ", " + text + hiddenInput;
        }
        else {
            td_text.innerHTML = text + hiddenInput;
        }

        template.innerHTML = template_backup;


        tbody.innerHTML += tr.outerHTML;
    }

}
function checkbox_toggeled(id, text, cb_id, name, parent_id, treeName)
{
	var cb = document.getElementById(cb_id);
	
	if(treeName == "OfficeUnitsTree")
	{
		addEmployee(id, text, cb_id, name, "ap_employees");
		fix_order_labels();
	}
	else
	{
		if(cb.checked)
		{
			console.log('checked');			
			addEmployeeToNotify(id, text, cb_id, name, "noti_employees")
								
		}
		else
		{
			console.log('unchecked');
			var tr_id = "tr_noti_selected_" + id;
			removeElementByID(tr_id);
			
		}
	}
	
}

function fillNextElementWithID(element)
{
	console.log("\n\nfillNextElementWithID called");

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById(element).innerHTML = this.responseText;
			
			console.log("Calling auto expand 2");
			doAutoExpand("office_unit_tree_panel2");
		} else if (this.readyState == 4 && this.status != 200) {
			alert('failed ' + this.status);
		}
	};
	xhttp.open("GET", "GenericTreeServlet?actionType=getUnitOriginWithCheckBox2&parentID=9581", true);
	xhttp.send();
}


function officeChanged( obj ){
	


}


window.onload =function ()
{
	$('#bigform').on('keyup keypress', function(e) {
		  var keyCode = e.keyCode || e.which;
		  if (keyCode === 13) { 
		    e.preventDefault();
		    return false;
		  }
		});
	console.log("Calling auto expand");
	doAutoExpand("office_unit_tree_panel");
	
	fillNextElementWithID("office_unit_tree_panel2");
	
	
}


</script>
