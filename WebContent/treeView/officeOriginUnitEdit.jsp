<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    String servletType = request.getParameter("servletType");
    System.out.println(" servletType = " + servletType);
    String pageTitle = (String) request.getAttribute("pageTitle");
    LoginDTO localLoginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
%>
<input type='hidden' id='servletType' value='<%=servletType%>'/>
<div id="ajax-content">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                         height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <path d="M8,9 L8,6.18181818 C8,4.76751186 8.76751186,4 10.1818182,4 L17.8181818,4 C19.2324881,4 20,4.76751186 20,6.18181818 L20,13.8181818 C20,15.2324881 19.2324881,16 17.8181818,16 L15,16 L15,18.8181818 C15,20.2324881 14.2324881,21 12.8181818,21 L5.18181818,21 C3.76751186,21 3,20.2324881 3,18.8181818 L3,11.1818182 C3,9.76751186 3.76751186,9 5.18181818,9 L8,9 Z"
                                  fill="#000000" fill-rule="nonzero"/>
                        </g>
                    </svg>
                    <%=(LM.getText(LC.OFFICE_UNIT_MANAGENENT_LABEL, localLoginDTO))%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row border-bottom mb-3">
                <%@include file="dropDownNode.jsp" %>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <button class="btn btn-primary" id="add_new_unit" style="display: none"
                            onclick="addNewOfficeOriginUnit()">
                        <%=LM.getText(LC.GLOBAL_ADD_NEW, loginDTO)%>
                    </button>
                    <div id="tree_view"
                         class="jstree jstree-2 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
                         role="tree" style="background: white" state="0">
                        <ul class="jstree-container-ul jstree-children" style="background: white"></ul>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12" id="formJsp" style="display: none">
                    <jsp:include page="../office_origin_units/office_origin_unitsEditBody.jsp"/>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var global_office_origins_id = 0;
    var officeUnitCategoryName;
    var addOrEdit = 0, editNodeId = 0;

    function clearEverything() {
        document.getElementById('unitNameBng_text_0').value = '';
        document.getElementById('unitNameEng_text_0').value = '';
        document.getElementById('unitLevel_text_0').value = 0;
        document.getElementById('status_checkbox_0').checked = false;
    }

    function onCategoryChange(unitCategory) {
        officeUnitCategoryName = unitCategory.options[unitCategory.selectedIndex].text;
    }

    // function plusClicked(table, id, text) {
    //     console.log('plus click: ', table, ' ', id, ' ', text);
    //     addOrEdit = 1;
    //     clearEverything();
    //     document.getElementById('formJsp').style.display = '';
    //     if (table !== null) {
    //         document.getElementById('parentUnitId_select_0').disabled = true;
    //         var option = document.createElement("option");
    //         option.value = id;
    //         option.text = text;
    //         document.getElementById('parentUnitId_select_0').add(option);
    //         document.getElementById('parentUnitId_select_0').value = id;
    //     } else {
    //         document.getElementById('parentUnitId_select_0').disabled = false;
    //     }
    // }

    function editNode2(table, id) {
        addOrEdit = 2;
        editNodeId = id;
        document.getElementById('formJsp').style.display = '';
        document.getElementById('parentUnitId_select_0').disabled = false;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.status == 200) {
                if (this.responseText != '') {
                    var result = JSON.parse(this.responseText);
                    document.getElementById('unitNameBng_text_0').value = result.unitNameBng;
                    document.getElementById('unitNameEng_text_0').value = result.unitNameEng;
                    document.getElementById('parentUnitId_select_0').value = result.parentUnitId;
                    document.getElementById('unitLevel_text_0').value = result.unitLevel;
                    document.getElementById('status_checkbox_0').checked = result.stat;
                    var cat = document.getElementById('officeUnitCategory_select_0').options;
                    var len = cat.length;
                    for (var i = 0; i < len; i++) {
                        console.log(cat.item(i).value, ' ', cat.item(i).text);
                        if (cat.item(i).text === result.officeUnitCategory) {
                            document.getElementById('officeUnitCategory_select_0').value = cat.item(i).value;
                            break;
                        }
                    }
                }
                else {
                    console.log("nul response");
                }
            }
            else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        var requestmsg = "Office_origin_unitsServlet" + "?actionType=getDetailsById" + "&id=" + id;
        xhttp.open("GET", requestmsg, true);
        xhttp.send();
    }

    function getOfficeOrigins() {
        getChildren(0, 0, "tree_view", "getOfficeOriginUnitRecursive", getOfficeOrigins, global_office_origins_id, "", "");
    }

    var x0, x1, x2, x3, x4, x5, x6, x7, x8, x9;

    function onSubmit2() {
        var actionType;
        if (addOrEdit === 1) actionType = 'add';
        else actionType = 'edit';
        var m = document.getElementById('select_office_ministries').value;
        var l = document.getElementById('select_office_layers').value;
        var o = document.getElementById('select_office_origins').value;
        x0 = document.getElementById('officeMinistryId_hidden_0').value = m;
        x1 = document.getElementById('officeLayerId_hidden_0').value = l;
        x2 = document.getElementById('officeOriginId_hidden_0').value = o;
        x3 = document.getElementById('unitNameBng_text_0').value;
        x4 = document.getElementById('unitNameEng_text_0').value;
        x5 = document.getElementById('officeUnitCategory_select_0').value;
        x7 = document.getElementById('parentUnitId_select_0').value;
        x8 = document.getElementById('unitLevel_text_0').value;
        x9 = document.getElementById('status_checkbox_0').value;
        var cat = document.getElementById('officeUnitCategory_select_0').options;

        var len = cat.length;
        for (var i = 0; i < len; i++) {
            console.log(cat.item(i).value, ' ', cat.item(i).text);
            if (cat.item(i).value === x5) {
                x6 = cat.item(i).text;
                break;
            }
        }
        console.log('ministry id: ', x0, ' office layer: ', x1, ' origin id: ', x2, ' name bn: ', x3,
            ' name en: ', x4, ' category: ', x5, 'category name: ', x6, ' unit id: ', x7, ' unit level: ', x8);

        //if (!checkValidity(x0, x1, x2, x3, x4, x5, x6, x7, x8, x9)) return;

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (this.responseText != '') {
                    document.getElementById('unitNameBng_text_0').value = '';
                    document.getElementById('unitNameEng_text_0').value = '';
                    document.getElementById('status_checkbox_0').value = 'false';
                    document.getElementById('unitLevel_text_0').value = '0';
                    document.getElementById('officeUnitCategory_select_0').selected = 1;
                    showToast(success_message_bng, success_message_eng);
                    document.getElementById("tree_view").innerHTML = "";

                    fillNextElementWithID(1, save_last_element);
                    // document.getElementById('add_new_unit').style.display = '';
                    //
                    // var office_origins = document.getElementById("select_office_origins");
                    // var office_origins_id = office_origins.options[office_origins.selectedIndex].value;
                    //
                    // document.getElementById("tree_view").innerHTML = "";
                    // document.getElementById("tree_view").setAttribute("state", 0);
                    //
                    // global_office_origins_id = office_origins_id;
                    // getChildren(0, 0, "tree_view", "getOfficeOriginUnits", null, office_origins_id, "", "");
                }
                else {
                    console.log("nul response");
                }
            }
            else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        var requestmsg = "Office_origin_unitsServlet" + "?actionType=" + actionType
            + "&id=" + editNodeId
            + "&officeMinistryId=" + x0
            + "&officeLayerId=" + x1
            + "&officeOriginId=" + x2
            + "&unitNameBng=" + x3
            + "&unitNameEng=" + x4
            + "&officeUnitCategory=" + x6
            + "&parentUnitId=" + x7
            + "&unitLevel=" + x8
            + "&status=" + x9;
        console.log("requestmsg = " + requestmsg);
        xhttp.open("POST", requestmsg, true);
        xhttp.send();
    }

    let save_last_element = null;

    function fillNextElementWithID(id, element) {
        autoExpand = true;
        if (element.getAttribute('id') === "node_div_3") {
            save_last_element = element;
            $('#add_new_unit').css('display', '');

            var office_origins = document.getElementById("select_office_origins");
            var office_origins_id = office_origins.options[office_origins.selectedIndex].value;

            document.getElementById("tree_view").innerHTML = "";
            document.getElementById("tree_view").setAttribute("state", 0);

            global_office_origins_id = office_origins_id;
            getChildren(0, 0, "tree_view", "getOfficeOriginUnits", null, office_origins_id, "", "");
            getOfficeOriginUnit();
            addNewOfficeOriginUnit();
        }
    }

    function newOriginUnitFound(id, element) {
        if (id === 0)
            console.log('newOriginUnitFound ', id, ' ', element);
    }
</script>

<script>
    function checkValidity(x0, x1, x2, x3, x4, x5, x6, x7, x8, x9) {
        if (x0 <= '0' || x1 <= '0' || x2 <= '0' || x3 == '' || x4 == '' || x5 == '0' || x6 == '' || x8 == null || x8 == '') {
            showError(fill_all_input_bng, fill_all_input_eng);
            return false;
        }
        return true;
    }
</script>