<%@ page language="java" %>
<%@ page import="java.util.*" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="treeView.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%

	LoginDTO loginDTO2 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String servletType = request.getParameter("servletType");
	String pageTitle = request.getParameter("pageTitle");
	String Language2 = LM.getText(LC.CENTRE_EDIT_LANGUAGE, loginDTO2);
	String Options;
			
%>
<input type='hidden' id='servletType' value='<%=servletType%>' />
<div class="row" style="margin: 0px !important;">
	<div class="col-md-12" style="padding: 5px !important;">
		<div id="ajax-content">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-check-square-o"></i><%=pageTitle%>
					</div>

				</div>
				<div class="portlet-window-body">
				

						

					
					<div class="row" >
					<div class="col-md-12 col-lg-12">
					<div id = "pathDiv" style = "display: none;">
					<label class="col-lg-1 control-label" style="padding-left:0px;">Select Approval Path</label>
						<div class="col-lg-3">
						<select class='form-control'  name='pathSelect' id='pathSelect' onChange="pathSelected()">
						</div>	
						</select>
					</div>
					</div>
					</div>


					
						<div class="portlet light col-md-6">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-check-square-o"></i>Approval Path Template
								</div>
							</div>
							<div class="portlet-window-body">
								<div
									class="jstree jstree-1 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
									id="template_path" role="tree" state="0">
									
								</div>
							</div>
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-check-square-o"></i>Employee to Notify after Final Approval Step
								</div>
							</div>
							<div class="portlet-window-body">
								<div id="office_unit_tree_panel"
									class="jstree jstree-2 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
									role="tree" state="0">
									<%@include file="treeNode.jsp"%>
								</div>
							</div>
						</div>
						
						

						<div class="portlet light col-md-6">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-check-square-o"></i>Approval Path
								</div>
							</div>
							<div class="portlet-window-body">
								<form class="form-horizontal"
									action="Approval_pathServlet?actionType=CopyFromTemplate&pathType=3"
									id="bigform" name="bigform" method="POST">
									<div id="office_unit_tree_panel"
										class="jstree jstree-2 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
										role="tree" state="0">
										<input type = "hidden" id = "ApprovalPathID" name = "ApprovalPathID" value = "-1" />
										<input type = "hidden" id = "OfficeID" name = "OfficeID" value = "-1" />
										<div class="table-responsive" style="display: none;"
											id="tableData2Div">
											<table id="tableData2"
												class="table table-bordered table-striped">
												<thead>
													<tr>
	
	
														<th>Cancel</th>
														<th>Designation</th>
														<th>Order</th>
														<th width="20%">Role</th>
														<th>Days Allowed</th>
	
													</tr>
												</thead>
												<tbody id="tbodyData2">
												</tbody>
											</table>
	
	
										</div>
										
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-check-square-o"></i>Employees To Notify
											</div>
										</div>
	
										<div class="table-responsive">
											<table class="table table-bordered">
												<tbody id="employees_tbody">
	
	
	
												</tbody>
											</table>
										</div>
										<div class="form-actions text-center" id="button-div" style="display:none">
												<a class="btn btn-danger"
													href="<%=request.getHeader("referer")%>"> Cancel </a>
												<button class="btn btn-success" type="submit">
													Submit</button>
											</div>
										</div>
								</form>

							</div>
							<div class="portlet light col-md-5">
							
						</div>
					</div>
				
			</div>


		</div>
	</div>
</div>

<table id='tr-template' style='display: none'>
	<tr>
		<td class="deleterow" style="width: 30px; text-align: center;"><i
			class="fa fa-times"></i>
		</td>
		<td class="text-left">&nbsp;&nbsp;শেখ হাসিনা, প্রধানমন্ত্রী,
			প্রধানমন্ত্রীর কার্যালয়, প্রধানমন্ত্রীর কার্যালয়			
		</td>
		<td>
			<span class="up-down-link"> 
				<a class="up-link">
					<span>
						<i onclick="moveup(this)" class="fa fa-arrow-circle-up"></i>
					</span>
				</a>
				<a class="down-link">
					<span> 
						<i onclick="movedown(this)" class="fa fa-arrow-circle-down"></i>
					</span>
				</a>
				<input type = "hidden" name = "organogram_id_template" value =""></input>
			</span>
		</td>
		
		<td>
			<select class='form-control'  name='roleType_template'   >
			<%
				
				Options = CatDAO.getOptions(Language2, "approval_role", 0 );			
	
				out.print(Options);
			%>
			</select>
		</td>
		
		<td>
			<input class='form-control' type = "number" name = "days" value =""></input>
		</td>
	</tr>
</table>

<table id='tr-template-2' style='display: none'>
	<tr>
		<td class="deleterow" style="width: 30px; text-align: center;"><i
			class="fa fa-times"></i>
		</td>
		<td class="text-left">
				
		</td>
		
		
		
	</tr>
</table>



<script type="text/javascript">

var OfficeID = <%=SessionConstants.OFFICE_ID%>;

function assignOrganogram(designation, role, organogram_id)
{
	console.log("checkbox clicked");
	var cb_id = "approval_template_cb_" + organogram_id;
	var cb = document.getElementById(cb_id);
	if(cb.checked)
	{
		console.log('checked');
		addRealPath(organogram_id, designation, cb_id, role, "tbodyData2");		
	}
	else
	{
		console.log('unchecked');
		var tr_id = "tr_selected_" + organogram_id;
		removeElementByID(tr_id);
	}
}

function pathSelected()
{
	console.log("pathSelected called");
	
	
	var e = document.getElementById("pathSelect");
	var ApprovalPathID = e.options[e.selectedIndex].value;
	//getFlatChildren();
	
	//document.getElementById("pathDiv").style= "display: block;";
	var element = document.getElementById("template_path");
	
	document.getElementById('ApprovalPathID').value = ApprovalPathID;
	document.getElementById('OfficeID').value = OfficeID;
	

	//console.log("treeType = " + treeType);
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{
			if(this.responseText !='')
			{				
				element.innerHTML = this.responseText ;
				document.getElementById("tableData2Div").style= "display: block;";
				document.getElementById("button-div").style= "display: block;";
				
			}
			else
			{
				console.log("null response");
			}
		}
		else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	};
	
  var requestmsg = "Approval_pathServlet?actionType="
		  + "getPathDetails"
		  + "&Language=<%=Language2%>"
		  + "&OfficeID=" + OfficeID
		  + "&ApprovalPathID=" + ApprovalPathID;
  console.log("requestmsg = " + requestmsg);

  xhttp.open("POST", requestmsg, true);
  xhttp.send();	
	
}

function getPathDiv(OfficeID)
{
	console.log("pathDiv called");
	//getFlatChildren();
	
	document.getElementById("pathDiv").style= "display: block;";
	var element = document.getElementById("pathSelect");
	
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{
			if(this.responseText !='')
			{				
				element.innerHTML = this.responseText ;							
			}
			else
			{
				console.log("nul response");
			}
		}
		else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	};
	
  var requestmsg = "Approval_pathServlet?actionType="
		  + "getSelectByOfficeID"
		  + "&Language=<%=Language2%>"
		  + "&OfficeID=" + OfficeID
		  ;
  console.log("requestmsg = " + requestmsg);

  xhttp.open("POST", requestmsg, true);
  xhttp.send();	
	
}

function fillNextElementWithID(id, element)
{
	console.log("\n\nback in js, got element = " + element.getAttribute('id'));
	if(element.getAttribute('id') === "node_div_4")
	{
		console.log("\n\nInside = " + element.getAttribute('id'));
		var office_origins = document.getElementById("select_office_origins");
		var office_origins_id = office_origins.options[office_origins.selectedIndex].value;
		var office = document.getElementById("select_offices");
		var OfficeID = office.options[office.selectedIndex].value;
		console.log("got office_origins_id = " + office_origins_id + " OfficeID = " + OfficeID);
		



		getPathDiv(OfficeID);
		
	}
	
}

function checkbox_toggeled(id, text, cb_id, name, parent_id)
{
	var cb = document.getElementById(cb_id);
	console.log("id = " + id + ", text = " + text + " name = " + name + " parent_id = " + parent_id)
	if(cb.checked)
	{
		console.log('checked');
		addEmployee(id, text, cb_id, name, "employees_tbody");		
	}
	else
	{
		console.log('unchecked');
		var tr_id = "tr_employee_selected_" + id;
		removeElementByID(tr_id);
	}
}

function addRealPath(id, text, cb_id, name, tbodyID) {
    if (document.getElementById("td_delete_" + id) == null) {
        var tbody = document.getElementById(tbodyID);
        var template = document.getElementById('tr-template');
        var template_backup = template.innerHTML;


        var tr = template.rows[0];
        var tr_backup = tr;
        var tr_id = "tr_selected_" + id;
        tr.setAttribute("id", tr_id);
        var i = tr.querySelector("i");
        i.setAttribute("onclick", "removeElementByIDandUncheckCheckbox('" + tr_id + "', '" + cb_id + "')");

        var hiddeninput = tr.querySelector('input[type="hidden"]');
        hiddeninput.setAttribute("name", "organogram_id");
        hiddeninput.setAttribute("value", id);
        hiddeninput.setAttribute("id", "organogram_id_hidden_" + id);
        console.log("organogram_id_hidden = " + document.getElementById("organogram_id_hidden_" + id).value);

        var select = tr.querySelector("select");
        select.setAttribute("name", "roleType");


        var td_delete = tr.cells[0];
        td_delete.setAttribute("id", "td_delete_" + id);


        var td_text = tr.cells[1];
        if (name !== "") {
            td_text.innerHTML = text + ", " + name;
        }
        else {
            td_text.innerHTML = text;
        }

        template.innerHTML = template_backup;


        tbody.innerHTML += tr.outerHTML;
    }

}

function addEmployee(id, text, cb_id, name, tbodyID) {
    if (document.getElementById("td_delete_employee_" + id) == null) {
        var tbody = document.getElementById(tbodyID);
        var template = document.getElementById('tr-template-2');
        var template_backup = template.innerHTML;


        var tr = template.rows[0];
        var tr_backup = tr;
        var tr_id = "tr_employee_selected_" + id;
        tr.setAttribute("id", tr_id);
        var i = tr.querySelector("i");
        i.setAttribute("onclick", "removeElementByIDandUncheckCheckbox('" + tr_id + "', '" + cb_id + "')");

       
		
        var hiddenInput = "<input type ='hidden' name='employee_to_notify' value='" + id + "' />";
      


        var td_delete = tr.cells[0];
        td_delete.setAttribute("id", "td_delete_employee_" + id);


        var td_text = tr.cells[1];
        if (name !== "") {
            td_text.innerHTML = name + ", " + text + hiddenInput;
        }
        else {
            td_text.innerHTML = text + hiddenInput;
        }

        template.innerHTML = template_backup;


        tbody.innerHTML += tr.outerHTML;
    }

}

window.onload =function ()
{
	getPathDiv(OfficeID);
}
</script>
