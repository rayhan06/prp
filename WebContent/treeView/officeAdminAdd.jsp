<%@ page import="util.RecordNavigator" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    String servletType = request.getParameter("servletType");
    System.out.println(" servletType = " + servletType);
    String pageTitle = (String) request.getAttribute("pageTitle");

    LoginDTO localLoginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
%>

<input type='hidden' id='servletType' value='<%=servletType%>'/>
<div class="row" style="margin: 0 !important;">
    <div class="col-md-12" style="padding: 5px !important;">
        <div id="ajax-content">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption" style="font-weight: bold">
                        <%=(LM.getText(LC.OFFICE_ADMIN_LABEL, localLoginDTO))%>
                    </div>
                </div>
                <div class="portlet-window-body">
                    <div class="row">
                        <%@include file="dropDownNode.jsp" %>
                        <hr>
                    </div>
                    <div id="search_table" style="height: 100%" class="py-5">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    // region Variable

    let office_unit_organogram_id = 0;

    let office = 0;
    let ministry_id = 0;
    let office_layer = 0;
    let office_origin = 0;
    let pageNumber = 10;

    let officeId = 0;
    let officeUnitId = 0;

    // endregion Variable

    // region Update Office Head

    function submitHead() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (this.responseText !== '') {
                    showToast(success_message_bng, success_message_eng);
                }
                else {
                    showToast(failed_message_bng, failed_message_eng);
                }
            }
            else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Post", "OfficeAdminServlet?actionType=submit" +
            '&id=' + office_unit_organogram_id +
            '&officeId=' + officeId +
            '&officeUnitId=' + officeUnitId, true);
        xhttp.send();
    }

    function toggleCheckbox(id, _office, _officeUnitId) {
        office_unit_organogram_id = id;
        officeId = _office;
        officeUnitId = _officeUnitId;
        console.log("Office unit organogram id", id);
    }

    // endregion pdate Office Head

    // Where tree node is end
    function fillNextElementWithID(id, element) {

        ministry_id = document.getElementById("select_office_ministries").value;
        office_layer = document.getElementById("select_office_layers").value;
        office_origin = document.getElementById("select_office_origins").value;
        office = document.getElementById("select_offices").value;

        console.log('ministry_id: ', ministry_id, ' office_layer: ', office_layer, ' office_origin: ', office_origin, ' office: ', office);
        console.log('________message______________ ', element.getId);
        if (element.getAttribute('id') === "node_div_4") {

            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    if (this.responseText !== '') {
                        document.getElementById("search_table").innerHTML = '';
                        document.getElementById("search_table").innerHTML = this.responseText;

                        $('#tableData').DataTable({
                            dom: 'B',
                            "ordering": false,
                            buttons: [
                                {
                                    extend: 'excel',
                                    charset: 'UTF-8'
                                }, {
                                    extend: 'print',
                                    charset: 'UTF-8'
                                }
                            ]
                        });
                        document.getElementById('tableData_wrapper').classList.add("pull-right");
                        document.getElementById('tableData').classList.remove("no-footer");
                    }
                    else {
                    }
                }
                else if (this.readyState === 4 && this.status !== 200) {
                    alert('failed ' + this.status);
                }
            };

            xhttp.open("Get", "OfficeAdminServlet?actionType=search" + '&ministry_id=' + ministry_id + '&office_layer=' + office_layer + '&office_origin=' + office_origin + '&office=' + office, true);
            xhttp.send();
        }
    }

    // region Next Data

    function next(id, page_no) {

        ministry_id = document.getElementById("select_office_ministries").value;
        office_layer = document.getElementById("select_office_layers").value;
        office_origin = document.getElementById("select_office_origins").value;
        office = document.getElementById("select_offices").value;
        pageNumber = document.getElementById("pagenumber").value;

        console.log('ministry_id: ', ministry_id, ' office_layer: ', office_layer, ' office_origin: ', office_origin, ' office: ', office);

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (this.responseText !== '') {
                    document.getElementById("search_table").innerHTML = '';
                    document.getElementById("search_table").innerHTML = this.responseText;

                    $('#tableData').DataTable({
                        dom: 'Bfrti',
                        "ordering": false,
                        buttons: [
                            'copy', {
                                extend: 'csv',
                                charset: 'UTF-8',
                                bom: true
                            }, {
                                extend: 'excel',
                                charset: 'UTF-8'
                            }, {
                                extend: 'print',
                                charset: 'UTF-8'
                            }
                        ]
                    });
                    document.getElementById('tableData_wrapper').classList.add("pull-right");
                    document.getElementById('tableData').classList.remove("no-footer");
                }
                else {
                }
            }
            else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "OfficeAdminServlet?actionType=search" +
            '&ministry_id=' + ministry_id +
            '&office_layer=' + office_layer +
            '&office_origin=' + office_origin +
            '&office=' + office +
            '&page_number=' + pageNumber +
            '&pageno=' + page_no +
            '&id=' + id, true);
        xhttp.send();
    }

    // endregion Next Data

    // Not working, so skip
    function pageNumberChange(id) {
        var perPageElement = document.getElementById("pagenumber").value;

        ministry_id = document.getElementById("select_office_ministries").value;
        office_layer = document.getElementById("select_office_layers").value;
        office_origin = document.getElementById("select_office_origins").value;
        office = document.getElementById("select_offices").value;
        pageNumber = document.getElementById("pagenumber").value;

        console.log('ministry_id: ', ministry_id, ' office_layer: ', office_layer, ' office_origin: ', office_origin, ' office: ', office);

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (this.responseText !== '') {
                    document.getElementById("search_table").innerHTML = '';
                    document.getElementById("search_table").innerHTML = this.responseText;
                }
                else {
                    document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
                }
            }
            else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "OfficeAdminServlet?actionType=search" +
            '&ministry_id=' + ministry_id +
            '&office_layer=' + office_layer +
            '&office_origin=' + office_origin +
            '&office=' + office +
            '&page_number=' + pageNumber +
            '&id=' + id +
            '&RECORDS_PER_PAGE=' + perPageElement, true);
        xhttp.send();
    }

    // region Searching

    function dosubmit(params, id) {
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById('search_table').innerHTML = this.responseText;
            }
            else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "OfficeAdminServlet?actionType=search" +
            '&ministry_id=' + ministry_id +
            '&office_layer=' + office_layer +
            '&office_origin=' + office_origin +
            '&office=' + office +
            '&page_number=' + pageNumber +
            '&' + params, true);

        xhttp.send();
    }

    function anyfield_changed() {
        ministry_id = document.getElementById("select_office_ministries").value;
        office_layer = document.getElementById("select_office_layers").value;
        office_origin = document.getElementById("select_office_origins").value;
        office = document.getElementById("select_offices").value;
        pageNumber = document.getElementById("pagenumber").value;

        let searchKey = document.getElementById('anyfield').value;
        if (searchKey === null || searchKey.length === 0) searchKey = "";

        const params = 'AnyField=' + document.getElementById('anyfield').value + '&search=true&ajax=true&RECORDS_PER_PAGE=' + document.getElementById('pagenumber').value;
        dosubmit(params, null);
    }

    function allfield_changed() {
        let params = 'AnyField=' + document.getElementById('anyfield').value;
        params += ('&name_eng=' + document.getElementById('name_eng').value);
        params += ('&name_bng=' + document.getElementById('name_bng').value);
        params += ('&personal_mobile=' + document.getElementById('personal_mobile').value);
        params += ('&personal_email=' + document.getElementById('personal_email').value);

        params += '&search=true&ajax=true&RECORDS_PER_PAGE=' + document.getElementById('pagenumber').value;
        dosubmit(params);
    }

    // endregion Searching

</script>


