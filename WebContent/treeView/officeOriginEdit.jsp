<%@ page import="office_layers.Office_layersDAO" %>
<%@ page import="office_layers.Office_layersDTO" %>
<%@ page import="office_origins.Office_originsDTO" %>
<%@ page import="office_origins.Office_originsDAO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    String servletType = request.getParameter("servletType");
    String ID = request.getParameter("ID");

    if (ID != null) {
        try {
            Office_originsDTO office_originsDTO = new Office_originsDAO().getOffice_originsDTOByID(Long.parseLong(ID));
            request.setAttribute("office_originsDTO", office_originsDTO);
            //request.setAttribute("actionType", "editOfficeLayer");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    System.out.println(" servletType = " + servletType);
    String pageTitle = (String) request.getAttribute("pageTitle");
    LoginDTO localLoginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
%>
<input type='hidden' id='servletType' value='<%=servletType%>'/>
<div id="ajax-content">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <i class="fas fa-plus-square text-info"></i>
                    <%=(LM.getText(LC.OFFICE_ORIGIN_LABEL, localLoginDTO))%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row border-bottom">
                <%@include file="dropDownNode.jsp" %>
            </div>
            <div class="row pt-5">
                <jsp:include page="../office_origins/office_originsEditBody.jsp"/>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var global_office_origins_id = 0;

    function getOfficeOrigins() {
        console.log("after getting office units");
        getChildren(0, 0, "origin_unit_tree_panel", "getOriginUnit", "", global_office_origins_id, "", "");
    }

    function fillNextElementWithID(id, element) {
        console.log("\n\nback in js, got element = " + element.getAttribute('id'));
        if (element.getAttribute('id') === "node_div_4") {
            var office_origins = document.getElementById("select_office_origins");
            var office_origins_id = office_origins.options[office_origins.selectedIndex].value;
            var office = document.getElementById("select_offices");
            var office_id = office.options[office.selectedIndex].value;
            console.log("got office_origins_id = " + office_origins_id + " office_id = " + office_id);
            document.getElementById("office_unit_tree_panel").innerHTML = "";
            document.getElementById("office_unit_tree_panel").setAttribute("state", 0);
            document.getElementById("origin_unit_tree_panel").innerHTML = "";
            document.getElementById("origin_unit_tree_panel").setAttribute("state", 0);
            global_office_origins_id = office_origins_id;
            getChildren(0, 0, "office_unit_tree_panel", "getUnit", getOfficeOrigins, office_id, "", "");
        }
    }
</script>