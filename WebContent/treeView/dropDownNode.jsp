<%@ page language="java" %>
<%@ page import="java.util.*" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="treeView.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.CENTRE_EDIT_LANGUAGE, loginDTO);
    String actionType = request.getParameter("actionType");
    String servletType2 = request.getParameter("servletType");
    TreeDTO treeDTO = (TreeDTO) request.getAttribute("treeDTO");
    ArrayList<Integer> nodeIDs = (ArrayList<Integer>) request.getAttribute("nodeIDs");
    int parentID = Integer.parseInt(request.getParameter("parentID"));
    int myLayer = ((Integer) request.getAttribute("myLayer")).intValue();
    int maxLayer = treeDTO.parentChildMap.length;
    String geoName = treeDTO.parentChildMap[myLayer];
    String englishNameColumn = treeDTO.englishNameColumn[myLayer];
    String banglaNameColumn = treeDTO.banglaNameColumn[myLayer];
    String treeName = treeDTO.treeName;
    int checkBoxType = treeDTO.checkBoxType;
    int plusButtonType = treeDTO.plusButtonType;
    String showName = treeDTO.showNameMap[myLayer];
%>
<%
    if (myLayer == 0) {
%>
<div class="form-group col-md-3" id='node_div_0'>
    <%
        }
    %>
    <label><%=showName %>
    </label>
    <select class="form-control" id="select_<%=geoName%>" name="select_<%=geoName%>" required="required"
            onChange="getChildrenSelect('<%=myLayer + 1%>', 'select_<%=geoName%>', 'node_div_<%=myLayer + 1%>', '<%=actionType%>', null, <%=parentID%>,'')">
        <option value='-1'><%=LM.getText(LC.OFFICE_MANAGEMENT_SELECT, loginDTO)%>
        </option>
        <%
            for (int i = 0; i < nodeIDs.size(); i++) {
                int id = nodeIDs.get(i);
        %>
        <option value='<%=id %>'><%=GenericTree.getName(geoName, id, Language, englishNameColumn, banglaNameColumn) %>
        </option>
        <%
            }
        %>
    </select>
    <%
        if (myLayer == 0) {
    %>
</div>
<%
    }
%>
<%
    if (myLayer == 0) {
        for (int i = 1; i < maxLayer; i++) {
%>
<div class="col-md-3 form-group" id='node_div_<%=i%>'>
    <label><%=treeDTO.showNameMap[i]%>
    </label>
    <select class="form-control">
        <option value='-1'><%=LM.getText(LC.OFFICE_MANAGEMENT_SELECT, loginDTO)%>
        </option>
    </select>
</div>
<%
    }
%>
<div class="col-md-3 form-group" id='node_div_<%=maxLayer%>'></div>
<%
    }
%>

