<%@ page import="office_layers.Office_layersDAO" %>
<%@ page import="office_layers.Office_layersDTO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    String servletType = request.getParameter("servletType");
    String ID = request.getParameter("ID");

    if (ID != null) {
        try {
            Office_layersDTO office_layersDTO = new Office_layersDAO().getOffice_layersDTOByID(Long.parseLong(ID));
            request.setAttribute("office_layersDTO", office_layersDTO);
            //request.setAttribute("actionType", "editOfficeLayer");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    System.out.println(" servletType = " + servletType);
    String pageTitle = (String) request.getAttribute("pageTitle");
    LoginDTO localLoginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
%>
<input type='hidden' id='servletType' value='<%=servletType%>'/>
<div id="ajax-content">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">

                <h3 class="kt-portlet__head-title">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                         height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <path d="M8,9 L8,6.18181818 C8,4.76751186 8.76751186,4 10.1818182,4 L17.8181818,4 C19.2324881,4 20,4.76751186 20,6.18181818 L20,13.8181818 C20,15.2324881 19.2324881,16 17.8181818,16 L15,16 L15,18.8181818 C15,20.2324881 14.2324881,21 12.8181818,21 L5.18181818,21 C3.76751186,21 3,20.2324881 3,18.8181818 L3,11.1818182 C3,9.76751186 3.76751186,9 5.18181818,9 L8,9 Z"
                                  fill="#000000" fill-rule="nonzero"/>
                        </g>
                    </svg>
                    <%=(LM.getText(LC.OFFICE_LAYER_LABEL, localLoginDTO))%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row border-bottom">
                <%@include file="dropDownNode.jsp" %>
            </div>
            <div class="row pt-5">
                <div class="col-6">
                    <jsp:include page="../office_layers/office_layersEditBody.jsp"/>
                </div>
                <div class="col-6">
                    <div id="hide_div"
                         class="jstree jstree-2 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
                         role="tree" style="background: white" state="0">
                        <ul class="jstree-container-ul jstree-children" style="background: white"></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var global_office_origins_id = 0;

    function getOfficeOrigins() {
        console.log("after getting office units");
    }

    function editNode(layers, id) {
        console.log("MESSAGE ", layers, ' ------- ', id)
    }

    function newOriginUnitFound(id, element) {

    }

    function fillNextElementWithID(id, element) {
        console.log("\n\nback in js, got element = " + element.getAttribute('id'));
        if (element.getAttribute('id') === "node_div_2") {
            const ministry_id = document.getElementById("select_office_ministries").value;
            document.getElementById("hide_div").innerHTML = "";
            document.getElementById("hide_div").setAttribute("state", 0);
            let e;
            autoExpand = true;
            getChildren(0, ministry_id, "hide_div", "getOfficeLevel", null, 0, "", "");
        }
    }
</script>