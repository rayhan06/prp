<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    String servletType = request.getParameter("servletType");
    String pageTitle = request.getParameter("pageTitle");
    LoginDTO loginDTO2 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
%>
<input type='hidden' id='servletType' value='<%=servletType%>'/>
<div id="ajax-content">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                         height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <path d="M8,9 L8,6.18181818 C8,4.76751186 8.76751186,4 10.1818182,4 L17.8181818,4 C19.2324881,4 20,4.76751186 20,6.18181818 L20,13.8181818 C20,15.2324881 19.2324881,16 17.8181818,16 L15,16 L15,18.8181818 C15,20.2324881 14.2324881,21 12.8181818,21 L5.18181818,21 C3.76751186,21 3,20.2324881 3,18.8181818 L3,11.1818182 C3,9.76751186 3.76751186,9 5.18181818,9 L8,9 Z"
                                  fill="#000000" fill-rule="nonzero"/>
                        </g>
                    </svg>
                    <%=(LM.getText(LC.OFFICE_UNIT_ORGANOGRAM_MANAGEMENT_TITLE, loginDTO2))%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row border-bottom">
                <%@include file="../dropDownNode.jsp" %>
            </div>
            <div class="row">
                <div class="kt-portlet col-5">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <i class="fas fa-random text-info"></i>
                                <%=(LM.getText(LC.OFFICE_UNIT_ORGANOGRAM_MANAGEMENT_OFFICE_ORIGIN_UNIT_MANAGEMENT, loginDTO2))%>
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div id="origin_unit_tree_panel"
                             class="jstree jstree-1 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
                             role="tree" state="0">
                            <ul class="jstree-container-ul jstree-children"></ul>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet col-2">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                কার্যক্রম
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="d-flex flex-column w-50">
                            <button type="button" id="officeUnitTransfer" onclick="leftToRight()"
                                    class="btn btn-primary">
                                <i class="fas fa-arrow-right"></i>
                            </button>
                            <button type="button" id="office_unit_tree_delete" onclick="rightToLeft()"
                                    class="btn btn-primary">
                                <i class="fas fa-arrow-left"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet col-5">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <i class="fas fa-random text-info"></i>
                                <%=(LM.getText(LC.OFFICE_UNIT_ORGANOGRAM_MANAGEMENT_OFFICE_UNIT_ORGANOGRAM, loginDTO2))%>
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div id="office_unit_tree_panel"
                             class="jstree jstree-2 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
                             role="tree" state="0">
                            <ul class="jstree-container-ul jstree-children"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    let last_toggle_node = {};

    function getIDsToSend(divID) {
        let ids = ("");
        let unit_ids = ("");
        let organogram_ids = ("");

        $("#" + divID).find(":checkbox").each(function () {
            if ($(this).is(':checked')) {
                console.log("found, id = " + $(this).attr('id'));
                const tokens = $(this).attr('id').split('_');
                const id = tokens[tokens.length - 1];
                if (tokens[tokens.length - 2] === "organograms") {
                    organogram_ids += "&organogram_id=" + id;
                }
                else if (tokens[tokens.length - 2] === "units") {
                    unit_ids += "&unit_id=" + id;
                }
            }
        });
        ids = organogram_ids + unit_ids;
        return ids;
    }

    function leftToRight() {
        const office_id = $('#select_offices').val();
        const leftDiv = $("#origin_unit_tree_panel");
        const rightDiv = $("#office_unit_tree_panel");

        const ids = getIDsToSend("origin_unit_tree_panel");
        if (ids == null || ids.length === 0) {
            showToast("পদবি সিলেক্ট করুন", "Please select organogram first");
            return;
        }

        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                showToast(success_message_bng, success_message_eng);

                $('#OfficeOriginUnitOrganogramsTree_checkbox_office_origin_units_' + last_toggle_node.id).click();

                $("#office_unit_tree_panel").html('').attr("state", 0);
                getChildren(0, office_id, "office_unit_tree_panel", "getUnitForOfficeUnitOrganogramManagement", "", 0, "", "");
            }
        };

        xhttp.open("POST", "OfficeUnitOrganogramManagementServlet?actionType=addOrganogram&office_id=" + office_id + ids, true);
        xhttp.send();
    }

    function rightToLeft() {
        const office_id = $('#select_offices').val();
        const leftDiv = $("#origin_unit_tree_panel");
        const rightDiv = $("#office_unit_tree_panel");

        const ids = getIDsToSend("office_unit_tree_panel");
        if (ids == null || ids.length === 0) {
            showToast("পদবি সিলেক্ট করুন", "Please select organogram first");
            return;
        }

        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                showToast(success_message_bng, success_message_eng);

                $("#office_unit_tree_panel").html('').attr("state", 0);
                getChildren(0, office_id, "office_unit_tree_panel", "getUnitForOfficeUnitOrganogramManagement", "", 0, "", "");
            }
        };

        xhttp.open("POST", "OfficeUnitOrganogramManagementServlet?actionType=deleteOrganogram&office_id=" + office_id + ids, true);
        xhttp.send();
    }

    function checkbox_toggeled(id, text, cb_id, name, parent_id) {
        last_toggle_node = {
            id: id, text: text, cb_id: cb_id, name: name, parent_id: parent_id
        };

        const treeName = cb_id.split("_")[0];
        fillChildren(id, text, cb_id, name, parent_id);
    }

    let global_office_origins_id = 0;

    function newOriginUnitFound(id, element) {
    }

    function getOfficeOrigins() {
        getChildren(0, global_office_origins_id, "origin_unit_tree_panel", "getOriginUnitForOfficeUnitOrganogramManagement", "", 0, "", "");
    }

    function fillNextElementWithID(id, element) {
        if (element.getAttribute('id') === "node_div_4") {
            autoExpand = true;
            const office_origins_id = $("#select_office_origins").val();

            const office_id = $("#select_offices").val();
            console.log("got office_origins_id = " + office_origins_id + " office_id = " + office_id);

            $("#office_unit_tree_panel").html('').attr("state", 0);
            $("#origin_unit_tree_panel").html('').attr("state", 0);

            global_office_origins_id = office_origins_id;
            getChildren(0, office_id, "office_unit_tree_panel", "getUnitForOfficeUnitOrganogramManagement", getOfficeOrigins, 0, "", "");
        }
    }
</script>

<!-- Modal -->
<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p>Changes History</p>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    function onHelpClick(geo, id, text) {
        $('#myModal').modal('show');
    }
</script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>