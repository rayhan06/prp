<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    String servletType = request.getParameter("servletType");
    System.out.println(" servletType = " + servletType);
    String pageTitle = (String) request.getAttribute("pageTitle");
    LoginDTO localLoginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
%>
<input type='hidden' id='servletType' value='<%=servletType%>'/>
<div id="ajax-content">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                         height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <path d="M8,9 L8,6.18181818 C8,4.76751186 8.76751186,4 10.1818182,4 L17.8181818,4 C19.2324881,4 20,4.76751186 20,6.18181818 L20,13.8181818 C20,15.2324881 19.2324881,16 17.8181818,16 L15,16 L15,18.8181818 C15,20.2324881 14.2324881,21 12.8181818,21 L5.18181818,21 C3.76751186,21 3,20.2324881 3,18.8181818 L3,11.1818182 C3,9.76751186 3.76751186,9 5.18181818,9 L8,9 Z"
                                  fill="#000000" fill-rule="nonzero"/>
                        </g>
                    </svg>
                    <%=(LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_ADD_OFFICE_ORIGIN_UNIT_ORGANOGRAMS_ADD_FORMNAME, localLoginDTO))%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row border-bottom mb-3">
                <%@include file="../dropDownNode.jsp" %>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div id="tree_view"
                         class="jstree jstree-2 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
                         role="tree" state="0">
                        <ul class="jstree-container-ul jstree-children" style="background: white"></ul>
                    </div>
                </div>
                <div id="edit_page" class="col-md-6 col-sm-12" style="display: none">
                    <jsp:include
                            page="../../office_origin_unit_organograms/office_origin_unit_organogramsEditBody.jsp"/>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var office_ministry_id = 0;
    var office_layer_id = 0;
    var office_origin_id = 0;
    var upper_layer_id = 0;

    var add_or_edit = 0;

    function newOriginUnitFound(id, element) {
    }

    function editNode() {
    }

    let previous_element = null;

    function fillNextElementWithID(id, element) {
        console.log(id, ' ', element.getAttribute('id'));
        console.log("\n\nback in js, got element = " + element.getAttribute('id'));
        if (element.getAttribute('id') === "node_div_3") {
            previous_element = element;
            autoExpand = true;
            office_ministry_id = document.getElementById("select_office_ministries") ? document.getElementById("select_office_ministries").value : 0;
            office_layer_id = document.getElementById("select_office_layers") ? document.getElementById("select_office_layers").value : 0;
            office_origin_id = document.getElementById("select_office_origins") ? document.getElementById("select_office_origins").value : 0;

            document.getElementById("tree_view").innerHTML = "";
            document.getElementById("tree_view").setAttribute("state", 0);

            getChildren(0, 0, "tree_view", "getOfficeStuffAndOfficeDoptorSection", null, office_origin_id, "", "");
            getAllOfficeOriginUnit();
        }
    }

    // function extraLayerClick(showExtraLayerName, table_name, id, parentID, upperLayerId) {
    //     console.log('Message ', showExtraLayerName, ' ', table_name, ' ', id, ' parent id: ', upperLayerId, ' upperlayer id: ', upperLayerId);
    //     clearAllField();
    //     add_or_edit = 1;
    //     if (id < 0) id = 0;
    //     document.getElementById('edit_page').style.display = '';
    //     document.getElementById('officeOriginUnitId_0').value = upperLayerId;
    //     var xhttp = new XMLHttpRequest();
    //     xhttp.onreadystatechange = function () {
    //         if (this.readyState == 4 && this.status == 200) {
    //             console.log('message ', this.responseText);
    //             var data = JSON.parse(this.responseText);
    //             var d = document.getElementById('officeOriginUnitId_0').value = data.office_origin_unit_id;
    //             //var d = document.getElementById('officeOriginUnitId_0').value = id;
    //         }
    //         else if (this.readyState == 4 && this.status != 200) {
    //             alert('failed ' + this.status);
    //         }
    //     };
    // }

    // function getAllOfficeOriginUnit() {
    //     var xhttp = new XMLHttpRequest();
    //     xhttp.onreadystatechange = function () {
    //         if (this.readyState == 4 && this.status == 200) {
    //             console.log('message ', this.responseText);
    //             var data = JSON.parse(this.responseText);
    //             console.log('message ', data);
    //             var select = document.getElementById('superiorUnitId_select_0');
    //             select.innerHTML = '';
    //             var optional = document.createElement("option");
    //             optional.text = '-- select one --';
    //             select.add(optional);
    //             for (var i = 0; i < data.length; i++) {
    //                 var d = data[i];
    //                 var option = document.createElement("option");
    //                 option.text = d.unit_name_bng;
    //                 option.value = d.id;
    //                 select.add(option);
    //             }
    //         }
    //         else if (this.readyState == 4 && this.status != 200) {
    //             alert('failed ' + this.status);
    //         }
    //     };
    //     xhttp.open("Get", "Office_origin_unit_organogramsServlet?actionType=officeOriginUnits" +
    //         "&office_ministry_id=" + office_ministry_id +
    //         "&office_layer_id=" + office_layer_id +
    //         "&office_origin_id=" + office_origin_id
    //         , true);
    //     xhttp.send();
    // }

    // function upperLayerIdChange() {
    //     upper_layer_id = document.getElementById('superiorUnitId_select_0').value;
    //     console.log('message ', upper_layer_id);
    //     var xhttp = new XMLHttpRequest();
    //     xhttp.onreadystatechange = function () {
    //         if (this.readyState == 4 && this.status == 200) {
    //             console.log('message ', this.responseText);
    //             const data = this.responseText.length > 0 ? JSON.parse(this.responseText) : '';
    //             console.log('message ', data);
    //             const select = document.getElementById('superiorDesignationId_select_0');
    //
    //             select.innerHTML = '';
    //             const option = document.createElement("option");
    //             option.text = '-- select one --';
    //             option.value = null;
    //             select.add(option);
    //
    //             for (let i = 0; i < data.length; i++) {
    //                 const d = data[i];
    //                 const option = document.createElement("option");
    //                 option.text = d.designation_bng;
    //                 option.value = d.id;
    //                 select.add(option);
    //             }
    //             if (add_or_edit === 2) {
    //                 var r = edit_office_origin_unit_organogram.superior_designation_id;
    //                 //document.getElementById('superiorDesignationId_select_0').value = r;
    //                 $('#superiorDesignationId_select_0').select2().select2('val', edit_office_origin_unit_organogram.superior_designation_id);
    //             }
    //         }
    //         else if (this.readyState == 4 && this.status != 200) {
    //             alert('failed ' + this.status);
    //         }
    //     };
    //     xhttp.open("Get", "Office_origin_unit_organogramsServlet?actionType=officeOriginUnitOrganograms" +
    //         "&office_origin_unit_id=" + upper_layer_id
    //         , true);
    //     xhttp.send();
    // }

    // function plusClicked(table, id, column_name) {
    //     document.getElementById('edit_page').style.display = '';
    //     add_or_edit = 2;
    //     var xhttp = new XMLHttpRequest();
    //     xhttp.onreadystatechange = function () {
    //         if (this.readyState == 4 && this.status == 200) {
    //             console.log('message ', this.responseText);
    //             var data = JSON.parse(this.responseText);
    //             edit_office_origin_unit_organogram = data;
    //             document.getElementById('officeOriginUnitId_0').value = edit_office_origin_unit_organogram.office_origin_unit_id;
    //             //document.getElementById('superiorUnitId_select_0').value = edit_office_origin_unit_organogram.superior_unit_id;
    //             $('#superiorUnitId_select_0').select2().select2('val', edit_office_origin_unit_organogram.superior_unit_id);
    //             //document.getElementById('superiorDesignationId_select_0').value = edit_office_origin_unit_organogram.superior_designation_id;
    //             $('#superiorDesignationId_select_0').select2().select2('val', edit_office_origin_unit_organogram.superior_designation_id);
    //             document.getElementById('designationEng_text_0').value = edit_office_origin_unit_organogram.designation_eng;
    //             document.getElementById('designationBng_text_0').value = edit_office_origin_unit_organogram.designation_bng;
    //             document.getElementById('designationLevel_text_0').value = edit_office_origin_unit_organogram.designation_level;
    //             document.getElementById('designationSequence_text_0').value = edit_office_origin_unit_organogram.designation_sequence;
    //             document.getElementById('status_checkbox_0').checked = edit_office_origin_unit_organogram.status;
    //         }
    //         else if (this.readyState == 4 && this.status != 200) {
    //             alert('failed ' + this.status);
    //         }
    //     };
    //     xhttp.open("Get", "Office_origin_unit_organogramsServlet?actionType=officeOriginUnitOrganogramsSingle&id=" + id, true);
    //     xhttp.send();
    // }

    function clearAllField() {
        document.getElementById('officeOriginUnitId_0').value = 0;
        document.getElementById('superiorUnitId_select_0').value = 0;
        document.getElementById('superiorDesignationId_select_0').value = 0;
        document.getElementById('designationEng_text_0').value = '';
        document.getElementById('designationBng_text_0').value = '';
        document.getElementById('designationLevel_text_0').value = 0;
        document.getElementById('designationSequence_text_0').value = 0;
        document.getElementById('status_checkbox_0').checked = false;
    }

    // function addNewOrEdit(row, validate) {
    //     try {
    //
    //
    //         var office_origin_unit_id = document.getElementById('officeOriginUnitId_0').value;
    //         var superior_unit_id = document.getElementById('superiorUnitId_select_0').value;
    //         var superior_designation_id = document.getElementById('superiorDesignationId_select_0').value;
    //         var designation_eng = document.getElementById('designationEng_text_0').value;
    //         var designation_bng = document.getElementById('designationBng_text_0').value;
    //         var designation_level = document.getElementById('designationLevel_text_0').value;
    //         var designation_sequence = document.getElementById('designationSequence_text_0').value;
    //         var status = document.getElementById('status_checkbox_0').checked;
    //
    //         if (parseInt(office_origin_unit_id) == 0) {
    //             showError('শাখা সিলেক্ট করুন, অফিস ষ্টাফ এর প্লাস বাটনে ক্লিক দিন', 'Click plus button to select unit');
    //             return;
    //         }
    //
    //         if (!checkValidity(office_origin_unit_id,
    //             superior_unit_id,
    //             superior_designation_id,
    //             designation_eng,
    //             designation_bng,
    //             designation_level,
    //             designation_sequence)) return;
    //
    //         if (!tryParseInt(superior_unit_id)) {
    //             superior_unit_id = 0;
    //         }
    //         if (!tryParseInt(superior_designation_id)) {
    //             superior_designation_id = 0;
    //         }
    //
    //         var xhttp = new XMLHttpRequest();
    //         xhttp.onreadystatechange = function () {
    //             if (this.readyState == 4 && this.status == 200) {
    //                 console.log('message ', this.responseText);
    //                 showToast(success_message_bng, success_message_eng);
    //                 clearAllField();
    //                 fillNextElementWithID(1, previous_element);
    //             }
    //             else if (this.readyState === 4 && this.status !== 200) {
    //                 alert('failed ' + this.status);
    //             }
    //         };
    //         xhttp.open("Post", "Office_origin_unit_organogramsServlet?actionType=" + (add_or_edit === 1 ? "add" : "edit") +
    //             "&identity=" + edit_office_origin_unit_organogram.id +
    //             "&officeOriginUnitId=" + office_origin_unit_id +
    //             "&superiorUnitId=" + superior_unit_id +
    //             "&superiorDesignationId=" + superior_designation_id +
    //             "&designationEng=" + designation_eng +
    //             "&designationBng=" + designation_bng +
    //             "&designationLevel=" + designation_level +
    //             "&designationSequence=" + designation_sequence +
    //             "&status=" + status
    //             , true);
    //         xhttp.send();
    //     } catch (e) {
    //         showError(fill_all_input_bng, fill_all_input_eng);
    //     }
    // }

    window.onload = function () {
    }
</script>

<script>
    function checkValidity(x0, x1, x2, x3, x4, x5, x6, x7, x8, x9) {
        console.log('validity: ', x0, x1, x2, x3, x4, x5, x6, x7, x8, x9);
        if (tryParseInt(x1) && !tryParseInt(x2)) {
            showError('উপরের স্তরের উপাধি দিন ', 'Please provide superior designation');
            return false;
        }

        if (x0 <= 0 || x3 == '' || x4 == '' || x5 == 0 || x6 <= 0) {
            showError(fill_all_input_bng, fill_all_input_eng);
            return false;
        }
        return true;
    }
</script>

