<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    String servletType = request.getParameter("servletType");
    String pageTitle = request.getParameter("pageTitle");
    LoginDTO loginDTO4 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO1 = UserRepository.getInstance().getUserDtoByUserId(loginDTO4.userID);
%>
<input type='hidden' id='servletType' value='<%=servletType%>'/>
<div class="kt-portlet">
    <div id="ajax-content">
        <div class="kt-portlet kt-portlet--responsive-tablet-and-mobile py-3">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M10.5,5 L19.5,5 C20.3284271,5 21,5.67157288 21,6.5 C21,7.32842712 20.3284271,8 19.5,8 L10.5,8 C9.67157288,8 9,7.32842712 9,6.5 C9,5.67157288 9.67157288,5 10.5,5 Z M10.5,10 L19.5,10 C20.3284271,10 21,10.6715729 21,11.5 C21,12.3284271 20.3284271,13 19.5,13 L10.5,13 C9.67157288,13 9,12.3284271 9,11.5 C9,10.6715729 9.67157288,10 10.5,10 Z M10.5,15 L19.5,15 C20.3284271,15 21,15.6715729 21,16.5 C21,17.3284271 20.3284271,18 19.5,18 L10.5,18 C9.67157288,18 9,17.3284271 9,16.5 C9,15.6715729 9.67157288,15 10.5,15 Z"
                                      fill="#000000"/>
                                <path d="M5.5,8 C4.67157288,8 4,7.32842712 4,6.5 C4,5.67157288 4.67157288,5 5.5,5 C6.32842712,5 7,5.67157288 7,6.5 C7,7.32842712 6.32842712,8 5.5,8 Z M5.5,13 C4.67157288,13 4,12.3284271 4,11.5 C4,10.6715729 4.67157288,10 5.5,10 C6.32842712,10 7,10.6715729 7,11.5 C7,12.3284271 6.32842712,13 5.5,13 Z M5.5,18 C4.67157288,18 4,17.3284271 4,16.5 C4,15.6715729 4.67157288,15 5.5,15 C6.32842712,15 7,15.6715729 7,16.5 C7,17.3284271 6.32842712,18 5.5,18 Z"
                                      fill="#000000" opacity="0.3"/>
                            </g>
                        </svg>
                        <%=(LM.getText(LC.OFFICE_UNIT_MANAGEMENT_TITLE, loginDTO4))%>
                    </h3>
                </div>
            </div>
        </div>
        <input type='hidden' id='office_id' value='<%=userDTO1.officeID%>'/>
        <div class="kt-portlet__body">
            <div class="row" style="display: none">
                <%@include file="../dropDownNode.jsp" %>
            </div>
            <div class="row clearfix">
                <div class="col-md-6 kt-portlet">
                    <div class="kt-portlet__body pl-0">
                    <div class="row">
			    <div class="form-group col-md-3"></div>
			    <div class="form-group col-md-6">
					<select class="form-control" id="search_office_select" name="<%= LM.getText(LC.CENTRE_EDIT_LANGUAGE, loginDTO)%>" required="required" onchange="searchOffice('search_office_select')">

					</select>

				</div>
			    </div>
                        <div id="office_unit_tree_panel"
                             class="jstree jstree-2 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
                             role="tree" state="0">
                            <ul class="jstree-container-ul jstree-children"></ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 kt-portlet">
                    <div id="edit_panel"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var previousSearchedElementID;

    function editNode(table_name, id) {
        if (table_name === 'offices') return;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (this.responseText !== '') {
                    document.getElementById('edit_panel').innerHTML = this.responseText;
                    select_2_call();
                }
                else {
                    console.log("nul response");
                }
            }
            else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        let servlet = document.getElementById('servletType').value;
        xhttp.open("Get", "Office_unitsServlet?actionType=getEditPage&getBodyOnly=true&ID=" + id, true);
        xhttp.send();
    }

    function getAllOfficeUnits(office_id) {
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
            	var select = document.getElementById('search_office_select');
            	select.innerHTML = "";
            	console.log(select.name);
                JSON.parse(this.responseText).forEach(function (val) {
                    var html = "";
                    if(select.name == "Bangla"){
                        html = "<option value=" + val.id +">"+ val.unit_name_bng +"</option>";
                    }else {
                        html = "<option value=" + val.id +">"+ val.unit_name_eng +"</option>";
                    }
                    document.getElementById('search_office_select').innerHTML += html;
                })
            }
        };
        xhttp.open("Get", "UnitNameUpdateServlet?actionType=getUnits&officeId=" + office_id, true);
        xhttp.send();
    
}
    
    function searchOffice(element_id, language) {
		var select = document.getElementById(element_id);
		if(select != null){
			var selectedOfficeUnit = select.options[select.selectedIndex].value;
			console.log(selectedOfficeUnit);
			if(previousSearchedElementID != null){
				document.getElementById(previousSearchedElementID).style.backgroundColor = "#FFFFFF";
			}
			previousSearchedElementID = "tree_node_" + selectedOfficeUnit;
			var node = document.getElementById(previousSearchedElementID);
			if(node != null){
				node.style.backgroundColor = "#FDFF47";
				var rect = node.getBoundingClientRect();
				window.scrollTo({
					  top: (rect.top-200),
					  left: 0,
					  behavior: 'smooth'
					});
			}
			
		}
	}
    
    function init() {
        let office_id = document.getElementById('office_id').value;
        document.getElementById("office_unit_tree_panel").innerHTML = "";
        document.getElementById("office_unit_tree_panel").setAttribute("state", 0);
        autoExpand = true;
        getAllOfficeUnits(office_id);
        getChildren(0, office_id, "office_unit_tree_panel", "getOfficeUnitsTree", null, 0, "", "", "");
    }

    window.onload = function () {
        init();
    };

    function newOriginUnitFound(id, element) {
    }
</script>
