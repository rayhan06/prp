<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    String servletType = request.getParameter("servletType");
    String pageTitle = request.getParameter("pageTitle");
    LoginDTO loginDTO2 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
%>
<input type='hidden' id='servletType' value='<%=servletType%>'/>
<div class="kt-portlet">
    <div id="ajax-content">
        <div class="kt-portlet kt-portlet--responsive-tablet-and-mobile py-3">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M8.43296491,7.17429118 L9.40782327,7.85689436 C9.49616631,7.91875282 9.56214077,8.00751728 9.5959027,8.10994332 C9.68235021,8.37220548 9.53982427,8.65489052 9.27756211,8.74133803 L5.89079566,9.85769242 C5.84469033,9.87288977 5.79661753,9.8812917 5.74809064,9.88263369 C5.4720538,9.8902674 5.24209339,9.67268366 5.23445968,9.39664682 L5.13610134,5.83998177 C5.13313425,5.73269078 5.16477113,5.62729274 5.22633424,5.53937151 C5.384723,5.31316892 5.69649589,5.25819495 5.92269848,5.4165837 L6.72910242,5.98123382 C8.16546398,4.72182424 10.0239806,4 12,4 C16.418278,4 20,7.581722 20,12 C20,16.418278 16.418278,20 12,20 C7.581722,20 4,16.418278 4,12 L6,12 C6,15.3137085 8.6862915,18 12,18 C15.3137085,18 18,15.3137085 18,12 C18,8.6862915 15.3137085,6 12,6 C10.6885336,6 9.44767246,6.42282109 8.43296491,7.17429118 Z"
                                      fill="#000000" fill-rule="nonzero"/>
                            </g>
                        </svg>
                        <%=(LM.getText(LC.OFFICE_INFORMATION_EDIT_TITLE, loginDTO2))%>
                    </h3>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row">
                <%@include file="../dropDownNode.jsp" %>
            </div>
            <div id="edit_office_info" style="display: none">
                <div class="form-group row">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label><%=(LM.getText(LC.OFFICE_INFORMATION_EDIT_DIVISION, loginDTO2))%>
                        </label>
                        <select id="select_geo_divisions" class="form-control"
                                onchange="divisionChange()"></select>
                        <span style="width: 15px"></span>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label><%=(LM.getText(LC.OFFICE_INFORMATION_EDIT_DISTRICT, loginDTO2))%>
                        </label>
                        <select id="select_geo_districts" class="form-control"
                                onchange="districtChange()"></select>
                        <span style="width: 15px"></span>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label><%=(LM.getText(LC.OFFICE_INFORMATION_EDIT_UPAZILA, loginDTO2))%>
                        </label>
                        <select id="select_geo_upazilas" class="form-control"
                                onchange="upazilaChange()"></select>
                        <span style="width: 15px"></span>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <label><%=(LM.getText(LC.OFFICE_INFORMATION_EDIT_UNION, loginDTO2))%>
                        </label>
                        <select id="select_geo_unions" class="form-control"
                                onchange="unionChange()"></select>
                    </div>
                </div>
                <div id="office_edit_body">
                    <jsp:include page="../../offices/officesEditBody.jsp"/>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var office_ministries_id;
    var office_layers_id;
    var office_origins_id;
    var offices_id;
    var geoDivisionId_hidden_0;
    var geoDistrictId_hidden_0;
    var geoUpazilaId_hidden_0;
    var geoUnionId_hidden_0;
    var division_bbs_code;
    var district_bbs_code;
    var upazila_bbs_code;
    var union_bbs_code;

    function fillNextElementWithID(id, element) {
        console.log("\n\nback in js, got element = " + element.getAttribute('id'));
        if (element.getAttribute('id') === "node_div_4") {
            document.getElementById('edit_office_info').style.display = '';
            office_ministries_id = document.getElementById('select_office_ministries').value;
            office_layers_id = document.getElementById('select_office_layers').value;
            office_origins_id = document.getElementById('select_office_origins').value;
            offices_id = document.getElementById('select_offices').value;
            getOfficeInformationByOfficeId(offices_id);
        }
    }

    function getOfficeInformationByOfficeId(office_id) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById('office_edit_body').innerHTML = this.responseText;
                select_2_call();
                geoDivisionId_hidden_0 = document.getElementById('geoDivisionId_hidden_0').value;
                geoDistrictId_hidden_0 = document.getElementById('geoDistrictId_hidden_0').value;
                geoUpazilaId_hidden_0 = document.getElementById('geoUpazilaId_hidden_0').value;
                geoUnionId_hidden_0 = document.getElementById('geoUnionId_hidden_0').value;
                getDivision(geoDivisionId_hidden_0);
                getDistrict(geoDivisionId_hidden_0, geoDistrictId_hidden_0);
                getUpazila(geoDistrictId_hidden_0, geoUpazilaId_hidden_0);
                getUnion(geoUpazilaId_hidden_0, geoUnionId_hidden_0);
            }
        };
        xhttp.open("Get", "OfficesServlet?actionType=getEditPage&ID=" + office_id, true);
        xhttp.send();
    }

    window.onload = function () {
        var parm = location.search.split('success=')[1] ? location.search.split('success=')[1] : false;
        if (parm) {
            history.pushState({}, null, location.search.split('&')[0]);
            showToast(success_message_bng, success_message_eng);
        }
    };

    function getDivision(select_id) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            document.getElementById('select_geo_divisions').innerHTML = null;
            if (this.readyState === 4 && this.status === 200) {
                JSON.parse(this.responseText).forEach(function (val) {
                    division_bbs_code = val.bbs_code;
                    var select = document.getElementById('select_geo_divisions');
                    select.options[select.options.length] = new Option(val.division_name_bng, val.id);
                });
                //document.getElementById('select_geo_divisions').value = select_id;
                $("#select_geo_divisions").select2().select2("val", select_id);
            }
        };
        xhttp.open("Get", "GeoRestServlet?actionType=getDivision", true);
        xhttp.send();
    }

    function divisionChange() {
        getDistrict(document.getElementById('select_geo_divisions').value, geoDistrictId_hidden_0);
    }

    function getDistrict(division_id, select_id) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            document.getElementById('select_geo_districts').innerHTML = null;
            if (this.readyState === 4 && this.status === 200) {
                JSON.parse(this.responseText).forEach(function (val) {
                    district_bbs_code = val.bbs_code;
                    var select = document.getElementById('select_geo_districts');
                    select.options[select.options.length] = new Option(val.district_name_bng, val.id);
                });
                //document.getElementById('select_geo_districts').value = select_id;
                $("#select_geo_districts").select2().select2("val", select_id);
            }
        };
        xhttp.open("Get", "GeoRestServlet?actionType=getDistrict&id=" + division_id, true);
        xhttp.send();
    }

    function districtChange() {
        getUpazila(document.getElementById('select_geo_districts').value, geoUpazilaId_hidden_0);
    }

    function getUpazila(district_id, select_id) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            document.getElementById('select_geo_upazilas').innerHTML = null;
            if (this.readyState === 4 && this.status === 200) {
                JSON.parse(this.responseText).forEach(function (val) {
                    upazila_bbs_code = val.bbs_code;
                    var select = document.getElementById('select_geo_upazilas');
                    select.options[select.options.length] = new Option(val.upazila_name_bng, val.id);
                });
                //document.getElementById('select_geo_upazilas').value = select_id;
                $("#select_geo_upazilas").select2().select2("val", select_id);
            }
        };
        xhttp.open("Get", "GeoRestServlet?actionType=getUpazila&id=" + district_id, true);
        xhttp.send();
    }

    function upazilaChange() {
        getUnion(document.getElementById('select_geo_upazilas').value, geoUnionId_hidden_0);
    }

    function getUnion(upazila_id, select_id) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById('select_geo_unions').innerHTML = null;
                JSON.parse(this.responseText).forEach(function (val) {
                    union_bbs_code = val.bbs_code;
                    var select = document.getElementById('select_geo_unions');
                    select.options[select.options.length] = new Option(val.union_name_bng, val.id);
                });
                //document.getElementById('select_geo_unions').value = select_id;
                $("#select_geo_unions").select2().select2("val", select_id);
            }
        };
        xhttp.open("Get", "GeoRestServlet?actionType=getUnion&id=" + upazila_id, true);
        xhttp.send();
    }
</script>