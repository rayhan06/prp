<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    String servletType = request.getParameter("servletType");
    String pageTitle = request.getParameter("pageTitle");
    LoginDTO loginDTO3 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
%>
<input type='hidden' id='servletType' value='<%=servletType%>'/>
<div class="kt-portlet">
    <div id="ajax-content">
        <div class="kt-portlet kt-portlet--responsive-tablet-and-mobile py-3">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M15.1231569,19.0111815 L7.83785094,14.818972 C8.31992102,14.3336937 8.67836566,13.7254559 8.86199856,13.0454449 L16.0980947,17.246999 C15.6352738,17.7346932 15.2940944,18.3389541 15.1231569,19.0111815 Z M7.75585639,9.10080708 L15.0774983,4.78750147 C15.2169157,5.48579221 15.5381369,6.11848298 15.9897205,6.63413231 L8.86499752,10.9657252 C8.67212677,10.2431476 8.28201274,9.60110795 7.75585639,9.10080708 Z"
                                      fill="#000000" fill-rule="nonzero"/>
                                <circle fill="#000000" opacity="0.3" cx="19" cy="4" r="3"/>
                                <circle fill="#000000" opacity="0.3" cx="19" cy="20" r="3"/>
                                <circle fill="#000000" opacity="0.3" cx="5" cy="12" r="3"/>
                            </g>
                        </svg>
                        <%=(LM.getText(LC.OFFICE_ORGANOGRAM_TRANSFER_TITLE, loginDTO3))%>
                    </h3>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div div="row">
                <%@include file="../dropDownNode.jsp" %>
            </div>
            <div class="row clearfix">
                <div class="col-md-6 kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <i class="fas fa-random text-info"></i>
                                <%=(LM.getText(LC.OFFICE_ORGANOGRAM_TRANSFER_LEFT, loginDTO3))%>
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body pl-0">
                        <div id="office_unit_tree_panel"
                             class="jstree jstree-2 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
                             role="tree" state="0">
                            <ul class="jstree-container-ul jstree-children"></ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 kt-portlet">
                    <div class="kt-portlet__head border-bottom-0 px-1">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <%=(LM.getText(LC.PROTIKOLPO_SETUP_ACTIVITY, loginDTO3))%>
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body px-1">
                        <button type="button" id="officeUnitTransfer" onclick="leftToRight()"
                                class="btn btn-primary btn-hover-brand btn-square">
                            <i class="fa fa-hand-o-right"></i>
                        </button>
                    </div>
                </div>
                <div class="col-md-5 kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <i class="fas fa-random text-info"></i>
                                <%=(LM.getText(LC.OFFICE_ORGANOGRAM_TRANSFER_RIGHT, loginDTO3))%>
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body pl-0">
                        <div>
                            <select class="form-control" id="units" style="min-width: 250px"
                                    onchange="rightSideOfficeUnitChange(this.value)"></select>
                        </div>
                        <div id="right_side_unit_tree_panel"
                             class="jstree jstree-2 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
                             role="tree" state="0">
                            <ul class="jstree-container-ul jstree-children"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="snackbar">Success</div>
<script type="text/javascript">
    var office_ministries_id;
    var office_layers_id;
    var office_origins_id;
    var offices_id;
    var selected_unit;

    function getIDsToSend(divID) {
        var ids = "";
        $("#" + divID).find(":checkbox").each(function () {
            if ($(this).is(':checked')) {
                console.log("found, id = " + $(this).attr('id'));
                var id = $(this).attr('data');
                ids += "__" + id;
            }
        });
        var arr = [];
        var first = ids.split("__");
        first.forEach(function (value) {
            var temp = [];
            var second = value.split('_');
            for (var i = 1; i < second.length; i++) {
                temp.push(second[i]);
            }
            if (temp.length > 0 && temp.length % 4 == 0) {
                for (var j = 1; j < temp.length; j += 2) {
                    arr.push(temp[j]);
                }
            }
        });
        console.log('after split: ', arr);
        return arr;
    }

    function leftToRight() {
        var ids = getIDsToSend("office_unit_tree_panel");
        console.log("ids to send : " + ids);

        if (ids == null || ids.length === 0) {
            showError('অনুগ্রহ করে পদবি সিলেক্ট করুন', 'Please select organogram first');
            return;
        }

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                showToast(success_message_bng, success_message_eng);

                document.getElementById("office_unit_tree_panel").innerHTML = "";
                document.getElementById("office_unit_tree_panel").setAttribute("state", 0);
                autoExpand = true;
                getChildren(0, offices_id, "office_unit_tree_panel", "getOfficeUnitAndOfficeUnitOrganogramTransfer", null, 0, "", "");
                rightSideOfficeUnitChange(selected_unit);
            }
        };
        var parm = "";
        ids.forEach(function (value) {
            parm += ('&id=' + value);
        });
        var newOfficeUnit = document.getElementById('units').value;
        xhttp.open("Post", "OfficeOrganogramTransferServlet?actionType=transfer&newOfficeUnit=" + newOfficeUnit + parm, true);
        xhttp.send();
    }

    function fillNextElementWithID(id, element) {
        console.log("\n\nback in js, got element = " + element.getAttribute('id'));
        if (element.getAttribute('id') === "node_div_4") {
            office_ministries_id = document.getElementById('select_office_ministries').value;
            office_layers_id = document.getElementById('select_office_layers').value;
            office_origins_id = document.getElementById('select_office_origins').value;
            offices_id = document.getElementById('select_offices').value;
            document.getElementById("office_unit_tree_panel").innerHTML = "";
            document.getElementById("office_unit_tree_panel").setAttribute("state", 0);
            getUnits(offices_id);
            autoExpand = true;
            selected_office = offices_id;
            getChildren(0, offices_id, "office_unit_tree_panel", "getOfficeUnitAndOfficeUnitOrganogramTransfer", null, 0, "", "");
        }
    }

    function getUnits(offices_id) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            document.getElementById('units').innerHTML = null;
            if (this.readyState === 4 && this.status === 200) {
                selected_unit = 0;
                JSON.parse(this.responseText).forEach(function (val) {
                    if (selected_unit === 0) selected_unit = val.id;
                    var select = document.getElementById('units');
                    select.options[select.options.length] = new Option(val.unit_name_bng, val.id);
                });
                document.getElementById("right_side_unit_tree_panel").innerHTML = "";
                document.getElementById("right_side_unit_tree_panel").setAttribute("state", 0);
                autoExpand = true;
                getChildren(0, selected_unit, "right_side_unit_tree_panel", "officeUnitOrganogramOnly", null, 0, "", "");
            }
        };
        xhttp.open("Get", "OfficeOrganogramTransferServlet?actionType=getOfficeUnit&id=" + offices_id, true);
        xhttp.send();
    }

    function rightSideOfficeUnitChange(unit_id) {
        document.getElementById("right_side_unit_tree_panel").innerHTML = "";
        document.getElementById("right_side_unit_tree_panel").setAttribute("state", 0);
        autoExpand = true;
        selected_unit = unit_id;
        getChildren(0, unit_id, "right_side_unit_tree_panel", "officeUnitOrganogramOnly", null, 0, "", "");
    }

    function newOriginUnitFound(id, element) {
    }

    function checkbox_toggeled(id, text, cb_id, name, parent_id) {
        var treeName = cb_id.split("_")[0];
        console.log("treeName = " + treeName);
        var ids = fillChildren(id, text, cb_id, name, parent_id);
        var text = "";
        for (i = 0; i < ids.length; i++) {
            text += ids[i] + " ";
        }
        console.log("ids: " + text);
    }
</script>
<style>
    #snackbar {
        visibility: hidden;
        min-width: 500px;
        background-color: #00a65a;
        color: #fff;
        text-align: center;
        padding: 16px;
        position: fixed;
        z-index: 1;
        left: 40%;
        bottom: 30px;
    }

    #snackbar.show {
        visibility: visible;
        -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }

    @-webkit-keyframes fadein {
        from {
            bottom: 0;
            opacity: 0;
        }
        to {
            bottom: 30px;
            opacity: 1;
        }
    }

    @keyframes fadein {
        from {
            bottom: 0;
            opacity: 0;
        }
        to {
            bottom: 30px;
            opacity: 1;
        }
    }

    @-webkit-keyframes fadeout {
        from {
            bottom: 30px;
            opacity: 1;
        }
        to {
            bottom: 0;
            opacity: 0;
        }
    }

    @keyframes fadeout {
        from {
            bottom: 30px;
            opacity: 1;
        }
        to {
            bottom: 0;
            opacity: 0;
        }
    }
</style>