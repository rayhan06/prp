<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    String servletType = request.getParameter("servletType");
    String pageTitle = request.getParameter("pageTitle");
    LoginDTO loginDTO2 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
%>
<input type='hidden' id='servletType' value='<%=servletType%>'/>
<div id="ajax-content">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                         height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <path d="M8,9 L8,6.18181818 C8,4.76751186 8.76751186,4 10.1818182,4 L17.8181818,4 C19.2324881,4 20,4.76751186 20,6.18181818 L20,13.8181818 C20,15.2324881 19.2324881,16 17.8181818,16 L15,16 L15,18.8181818 C15,20.2324881 14.2324881,21 12.8181818,21 L5.18181818,21 C3.76751186,21 3,20.2324881 3,18.8181818 L3,11.1818182 C3,9.76751186 3.76751186,9 5.18181818,9 L8,9 Z"
                                  fill="#000000" fill-rule="nonzero"/>
                        </g>
                    </svg>
                    <%=(LM.getText(LC.OFFICE_UNIT_MANAGEMENT_TITLE, loginDTO2))%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row border-bottom">
                <%@include file="../dropDownNode.jsp" %>
            </div>
            <div class="row">
                <div class="kt-portlet col-5">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <i class="fas fa-random text-info"></i>
                                <%=(LM.getText(LC.OFFICE_UNIT_MANAGEMENT_OFFICE_ORIGIN_UNIT, loginDTO2))%>
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div
                                class="jstree jstree-1 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
                                id="origin_unit_tree_panel" role="tree" state="0">
                            <ul class="jstree-container-ul jstree-children"></ul>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet col-2">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <%=(LM.getText(LC.PROTIKOLPO_SETUP_ACTIVITY, loginDTO2))%>
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="d-flex flex-column w-50">
                            <button type="button" id="officeUnitTransfer" onclick="leftToRight()"
                                    class="btn btn-primary">
                                <i class="fas fa-arrow-right"></i>
                            </button>
                            <button type="button" id="office_unit_tree_delete" onclick="rightToLeft()"
                                    class="btn btn-primary">
                                <i class="fas fa-arrow-left"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet col-5">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <i class="fas fa-random text-info"></i>
                                <%=(LM.getText(LC.OFFICE_UNIT_MANAGEMENT_OFFICE_UNIT, loginDTO2))%>
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div id="office_unit_tree_panel"
                             class="jstree jstree-2 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
                             role="tree" state="0">
                            <ul class="jstree-container-ul jstree-children"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    let last_toggle_tree = {};

    function checkbox_toggeled(id, text, cb_id, name, parent_id) {
        last_toggle_tree = {
            id: id,
            text: text,
            cb_id: cb_id,
            name: name,
            parent_id: parent_id
        };

        const treeName = cb_id.split("_")[0];
        const ids = fillChildren(id, text, cb_id, name, parent_id);
    }

    function getLeftTree() {
        autoExpand = true;
        const office_origins_id = $('#select_office_origins').val();
        $("#origin_unit_tree_panel").html('').attr("state", 0);
        getChildren(0, 0, "origin_unit_tree_panel", "getOfficeOriginAsLeft", "", office_origins_id, "", "");
    }

    function getRightTree() {
        autoExpand = true;
        const office_id = $('#select_offices').val();
        $("#office_unit_tree_panel").html('').attr("state", 0);
        getChildren(0, 0, "office_unit_tree_panel", "getOfficeOriginAsRight", null, office_id, null, "", "");
    }

    function fillNextElementWithID(id, element) {

        if (element.getAttribute('id') === "node_div_3") {
            getLeftTree();
        } else if (element.getAttribute('id') === "node_div_4") {
            getRightTree();
        }
    }

    function getIDsToSend(divID) {
        let ids = ("");
        $("#" + divID).find(":checkbox").each(function () {
            if ($(this).is(':checked')) {
                console.log("found, id = " + $(this).attr('id'));
                const id = $(this).attr('data');
                ids += "__" + id;
            }
        });
        return ids;
    }

    function rightToLeft() {
        const leftDiv = $("#origin_unit_tree_panel");
        const rightDiv = $("#office_unit_tree_panel");
        const office_id = $('#select_offices').val();

        const ids = getIDsToSend("office_unit_tree_panel");
        if (ids == null || ids.length === 0) {
            showToast("পদবি সিলেক্ট করুন", "Please select designation first");
            return;
        }

        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                console.log(this.responseText);
                let res = JSON.parse(this.responseText);
                if (res.success === 0) {
                    showToast(success_message_bng, success_message_eng);
                } else {
                    showToast('সব শাখা ডিলিট করা সম্ভব হয়নি', "Can't delete all unit");
                }

                $("#office_unit_tree_panel").html('').attr("state", 0);
                getChildren(0, 0, "office_unit_tree_panel", "getOfficeOriginAsRight", "", office_id, "", "");
            }
        };

        xhttp.open("POST", "OfficeUnitManagementServlet?actionType=deleteOffices&office_id=" + office_id + "&ids=" + ids, true);
        xhttp.send();
    }

    function leftToRight() {
        const leftDiv = $("#origin_unit_tree_panel");
        const rightDiv = $("#office_unit_tree_panel");
        const office_id = $('#select_offices').val();

        const ids = getIDsToSend("origin_unit_tree_panel");
        if (ids == null || ids.length === 0) {
            showToast("পদবি সিলেক্ট করুন", "Please select designation first");
            return;
        }

        if (parseInt(office_id) <= 0) {
            showToast("ড্রপ ডাউন থেকে অফিস সেলেক্ট করুন", "Please select office from dropdown");
            return;
        }

        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                showToast(success_message_bng, success_message_eng);

                $("#" + last_toggle_tree.cb_id).prop("checked", false);
                checkbox_toggeled(last_toggle_tree.id, last_toggle_tree.text, last_toggle_tree.cb_id, last_toggle_tree.name, last_toggle_tree.parent_id);

                $("#office_unit_tree_panel").html('').attr("state", 0);
                getChildren(0, 0, "office_unit_tree_panel", "getOfficeOriginAsRight", "", office_id, "", "");
            }
        };

        xhttp.open("POST", "OfficeUnitManagementServlet?actionType=addOffices&office_id=" + office_id + "&ids=" + ids, true);
        xhttp.send();
    }

    function editNode(table, id) {
        console.log('edit node ', table, ' ', id);
    }

    $(document).ready(function () {
        registerUnDownClick(function (up_down, geo, liID, siblingID) {
            if (up_down === 'up') {
                console.log(up_down, ' ', geo, ' ', liID, ' ', siblingID);
                const a = liID.split('_')[4];
                const b = siblingID.split('_')[4];

                console.log(a, ' ', b);

                const xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        showToast(success_message_bng, success_message_eng);
                    }
                    else if (this.readyState == 4 && this.status != 200) {
                        alert('failed ' + this.status);
                    }
                };

                xhttp.open("POST", "OfficeUnitManagementServlet?actionType=updateLevel&id0=" + a + "&id1=" + b, true);
                xhttp.send();

            } else {
                console.log(up_down, ' ', geo, ' ', liID, ' ', siblingID);
                const a = liID.split('_')[4];
                const b = siblingID.split('_')[4];

                console.log(a, ' ', b);

                const xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        showToast(success_message_bng, success_message_eng);
                    }
                    else if (this.readyState == 4 && this.status != 200) {
                        alert('failed ' + this.status);
                    }
                };

                xhttp.open("POST", "OfficeUnitManagementServlet?actionType=updateLevel&id0=" + a + "&id1=" + b, true);
                xhttp.send();
            }
        });
    });

</script>

<!-- Modal -->
<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="history_list">
                    <table class="table table-bordered table-striped" style="width: 100%">
                        <thead class="thead-light text-center">
                        <tr>
                            <td>#</td>
                            <td>বাংলা নাম</td>
                            <td>ইংরেজি নাম</td>
                        </tr>
                        </thead>
                        <tbody id="changes_list" colspan="3">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="noChnagesModel" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="no_change">
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function onHelpClick(geo, id, text) {

        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                try {
                    let html = '', id = 1;
                    JSON.parse(this.responseText).forEach(function (val) {
                        html += '<tr>' +
                            '<td><label>' + (id++) + '</label></td>' +
                            '<td><label>' + val.name_bng + '</label></td>' +
                            '<td> <label>' + val.name_eng + '</label></td>' +
                            '</tr>';
                    });
                    $('#changes_list').html('').html(html);
                    $('#myModal').modal('show');
                } catch (e) {
                    $('#no_change').html($('#my_language').val() == 1 ? 'কোন পরিবর্তন নেই' : 'No changes found');
                    $('#noChnagesModel').modal('show');
                }
            }
        };

        xhttp.open("Get", "HistoryServlet?actionType=getUnitNameChangesHistory&unit_id=" + id, true);
        xhttp.send();
    }
</script>

