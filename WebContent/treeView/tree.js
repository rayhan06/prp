 var autoExpand = false;


    function doAutoExpand(parentElementID) {
		var searchText = "search";
		if ( $( "#language" ).length ) {
 			var language =  $( "#language" ).val();
			if(language.toLowerCase() == "bangla")
			{
				searchText = "খুঁজুন";
			}
		 
		}
    	console.log("doAutoExpand called " + parentElementID);
    	var searbox = "<div class='form-inline' style='margin-left:40px;'><input type='text' "
    	+ " class='form-control' name='treeSearch' id='" + parentElementID + "_treeSearch' tag='pb_html' onkeyup=\"searchIfEnter('" + parentElementID + "_treeSearch', '" + parentElementID + "', event)\"/>"
    	+ "<button type=\"button\" onclick=\"searchTree('" + parentElementID + "_treeSearch', '" + parentElementID + "')\" class=\"btn btn-primary\">" + searchText + "</button>"
    	+ "</div>" ;

    	var oldHtml = $("#" + parentElementID).html();
    	$("#" + parentElementID).html(searbox + oldHtml);
        $("#" + parentElementID).find(".fa-arrows").each(function () {
            console.log("found, id = " + $(this).attr('id'));
            $(this).click();
        });
        
        $("#" + parentElementID).find("a").each(function () {
            //if($(this).css("color") == "red")
           	{
            	$(this).css("color", "black");
           	}                
        });
    }
    
    function searchIfEnter(id, parentElementID, event)
    {
    	var keyCode = event.keyCode || event.which;
    	  if (keyCode === 13) { 
    		  searchTree(id, parentElementID);
    	  }
    }
    
    function searchTree(id, parentElementID)
    {
    	var value = $("#" + id).val();
    	console.log("value = " + value + " parentElementID = " + parentElementID);
    	if(value != "")
   		{
    		$("#" + parentElementID).find("li").each(function () {
                if($(this).attr("state") == '1')
               	{
                	$(this).find(".fa-arrows")[0].click();
               	}                
            });
    		$("#" + parentElementID).find("a").each(function () {
                //if($(this).css("color") == "red")
               	{
                	$(this).css("color", "black");
               	}                
            });
    		 $("#" + parentElementID).find("a").each(function () {
 	            
 	            var html = $(this).html().toLowerCase();
 	            if(html.includes(value.toLowerCase()))
             	{
 	            	//console.log("html = " + html);
 	            	$(this).css("color", "red");
 	            	var li = $(this).closest( "li" ).parent().closest("li");
 	            	//console.log("li = " + li.attr("id"));
 	            	li.find(".fa-arrows").each(function () {
 	                    //console.log("found, id = " + $(this).attr('id'));
 	                    $(this).click();
 	                });
             	}
 	           
 	        });
   		}
	   	
    }

    function getChildrenInElement(layer, id, element, treeType, actionType, returnfunc, parentID, parentElementID, extraParams, layer_id_pairs, dontKnow, dataLoadCallback) {
        var servletType = document.getElementById('servletType').value;
        if (layer_id_pairs === undefined) {
            layer_id_pairs = "";
        }
        console.log("layer_id_pairs = " + layer_id_pairs);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (this.responseText !== '') {
                    //console.log("\n\nResponse: " + this.responseText + "\n\n");
                    if (treeType === "select") {
                        element.innerHTML = this.responseText;
                        if (dataLoadCallback !== undefined && dataLoadCallback !== null) {
                            dataLoadCallback(true);
                        }
                    }
                    else {
                        element.innerHTML += this.responseText;
                        try {
                            //newOriginUnitFound(id, element);
                        } catch (e) {
                            //console.log('Please implement newOriginUnitFound(id, element) ' + e);
                        }
                    }

                    if (autoExpand === true) {
                        doAutoExpand(parentElementID);
                    }

                    if (returnfunc) {
                        returnfunc();
                    }
                    if (treeType === "select") 
                    {
                        try {
                            fillNextElementWithID(id, element);
                        } catch (e) {
                            console.log('Please implement fillNextElementWithID(id, element); ' + e);
                        }
                        
                    }
                    element.getElementsByTagName('ul')[0].style = "display:none";
                    element.setAttribute("state", 2);
                }
                else {
                    console.log("null response");
                    try {
                        fillNextElementWithID(id, element);
                    } catch (e) {
                        console.log('Please implement fillNextElementWithID(id, element); ' + e);
                    }
                }
                //select_2_call();
            }
            else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        var requestmsg = servletType + "?actionType=" + actionType
            + "&myLayer=" + layer
            + "&id=" + id
            + "&treeType=" + treeType
            + "&parentID=" + parentID
            + "&parentElementID=" + parentElementID
            + "&layer_id_pairs=" + layer_id_pairs
            + extraParams;

        //console.log("requestmsg = " + requestmsg);

        xhttp.open("Get", requestmsg, true);
        xhttp.send();
    }

    // works for select
    function getChildrenSelect(layer, selectElement, divElement, actionType, retunfunc, parentID, extraParams, layer_id_pairs, dataLoadCallback) {
        //console.log("getChildren called for dropdown");
        //console.log("servletType = " + document.getElementById('servletType').value);
        //console.log("actionType = " + actionType);
        var div = document.getElementById(divElement);
        var select = document.getElementById(selectElement);
        var id = select.options[select.selectedIndex].value;

        getChildrenInElement(layer, id, div, "select", actionType, retunfunc, 0, parentID, divElement, extraParams, layer_id_pairs, dataLoadCallback); // 12 parm
    }

    function getChildren(layer, id, parentElement, actionType, retunfunc, parentID, extraParams, checkBoxID, layer_id_pairs) {
        //console.log("servletType = " + document.getElementById('servletType').value);
        //console.log("actionType = " + actionType + " layer = " + layer + " id= " + id + ' parentID = ' + parentID + ' parentElement = ' + parentElement);
        var parent = document.getElementById(parentElement);
        if (parent.getAttribute('state') == '0') {
            var treeCBStatus = document.getElementById("treeCBStatus_" + id);
            if (treeCBStatus !== null) {
                treeCBStatus.value = 1;
            }
            var xhttp = new XMLHttpRequest();
            
            var checkBox = document.getElementById(checkBoxID);
            var checkBoxChecked = false;
            if (checkBox !== null) {
                checkBoxChecked = checkBox.checked;
            }
            extraParams += "&checkBoxChecked=" + checkBoxChecked;
            getChildrenInElement(layer, id, parent, "tree", actionType, retunfunc, parentID, parentElement, extraParams, layer_id_pairs);
        }
        else if (parent.getAttribute('state') == '1') {
            console.log("state = " + parent.getAttribute('state'));
            parent.getElementsByTagName('ul')[0].style = "display:none";
            parent.setAttribute("state", 2);
        }
        else if (parent.getAttribute('state') == '2') {
            console.log("state = " + parent.getAttribute('state'));
            parent.getElementsByTagName('ul')[0].style = "display:block";
            parent.setAttribute("state", 1);
        }

    }


    function getChildrenFlat(layer, id, parentElement, actionType, retunfunc, parentID, extraParams, layer_id_pairs) {
        //console.log("servletType = " + document.getElementById('servletType').value);
        //console.log("actionType = " + actionType + " layer = " + layer + " id= " + id + ' parentID = ' + parentID);
        var parent = document.getElementById(parentElement);

        var xhttp = new XMLHttpRequest();
        parent.setAttribute("state", 1);
        getChildrenInElement(layer, id, parent, "flat", actionType, retunfunc, parentID, parentElement, extraParams, layer_id_pairs);


    }

    function fillChildren(id, text, cb_id, name, parent_id) {
        var cb = document.getElementById(cb_id);
        var parent = document.getElementById(parent_id);
        var child_cbs = parent.querySelectorAll('input[type = "checkbox"]');
        var treeCBChecked = document.getElementById("treeCBChecked_" + id);
        var ids_checked_or_unchecked = [];
        var i, isChecked;
        if (cb.checked) {
            console.log('checked');
            isChecked = true;
            treeCBChecked.value = id;
            ids_checked_or_unchecked[0] = id;
            for (i = 0; i < child_cbs.length; i++) {
                child_cbs[i].checked = true;
                document.getElementById("treeCBChecked_" + child_cbs[i].value).value = child_cbs[i].value;
                ids_checked_or_unchecked[i + 1] = child_cbs[i].value;
            }
        }
        else {
            console.log('unchecked');
            isChecked = false;
            treeCBChecked.value = -1;
            treeCBChecked.value = id;
            for (i = 0; i < child_cbs.length; i++) {
                child_cbs[i].checked = false;
                document.getElementById("treeCBChecked_" + child_cbs[i].value).value = -1;
                ids_checked_or_unchecked[i + 1] = child_cbs[i].value;
            }
        }
        return [ids_checked_or_unchecked, isChecked];
    }

    function moveup(i) {
        var row = i.closest("tr");
        console.log("rowindex = " + row.rowIndex);

        var sibling = row.previousElementSibling;
        var parent = row.parentNode;

        parent.insertBefore(row, sibling);
        fix_order_labels();
    }

    function movedown(i) {
        var row = i.closest("tr");
        console.log("rowindex = " + row.rowIndex);
        var sibling = row.nextElementSibling;
        var parent = row.parentNode;

        parent.insertBefore(sibling, row);
        fix_order_labels();
    }

    function removeElementByID(id) {
        console.log("removing element " + id);
        var element = document.getElementById(id);
        element.parentNode.removeChild(element);
    }

    function removeElementByIDandUncheckCheckbox(id, cb_id) {
        removeElementByID(id);
        document.getElementById(cb_id).checked = false;
    }

    function addElement(id, text, cb_id, name, tbodyID) {
        if (document.getElementById("td_delete_" + id) == null) {
            var tbody = document.getElementById(tbodyID);
            var template = document.getElementById('tr-template');
            var template_backup = template.innerHTML;


            var tr = template.rows[0];
            var tr_backup = tr;
            var tr_id = "tr_selected_" + id;
            tr.setAttribute("id", tr_id);
            var i = tr.querySelector("i");
            i.setAttribute("onclick", "removeElementByIDandUncheckCheckbox('" + tr_id + "', '" + cb_id + "')");

            var hiddeninput = tr.querySelector('input[type="hidden"]');
            hiddeninput.setAttribute("name", "organogram_id");
            hiddeninput.setAttribute("value", id);
            hiddeninput.setAttribute("id", "organogram_id_hidden_" + id);
            console.log("organogram_id_hidden = " + document.getElementById("organogram_id_hidden_" + id).value);

            var select = tr.querySelector("select");
            select.setAttribute("name", "roleType");


            var td_delete = tr.cells[0];
            td_delete.setAttribute("id", "td_delete_" + id);


            var td_text = tr.cells[1];
            if (name !== "") {
                td_text.innerHTML = text + ", " + name;
            }
            else {
                td_text.innerHTML = text;
            }

            template.innerHTML = template_backup;


            tbody.innerHTML += tr.outerHTML;
        }

    }

    function generateIDFromArray(stringArray) {
        var id = "";
        for (index in stringArray) {
            id = id + stringArray[index];
            if (index < stringArray.length - 1) {
                id = id + '_';
            }
        }
        return id;
    }

    function onUpClick(geo, id, text, ul_id, button) {

        var li = button.closest("li");
        var sibling = li.previousElementSibling;
        var parent = li.parentNode;
        var liID = li.id;
        var siblingID = sibling.id;
        var liIDSubArray = liID.split('_');
        var siblingIDSubArray = siblingID.split('_');

        var liIDIndex = liIDSubArray[liIDSubArray.length - 1];
        var siblingIDIndex = siblingIDSubArray[siblingIDSubArray.length - 1];

        liIDSubArray.pop();
        siblingIDSubArray.pop();

        liIDSubArray.push(siblingIDIndex);
        siblingIDSubArray.push(liIDIndex);
        var newLiID = generateIDFromArray(liIDSubArray);
        var newSiblingID = generateIDFromArray(siblingIDSubArray);

        li.id = newLiID;
        sibling.id = newSiblingID;

        parent.insertBefore(li, sibling);
        if (callback02 != null) {
            callback02('up', geo, liID, siblingID);
        }
    }

    function onDownClick(geo, id, text, ul_id, button) {
        var li = button.closest("li");
        var sibling = li.nextElementSibling;
        var parent = li.parentNode;

        var liID = li.id;
        var siblingID = sibling.id;
        var liIDSubArray = liID.split('_');
        var siblingIDSubArray = siblingID.split('_');

        var liIDIndex = liIDSubArray[liIDSubArray.length - 1];
        var siblingIDIndex = siblingIDSubArray[siblingIDSubArray.length - 1];

        liIDSubArray.pop();
        siblingIDSubArray.pop();

        liIDSubArray.push(siblingIDIndex);
        siblingIDSubArray.push(liIDIndex);
        var newLiID = generateIDFromArray(liIDSubArray);
        var newSiblingID = generateIDFromArray(siblingIDSubArray);

        li.id = newLiID;
        sibling.id = newSiblingID;

        parent.insertBefore(sibling, li);

        if (callback02 != null) {
            callback02('down', geo, liID, siblingID);
        }
    }

    let callback02 = null;

    function registerUnDownClick(_callback) {
        callback02 = _callback;
    }