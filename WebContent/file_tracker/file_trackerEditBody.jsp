<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="file_tracker.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.OfficeUnitTypeEnum" %>
<%@ page import="employee_records.EmployeeFlatInfoDTO" %>
<%@ page import="java.util.stream.Collectors" %>

<%
    File_trackerDTO file_trackerDTO = new File_trackerDTO();
    List<EmployeeFlatInfoDTO> employeeFlatInfoDTOList = new ArrayList<>();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        file_trackerDTO = File_trackerDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = file_trackerDTO;
    String tableName = "file_tracker";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.FILE_TRACKER_ADD_FILE_TRACKER_ADD_FORMNAME, loginDTO);
    String servletName = "File_trackerServlet";
    if (file_trackerDTO.iD != -1) {
        try {
            employeeFlatInfoDTOList.add(EmployeeFlatInfoDTO.getFlatInfoOfDefaultOffice(
                    file_trackerDTO.receiverRecordsId, LM.getLanguage(userDTO)
            ));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="File_trackerServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-10 offset-1">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-10 offset-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=file_trackerDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right">
                                            <%=LM.getText(LC.FILE_TRACKER_ADD_FILENUMBER, loginDTO)%>
                                        </label>
                                        <div class="col-9">
                                            <input type='text' class='form-control' name='fileNumber'
                                                   id='fileNumber_text_<%=i%>' value='<%=file_trackerDTO.fileNumber%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right">
                                            <%=LM.getText(LC.FILE_TRACKER_ADD_DESCRIPTION, loginDTO)%>
                                        </label>
                                        <div class="col-9">
                                            <textarea class='form-control' name='description' style="resize: none"
                                                      rows="3"
                                                      id='description_textarea_<%=i%>' maxlength="1024"
                                                      tag='pb_html'><%=file_trackerDTO.description%></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right">
                                            <%=LM.getText(LC.FILE_TRACKER_ADD_BRANCHID, loginDTO)%>
                                        </label>
                                        <div class="col-9">
                                            <select class='form-control' name='branchId' id='branchId_select2_<%=i%>'
                                                    tag='pb_html'>
                                                <%=Office_unitsRepository.getInstance().buildOptionsByOfficeOrder(Language, OfficeUnitTypeEnum.DIVISION.getValue(), file_trackerDTO.branchId)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right">
                                            <%=LM.getText(LC.FILE_TRACKER_ADD_RECEIVEDATE, loginDTO)%>
                                        </label>
                                        <div class="col-9">
                                            <%value = "receiveDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='receiveDate' id='receiveDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(file_trackerDTO.receiveDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right">
                                            <%=LM.getText(LC.FILE_TRACKER_ADD_DISTRIBUTIONDATE, loginDTO)%>
                                        </label>
                                        <div class="col-9">
                                            <%value = "distributionDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='distributionDate'
                                                   id='distributionDate_date_<%=i%>'
                                                   value='<%=file_trackerDTO.distributionDate==SessionConstants.MIN_DATE?"":dateFormat.format(new Date(file_trackerDTO.distributionDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-md-right"><%=isLanguageEnglish ? "Recipient" : "প্রাপক"%>
                                        </label>
                                        <div class="col-9">
                                            <!-- Button trigger modal -->
                                            <button type="button"
                                                    class="btn btn-primary btn-block shadow btn-border-radius"
                                                    id="addToEmployee_modal_button">
                                                <%=isLanguageEnglish ? "Select Recipient" : "প্রাপক বাছাই করুন"%>
                                            </button>
                                            <table class="table table-bordered table-striped">
                                                <thead></thead>

                                                <tbody id="tagged_employee_table">
                                                <tr style="display: none;">
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        <button type="button" class="btn btn-sm delete-trash-btnbtn"
                                                                onclick="remove_containing_row(this,'tagged_employee_table');">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <%if (employeeFlatInfoDTOList != null) {%>
                                                <%for (EmployeeFlatInfoDTO employeeFlatInfoDTO : employeeFlatInfoDTOList) {%>
                                                <tr>
                                                    <td>
                                                        <%=employeeFlatInfoDTO.employeeUserName%>
                                                    </td>
                                                    <td>
                                                        <%=isLanguageEnglish ? (employeeFlatInfoDTO.officeNameEn + "(" + employeeFlatInfoDTO.organogramNameEn + ")")
                                                                : (employeeFlatInfoDTO.officeNameBn + "(" + employeeFlatInfoDTO.organogramNameBn + ")")%>
                                                    </td>
                                                    <td>
                                                        <%=isLanguageEnglish ? employeeFlatInfoDTO.employeeNameEn : employeeFlatInfoDTO.employeeNameBn%>
                                                    </td>
                                                    <td id='<%=employeeFlatInfoDTO.employeeRecordsId%>_td_button'>
                                                        <button type="button" class="btn btn-sm delete-trash-btn"
                                                                onclick="remove_containing_row(this,'tagged_employee_table');">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <%}%>
                                                <%}%>
                                                </tbody>
                                            </table>
                                            <input type='hidden' class='form-control' name='receiverRecordsId'
                                                   id='receiverRecordsId'
                                                   value='<%=file_trackerDTO.receiverRecordsId%>' tag='pb_html'/>

                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right">
                                            <%=LM.getText(LC.FILE_TRACKER_ADD_FILEDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-9">
                                            <%
                                                fileColumnName = "fileDropzone";
                                                if (actionName.equals("ajax_edit")) {
                                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(file_trackerDTO.fileDropzone);

                                            %>
                                            <%@include file="../pb/dropzoneEditor.jsp" %>
                                            <%
                                                } else {
                                                    ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                    file_trackerDTO.fileDropzone = ColumnID;
                                                }
                                            %>

                                            <div class="dropzone"
                                                 action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=file_trackerDTO.fileDropzone%>">
                                                <input type='file' style="display:none" name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value='' tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=file_trackerDTO.fileDropzone%>'/>


                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.FILE_TRACKER_ADD_FILE_TRACKER_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.FILE_TRACKER_ADD_FILE_TRACKER_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script type="text/javascript">

    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';

    function PreprocessBeforeSubmiting(row, action) {
        console.log("action = " + action);
        preprocessDateBeforeSubmitting('receiveDate', row);
        // preprocessTimeBeforeSubmitting('receiveTime', row);
        preprocessDateBeforeSubmitting('distributionDate', row);
        // preprocessTimeBeforeSubmitting('distributionTime', row);
        let added_employee = '';
        if (typeof added_employee_info_map.keys().next().value !== 'undefined') {
            added_employee = added_employee_info_map.keys().next().value;
        }
        document.getElementById('receiverRecordsId').value = added_employee;


        let msg = null;
        if (document.getElementById('fileNumber_text_0').value === '') {
            msg = isLangEng ? "File number not found" : "ফাইল নাম্বার পাওয়া যায় নি";
        } else if (document.getElementById('description_textarea_0').value === '') {
            msg = isLangEng ? "File description not found" : "ফাইলের বর্নণা পাওয়া যায় নি";
        } else if (document.getElementById('branchId_select2_0').value === '') {
            msg = isLangEng ? "Office branch not found" : "অফিস/দপ্তর পাওয়া যায় নি";
        } else if (document.getElementById('receiveDate_date_0').value === '') {
            msg = isLangEng ? "Receive date not found" : "প্রাপ্তির তারিখ পাওয়া যায় নি";
        }

        if (msg) {
            $('#toast_message').css('background-color', '#ff6063');
            showToastSticky(msg, msg);
            buttonStateChange(false);
            return false;
        }
        submitAddForm2();
        return false;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "File_trackerServlet");
    }

    function init(row) {

        $("#branchId_select2_" + row).select2({
            dropdownAutoWidth: true
        });
        setDateByStringAndId('receiveDate_js_' + row, $('#receiveDate_date_' + row).val());
        // setTimeById('receiveTime_js_' + row, $('#receiveTime_time_' + row).val(), true);
        setDateByStringAndId('distributionDate_js_' + row, $('#distributionDate_date_' + row).val());
        // setTimeById('distributionTime_js_' + row, $('#distributionTime_time_' + row).val(), true);

    }

    added_employee_info_map = new Map(
        <%if(employeeFlatInfoDTOList != null){%>
        <%=employeeFlatInfoDTOList.stream()
                        .map(detailsDTO -> "['" + detailsDTO.employeeRecordsId + "'," + detailsDTO.getIdsStrInJsonString() + "]")
                        .collect(Collectors.joining(",","[","]"))
        %>
        <%}%>
    );
    var row = 0;
    $(document).ready(function () {
        init(row);
        //CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_employee_table', {
                info_map: added_employee_info_map,
                isSingleEntry: true
            }],

        ]
    );

    modal_button_dest_table = 'none';

    $('#addToEmployee_modal_button').on('click', function () {
        //alert('CLICKED');
        modal_button_dest_table = 'tagged_employee_table';
        $('#search_emp_modal').modal();
    });


</script>






