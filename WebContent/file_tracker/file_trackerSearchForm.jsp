<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="file_tracker.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="employee_records.EmployeeFlatInfoDTO" %>


<%!
%><%
    String navigator2 = "navFILE_TRACKER";
    String servletName = "File_trackerServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><%=LM.getText(LC.FILE_TRACKER_ADD_SERIAL, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.FILE_TRACKER_ADD_FILENUMBER, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.FILE_TRACKER_ADD_DESCRIPTION, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.FILE_TRACKER_ADD_BRANCHID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.FILE_TRACKER_ADD_RECEIVEDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.FILE_TRACKER_ADD_DISTRIBUTIONDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.FILE_TRACKER_ADD_RECEIVERRECORDSID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.FILE_TRACKER_SEARCH_FILE_TRACKER_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span>All</span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<File_trackerDTO>) rn2.list;

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        File_trackerDTO file_trackerDTO = (File_trackerDTO) data.get(i);


        %>
        <tr>


            <td>
                <%=Utils.getDigits(file_trackerDTO.serial, Language)%>
            </td>

            <td>
                <%=file_trackerDTO.fileNumber%>
            </td>

            <td>
                <%=file_trackerDTO.description%>
            </td>

            <td>
                <%=isLanguageEnglish ? Office_unitsRepository.getInstance().getOffice_unitsDTOByID(file_trackerDTO.branchId).unitNameEng : Office_unitsRepository.getInstance().getOffice_unitsDTOByID(file_trackerDTO.branchId).unitNameBng%>
            </td>

            <td>
                <%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, file_trackerDTO.receiveDate), Language)%>
            </td>


            <td>
                <%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, file_trackerDTO.distributionDate), Language)%>
            </td>


            <%
                EmployeeFlatInfoDTO employeeFlatInfoDTO = null;
                try {
                    employeeFlatInfoDTO = EmployeeFlatInfoDTO.getFlatInfoOfDefaultOffice(
                            file_trackerDTO.receiverRecordsId, Language
                    );
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            %>
            <td>
                <%=Utils.getDigits(employeeFlatInfoDTO != null ? employeeFlatInfoDTO.getCommaFormattedInfo(Language) : "", Language)%>
            </td>


            <%CommonDTO commonDTO = file_trackerDTO; %>
            <%@include file="../pb/searchAndViewButton.jsp" %>

            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID' value='<%=file_trackerDTO.iD%>'/></span>
                </div>
            </td>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			