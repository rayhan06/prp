<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="file_tracker.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="employee_records.EmployeeFlatInfoDTO" %>


<%
    String servletName = "File_trackerServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    File_trackerDTO file_trackerDTO = File_trackerDAO.getInstance().getDTOByID(id);
    CommonDTO commonDTO = file_trackerDTO;
%>
<%@include file="../pb/viewInitializer.jsp" %>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.FILE_TRACKER_ADD_FILE_TRACKER_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-10 offset-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.FILE_TRACKER_ADD_FILE_TRACKER_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-3 col-form-label text-right">
                                        <%=LM.getText(LC.FILE_TRACKER_ADD_SERIAL, loginDTO)%>
                                    </label>
                                    <div class="col-9">
                                        <%=Utils.getDigits(file_trackerDTO.serial, Language)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-3 col-form-label text-right">
                                        <%=LM.getText(LC.FILE_TRACKER_ADD_FILENUMBER, loginDTO)%>
                                    </label>
                                    <div class="col-9">
                                        <%=file_trackerDTO.fileNumber%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-3 col-form-label text-right">
                                        <%=LM.getText(LC.FILE_TRACKER_ADD_DESCRIPTION, loginDTO)%>
                                    </label>
                                    <div class="col-9">
                                        <%=file_trackerDTO.description%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-3 col-form-label text-right">
                                        <%=LM.getText(LC.FILE_TRACKER_ADD_BRANCHID, loginDTO)%>
                                    </label>
                                    <div class="col-9">
                                        <%=isLanguageEnglish ? Office_unitsRepository.getInstance().getOffice_unitsDTOByID(file_trackerDTO.branchId).unitNameEng : Office_unitsRepository.getInstance().getOffice_unitsDTOByID(file_trackerDTO.branchId).unitNameBng%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-3 col-form-label text-right">
                                        <%=LM.getText(LC.FILE_TRACKER_ADD_RECEIVEDATE, loginDTO)%>
                                    </label>
                                    <div class="col-9">
                                        <%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, file_trackerDTO.receiveDate), Language)%>
                                    </div>
                                </div>


                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-3 col-form-label text-right">
                                        <%=LM.getText(LC.FILE_TRACKER_ADD_DISTRIBUTIONDATE, loginDTO)%>
                                    </label>
                                    <div class="col-9">
                                        <%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, file_trackerDTO.distributionDate), Language)%>
                                    </div>
                                </div>



                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-3 col-form-label text-right">
                                        <%=LM.getText(LC.FILE_TRACKER_ADD_RECEIVERRECORDSID, loginDTO)%>
                                    </label>
                                    <div class="col-9">
                                        <%
                                            EmployeeFlatInfoDTO employeeFlatInfoDTO = null;
                                            try {
                                                employeeFlatInfoDTO = EmployeeFlatInfoDTO.getFlatInfoOfDefaultOffice(
                                                        file_trackerDTO.receiverRecordsId, Language
                                                );
                                            } catch (Exception ex) {
                                                System.out.println(ex.getMessage());
                                            }
                                        %>
                                        <%=Utils.getDigits(employeeFlatInfoDTO != null ? employeeFlatInfoDTO.getCommaFormattedInfo(Language) : "", Language)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-3 col-form-label text-right">
                                        <%=LM.getText(LC.FILE_TRACKER_ADD_FILEDROPZONE, loginDTO)%>
                                    </label>
                                    <div class="col-9">
                                        <%
                                            String fileColumnName = "";
                                            List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(file_trackerDTO.fileDropzone);
                                        %>
                                        <table>
                                            <tr>
                                                <%
                                                    if (fileList != null) {
                                                        for (int j = 0; j < fileList.size(); j++) {
                                                            FilesDTO filesDTO = fileList.get(j);
                                                            byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                %>
                                                <td id='<%=fileColumnName%>_td_<%=filesDTO.iD%>'>
                                                    <%
                                                        if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                    %>
                                                    <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)
						%>' style='width:100px'/>
                                                    <%
                                                        }
                                                    %>
                                                    <a href='<%=servletName%>?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                       download><%=filesDTO.fileTitle%>
                                                    </a>

                                                </td>
                                                <%
                                                        }
                                                    }
                                                %>
                                            </tr>
                                        </table>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>