

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="payroll_month_bill.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Payroll_month_billServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Payroll_month_billDTO payroll_month_billDTO = Payroll_month_billDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = payroll_month_billDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PAYROLL_MONTH_BILL_ADD_PAYROLL_MONTH_BILL_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.PAYROLL_MONTH_BILL_ADD_PAYROLL_MONTH_BILL_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PAYROLL_MONTH_BILL_ADD_ALLOWANCEEMPLOYEEINFOID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = payroll_month_billDTO.allowanceEmployeeInfoId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PAYROLL_MONTH_BILL_ADD_MONTHYEAR, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = payroll_month_billDTO.monthYear + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PAYROLL_MONTH_BILL_ADD_BUDGETOFFICEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = payroll_month_billDTO.budgetOfficeId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PAYROLL_MONTH_BILL_ADD_EMPLOYMENTCAT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = payroll_month_billDTO.employmentCat + "";
											%>
											<%
											value = CatRepository.getInstance().getText(Language, "employment", payroll_month_billDTO.employmentCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
			
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>