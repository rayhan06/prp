<%@page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@page import="bangladehi_number_format_util.BangladeshiNumberInWord" %>
<%@page import="budget_office.Budget_officeRepository" %>
<%@ page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="payroll_month_bill.Payroll_month_billDTO" %>
<%@page import="pbReport.DateUtils" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="util.StringUtils" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="static util.StringUtils.convertToBanNumber" %>
<%@ page import="static util.StringUtils.convertToBanNumber" %>
<%@ page import="payroll_allowance_configuration.Payroll_allowance_configurationModel" %>
<%@ page import="payroll_allowance_configuration.Payroll_allowance_configurationRepository" %>
<%@ page import="tax_deduction_configuration.Tax_deduction_configurationRepository" %>
<%@ page import="employee_records.EmploymentEnum" %>
<%@ page import="tax_deduction_configuration.Tax_deduction_configurationModel" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="payroll_month_bill_allowance.Payroll_month_bill_allowanceDTO" %>
<%@ page import="payroll_month_bill.Payroll_month_billDAO" %>
<%@ page import="payroll_month_bill_allowance.Payroll_month_bill_allowanceDAO" %>
<%@ page import="pb.Utils" %>
<%@ page import="payroll_month_bill_deduction.Payroll_month_bill_deductionDTO" %>
<%@ page import="payroll_month_bill_deduction.Payroll_month_bill_deductionDAO" %>
<%@ page import="java.util.Comparator" %>
<%@ page import="allowance_employee_info.AllowanceEmployeeInfoDTO" %>
<%@ page import="allowance_employee_info.AllowanceEmployeeInfoRepository" %>
<%@ page import="com.sun.xml.internal.ws.policy.privateutil.PolicyUtils" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="test_lib.EmployeeRecordDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="payroll_month_bill.Payroll_month_billServlet" %>
<%@ page import="java.util.stream.IntStream" %>
<%@ page import="allowance_configure.Allowance_configureRepository" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>
<%@ page import="budget_mapping.Budget_mappingDTO" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="budget.BudgetCategoryEnum" %>
<%@ page import="budget_operation.Budget_operationRepository" %>
<%@ page import="budget.BudgetDAO" %>
<%@ page import="budget.BudgetUtils" %>
<%@page pageEncoding="UTF-8" %>

<%
    List<Payroll_month_billDTO> month_billDTOS = (List<Payroll_month_billDTO>) request.getAttribute("monthBillDTOs");
    long totalNetAmount=month_billDTOS.stream().mapToLong(e->e.netAmount).sum();
    List<AllowanceEmployeeInfoDTO> allowanceEmployeeInfoDTOS = month_billDTOS.stream()
            .map(dto -> AllowanceEmployeeInfoRepository.getInstance().getById(dto.allowanceEmployeeInfoId))
            .collect(Collectors.toList());
    Map<Long, Long> mapByAllowanceEmployeeId = month_billDTOS.stream().collect(Collectors.toMap(dto -> dto.allowanceEmployeeInfoId, dto -> dto.iD, (e1, e2) -> e1));
    Map<Long, List<AllowanceEmployeeInfoDTO>> mapByOfficeUnitId = allowanceEmployeeInfoDTOS.stream()
            .collect(Collectors.groupingBy(dto -> dto.officeUnitId));
    List<Long> payrollIds = month_billDTOS.stream().map(dto -> dto.iD).collect(Collectors.toList());
    List<Payroll_month_bill_allowanceDTO> allowanceDTOs = Payroll_month_bill_allowanceDAO.getInstance().getBypayrollMonthBillIds(payrollIds);
    Map<Long, Integer> allowanceSumMap = Payroll_month_bill_allowanceDAO.getInstance().getSumOfAllowance(allowanceDTOs);
    List<Payroll_month_bill_deductionDTO> deductionDTOS = Payroll_month_bill_deductionDAO.getInstance().getBypayrollMonthBillIds(payrollIds);
    Map<Long, Integer> deductionSumMap = Payroll_month_bill_deductionDAO.getInstance().getSumOfDeduction(deductionDTOS);
    long monthYear = (long) request.getAttribute("monthYear");
    long budgetOfficeId = (long) request.getAttribute("budgetOfficeId");
    Budget_mappingDTO budgetMappingDTO = Budget_mappingRepository.getInstance()
            .getDTO(budgetOfficeId, BudgetCategoryEnum.OPERATIONAL.getValue());
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getLanguage(loginDTO);

    String pdfFileName = "Payroll Bill Forwarding "
            + DateUtils.getMonthYear(monthYear, "English", " ") + " "
            + Budget_officeRepository.getInstance().getText(budgetOfficeId, "English");
    String voucherNumber = "";
    if (month_billDTOS != null && month_billDTOS.size() > 0) {
        voucherNumber = month_billDTOS.get(0).voucherNumber;
    }
    List<Payroll_allowance_configurationModel> allowance_configurationModels;
    List<Tax_deduction_configurationModel> deduction_configurationModels;
    long salaryConfigurationId = Payroll_allowance_configurationRepository.getInstance().getBySubCodeId(Payroll_month_billServlet.PRIVILEGE_SALARY_SUB_CODE_ID).iD;
    allowance_configurationModels = allowanceDTOs
            .stream()
            .distinct()
            .map(dto -> new Payroll_allowance_configurationModel(dto, "BANGLA"))
            .sorted(Comparator.comparing(dto -> dto.code))
            .collect(Collectors.toList());
    int salaryIndex = IntStream.range(0, allowance_configurationModels.size())
            .filter(i -> Long.parseLong(allowance_configurationModels.get(i).configurationId) == salaryConfigurationId)
            .findFirst()
            .orElse(-1);
    if (salaryIndex > -1) {
        Payroll_allowance_configurationModel salaryModel = allowance_configurationModels.get(salaryIndex);
        allowance_configurationModels.remove(salaryIndex);
        //allowance_configurationModels.add(0, salaryModel);
    }
    deduction_configurationModels = deductionDTOS
            .stream()
            .distinct()
            .map(dto -> new Tax_deduction_configurationModel(dto, "BANGLA"))
            .sorted(Comparator.comparing(dto -> dto.code))
            .collect(Collectors.toList());

    int allowanceRowspans1 = ((allowance_configurationModels.size() % 2) == 0) ? allowance_configurationModels.size() / 2 : (allowance_configurationModels.size() / 2) + 1;
    allowanceRowspans1=allowanceRowspans1==0?1:allowanceRowspans1;
    int allowanceRowspans2 = ((allowance_configurationModels.size()+1) - allowanceRowspans1) + 1;
    int deductionRowspans1 = (deduction_configurationModels.size() / 4) + 1;
    int deductionRowspans2 = (deduction_configurationModels.size() / 4) + 1;
    int deductionRowspans3 = (deduction_configurationModels.size() / 4) + 1;
    int deductionRowspans4 = deduction_configurationModels.size() - (deductionRowspans1 + deductionRowspans2 + deductionRowspans3);
    deductionRowspans4 = deductionRowspans4 <= 0 ? 1 : deductionRowspans4;
    String budgetOfficeNameBn = Budget_officeRepository.getInstance().getText(budgetOfficeId, "Bangla");
    int allowanceIndex = 0;
    int deductionIndex = 0;
    int totalAllowanceAmount = allowanceSumMap.values().stream().mapToInt(e -> e).sum();
    int totalDeductionAmount = deductionSumMap.values().stream().mapToInt(e -> e).sum();
    Map<Long, Payroll_month_bill_allowanceDTO> salaryMap = allowanceDTOs.stream().filter(dto -> dto.payrollAllowanceConfigurationId == salaryConfigurationId).collect(Collectors.toMap(dto -> dto.payrollMonthBillId, dto -> dto, (e1, e2) -> e1));
    Map<Long, List<Payroll_month_bill_allowanceDTO>> allowanceDTOMap = allowanceDTOs.stream().filter(dto -> dto.payrollAllowanceConfigurationId != salaryConfigurationId).collect(Collectors.groupingBy(dto -> dto.payrollMonthBillId));
    Map<Long, List<Payroll_month_bill_deductionDTO>> deductionDTOMap = deductionDTOS.stream().collect(Collectors.groupingBy(dto -> dto.payrollMonthBillId));
    long uptoLastBillTotal = Payroll_month_billDAO.getInstance().getUptoLastBillTotal(
            budgetOfficeId,
            BudgetUtils.getEconomicYearStartMonthYear(monthYear),
            monthYear
    );
    long allocatedBudget = BudgetDAO.getInstance().getAllocatedBudget(
            System.currentTimeMillis(),
            budgetMappingDTO.iD,
            Payroll_month_billServlet.PRIVILEGE_SALARY_SUB_CODE_ID
    );
    String allocatedBudgetStr = allocatedBudget <= 0L ? ""
            : BangladeshiNumberFormatter.getFormattedNumber(
            convertToBanNumber(String.valueOf(allocatedBudget))
    );
    String totalExpenditure=(uptoLastBillTotal+totalNetAmount) <= 0L ? ""
            : BangladeshiNumberFormatter.getFormattedNumber(
            convertToBanNumber(String.valueOf(uptoLastBillTotal+totalNetAmount))
    );
    String remainingBudgetStr = allocatedBudget <= 0L ? ""
            : BangladeshiNumberFormatter.getFormattedNumber(
            convertToBanNumber(String.valueOf(allocatedBudget - uptoLastBillTotal))
    );
    Map<Long,Payroll_month_bill_deductionDTO>mapForHouseRentDeduction=deductionDTOS.stream()
            .filter(dto->dto.taxDeductionConfigurationId==Tax_deduction_configurationRepository.getInstance().getHouseRentDeductionId())
            .collect(Collectors.toMap(dto->dto.payrollMonthBillId,dto->dto,(e1,e2)->e1));
%>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    .fix-fill {
        overflow: hidden;
        white-space: nowrap;
    }

    .to-print-font * {
        font-size: 12px;
    }

    .to-print-font h1 {
        font-size: 16px;
        font-weight: bold;
    }

    .to-print-font h2 {
        font-size: 14px;
        font-weight: bold;
    }

    .to-print-font h3 {
        font-size: 13px;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .5in;
        background: white;
        margin-bottom: 10px;
    }

    .page[data-size="A4-landscape"] {
        width: 297mm;
        height: 210mm;
        padding: .5in;
        background: white;
        margin-bottom: 10px;
    }

    .foot-note {
        font-size: 11px !important;
    }

    .blank-to-fill {
        display: inline-block;
        border-bottom: 1px dotted black;
        width: inherit;
    }

    .table-bordered > :not(caption) > * {
        border-color: black;
    }

    .table-bordered > :not(caption) > * > * {
        border-color: black;
    }

    .table-bordered td,
    .table-bordered th {
        padding: 5px;
    }

    .align-top {
        vertical-align: top;
    }

    th {
        text-align: center;
    }

    .landscape td {
        text-align: center;
        font-size: 10px !important;
    }
</style>

<div class="kt-content" id="kt_content">
    <div class="row">
        <div class="kt-portlet">
            <div class="kt-portlet__body page-bg" id="bill-div">
                <div class="kt-subheader__main ml-4">
                    <label class="h2 kt-subheader__title" style="color: #00a1d4;">
                        <%=UtilCharacter.getDataByLanguage(Language, "মাসিক পেরোল বিল", "Monthly Payroll Bill")%>
                    </label>
                </div>
                <hr style="border-top: 1px solid rgba(0, 0, 0, 0.1); margin: 10px 10px">

                <div class="ml-auto m-3">
                    <button type="button" class="btn" id='download-pdf'
                            onclick="downloadTemplatesAsPdf()">
                        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                </div>

                <div style="margin: auto;" class="to-print-font">
                    <%--Reference Documents from Parliament\Kawsar, AO, Fin 01550445791\O.T Bil Forwarding  All  February  21--%>
                    <div class="container to-print-font" id="to-print-div-pot">
                        <section class="page shadow" data-size="A4">
                            <div class="text-center">
                                <h1>প্রিভিলেজ কর্মচারীগণের বেতনের বিল</h1>
                                <h2>
                                    বাংলাদেশ জাতীয় সংসদের
                                    <%=budgetOfficeNameBn%>
                                    কার্যালয়ে
                                    কর্মরত <%=convertToBanNumber(String.valueOf(month_billDTOS.size()))%> জন কর্মকর্তার
                                    <%=DateUtils.getMonthYear(monthYear, "BANGLA", "/")%> খ্রিঃ মাসের বেতন ভাতা বিল ।
                                </h2>
                                <h3>
                                    দপ্তরের
                                    নামঃ&nbsp;<%=budgetOfficeNameBn%>
                                </h3>
                                <h3>
                                    <%-- ১০২ সংসদ কোড-অপারেশন কোড-৩১১১৩২৭ অধিকাল ভাতা কোড --%>
                                    কোড নংঃ
                                        <%=Budget_operationRepository.getInstance().getCode(
                                                budgetMappingDTO.budgetOperationId,
                                                "Bangla"
                                        )%>
                                    -৩২১১১০৯ সাকুল্য বেতন(সরকারী কর্মচারী ব্যতীত)
                                </h3>
                            </div>

                            <div>
                                <div class="mt-4">
                                    <div class="row">
                                        <div class="col-3 fix-fill">
                                            টোকেন নং
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="col-3 fix-fill">
                                            তারিখ
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="col-3 fix-fill">
                                            ভাউচার নং
                                            <div class="blank-to-fill">
                                                &nbsp;&nbsp;<%=Utils.getDigits(voucherNumber,"BANGLA")%>
                                            </div>
                                        </div>
                                        <div class="col-3 fix-fill">
                                            তারিখ
                                            <div class="blank-to-fill">
                                                &nbsp;&nbsp;<%=StringUtils.getFormattedDate("bangla", System.currentTimeMillis())%>
                                            </div>
                                        </div>
                                    </div>

                                    <table class="table-bordered mt-2 w-100">
                                        <thead>
                                        <tr>
                                            <th width="3%"></th>
                                            <th width="37%">নির্দেশাবলী</th>
                                            <th width="35%">বিবরণ</th>
                                            <th width="25%">টাকা</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="align-top" rowspan="<%=allowanceRowspans1%>"> ১</td>
                                            <td class="align-top" rowspan="<%=allowanceRowspans1%>">
                                                অবিলিকৃত/স্থ’গিত টাকা যথাযথ কলামে লাল কালিতে লিখিতে হইবে এবং যোগ দেওয়ার
                                                সময় উহা বাদ
                                                রাখিতে হইবে।
                                            </td>
                                            <td>
                                                <%="৩২১১১০৯-সাকুল্য বেতন"%>
                                            </td>
                                            <td class="text-right">
                                                <%=Utils.getDigits(allowanceSumMap.get(salaryConfigurationId)==null?0:allowanceSumMap.get(salaryConfigurationId),"BANGLA")%>/-
                                            </td>
                                        </tr>
                                        <%
                                            for (int i = 1; i < allowanceRowspans1; i++) {

                                        %>
                                        <tr>

                                            <%
                                                int amount = 0;
                                                if (i-1 < allowance_configurationModels.size()) {
                                                    String code = allowance_configurationModels.get(i-1).code;
                                                    String name = allowance_configurationModels.get(i-1).name;
                                                    amount = allowanceSumMap.get(Long.parseLong(allowance_configurationModels.get(i).configurationId));
                                            %>
                                            <td>
                                                <%="*" + name%>
                                            </td>
                                            <td class="text-right">
                                                <%=Utils.getDigits(amount, "BANGLA")%>/-
                                            </td>
                                            <%
                                                    allowanceIndex++;
                                                }
                                            %>


                                        </tr>
                                        <%
                                            }
                                        %>
                                        <tr>
                                            <td class="align-top" rowspan="<%=allowanceRowspans2%>"> ২</td>
                                            <td class="align-top" rowspan="<%=allowanceRowspans2%>">
                                                বেতন বৃদ্ধিও সার্টিফিকেট বা অনুুপস্থিত কর্মচারীগণের তালিকায়ক স্থান পায়
                                                নাই এমন ঘটনাসমূহ যথা-মৃত্যু, অবসর গ্রহণ, স্থায়ী বদরী ও প্রথম নিয়োগ
                                                মন্তব্য কলামে
                                                লিখিতে
                                                হইবে।
                                            </td>
                                            <td>
                                                <%
                                                    int amount = 0;
                                                    if (allowanceIndex < allowance_configurationModels.size()) {
                                                        String code = allowance_configurationModels.get(allowanceIndex).code;
                                                        String name = allowance_configurationModels.get(allowanceIndex).name;
                                                        amount = allowanceSumMap.get(Long.parseLong(allowance_configurationModels.get(allowanceIndex).configurationId));

                                                %>
                                                <%="*" + name%>
                                            </td>
                                            <td class="text-right">
                                                <%=Utils.getDigits(amount, "BANGLA")%>/-
                                            </td>
                                            <%
                                                allowanceIndex++;
                                            } else {
                                            %>
                                            </td>
                                            <td>
                                            </td>
                                            <%
                                                }
                                            %>
                                        </tr>
                                        <%
                                            for (int i = allowanceIndex; i < allowanceRowspans1 + allowanceRowspans2 - 2; i++) {

                                        %>
                                        <tr>

                                            <%
                                                amount = 0;
                                                if (i < allowance_configurationModels.size()) {

                                                    String code = allowance_configurationModels.get(i).code;
                                                    String name = allowance_configurationModels.get(i).name;
                                                    amount = allowanceSumMap.get(Long.parseLong(allowance_configurationModels.get(allowanceIndex).configurationId));

                                            %>
                                            <td>
                                                <%="*" + name%>
                                            </td>
                                            <%
                                                    allowanceIndex++;
                                                }
                                            %>

                                            <td class="text-right">
                                                <%=Utils.getDigits(amount, "BANGLA")%>/-
                                            </td>

                                        </tr>
                                        <%
                                            }
                                        %>
                                        <tr>
                                            <td class="text-right">
                                                <strong>
                                                    মোট দাবী (ক) =
                                                </strong>
                                            </td>
                                            <td class="text-right">
                                                <%=Utils.getDigits(totalAllowanceAmount, "BANGLA")%>/-
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="align-top" rowspan="<%=deductionRowspans1+1%>"> ৩</td>
                                            <td class="align-top" rowspan="<%=deductionRowspans1+1%>">
                                                কোন দাবিকৃত বেতন বৃদ্ধি সরকারী কর্মচারীর দক্ষতার সীমা অতিক্রম করার আওতায়
                                                পডিলে
                                                সংশ্লিষ্ট কর্মচারী উক্ত সীমা অতিক্রম করার উপযুক্ত কর্তৃপক্ষের প্রত্যায়ন
                                                দ্বারা
                                                সমর্থিত হইতে হইবে। (এস আর ১৫৬)।
                                            </td>
                                            <td class="text-center">
                                                <strong>
                                                    কর্তন ও আয়ঃ
                                                </strong>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <%
                                            for (int i = deductionIndex; i < deductionRowspans1; i++) {

                                        %>
                                        <tr>

                                            <%
                                                amount = 0;
                                                if (i < deduction_configurationModels.size()) {
                                                    String code = deduction_configurationModels.get(i).code;
                                                    String name = deduction_configurationModels.get(i).name;
                                                    amount = deductionSumMap.get(Long.parseLong(deduction_configurationModels.get(i).configurationId));
                                            %>
                                            <td>
                                                <%=code + " - " + name%>
                                            </td>
                                            <%
                                                    deductionIndex++;
                                                }
                                            %>

                                            <td class="text-right">
                                                <%=Utils.getDigits(amount, "BANGLA")%>/-
                                            </td>

                                        </tr>
                                        <%
                                            }
                                        %>
                                        <%--                                        <tr>--%>
                                        <%--                                            <td>রাজস্ব স্ট্যাম্প বাবদ কর্তন</td>--%>
                                        <%--                                            <td class="text-right">--%>
                                        <%--                                                <%=0%>/---%>
                                        <%--                                            </td>--%>
                                        <%--                                        </tr>--%>

                                        <tr>
                                            <td class="align-top" rowspan="<%=deductionRowspans2%>"> ৪</td>
                                            <td class="align-top" rowspan="<%=deductionRowspans2%>">
                                                অধঃস্তন সরকারী কর্মচারী এবং এস. আর. ১৫২ তে উলি­খিত সরকারী সরকারী
                                                কর্মচারদের নাম
                                                বেতনের বিলে বাদ দেওয়া যাইতে পারে।
                                            </td>
                                            <td>
                                                <%
                                                    amount = 0;
                                                    if (deductionIndex < deduction_configurationModels.size()) {
                                                        String code = deduction_configurationModels.get(deductionIndex).code;
                                                        String name = deduction_configurationModels.get(deductionIndex).name;
                                                        amount = deductionSumMap.get(Long.parseLong(deduction_configurationModels.get(deductionIndex).configurationId));

                                                %>
                                                <%=code + " - " + name%>
                                            </td>
                                            <td class="text-right">
                                                <%=Utils.getDigits(amount == 0 ? "" : String.valueOf(amount), "BANGLA")%>
                                                /-
                                            </td>
                                            <%
                                                deductionIndex++;
                                            } else {
                                            %>
                                            </td>
                                            <td>
                                            </td>
                                            <%
                                                }
                                            %>
                                        </tr>
                                        <%
                                            for (int i = deductionIndex; i < deductionRowspans1 + deductionRowspans2; i++) {

                                        %>
                                        <tr>
                                            <%
                                                amount = 0;
                                                if (i < deduction_configurationModels.size()) {
                                                    String code = deduction_configurationModels.get(i).code;
                                                    String name = deduction_configurationModels.get(i).name;
                                                    amount = deductionSumMap.get(Long.parseLong(deduction_configurationModels.get(i).configurationId));

                                            %>
                                            <td>
                                                <%=code + " - " + name%>
                                            </td>
                                            <%
                                                    deductionIndex++;
                                                }
                                            %>

                                            <%
                                                if (i < deduction_configurationModels.size()) {
                                            %>
                                            <td class="text-right">
                                                <%=Utils.getDigits(amount, "BANGLA")%>/-
                                            </td>
                                            <%
                                                }
                                            %>

                                        </tr>
                                        <%
                                            }
                                        %>
                                        <tr>
                                            <td class="align-top" rowspan="<%=deductionRowspans3%>"> ৫</td>
                                            <td class="align-top" rowspan="<%=deductionRowspans3%>">
                                                অধঃস্তন সরকারী কর্মচারী এবং এস. আর. ১৫২ তে উলি­খিত সরকারী সরকারী
                                                কর্মচারদের নাম
                                                বেতনের বিলে বাদ দেওয়া যাইতে পারে।
                                            </td>
                                            <td>
                                                <%
                                                    amount = 0;
                                                    if (deductionIndex < deduction_configurationModels.size()) {
                                                        String code = deduction_configurationModels.get(deductionIndex).code;
                                                        String name = deduction_configurationModels.get(deductionIndex).name;
                                                        amount = deductionSumMap.get(Long.parseLong(deduction_configurationModels.get(deductionIndex).configurationId));

                                                %>
                                                <%=code + " - " + name%>
                                            </td>
                                            <td class="text-right">
                                                <%=Utils.getDigits(amount, "BANGLA")%>/-
                                            </td>
                                            <%
                                                deductionIndex++;
                                            } else {
                                            %>
                                            </td>
                                            <td>
                                            </td>
                                            <%
                                                }
                                            %>
                                        </tr>
                                        <%
                                            for (int i = deductionIndex; i < deductionRowspans1 + deductionRowspans2 + deductionRowspans3; i++) {

                                        %>
                                        <tr>
                                            <%
                                                amount = 0;
                                                if (i < deduction_configurationModels.size()) {

                                                    String code = deduction_configurationModels.get(i).code;
                                                    String name = deduction_configurationModels.get(i).name;
                                                    amount = deductionSumMap.get(Long.parseLong(deduction_configurationModels.get(i).configurationId));
                                            %>
                                            <td>
                                                <%=code + " - " + name%>
                                            </td>
                                            <%
                                                    deductionIndex++;
                                                }
                                            %>
                                            <%
                                                if (i < deduction_configurationModels.size()) {
                                            %>
                                            <td class="text-right">
                                                <%=Utils.getDigits(amount, "BANGLA")%>/-
                                            </td>
                                            <%
                                                }
                                            %>
                                        </tr>
                                        <%
                                            }
                                        %>
                                        <tr>
                                            <td class="align-top" rowspan="<%=deductionRowspans4%>">৬</td>
                                            <td class="align-top" rowspan="<%=deductionRowspans4%>">
                                                স্থায়ী পদে নিযুক্ত ব্যক্তিদের নাম স্থায়ী পদের বেতন গ্রহণের মাপ কাঠিতে
                                                জ্যেষ্ঠত্বের
                                                ক্রম অনুসারে লিখিতে হইবে এবং খালি পদসমূহ স্থানাপন্ন লোকদিগকে দেখাইতে
                                                হইবে।
                                            </td>
                                            <td>
                                                <%
                                                    amount = 0;
                                                    if (deductionIndex < deduction_configurationModels.size()) {
                                                        String code = deduction_configurationModels.get(deductionIndex).code;
                                                        String name = deduction_configurationModels.get(deductionIndex).name;
                                                        amount = deductionSumMap.get(Long.parseLong(deduction_configurationModels.get(deductionIndex).configurationId));

                                                %>
                                                <%=code + " - " + name%>
                                            </td>
                                            <td class="text-right">
                                                <%=Utils.getDigits(amount, "BANGLA")%>/-
                                            </td>
                                            <%
                                                deductionIndex++;
                                            } else {
                                            %>
                                            </td>
                                            <td>
                                            </td>
                                            <%
                                                }
                                            %>

                                        </tr>
                                        <%
                                            for (int i = deductionIndex; i < deductionRowspans1 + deductionRowspans2 + deductionRowspans3 + deductionRowspans4; i++) {

                                        %>
                                        <tr>

                                            <%
                                                amount = 0;
                                                if (i < deduction_configurationModels.size()) {
                                                    String code = deduction_configurationModels.get(i).code;
                                                    String name = deduction_configurationModels.get(i).name;
                                                    amount = deductionSumMap.get(Long.parseLong(deduction_configurationModels.get(i).configurationId));
                                            %>
                                            <td>
                                                <%=code + " - " + name%>
                                            </td>
                                            <%
                                                    deductionIndex++;
                                                }
                                            %>

                                            <%
                                                if (i < deduction_configurationModels.size()) {
                                            %>
                                            <td class="text-right">
                                                <%=Utils.getDigits(amount, "BANGLA")%>/-
                                            </td>
                                            <%
                                                }
                                            %>

                                        </tr>
                                        <%
                                            }
                                        %>
                                        <tr>
                                            <td class="align-top" rowspan="3"> ৭</td>
                                            <td class="align-top" rowspan="3">
                                                বেতন বিলে কর্তন ও অদায়ের পৃথক পৃথক সিডিউল বেতনের বিলে সংযুক্ত করিতে
                                                হইবে।
                                            </td>
                                            <td class="text-right">
                                                <strong>
                                                    মোট কর্তন আদায় (খ)
                                                </strong>
                                            </td>
                                            <td class="text-right">
                                                <%=Utils.getDigits(totalDeductionAmount, "BANGLA")%>/-
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <strong>
                                                    নীট দাবী (ক-খ)
                                                </strong>
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter
                                                        .
                                                                getFormattedNumber
                                                                        (
                                                                                Utils.getDigits(totalAllowanceAmount - totalDeductionAmount, "BANGLA")
                                                                        )%>/-
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <br>
                                                প্রদানের জন্য নীট টাকার প্রয়োজন কথায়
                                                <%=BangladeshiNumberInWord
                                                        .
                                                                convertToWord
                                                                        (
                                                                                Utils.getDigits(totalAllowanceAmount - totalDeductionAmount, "BANGLA")
                                                                        )%>
                                                টাকা (মাত্র)
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </section>

                        <section class="page shadow" data-size="A4">
                            <p class="mt-3">
                                ১. (ক) বিলের টাকা বুঝিয়া পাইলাম <br>
                                (খ) প্রত্যয়ন করিতেছি যে, নিম্নে বিশদভাবে বর্ণিত টাকা (যাহা এই বিল হইতে কর্তন করিয়া ফেরত
                                দেওয়া
                                হইয়াছে) ব্যতীত এই তারিখের <br> ১* মাস/২ মাস/৩ মাস পূর্বে উত্তোলিত বিলের অন্তভূক্ত টাকা
                                যথার্থ
                                ব্যক্তিদের
                                প্রদান করা হইয়াছে। <br>
                                * প্রযোজ্য ক্ষেত্রে টিক ঢিহ্ন দিন। <br>
                                (গ) প্রত্যয়ন করিতেছি যে, কর্মচারীদের নিকট হইতে অর্থ প্রাপ্তির ষ্ট্যাম্পসহ রশিদ গ্রহন
                                করিয়া বেতন সহিত
                                সংযুক্ত করা হইয়াছে।<br>
                                ২. বিলের সাথে একটি অনুপস্থিতির তালিকা প্রদান করা হইল।<br>
                                ৩. প্রত্যয়ন করা যাইতেছে যে, এই কার্যালয়ের সকল নিয়োগ, স্থায়ী ও অস্থায়ী পদোন্নতি সংক্রান্ত
                                তথ্যাদি সংশ্লিষ্ট কর্মচারীগণের নিজ নিজ চাকুরী বহিতে আমার সত্যায়নে লিপিবদ্ধ হইয়াছে।<br>
                                ৪. প্রত্যায়ন করা যাইতেছে, চাকুরী বহিতে প্রাপ্য ছুটির হিসাব এবং প্রযোজ্য ছুটির বিধি
                                অনুয়ায়ী
                                প্রাপ্য ছুটি ছাড়া কাহাকেও কোন ছুটি মঞ্জুর করা হয় নাই। আমি নিশ্চিত যে তাহাদের ছুটি পাওনা
                                ছিল এবং সকল
                                ছুটির মঞ্জুরী ও ছুটিতে বা ছুটি হইতে ফিরিয়া আসা, সাময়িক কর্মচ্যুতি ও অন্য কাজে যাওয়া ও
                                অন্যান্য ঘটনা
                                নিয়ম
                                মোতাবেক চাকুরী বহিতে এবং ছুটির হিসাবে আমার সত্যায়নে লিপিবদ্ধ করা হইযাছে।<br>
                                ৫. প্রত্যায়ন করা যাইতেছে যে, যে সকল সরকারী কর্মচারীর নাম উলে­খ করা হয় নাই, কিন্তু এই
                                বিলে
                                বেতন দাবী করা হইয়াছে। চলতি মাসে তাহারা যথার্থই সরকারী চাকুরীতে নিয়োজিত ছিলেন।<br>
                                ৬. প্রত্যায়ন করা যাইতেছে যে, যে সকল সরকারী কর্মচারীর বাড়ী ভাড়া ভাতা এই বিলে দাবী করা
                                হইয়াছে,
                                তাহারা সরকারী কোন বাসস্থানে বসবাস করেন নাই।<br>
                                ৭. প্রত্যায়ন করা যাইতেছে, যে ক্ষেত্রে ছুটির/অস্থায়ী বদলী কালীন সময়ের জন্য ক্ষতিপূরণ ভাতা
                                দাবী
                                করা হইয়াছে, সেই ক্ষেত্রে কর্মচারীর একই বা স¦পদে ফিরিয়া আসার সম্ভাব্যতা ছুটি/অস্থায়ী
                                বদলীর মূল আদেশে
                                লিপিবদ্ধ করা হইয়াছে।<br>
                                ৮. প্রত্যায়ন করা যাইতেছে যে, কর্মচারীদের ছুটি কালীন বেতন, ছুটিতে যাওয়ার সময় যে হারে বেতন
                                গ্রহণ করিতেছিলেন, সেই হাওে দাবী করা হইয়াছে।<br>
                                ৯. প্রত্যায়ন করা যাইতেছে যে, অবসর গ্রহণ করিয়াছেন এমন কোন কর্মচারীর নাম এই বিলে
                                অন্তর্ভূক্ত
                                করা হয় নাই।<br>
                            </p>
                            <h3 class="text-center">অনুপস্থিত ব্যক্তির ফেরত দেওয়া বেতনের বিবরণ</h3>
                            <table class="table-bordered w-100">
                                <thead>
                                <tr>
                                    <th width="8%">সেকশন</th>
                                    <th width="50%">নাম</th>
                                    <th width="15%">সময়</th>
                                    <th width="27%">টাকার অংক (টা./পয়সা)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td rowspan="4"></td>
                                    <td rowspan="4"></td>
                                    <td class="text-right">
                                        বাজেটে বরাদ্দ =
                                    </td>
                                    <td class="text-right">
                                        <%=allocatedBudgetStr%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right">
                                        মোট খরচ =
                                    </td>
                                    <td class="text-right">
                                        <%=totalExpenditure%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right">
                                        অবশিষ্ট =
                                    </td>
                                    <td>
                                        <%=remainingBudgetStr%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <br>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                            <div class="row mt-4">
                                <div class="col-6">
                                    <div class="mt-1">
                                        <div class="fix-fill mt-4 w-100">
                                            স্থান
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            তারিখ
                                            <div class="blank-to-fill"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mt-1">
                                        <div class="fix-fill mt-4 w-100">
                                            আয়ন কর্মকর্তার স্বাক্ষর
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            নাম
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            পদবী
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-5 w-100">
                                            সীল
                                            <div class="blank-to-fill"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mt-3" style="border-top: 5px solid black;">
                                <div class="text-center mt-2">
                                    <h2>হিসাবরক্ষণ অফিসে ব্যবহারের জন্য</h2>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-3 fix-fill">
                                        টাকা
                                        <div class="blank-to-fill">
                                            &nbsp;&nbsp;<%=BangladeshiNumberFormatter
                                                .
                                                        getFormattedNumber
                                                                (
                                                                        Utils.getDigits(totalAllowanceAmount - totalDeductionAmount, "BANGLA")
                                                                )%>/-
                                        </div>
                                    </div>
                                    <div class="col-9 fix-fill">
                                        (কথায়)
                                        <div class="blank-to-fill">
                                            &nbsp;&nbsp;<%=BangladeshiNumberInWord
                                                .
                                                        convertToWord
                                                                (
                                                                        Utils.getDigits(totalAllowanceAmount - totalDeductionAmount, "BANGLA")
                                                                )%> টাকা
                                            (মাত্র)
                                        </div>
                                    </div>
                                    প্রদানের জন্য পাস কর হল
                                </div>

                                <div class="row mt-5">
                                    <div class="col-4">
                                        <div>
                                            <strong>অডিটর (স্বাক্ষর)</strong>
                                        </div>
                                        <div class="fix-fill mt-5">
                                            নাম........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                        </div>
                                        <div class="fix-fill mt-3">
                                            তাং........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div>
                                            <strong>সুপার (স্বাক্ষর)</strong>
                                        </div>
                                        <div class="fix-fill mt-5">
                                            নাম........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                        </div>
                                        <div class="fix-fill mt-3">
                                            তাং........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div>
                                            <strong>হিসাবরক্ষণ অফিসার (স্বাক্ষর)</strong>
                                        </div>
                                        <div class="fix-fill mt-5">
                                            নাম........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                        </div>
                                        <div class="fix-fill mt-3">
                                            তাং........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="page shadow" data-size="A4">
                            <%
                                for (Map.Entry<Long, List<AllowanceEmployeeInfoDTO>> entry : mapByOfficeUnitId.entrySet()) {
                                    Map<String, Long> mapByOrganogramEn = entry.getValue()
                                            .stream()
                                            .collect(Collectors.groupingBy(dto -> dto.organogramNameEn, Collectors.counting()));
                                    Map<String, String> mapforOrganogramBn = entry.getValue()
                                            .stream()
                                            .collect(Collectors.toMap(dto -> dto.organogramNameEn, dto -> dto.organogramNameBn, (e1, e2) -> e1));
                                    String headName = "";
                                    Long officeHeadOrganogramId = null;
                                    OfficeUnitOrganograms headOrganograms = OfficeUnitOrganogramsRepository.getInstance().getOfficeUnitHeadWithActiveEmpStatusAndNotConsiderSubstitute(entry.getKey());
                                    if (headOrganograms != null) {
                                        officeHeadOrganogramId = headOrganograms.id;
                                    }
                                    if (officeHeadOrganogramId != null) {
                                        headName = "মাননীয় " + OfficeUnitOrganogramsRepository.getInstance().getDesignation("BANGLA", officeHeadOrganogramId) + " ";
                                        headName += Employee_recordsRepository.getInstance().
                                                getById(EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(officeHeadOrganogramId).employeeRecordId).nameBng;
                                    }
                            %>
                            <div class="mt-4" style="padding-bottom: 50px">
                                <table class="table-bordered mt-2 w-100">
                                    <thead>
                                    <tr>
                                        <th colspan="5 " width="100%">
                                            <%=headName%>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="5 " width="100%">
                                            S...................... L .......................................... O
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <%
                                        long totalDesignation = 0;
                                        for (Map.Entry<String, Long> entry2 : mapByOrganogramEn.entrySet()) {
                                            totalDesignation += entry2.getValue();
                                    %>
                                    <tr>
                                        <td width="40%">
                                            <%=mapforOrganogramBn.get(entry2.getKey())%>
                                        </td>
                                        <td width="10%">
                                            X
                                        </td>
                                        <td width="10%">
                                            X
                                        </td>
                                        <td width="15%">
                                            <%=Utils.getDigits(entry2.getValue(), "BANGLA")%> জন
                                        </td>
                                        <td width="25%">
                                        </td>
                                    </tr>
                                    <%
                                        }
                                    %>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td width="40%">
                                            মোট
                                        </td>
                                        <td width="10%">
                                        </td>
                                        <td width="10%">
                                        </td>
                                        <td width="15%">
                                            <%=Utils.getDigits(totalDesignation, "BANGLA")%> জন
                                        </td>
                                        <td width="25%">
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <%
                                }
                            %>
                        </section>
                        <section class="page shadow" data-size="A4">
                            <div class="text-center">
                                <h1 style="font-weight:bold">বাংলাদেশ জাতীয় সংসদ সচিবালয়</h1>
                                <h2 style="display: inline-block; border-bottom: 1px solid black">অর্থ শাখা-১</h2>
                                <h3>
                                    <%=DateUtils.getMonthYear(monthYear, "BANGLA", "/")%> খ্রিঃ মাসের
                                    <%=budgetOfficeNameBn%>
                                    কার্যালয়ের
                                    কর্মচারীদের বেতন ও ভাতা বিল হইতে ১-৩২৩৭-০০-২১১১ বাড়ী ভাড়া কর্তনের বিবরণীঃ
                                </h3>
                            </div>
                            <div>
                                <div class="mt-4">
                                    <table class="table-bordered mt-2 w-100">
                                        <thead>
                                        <tr>
                                            <th>
                                                ক্রমিক
                                            </th>
                                            <th>
                                                নাম
                                            </th>
                                            <th>
                                                পদবী
                                            </th>
                                            <th>
                                                বাড়ী/বাসা নম্বর
                                            </th>
                                            <th>
                                                মূলবেতন
                                            </th>
                                            <th>
                                                বাড়ী ভাড়া কর্তন
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <%
                                            int houseCountIndex=1;
                                            long tableTotalSalary=0;
                                            long tableTotalDeduction=0;
                                            for(int i=0;i<month_billDTOS.size();i++){
                                                Payroll_month_bill_deductionDTO tmpDTO=mapForHouseRentDeduction.get(month_billDTOS.get(i).iD);
                                                if(tmpDTO.amount>0){
                                                    String name=allowanceEmployeeInfoDTOS.get(i).nameBn;
                                                    String designation=allowanceEmployeeInfoDTOS.get(i).organogramNameBn+","+allowanceEmployeeInfoDTOS.get(i).officeNameBn;
                                                    String houseAddress=tmpDTO.dataField;
                                                    long mainSalary=salaryMap.get(month_billDTOS.get(i).iD).amount;
                                                    long deduction=tmpDTO.amount;
                                                    tableTotalSalary+=mainSalary;
                                                    tableTotalDeduction+=deduction;
                                        %>
                                                <tr>
                                                    <td>
                                                        <%=Utils.getDigits(houseCountIndex,"BANGLA")%>
                                                    </td>
                                                    <td>
                                                        <%=name%>
                                                    </td>
                                                    <td>
                                                        <%=designation%>
                                                    </td>
                                                    <td>
                                                        <%=houseAddress%>
                                                    </td>
                                                    <td>
                                                        <%=Utils.getDigits(mainSalary,"BANGLA")%>
                                                    </td>
                                                    <td>
                                                        <%=Utils.getDigits(deduction,"BANGLA")%>
                                                    </td>
                                                </tr>
                                        <%
                                                    houseCountIndex++;
                                                }
                                        %>
                                        <%
                                            }
                                        %>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="4"></td>
                                            <td>
                                                <%=Utils.getDigits(tableTotalSalary,"BANGLA")%>
                                            </td>
                                            <td>
                                                <%=Utils.getDigits(tableTotalDeduction,"BANGLA")%>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="text-right" style="padding-right: 70px">
                                    কথায়ঃ <%=BangladeshiNumberInWord.convertToWord(Utils.getDigits(tableTotalDeduction,"BANGLA"))%> টাকা মাত্র
                                </div>
                            </div>
                        </section>
                    </div>

                    <div class="container to-print-font landscape" id="to-print-div-land">
                        <%
                            final int rowsPerPage = 4;
                            boolean firstOffice = true;
                            for (Map.Entry<Long, List<AllowanceEmployeeInfoDTO>> entry : mapByOfficeUnitId.entrySet()) {
                                int index = 0;
                                long totalSalary = 0;
                                long totalAllowance = 0;
                                long totalReduction = 0;
                                Map<Long, Long> officeAllowanceSumMap = new HashMap<>();
                                Map<Long, Long> officeDeductionSumMap = new HashMap<>();
                                boolean isFirstPage = (index == 0) && firstOffice;
                                if (firstOffice)
                                    firstOffice = false;
                                boolean officeHeadNameShown = false;
                                List<AllowanceEmployeeInfoDTO> employeeInOfficeList = entry.getValue();
                                while (index < employeeInOfficeList.size()) {

                        %>
                        <section class="page shadow" data-size="A4-landscape">
                            <%
                                if (isFirstPage) {
                            %>
                            <div class="text-center">
                                <h1 style="font-weight: bold">প্রিভিলেজ  কর্মকর্তা/কর্মচারীদের বেতন বিল </h1>
                                <h2 style="display: inline-block;"><%=budgetOfficeNameBn%></h2>
                            </div>
                            <%
                                }
                            %>
                            <div>
                                <div class="mt-4">
                                    <table class="table-bordered mt-2" style="max-width: 100% !important;">
                                        <thead>
                                        <tr style="height: 150%">
                                            <th>পদেও ক্রমিক নং</th>
                                            <th>সেরেস্তার শাখা ও পদসহ কর্মচারীদের নাম(পি,পি,এফ নং ও ডাক জীবন বীমা নং)
                                            </th>
                                            <th>সাকূল্য বেতন-৩২১১১০৯</th>
                                            <th>দিন</th>
                                            <%
                                                for (Payroll_allowance_configurationModel allowance_configurationModel : allowance_configurationModels) {
                                            %>
                                            <th><%=allowance_configurationModel.name%>
                                            </th>
                                            <%
                                                }
                                            %>
                                            <th>বেতন ও ভাতার মোট দাবী(ক)</th>
                                            <th colspan="<%=deduction_configurationModels.size()+1%>">কর্তন ও আদায়</th>
                                            <th>নীট দাবী(ক-খ)</th>
                                            <th>প্রাপ্ত রশিদ</th>
                                            <th>স্বাক্ষর</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <%=budgetOfficeNameBn%>
                                                কার্যালয়ে
                                                কর্মরত কর্মকর্তা এবং ৩য় ও ৪র্থ শ্রেণীর
                                                কর্মচারী <%=convertToBanNumber(String.valueOf(month_billDTOS.size()))%>
                                                জন
                                                (<%=DateUtils.getMonthYear(monthYear, "BANGLA", "/")%>)
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <%
                                                for (Payroll_allowance_configurationModel allowance_configurationModel : allowance_configurationModels) {
                                            %>
                                            <td></td>
                                            <%
                                                }
                                            %>
                                            <td></td>
                                            <%
                                                for (Tax_deduction_configurationModel tax_deduction_configurationModel : deduction_configurationModels) {
                                                    String[] codePart = tax_deduction_configurationModel.code.split("-");
                                            %>
                                            <td>
                                                <%=tax_deduction_configurationModel.name%>
                                                <%
                                                    for (int i = 0; i < codePart.length; i++) {
                                                %>
                                                <%=(i + 1) == codePart.length ? codePart[i] : codePart[i] + "-"%>
                                                <%
                                                    }
                                                %>
                                            </td>
                                            <%
                                                }
                                            %>
                                            <td>মোট কর্তন ও আদায়(খ)</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <%
                                                int colIndex = 1;
                                            %>
                                            <td><%=Utils.getDigits(colIndex++, "Bangla")%>
                                            </td>
                                            <td>
                                                <%=Utils.getDigits(colIndex++, "Bangla")%>
                                            </td>
                                            <td><%=Utils.getDigits(colIndex++, "Bangla")%>
                                            </td>
                                            <td><%=Utils.getDigits(colIndex++, "Bangla")%>
                                            </td>
                                            <%
                                                for (Payroll_allowance_configurationModel allowance_configurationModel : allowance_configurationModels) {
                                            %>
                                            <td><%=Utils.getDigits(colIndex++, "Bangla")%>
                                            </td>
                                            <%
                                                }
                                            %>
                                            <td><%=Utils.getDigits(colIndex++, "Bangla")%>
                                            </td>
                                            <%
                                                for (Tax_deduction_configurationModel tax_deduction_configurationModel : deduction_configurationModels) {
                                            %>
                                            <td>
                                                <%=Utils.getDigits(colIndex++, "Bangla")%>
                                            </td>
                                            <%
                                                }
                                            %>
                                            <td><%=Utils.getDigits(colIndex++, "Bangla")%>
                                            </td>
                                            <td><%=Utils.getDigits(colIndex++, "Bangla")%>
                                            </td>
                                            <td><%=Utils.getDigits(colIndex++, "Bangla")%>
                                            </td>
                                            <td><%=Utils.getDigits(colIndex++, "Bangla")%>
                                            </td>
                                        </tr>
                                        <%
                                            if (!officeHeadNameShown) {
                                                String headName = "";
                                                Long officeHeadOrganogramId = null;
                                                OfficeUnitOrganograms headOrganograms = OfficeUnitOrganogramsRepository.getInstance().getOfficeUnitHeadWithActiveEmpStatusAndNotConsiderSubstitute(entry.getKey());
                                                if (headOrganograms != null) {
                                                    officeHeadOrganogramId = headOrganograms.id;
                                                }
                                                if (officeHeadOrganogramId != null) {
                                                    headName = "মাননীয় " + OfficeUnitOrganogramsRepository.getInstance().getDesignation("BANGLA", officeHeadOrganogramId) + " ";
                                                    headName += Employee_recordsRepository.getInstance().
                                                            getById(EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(officeHeadOrganogramId).employeeRecordId).nameBng;
                                                }
                                                headName += " এর কার্যালয়ে কর্মরত ব্যক্তিগত কর্মচারীদের বেতন ও ভাতা";
                                        %>
                                        <tr>
                                            <td style="text-align: center;font-weight: bold"
                                                colspan="<%=9+allowance_configurationModels.size()+deduction_configurationModels.size()%>"><%=headName%></td>
                                        </tr>
                                        <%
                                            }
                                        %>
                                        <%
                                            int rowsInThisPage = 0;
                                            while (index < employeeInOfficeList.size() && rowsInThisPage < rowsPerPage) {
                                                rowsInThisPage++;
                                                long rowtotalAllowance = 0;
                                                long rowtotalDeduction = 0;
                                                long allowanceEmployeeInfoId = entry.getValue().get(index++).iD;
                                                long payRollId = mapByAllowanceEmployeeId.get(allowanceEmployeeInfoId);
                                                List<Payroll_month_bill_allowanceDTO> rowAllowanceDTOs = allowanceDTOMap.get(payRollId);
                                                List<Payroll_month_bill_deductionDTO> rowDeductionDTOs = deductionDTOMap.get(payRollId);
                                                Map<Long, Integer> rowAllowanceMap = rowAllowanceDTOs.stream()
                                                        .collect(Collectors.toMap(dto -> dto.payrollAllowanceConfigurationId, dto -> dto.amount, (e1, e2) -> e1));
                                                Map<Long, Integer> rowDeductionMap = rowDeductionDTOs.stream()
                                                        .collect(Collectors.toMap(dto -> dto.taxDeductionConfigurationId, dto -> dto.amount, (e1, e2) -> e1));
                                        %>
                                        <tr>
                                            <td><%=Utils.getDigits(index, "BANGLA")%></td>
                                            <td>
                                                জনাব <%=AllowanceEmployeeInfoRepository.getInstance().getById(allowanceEmployeeInfoId).nameBn%><br><%=AllowanceEmployeeInfoRepository.getInstance().getById(allowanceEmployeeInfoId).organogramNameBn%></td>
                                            <%
                                                long rowSalary = salaryMap.get(payRollId) != null ? salaryMap.get(payRollId).amount : 0;
                                                totalSalary += rowSalary;
                                                rowtotalAllowance += rowSalary;
                                                totalAllowance += rowSalary;
                                            %>
                                            <td><%=Utils.getDigits(rowSalary > 0 ? String.valueOf(rowSalary) : "", "Bangla")%></td>
                                            <td></td>
                                            <%
                                                for (Payroll_allowance_configurationModel allowance_configurationModel : allowance_configurationModels) {
                                                    long allowanceVal = rowAllowanceMap.get(Long.parseLong(allowance_configurationModel.configurationId));
                                                    long tmp = officeAllowanceSumMap.get(Long.parseLong(allowance_configurationModel.configurationId)) == null ? 0 : officeAllowanceSumMap.get(Long.parseLong(allowance_configurationModel.configurationId));
                                                    officeAllowanceSumMap.put(Long.valueOf(allowance_configurationModel.configurationId), tmp + allowanceVal);
                                                    rowtotalAllowance += allowanceVal;
                                                    totalAllowance += allowanceVal;
                                                    String showAllowanceVal = allowanceVal > 0 ? Utils.getDigits(allowanceVal, "BANGLA") : "";
                                            %>
                                            <td><%=showAllowanceVal%></td>
                                            <%
                                                }
                                            %>
                                            <td><%=Utils.getDigits(rowtotalAllowance, "BANGLA")%></td>
                                            <%
                                                for (Tax_deduction_configurationModel tax_deduction_configurationModel : deduction_configurationModels) {
                                                    long deductionVal = rowDeductionMap.get(Long.parseLong(tax_deduction_configurationModel.configurationId));
                                                    long tmp = officeDeductionSumMap.get(Long.parseLong(tax_deduction_configurationModel.configurationId)) == null ? 0 : officeDeductionSumMap.get(Long.parseLong(tax_deduction_configurationModel.configurationId));
                                                    officeDeductionSumMap.put(Long.valueOf(tax_deduction_configurationModel.configurationId), tmp + deductionVal);
                                                    rowtotalDeduction += deductionVal;
                                                    totalReduction += deductionVal;
                                                    String showDeductionVal = deductionVal > 0 ? Utils.getDigits(deductionVal, "BANGLA"): "";
                                            %>
                                            <td><%=showDeductionVal%></td>
                                            <%
                                                }
                                            %>
                                            <td><%=Utils.getDigits(rowtotalDeduction, "BANGLA")%></td>
                                            <td><%=Utils.getDigits(rowtotalAllowance - rowtotalDeduction, "BANGLA")%></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <%
                                            }
                                        %>
                                        </tbody>
                                        <%
                                            if(index>=employeeInOfficeList.size()){
                                        %>
                                        <tfoot style="background-color: #b6b3b3">
                                        <tr>
                                            <td></td>
                                            <td style="font-weight: bold;text-align: right">(১) &nbsp;&nbsp;উপ মোট</td>
                                            <td><%=Utils.getDigits(totalSalary, "BANGLA")%></td>
                                            <td></td>
                                            <%
                                                for (Payroll_allowance_configurationModel allowance_configurationModel : allowance_configurationModels) {
                                                    long tmp = officeAllowanceSumMap.get(Long.parseLong(allowance_configurationModel.configurationId)) == null ? 0 : officeAllowanceSumMap.get(Long.parseLong(allowance_configurationModel.configurationId));
                                            %>
                                            <td><%=Utils.getDigits(tmp, "BANGLA")%></td>
                                            <%
                                                }
                                            %>
                                            <td><%=Utils.getDigits(totalAllowance, "BANGLA")%></td>
                                            <%
                                                for (Tax_deduction_configurationModel tax_deduction_configurationModel : deduction_configurationModels) {
                                                    long tmp = officeDeductionSumMap.get(Long.parseLong(tax_deduction_configurationModel.configurationId)) == null ? 0 : officeDeductionSumMap.get(Long.parseLong(tax_deduction_configurationModel.configurationId));
                                            %>
                                            <td><%=Utils.getDigits(tmp, "BANGLA")%></td>
                                            <%
                                                }
                                            %>
                                            <td><%=Utils.getDigits(totalReduction, "BANGLA")%></td>
                                            <td><%=Utils.getDigits(totalAllowance - totalReduction, "BANGLA")%></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        </tfoot>
                                        <%
                                            }
                                        %>
                                    </table>
                                </div>
                            </div>
                        </section>
                        <%
                            }
                        %>
                        <%
                            }
                        %>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function downloadTemplatesAsPdf() {
        downloadTemplateAsPdf('to-print-div-pot', '<%=pdfFileName%> - 1', 'portrait');
        downloadTemplateAsPdf('to-print-div-land', '<%=pdfFileName%> - 2', 'landscape');
    }

    function downloadTemplateAsPdf(divId, fileName, orientation = 'portrait') {
        let content = document.getElementById(divId);
        const opt = {
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4', orientation: orientation}
        };
        html2pdf().from(content).set(opt).save();
    }
</script>