<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="payroll_month_bill.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@page pageEncoding="UTF-8" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="allowance_employee_info.AllowanceEmployeeInfoDTO" %>
<%@ page import="allowance_employee_info.AllowanceEmployeeInfoRepository" %>

<%
    String navigator2 = "navPAYROLL_MONTH_BILL";
    String servletName = "Payroll_month_billServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th>
                <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_MONTHYEAR, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_EMPLOYEERECORDSID, loginDTO)%>
            </th>
            <th>
                <%=getDataByLanguage(
                        Language,
                        "মোবাইল নাম্বার",
                        "Mobile Number"
                )%>
            </th>
            <th>
                <%=getDataByLanguage(
                        Language,
                        "সঞ্চয়ী হিসাব নাম্বার",
                        "Savings Account Number"
                )%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<Payroll_month_billDTO>) rn2.list;

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Payroll_month_billDTO payroll_month_billDTO = (Payroll_month_billDTO) data.get(i);


        %>
        <tr>
            <td>
                <%=DateUtils.getMonthYear(payroll_month_billDTO.monthYear, Language)%>
            </td>
            <td>
                <%
                    AllowanceEmployeeInfoDTO employeeInfoDTO =
                            AllowanceEmployeeInfoRepository.getInstance()
                                    .getById(payroll_month_billDTO.allowanceEmployeeInfoId);

                %>
                <%=getDataByLanguage(Language, employeeInfoDTO.nameBn, employeeInfoDTO.nameEn)%>
                <br>
                <b>
                    <%=getDataByLanguage(Language, employeeInfoDTO.organogramNameBn, employeeInfoDTO.organogramNameEn)%>
                </b>
                <br>
                <%=getDataByLanguage(Language, employeeInfoDTO.officeNameBn, employeeInfoDTO.officeNameEn)%>
            </td>

            <td>
                <%=Utils.getDigits(employeeInfoDTO.mobileNumber, Language)%>
            </td>

            <td>
                <%=Utils.getDigits(employeeInfoDTO.savingAccountNumber, Language)%>
            </td>


        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			