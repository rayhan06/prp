<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="payroll_month_bill.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="employee_records.EmploymentEnum" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="tax_deduction_configuration.Tax_deduction_configurationModel" %>
<%@ page import="tax_deduction_configuration.Tax_deduction_configurationRepository" %>
<%@ page import="payroll_allowance_configuration.Payroll_allowance_configurationModel" %>
<%@ page import="payroll_allowance_configuration.Payroll_allowance_configurationRepository" %>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    actionName = request.getParameter("actionType").equals("edit") ? "edit" : "add";
    String formTitle = LM.getText(LC.PAYROLL_MONTH_BILL_ADD_PAYROLL_MONTH_BILL_ADD_FORMNAME, loginDTO);
    String context = request.getContextPath() + "/";
    List<Integer> filterEmployment = CatRepository.getInstance().getCategoryLanguageModelList("employment").stream()
                                                  .filter(dto -> dto.categoryValue != EmploymentEnum.PRIVILEGED.getValue())
                                                  .map(dto -> dto.categoryValue)
                                                  .collect(Collectors.toList());
    List<Payroll_allowance_configurationModel> allowance_configurationModels =
            Payroll_allowance_configurationRepository.getInstance()
                                                     .getActiveModels(EmploymentEnum.PRIVILEGED.getValue(), Language);
    List<Tax_deduction_configurationModel> deduction_configurationModels =
            Tax_deduction_configurationRepository.getInstance()
                                                 .getActiveModels(EmploymentEnum.PRIVILEGED.getValue(), Language);
    int totalColumns = 5 + allowance_configurationModels.size() + deduction_configurationModels.size() + 2;
    int PRIVILEGED_EMPLOYEE_CAT = EmploymentEnum.PRIVILEGED.getValue();
    long houseRentDeductionId=Tax_deduction_configurationRepository.getInstance().getHouseRentDeductionId();
%>

<style>
    .template-row {
        display: none;
    }

    input[type="text"] {
        min-width: 20ch;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal" id="bill-form" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12 row mt-2">
                        <div class="col-lg-3 col-md-6 form-group">
                            <label class="h5" for="employmentCat">
                                <%=LM.getText(LC.PAYROLL_MONTH_BILL_ADD_EMPLOYMENTCAT, loginDTO)%>
                            </label>
                            <input type="text" class='form-control' readonly
                                   value="<%=CatRepository.getName(Language, "employment", PRIVILEGED_EMPLOYEE_CAT)%>">
                            <input type='hidden' name='employmentCat' id="employmentCat"
                                   value='<%=PRIVILEGED_EMPLOYEE_CAT%>'>
                        </div>
                        <div class="col-lg-3 col-md-6 form-group">
                            <label class="h5" for="budgetInstitutionalGroup">
                                <%=LM.getText(LC.BUDGET_INSTITUTIONAL_GROUP, loginDTO)%>
                            </label>
                            <select id="budgetInstitutionalGroup"
                                    name='budgetInstitutionalGroupId'
                                    class='form-control rounded shadow-sm'
                                    onchange="institutionalGroupChanged(this);"
                            >
                                <%=Budget_institutional_groupRepository.getInstance().buildOptions(
                                        Language, 0L, false
                                )%>
                            </select>
                        </div>

                        <div class="col-lg-3 col-md-6 form-group">
                            <label class="h5" for="budgetOffice">
                                <%=LM.getText(LC.BUDGET_OFFICE, loginDTO)%>
                            </label>
                            <select id="budgetOffice" name='budgetOfficeId'
                                    class='form-control rounded shadow-sm'
                                    onchange="budgetOfficeChanged(this);">
                                <%--Dynamically Added with AJAX--%>
                            </select>
                        </div>

                        <div class="col-lg-3 col-md-6 form-group">
                            <label class="h5" for="budgetOffice">
                                <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_MONTHYEAR, loginDTO)%>
                            </label>
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="monthYear_js"/>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                <jsp:param name="HIDE_DAY" value="true"/>
                            </jsp:include>
                            <input type='hidden' name='monthYear' id='monthYear' value=''>
                        </div>
                    </div>
                </div>

                <div class="mt-4 table-responsive">
                    <table id="allowance-table" class="table text-nowrap table-bordered table-striped">
                        <thead>
                        <tr id="dynamic-header-1">
                            <th rowspan="2">
                                <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_EMPLOYEERECORDSID, loginDTO)%>
                            </th>
                            <th rowspan="2">
                                <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_ORGANOGRAMID, loginDTO)%>
                            </th>
                            <th rowspan="2">
                                <%=getDataByLanguage(
                                        Language,
                                        "জাতীয় পরিচয় পত্রের নম্বর",
                                        "NID Number"
                                )%>
                            </th>
                            <th rowspan="2">
                                <%=getDataByLanguage(
                                        Language,
                                        "মোবাইল নাম্বার",
                                        "Mobile Number"
                                )%>
                            </th>
                            <th rowspan="2">
                                <%=getDataByLanguage(
                                        Language,
                                        "সঞ্চয়ী হিসাব নাম্বার",
                                        "Savings Account Number"
                                )%>
                            </th>
                            <th colspan=<%=allowance_configurationModels.size()%> class="text-center">
                                <%=getDataByLanguage(
                                        Language,
                                        "ভাতা",
                                        "Allowances"
                                )%>
                            </th>
                            <th colspan=<%=deduction_configurationModels.size()%> class="text-center">
                                <%=getDataByLanguage(
                                        Language,
                                        "কর্তন ও আদায়",
                                        "Deduction and Collection"
                                )%>
                            </th>
                            <th rowspan="2">
                                <%=getDataByLanguage(
                                        Language,
                                        "মোট দাবী",
                                        "Total Amount"
                                )%>
                            </th>
                            <th rowspan="2">
                                <%=getDataByLanguage(
                                        Language,
                                        "নীট দাবী",
                                        "Net Amount"
                                )%>
                            </th>
                        </tr>
                        <tr id="dynamic-header-2">
                            <%
                                for (Payroll_allowance_configurationModel model : allowance_configurationModels) {
                            %>
                            <th>
                                <%=model.name%>
                            </th>
                            <%
                                }
                            %>
                            <%
                                for (Tax_deduction_configurationModel model : deduction_configurationModels) {
                            %>
                            <th>
                                <%=model.name%>
                            </th>
                            <%
                                }
                            %>
                        </tr>
                        </thead>
                        <tbody class="main-tbody">
                        </tbody>

                        <%--don't put these tr inside tbody--%>
                        <tr class="loading-gif" style="display: none;">
                            <td class="text-center" colspan="100%">
                                <img alt="" class="loading"
                                     src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
                                <span><%=getDataByLanguage(Language, "লোড হচ্ছে...", "Loading...")%></span>
                            </td>
                        </tr>

                        <tr class="template-row" id="template-row">
                            <input type="hidden" name="employeeRecordsId" value="-1">
                            <input type="hidden" name="payrollMonthBillId" value="-1">
                            <td class="row-data-name"></td>
                            <td class="row-data-organogramName"></td>
                            <td class="row-data-nid"></td>
                            <td class="row-data-mobileNumber"></td>
                            <td class="row-data-savingAccountNumber"></td>
                            <%
                                for (Payroll_allowance_configurationModel model : allowance_configurationModels) {
                            %>
                            <td class="row-data-allowance_<%=model.configurationId%>">
                            </td>
                            <%
                                }
                            %>
                            <%
                                for (Tax_deduction_configurationModel model : deduction_configurationModels) {
                            %>
                            <td class="row-data-deduction_<%=model.configurationId%>">
                            </td>
                            <%
                                }
                            %>
                            <td class="row-data-totalAmount"></td>
                            <td class="row-data-netAmount"></td>
                        </tr>
                    </table>
                </div>

                <div class="row">
                    <div class="col-12 mt-3 text-right">
                        <button id="prepareBill-btn"
                                class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                                type="button" onclick="submitForm('prepareBill')">
                            <%=getDataByLanguage(Language, "বিল প্রস্তুত করুন", "Prepare Bill")%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<%@include file="../common/table-sum-utils.jsp" %>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<script type="text/javascript">
    let actionName = 'add';
    let isAlreadyAddedGlobal = false;
    let totalColumn = <%=totalColumns%>;
    const isLangEng = '<%=Language%>'.toLowerCase() == 'english';

    function showOrHideLoadingGif(tableId, toShow) {
        const loadingGif = $('#' + tableId + ' tr.loading-gif');
        if (toShow) {
            document.querySelector('#' + tableId + ' tbody.main-tbody').innerHTML = '';
            document.querySelectorAll('#' + tableId + ' tfoot').forEach(tfoot => tfoot.remove());
            loadingGif.show();
        } else loadingGif.hide();
    }

    function clearOffice() {
        document.getElementById('budgetOffice').innerHTML = '';
    }

    function clearMonthYear() {
        resetDateById('monthYear_js');
    }

    function clearTable() {
        const tableBody = document.querySelector('#allowance-table tbody.main-tbody');
        const emptyTableText = '<%=getDataByLanguage(Language, "কোন তথ্য পাওয়া যায়নি", "No data found")%>';
        tableBody.innerHTML = '<tr class="text-center"><td colspan="100%">'
            + emptyTableText + '</td></tr>';

        document.querySelectorAll('#allowance-table tfoot')
                .forEach(tfoot => tfoot.remove());
    }

    function clearNextLevels(startIndex) {
        const toClearFunctions = [
            clearOffice, clearMonthYear, clearTable
        ];
        for (let i = startIndex; i < toClearFunctions.length; i++) {
            toClearFunctions[i]();
        }
    }

    function employmentCatChanged() {
        //clearNextLevels(0);
        //TODO:any action if needed
    }

    async function institutionalGroupChanged(selectElement) {
        const selectedInstitutionalGroupId = selectElement.value;
        clearNextLevels(0);
        if (selectedInstitutionalGroupId === '') return;
        const url = 'Budget_mappingServlet?actionType=getBudgetOfficeList&withCode=false'
            + '&budget_instituitional_group_id=' + selectedInstitutionalGroupId;

        const response = await fetch(url);
        document.getElementById('budgetOffice').innerHTML = await response.text();
    }

    function budgetOfficeChanged() {
        clearNextLevels(1);
    }

    function createHeader1(allowanceSize, deductionSize) {
        const tableHeader1 = document.getElementById('dynamic-header-1');
        tableHeader1.innerText = '';
        let th = document.createElement('th');
        th.rowSpan = 2;
        th.innerText = '<%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_EMPLOYEERECORDSID, loginDTO)%>';
        tableHeader1.append(th);
        th = document.createElement('th');
        th.rowSpan = 2;
        th.innerText = '<%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_ORGANOGRAMID, loginDTO)%>';
        tableHeader1.append(th);
        th = document.createElement('th');
        th.rowSpan = 2;
        th.innerText = '<%=getDataByLanguage(
                                        Language,
                                        "জাতীয় পরিচয় পত্রের নম্বর",
                                        "NID Number")%>';
        tableHeader1.append(th);
        th = document.createElement('th');
        th.rowSpan = 2;
        th.innerText = '<%=getDataByLanguage(
                    Language,
                    "মোবাইল নাম্বার",
                    "Mobile Number"
            )%>';
        tableHeader1.append(th);
        th = document.createElement('th');
        th.rowSpan = 2;
        th.innerText = '<%=getDataByLanguage(
                    Language,
                    "সঞ্চয়ী হিসাব নাম্বার",
                    "Savings Account Number"
            )%>';
        tableHeader1.append(th);

        th = document.createElement('th');
        th.colSpan = allowanceSize;
        th.className = "text-center"
        th.innerText = '<%=getDataByLanguage(
                    Language,
                    "ভাতা",
                    "Allowances"
            )%>';
        tableHeader1.append(th);

        th = document.createElement('th');
        th.colSpan = deductionSize;
        th.className = "text-center"
        th.innerText = '<%=getDataByLanguage(
                    Language,
                    "কর্তন ও আদায়",
                    "Deduction and Collection"
            )%>';
        tableHeader1.append(th);

        tableHeader1.append(th);
        th = document.createElement('th');
        th.rowSpan = 2;
        th.innerText = '<%=getDataByLanguage(
                    Language,
                    "মোট দাবী",
                    "Total Amount"
            )%>';
        tableHeader1.append(th);
        tableHeader1.append(th);
        th = document.createElement('th');
        th.rowSpan = 2;
        th.innerText = '<%=getDataByLanguage(
                    Language,
                    "নীট দাবী",
                    "Net Amount"
            )%>';
        tableHeader1.append(th);
    }

    function createHeader2(allowanceModels, deductionModels) {
        const tableHeader2 = document.getElementById('dynamic-header-2');
        tableHeader2.innerText = ''
        for (let allowance of allowanceModels) {
            const th = document.createElement('th');
            th.innerText = allowance.name;
            tableHeader2.append(th);
        }
        for (let deduction of deductionModels) {
            const th = document.createElement('th');
            th.innerText = deduction.name;
            tableHeader2.append(th);
        }
    }

    function createTemplateRow(allowanceModels, deductionModels) {
        const templateRow = document.getElementById('template-row');
        templateRow.innerText = '';
        let input = document.createElement('input');
        input.hidden = true;
        input.name = "employeeRecordsId";
        input.value = "-1"
        templateRow.append(input);
        input = document.createElement('input');
        input.hidden = true;
        input.name = "payrollMonthBillId";
        input.value = "-1"
        templateRow.append(input);
        let td = document.createElement('td');
        td.className = "row-data-name";
        templateRow.append(td);
        td = document.createElement('td');
        td.className = "row-data-organogramName";
        templateRow.append(td);
        td = document.createElement('td');
        td.className = "row-data-nid";
        templateRow.append(td);
        td = document.createElement('td');
        td.className = "row-data-mobileNumber";
        templateRow.append(td);
        td = document.createElement('td');
        td.className = "row-data-savingAccountNumber";
        templateRow.append(td);
        for (let allowance of allowanceModels) {
            td = document.createElement('td');
            td.className = "row-data-allowance_" + allowance.configurationId;
            templateRow.append(td);
        }
        for (let deduction of deductionModels) {
            td = document.createElement('td');
            td.className = "row-data-deduction_" + deduction.configurationId;
            templateRow.append(td);
        }
        td = document.createElement('td');
        td.className = "row-data-totalAmount";
        templateRow.append(td);
        td = document.createElement('td');
        td.className = "row-data-netAmount";
        templateRow.append(td);
    }

    async function monthYearChanged(monthYear) {
        const budgetOfficeId = document.getElementById('budgetOffice').value;
        const employmentCat = document.getElementById('employmentCat').value;
        if (budgetOfficeId === '') return;

        showOrHideLoadingGif('allowance-table', true);

        const url = 'Payroll_month_billServlet?actionType=ajax_getMonthPayrollData'
            + '&monthYear=' + monthYear
            + '&budgetOfficeId=' + budgetOfficeId
            + '&employmentCat=' + employmentCat;
        const response = await fetch(url);

        const {isAlreadyAdded, monthBillModels, allowanceModels, deductionModels} = await response.json();
        console.log("isAlreadyAdded", isAlreadyAdded);
        console.log("monthBillModels", monthBillModels);
        console.log(allowanceModels)
        console.log(deductionModels)
        console.log(monthBillModels)

        if (isAlreadyAdded)
            actionName = 'edit';
        else
            actionName = 'add';
        isAlreadyAddedGlobal = isAlreadyAdded;
        const tableBody = document.querySelector('#allowance-table tbody');
        const templateRow = document.querySelector('#allowance-table tr.template-row');

        const allowanceLength = allowanceModels.length;
        const deductionLength = deductionModels.length;
        totalColumn = 5 + allowanceLength + deductionLength + 2;
        createHeader1(allowanceLength, deductionLength);
        createHeader2(allowanceModels, deductionModels);
        createTemplateRow(allowanceModels, deductionModels);
        showOrHideLoadingGif('allowance-table', false);
        if (monthBillModels.length === 0) {
            clearTable();
        } else {
            let idx = 0
            for (let model of monthBillModels) {
                showModelInTable(tableBody, templateRow, model, idx)
                idx++;
            }
            // monthBillModels.forEach(model => );
            calculateAllRowTotal();
        }
    }

    function keyDownEvent(e) {
        $(e.target).data('previousValue', $(e.target).val());
        let isvalid = inputValidationForIntValue(e, $(this), null);
        return true == isvalid;
    }

    function onKeyUp(e) {
        const previousData = $(e.target).data('previousValue');
        const currentData = $(e.target).val();
        const element = document.getElementById(e.target.id).parentElement.parentElement;
        const netAmountElement = element.querySelector('.row-data-netAmount');
        netAmountElement.innerText = Number(netAmountElement.innerText) + Number(previousData) - Number(currentData);
        calculateAllRowTotal();
    }

    function getInputElement(id, name, value, onKeyUp) {
        const inputElement = document.createElement('input');
        inputElement.classList.add('form-control');
        inputElement.type = 'text';
        inputElement.id = id;
        inputElement.name = name;
        inputElement.value = value;
        inputElement.onkeydown = keyDownEvent;
        if (onKeyUp) inputElement.onkeyup = onKeyUp;
        return inputElement;
    }

    function getTextAreaElement(id, name, value) {
        const inputElement = document.createElement('textarea');
        inputElement.classList.add('form-control');
        inputElement.placeholder=isLangEng?'House Address':'বাড়ির ঠিকানা';
        inputElement.rows = 3;
        inputElement.style.textAlign='left';
        inputElement.style.resize='none';
        inputElement.style.width='100%';
        inputElement.id = id;
        inputElement.name = name;
        inputElement.value = value;
        return inputElement;
    }

    function setRowData(templateRow, payrollMonthBillModel, allowanceMap, deductionMap, idx) {
        const rowDataPrefix = 'row-data-';

        for (const key in payrollMonthBillModel) {
            const td = templateRow.querySelector('.' + rowDataPrefix + key);
            if (!td) continue;
            td.innerText = payrollMonthBillModel[key];
        }
        allowanceMap.forEach((val, key) => {
            const td = templateRow.querySelector('.' + rowDataPrefix + 'allowance_' + key);
            if (td) {
                td.innerText = val;
            }
        });
        deductionMap.forEach((val, key) => {
            const td = templateRow.querySelector('.' + rowDataPrefix + 'deduction_' + key);
            if (td) {
                if (isAlreadyAddedGlobal) {
                    if(val.amount>0) {
                        td.innerText = val.amount;
                        if (key ==<%=houseRentDeductionId%>) {
                            td.innerText += '\n' + val.dataField;
                        }
                    }
                } else {
                    td.append(getInputElement('deduction_' + key + '_' + idx, 'deduction_' + key, val.amount, onKeyUp));
                    if(key==<%=houseRentDeductionId%>){
                        td.append(getTextAreaElement('deduction_'+key+'_'+'address'+'_'+idx,'deduction_'+key+'_'+'address',''));
                    }
                }

            }
        });
    }

    function showModelInTable(tableBody, templateRow, payrollMonthBillModel, idx) {
        const modelRow = templateRow.cloneNode(true);
        modelRow.classList.remove('template-row');
        modelRow.querySelector('input[name="employeeRecordsId"]').value = payrollMonthBillModel.employeeRecordsId;
        modelRow.querySelector('input[name="payrollMonthBillId"]').value = payrollMonthBillModel.payrollMonthBillId;
        const allowanceMap = new Map();
        const deductionMap = new Map();
        payrollMonthBillModel.allowanceDTOList.forEach(dto => {
            allowanceMap.set(dto.payrollAllowanceConfigurationId, dto.amount)
        });
        payrollMonthBillModel.deductionDTOList.forEach(dto => {
            deductionMap.set(dto.taxDeductionConfigurationId, dto)
        });
        setRowData(modelRow, payrollMonthBillModel, allowanceMap, deductionMap, idx);
        tableBody.append(modelRow);
    }

    function calculateAllRowTotal() {
        const colIndicesToSum = [totalColumn - 2, totalColumn - 1];
        const totalTitleColSpan = 1;
        const totalTitle = '<%=getDataByLanguage(Language, "সর্বমোট", "Total")%>';
        setupTotalRow('allowance-table', totalTitle, totalTitleColSpan, colIndicesToSum);
    }

    function setButtonDisableState(value) {
        $('#prepareBill-btn').prop('disabled', value);
    }

    const form = $('#bill-form');

    function submitForm(source) {
        if (!form.valid()) return;

        setButtonDisableState(true);
        $.ajax({
            type: "POST",
            url: "Payroll_month_billServlet?actionType=ajax_" + actionName + "&source=" + source,
            data: form.serialize(),
            dataType: 'JSON',
            success: function (response) {
                if (response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                    setButtonDisableState(false);
                } else if (response.responseCode === 200) {
                    window.location.assign(getContextPath() + response.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                setButtonDisableState(false);
            }
        });
    }

    function init() {
        $('#monthYear_js').on('datepicker.change', (event, param) => {
            const isValidDate = dateValidator('monthYear_js', true);
            const monthYear = getDateStringById('monthYear_js');
            if (isValidDate) {
                $('#monthYear').val(monthYear);
                monthYearChanged(monthYear);
            } else {
                clearTable();
            }
        });
        form.validate({
            rules: {
                employmentCat: "required",
                budgetInstitutionalGroupId: "required",
                budgetOfficeId: "required",
                monthYear: "required"
            },
            messages: {
                employmentCat: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                budgetInstitutionalGroupId: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                budgetOfficeId: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                monthYear: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>'
            }
        })
    }

    $(document).ready(function () {
        init();
    });
</script>