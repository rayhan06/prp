<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%

    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.TICKET_TYPE_REPORT_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>
<div class="">
    <div class="row mx-2">
        <div class="search-criteria-div col-md-6">
            <div class="form-group row">
                <label class="col-md-3 col-form-label text-md-right">
                    <%=LM.getText(LC.SUPPORT_TICKET_ADD_TICKETISSUESTYPE, loginDTO)%>
                </label>
                <div class="col-md-9">
                    <select class='form-control' name='ticketIssuesType' id='ticketIssuesType'
                            onchange='getSubType("ticketIssuesSubtypeType", this.value)'>
                        <%
                            Options = CommonDAO.getOptions(Language, "ticket_issues", CatDTO.CATDEFAULT);
                        %>
                        <%=Options%>
                    </select>
                </div>
            </div>
        </div>
        <div id="doctorType" class="search-criteria-div col-md-6">
            <div class="form-group row">
                <label class="col-md-3 col-form-label text-md-right">
                    <%=LM.getText(LC.SUPPORT_TICKET_ADD_TICKETISSUESSUBTYPETYPE, loginDTO)%>
                </label>
                <div class="col-md-9">
                    <select class='form-control' name='ticketIssuesSubtypeType' id='ticketIssuesSubtypeType'>
                        <%
                            Options = CommonDAO.getOptions(Language, "ticket_issues_subtype", CatDTO.CATDEFAULT);
                        %>
                        <%=Options%>
                    </select>

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <%@include file="../pbreport/yearmonth.jsp" %>
        </div>
        <div class="col-md-6">
            <%@include file="../pbreport/calendar.jsp" %>
        </div>
    </div>
</div>
<script type="text/javascript">
    function init() {
        dateTimeInit($("#Language").val());
    }

    function PreprocessBeforeSubmiting() {
    }

    function getSubType(childElement, value) {
        console.log("getting Element " + value);

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById(childElement).innerHTML = this.responseText;

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        console.log("selected value = " + value);

        xhttp.open("POST", "Support_ticketServlet?actionType=getSubType&type="
            + value + "&language=<%=Language%>", true);
        xhttp.send();


    }
</script>