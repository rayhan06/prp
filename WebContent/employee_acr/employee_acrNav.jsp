<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="common.RoleEnum" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="pb.Utils" %>
<%@ page import="pb.OptionDTO" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="util.StringUtils" %>
<%@ page import="java.util.Calendar" %>
<%@page pageEncoding="UTF-8" %>
<%
    String url = "Employee_acrServlet?actionType=search";
%>
<%@include file="../pb/navInitializer.jsp" %>
<%
    boolean isAdmin = navUserDTO.roleID == RoleEnum.ADMIN.getRoleId() || navUserDTO.roleID == RoleEnum.HR_ADMIN.getRoleId();
    List<OptionDTO> yearOptionDTOList = new ArrayList<>();
    Calendar calendar=Calendar.getInstance();

    int startYear = 1970;
    int endYear = calendar.get(Calendar.YEAR)+1;
    for (int x = endYear; x >= startYear; x--) {
        OptionDTO optionDTO = new OptionDTO(String.valueOf(x), StringUtils.convertToBanNumber(String.valueOf(x)), String.valueOf(x));
        yearOptionDTOList.add(optionDTO);
    }
    boolean isLangEng = Language.equalsIgnoreCase("English");
%>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <div class="ml-1">
            <div class="row">
                <%
                    if (isAdmin) {
                %>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=UtilCharacter.getDataByLanguage(Language, "অফিস", "Office")%>
                        </label>
                        <div class="col-md-8 col-xl-9 col-form-label">
                            <%--Office Select Modal Button--%>
                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                    id="officeUnit_modal_button">
                                <%=UtilCharacter.getDataByLanguage(Language, "অফিস খুঁজুন", "Search Office")%>
                            </button>
                            <div class="input-group mr-1" id="office_units_id_div" style="display: none">
                                <input type="hidden" id='office_units_id' name="office_units_id" value='' tag='pb_html'>
                                <button type="button" class="btn btn-secondary btn-block shadow-sm btn-border-radius form-control"
                                        id="office_units_id_text" onclick="officeModalEditButtonClicked()">
                                </button>
                                <span class="input-group-btn" tag='pb_html'>
                                <button type="button" class="btn btn-outline-danger"
                                        id='office_units_id_crs_btn' tag='pb_html'>
									x
								</button>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label" for="onlySelectedOffice">
                            <%=isLangEng ? "Only Selected Office" : "শুধুমাত্র নির্বাচিত অফিস"%>
                        </label>
                        <div class="col-md-8 col-xl-9 col-form-label" id='onlySelectedOffice_div'>
                            <input type='checkbox' class='form-control-sm mt-1' name='onlySelectedOffice'
                                   id='onlySelectedOffice'
                                   onchange="this.value = this.checked;" value='false' tag='pb_html'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=UtilCharacter.getDataByLanguage(Language, "কর্মকর্তা", "Employee")%>
                        </label>
                        <div class="col-md-8 col-xl-9 col-form-label">
                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                    id="search_emp_acr_modal_button">
                                <%=UtilCharacter.getDataByLanguage(Language, "কর্মকর্তা খুঁজুন", "Search Employee")%>
                            </button>
                        </div>
                        <input type="hidden" id='employee_records_id' name="employee_records_id" value='' tag='pb_html'>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped text-nowrap" style="margin-top: 50px;">
                                <thead></thead>
                                <tbody id="search_emp_acr_table">
                                <tr style="display: none;">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <button type="button" class="btn btn-sm delete-trash-btn"
                                                onclick="remove_containing_row(this,'search_emp_acr_table');">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <%
                    }
                %>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"
                               for="status_cat"><%=LM.getText(LC.EMPLOYEE_ACR_ADD_STATUS, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='status_cat' id='status_cat'>
                                <%=CatRepository.getInstance().buildOptions("status", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=UtilCharacter.getDataByLanguage(Language, "এসিআর হতে শুরু", "ACR From Start")%>
                        </label>
                        <div class="col-md-8 col-xl-9 col-form-label">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="acr_from_start_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type='hidden' id="acr_from_start" name="acr_from_start" value='' tag='pb_html'/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=UtilCharacter.getDataByLanguage(Language, "এসিআর হতে শেষ", "ACR From End")%>
                        </label>
                        <div class="col-md-8 col-xl-9 col-form-label">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="acr_from_end_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type='hidden' id="acr_from_end" name="acr_from_end" value='' tag='pb_html'/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=UtilCharacter.getDataByLanguage(Language, "এসিআর পর্যন্ত শুরু", "ACR To Start")%>
                        </label>
                        <div class="col-md-8 col-xl-9 col-form-label">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="acr_to_start_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type='hidden' id="acr_to_start" name="acr_to_start" value='' tag='pb_html'/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=UtilCharacter.getDataByLanguage(Language, "এসিআর পর্যন্ত শেষ", "ACR To End")%>
                        </label>
                        <div class="col-md-8 col-xl-9 col-form-label">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="acr_to_end_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type='hidden' id="acr_to_end" name="acr_to_end" value='' tag='pb_html'/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label" for="is_partial">
                            <%=LM.getText(LC.EMPLOYEE_ACR_ADD_YEAR, loginDTO)%>
                        </label>
                        <div class="col-md-8 col-xl-9 col-form-label" id='year_div'>
                            <select class='form-control rounded'
                                    name='year' id='acr_year' tag='pb_html'>
                                <%=Utils.buildOptions(yearOptionDTOList, Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label" for="is_partial">
                            <%=LM.getText(LC.EMPLOYEE_ACR_ADD_PARTIAL_ACR, loginDTO)%>
                        </label>
                        <div class="col-md-8 col-xl-9 col-form-label" id='is_partial_div'>
                            <input type='checkbox' class='form-control-sm mt-1' name='is_partial'
                                   id='is_partial'
                                   onchange="this.value = this.checked;" value='false' tag='pb_html'>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->
<!-- End: search control -->

<%@include file="../common/pagination_with_go2.jsp" %>
<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">
    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        var params = 'AnyField=' + document.getElementById('anyfield').value;


        $("#acr_from_start").val(getDateStringById("acr_from_start_date_js"));
        if ($('#acr_from_start').val()) {
            params += '&acr_from_start=' + getBDFormattedDate('acr_from_start');
        }
        $("#acr_from_end").val(getDateStringById("acr_from_end_date_js"));
        if ($('#acr_from_start').val()) {
            params += '&acr_from_end=' + getBDFormattedDate('acr_from_end');
        }

        $("#acr_to_start").val(getDateStringById("acr_to_start_date_js"));
        if ($('#acr_to_start').val()) {
            params += '&acr_to_start=' + getBDFormattedDate('acr_to_start');
        }
        $("#acr_to_end").val(getDateStringById("acr_to_end_date_js"));
        if ($('#acr_to_end').val()) {
            params += '&acr_to_end=' + getBDFormattedDate('acr_to_end');
        }
        if ($("#office_units_id").val()) {
            params += '&office_units_id=' + $("#office_units_id").val();
        }
        if ($("#employee_records_id").val()) {
            params += '&employee_records_id=' + $("#employee_records_id").val();
        }
        params += "&onlySelectedOffice=" + document.getElementById('onlySelectedOffice').value;
        params += "&is_partial=" + (document.getElementById('is_partial').value === 'true' ? '1' : '0');
        params += "&status_cat=" + document.getElementById('status_cat').value;
        params += "&year=" + document.getElementById('acr_year').value;
        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }

        console.log(selectedOffice);

        $('#officeUnit_modal_button').hide();
        $('#office_units_id_div').show();

        $('#office_units_id_div').show();

        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id').val(selectedOffice.id);

        //fetchDesignation(selectedOffice.id);
    }

    $('#office_units_id_crs_btn').on('click', function () {
        $('#officeUnit_modal_button').show();
        $('#office_units_id_div').hide();

        $('#office_units_id').val('');
        document.getElementById('office_units_id_text').innerHTML = '';
        document.getElementById('rank_cat').innerHTML = '';
    });

    // Office Select Modal
    // modal trigger button
    // this part is needed because if one may have more than one place to select office
    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnit', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    $('#officeUnit_modal_button').on('click', function () {
        officeSelectModalUsage = 'officeUnit';
        $('#search_office_modal').modal();
    });

    function officeModalEditButtonClicked() {
        officeSelectModalUsage = 'officeUnit';
        officeSearchSetSelectedOfficeLayers($('#office_units_id').val());
        $('#search_office_modal').modal();
    }


    <%--Search Emp ACR--%>
    search_emp_acr_map = new Map();
    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map([
        ['search_emp_acr_table', {
            info_map: search_emp_acr_map,
            isSingleEntry: true,
            callBackFunction: function (empInfo) {
                console.log('callBackFunction called with latest added emp info JSON');
                console.log(empInfo);
                $('#employee_records_id').val(empInfo.employeeRecordId);
            }
        }]
    ]);
    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#search_emp_acr_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'search_emp_acr_table';
        $('#search_emp_modal').modal();
    });

    function remove_containing_row(button, table_name) {
        // td > button id = "<employee record id>_td_button"
        let td_button = button.parentNode;
        let employee_record_id = td_button.id.split("_")[0];

        let added_info_map = table_name_to_collcetion_map.get(table_name).info_map;
        added_info_map.delete(employee_record_id);
        console.log('Employee to Remove', employee_record_id);
        console.log('Map After Removal', added_info_map);

        let containing_row = button.parentNode.parentNode;
        // console.log('containing_row.remove()');
        containing_row.remove();
        $('#employee_records_id').val('');
    }

    $(document).ready(() => {
        select2SingleSelector('#status_cat', '<%=Language%>');
        select2SingleSelector('#acr_year', '<%=Language%>');
    });
</script>

