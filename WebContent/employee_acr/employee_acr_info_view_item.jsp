<%@page import="pb.CatRepository" %>
<%@ page import="util.StringUtils" %>
<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>

<td style="vertical-align: middle;text-align: left"><%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(employee_acrDTO.year))%>
</td>
<td style="vertical-align: middle;text-align: left"><%=CatRepository.getInstance().getText(Language, "status", employee_acrDTO.status)%>
</td>
<td style="vertical-align: middle;text-align: left"><%=StringUtils.getFormattedDate(Language, employee_acrDTO.acrFrom)%>
</td>
<td style="vertical-align: middle;text-align: left"><%=StringUtils.getFormattedDate(Language, employee_acrDTO.acrTo)%>
</td>
<td style="vertical-align: middle;text-align: left"><%=StringEscapeUtils.escapeHtml4(employee_acrDTO.remarks)%>
</td>
<%
    if (!actionType.equals("viewSummary")) {
%>
<td style="text-align: center; vertical-align: middle;">
    <form action="Employee_acrServlet?isPermanentTable=true&actionType=delete&tab=3&ID=<%=employee_acrDTO.iD%>&empId=<%=ID%>&userId=<%=request.getParameter("userId")%>"
          method="POST" id="acrTableForm<%=acrIndex%>" enctype="multipart/form-data">

        <div class="btn-group" role="group" aria-label="Basic example">
            <button style="max-height: 30px" class="btn-primary" type="button"
                    title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_VIEW_DETAILS, loginDTO) %>"
                    onclick="location.href='Employee_acrServlet?actionType=view&ID=<%=employee_acrDTO.iD%>&empId=<%=employee_acrDTO.employeeRecordsId%>&userId=<%=request.getParameter("userId")%>'">
                <i class="fa fa-eye"></i></button>&nbsp;
            <button style="max-height: 30px" class="btn-success" title="Edit" type="button"
                    onclick="location.href='Employee_acrServlet?actionType=getEditPage&ID=<%=employee_acrDTO.iD%>&empId=<%=ID%>&tab=3&userId=<%=request.getParameter("userId")%>'">
                <i
                        class="fa fa-edit"></i></button>&nbsp;
            <button style="max-height: 30px" class="btn-danger" title="Delete" type="button"
                    onclick="deleteItem('acrTableForm',<%=acrIndex%>)"><i
                    class="fa fa-trash"></i></button>
        </div>
    </form>
</td>
<%
    }
%>