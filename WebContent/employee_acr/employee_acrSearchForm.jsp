<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="employee_acr.*" %>
<%@ page import="java.util.List" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@page pageEncoding="UTF-8" %>

<%
    String navigator2 = "";
    String servletName = "Employee_acrServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>

<%
    List<Employee_acrDTO> data = (List<Employee_acrDTO>) rn2.list;
    if (data != null && data.size() > 0) {
%>
<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead class="text-nowrap">
        <tr class="text-nowrap">
            <th><%=UtilCharacter.getDataByLanguage(Language,
                    "কর্মকর্তার নাম", "Employee Name")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language,
                    "কর্মকর্তার অফিস", "Employee Office")%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_ACR_ADD_YEAR, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_ACR_ADD_ACRFROM, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_ACR_ADD_ACRTO, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_ACR_ADD_STATUS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_ACR_ADD_REMARKS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_ACR_REPORT_WHERE_ISPARTIAL, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_ACR_SEARCH_EMPLOYEE_ACR_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="text-center">
                <span><%="English".equalsIgnoreCase(Language) ? "All" : "সকল"%></span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody class="text-nowrap">
        <%
            for (Employee_acrDTO employee_acrDTO : data) {
        %>
        <tr>
            <td>
                <%=Employee_recordsRepository.getInstance().getEmployeeName(employee_acrDTO.employeeRecordsId, Language)%>
            </td>
            <td>
                <%
                    Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employee_acrDTO.officeUnitId);
                    String officeName = "";
                    if (officeUnitsDTO != null) {
                        officeName = ((isLanguageEnglish) ? officeUnitsDTO.unitNameEng : officeUnitsDTO.unitNameBng);
                    }
                %>
                <%=officeName%>
            </td>
            <td>
                <%=isLanguageEnglish ? employee_acrDTO.year : StringUtils.convertToBanNumber(String.valueOf(employee_acrDTO.year))%>
            </td>
            <td>
                <%=StringUtils.getFormattedDate(Language, employee_acrDTO.acrFrom)%>
            </td>
            <td>
                <%=StringUtils.getFormattedDate(Language, employee_acrDTO.acrTo)%>
            </td>

            <td>
                <%=CatRepository.getInstance().getText(Language, "status", employee_acrDTO.status) %>
            </td>

            <td>
                <%=StringEscapeUtils.escapeHtml4(employee_acrDTO.remarks)%>
            </td>
            <td>
                <%=CatRepository.getInstance().getText(Language, "is_partial", employee_acrDTO.isPartial ? 1 : 0)%>
            </td>
            <td>
                <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
                        onclick="location.href='Employee_acrServlet?actionType=view&ID=<%=employee_acrDTO.iD%>&empId=<%=employee_acrDTO.employeeRecordsId%>'">
                    <i class="fa fa-eye"></i>
                </button>

            </td>

            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #ff6b6b;"
                        onclick="location.href='Employee_acrServlet?actionType=getEditPage&ID=<%=employee_acrDTO.iD%>&empId=<%=employee_acrDTO.employeeRecordsId%>'"
                >
                    <i class="fa fa-edit"></i>
                </button>
            </td>

            <td>
                <div class='checker text-right'>
                    <span class='chkEdit'><input type='checkbox' name='ID' value='<%=employee_acrDTO.iD%>'/></span>
                </div>
            </td>
        </tr>
        <% } %>
        </tbody>
    </table>
</div>
<%
} else {
%>
<label style="width: 100%;text-align: center;font-size: larger;font-weight: bold;color: red">
    <%=isLanguageEnglish ? "No information is found" : "কোন তথ্য পাওয়া যায় নি"%>
</label>
<%
    }
%>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>