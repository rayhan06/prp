<%@page import="employee_acr.Employee_acrDTO" %>
<%@page import="java.util.List" %>

<input type="hidden" data-ajax-marker="true">

<table class="table table-striped table-bordered" style="font-size: 14px">
    <thead>
    <tr>
        <th style="vertical-align: middle;text-align: left"><b><%= LM.getText(LC.EMPLOYEE_ACR_ADD_YEAR, loginDTO) %></b></th>
        <th style="vertical-align: middle;text-align: left"><b><%= LM.getText(LC.EMPLOYEE_ACR_ADD_STATUS, loginDTO) %></b></th>
        <th style="vertical-align: middle;text-align: left"><b><%= LM.getText(LC.EMPLOYEE_ACR_ADD_ACRFROM, loginDTO) %></b></th>
        <th style="vertical-align: middle;text-align: left"><b><%= LM.getText(LC.EMPLOYEE_ACR_ADD_ACRTO, loginDTO) %></b></th>
        <th style="vertical-align: middle;text-align: left"><b><%= LM.getText(LC.EMPLOYEE_ACR_ADD_REMARKS, loginDTO) %></b></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <%
        List<Employee_acrDTO> employeeAcrDtos = (List<Employee_acrDTO>) request.getAttribute("employeeAcrDtos");
        int acrIndex = 0;
        if (employeeAcrDtos != null && employeeAcrDtos.size() > 0) {
            for (Employee_acrDTO employee_acrDTO : employeeAcrDtos) {
                ++acrIndex;
    %>
    <tr>
        <%@include file="/employee_acr/employee_acr_info_view_item.jsp" %>
    </tr>
    <% }
    } %>
    </tbody>
</table>
<div class="row">
    <div class="col-12 text-right mt-3">
        <button class="btn btn-gray m-t-10 rounded-pill"
                onclick="location.href='Employee_acrServlet?actionType=getAddPage&empId=<%=ID%>&tab=3&userId=<%=request.getParameter("userId")%>'">
            <i class="fa fa-plus"></i>&nbsp; <%=LM.getText(LC.ACR_ADD_ACR, loginDTO)%>
        </button>
    </div>
</div>