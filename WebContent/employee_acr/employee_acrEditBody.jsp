<%@page import="employee_acr.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="files.FilesDTO" %>
<%@ page import="dbm.DBMW" %>
<%@ page import="util.StringUtils" %>
<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@ page import="common.BaseServlet" %>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    Employee_acrDTO employee_acrDTO;
    String formTitle;
    if (isEditActionType) {
        formTitle = LM.getText(LC.EMPLOYEE_ACR_EDIT_EMPLOYEE_ACR_EDIT_FORMNAME, loginDTO);
        employee_acrDTO = (Employee_acrDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    } else {
        formTitle = LM.getText(LC.EMPLOYEE_ACR_ADD_EMPLOYEE_ACR_ADD_FORMNAME, loginDTO);
        employee_acrDTO = new Employee_acrDTO();
    }

    int year = Calendar.getInstance().get(Calendar.YEAR);
    List<OptionDTO> yearOptionDTOList = new ArrayList<>();
    OptionDTO optionDTO;
    optionDTO = new OptionDTO("Year", "বছর", "0");
    yearOptionDTOList.add(optionDTO);
    Date curDate = new Date();
    int startYear = 1900;
    int endYear = curDate.getYear() + 1900;
    for (int x = endYear; x >= startYear; x--) {
        optionDTO = new OptionDTO(String.valueOf(x), StringUtils.convertToBanNumber(String.valueOf(x)), String.valueOf(x));
        yearOptionDTOList.add(optionDTO);
    }
    //Employee_acrDonebyDAO employee_acrDonebyDAO = new Employee_acrDonebyDAO();
    //List<Employee_acrDonebyDTO> employee_acrDonebyDTOS = employee_acrDonebyDAO.getByForeignKey(String.valueOf(employee_acrDTO.iD));
    long empId;
    if (request.getParameter("empId") == null) {
        empId = userDTO.employee_record_id;
    }else{
        empId = Long.parseLong(request.getParameter("empId"));
    }
    String url = "Employee_acrServlet?actionType=" + actionName + "&isPermanentTable=true&empId=" + empId+"&iD="+employee_acrDTO.iD;
    if (request.getParameter("tab") != null) {
        url += "&tab=" + request.getParameter("tab")+"&userId="+request.getParameter("userId");
    }
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i><%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form" id="acrForm" action="<%=url%>">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10  offset-md-1">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-10  offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right" for="year_select2_<%=i%>">
                                            <%=LM.getText(LC.EMPLOYEE_ACR_ADD_YEAR, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='year_div_<%=i%>'>
                                            <select class='form-control yearSelection rounded reve-datepicker'
                                                    name='year' id='year_select2_<%=i%>' tag='pb_html'>
                                                <%=Utils.buildOptionsWithoutSelectWithSelectId(yearOptionDTOList, Language, String.valueOf(employee_acrDTO.year))%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMPLOYEE_ACR_ADD_ACRFROM, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='acrFrom_div_<%=i%>'>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="acr-from-date-js"/>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                            </jsp:include>

                                            <input type='hidden' class='form-control' id='acr-from-date'
                                                   name='acrFrom' value=''
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMPLOYEE_ACR_ADD_ACRTO, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='acrTo_div_<%=i%>'>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="acr-to-date-js"/>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                                <jsp:param name="END_YEAR" value="<%=year + 5%>"/>
                                            </jsp:include>
                                            <input type='hidden' class='form-control' id='acr-to-date'
                                                   name='acrTo' value=''
                                                   tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                               for="isPartial_checkbox">
                                            <%=LM.getText(LC.EMPLOYEE_ACR_ADD_PARTIAL_ACR, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='isPartialDiv'>
                                            <input type='checkbox' class='form-control-sm mt-1' name='isPartial'
                                                   id='isPartial_checkbox'
                                                   value='true' <%=(isEditActionType && employee_acrDTO.isPartial) ? ("checked"):""%> >
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right" for="remarks_textarea_<%=i%>">
                                            <%=LM.getText(LC.EMPLOYEE_ACR_ADD_REMARKS, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='remarks_div_<%=i%>'>
                                            <textarea rows="4" style="resize: none" class='form-control rounded-lg'
                                                      name='remarks' id='remarks_textarea_<%=i%>' wrap="soft"
                                                      maxlength="1024"
                                                      form="acrForm"
                                                      tag='pb_html'><%=employee_acrDTO.remarks != null ? StringEscapeUtils.escapeHtml4(employee_acrDTO.remarks) : ""%></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_FILESDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9 table-responsive" id='filesDropzone_div_<%=i%>'>
                                            <%
                                                if (isEditActionType) {
                                                    List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(employee_acrDTO.filesDropzone);
                                            %>
                                            <table>
                                                <tr>
                                                    <%
                                                        if (filesDropzoneDTOList != null) {
                                                            for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                                                FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                                                byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                    %>
                                                    <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                                        <%
                                                            if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                        %>
                                                        <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                             style='width:100px'/>
                                                        <%
                                                            }
                                                        %>
                                                        <a href='Employee_acrServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                           download><%=filesDTO.fileTitle%>
                                                        </a>
                                                        <a class='btn btn-danger'
                                                           onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>
                                                    </td>
                                                    <%
                                                            }
                                                        }
                                                    %>
                                                </tr>
                                            </table>
                                            <%
                                                }
                                            %>

                                            <%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>
                                            <div class="dropzone"
                                                 action="Employee_education_infoServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=isEditActionType?employee_acrDTO.filesDropzone:ColumnID%>">
                                                <input type='file' style="display:none" name='filesDropzoneFile'
                                                       id='filesDropzone_dropzone_File_<%=i%>' tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='filesDropzoneFilesToDelete'
                                                   id='filesDropzoneFilesToDelete_<%=i%>' value=''
                                                   tag='pb_html'/>
                                            <input type='hidden' name='filesDropzone'
                                                   id='filesDropzone_dropzone_<%=i%>'
                                                   tag='pb_html'
                                                   value='<%=isEditActionType?employee_acrDTO.filesDropzone:ColumnID%>'/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-11 text-right">
                        <button id="cancel-btn" type="button" class="btn-sm shadow text-white border-0 cancel-btn"
                                onclick="location.href = '<%=request.getHeader("referer")%>'">
                            <%=LM.getText(LC.EMPLOYEE_ACR_ADD_EMPLOYEE_ACR_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                onclick="submitForm()">
                            <%=LM.getText(LC.EMPLOYEE_ACR_ADD_EMPLOYEE_ACR_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">
    const form = $("#acrForm");

    $(document).ready(function () {
        $.validator.addMethod('yearSelection', function (value, element) {
            return value != 0;
        });

        form.validate({
            rules: {
                year: {
                    required: true,
                    yearSelection: true
                }
            },
            messages: {
                year: "<%=LM.getText(LC.EMPLOYEE_ACR_ADD_ENTER_YEAR, loginDTO)%>"
            }
        });

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        $('#acr-from-date-js').on('datepicker.change', () => {
            setMinDateById('acr-to-date-js', getDateStringById('acr-from-date-js'));
        });

        <%if(isEditActionType) {%>
        $("#year").val('<%=employee_acrDTO.year%>');
        setDateByTimestampAndId('acr-from-date-js', <%=employee_acrDTO.acrFrom%>);
        setDateByTimestampAndId('acr-to-date-js', <%=employee_acrDTO.acrTo%>);
        <%}%>
    });

    function isFromValid() {
        $('#acr-from-date').val(getDateStringById('acr-from-date-js'));
        $('#acr-to-date').val(getDateStringById('acr-to-date-js'));
        const jQueryValid = form.valid();
        const fromDateValid = dateValidator('acr-from-date-js', true, {
            'errorEn': '<%=LM.getInstance().getText("English", LC.EMPLOYEE_ACR_ADD_ENTER_VALID_DATE)%>',
            'errorBn': '<%=LM.getInstance().getText("Bangla", LC.EMPLOYEE_ACR_ADD_ENTER_VALID_DATE)%>'
        });
        const toDateValid = dateValidator('acr-to-date-js', true, {
            'errorEn': '<%=LM.getInstance().getText("English", LC.EMPLOYEE_ACR_ADD_ENTER_VALID_DATE)%>',
            'errorBn': '<%=LM.getInstance().getText("Bangla", LC.EMPLOYEE_ACR_ADD_ENTER_VALID_DATE)%>'
        });
        return jQueryValid && fromDateValid && toDateValid;
    }

    function submitForm(){
        buttonStateChange(true);
        if(isFromValid()){
            submitAjaxForm('acrForm');
        }else{
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value){
        $('#submit-btn').prop('disabled',value);
        $('#cancel-btn').prop('disabled',value);
    }
</script>


<style>
    .acr-cross-picker {
        height: 34px;
        width: 41px;
        border: 1px solid #c2cad8;
    }

    .input-width89 {
        width: 92%;
        float: left;
    }

    .hide-calendar .ui-datepicker-calendar {
        display: none;
    }
</style>