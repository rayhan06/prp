<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="employee_acr.*" %>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="files.*" %>
<%@ page import="util.StringUtils" %>
<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    Employee_acrDTO employee_acrDTO = (Employee_acrDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.EMPLOYEE_ACR_ADD_EMPLOYEE_ACR_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background-color: white"><%=LM.getText(LC.EMPLOYEE_ACR_ADD_EMPLOYEE_ACR_ADD_FORMNAME, loginDTO)%></h4>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped text-nowrap">
                                            <tr>
                                                <td style="width:35%">
                                                    <b><%=LM.getText(LC.EMPLOYEE_ACR_ADD_YEAR, loginDTO)%>
                                                    </b></td>
                                                <td>
                                                    <%=isLangEng?employee_acrDTO.year:StringUtils.convertToBanNumber(String.valueOf(employee_acrDTO.year))%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:35%">
                                                    <b><%=LM.getText(LC.EMPLOYEE_ACR_REPORT_WHERE_ISPARTIAL, loginDTO)%>
                                                    </b></td>
                                                <td><%=CatRepository.getInstance().getText(Language, "is_partial", employee_acrDTO.isPartial ? 1 : 0)%>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="width:35%">
                                                    <b><%=LM.getText(LC.EMPLOYEE_ACR_ADD_ACRFROM, loginDTO)%>
                                                    </b></td>
                                                <td>
                                                    <%=StringUtils.getFormattedDate(Language, employee_acrDTO.acrFrom)%>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="width:35%">
                                                    <b><%=LM.getText(LC.EMPLOYEE_ACR_ADD_ACRTO, loginDTO)%>
                                                    </b></td>
                                                <td><%=StringUtils.getFormattedDate(Language, employee_acrDTO.acrTo)%>
                                                </td>
                                            </tr>

                                            <tr>

                                                <td style="width:35%">
                                                    <b><%=LM.getText(LC.EMPLOYEE_ACR_REPORT_WHERE_ACR_SUBMISSION_DATE, loginDTO)%>
                                                    </b></td>
                                                <td><%=StringUtils.getFormattedDate(Language, employee_acrDTO.insertionDate)%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:35%">
                                                    <b><%=LM.getText(LC.EMPLOYEE_ACR_ADD_REMARKS, loginDTO)%>
                                                    </b></td>
                                                <td><%=StringEscapeUtils.escapeHtml4(employee_acrDTO.remarks)%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="data-view-heading">
                                                    <b>
                                                        <%=LM.getText(LC.FILES_SEARCH_ANYFIELD, loginDTO)%>
                                                    </b>
                                                </td>
                                                <td colspan="3">
                                                    <table>
                                                        <tr>
                                                            <%
                                                                List<FilesDTO> filesDropzoneDTOList = new FilesDAO().getMiniDTOsByFileID(employee_acrDTO.filesDropzone);
                                                            %>
                                                            <table>
                                                                <tr>
                                                                    <%
                                                                        if (filesDropzoneDTOList != null) {
                                                                            for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                                                                FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                                                                byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                                    %>
                                                                    <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                                                        <%
                                                                            if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                                        %>
                                                                        <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                                             style='width:100px'/>
                                                                        <%
                                                                            }
                                                                        %>
                                                                        <a href='Employee_acrServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                                           download>
                                                                            <%=filesDTO.fileTitle%>
                                                                        </a>
                                                                    </td>
                                                                    <%
                                                                            }
                                                                        }
                                                                    %>
                                                                </tr>
                                                            </table>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>