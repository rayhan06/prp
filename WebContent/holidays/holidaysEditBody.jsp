<%@page import="holidays.HolidaysDTO" %>
<%@page import="language.LC" %>

<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>


<%
    HolidaysDTO holidaysDTO;
    holidaysDTO = (HolidaysDTO) request.getAttribute("holidaysDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (holidaysDTO == null) {
        holidaysDTO = new HolidaysDTO();

    }
    System.out.println("holidaysDTO = " + holidaysDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle;
    if (actionName.equals("edit")) {
        formTitle = LM.getText(LC.HOLIDAYS_EDIT_HOLIDAYS_EDIT_FORMNAME, loginDTO);
    } else {
        formTitle = LM.getText(LC.HOLIDAYS_ADD_HOLIDAYS_ADD_FORMNAME, loginDTO);
    }

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

%>


<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%>
        </h3>
    </div>
    <div class="box-body">
        <form class="form-horizontal" action="HolidaysServlet?actionType=<%=actionName%>&identity=<%=ID%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="form-body">


                <%
                    String Language = LM.getText(LC.HOLIDAYS_EDIT_LANGUAGE, loginDTO);
                    String Options;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    Date date = new Date();
                    String datestr = dateFormat.format(date);
                %>


                <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>' value='<%=holidaysDTO.iD%>'
                       tag='pb_html'/>


                <label class="col-lg-3 control-label">
                    <%=(actionName.equals("edit")) ? (LM.getText(LC.HOLIDAYS_EDIT_HOLIDAYDATE, loginDTO)) : (LM.getText(LC.HOLIDAYS_ADD_HOLIDAYDATE, loginDTO))%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='holidayDate_div_<%=i%>'>
                        <input type='date' class='form-control' name='holidayDate_Date_<%=i%>'
                               id='holidayDate_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_holidayDate = new SimpleDateFormat("dd-MM-yyyy");
	String formatted_holidayDate = format_holidayDate.format(new Date(holidaysDTO.holidayDate));
	%>
                                       '<%=formatted_holidayDate%>'
                        <%
                        } else {
                        %>
                        '<%=datestr%>'
                        <%
                            }
                        %>
                        tag='pb_html'>
                        <input type='hidden' class='form-control' name='holidayDate' id='holidayDate_date_<%=i%>'
                               value=<%=actionName.equals("edit")?("'" + holidaysDTO.holidayDate + "'"):("'" + "0" + "'")%>  tag='pb_html'>

                    </div>
                </div>


                <label class="col-lg-3 control-label">
                    <%=(actionName.equals("edit")) ? (LM.getText(LC.HOLIDAYS_EDIT_YEAR, loginDTO)) : (LM.getText(LC.HOLIDAYS_ADD_YEAR, loginDTO))%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='year_div_<%=i%>'>
                        <input type='number' class='form-control' name='year' id='year_number_<%=i%>' min='1900'
                               value=<%=actionName.equals("edit")?("'" + holidaysDTO.year + "'"):("'" + 1900 + "'")%>  tag='pb_html'>

                    </div>
                </div>


                <label class="col-lg-3 control-label">
                    <%=(actionName.equals("edit")) ? (LM.getText(LC.HOLIDAYS_EDIT_DESCRIPTION, loginDTO)) : (LM.getText(LC.HOLIDAYS_ADD_DESCRIPTION, loginDTO))%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='description_div_<%=i%>'>
                        <input type='text' class='form-control' name='description' id='description_text_<%=i%>'
                               value=<%=actionName.equals("edit")?("'" + holidaysDTO.description + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                    </div>
                </div>


                <input type='hidden' class='form-control' name='isDeleted' id='isDeleted_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + holidaysDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>


                <input type='hidden' class='form-control' name='lastModificationTime'
                       id='lastModificationTime_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + holidaysDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>


                <div class="form-actions text-center">
                    <a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
                        <%
                            if (actionName.equals("edit")) {
                        %>
                        <%=LM.getText(LC.HOLIDAYS_EDIT_HOLIDAYS_CANCEL_BUTTON, loginDTO)%>
                        <%
                        } else {
                        %>
                        <%=LM.getText(LC.HOLIDAYS_ADD_HOLIDAYS_CANCEL_BUTTON, loginDTO)%>
                        <%
                            }

                        %>
                    </a>
                    <button class="btn btn-success" type="submit">
                        <%
                            if (actionName.equals("edit")) {
                        %>
                        <%=LM.getText(LC.HOLIDAYS_EDIT_HOLIDAYS_SUBMIT_BUTTON, loginDTO)%>
                        <%
                        } else {
                        %>
                        <%=LM.getText(LC.HOLIDAYS_ADD_HOLIDAYS_SUBMIT_BUTTON, loginDTO)%>
                        <%
                            }
                        %>
                    </button>
                </div>

            </div>

        </form>

    </div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        }
        else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }

        preprocessDateBeforeSubmitting('holidayDate', row);

        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "HolidaysServlet");
    }

    function init(row) {


    }

    var row = 0;
    bkLib.onDomLoaded(function () {
    });

    window.onload = function () {
        init(row);
    }

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






