
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="holidays.HolidaysDTO"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>

<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>





<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.HOLIDAYS_EDIT_LANGUAGE, loginDTO);


String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>	

<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.HOLIDAYS_EDIT_HOLIDAYDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.HOLIDAYS_EDIT_YEAR, loginDTO)%></th>
								<th><%=LM.getText(LC.HOLIDAYS_EDIT_DESCRIPTION, loginDTO)%></th>
															
							</tr>
						</thead>
						<tbody>
						<%
								ArrayList data = (ArrayList) session.getAttribute("holidaysDTOs");

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											System.out.println("In jsp, parsed dto = " + data.get(i));
											HolidaysDTO holidaysDTO =  (HolidaysDTO)data.get(i);
											long  ID = holidaysDTO.iD;
											out.println("<tr id = 'tr_" + i + "'>");
						%>



























	
















			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=holidaysDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_holidayDate'>")%>
			
	
	<div class="form-inline" id = 'holidayDate_div_<%=i%>'>
		<input type='date' class='form-control'  name='holidayDate_Date_<%=i%>' id = 'holidayDate_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_holidayDate = new SimpleDateFormat("dd-MM-yyyy");
	String formatted_holidayDate = format_holidayDate.format(new Date(holidaysDTO.holidayDate));
	%>
	'<%=formatted_holidayDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
		<input type='hidden' class='form-control'  name='holidayDate' id = 'holidayDate_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + holidaysDTO.holidayDate + "'"):("'" + "0" + "'")%>  tag='pb_html'>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_year'>")%>
			
	
	<div class="form-inline" id = 'year_div_<%=i%>'>
		<input type='number' class='form-control'  name='year' id = 'year_number_<%=i%>' min='1900' value=<%=actionName.equals("edit")?("'" + holidaysDTO.year + "'"):("'" + 1900 + "'")%>  tag='pb_html'>
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_description'>")%>
			
	
	<div class="form-inline" id = 'description_div_<%=i%>'>
		<input type='text' class='form-control'  name='description' id = 'description_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + holidaysDTO.description + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + holidaysDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=holidaysDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
							<%					
											out.println("</tr>");
										}
									}
								}
								catch(Exception ex)
								{
									ex.printStackTrace();
								}
						%>
						</tbody>
					</table>
</div>