<%@page pageEncoding="UTF-8" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="ot_bill_submission_config.OT_bill_submission_configDTO" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="allowance_configure.Allowance_configureDTO" %>
<%@ page import="allowance_configure.Allowance_configureDAO" %>
<%@ page import="allowance_configure.AllowanceCatEnum" %>
<%@ page import="office_units.Office_unitsRepository" %>

<%
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String formTitle = isLanguageEnglish ? "Overtime Bill Configuration" : "অধিকাল ভাতা বিলের তথ্য নির্ধারণ";

    String actionName = "ajax_edit";
    boolean isEditPage = true;
    OT_bill_submission_configDTO otBillSubmissionConfigDTO = (OT_bill_submission_configDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    if (otBillSubmissionConfigDTO == null) {
        otBillSubmissionConfigDTO = new OT_bill_submission_configDTO();
        isEditPage = false;
        actionName = "ajax_add";
        Allowance_configureDTO kaType = Allowance_configureDAO.getInstance().getByAllowanceCatEnum(AllowanceCatEnum.OVERTIME_A_ALLOWANCE);
        if (kaType != null) {
            otBillSubmissionConfigDTO.kaDailyRate = kaType.allowanceAmount;
            otBillSubmissionConfigDTO.revenueStampDeduction = kaType.taxDeduction;
            otBillSubmissionConfigDTO.ordinanceText = kaType.ordinanceText;
        }
        Allowance_configureDTO khaType = Allowance_configureDAO.getInstance().getByAllowanceCatEnum(AllowanceCatEnum.OVERTIME_B_ALLOWANCE);
        if (khaType != null) {
            otBillSubmissionConfigDTO.khaDailyRate = khaType.allowanceAmount;
        }
    }

    long defaultStartTime = DateUtils.get12amTime(System.currentTimeMillis());
    long defaultEndTime = defaultStartTime + (DateUtils.ONE_DAY_IN_MILLIS * 7) - 1;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal" id="ot-bill-submission-config-form">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <%--for data goes here--%>

                                    <input type="hidden" name="iD" value="<%=otBillSubmissionConfigDTO.iD%>">

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=isLanguageEnglish ? "Bill Date (From)" : "বিলের তারিখ (হতে)"%>
                                        </label>
                                        <div class="col-md-9">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="billStartDate_js"/>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                            </jsp:include>
                                            <input type='hidden' name='billStartDate' id='billStartDate' value=''>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=isLanguageEnglish ? "Bill Date (To)" : "বিলের তারিখ (পর্যন্ত)"%>
                                        </label>
                                        <div class="col-md-9">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="billEndDate_js"/>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                            </jsp:include>
                                            <input type='hidden' name='billEndDate' id='billEndDate' value=''>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="overtimeBillTypeCat">
                                            <%=isLanguageEnglish ? "Bill Type" : "বিলের ধরণ"%>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='overtimeBillTypeCat'
                                                    id='overtimeBillTypeCat'
                                                    onchange="overtimeBillTypeCatChanged(this)"
                                                    style="width: 100%">
                                                <%=CatRepository.getInstance().buildOptions(
                                                        SessionConstants.OT_BILL_TYPE_CAT_DOMAIN_NAME,
                                                        Language,
                                                        otBillSubmissionConfigDTO.overtimeBillTypeCat
                                                )%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="kaDailyRate">
                                            <%=isLanguageEnglish ? "ক Daily Rate" : "ক-এর দৈনিক হার"%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control'
                                                   id="kaDailyRate"
                                                   name='kaDailyRate'
                                                   data-only-int="true"
                                                   value='<%=otBillSubmissionConfigDTO.kaDailyRate%>'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="khaDailyRate">
                                            <%=isLanguageEnglish ? "খ Daily Rate" : "খ-এর দৈনিক হার"%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control'
                                                   id="khaDailyRate"
                                                   name='khaDailyRate'
                                                   data-only-int="true"
                                                   value='<%=otBillSubmissionConfigDTO.khaDailyRate%>'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"
                                               for="revenueStampDeduction">
                                            <%=isLanguageEnglish ? "Revenue Stamp Deduction" : "রাজস্ব স্ট্যাম্প বাবদ কর্তন"%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control'
                                                   id="revenueStampDeduction"
                                                   name='revenueStampDeduction'
                                                   data-only-int="true"
                                                   value='<%=otBillSubmissionConfigDTO.revenueStampDeduction%>'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="ordinanceText">
                                            <%=isLanguageEnglish ? "Ordinance Description" : "আদেশের বিবরণ"%>
                                        </label>
                                        <div class="col-md-9">
                                            <textarea class="form-control"
                                                      name="ordinanceText"
                                                      id="ordinanceText"
                                                      style="resize: none"
                                                      rows="5"
                                                      maxlength="4096"
                                            ><%=otBillSubmissionConfigDTO.ordinanceText%></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=isLanguageEnglish ? "Submission Start Date" : "জমা শুরুর তারিখ"%>
                                        </label>
                                        <div class="col-md-9">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="startDate_js"/>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                            </jsp:include>
                                            <input type='hidden' name='startDate' id='startDate' value=''>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=isLanguageEnglish ? "Submission  End Date" : "জমা শেষের তারিখ"%>
                                        </label>
                                        <div class="col-md-9">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="endDate_js"/>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                            </jsp:include>
                                            <input type='hidden' name='endDate' id='endDate' value=''>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=isLanguageEnglish ? "Applicable for Offices" : "যেসব অফিসের জন্য প্রযোজ্য"%>
                                        </label>
                                        <div class="col-md-9" style="display: flex; align-items: center;">
                                            <div>
                                                <label for="allOfficesCheckbox">
                                                    <%=isLanguageEnglish ? "All Offices" : "সকল অফিস"%>
                                                </label>
                                            </div>
                                            <div class="ml-3">
                                                <input type="checkbox"
                                                       id="allOfficesCheckbox"
                                                       name="allOfficesCheckbox"
                                                       <%=otBillSubmissionConfigDTO.allowedOfficeIds == null ? "checked" : ""%>
                                                       onchange="allOfficesCheckboxChanged(this)"
                                                >
                                            </div>
                                        </div>
                                        <div class="offset-md-3 col-md-9" id="allowedOfficeIds-div">
                                            <select class='form-control rounded shadow-sm'
                                                    id="allowedOfficeIds"
                                                    name="allowedOfficeIds"
                                                    multiple="multiple"
                                            >
                                                <%=Office_unitsRepository.getInstance().buildOptionsMultiSelect(
                                                        Language,
                                                        otBillSubmissionConfigDTO.getAllowedOfficeIdsStr()
                                                )%>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button type="button" id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                    onclick="location.href = '<%=request.getHeader("referer")%>'">
                                <%=isLanguageEnglish ? "Cancel" : "বাতিল করুন"%>
                            </button>
                            <button type="button" id="submit-btn"
                                    class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    onclick="submitOtBillSubmissionConfigForm()">
                                <%=isLanguageEnglish ? "Submit" : "জমাদিন"%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    const form = $('#ot-bill-submission-config-form');
    const billStartDateInput = $('#billStartDate');
    const billEndDateInput = $('#billEndDate');
    const startDateInput = $('#startDate');
    const endDateInput = $('#endDate');
    const allOfficesCheckbox = document.getElementById('allOfficesCheckbox');
    const $allowedOfficeIds = $('#allowedOfficeIds');
    const $allowedOfficeIdsDiv = $('#allowedOfficeIds-div');

    $(() => {
        select2MultiSelector("#allowedOfficeIds", '<%=Language%>');
        allOfficesCheckboxChanged(allOfficesCheckbox);

        $('#billStartDate_js').on('datepicker.change', (event, param) => {
            const isValidDate = dateValidator('billStartDate_js', true);
            const dateStr = isValidDate ? getDateStringById('billStartDate_js') : '';
            billStartDateInput.val(dateStr);
        });

        $('#billEndDate_js').on('datepicker.change', (event, param) => {
            const isValidDate = dateValidator('billEndDate_js', true);
            const dateStr = isValidDate ? getDateStringById('billEndDate_js') : '';
            billEndDateInput.val(dateStr);
        });

        $('#startDate_js').on('datepicker.change', (event, param) => {
            const isValidDate = dateValidator('startDate_js', true);
            const dateStr = isValidDate ? getDateStringById('startDate_js') : '';
            startDateInput.val(dateStr);
        });

        $('#endDate_js').on('datepicker.change', (event, param) => {
            const isValidDate = dateValidator('endDate_js', true);
            const dateStr = isValidDate ? getDateStringById('endDate_js') : '';
            endDateInput.val(dateStr);
        });

        <%if(isEditPage){%>
        setDateByTimestampAndId('billStartDate_js', <%=otBillSubmissionConfigDTO.billStartDate%>);
        billStartDateInput.val(getDateStringById('billStartDate_js'));

        setDateByTimestampAndId('billEndDate_js', <%=otBillSubmissionConfigDTO.billEndDate%>);
        billEndDateInput.val(getDateStringById('billEndDate_js'));

        setDateByTimestampAndId('startDate_js', <%=otBillSubmissionConfigDTO.startDate%>);
        startDateInput.val(getDateStringById('startDate_js'));

        setDateByTimestampAndId('endDate_js', <%=otBillSubmissionConfigDTO.endDate%>);
        endDateInput.val(getDateStringById('endDate_js'));
        <%} else {%>
        setDateByTimestampAndId('startDate_js', <%=defaultStartTime%>);
        startDateInput.val(getDateStringById('startDate_js'));

        setDateByTimestampAndId('endDate_js', <%=defaultEndTime%>);
        endDateInput.val(getDateStringById('endDate_js'));
        <%}%>
    });

    function submitOtBillSubmissionConfigForm() {
        submitAjaxByData(form.serialize(), "OT_bill_submission_configServlet?actionType=<%=actionName%>");
    }

    function allOfficesCheckboxChanged(allOfficesCheckbox) {
        if(allOfficesCheckbox.checked) {
            $allowedOfficeIdsDiv.hide();
            $allowedOfficeIds.val([]);
        } else {
            $allowedOfficeIdsDiv.show();
        }
        $allowedOfficeIds.change();
    }

    const ARREARS_BILL = '2';

    function overtimeBillTypeCatChanged(selectElem) {
        const overtimeBillType = selectElem.value;
        allOfficesCheckbox.checked = overtimeBillType !== ARREARS_BILL;
        allOfficesCheckboxChanged(allOfficesCheckbox);
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }
</script>