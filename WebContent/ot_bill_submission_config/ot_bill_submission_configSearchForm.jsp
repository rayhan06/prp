<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.util.List" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="util.StringUtils" %>
<%@ page import="ot_bill_submission_config.OT_bill_submission_configDTO" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@page pageEncoding="UTF-8" %>

<%
    String navigator2 = "navOVERTIME_ALLOWANCE";
    String servletName = "OT_bill_submission_configServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead class="text-center">
        <tr>
            <th>
                <%=isLanguageEnglish ? "Bill Date" : "বিলের তারিখ"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Bill Type" : "বিলের ধরণ"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Submission Date" : "জমার তারিখ"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Edit" : "পরিবর্তন"%>
            </th>
            <th class="">
                <div class="text-center">
                    <span><%=UtilCharacter.getDataByLanguage(Language, "সব", "All")%></span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<OT_bill_submission_configDTO> data = (List<OT_bill_submission_configDTO>) recordNavigator.list;
            try {
                if (data != null) {
                    for (OT_bill_submission_configDTO otBillSubmissionConfigDTO : data) {
        %>
        <tr>
            <td>
                <%=DateUtils.getDateRangeString(
                        otBillSubmissionConfigDTO.billStartDate,
                        otBillSubmissionConfigDTO.billEndDate,
                        isLanguageEnglish
                )%>
            </td>
            <td>
                <%=CatRepository.getInstance().getText(
                        isLanguageEnglish,
                        SessionConstants.OT_BILL_TYPE_CAT_DOMAIN_NAME,
                        otBillSubmissionConfigDTO.overtimeBillTypeCat
                )%>
            </td>
            <td>
                <%=StringUtils.getFormattedDate(isLanguageEnglish, otBillSubmissionConfigDTO.startDate)%>
                &nbsp;<%=isLanguageEnglish ? "To" : "হতে"%>&nbsp;
                <%=StringUtils.getFormattedDate(isLanguageEnglish, otBillSubmissionConfigDTO.endDate)%>
            </td>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=otBillSubmissionConfigDTO.iD%>'"
                >
                    <i class="fa fa-edit"></i>
                </button>
            </td>
            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'>
                        <input type='checkbox' name='ID' value='<%=otBillSubmissionConfigDTO.iD%>'/>
                    </span>
                </div>
            </td>
        </tr>
        <%
                    }
                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>