<%@page pageEncoding="UTF-8" %>
<%@ page import="overtime_bill.BillStatusView" %>
<%@ page import="java.util.List" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="static overtime_bill.BillStatusView.*" %>
<%@ page import="overtime_bill.BillStatusResponse" %>

<%
    List<BillStatusResponse> billStatusResponses = (List<BillStatusResponse>) request.getAttribute("billStatusResponses");

    boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;

    final Map<BillStatusView, String> cssClassByView = new HashMap<>();
    cssClassByView.put(NOT_SUBMITTED, "badge badge-danger");
    cssClassByView.put(SUBMITTED_WITHOUT_EMPLOYEE, "badge badge-secondary");
    cssClassByView.put(WAITING_FOR_APPROVAL, "badge badge-warning");
    cssClassByView.put(SENT_TO_FINANCE, "badge badge-info");
    cssClassByView.put(FINANCE_PREPARED, "badge badge-success");
%>

<table class="table table-bordered">
    <%if (billStatusResponses.isEmpty()) {%>
    <tr>
        <td class="text-center">
            <%=isLangEn ? "No data found" : "কোন তথ্য পাওয়া যায়নি"%>
        </td>
    </tr>
    <%
    } else {
        for (BillStatusResponse billStatusResponse : billStatusResponses) {
    %>
    <tr>
        <td>
            <%=billStatusResponse.billDescription%>
        </td>
        <td>
            <span class="<%=cssClassByView.get(billStatusResponse.status)%>">
                <%=billStatusResponse.message%>
            </span>
        </td>
    </tr>
    <%
            }
        }
    %>
</table>