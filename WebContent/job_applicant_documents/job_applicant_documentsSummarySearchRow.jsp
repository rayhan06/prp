<%@page pageEncoding="UTF-8" %>

<%@page import="job_applicant_documents.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="files.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="recruitment_job_description.RecruitmentJobSpecificFilesDAO" %>
<%@ page import="recruitment_job_description.RecruitmentJobSpecificFilesDTO" %>
<%
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String Language = LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_LANGUAGE, loginDTO);
	String Language2 = Language;

	UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);



	RecruitmentJobSpecificFilesDTO dto = (RecruitmentJobSpecificFilesDTO)request.getAttribute("recruitment_job_specific_filesDTO");
	CommonDTO commonDTO = dto;


	System.out.println("RecruitmentJobSpecificFilesDTO = " + dto);


	int i = Integer.parseInt(request.getParameter("rownum"));
	out.println("<td style=\"  width: 20%; height: auto; border: 1px solid gray; display:none;\"><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

	String value = "";


	FilesDAO filesDAO = new FilesDAO();

	String Options = "";
	boolean formSubmit = false;
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

	Map<Long, Job_applicant_documentsDTO> job_applicant_documentsDTOMap = null;
	Job_applicant_documentsDTO job_applicant_documentsDTO = null;
	job_applicant_documentsDTOMap = (Map<Long, Job_applicant_documentsDTO>)request.getAttribute("job_applicant_documentsDTOMap");


	job_applicant_documentsDTO = job_applicant_documentsDTOMap.get(dto.iD);

	System.out.println(job_applicant_documentsDTO.iD);


%>







<td style="  width: 45%; height: auto; border: 1px solid gray;"  class="">
	<%
		String name = "";
		if(Language.equalsIgnoreCase("english")){
			name = dto.nameEn;
		} else {
			name = dto.nameBn;
		}
	%>
	<%= name%>
</td>
<td style="  width: 25%; height: auto; border: 1px solid gray;"  class="">
	<%
		String text = "";

		if(dto.isMandatoryFiles){
			text = LM.getText(LC.JOB_APPLICANT_DOCUMENTS_YES, loginDTO);
		} else {
			text = LM.getText(LC.JOB_APPLICANT_DOCUMENTS_NO, loginDTO);
		}
	%>
	<%= text%>
</td>
<td style="  width: 30%; height: auto; border: 1px solid gray;"  class="">
	<%
		List<FilesDTO> filesDTOS = new FilesDAO().getMiniDTOsByFileID(job_applicant_documentsDTO.filesDropzone);
		boolean flag = filesDTOS.size() > 0;
    %>
	<%=flag?(LM.getText(LC.ETHNIC_MINORITY_YES, loginDTO)):(LM.getText(LC.ETHNIC_MINORITY_NO, loginDTO))%>

</td>
<%--<td class="filesColumn" style="  width: 20%; height: auto; border: 1px solid gray;"  class="">--%>
<%--	<div class="">--%>
<%--		<%--%>

<%--				List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(job_applicant_documentsDTO.filesDropzone);--%>
<%--		%>--%>
<%--		<table>--%>
<%--			<tr>--%>
<%--				<%--%>
<%--					if(filesDropzoneDTOList != null)--%>
<%--					{--%>
<%--						for(int j = 0; j < filesDropzoneDTOList.size(); j ++)--%>
<%--						{--%>
<%--							FilesDTO filesDTO = filesDropzoneDTOList.get(j);--%>
<%--							byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);--%>
<%--				%>--%>
<%--				<td style="  width: 20%; height: auto; border: 1px solid gray;"  id = 'filesDropzone_td_<%=filesDTO.iD%>'>--%>
<%--					<%--%>
<%--						if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)--%>
<%--						{--%>
<%--					%>--%>
<%--&lt;%&ndash;					<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)&ndash;%&gt;--%>
<%--&lt;%&ndash;						%>' style='width:100px' />&ndash;%&gt;--%>
<%--					<%--%>
<%--						}--%>
<%--					%>--%>
<%--					<a href = 'Job_applicant_documentsServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>--%>
<%--				</td>--%>
<%--				<%--%>
<%--						}--%>
<%--					}--%>
<%--				%>--%>
<%--			</tr>--%>
<%--		</table>--%>

<%--	</div>--%>

<%--</td>--%>
















																						
											

