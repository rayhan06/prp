<%@ page import="recruitment_job_description.RecruitmentJobSpecificFilesDTO" %>
<%@ page import="java.util.List" %>
<%@page import="dbm.*" %>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="user.UserRepository" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="user.UserDTO" %>
<%@ page import="job_applicant_documents.Job_applicant_documentsDTO" %>
<%@ page import="java.util.Map" %>
<%@ page import="files.FilesDTO" %>
<%@ page import="files.FilesDAO" %>


<%

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    Map<Long, Job_applicant_documentsDTO> job_applicant_documentsDTOMap = null;
    Job_applicant_documentsDTO job_applicant_documentsDTO = null;
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
    {
        actionName = "add";
    }
    else
    {
        actionName = "edit";
        job_applicant_documentsDTOMap = (Map<Long, Job_applicant_documentsDTO>)request.getAttribute("job_applicant_documentsDTOMap");
    }


    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    List<RecruitmentJobSpecificFilesDTO> recruitmentJobSpecificFilesDTOS = null;
    recruitmentJobSpecificFilesDTOS = (List<RecruitmentJobSpecificFilesDTO>)request.getAttribute("recruitmentJobSpecificFilesDTOS");

    FilesDAO filesDAO = new FilesDAO();
    String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);
%>



<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-gift"></i>
            <%=LM.getText(LC.JOB_APPLICANT_DOCUMENTS_ANYFIELD, loginDTO)%>
        </h3>

        <div class="box-body">
            <form id = "doc-form" class="form-horizontal">
                <div class="form-body row">
                    <div class="col-md-2"></div>
                    <div class="form-group col-md-8">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr class="">
<%--                                    <th scope="row">1</th>--%>
                                    <th class="">
                                        <%=LM.getText(LC.JOB_APPLICANT_DOCUMENTS_NAME, loginDTO)%>
                                    </th>
                                    <th class=""><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_CERTS_ISMANDATORYCERTS, loginDTO)%></th>
                                    <th class=""> <%=LM.getText(LC.JOB_APPLICANT_DOCUMENTS_GIVEN, loginDTO)%></th>
                                    <th class="">
                                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_FILESDROPZONE, loginDTO)%>
                                    </th>
                                    <%--										<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_MODIFIEDBY, loginDTO)%></th>--%>
                                    <%--                            <th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_REMOVE, loginDTO)%></th>--%>
                                </tr>
                                </thead>
                                <tbody id="">
                                <input type='hidden'  name='DocFilesDropzoneFilesToDelete' id = 'filesDropzoneFilesToDelete' value=''  tag='pb_html'/>
                                <%
                                    for(int i = 0; i < recruitmentJobSpecificFilesDTOS.size(); i++){
                                        RecruitmentJobSpecificFilesDTO dto = recruitmentJobSpecificFilesDTOS.get(i);
                                        if(actionName.equals("edit")){
                                            job_applicant_documentsDTO = job_applicant_documentsDTOMap.get(dto.iD);

                                %>
                                            <input type='hidden'   name='' id = 'job_applicant_documents_id_<%=i%>' value="<%=job_applicant_documentsDTO.iD%>">


                                <%

                                        }

                                %>

                                <tr >
<%--                                    <th scope="row">1</th>--%>
                                    <input type='hidden'   name='' id = 'recruitment_job_specific_files_id_<%=i%>' value="<%=dto.iD%>">

                                    <td class="">
                                        <%
                                            String name = "";
                                            if(Language.equalsIgnoreCase("english")){
                                                name = dto.nameEn;
                                            } else {
                                                name = dto.nameBn;
                                            }
                                        %>
                                        <%= name%>
                                    </td>
                                    <td class="">
                                        <%
                                            String text = "";

                                                if(dto.isMandatoryFiles){
                                                   text = LM.getText(LC.JOB_APPLICANT_DOCUMENTS_YES, loginDTO);
                                                } else {
                                                    text = LM.getText(LC.JOB_APPLICANT_DOCUMENTS_NO, loginDTO);
                                                }
                                        %>
                                        <%= text%>
                                    </td>
                                    <td class="">
                                        <input type='checkbox' class=''  name='is_checked_<%=i%>' id = 'is_checked_<%=i%>' value='true' <%=(actionName.equals("edit") && String.valueOf(job_applicant_documentsDTO.isChecked).equals("true"))?("checked"):""%>  tag='pb_html'><br>

                                    </td>
                                    <td class="">
                                        <div class="">
                                            <%
                                                if(actionName.equals("edit"))
                                                {
                                                    List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(job_applicant_documentsDTO.filesDropzone);
                                            %>
                                            <table>
                                                <tr>
                                                    <%
                                                        if(filesDropzoneDTOList != null)
                                                        {
                                                            for(int j = 0; j < filesDropzoneDTOList.size(); j ++)
                                                            {
                                                                FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                                                byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                    %>
                                                    <td id = 'filesDropzone_td_<%=filesDTO.iD%>'>
                                                        <%
                                                            if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)
                                                            {
                                                        %>
                                                        <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)
						%>' style='width:100px' />
                                                        <%
                                                            }
                                                        %>
                                                        <a href = 'Job_applicant_documentsServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>
                                                        <a class='btn btn-danger' onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete")'>x</a>
                                                    </td>
                                                    <%
                                                            }
                                                        }
                                                    %>
                                                </tr>
                                            </table>
                                            <%
                                                }
                                            %>
                                            <%
                                                long ColumnID;
                                                if(actionName.equals("edit")){
                                                    ColumnID = job_applicant_documentsDTO.filesDropzone;
                                                } else {
                                                    ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                }
                                            %>


                                            <div class="dropzone" action="Job_applicant_documentsServlet?actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=ColumnID%>">
                                                <input type='file' style="display:none"  name='filesDropzoneFile' id = 'filesDropzone_dropzone_File_<%=i%>'  tag='pb_html'/>
                                            </div>

                                            <input type='hidden' name='filesDropzone' id = 'filesDropzone_dropzone_<%=i%>'  tag='pb_html' value='<%=ColumnID%>'/>


                                        </div>

                                    </td>
                                </tr>


                                <%

                                    }
                                %>
                                </tbody>
                            </table>
                            <div class="form-actions text-center">
                                <a class="btn btn-success" style="color: white" onclick="submit()">
                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_DESCRIPTION_SUBMIT_BUTTON, loginDTO)%>
                                </a>
<%--                                <button class="btn btn-success" type="submit">--%>

<%--                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_DESCRIPTION_SUBMIT_BUTTON, loginDTO)%>--%>

<%--                                </button>--%>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">

    var rowCount = 0;
    var jobId,jobApplicantId, actionName;

    window.onload =function ()
    {
        rowCount = <%=recruitmentJobSpecificFilesDTOS.size()%>;
        jobId = <%=request.getParameter("jobId")%>;
        jobApplicantId = <%=request.getParameter("jobApplicantId")%>;
        actionName = '<%=actionName%>';

    }

    function validate(){
        let formSelector = $("#doc-form");
        formSelector.validate();
        <% for(int i= 0; i < recruitmentJobSpecificFilesDTOS.size(); i++){
            Boolean mandatory = recruitmentJobSpecificFilesDTOS.get(i).isMandatoryFiles;
        %>
        if(<%=mandatory%> + '' == 'true'){
            let id = 'is_checked_' + <%=i%>;
            $('#' + id).rules("add", {
                required: true,
                messages: {
                    required: "Please Select",
                }
            });
        }

        <% }
        %>

        return formSelector.valid();


    }



    function submit(){
        if(validate()){
            let formData = new FormData();
            formData.append('rowCount', rowCount);
            formData.append('jobApplicantId', jobApplicantId);
            formData.append('jobId', jobId);

            for(let i = 0; i < rowCount ; i++){
                let id;

                id = 'is_checked_' + i;
                formData.append(id, document.getElementById(id).checked);


                if(actionName == 'add'){
                    id = 'recruitment_job_specific_files_id_' + i;
                    formData.append(id, document.getElementById(id).value);
                    id = 'filesDropzone_dropzone_' + i;
                    formData.append(id, document.getElementById(id).value);

                } else {
                    id = 'job_applicant_documents_id_' + i;
                    formData.append(id, document.getElementById(id).value);
                    id = 'filesDropzoneFilesToDelete';
                    formData.append('fileDeleteIds', document.getElementById(id).value);

                }



            }

            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    window.location = 'Job_applicant_documentsServlet?actionType=getEditPage&jobId=' + jobId + '&jobApplicantId=' + jobApplicantId;

                }
                else if (this.readyState == 4 && this.status != 200) {
                    //alert('failed ' + this.status);
                }
            };



            var params;
            if(actionName == 'add'){
                params= "Job_applicant_documentsServlet?actionType=add";
            } else {
                params= "Job_applicant_documentsServlet?actionType=edit";
            }
            xhttp.open("POST",params, true);
            xhttp.send(formData);

        }

    }

</script>
