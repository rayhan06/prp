
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="job_applicant_documents.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.text.SimpleDateFormat"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="job_applicant_documents.Job_applicant_documentsDAO" %>
<%@ page import="job_applicant_documents.Job_applicant_documentsDTO" %>
<%@ page import="recruitment_job_description.RecruitmentJobSpecificFilesDTO" %>
<%@ page import="recruitment_job_description.RecruitmentJobSpecificFilesDAO" %>
<%@ page import="java.util.*" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="recruitment_job_required_files.Recruitment_job_required_filesDTO" %>
<%@ page import="recruitment_job_required_files.Recruitment_job_required_filesDAO" %>
<%@ page import="recruitment_job_description.RecruitmentJobSpecificFilesRepository" %>
<%@ page import="recruitment_job_required_files.Recruitment_job_required_filesRepository" %>
<%@ page import="com.google.gson.Gson" %>


<%
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String failureMessage = (String)request.getAttribute("failureMessage");
	if(failureMessage == null || failureMessage.isEmpty())
	{
		failureMessage = "";
	}


	Job_applicant_documentsDAO job_applicant_documentsDAO = new Job_applicant_documentsDAO();


	String ajax = request.getParameter("ajax");
	boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<%

	if(hasAjax == false)
	{
		Enumeration<String> parameterNames = request.getParameterNames();

		while (parameterNames.hasMoreElements())
		{

			String paramName = parameterNames.nextElement();

			if(!paramName.equalsIgnoreCase("actionType"))
			{
				String[] paramValues = request.getParameterValues(paramName);
				for (int i = 0; i < paramValues.length; i++)
				{
					String paramValue = paramValues[i];

%>

<%

				}
			}


		}
	}

	String formTitle = LM.getText(LC.JOB_APPLICANT_DOCUMENTS_ANYFIELD, loginDTO);
//	String formTitle = "";

%>

<%--<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>--%>

<div class="col-lg-12">
	<div class="kt-portlet shadow-none" style="margin-top: -20px">

		<div class="kt-portlet__body form-body">


<div class="table-responsive">
	<table id="tableData" class="table table-bordered table-striped">
		<thead class="thead-light">
		<tr class="">
			<%--                                    <th style="  width: 20%; height: auto; border: 1px solid gray;" scope="row">1</th>--%>
			<th style="  width: 45%; height: auto; border: 1px solid gray;" class="">
				<%=LM.getText(LC.JOB_APPLICANT_DOCUMENTS_NAME, loginDTO)%>
			</th>
			<th style="  width: 25%; height: auto; border: 1px solid gray;" class=""><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_CERTS_ISMANDATORYCERTS, loginDTO)%></th>
			<th style="  width: 30%; height: auto; border: 1px solid gray;" class=""> <%=LM.getText(LC.JOB_APPLICANT_DOCUMENTS_GIVEN, loginDTO)%></th>
<%--			<th class="filesColumn" style="  width: 20%; height: auto; border: 1px solid gray;" class="">--%>
<%--				<%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_FILESDROPZONE, loginDTO)%>--%>
<%--			</th>--%>
			<%--										<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_MODIFIEDBY, loginDTO)%></th>--%>
			<%--                            <th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_REMOVE, loginDTO)%></th>--%>
		</tr>
		</thead>
		<tbody>
		<%
//			RecruitmentJobSpecificFilesDAO recruitmentJobSpecificFilesDAO = new RecruitmentJobSpecificFilesDAO();
			long jobId = Long.parseLong(request.getParameter("jobId"));
			List<RecruitmentJobSpecificFilesDTO> data = RecruitmentJobSpecificFilesRepository.getInstance()
					.getRecruitmentJobSpecificFilesDTOByJobDescriptionId(jobId);
//					recruitmentJobSpecificFilesDAO.getRecruitmentJobSpecificFilesDTOListByRecruitmentJobDescriptionID(jobId);

			List<Long> reqFileIds = data.stream().map(i -> i.recruitmentJobRequiredFilesType).collect(Collectors.toList());

			Recruitment_job_required_filesDAO recruitment_job_required_filesDAO =
					new Recruitment_job_required_filesDAO();

			List<Recruitment_job_required_filesDTO> recruitment_job_required_filesDTOS = new ArrayList<>();
			reqFileIds.forEach(i -> {
				Recruitment_job_required_filesDTO dto = Recruitment_job_required_filesRepository.
						getInstance().getRecruitment_job_required_filesDTOByID(i);
				if(dto != null){
					recruitment_job_required_filesDTOS.add(dto);
				}
			});
//					(List<Recruitment_job_required_filesDTO>)
//					recruitment_job_required_filesDAO.getDTOs(reqFileIds);
			Map<Long, Recruitment_job_required_filesDTO> recruitment_job_required_filesDTOMap = new HashMap<>(
					recruitment_job_required_filesDTOS.stream().collect(Collectors.toMap(s -> s.iD, s -> s))) ;

			List<Job_applicant_documentsSummaryDTO> summaryDTOS = (List<Job_applicant_documentsSummaryDTO>)
					request.getSession(true).getAttribute("job_applicant_documentsSummaryDTOList");
			Gson gson = new Gson();

			List<Job_applicant_documentsDTO> job_applicant_documentsDTOS = new ArrayList<>();
			if(summaryDTOS != null){
				summaryDTOS.forEach(i -> {
					String json = gson.toJson(i);
					Job_applicant_documentsDTO dto = gson.fromJson(json, Job_applicant_documentsDTO.class);
					job_applicant_documentsDTOS.add(dto);
				});
			}
//					(List<Job_applicant_documentsDTO>)
//					request.getSession(true).getAttribute("job_applicant_documentsSummaryDTOList");
//			if()
//					job_applicant_documentsDAO.
//					getJob_applicant_documentsDTOListByjobApplicantIdAndJobId(Long.parseLong(request.getParameter("ID")), jobId);

			data.forEach(i -> {
				if(recruitment_job_required_filesDTOMap.containsKey(i.recruitmentJobRequiredFilesType)){
					i.nameBn = recruitment_job_required_filesDTOMap.get(i.recruitmentJobRequiredFilesType).nameBn;
					i.nameEn = recruitment_job_required_filesDTOMap.get(i.recruitmentJobRequiredFilesType).nameEn;
				}
			});

			Map<Long, Job_applicant_documentsDTO> job_applicant_documentsDTOMap = new HashMap();
			HashSet<Long> foundRecruitementJobSpecificFilesId = new HashSet();

			for (Job_applicant_documentsDTO innerDTO: job_applicant_documentsDTOS) {
				job_applicant_documentsDTOMap.put(innerDTO.recruitmentJobSpecificFilesId, innerDTO);
				foundRecruitementJobSpecificFilesId.add(innerDTO.recruitmentJobSpecificFilesId);
			}

			request.setAttribute("job_applicant_documentsDTOMap",job_applicant_documentsDTOMap);

			try
			{

				if (data != null)
				{
					int size = data.size();
					System.out.println("data not null and size = " + size + " data = " + data);

					for(int i = 0; i < data.size(); i++){

						RecruitmentJobSpecificFilesDTO dto = data.get(i);

						if (!foundRecruitementJobSpecificFilesId.contains(dto.iD))continue;


		%>
		<tr id = 'tr_<%=i%>'>
			<%

			%>


			<%
				request.setAttribute("recruitment_job_specific_filesDTO",dto);
			%>

			<jsp:include page="job_applicant_documentsSummarySearchRow.jsp">
				<jsp:param name="pageName" value="searchrow" />
				<jsp:param name="rownum" value="<%=i%>" />
			</jsp:include>


			<%

			%>
		</tr>
		<%
					}

					System.out.println("printing done");
				}
				else
				{
					System.out.println("data  null");
				}
			}
			catch(Exception e)
			{
				System.out.println("JSP exception " + e);
			}
		%>



		</tbody>

	</table>
</div>

</div>
</div>
</div>



			