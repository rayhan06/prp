<%@page import="drug_information.Drug_informationDTO"%>
<%@page import="drug_information.Drug_informationDAO"%>
<%@page import="category.CategoryRepository"%>
<%@page import="category.CategoryDTO"%>
<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="nurse_action.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>
<%@ page import="appointment.*"%>
<%@ page import="family.*"%>
<%@page import="other_office.*"%>
<%@page import="borrow_medicine.*"%>
<%@page import="department_requisition_lot.*"%>

<%
Nurse_actionDTO nurse_actionDTO = new Nurse_actionDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	nurse_actionDTO = Nurse_actionDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = nurse_actionDTO;
String tableName = "nurse_action";
AppointmentDAO appointmentDAO = new AppointmentDAO();
AppointmentDTO appointmentDTO = null;
if(request.getParameter("appointmentId") != null)
{
	nurse_actionDTO.appointmentId = Long.parseLong(request.getParameter("appointmentId"));
	appointmentDTO = appointmentDAO.getDTOByID(nurse_actionDTO.appointmentId);
	nurse_actionDTO.patientOrganogramId = appointmentDTO.patientId;
	nurse_actionDTO.patientUserId = appointmentDTO.employeeUserName;
	nurse_actionDTO.name = appointmentDTO.patientName;
	nurse_actionDTO.phone = appointmentDTO.phoneNumber;
	nurse_actionDTO.whoIsThePatientCat = appointmentDTO.whoIsThePatientCat;
	nurse_actionDTO.othersDesignationAndId = appointmentDTO.othersDesignationAndId;
	nurse_actionDTO.othersOfficeId = appointmentDTO.othersOfficeId;
	nurse_actionDTO.othersOfficeEn = appointmentDTO.othersOfficeEn;
	nurse_actionDTO.othersOfficeBn = appointmentDTO.othersOfficeBn;
}
PersonalStockDAO personalStockDAO = PersonalStockDAO.getInstance();
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.NURSE_ACTION_ADD_NURSE_ACTION_ADD_FORMNAME, loginDTO);
String servletName = "Nurse_actionServlet";
Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
PersonalStockDTO accuDTO = personalStockDAO.getDTOByDeptAndDrugId((int)Department_requisition_lotDTO.EMERGENCY, Drug_informationDTO.ACCU_CHECK_ID, SessionConstants.MEDICAL_ITEM_DRUG);
int accuCount = 0;
if(accuDTO != null)
{
	accuCount = accuDTO.detailedCount;
}
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp" >
<jsp:param name="isHierarchyNeeded" value="false" />
</jsp:include>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Nurse_actionServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>

														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=nurse_actionDTO.iD%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name = 'nurseOrganogramId' id = 'nurseOrganogramId_hidden_<%=i%>' value='<%=userDTO.organogramID%>' tag='pb_html'/>									
														<input type='hidden' class='form-control'  name='appointmentId' id = 'appointmentId_hidden_<%=i%>' value='<%=nurse_actionDTO.appointmentId%>' tag='pb_html'/>
														
														<div class="form-group row">
		                                                       <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.HM_DATE, loginDTO)%></label>
		                                                            
	                                                            <div class="col-md-8">
			                                                        <jsp:include page="/date/date.jsp">
		                                                            <jsp:param name="DATE_ID"
		                                                                       value="actionDate_date_js"></jsp:param>
		                                                            <jsp:param name="LANGUAGE"
		                                                                       value="<%=Language%>"></jsp:param>
		                                                       		 </jsp:include>
		
			                                                        <input type='hidden'
			                                                               class='form-control formRequired datepicker'
			                                                               readonly="readonly" data-label="Document Date"
			                                                               id='actionDate_date_<%=i%>' name='actionDate' value=<%
																				String formatted_actionDate = dateFormat.format(new Date(nurse_actionDTO.actionDate));
																			%>
			                                                                '<%=formatted_actionDate%>'/>
			                                                    </div>
				                                                    
		                                                           
		                                                </div>
														<%
														if(request.getParameter("appointmentId") != null)
														{
															%>
															 <div class="form-group row d-flex align-items-center">
							                                    <label class="col-4 col-form-label text-right">
							                                        <%=LM.getText(LC.HM_REFERENCE_EMPLOYEE, loginDTO)%>
							                                    </label>
							                                    <div class="col-8">
																		 <%=LM.getText(LC.HM_ID, loginDTO)%>:<%=nurse_actionDTO.patientUserId%>
																		 <br><%=WorkflowController.getNameFromUserName(nurse_actionDTO.patientUserId, Language)%>			
							                                    </div>
							                                </div>
										
															<div class="form-group row d-flex align-items-center">
							                                    <label class="col-4 col-form-label text-right">
							                                        <%=LM.getText(LC.HM_PATIENT_NAME, loginDTO)%>
							                                    </label>
							                                    <div class="col-8">
																		 <b><%=nurse_actionDTO.name%></b>
																		    <%
																		    if(nurse_actionDTO.othersOfficeId != -1)
																		    {
																		    	%>
																		    	<br>
																		    	<%=nurse_actionDTO.othersDesignationAndId.equalsIgnoreCase("")?"":nurse_actionDTO.othersDesignationAndId + ", "%>
																		    	<%
																		    	if(isLanguageEnglish)
																		    	{
																		    		%>
																		    		<%=nurse_actionDTO.othersOfficeEn%>
																		    		<%
																		    	}
																		    	else
																		    	{
																		    		%>
																		    		<%=nurse_actionDTO.othersOfficeBn%>
																		    		<%
																		    	}
																		    }
																		    
																		    %>
							                                    </div>
							                                </div>
							                                
							                                <div class="form-group row d-flex align-items-center">
							                                    <label class="col-4 col-form-label text-right">
							                                        <%=LM.getText(LC.HM_PHONE, loginDTO)%>
							                                    </label>
							                                    <div class="col-8">
																		 <%=Utils.getDigits(nurse_actionDTO.phone, Language)%>			
							                                    </div>
							                                </div>
															<%
														}
														else
														{
														%>
															<div class="form-group row">
		                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.HM_USER_NAME, loginDTO)%></label>
		                                                            
		                                                            <div class="col-md-6">
				                                                        <input type='text' class='form-control'
				                                                               name='patientUserId' id='patientUserId'
				                                                               tag='pb_html'/>
				                                                    </div>
				                                                    <div class="col-md-2 text-md-right">
				                                                        <button type="button"
				                                                                class="btn btn-border-radius text-white shadow"
				                                                                style="background-color: #4a87e2;"
				                                                                onclick="patient_inputted($('#patientUserId').val(), '')">
				                                                            <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
				                                                        </button>
				                                                    </div>
		                                                           
		                                                      </div>
															<div class="form-group row">
		                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%></label>
		                                                            <div class="col-md-8">
																		<button type="button" class="btn btn-primary btn-block shadow btn-border-radius" onclick="addEmployeeWithRow(this.id)" id="patientOrganogramId_button_<%=i%>" tag='pb_html'><%=LM.getText(LC.HM_ADD_EMPLOYEE, loginDTO)%></button>
																		<table class="table table-bordered table-striped">
																			<tbody id="patientOrganogramId_table_<%=i%>" tag='pb_html'>
																			
																			</tbody>
																		</table>
																		<input type='hidden' class='form-control'  name = 'patientOrganogramId' id = 'patientOrganogramId_hidden_<%=i%>' value='<%=nurse_actionDTO.patientOrganogramId%>' tag='pb_html'/>
																																			</div>
		                                                      </div>
                                                      
			                                                 <div id = "patientDiv" style = "display:none">
			                                                      
			                                                     <div class="form-group row" id = "patientTypeDiv">
			
																	<label class="col-md-4 col-form-label text-md-right">
																		<%=LM.getText(LC.SELECT_PATIENT_HM, loginDTO)%>
																	</label>
																	<div class="col-md-8">
																		<select class='form-control'
																			name='whoIsThePatientCat'
																			onchange="getInfo()"																			
																			id='whoIsThePatientCat_category_<%=i%>' tag='pb_html'>
																		</select>
																	</div>
						
																</div>
			                                                     <div class="form-group row">
			                                                           <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.NURSE_ACTION_ADD_NAME, loginDTO)%></label>
			                                                           <div class="col-md-8">
																		<input type='text' class='form-control'  name='name' id = 'name' value='<%=nurse_actionDTO.name%>'   tag='pb_html'/>					
																	</div>
			                                                     </div>									
																<div class="form-group row">
			                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.NURSE_ACTION_ADD_PHONE, loginDTO)%></label>
			                                                            <div class="col-md-8">
			                                                            	<div class="input-group mb-2">
													                            <div class="input-group-prepend">
													                                <div class="input-group-text"><%=Language.equalsIgnoreCase("english")? "+88" : "+৮৮"%></div>
													                            </div>
													                            <input type='text' class='form-control' required
							                                                       name='phone' id='phone'
							                                                       value='<%=Utils.getPhoneNumberWithout88(nurse_actionDTO.phone, Language)%>'
							                                                       tag='pb_html'>
																             </div>
																		</div>
			                                                      </div>
			                                                      
			                                                      <div id = "othersOfficeDiv" style = "display:none">
													
																	<div class="form-group row">
							
																		<label class="col-md-4 col-form-label text-md-right">
																			<%=LM.getText(LC.HM_OFFICE, loginDTO)%>
																		</label>
																		<div class="col-md-8">																											
																			<select class='form-control' name='othersOfficeId'
						                                                            id='currentOffice_category' tag='pb_html'>
						                                                        <%=Other_officeRepository.getInstance().buildOptions(Language, null)%>
						                                                    </select>
																		</div>	
																	</div>
																	
																	<div class="form-group row">
							
																		<label class="col-md-4 col-form-label text-md-right">
																			<%=Language.equalsIgnoreCase("english")?"Designation and Id":"পদবী এবং আইডি"%>
																		</label>
																		<div class="col-md-8">
																			<input type='text' class='form-control'
																				name='othersDesignationAndId' id='othersDesignationAndId'
																				value='<%=nurse_actionDTO.othersDesignationAndId%>'
																				tag='pb_html' />
																		</div>	
																	</div>
																
																
															</div>
														</div>
														<%
														}
														%>
																																	
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
               <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.NURSE_ACTION_ADD_NURSE_ACTION_DETAILS, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
									<tr>
										<th><%=LM.getText(LC.NURSE_ACTION_ADD_NURSE_ACTION_DETAILS_NURSEACTIONCAT, loginDTO)%></th>
										<th><%=LM.getText(LC.NURSE_ACTION_ADD_NURSE_ACTION_DETAILS_DESCRIPTION, loginDTO)%></th>
										<th><%=LM.getText(LC.HM_IS_TESTING_DONE, loginDTO)%></th>
									</tr>
								</thead>
							<tbody id="field-NurseActionDetails">
								<%
									
										int index = -1;
										List<CatDTO> nurseActionCats= CatRepository.getDTOs("nurse_action", CatRepository.SORT_BY_ORDERING);
										System.out.println("cat size = " + nurseActionCats.size()) ;
										
										for(CatDTO cat: nurseActionCats)
										{
											if(cat.value == NurseActionDetailsDTO.CALL_ATTEND)
											{
												continue;
											}
											index++;
											
											System.out.println("index index = "+index);

								%>	
							
								<tr id = "NurseActionDetails_<%=index + 1%>">
									
									<td>
											<input type = "hidden" value = "<%=cat.value%>"	name='nurseActionDetails.nurseActionCat' />									
											<input type = "text" readonly value = "<%=Language.equalsIgnoreCase("English")?cat.nameEn:cat.nameBn%>"	class='form-control' />

									</td>
									<td>										
											<textarea  class='form-control'  name='nurseActionDetails.description' id = 'description_text_<%=childTableStartingID%>' tag='pb_html'></textarea>				
											
										<%
										if(cat.value == NurseActionDetailsDTO.ACTION_BLOOD_SUGAR)
										{
											%>
											<b><%=Language.equalsIgnoreCase("english")?"Strip Count":"স্ট্রিপের সংখ্যা"%>:</b><br>
											<table>
												<tr>
													<td>
													<%=LM.getText(LC.HM_COUNT, loginDTO)%>: <%=Utils.getDigits(accuCount, Language)%>
													<%
													%>
													</td>
													<td>
													<input type = "number" max="<%=accuCount%>" name='nurseActionDetails.stripCount' id = 'stripCount_number_<%=childTableStartingID%>' tag='pb_html' value = '0'	class='form-control' />
													</td>
<!-- 													<td> -->
<%-- 													<%=Language.equalsIgnoreCase("english")?"Box Used":"বাক্স খালি হয়েছে"%>: --%>
<%-- 													<input type='checkbox' name='boxUsedCb' id = 'boxUsedCb_<%=childTableStartingID%>' value='' onchange = "setBoxUsed(this.id)" --%>
<!-- 												   	class="form-control-sm"/> -->
<!-- 													</td> -->
													
												</tr>
												
											</table>
											<%
										}
										else
										{
											%>
											<input type = "hidden" name='nurseActionDetails.stripCount' id = 'stripCount_number_<%=childTableStartingID%>' tag='pb_html' value = '0'	class='form-control' />
											
											<%
										}
										%>
										<input type = "hidden" name='nurseActionDetails.boxUsed' id = 'boxUsed_<%=childTableStartingID%>' tag='pb_html' value = 'false'	class='form-control' />
										
									</td>
									<td>
										<span id='chkEdit'>
											<input type='checkbox' name='isDoneCB' id = 'isDoneCB_<%=childTableStartingID%>' value='' onchange = "setIsDone(this.id)"
												   class="form-control-sm"/>
											<input type='hidden' name='isDone' id = 'isDone_<%=childTableStartingID%>' value='false'
												   class="form-control-sm"/>
										</span>
									</td>
								</tr>								
								<%	
											childTableStartingID ++;
										}
									
								%>						
						
								</tbody>
							</table>
						</div>

                      </div>
                    </div>               
                <div class="form-actions text-right mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.NURSE_ACTION_ADD_NURSE_ACTION_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.NURSE_ACTION_ADD_NURSE_ACTION_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>				

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	var actionDate = getDateStringById('actionDate_date_js', 'DD/MM/YYYY');
    $("#actionDate_date_<%=i%>").val(actionDate);

	<%
	if(request.getParameter("appointmentId") == null)
	{
	%>
		var convertedPhoneNumber = phoneNumberAdd88ConvertLanguage($('#phone').val(), '<%=Language%>');
		if($("#phone").val() == "" || $("#name").val() == "")
		{
			toastr.error("Name or Phone number cannot be empty.");
			return false
		}
		$("#phone").val(convertedPhoneNumber);
	
		var trueCount = 0;
		for(i = 1; i < child_table_extra_id; i ++)
		{
			if($("#isDone_" + i).val() == "true")
			{
				trueCount ++;
			}
		}
		if(trueCount == 0)
		{
			toastr.error("You did nothing!");
		}
		else if($("#patientUserId").val() == "")
		{
			toastr.error("No patient selected.");
		}
		else
		{
			submitAddForm2();
		}
	<%
	}
	else
	{
	%>
		var trueCount = 0;
		for(i = 1; i < child_table_extra_id; i ++)
		{
			if($("#isDone_" + i).val() == "true")
			{
				trueCount ++;
			}
		}
		if(trueCount == 0)
		{
			toastr.error("You did nothing!");
		}		
		else
		{
			submitAddForm2();
		}
	<%
	}
	%>
	
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Nurse_actionServlet");	
}

function init(row)
{

	setDateByTimestampAndId('actionDate_date_js', '<%=nurse_actionDTO.actionDate%>');
	
}

var row = 0;
$(document).ready(function(){
	init(row);

	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;

function getInfo()
{
	var userName = $("#patientUserId").val();
	console.log("userName = " + userName);
	$('#name').val("");
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
        	try 
        	{
	            var familyMemberDTO = JSON.parse(this.responseText);
	            <%
	            if(Language.equalsIgnoreCase("english"))
	            {
	            %>
	            $('#name').val(familyMemberDTO.nameEn);
	            <%
	            }
	            else
	            {
	            %>
	            $('#name').val(familyMemberDTO.nameBn);
	            <%
	            }
	            %>
	            if(familyMemberDTO.mobile != "")
	           	{
	            	$('#phone').val(phoneNumberRemove88ConvertLanguage(familyMemberDTO.mobile, '<%=Language%>'));
	           	}
        	}
        	catch (e)
        	{
        		
        	}
            
           
        } else {
        	
        }
    };

    xhttp.open("POST", "FamilyServlet?actionType=getPatientDetails&familyMemberId=" + $("#whoIsThePatientCat_category_0").val() + "&userName=" + userName + "&language=<%=Language%>", true);
    xhttp.send();
}

function patient_inputted(userName, orgId) {
    console.log('changed value: ' + userName);
    $("#patientUserId").val(userName);
    console.log("set userName = " + $("#patientUserId").val());
    
    $("#patientDiv").removeAttr("style");
    $('#name').val("");
	$('#phone').val("");
	$("#whoIsThePatientCat_category_0").val("-1");
    
    if(convertToEnglishNumber(userName) == '<%=SessionConstants.OUTSIDER_PATIENT%>')
   	{
   		$("#othersOfficeDiv").removeAttr("style");
   		$("#patientTypeDiv").css("display", "none");
   		
   	}
    else
   	{
    	$("#othersOfficeDiv").css("display", "none");
    	$("#patientTypeDiv").removeAttr("style");
	    
	    
	    
    	var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var options = this.responseText;
                if(options == null)
               	{
                	$("#patientUserId").val("");
                	toastr.error("Invalid Patient");
               	}
                else
               	{
                	 $("#whoIsThePatientCat_category_0").html(options);
                     getInfo();
               	}
            }
            else {
            	
            	
            }
        };

        xhttp.open("POST", "FamilyServlet?actionType=getFamily&userName=" + userName + "&language=<%=Language%>&defaultOption=-1", true);
        xhttp.send();
   	}
}

function setIsDone(cbId)
{
	var rowId = cbId.split("_")[1];
	var hiddenId = "isDone_" + rowId;
	$("#" + hiddenId).val($("#" + cbId).prop("checked"));
}

function setBoxUsed(cbId)
{
	var rowId = cbId.split("_")[1];
	var hiddenId = "boxUsed_" + rowId;
	$("#" + hiddenId).val($("#" + cbId).prop("checked"));
}


</script>






