

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="nurse_action.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>
<%@page import="appointment.*"%>




<%
String servletName = "Nurse_actionServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Nurse_actionDTO nurse_actionDTO = Nurse_actionDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = nurse_actionDTO;
AppointmentDAO appointmentDAO = new AppointmentDAO();
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.NURSE_ACTION_ADD_NURSE_ACTION_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.NURSE_ACTION_ADD_NURSE_ACTION_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.HM_NURSE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											  <%
											        value = WorkflowController.getNameFromOrganogramId(nurse_actionDTO.nurseOrganogramId, Language);
											    %>
											
											    <b><%=value%></b>
											    
											    <br><%=LM.getText(LC.HM_ID, loginDTO)%>:<%=nurse_actionDTO.nurseUserId%>
											   
				
			
                                    </div>
                                </div>
			
								
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.HM_APPOINTMENT, loginDTO)%> <%=LM.getText(LC.HM_ID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
										<%
											if(nurse_actionDTO.appointmentId != -1)
											{
												AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(nurse_actionDTO.appointmentId);
												if(appointmentDTO != null)
												{
													%>
													<%=Utils.getDigits(appointmentDTO.sl, Language)%>
													<%
											
												}
											}
											%>
				
			
                                    </div>
                                </div>
                                
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.HM_REFERENCE_EMPLOYEE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											 <%=LM.getText(LC.HM_ID, loginDTO)%>:<%=nurse_actionDTO.patientUserId%>
											 <br><%=WorkflowController.getNameFromUserName(nurse_actionDTO.patientUserId, Language)%>			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.HM_PATIENT_NAME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											 <b><%=nurse_actionDTO.name%></b>
											    <%
											    if(nurse_actionDTO.othersOfficeId != -1)
											    {
											    	%>
											    	<br>
											    	<%=nurse_actionDTO.othersDesignationAndId.equalsIgnoreCase("")?"":nurse_actionDTO.othersDesignationAndId + ", "%>
											    	<%
											    	if(isLanguageEnglish)
											    	{
											    		%>
											    		<%=nurse_actionDTO.othersOfficeEn%>
											    		<%
											    	}
											    	else
											    	{
											    		%>
											    		<%=nurse_actionDTO.othersOfficeBn%>
											    		<%
											    	}
											    }
											    
											    %>
                                    </div>
                                </div>
                                
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.HM_PHONE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											 <%=Utils.getDigits(nurse_actionDTO.phone, Language)%>			
                                    </div>
                                </div>
			
								
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.NURSE_ACTION_ADD_ACTIONDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = nurse_actionDTO.actionDate + "";
											%>
											<%
											String formatted_actionDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_actionDate, Language)%>
				
			
                                    </div>
                                </div>
			
			
			
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%=LM.getText(LC.NURSE_ACTION_ADD_NURSE_ACTION_DETAILS, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.NURSE_ACTION_ADD_NURSE_ACTION_DETAILS_NURSEACTIONCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.NURSE_ACTION_ADD_NURSE_ACTION_DETAILS_DESCRIPTION, loginDTO)%></th>
								<th><%=Language.equalsIgnoreCase("english")?"Strip Count":"স্ট্রিপের সংখ্যা"%></th>
							</tr>
							<%
                        	NurseActionDetailsDAO nurseActionDetailsDAO = NurseActionDetailsDAO.getInstance();
                         	List<NurseActionDetailsDTO> nurseActionDetailsDTOs = (List<NurseActionDetailsDTO>)nurseActionDetailsDAO.getDTOsByParent("nurse_action_id", nurse_actionDTO.iD);
                         	
                         	for(NurseActionDetailsDTO nurseActionDetailsDTO: nurseActionDetailsDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%
											value = nurseActionDetailsDTO.nurseActionCat + "";
											%>
											<%
											value = CatRepository.getInstance().getText(Language, "nurse_action", nurseActionDetailsDTO.nurseActionCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = nurseActionDetailsDTO.description + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%=nurseActionDetailsDTO.nurseActionCat == NurseActionDetailsDTO.ACTION_BLOOD_SUGAR ?  Utils.getDigits(nurseActionDetailsDTO.stripCount, Language):""%>
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
        </div>
    </div>
</div>