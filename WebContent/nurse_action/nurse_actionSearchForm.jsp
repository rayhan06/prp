
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="nurse_action.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="appointment.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
String navigator2 = "navNURSE_ACTION";
String servletName = "Nurse_actionServlet";
AppointmentDAO appointmentDAO = new AppointmentDAO();
NurseActionDetailsDAO nurseActionDetailsDAO = NurseActionDetailsDAO.getInstance();
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped text-nowrap">
						<thead>
							<tr>
								<th><%=LM.getText(LC.HM_SL, loginDTO)%>
            					</th>
								<th><%=LM.getText(LC.HM_NURSE, loginDTO)%></th>
								
								<th><%=LM.getText(LC.HM_APPOINTMENT, loginDTO)%> <%=LM.getText(LC.HM_ID, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_PATIENT_ID, loginDTO)%></th>
								
								<th><%=LM.getText(LC.NURSE_ACTION_ADD_ACTIONDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_ACTION, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								

								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Nurse_actionDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Nurse_actionDTO nurse_actionDTO = (Nurse_actionDTO) data.get(i);
																																
											
											%>
											<tr>
											<td>
												<%=Utils.getDigits(i + 1 + ((rn2.getCurrentPageNo() - 1) * rn2.getPageSize()), Language) %>
											</td>
		
											<td>
											 <%
											        value = WorkflowController.getNameFromOrganogramId(nurse_actionDTO.nurseOrganogramId, Language);
											    %>
											
											    <b><%=value%></b>
											    
											    <br><%=LM.getText(LC.HM_ID, loginDTO)%>:<%=nurse_actionDTO.nurseUserId%>
											   
				
			
											</td>
		
											
											<td>
											<%
											if(nurse_actionDTO.appointmentId != -1)
											{
												AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(nurse_actionDTO.appointmentId);
												if(appointmentDTO != null)
												{
													%>
													<%=Utils.getDigits(appointmentDTO.sl, Language)%>
													<%
											
												}
											}
											%>
				
											
				
			
											</td>
											
		
											<td>
											
											    <b><%=nurse_actionDTO.name%></b>
											    <%
											    if(nurse_actionDTO.othersOfficeId != -1)
											    {
											    	%>
											    	<br>
											    	<%=nurse_actionDTO.othersDesignationAndId.equalsIgnoreCase("")?"":nurse_actionDTO.othersDesignationAndId + ", "%>
											    	<%
											    	if(isLanguageEnglish)
											    	{
											    		%>
											    		<%=nurse_actionDTO.othersOfficeEn%>
											    		<%
											    	}
											    	else
											    	{
											    		%>
											    		<%=nurse_actionDTO.othersOfficeBn%>
											    		<%
											    	}
											    }
											    
											    %>
											    <br><%=LM.getText(LC.HM_ID, loginDTO)%>:<%=nurse_actionDTO.patientUserId%>
											    <br><%=Utils.getDigits(nurse_actionDTO.phone, Language)%>
				
			
											</td>
		
											
		
											<td>
											<%
											value = nurse_actionDTO.actionDate + "";
											%>
											<%
											String formatted_actionDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_actionDate, Language)%>
				
			
											</td>
											
											<td>
											<%
											List<NurseActionDetailsDTO> nurseActionDetailsDTOs = (List<NurseActionDetailsDTO>)nurseActionDetailsDAO.getDTOsByParent("nurse_action_id", nurse_actionDTO.iD);
				                         	
											boolean b1st = true;
				                         	for(NurseActionDetailsDTO nurseActionDetailsDTO: nurseActionDetailsDTOs)
				                         	{
				                         		if(!b1st)
				                         		{
				                         			%>
				                         			, 
				                         			<%
				                         		}
				                         		%>
				                         		<%=CatRepository.getInstance().getText(Language, "nurse_action", nurseActionDetailsDTO.nurseActionCat)%>
				                         		<%
				                         		b1st = false;
				                         	}
											%>
											</td>

	
											<td>
											    
											    <button
											            type="button"
											            class="btn-sm border-0 shadow btn-border-radius text-white"
											            style="background-color: #ff6b6b;"
											            onclick="location.href='<%=servletName%>?actionType=view&ID=<%=nurse_actionDTO.iD%>'"
											    >
											        <i class="fa fa-eye"></i>
											    </button>
											   
											</td>										
																						
											<td class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=nurse_actionDTO.iD%>'/></span>
												</div>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			