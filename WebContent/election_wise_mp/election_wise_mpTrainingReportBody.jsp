<%@ page import="util.HttpRequestUtils" %>
<%@ page import="language.LM" %>
<%@ page import="language.LC" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="election_wise_mp.Election_wise_mpRepository" %>
<%@ page contentType="text/html;charset=utf-8" %>

<%
    boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String reportName = isLangEn ? "Conference Report of Members of Parliaments"
                                 : "সংসদ সদস্যগণের সম্মেলনের রিপোর্ট";
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-cubes"></i>
                    <%=reportName%>
                </h3>
            </div>
        </div>
        <form>
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                        </div>
                                    </div>

                                    <div id="searchCriteria" class="">
                                        <div class="search-criteria-div col-12">
                                            <div class="form-group row">
                                                <label class="col-md-2 control-label text-md-right"
                                                       for="electionWiseMpIds">
                                                    <%=isLangEn ? "Members of Parliament" : "সংসদ সদস্য"%>
                                                </label>
                                                <div class="col-md-9">
                                                    <select multiple="multiple" class='form-control'
                                                            name='electionWiseMpIds' id='electionWiseMpIds'
                                                    >
                                                        <%=Election_wise_mpRepository.getInstance().buildOptionsOfActiveMps(Language, null)%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right mt-3">
                        <button type="button" class="btn btn-sm btn-info shadow btn-border-radius"
                                id='reportSearchButton'
                                onclick="getReportTableWithAjax()">
                            <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%>
                        </button>
                    </div>
                </div>
                <div class="">
                    <div class="form-body">
                        <div class="d-flex flex-column flex-md-row align-items-md-end justify-content-md-between mt-5">
                            <h5 class="table-title">
                                <%=reportName%>
                            </h5>
                            <div id="btndiv" class="mb-2">
                                <button type="button"
                                        class="btn btn-sm btn-warning text-white shadow btn-border-radius"
                                        id='reportPrinter'
                                        onclick="localPrint()"><%=LM.getText(LC.HM_PRINT, loginDTO)%>
                                </button>
                            </div>
                        </div>
                        <div>
                            <div class="table-responsive" id="div-to-print">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div id="reportTable_info" class="my-3">
                                    </div>
                                    <h6 class="mr-2 font-weight-bold mb-0" id="not-senior-wise">
                                        জ্যেষ্ঠতার ভিত্তিতে নয়
                                    </h6>
                                </div>
                                <table class="table table-striped table-bordered"
                                       id="trainingReportTable"
                                >
                                    <thead>
                                    <tr>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<style>
    .loader-container-circle {
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.2);
        position: fixed;
        top: 0;
        left: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: 3;
        visibility: visible;
    }

    @media (min-width: 1015px) {
        .loader-container-circle {
            width: calc(100% + 260px);
            height: 100%;
            background-color: rgba(0, 0, 0, 0.1);
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            z-index: 3;
            visibility: visible;
        }
    }

    .loader-circle {
        width: 50px;
        height: 50px;
        border: 5px solid;
        color: #3498db;
        border-radius: 50%;
        border-top-color: transparent;
        animation: loader 1.2s linear infinite;
    }

    @keyframes loader {
        25% {
            color: #2ecc71;
        }
        50% {
            color: #f1c40f;
        }
        75% {
            color: #e74c3c;
        }
        to {
            transform: rotate(360deg);
        }
    }
</style>

<div class="loader-container-circle" id="full-page-loader">
    <div class="loader-circle"></div>
</div>

<script>
    const $fullPageLoader = $('#full-page-loader');
    const $trainingReportTable = $('#trainingReportTable');

    $(document).ready(() => {
        select2MultiSelector('#electionWiseMpIds', '<%=Language%>');
        $fullPageLoader.hide();
    });

    function localPrint() {
        let scrollY = Math.ceil(window.scrollY);
        printAnyDiv('div-to-print');
        window.scrollTo(0, scrollY);
    }

    async function getReportTableWithAjax() {
        $fullPageLoader.show();
        $trainingReportTable.html('');
        let electionWiseMpIds = '';
        const $electionWiseMpIds = $('#electionWiseMpIds');
        if ($electionWiseMpIds != null) {
            electionWiseMpIds = $electionWiseMpIds.val().join(',');
        }
        try {
            const url = "Election_wise_mpServlet?actionType=ajax_getTrainingReport"
                        + "&electionWiseMpIds=" + electionWiseMpIds;
            const res = await fetch(url);
            $trainingReportTable.html(await res.text());
        } catch (error) {
            console.error(error);
            $trainingReportTable.html('সার্ভারে সমস্যা! কিছুক্ষন পর চেষ্টা করুন');
        }
        $fullPageLoader.hide();
    }
</script>