<%@ page import="election_wise_mp.Election_wise_mpInfoModel" %>
<%@ page import="java.util.List" %>
<%@ page import="election_wise_mp.Election_wise_mpTrainingReportModel" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Collections" %>
<%@ page import="geolocation.GeoCountryRepository" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page contentType="text/html;charset=utf-8" %>

<%
    List<Election_wise_mpInfoModel> mpInfoModels =
            (List<Election_wise_mpInfoModel>) request.getAttribute("mpInfoModels");

    List<Election_wise_mpTrainingReportModel> mpTrainingReportModels =
            (List<Election_wise_mpTrainingReportModel>) request.getAttribute("mpTrainingReportModels");

    Map<Long, List<Election_wise_mpTrainingReportModel>> mpTrainingReportModelsByElectionWiseMpId =
            mpTrainingReportModels.stream()
                                  .collect(Collectors.groupingBy(
                                          mpTrainingReportModel -> mpTrainingReportModel.electionWiseMpId
                                  ));
%>

<thead>
<tr>
    <th class="text-center">বাংলাদেশ নাম্বার</th>
    <th class="text-center">নাম</th>
    <th class="text-center">সম্মেলনের নাম</th>
    <th class="text-center">দেশ</th>
    <th class="text-center">সম্মেলনের সময়কাল</th>
</tr>
</thead>

<tbody>
<%if (mpInfoModels.isEmpty()) {%>
<tr>
    <td class="text-center" colspan="100%">
        সম্মেলন বিষয়ক কোনো তথ্য পাওয়া যায় নি
    </td>
</tr>
<%
} else {
    for (Election_wise_mpInfoModel mpInfoModel : mpInfoModels) {
        List<Election_wise_mpTrainingReportModel> reportModels =
                mpTrainingReportModelsByElectionWiseMpId.getOrDefault(mpInfoModel.electionWiseMpId, Collections.emptyList());
        int rowSpan = reportModels.isEmpty() ? 1 : reportModels.size();
%>
<tr>
    <td class="text-center" rowspan="<%=rowSpan%>">
        <%=mpInfoModel.bangladeshNumberBn%>
    </td>
    <td class="text-center" rowspan="<%=rowSpan%>">
        <%=mpInfoModel.mpNameBn%>
    </td>
    <%if (reportModels.isEmpty()) {%>
    <td class="text-center" colspan="3">
        সম্মেলন বিষয়ক কোনো তথ্য পাওয়া যায় নি
    </td>
    <%
    } else {
        Election_wise_mpTrainingReportModel firstReportModel = reportModels.get(0);
    %>
    <td>
        <%=firstReportModel.trainingNameBn%>
    </td>
    <td>
        <%=GeoCountryRepository.getInstance().getText(false, firstReportModel.country)%>
    </td>
    <td>
        <%=DateUtils.getDateInWord("bangla", firstReportModel.startDate)%>
        -
        <%=DateUtils.getDateInWord("bangla", firstReportModel.endDate)%>
    </td>
    <%}%>
</tr>

<%
    for (int i = 1; i < reportModels.size(); ++i) {
        Election_wise_mpTrainingReportModel reportModel = reportModels.get(i);
%>
<tr>
    <td>
        <%=reportModel.trainingNameBn%>
    </td>
    <td>
        <%=GeoCountryRepository.getInstance().getText(false, reportModel.country)%>
    </td>
    <td>
        <%=DateUtils.getDateInWord("bangla", reportModel.startDate)%>
        -
        <%=DateUtils.getDateInWord("bangla", reportModel.endDate)%>
    </td>
</tr>
<%}%>

<%
        }
    }
%>
</tbody>