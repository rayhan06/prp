<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="election_wise_mp.*" %>
<%@page import="java.util.*" %>
<%@page import="election_details.*" %>
<%@page import="employee_records.*" %>
<%@page import="political_party.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="election_constituency.Election_constituencyRepository" %>

<%
    Election_wise_mpDTO election_wise_mpDTO;
    election_wise_mpDTO = (Election_wise_mpDTO) request.getAttribute("election_wise_mpDTO");
    List<Election_detailsDTO> electionDetailsDtos = (List<Election_detailsDTO>) request.getAttribute("electionDetailsDtos");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (election_wise_mpDTO == null) {
        election_wise_mpDTO = new Election_wise_mpDTO();

    }
    System.out.println("election_wise_mpDTO = " + election_wise_mpDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.ELECTION_WISE_MP_ADD_ELECTION_WISE_MP_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;
    int childTableStartingID = 1;
    boolean isPermanentTable = true;
    String Language = LM.getText(LC.ELECTION_WISE_MP_EDIT_LANGUAGE, loginDTO);
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i><%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form"
              action="Election_wise_mpServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="election-wise-mp-form" name="election-wise-mp-form" method="POST"
              enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <!-- FORM BODY SKULL -->
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-10 offset-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%= LM.getText(LC.ELECTION_WISE_MP_ELECTION, loginDTO) %>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-8" id='electionDetails'>
                                            <select onchange="setDate()" class='form-control'
                                                    name='electionDetailsId'
                                                    id='electionDetailsId' tag='pb_html'>
                                                <%= Election_detailsRepository.getInstance().buildOptions(Language, election_wise_mpDTO.electionDetailsId)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%= LM.getText(LC.ELECTION_WISE_MP_ELECTION_CONSTITUENCY, loginDTO) %>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-8" id='electionConstituency'>
                                            <select class='form-control'
                                                    name='electionConstituencyId'
                                                    id='electionConstituencyId'
                                                    tag='pb_html'>
                                                <%if (actionName.equals("edit")) {%>
                                                <%= Election_constituencyRepository.getInstance().buildOptions(Language, election_wise_mpDTO.electionConstituencyId)%>
                                                <%}%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%= LM.getText(LC.ELECTION_WISE_MP_MEMBER_OF_PARLIAMENT, loginDTO) %>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-8" id='employeeRecords'>
                                            <select class='form-control'
                                                    name='employeeRecordsId' id='employeeRecordsId'
                                                    tag='pb_html'>
                                                <%=new Employee_recordsDAO().mpMemberBuildOption(Language, election_wise_mpDTO.employeeRecordsId)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%= LM.getText(LC.ELECTION_WISE_MP_POLITICAL_PARTY, loginDTO) %>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-8" id='politicalParty'>
                                            <select class='form-control'
                                                    name='politicalPartyId' id='politicalPartyId'
                                                    tag='pb_html'>
                                                <%=Political_partyRepository.getInstance().buildOptions(Language, election_wise_mpDTO.politicalPartyId)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.ELECTION_WISE_MP_ADD_STARTDATE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-8" id='startDate_div_<%=i%>'>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID"
                                                           value="startDate_date_js"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                        </div>
                                        <input type='hidden' class='form-control'
                                               id='startDate_date_<%=i%>' name='startDate'
                                               value=''
                                               tag='pb_html'/>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.ELECTION_WISE_MP_ADD_ENDDATE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-8" id='endDate_div_<%=i%>'>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID"
                                                           value="endDate_date_js"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                        </div>
                                        <input type='hidden' class='form-control'
                                               id='endDate_date_<%=i%>' name='endDate'
                                               value=''
                                               tag='pb_html'/>
                                    </div>
                                    <!-- Hidden Fields -->
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=election_wise_mpDTO.iD%>' tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-10 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                            <%=LM.getText(LC.ELECTION_WISE_MP_ADD_ELECTION_WISE_MP_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                            <%=LM.getText(LC.ELECTION_WISE_MP_ADD_ELECTION_WISE_MP_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    var startDateMap = new Map();
    var endDateMap = new Map();
    let language = '<%=Language%>';
    $(document).ready(function () {

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        select2SingleSelector("#electionDetailsId", "<%=Language%>");
        select2SingleSelector("#electionConstituencyId", "<%=Language%>");
        select2SingleSelector("#employeeRecordsId", "<%=Language%>");
        select2SingleSelector("#politicalPartyId", "<%=Language%>");
        loadElectionDates();
        $('#startDate_date_js').on('datepicker.change', () => {
            setMinDateById('endDate_date_js', getDateStringById('startDate_date_js'));
        });

        dateTimeInit("<%=Language%>");
        $.validator.addMethod('electionSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('constituencySelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('employeeSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('politicalPartySelection', function (value, element) {
            return value != 0;
        });
        $("#election-wise-mp-form").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                electionDetailsId: {
                    required: true,
                    electionSelection: true
                },
                electionConstituencyId: {
                    required: true,
                    constituencySelection: true
                },
                employeeRecordsId: {
                    required: true,
                    employeeSelection: true
                },
                politicalPartyId: {
                    required: true,
                    politicalPartySelection: true
                }
            },

            messages: {
                electionDetailsId: "Please enter election",
                electionConstituencyId: "Please enter election constituency",
                employeeRecordsId: "Please enter employee",
                politicalPartyId: "Please enter political party"

            }
        });

    });

    function PreprocessBeforeSubmiting(row, validate) {
        $('#startDate_date_0').val(getDateStringById('startDate_date_js'));
        $('#endDate_date_0').val(getDateStringById('endDate_date_js'));
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }
        return dateValidator('startDate_date_js', true) && dateValidator('endDate_date_js', true);
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Election_wise_mpServlet");
    }

    var row = 0;

    window.onload = function () {
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;
    $(function () {

        <%
      if(actionName.equals("edit")) {
         String startStr = election_wise_mpDTO.startDate > SessionConstants.MIN_DATE ? dateFormat.format(new Date(election_wise_mpDTO.startDate)) : "";
         String endStr = election_wise_mpDTO.endDate > SessionConstants.MIN_DATE ? dateFormat.format(new Date(election_wise_mpDTO.endDate)) : "";
         %>
        $("#startDate_date_0").val('<%=startStr%>');
        $("#endDate_date_0").val('<%=endStr%>');
        setDateByStringAndId('startDate_date_js', '<%=startStr%>');
        setDateByStringAndId('endDate_date_js', '<%=endStr%>');
        setMinDateById('endDate_date_js', getDateStringById('startDate_date_js'));
        <%}
        %>
    });

    function loadElectionDates() {

        <%
        for(Election_detailsDTO dto: electionDetailsDtos) {
            String dateStr = dateFormat.format(new Date(dto.electionDate));%>
        startDateMap[<%=dto.iD%>] = '<%=dateStr%>';

        <%}
        %>

        <%
        for(Election_detailsDTO dto: electionDetailsDtos) {
            String dateStr = dateFormat.format(new Date(dto.parliamentLastDate));%>
        endDateMap[<%=dto.iD%>] = '<%=dateStr%>';
        <%}
        %>

    }

    function setDate() {
        let selectedVal = $('#electionDetailsId').val();
        $("#startDate_date_0").val(startDateMap[selectedVal]);
        $("#endDate_date_0").val(endDateMap[selectedVal]);
        changeElection();
    }

    function changeElection() {
        document.getElementById("electionConstituencyId").innerHTML = '';
        electionDetailsId = $('#electionDetailsId').val();
        if (!electionDetailsId) {
            return;
        }
        let url = "Election_constituencyServlet?actionType=buildElectionConstituency&election_id=" + electionDetailsId + "&language=" + language;
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                console.log(fetchedData);
                document.getElementById("electionConstituencyId").innerHTML = fetchedData;
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
</script>






