<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="election_wise_mp.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page import="election_details.Election_detailsDTO" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="election_constituency.Election_constituencyDTO" %>
<%@ page import="election_constituency.Election_constituencyRepository" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="political_party.Political_partyDTO" %>
<%@ page import="political_party.Political_partyRepository" %>
<%@ page import="util.StringUtils" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String Language = LM.getText(LC.ELECTION_WISE_MP_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equals("English");

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    Election_wise_mpDTO election_wise_mpDTO = Election_wise_mpRepository.getInstance().getDTOByID(id);
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.ELECTION_WISE_MP_ADD_ELECTION_WISE_MP_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div>
                <h5 class="table-title">
                    <%=LM.getText(LC.ELECTION_WISE_MP_ADD_ELECTION_WISE_MP_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">
                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.ELECTION_WISE_MP_ELECTION, loginDTO)%>
                            </b></td>
                            <td>
                                <%
                                    Election_detailsDTO electionDetailsDTO = Election_detailsRepository.getInstance().getElectionDetailsDTOByID(election_wise_mpDTO.electionDetailsId);
                                %>
                                <%= electionDetailsDTO == null ? "" : (isLanguageEnglish ? electionDetailsDTO.parliamentNumber : StringUtils.convertToBanNumber(String.valueOf(electionDetailsDTO.parliamentNumber)))%>
                            </td>
                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.ELECTION_WISE_MP_ELECTION_CONSTITUENCY, loginDTO)%>
                                </b></td>
                            <td>
                                <%
                                    Election_constituencyDTO constituencyDTO = Election_constituencyRepository.getInstance().getElection_constituencyDTOByID(election_wise_mpDTO.electionConstituencyId);
                                %>
                                <%= constituencyDTO == null ? "" : (isLanguageEnglish ? String.valueOf(constituencyDTO.constituencyNumber).concat(" | ").concat(constituencyDTO.constituencyNameEn)
                                        : StringUtils.convertToBanNumber(String.valueOf(constituencyDTO.constituencyNumber)).concat(" | ").concat(constituencyDTO.constituencyNameBn))
                                %>
                            </td>
                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.ELECTION_WISE_MP_MEMBER_OF_PARLIAMENT, loginDTO)%>
                            </b></td>
                            <td>
                                <%
                                    Employee_recordsDTO recordDTO = Employee_recordsRepository.getInstance().getById(election_wise_mpDTO.employeeRecordsId);
                                %>
                                <%= recordDTO == null ? "" : (isLanguageEnglish ? recordDTO.nameEng : recordDTO.nameBng)%>
                            </td>
                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.ELECTION_WISE_MP_POLITICAL_PARTY, loginDTO)%>
                            </b></td>
                            <td>
                                <%
                                    Political_partyDTO politicalPartyDTO = Political_partyRepository.getInstance().getPolitical_partyDTOByID(election_wise_mpDTO.politicalPartyId);
                                %>
                                <%=politicalPartyDTO == null ? "" : (isLanguageEnglish ? politicalPartyDTO.nameEn : politicalPartyDTO.nameBn)%>
                            </td>
                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.ELECTION_WISE_MP_ADD_STARTDATE, loginDTO)%>
                            </b></td>
                            <td>
                                <%
                                    String formatted_startDate = "";
                                    if (election_wise_mpDTO.startDate > SessionConstants.MIN_DATE) {
                                        formatted_startDate = simpleDateFormat.format(new Date(election_wise_mpDTO.startDate));
                                        if (!isLanguageEnglish) {
                                            formatted_startDate = StringUtils.convertToBanNumber(formatted_startDate);
                                        }
                                    }
                                %>
                                <%=formatted_startDate%>
                            </td>

                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.ELECTION_WISE_MP_ADD_ENDDATE, loginDTO)%>
                            </b></td>
                            <td>
                                <%
                                    String formatted_endDate = "";
                                    if (election_wise_mpDTO.endDate > SessionConstants.MIN_DATE) {
                                        formatted_endDate = simpleDateFormat.format(new Date(election_wise_mpDTO.endDate));
                                        if (!isLanguageEnglish) {
                                            formatted_endDate = StringUtils.convertToBanNumber(formatted_endDate);
                                        }
                                    }
                                %>
                                <%=formatted_endDate%>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>