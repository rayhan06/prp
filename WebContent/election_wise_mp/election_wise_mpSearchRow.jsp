<%@page pageEncoding="UTF-8" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="election_constituency.Election_constituencyRepository" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="political_party.Political_partyRepository" %>
<%@ page import="util.StringUtils" %>


<td id='<%=i%>_electionDetailsId'>
    <%= Election_detailsRepository.getInstance().getText(election_wise_mpDTO.electionDetailsId, Language)%>
</td>


<td id='<%=i%>_electionConstituencyId'>
    <%=Election_constituencyRepository.getInstance().getConstituencyText(election_wise_mpDTO.electionConstituencyId, Language)%>
</td>


<td id='<%=i%>_employeeRecordsId'>
    <%=Employee_recordsRepository.getInstance().getEmployeeName(election_wise_mpDTO.employeeRecordsId, Language) %>
</td>


<td id='<%=i%>_politicalPartyId'>
    <%= Political_partyRepository.getInstance().getText(election_wise_mpDTO.politicalPartyId, Language)%>
</td>


<td id='<%=i%>_startDate'>
    <%=StringUtils.getFormattedDate(Language, election_wise_mpDTO.startDate)%>
</td>

<td id='<%=i%>_endDate'>
    <%=StringUtils.getFormattedDate(Language, election_wise_mpDTO.endDate)%>
</td>