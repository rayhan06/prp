<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="election_wise_mp.*" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.List" %>
<%@ page import="common.BaseServlet" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String navigator2 = SessionConstants.NAV_ELECTION_WISE_MP;
    RecordNavigator rn2 = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    String Language = LM.getText(LC.ELECTION_WISE_MP_EDIT_LANGUAGE, loginDTO);
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap" style="width: 100%">
        <thead>
        <tr>
            <th><%=LM.getText(LC.ELECTION_WISE_MP_ELECTION, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.ELECTION_WISE_MP_ELECTION_CONSTITUENCY, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.ELECTION_WISE_MP_MEMBER_OF_PARLIAMENT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.ELECTION_WISE_MP_POLITICAL_PARTY, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.ELECTION_WISE_MP_ADD_STARTDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.ELECTION_WISE_MP_ADD_ENDDATE, loginDTO)%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Election_wise_mpDTO> data = (List<Election_wise_mpDTO>) rn2.list;
            if (data != null && data.size() > 0) {
                for (int i = 0; i < data.size(); i++) {
                    Election_wise_mpDTO election_wise_mpDTO = data.get(i);
        %>
        <tr id='tr_<%=i%>'>
            <%@include file="/election_wise_mp/election_wise_mpSearchRow.jsp" %>
        </tr>
        <%
                }
            }
        %>
        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>