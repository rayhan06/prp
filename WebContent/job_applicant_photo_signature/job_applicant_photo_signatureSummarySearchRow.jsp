<%@page pageEncoding="UTF-8" %>

<%@page import="job_applicant_photo_signature.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="files.FilesDTO" %>
<%@ page import="files.FilesDAO" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.JOB_APPLICANT_PHOTO_SIGNATURE_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);




Job_applicant_photo_signatureDTO job_applicant_photo_signatureDTO = (Job_applicant_photo_signatureDTO)request.getAttribute("job_applicant_photo_signatureDTO");
CommonDTO commonDTO = job_applicant_photo_signatureDTO;
String servletName = "Job_applicant_photo_signatureServlet";


System.out.println("job_applicant_photo_signatureDTO = " + job_applicant_photo_signatureDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Job_applicant_photo_signatureDAO job_applicant_photo_signatureDAO = new Job_applicant_photo_signatureDAO();


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


		
											
											<td id = '<%=i%>_photoOrSignature'>
											<%
											value = job_applicant_photo_signatureDTO.photoOrSignature==1? LM.getText(LC.PHOTO_OR_SIGNATURE_PHOTO):LM.getText(LC.PHOTO_OR_SIGNATURE_SIGNATURE) + "";
											%>
														
											<%=value%>
				
			
											</td>


<td class="">
	<div class="">
		<%

			FilesDAO filesDAO = new FilesDAO();
			List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(job_applicant_photo_signatureDTO.filesDropzone);
		%>
		<table>
			<tr>
				<%
					if(filesDropzoneDTOList != null)
					{
						for(int j = 0; j < filesDropzoneDTOList.size(); j ++)
						{
							FilesDTO filesDTO = filesDropzoneDTOList.get(j);
							byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
				%>
				<td id = 'filesDropzone_td_<%=filesDTO.iD%>'>
					<%
						if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)
						{
					%>
					<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)
						%>' style='width:100px' />
					<%
						}
					%>
					<a href = 'Job_applicant_photo_signatureServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>
				</td>
				<%
						}
					}
				%>
			</tr>
		</table>

	</div>

</td>
