
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="job_applicant_photo_signature.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="java.util.List" %>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}

String value = "";
String Language = LM.getText(LC.JOB_APPLICANT_PHOTO_SIGNATURE_EDIT_LANGUAGE, loginDTO);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


Job_applicant_photo_signatureDAO job_applicant_photo_signatureDAO = new Job_applicant_photo_signatureDAO();


String successMessageForwarded = "Forwarded to your Senior Office";
String successMessageApproved = "Approval Done";

String ajax = request.getParameter("ajax");
boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>	

<%

if(hasAjax == false)
{
	Enumeration<String> parameterNames = request.getParameterNames();

	while (parameterNames.hasMoreElements()) 
	{

		String paramName = parameterNames.nextElement();
	   
		if(!paramName.equalsIgnoreCase("actionType"))
		{
			String[] paramValues = request.getParameterValues(paramName);
			for (int i = 0; i < paramValues.length; i++) 
			{
				String paramValue = paramValues[i];
				
				%>
				
				<%
				
			}
		}
	   

	}
}

					String formTitle = LM.getText(LC.JOB_APPLICANT_PHOTO_SIGNATURE_ADD_JOB_APPLICANT_PHOTO_SIGNATURE_ADD_FORMNAME, loginDTO);

				%>

<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.JOB_APPLICANT_PHOTO_SIGNATURE_ADD_PHOTOORSIGNATURE, loginDTO)%></th>
								<th><%=LM.getText(LC.JOB_APPLICANT_PHOTO_SIGNATURE_ADD_FILESDROPZONE, loginDTO)%></th>
								
							</tr>
						</thead>
						<tbody>
							<%
								List<Job_applicant_photo_signatureDTO> data = job_applicant_photo_signatureDAO.getDTOByJobApplicantID(600);

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Job_applicant_photo_signatureDTO job_applicant_photo_signatureDTO = data.get(i);
																																
											
											%>
											<tr id = 'tr_<%=i%>'>
											<%
											
								%>
											
		
								<%  								
								    request.setAttribute("job_applicant_photo_signatureDTO",job_applicant_photo_signatureDTO);
								%>  
								
								 <jsp:include page="./job_applicant_photo_signatureSummarySearchRow.jsp">
								 		<jsp:param name="pageName" value="searchrow" />
								 		<jsp:param name="rownum" value="<%=i%>" />
								 </jsp:include>			

								
								<%

											%>
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>



			