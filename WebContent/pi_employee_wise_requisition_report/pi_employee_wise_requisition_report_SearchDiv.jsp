<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%

    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.PI_EMPLOYEE_WISE_REQUISITION_REPORT_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>
<div class="row">
    <div class="col-12">
        <div class="search-criteria-div" >
            <div class="form-group row">
                <label class="col-3 control-label text-right">
                    <%=LM.getText(LC.PI_EMPLOYEE_WISE_REQUISITION_REPORT_WHERE_OFFICEUNITID, loginDTO)%>
                </label>
                <div class="col-7 offset-1">
                    <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                            id="officeUnitId_modal_button"
                            onclick="officeModalButtonClicked();">
                        <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                    </button>

                    <div class="input-group" id="officeUnitId_div" style="display: none">
                        <input type="hidden" name='officeUnitId'
                               id='officeUnitId_input' value="">
                        <button type="button" class="btn btn-secondary form-control shadow btn-border-radius"
                                disabled id="officeUnitId_text"></button>
                        <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                                    <button type="button" class="btn btn-outline-danger"
                                            onclick="crsBtnClicked('officeUnitId');"
                                            id='officeUnitId_crs_btn' tag='pb_html'>
                                        x
                                    </button>
                                </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="search-criteria-div">
            <div class="form-group row">
                <label class="col-3 control-label text-right">
                    <%=LM.getText(LC.PI_EMPLOYEE_WISE_REQUISITION_REPORT_WHERE_FISCALYEAR, loginDTO)%>
                </label>
                <div class="col-7 offset-1">
                    <select class='form-control' name='fiscalYear'
                            id='fiscalYear'
                            tag='pb_html'>
                        <%= Fiscal_yearRepository.getInstance().buildOptions(Language, -1L) %>
                    </select>
                </div>

            </div>
        </div>
        <div class="search-criteria-div" style="display: none;">
            <div class="form-group row">
                <label class="col-sm-3 control-label text-right">
                    <%=LM.getText(LC.PI_EMPLOYEE_WISE_REQUISITION_REPORT_WHERE_ISDELETED, loginDTO)%>
                </label>
                <div class="col-sm-9">
                    <input class='form-control' name='isDeleted' id='isDeleted' value=""/>
                </div>
            </div>
        </div>

    </div>
</div>
<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script type="text/javascript">
    const unitSelector = $('#officeUnitId_input');
    /*Office unit modal start*/
    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        $('#officeUnitId_modal_button').hide();
        $('#officeUnitId_div').show();
        document.getElementById('officeUnitId_text').innerHTML = selectedOffice.name;
        unitSelector.val(selectedOffice.id);
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    function officeModalButtonClicked() {
        // console.log('Button Clicked!');
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
    }

    /*Office unit modal end*/
    function init() {
        dateTimeInit($("#Language").val());
    }

    function PreprocessBeforeSubmiting() {
    }
    $(document).ready(function () {
        select2SingleSelector('#fiscalYear', '<%=Language%>');
    });
</script>