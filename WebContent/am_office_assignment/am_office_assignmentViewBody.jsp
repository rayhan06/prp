<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="am_office_assignment.*" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="files.*" %>
<%@page import="util.*" %>
<%@ page import="am_office_assignment_request.Am_office_assignment_requestDTO" %>
<%@ page import="am_office_assignment_request.Am_office_assignment_requestDAO" %>
<%@ page import="util.StringUtils" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="pb.Utils" %>
<%@ page import="am_parliament_building_block.Am_parliament_building_blockRepository" %>
<%@ page import="am_parliament_building_level.Am_parliament_building_levelRepository" %>
<%@ page import="am_parliament_building_room.Am_parliament_building_roomRepository" %>
<%@ page import="am_minister_hostel_block.Am_minister_hostel_blockRepository" %>
<%@ page import="am_minister_hostel_unit.Am_minister_hostel_unitRepository" %>
<%@ page import="am_minister_hostel_level.Am_minister_hostel_levelRepository" %>
<%@ page import="am_minister_hostel_block.AmMinisterHostelSideRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="am_other_office_unit.Am_other_office_unitRepository" %>
<%@ page import="am_other_office_unit.Am_other_office_unitDTO" %>
<%@ page import="common.BaseServlet" %>


<%
    String servletName = "Am_office_assignmentServlet";
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    Am_office_assignmentDTO am_office_assignmentDTO = (Am_office_assignmentDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    Am_office_assignment_requestDTO am_office_assignment_requestDTO = Am_office_assignment_requestDAO
            .getInstance()
            .getDTOFromID(am_office_assignmentDTO.amOfficeAssignmentRequestId);
    String context = request.getContextPath() + "/";
%>
<style>
    .text-color {
        color: #0098bf;
    }

    .form-group label {
        font-weight: 600 !important;
    }

    .form-group {
        margin-bottom: .5rem;
    }
</style>

<div class="kt-content" id="kt_content">
    <div class="kt-portlet" style="background-color: #F2F2F2!important;" id="to-print-div">
        <div class="kt-portlet__body m-4" style="background-color: #f6f9fb">


            <div class="row">
                <div class="col-12 text-center">
                    <img
                            width="10%"
                            src="<%=context%>assets/static/parliament_logo.png"
                            alt="logo"
                            class="logo-default"
                    />
                    <h2 class="mt-2 text-color">
                        <%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_BANGLADESH_NATIONAL_PARLIAMENT, loginDTO)%>
                    </h2>
                    <h4 class="text-dark">
                        <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_ASSET_MANAGEMENT_BRANCH, loginDTO)%>
                    </h4>
                    <h5 class="text-dark">
                        <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_APPROVAL_LETTER_FOR_ALLOTMENT_OF_GOVERNMENT_OFFICE, loginDTO)%>
                    </h5>
                </div>
            </div>
            <form class="form-horizontal">


                <div class="row mt-5">


                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.REPORT_SEARCH_NAME, loginDTO)%>

                            </label>
                            <div class="col-9" id="emp_name">

                                <%=UtilCharacter.getDataByLanguage(Language, am_office_assignment_requestDTO.requesterNameBn, am_office_assignment_requestDTO.requesterNameEn)%>

                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_OFFICE, loginDTO)%>

                            </label>
                            <div class="col-9" id="emp_ofc">

                                <%=UtilCharacter.getDataByLanguage(Language, am_office_assignment_requestDTO.requesterOfficeUnitNameBn, am_office_assignment_requestDTO.requesterOfficeUnitNameEn)%>

                            </div>

                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO)%>

                            </label>
                            <div class="col-9" id="emp_org">

                                <%=UtilCharacter.getDataByLanguage(Language, am_office_assignment_requestDTO.requesterOfficeUnitOrgNameBn, am_office_assignment_requestDTO.requesterOfficeUnitOrgNameEn)%>

                            </div>

                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO)%>

                            </label>
                            <div class="col-9" id="emp_phone">
                                <%=UtilCharacter.convertDataByLanguage(Language, am_office_assignment_requestDTO.requesterPhoneNum)%>
                            </div>

                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.CANDIDATE_LIST_APPLY_DATE, loginDTO)%>

                            </label>
                            <div class="col-9">
                                <%=StringUtils.getFormattedDate(Language, am_office_assignment_requestDTO.insertionDate)%>
                            </div>

                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_ALLOCATION_REQUEST_DATE, loginDTO)%>

                            </label>
                            <div class="col-9">
                                <%=StringUtils.getFormattedDate(Language, am_office_assignment_requestDTO.assignmentDate)%>
                            </div>

                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_ASSIGNMENTDATE, loginDTO)%>

                            </label>
                            <div class="col-9">
                                <%=StringUtils.getFormattedDate(Language, am_office_assignmentDTO.assignmentDate)%>
                            </div>

                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-12 col-form-label mt-2 text-color">
                                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_ASSIGNED_IN_THE_NAME_OF_THAT_OFFICE_OR_PERSON, loginDTO)%>
                            </label>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label ">
                                <%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLEOFFICENAME, loginDTO)%>

                            </label>
                            <div class="col-8">

                                <%
                                    if (am_office_assignmentDTO.officeType == 1) {
                                        Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(am_office_assignmentDTO.officeUnitId);
                                %>

                                <%=UtilCharacter.getDataByLanguage(Language, office_unitsDTO.unitNameBng, office_unitsDTO.unitNameEng)%>

                                <%
                                } else {
                                    Am_other_office_unitDTO am_other_office_unitDTO = Am_other_office_unitRepository.getInstance().getAm_other_office_unitDTOByID(am_office_assignmentDTO.officeUnitId);
                                %>

                                <%=UtilCharacter.getDataByLanguage(Language, am_other_office_unitDTO.officeNameBn, am_other_office_unitDTO.officeNameEn)%>

                                <%}%>


                            </div>

                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label ">
                                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_BUILDINGTYPECAT, loginDTO)%>

                            </label>
                            <div class="col-8">

                                <%=CatRepository.getInstance().getText(Language, "building_type", am_office_assignmentDTO.buildingTypeCat)%>

                            </div>

                        </div>
                    </div>

                    <%if (am_office_assignmentDTO.buildingTypeCat == 1) {%>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label ">
                                <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_LEVELTYPE, loginDTO)%>

                            </label>
                            <div class="col-8">

                                <%=Am_parliament_building_levelRepository.getInstance().getText(am_office_assignmentDTO.level)%>

                            </div>

                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label ">
                                <%=LM.getText(LC.ASSIGN_ASSET_ADD_BLOCKCAT, loginDTO)%>

                            </label>
                            <div class="col-8">

                                <%=Am_parliament_building_blockRepository.getInstance().getText(am_office_assignmentDTO.block, Language)%>

                            </div>

                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label ">
                                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_ROOMNO, loginDTO)%>

                            </label>
                            <div class="col-8">

                                <%=Am_parliament_building_roomRepository.getInstance().getText(am_office_assignmentDTO.roomNo)%>

                            </div>

                        </div>
                    </div>


                    <%} else if (am_office_assignmentDTO.buildingTypeCat == 2) {%>

                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label ">
                                <%=LM.getText(LC.ASSIGN_ASSET_ADD_BLOCKCAT, loginDTO)%>

                            </label>
                            <div class="col-8">

                                <%=Am_minister_hostel_blockRepository.getInstance().getAm_minister_hostel_blockDTOByID(am_office_assignmentDTO.block).blockNo%>

                            </div>

                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label ">
                                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_SEARCH_SIDE, loginDTO)%>

                            </label>
                            <div class="col-8">

                                <%=CatRepository.getInstance().getText(Language, "am_minister_hostel_side", AmMinisterHostelSideRepository.getInstance().getAmMinisterHostelSideDTOByID(am_office_assignmentDTO.side).ministerHostelSideCat)%>

                            </div>

                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label ">
                                <%=LM.getText(LC.ISSUED_TO_OFFICE_TYPE_UNIT, loginDTO)%>

                            </label>
                            <div class="col-8">

                                <%=Am_minister_hostel_unitRepository.getInstance().getAm_minister_hostel_unitDTOByID(am_office_assignmentDTO.unit).unitNumber%>

                            </div>

                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label ">
                                <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_LEVELTYPE, loginDTO)%>

                            </label>
                            <div class="col-8">

                                <%=CatRepository.getInstance().getText(Language, "am_minister_hostel_level", Am_minister_hostel_levelRepository.getInstance().getAm_minister_hostel_levelDTOByID(am_office_assignmentDTO.level).amMinisterHostelLevelCat)%>

                            </div>

                        </div>
                    </div>

                    <%}%>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-12 col-form-label mt-2 text-color">
                                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_NECESSARY_DOCUMENTS, loginDTO)%>
                            </label>
                        </div>
                    </div>
                    <div class="col-12">

                        <%
                            String fileColumnName = "filesDropzone";
                            int i = 0;
                            FilesDAO filesDAO = new FilesDAO();
                            if (true) {
                                List<AmOfficeAssignmentFilesDTO> amOfficeAssignmentFilesDTOS = AmOfficeAssignmentFilesDAO.getInstance().getByAmOfficeAssignmentId(am_office_assignmentDTO.iD);

                                for (AmOfficeAssignmentFilesDTO amOfficeAssignmentFilesDTO : amOfficeAssignmentFilesDTOS) {


                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(amOfficeAssignmentFilesDTO.filesDropzone);
                        %>
                        <table>

                            <%
                                if (fileList != null) {
                                    for (int j = 0; j < fileList.size(); j++) {
                                        FilesDTO filesDTO = fileList.get(j);
                                        byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                            %>
                            <tr>

                                <td>

                                    <%=amOfficeAssignmentFilesDTO.title%>

                                </td>

                                <td id='<%=fileColumnName%>_td_<%=filesDTO.iD%>'>
                                    <a href='<%=servletName%>?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                       download><%=filesDTO.fileTitle%>
                                    </a>
                                    <%
                                        if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                    %>
                                    <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)
								%>' style='width:100px'/>
                                    <%
                                        }
                                    %>


                                </td>
                            </tr>
                            <%
                                        }
                                    }

                                }
                            %>

                        </table>
                        <%
                            }
                        %>
                    </div>


                </div>

            </form>

        </div>
    </div>

    <div class="form-actions text-right mt-4">

        <button class="btn btn-sm btn-primary shadow btn-border-radius" onclick="history.back()">
            <%=LM.getText(LC.GO_BACK_BACK, loginDTO)%>
        </button>

        <button type="button" class="btn" id='download-pdf'
                onclick="downloadTemplateAsPdf('to-print-div', '<%=am_office_assignment_requestDTO.requesterNameEn%>');">
            <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
        </button>

    </div>

</div>


<script type="text/javascript">

    $(document).ready(function () {
        if ($(document).prop('title') === 'View') {
            $(document).prop('title', '<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_HISTORY_REPORT_SELECT_ASSIGNMENTDETAILSEN, loginDTO)%>');
        }

    });

    function downloadTemplateAsPdf(divId, fileName) {
        let content = document.getElementById(divId);
        const opt = {
            margin: 0.5,
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4'}
        };
        html2pdf().from(content).set(opt).save();
    }


</script>

<style>
    .required {
        color: red;
    }
</style>