<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    String url = "Am_office_assignmentServlet?actionType=search";
    Employee_recordsDTO employeeRecordsDTO = null;
    String employeeRecordIdStr = request.getParameter("employeeRecordsId");
    if (employeeRecordIdStr != null && employeeRecordIdStr.length() > 0) {
        employeeRecordsDTO = Employee_recordsRepository.getInstance()
                .getById(Long.parseLong(employeeRecordIdStr));
    }

    String toggle = request.getParameter("toggle");
    boolean isExpanded = toggle != null && toggle.length() > 0 && toggle.equalsIgnoreCase("true");

%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0,false)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>

    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">

            <div class="row">

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=LM.getText(LC.FUND_MANAGEMENT_ADD_EMPLOYEERECORDSID, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                    id="employeeRecordId_modal_button"
                                    onclick="employeeRecordIdModalBtnClicked();">
                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                            </button>
                            <div class="input-group" id="employeeRecordId_div" style="display: none">
                                <input type="hidden" name='employeeRecordId' id='employeeRecordId_input' value=""
                                       onchange='setSearchChanged()'>
                                <button type="button" class="btn btn-secondary form-control" disabled
                                        id="employeeRecordId_text"></button>
                                <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                                    <button type="button" class="btn btn-outline-danger"
                                            onclick="crsBtnClicked('employeeRecordId');"
                                            id='employeeRecordId_crs_btn' tag='pb_html'>
                                        x
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.FUND_APPROVAL_MAPPING_ADD_FUNDAPPLICATIONSTATUSCAT, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='fundApplicationStatusCat'
                                    id='fund_application_status_cat' onSelect='setSearchChanged()'>
                                <%=CatRepository.getInstance().buildOptions("am_office_assignment_request_status", Language, -1)%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_BUILDINGTYPECAT, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='building_type_cat' id='building_type_cat'
                                    onSelect='setSearchChanged()'>
                                <%
                                    Options = CatRepository.getInstance().buildOptions("building_type", Language, -1);
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_ASSIGNMENTDATE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="assignment_date_start_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type="hidden" id="assignment_date_start" name="assignment_date_start">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0,true)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>


<template id="loader">
    <div class="modal-body">
        <div class="search-loader-container-circle ">
            <div class="search-loader-circle"></div>
        </div>
    </div>
</template>

<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>
<script type="text/javascript">

    const requesterSelector = $('#employeeRecordId_input');
    const statusSelector = $('#fund_application_status_cat');
    const buildingTypeSelector = $('#building_type_cat');
    const assignmentDateSelector = $('#assignment_date_start');
    const anyFieldSelector = $('#anyfield');
    const servletName = "Am_office_assignmentServlet";
    let employeeModel;

    $(document).ready(() => {

        readyInit(servletName);

        <%if(employeeRecordsDTO != null) {%>
        employeeModel = JSON.parse('<%=EmployeeSearchModalUtil.getEmployeeDefaultSearchModelJson(employeeRecordsDTO.iD)%>')
        employeeRecordIdInInput(employeeModel);
        <%}%>

        <%if(isExpanded){%>
        document.querySelector('.kt-portlet__body').style.display = 'block';
        document.querySelector('#kt_portlet_tools_1').classList.remove('kt-portlet--collapse');
        <%}%>

    });

    /*Employee selector modal started*/

    function employeeRecordIdInInput(empInfo) {
        employeeModel = empInfo;
        $('#employeeRecordId_modal_button').hide();
        $('#employeeRecordId_div').show();

        let language = '<%=Language.toLowerCase()%>'
        let designation;
        if (language === 'english') {
            designation = empInfo.employeeNameEn + ' (' + empInfo.organogramNameEn + ', ' + empInfo.officeUnitNameEn + ')';
        } else {
            designation = empInfo.employeeNameBn + ' (' + empInfo.organogramNameBn + ', ' + empInfo.officeUnitNameBn + ')';
        }
        document.getElementById('employeeRecordId_text').innerHTML = designation;
        $('#employeeRecordId_input').val(empInfo.employeeRecordId);
    }

    table_name_to_collcetion_map = new Map([
        ['employeeRecordId', {
            isSingleEntry: true,
            callBackFunction: employeeRecordIdInInput
        }]
    ]);

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    function employeeRecordIdModalBtnClicked() {
        modal_button_dest_table = 'employeeRecordId';
        $('#search_emp_modal').modal();
    }

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
    }

    /*Employee selector modal end*/

    function resetInputs() {
        crsBtnClicked('employeeRecordId');
        $('select').prop('selectedIndex', 0);
        anyFieldSelector.val('');
        resetDateById('assignment_date_start_js');
    }


    window.addEventListener('popstate', e => {
        if (e.state) {

            let params = e.state.params;
            employeeModel = e.state.employeeModel;
            if (employeeModel) {
                employeeRecordIdInInput(employeeModel);
            }
            dosubmit(params, false);
            //resetInputs();
            let arr = params.split('&');
            arr.forEach(e => {
                let item = e.split('=');
                if (item.length === 2) {
                    switch (item[0]) {

                        case 'employeeRecordsId':
                            if (item[1].length == 0) {
                                crsBtnClicked('employeeRecordId');
                            }
                            break;
                        case 'amOfficeAssignmentStatusCat':
                            $('#fund_application_status_cat option[value="' + item[1] + '"]').prop('selected', 'selected').change();
                            break;
                        case 'building_type_cat':
                            $('#building_type_cat').prop('selectedIndex', item[1]);
                            break;
                        case 'assignment_date':
                            setDateByTimestampAndId('assignment_date_start_js', item[1]);
                            break;
                        case 'AnyField':
                            anyFieldSelector.val(item[1]);
                            break;
                        case 'toggle':
                            if (item[1] === 'true') {
                                document.querySelector('.kt-portlet__body').style.display = 'block';
                                document.querySelector('#kt_portlet_tools_1').classList.remove('kt-portlet--collapse');
                            } else {
                                document.querySelector('.kt-portlet__body').style.display = 'none';
                                document.querySelector('#kt_portlet_tools_1').classList.add('kt-portlet--collapse');
                            }


                            break;


                        default:
                            setPaginationFields(item);
                    }
                }
            });
        } else {
            dosubmit(null, false);
            resetInputs();
            resetPaginationFields();
        }
    });


    function dosubmit(params, pushState = true) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (pushState) {
                    let stateParam = {
                        'params': params,
                        'employeeModel': employeeModel
                    }
                    history.pushState(stateParam, '', servletName + '?actionType=search&' + params);
                }
                setTimeout(() => {
                    document.getElementById('tableForm').innerHTML = this.responseText;
                    setPageNo();
                    searchChanged = 0;
                }, 200);
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        let url = "<%=action%>&ajax=true&isPermanentTable=true";
        if (params) {
            url += "&" + params;
        }
        xhttp.open("Get", url, false);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number, pushState) {
        let params = 'search=true';
        let toggleFlag = 0;

        if (anyFieldSelector.val()) {
            params += '&AnyField=' + anyFieldSelector.val();
        }

        if (buildingTypeSelector.val()) {
            params += '&building_type_cat=' + $("#building_type_cat").val();
            toggleFlag = 1;
        }

        if (statusSelector.val()) {
            params += '&amOfficeAssignmentStatusCat=' + $("#fund_application_status_cat").val();
            toggleFlag = 1;
        }
        if (requesterSelector.val()) {
            params += '&employeeRecordsId=' + $('#employeeRecordId_input').val();
            toggleFlag = 1;
        }
        let assignmentDate = $("#assignment_date_start").val(getDateStringById('assignment_date_start_js', 'DD/MM/YYYY'));
        if (assignmentDate.val()) {
            // let localDate = getBDFormattedDate('assignment_date_start');
            params += '&assignment_date=' + getDateTimestampById('assignment_date_start_js');
            toggleFlag = 1;
        }


        //params += '';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        let toggleVal = toggleFlag === 1 ? true : false;
        params += '&toggle=' + toggleVal;
        dosubmit(params, pushState);

    }

</script>

