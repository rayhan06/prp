<%@ page import="am_parliament_building_level.Am_parliament_building_levelRepository" %>

<div class="row">
    <div class="col-6">
        <div class="form-group row d-flex align-items-center">
            <label class="col-4 col-form-label "><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_LEVEL, loginDTO)%><span class="required" style="color: red"> * </span></label>
            <div class="col-8">
    <%--            <input type='text' class='form-control'  name='level' id = 'level_text_<%=i%>' value='<%=am_office_assignmentDTO.level%>'   tag='pb_html'/>--%>
                <select  class='form-control' onchange="parliamentBuildingLevelTypeChanged(this)"  name='parliamentBuildingLevel' id = 'parliamentBuildingLevel'   tag='pb_html'>
                    <%

                        String levelOptions = Am_parliament_building_levelRepository.getInstance().getOptions(Language, -1);
                    %>
                    <%=levelOptions%>
                </select>
            </div>
        </div>

    </div>


    <div class="col-6">
        <div class="form-group row d-flex align-items-center">
            <label class="col-4 col-form-label "><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_BLOCK, loginDTO)%><span class="required" style="color: red"> * </span></label>
            <div class="col-8">
    <%--            <input type='text' class='form-control'  name='block' id = 'block_text_<%=i%>' value='<%=am_office_assignmentDTO.block%>'   tag='pb_html'/>--%>
                <select id="parliamentBuildingBlock"
                        class='form-control rounded shadow-sm w-100'   name="parliamentBuildingBlock"
                        onchange="parliamentBuildingBlockChanged(this)"
                >
                </select>
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row d-flex align-items-center">
            <label class="col-4 col-form-label "><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_ROOMNO, loginDTO)%><span class="required" style="color: red"> * </span></label>
            <div class="col-8">
    <%--            <input type='text' class='form-control'  name='roomNo' id = 'roomNo_text_<%=i%>' value='<%=am_office_assignmentDTO.roomNo%>'   tag='pb_html'/>--%>

                    <select id="parliamentBuildingRoomNo"
                            class='form-control rounded shadow-sm w-100'   name="roomNo"
                    >
                    </select>
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row d-flex align-items-center">
            <label class="col-4 col-form-label "><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_REMARKS, loginDTO)%></label>
            <div class="col-8">
                <input type='text' class='form-control'  name='remarks' id = 'remarks_text_<%=i%>' value='<%=am_office_assignmentDTO.remarks%>'   tag='pb_html'/>
            </div>
        </div>
    </div>

</div>