<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="am_office_assignment.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="dbm.DBMW" %>
<%@ page import="com.mysql.cj.xdevapi.Column" %>
<%@ page import="am_office_assignment_request.Am_office_assignment_requestDTO" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%


    Am_office_assignmentDTO am_office_assignmentDTO;
    am_office_assignmentDTO = (Am_office_assignmentDTO) request.getAttribute("am_office_assignmentDTO");
    CommonDTO commonDTO = am_office_assignmentDTO;
    if (am_office_assignmentDTO == null) {
        am_office_assignmentDTO = new Am_office_assignmentDTO();

    }

    Am_office_assignment_requestDTO am_office_assignment_requestDTO = (Am_office_assignment_requestDTO) request.getAttribute("am_office_assignment_requestDTO");

    String tableName = "am_office_assignment";

%>
<%@include file="../pb/addInitializer.jsp" %>
<%
    String formTitle = LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_AM_OFFICE_ASSIGNMENT_ADD_FORMNAME, loginDTO);
    String servletName = "Am_office_assignmentServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <%--        <form class="form-horizontal"--%>
        <%--              action="Am_office_assignmentServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"--%>
        <%--              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"--%>
        <%--              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">--%>

        <form class="form-horizontal" id="bigform" name="bigform" method="POST" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-10 offset-1">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>


                                        <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                               value='<%=am_office_assignmentDTO.iD%>' tag='pb_html'/>


                                        <input type='hidden' class='form-control' name='amOfficeAssignmentRequestId'
                                               id='amOfficeAssignmentRequestId_hidden_<%=i%>'
                                               value='<%=am_office_assignment_requestDTO.iD%>' tag='pb_html'/>


                                        <div class="d-flex justify-content-center row">
                                            <label class="col-4 text-right offset-1 font-weight-bold">
                                                <%=UtilCharacter.getDataByLanguage(Language,
                                                        "অনুরোধকারীর নাম", "Requester Name")%>
                                            </label>
                                            <span class="col-6 ">
                                                <%=UtilCharacter.getDataByLanguage(Language,
                                                        am_office_assignment_requestDTO.requesterNameBn, am_office_assignment_requestDTO.requesterNameEn)%>

                                            </span>
                                        </div>
                                        <div class="d-flex justify-content-center row">
                                            <label class="col-4 text-right offset-1 font-weight-bold">
                                                <%=LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO)%>
                                            </label>
                                            <span class="col-6 ">
                                                <%=UtilCharacter.getDataByLanguage(Language,
                                                        am_office_assignment_requestDTO.requesterOfficeUnitOrgNameBn,
                                                        am_office_assignment_requestDTO.requesterOfficeUnitOrgNameEn)%>

                                            </span>
                                        </div>
                                        <div class="d-flex justify-content-center row">
                                            <label class="col-4 text-right offset-1 font-weight-bold">
                                                <%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_OFFICE, loginDTO)%>
                                            </label>
                                            <span class="col-6 ">
                                                <%=UtilCharacter.getDataByLanguage(Language,
                                                        am_office_assignment_requestDTO.requesterOfficeUnitNameBn,
                                                        am_office_assignment_requestDTO.requesterOfficeUnitNameEn)%>
                                            </span>
                                        </div>



                                        <div class="row p-5">

                                            <div class="col-12">
                                                <div class="form-group row d-flex align-items-center">
                                                    <label class="col-4 col-form-label">
                                                        <%=UtilCharacter.getDataByLanguage(Language, "অফিসের অবস্থান বাছাই করুন",
                                                                "Select office location")%>
                                                        <span class="required" style="color: red"> * </span>
                                                    </label>

                                                    <div class="col-4">
                                                        <input type="radio" id="parliamentOffice" name="officeType"
                                                               value="1">
                                                        <label for="parliamentOffice"><%=UtilCharacter.getDataByLanguage(Language, "সংসদ ভবন",
                                                                "Parliament Building")%>
                                                        </label><br>
                                                    </div>
                                                    <div class="col-4">
                                                        <input type="radio" id="otherOffice" name="officeType"
                                                               value="2">
                                                        <label for="otherOffice"><%=LM.getText(LC.LANGUAGE_OTHERS, loginDTO)%>
                                                        </label><br>
                                                    </div>


                                                </div>
                                            </div>

                                            <div class="col-12" style="display: none;" id="selectParliamentOffice">
                                                <div class="form-group row d-flex align-items-center">
                                                    <label class="col-2 col-form-label">
                                                        <%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_ADD_OFFICEID, loginDTO)%>
                                                        <span class="required" style="color: red;"> * </span>
                                                    </label>

                                                    <div class="col-8">
                                                        <input type="hidden" name='officeUnitId'
                                                               id='office_units_id_input' value="">
                                                        <button type="button"
                                                                class="btn btn-secondary form-control shadow btn-border-radius"
                                                                disabled id="office_units_id_text"></button>
                                                    </div>
                                                    <div class="col-2">
                                                        <button type="button"
                                                                class="btn btn-primary btn-block shadow btn-border-radius"
                                                                id="office_units_id_modal_button"
                                                                onclick="officeModalButtonClicked();">
                                                            <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                                                        </button>
                                                    </div>


                                                </div>
                                            </div>

                                            <div class="col-12" style="display: none;" id="selectOtherOffice">
                                                <div class="form-group row d-flex align-items-center">
                                                    <label class="col-2 col-form-label">
                                                        <%=LM.getText(LC.LANGUAGE_OTHERS, loginDTO)%>
                                                        <span class="required" style="color: red;"> * </span>
                                                    </label>
                                                    <div class="col-8">
                                                        <select class='form-control' name='select2OtherOffice'
                                                                id='select2OtherOffice' tag='pb_html'>
                                                        </select>
                                                    </div>


                                                </div>
                                            </div>


                                            <div class="col-6">
                                                <div class="form-group row d-flex align-items-center">

                                                    <label class="col-4 col-form-label"><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_BUILDINGTYPECAT, loginDTO)%><span
                                                            class="required" style="color: red;"> * </span></label>

                                                    <div class="col-8">
                                                        <select class='form-control'
                                                                onchange="buildingTypeChanged(this)"
                                                                name='buildingTypeCat'
                                                                id='buildingTypeCat_category_<%=i%>' tag='pb_html'>
                                                            <%
                                                                Options = CatRepository.getInstance().buildOptions("building_type", Language, am_office_assignmentDTO.buildingTypeCat);
                                                            %>
                                                            <%=Options%>
                                                        </select>

                                                    </div>

                                                </div>

                                            </div>
                                            <div class="col-6">
                                                <div class="form-group row d-flex align-items-center">
                                                    <label class="col-4 col-form-label "><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_ASSIGNMENTDATE, loginDTO)%><span
                                                            class="required" style="color: red;"> * </span></label>

                                                    <div class="col-8">
                                                        <%value = "assignmentDate_js_" + i;%>
                                                        <jsp:include page="/date/date.jsp">
                                                            <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                            <jsp:param name="LANGUAGE"
                                                                       value="<%=Language%>"></jsp:param>
                                                        </jsp:include>
                                                        <input type='hidden' name='assignmentDate'
                                                               id='assignmentDate_date_<%=i%>'
                                                               value='<%=dateFormat.format(new Date(am_office_assignmentDTO.assignmentDate))%>'
                                                               tag='pb_html'>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-12" style="display: none;" id="ParliamentBuildingContent">
                                                <div class="w-100">
                                                    <%@ include file="ParliamentBuildingContent.jsp" %>
                                                </div>
                                            </div>
                                            <div class="col-12" style="display: none;" id="MinisterHostelContent">
                                                <div class="w-100">
                                                    <%@ include file="MinisterHostelContent.jsp" %>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_AM_OFFICE_ASSIGNMENT_FILES, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_AM_OFFICE_ASSIGNMENT_FILES_TITLE, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_AM_OFFICE_ASSIGNMENT_FILES_FILESDROPZONE, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_AM_OFFICE_ASSIGNMENT_FILES_REMOVE, loginDTO)%>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="field-AmOfficeAssignmentFiles">


                                <%
                                    if (actionName.equals("edit")) {
                                        int index = -1;


                                        for (AmOfficeAssignmentFilesDTO amOfficeAssignmentFilesDTO : am_office_assignmentDTO.amOfficeAssignmentFilesDTOList) {
                                            index++;

                                            System.out.println("index index = " + index);

                                %>

                                <tr id="AmOfficeAssignmentFiles_<%=index + 1%>">
                                    <td style="display: none;">

                                        <input type='hidden' class='form-control' name='amOfficeAssignmentFiles.iD'
                                               id='iD_hidden_<%=childTableStartingID%>'
                                               value='<%=amOfficeAssignmentFilesDTO.iD%>' tag='pb_html'/>

                                    </td>
                                    <td>


                                        <input type='text' class='form-control' name='amOfficeAssignmentFiles.title'
                                               id='title_text_<%=childTableStartingID%>'
                                               value='<%=amOfficeAssignmentFilesDTO.title%>' tag='pb_html'/>
                                    </td>
                                    <td>


                                        <%
                                            fileColumnName = "filesDropzone";
                                            if (actionName.equals("edit")) {
                                                List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(amOfficeAssignmentFilesDTO.filesDropzone);
                                        %>
                                        <%@include file="../pb/dropzoneEditor.jsp" %>
                                        <%
                                            } else {
                                                ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                amOfficeAssignmentFilesDTO.filesDropzone = ColumnID;
                                            }
                                        %>

                                        <div class="dropzone"
                                             action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=amOfficeAssignmentFilesDTO.filesDropzone%>">
                                            <input type='file' style="display:none" name='<%=fileColumnName%>File'
                                                   id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                        </div>
                                        <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                               id='<%=fileColumnName%>FilesToDelete_<%=i%>' value='' tag='pb_html'/>
                                        <input type='hidden' name='<%=fileColumnName%>'
                                               id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                               value='<%=amOfficeAssignmentFilesDTO.filesDropzone%>'/>


                                    </td>


                                    <td>
										<span id='chkEdit'>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
                                                   class="form-control-sm"/>
										</span>
                                    </td>
                                </tr>
                                <%
                                            childTableStartingID++;
                                        }
                                    }
                                %>

                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-9 text-right">
                                <button
                                        id="add-more-AmOfficeAssignmentFiles"
                                        name="add-moreAmOfficeAssignmentFiles"
                                        type="button"
                                        class="btn btn-sm text-white add-btn shadow">
                                    <i class="fa fa-plus"></i>
                                    <%=LM.getText(LC.HM_ADD, loginDTO)%>
                                </button>
                                <button
                                        id="remove-AmOfficeAssignmentFiles"
                                        name="removeAmOfficeAssignmentFiles"
                                        type="button"
                                        class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        </div>

                        <%AmOfficeAssignmentFilesDTO amOfficeAssignmentFilesDTO = new AmOfficeAssignmentFilesDTO();%>

                        <template id="template-AmOfficeAssignmentFiles">
                            <tr>
                                <td style="display: none;">

                                    <input type='hidden' class='form-control' name='amOfficeAssignmentFiles.iD'
                                           id='iD_hidden_' value='<%=amOfficeAssignmentFilesDTO.iD%>' tag='pb_html'/>


                                </td>
                                <td>

                                    <input type='text' class='form-control' name='amOfficeAssignmentFiles.title'
                                           id='title_text_' value='<%=amOfficeAssignmentFilesDTO.title%>'
                                           tag='pb_html'/>
                                </td>

                                <td style="display: none;">


                                    <%
                                        fileColumnName = "filesDropzone";

//															ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
//															amOfficeAssignmentFilesDTO.filesDropzone = ColumnID;

                                    %>
                                    <input type='hidden' name='amOfficeAssignmentFiles.<%=fileColumnName%>'
                                           id='<%=fileColumnName%>_dropzone_' tag='pb_html'/>

                                    <%--														<div class="dropzone" action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=amOfficeAssignmentFilesDTO.filesDropzone%>">--%>
                                    <%--															<input type='file' style="display:none"  name='<%=fileColumnName%>File' id = '<%=fileColumnName%>_dropzone_File_'  tag='pb_html'/>--%>
                                    <%--														</div>--%>
                                    <%--														<input type='hidden'  name='<%=fileColumnName%>FilesToDelete' id = '<%=fileColumnName%>FilesToDelete_' value=''  tag='pb_html'/>--%>
                                    <%--														<input type='hidden' name='<%=fileColumnName%>' id = '<%=fileColumnName%>_dropzone_'  tag='pb_html' value='<%=amOfficeAssignmentFilesDTO.filesDropzone%>'/>--%>


                                </td>


                                <td class="groups-section" tag='pb_html' id='<%=fileColumnName%>_dropzoneDiv_'>


                                </td>


                                <td>
											<span id='chkEdit2'>
												<input type='checkbox' name='checkbox' value='' deletecb='true'
                                                       class="form-control-sm"/>
											</span>
                                </td>
                            </tr>

                        </template>
                    </div>
                </div>
                <div class="form-actions text-right mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_AM_OFFICE_ASSIGNMENT_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button id="approve_btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                            onclick="approveApplication()">
                        <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_AM_OFFICE_ASSIGNMENT_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>


<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">


    var row = 0;
    const amOfficeAssignmentForm = $("#bigform");
    let otherOffices;
    let language = "<%=Language%>";
    const isLangEng = '<%=Language%>'.toLowerCase() == 'english';
    $(document).ready(function () {
        init(row);
        select2SingleSelector("#select2OtherOffice", '<%=Language%>');
        fetchOtherOffices(otherOffices);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        $("#ParliamentBuildingContent").hide();
        $("#MinisterHostelContent").hide();

        amOfficeAllocationDocumentReady();


    });

    function validate() {
        let formSelector = $("#bigform");
        var validate = formSelector.validate();
        var valid = formSelector.valid();

        if (!valid) {
            formSelector.find(":input.error:first").focus();
        }

        return valid;

    }

    function amOfficeAllocationDocumentReady() {


        $.validator.addMethod('officeTypeSelection', function (value, element) {

            return value != 0;
        });

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {

                officeType: {
                    required: true,
                },
                buildingTypeCat: {
                    required: true,
                },

                parliamentBuildingLevel: {
                    required: function () {

                        if ($('#ParliamentBuildingContent').is(":visible")) {
                            return true;
                        }
                        return false;
                    }
                },
                parliamentBuildingBlock: {
                    required: function () {

                        if ($('#ParliamentBuildingContent').is(":visible")) {
                            return true;
                        }
                        return false;
                    }
                },

                roomNo: {
                    required: function () {

                        if ($('#ParliamentBuildingContent').is(":visible")) {
                            return true;
                        }
                        return false;
                    }
                },

                mpHostelBlock: {
                    required: function () {

                        if ($('#MinisterHostelContent').is(":visible")) {
                            return true;
                        }
                        return false;
                    }
                },
                side: {
                    required: function () {

                        if ($('#MinisterHostelContent').is(":visible")) {
                            return true;
                        }
                        return false;
                    }
                },
                unit: {
                    required: function () {

                        if ($('#MinisterHostelContent').is(":visible")) {
                            return true;
                        }
                        return false;
                    }
                },
                mpHostelLevel: {
                    required: function () {

                        if ($('#MinisterHostelContent').is(":visible")) {
                            return true;
                        }
                        return false;
                    }
                },

            },

            messages: {
                officeType: "<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_PLEASE_SELECT_OFFICE_TYPE, loginDTO)%>",
                buildingTypeCat: "<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_PLEASE_SELECT_BUILDING_TYPE, loginDTO)%>",
                parliamentBuildingLevel: "<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_PLEASE_SELECT_LEVEL_NUMBER, loginDTO)%>",
                parliamentBuildingBlock: "<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_PLEASE_SELECT_BLOCK, loginDTO)%>",
                roomNo: "<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_PLEASE_SELECT_ROOM, loginDTO)%>",
                mpHostelBlock: "<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_PLEASE_SELECT_BLOCK, loginDTO)%>",
                side: "<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_PLEASE_SELECT_SIDE, loginDTO)%>",
                unit: "<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_PLEASE_SELECT_UNIT, loginDTO)%>",
                mpHostelLevel: "<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_PLEASE_SELECT_LEVEL_NUMBER, loginDTO)%>",

            }
        });
    }

    function fetchOtherOffices(otherOffices) {

        let url = "Am_other_office_unitServlet?actionType=getAllOtherOffices";
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {

                $('#select2OtherOffice').html("");
                let o;
                let str;
                if (language === 'English') {
                    str = 'Select';
                    o = new Option('Select', '-1');
                } else {
                    o = new Option('বাছাই করুন', '-1');
                    str = 'বাছাই করুন';
                }
                $(o).html(str);
                $('#select2OtherOffice').append(o);

                const response = JSON.parse(fetchedData);

                if (response && response.length > 0) {
                    for (let x in response) {

                        if (language === 'English') {
                            str = response[x].englishText;
                        } else {
                            str = response[x].banglaText;
                        }

                        if (otherOffices && otherOffices.length > 0) {
                            if (otherOffices.includes(response[x].value + '')) {
                                o = new Option(str, response[x].value, false, true);
                            } else {
                                o = new Option(str, response[x].value);
                            }
                        } else {
                            o = new Option(str, response[x].value);
                        }
                        $(o).html(str);
                        $('#select2OtherOffice').append(o);
                    }
                }


            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    $('input[type=radio][name=officeType]').change(function () {
        if (this.value == '1') {
            $("#selectParliamentOffice").show();
            $("#selectOtherOffice").hide();
        } else if (this.value == '2') {
            $("#selectOtherOffice").show();
            $("#selectParliamentOffice").hide();
        }
    });

    function updateApprovalMappingTable() {

        const url = 'Am_office_approval_mappingServlet?actionType=approveApplication&amOfficeAssignmentRequestId=<%=am_office_assignment_requestDTO.iD%>';
        $.ajax({
            url: url,
            type: "POST",
            async: false,
            success: function (fetchedData) {
                const response = JSON.parse(fetchedData);
                if (response && response.responseCode === 0) {

                    showToastSticky(response.msg, response.msg);
                } else if (response && response.responseCode === 200) {
                    //submitForm();
                    window.location.replace(getContextPath() + response.msg);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
            }
        });
    }

    function approveApplication() {

        if (validate()) {
            let msg = '<%=getDataByLanguage(Language, "আপনি কি আবেদন অনমোদনের ব্যাপারে নিশ্চিত?", "Are you sure to approve application?")%>';
            let confirmButtonText = '<%=StringUtils.getYesNo(Language, true)%>';
            let cancelButtonText = '<%=StringUtils.getYesNo(Language, false)%>';
            messageDialog('', msg, 'success', true, confirmButtonText, cancelButtonText, () => {
                $('#approve_btn').prop("disabled", true);
                //updateApprovalMappingTable();
                submitForm();
            }, () => {
            });
        }


    }

    function submitForm() {
        buttonStateChange(true);
        if (PreprocessBeforeSubmiting(0)) {
            $.ajax({
                type: "POST",
                url: "Am_office_assignmentServlet?actionType=ajax_<%=actionName%>",
                data: amOfficeAssignmentForm.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        //window.location.replace(getContextPath()+response.msg);
                        updateApprovalMappingTable();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        } else {
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    const buildingTypeChanged = (selectedVal) => {
        //clearSelects(0,0);
        //clearSelects(1,0);
        switch (parseInt(selectedVal.value)) {
            case 1:
                $("#ParliamentBuildingContent").show();
                $("#MinisterHostelContent").hide();
                break;
            case 2:
                $("#ParliamentBuildingContent").hide();
                $("#MinisterHostelContent").show();
                break;
            default:
                $("#ParliamentBuildingContent").hide();
                $("#MinisterHostelContent").hide();
        }
    }

    const parliamentBuildingLevelTypeChanged = async (selectedVal) => {

        const levelId = selectedVal.value;
        clearSelects(0, 1);
        if (levelId === '') return;

        const url = 'Am_parliament_building_blockServlet?actionType=getBlockByLevelId&levelId='
            + levelId + '&defaultValue=-1';

        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                const response = JSON.parse(fetchedData);
                if (response && response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                } else if (response && response.responseCode === 200) {
                    document.getElementById('parliamentBuildingBlock').innerHTML = response.msg;
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
            }
        });

    }

    const parliamentBuildingBlockChanged = async (selectedVal) => {

        const block = selectedVal.value;
        clearSelects(0, 2);
        if (block === '') return;

        const url = 'Am_parliament_building_roomServlet?actionType=getRoomByBlockId&blockId='
            + block + '&defaultValue=-1';

        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                const response = JSON.parse(fetchedData);
                document.getElementById('parliamentBuildingRoomNo').innerHTML = response.msg;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
            }
        });
        //const response = await fetch(url);
        //document.getElementById('parliamentBuildingRoomNo').innerHTML = await response.text();

    }

    const mpHostelBlockTypeChanged = async (selectedVal) => {

        const blockType = selectedVal.value;
        clearSelects(1, 1);
        if (blockType === '') return;

        const url = 'Am_minister_hostel_blockServlet?actionType=getSideOptionsByBlockId&blockId='
            + blockType;
        // const response = await fetch(url);
        // document.getElementById('mpHostelSide').innerHTML = await response.text();
        ajaxGet(url, setD, setD)

    }

    function setD(data) {
        document.getElementById('mpHostelSide').innerHTML = data;
    }

    function ajaxGet(url, onSuccess, onError) {
        $.ajax({
            type: "GET",
            url: getContextPath() + url,
            dataType: "json",
            success: onSuccess,
            error: onError,
            complete: function () {
                // $.unblockUI();
            }
        });
    }

    function setMpHostelUnit(data) {
        document.getElementById('mpHostelUnit').innerHTML = data;
    }

    function setMpHostelLevel(data) {
        document.getElementById('mpHostelLevel').innerHTML = data.responseText;
    }


    const mpHostelSideChanged = async (selectedVal) => {

        const side = selectedVal.value;
        clearSelects(1, 2);
        if (side === '') return;

        const url = 'Am_minister_hostel_unitServlet?actionType=getUnitOptionsBySideId&sideId='
            + side;
        ajaxGet(url, setMpHostelUnit, setMpHostelUnit);
        //const response = await fetch(url);
        //document.getElementById('mpHostelUnit').innerHTML = await response.text();

    }

    const mpHostelUnitChanged = async (selectedVal) => {

        const unit = selectedVal.value;
        clearSelects(1, 3);
        if (unit === '') return;

        const url = 'Am_minister_hostel_levelServlet?actionType=getLevelOptionsByUnitId&unitId='
            + unit;
        ajaxGet(url, setMpHostelLevel, setMpHostelLevel);
        //const response = await fetch(url);
        //document.getElementById('mpHostelLevel').innerHTML = await response.text();

    }

    function clearSelects(arrayIndex, startIndex) {
        const selectIds = [['parliamentBuildingLevel', 'parliamentBuildingBlock', 'parliamentBuildingRoomNo'], ['mpHostelBlock', 'mpHostelSide', 'mpHostelUnit', 'mpHostelLevel']];

        selectIds[arrayIndex].forEach((item) => {
            if (selectIds[arrayIndex].indexOf(item) >= startIndex) {
                $('#' + item).html('');
            }
        });

    }


    function PreprocessBeforeSubmiting(row, validate) {
        preprocessDateBeforeSubmitting('assignmentDate', row);

        for (i = 1; i < child_table_extra_id; i++) {
        }
        amOfficeAssignmentForm.validate();
        return amOfficeAssignmentForm.valid();
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Am_office_assignmentServlet");
    }

    function init(row) {

        setDateByStringAndId('assignmentDate_js_' + row, $('#assignmentDate_date_' + row).val());

        for (i = 1; i < child_table_extra_id; i++) {
        }

    }


    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-AmOfficeAssignmentFiles").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-AmOfficeAssignmentFiles");

            $("#field-AmOfficeAssignmentFiles").append(t.html());


            SetCheckBoxValues("field-AmOfficeAssignmentFiles");

            var tr = $("#field-AmOfficeAssignmentFiles").find("tr:last-child");

            tr.attr("id", "AmOfficeAssignmentFiles_" + child_table_extra_id);

            let jsColumnID = '';

            const url = 'Am_office_assignmentServlet?actionType=getFileNextSequenceId';

            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    const response = JSON.parse(fetchedData);
                    jsColumnID = response.msg;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                }
            });

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                console.log(index + ": " + $(this).attr('id'));
                let childDivId = 'childDrozone_' + prev_id + child_table_extra_id;

                if ($(this).attr('id').includes('filesDropzone_dropzoneDiv_')) {
                    var HtmlCodeOfSection = ' <div class="dropzone" id="' + childDivId + '" </div>';

                    $("#" + $(this).attr('id')).append(HtmlCodeOfSection);
                    Dropzone.autoDiscover = false;
                    var myDropzone = new Dropzone("div#" + childDivId, {
                        url: "Am_office_assignmentServlet?pageType=add&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=" + jsColumnID
                    });
                }
                if ($(this).attr('id').includes('filesDropzone_dropzone_')) {
                    $("#" + $(this).attr('id')).val(jsColumnID);
                    console.log(jsColumnID);
                }
            });

            let dropdownMsg = isLangEng ? "Drop files here to upload" : "ফাইল আপলোড করার জন্য এখানে ফাইল ড্রপ করুন";
            const dropzoneMsgArr = document.querySelectorAll('.dz-default.dz-message');
            dropzoneMsgArr.forEach((item) => {
                item.innerHTML = "<span>" + dropdownMsg + "</span>";
            });
            child_table_extra_id++;
        });


    $("#remove-AmOfficeAssignmentFiles").click(function (e) {
        var tablename = 'field-AmOfficeAssignmentFiles';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length 1= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[deletecb="true"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
                console.log("tr.childNodes.length 2= " + tr.childNodes.length);

            }

        }
    });


    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        // console.log(selectedOffice);
        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id_input').val(selectedOffice.id);
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    function officeModalButtonClicked() {
        // console.log('Button Clicked!');
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }


</script>






