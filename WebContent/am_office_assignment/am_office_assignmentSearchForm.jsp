
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="am_office_assignment.*"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@ page import="java.util.List" %>
<%@ page import="am_office_assignment_request.Am_office_assignment_requestDTO" %>
<%@ page import="am_office_assignment_request.Am_office_assignment_requestDAO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="am_other_office_unit.Am_other_office_unitRepository" %>
<%@ page import="am_other_office_unit.Am_other_office_unitDTO" %>


<%
String navigator2 = "navAM_OFFICE_ASSIGNMENT";
String servletName = "Am_office_assignmentServlet";

%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped text-nowrap">
						<thead>
							<tr>
								<th><%=LM.getText(LC.VM_FUEL_REQUEST_REPORT_WHERE_REQUESTEREMPID, loginDTO)%></th>
								<th><%=LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO)%></th>
								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_OFFICE, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_ASSIGNMENTDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLEOFFICENAME, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_BUILDINGTYPECAT, loginDTO)%></th>
								<th><%=LM.getText(LC.FUND_APPROVAL_MAPPING_ADD_FUNDAPPLICATIONSTATUSCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>

								
								
							</tr>
						</thead>
						<tbody>

							<%
								List<Am_office_assignmentDTO> data = (List<Am_office_assignmentDTO>) rn2.list;
								if (data != null && data.size() > 0) {

									for (Am_office_assignmentDTO am_office_assignmentDTO : data) {

										Office_unitsDTO office_unitsDTO = null;
										Am_other_office_unitDTO am_other_office_unitDTO = null;

										if(am_office_assignmentDTO.officeType==1){
											office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(am_office_assignmentDTO.officeUnitId);
										}

										else if(am_office_assignmentDTO.officeType==2){
											 am_other_office_unitDTO  = Am_other_office_unitRepository.getInstance().getAm_other_office_unitDTOByID(am_office_assignmentDTO.officeUnitId);
										}


										//Am_office_assignment_requestDTO am_office_assignment_requestDTO =  Am_office_assignment_requestDAO.getInstance().getDTOFromID(am_office_assignmentDTO.amOfficeAssignmentRequestId);
							%>
							<tr>
								<%@include file="am_office_assignmentSearchRow.jsp" %>
							</tr>
							<% }
							} %>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			