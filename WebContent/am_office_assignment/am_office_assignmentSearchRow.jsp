<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@ page import="util.CommonDTO" %>
<%@ page import="util.StringUtils" %>
<%@ page import="am_office_assignment_request.AmOfficeAssignmentRequestStatus" %>

<td>
    <%=UtilCharacter.getDataByLanguage(Language,am_office_assignmentDTO.requesterNameBn,am_office_assignmentDTO.requesterNameEn)%>
</td>

<td>
    <%=UtilCharacter.getDataByLanguage(Language,am_office_assignmentDTO.requesterOfficeUnitOrgNameBn,am_office_assignmentDTO.requesterOfficeUnitOrgNameEn)%>
</td>

<td>
    <%=UtilCharacter.getDataByLanguage(Language,am_office_assignmentDTO.requesterOfficeUnitNameBn,am_office_assignmentDTO.requesterOfficeUnitNameEn)%>
</td>

<td>
    <%=StringUtils.getFormattedDate(Language,am_office_assignmentDTO.assignmentDate)%>
</td>

<%if(am_office_assignmentDTO.officeType==1){%>
    <td>
        <%=UtilCharacter.getDataByLanguage(Language,office_unitsDTO.unitNameBng,office_unitsDTO.unitNameEng)%>
    </td>
<%}%>

<%if(am_office_assignmentDTO.officeType==2){%>
<td>
    <%=UtilCharacter.getDataByLanguage(Language,am_other_office_unitDTO.officeNameBn,am_other_office_unitDTO.officeNameEn)%>
</td>
<%}%>


<td>
    <%=CatRepository.getInstance().getText(Language,"building_type",am_office_assignmentDTO.buildingTypeCat)%>
</td>

<td>
    <span class="btn btn-sm border-0 shadow"
          style="background-color: <%=AmOfficeAssignmentRequestStatus.getColor(am_office_assignmentDTO.status)%>; color: white; border-radius: 8px;cursor: text">
            <%=CatRepository.getInstance().getText(
                    Language, "am_office_assignment_request_status", am_office_assignmentDTO.status
            )%>
    </span>
</td>

<%CommonDTO commonDTO = am_office_assignmentDTO; %>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='<%=servletName%>?actionType=view&ID=<%=commonDTO.iD%>'"
    >
        <i class="fa fa-eye"></i>
    </button>
</td>
