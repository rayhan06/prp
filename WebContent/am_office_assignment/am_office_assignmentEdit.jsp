<%@ page import="static permission.MenuConstants.AM_OFFICE_APPROVAL_MAPPING_SEARCH" %>
<%@ page import="static permission.MenuConstants.AM_OFFICE_ASSIGNMENT_REQUEST" %>
<%@ page import="static permission.MenuConstants.*" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.ArrayList" %>
<%
	request.setAttribute("menuIDPath", new ArrayList<>(Arrays.asList(ASSET_MANAGEMENT,
			AM_OFFICE_ASSIGNMENT_REQUEST,
			AM_OFFICE_APPROVAL_MAPPING_SEARCH)));
%>

<jsp:include page="../common/layout.jsp" flush="true">
<jsp:param name="title" value="Edit User" /> 
	<jsp:param name="body" value="../am_office_assignment/am_office_assignmentEditBody.jsp" />
</jsp:include> 