<%@ page import="am_minister_hostel_level.Am_minister_hostel_levelServlet" %>
<%@ page import="am_minister_hostel_block.Am_minister_hostel_blockDTO" %>
<%@ page import="am_minister_hostel_block.Am_minister_hostel_blockRepository" %>

<div class="row">
    <div class="col-6">
        <div class="form-group row d-flex align-items-center">
            <label class="col-4 col-form-label "><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_BLOCK, loginDTO)%><span class="required" style="color: red"> * </span></label>
            <div class="col-8">

                <select  class='form-control' onchange="mpHostelBlockTypeChanged(this)"  name='mpHostelBlock' id = 'mpHostelBlock'   tag='pb_html'>
                    <%
                        List<Am_minister_hostel_blockDTO> am_minister_hostel_blockDTOS = Am_minister_hostel_blockRepository.getInstance().getAm_minister_hostel_blockList();
                        String blockOptions = Utils.buildSelectOption(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language.equalsIgnoreCase("english"));

                        StringBuilder option = new StringBuilder();
                        for(Am_minister_hostel_blockDTO am_minister_hostel_blockDTO:am_minister_hostel_blockDTOS){
                            option.append("<option value = '").append(am_minister_hostel_blockDTO.iD).append("'>");
                            option.append(Utils.getDigits(am_minister_hostel_blockDTO.blockNo,HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language)).append("</option>");
                        }
                        blockOptions+=option.toString();
                    %>
                    <%=blockOptions%>

                </select>
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row d-flex align-items-center">
            <label class="col-4 col-form-label "><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_SIDE, loginDTO)%><span class="required" style="color: red"> * </span></label>
            <div class="col-8">

                <select id="mpHostelSide"
                        class='form-control rounded shadow-sm w-100'   name="side"
                        onchange="mpHostelSideChanged(this)"
                >
                </select>

            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row d-flex align-items-center">
            <label class="col-4 col-form-label "><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_UNIT, loginDTO)%><span class="required" style="color: red"> * </span></label>
            <div class="col-8">
                <select id="mpHostelUnit"
                        class='form-control rounded shadow-sm w-100'   name="unit"
                        onchange="mpHostelUnitChanged(this)"
                >
                </select>
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row d-flex align-items-center">
            <label class="col-4 col-form-label "><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_LEVEL, loginDTO)%><span class="required" style="color: red"> * </span></label>
            <div class="col-8">
                <select id="mpHostelLevel"
                        class='form-control rounded shadow-sm w-100'   name="mpHostelLevel"
                >
                </select>
            </div>
        </div>

    </div>
</div>