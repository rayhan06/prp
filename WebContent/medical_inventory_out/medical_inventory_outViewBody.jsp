

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="medical_inventory_out.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>




<%
String servletName = "Medical_inventory_outServlet";
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.MEDICAL_INVENTORY_OUT_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
Medical_inventory_outDAO medical_inventory_outDAO = new Medical_inventory_outDAO("medical_inventory_out");
Medical_inventory_outDTO medical_inventory_outDTO = medical_inventory_outDAO.getDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.MEDICAL_INVENTORY_OUT_ADD_MEDICAL_INVENTORY_OUT_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="form-body">
                <h5 class="table-title">
                    <%=LM.getText(LC.MEDICAL_INVENTORY_OUT_ADD_MEDICAL_INVENTORY_OUT_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
									

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.MEDICAL_INVENTORY_OUT_ADD_MEDICALITEMCAT, loginDTO)%></b></td>
								<td>
						
											<%
											value = medical_inventory_outDTO.medicalItemCat + "";
											%>
											<%
											value = CatRepository.getInstance().getText(Language, "medical_item", medical_inventory_outDTO.medicalItemCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.MEDICAL_INVENTORY_OUT_ADD_DRUGINFORMATIONID, loginDTO)%></b></td>
								<td>
						
											<%
											value = medical_inventory_outDTO.drugInformationId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.MEDICAL_INVENTORY_OUT_ADD_MEDICALREAGENTNAMEID, loginDTO)%></b></td>
								<td>
						
											<%
											value = medical_inventory_outDTO.medicalReagentNameId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.MEDICAL_INVENTORY_OUT_ADD_MEDICALEQUIPMENTNAMEID, loginDTO)%></b></td>
								<td>
						
											<%
											value = medical_inventory_outDTO.medicalEquipmentNameId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.MEDICAL_INVENTORY_OUT_ADD_STOCKOUTQUANTITY, loginDTO)%></b></td>
								<td>
						
											<%
											value = medical_inventory_outDTO.stockOutQuantity + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.MEDICAL_INVENTORY_OUT_ADD_UNITPRICE, loginDTO)%></b></td>
								<td>
						
											<%
											value = medical_inventory_outDTO.unitPrice + "";
											%>
											<%
											value = String.format("%.1f", medical_inventory_outDTO.unitPrice);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.MEDICAL_INVENTORY_OUT_ADD_TRANSACTIONDATE, loginDTO)%></b></td>
								<td>
						
											<%
											value = medical_inventory_outDTO.transactionDate + "";
											%>
											<%
											String formatted_transactionDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_transactionDate, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.MEDICAL_INVENTORY_OUT_ADD_REMARKS, loginDTO)%></b></td>
								<td>
						
											<%
											value = medical_inventory_outDTO.remarks + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			
			
			
			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.MEDICAL_INVENTORY_OUT_ADD_EXPIRYDATE, loginDTO)%></b></td>
								<td>
						
											<%
											value = medical_inventory_outDTO.expiryDate + "";
											%>
											<%
											String formatted_expiryDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_expiryDate, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.MEDICAL_INVENTORY_OUT_ADD_MEDICALINVENTORYINID, loginDTO)%></b></td>
								<td>
						
											<%
											value = medical_inventory_outDTO.medicalInventoryInId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			
			
			
			
		
						</table>
                  </div>
			</div>	

        </div>
	</div>
</div>