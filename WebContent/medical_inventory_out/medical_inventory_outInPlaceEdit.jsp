<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="medical_inventory_out.Medical_inventory_outDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Medical_inventory_outDTO medical_inventory_outDTO = (Medical_inventory_outDTO)request.getAttribute("medical_inventory_outDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(medical_inventory_outDTO == null)
{
	medical_inventory_outDTO = new Medical_inventory_outDTO();
	
}
System.out.println("medical_inventory_outDTO = " + medical_inventory_outDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.MEDICAL_INVENTORY_OUT_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=medical_inventory_outDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_medicalItemCat'>")%>
			
	
	<div class="form-inline" id = 'medicalItemCat_div_<%=i%>'>
		<select class='form-control'  name='medicalItemCat' id = 'medicalItemCat_category_<%=i%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "medical_item", medical_inventory_outDTO.medicalItemCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "medical_item", -1);			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_drugInformationId'>")%>
			

		<input type='hidden' class='form-control'  name='drugInformationId' id = 'drugInformationId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + medical_inventory_outDTO.drugInformationId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_medicalReagentNameId'>")%>
			

		<input type='hidden' class='form-control'  name='medicalReagentNameId' id = 'medicalReagentNameId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + medical_inventory_outDTO.medicalReagentNameId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_medicalEquipmentNameId'>")%>
			

		<input type='hidden' class='form-control'  name='medicalEquipmentNameId' id = 'medicalEquipmentNameId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + medical_inventory_outDTO.medicalEquipmentNameId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_stockOutQuantity'>")%>
			
	
	<div class="form-inline" id = 'stockOutQuantity_div_<%=i%>'>
		<input type='text' class='form-control'  name='stockOutQuantity' id = 'stockOutQuantity_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + medical_inventory_outDTO.stockOutQuantity + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_unitPrice'>")%>
			
	
	<div class="form-inline" id = 'unitPrice_div_<%=i%>'>
		<input type='text' class='form-control'  name='unitPrice' id = 'unitPrice_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + medical_inventory_outDTO.unitPrice + "'"):("'" + "0.0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_transactionDate'>")%>
			
	
	<div class="form-inline" id = 'transactionDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'transactionDate_date_<%=i%>' name='transactionDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_transactionDate = dateFormat.format(new Date(medical_inventory_outDTO.transactionDate));
	%>
	'<%=formatted_transactionDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_remarks'>")%>
			
	
	<div class="form-inline" id = 'remarks_div_<%=i%>'>
		<input type='text' class='form-control'  name='remarks' id = 'remarks_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + medical_inventory_outDTO.remarks + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedByUserId'>")%>
			

		<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + medical_inventory_outDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedByOrganogramId'>")%>
			

		<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + medical_inventory_outDTO.insertedByOrganogramId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + medical_inventory_outDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_expiryDate'>")%>
			
	
	<div class="form-inline" id = 'expiryDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'expiryDate_date_<%=i%>' name='expiryDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_expiryDate = dateFormat.format(new Date(medical_inventory_outDTO.expiryDate));
	%>
	'<%=formatted_expiryDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_medicalInventoryInId'>")%>
			

		<input type='hidden' class='form-control'  name='medicalInventoryInId' id = 'medicalInventoryInId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + medical_inventory_outDTO.medicalInventoryInId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + medical_inventory_outDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=medical_inventory_outDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Medical_inventory_outServlet?actionType=view&ID=<%=medical_inventory_outDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Medical_inventory_outServlet?actionType=view&modal=1&ID=<%=medical_inventory_outDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	