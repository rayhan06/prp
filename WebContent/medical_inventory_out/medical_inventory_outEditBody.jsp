
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="medical_inventory_out.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="util.TimeFormat"%>

<%
Medical_inventory_outDTO medical_inventory_outDTO;
medical_inventory_outDTO = (Medical_inventory_outDTO)request.getAttribute("medical_inventory_outDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(medical_inventory_outDTO == null)
{
	medical_inventory_outDTO = new Medical_inventory_outDTO();
	
}
System.out.println("medical_inventory_outDTO = " + medical_inventory_outDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = LM.getText(LC.MEDICAL_INVENTORY_OUT_ADD_MEDICAL_INVENTORY_OUT_ADD_FORMNAME, loginDTO);


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

boolean isPermanentTable = true;
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="Medical_inventory_outServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">


<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.MEDICAL_INVENTORY_OUT_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=medical_inventory_outDTO.iD%>' tag='pb_html'/>
	
												
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.MEDICAL_INVENTORY_OUT_ADD_MEDICALITEMCAT, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'medicalItemCat_div_<%=i%>'>	
		<select class='form-control'  name='medicalItemCat' id = 'medicalItemCat_category_<%=i%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "medical_item", medical_inventory_outDTO.medicalItemCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "medical_item", -1);			
}
%>
<%=Options%>
		</select>
		
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='drugInformationId' id = 'drugInformationId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + medical_inventory_outDTO.drugInformationId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='medicalReagentNameId' id = 'medicalReagentNameId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + medical_inventory_outDTO.medicalReagentNameId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='medicalEquipmentNameId' id = 'medicalEquipmentNameId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + medical_inventory_outDTO.medicalEquipmentNameId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.MEDICAL_INVENTORY_OUT_ADD_STOCKOUTQUANTITY, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'stockOutQuantity_div_<%=i%>'>	
		<input type='text' class='form-control'  name='stockOutQuantity' id = 'stockOutQuantity_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + medical_inventory_outDTO.stockOutQuantity + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.MEDICAL_INVENTORY_OUT_ADD_UNITPRICE, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'unitPrice_div_<%=i%>'>	
		<input type='text' class='form-control'  name='unitPrice' id = 'unitPrice_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + medical_inventory_outDTO.unitPrice + "'"):("'" + "0.0" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.MEDICAL_INVENTORY_OUT_ADD_TRANSACTIONDATE, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'transactionDate_div_<%=i%>'>	
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'transactionDate_date_<%=i%>' name='transactionDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_transactionDate = dateFormat.format(new Date(medical_inventory_outDTO.transactionDate));
	%>
	'<%=formatted_transactionDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.MEDICAL_INVENTORY_OUT_ADD_REMARKS, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'remarks_div_<%=i%>'>	
		<input type='text' class='form-control'  name='remarks' id = 'remarks_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + medical_inventory_outDTO.remarks + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + medical_inventory_outDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + medical_inventory_outDTO.insertedByOrganogramId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + medical_inventory_outDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.MEDICAL_INVENTORY_OUT_ADD_EXPIRYDATE, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'expiryDate_div_<%=i%>'>	
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'expiryDate_date_<%=i%>' name='expiryDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_expiryDate = dateFormat.format(new Date(medical_inventory_outDTO.expiryDate));
	%>
	'<%=formatted_expiryDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='medicalInventoryInId' id = 'medicalInventoryInId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + medical_inventory_outDTO.medicalInventoryInId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + medical_inventory_outDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + medical_inventory_outDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
					
	






				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						%>
						<%=LM.getText(LC.MEDICAL_INVENTORY_OUT_EDIT_MEDICAL_INVENTORY_OUT_CANCEL_BUTTON, loginDTO)%>
						<%
					}
					else
					{
						%>
						<%=LM.getText(LC.MEDICAL_INVENTORY_OUT_ADD_MEDICAL_INVENTORY_OUT_CANCEL_BUTTON, loginDTO)%>
						<%
					}
					
					%>
					</a>
					<button class="btn btn-success" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						%>
						<%=LM.getText(LC.MEDICAL_INVENTORY_OUT_EDIT_MEDICAL_INVENTORY_OUT_SUBMIT_BUTTON, loginDTO)%>
						<%
					}
					else
					{
						%>
						<%=LM.getText(LC.MEDICAL_INVENTORY_OUT_ADD_MEDICAL_INVENTORY_OUT_SUBMIT_BUTTON, loginDTO)%>
						<%
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


$(document).ready( function(){

    dateTimeInit("<%=Language%>");
});

function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}


	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Medical_inventory_outServlet");	
}

function init(row)
{


	
}

var row = 0;
	
window.onload =function ()
{
	init(row);
	CKEDITOR.replaceAll();
}

var child_table_extra_id = <%=childTableStartingID%>;



</script>






