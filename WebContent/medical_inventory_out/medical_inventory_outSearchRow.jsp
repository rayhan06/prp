<%@page pageEncoding="UTF-8" %>

<%@page import="medical_inventory_out.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.MEDICAL_INVENTORY_OUT_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_MEDICAL_INVENTORY_OUT;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Medical_inventory_outDTO medical_inventory_outDTO = (Medical_inventory_outDTO)request.getAttribute("medical_inventory_outDTO");
CommonDTO commonDTO = medical_inventory_outDTO;
String servletName = "Medical_inventory_outServlet";


System.out.println("medical_inventory_outDTO = " + medical_inventory_outDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Medical_inventory_outDAO medical_inventory_outDAO = (Medical_inventory_outDAO)request.getAttribute("medical_inventory_outDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
		
											<td id = '<%=i%>_medicalItemCat'>
											<%
											value = medical_inventory_outDTO.medicalItemCat + "";
											%>
											<%
											value = CatRepository.getInstance().getText(Language, "medical_item", medical_inventory_outDTO.medicalItemCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_drugInformationId'>
											<%
											value = medical_inventory_outDTO.drugInformationId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_medicalReagentNameId'>
											<%
											value = medical_inventory_outDTO.medicalReagentNameId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_medicalEquipmentNameId'>
											<%
											value = medical_inventory_outDTO.medicalEquipmentNameId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_stockOutQuantity'>
											<%
											value = medical_inventory_outDTO.stockOutQuantity + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_unitPrice'>
											<%
											value = medical_inventory_outDTO.unitPrice + "";
											%>
											<%
											value = String.format("%.1f", medical_inventory_outDTO.unitPrice);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_transactionDate'>
											<%
											value = medical_inventory_outDTO.transactionDate + "";
											%>
											<%
											String formatted_transactionDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_transactionDate, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_remarks'>
											<%
											value = medical_inventory_outDTO.remarks + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
		
											<td id = '<%=i%>_expiryDate'>
											<%
											value = medical_inventory_outDTO.expiryDate + "";
											%>
											<%
											String formatted_expiryDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_expiryDate, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_medicalInventoryInId'>
											<%
											value = medical_inventory_outDTO.medicalInventoryInId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
		
	

											<td>
												<button
														type="button"
														class="btn-sm border-0 shadow bg-light btn-border-radius"
														style="color: #ff6b6b;"
														onclick="location.href='Medical_inventory_outServlet?actionType=view&ID=<%=medical_inventory_outDTO.iD%>'"
												>
													<i class="fa fa-eye"></i>
												</button>												
											</td>
	
											<td id = '<%=i%>_Edit'>
												<button
														type="button"
														class="btn-sm border-0 shadow btn-border-radius text-white"
														style="background-color: #ff6b6b;"
														onclick="location.href='Medical_inventory_outServlet?actionType=getEditPage&ID=<%=medical_inventory_outDTO.iD%>'"
												>
													<i class="fa fa-edit"></i>
												</button>																			
											</td>											
											
											
											<td id='<%=i%>_checkbox' class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=medical_inventory_outDTO.iD%>'/></span>
												</div>
											</td>
																						
											

