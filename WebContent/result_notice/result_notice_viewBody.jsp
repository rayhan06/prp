<%@ page import="job_applicant_application.Job_applicant_applicationDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="job_applicant_application.Job_applicant_applicationRepository" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="admit_card.Admit_cardDTO" %>
<%@ page import="admit_card.Admit_cardDAO" %>
<%@ page import="pb.Utils" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="recruitment_job_specific_exam_type.RecruitmentJobSpecificExamTypeDTO" %>
<%@ page import="recruitment_job_specific_exam_type.JobSpecificExamTypeRepository" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameRepository" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameDTO" %><%--
  Created by IntelliJ IDEA.
  User: Nafees
  Date: 3/8/2022
  Time: 2:50 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%

    String Language = "Bangla";
    List<Recruitment_test_nameDTO> recruitment_test_nameDTOS = Recruitment_test_nameRepository.getInstance().
            getRecruitment_test_nameList().stream().filter(e -> e.recruitmentTestNamePublishCat == 2).collect(Collectors.toList());

    List<Job_applicant_applicationDTO> job_applicant_applicationDTOS = Job_applicant_applicationRepository.
            getInstance().getJob_applicant_applicationList();

    if(job_applicant_applicationDTOS != null && job_applicant_applicationDTOS.size() > 0){
        job_applicant_applicationDTOS = job_applicant_applicationDTOS.
                stream()
                .filter(e -> e.isSelected == 1 || e.isSelected == 2)
                //.filter(UtilCharacter.distinctByKey(p -> p.level))
                .collect(Collectors.toList());
    }


    Admit_cardDAO admit_cardDAO = new Admit_cardDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");


%>
<head>
    <title>View Result</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>

<body>
<%
    if (recruitment_test_nameDTOS != null && recruitment_test_nameDTOS.size() > 0) {
        for (Recruitment_test_nameDTO recruitment_test_nameDTO : recruitment_test_nameDTOS) {  %>


<%
    List<Recruitment_job_descriptionDTO> recruitmentJobDescriptionDTOS = Recruitment_job_descriptionRepository.getInstance()
            .getRecruitment_job_descriptionListByRecTestNameId(recruitment_test_nameDTO.iD);

    if (recruitmentJobDescriptionDTOS != null && recruitmentJobDescriptionDTOS.size() > 0) {
%>

<h1 style="display: flex;justify-content: center"><%=UtilCharacter.getDataByLanguage(Language, recruitment_test_nameDTO.nameBn,
        recruitment_test_nameDTO.nameEn)%></h1>
<h3 style="display: flex;justify-content: center"><%=UtilCharacter.getDataByLanguage(Language, "(ফলাফল সংক্রান্ত নোটিশ)",
        "(Result related notice)")%></h3>
<div class="table-responsive mt-5 mb-5" >
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><%=UtilCharacter.getDataByLanguage(Language, "ক্রমিক নং", "Serial No.")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "পদের নাম", "Position name")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "লিখিত পরীক্ষা সংক্রান্ত", "Concerning the written test")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "ব্যবহারিক পরীক্ষা সংক্রান্ত", "Concerning the practical test")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "মৌখিক পরীক্ষা সংক্রান্ত", "Concerning the viva test")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "স্বাস্থ্য পরীক্ষা সংক্রান্ত", "Concerning the health test")%>
            </th>
        </tr>
        </thead>

        <%
            int i = 1;
            for(Recruitment_job_descriptionDTO recruitment_job_descriptionDTO : recruitmentJobDescriptionDTOS){


                String postName = "";
                String writtenExamDetails="X";
                String practicalExamDetails="X";
                String vivaExamDetails="X";
                String healthExamDetails="X";

                if (recruitment_job_descriptionDTO != null) {
                    postName = recruitment_job_descriptionDTO.jobTitleBn;

                    //no need recruitment test name id. Since every recruitment job description id is unique
                    List<RecruitmentJobSpecificExamTypeDTO> recruitmentJobSpecificExamTypeDTOS = JobSpecificExamTypeRepository.getInstance()
                            .getRecruitmentJobSpecificExamTypeDTOByjob_id(recruitment_job_descriptionDTO.iD);

                    String approve = "SHORT LISTED";
                    for(RecruitmentJobSpecificExamTypeDTO jobSpecificExamTypeDTO : recruitmentJobSpecificExamTypeDTOS){
                        String jobExamType = CatRepository.getName(Language, "job_exam_type", jobSpecificExamTypeDTO.jobExamTypeCat);

                        if(jobSpecificExamTypeDTO.isSelected){

                            if(job_applicant_applicationDTOS != null){
                                Job_applicant_applicationDTO job_applicant_applicationDTO = job_applicant_applicationDTOS.stream()
                                        .filter(e -> e.level == jobSpecificExamTypeDTO.order && e.jobId ==recruitment_job_descriptionDTO.iD).findFirst().orElse(null);

                                if(jobExamType.equals("লিখিত")){

                                    if(job_applicant_applicationDTO != null){
                                        String writtenExamPath = request.getContextPath()+"/PublicServlet?actionType=getResultNotice&job="+job_applicant_applicationDTO.jobId+"&level="+job_applicant_applicationDTO.level+"&approve="+approve;
                                        writtenExamDetails ="<a class=\"text-light\" href="+writtenExamPath+" target=\"_blank\" ><u>বিস্তারিত</u></a>";
                                    }
                                    else{
                                        writtenExamDetails="এখনো প্রকাশ করা হয়নি";
                                    }


                                }

                                else if(jobExamType.equals("ব্যবহারিক")){
                                    if(job_applicant_applicationDTO != null){
                                        String practicalExamPath = request.getContextPath()+"/PublicServlet?actionType=getResultNotice&job="+job_applicant_applicationDTO.jobId+"&level="+job_applicant_applicationDTO.level+"&approve="+approve;
                                        practicalExamDetails ="<a class=\"text-light\" href="+practicalExamPath+" target=\"_blank\" ><u>বিস্তারিত</u></a>";
                                    }
                                    else{
                                        practicalExamDetails="এখনো প্রকাশ করা হয়নি";
                                    }

                                }

                                else if(jobExamType.equals("মৌখিক")){

                                    if(job_applicant_applicationDTO != null){
                                        String vivaExamPath = request.getContextPath()+"/PublicServlet?actionType=getResultNotice&job="+job_applicant_applicationDTO.jobId+"&level="+job_applicant_applicationDTO.level+"&approve="+approve;
                                        vivaExamDetails ="<a class=\"text-light\" href="+vivaExamPath+" target=\"_blank\" ><u>বিস্তারিত</u></a>";
                                    }
                                    else{
                                        vivaExamDetails="এখনো প্রকাশ করা হয়নি";
                                    }

                                }
                                else if(jobExamType.equals("স্বাস্থ্য")){

                                    if(job_applicant_applicationDTO != null){

                                    }
                                    else{
                                        healthExamDetails="এখনো প্রকাশ করা হয়নি";
                                    }

                                }

                            }
                        }




                    }%>

        <tr>
            <td><%=UtilCharacter.convertDataByLanguage(Language,String.valueOf(i++))%></td>
            <td><%=postName%></td>
            <td><%=writtenExamDetails%></td>
            <td><%=practicalExamDetails%></td>
            <td><%=vivaExamDetails%></td>
            <td><%=healthExamDetails%></td>

        </tr>


        <%}
        }   %>


        <%}%>
    </table>
</div>
<%}%>
<%} else {%>
<p style="display: flex;justify-content: center;align-items: center;font-weight: bold;font-size: larger;color: red;">
    কোনো পরীক্ষার ফলাফলের তথ্য খুঁজে পাওয়া যায়নি</p>
<%}%>

</body>

<style>
    @import url('https://fonts.maateen.me/solaiman-lipi/font.css');

    body {
        /*background: #eee;*/
        background: linear-gradient(90deg, rgba(16, 66, 106, 1) 1%, rgba(7, 106, 154, 1) 32%, rgba(10, 67, 113, 1) 67%);
        padding: 3rem;
        font-family: 'SolaimanLipi', sans-serif;
        font-weight: 400;
        color: #dadada;
    }

    .notification-container {
        /*background: #fff;*/
        padding: 1rem 2rem;
        border-radius: 6px;
        cursor: pointer;
        margin-bottom: 1.2rem;
        margin-top: 0rem;
        -webkit-transition: all 5s ease;
        transition: all 5s ease;
        background: rgba(255, 255, 255, 0.25);
        /*box-shadow: rgba(0, 0, 0, 0.19) 0px 10px 20px, rgba(0, 0, 0, 0.23) 0px 6px 6px;*/
        box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px;
        backdrop-filter: blur(4px);
        -webkit-backdrop-filter: blur(4px);
        border-radius: 10px;
        /*border: 1px solid rgba( 255, 255, 255, 0.18 );*/
    }

    .notification-container:hover, .notification-container:focus {
        box-shadow: rgba(0, 0, 0, 0.25) 0px 25px 50px -12px;
        position: relative;
        top: -5px;
    }

    .notification-container .notification-text {
        /*color: #515155;*/
        margin-bottom: 0.2rem;
        margin-top: .2rem;
        font-weight: 600;
    }

    .notification-container .notification-grade {
        /*color: #2b2b2b;*/
        margin-bottom: 0.2rem;
        margin-top: 0rem;
    }

    .notification-container .date-title {
        /*color: #2b2b2b;*/
        margin-bottom: 0rem;
        margin-top: 0rem;
    }

    .notification-container .date-title .date-value {
        /*color: #2b2b2b;*/
        margin-bottom: 0rem;
        margin-top: 0rem;
    }
</style>
<script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous"
></script>
<script>
    $(document).ready(function () {
        $(document).on("click", ".notification-container", function () {

            let content = this;
            let job = content.querySelector('.job').value;
            let level = content.querySelector('.level').value;
            let approve = content.querySelector('.approve').value;
            let url = 'PublicServlet?actionType=getResultNotice&job=' + job + '&level=' + level + '&approve=' + approve;
            window.open(url, "_blank");
        });
    });
</script>
