<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="travel_allowance.*" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="geolocation.GeoLocationUtils" %>


<%
    String navigator2 = "navTRAVEL_ALLOWANCE";
    String servletName = "Travel_allowanceServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th>
                <%=LM.getText(LC.FUND_MANAGEMENT_ADD_EMPLOYEERECORDSID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_PURPOSE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_STARTDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_STARTTIMECAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_ENDDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_ENDTIMECAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_STARTADDRESS, loginDTO)%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "Bill Form" : "বিল ফর্ম"%>
            </th>
            <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_SEARCH_TRAVEL_ALLOWANCE_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span>All</span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Travel_allowanceDTO> data = (List<Travel_allowanceDTO>) recordNavigator.list;

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Travel_allowanceDTO travel_allowanceDTO = data.get(i);


        %>
        <tr>
            <td>
                <%=getDataByLanguage(Language, travel_allowanceDTO.employeeNameBn, travel_allowanceDTO.employeeNameEn)%>
                <br>
                <b>
                    <%=getDataByLanguage(Language, travel_allowanceDTO.organogramNameBn, travel_allowanceDTO.organogramNameEn)%>
                </b>
                <br>
                <%=getDataByLanguage(Language, travel_allowanceDTO.officeNameBn, travel_allowanceDTO.officeNameEn)%>
            </td>

            <td>
                <%=travel_allowanceDTO.purpose%>
            </td>

            <td>
                <%
                    String formatted_startDate = simpleDateFormat.format(new Date(travel_allowanceDTO.startDate));
                %>
                <%=Utils.getDigits(formatted_startDate, Language)%>


            </td>

            <td>
                <%
                    value = CatRepository.getInstance().getText(Language, "travel_time", travel_allowanceDTO.startTimeCat);
                %>
                <%=Utils.getDigits(value, Language)%>
            </td>

            <td>
                <%
                    String formatted_endDate = simpleDateFormat.format(new Date(travel_allowanceDTO.endDate));
                %>
                <%=Utils.getDigits(formatted_endDate, Language)%>
            </td>

            <td>
                <%
                    value = CatRepository.getInstance().getText(Language, "travel_time", travel_allowanceDTO.endTimeCat);
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>


            <td>
                <%=GeoLocationUtils.getGeoLocationString(travel_allowanceDTO.startAddress, Language)%>
            </td>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=form&ID=<%=travel_allowanceDTO.iD%>'"
                >
                    <%=Language.equalsIgnoreCase("English") ? "Bill Form" : "বিল ফর্ম"%>
                </button>
            </td>

            <%CommonDTO commonDTO = travel_allowanceDTO; %>
            <td>
                <%
                    if (commonDTO.jobCat == SessionConstants.DEFAULT_JOB_CAT) {
                %>
                <button
                        type="button"
                        class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=commonDTO.iD%>'"
                >
                    <i class="fa fa-edit"></i>
                </button>
                <%
                    }
                %>
            </td>

            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID' value='<%=travel_allowanceDTO.iD%>'/></span>
                </div>
            </td>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>


			