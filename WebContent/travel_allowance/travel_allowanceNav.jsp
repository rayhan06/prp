<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    System.out.println("Inside nav.jsp");
    String url = "Travel_allowanceServlet?actionType=search";
    String toolbarOpen=request.getParameter("toolbarOpen")==null?"0":request.getParameter("toolbarOpen");

    Office_unitsDTO officeUnitsDTO = null;
    Employee_recordsDTO employeeRecordsDTO = null;
    String officeUnitIdStr = request.getParameter("officeUnitId");
    String employeeRecordIdStr = request.getParameter("employeeRecordId");
    if (officeUnitIdStr != null && officeUnitIdStr.length() > 0) {
        officeUnitsDTO = Office_unitsRepository.getInstance()
                .getOffice_unitsDTOByID(Long.parseLong(officeUnitIdStr));
    }
    if (employeeRecordIdStr != null && employeeRecordIdStr.length() > 0) {
        employeeRecordsDTO = Employee_recordsRepository.getInstance()
                .getById(Long.parseLong(employeeRecordIdStr));
    }
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0,true)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_STARTDATE, loginDTO)%>
                            (<%=LM.getText(LC.HM_FROM, loginDTO)%> )
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="start_date_start_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type="hidden" id="start_date_start" name="start_date_start">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_STARTDATE, loginDTO)%>
                            (<%=LM.getText(LC.HM_TO, loginDTO)%> )
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="start_date_end_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type="hidden" id="start_date_end" name="start_date_end">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_ENDDATE, loginDTO)%>
                            (<%=LM.getText(LC.HM_FROM, loginDTO)%>)
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="end_date_start_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type="hidden" id="end_date_start" name="end_date_start">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_ENDDATE, loginDTO)%>
                            (<%=LM.getText(LC.HM_TO, loginDTO)%>)
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="end_date_end_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type="hidden" id="end_date_end" name="end_date_end">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_STARTTIMECAT, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='start_time_cat' id='start_time_cat'
                                    onSelect='setSearchChanged()'>
                                <%
                                    Options = CatRepository.getInstance().buildOptions("travel_time", Language, null);
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_ENDTIMECAT, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='end_time_cat' id='end_time_cat'
                                    onSelect='setSearchChanged()'>
                                <%
                                    Options = CatRepository.getInstance().buildOptions("travel_time", Language, null);
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=LM.getText(LC.TRAVEL_ALLOWANCE_VIEW_APPLICANT, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                    id="employeeRecordId_modal_button"
                                    onclick="employeeRecordIdModalBtnClicked();">
                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                            </button>
                            <div class="input-group" id="employeeRecordId_div" style="display: none">
                                <input type="hidden" name='employeeRecordId' id='employeeRecordId_input' value=""
                                       onchange='setSearchChanged()'>
                                <button type="button" class="btn btn-secondary form-control" disabled
                                        id="employeeRecordId_text"></button>
                                <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                                    <button type="button" class="btn btn-outline-danger"
                                            onclick="crsBtnClicked('employeeRecordId');"
                                            id='employeeRecordId_crs_btn' tag='pb_html'>
                                        x
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.FUND_MANAGEMENT_ADD_OFFICEUNITID, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <%--Office Unit Modal Trigger Button--%>
                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                    id="office_units_id_modal_button"
                                    onclick="officeModalButtonClicked();">
                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                            </button>
                            <%--Modal Included in employee_office_report_Body.jsp--%>
                            <div class="input-group" id="office_units_id_div" style="display: none">
                                <input type="hidden" name='officeUnitId' id='office_units_id_input' value="">
                                <button type="button"
                                        class="btn btn-secondary btn-block shadow-sm btn-border-radius form-control"
                                        id="office_units_id_text" onclick="officeModalEditButtonClicked()">
                                </button>
                                <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                                    <button type="button" class="btn btn-outline-danger"
                                            onclick="crsBtnClicked('office_units_id');"
                                            id='office_units_id_crs_btn' tag='pb_html'>
                                        x
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0,true)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <div class="search-loader-container-circle ">
            <div class="search-loader-circle"></div>
        </div>
    </div>
</template>
<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>

<script type="text/javascript">
    const start_time_catSelector = $('#start_time_cat');
    const end_time_catSelector = $('#end_time_cat');
    const anyFieldSelector = $('#anyfield');
    let employeeModel;
    let officeUnitModel;

    function resetInputs() {
        resetDateById('start_date_start_js');
        resetDateById('start_date_end_js');
        resetDateById('end_date_start_js');
        resetDateById('end_date_end_js');
        start_time_catSelector.select2("val", '-1');
        end_time_catSelector.select2("val", '-1');
        crsBtnClicked('employeeRecordId');
        crsBtnClicked('office_units_id');
    }

    window.addEventListener('popstate', e => {
        if (e.state) {
            let params = e.state.params;
            employeeModel = e.state.employeeModel;
            officeUnitModel = e.state.officeUnitModel;
            if (officeUnitModel) {
                viewOfficeIdInInput(officeUnitModel);
            }
            if (employeeModel) {
                employeeRecordIdInInput(employeeModel);
            }
            dosubmit(params, false);
            //resetInputs();
            let arr = params.split('&');
            arr.forEach(e => {
                    let item = e.split('=');
                    if (item.length === 2) {
                        switch (item[0]) {

                            case 'start_date_start':
                                setDateByTimestampAndId('start_date_start_js', item[1]);
                                break;
                            case 'start_date_end':
                                setDateByTimestampAndId('start_date_end_js', item[1]);
                                break;
                            case 'end_date_start':
                                setDateByTimestampAndId('end_date_start_js', item[1]);
                                break;
                            case 'end_date_end':
                                setDateByTimestampAndId('end_date_end_js', item[1]);
                                break;

                            case 'start_time_cat':
                                start_time_catSelector.val(item[1]).trigger('change');
                                break;
                            case 'end_time_cat':
                                end_time_catSelector.val(item[1]).trigger('change');
                                break;
                            case 'AnyField':
                                anyFieldSelector.val(item[1]);
                                break;
                            case 'employeeRecordId':
                                if (item[1].length == 0)
                                    crsBtnClicked('employeeRecordId');
                                break;
                            case 'officeUnitId':
                                if (item[1].length == 0)
                                    crsBtnClicked('office_units_id');
                                break;
                            default:
                                setPaginationFields(item);
                        }
                    }
                }
            );
        } else {
            dosubmit(null, false);
            resetInputs();
            resetPaginationFields();
        }
    });

    $(document).ready(() => {
        readyInit('Travel_allowanceServlet');
        select2SingleSelector('#start_time_cat', '<%=Language%>');
        select2SingleSelector('#end_time_cat', '<%=Language%>');
        if('<%=toolbarOpen%>'=='1'){
            document.querySelector('.kt-portlet__body').style.display = 'block';
            document.querySelector('#kt_portlet_tools_1').classList.remove('kt-portlet--collapse')
        }
        <%if(officeUnitsDTO != null) {%>
        officeUnitModel = {
            name: '<%=UtilCharacter.getDataByLanguage(Language, officeUnitsDTO.unitNameBng, officeUnitsDTO.unitNameEng)%>',
            id: <%=officeUnitsDTO.iD%>
        };
        viewOfficeIdInInput(officeUnitModel);
        <%}%>
        <%if(employeeRecordsDTO != null) {
        EmployeeOfficeDTO  employeeOfficeDTO =  EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employeeRecordsDTO.iD);
        Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
        %>
        employeeModel = {
            employeeNameEn: '<%= employeeRecordsDTO.nameEng%>',
            employeeNameBn: '<%= employeeRecordsDTO.nameBng%>',
            organogramNameEn: '<%= officeUnitOrganograms.designation_eng%>',
            organogramNameBn: '<%= officeUnitOrganograms.designation_bng%>',
            officeUnitNameEn: '<%= office_unitsDTO.unitNameEng%>',
            officeUnitNameBn: '<%= office_unitsDTO.unitNameBng%>',
            employeeRecordId: <%=employeeRecordsDTO.iD%>
        };
        employeeRecordIdInInput(employeeModel);
        <%}%>
    });

    function dosubmit(params, pushState = true) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (pushState) {
                    let stateParam = {
                        'params': params,
                        'employeeModel': employeeModel,
                        'officeUnitModel': officeUnitModel
                    }
                    history.pushState(stateParam, '', 'Travel_allowanceServlet?actionType=search&' + params);
                }
                setTimeout(() => {
                    document.getElementById('tableForm').innerHTML = this.responseText ;
                    setPageNo();
                    searchChanged = 0;
                }, 500);
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Travel_allowanceServlet?actionType=search&ajax=true&isPermanentTable=true&" + params, false);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number, pushState = true) {
        let params = 'search=true';
        if (document.getElementById('anyfield').value) {
            params += '&AnyField=' + document.getElementById('anyfield').value
        }

        $("#start_date_start").val(getDateStringById('start_date_start_js', 'DD/MM/YYYY'));
        params += '&start_date_start=' + getBDFormattedDate('start_date_start');
        $("#start_date_end").val(getDateStringById('start_date_end_js', 'DD/MM/YYYY'));
        params += '&start_date_end=' + getBDFormattedDate('start_date_end');
        if ($("#start_time_cat").val() != -1 && $("#start_time_cat").val() != "") {
            params += '&start_time_cat=' + document.getElementById('start_time_cat').value;
        }
        $("#end_date_start").val(getDateStringById('end_date_start_js', 'DD/MM/YYYY'));
        params += '&end_date_start=' + getBDFormattedDate('end_date_start');
        $("#end_date_end").val(getDateStringById('end_date_end_js', 'DD/MM/YYYY'));
        params += '&end_date_end=' + getBDFormattedDate('end_date_end');
        if ($("#end_time_cat").val() != -1 && $("#end_time_cat").val() != "") {
            params += '&end_time_cat=' + document.getElementById('end_time_cat').value;
        }
        params += '&employeeRecordId=' + $('#employeeRecordId_input').val();
        params += '&officeUnitId=' + $('#office_units_id_input').val();
        if(document.getElementsByClassName("kt-portlet__body")[0].style['display']!='none'){
            params+='&toolbarOpen=1';
        }
        //params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params, pushState);

    }

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
    }

    function viewOfficeIdInInput(selectedOffice) {

        if (selectedOffice.id === '') {
            return;
        }
        officeUnitModel = selectedOffice;
        console.log(selectedOffice);

        $('#office_units_id_modal_button').hide();
        $('#office_units_id_div').show();

        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id_input').val(selectedOffice.id);
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function officeModalEditButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        officeSearchSetSelectedOfficeLayers($('#office_units_id_input').val());
        $('#search_office_modal').modal();
    }

    function employeeRecordIdInInput(empInfo) {
        console.log(empInfo);
        employeeModel = empInfo;
        $('#employeeRecordId_modal_button').hide();
        $('#employeeRecordId_div').show();

        let language = '<%=Language.toLowerCase()%>'
        let designation;
        if (language === 'english') {
            designation = empInfo.employeeNameEn + ' (' + empInfo.organogramNameEn + ', ' + empInfo.officeUnitNameEn + ')';
        } else {
            designation = empInfo.employeeNameBn + ' (' + empInfo.organogramNameBn + ', ' + empInfo.officeUnitNameBn + ')';
        }
        document.getElementById('employeeRecordId_text').innerHTML = designation;
        $('#employeeRecordId_input').val(empInfo.employeeRecordId);
    }

    table_name_to_collcetion_map = new Map([
        ['employeeRecordId', {
            isSingleEntry: true,
            callBackFunction: employeeRecordIdInInput
        }]
    ]);

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    function employeeRecordIdModalBtnClicked() {
        modal_button_dest_table = 'employeeRecordId';
        $('#search_emp_modal').modal();
    }
</script>

