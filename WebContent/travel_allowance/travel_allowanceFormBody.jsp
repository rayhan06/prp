<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="travel_allowance.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>

<%@ page import="geolocation.*" %>
<%@ page import="java.util.concurrent.TimeUnit" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberInWord" %>
<%@include file="../pb/viewInitializer.jsp" %>
<%
    String servletName = "Travel_allowanceServlet";
    String ID = request.getParameter("ID");

    long id = Long.parseLong(ID);
    Travel_allowanceDTO travel_allowanceDTO = Travel_allowanceDAO.getInstance().getDTOByID(id);
    CommonDTO commonDTO = travel_allowanceDTO;
    String context = request.getContextPath() + "/";
    String startAddress = travel_allowanceDTO.startAddress;
    long startDate = travel_allowanceDTO.startDate;
    int startTimeCat = travel_allowanceDTO.startTimeCat;
    String pdfFileName = "Travel_bill_form_ "
            + travel_allowanceDTO.employeeUserName + "_" + simpleDateFormat.format(new Date(travel_allowanceDTO.startDate));

%>
<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    .fix-fill {
        overflow: hidden;
        white-space: nowrap;
    }

    #to-print-div * {
        font-size: 12px;
    }

    #to-print-div h1 {
        font-size: 16px;
        font-weight: bold;
    }

    #to-print-div h2 {
        font-size: 14px;
        font-weight: bold;
    }

    #to-print-div h3 {
        font-size: 13px;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .5in;
        background: white;
        margin-bottom: 10px;
    }

    .page[data-size="A4-landscape"] {
        width: 297mm;
        height: 210mm;
        padding: .1in;
        background: white;
        margin-bottom: 10px;
    }

    .foot-note {
        font-size: 11px !important;
    }

    .blank-to-fill {
        display: inline-block;
        border-bottom: 1px dotted black;
        width: inherit;
    }

    .table-bordered > :not(caption) > * {
        border-color: black;
    }

    .table-bordered > :not(caption) > * > * {
        border-color: black;
    }

    .table-bordered td,
    .table-bordered th {
        padding: 5px;
    }

    .align-top {
        vertical-align: top;
    }

    th {
        text-align: center;
    }
</style>

<div class="kt-content p-0" id="kt_content">

    <div class="kt-portlet">

        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=UtilCharacter.getDataByLanguage(Language, "ভ্রমন ভাতা বিল ফর্ম", "Travel Allowance Bill Form")%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body" id="bill-div">

            <div class="ml-auto m-5">
                <button type="button" class="btn" id='download-pdf'
                        onclick="downloadTemplateAsPdf('to-print-div', '<%=pdfFileName%>');">
                    <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                </button>
            </div>
            <div style="margin: auto;">
                <div class="container" id="to-print-div">
                    <section class="page shadow" data-size="A4-landscape">
                        <div>
                            <div class="row">
                                <div class="offset-3 col-6">
                                    <h4><%=travel_allowanceDTO.officeNameBn%>. ভ্রমণ ব্যয় বিল(নন-গেজেটেড
                                        কর্মচারীগনের), <%=DateUtils.getMonthYear(travel_allowanceDTO.lastModificationTime, "Bangla", ", ")%>
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div>

                            <td class=" div_border attachement-div">
                                <table class="table table-bordered">
                                    <tr>
                                        <th></th>
                                        <th colspan="7">ভ্রমন ও বিরতির বিবরণ</th>
                                        <th>ভ্রমনের প্রকার</th>
                                        <th colspan="3">আকাশ, পথ, রেল-স্টীমারের ভাড়া</th>
                                        <th colspan="3">সড়ক, ট্রলিতে ভ্রমন</th>
                                        <th colspan="3">দৈনিক ভাতা</th>
                                        <th colspan="2">প্রকৃত খরচ</th>
                                        <th rowspan="3">ভ্রমন ও যাত্রা বিরতির উদ্দেশ্য</th>
                                        <th rowspan="3">প্রত্যেক লাইনের সমষ্টি</th>
                                        <th rowspan="3">উপযোজন</th>
                                        <th rowspan="3">মন্তব্য</th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th colspan="4">প্রস্থান</th>
                                        <th colspan="3">আগমন</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>আগমন</th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <th>নাম, পদবী</th>
                                        <th>প্রধান অফিস</th>
                                        <th>স্টেশন</th>
                                        <th>তারিখ</th>
                                        <th>সময়</th>
                                        <th>স্টেশন</th>
                                        <th>তারিখ</th>
                                        <th>সময়</th>
                                        <th>বিমান/রেল/স্টিমার</th>
                                        <th>শ্রেণি</th>
                                        <th>ভাড়ার কতগুণ</th>
                                        <th>টাকার অংক</th>
                                        <th>কত কি,মি,</th>
                                        <th>হার</th>
                                        <th>টাকার অংক</th>
                                        <th>দিনের সংখ্যা</th>
                                        <th>হার</th>
                                        <th>মোট</th>
                                        <th>বিবরণ</th>
                                        <th>টাকার অংক</th>
                                    </tr>
                                    <tr>
                                        <%
                                            for (i = 1; i <= 24; i++) {
                                        %>
                                        <td><%=Utils.getDigits(i, "Bangla")%>
                                        </td>
                                        <%}%>
                                    </tr>
                                    <%
                                        double totalCost = 0;
                                        double totalDays = 0;
                                        List<TravelDestinationDTO> travelDestinationDTOs = travel_allowanceDTO.travelDestinationDTOList;
                                        i = 0;
                                        for (TravelDestinationDTO travelDestinationDTO : travelDestinationDTOs) {
                                            i++;
                                            totalCost += travelDestinationDTO.cost;
                                    %>
                                    <tr>
                                        <% if (i == 1) {
                                        %>
                                        <td><%=travel_allowanceDTO.employeeNameBn%>
                                            ,<%=travel_allowanceDTO.organogramNameBn%>
                                        </td>
                                        <%} else {%>
                                        <td></td>
                                        <%
                                            }%>
                                        <td></td>


                                        <td><%=GeoLocationUtils.getGeoLocationString(startAddress, "Bangla")%>
                                        </td>
                                        <td>
                                            <%=Utils.getDigits(simpleDateFormat.format(new Date(startDate)), "Bangla")%>
                                        </td>
                                        <td>
                                            <%=CatRepository.getInstance().getText("Bangla", "travel_time", startTimeCat)%>
                                        </td>
                                        <td><%=GeoLocationUtils.getGeoLocationString(travelDestinationDTO.destinationAddress, "Bangla")%>
                                        </td>
                                        <td><%
                                            String reachDate = simpleDateFormat.format(new Date(travelDestinationDTO.reachDate));
                                        %>
                                            <%=Utils.getDigits(reachDate, "Bangla")%>
                                        </td>
                                        <td><%=CatRepository.getInstance().getText("Bangla", "travel_time", travelDestinationDTO.timeCat)%>
                                        </td>
                                        <td>
                                            <%=CatRepository.getInstance().getText("Bangla", "travel_medium", travelDestinationDTO.travelMediumCat)%>
                                        </td>

                                        <td>
                                            <%=CatRepository.getInstance().getText("Bangla", "travel_type", travelDestinationDTO.travelTypeCat)%>
                                        </td>
                                        <td>
                                            <%=Utils.getDigits(String.format("%.2f", travelDestinationDTO.distanceOrSubCost), "Bangla")%>
                                            *২
                                        </td>
                                        <td>
                                            <%=Utils.getDigits(String.format("%.2f", travelDestinationDTO.cost), "Bangla")%>
                                        </td>

                                        <td></td>
                                        <td></td>
                                        <td></td>

                                        <td>
                                            <% long diff = travelDestinationDTO.reachDate - startDate;
                                                long days = TimeUnit.MILLISECONDS.toDays(diff);
                                                boolean isHalf = false;
                                                if (startTimeCat != travelDestinationDTO.timeCat) {
                                                    isHalf = true;
                                                    if (startTimeCat == 1) {
                                                        totalDays += 0.5;
                                                    } else {
                                                        totalDays += 0.5;
                                                        days--;
                                                    }
                                                }
                                                totalDays += days;
                                            %>
                                            <%=Utils.getDigits(days, "Bangla")%>
                                            <% if (isHalf) {%>
                                            <%=Utils.getDigits("1/2", "Bangla")%>
                                            <%}%> দিন
                                        </td>
                                        <td></td>
                                        <td>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <%
                                            if (i == 1) {
                                        %>
                                        <td><%=travel_allowanceDTO.purpose%>
                                        </td>
                                        <%} else {%>
                                        <td></td>
                                        <%}%>
                                        <td>
                                        </td>
                                        <td></td>
                                        <td></td>


                                        <%
                                                startAddress = travelDestinationDTO.destinationAddress;
                                                startDate = travelDestinationDTO.reachDate;
                                                startTimeCat = travelDestinationDTO.timeCat;
                                            }%>
                                    </tr>
                                    <tr>
                                        <td colspan="11">সর্বমোট</td>
                                        <td><%=Utils.getDigits(String.format("%.2f", totalCost), "Bangla")%>
                                        </td>
                                        <td colspan="3"></td>
                                        <td><%
                                            int fullDays = (int) totalDays;
                                        %>
                                            <%=Utils.getDigits(fullDays, "Bangla")%>
                                            <%
                                                if (fullDays != totalDays) {
                                            %>
                                            <%=Utils.getDigits("1/2", "Bangla")%>
                                            <%}%> দিন
                                        </td>
                                        <td colspan="6"></td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="12">কর্তন-অবিলকৃত ভ্রমন ব্যয়(অপর পৃষ্ঠায় বিবরণ)<br>
                                            নীট
                                            প্রদেয় <%=Utils.getDigits(String.format("%.2f", totalCost), "Bangla")%>
                                            (কথায়)
                                            &nbsp;<%=BangladeshiNumberInWord.convertToWord(String.valueOf(Utils.getDigits(String.format("%.2f", totalCost), "Bangla")))%>
                                            টাকায় মাত্র।
                                        </td>
                                        <td colspan="10">বাজেট বরাদ্দ</td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" rowspan="2">
                                            স্টেশন ............ ঢাকা ............<br>
                                            তারিখ ...............
                                        </td>
                                        <td colspan="6" rowspan="2">
                                            অফিস প্রধানের সাক্ষর .....................<br>
                                            নাম ................<br>
                                            পদবী ...............<br>

                                        </td>
                                        <td colspan="10">এই বিল সহ ব্যয়</td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="10">অবশিষ্ট</td>
                                        <td colspan="2"></td>
                                    </tr>
                                </table>
                        </div>

                    </section>
                    <section class="page shadow" data-size="A4-landscape">
                        <div>
                            <div class="row col-12">(ক) স্টিমার ছাড়া সমুদ্র বা নদীতে বাষ্পীয় লঞ্চে অথবা অন্য কোনো
                                জলযানে ভ্রমণ, সড়ক পথে ভ্রমণের অন্তর্ভুক্ত হবে। ভ্রমণের নির্দিষ্ট রকম উল্লেখ্য করিতে
                                হইবে।
                            </div>
                            <div class="row col-12">(খ) স্টিমার কোম্পানির ভাড়া দুই রকম - একটিতে খাওয়া অন্তর্ভুক্ত,
                                একটিতে খাওয়া ব্যবস্থা নাই - এইরূপ ক্ষেত্রে ভাড়া শব্দের অর্থ খাওয়া্র ব্যবস্থা ছাড়া
                                ভাড়া বুঝাইবে।
                            </div>
                            <div class="row col-12">গেজেটেড ও নন-গেজেটেড সরকারী কর্মচারীদের জন্য ভ্রমন ভাতার যৌথ
                                বরাদ্দ থাকিলে এই কলামে যৌথ বরাদ্দ ও খরচ দেখাইতে হইবে।
                            </div>
                            <div class="row col-12">(ক) ভ্রমন ব্যয়ের ক্ষেত্রে গেজেটেড ও নন-গেজেটেড কর্মচারীদের
                                ক্ষেত্রেও সংযুক্ত উপযোজন করা হইয়া থাকিলে, সংযুক্ত উপযোজন হিসেবেই উপযোজন কলামে
                                দেখাইতে হইবে।
                            </div>
                            <div class="row col-12">(খ) ভ্রমন ব্যয়ের এই ফর্ম ব্যবহৃত হইলে নিম্নলিখিত কোড ব্যবহার
                                করিতে হইবে।
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    function downloadTemplateAsPdf(divId, fileName, orientation = 'landscape') {
        let content = document.getElementById(divId);
        const opt = {
            margin: 0.5,
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4', orientation: orientation}
        };
        html2pdf().from(content).set(opt).save();
    }
</script>