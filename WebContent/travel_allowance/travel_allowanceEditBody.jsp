<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="employee_records.EmployeeFlatInfoDTO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="travel_allowance.Travel_allowanceDTO" %>
<%@ page import="travel_allowance.Travel_allowanceDAO" %>
<%@ page import="travel_allowance.TravelDestinationDTO" %>
<%@include file="../pb/addInitializer.jsp" %>
<%
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    Travel_allowanceDTO travel_allowanceDTO;
    List<EmployeeFlatInfoDTO> employeeFlatInfoDTOList = new ArrayList<>();
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        travel_allowanceDTO = Travel_allowanceDAO.getInstance().getDTOByID(Long.parseLong(ID));
        employeeFlatInfoDTOList.add(EmployeeFlatInfoDTO.getFlatInfoOfDefaultOffice(
                travel_allowanceDTO.employeeRecordsId, LM.getLanguage(userDTO)
        ));
    } else {
        actionName = "add";
        travel_allowanceDTO = new Travel_allowanceDTO();
    }
    String tableName = "travel_allowance";

    String context = request.getContextPath() + "/";
    long curTime = Calendar.getInstance().getTimeInMillis();
%>
<%
    String formTitle = LM.getText(LC.TRAVEL_ALLOWANCE_ADD_TRAVEL_ALLOWANCE_ADD_FORMNAME, loginDTO);
    String servletName = "Travel_allowanceServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form" id="travelAllowanceForm" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>


                                        <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                               value='<%=travel_allowanceDTO.iD%>' tag='pb_html'/>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.TRAVEL_ALLOWANCE_VIEW_APPLICANT, loginDTO)%><span
                                                    class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <!-- Button trigger modal -->
                                                <button type="button"
                                                        class="btn btn-primary btn-block shadow btn-border-radius"
                                                        id="addToEmployee_modal_button">
                                                    <%=isLanguageEnglish ? "Select Applicant" : "আবেদনকারী বাছাই করুন"%>
                                                </button>
                                                <table class="table table-bordered table-striped">
                                                    <thead></thead>

                                                    <tbody id="tagged_employee_table">
                                                    <tr style="display: none;">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <button type="button" class="btn btn-sm delete-trash-btnbtn"
                                                                    onclick="remove_containing_row(this,'tagged_employee_table');">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <%if (employeeFlatInfoDTOList != null) {%>
                                                    <%for (EmployeeFlatInfoDTO employeeFlatInfoDTO : employeeFlatInfoDTOList) {%>
                                                    <tr>
                                                        <td>
                                                            <%=employeeFlatInfoDTO.employeeUserName%>
                                                        </td>
                                                        <td>
                                                            <%=isLanguageEnglish ? (employeeFlatInfoDTO.officeNameEn + "(" + employeeFlatInfoDTO.organogramNameEn + ")")
                                                                    : (employeeFlatInfoDTO.officeNameBn + "(" + employeeFlatInfoDTO.organogramNameBn + ")")%>
                                                        </td>
                                                        <td>
                                                            <%=isLanguageEnglish ? employeeFlatInfoDTO.employeeNameEn : employeeFlatInfoDTO.employeeNameBn%>
                                                        </td>
                                                        <td id='<%=employeeFlatInfoDTO.employeeRecordsId%>_td_button'>
                                                            <button type="button" class="btn btn-sm delete-trash-btn"
                                                                    onclick="remove_containing_row(this,'tagged_employee_table');">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <%}%>
                                                    <%}%>
                                                    </tbody>
                                                </table>
                                                <input type='hidden' class='form-control' name='employeeRecordsId'
                                                       id='employeeRecordsId' value='' tag='pb_html'/>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_PURPOSE, loginDTO)%>
                                                <span class="required">*</span> </label>
                                            <div class="col-md-8">
                                                <%--                                                <input type='text' class='form-control' name='purpose'--%>
                                                <%--                                                       id='purpose_text_<%=i%>' value='<%=travel_allowanceDTO.purpose%>'--%>
                                                <%--                                                       tag='pb_html'/>--%>
                                                <textarea type='text' class='form-control' name='purpose'
                                                          id='purpose_text' rows="4"
                                                          placeholder="<%=isLanguageEnglish?"Enter travel purpose":"ভ্রমণের উদ্দেশ্য লিখুন"%>"
                                                          onkeyup="updateDescriptionLen()"
                                                          style="text-align: left;resize: none; width: 100%"
                                                          style="text-align: left;resize: none; width: 100%"><%=travel_allowanceDTO.purpose == null ? "" : travel_allowanceDTO.purpose.trim()%></textarea>
                                                <p id="purpose_len"
                                                   style="width: 100%;text-align: right;font-size: small"><%=(travel_allowanceDTO.purpose == null ? "" : travel_allowanceDTO.purpose.trim()).length()%>
                                                    /1024</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_STARTDATE, loginDTO)%>
                                                <span class="required">*</span> </label>
                                            <div class="col-md-8">
                                                <%value = "startDate_js";%>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='startDate' id='startDate_date'
                                                       value='<%=dateFormat.format(new Date(actionName.equalsIgnoreCase("edit")?travel_allowanceDTO.startDate:curTime))%>'
                                                       tag='pb_html'>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_STARTTIMECAT, loginDTO)%>
                                                <span class="required">*</span> </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='startTimeCat'
                                                        id='startTimeCat_category_<%=i%>' tag='pb_html'>
                                                    <%
                                                        Options = CatRepository.getInstance().buildOptions("travel_time", Language, travel_allowanceDTO.startTimeCat);
                                                    %>
                                                    <%=Options%>
                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_ENDDATE, loginDTO)%>
                                                <span class="required">*</span> </label>
                                            <div class="col-md-8">
                                                <%value = "endDate_js";%>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='endDate' id='endDate_date'
                                                       value='<%=dateFormat.format(new Date(actionName.equalsIgnoreCase("edit")?travel_allowanceDTO.endDate:curTime))%>'
                                                       tag='pb_html'>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_ENDTIMECAT, loginDTO)%>
                                                <span class="required">*</span> </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='endTimeCat'
                                                        id='endTimeCat_category_<%=i%>' tag='pb_html'>
                                                    <%
                                                        Options = CatRepository.getInstance().buildOptions("travel_time", Language, travel_allowanceDTO.endTimeCat);
                                                    %>
                                                    <%=Options%>
                                                </select>

                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_STARTADDRESS, loginDTO)%>
                                                <span class="required">*</span> </label>
                                            <div class="col-md-8">
                                                <jsp:include page="/geolocation/geoLocation.jsp">
                                                    <jsp:param name="GEOLOCATION_ID"
                                                               value="startAddress_js"></jsp:param>
                                                    <jsp:param name="LANGUAGE"
                                                               value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type="hidden" id="startAddress"
                                                       name="startAddress">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_TRAVEL_DESTINATION, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped text-nowrap" id="travel-destination-table">
                                <thead>
                                <tr>
                                    <th><%=isLanguageEnglish ? "Arrival Date" : "পৌছানোর তারিখ"%><span
                                            class="required">*</span>
                                    </th>
                                    <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_ENDTIMECAT, loginDTO)%><span
                                            class="required">*</span>
                                    </th>
                                    <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_TRAVEL_DESTINATION_DESTINATIONADDRESS, loginDTO)%><span
                                            class="required">*</span>
                                    </th>
                                    <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_TRAVEL_DESTINATION_TRAVELMEDIUMCAT, loginDTO)%><span
                                            class="required">*</span>
                                    </th>
                                    <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_TRAVEL_DESTINATION_TRAVELTYPECAT, loginDTO)%><span
                                            class="required">*</span>
                                    </th>
                                    <th><%=isLanguageEnglish ? "Sub-cost(Taka)" : "উপ-খরচ(টাকা)"%>
                                        <span
                                                class="required">*</span>
                                    </th>
                                    <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_TRAVEL_DESTINATION_COST, loginDTO)%><span
                                            class="required">*</span>
                                    </th>
                                    <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_TRAVEL_DESTINATION_REMOVE, loginDTO)%>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="field-TravelDestination">


                                <%
                                    if (actionName.equals("edit")) {
                                        int index = -1;


                                        for (TravelDestinationDTO travelDestinationDTO : travel_allowanceDTO.travelDestinationDTOList) {
                                            index++;

                                            System.out.println("index index = " + index);

                                %>

                                <tr id="TravelDestination_<%=index + 1%>">

                                    <td>
                                        <%
                                            value = "date_js_" + childTableStartingID;
                                        %>
                                        <jsp:include page="/date/date.jsp">
                                            <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                            <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                        </jsp:include>
                                        <input type='hidden' name='travelDestination.reachDate'
                                               id='date_date_<%=childTableStartingID%>'
                                               value='<%=travelDestinationDTO.reachDate%>'
                                               tag='pb_html'>
                                    </td>
                                    <td>


                                        <select class='form-control' name='travelDestination.timeCat'
                                                id='timeCat_category_<%=childTableStartingID%>' tag='pb_html'>
                                            <%
                                                Options = CatRepository.getInstance().buildOptions("travel_time", Language, travelDestinationDTO.timeCat);
                                            %>
                                            <%=Options%>
                                        </select>

                                    </td>
                                    <td>
                                            <%
                                            value = "destinationAddress_geolocation_js_" + childTableStartingID;
                                        %>
                                        <jsp:include page="/geolocation/geoLocation.jsp">
                                            <jsp:param name="GEOLOCATION_ID"
                                                       value="<%=value%>"></jsp:param>
                                            <jsp:param name="LANGUAGE"
                                                       value="<%=Language%>"></jsp:param>
                                        </jsp:include>
                                        <input type="hidden"
                                               id="destinationAddress_geolocation_<%=childTableStartingID%>"
                                               name="travelDestination.destinationAddress" tag='pb_html'>
                                    <td>


                                        <select class='form-control' name='travelDestination.travelMediumCat'
                                                id='travelMediumCat_category_<%=childTableStartingID%>' tag='pb_html'>
                                            <%
                                                Options = CatRepository.getInstance().buildOptions("travel_medium", Language, travelDestinationDTO.travelMediumCat);
                                            %>
                                            <%=Options%>
                                        </select>

                                    </td>
                                    <td>
                                        <select class='form-control' name='travelDestination.travelTypeCat'
                                                id='travelTypeCat_category_<%=childTableStartingID%>' tag='pb_html'>
                                            <%
                                                Options = CatRepository.getInstance().buildOptions("travel_type", Language, travelDestinationDTO.travelTypeCat);
                                            %>
                                            <%=Options%>
                                        </select>

                                    </td>
                                    <td>
                                        <input type='text' class='form-control'
                                               name='travelDestination.distanceOrSubCost'
                                               data-only-number="true"
                                               id='distanceOrSubCost_number_<%=childTableStartingID%>'
                                               value='<%=String.format("%.2f", travelDestinationDTO.distanceOrSubCost)%>'
                                               tag='pb_html'>
                                    </td>
                                    <td>
                                        <div id="cost_<%=childTableStartingID%>"
                                             tag='pb_html'><%=String.format("%.2f", travelDestinationDTO.cost)%>
                                        </div>
                                    </td>
                                    <td>
										<span id='chkEdit'>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
                                                   id="field-TravelDestination_cb_<%=index%>"
                                                   class="form-control-sm"/>
										</span>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control'
                                               name='travelDestination.iD'
                                               id='iD_hidden_<%=childTableStartingID%>'
                                               value='<%=travelDestinationDTO.iD%>'
                                               tag='pb_html'/>
                                    </td>
                                </tr>
                                <%
                                            childTableStartingID++;
                                        }
                                    }
                                %>

                                </tbody>
                            </table>
                        </div>
                        <div class="form-group mt-3">
                            <div class="text-right">
                                <button
                                        id="add-more-TravelDestination"
                                        name="add-moreTravelDestination"
                                        type="button"
                                        class="btn btn-sm text-white add-btn shadow">
                                    <i class="fa fa-plus"></i>
                                    <%=LM.getText(LC.HM_ADD, loginDTO)%>
                                </button>
                                <button
                                        id="remove-TravelDestination"
                                        name="removeTravelDestination"
                                        type="button"
                                        class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        </div>

                        <%TravelDestinationDTO travelDestinationDTO = new TravelDestinationDTO();%>

                        <template id="template-TravelDestination">
                            <tr>
                                <td>
                                    <%
                                        value = "date_js_";
                                    %>
                                    <jsp:include page="/date/date.jsp">
                                        <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                        <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                    </jsp:include>
                                    <input type='hidden' name='travelDestination.reachDate' id='date_date_'
                                           value='<%=travelDestinationDTO.reachDate%>'
                                           tag='pb_html'>
                                </td>
                                <td>


                                    <select class='form-control' name='travelDestination.timeCat'
                                            id='timeCat_category_' tag='pb_html'>
                                        <%
                                            Options = CatRepository.getInstance().buildOptions("travel_time", Language, travelDestinationDTO.timeCat);
                                        %>
                                        <%=Options%>
                                    </select>

                                </td>
                                <td>
                                    <jsp:include page="/geolocation/geoLocation.jsp">
                                        <jsp:param name="GEOLOCATION_ID"
                                                   value="destinationAddress_geolocation_js_"></jsp:param>
                                        <jsp:param name="LANGUAGE"
                                                   value="<%=Language%>"></jsp:param>
                                    </jsp:include>
                                    <input type="hidden" id="destinationAddress_geolocation_"
                                           name="travelDestination.destinationAddress" tag='pb_html'>
                                </td>
                                <td>


                                    <select class='form-control' name='travelDestination.travelMediumCat'
                                            id='travelMediumCat_category_' tag='pb_html'>
                                        <%
                                            Options = CatRepository.getInstance().buildOptions("travel_medium", Language, travelDestinationDTO.travelMediumCat);
                                        %>
                                        <%=Options%>
                                    </select>

                                </td>
                                <td>


                                    <select class='form-control' name='travelDestination.travelTypeCat'
                                            id='travelTypeCat_category_' tag='pb_html'>
                                        <%
                                            Options = CatRepository.getInstance().buildOptions("travel_type", Language, travelDestinationDTO.travelTypeCat);
                                        %>
                                        <%=Options%>
                                    </select>

                                </td>
                                <td>
                                    <input type='text' class='form-control' name='travelDestination.distanceOrSubCost'
                                           data-only-number="true"
                                           id='distanceOrSubCost_number_'
                                           value='<%=travelDestinationDTO.distanceOrSubCost%>' tag='pb_html'>
                                </td>
                                <td>
                                    <div id="cost_" tag='pb_html'><%=travelDestinationDTO.cost%>
                                    </div>
                                    <%--                                    <input type='text' class='form-control' name='travelDestination.cost'--%>
                                    <%--                                           id='cost_number_'--%>
                                    <%--                                           data-only-integer="true"--%>
                                    <%--                                           value='<%=travelDestinationDTO.cost%>' tag='pb_html'>--%>
                                </td>
                                <td>
											<span id='chkEdit_'>
												<input type='checkbox' name='checkbox' value='' deletecb='true'
                                                       class="form-control-sm"/>
											</span>
                                </td>

                                <td style="display: none;">
                                    <input type='hidden' class='form-control' name='travelDestination.iD'
                                           id='iD_hidden_'
                                           value='-1' tag='pb_html'/>
                                </td>
                            </tr>

                        </template>
                    </div>
                </div>
                <div class="form-actions text-center mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_TRAVEL_ALLOWANCE_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                            onclick="submitTravelAllowanceForm()">
                        <%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_TRAVEL_ALLOWANCE_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<%@include file="../common/table-sum-utils.jsp" %>
<script type="text/javascript">
    const travelAllowanceForm = $('#travelAllowanceForm');
    const cancelBtn = $('#cancel-btn');
    const submitBtn = $('#submit-btn');
    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';
    var row = 0;


    function isFromValid() {


        const jQueryValid = travelAllowanceForm.valid();
        const fromDateValid = dateValidator('startDate_js', true, {
            'errorEn': '<%=LM.getInstance().getText("English", LC.EMPLOYEE_ACR_ADD_ENTER_VALID_DATE)%>',
            'errorBn': '<%=LM.getInstance().getText("Bangla", LC.EMPLOYEE_ACR_ADD_ENTER_VALID_DATE)%>'
        });
        const toDateValid = dateValidator('endDate_js', true, {
            'errorEn': '<%=LM.getInstance().getText("English", LC.EMPLOYEE_ACR_ADD_ENTER_VALID_DATE)%>',
            'errorBn': '<%=LM.getInstance().getText("Bangla", LC.EMPLOYEE_ACR_ADD_ENTER_VALID_DATE)%>'
        });
        <%--let dateValid = true;--%>
        <%--for (i = 1; i < child_table_extra_id; i++) {--%>


        <%--    dateValid &&= dateValidator('date_js_' + i, true, {--%>
        <%--        'errorEn': '<%=LM.getInstance().getText("English", LC.EMPLOYEE_ACR_ADD_ENTER_VALID_DATE)%>',--%>
        <%--        'errorBn': '<%=LM.getInstance().getText("Bangla", LC.EMPLOYEE_ACR_ADD_ENTER_VALID_DATE)%>'--%>
        <%--    });--%>
        <%--}--%>
        const startAddressValid = geoLocationValidator('startAddress_js');
        return jQueryValid && fromDateValid && toDateValid && startAddressValid;
    }

    function submitTravelAllowanceForm() {

        let added_employee = '';
        if (typeof added_employee_info_map.keys().next().value !== 'undefined') {
            added_employee = added_employee_info_map.keys().next().value;
        }
        document.getElementById('employeeRecordsId').value = added_employee;
        document.getElementById("startDate_date").value = getDateTimestampById('startDate_js');
        document.getElementById("endDate_date").value = getDateTimestampById('endDate_js');
        $('#startAddress').val(getGeoLocation('startAddress_js'));
        for (i = 1; i < child_table_extra_id; i++) {
            if (document.getElementById("date_js_" + i)) {
                console.log(getDateTimestampById('date_js_' + i))
                // preprocessDateBeforeSubmitting('date', i);
                document.getElementById("date_date_" + i).value = getDateTimestampById('date_js_' + i);
            }
            if (document.getElementById("destinationAddress_geolocation_js_" + i)) {
                //preprocessGeolocationBeforeSubmitting('destinationAddress', i, false);
                $('#destinationAddress_geolocation_' + i).val(getGeoLocation('destinationAddress_geolocation_js_' + i));
            }
            if (document.getElementById("isDeleted_checkbox_" + i)) {
                if (document.getElementById("isDeleted_checkbox_" + i).getAttribute("processed") == null) {
                    preprocessCheckBoxBeforeSubmitting('isDeleted', i);
                    document.getElementById("isDeleted_checkbox_" + i).setAttribute("processed", "1");
                }
            }
        }
        let msg = null;


        setButtonState(true);

        let params = travelAllowanceForm.serialize().split('&');
        //console.log(travelAllowanceForm)

        for (let i = 0; i < params.length; i++) {
            let param = params[i].split('=');
            switch (param[0]) {
                case 'employeeRecordsId':
                    if (!param[1]) {
                        msg = isLangEng ? "Employee is not selected" : "কর্মচারী যুক্ত করা হয় নি";
                    }
                    break;
                case 'purpose':
                    if (!param[1]) {
                        msg = isLangEng ? "Travel purpose is missing" : "ভ্রমণের উদ্দেশ্য পাওয়া যায়নি";
                    }
                    break;
                case 'startTimeCat':
                    if (!param[1]) {
                        msg = isLangEng ? "Start time category is not selected" : "ভ্রমণ শুরুর ক্যাটাগরি বাছাই করা হয় নি";
                    }
                    break;
                case 'endTimeCat':
                    if (!param[1]) {
                        msg = isLangEng ? "End time category is not selected" : "ভ্রমণ শেষের ক্যাটাগরি বাছাই করা হয় নি";
                    }
                    break;
                case 'travelDestination.travelTypeCat':
                    if (!param[1]) {
                        msg = isLangEng ? "Travel type category is not selected" : "ভ্রমণ টাইপ ক্যাটাগরি বাছাই করা হয় নি";
                    }
                    break;

                case 'travelDestination.travelMediumCat':
                    if (!param[1]) {
                        msg = isLangEng ? "Travel medium category is not selected" : "ভ্রমণ মাধ্যম ক্যাটাগরি বাছাই করা হয় নি";
                    }
                    break;
                case 'travelDestination.distanceOrSubCost':
                    if (!param[1]) {
                        msg = isLangEng ? "Travel cost is missing" : "ভ্রমণের খরচ পাওয়া যায়নি";
                    }
                    break;

            }
            if (msg) {
                $('#toast_message').css('background-color', '#ff6063');
                showToastSticky(msg, msg);
                setButtonState(false);
                return;
            }
        }
        if (child_table_extra_id == 1) {
            msg = isLangEng ? "Travel destination is missing" : "ভ্রমণের গন্তব্য পাওয়া যায়নি";
            $('#toast_message').css('background-color', '#ff6063');
            showToast(msg, msg);
            setButtonState(false);
            return;
        }
        if (isFromValid()) {
            $.ajax({
                type: "POST",
                url: "Travel_allowanceServlet?actionType=ajax_<%=actionName%>&isPermanentTable=<%=isPermanentTable%>",
                data: travelAllowanceForm.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToast(response.msg, response.msg);
                        setButtonState(false);
                    } else if (response.responseCode === 200) {
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    setButtonState(false);
                }
            });
        } else {
            setButtonState(false);
        }

    }

    function updateDescriptionLen() {
        $('#purpose_len').text($('#purpose_text').val().length + "/1024");
    }

    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Travel_allowanceServlet");
    }

    function setButtonState(value) {
        cancelBtn.prop("disabled", value);
        submitBtn.prop("disabled", value);
    }

    function keyDownEvent(e) {
        let isvalid = inputValidationForFloatValue(e, $(this), 2, null);
        return true == isvalid;
    }

    function init(row) {

        initNumberInput();
        setDateByStringAndId('startDate_js', $('#startDate_date').val());
        setDateByStringAndId('endDate_js', $('#endDate_date').val());
        setGeoLocation('<%=travel_allowanceDTO.startAddress%>', 'startAddress_js');
        <%
        int idx = 1;
        for(TravelDestinationDTO dto:travel_allowanceDTO.travelDestinationDTOList){
        %>
        setGeoLocation('<%=dto.destinationAddress%>', 'destinationAddress_geolocation_js_<%=idx%>');
        // initGeoLocation('destinationAddress_geoSelectField_', i, "Travel_allowanceServlet");
        setDateByTimestampAndId('date_js_<%=idx%>', $('#date_date_<%=idx%>').val());
        <%
        idx++;
        }%>

        for (i = 1; i < child_table_extra_id; i++) {
            $('input#distanceOrSubCost_number_' + i).on('keyup', function () {
                const rowId = this.id.split("_")[2];
                document.getElementById('cost_' + rowId).innerText = (Number(this.value) * 2.0).toFixed(2)
                showSumOfColumnValues('travel-destination-table', 6, null, 2);
            });
        }

        $("#startDate_js").on('datepicker.change', () => {

            setMinDateById("endDate_js", getDateStringById("startDate_js"));

            for (i = 1; i < child_table_extra_id; i++) {
                setMinDateById('date_js_' + i, getDateStringById("startDate_js"));
            }
        });
        $("#endDate_js").on('datepicker.change', () => {
            setMaxDateById("startDate_js", getDateStringById("endDate_js"));

            for (i = 1; i < child_table_extra_id; i++) {
                setMaxDateById('date_js_' + i, getDateStringById("endDate_js"));
            }
        });
        <%
      if(actionName.equals("edit")) {
         %>
        setDateByTimestampAndId('startDate_js', <%=travel_allowanceDTO.startDate%>);
        setDateByTimestampAndId('endDate_js', <%=travel_allowanceDTO.endDate%>);
        setMinDateById("endDate_js", getDateStringById("startDate_js"));
        setMaxDateById("startDate_js", getDateStringById("endDate_js"));
        <%
      if(travel_allowanceDTO.travelDestinationDTOList.size()>0) {
         %>
        initTable();
        <%}
      }else{
                %>
        addRow();

        <%}
        %>
        document.getElementById("field-TravelDestination_cb_0").disabled = true;
        document.getElementById("field-TravelDestination_cb_0").style = "display:none";
    }

    function initTable() {
        const colIndicesToSum = [6];
        const totalTitleColSpan = 6;
        const totalTitle = '<%=LM.getText(LC.BUDGET_SUBTOTAL,loginDTO)%>';
        setupTotalRow('travel-destination-table', totalTitle, totalTitleColSpan, colIndicesToSum, 1, null, 2);
    }

    function initNumberInput() {
        document.querySelectorAll('[data-only-number="true"]')
            .forEach(inputField => inputField.onkeydown = keyDownEvent);
        // document.querySelectorAll('[data-only-integer="true"]')
        //     .forEach(inputField => inputField.oninput = sumThisColumn);
    }


    $(document).ready(function () {
        init(row);
        // CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })

    });

    var child_table_extra_id = <%=childTableStartingID%>;

    function addRow() {
        var t = $("#template-TravelDestination");

        $("#field-TravelDestination").append(t.html());
        SetCheckBoxValues("field-TravelDestination");

        var tr = $("#field-TravelDestination").find("tr:last-child");

        tr.attr("id", "TravelDestination_" + child_table_extra_id);

        tr.find("[tag='pb_html']").each(function (index) {
            var prev_id = $(this).attr('id');
            $(this).attr('id', prev_id + child_table_extra_id);
            //$(this).attr('name', prev_id + child_table_extra_id);
            console.log(index + ": " + $(this).attr('id'));
        });

        $('input#distanceOrSubCost_number_' + child_table_extra_id).on('keyup', function () {
            const rowId = this.id.split("_")[2];
            document.getElementById('cost_' + rowId).innerText = (Number(this.value) * 2.0).toFixed(2);
            showSumOfColumnValues('travel-destination-table', 6, null, 2);
        });

        // $('#cost_number_' + child_table_extra_id).on('change', sumThisColumn);

        select2SingleSelector('#daySelectiondate_js_' + child_table_extra_id, '<%=Language%>');
        select2SingleSelector('#monthSelectiondate_js_' + child_table_extra_id, '<%=Language%>');
        select2SingleSelector('#yearSelectiondate_js_' + child_table_extra_id, '<%=Language%>');

        setMinDateById('date_js_' + child_table_extra_id, getDateStringById("startDate_js"));
        setMaxDateById('date_js_' + child_table_extra_id, getDateStringById("endDate_js"));


        // $("#destinationAddress_geoSelectField_" + child_table_extra_id).attr("row", child_table_extra_id);
        // setGeoLocation('', 'destinationAddress_geolocation_js_' + child_table_extra_id);
        // initGeoLocation('destinationAddress_geoSelectField_', child_table_extra_id, "Travel_allowanceServlet");

        if (child_table_extra_id == 1) {
            initTable()
        }

        child_table_extra_id++;

        initNumberInput();
    }

    $("#add-more-TravelDestination").click(
        function (e) {
            e.preventDefault();
            addRow();
        });


    $("#remove-TravelDestination").click(function (e) {
        var tablename = 'field-TravelDestination';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[deletecb="true"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }
        }
        showSumOfColumnValues("travel-destination-table", 6, null, 2);
    });
    // map to store and send added employee data
    added_employee_info_map = new Map(
        <%if(employeeFlatInfoDTOList != null){%>
        <%=employeeFlatInfoDTOList.stream()
                        .map(detailsDTO -> "['" + detailsDTO.employeeRecordsId + "'," + detailsDTO.getIdsStrInJsonString() + "]")
                        .collect(Collectors.joining(",","[","]"))
        %>
        <%}%>
    );

    table_name_to_collcetion_map = new Map(
        [
            ['tagged_employee_table', {
                info_map: added_employee_info_map,
                isSingleEntry: true
            }],

        ]
    );

    modal_button_dest_table = 'none';

    $('#addToEmployee_modal_button').on('click', function () {
        //alert('CLICKED');
        modal_button_dest_table = 'tagged_employee_table';
        $('#search_emp_modal').modal();
    });


</script>






