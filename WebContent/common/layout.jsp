<%@page import="user.*" %>
<%@page import="org.apache.commons.lang3.ArrayUtils" %>
<%@ page import="util.CommonConstant" %>
<%@ page import="permission.MenuDTO" %>
<%@ page import="permission.MenuRepository" %>
<%@page contentType="text/html;charset=utf-8" %>
<%
    String context = request.getContextPath() + "/";
    System.out.println("context (layout.jsp) : " + context);
    String pluginsContext = context + "assets/global/plugins/";
    request.setAttribute("context", context);
    request.setAttribute("pluginsContext", pluginsContext);
    String tabTitle = request.getParameter("title");
    List<Integer> menuIDPath = (List<Integer>) request.getAttribute("menuIDPath");
    menuIDPath = (menuIDPath == null) ? new ArrayList<Integer>() : menuIDPath;
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = null;

    if (loginDTO.isOisf == 1) {
        userDTO = UserRepository.getUserDTOByOrganogramID(loginDTO.userID);
    } else {
        userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    }

    String pageTitle = "";

    if (menuIDPath.isEmpty()) {
    	System.out.println("menuIDPath.isEmpty() ");
        pageTitle = tabTitle;
        if (pageTitle == null) {
            if (userDTO != null) {
                pageTitle = (LM.getLanguageIDByUserDTO(userDTO) == CommonConstant.Language_ID_English ? "Dashboard" : "ড্যাশবোর্ড");
            } else {
                pageTitle = "Dashboard";
            }
        }
    } else {

        int currentMenuID = menuIDPath.get(menuIDPath.size() - 1);
        System.out.println("currentMenuID = " + currentMenuID);
        MenuDTO currentMenu = MenuRepository.getInstance().getMenuDTOByMenuID(currentMenuID);
        if (userDTO != null) {
            pageTitle = (LM.getLanguageIDByUserDTO(userDTO) == CommonConstant.Language_ID_English ? currentMenu.menuName : currentMenu.menuNameBangla);
            System.out.println("currentMenuID jsp pageTitle= " + pageTitle);
            if(request.getParameter("actionType").equalsIgnoreCase("View"))
            {
            	pageTitle = "View";
            }
            else if(request.getParameter("actionType").equalsIgnoreCase("getEditPage") || request.getParameter("actionType").equalsIgnoreCase("edit"))
            {
            	System.out.println("Setting pagetitle = getEditPage");
            	pageTitle = "Edit";
            }
            
        } else {
            pageTitle = "Dashboard";
        }
    }
    int my_language = LM.getLanguageIDByUserDTO(userDTO) == CommonConstant.Language_ID_English ? 2 : 1;
%>
<!DOCTYPE html>
<html lang="en">
<head>
   

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1">
   
    <title><%=pageTitle %>
    </title>
    <%@ include file="../skeleton/head.jsp" %>

    <%
        String[] cssStr = request.getParameterValues("css");
        for (int i = 0; ArrayUtils.isNotEmpty(cssStr) && i < cssStr.length; i++) {
    %>
    <link href="${context}<%=cssStr[i]%>" rel="stylesheet" type="text/css"/>
    <%}%>

    <script type="text/javascript">
        var context = '${context}';
        var pluginsContext = '${pluginsContext}';
    </script>

</head>
<%
    String fullMenu = "'";

    for (int i = 0; i < menuIDPath.size(); i++) {

        int menuID = menuIDPath.get(i);
        if (i != 0) {
            fullMenu += "','";
        }
        fullMenu += menuID;
    }
    fullMenu += "'";
    int j;
%>
<body style="font-size: 16px !important;"
      class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

<input type="hidden" id="my_language" value="<%=my_language%>"/>
<!-- begin:: Page -->

<%@include file="header_and_body.jsp" %>

<%
    String[] helpers = request.getParameterValues("helpers");
    for (int i = 0; ArrayUtils.isNotEmpty(helpers) && i < helpers.length; i++) {
%>
<jsp:include page="<%=helpers[i] %>" flush="true">
    <jsp:param name="helper" value="<%=i %>"/>
</jsp:include>
<%}%>
<%
    String[] jsStr = request.getParameterValues("js");
    for (int i = 0; ArrayUtils.isNotEmpty(jsStr) && i < jsStr.length; i++) {
%>
<script src="${context}<%=jsStr[i]%>" type="text/javascript"></script>
<%}%>

<div id="toast_message" class="custom-toast">
    <div style="float: right;margin-left: 4px;cursor: pointer"><i class="fa fa-times" onclick="onClickHideToast()"></i>
    </div>
    <div id="toast_message_div"></div>
</div>
<div id="error_message" class="custom-toast">
    <div style="float: right;margin-left: 4px;cursor: pointer"><i class="fa fa-times" onclick="onClickHideToast()"></i>
    </div>
    <div id="error_message_div"></div>
</div>

<script>
    var success_message_bng = 'সম্পন্ন হয়েছে';
    var success_message_eng = 'Success';

    var failed_message_bng = 'ব্যর্থ হয়েছে';
    var failed_message_eng = 'Failed';

    var fill_all_input_bng = "অনুগ্রহ করে সব তথ্য যথাযথ ভাবে দিন";
    var fill_all_input_eng = "Please fill all input correctly";

    var no_data_found_eng = "No data found";
    var no_data_found_bng = "কোন তথ্য পাওয়া যায় নি";

    var inplaceSubmitButton = '<%=LM.getText(LC.GLOBAL_SUBMIT, loginDTO)%>';

    function showToast(bn, en) {
        const x = document.getElementById("toast_message_div");
        x.innerHTML = '';
        const my_language = document.getElementById('my_language').value;

        x.innerHTML = parseInt(my_language) === 1 ? bn : en;
        document.getElementById('toast_message').classList.add("show");

        setTimeout(function () {
            document.getElementById('toast_message').classList.remove("show");
        }, 3000);
    }

    function getContextPath() {
        let contextPathForLayout = '<%=context.replace("../","")%>';
        if (!contextPathForLayout.startsWith("/")) {
            contextPathForLayout = "/" + contextPathForLayout;
        }
        if (!contextPathForLayout.endsWith("/")) {
            contextPathForLayout += "/";
        }
        return contextPathForLayout;
    }

    function showToastSticky(bn, en) {
        const x = document.getElementById("toast_message_div");
        x.innerHTML = '';
        const my_language = document.getElementById('my_language').value;
        x.innerHTML = parseInt(my_language) === 1 ? bn : en;
        document.getElementById('toast_message').classList.add("show-sticky");
    }

    function showError(bn, en) {
        document.getElementById("error_message_div").innerHTML = ('');
        const my_language = document.getElementById('my_language').value;

        document.getElementById("error_message_div").innerHTML = parseInt(my_language) === 1 ? bn : en;
        document.getElementById('error_message').classList.add("show");

        setTimeout(function () {
            document.getElementById('error_message').classList.remove("show");
        }, 3000);
    }

    function getLanguageBaseText(en, bn) {
        const my_language = document.getElementById('my_language').value;
        return parseInt(my_language) === 1 ? (bn == undefined || bn == null ? "পাওয়া যায় নি" : bn) : (en == undefined || en == null ? "Not found" : en);
    }

    function onClickHideToast() {
        document.getElementById('toast_message').classList.remove("show");
        document.getElementById('toast_message').classList.remove("show-sticky");
        document.getElementById('error_message').classList.remove("show");
    }
</script>

<script>
    var global_my_language = '1';

    function isNullOrNegativeValue(arr) {
        for (let i = 0; i < arr.length; i++) {
            if (arr[i] === undefined || arr[i] === null || arr[i] == '' || arr[i].length == 0 || arr[i] == '-1') return true;
        }
        return false;
    }

    function optional_select(op) {
        const my_language = document.getElementById('my_language').value;
        op = new Option(my_language == '2' ? "-- Please select one --" : "-- বাছাই করুন --", '-1');
        select_2_call();
    }

    $(document).ready(function () {
        global_my_language = document.getElementById('my_language').value;
        /*$("select").select2({
            dropdownAutoWidth: true,
            placeholder: my_language == '2' ? "-- Please select one --" : "-- বাছাই করুন --"
        });*/

        activateMenu();
        if($(document).prop('title') == 'View')
        {
        	if($(".table-title").length)
        	{
    			console.log("table-title found");
    			$(document).prop('title', $(".table-title").html());
        	}
    		else if ($("div .sub_title").length)
   			{
    			$(document).prop('title',$("div .sub_title > h4").html());
   			}
        	
        }
        
        if($(document).prop('title') == 'Edit User' || $(document).prop('title') == 'Edit')
        {
        	if ($("div .sub_title").length)
   			{
    			$(document).prop('title',$("div .sub_title > h4").html());
   			}
        	
        }
        
        /*if($(".kt-subheader__title").length)
        {
        	$(document).prop('title', $(".kt-subheader__title").html().replace("&nbsp;",""));
        	
        }*/
        
        
        
    });

    function getDataTable() {
        $('#tableData').DataTable({
            dom: 'B',
            "ordering": false,
            buttons: [
                {
                    extend: 'excel',
                    charset: 'UTF-8'
                }, {
                    extend: 'print',
                    charset: 'UTF-8'
                }
            ]
        });
        document.getElementById('tableData_wrapper').classList.add("pull-right");
        document.getElementById('tableData').classList.remove("no-footer");
    }

    var fullMenu = [<%=fullMenu%>];

</script>

<script src="<%=context%>ek_sheba_common/ek_sheba_commom_js.js" type="text/javascript"></script>
<script src="<%=context%>ek_sheba_common/ek_sheba_geo.js" type="text/javascript"></script>

<style>
    .select2-container--bootstrap .select2-selection--single .select2-selection__rendered {
        padding-top: 7px;
    }

    .custom-toast {
        visibility: hidden;
        width: 300px;
        min-height: 60px;
        color: #fff;
        text-align: center;
        padding: 20px 30px;
        position: fixed;
        z-index: 1;
        right: 23px;
        bottom: 60px;
        font-size: 13pt;
    }

    #toast_message {
        background-color: #008aa6;
    }

    #error_message {
        background-color: #ca5e59;
    }

    #toast_message.show {
        visibility: visible;
        -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }

    #toast_message.show-sticky {
        visibility: visible;
        -webkit-animation: fadein 0.5s;
        animation: fadein 0.5s;
    }

    #error_message.show {
        visibility: visible;
        -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }

    @-webkit-keyframes fadein {
        from {
            right: -38%;
            opacity: 0;
        }
        to {
            right: 23px;
            opacity: 1;
        }
    }

    @keyframes fadein {
        from {
            right: -38%;
            opacity: 0;
        }
        to {
            right: 23px;
            opacity: 1;
        }
    }

    @-webkit-keyframes fadeout {
        from {
            right: 23px;
            opacity: 1;
        }
        to {
            right: -38%;
            opacity: 0;
        }
    }

    @keyframes fadeout {
        from {
            right: 23px;
            opacity: 1;
        }
        to {
            right: -38%;
            opacity: 0;
        }
    }

    .modal {
    / / position: absolute;
        float: left;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        width: 100%;
        height: 100%;
    }

    .hiddenOtherField{
        display: none;
    }

</style>
</body>
</html>


