String.prototype.format = function () {
    let a = this;
    for (let i = 0; i < arguments.length; i++) {
        a = a.replace("{" + i + "}", arguments[i])
    }
    return a
};

const maxLengthErrorBng = "{0} এর দৈর্ঘ্য {1} এর বেশি হওয়া উচিত নয়";
const maxLengthErrorEng = "{0} length should not more than {1}";

const minLengthErrorBng = "{0} এর দৈর্ঘ্য {1} এর কম হওয়া উচিত নয়";
const minLengthErrorEng = "{0} length should not smaller than {1}";

const maxValueErrorBng = "{0} এর মান {1} এর বেশি হওয়া উচিত নয়";
const maxValueErrorEng = "{0} value should not more than {1}";

const minValueErrorBng = "{0} এর মান {1} এর কম হওয়া উচিত নয়";
const minValueErrorEng = "{0} value should not smaller than {1}";

const requiredErrorBng = "{0}, অবশ্যক, অনুগ্রহ করে পূরণ করুন";
const requiredErrorEng = "{0}, is required, please fill properly";

const integerErrorBng = "{0}, পূর্ণসংখ্যা মান প্রদান করুন";
const integerErrorEng = "{0}, Please provide integer value";

const bbsCodeErrorBng = "কোড টি আগে থেকেই আছে, অনুগ্রহ করে আবার দিন";
const bbsCodeErrorEng = "This BBS Code already exist, please provide a new one";

const duplicationErrorBng = "{0}, আগে থেকেই আছে, অনুগ্রহ করে আবার দিন";
const duplicationErrorEng = "{0} already exist, please provide a new one";

const dropdownErrorBng = "ড্রপডাউন থেকে {0} নির্বাচন করুন";
const dropdownErrorEng = "Select {0} from dropdown";

const deleteSuccessBng = "ডিলিট সম্পন্ন হয়েছে";
const deleteSuccessEng = "Deleted";

const deleteFailedBng = "ডিলিট সম্পন্ন হয়নি";
const deleteFailedEng = "Can't Delete";