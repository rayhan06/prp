<%@page import="language.LC" %>
<%@page import="language.LM" %>
<div class="portlet box portlet-btcl light" style="border-top:none; box-shadow:unset !important; display: none">

    <div class="row">
        <div class="col-lg-offset-5 col-xs-12 col-sm-12 col-md-12 col-lg-7">
            <div class="container-fluid">
                <div class="row pull-right">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-5 custom-align-content desktop-only"
                         style="margin-bottom: 5px; display: none">
                        <div class="row" style="display: none;">
                            <div class="col-xs-5 col-sm-4 col-md-4 col-lg-7" style="display: none;">
                                <label style="font-size: 15px;margin-top: 5px;"><%=LM.getText(LC.GLOBAL_RECORD_PER_PAGE, loginDTO)%>
                                </label>
                            </div>
                            <div class="col-xs-4 col-sm-8 col-md-8 col-lg-5" style="display: none;">
                                <input onclick="pageNumberChange()" type="text" class="form-control"
                                       name="RECORDS_PER_PAGE" id="pagenumber"
                                       placeholder="" value="<%=rn.getPageSize()%>">
                            </div>
                        </div>

                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                        <nav aria-label="Page navigation">
                            <ul class="pagination" style="margin: -1px 0px 0px 5px;">
                                <li class="page-item" style="display:table-cell !important;">
                                    <a class="page-link" onclick="next('first')"
                                       aria-label="First"
                                       title="Left">
                                        <i class="fa fa-angle-double-left" aria-hidden="true"
                                           style="padding:3.5px"></i>
                                    </a>
                                </li>
                                <li class="page-item" style="display:table-cell !important;">
                                    <a class="page-link" onclick="next('previous')"
                                       aria-label="Previous" title="Previous">
                                        <i class="fa fa-angle-left" aria-hidden="true" style="padding:3.5px"></i>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>

                                <li class="page-item" style="display:table-cell !important;">
                                    <a class="page-link" onclick="next('next')" aria-label="Next"
                                       title="Next">
                                        <i class="fa fa-angle-right" aria-hidden="true" style="padding:3.5px"></i>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                                <li class="page-item" style="display:table-cell !important;">
                                    <a class="page-link" onclick="next('last') " aria-label=" Last"
                                       title="Last">
                                        <i class="fa fa-angle-double-right" aria-hidden="true"
                                           style="padding:3.5px"></i>
                                        <span class="sr-only">Last</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>

                    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4" style="left:25px">
                        <i><%=LM.getText(LC.GLOBAL_PAGE, loginDTO) %>
                        </i>
                        <input type="text" name="pageno" id="pageno" value='<%=pageno%>' size="15"
                               class='custom-from-control' style="height:32px !important"> <i
                            class="hidden-xs"><%=LM.getText(LC.GLOBAL_OF, loginDTO) %>
                    </i>
                        <i id='totalpage desktop-only'>
                            <%=rn.getTotalPages()%>
                        </i>

                        <input type="hidden" name="go" value="yes"/>
                        <input type="hidden" name="mode" value="search"/>
                        <input type="submit" class="btn btn-sm green-haze btn-outline sbold uppercase"
                               value="<%=LM.getText(LC.GLOBAL_GO, loginDTO)%>"
                               style="height: 32px;margin-bottom: 3px;"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%@ page import="java.util.List" %>
<div style="background: #fff;">
    <div style="display: inline; float:left; padding-right: 10px;">
        <%
            int total22 = rn.getTotalRecords();
            int pageSize44 = rn.getPageSize();
            int pageNo66 = rn.getCurrentPageNo();

            int start77 = (pageNo66 - 1) * pageSize44 + 1;
            int end99 = start77 + pageSize44 - 1;
        %>
        <label style="padding-top: 10px">Showing <%=start77%> to <%=end99%> of <%=total22%> entries</label>
    </div>

    <div class="pagination" style="display: inline;">
        <a onclick="next('first')" class="action_button">&laquo;</a>
        <%
            List<Integer> paginationPages55 = new ArrayList<Integer>();
            int totalPage33 = rn.getTotalPages() > rn.getM_numberOfPageCountToShow() ? rn.getM_numberOfPageCountToShow() : rn.getTotalPages();

            if (totalPage33 > 5) {
                paginationPages55.add(1);
                if (rn.getCurrentPageNo() == 1) {
                    if (!paginationPages55.contains(1 + 1) && 1 + 1 < totalPage33) paginationPages55.add(1 + 1);
                }

                for (int i = 1 + 1; i <= totalPage33 - 1; i++) {
                    if (paginationPages55.contains(i)) {
                        continue;
                    } else if (i == rn.getCurrentPageNo()) {
                        if (!paginationPages55.contains(i - 1) && i - 1 > 0) paginationPages55.add(i - 1);
                        paginationPages55.add(i);
                        if (!paginationPages55.contains(i + 1) && i + 1 < totalPage33) paginationPages55.add(i + 1);
                    } else {
                        paginationPages55.add(0);
                    }
                }

                if (!paginationPages55.contains(totalPage33)) {
                    if (rn.getCurrentPageNo() == totalPage33) {
                        if (!paginationPages55.contains(totalPage33 - 1) && totalPage33 - 1 > 0)
                            paginationPages55.add(totalPage33 - 1);
                    }
                    paginationPages55.add(totalPage33);
                }
            } else {
                for (int pageIndex = 1; pageIndex <= totalPage33; pageIndex++) {
                    paginationPages55.add(pageIndex);
                }
            }

            for (int i = 0; i < paginationPages55.size(); i++) {
                int pageIndex = paginationPages55.get(i);
                if (pageIndex == rn.getCurrentPageNo()) {
        %>
        <a style="background-color: #337ab7;color: #ffffff;" onclick="next('current', <%=pageIndex%>)"
           class="form-inline"><%=pageIndex%>
        </a>

        <%
        } else if (pageIndex == 0) {
            while (paginationPages55.get(i) == 0) i++;
            i--;

        %>
        <div>
            <label class="form-inline">
                <input type="submit" value=".">
            </label>
        </div>
        <%

        } else {
        %>
        <a onclick="next('current', <%=pageIndex%>)" class="action_button form-inline"><%=pageIndex%>
        </a>
        <%
                }
            }
        %>
        <%--    <a href="<%=link%><%=concat%>pageNumber=1" class="active">1</a>--%>

        <a onclick="next('last', )" class="action_button">&raquo;</a>
    </div>
</div>

<input type="hidden" id="pagenumber" value="20"/>

<style>
    .pagination .form-inline {
        float: left;
    }

    .pagination input[type="submit"] {
        text-align: center;
        background-color: #dddddd;
        border: none;
        padding: 8px 14px;
        margin-right: 5px;
        color: black;
    }

    .pagination input[type="submit"]:hover,
    .pagination input[type="submit"].active {
        background-color: #337ab7;
        color: #ffffff;
    }

    .pagination .action_button {
        margin-right: 5px;
    }

    .pagination::after {
        content: " ";
        display: table;
        clear: both;
    }
</style>