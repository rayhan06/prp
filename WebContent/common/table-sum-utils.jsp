<%@page pageEncoding="UTF-8" %>

<script>
    const digitsByLanguage = {
        "bangla": ['০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯'],
        "english": ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    }

    function convertString(numStr, fromLanguage, toLanguage) {
        numStr = String(numStr);
        return [...numStr].map(char => convertCharacter(char, fromLanguage, toLanguage))
            .join('');
    }

    function convertCharacter(char, fromLanguage, toLanguage) {
        const digitIndex = digitsByLanguage[fromLanguage].indexOf(char);
        if (digitIndex < 0) return char;
        return digitsByLanguage[toLanguage][digitIndex];
    }

    function getTotalColId(tableId, columnIndex) {
        return tableId + '-total-col-' + columnIndex;
    }

    function getTotal(tableId, columnIndex) {
        const cellValue = extractCellValue(
            document.getElementById(getTotalColId(tableId, columnIndex))
        );
        return Number(convertString(cellValue, 'bangla', 'english'));
    }

    function clearFooter(tableId) {
        const table = document.getElementById(tableId);
        table.querySelectorAll('tfoot').forEach(tfoot => tfoot.remove());
    }

    function addTotalRowInTfoot(tableId, totalTitle, totalTitleColSpan, colToIgnore) {
        const table = document.getElementById(tableId);
        let nCols = table.querySelector('tbody').rows[0].cells.length;

        if (colToIgnore !== null && colToIgnore !== undefined) {
            nCols -= colToIgnore
        }
        const titleRow = document.createElement('tr');

        const titleTd = document.createElement('td');
        titleTd.colSpan = totalTitleColSpan;
        titleTd.innerText = totalTitle;
        titleTd.style.fontWeight = 'bold';

        titleRow.append(titleTd);

        for (let columnIndex = totalTitleColSpan; columnIndex < nCols; columnIndex++) {
            const td = document.createElement('td');
            td.id = getTotalColId(tableId, columnIndex);
            titleRow.append(td);
        }

        const tableFoot = document.createElement('tfoot');
        tableFoot.innerHTML = '';
        tableFoot.append(titleRow);

        table.querySelectorAll('tfoot').forEach(tfoot => tfoot.remove());
        table.append(tableFoot);
    }

    function getIfValidOrOther(str, other) {
        str = convertString(str.trim(), "bangla", "english");
        const num = Number(str);
        return isNaN(num) ? other : num;
    }

    function extractCellValue(cellElement) {
        if (!cellElement) return 0;
        const input = cellElement.querySelector('input');
        const val = input ? input.value : cellElement.innerText;
        return getIfValidOrOther(val, 0);
    }

    function showSumOfFixedValue(sumValue, tableId, columnIndex, language, afterDecimal) {
        const totalCol = document.getElementById(
            getTotalColId(tableId, columnIndex)
        );
        if (language && language.toLowerCase() === 'bangla')
            sumValue = convertString(sumValue, 'english', 'bangla');
        totalCol.innerText = sumValue;
        if (afterDecimal) {
            totalCol.innerText = sumValue.toFixed(afterDecimal);
        }
    }

    function showSumOfColumnValues(tableId, columnIndex, language, afterDecimal) {
        const tableBody = document.querySelector('#' + tableId + " tbody");
        const columnCells = [...tableBody.rows].map(row => row.cells[columnIndex]);

        let columnsValueSum = columnCells.reduce((acc, cell) => acc + extractCellValue(cell), 0);

        const totalCol = document.getElementById(
            getTotalColId(tableId, columnIndex)
        );
        if (language && language.toLowerCase() === 'bangla')
            columnsValueSum = convertString(columnsValueSum, 'english', 'bangla');

        totalCol.innerText = columnsValueSum;
        if (afterDecimal) {
            totalCol.innerText = columnsValueSum.toFixed(afterDecimal);
        }
    }

    function sumThisColumn() {
        const tableId = this.closest('table').id;
        const columnIndex = this.closest('td').cellIndex;
        showSumOfColumnValues(tableId, columnIndex);
    }

    function sumInputElementColumn(inputElement, afterDecimal, language) {
        const tableId = inputElement.closest('table').id;
        const columnIndex = inputElement.closest('td').cellIndex;
        showSumOfColumnValues(tableId, columnIndex, language, afterDecimal);
    }

    function setupTotalRow(tableId, totalTitle, totalTitleColSpan, colIndicesToSum, colToIgnore, language, afterDecimal) {
        addTotalRowInTfoot(tableId, totalTitle, totalTitleColSpan, colToIgnore);
        colIndicesToSum.forEach(columnIndex => showSumOfColumnValues(tableId, columnIndex, language, afterDecimal));
    }

    function setupTotalRowWithKnownValue(sumValue, tableId, columnIndex, totalTitle, totalTitleColSpan, colToIgnore, language, afterDecimal) {
        addTotalRowInTfoot(tableId, totalTitle, totalTitleColSpan, colToIgnore);
        showSumOfFixedValue(sumValue, tableId, columnIndex, language, afterDecimal);
    }

    function setupTotalRowWithKnownValues(sumValues, tableId, columnIndices, totalTitle, totalTitleColSpan, colToIgnore, language, afterDecimal) {
        addTotalRowInTfoot(tableId, totalTitle, totalTitleColSpan, colToIgnore);
        for(let i = 0;i < columnIndices.length; ++i) {
            showSumOfFixedValue(sumValues[i], tableId, columnIndices[i], language, afterDecimal);
        }
    }
</script>