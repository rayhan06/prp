<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="common.NameDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@ page import="util.HttpRequestUtils" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    RecordNavigator rn2 = (RecordNavigator) request.getAttribute("recordNavigator");
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
%>
<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><%=LM.getText(LC.RESULT_EXAM_EDIT_NAMEBN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.RESULT_EXAM_EDIT_NAMEEN, loginDTO)%>
            </th>
            <th style="width: 100px"><%=LM.getText(LC.RESULT_EXAM_SEARCH_RESULT_EXAM_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="text-center" style="width: 25px">
                <span><%="English".equalsIgnoreCase(Language)?"All":"সকল"%></span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            List<NameDTO> data = (List<NameDTO>) rn2.list;
            if (data != null && data.size()>0) {
                for (NameDTO nameDTO : data) {
        %>
        <tr>
            <td>
                <%=StringEscapeUtils.escapeHtml4(nameDTO.nameBn)%>
            </td>

            <td>
                <%=StringEscapeUtils.escapeHtml4(nameDTO.nameEn)%>
            </td>

            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ff6b6b;"
                        onclick="location.href='<%=(String)request.getAttribute("servletName")%>?actionType=getEditPage&ID=<%=nameDTO.iD%>'">
                    <i class="fa fa-edit"></i>
                </button>
            </td>

            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID' value='<%=nameDTO.iD%>'/></span>
                </div>
            </td>
        </tr>
        <% } } %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>