<!DOCTYPE html>
<%@page import="user.*"%>
<%@page import="org.apache.commons.lang3.ArrayUtils"%>
<%@ page import="dashboard.DashboardDTO" %>
<%@ page import="dashboard.DashboardService" %>
<%@page contentType="text/html;charset=utf-8" %>
<%
	String context = "../../.."  + request.getContextPath() + "/";
	String pluginsContext = context +"assets/global/plugins/";   
    request.setAttribute("context", context);
    request.setAttribute("pluginsContext",pluginsContext);   

	List<Integer> menuIDPath = (List<Integer>)request.getAttribute("menuIDPath");
	if(menuIDPath == null){
		menuIDPath = new ArrayList<Integer>();
	}
	
	LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	
	UserDTO userDTO = null;
	userDTO = UserRepository.getUserDTOByUserID(loginDTO);

	String pageTitle="";
	
	if(menuIDPath.isEmpty()){
		pageTitle = request.getRequestURL().toString();
		if(userDTO != null){
			pageTitle=(LM.getLanguageIDByUserDTO(userDTO)==CommonConstant.Language_ID_English?"Dashboard":"ড্যাশবোর্ড");
		}
		else{
			pageTitle="Dashboard";
		}
	}else{

		int currentMenuID = menuIDPath.get(menuIDPath.size()-1);
		MenuDTO currentMenu = MenuRepository.getInstance().getMenuDTOByMenuID(currentMenuID);
		
		if(userDTO != null){
			pageTitle=(LM.getLanguageIDByUserDTO(userDTO)==CommonConstant.Language_ID_English?currentMenu.menuName:currentMenu.menuNameBangla);
		}
		else{
			pageTitle="Dashboard";
		}
	}

	DashboardService dashboardService = new DashboardService();
	dashboardService.Language = LM.getText(LC.SUPPORT_TICKET_EDIT_LANGUAGE, loginDTO);
	

	if( userDTO.roleID == 1L && userDTO.unitID == 78040L ){
		request.setAttribute( "isAdmin", "true" );
	}
	else{
		request.setAttribute( "isAdmin", "false" );
	}
	
	Boolean dontReplaceDashboardDTO = (Boolean)request.getAttribute("dontReplaceDashboardDTO");
	if(dontReplaceDashboardDTO == null)
	{
		System.out.println("#############################Getting dashboard##############################");
		DashboardDTO dashboardDTO = dashboardService.getDashboardDTOByUserDTO( userDTO );
		request.setAttribute( "dashboardDTO", dashboardDTO );
	}
	
%>
<html lang="en">
	<head>


		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1">

		<title><%=pageTitle %></title>
		<%@ include file="../skeleton/head.jsp"%>

		<%String[] cssStr = request.getParameterValues("css");
		for(int i = 0; ArrayUtils.isNotEmpty(cssStr) &&i < cssStr.length;i++){%>
			<link href="${context}<%=cssStr[i]%>" rel="stylesheet" type="text/css" />
		<%}%>
	</head>
<%
	String  fullMenu="'";
	for(int i = 0;i<menuIDPath.size();i++){

		int menuID = menuIDPath.get(i);
		if(i!=0){
			fullMenu+="','";
		}
		fullMenu+=menuID;
	}
	fullMenu+="'";
	int j;
%>

	<body style="font-size: 14px !important;" class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
		<div id="fakeLoader"></div>

		<%@include file="header_and_body.jsp"%>

		<%
			String[] helpers = request.getParameterValues("helpers");
			for(int i = 0; ArrayUtils.isNotEmpty(helpers)&& i < helpers.length;i++){
		%>
			<jsp:include page="<%=helpers[i] %>" flush="true">
				<jsp:param name="helper" value="<%=i %>" />
			</jsp:include>
		<%}%>
		<%
		String[] jsStr = request.getParameterValues("js");
		for(int i = 0; ArrayUtils.isNotEmpty(jsStr)&& i < jsStr.length;i++) {
		%>
			<script src="${context}<%=jsStr[i]%>" type="text/javascript"></script>
		<%}%>
	</body>
</html>