<%@ page import="marquee_text.Marquee_textDTO" %>
<%@ page import="marquee_text.Marquee_textRepository" %>
<%@ page import="util.HttpRequestUtils" %>
<!-- begin:: Header Mobile -->
<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
    <div class="kt-header-mobile__logo">
        <a href="<%=context%>Welcome.do">
            <img src="<%=context%>assets/static/prplogo.png" alt="logo" class="logo-default" style="height: 50px; margin: 0px !important" />
        </a>
    </div>
    <div class="kt-header-mobile__toolbar">
        <button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
        <button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
    </div>
</div>
<!-- end:: Header Mobile -->

<style>
    .marquee-after-login li a {
        font-size: 125%;
        color: tomato;
    }

    .marquee-after-login li a::before {
        content: '*';
        color: tomato;
        font-size: 125%;
        margin-right: 7px;
    }

    .notification-text {
        color: #fff;
        display: flex;
        align-items: flex-start;
    }

    .notification-text:hover, .notification-text:focus {
        color: #e0e0e0;
        text-decoration: none;
        transition: .3s;
    }
</style>

<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <%@ include file="../skeleton/menu.jsp"%>

        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <%@ include file="../skeleton/header.jsp"%>

            <!-- end:: Header -->
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

                <%
                    List<Marquee_textDTO> visibleMarqueeTextDTOs = Marquee_textRepository.getInstance().getVisibleDtoList(System.currentTimeMillis());
                    if(!visibleMarqueeTextDTOs.isEmpty()) {
                %>
                <marquee class="marquee-after-login" onMouseOver="this.stop()" onMouseOut="this.start()">
                    <ul class="d-flex list-unstyled mt-3 mb-0">
                        <%for(Marquee_textDTO visibleMarqueeTextDTO : visibleMarqueeTextDTOs){%>
                        <li class="mr-5">
                            <a class="notification-text" href="<%=visibleMarqueeTextDTO.getUrl()%>">
                                <%=visibleMarqueeTextDTO.getText(isLangEng)%>
                            </a>
                        </li>
                        <%}%>
                    </ul>
                </marquee>
                <%}%>

                <jsp:include page='<%=request.getParameter("body")%>' />
            </div>

            <%@ include file="../skeleton/footer.jsp"%>

        </div>

    </div>
</div>

<%@ include file="../skeleton/includes.jsp"%>