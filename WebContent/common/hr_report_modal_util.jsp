<%@page pageEncoding="UTF-8" %>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<%
    String modalTitle = Language.equalsIgnoreCase("English") ? "Find Designation" : "পদবী খুঁজুন";
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
    <jsp:param name="modalTitle" value="<%=modalTitle%>"/>
</jsp:include>

<script>
    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInTags,
            isMultiSelect: true,
            keepLastSelectState: true
        }]
    ]);

    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function officeModalEditButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        officeSearchSetSelectedOfficeLayers($('#office_units_id_input').val());
        $('#search_office_modal').modal();
    }

    function viewOgranogramIdInInput(empInfo) {
        console.log(empInfo);

        $('#organogram_id_modal_button').hide();
        $('#organogram_id_div').show();

        let language = '<%=Language.toLowerCase()%>'
        let designation;
        if (language === 'english') {
            designation = empInfo.organogramNameEn + ', ' + empInfo.officeUnitNameEn;
        } else {
            designation = empInfo.organogramNameBn + ', ' + empInfo.officeUnitNameBn;
        }
        document.getElementById('organogram_id_text').innerHTML = designation;
        $('#organogram_id_input').val(empInfo.organogramId);
    }

    table_name_to_collcetion_map = new Map([
        ['organogramId', {
            isSingleEntry: true,
            callBackFunction: viewOgranogramIdInInput
        }]
    ]);

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    function organogramIdModalBtnClicked() {
        modal_button_dest_table = 'organogramId';
        $('#search_emp_modal').modal();
    }
</script>