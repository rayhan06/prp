const property_key = [
    "duplicate"
];

class DataValidation {

    static max_length(maxLength, model, property, propertyName) {
        const realLength = model[property].length;

        if (realLength > maxLength) {
            showError(maxLengthErrorBng.format(propertyName, maxLength), maxLengthErrorEng.format(propertyName, maxLength));
            return false;
        }
        return true;
    }

    static min_length(minLength, model, property, propertyName) {
        const realLength = model[property].length;

        if (realLength < minLength) {
            showError(minLengthErrorBng.format(propertyName, minLength), minLengthErrorEng.format(propertyName, minLength));
            return false;
        }
        return true;
    }

    static max_value(maxValue, model, property, propertyName) {
        const realValue = parseInt(model[property]);

        if (realValue > maxValue) {
            showError(maxValueErrorBng.format(propertyName, maxValue), maxValueErrorEng.format(propertyName, maxValue));
            return false;
        }
        return true;
    }

    static min_value(minLength, model, property, propertyName) {
        const realLength = model[property].length;

        if (realLength < minLength) {
            showError(minValueErrorBng.format(propertyName, minLength), minValueErrorEng.format(propertyName, minLength));
            return false;
        }
        return true;
    }

    static required(isrequired, model, property, propertyName) {
        if (isrequired === false) return true;

        if (model[property] === undefined || model[property] === null || model[property].length === 0) {
            showError(requiredErrorBng.format(propertyName), requiredErrorEng.format(propertyName));
            return false;
        }
        return true;
    }

    static is_integer(isInteger, model, property, propertyName) {
        if (isInteger === false) return true;

        const value = parseInt(model[property]);

        if (typeof(value) !== 'number' || Math.floor(value) !== value) {
            showError(integerErrorBng.format(propertyName), integerErrorEng.format(propertyName));
            return false;
        }
        return true;
    }

    static from_dropdown(isFromDropdown, model, property, propertyName) {
        if (isFromDropdown === false) return true;

        const value = parseInt(model[property]);

        if (typeof(value) !== 'number' || Math.floor(value) !== value || Math.floor(value) <= 0) {
            showError(dropdownErrorBng.format(propertyName), dropdownErrorEng.format(propertyName));
            return false;
        }
        return true;
    }

    static duplicate(servlet_info, model, property, propertyName) {

        const parameter = DataValidation.getParameterToCheckDuplication(servlet_info, model, property);

        const xhttp = new XMLHttpRequest();
        xhttp.open("GET", parameter, false);
        xhttp.send();

        if (xhttp.readyState === 4 && xhttp.status === 200) {
            console.log(xhttp.responseText);
            if (xhttp.responseText === 'true') {
                showError(duplicationErrorBng.format(propertyName), duplicationErrorEng.format(propertyName));
                return false;
            } else {
                return true;
            }
        }
    }

    static getParameterToCheckDuplication(servlet_info, model, property) {
        const keys = Object.keys(servlet_info);

        let parameter = servlet_info[keys[0]];
        for (let i = 1; i < keys.length; i++) {
            const key = keys[i];
            const val = servlet_info[key];

            if (val === null) continue;
            parameter += (key + "=" + val + "&");
        }
        parameter += ("iD" + "=" + model["iD"] + "&");
        parameter += (property + "=" + model[property]);

        return parameter;
    }
}