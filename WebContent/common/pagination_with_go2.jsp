<%@page import="language.LC" %>
<%@page import="language.LM" %>

<div class="kt-portlet my-4">
    <div class="kt-portlet__body py-0 pr-2">
        <div class="d-flex justify-content-end align-items-center flex-wrap">
            <div class="d-flex flex-column flex-sm-row align-items-center py-3">
            	<div class="d-flex align-items-center">
                    <span class="smsize">
                   	<%=LM.getText(LC.CENTRE_REPORT_OTHER_TOTAL, loginDTO)%> <%=LM.getText(LC.HM_COUNT, loginDTO)%>
                	</span>
                    <input
                            style="border: 1px solid #D3D3D3; width: 70px; height:30px !important; border-radius: 6px"
                            
                            type="text"
                            class="onlyBanglaAndEnglishDigitOnly form-control text-center ml-2"
                            readoonly
                          	name = "trData"
                            placeholder=""
                            value="<%=rn.getTotalRecords()%>"
                    >
                </div>
                <div class="d-flex align-items-center">
                    <span class="smsize">
                    <%=LM.getText(LC.GLOBAL_RECORD_PER_PAGE, loginDTO)%>
                	</span>
                    <input
                            style="border: 1px solid #D3D3D3; width: 70px; height:30px !important; border-radius: 6px"
                            onpaste="return false;"
                            type="text"
                            class="onlyBanglaAndEnglishDigitOnly form-control text-center ml-2"
                            onkeyup="syncRPP(this.value)"
                            name="RECORDS_PER_PAGE"
                            id="RECORDS_PER_PAGE"
                            placeholder=""
                            value="<%=rn.getPageSize()%>"
                    >
                </div>
                <div class="mx-3 my-3 my-sm-0">
                    <nav aria-label="Page navigation" class="d-flex align-items-center">
                        <div
                                class="d-flex align-items-center justify-content-center page-item"
                                style="border: 1px solid #D3D3D3;width: 40px; height:30px !important; border-radius: 6px 0 0 6px; border-right: none"
                        >
                            <a class="page-link border-0 bg-transparent" onclick="setPageNoAndSubmit(0)" title="Left" aria-label="First">
                                <i class="fa fa-angle-double-left" aria-hidden="true" style="cursor: pointer"
                                   aria-label="First"></i>
                            </a>
                        </div>
                        <div
                                class="d-flex align-items-center justify-content-center page-item"
                                style="border: 1px solid #D3D3D3;width: 40px; height:30px !important; border-radius: 0 0 0 0;border-right: none"
                        >
                            <a class="page-link border-0 bg-transparent" onclick="setPageNoAndSubmit(2)" title="Previous" aria-label="Previous">
                                <i class="fa fa-angle-left" aria-hidden="true" style="cursor: pointer"></i>
                            </a>
                        </div>
                        <div
                                class="d-flex align-items-center justify-content-center page-item"
                                style="border: 1px solid #D3D3D3;width: 40px; height:30px !important; border-radius: 0 0 0 0; border-right: none"
                        >
                            <a class="page-link border-0 bg-transparent" onclick="setPageNoAndSubmit(1)" title="Next" aria-label="Next">
                                <i class="fa fa-angle-right" aria-hidden="true" style="cursor: pointer"></i>
                            </a>
                        </div>
                        <div
                                class="d-flex align-items-center justify-content-center page-item"
                                style="border: 1px solid #D3D3D3;width: 40px; height:30px !important; border-radius: 0 6px 6px 0;"
                        >
                            <a class="page-link border-0 bg-transparent" onclick="setPageNoAndSubmit(3)" title="Last" aria-label="Last">
                                <i class="fa fa-angle-double-right" aria-hidden="true" style="cursor: pointer"></i>
                            </a>
                        </div>
                    </nav>
                </div>
                <div class="d-flex align-items-center">
                    <span class="mr-2">
                    <%=LM.getText(LC.GLOBAL_PAGE, loginDTO) %>
                </span>
                    <input
                            style="border: 1px solid #D3D3D3; width: 50px; height:30px !important; border-radius: 6px"
                            onpaste="return false;"
                            type="text"
                            name="pageno" onkeyup="syncPageno(this.value)"
                            id="pageno"
                            value='<%=rn.getCurrentPageNo()%>'
                            size="5"
                            class='onlyBanglaAndEnglishDigitOnly custom-from-control from-control text-center'
                    />
                    <span class="hidden-xs ml-2">
                    <%=LM.getText(LC.GLOBAL_OF, loginDTO) %>
                </span>
                    <span name='totalpage' class="ml-1">
                    <%=rn.getTotalPages()%>
                </span>
                    <a
                            type="submit"
                            onClick="allfield_changed('go',0)"
                            value=""
                            class="btn btn-sm green-haze btn-outline pl-1">
                        <i class="fa fa-arrow-alt-circle-right fa-2x bg-light text-success mb-1"
                           style="cursor: pointer">
                        </i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<%=context%>/assets/scripts/input_validation.js"></script>
<script type="text/javascript">
function syncRPP(value)
{
	$('[name = "RECORDS_PER_PAGE"]').each(function(){
	    $(this).val(value);
	});
}
function syncPageno(value)
{
	$('[name = "pageno"]').each(function(){
	    $(this).val(value);
	});
}
</script>
