<%@ page import="sessionmanager.SessionConstants" %>
<%
    String errorMsg = (String) request.getAttribute(SessionConstants.ERROR_MESSAGE );
    if( errorMsg != null ){
        request.getSession().removeAttribute( SessionConstants.ERROR_MESSAGE );
%>
<div class="alert alert-danger" role="alert">
    <b><%=errorMsg %></b>
</div>
<%}%>