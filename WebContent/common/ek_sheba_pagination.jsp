<%@ page import="java.util.List" %>
<div style="background: #fff;">
    <div style="display: inline; float:left; padding-right: 10px;">
        <%
            int total = rn.getTotalRecords();
            int pageSize = rn.getPageSize();
            int pageNo = rn.getCurrentPageNo();

            int start = (pageNo - 1) * pageSize + 1;
            int end = start + pageSize - 1;
        %>
        <label style="padding-top: 10px">Showing <%=start%> to <%=end%> of <%=total%> entries</label>
    </div>

    <div class="pagination" style="display: inline;">
        <a href="<%=link%><%=concat%>id=previous" class="action_button">&laquo;</a>
        <%
            List<Integer> paginationPages = new ArrayList<Integer>();
            int totalPage = rn.getTotalPages() > rn.getM_numberOfPageCountToShow() ? rn.getM_numberOfPageCountToShow() : rn.getTotalPages();

            if (totalPage > 5) {
                paginationPages.add(1);
                if (rn.getCurrentPageNo() == 1) {
                    if (!paginationPages.contains(1 + 1) && 1 + 1 < totalPage) paginationPages.add(1 + 1);
                }

                for (int i = 1 + 1; i <= totalPage - 1; i++) {
                    if (paginationPages.contains(i)) {
                        continue;
                    } else if (i == rn.getCurrentPageNo()) {
                        if (!paginationPages.contains(i - 1) && i - 1 > 0) paginationPages.add(i - 1);
                        paginationPages.add(i);
                        if (!paginationPages.contains(i + 1) && i + 1 < totalPage) paginationPages.add(i + 1);
                    } else {
                        paginationPages.add(0);
                    }
                }

                if (!paginationPages.contains(totalPage)) {
                    if (rn.getCurrentPageNo() == totalPage) {
                        if (!paginationPages.contains(totalPage - 1) && totalPage - 1 > 0)
                            paginationPages.add(totalPage - 1);
                    }
                    paginationPages.add(totalPage);
                }
            } else {
                for (int pageIndex = 1; pageIndex <= totalPage; pageIndex++) {
                    paginationPages.add(pageIndex);
                }
            }

            for (int i = 0; i < paginationPages.size(); i++) {
                int pageIndex = paginationPages.get(i);
                if (pageIndex == rn.getCurrentPageNo()) {
        %>
        <form action="<%=action%>" method="POST" id='searchform' class="form-inline">
            <input type="submit" class="active" value="<%=pageIndex%>">
            <input type="hidden" name="go" value="yes"/>
            <input type="hidden" name="mode" value="search"/>
            <input type="hidden" name="pageno" value="<%=pageIndex%>">
        </form>

        <%
        } else if (pageIndex == 0) {
            while (paginationPages.get(i) == 0) i++;
            i--;

        %>
        <div>
            <label class="form-inline">
                <input type="submit" value=".">
            </label>
        </div>
        <%

        } else {
        %>
        <form action="<%=action%>" method="POST" id='searchform' class="form-inline">
            <input type="submit" style="text-align:center;" value="<%=pageIndex%>">
            <input type="hidden" name="go" value="yes"/>
            <input type="hidden" name="mode" value="search"/>
            <input type="hidden" name="pageno" value="<%=pageIndex%>">
        </form>
        <%
                }
            }
        %>
        <%--    <a href="<%=link%><%=concat%>pageNumber=1" class="active">1</a>--%>

        <a href="<%=link%><%=concat%>id=next" class="action_button">&raquo;</a>
    </div>
</div>

<input type="hidden" id="pagenumber" value="20"/>

<style>
    .pagination .form-inline {
        float: left;
        cursor: pointer;
    }

    .pagination input[type="submit"] {
        text-align: center;
        background-color: #dddddd;
        border: none;
        padding: 8px 14px;
        margin-right: 5px;
        color: black;
    }

    .pagination input[type="submit"]:hover,
    .pagination input[type="submit"].active {
        background-color: #337ab7;
        color: #ffffff;
        cursor: pointer;
    }

    .pagination .action_button {
        margin-right: 5px;
    }

    .pagination::after {
        content: " ";
        display: table;
        clear: both;
    }
</style>

