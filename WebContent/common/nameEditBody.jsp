<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="common.NameDTO" %>
<%@ page import="common.NameRepository" %>
<%
    String context = request.getContextPath() + "/";
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    NameDTO nameDTO;
    String actionName;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "ajax_edit";
        NameRepository nameRepository = (NameRepository) request.getAttribute("NameRepository");
        nameDTO = nameRepository.getDTOByID(Long.parseLong(ID));
    } else {
        actionName = "ajax_add";
        nameDTO = new NameDTO();
    }
    String formTitle = LM.getText((Integer)request.getAttribute("titleValue"), loginDTO);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i><%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form"
              action="<%=(String)request.getAttribute("servletName")%>?actionType=<%=actionName%>&isPermanentTable=true&id=<%=nameDTO.iD%>"
              id="bigform" name="bigform">

            <!-- FORM BODY SKULL -->
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="nameEn_text">
                                            <%=LM.getText(LC.BANK_NAME_ADD_NAMEEN, loginDTO)%>
                                            <span class="required" style="color: red"> * </span>
                                        </label>
                                        <div class="col-md-9" id='nameEn_div'>
                                            <input type='text' class='form-control' name='nameEn' id='nameEn_text' required
                                                   value='<%=nameDTO.nameEn == null ? "" : nameDTO.nameEn%>'   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="nameBn_text">
                                            <%=LM.getText(LC.BANK_NAME_ADD_NAMEBN, loginDTO)%>
                                            <span class="required" style="color: red"> * </span>
                                        </label>
                                        <div class="col-md-9" id='nameBn_div'>
                                            <input type='text' class='form-control' name='nameBn' id='nameBn_text' required
                                                   value='<%=nameDTO.nameBn == null ? "" : nameDTO.nameBn%>'   tag='pb_html'/>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                            <%=LM.getText(LC.BANK_NAME_ADD_BANK_NAME_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit" onclick="submitForm()">
                            <%=LM.getText(LC.BANK_NAME_ADD_BANK_NAME_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<%=context%>assets/scripts/type_validation.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#cancel-btn").click(e => {
            e.preventDefault();
            window.history.back();
        });
    });
</script>