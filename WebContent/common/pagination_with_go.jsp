<%@page import="language.LC"%>
<%@page import="language.LM"%>
<div class="portlet box portlet-btcl light" style="border-top:none; box-shadow:unset !important;">
	<form action="<%=action%>" method="POST" id='searchform' class="form-inline">
	<div class="row">
		<div class="col-lg-offset-5 col-xs-12 col-sm-12 col-md-12 col-lg-7">
			<div class="container-fluid">
			<div class="row pull-right">
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-5 custom-align-content desktop-only" style="margin-bottom: 5px;">
					<div class="row">
						<div class="col-xs-5 col-sm-4 col-md-4 col-lg-7">
							<label style="font-size: 15px;margin-top: 5px;"><%=LM.getText(LC.GLOBAL_RECORD_PER_PAGE, loginDTO)%></label>
						</div>
						<div class="col-xs-4 col-sm-8 col-md-8 col-lg-5">
						<input type="text" class="form-control" name="RECORDS_PER_PAGE"  placeholder="" value="<%=rn.getPageSize()%>">
						</div>
					</div>
					
				</div>
				
				<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
					
					<nav aria-label="Page navigation" >
					  <ul class="pagination" style="margin: -1px 0px 0px 5px;">
					   <li class="page-item" style="display:table-cell !important;">
					      <a class="page-link" href="<%=link%><%=concat%>id=first" aria-label="First"  title="Left" >
					        <i class="fa fa-angle-double-left" aria-hidden="true" style="padding:3.5px"></i>
					        
					      </a>
					    </li>
					    <li class="page-item" style="display:table-cell !important;">
					      <a class="page-link" href="<%=link%><%=concat%>id=previous" aria-label="Previous" title="Previous">
					         <i class="fa fa-angle-left" aria-hidden="true" style="padding:3.5px"></i>
					        <span class="sr-only">Previous</span>
					      </a>
					    </li>
					
					     <li class="page-item" style="display:table-cell !important;">
					      <a class="page-link" href="<%=link%><%=concat%>id=next" aria-label="Next" title="Next">
					         <i class="fa fa-angle-right" aria-hidden="true" style="padding:3.5px"></i>
					        <span class="sr-only">Next</span>
					      </a>
					    </li>
					    <li class="page-item" style="display:table-cell !important;">
					      <a class="page-link" href="<%=link%><%=concat%>id=last" aria-label="Last"  title="Last" >
					        <i class="fa fa-angle-double-right" aria-hidden="true" style="padding:3.5px"></i>
					        <span class="sr-only">Last</span>
					      </a>
					    </li>
					    
					   
					  </ul>
					</nav>
				   
				</div>
				
				<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4" style="left:25px">
					<i><%=LM.getText(LC.GLOBAL_PAGE, loginDTO) %> </i>
							<input type="text" name="pageno"  value='<%=pageno%>' size="15" class='custom-from-control' style="height:32px !important"> <i class="hidden-xs"><%=LM.getText(LC.GLOBAL_OF, loginDTO) %></i>
						<i id='totalpage desktop-only'>
							<%=rn.getTotalPages()%>
						</i>
				
						<input type="hidden" name="go" value="yes" />
						<input type="hidden" name="mode" value="search" />
						<input type="submit" class="btn btn-sm green-haze btn-outline sbold uppercase" value="<%=LM.getText(LC.GLOBAL_GO, loginDTO)%>" style="height: 32px;margin-bottom: 3px;"/>
				</div>
			</div>
			
			
			</div>
		</div>
	</div>
	</form>
</div>
