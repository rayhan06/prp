<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@ page import="util.RecordNavigator"%>
<%
    String url = request.getAttribute("servletName") +"?actionType=search";
    String navigator = (String) request.getAttribute("NAV_NAME");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String pageno = "";

    RecordNavigator rn = (RecordNavigator) request.getAttribute("recordNavigator");
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();
    boolean isPermanentTable = true;

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    int pagination_number = 0;
    String searchPageTitle = LM.getText((Integer)request.getAttribute("searchPageTitleValue"), loginDTO);
%>

<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <i class="fa fa-search fa-2x" style="color:#0098bf !important;"></i>&nbsp;
        <h3 class="kt-subheader__title">
            <!-- Form Name -->
            <%=searchPageTitle%>
        </h3>
    </div>
</div>

<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content" style="background: white">
    <div class="row shadow-none border-0">
        <div class="col-lg-12">
            <!-- Search Nav -->
            <jsp:include page="./nameNav.jsp" flush="true">
                <jsp:param name="url" value="<%=url%>" />
                <jsp:param name="pageName" value="<%=searchPageTitle%>" />
            </jsp:include>

            <div style="height: 1px; background: #ecf0f5"></div>
            <div class="kt-portlet shadow-none">
                <div class="kt-portlet__body">
                    <!-- Search Form -->
                    <form action="<%=(String)request.getAttribute("servletName")%>?isPermanentTable=true&actionType=delete" method="POST" id="tableForm" enctype = "multipart/form-data">
                        <jsp:include page="nameSearchForm.jsp" flush="true">
                            <jsp:param name="pageName" value="<%=searchPageTitle%>" />
                        </jsp:include>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <% pagination_number = 1;%>
    <%@include file="../common/pagination_with_go2.jsp" %>
</div>

<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        initDeleteCheckBoxes();
    });
</script>