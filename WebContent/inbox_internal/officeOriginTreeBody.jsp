<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%

	String servletType = request.getParameter("servletType");
	String pageTitle = request.getParameter("pageTitle");

			
%>
<input type='hidden' id='servletType' value='<%=servletType%>' />
<div class="row" style="margin: 0px !important;">
	<div class="col-md-12" style="padding: 5px !important;">
		<div id="ajax-content">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-check-square-o"></i><%=pageTitle%>
					</div>

				</div>
				<div class="portlet-window-body">
					<div class="row">
						<%@include file="dropDownNode.jsp"%>
						<hr>
					</div>


					<div class="row">
						<div class="portlet light col-md-5">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-check-square-o"></i>মৌলিক শাখার পদবি
								</div>
							</div>
							<div class="portlet-window-body">
								<div
									class="jstree jstree-1 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
									id="origin_unit_tree_panel" role="tree" state="0">
									<ul class="jstree-container-ul jstree-children"></ul>
								</div>
							</div>
						</div>
						<div class="portlet light col-md-1">
							<div class="portlet-title">
								<div class="caption">কার্যক্রম</div>
							</div>
							<div class="portlet-window-body">
								<button type="button" id="officeUnitTransfer" onclick="leftToRight()"
									class="btn   btn-success">
									<i class="fa fa-hand-o-right"></i>
								</button>
								<br> <br>
								<button type="button" id="office_unit_tree_delete"
									class="btn   btn-danger">
									<i class="fa fa-hand-o-left"></i>
								</button>
							</div>
						</div>
						<div class="portlet light col-md-5">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-check-square-o"></i>অফিস শাখা পদবি
								</div>
							</div>
							<div class="portlet-window-body">
								<div id="office_unit_tree_panel"
									class="jstree jstree-2 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
									role="tree" state="0">
									<ul class="jstree-container-ul jstree-children"></ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>
	</div>
</div>



<script type="text/javascript">

function getIDsToSend(divID)
{
	var ids = "";
	

	
	
	$("#" + divID).find( ":checkbox" ).each(function() 
	{
		if($(this).is(':checked') )
		{
			console.log("found, id = " + $(this).attr('id'));
			var id = $(this).attr('data');			
			ids += "__" + id;
		}
		
 	});
	
	//console.log("Organogram_ids to send : " + Organogram_ids);
	//console.log("Unit_ids to send : " + Unit_ids);
	
	return ids;
}

function leftToRight()
{
	var leftDiv = document.getElementById("origin_unit_tree_panel");
	var rightDiv = document.getElementById("office_unit_tree_panel");
	
	console.log("leftToRight clicked");
	
	var ids = getIDsToSend("origin_unit_tree_panel");
	console.log("ids to send : " + ids);
	
	
}



function checkbox_toggeled(id, text, cb_id, name, parent_id)
{
	var treeName = cb_id.split("_")[0];
	console.log("treeName = " + treeName) ;
	var ids = fillChildren(id, text, cb_id, name, parent_id);
	var text = "";
	for (i = 0; i < ids.length; i++) 
	{
		  text += ids[i] + " ";
	}
	console.log("ids: " + text);
}

var global_office_origins_id = 0;
function getOfficeOrigins()
{
	console.log("after getting office units");
	getChildren(0, 0, "origin_unit_tree_panel", "getOriginUnit", "", global_office_origins_id,"", "");
}
function fillNextElementWithID(id, element)
{
	console.log("\n\nback in js, got element = " + element.getAttribute('id'));
	if(element.getAttribute('id') === "node_div_offices")
	{
		autoExpand = true;
		var office_origins = document.getElementById("select_office_origins");
		var office_origins_id = office_origins.options[office_origins.selectedIndex].value;
		var office = document.getElementById("select_offices");
		var office_id = office.options[office.selectedIndex].value;
		console.log("got office_origins_id = " + office_origins_id + " office_id = " + office_id);
		
		document.getElementById("office_unit_tree_panel").innerHTML = "";
		document.getElementById("office_unit_tree_panel").setAttribute("state", 0);
		document.getElementById("origin_unit_tree_panel").innerHTML = "";
		document.getElementById("origin_unit_tree_panel").setAttribute("state", 0);
		
		global_office_origins_id = office_origins_id;
		getChildren(0, 0, "office_unit_tree_panel", "getUnit", getOfficeOrigins, office_id, "", "");		
		
	}	
}
</script>
