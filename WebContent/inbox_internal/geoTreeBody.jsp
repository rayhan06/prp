<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String servletType = request.getParameter("servletType");
	String pageTitle = request.getParameter("pageTitle");

			
%>
<input type = 'hidden' id = 'servletType' value = '<%=servletType%>' />
<div class="col-md-12" style="padding: 5px !important;">
	<div id="ajax-content">
		<div class="row">
			<div class="col-md-5">
				<div class="portlet green-meadow box">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-cogs"></i><%=pageTitle %>
						</div>

					</div>
					<div class="portlet-body">
						<div id="geotree"
							class="jstree jstree-1 jstree-default jstree-default-large"
							role="tree" aria-activedescendant="node_dist_1">

							<%@include file="treeNode.jsp"%>

						</div>
					</div>
				</div>
			</div>
			<div class="col-md-7" id="geo_desc_div"></div>
		</div>
		
		<div class="col-md-7" id="geo_desc_div">
		</div>
	</div>
</div>
<script type="text/javascript">
function fillNextElementWithID(id, element)
{
}
function editNode(geoName, id)
{
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{
			if(this.responseText !='')
			{
				document.getElementById('geo_desc_div').innerHTML = this.responseText ;		
			}
			else
			{
				console.log("nul response");
			}
		}
		else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	};

  xhttp.open("Get", "Geo_" + geoName + "sServlet?actionType=getEditPage&getBodyOnly=true&ID=" + id, true);
  xhttp.send();	
}
</script>
