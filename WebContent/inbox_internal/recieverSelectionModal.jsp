<%@ page import="edms_documents.Edms_documentsDAO" %>
<%@ page import="edms_documents.Edms_documentsDTO" %>
<div class="modal fade" id="docDistributionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">

        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <label class="control-label"> <b>Distribute File</b> </label>
                <form  action="InboxInternalServlet?actionType=distributeDoc" id="bigform" name="bigform"  method="POST" style=" border: 1px solid black; padding: 10px;">
                    <div class="portlet-window-body">
                        <div class="row" style="margin-left: 0; margin-right: 0;">

                            <div class="col-md-12">
                                <div class="portlet light">

                                    <div class="portlet-window-body">

                                        <%if( tempList != null && tempList.size() != 0 ){ %>
                                        <div class="form-group" style="border:1px solid #659be0">
                                            <label class="control-label label-center">Existing Files</label>
                                            <table class="table table-bordered" >
                                                <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>File</th>
                                                    <th>Title</th>
                                                    <th>Tag</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <%
                                                    for(int k = 0; k < tempList.size(); k++ ){
                                                        String encode = Base64.getEncoder().encodeToString(tempList.get(k).fileData);
                                                %>
                                                <tr>
                                                    <td><%=tempList.get(k).fileTitle %></td>
                                                    <td>
                                                        <a href="Project_trackerServlet?actionType=getFile&id=<%=tempList.get(k).iD%>" class="download_file">Download</a>
                                                        <a href="#" data-toggle="modal" data-target="#dataFileModal_1_<%=k%>">View</a>
                                                        <div class="modal fade" id="dataFileModal_1_<%=k%>">
                                                            <div class="modal-dialog modal-lg" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-body">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">×</span>
                                                                        </button>
                                                                        <object type="<%=tempList.get(k).fileType%>" data="data:<%=tempList.get(k).fileType%>;base64,<%out.write(encode) ;%>" width="100%" height="500" style="height: 85vh;">No Support</object>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    <td>
                                                        <input type="hidden"  name='projectTrackerFiles.titleedit.id' id = 'dataFile_file_id_<%=k%>' value='<%=tempList.get(k).iD%>'  />
                                                        <input type='text' class='form-control'  name='projectTrackerFiles.titleedit' id = 'dataFile_file_title_<%=k%>' value='<%=tempList.get(k).fileTitle %>'  />
                                                    </td>
                                                    <td>
                                                        <input type='text' class='form-control'  name='projectTrackerFiles.tagedit' id = 'dataFile_file_tag_<%=k%>' value='<%=tempList.get(k).fileTag %>'  />
                                                    </td>
                                                </tr>
                                                <% } %>
                                                </tbody>
                                            </table>
                                        </div>
                                        <%}%>

                                        <div class="form-group">
                                            <label class="control-label"> Deadline: </label>
                                            <span> <%= sdfddMMyyyy.format( new Date( edms_documentsDTO.deadline ) )%></span> <br/>
                                            <label class="control-label">Update Deadline: </label>
                                            <input type="text" readonly="readonly" class="datepicker" name="inbox_sender_deadline" dataformatas="dd/MM/yyyy"/>
                                        </div>

                                        <div class="form-group"> <%-- style="max-height: 200px;">--%>
                                            <label class="control-label"> Action Details: </label>
                                            <textarea name="note" id="note" cols="5" rows="5"></textarea>
                                        </div>

                                        <div class="clearfix"></div>

                                        <template id="template-ProjectTrackerFiles" >
                                            <tr>
                                                <td>
                                                    <input type='file' class='form-control'  name='projectTrackerFiles.dataFile'/>
                                                </td>

                                                <td>
                                                    <div>
                                                            <span id='chkEdit' >
                                                                <input type='checkbox' name='checkbox' value=''/>
                                                            </span>
                                                    </div>
                                                </td>
                                            </tr>
                                        </template>


                                        <div class="clearfix"></div>


                                        <br/>
                                        <div class="form-group">
                                            <label class="control-label"> Select Receivers: </label>
                                        </div>
                                        <div class="row">

                                            <div class="col-md-12">

                                                <table class="table table-striped table-bordered">

                                                    <thead>
                                                    <th>Name & Designation</th>
                                                    <%--<th>Name</th>--%>
                                                    <th>Action</th>
                                                    <th>Information</th>
                                                    </thead>

                                                    <tbody id="receiverTbody">

                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="portlet light col-md-5">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-check-square-o"></i>Select Designations
                                                    </div>
                                                </div>
                                                <div class="portlet-window-body">
                                                    <div id="office_unit_tree_panel"
                                                         class="jstree jstree-2 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
                                                         role="tree" state="0">
                                                        <%@include file="treeNode.jsp"%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                    </div>

                                </div>
                            </div>

                        </div>

                        <div class="form-actions text-center">
                            <a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
                                CANCEL
                            </a>
                            <a onclick="myfunction()" class="btn btn-success" >
                                SUBMIT
                            </a>
                        </div>
                    </div>
                </form>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>