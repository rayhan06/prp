<%@ page import="util.*" %>
<%@page import="dbm.*" %>
<%@page import="pb.CatDAO" %>
<%@page import="approval_path.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="java.util.*" %>

<%
    String module = servletName.substring(0, servletName.length() - "Servlet".length()).toLowerCase();
//System.out.println("module = " + module);
    String docType = SessionConstants.tableDocTypeMap.getOrDefault(module, "");
//System.out.println("docType = " + docType);
    Approval_pathDAO approval_pathDAO2 = new Approval_pathDAO();
    List<Approval_pathDTO> approval_pathDTOs = approval_pathDAO2.getallByOfficeAndModule(userDTO.unitID, docType);
%>


<div class="modal fade" id="sendToApprovalPathModal" tabindex="-1"
     role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Approval Path Initiation</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <%

                    if (commonDTO.jobCat == -1) {
                %>

                <div style="padding: 5px;"
                >

                    <div class="row">

                        <div class="col-sm-2">
                            <label class="control-label"> Send to Approval Path </label>
                        </div>

                        <div class="form-group col-sm-10">
                            <div id='jobCat_div'>
                                <select class='form-control' style="width:100%;" name='jobCat'
                                        id='modal_jobCat_category_<%=i%>'
                                        tag='pb_html' onchange="showApprovalPathdetails(<%=i%>)">
                                    <option value="-1">Select</option>
                                    <%
                                        for (Approval_pathDTO approval_pathDTO : approval_pathDTOs) {
                                    %>
                                    <option value="<%=approval_pathDTO.jobCat%>"><%=approval_pathDTO.nameEn%>
                                    </option>
                                    <%
                                        }
                                    %>

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-2">
                            <label class="control-label"> Approval Path Details </label>
                        </div>
                        <div class="form-group col-sm-10">
                            <div class="table-responsive">
                                <div class="row div_border attachement-div" id="ap_details"></div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-2">
                            <label class="control-label"> Instructions </label>
                        </div>

                        <div class="form-group col-sm-10">

							<textarea class='form-control' name='remarks' id='<%=i%>_remarks'
                                      tag='pb_html'></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-2">
                            <label class="control-label"> Additional Documents (if any) </label>
                        </div>
                        <%
                            long ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                        %>
                        <div class="form-group col-sm-10">
                            <div class="dropzone"
                                 action="Approval_execution_tableServlet?pageType=add&actionType=UploadFilesFromDropZone&columnName=approval_attached_fileDropzone&ColumnID=<%=ColumnID%>">
                                <input type='file' style="display: none"
                                       name='approval_attached_fileDropzoneFile'
                                       id='approval_attached_fileDropzone_dropzone_File_<%=i%>'
                                       tag='pb_html'/>
                            </div>
                            <input type='hidden'
                                   name='approval_attached_fileDropzoneFilesToDelete'
                                   id='approval_attached_fileDropzoneFilesToDelete_<%=i%>' value=''
                                   tag='pb_html'/> <input type='hidden' name='fileID'
                                                          id='approval_attached_fileDropzone_dropzone_<%=i%>'
                                                          tag='pb_html' value='<%=ColumnID%>'/>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-12 text-right">
                            <a type="button" class="btn btn-primary submit-btn text-white btn-border-radius shadow" name="sendToApprovalPathButton_<%=i%>"
                               id="submitButton" style="visibility: hidden;"
                               name="sendToApprovalPathButton_<%=i%>"
                               onclick='approve("<%=commonDTO.iD%>", "", <%=i%>, "<%=servletName%>", 3, true, false)'>Send</a>
                        </div>
                    </div>
                </div>

                <%}%>

            </div>

            <div class="modal-footer border-0">
                <button type="button" class="btn border-0 shadow cancel-btn text-white btn-border-radius"
                        data-dismiss="modal">Close
                </button>
            </div>

        </div>
    </div>
</div>

<script>
    <%--$(document).ready(() => {--%>
    <%--    $("#submitButton").onclick((e) => {--%>
    <%--        e.preventDefault()--%>
    <%--        location.href='approve("<%=commonDTO.iD%>", "", <%=i%>, "<%=servletName%>", 3, true, false)'--%>
    <%--    })--%>
    <%--})--%>
</script>