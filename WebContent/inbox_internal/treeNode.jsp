<%@ page language="java" %>
<%@ page import="java.util.*" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="treeView.*" %>
<%@ page import="workflow.WorkflowController" %>
<%@ page import="user.UserNameAndDetails" %>
<%
    LoginDTO loginDTO3 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String Language = LM.getText(LC.CENTRE_EDIT_LANGUAGE, loginDTO3);
    String actionType = request.getParameter("actionType");
    String servletType2 = request.getParameter("servletType");
    TreeDTO treeDTO = (TreeDTO) request.getAttribute("treeDTO");

    ArrayList<Integer> nodeIDs = (ArrayList<Integer>) request.getAttribute("nodeIDs");

    int parentID = Integer.parseInt(request.getParameter("parentID"));
    int myLayer = ((Integer) request.getAttribute("myLayer")).intValue();
    int maxLayer = treeDTO.parentChildMap.length;

    String geoName = treeDTO.parentChildMap[myLayer];
   	String englishNameColumn = treeDTO.englishNameColumn[myLayer];
	String banglaNameColumn = treeDTO.banglaNameColumn[myLayer];
    String treeName = treeDTO.treeName;
    int checkBoxType = treeDTO.checkBoxType;
    int plusButtonType = treeDTO.plusButtonType;
    String showName = treeDTO.showNameMap[myLayer];

    System.out.println("In treeNode, myLayer = " + myLayer + " parentID = " + parentID);

    String parentCheckBoxChecked = request.getParameter("checkBoxChecked");
    boolean bParentCheckBoxChecked = false;
    if (parentCheckBoxChecked != null && !parentCheckBoxChecked.equals(""))
    {
        bParentCheckBoxChecked = Boolean.parseBoolean(parentCheckBoxChecked);
    }

    System.out.println("cccccccccccccc checkBoxType = " + checkBoxType + " bParentCheckBoxChecked = " + bParentCheckBoxChecked);
    String nameText = "";

    ArrayList<Integer> recursiveNodeTypes = null;

    int isRecursive = 0;
    if (request.getAttribute("isRecursive") != null)
    {
        isRecursive = ((Integer) request.getAttribute("isRecursive")).intValue();
        recursiveNodeTypes = (ArrayList<Integer>) request.getAttribute("recursiveNodeTypes");
    }

    boolean[] hasExtraLayerPlusButton = treeDTO.hasExtraLayerPlusButton;
%>

<ul class="jstree-container-ul jstree-children">
    <%
        int prevNodeType = 0;
        int extraLayerIndex = 0;
        boolean alreadyDrawnAnExtraUL = false;
        int i3 = 0;
        for (i3 = 0; i3 < nodeIDs.size(); i3++)
        {
            int id = nodeIDs.get(i3);
            int recursiveNodeType = 0;
            int realMyLayer = myLayer;

            boolean drawExtraUl = false;
            if (isRecursive != 0)
            {
                recursiveNodeType = recursiveNodeTypes.get(i3);
            }
            int nextLayer = realMyLayer + 1;
            if (recursiveNodeType == 1) //recursive node parent found, dont increment layer
            {
                nextLayer = realMyLayer;
            }
            else
            {
                realMyLayer++;
            }
            if (recursiveNodeType > 0) //recursive node parent found, dont increment layer
            {
                geoName = treeDTO.parentChildMap[realMyLayer];
                englishNameColumn = treeDTO.englishNameColumn[realMyLayer];
                banglaNameColumn = treeDTO.banglaNameColumn[realMyLayer];
            }

            if (prevNodeType != recursiveNodeType && treeDTO.drawExtraLayer)
            {
                drawExtraUl = true;
                prevNodeType = recursiveNodeType;
                extraLayerIndex = myLayer + recursiveNodeType - 1;
                showName = treeDTO.showNameMap[extraLayerIndex];
               
            }
            //System.out.println("realMyLayer = " + realMyLayer + " maxLayer = " + maxLayer);
            String text = "";
            if (servletType2.equalsIgnoreCase("InboxInternalServlet"))
            {
                text = GenericTree.getName(geoName, id, Language, englishNameColumn, banglaNameColumn);
            }
           // else if (servletType2.equalsIgnoreCase("InboxServlet"))
          //  {
          //      text = GenericTree.getName(geoName, id, Language, englishNameColumn, banglaNameColumn);
           // }
            else
            {
                text = GeoTree.getName(geoName, id, Language);
            }
			String layer_id_pairs = treeDTO.layer_id_pairs + "_" + realMyLayer + "_" + id;
            text = text.replace("'", "");
    %>

    <% if (drawExtraUl) {
        if (alreadyDrawnAnExtraUL) //need to close li
        {
            System.out.println("Already drawn, closing ul and li");
    %>
</ul>
<%
        alreadyDrawnAnExtraUL = false;
    }
%>
<li role="treeitem" aria-expanded="true" id="extra_li_<%=recursiveNodeType%>_<%=geoName%>_<%=id%>"
    class="jstree-node  jstree-open" state="2">
    <i
            class="fa fa-arrows" id="extra_plus_<%=recursiveNodeType%>_<%=geoName%>_<%=id%>"
            onclick="getChildren(0, 0, 'extra_li_<%=recursiveNodeType%>_<%=geoName%>_<%=id%>','', null, 0,'', '');">
    </i>
    <a class="jstree-anchor" href="#"></a>
    <i class="jstree-icon jstree-themeicon icon icon-arrow-right jstree-themeicon-custom"></i>
    <%=showName%>
    <%
        if (hasExtraLayerPlusButton != null && hasExtraLayerPlusButton.length >= extraLayerIndex && hasExtraLayerPlusButton[extraLayerIndex]) {
    %>
    <i class="glyphicon glyphicon-plus"
       onclick="extraLayerClick('<%=showName%>', '<%=geoName%>', '<%=id%>')"></i>
    <%
        }
    %>

    <ul class="jstree-container-ul jstree-children" style="display:none">
        <%
                alreadyDrawnAnExtraUL = true;
                System.out.println("Opened ul and li");
            }
        %>

        <%
            if(id != -1)
            {
        %>

        <li role="treeitem" aria-expanded="true" id="<%=treeName%>_div_<%=geoName%>_<%=id%>"
            class="jstree-node  jstree-open" state="0">

            <%
                if ((isRecursive == 1 && nextLayer < maxLayer - 1) || (isRecursive == 0 && nextLayer < maxLayer)) //not leaf
                {
            %>
            <i
                    class="fa fa-arrows" id="plus_<%=geoName%>_<%=id%>"
                    onclick="getChildren(
										'<%=nextLayer%>',
										'<%=id%>',
										'<%=treeName%>_div_<%=geoName%>_<%=id%>',
										'<%=actionType%>',
										null,
										<%=parentID%>,
										'&prevLayer=<%=myLayer%>',
										'<%=treeName%>_checkbox_<%=geoName%>_<%=id%>',
										'<%=layer_id_pairs%>'
										);"
            >
            </i>
            <%
                }
                if ((checkBoxType == 1 && realMyLayer == maxLayer ) || checkBoxType == 2
                        || (checkBoxType == 4 && realMyLayer == maxLayer && WorkflowController.getEmployeeRecordIDFromOrganogramID(id) != -1))
                {
            %>
            <input type="hidden" name="treeCBrecursiveNodeType" value="<%=recursiveNodeType%>"/>
            <input type="hidden" name="treeCBLayer" value="<%=realMyLayer%>"/>
            <input type="hidden" name="treeCBType" value="<%=checkBoxType%>"/>
            <input type="hidden" name="treeCBStatus" value="0" id="treeCBStatus_<%=id%>"/>
            <input type="hidden" name="treeCBChecked" value="-1" id="treeCBChecked_<%=id%>"/>
            <input type="checkbox" id="<%=treeName%>_checkbox_<%=geoName%>_<%=id%>" name="treeCB"
                   value="<%=id%>" data="<%=layer_id_pairs%>"
                   onclick="checkbox_toggeled(
                            '<%=id%>',
                            '<%=text%>',
                            '<%=treeName%>_checkbox_<%=geoName%>_<%=id%>',
                            '<%=GenericTree.getNameFromChildID( id, Language, geoName )%>',
                            '<%=treeName%>_div_<%=geoName%>_<%=id%>',
                            this
                           )"
                <%=bParentCheckBoxChecked?"checked":""%>>
            <%
                }
                else if (checkBoxType == 4 && realMyLayer == maxLayer && WorkflowController.getEmployeeRecordIDFromOrganogramID(id) == -1)
                {
                    %>
                    &nbsp &nbsp
                    <%
                }
            %>
            <a class="jstree-anchor" href="#"></a>
            <i class="jstree-icon jstree-themeicon icon icon-arrow-right jstree-themeicon-custom"></i>
            <%
                UserNameAndDetails userNameAndDetails = new UserNameAndDetails();
                if(realMyLayer == maxLayer)
                {
                    userNameAndDetails = WorkflowController.getUserNameAndDetailsFromOrganogramId( id, Language );
                    String employeeName = userNameAndDetails.name;
                    if(employeeName!= null && !employeeName.equalsIgnoreCase(""))
                    {
                        text = employeeName + ", " + text;
                    }
                }
            %>
            <a href="javascript:" data-type="division" data-id="1" data-org-id="<%=id%>" class="org_id_name"
               data-name-initial="<%=userNameAndDetails.alias%>"
               onclick="editNode('<%=geoName%>', '<%=id%>');"><%=text %>
            </a>

            <%
                if ((plusButtonType == 1 && realMyLayer == maxLayer - 1) || plusButtonType == 2)
                {
            %>

            <span class="glyphicon glyphicon-plus" aria-hidden="true"
                  onclick="plusClicked('<%=geoName%>', '<%=id%>', '<%=text %>');"></span>

            <%
                }
            %>
        </li>

        <%
            }
        %>

        <%
            }
        %>
        <%
            if (alreadyDrawnAnExtraUL && i3 == nodeIDs.size())
            {
                System.out.println("Already drawn, closing ul and li");
            }
        %>
    </ul>
</li>
</ul>