<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import="inbox.*"%>
<%@page import="project_tracker.*"%>
<%@page import="inbox_status.*"%>
<%@page import="user.*"%>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.List" %>
<%@ page import="com.sun.org.apache.xpath.internal.operations.Bool" %>
<%@ page import="util.CommonConstant" %>
<%@ page import="inbox_movements.Inbox_movementsDTO" %>
<%@ page import="inbox_movements.Inbox_movementsDAO" %>
<%@ page import="util.*" %>

<%
	String servletType = request.getParameter("servletType");
	String pageTitle = request.getParameter("pageTitle");
	String Options;
	LoginDTO loginDTO2 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String Language2 = LM.getText(LC.CENTRE_EDIT_LANGUAGE, loginDTO2);
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	SimpleDateFormat sdfddMMyyyy = new SimpleDateFormat("dd/MM/yyyy");
	Date date = new Date();
	String datestr = dateFormat.format(date);
	
	InboxDAO inboxDAO = new InboxDAO();//(InboxDAO)request.getAttribute("inboxDAO");

	String tempId = request.getParameter("row_id");

	String rowId = tempId;

	String tempTabId = request.getParameter("tab_id");

	Map<String, String> tempData = inboxDAO.getTabDetailsData(tempTabId,tempId);
	
	long tempIdLong = Long.parseLong(tempId.trim());
	
	UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO2);
	
	String tempInboxID = tempData.get("inbox_id");

	InboxDTO inboxDTO = (InboxDTO) inboxDAO.getDTOByID( Long.parseLong( tempInboxID ) );
	
	String tempCurrentStatusID = tempData.get("status");
	
	long tempCurrentStatusIDLong = 0;
	
	if(tempCurrentStatusID != null) {
	
	 tempCurrentStatusIDLong = Long.parseLong(tempCurrentStatusID);
	}
	
	FilesDAO tempDao = new FilesDAO();
	
	String servletName = "Edms_documentsServlet";
	
	List<FilesDTO> tempList = tempDao.getAllFiles("inbox_movements",tempIdLong);
	 
	Inbox_statusDAO tempInboxStatusDAO = new Inbox_statusDAO();
	List<Inbox_statusDTO> tempStatusDTOList = tempInboxStatusDAO.getActionByRoleID(userDTO.roleID,tempCurrentStatusIDLong);

	Inbox_movementsDTO inbox_movementsDTO = (Inbox_movementsDTO) new Inbox_movementsDAO().getDTOByID( Long.parseLong( rowId ), "inbox_movements" );
	Edms_documentsDTO edms_documentsDTO = (Edms_documentsDTO) new Edms_documentsDAO().getDTOByID( inboxDTO.inboxOriginatorId );

	CommonDTO commonDTO = edms_documentsDTO;
	Boolean canDistribute = false;
	Boolean canFinalize = false;

	if( tempStatusDTOList != null ) {
		for (Inbox_statusDTO inbox_statusDTO : tempStatusDTOList) {

			if ( inbox_statusDTO.ID == CommonConstant.DISTRIBUTION_STATE_ID && inbox_movementsDTO.toOrganogramId == userDTO.organogramID && inbox_movementsDTO.actionTaken == false ) {

				canDistribute = true;
				canFinalize = true;
			}
		}
	}

	int i = 0;

	System.out.println( "edms document creator id - " + edms_documentsDTO.userId );
	UserDTO documentCreatorUserDTO = UserRepository.getUserDTOByUserID( edms_documentsDTO.userId );
	System.out.println( "edms document creator user dto - " + documentCreatorUserDTO );

	Boolean isDocumentCreatedByDevelopmentPartner = documentCreatorUserDTO.roleID == 9801;
	System.out.println( "edms document creator isDocumentCreatedByDevelopmentPartner - " + isDocumentCreatedByDevelopmentPartner );
%>

<br/>
<div class="">

	<%if( canDistribute && edms_documentsDTO.jobCat == -1){%>
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#docDistributionModal">
			Distribute Document
		</button>

		<button type="button" class="btn btn-success" data-toggle="modal" data-target="#sendToApprovalPathModal" >
			<%=LM.getText(LC.HM_SEND_TO_APPROVAL_PATH, loginDTO)%>
		</button>

	<%}%>

	<%if( canFinalize ){ %>
		<button type="button" class="btn btn-default">
			Finalize Document
		</button>
	<%}%>

	<a class="btn btn-info" href="InboxHistoryServlet?actionType=getHistory&id=<%=tempInboxID%>" ><%=LM.getText(LC.HM_HISTORY, loginDTO)%></a>
	<%--<a class="btn btn-info" href="InboxHistoryServlet?actionType=getHistory" >Back</a>--%>

	<% if( isDocumentCreatedByDevelopmentPartner ){ %>

	    <a class="btn btn-warning" href="/Edms_documentsServlet?actionType=getEditPage&ID=<%=edms_documentsDTO.iD%>" >Edit</a>
	<% } %>

</div>
<br/>

<input type='hidden' id='servletType' value='<%=servletType%>' />
<div class="row" style="margin: 0px !important;">
	<div class="col-md-12" style="padding: 5px !important;">
		<div id="ajax-content">
			<div class="portlet light">

				<jsp:include page="../edms_documents/edms_documentsViewForInbox.jsp">
					<jsp:param name="ID" value="<%=inboxDTO.inboxOriginatorId%>"></jsp:param>
				</jsp:include>

				<div class="portlet light" style="background-color: white;">

					<div class="portlet-title">
						<h3>Inbox Message Details</h3>
					</div>

					<div class="portlet-window-body">

						<div class="form-group" style="border:1px solid #659be0; padding: 10px;">
							<label class="control-label label-center"> <u>Message Details</u> </label>
							<%
								String message = tempData.get("note");
								if( message == null || message.length() == 0 ){

									message = "No message available";
								}
							%>
							<div><%=message%></div>
						</div>

					</div>

				</div>

				<%@include file="recieverSelectionModal.jsp"%>
				<%@include file="sendToApprovalPathModal.jsp"%>

			</div>


		</div>
	</div>
</div>



<script type="text/javascript">

var tempOrganogramId = "";

var designation = "";

var tempOrganogramIdForward = "";

var tempOrganogramId_cc_array = [];

var tempForwardFlag = false;

var tempCCFlag = false;

function populateReceiverTable( data ) {

    var tr = $("<tr/>");

    $(tr).attr( "org_id", data["org_id"] );

    var designation = $("<td/>");
    var name = $("<td/>");
    var mainReceiver = $("<td/>");
    var cc = $("<td/>");

    var mainReceiverRadio = "<input type='radio' name='mainReceiver' value='" + data["org_id"] + "' /> ";
    var ccCheckbox =  "<input type='checkbox' name='ccReceiver' value='" + data["org_id"] + "' /> ";

    $(designation).text( data['org_name'] );
	/*$(name).text( "Temp Name" );*/
    $( mainReceiver ).html( mainReceiverRadio );
    $( cc ).html( ccCheckbox );

    $(tr).append( designation );
    /*$(tr).append( name );*/
    $(tr).append( mainReceiver );
    $(tr).append( cc );

    $("#receiverTbody").append( tr );
}

function deleteReceiverTableRow( data ){

    var id = data["org_id"];
	$( "#receiverTbody tr[org_id='" + id + "']" ).remove();
}

$(document).on( "click", "#receiverTbody input[type='radio']", function( e ){

    var checkbox = $(e.target).closest( "tr" ).find( "input[type='checkbox']" );

	if($( e.target ).is(':checked')) {

        $( checkbox ).prop( "checked", false );
	}
});

function checkbox_toggeled(id, text, cb_id, name, parent_id, checkbox )
{

    text = $(checkbox).closest( "li" ).find( "a.org_id_name" ).html(); // data( "org-id" );

    if( checkbox.checked == true ) {
        populateReceiverTable({

            org_id: id,
            org_name: text
        });
    }
    else{

        deleteReceiverTableRow({

			org_id: id
		});
	}

	var cb = document.getElementById(cb_id);
	var parent = document.getElementById(parent_id);
	var child_cbs = parent.querySelectorAll('input[type = "checkbox"]');
	var treeCBChecked = document.getElementById("treeCBChecked_" + id);
	var i;
	//alert("id : " + id);
	if(cb.checked)
	{
		console.log('checked');
		treeCBChecked.value = id;
		
		tempOrganogramId_cc_array.push(id);
		
		for (i = 0; i < child_cbs.length; i++) 
		{
			child_cbs[i].checked = true;
			document.getElementById("treeCBChecked_" + child_cbs[i].value).value = child_cbs[i].value;
		}
	}
	else
	{
		console.log('unchecked');
		treeCBChecked.value = -1;
		for (i = 0; i < child_cbs.length; i++) 
		{
			child_cbs[i].checked = false;
			document.getElementById("treeCBChecked_" + child_cbs[i].value).value = -1;
		}
	}
}

function fillNextElementWithID(id, element)
{
	console.log("\n\nback in js, got element = " + element.getAttribute('id'));
	
}
</script>

<script>
$(function(){
	CKEDITOR.replace( 'note' , {

        //height: '200px'
    } );
});
	
</script>

<script>
	$("#add-more-ProjectTrackerFiles").click(function(e) {
		e.preventDefault();
		var t = $("#template-ProjectTrackerFiles");
		$("#field-ProjectTrackerFiles").append(t.html());
		SetCheckBoxValues("field-ProjectTrackerFiles");

	});
	
    
    $("#remove-ProjectTrackerFiles").click(function(e){
	    var tablename = 'field-ProjectTrackerFiles';
	    var i = 0;
		console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
		var element = document.getElementById(tablename);

		var j = 0;
		for(i = document.getElementById(tablename).childNodes.length - 1; i >= 0 ; i --)
		{
			var tr = document.getElementById(tablename).childNodes[i];
			if(tr.nodeType === Node.ELEMENT_NODE)
			{
				console.log("tr.childNodes.length= " + tr.childNodes.length);
				var checkbox = tr.querySelector('input[type="checkbox"]');				
				if(checkbox.checked == true)
				{
					tr.remove();
				}
				j ++;
			}
			
		}    	
    });
    
    
    
    
    function enableCC() {
    	
    	document.getElementById("office_unit_tree_panel_cc").style.display = "inline"; 
    	
    	
    	
    	document.getElementById("cc_forward_text").innerHTML = "CC TO";
    	
    	tempCCFlag = true;
    	
    	//document.getElementById("office_unit_tree_panel_cc").style.visibility = 'visible';
    	
    	//document.getElementById("office_unit_tree_panel_cc").visible = true;
    	
    //	[DIV].visible = true;
    	
    	//alert("hi");
    
    }
    
function enableForward() {
    	
	document.getElementById("office_unit_tree_panel_cc").style.display = "inline"; 
	
	document.getElementById("cc_forward_text").innerHTML = "FORWARD TO";
	
	tempForwardFlag = true;
    
    }
    
 function  myfunction() {
    	
        	if(tempCCFlag)
        		{ 
        		
        		document.getElementById("bigform").action = "InboxInternalServlet?actionType=cc="+ tempOrganogramId_cc_array.toString() ; //Setting form action to "success.php" page
        	    document.getElementById("bigform").submit();
        		
        		
        		}
        	else if(tempForwardFlag)
        		{
        		document.getElementById("bigform").action = "InboxInternalServlet?actionType=forward&id="+tempOrganogramId_cc_array.toString() ; //Setting form action to "success.php" page
        	    document.getElementById("bigform").submit(); // Submitting form
        		}
        	else
        		{
        		
        		var tempTabID = "<%=tempTabId%>";
        		
        		var tempRowID = "<%=tempId%>";
        		
        		document.getElementById("bigform").action = "InboxInternalServlet?actionType=distributeDoc&row_id=" +tempRowID + "&tab_id=" + tempTabID; // + "&id="+tempOrganogramId_cc_array.toString() ; //Setting form action to "success.php" page
        	    
        		//alert(document.getElementById("bigform").action);
        		document.getElementById("bigform").submit(); // Submitting form
        		}
    	 
    
    }
 
 function getChildren2(layer, id, parentElement, actionType, retunfunc, parentID, extraParams, checkBoxID, layer_id_pairs)
	{
		//console.log("servletType = " + document.getElementById('servletType').value);
		//console.log("actionType = " + actionType + " layer = " + layer + " id= " + id + ' parentID = ' + parentID + ' parentElement = ' + parentElement);
		var parent = document.getElementById(parentElement);
		if(parent.getAttribute('state') == '0')
		{
			var treeCBStatus = document.getElementById("treeCBStatus_" + id);
			if(treeCBStatus !== null)
			{
				treeCBStatus.value = 1;
			}
			var xhttp = new XMLHttpRequest();
			parent.setAttribute("state", 1);
			var checkBox = document.getElementById(checkBoxID);
			var checkBoxChecked = false;
			if(checkBox !== null)
			{
				checkBoxChecked = checkBox.checked;
			}
			extraParams += "&checkBoxChecked=" + checkBoxChecked;
			getChildrenInElement(layer, id, parent, "tree", actionType, retunfunc, parentID, parentElement, extraParams, layer_id_pairs);
		}
		else if(parent.getAttribute('state') == '1')
		{
			console.log("state = " + parent.getAttribute('state'));
			parent.getElementsByTagName('ul')[0].style = "display:none";
			parent.setAttribute("state", 2);
		}
		else if(parent.getAttribute('state') == '2')
		{
			console.log("state = " + parent.getAttribute('state'));
			parent.getElementsByTagName('ul')[0].style = "display:block";
			parent.setAttribute("state", 1);
		}
		
	}
	
</script>


<script type="text/javascript">

    


    window.onload =function ()
    {
        console.log("using ckEditor");
        CKEDITOR.replaceAll();
    }

    $(document).ready( function(){

        $('.datepicker').datepicker({

            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            todayHighlight:	true,
            showButtonPanel: true
        });
    });
</script>