<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_requisition.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>

<%@ page import="geolocation.*" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="static sessionmanager.SessionConstants.VM_REQUISITION_PER_KM_FARE" %>
<%@ page import="static sessionmanager.SessionConstants.VM_REQUISITION_HOURLY_FARE" %>
<%@ page import="static sessionmanager.SessionConstants.*" %>


<%
    String servletName = "Vm_requisition_pendingServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Vm_requisitionDAO vm_requisitionDAO = new Vm_requisitionDAO("vm_requisition");
    Vm_requisitionDTO vm_requisitionDTO = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(id);
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    String context = request.getContextPath() + "/";

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

    String filter = " vehicle_requisition_purpose_cat = 2 AND payment_given != 1 AND status = 4 AND requester_org_id = " + vm_requisitionDTO.requesterOrgId + " ";
    List<Vm_requisitionDTO> unPaids = Vm_requisitionRepository.getInstance().getUnpaidPersonalVm_requisitionDTOByRequester_org_id(vm_requisitionDTO.requesterOrgId);

    double unPaidBill = 0.0;
    for (Vm_requisitionDTO unPaid : unPaids) {
        unPaidBill += (unPaid.totalTripDistance * VM_REQUISITION_PER_KM_FARE) +
                (unPaid.totalTripTime * VM_REQUISITION_HOURLY_FARE) +
                (unPaid.petrolAmount * VM_REQUISITION_PETROL_PER_LITER) -
                unPaid.paidAmount;
    }

%>

<style>
    .text-color {
        color: #0098bf;
    }

    .form-group label {
        font-weight: 600 !important;
    }
</style>

<!-- begin:: Subheader -->
<div class="ml-auto mr-3 mt-4">
    <%
        if (false) {
    %>
    <button type="button" class="btn" id='printer'
            onclick="downloadPdf('prescription.pdf','modalbody')">
        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
    <button type="button" class="btn" id='printer'
            onclick="printDiv('modalbody')">
        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
    <%
        }
    %>
</div>

<%@ include file="../vm_requisition/vm_requisitionCommonViewBody.jsp" %>
<div class="form-actions text-center mb-5">
    <div class="col-12">
        <div class="form-actions text-right">
            <%
                if (vm_requisitionDTO.status == CommonApprovalStatus.PENDING.getValue()) {
            %>
            <button class="btn btn-sm shadow text-white cancel-btn btn-border-radius"
                    onclick="event.preventDefault();location.href='Vm_requisition_pendingServlet?actionType=getEditPage&ID=<%=vm_requisitionDTO.iD%>&decision=Reject'">
                <%=LM.getText(LC.HM_REJECT, loginDTO)%>
            </button>

            <button class="btn btn-sm shadow text-white submit-btn ml-2 btn-border-radius"
                    onclick="event.preventDefault();location.href='Vm_requisition_pendingServlet?actionType=getEditPage&ID=<%=vm_requisitionDTO.iD%>&decision=Approve'">
                <%=LM.getText(LC.HM_APPROVE, loginDTO)%>
            </button>
            <%
                }
            %>
        </div>
    </div>
</div>