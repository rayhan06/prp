<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_requisition.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="employee_assign.EmployeeSearchIds" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleDAO" %>

<%
    Vm_requisitionDTO vm_requisitionDTO;
    vm_requisitionDTO = (Vm_requisitionDTO) request.getAttribute("vm_requisitionDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    if (vm_requisitionDTO == null) {
        vm_requisitionDTO = new Vm_requisitionDTO();

    }
    System.out.println("vm_requisitionDTO = " + vm_requisitionDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.HM_APPROVE, loginDTO);
    String servletName = "Vm_requisition_pendingServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Vm_requisition_pendingServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-10 offset-1">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-10 offset-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD'
                                           id='iD_hidden_<%=i%>' value='<%=vm_requisitionDTO.iD%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.insertedByUserId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.isDeleted%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.searchColumn%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='requesterOrgId'
                                           id='requesterOrgId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterOrgId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='decisionByOrgId'
                                           id='decisionByOrgId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByOrgId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='driverOrgId'
                                           id='driverOrgId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverOrgId%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEREMPID, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-8">
                                            <input type="hidden" class='form-control'
                                                   name='employeeRecordsId' id='employeeRecordsId'
                                                   value=''>
                                            <button type="button"
                                                    class="btn btn-primary btn-block shadow btn-border-radius mb-3"
                                                    id="tagDriver_modal_button">
                                                <%=LM.getText(LC.HM_SELECT, userDTO)%>
                                            </button>
                                            <table class="table table-bordered table-striped">
                                                <thead></thead>

                                                <tbody id="tagged_driver_table">
                                                <tr style="display: none;">
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        <button type="button"
                                                                class="btn btn-sm cancel-btn text-white shadow btn-no-border pl-4"
                                                                style="padding-right: 14px"
                                                                onclick="remove_containing_row(this,'tagged_driver_table');">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>

                                                <% if (actionName.equals("edit") && vm_requisitionDTO.driverEmpId != -1) { %>
                                                <tr>
                                                    <td><%=Employee_recordsRepository.getInstance()
                                                            .getById(vm_requisitionDTO.driverEmpId).employeeNumber%>
                                                    </td>
                                                    <td>
                                                        <%=isLanguageEnglish ? (vm_requisitionDTO.driverNameEn)
                                                                : (vm_requisitionDTO.driverNameBn)%>
                                                    </td>

                                                    <%
                                                        String postName = isLanguageEnglish ? (vm_requisitionDTO.driverOfficeUnitOrgNameEn + ", " + vm_requisitionDTO.driverOfficeUnitNameEn)
                                                                : (vm_requisitionDTO.driverOfficeUnitOrgNameBn + ", " + vm_requisitionDTO.driverOfficeUnitNameBn);
                                                    %>

                                                    <td><%=postName%>
                                                    </td>
                                                    <td id='<%=vm_requisitionDTO.driverEmpId%>_td_button'>
                                                        <button type="button"
                                                                class="btn btn-sm cancel-btn text-white shadow btn-no-border pl-4"
                                                                style="padding-right: 14px"
                                                                onclick="remove_containing_row(this,'tagged_driver_table');">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <%}%>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.VM_REQUISITION_SEARCH_REQUESTERNAMEEN, loginDTO)%>
                                        </label>
                                        <input type="text" class="form-control col-8" readonly
                                               value='<%value = vm_requisitionDTO.requesterNameEn + ", " + vm_requisitionDTO.requesterOfficeUnitOrgNameEn;%><%=Utils.getDigits(value, Language)%>'
                                        />
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.VM_REQUISITION_SEARCH_REQUESTERNAMEBN, loginDTO)%>
                                        </label>
                                        <input type="text" class="form-control col-8" readonly
                                               value='<%value = vm_requisitionDTO.requesterNameBn + ", " + vm_requisitionDTO.requesterOfficeUnitOrgNameBn;%><%=Utils.getDigits(value, Language)%>'
                                        />
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.VM_REQUISITION_ADD_STARTDATE, loginDTO)%>
                                        </label>
                                        <input type="text"
                                               class="form-control col-8"
                                               readonly
                                               value='<%value = vm_requisitionDTO.startDate + "";%><%String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));%><%=Utils.getDigits(formatted_startDate, Language)%>'
                                        />
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.VM_REQUISITION_ADD_ENDDATE, loginDTO)%>
                                        </label>
                                        <input type="text"
                                               class="form-control col-8"
                                               readonly
                                               value='<%value = vm_requisitionDTO.endDate + "";%><%String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));%><%=Utils.getDigits(formatted_endDate, Language)%>'
                                        />
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.VM_REQUISITION_ADD_STARTADDRESS, loginDTO)%>
                                        </label>
                                        <input type="text"
                                               class="form-control col-8"
                                               readonly
                                               value='<%value = vm_requisitionDTO.startAddress + "";value = value.replaceAll("\\$", " -> ");%><%=Utils.getDigits(value, Language)%>'
                                        />
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.VM_REQUISITION_ADD_ENDADDRESS, loginDTO)%>
                                        </label>
                                        <input type="text"
                                               class="form-control col-8"
                                               readonly
                                               value='<%value = vm_requisitionDTO.endAddress + "";value = value.replaceAll("\\$", " -> ");%><%=Utils.getDigits(value, Language)%>'
                                        />

                                    </div>


                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.VM_REQUISITION_ADD_STARTTIME, loginDTO)%>
                                        </label>
                                        <input type="text"
                                               class="form-control col-8"
                                               readonly
                                               value='<%value = vm_requisitionDTO.startTime + "";%><%=Utils.getDigits(value, Language)%>'
                                        />
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.VM_REQUISITION_ADD_ENDTIME, loginDTO)%>
                                        </label>
                                        <input type="text"
                                               class="form-control col-8"
                                               readonly
                                               value='<%value = vm_requisitionDTO.endTime + "";%><%=Utils.getDigits(value, Language)%>'
                                        />
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLEREQUISITIONPURPOSECAT, loginDTO)%>
                                        </label>
                                        <input type="text"
                                               class="form-control col-8"
                                               readonly
                                               value='<%value = vm_requisitionDTO.vehicleRequisitionPurposeCat + "";%><%value = CatRepository.getName(Language, "vehicle_requisition_purpose", vm_requisitionDTO.vehicleRequisitionPurposeCat);%><%=Utils.getDigits(value, Language)%>'
                                        />
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.VM_REQUISITION_EDIT_VEHICLETYPECAT, loginDTO)%>
                                        </label>
                                        <input type="text"
                                               class="form-control col-8"
                                               readonly
                                               value='<%value = vm_requisitionDTO.vehicleTypeCat + "";%><%value = CatRepository.getName(Language, "vehicle_type", vm_requisitionDTO.vehicleTypeCat);%><%=Utils.getDigits(value, Language)%>'
                                        />
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.VM_REQUISITION_ADD_TRIPDESCRIPTION, loginDTO)%>
                                        </label>
                                        <input type="text"
                                               class="form-control col-8"
                                               readonly
                                               value='<%value = vm_requisitionDTO.tripDescription + "";%><%=Utils.getDigits(value, Language)%>'
                                        />
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.VM_REQUISITION_ADD_GIVENVEHICLETYPE, loginDTO)%>
                                        </label>
                                        <div class="col-8 px-0">
                                            <select class='form-control' name='givenVehicleType'
                                                    id='givenVehicleType_select_<%=i%>' tag='pb_html'
                                                    onchange="loadVehicleList(this)">
                                                <%
                                                    Options = CatRepository.getOptions(Language, "vehicle_type", vm_requisitionDTO.givenVehicleType);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row" id="vehicleIdDiv">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_REQUISITION_ADD_GIVENVEHICLEID, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-8 px-0">
                                            <select class='form-control' name='givenVehicleId'
                                                    id='givenVehicleId_select_<%=i%>' tag='pb_html'>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONDESCRIPTION, loginDTO)%>
                                        </label>
                                        <div class="col-8 px-0">
                                            <textarea
                                                    class='form-control'
                                                    name='decisionDescription'
                                                    id='decisionDescription_text_<%=i%>'
                                            >
                                            </textarea>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='requesterOfficeId'
                                           id='requesterOfficeId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterOfficeId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='decisionByOfficeId'
                                           id='decisionByOfficeId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByOfficeId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='driverOfficeId'
                                           id='driverOfficeId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverOfficeId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='requesterOfficeUnitId'
                                           id='requesterOfficeUnitId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterOfficeUnitId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='decisionByOfficeUnitId'
                                           id='decisionByOfficeUnitId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByOfficeUnitId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='driverOfficeUnitId'
                                           id='driverOfficeUnitId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverOfficeUnitId%>'
                                           tag='pb_html'/>
                                    <input type='text' style='display: none' class='form-control'
                                           name='requesterEmpId' id='requesterEmpId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterEmpId%>' tag='pb_html'/>
                                    <input type='text' style='display: none' class='form-control'
                                           name='status' id='status_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterEmpId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='decisionByEmpId'
                                           id='decisionByEmpId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByEmpId%>'
                                           tag='pb_html'/>
                                    <input type='text' style='display: none' class='form-control'
                                           name='driverEmpId' id='driverEmpId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverEmpId%>' tag='pb_html'/>


                                    <input type='hidden' class='form-control'
                                           name='requesterOfficeNameEn'
                                           id='requesterOfficeNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterOfficeNameEn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='decisionByOfficeNameEn'
                                           id='decisionByOfficeNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByOfficeNameEn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='driverOfficeNameEn'
                                           id='driverOfficeNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverOfficeNameEn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='requesterOfficeUnitNameEn'
                                           id='requesterOfficeUnitNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterOfficeUnitNameEn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='decisionByOfficeUnitNameEn'
                                           id='decisionByOfficeUnitNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByOfficeUnitNameEn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='driverOfficeUnitNameEn'
                                           id='driverOfficeUnitNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverOfficeUnitNameEn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='requesterNameEn'
                                           id='requesterNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterNameEn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='decisionByNameEn'
                                           id='decisionByNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByNameEn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='driverNameEn'
                                           id='driverNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverNameEn%>' tag='pb_html'/>


                                    <input type='hidden' class='form-control'
                                           name='requesterOfficeNameBn'
                                           id='requesterOfficeNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterOfficeNameBn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='decisionByOfficeNameBn'
                                           id='decisionByOfficeNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByOfficeNameBn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='driverOfficeNameBn'
                                           id='driverOfficeNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverOfficeNameBn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='requesterOfficeUnitNameBn'
                                           id='requesterOfficeUnitNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterOfficeUnitNameBn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='decisionByOfficeUnitNameBn'
                                           id='decisionByOfficeUnitNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByOfficeUnitNameBn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='driverOfficeUnitNameBn'
                                           id='driverOfficeUnitNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverOfficeUnitNameBn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='requesterNameBn'
                                           id='requesterNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterNameBn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='decisionByNameBn'
                                           id='decisionByNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByNameBn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='driverNameBn'
                                           id='driverNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverNameBn%>' tag='pb_html'/>


                                    <input type='hidden' class='form-control'
                                           name='requesterOfficeUnitOrgNameEn'
                                           id='requesterOfficeUnitOrgNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterOfficeUnitOrgNameEn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='decisionByOfficeUnitOrgNameEn'
                                           id='decisionByOfficeUnitOrgNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByOfficeUnitOrgNameEn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='driverOfficeUnitOrgNameEn'
                                           id='driverOfficeUnitOrgNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverOfficeUnitOrgNameEn%>'
                                           tag='pb_html'/>


                                    <input type='hidden' class='form-control'
                                           name='requesterOfficeUnitOrgNameBn'
                                           id='requesterOfficeUnitOrgNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterOfficeUnitOrgNameBn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='decisionByOfficeUnitOrgNameBn'
                                           id='decisionByOfficeUnitOrgNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByOfficeUnitOrgNameBn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='driverOfficeUnitOrgNameBn'
                                           id='driverOfficeUnitOrgNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverOfficeUnitOrgNameBn%>'
                                           tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='requesterPhoneNum'
                                           id='requesterPhoneNum_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterPhoneNum%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='decisionByPhoneNum'
                                           id='decisionByPhoneNum_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByPhoneNum%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='driverPhoneNum'
                                           id='driverPhoneNum_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverPhoneNum%>' tag='pb_html'/>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-11 form-actions text-right mt-3">
                        <a class="btn btn-sm cancel-btn text-white shadow btn-border-radius"
                           href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.VM_REQUISITION_ADD_VM_REQUISITION_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button class="btn  btn-sm submit-btn text-white shadow btn-border-radius ml-2"
                                onclick="event.preventDefault();submitBigForm()"><%=LM.getText(LC.VM_REQUISITION_ADD_VM_REQUISITION_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<jsp:include page="../vm_requisition_pending/employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
    <jsp:param name="ID" value="<%=vm_requisitionDTO.iD%>"/>
    <jsp:param name="all" value="0"/>
</jsp:include>


<script type="text/javascript">

    function loadVehicleList(event) {

        let url = "Vm_vehicleServlet?actionType=getByVehicleType&ID=" + event.value + "&vmRequisitionId=" + '<%=vm_requisitionDTO.iD%>';
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                // const response = JSON.parse(fetchedData);
                $("#givenVehicleId_select_0").html(fetchedData);
            },
            error: function (error) {
                console.log(error);
            }
        });

    }


    function processResponse(data) {

        if (data.includes('Vehicle occupied')) {
            var alertMsg = "<%=Language.equals("English") ? "Sorry, this vehicle is occupied!" : "দুঃখিত, এই গাড়ি অন্যত্র নিযুক্ত"%>";
            bootbox.alert(alertMsg
                , function (result) {
                });
        } else if (data.includes('Driver occupied')) {
            var alertMsg = "<%=Language.equals("English") ? "Sorry, this driver is occupied!" : "দুঃখিত, এই চালক অন্যত্র নিযুক্ত"%>";
            bootbox.alert(alertMsg
                , function (result) {
                });
        } else {
            window.location = "Vm_requisition_pendingServlet?actionType=search&status=<%=CommonApprovalStatus.SATISFIED.getValue()%>";
        }
    }

    function ajaxPost(url, accept, send, postData, onSuccess, onError) {
        $.ajax({
            type: "POST",
            url: url,
            dataType: accept,
            contentType: send,
            data: postData,
            success: onSuccess,
            error: onError,
            complete: function () {
                // $.unblockUI();
            }
        });
    }


    function submitBigForm() {

        if (PreprocessBeforeSubmiting(0, '<%=actionName%>')) {
            var form = $("#bigform");

            var actionUrl = form.attr("action");
            var postData = (form.serialize());

            ajaxPost(actionUrl, "json", "application/x-www-form-urlencoded", postData, processResponse, processResponse);
        }


    }


    function PreprocessBeforeSubmiting(row, validate) {

        var vehicle_id = $("#givenVehicleId_select_0").val();
        if (!(vehicle_id != undefined && vehicle_id != null && vehicle_id != -1)) {
            toastr.error(
                "<%=Language.equals("English") ? "Please Select vehicle" : "অনুগ্রহপূর্বক গাড়ি সিলেক্ট করুন"%>"
            );
            return false;
        }

        let data = added_driver_map.keys().next();
        if (!(data && data.value)) {
            toastr.error(
                "<%=Language.equals("English") ? "Please Select driver" : "অনুগ্রহপূর্বক চালক সিলেক্ট করুন"%>"
            );
            return false;
        }
        document.getElementById('driverEmpId_hidden_0').value = JSON.stringify(
            Array.from(added_driver_map.values()));

        document.getElementById('status_hidden_0').value = '<%=CommonApprovalStatus.SATISFIED.getValue()%>';

        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Vm_requisition_pendingServlet");
    }

    function init(row) {


    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        // CKEDITOR.replaceAll();
    });

    var child_table_extra_id = <%=childTableStartingID%>;


    function insertAddressRow(tableId) {
        var myTable = document.getElementById(tableId);
        var currentIndex = myTable.rows.length;
        var currentRow = myTable.insertRow(-1);

        var linksBox = document.createElement("input");
        linksBox.setAttribute("name", "startAddresses");
        linksBox.setAttribute("id", "startAddress_TextField_" + currentIndex);
        linksBox.setAttribute("type", "text");
        linksBox.setAttribute("class", "form-control");
        linksBox.setAttribute("placeholder", "Road Number, House Number etc");
        linksBox.setAttribute("tag", "pb_html");

        var addRowBox = document.createElement("input");
        addRowBox.setAttribute("type", "button");
        addRowBox.setAttribute("value", "- ");
        addRowBox.setAttribute("onclick", "deleteAddressRow('" + tableId + "', this);");
        addRowBox.setAttribute("class", "button");

        var currentCell = currentRow.insertCell(-1);
        currentCell.appendChild(linksBox);

        currentCell = currentRow.insertCell(-1);
        currentCell.appendChild(addRowBox);
    }


    function deleteAddressRow(tableId, button) {
        var myTable = document.getElementById(tableId);
        var rowIndex = button.parentNode.parentNode.rowIndex;
        myTable.deleteRow(rowIndex);
    }


    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button

    // map to store and send added employee data
    // map to store and send added employee data


    <%
    List<EmployeeSearchIds> addedEmpIdsList = null;
    if(actionName.equals("edit") && vm_requisitionDTO.driverEmpId != -1) {
        addedEmpIdsList = Arrays.asList(new EmployeeSearchIds
        (vm_requisitionDTO.driverEmpId,vm_requisitionDTO.driverOfficeUnitId,vm_requisitionDTO.driverOrgId));
    }
    %>

    added_driver_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(addedEmpIdsList)%>);


    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_driver_table', {
                info_map: added_driver_map,
                isSingleEntry: true
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagDriver_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_driver_table';
        $('#search_emp_modal').modal();
    });


</script>


<style>
    .required {
        color: red;
    }
</style>






