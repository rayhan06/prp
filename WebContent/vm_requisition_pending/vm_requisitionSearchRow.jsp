<%@page pageEncoding="UTF-8" %>

<%@page import="vm_requisition.*" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_VM_REQUISITION;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Vm_requisitionDTO vm_requisitionDTO = (Vm_requisitionDTO) request.getAttribute("vm_requisitionDTO");
    CommonDTO commonDTO = vm_requisitionDTO;
    String servletName = "Vm_requisition_pendingServlet";


    System.out.println("vm_requisitionDTO = " + vm_requisitionDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Vm_requisitionDAO vm_requisitionDAO = (Vm_requisitionDAO) request.getAttribute("vm_requisitionDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_requesterOrgId'>
    <%
        value = Language.equals("English") ?
                vm_requisitionDTO.requesterNameEn + ", " + vm_requisitionDTO.requesterOfficeUnitOrgNameEn
                : vm_requisitionDTO.requesterNameBn + ", " + vm_requisitionDTO.requesterOfficeUnitOrgNameBn
        ;
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<%--											<td id = '<%=i%>_decisionByOrgId'>--%>
<%--											<%--%>
<%--											value = vm_requisitionDTO.decisionByOrgId + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_driverOrgId'>--%>
<%--											<%--%>
<%--											value = vm_requisitionDTO.driverOrgId + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>

<td id='<%=i%>_startDate'>
    <%
        value = vm_requisitionDTO.startDate + "";
    %>
    <%
        String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_startDate, Language)%>


</td>

<td id='<%=i%>_endDate'>
    <%
        value = vm_requisitionDTO.endDate + "";
    %>
    <%
        String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_endDate, Language)%>


</td>

<td id='<%=i%>_startAddress'>
    <%
        value = vm_requisitionDTO.startAddress + "";
        value = value.replaceAll("\\$", " -> ");
    %>
    <%=value%>


</td>

<td id='<%=i%>_endAddress'>
    <%
        value = vm_requisitionDTO.endAddress + "";
        value = value.replaceAll("\\$", " -> ");
    %>
    <%=value%>


</td>

<%--											<td id = '<%=i%>_vehicleRequisitionPurposeCat'>--%>
<%--											<%--%>
<%--											value = vm_requisitionDTO.vehicleRequisitionPurposeCat + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											value = CatRepository.getInstance().getName(Language, "vehicle_requisition_purpose", vm_requisitionDTO.vehicleRequisitionPurposeCat);--%>
<%--											%>	--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>

<td id='<%=i%>_vehicleTypeCat'>
    <%
        value = vm_requisitionDTO.vehicleTypeCat + "";
    %>
    <%
        value = CatRepository.getName(Language, "vehicle_type", vm_requisitionDTO.vehicleTypeCat);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<%--											<td id = '<%=i%>_tripDescription'>--%>
<%--											<%--%>
<%--											value = vm_requisitionDTO.tripDescription + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>


<td id='<%=i%>_status'>
    <%
        value = CatRepository.getName(Language, "vm_requisition_status", vm_requisitionDTO.status);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<%--											<td id = '<%=i%>_requesterOfficeId'>--%>
<%--											<%--%>
<%--											value = vm_requisitionDTO.requesterOfficeId + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="event.preventDefault();location.href='Vm_requisition_pendingServlet?actionType=view&ID=<%=vm_requisitionDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>

    <%
        if (vm_requisitionDTO.status == CommonApprovalStatus.PENDING.getValue()) {
    %>
    <button class="btn btn-sm border-0 shadow text-white btn-border-radius"
            style="background-color: #fd397a;"
            onclick="event.preventDefault();location.href='Vm_requisition_pendingServlet?actionType=getEditPage&ID=<%=vm_requisitionDTO.iD%>&decision=Reject'">
        <%=LM.getText(LC.HM_REJECT, loginDTO)%>
    </button>

    <button class="btn btn-sm border-0 shadow text-white btn-border-radius"
            style="background-color: #5867dd;"
            onclick="event.preventDefault();location.href='Vm_requisition_pendingServlet?actionType=getEditPage&ID=<%=vm_requisitionDTO.iD%>&decision=Approve'">
        <%=LM.getText(LC.HM_APPROVE, loginDTO)%>
    </button>
    <%
        }
    %>
</td>


<%--											<td id='<%=i%>_checkbox'>--%>
<%--												<div class='checker'>--%>
<%--													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=vm_requisitionDTO.iD%>'/></span>--%>
<%--												</div>--%>
<%--											</td>--%>
																						
											

