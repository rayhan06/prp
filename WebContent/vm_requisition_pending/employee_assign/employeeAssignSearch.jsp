<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.ArrayList" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="java.util.Map" %>
<%@ page import="employee_assign.EmployeeAssignDTO" %>
<%@ page import="test_lib.util.Pair" %>
<%@page pageEncoding="utf-8" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.EMPLOYEE_HISTORY_EDIT_LANGUAGE, loginDTO);
%>
<div class="">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute("data");
            int rowIndex = 0;
            long currentTime = System.currentTimeMillis();
            String disableRadioButtonTD = "<td>" +
                    "<label class='kt-radio kt-radio--bold kt-radio--brand'>" +
                    "<input name='radio_check' disabled" +
                    " type='radio'>" +
                    "<span></span>" +
                    "</label>" +
                    "</td>";
            try {
                if (data != null) {
                    for (Object aData : data) {
                        Pair pair = (Pair) aData;
                        Map<Object, Object> unitMap = (Map<Object, Object>) pair.getKey();
                        ArrayList<EmployeeAssignDTO> employeeList = (ArrayList<EmployeeAssignDTO>) pair.getValue();
                        String unitNameEng = (String) unitMap.get("unit_name_eng");
                        String unitNameBng = (String) unitMap.get("unit_name_bng");
                        out.println("<tr>\n" + "<td colspan=\"6\" style=\"font-size: 18px; font-weight: bold;\">");

                        if(Language.equalsIgnoreCase("English")){
                            value = (unitNameEng != null && unitNameEng.length() > 0) ?
                                    unitNameEng : "Unit Name Not Found";
                        }else{
                            value = (unitNameBng != null && unitNameBng.length() > 0) ?
                                    unitNameBng : "ইউনিটের নাম পাওয়া যায়নি";
                        }

                        out.println(value);
                        out.println("</td>\n" + "</tr>");
                        for (int j = 0; j < employeeList.size(); j++) {
                            out.println("<tr>\n");

                            if (employeeList.get(j).last_office_date == SessionConstants.MIN_DATE || currentTime<=employeeList.get(j).last_office_date) {
                                out.println(disableRadioButtonTD);
                            } else {
                                out.println("<td>\n" +
                                        "<label class='kt-radio kt-radio--bold kt-radio--brand'>\n" +
                                        "<input name='radio_check'" +
                                        "onclick=assignEmployeeCheck(" + (rowIndex + "," +
                                        employeeList.get(j).employee_id + "," +
                                        employeeList.get(j).officeId + "," +
                                        employeeList.get(j).office_unit_id + "," +
                                        employeeList.get(j).organogram_id + "," +
                                        employeeList.get(j).designation_level + "," +
                                        employeeList.get(j).designation_sequence + "," +
                                        employeeList.get(j).employee_office_id) +
                                        ")" +
                                        " type=\"radio\">\n" +
                                        "<span><span>\n" +
                                        "</label>\n" +
                                        "</td>\n");
                            }
                            out.println("<td id='" + employeeList.get(j).organogram_id + "'>");
                            out.println(Language.equals("English") ? employeeList.get(j).organogram_name_eng : employeeList.get(j).organogram_name_bng);
                            out.println("</td>\n");
                            if (employeeList.get(j).last_office_date == SessionConstants.MIN_DATE || currentTime<=employeeList.get(j).last_office_date) {
                                out.println("<td>");
                                out.println(Language.equals("English") ? employeeList.get(j).employee_name_eng : employeeList.get(j).employee_name_bng);
                                out.println("</td>\n");
                                out.println("<td>");
                                out.println(employeeList.get(j).username);
                                out.println("</td>\n");
                            } else {
                                out.println("<td>");
                                out.println(Language.equals("English") ? "Employee not found" : "কর্মকর্তা পাওয়া যায় নি");
                                out.println("</td>\n");
                                out.println("<td>");
                                out.println(Language.equals("English") ? "Username not found" : "ইউজারনেম পাওয়া যায় নি");
                                out.println("</td>\n");
                            }
                            if (employeeList.get(j).last_office_date == SessionConstants.MIN_DATE || currentTime<=employeeList.get(j).last_office_date) {
                                out.println("<td style='display: none'>\n" +
                                        "<input type=\"date\">\n" +
                                        "</td>\n" +
                                        "<td>\n" +
                                        "<button class='btn btn-outline-info' style='display: none' onclick=assignEmployeeOK(" + (employeeList.get(j).employee_id) + ")>Ok</button>\n" +
                                        "</td>\n");
                                out.println("<td  style='cursor: pointer;' class='assignedPermission'>\n" +
                                        "<button onclick='showPermittedApp(" + employeeList.get(j).employee_id + "," + employeeList.get(j).organogram_id + ") ' class='btn btn-primary btn-hover-brand btn-square'><i class='fa fa-info'></i></button>" +
                                        "</td>");
                            } else {
                                out.println("<td >\n" +
                                        "<select class='form-control' id='incharge_label_" + (rowIndex++) + "'>" +
                                        "<option value=\"\" selected=\"selected\">--Select Incharge Type--</option>" +
                                        "<option value=\"In charge\">In charge</option>" +
                                        "<option value=\"Ongoing responsibility\">Ongoing responsibility</option>" +
                                        "<option value=\"Additional responsibility\">Additional responsibility</option>" +
                                        "<option value=\"Routine responsibility\">Routine responsibility</option>" +
                                        "</select>" +
                                        "</td>\n" +
                                        "<td>\n" +
                                        "<button class='btn btn-outline-info' onclick=assignEmployeeOK(" + (employeeList.get(j).employee_id) + ")>Ok</button>\n" +
                                        "</td>\n");
                            }
                            out.println("</tr>");
                        }
                    }
                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">
</script>
<style>
    .btn:hover {
        color: #fff;
    }
</style>