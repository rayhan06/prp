<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="office_units.Office_unitsRepository" %>

<div id="drop_down" class="row" style="padding: 15px;">
    <div class="col-md-12">
        <div id="ajax-content">
            <form class="form-control">
                <label class="radio-inline" style="margin-left: 10px">
                    <input class="form-check-input" type="radio" name="search_by" value="by_organogram" checked onclick="showEmployeeList();"> Search
                    By Organogram
                </label>
                <label class="radio-inline ml-5">
                    <input class="form-check-input" type="radio" name="search_by" value="by_otherInfo"> Search By Other
                    Info
                </label>
<%--                <label class="radio-inline ml-5">--%>
<%--                    <input class="form-check-input" type="radio" name="search_by" value="tree_search">  Tree Search--%>
<%--                </label>--%>
                <hr style="border-top: 1px solid rgba(0, 0, 0, 0.1); margin: 10px 10px">
            </form>

            <hr/>

            <div class="search_by_div" id="by_organogram_div">
                <form class="kt-form">
                    <div class="kt-form" id="officeLayer_dropdown_div">
                        <%-- BEGIN: Making Dreopdown Dynamic --%>
                        <div id="officeLayer_div_1" class="form-group col-lg-10 office-layer" style="display: none; margin-top: 10px">
                            <label><%=(isLanguageEnglish ? "Office" : "দপ্তর")%></label>
                            <div>
                                <select class='form-control' data-label="Office"
                                        id='officeLayer_select_1' tag='pb_html'
                                        onchange="layerChange(this);">
                                </select>
                            </div>
                        </div>
                        <%-- END: Making Dreopdown Dynamic --%>
                    </div>
                </form>
            </div>
            <div class="search_by_div" id="by_otherInfo_div" style="display: none;margin-top: 10px">
                <form id="onOtherInfoSearch_form" action="EmployeeAssignServlet" method="GET">
                    <div class="form-group col-md-12">
                        <label for="search_by_userName">Employee Id</label>
                        <input type="text" class="form-control" name="userName" id="search_by_userName">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="search_by_nameEn">Name(English)</label>
                        <input type="text" class="form-control" name="nameEn" id="search_by_nameEn">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="search_by_nameBn">Name(Bangla)</label>
                        <input type="text" class="form-control" name="nameBn" id="search_by_nameBn">
                    </div>
                    <div class="form-group col-md-12">
                        <button type="submit" style="margin-top: 20px!important; float: right; border-radius: 8px"
                                class="btn btn-primary shadow">Search
                        </button>
                    </div>
                </form>
            </div>

            <div class="search_by_div" id="tree_search_div" style="display: none;margin-top: 10px">
                <jsp:include page="/tree/tree.jsp">
                    <jsp:param name="TREE_ID" value="jsTree"></jsp:param>
                    <jsp:param name="REMOVE_PLUGIN" value="state"></jsp:param>
                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                </jsp:include>
            </div>

            <hr style="border-top: 1px solid rgba(0, 0, 0, 0.1); margin: 10px 10px">

            <div style="margin: 5px 10px" class="" id="employeeSearchModal_table_div">
            </div>
        </div>
    </div>
</div>