<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_requisition.*" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

    Vm_requisitionDAO vm_requisitionDAO = (Vm_requisitionDAO) request.getAttribute("vm_requisitionDAO");


    String navigator2 = SessionConstants.NAV_VM_REQUISITION;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<%

    if (hasAjax == false) {
        Enumeration<String> parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {

            String paramName = parameterNames.nextElement();

            if (!paramName.equalsIgnoreCase("actionType")) {
                String[] paramValues = request.getParameterValues(paramName);
                for (int i = 0; i < paramValues.length; i++) {
                    String paramValue = paramValues[i];

%>

<%

                }
            }


        }
    }

%>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTERORGID, loginDTO)%>
            </th>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYORGID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVERORGID, loginDTO)%></th>--%>
            <th><%=LM.getText(LC.VM_REQUISITION_ADD_STARTDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_REQUISITION_ADD_ENDDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_REQUISITION_ADD_STARTADDRESS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_REQUISITION_ADD_ENDADDRESS, loginDTO)%>
            </th>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLEREQUISITIONPURPOSECAT, loginDTO)%></th>--%>
            <th><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLETYPECAT, loginDTO)%>
            </th>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_TRIPDESCRIPTION, loginDTO)%></th>--%>
            <th><%=LM.getText(LC.VM_REQUISITION_ADD_STATUS, loginDTO)%>
            </th>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONON, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONDESCRIPTION, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_GIVENVEHICLETYPE, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_GIVENVEHICLEID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_RECEIVEDATE, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_TOTALTRIPDISTANCE, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_TOTALTRIPTIME, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_PETROLGIVEN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_PETROLAMOUNT, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_PAYMENTGIVEN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_PAYMENTTYPE, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_STARTTIME, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_ENDTIME, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_RECEIVETIME, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICEID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICEID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICEID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICEUNITID, loginDTO)%></th>--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICEUNITID, loginDTO)%></th>&ndash;%&gt;--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICEUNITID, loginDTO)%></th>&ndash;%&gt;--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEREMPID, loginDTO)%></th>--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYEMPID, loginDTO)%></th>&ndash;%&gt;--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEREMPID, loginDTO)%></th>&ndash;%&gt;--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTERPHONENUM, loginDTO)%></th>--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYPHONENUM, loginDTO)%></th>&ndash;%&gt;--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVERPHONENUM, loginDTO)%></th>&ndash;%&gt;--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTERNAMEEN, loginDTO)%></th>--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYNAMEEN, loginDTO)%></th>&ndash;%&gt;--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVERNAMEEN, loginDTO)%></th>&ndash;%&gt;--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTERNAMEBN, loginDTO)%></th>--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYNAMEBN, loginDTO)%></th>&ndash;%&gt;--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVERNAMEBN, loginDTO)%></th>&ndash;%&gt;--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICENAMEEN, loginDTO)%></th>--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICENAMEEN, loginDTO)%></th>&ndash;%&gt;--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICENAMEEN, loginDTO)%></th>&ndash;%&gt;--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICENAMEBN, loginDTO)%></th>--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICENAMEBN, loginDTO)%></th>&ndash;%&gt;--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICENAMEBN, loginDTO)%></th>&ndash;%&gt;--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICEUNITNAMEEN, loginDTO)%></th>--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICEUNITNAMEEN, loginDTO)%></th>&ndash;%&gt;--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICEUNITNAMEEN, loginDTO)%></th>&ndash;%&gt;--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICEUNITNAMEBN, loginDTO)%></th>--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICEUNITNAMEBN, loginDTO)%></th>&ndash;%&gt;--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICEUNITNAMEBN, loginDTO)%></th>&ndash;%&gt;--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICEUNITORGNAMEEN, loginDTO)%></th>--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICEUNITORGNAMEEN, loginDTO)%></th>&ndash;%&gt;--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICEUNITORGNAMEEN, loginDTO)%></th>&ndash;%&gt;--%>
            <%--								<th><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICEUNITORGNAMEBN, loginDTO)%></th>--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICEUNITORGNAMEBN, loginDTO)%></th>&ndash;%&gt;--%>
            <%--&lt;%&ndash;								<th><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICEUNITORGNAMEBN, loginDTO)%></th>&ndash;%&gt;--%>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th style="text-align: center"><%=LM.getText(LC.VM_REQUISITION_APPROVER_SEARCH_VM_REQUISITION_APPROVER_EDIT_BUTTON, loginDTO)%>
            </th>
            <%--								<th class="">--%>
            <%--									<span class="ml-4">All</span>--%>
            <%--									<div class="d-flex align-items-center mr-3">--%>
            <%--										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">--%>
            <%--											<i class="fa fa-trash"></i>&nbsp;--%>
            <%--										</button>--%>
            <%--										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>--%>
            <%--									</div>--%>
            <%--								</th>--%>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_VM_REQUISITION);

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Vm_requisitionDTO vm_requisitionDTO = (Vm_requisitionDTO) data.get(i);


        %>
        <tr id='tr_<%=i%>'>
            <%

            %>


            <%
                request.setAttribute("vm_requisitionDTO", vm_requisitionDTO);
            %>

            <jsp:include page="./vm_requisitionSearchRow.jsp">
                <jsp:param name="pageName" value="searchrow"/>
                <jsp:param name="rownum" value="<%=i%>"/>
            </jsp:include>


            <%

            %>
        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			