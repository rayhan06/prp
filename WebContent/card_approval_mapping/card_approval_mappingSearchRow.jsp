<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.StringUtils" %>
<%@ page import="card_info.*" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>

<%
    Card_infoDTO card_infoDTO = mapByCardInfoId.get(card_approval_mappingDTO.cardInfoId);
%>
<td style='display:none;' class="text-nowrap"><input type='hidden' id='failureMessage_" + i + "' value=''/></td>

<td id='<%=i%>_cardRequester' class="text-nowrap">
    <%= isLanguageEnglish ? card_infoDTO.insertByNameEng : card_infoDTO.insertByNameBng%>
    <br><b><%= isLanguageEnglish ? card_infoDTO.insertByOfficeUnitOrganogramEng : card_infoDTO.insertByOfficeUnitOrganogramBng%></b>
    <br><%= isLanguageEnglish ? card_infoDTO.insertByOfficeUnitEng : card_infoDTO.insertByOfficeUnitBng%>
</td>

<td id='<%=i%>_cardInfoId' class="text-nowrap">
    <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(card_approval_mappingDTO.cardInfoId))%>
    <div style="height: 5px"></div>
    <span class="btn btn-sm border-0 shadow" style="background-color: <%=CardCategoryEnum.getColor(card_infoDTO.cardCat)%>; color: white; border-radius: 8px;cursor: text">
        <%=CatRepository.getInstance().getText(Language, "card", card_infoDTO.cardCat)%>
    </span>
    <%
        if(mapByReissueCardId.get(card_infoDTO.iD)!=null){
    %>
    <div style="height: 5px"></div>
        <span class="btn btn-sm border-0 shadow"
              style="background-color: darkred; color: white; border-radius: 8px;cursor: text">
                     <%=LM.getText(LC.LOST_CARD_INFO_ADD_REISSUE_CARD, loginDTO)%>
        </span>
    <%
        }
    %>
</td>

<td id='<%=i%>_cardHolder' class="text-nowrap">
    <%
        CardEmployeeInfoDTO cardEmployeeInfoDTO = mapByCardEmployeeInfoId.get(card_infoDTO.cardEmployeeInfoId);
        String str = null;
        if (cardEmployeeInfoDTO != null) {
            str = isLanguageEnglish ? cardEmployeeInfoDTO.nameEn : cardEmployeeInfoDTO.nameBn;
        }
        CardEmployeeOfficeInfoDTO cardEmployeeOfficeInfoDTO = mapByCardEmployeeOfficeInfoDTOId.get(card_infoDTO.cardEmployeeOfficeInfoId);
        if (cardEmployeeOfficeInfoDTO != null) {
            String s1 = isLanguageEnglish ? "<b>"+cardEmployeeOfficeInfoDTO.organogramEng + "</b><br>" + cardEmployeeOfficeInfoDTO.officeUnitEng
                    : "<b>"+cardEmployeeOfficeInfoDTO.organogramBng + "</b><br>" + cardEmployeeOfficeInfoDTO.officeUnitBng;
            if (str == null) {
                str = s1;
            } else {
                str += "<br>" + s1;
            }
        }
        if (str == null) {
            str = "";
        }
    %>
    <%=str%>
</td>
<td id='<%=i%>_cardEmployeeImagesId' style="text-align: center" class="text-nowrap">
    <%
        CardEmployeeImagesDTO cardEmployeeImagesDTO = mapByEmployeeImagesDTOId.get(card_infoDTO.cardEmployeeImagesId);
        byte[] encodeBase64Photo = null;

        if(cardEmployeeImagesDTO!=null && cardEmployeeImagesDTO.photo!=null){
            encodeBase64Photo = Base64.encodeBase64(cardEmployeeImagesDTO.photo);
        }
    %>
    <img width="75px" height="75px"
         src='data:image/jpg;base64,<%=encodeBase64Photo != null ? new String(encodeBase64Photo) : ""%>'>
</td>

<td id='<%=i%>_isPoliceVerificationRequired' class="text-nowrap">
    <%=StringUtils.getYesNo(Language,card_infoDTO.isPoliceVerificationRequired)%>
</td>

<td id='<%=i%>_cardApprovalStatusCat' class="text-nowrap">
    <span class="btn btn-sm border-0 shadow"
          style="background-color: <%=ApprovalStatus.getColor(card_approval_mappingDTO.cardApprovalStatusCat)%>; color: white; border-radius: 8px;cursor: text">
        <%=CatRepository.getInstance().getText(Language, "card_approval_status", card_approval_mappingDTO.cardApprovalStatusCat)%>
    </span>
</td>

<td class="text-nowrap">
    <span class="btn btn-sm border-0 shadow" style="background-color: #22ccc1; color: white; border-radius: 8px">
        <a href='Card_approval_mappingServlet?actionType=getApprovalPage&cardInfoId=<%=card_approval_mappingDTO.cardInfoId%>&language=<%=Language%>'
           style="color: #FFFFFF"><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></a>
    </span>
</td>