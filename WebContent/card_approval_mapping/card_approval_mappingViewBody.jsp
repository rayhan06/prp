<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="card_approval_mapping.*" %>


<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String value;
    String Language = LM.getText(LC.CARD_APPROVAL_MAPPING_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Card_approval_mappingDAO card_approval_mappingDAO = Card_approval_mappingDAO.getInstance();
    Card_approval_mappingDTO card_approval_mappingDTO = card_approval_mappingDAO.getDTOFromID(id);
%>


<!-- <div class="modal-content viewmodal"> -->
<div class="menubottom">
    <div class="modal-header">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-9 col-sm-12">
                    <h5 class="modal-title"><%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARD_APPROVAL_MAPPING_ADD_FORMNAME, loginDTO)%>
                    </h5>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-success app_register"
                               data-id="419637"> Register </a>
                        </div>
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-danger app_reject"
                               data-id="419637"> Reject </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <div class="modal-body container">

        <div class="row div_border office-div">

            <div class="col-md-12">
                <h3><%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARD_APPROVAL_MAPPING_ADD_FORMNAME, loginDTO)%>
                </h3>
                <table class="table table-bordered table-striped">


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARDINFOID, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = card_approval_mappingDTO.cardInfoId + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARDAPPROVALID, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = card_approval_mappingDTO.cardApprovalId + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_SEQUENCE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = card_approval_mappingDTO.sequence + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARDAPPROVALSTATUSCAT, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = CatDAO.getName(Language, "card_approval_status", card_approval_mappingDTO.cardApprovalStatusCat);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_EMPLOYEERECORDSID, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = card_approval_mappingDTO.employeeRecordsId + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                </table>
            </div>


        </div>


    </div>
</div>