<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="card_approval_mapping.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="user.*" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="lost_card_info.Lost_card_infoDTO" %>
<%@ page import="lost_card_info.Lost_card_infoDAO" %>
<%@ page import="java.util.*" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>
<%@page pageEncoding="UTF-8" %>


<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    RecordNavigator rn2 = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    Map<Long,CardEmployeeInfoDTO> mapByCardEmployeeInfoId;
    Map<Long,CardEmployeeOfficeInfoDTO> mapByCardEmployeeOfficeInfoDTOId;
    Map<Long,CardApprovalDTO> mapByCardApprovalDTOId;
    Map<Long,CardEmployeeImagesDTO> mapByEmployeeImagesDTOId;
    Map<Long,Lost_card_infoDTO> mapByReissueCardId;
%>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr class="text-nowrap">
            <th><%=LM.getText(LC.CARD_INFO_REQUESTER_INFORMATION_DESIGNATION_OFFICE, loginDTO)%></th>
            <th><%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARDINFOID, loginDTO)%></th>
            <th colspan="2"><%=LM.getText(LC.CARD_APPROVAL_MAPPING_CARDHOLDER_INFORMATION, loginDTO)%></th>
            <th><%=LM.getText(LC.CARD_INFO_ADD_ISPOLICEVERIFICATIONREQUIRED, loginDTO)%></th>
            <th><%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARDAPPROVALSTATUSCAT, loginDTO)%></th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Card_approval_mappingDTO> data = (List<Card_approval_mappingDTO>) rn2.list;
                if (data != null && data.size()>0) {
                    List<Long> cardEmployeeInfoIds = new ArrayList<>();
                    List<Long> cardEmployeeOfficeInfoIds = new ArrayList<>();
                    List<Long> cardEmployeeImagesIds = new ArrayList<>();
                    List<Long> cardApprovalIds = new ArrayList<>();
                    List<Long> cardInfoIds = new ArrayList<>();
                    data.forEach(dto->{
                        cardApprovalIds.add(dto.cardApprovalId);
                        cardInfoIds.add(dto.cardInfoId);
                    });
                    List<CardApprovalDTO> cardApprovalDTOList = CardApprovalRepository.getInstance().getByIds(cardApprovalIds);
                    List<Lost_card_infoDTO> lostCardInfoDTOList = Lost_card_infoDAO.getInstance().getByReIssueCardInfoIds(cardInfoIds);
                    List<Card_infoDTO> cardInfoDTOList = CardInfoDAO.getInstance().getDTOs(cardInfoIds);
                    Map<Long,Card_infoDTO> mapByCardInfoId = new HashMap<>();
                    for(Card_infoDTO dto : cardInfoDTOList){
                        cardEmployeeInfoIds.add(dto.cardEmployeeInfoId);
                        cardEmployeeOfficeInfoIds.add(dto.cardEmployeeOfficeInfoId);
                        cardEmployeeImagesIds.add(dto.cardEmployeeImagesId);
                        mapByCardInfoId.put(dto.iD,dto);
                    }
                    
                    mapByCardEmployeeInfoId = CardEmployeeInfoRepository.getInstance().getByIds(cardEmployeeInfoIds).stream()
                            .collect(Collectors.toMap(dto->dto.iD,dto->dto,(e1,e2)->e1));
                    mapByCardEmployeeOfficeInfoDTOId = CardEmployeeOfficeInfoRepository.getInstance().getByIds(cardEmployeeOfficeInfoIds).stream()
                            .collect(Collectors.toMap(dto->dto.iD,dto->dto,(e1,e2)->e1));
                    mapByCardApprovalDTOId = cardApprovalDTOList.stream()
                            .collect(Collectors.toMap(dto->dto.iD,dto->dto,(e1,e2)->e1));
                    mapByEmployeeImagesDTOId = CardEmployeeImagesDAO.getInstance().getDTOs(cardEmployeeImagesIds).stream()
                            .collect(Collectors.toMap(dto->dto.iD,dto->dto,(e1,e2)->e1));
                    mapByReissueCardId = lostCardInfoDTOList.stream()
                            .collect(Collectors.toMap(dto->dto.reissueCardInfoId,dto->dto,(e1,e2)->e1));
                    for (int i = 0; i < data.size(); i++) {
                        Card_approval_mappingDTO card_approval_mappingDTO = data.get(i);
        %>
        <tr id='tr_<%=i%>'>
            <%@include file="card_approval_mappingSearchRow.jsp"%>
        </tr>
        <%}}%>
        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>