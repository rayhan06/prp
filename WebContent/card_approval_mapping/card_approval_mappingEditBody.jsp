<%@page import="login.LoginDTO" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="card_approval_mapping.CardApprovalModel" %>
<%@ page import="user.UserDTO" %>
<%@ page import="card_approval_mapping.Card_approval_mappingDTO" %>
<%@ page import="card_approval_mapping.Card_approval_mappingDAO" %>
<%@ page import="task_type_approval_path.TaskTypeApprovalPathRepository" %>
<%@ page import="lost_card_info.Lost_card_infoDTO" %>
<%@ page import="lost_card_info.Lost_card_infoDAO" %>
<%@ page import="files.FilesDAO" %>
<%@ page import="files.FilesDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="card_info.*" %>
<%@ page import="util.StringUtils" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="java.util.Map" %>
<%@ page import="police_verification.Police_verificationDTO" %>
<%@ page import="police_verification.Police_verificationDAO" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    long cardInfoId = Long.parseLong(request.getParameter("cardInfoId"));
    Lost_card_infoDTO lostCardInfoDTO = Lost_card_infoDAO.getInstance().getByReIssueCardInfoId(cardInfoId);
    Card_infoDTO lastCardInfoDTO = null;
    if (lostCardInfoDTO != null) {
        lastCardInfoDTO = CardInfoDAO.getInstance().getDTOFromID(lostCardInfoDTO.cardInfoId);
    }
    Card_approval_mappingDTO cardApprovalMappingDTO = Card_approval_mappingDAO.getInstance().getByCardInfoIdAndApproverEmployeeRecordId(cardInfoId, userDTO.employee_record_id);
    boolean hasNextApprover = TaskTypeApprovalPathRepository.getInstance().hasNextApprover(cardApprovalMappingDTO.taskTypeId, cardApprovalMappingDTO.sequence + 1);
    String formTitle = LM.getText(LC.CARD_INFO_ADD_CARD_INFO_ADD_FORMNAME, loginDTO);
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;

    String context = request.getContextPath() + "/";
    CardApprovalModel approvalModel = (CardApprovalModel) request.getAttribute("cardApprovalModel");
    if (approvalModel == null)
        approvalModel = new CardApprovalModel();
    if (lostCardInfoDTO != null && lastCardInfoDTO != null) {
        approvalModel.validationEndDate = lastCardInfoDTO.validationTo;
    }
    Card_infoDTO card_infoDTO = CardInfoDAO.getInstance().getDTOFromID(approvalModel.cardInfoId);
    int sequence = approvalModel.cardApprovalMappingDTO.sequence;
    boolean policeVerification = card_infoDTO.isPoliceVerificationRequired;
    String fileColumnName = "";
    FilesDAO filesDAO = new FilesDAO();
    String servletName = "Lost_card_infoServlet";
    List<Card_approval_mappingDTO> cardApprovalMappingDTOList = Card_approval_mappingDAO.getInstance().getAllApprovalDTOByCardInfoIdNotPending(cardInfoId);
    List<Long> cardApprovalIds = cardApprovalMappingDTOList.stream()
            .map(e -> e.cardApprovalId)
            .collect(Collectors.toList());
    Map<Long, CardApprovalDTO> mapByCardApprovalDTOId = CardApprovalRepository.getInstance()
            .getByIds(cardApprovalIds)
            .stream()
            .collect(Collectors.toMap(e -> e.iD, e -> e, (e1, e2) -> e1));
    Police_verificationDTO policeVerificationDTO = null;
    if (card_infoDTO.isPoliceVerificationRequired) {
        policeVerificationDTO = Police_verificationDAO.getInstance().getByCardInfoId(cardInfoId);
    }
%>
<style>
    .fly-in-from-down {
        animation: flyFromDown 1s ease-out;
    }

    @keyframes flyFromDown {
        0% {
            transform: translateY(200%);
        }
        100% {
            transform: translateY(0%);
        }
    }

    .top-section-font {
        color: #00a1d4;
        font-weight: 500;
    }

    .page-bg {
        background-color: #f9f9fb;
    }
</style>

<!-- begin:: Subheader -->
<div class="ml-4 mt-4">
    <div class="kt-subheader__main">
        <h2 class="kt-subheader__title" style="color: #00a1d4;">
            <i class="far fa-address-card"></i> <%=formTitle%>
        </h2>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content" id="kt_content">
    <div class="row">
        <div class="kt-portlet">
            <div class="kt-portlet__body m-4 page-bg">
                <div class="row">
                    <div class="col-12 row">
                        <div class="col-3">
                            <span class="btn btn-sm border-0 shadow"
                                  style="background-color: <%=CardCategoryEnum.getColor(card_infoDTO.cardCat)%>; color: white; border-radius: 8px;cursor: text">
                                <%=CatRepository.getInstance().getText(Language, "card", card_infoDTO.cardCat)%>
                            </span>
                            <%
                                if (lostCardInfoDTO != null) {
                            %>
                            &nbsp;&nbsp;
                            <span class="btn btn-sm border-0 shadow"
                                  style="background-color: darkred; color: white; border-radius: 8px;cursor: text">
                                        <%=LM.getText(LC.LOST_CARD_INFO_ADD_REISSUE_CARD, loginDTO)%>
                            </span>
                            <%
                                }
                            %>
                            <div style="height: 10px"></div>
                            <span class="btn btn-sm border-0 shadow"
                                  style="background-color: <%=card_infoDTO.isPoliceVerificationRequired ? "crimson" : "forestgreen"%>; color: white; border-radius: 8px;cursor: text">
                                        <%=card_infoDTO.isPoliceVerificationRequired ?
                                                LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_POLICE_VERIFICATION_IS_REQUIRED, loginDTO) :
                                                LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_POLICE_VERIFICATION_IS_NOT_REQUIRED, loginDTO)
                                        %>
                            </span>
                        </div>
                        <div class="col-6 text-center">
                            <img width="13%"
                                 src="<%=context%>assets/static/parliament_logo.png"
                                 alt="logo"
                                 class="logo-default"
                            />
                            <h5 class="text-center mt-3 top-section-font">
                                <%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
                            </h5>
                            <h5 class="text-center mt-2 top-section-font">
                                <%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                            </h5>
                        </div>
                        <div class="col-3 text-right">
                            <img src='<%=approvalModel.photoSrc%>' alt="employee photo" width="45%" id="photo">
                        </div>
                    </div>
                </div>
                <div class="row my-5">
                    <%
                        if (cardApprovalMappingDTO.cardApprovalStatusCat == ApprovalStatus.DISSATISFIED.getValue()) {
                    %>
                    <div class="col-12 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5"><%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_REASON_FOR_DISSATISFACTION, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="h5" id="rejection_reason" style="color: red"><%=card_infoDTO.comment%>
                            </label>
                        </div>
                    </div>
                    <%
                        }
                    %>
                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5"><%=LM.getText(LC.CARD_INFO_ADD_APPLICANT_NAME, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="h5" id="name"><%=approvalModel.name%>
                            </label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5">
                                <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_DATEOFBIRTH, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="h5" id="dateOfBirth"><%=approvalModel.dateOfBirth%>
                            </label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5">
                                <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_POST, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="h5" id="post"><%=approvalModel.post%>
                            </label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5">
                                <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_OFFICE, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label id="office" class="h5"><%=approvalModel.office%>
                            </label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5">
                                <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_FATHERNAMEENG, loginDTO).replaceAll("\\(.+\\)", "")%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="h5" id="fatherName"><%=approvalModel.fatherName%>
                            </label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5">
                                <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_MOTHERNAMEENG, loginDTO).replaceAll("\\(.+\\)", "")%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="h5" id="motherName"><%=approvalModel.motherName%>
                            </label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5">
                                <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NID, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="h5" id="nidNumber"><%=approvalModel.nidNumber%>
                            </label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5">
                                <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PRESENTADDRESS_ENG, loginDTO).replaceAll("\\(.+\\)", "")%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="h5" id="presentAddress"><%=approvalModel.presentAddress%>
                            </label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5">
                                <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PERMANENTADDRESS_ENG, loginDTO).replaceAll("\\(.+\\)", "")%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="h5" id="permanentAddress"><%=approvalModel.permanentAddress%>
                            </label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5">
                                <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_HEIGHT, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="h5" id="height"><%=approvalModel.height%>
                            </label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5">
                                <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_IDENTIFICATION_MARK, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="h5" id="identificationSign"><%=approvalModel.identificationSign%>
                            </label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5">
                                <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_BLOODGROUP, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="h5" id="bloodGroup"><%=approvalModel.bloodGroup%>
                            </label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5">
                                <%=LM.getText(LC.HR_MANAGEMENT_OTHERS_OFFICE_PHONE_NUMBER, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="h5" id="mobileNumber"><%=approvalModel.mobileNumber%>
                            </label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5">
                                <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_SIGNATURE, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <img src='<%=approvalModel.signatureSrc%>' alt="employee signature" id="signature"
                                 width="30%">
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5">
                                <%=LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_NID_FRONT_SIDE, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <img src='<%=approvalModel.nidFrontSrc%>' alt="employee nid front side" id="nidFront"
                                 width="60%">
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5">
                                <%=LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_NID_BACK_SIDE, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <img src='<%=approvalModel.nidBackSrc%>' alt="employee nid back side" id="nidBack"
                                 width="60%">
                        </div>
                    </div>

                    <%
                        if (policeVerificationDTO != null) {
                    %>
                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5">
                                <%=LM.getText(LC.CARD_INFO_POLICE_VERIFICATION_DOCUMENT, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <%
                                {
                                    fileColumnName = "police_verification";
                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(policeVerificationDTO.fileDropzone);
                            %>
                            <%@include file="../pb/dropzoneEditor2.jsp" %>
                            <%
                                }
                            %>
                        </div>
                    </div>
                    <%
                        }
                    %>

                    <%
                        if (lostCardInfoDTO != null) {
                    %>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5">
                                <%=LM.getText(LC.LOST_CARD_INFO_ADD_GD_COPY, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <%
                                {
                                    fileColumnName = "gdCopyFileDropzone";
                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(lostCardInfoDTO.gdCopyFileDropzone);
                            %>
                            <%@include file="../pb/dropzoneEditor2.jsp" %>
                            <%
                                }
                            %>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5">
                                <%=LM.getText(LC.LOST_CARD_INFO_ADD_MONEY_RECEIPT_COPY, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <%
                                {
                                    fileColumnName = "moneyReceiptFileDropzone";
                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(lostCardInfoDTO.moneyReceiptFileDropzone);
                            %>
                            <%@include file="../pb/dropzoneEditor2.jsp" %>
                            <%
                                }
                            %>
                        </div>
                    </div>
                    <%
                        }
                    %>
                </div>
                <%
                    if (approvalModel.cardApprovalMappingDTO.cardApprovalStatusCat == ApprovalStatus.PENDING.getValue()) {
                %>
                <div class="row my-5" align="right">
                    <div class="col-12">

                        <%
                            if (hasNextApprover) {
                        %>
                        <% if (policeVerification && sequence == 2) {

                        %>
                        <button type="button" class="btn btn-success shadow ml-2" style="border-radius: 8px;"
                                id="police_verification_btn" onclick="policeVerification()">
                            <%=LM.getText(LC.CARD_APPROVAL_POLICE_VERIFICATION, loginDTO)%>
                        </button>
                        <%
                        } else {
                        %>
                        <button type="button" class="btn btn-success shadow ml-2" style="border-radius: 8px;"
                                id="approve_btn"
                                onclick="approveCard()"><%=LM.getText(LC.CARD_APPROVAL_MAPPING_APPROVAL, loginDTO)%>
                        </button>
                        <%
                            }
                        } else {
                        %>
                        <button type="button" class="btn btn-success shadow ml-2" style="border-radius: 8px;"
                                id="modal_btn"
                                onclick="openModal()"><%=LM.getText(LC.CARD_APPROVAL_MAPPING_APPROVAL, loginDTO)%>
                        </button>
                        <%
                            }
                        %>
                        <button type="button" class="btn btn-danger shadow ml-2" style="border-radius: 8px;"
                                id="reject_btn"
                                onclick="reject()"><%=LM.getText(LC.CARD_APPROVAL_MAPPING_DO_NOT_APPOVE, loginDTO)%>
                        </button>
                    </div>
                </div>
                <%
                    }
                %>

                <div id="card_requester_info">
                    <div class="col-md-12">
                        <h3><%=LM.getText(LC.CARD_INFO_REQUESTER_INFORMATION, loginDTO)%>
                        </h3>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.CARD_INFO_NAME, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.CARD_INFO_DESIGNATION_OFFICE, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.CARD_INFO_REQUEST_CREATE_DATE_TIME, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <%
                                String name, officeName, designation;
                                if (card_infoDTO.employeeRecordsId == card_infoDTO.insertedBy) {
                                    name = approvalModel.name;
                                    officeName = approvalModel.office;
                                    designation = approvalModel.post;
                                } else {
                                    Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(card_infoDTO.insertedBy);
                                    name = isLanguageEnglish ? employeeRecordsDTO.nameEng : employeeRecordsDTO.nameBng;
                                    EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(card_infoDTO.insertedBy);
                                    Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
                                    officeName = officeUnitsDTO == null ? "" : (isLanguageEnglish ? officeUnitsDTO.unitNameEng : officeUnitsDTO.unitNameBng);
                                    OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
                                    designation = officeUnitOrganograms == null ? "" : (isLanguageEnglish ? officeUnitOrganograms.designation_eng : officeUnitOrganograms.designation_bng);
                                }

                            %>
                            <tr>
                                <td><%=name%>
                                </td>
                                <td><b><%=designation%>
                                </b><br><%=officeName%>
                                </td>
                                <td><%=StringUtils.convertToDateAndTime(isLanguageEnglish, card_infoDTO.insertionTime)%>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="approval_info_div">
                    <div class="col-md-12">
                        <h3><%=LM.getText(LC.CARD_INFO_APPROVAL_INFORMATION, loginDTO)%>
                        </h3>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.CARD_INFO_APPROVER_OFFICE_INFORMATION, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.CARD_INFO_APPROVAL_STATUS, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.CARD_INFO_APPROVAL_DATE_TIME, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <%
                                for (Card_approval_mappingDTO approvalMappingDTO : cardApprovalMappingDTOList) {
                                    CardApprovalDTO cardApprovalDTO = mapByCardApprovalDTOId.get(approvalMappingDTO.cardApprovalId);
                            %>
                            <tr>
                                <td>
                                    <b><%=isLanguageEnglish ? cardApprovalDTO.officeUnitEng : cardApprovalDTO.officeUnitBng%>
                                    </b></td>
                                <td>
                                    <span class="btn btn-sm border-0 shadow"
                                          style="background-color: <%=ApprovalStatus.getColor(approvalMappingDTO.cardApprovalStatusCat)%>; color: white; border-radius: 8px;cursor: text">
                                        <%=CatRepository.getInstance().getText(Language, "card_approval_status", approvalMappingDTO.cardApprovalStatusCat)%>
                                    </span>
                                </td>
                                <td>
                                    <%=StringUtils.convertToDateAndTime(isLanguageEnglish, approvalMappingDTO.lastModificationTime)%>
                                </td>
                            </tr>
                            <%
                                }
                            %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="approval_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h4 class="caption" style="color: #56b2cf; margin-left: 10px">
                    <%=LM.getText(LC.CARD_APPROVAL_MAPPING_APPROVAL, loginDTO)%>
                </h4>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body modal-lg">
                <div class="row my-5">
                    <div class="col-12">
                        <form class="form-horizontal text-right" id="permanentCardForm">
                            <div class="form-group row ">
                                <label class="col-3 col-form-label">
                                    <%=LM.getText(LC.CARD_APPROVAL_MAPPING_VALIDATION_START_DATE, loginDTO)%>
                                    <span class="required">*</span>
                                </label>
                                <div class="col-9"
                                     id='validation_date_startdiv'>
                                    <jsp:include page="/date/date.jsp">
                                        <jsp:param name="DATE_ID"
                                                   value="validation-start-date-js"></jsp:param>
                                        <jsp:param name="LANGUAGE"
                                                   value="<%=Language%>"></jsp:param>
                                    </jsp:include>
                                    <input type='hidden' class='form-control'
                                           id='validation-start-date'
                                           name='validationStartDate' value=''
                                           tag='pb_html'/>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label class="col-3 col-form-label">
                                    <%=LM.getText(LC.CARD_APPROVAL_MAPPING_VALIDATION_END_DATE, loginDTO)%>
                                    <span class="required">*</span>
                                </label>
                                <div class="col-9"
                                     id='validation_date_enddiv'>
                                    <jsp:include page="/date/date.jsp">
                                        <jsp:param name="DATE_ID"
                                                   value="validation-end-date-js"></jsp:param>
                                        <jsp:param name="LANGUAGE"
                                                   value="<%=Language%>"></jsp:param>
                                        <jsp:param name="END_YEAR"
                                                   value="2050"></jsp:param>
                                    </jsp:include>
                                    <input type='hidden' class='form-control'
                                           id='validation-end-date'
                                           name='validationEndDate' value=''
                                           tag='pb_html'/>
                                </div>
                            </div>
                            <div class="row my-5" align="right">
                                <div class="col-12">
                                    <button type="button" class="btn btn-danger shadow" style="border-radius: 8px;"
                                            data-dismiss="modal"
                                    ><%=LM.getText(LC.CLOSE_CLOSE, loginDTO)%>
                                    </button>
                                    <button class="btn btn-success shadow ml-2" style="border-radius: 8px;"
                                            onclick="submitValidate()"
                                            type="button"><%=LM.getText(LC.CARD_INFO_ADD_CARD_INFO_SUBMIT_BUTTON, loginDTO)%>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display: none">
    <form class="form-horizontal text-right"
          action="Police_verificationServlet"
          id="policeVerificationForm" name="policeVerificationForm" method="GET"
          enctype="multipart/form-data">
        <input type="hidden" name="actionType" value="getAddPage">
        <input type="hidden" name="cardInfoId" value="<%=cardInfoId%>">
    </form>
</div>

<script>
    $(document).ready(function () {
        setDateByTimestampAndId('validation-start-date-js', <%=approvalModel.validationStartDate%>);
        setDateByTimestampAndId('validation-end-date-js', <%=approvalModel.validationEndDate%>);
        $('#validation-start-date-js').on('datepicker.change', () => {
            setMinDateById('validation-end-date-js', getDateStringById('validation-start-date-js'));
        });
    });
    $('#approval_modal').on('show.bs.modal', function () {

    });

    function openModal() {
        $("#approval_modal").modal();
    }

    function PreprocessBeforeSubmiting() {
        $('#validation-start-date').val(getDateStringById('validation-start-date-js'));
        $('#validation-end-date').val(getDateStringById('validation-end-date-js'));
        return dateValidator('validation-start-date-js', true, {
            'errorEn': 'Enter Validation Start Date!',
            'errorBn': 'মেয়াদ শুরুর তারিখ প্রবেশ করুন!'
        }) && dateValidator('validation-end-date-js', true, {
            'errorEn': 'Enter Validation End Date!',
            'errorBn': 'মেয়াদ শেষের তারিখ প্রবেশ করুন!'
        });
    }

    function reject() {
        let msg = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_REASON_FOR_DISSATISFACTION, loginDTO)%>';
        let placeHolder = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_TYPE_DISSATISFACTION_REASON_HERE, loginDTO)%>';
        let confirmButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_YES_DISSATISFIED, loginDTO)%>';
        let cancelButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARD_APPROVAL_MAPPING_CANCEL_BUTTON, loginDTO)%>';
        let emptyReasonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_PLEASE_WRITE_DISSATISFACTION_REASON, loginDTO)%>';
        dialogMessageWithTextBox(msg, placeHolder, confirmButtonText, cancelButtonText, emptyReasonText, (reason) => {
            submitAjaxByData({reject_reason: reason},"Card_approval_mappingServlet?actionType=ajax_rejectCard&cardInfoId=<%=cardInfoId%>");
        });
    }

    function approveCard() {
        let msg = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_ARE_YOU_SATISFIED, loginDTO)%>';
        let confirmButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_YES_SATISFIED, loginDTO)%>';
        let cancelButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARD_APPROVAL_MAPPING_CANCEL_BUTTON, loginDTO)%>';
        messageDialog('', msg, 'success', true, confirmButtonText, cancelButtonText, () => {
            submitAjaxByData(null,"Card_approval_mappingServlet?actionType=ajax_approved_card&cardInfoId=<%=cardInfoId%>");
        });
    }

    function buttonStateChange(value) {
        $(':button').prop('disabled', value);
    }

    function policeVerification() {
        $('#reject_btn').prop("disabled", true);
        $('#police_verification_btn').prop("disabled", true);
        $('#policeVerificationForm').submit();
    }

    function submitValidate() {
        buttonStateChange(true);
        if (PreprocessBeforeSubmiting()) {
            let data = {
                validationStartDate: $('#validation-start-date').val(),
                validationEndDate: $('#validation-end-date').val()
            };
            submitAjaxByData(data,"Card_approval_mappingServlet?actionType=ajax_approvedCard&cardInfoId=<%=cardInfoId%>");
        } else {
            buttonStateChange(false);
        }
    }
</script>