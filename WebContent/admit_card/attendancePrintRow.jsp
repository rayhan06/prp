<%@page pageEncoding="UTF-8" %>
<%@page import="admit_card.Admit_cardDTO" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="job_applicant_photo_signature.Job_applicant_photo_signatureDAO" %>
<%@ page import="files.FilesDTO" %>
<%@ page import="files.FilesDAO" %>
<%@ page import="java.util.List" %>
<%@ page import="admit_card.AttendanceHelperDTO" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="pb.Utils" %>
<%@ page import="org.jsoup.Jsoup" %>
<%@ page import="org.jsoup.safety.Whitelist" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);
    Admit_cardDTO admit_cardDTO = (Admit_cardDTO) request.getAttribute("admit_cardDTO");
    List<AttendanceHelperDTO> attendanceDTOs = (List<AttendanceHelperDTO>) request.getAttribute("attendanceDTOs");
    if (attendanceDTOs == null) {
        attendanceDTOs = new ArrayList<>();

    }

    String context2 = "../../.." + request.getContextPath() + "/";

    Recruitment_job_descriptionDTO recruitment_job_descriptionDTO =
            (Recruitment_job_descriptionDTO) request.getAttribute("jobDescriptionDTO");
    if (recruitment_job_descriptionDTO == null) {
        recruitment_job_descriptionDTO = new Recruitment_job_descriptionDTO();
    }


    Job_applicant_photo_signatureDAO photo_signatureDAO =
            (Job_applicant_photo_signatureDAO) request.getAttribute("photo_signatureDAO");
    if (photo_signatureDAO == null) {
        photo_signatureDAO = new Job_applicant_photo_signatureDAO();
    }

    FilesDAO filesDAO =
            (FilesDAO) request.getAttribute("filesDAO");
    if (filesDAO == null) {
        filesDAO = new FilesDAO();
    }


%>


<main class="html-2-pdfwrapper my-2 mx-3">
    <div class=" row pt-2 px-2 rounded d-flex align-items-center m-0">
        <div class="col-4">
            <img
                    src="<%=context2%>assets/images/bangladesh-parliament-logo.png"
                    alt="logo"
                    class=" "
                    style="width: 19%;"
            />
        </div>
        <div class="col-4 text-center">
            <b><p style="font-size: 17px;"> বাংলাদেশ জাতীয় সংসদ সচিবালয় </p></b>
            <p class=""> নিয়োগ পরীক্ষা - ২০২১ </p>
            <p class=""> উপস্থিতি তালিকা </p>
        </div>
        <div class="col-4 text-right">
            <img
                    src="<%=context2%>assets/images/mujib_borsho_logo.png"
                    alt="logo" class=" "
                    style="width: 25%;"
            />
        </div>
    </div>
    <%--    <div class="row text-center" >--%>
    <%--        <div class="col-12">--%>
    <%--            কেন্দ্রঃ <%=Jsoup.clean(admit_cardDTO.placeOfExam, Whitelist.simpleText())%>--%>
    <%--        </div>--%>

    <%--    </div>--%>
    <div class="row text-center">
        <div class="col-12">
            <p>
                কেন্দ্রঃ <%=Jsoup.clean(attendanceDTOs.get(0).examVenue, Whitelist.simpleText())%>
            </p>
            <p style="font-size: 17px;">
                পদের নামঃ <%=recruitment_job_descriptionDTO.jobTitleBn %>
            </p>
        </div>
    </div>


    <%--    <div class="row mt-5">--%>
    <%--        <div class="col-12">--%>
    <%--            <h4 class="mb-3 text-center">--%>
    <%--                পদের নামঃ <%=recruitment_job_descriptionDTO.jobTitleBn %>--%>
    <%--            </h4>--%>
    <%--        </div>--%>
    <%--    </div>--%>


    <div class="mt-5">
        <div class="table-responsive">
            <table id="" class="table table-bordered">
                <thead class="text-center">
                <tr>
                    <th> রোল</th>
                    <th > নাম ও পিতার নাম</th>
                    <th style="width: 210px">ছবি</th>
                    <th>স্বাক্ষর</th>
                </tr>
                </thead>
                <tbody>

                <%
                    for (AttendanceHelperDTO dto : attendanceDTOs) {
                %>
                <tr>
                    <td style="text-align: center">
                        <%
                            String rollNo = Utils.getDigitBanglaFromEnglish(dto.rollNo);
                        %>
                        <%=rollNo%>
                    </td>
                    <td>
                        <%
                            String firstLine = "প্রার্থীর নামঃ " + dto.name;
                            String secondLine = "পিতাঃ " + dto.fatherName;
                        %>
                        <%=firstLine%>
                        <br>
                        <%=secondLine%>
                    </td>
                    <td class="text-center">
                        <%
                            if (dto.fileId != 0) {
                                List<FilesDTO> filesDTOsOfPhoto = filesDAO.getDTOsByFileID(dto.fileId);
                                FilesDTO signatureFile = filesDTOsOfPhoto.isEmpty() ? null : filesDTOsOfPhoto.get(0);
                                if (signatureFile != null && signatureFile.fileBlob != null) {
                                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(signatureFile.fileBlob);

                                    out.println("<img width='200' src='" + "data:" + signatureFile.fileTypes + ";base64," + new String(encodeBase64)
                                            + "'>");
                                }

                            }
                        %>
                    </td>
                    <td></td>
                </tr>
                <%
                    }
                %>

                </tbody>
            </table>
        </div>
    </div>


    <div class="mt-5 page-break">

    </div>
</main>



