<%@page import="admit_card.Admit_cardDAO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>

<%@ page language="java" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="util.RecordNavigator" %>


<%@ page import="java.util.List" %>
<%@ page import="parliament_job_applicant.Parliament_job_applicantDTO" %>
<%@ page import="javafx.util.Pair" %>
<%@ page import="recruitment_job_specific_exam_type.JobSpecificExamTypeDao" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="admit_card.Admit_cardDTO" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDTO" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="admit_card.AttendanceHelperDTO" %>
<%@ page import="recruitment_exam_venue.Recruitment_exam_venueDTO" %>
<%@ page import="recruitment_exam_venue.Recruitment_exam_venueRepository" %>
<%@ page import="recruitment_exam_venue.RecruitmentExamVenueItemDTO" %>
<%@ page import="recruitment_exam_venue.RecruitmentExamVenueItemRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@page pageEncoding="UTF-8" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);

    List<Job_applicant_applicationDTO> job_applicant_applicationDTOS = (List<Job_applicant_applicationDTO>) request.getAttribute("job_applicant_applicationDTOS");
    if (job_applicant_applicationDTOS == null) {
        job_applicant_applicationDTOS = new ArrayList<>();
    }


    String navigator2 = SessionConstants.NAV_ADMIT_CARD;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();

    String context = "../../.." + request.getContextPath() + "/";

    int defaultRowCount = 10;
    List<AttendanceHelperDTO> attendanceDTOs = new ArrayList<>();

%>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Form</title>

    <style>
        @media print {
            .page-break {
                page-break-after: always;
            }

            main{
                margin-left: 0px!important;
                margin-right: 0px!important;
            }
        }
    </style>

</head>
<body id="random" name="random">

<%
    try {



        List<Job_applicant_applicationDTO> tempJobApp =  job_applicant_applicationDTOS;

        List<Long> distinctSeatPlans =  tempJobApp.stream()
                .map(e -> e.seatPlanChildId)
                .distinct()
                .collect(Collectors.toList());

        for(Long distinctSeatPlan : distinctSeatPlans){

            job_applicant_applicationDTOS = tempJobApp.stream()
                    .filter(e -> e.seatPlanChildId == distinctSeatPlan)
                    .collect(Collectors.toList());
            attendanceDTOs = new ArrayList<>();
            if (job_applicant_applicationDTOS != null) {
                AttendanceHelperDTO dto;
                Parliament_job_applicantDTO row;
                int size = job_applicant_applicationDTOS.size();
                int i ;
                System.out.println("data not null and size = " + size + " data = " + job_applicant_applicationDTOS);
                for (i = 0; i < size; i++) {
                    dto = new AttendanceHelperDTO();

                    String examVenueInfo = "";
                    if(job_applicant_applicationDTOS.get(i) != null ){
                        Job_applicant_applicationDTO jobAppApp = job_applicant_applicationDTOS.get(i);
                        Recruitment_exam_venueDTO recruitment_exam_venueDTO = Recruitment_exam_venueRepository.getInstance()
                                .getRecruitment_exam_venueDTOByiD(jobAppApp.recruitmentExamVenueId);
                        RecruitmentExamVenueItemDTO recruitmentExamVenueItemDTO = RecruitmentExamVenueItemRepository
                                .getInstance().getRecruitmentExamVenueItemDTOByiD(jobAppApp.recruitmentExamVenueItemId);
                        if(recruitmentExamVenueItemDTO != null){
                            examVenueInfo += "রুম নম্বরঃ "+recruitmentExamVenueItemDTO.roomNo+", ফ্লোরঃ "+recruitmentExamVenueItemDTO.floor+", ভবনঃ "+ recruitmentExamVenueItemDTO.building+", ";
                        }
                        if(recruitment_exam_venueDTO != null){
                            examVenueInfo +=  recruitment_exam_venueDTO.nameBn;
                        }
                    }

                    dto.name = job_applicant_applicationDTOS.get(i).applicant_name_bn;
                    dto.fatherName = job_applicant_applicationDTOS.get(i).father_name;
                    dto.rollNo = job_applicant_applicationDTOS.get(i).rollNumber;
                    dto.fileId = job_applicant_applicationDTOS.get(i).photo_file_id;
                    dto.examVenue = examVenueInfo ;
                    attendanceDTOs.add(dto);
                    if(attendanceDTOs.size() == defaultRowCount){
                        request.setAttribute("attendanceDTOs", attendanceDTOs);
%>

<jsp:include page="./attendancePrintRow.jsp">
    <jsp:param name="pageName" value="searchrow"/>
    <jsp:param name="rownum" value="<%=i%>"/>
</jsp:include>

<%
            attendanceDTOs = new ArrayList<>();
        }
    }

    if(attendanceDTOs.size() > 0){
        request.setAttribute("attendanceDTOs", attendanceDTOs);

%>
<jsp:include page="./attendancePrintRow.jsp">
    <jsp:param name="pageName" value="searchrow"/>
    <jsp:param name="rownum" value="<%=i%>"/>
</jsp:include>
<%
                }
            }
        }
    } catch (Exception ex) {
        System.out.println("data not null and size = " + ex);
    }
%>

<script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="<%=context%>assets/scripts/util1.js"></script>
<script>


    $(document).ready(function () {
    });


</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>
			