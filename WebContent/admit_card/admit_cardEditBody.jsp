<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="admit_card.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDAO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="config.GlobalConfigurationRepository" %>
<%@ page import="config.GlobalConfigConstants" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_assign.EmployeeSearchIds" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameRepository" %>

<%
    String template = (GlobalConfigurationRepository.
            getGlobalConfigDTOByID(GlobalConfigConstants.ADMIT_CARD_TEMPLATE).value);


    Admit_cardDTO admit_cardDTO;
    admit_cardDTO = (Admit_cardDTO) request.getAttribute("admit_cardDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    if (admit_cardDTO == null) {
        admit_cardDTO = new Admit_cardDTO();

    }
    System.out.println("admit_cardDTO = " + admit_cardDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.ADMIT_CARD_ADD_ADMIT_CARD_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;

    String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;


    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");


//	Employee_recordsDTO employee_recordsDTO = null;
//	if(actionName.equals("edit")) {
//		employee_recordsDTO = new Employee_recordsDAO().getEmployee_recordsDTOByID(admit_cardDTO.employeeRecordsId);
//	}
%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important; margin-bottom: -18px">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Admit_cardServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="formSubmit()">
            <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>' value='<%=admit_cardDTO.iD%>'
                   tag='pb_html'/>
            <input type='hidden' class='form-control' name='insertedByUserId' id='insertedByUserId_hidden_<%=i%>'
                   value=<%=actionName.equals("edit")?("'" + admit_cardDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
            <input type='hidden' class='form-control' name='insertionDate' id='insertionDate_hidden_<%=i%>'
                   value=<%=actionName.equals("edit")?("'" + admit_cardDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
            <input type='hidden' class='form-control' name='modifiedBy' id='modifiedBy_hidden_<%=i%>'
                   value=<%=actionName.equals("edit")?("'" + admit_cardDTO.modifiedBy + "'"):("'" + "" + "'")%> tag='pb_html'/>
            <input type='hidden' class='form-control' name='isDeleted' id='isDeleted_hidden_<%=i%>'
                   value=<%=actionName.equals("edit")?("'" + admit_cardDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
            <input type='hidden' class='form-control' name='lastModificationTime'
                   id='lastModificationTime_hidden_<%=i%>'
                   value=<%=actionName.equals("edit")?("'" + admit_cardDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 control-label text-md-right">
                                            <%=LM.getText(LC.RECRUITMENT_TEST_NAME_ADD_RECRUITMENT_TEST_NAME_ADD_FORMNAME, loginDTO)%><span
                                                class="required">*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='recruitmentTestNameId'
                                                    id='recruitmentTestNameId'
                                                    onchange="onRecruitmentTestNameChange(this)" tag='pb_html'>
                                                <%= Recruitment_test_nameRepository.getInstance().buildOptions(Language, admit_cardDTO.recruitmentTestName) %>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.ADMIT_CARD_ADD_RECRUITMENTJOBDESCRIPTIONTYPE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <select onchange="jobChange()" class='form-control'
                                                    name='recruitmentJobDescriptionType'
                                                    id='recruitmentJobDescriptionType_select_<%=i%>' tag='pb_html'>
<%--                                                <%--%>

<%--                                                    Recruitment_job_descriptionDAO recruitment_job_descriptionDAO = new Recruitment_job_descriptionDAO();--%>
<%--                                                    List<Recruitment_job_descriptionDTO> recruitment_job_descriptionDTOS = recruitment_job_descriptionDAO.--%>
<%--                                                            getAllRecruitment_job_descriptionForCreatingAdmitCard();--%>


<%--                                                    if (Language.equalsIgnoreCase("english")) { %>--%>
<%--                                                <option value="0">Please Select</option>--%>
<%--                                                <% } else { %>--%>
<%--                                                <option value="0">বাছাই করুন</option>--%>
<%--                                                <% }--%>

<%--                                                    for (int k = 0; k < recruitment_job_descriptionDTOS.size(); k++) {--%>
<%--                                                        Recruitment_job_descriptionDTO job_descriptionDTO = recruitment_job_descriptionDTOS.get(k);--%>
<%--                                                        String optionText = "";--%>
<%--                                                        long optionValue = job_descriptionDTO.iD;--%>
<%--                                                        if (Language.equalsIgnoreCase("english")) {--%>
<%--                                                            optionText = job_descriptionDTO.jobTitleEn;--%>
<%--                                                        } else {--%>
<%--                                                            optionText = job_descriptionDTO.jobTitleBn;--%>
<%--                                                        }--%>

<%--                                                        if (actionName.equals("edit")) {--%>
<%--                                                            if (optionValue == admit_cardDTO.recruitmentJobDescriptionType) { %>--%>
<%--                                                <option value=<%=optionValue%> selected><%=optionText%>--%>
<%--                                                </option>--%>
<%--                                                <% } else { %>--%>
<%--                                                <option value= <%=optionValue%>><%=optionText%>--%>
<%--                                                </option>--%>
<%--                                                <% }--%>
<%--                                                } else { %>--%>
<%--                                                <option value= <%=optionValue%>><%=optionText%>--%>
<%--                                                </option>--%>
<%--                                                <%--%>
<%--                                                        }--%>

<%--                                                    }--%>
<%--                                                %>--%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.ADMIT_CARD_ADD_PLACEOFEXAM, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='placeOfExam'
                                                   id='placeOfExam_text_<%=i%>'
                                                   value=<%=actionName.equals("edit")?("'" + admit_cardDTO.placeOfExam + "'"):("'" + "" + "'")%>   tag='pb_html'/>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.ADMIT_CARD_ADD_DATEOFEXAM, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="date-of-exam-js"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>

                                            <input type='hidden' class='form-control' id='dateOfExam_date_<%=i%>'
                                                   name='dateOfExam' value='' tag='pb_html'/>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.PI_PACKAGE_VENDOR_EDIT_PI_PACKAGE_VENDOR_EDIT_FORMNAME, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <jsp:include page="/time/time.jsp">
                                                <jsp:param name="TIME_ID" value="jsTimeIn"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                            </jsp:include>

                                            <input type='hidden' class='form-control' name='timeOfExam'
                                                   id='timeOfExam_text_<%=i%>' value='' tag='pb_html'/>


                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CHILDREN_PIPACKAGEVENDORID, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <jsp:include page="/time/time.jsp">
                                                <jsp:param name="TIME_ID" value="jsTimeEnd"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                            </jsp:include>

                                            <input type='hidden' class='form-control' name='endTimeOfExam'
                                                   id='endTimeOfExam_text_<%=i%>' value='' tag='pb_html'/>


                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.ADMIT_CARD_ADD_EMPLOYEERECORDSID, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <input type="hidden" class='form-control' name='employeeRecordsId'
                                                   id='employeeRecordsId'
                                                   value=''>

                                            <input type='hidden' class='form-control' name='employeeRecordsType'
                                                   id='employeeRecordsType' tag='pb_html'/>
                                            <button type="button"
                                                    class="btn btn-primary btn-block shadow btn-border-radius mb-3"
                                                    id="tagEmp_modal_button">
                                                <%=LM.getText(LC.HM_SELECT, userDTO)%>
                                            </button>
                                            <table class="table table-bordered table-striped">
                                                <thead></thead>

                                                <tbody id="tagged_emp_table">
                                                <tr style="display: none;">
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        <button type="button" class="btn btn-sm delete-trash-btn"
                                                                onclick="remove_containing_row(this,'tagged_emp_table');">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>

                                                <% if (actionName.equals("edit")) { %>
                                                <tr>
                                                    <td><%=Employee_recordsRepository.getInstance()
                                                            .getById(admit_cardDTO.employeeRecordsId).employeeNumber%>
                                                    </td>
                                                    <td>
                                                        <%=(admit_cardDTO.signatory_name)%>
                                                    </td>

                                                    <%
                                                        String postName = isLanguageEnglish ? (admit_cardDTO.signatory_post_name + ", " + admit_cardDTO.signatory_office_unit_name)
                                                                : (admit_cardDTO.signatory_post_name_bn + ", " + admit_cardDTO.signatory_office_unit_name_bn);
                                                    %>

                                                    <td><%=postName%>
                                                    </td>
                                                    <td id='<%=admit_cardDTO.employeeRecordsId%>_td_button'>
                                                        <button type="button"
                                                                class="btn btn-sm delete-trash-btn"
                                                                onclick="remove_containing_row(this,'tagged_emp_table');">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <%}%>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.ADMIT_CARD_SEARCH_RULES, loginDTO)%>

                                        </label>
                                        <div class="col-md-9">
														<textarea
                                                                rows="8"
                                                                class='form-control'
                                                                name='rules'
                                                                id='rules_text_<%=i%>'
                                                                tag='pb_html'><%=actionName.equals("edit") ? (admit_cardDTO.rules) : template%>
														</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.ADMIT_CARD_SEARCH_RANGE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9 d-flex  align-items-center">
                                            <%
                                                String checked = admit_cardDTO.range ? "checked" : "";
                                            %>
                                            <input type="checkbox" id="range" class='form-control-sm'
                                                   name="range" <%=checked%> >


                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <label class="col-md-3  rangeClass">

                                        </label>
                                        <label id="rangeSuggestion" class="col-md-9  rangeClass">

                                        </label>
                                    </div>
                                    <div class="form-group row ">
                                        <label class="col-md-3 col-form-label text-md-right rangeClass">
                                            <%=LM.getText(LC.ADMIT_CARD_ADD_ROLLNOFROM, loginDTO)%>

                                        </label>
                                        <div class="col-md-9 rangeClass">
                                            <input type='number' class='form-control' name='rollNoFrom'
                                                   id='rollNoFrom_number_<%=i%>' min='0' max='1000000'
                                                   value=<%=actionName.equals("edit")?("'" + admit_cardDTO.rollNoFrom + "'"):("'" + 0 + "'")%>  tag='pb_html'>

                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <label class="col-md-3 col-form-label text-md-right rangeClass">
                                            <%=LM.getText(LC.ADMIT_CARD_ADD_ROLLNOTO, loginDTO)%>

                                        </label>
                                        <div class="col-md-9 rangeClass">
                                            <input type='number' class='form-control' name='rollNoTo'
                                                   id='rollNoTo_number_<%=i%>' min='0' max='1000000'
                                                   value=<%=actionName.equals("edit")?("'" + admit_cardDTO.rollNoTo + "'"):("'" + 0 + "'")%>  tag='pb_html'>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 text-right">
                        <div class="form-actions mt-3">
                            <a id="cancel-btn" class="btn btn-sm cancel-btn text-white shadow"
                               href="<%=request.getHeader("referer")%>">
                                <%=LM.getText(LC.ADMIT_CARD_ADD_ADMIT_CARD_CANCEL_BUTTON, loginDTO)%>
                            </a>
                            <button id="submit-btn" class="btn btn-sm submit-btn text-white shadow ml-2" type="submit">
                                <%=LM.getText(LC.ADMIT_CARD_ADD_ADMIT_CARD_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">
    let prevSelectedItem;
    let lang = '<%=Language%>';
    $(document).ready(function () {

        <% if(actionName.equalsIgnoreCase("edit")){ %>
            <%--prevSelectedItem = '<%=admit_cardDTO.recruitmentJobDescriptionType%>';--%>
            onRecruitmentTestNameChange(document.getElementById('recruitmentTestNameId'));
        <% }%>

        select2SingleSelector("#recruitmentJobDescriptionType_select_0",'<%=Language%>');
        select2SingleSelector("#recruitmentTestNameId",'<%=Language%>');
        //fetchAllJobs(prevSelectedItem);

        dateTimeInit("<%=Language%>");
        $('#timeOfExam_time_test_0').keypress(function (e) {
            e.preventDefault();
        });

        $.validator.addMethod('jobSelector', function (value, element) {
            return value != 0;
        });

        $.validator.addMethod('empSelector', function (value, element) {
            return value != 0;
        });

        // employeeRecordsId  $('#range').is(":checked")

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                recruitmentJobDescriptionType: {
                    required: true,
                    jobSelector: true,
                },
                // employeeRecordsId: {
                //     required: true,
                //     empSelector: true,
                // },
                // dateOfExam: "required",
                // timeOfExamTest: "required",
                placeOfExam: "required",
                rollNoFrom: {
                    required: function () {
                        if ($('#range').is(":checked")) {
                            return true;
                        }
                        return false;
                    }
                },
                rollNoTo: {
                    required: function () {
                        if ($('#range').is(":checked")) {
                            return true;
                        }
                        return false;
                    }
                },
            },
            messages: {
                placeOfExam: "পরীক্ষার স্থান নির্বাচন করুন ",
                recruitmentJobDescriptionType: "চাকরি নির্বাচন করুন ",
                // dateOfExam: "পরীক্ষার তারিখ নির্বাচন করুন ",
                // timeOfExamTest: "পরীক্ষার সময় নির্বাচন করুন ",
                // employeeRecordsId: "স্বাক্ষরকারী ব্যক্তি নির্বাচন করুন ",
                rollNoFrom: "প্রয়োজনীয় ",
                rollNoTo: "প্রয়োজনীয় ",

            }
        });

        init();
        CKEDITOR.replaceAll();


    });

    function fetchAllJobs(otherOffices) {

        let url = "Admit_cardServlet?actionType=getAllOtherOffices";
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {

                $('#recruitmentJobDescriptionType_select_0').html("");
                let o;
                let str;
                if (language === 'English') {
                    str = 'Select';
                    o = new Option('Select', '-1');
                } else {
                    o = new Option('বাছাই করুন', '-1');
                    str = 'বাছাই করুন';
                }
                $(o).html(str);
                $('#recruitmentJobDescriptionType_select_0').append(o);

                const response = JSON.parse(fetchedData);

                if (response && response.length > 0) {
                    for (let x in response) {

                        if (language === 'English') {
                            str = response[x].englishText;
                        } else {
                            str = response[x].banglaText;
                        }

                        if (otherOffices && otherOffices.length > 0) {
                            if (otherOffices.includes(response[x].value + '')) {
                                o = new Option(str, response[x].value, false, true);
                            } else {
                                o = new Option(str, response[x].value);
                            }
                        } else {
                            o = new Option(str, response[x].value);
                        }
                        $(o).html(str);
                        $('#recruitmentJobDescriptionType_select_0').append(o);
                    }
                }


            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function PreprocessBeforeSubmiting() {
        let data = added_employee_info_map.keys().next();
        if (!(data && data.value)) {
            toastr.error("অনুগ্রহ করে স্বাক্ষরকারী ব্যক্তি বাছাই করুন ");
            return false;
        }
        document.getElementById('employeeRecordsId').value = JSON.stringify(
            Array.from(added_employee_info_map.values()));

        $('#dateOfExam_date_0').val(getDateStringById('date-of-exam-js'));
        document.getElementById('timeOfExam_text_0').value = getTimeById('jsTimeIn', true);
        document.getElementById('endTimeOfExam_text_0').value = getTimeById('jsTimeEnd', true);

        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }

        return dateValidator('date-of-exam-js', true, {
                errorEn: "পরীক্ষার তারিখ নির্বাচন করুন",
                errorBn: "পরীক্ষার তারিখ নির্বাচন করুন",
            }
        ) && timeNotEmptyValidator('jsTimeIn', {
            errorEn: "পরীক্ষার শুরুর সময় নির্বাচন করুন",
            errorBn: "পরীক্ষার শুরুর সময় নির্বাচন করুন",
        }) && timeNotEmptyValidator('jsTimeEnd', {
            errorEn: "পরীক্ষার শেষের সময় নির্বাচন করুন",
            errorBn: "পরীক্ষার শেষের সময় নির্বাচন করুন",
        });

    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function formSubmit() {
        event.preventDefault();
        let form = $("#bigform");
        form.validate();
        let valid = form.valid() && PreprocessBeforeSubmiting();
        if (valid) {
            buttonStateChange(true);
            let url = form.attr('action');
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        }
    }

    function onRecruitmentTestNameChange(selectedVal) {

        let recruitmentTestNameId = selectedVal.value;

        if (recruitmentTestNameId !== null && recruitmentTestNameId > 0 && recruitmentTestNameId !== "") {
            const url = 'Recruitment_job_descriptionServlet?actionType=getJobDescriptionForAdmitCardByRecTestName&recruitmentTestNameId=' + recruitmentTestNameId+
                '&selectedId='+<%=admit_cardDTO.recruitmentJobDescriptionType%>;

            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (response) {
                    document.getElementById('recruitmentJobDescriptionType_select_0').innerHTML = response;
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }


    function init() {

        <% if(actionName.equalsIgnoreCase("add")){ %>
        $(".rangeClass").css("display", "none");
        <% } else {


                if(admit_cardDTO.range){ %>
        $(".rangeClass").css("display", "block");
        <% } else { %>
        $(".rangeClass").css("display", "none");
        <% } %>

        setDateByTimestampAndId('date-of-exam-js', <%=admit_cardDTO.dateOfExam%>);
        setTimeById('jsTimeIn', '<%=admit_cardDTO.timeOfExam%>');
        setTimeById('jsTimeEnd', '<%=admit_cardDTO.endTimeOfExam%>');

        <% } %>

        var today = new Date();
        var todayDate = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
        setMinDateById('date-of-exam-js', todayDate);


    }

    var row = 0;

    var child_table_extra_id = <%=childTableStartingID%>;


    function jobChange() {
        let jobValue = document.getElementById('recruitmentJobDescriptionType_select_0').value;
        if (jobValue && jobValue.length > 0 && jobValue != 0) {
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    // document.getElementById('level').innerHTML = this.responseText;
                    document.getElementById('rangeSuggestion').innerHTML = this.responseText;
                } else if (this.readyState == 4 && this.status != 200) {
                }
            };

            let params = "Admit_cardServlet?actionType=prevDataLoad";
            params = params + "&language=" + lang + "&jobId=" + jobValue;
            xhttp.open("Get", params, true);
            xhttp.send();
        } else {
            document.getElementById('rangeSuggestion').innerHTML = '';
        }
    }

    $("#range").change(function () {
        if (this.checked) {
            $(".rangeClass").css("display", "block");
        } else {
            $(".rangeClass").css("display", "none")
        }
    });


    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button

    // map to store and send added employee data
    // map to store and send added employee data


    <%
    List<EmployeeSearchIds> addedEmpIdsList = null;
    if(actionName.equals("edit")) {
        addedEmpIdsList = Arrays.asList(new EmployeeSearchIds
        (admit_cardDTO.employeeRecordsId,admit_cardDTO.unit_id,admit_cardDTO.post_id));
    }
    %>

    added_employee_info_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(addedEmpIdsList)%>);


    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_emp_table', {
                info_map: added_employee_info_map,
                isSingleEntry: true
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagEmp_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_emp_table';
        $('#search_emp_modal').modal();
    });


</script>

<style>
    .required {
        color: red;
    }
</style>






