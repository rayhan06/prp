<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="admit_card.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDAO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="util.UtilCharacter" %>
<%@page pageEncoding="UTF-8" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    Admit_cardDAO admit_cardDAO = (Admit_cardDAO) request.getAttribute("admit_cardDAO");


    String navigator2 = SessionConstants.NAV_ADMIT_CARD;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>




<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.RECRUITMENT_TEST_NAME_ADD_RECRUITMENT_TEST_NAME_ADD_FORMNAME, loginDTO)%></th>
            <th><%=LM.getText(LC.ADMIT_CARD_ADD_RECRUITMENTJOBDESCRIPTIONTYPE, loginDTO)%>
            </th>
            <%--								<th><%=LM.getText(LC.CANDIDATE_LIST_LEVEL, loginDTO)%></th>--%>
            <th><%=LM.getText(LC.ADMIT_CARD_ADD_PLACEOFEXAM, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.ADMIT_CARD_ADD_DATEOFEXAM, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.ADMIT_CARD_ADD_TIMEOFEXAM, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.ADMIT_CARD_ADD_ROLLNOFROM, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.ADMIT_CARD_ADD_ROLLNOTO, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.ADMIT_CARD_ADD_EMPLOYEERECORDSID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>

            <th><%=LM.getText(LC.ADMIT_CARD_SEARCH_PRINT, loginDTO)%>
            </th>
            <th>
                <%=UtilCharacter.getDataByLanguage(Language, "উপস্থিতি", "Attendance")%>
            </th>
            <th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_RECRUITMENT_JOB_DESCRIPTION_EDIT_BUTTON, loginDTO)%>
            </th>
			<th class="text-center">
                <span><%="English".equalsIgnoreCase(Language)?"All":"সকল"%></span>
				<div class="d-flex align-items-center justify-content-between">
					<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
						<i class="fa fa-trash"></i>&nbsp;
					</button>
					<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
				</div>
			</th>
        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_ADMIT_CARD);

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Admit_cardDTO admit_cardDTO = (Admit_cardDTO) data.get(i);


        %>
        <tr id='tr_<%=i%>'>
            <%

            %>


            <%
                request.setAttribute("admit_cardDTO", admit_cardDTO);
            %>

            <jsp:include page="./admit_cardSearchRow.jsp">
                <jsp:param name="pageName" value="searchrow"/>
                <jsp:param name="rownum" value="<%=i%>"/>
            </jsp:include>


            <%

            %>
        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			