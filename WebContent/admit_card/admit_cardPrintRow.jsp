<%@page pageEncoding="UTF-8" %>

<%@page import="admit_card.Admit_cardDTO" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>


<%@ page import="login.LoginDTO" %>
<%@page import="pb.CommonDAO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="admit_card.Admit_cardDAO" %>
<%@ page import="common.QRCodeUtil" %>
<%@ page import="util.TimeFormat" %>
<%@ page import="parliament_job_applicant.Parliament_job_applicantDTO" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="employee_records.Employee_recordsDAO" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ page import="job_applicant_photo_signature.Job_applicant_photo_signatureDAO" %>
<%@ page import="job_applicant_photo_signature.Job_applicant_photo_signatureDTO" %>
<%@ page import="files.FilesDTO" %>
<%@ page import="files.FilesDAO" %>
<%@ page import="java.util.List" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDTO" %>
<%@ page import="geolocation.GeoLocationDAO2" %>
<%@ page import="recruitment_exam_venue.Recruitment_exam_venueDTO" %>
<%@ page import="recruitment_seat_plan.Recruitment_seat_planRepository" %>
<%@ page import="recruitment_exam_venue.Recruitment_exam_venueRepository" %>
<%@ page import="recruitment_exam_venue.RecruitmentExamVenueItemDTO" %>
<%@ page import="recruitment_exam_venue.RecruitmentExamVenueItemRepository" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);

    Admit_cardDTO admit_cardDTO = (Admit_cardDTO) request.getAttribute("admit_cardDTO");
    String levelName = (String) request.getAttribute("levelName");
    Parliament_job_applicantDTO row = (Parliament_job_applicantDTO) request.getAttribute("parliament_applicantDTO");
    if (row == null) {
        row = new Parliament_job_applicantDTO();

    }

    Job_applicant_applicationDTO jobAppApp = (Job_applicant_applicationDTO) request.getAttribute("jobAppApp");
    String examVenueInfo = "";
    if(jobAppApp != null ){
        Recruitment_exam_venueDTO recruitment_exam_venueDTO = Recruitment_exam_venueRepository.getInstance().getRecruitment_exam_venueDTOByiD(jobAppApp.recruitmentExamVenueId);
        RecruitmentExamVenueItemDTO recruitmentExamVenueItemDTO = RecruitmentExamVenueItemRepository
                .getInstance().getRecruitmentExamVenueItemDTOByiD(jobAppApp.recruitmentExamVenueItemId);
        if(recruitmentExamVenueItemDTO != null){
            examVenueInfo += "রুম নম্বরঃ "+recruitmentExamVenueItemDTO.roomNo+", ফ্লোরঃ "+recruitmentExamVenueItemDTO.floor+", ভবনঃ "+ recruitmentExamVenueItemDTO.building+", ";
        }
        if(recruitment_exam_venueDTO != null){
            examVenueInfo +=  recruitment_exam_venueDTO.nameBn;
        }
    }

    System.out.println("row = " + row);
    StringBuilder qrContent = new StringBuilder();
    String context2 = "../../.." + request.getContextPath() + "/";

    Recruitment_job_descriptionDTO recruitment_job_descriptionDTO =
            (Recruitment_job_descriptionDTO) request.getAttribute("jobDescriptionDTO");
    if (recruitment_job_descriptionDTO == null) {
        recruitment_job_descriptionDTO = new Recruitment_job_descriptionDTO();
    }

    Employee_recordsDTO employee_recordsDTO =
            (Employee_recordsDTO) request.getAttribute("employee_recordsDTO");
    if (employee_recordsDTO == null) {
        employee_recordsDTO = new Employee_recordsDTO();
    }

    String designation =
            (String) request.getAttribute("designation");
    if (designation == null) {
        designation = "";
    }

    Job_applicant_photo_signatureDAO photo_signatureDAO =
            (Job_applicant_photo_signatureDAO) request.getAttribute("photo_signatureDAO");
    if (photo_signatureDAO == null) {
        photo_signatureDAO = new Job_applicant_photo_signatureDAO();
    }

    FilesDAO filesDAO =
            (FilesDAO) request.getAttribute("filesDAO");
    if (filesDAO == null) {
        filesDAO = new FilesDAO();
    }


%>


<main class="html-2-pdfwrapper my-5 mx-3">
    <div class="border border-dark row py-3 px-2 rounded d-flex align-items-center m-0">
        <div class="col-4">
            <img
                    src="<%=context2%>assets/images/bangladesh-parliament-logo.png"
                    alt="logo"
                    class=" "
                    style="width: 19%;"
            />
        </div>
        <div class="col-4 text-center">
            <h4 class="text-nowrap">
                বাংলাদেশ জাতীয় সংসদ সচিবালয়
            </h4>
            <span class="table-title">
                www.parliament.gov.bd
            </span>
        </div>
        <div class="col-4 text-right">
            <img
                    src="<%=context2%>assets/images/mujib_borsho_logo.png"
                    alt="logo" class=" "
                    style="width: 25%;"
            />
        </div>
    </div>
    <%
        StringBuilder initialText = new StringBuilder();
        StringBuilder date_and_time_of_exam = new StringBuilder();
        //initialText.append("জাতীয় সংসদ সচিবালয়ে ");
    %>
    <%
        String value = recruitment_job_descriptionDTO.jobTitleBn;
//            value = CommonDAO.getName(Integer.parseInt(value), "position",  "name_bn", "id");
        qrContent.append("পদের নাম: ").append(value);
//        initialText.append(value).
//                append(" পদে নিয়োগের জন্য লিখিত পরীক্ষা আগামী ");

        String dateValue = admit_cardDTO.dateOfExam + "";
        SimpleDateFormat format_dateOfExam = new SimpleDateFormat("dd MMMM yyyy");
        String formatted_dateOfExam = format_dateOfExam.format(new Date(Long.parseLong(dateValue)));
        String formattedDate = Admit_cardDAO.getDateBanglaFromEnglish(formatted_dateOfExam);
        qrContent.append("\n").append("তারিখ: ").append(formattedDate);
        UtilCharacter utilCharacter = new UtilCharacter();

        String weekDay = TimeFormat.weekDayInBangla[new Date(admit_cardDTO.dateOfExam).getDay()];
        String morningOrNoon = TimeFormat.getMorningOrNoonInBangla(admit_cardDTO.timeOfExam);
        String time = TimeFormat.replaceAmPM(UtilCharacter.convertNumberEnToBn(admit_cardDTO.timeOfExam));
        String endTime = TimeFormat.replaceAmPM(UtilCharacter.convertNumberEnToBn(admit_cardDTO.endTimeOfExam));

//        initialText.append(utilCharacter.convertNumberEnToBn(formattedDate)).append(" তারিখ ").
//                append(weekDay).append(" ").append(morningOrNoon).append(" ").
//                append(time).append(" টায় ").
//                append(admit_cardDTO.placeOfExam).
//                append(" অনুষ্ঠিত হবে। ").
//                append("ইস্যুকৃত প্রবেশপত্রসহ যথাসময়ে উপস্থিত হয়ে পরীক্ষায় অংশগ্রহণের জন্য অনুরোধ জানানো হলো।");
        date_and_time_of_exam.append(weekDay).append(",").
                append(UtilCharacter.newDateFormat(UtilCharacter.convertNumberEnToBn(formattedDate))).append(",").append("<br>").
                append(morningOrNoon).append(" ").
                append(time).append("-").append(endTime).append(" ঘটিকা ");

        initialText.
                append("ইস্যুকৃত প্রবেশপত্রসহ যথাসময়ে উপস্থিত হয়ে পরীক্ষায় অংশগ্রহণের জন্য অনুরোধ জানানো হলো।");

        // + " (" + levelName + ")"

    %>
    <div class="row mt-5">
        <div class="col-12">
            <h4 class="mb-3 text-center">
                প্রবেশপত্র -  <%=value %>
            </h4>
        </div>
    </div>
    <div class="row mt-1">
        <div class="col-12">
            <h6 class="text-center">
                <%=initialText.toString()%>
            </h6>
        </div>
    </div>
    <div class="border border-dark rounded p-3 mt-5">
        <%
            //                        UtilCharacter utilCharacter = new UtilCharacter();
//                        long rowId = row.iD + 10000;
            String rollNo = UtilCharacter.convertNumberEnToBn(jobAppApp.rollNumber);
            qrContent.append("\n").append("রোল নং:  ").append(rollNo);

        %>
        <div class="row d-flex align-items-center justify-content-between" style="padding: 0px!important;">
            <div class="col-6">
                <div class="row">
                    <div class="col-4">
                        <h5 class="font-weight-bold">
                            রোল নং :
                        </h5>
                    </div>
                    <div class="col-8">
                        <h5 class="font-weight-bold">
                            <%=rollNo%>
                        </h5>
                    </div>
                </div>

                <div class="row my-3">

                    <div class="col-4">
                        <span class="">
                            পরীক্ষা কেন্দ্র :
                        </span>
                    </div>
                    <div class="col-8">
                        <span class="">
                            <%=examVenueInfo%>
                        </span>
                    </div>
                </div>

                <div class="row my-3">

                    <div class="col-4">
                        <span class="">
                            তারিখ ও সময় :
                        </span>
                    </div>
                    <div class="col-8">
                        <span class="">
                            <%=date_and_time_of_exam%>
                        </span>
                    </div>
                </div>

                <div class="row my-3">
                    <%
                        qrContent.append("\n").append("জনাব/বেগম ").append(row.nameBn);
                    %>
                    <div class="col-4">
                        <span class="">
                            জনাব/বেগম :
                        </span>
                    </div>
                    <div class="col-8">
                        <span class="">
                            <%=row.nameBn%>
                        </span>
                    </div>
                </div>
                <div class="row my-3">
                    <%

                        String fatherOrHusband = row.fatherName;
                        if (row.genderCat == 2 && row.maritalStatusCat == 2) {
                            fatherOrHusband = row.spouseName;
                        }
                        qrContent.append("\n").append("পিতা/স্বামীর নাম: ").append(fatherOrHusband);
                    %>
                    <div class="col-4">
                <span class="">
                    পিতা/স্বামীর নাম :
                </span>
                    </div>
                    <div class="col-8">
                <span class="">
                    <%=fatherOrHusband%>
                </span>
                    </div>
                </div>
                <div class="row my-3">
                    <%
                        qrContent.append("\n").append("মাতার নাম: ").append(row.motherName);
                    %>
                    <div class="col-4">
                <span class="">
                    মাতার নাম :
                </span>
                    </div>
                    <div class="col-8">
                <span class="">
                    <%=row.motherName%>
                </span>
                    </div>
                </div>
                <div class="row my-3">
                    <%

                        value = GeoLocationDAO2.parseText(row.permanentAddress, Language);
                        String[] list = value.split(",");
                        int size = list.length;
                        String fullAddr = GeoLocationDAO2.parseDetails(row.permanentAddress);
                        for (int i = size - 1; i >= 0; i--) {
                            fullAddr += ", " + list[i];
                        }

                        qrContent.append("\n").append("স্থায়ী  ঠিকানা: ").append(fullAddr);
                    %>
                    <div class="col-4">
                <span class="">
                    স্থায়ী ঠিকানা :
                </span>
                    </div>
                    <div class="col-8">
                <span class="">
                    <%=fullAddr%>
                </span>
                    </div>
                </div>
            </div>
            <div class="col-3 text-right">
                <div class="text-right">
                    <%
                        String QR = QRCodeUtil.getQrCodeAsBase64(qrContent.toString());
                    %>
                    <img style="height: 22%" src="<%= QR %>"/>
                </div>
            </div>
            <div class="col-3 text-right">
                <%

//                    Job_applicant_photo_signatureDTO photo = photo_signatureDAO.
//                            getDTOByJobApplicantIDAndFileType(row.iD, 1).get(0);
                    if (jobAppApp.photo_file_id != 0) {
                        List<FilesDTO> filesDTOsOfPhoto = filesDAO.getDTOsByFileID(jobAppApp.photo_file_id);
                        FilesDTO signatureFile = filesDTOsOfPhoto.isEmpty() ? null : filesDTOsOfPhoto.get(0);

                        if (signatureFile != null && signatureFile.fileBlob != null) {
                            byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(signatureFile.fileBlob);
                            value = signatureFile.fileBlob + "";

                            out.println("<img src='" + "data:" + signatureFile.fileTypes + ";base64," + new String(encodeBase64) + "'  style='width:75%;' >");
                        }

                    }
                %>
            </div>
        </div>
    </div>
    <div class="mt-5">
        <div class="row">
            <div class="col-3 offset-9 text-center">
                <div>
                    <%
                        byte[] encodeBase64Photo = Base64.encodeBase64(employee_recordsDTO.signature);
                        String defaultPhoto = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAUDBAQEAwUEBAQFBQUGBwwIBwcHBw8LCwkMEQ8SEhEPERETFhwXExQaFRERG" +
                                "CEYGh0dHx8fExciJCIeJBweHx7/wAALCABkASwBAREA/8QAFQABAQAAAAAAAAAAAAAAAAAAAAj/xAAUEAEAAAAAAAAAAAAAAAAAAAAA/9oACAEBAAA" +
                                "/ALLAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" +
                                "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH/9k=";
                    %>
                    <img
                            src="data:image/jpg;base64,<%=employee_recordsDTO.signature != null ? new String(encodeBase64Photo) : (defaultPhoto)%>"
                            width="50%"
                            id="applicantSignature"
                    >
                </div>
                <div>
                    <span
                            class=""
                            id="applicantName"
                            placeholder="নাম"
                    >
                    (<%out.println(employee_recordsDTO.nameBng);%>)
                </span>
                </div>
                <div>
                    <span
                            class="text-nowrap"
                            id="applicantOrganogramName"
                            placeholder="পদের নাম"
                    >
                    <%out.println(admit_cardDTO.signatory_post_name_bn);%>
                </span>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-5">
        <h5 class="text-dark text-center mb-0 py-2 pl-1 rounded" style="background-color: #cccccc">
            পরীক্ষার্থীদের জন্য জ্ঞাতব্য ও আবশ্যকীয় পালনীয় বিষয়াদি (এই অংশটুকু অনেক ক্ষেত্রে পরিবর্তনযোগ্য)
        </h5>
        <div class="note mt-0 pt-4 pb-2" style="background: #F8F9F9 !important;">
            <%=admit_cardDTO.rules%>
        </div>
    </div>
<%--    <div class="mt-5" style="border-top: 1px dashed #212121"></div>--%>
    <div class="mt-1 page-break">
        <h6>
            <%
                out.println(row.nameBn);
            %>
        </h6>
        <h6>
            <%

                out.println("C/O " + row.careOfPermanent);
            %>
        </h6>
        <h6>
            <%
                value = GeoLocationDAO2.parseDetails(row.permanentAddress);
                out.println(value);
            %>
        </h6>
        <h6>
            <%
                value = GeoLocationDAO2.parseText(row.permanentAddress, Language);
                list = value.split(",");
                size = list.length;
                fullAddr = "";
                for (int i = size - 1; i >= 0; i--) {
                    if (i == size - 1) {
                        fullAddr += list[i];
                    } else {
                        fullAddr += ", " + list[i];
                    }
                }
                out.println(fullAddr);
            %>
        </h6>
    </div>
</main>

