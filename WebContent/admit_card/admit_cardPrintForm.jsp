<%@page import="admit_card.Admit_cardDAO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>

<%@ page language="java" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="util.RecordNavigator" %>


<%@ page import="java.util.List" %>
<%@ page import="parliament_job_applicant.Parliament_job_applicantDTO" %>
<%@ page import="javafx.util.Pair" %>
<%@ page import="recruitment_job_specific_exam_type.JobSpecificExamTypeDao" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="admit_card.Admit_cardDTO" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDTO" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>


<%
    //out.println("<input type='hidden' id='failureMessage_general' value='" +  + "'/>");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String value = "";
    String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    Admit_cardDAO admit_cardDAO = (Admit_cardDAO) request.getAttribute("admit_cardDAO");
    Map<Long, Parliament_job_applicantDTO> job_applicantDTOMap =
            (Map<Long, Parliament_job_applicantDTO>) request.getAttribute("job_applicantDTOMap");
    if (job_applicantDTOMap == null) {
        job_applicantDTOMap = new HashMap<>();
    }

    List<Job_applicant_applicationDTO> job_applicant_applicationDTOS = (List<Job_applicant_applicationDTO>) request.getAttribute("job_applicant_applicationDTOS");
    if (job_applicant_applicationDTOS == null) {
        job_applicant_applicationDTOS = new ArrayList<>();
    }


    String navigator2 = SessionConstants.NAV_ADMIT_CARD;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();

    String context = "../../.." + request.getContextPath() + "/";


    String levelName = "";
    Admit_cardDTO admit_cardDTO = (Admit_cardDTO) request.getAttribute("admit_cardDTO");
    Pair<String, String> newValue = new JobSpecificExamTypeDao().getNameByJobIDAndLevel
            (admit_cardDTO.recruitmentJobDescriptionType, admit_cardDTO.level);
    if (newValue != null) {
        levelName = UtilCharacter.getDataByLanguage(Language, newValue.getValue(), newValue.getKey());
    }

    request.setAttribute("levelName", levelName);

%>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Form</title>

    <style>
        @media print {
            .page-break {
                page-break-after: always;
            }

            main{
                margin-left: 0px!important;
                margin-right: 0px!important;
            }
        }
    </style>

</head>
<body id="random" name="random">

<%
    try {

        if (job_applicant_applicationDTOS != null) {
            int size = job_applicant_applicationDTOS.size();
            System.out.println("data not null and size = " + size + " data = " + job_applicant_applicationDTOS);
            for (int i = 0; i < size; i++) {
                long applicantId = job_applicant_applicationDTOS.get(i).jobApplicantId;
                if (!job_applicantDTOMap.containsKey(applicantId)) {
                    continue;
                }
                Parliament_job_applicantDTO row = job_applicantDTOMap.get(applicantId);
%>
<%
    request.setAttribute("parliament_applicantDTO", row);
    request.setAttribute("jobAppApp", job_applicant_applicationDTOS.get(i));
%>
<jsp:include page="./admit_cardPrintRow.jsp">
    <jsp:param name="pageName" value="searchrow"/>
    <jsp:param name="rownum" value="<%=i%>"/>
</jsp:include>
<%
            }
        }
    } catch (Exception ex) {
        System.out.println("data not null and size = " + ex);
    }
%>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<%--<button onclick="generate()">Generate PDF</button>--%>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="<%=context%>assets/scripts/util1.js"></script>
<script>

    margins = {
        top: 70,
        bottom: 40,
        left: 30,
        width: 550
    };


    generate = function () {
        var mywindow = window.open('', 'PRINT', "width=" + screen.availWidth + ",height=" + screen.availHeight);
        mywindow.document.write('<html><head>');
        mywindow.document.write('<meta charset="UTF-8">\n' +
            '    <meta name="viewport" content="width=device-width, initial-scale=1.0">');
        mywindow.document.write('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">');
        mywindow.document.write('<style type="text/css"> @media screen {html, body{font-family: "kalpurushregular"!important;font-size: 8px;box-sizing: border-box;width: 100 %;height: 98 %;margin: 0px;padding: 0px;font-weight: 300} .container{width: 1170px;margin: 0 auto}.col{float: left}.col-25{width: 25%}.col-50{width: 50%}.col-75{width: 75%}.col-100{width: 100%}.col-offset-75{margin-left: 75%}.colr{float: right}.row: after{content: "";display: table;clear: both}.txt-center{text-align: center}.ul-text{text-decoration: underline}.title{padding-top: 56px}.admit{margin-top: 0}.txt-right{text-align: right}.note-row{border: 1px solid #000;padding: 10px}.note-list{list-style: none}.note-list li{margin-bottom: 5px}.note-list li span{margin-right: 10px} .height-150 {max-height: 150px;}.margin-top-percentage-05{ margin-top: 5%;}} ' +
            '@media print {@page { size: auto;  margin: 0mm; } html, body{font-family: "kalpurushregular"!important;font-size: 12px;box-sizing: border-box;margin: 0px;padding: 0px;font-weight: 300} .pagebreak { page-break-before: always; } .container{width: 1170px;margin: 0 auto}.col{float: left}.col-25{width: 25%}.col-50{width: 50%}.col-75{width: 75%}.col-100{width: 100%}.col-offset-75{margin-left: 75%}.colr{float: right}.txt-center{text-align: center}.ul-text{text-decoration: underline}.title{padding-top: 56px}.admit{margin-top: 0}.txt-right{text-align: right} .note-row{border: 1px solid #000;padding: 0.5em}.note-list{list-style: none}.note-list li{margin-bottom: 5px}.note-list li span{margin-right: 10px} .height-150 {max-height: 150px;}.margin-top-10 {margin-top: 10px;} .margin-top-percentage-05{margin-top: 3%;}} </style>');
        mywindow.document.write('</head><body >');
        var table = $(".html-2-pdfwrapper");
        $.each(table, function (key, val) {
            var doc = $(val);
            mywindow.document.write(doc.html());
        });
        //mywindow.document.write(table.html());
        mywindow.document.write('</body></html>');
        var isPrinting = false;
        var is_chrome = Boolean(mywindow.chrome);
        if (is_chrome) {
            mywindow.onload = function () { // wait until all resources loaded
                isPrinting = true;
                mywindow.focus(); // necessary for IE >= 10
                mywindow.print();  // change window to mywindow
                mywindow.close();// change window to mywindow
                isPrinting = false;
            };
            setTimeout(function () {
                if (!isPrinting) {
                    mywindow.print();
                    mywindow.close();
                }
            }, 300);
        } else {
            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10
            mywindow.print();
            mywindow.close();
        }
        return true;
    };


    $(document).ready(function () {
        // generate();
        // downloadPdf("admit_card.pdf", "random");
        // printAnyDiv("random")
    });

    function printAnyDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }


</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>
			