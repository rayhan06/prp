<%@page pageEncoding="UTF-8" %>

<%@page import="admit_card.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="employee_records.Employee_recordsDAO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDAO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="recruitment_job_specific_exam_type.JobSpecificExamTypeDao" %>
<%@ page import="javafx.util.Pair" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionRepository" %>
<%@ page import="recruitment_job_specific_exam_type.RecruitmentJobSpecificExamTypeDTO" %>
<%@ page import="recruitment_job_specific_exam_type.JobSpecificExamTypeRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameDTO" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameRepository" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_ADMIT_CARD;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Admit_cardDTO admit_cardDTO = (Admit_cardDTO) request.getAttribute("admit_cardDTO");
    CommonDTO commonDTO = admit_cardDTO;
    String servletName = "Admit_cardServlet";


    System.out.println("admit_cardDTO = " + admit_cardDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Admit_cardDAO admit_cardDAO = (Admit_cardDAO) request.getAttribute("admit_cardDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    List<RecruitmentJobSpecificExamTypeDTO> jobSpecificExamTypeDTOS =
            JobSpecificExamTypeRepository.getInstance().
                    getRecruitmentJobSpecificExamTypeDTOByjob_id(admit_cardDTO.recruitmentJobDescriptionType);

    List<String> examNameList = new ArrayList<>();
    List<Integer> selectedExamOrderList = new ArrayList<>();
    if(jobSpecificExamTypeDTOS != null && jobSpecificExamTypeDTOS.size() > 0){
        List<Integer> examOrderList = jobSpecificExamTypeDTOS.stream().filter(f -> f.isSelected).map(e -> e.order).sorted().collect(Collectors.toList());

        List<RecruitmentJobSpecificExamTypeDTO> recruitmentJobSpecificExamTypeDTOS = JobSpecificExamTypeRepository.getInstance()
                .getRecruitmentJobSpecificExamTypeDTOByjob_id(admit_cardDTO.recruitmentJobDescriptionType);
        for(Integer examOrder : examOrderList){
            String recruitmentJobSpecificExam = "";
            RecruitmentJobSpecificExamTypeDTO jobSpecificExamTypeDTO = recruitmentJobSpecificExamTypeDTOS.stream()
                    .filter(e -> e.isSelected &&  e.order == examOrder)
                    .findFirst()
                    .orElse(null);
            if(jobSpecificExamTypeDTO != null){
                recruitmentJobSpecificExam = CatRepository.getName(Language, "job_exam_type", jobSpecificExamTypeDTO.jobExamTypeCat);
                examNameList.add(recruitmentJobSpecificExam);
                selectedExamOrderList.add(examOrder);
            }

        }
    }

%>

<td id='<%=i%>_recruitmentTestName'>
    <%

        Recruitment_test_nameDTO recruitment_test_nameDTO = Recruitment_test_nameRepository.getInstance().
                getRecruitment_test_nameDTOByiD(admit_cardDTO.recruitmentTestName);
        if (admit_cardDTO.recruitmentTestName != 0 && recruitment_test_nameDTO != null && Language.equalsIgnoreCase("english")) {
            value = recruitment_test_nameDTO.nameEn + "";
        } else if (admit_cardDTO.recruitmentTestName != 0 && recruitment_test_nameDTO != null && Language.equalsIgnoreCase("bangla")) {
            value = recruitment_test_nameDTO.nameBn + "";
        }else{
            value = "";
        } %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_recruitmentJobDescriptionType' style="vertical-align: middle">
    <%--											<%--%>
    <%--											value = admit_cardDTO.recruitmentJobDescriptionType + "";--%>
    <%--											%>--%>
    <%--											<%--%>
    <%--											value = CommonDAO.getName(Integer.parseInt(value), "recruitment_job_description", Language.equals("English")?"name_en":"name_bn", "id");--%>
    <%--											%>--%>
    <%--														--%>
    <%--											<%=Utils.getDigits(value, Language)%>--%>

    <%

        value = "";

//													Recruitment_job_descriptionDAO job_descriptionDAO = new Recruitment_job_descriptionDAO();
        Recruitment_job_descriptionDTO descriptionDTO = Recruitment_job_descriptionRepository.
                getInstance().getRecruitment_job_descriptionDTOByID(admit_cardDTO.recruitmentJobDescriptionType);
//															job_descriptionDAO.getDTOByID();
        if (descriptionDTO != null) {
            if (Language.equalsIgnoreCase("english")) {
                value = descriptionDTO.jobTitleEn;
            } else {
                value = descriptionDTO.jobTitleBn;
            }
        }

    %>

    <%=value%>


</td>

<%--<td>--%>
<%--	<%--%>
<%--		value = "";--%>
<%--		Pair<String, String> newValue = new JobSpecificExamTypeDao().getNameByJobIDAndLevel--%>
<%--				(admit_cardDTO.recruitmentJobDescriptionType, admit_cardDTO.level);--%>
<%--		if(newValue != null){--%>
<%--			value = UtilCharacter.getDataByLanguage(Language, newValue.getValue(), newValue.getKey());--%>
<%--		}--%>
<%--	%>--%>
<%--	<%=value%>--%>
<%--</td>--%>


<td id='<%=i%>_placeOfExam' style="vertical-align: middle">
    <%
        value = admit_cardDTO.placeOfExam + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_dateOfExam' style="vertical-align: middle">
    <%
        value = admit_cardDTO.dateOfExam + "";
    %>
    <%
        String formatted_dateOfExam = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_dateOfExam, Language)%>


</td>


<td id='<%=i%>_timeOfExam' style="vertical-align: middle">
    <%
        //											value = TimeFormat.getInAmPmFormat(admit_cardDTO.timeOfExam);
    %>

    <%=Utils.getDigits(admit_cardDTO.timeOfExam, Language)%>


</td>


<td id='<%=i%>_rollNoFrom' style="vertical-align: middle">
    <%
        value = "";
        if (admit_cardDTO.range) {
            value = admit_cardDTO.rollNoFrom + "";
        }

    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_rollNoTo' style="vertical-align: middle">
    <%
        value = "";
        if (admit_cardDTO.range) {
            value = admit_cardDTO.rollNoTo + "";
        }
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_employeeRecordsId' style="vertical-align: middle">
    <%--											<%--%>
    <%--											value = admit_cardDTO.employeeRecordsId + "";--%>
    <%--											%>--%>

    <%=UtilCharacter.getDataByLanguage(Language, admit_cardDTO.signatory_name_bn,
            admit_cardDTO.signatory_name)%>


</td>


<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Admit_cardServlet?actionType=view&ID=<%=admit_cardDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td>
    <button class="btn btn-sm btn-primary shadow btn-border-radius pl-4 pb-2"
            onclick="return printA('<%=admit_cardDTO.iD%>')">
        <i class="fa fa-print"></i>
    </button>
</td>

<td>
    <%
        int specificExamIndex = 0;
        for(String examName : examNameList){  %>

            <button class="btn btn-sm btn-primary shadow btn-border-radius pl-4 pb-2"
                    onclick="return getAttendanceForSpecificExam('<%=admit_cardDTO.iD%>','<%=selectedExamOrderList.get(specificExamIndex++)%>')">
                <i class="fa fa-user"></i> <br><%=examName%>
            </button>

        <%}%>

</td>

<td id='<%=i%>_Edit'>
    <button class="btn-sm border-0 shadow btn-border-radius text-white" style="background-color: #ff6b6b;"
            onclick="edit('<%=admit_cardDTO.iD%>')">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'>
            <input type='checkbox' name='ID' value='<%=admit_cardDTO.iD%>'/>
        </span>
    </div>
</td>


<%--<script>--%>

<%--</script>--%>
