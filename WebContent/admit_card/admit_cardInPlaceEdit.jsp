<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="admit_card.Admit_cardDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Admit_cardDTO admit_cardDTO = (Admit_cardDTO)request.getAttribute("admit_cardDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(admit_cardDTO == null)
{
	admit_cardDTO = new Admit_cardDTO();
	
}
System.out.println("admit_cardDTO = " + admit_cardDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.ADMIT_CARD_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=admit_cardDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_recruitmentJobDescriptionType'>")%>
			
	
	<div class="form-inline" id = 'recruitmentJobDescriptionType_div_<%=i%>'>
		<select class='form-control'  name='recruitmentJobDescriptionType' id = 'recruitmentJobDescriptionType_select_<%=i%>'   tag='pb_html'>
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "recruitment_job_description", "recruitmentJobDescriptionType_select_" + i, "form-control", "recruitmentJobDescriptionType", admit_cardDTO.recruitmentJobDescriptionType + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "recruitment_job_description", "recruitmentJobDescriptionType_select_" + i, "form-control", "recruitmentJobDescriptionType" );			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_placeOfExam'>")%>
			
	
	<div class="form-inline" id = 'placeOfExam_div_<%=i%>'>
		<input type='text' class='form-control'  name='placeOfExam' id = 'placeOfExam_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + admit_cardDTO.placeOfExam + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_dateOfExam'>")%>
			
	
	<div class="form-inline" id = 'dateOfExam_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'dateOfExam_date_<%=i%>' name='dateOfExam' value=<%
if(actionName.equals("edit"))
{
	String formatted_dateOfExam = dateFormat.format(new Date(admit_cardDTO.dateOfExam));
	%>
	'<%=formatted_dateOfExam%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_timeOfExam'>")%>
			
	
	<div class="form-inline" id = 'timeOfExam_div_<%=i%>'>
			<%
                 value = "";
                 if(actionName.equals("edit")) {
                	 value = TimeFormat.getInAmPmFormat(test_babaDTO.timeOfBirth);
                 }
             %>	
			<div class='input-group date edms-datetimepicker'>
                <input type='text' class="form-control" value="<%=value%>" id = 'timeOfExam_time_<%=i%>' name='timeOfExam'  tag='pb_html'/>
                <span class="input-group-addon" style="width:45px;">
                   <span><i class="fa fa-clock-o"></i></span>
                </span>
            </div>	
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_rollNoFrom'>")%>
			
	
	<div class="form-inline" id = 'rollNoFrom_div_<%=i%>'>
		<input type='number' class='form-control'  name='rollNoFrom' id = 'rollNoFrom_number_<%=i%>' min='0' max='1000000' value=<%=actionName.equals("edit")?("'" + admit_cardDTO.rollNoFrom + "'"):("'" + 0 + "'")%>  tag='pb_html'>
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_rollNoTo'>")%>
			
	
	<div class="form-inline" id = 'rollNoTo_div_<%=i%>'>
		<input type='number' class='form-control'  name='rollNoTo' id = 'rollNoTo_number_<%=i%>' min='0' max='1000000' value=<%=actionName.equals("edit")?("'" + admit_cardDTO.rollNoTo + "'"):("'" + 0 + "'")%>  tag='pb_html'>
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeRecordsId'>")%>
			
	
	<div class="form-inline" id = 'employeeRecordsId_div_<%=i%>'>
		<select class='form-control'  name='employeeRecordsId' id = 'employeeRecordsId_select2_<%=i%>'   tag='pb_html'>
			<option class='form-control'  value='0' <%=(actionName.equals("edit") && String.valueOf(admit_cardDTO.employeeRecordsId).equals("0"))?("selected"):""%>>0<br>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedByUserId" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=admit_cardDTO.insertedByUserId%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=admit_cardDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=admit_cardDTO.modifiedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + admit_cardDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=admit_cardDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Admit_cardServlet?actionType=view&ID=<%=admit_cardDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Admit_cardServlet?actionType=view&modal=1&ID=<%=admit_cardDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	