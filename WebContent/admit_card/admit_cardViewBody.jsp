<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="admit_card.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDAO" %>
<%@ page import="test_lib.EmployeeRecordDTO" %>
<%@ page import="employee_records.Employee_recordsDAO" %>
<%@ page import="util.TimeFormat" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="recruitment_job_specific_exam_type.JobSpecificExamTypeDao" %>
<%@ page import="javafx.util.Pair" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionRepository" %>
<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Admit_cardDTO admit_cardDTO = Admit_cardRepository.getInstance().getAdmit_cardDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important; margin-bottom: -18px">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.ADMIT_CARD_ADD_ADMIT_CARD_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div>
                <h5 class="table-title">
                    <%=LM.getText(LC.ADMIT_CARD_ADD_ADMIT_CARD_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">

                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.ADMIT_CARD_ADD_RECRUITMENTJOBDESCRIPTIONTYPE, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = "";
//											String finalValue= "";
                                    Recruitment_job_descriptionDTO descriptionDTO = Recruitment_job_descriptionRepository.getInstance().
                                            getRecruitment_job_descriptionDTOByID(admit_cardDTO.recruitmentJobDescriptionType);
                                    if (descriptionDTO != null) {
                                        if (Language.equalsIgnoreCase("english")) {
                                            value = descriptionDTO.jobTitleEn;
                                        } else {
                                            value = descriptionDTO.jobTitleBn;
                                        }
                                    }
                                %>
                                <%--											<%--%>
                                <%--//											value = CommonDAO.getName(Integer.parseInt(value), "recruitment_job_description", Language.equals("English")?"name_en":"name_bn", "id");--%>
                                <%--											%>--%>

                                <%=value%>


                            </td>

                        </tr>


                        <%--							<tr>--%>
                        <%--								<td style="width:30%"><b><%=LM.getText(LC.CANDIDATE_LIST_LEVEL, loginDTO)%></b></td>--%>
                        <%--								<td>--%>

                        <%--									<%--%>
                        <%--										value = "";--%>
                        <%--										Pair<String, String> newValue = new JobSpecificExamTypeDao().getNameByJobIDAndLevel--%>
                        <%--												(admit_cardDTO.recruitmentJobDescriptionType, admit_cardDTO.level);--%>
                        <%--										if(newValue != null){--%>
                        <%--											value = UtilCharacter.getDataByLanguage(Language, newValue.getValue(), newValue.getKey());--%>
                        <%--										}--%>
                        <%--									%>--%>
                        <%--									<%=value%>--%>


                        <%--								</td>--%>

                        <%--							</tr>--%>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.ADMIT_CARD_ADD_PLACEOFEXAM, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = admit_cardDTO.placeOfExam + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.ADMIT_CARD_ADD_DATEOFEXAM, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = admit_cardDTO.dateOfExam + "";
                                %>
                                <%
                                    String formatted_dateOfExam = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_dateOfExam, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.ADMIT_CARD_ADD_TIMEOFEXAM, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = admit_cardDTO.timeOfExam;
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <% if (admit_cardDTO.range) { %>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.ADMIT_CARD_ADD_ROLLNOFROM, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = admit_cardDTO.rollNoFrom + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.ADMIT_CARD_ADD_ROLLNOTO, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = admit_cardDTO.rollNoTo + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <% } %>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.ADMIT_CARD_ADD_EMPLOYEERECORDSID, loginDTO)%>
                                </b></td>
                            <td>


                                <%=
                                UtilCharacter.getDataByLanguage(Language, admit_cardDTO.signatory_name_bn,
                                        admit_cardDTO.signatory_name)             %>


                            </td>

                        </tr>

<%--                        <tr>--%>
<%--                            <td style="width:30%">--%>
<%--                                <b><%=LM.getText(LC.ADMIT_CARD_SEARCH_RULES, loginDTO)%>--%>
<%--                                </b></td>--%>
<%--                            <td>--%>


<%--                                <%=StringEscapeUtils.escapeHtml4(admit_cardDTO.rules)%>--%>


<%--                            </td>--%>

<%--                        </tr>--%>


                    </table
                </div>
            </div>

        </div>
    </div>
</div>


<!-- <div class="modal-content viewmodal"> -->
<%--<div class="menubottom">--%>
<%--            <div class="modal-header">--%>
<%--                <div class="col-md-12">--%>
<%--                    <div class="row">--%>
<%--                        <div class="col-md-9 col-sm-12">--%>
<%--                            <h5 class="modal-title"><%=LM.getText(LC.ADMIT_CARD_ADD_ADMIT_CARD_ADD_FORMNAME, loginDTO)%></h5>--%>
<%--                        </div>--%>
<%--                        <div class="col-md-3 col-sm-12">--%>
<%--                            <div class="row">--%>
<%--                                <div class="col-md-6">--%>
<%--                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>--%>
<%--                                </div>--%>
<%--                                <div class="col-md-6">--%>
<%--                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </div>--%>


<%--            </div>--%>

<%--            <div class="modal-body container">--%>
<%--			--%>
<%--			<div class="row div_border office-div">--%>

<%--                    <div class="col-md-12">--%>
<%--                        <h3><%=LM.getText(LC.ADMIT_CARD_ADD_ADMIT_CARD_ADD_FORMNAME, loginDTO)%></h3>--%>
<%--                    </div>--%>
<%--			--%>


<%--			</div>	--%>

<%--               --%>


<%--        </div>--%>