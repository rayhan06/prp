<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@page import="java.util.Calendar" %>
<%@page import="pbReport.DateUtils" %>

<%@ page import="pb.*" %>
<%@ page import="am_parliament_building_level.Am_parliament_building_levelRepository" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row mx-2 mx-md-0">
    <div class="col-12">
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-md-right">
					<%=LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_LEVELTYPE, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  onchange="loadBlockList()" name='levelType' id = 'levelType_select_<%=i%>'   tag='pb_html'>
						<%
							Options = Am_parliament_building_levelRepository.getInstance().getOptions(Language, -1);
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-md-right">
					<%=LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_BLOCKTYPE, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='blockType' id = 'blockType_select_<%=i%>'   tag='pb_html'>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-md-right">
					<%=LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_ROOMNO, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<input type='text' class='form-control'  name='roomNo' id = 'roomNo_text_<%=i%>'   tag='pb_html'/>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">

			<div class="form-group row">
				<label class="col-sm-3 control-label text-md-right"><%=LM.getText(LC.PI_REQUISITION_ADD_STATUS, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control' name='status'
							id='status'
							tag='pb_html'>
						<%=CommonApprovalStatus.buildOptionForAsset(Language)%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div" style = "display: none;">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-md-right">
					<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_WHERE_ISDELETED, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<input class='form-control'  name='isDeleted' id = 'isDeleted' value=""/>							
				</div>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">



	function loadBlockList() {

		let levelId = document.getElementById('levelType_select_0').value;

		if(levelId === ''){
			$("#blockType_select_0").html("");
			return ;
		}


		let blockId = '-1';
		let url = "Am_parliament_building_blockServlet?actionType=getBlockByLevelId&levelId=" +
				levelId + "&defaultValue=" + blockId;
		$.ajax({
			url: url,
			type: "GET",
			async: false,
			success: function (fetchedData) {
				const response = JSON.parse(fetchedData);
				if ( response && response.responseCode === 0) {
					$('#toast_message').css('background-color', '#ff6063');
					showToastSticky(response.msg, response.msg);
				}
				else if (response && response.responseCode === 200) {
					$("#blockType_select_0").html(response.msg);
				}

			},
			error: function (jqXHR, textStatus, errorThrown) {
				toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
						+ ", Message: " + errorThrown);
			}
		});

	}

	function loadRoomList() {

		let url = "Am_parliament_building_roomServlet?actionType=getRoomByBlockId&blockId=201&defaultValue=-1";
		$.ajax({
			url: url,
			type: "GET",
			async: false,
			success: function (fetchedData) {
				const response = JSON.parse(fetchedData);
			},
			error: function (jqXHR, textStatus, errorThrown) {
				toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
						+ ", Message: " + errorThrown);
			}
		});

	}


	function ajaxGet(url, onSuccess, onError) {
		$.ajax({
			type: "GET",
			url: getContextPath()+url,
			dataType: "json",
			success: onSuccess,
			error: onError,
			complete: function () {
				// $.unblockUI();
			}
		});
	}


$(document).ready(() => {
	showFooter = false;
	select2SingleSelector('#levelType_select_0', '<%=Language%>');
	select2SingleSelector('#blockType_select_0', '<%=Language%>');
	select2SingleSelector('#status', '<%=Language%>');
});
function init()
{
    dateTimeInit($("#Language").val());

	loadBlockList();

}
function PreprocessBeforeSubmiting()
{
}

</script>