<%@ page import="recruitment_job_specific_exam_type.RecruitmentJobSpecificExamTypeDTO" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="java.util.List" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="util.UtilCharacter" %>
<%@page pageEncoding="UTF-8" %>

<%
    RecruitmentJobSpecificExamTypeDTO jobSpecificExamType = null;

    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    List<RecruitmentJobSpecificExamTypeDTO> recruitmentJobSpecificExamTypeDTOS = null;
    recruitmentJobSpecificExamTypeDTOS = (List<RecruitmentJobSpecificExamTypeDTO>)
            request.getAttribute("jobSpecificExamTypeDTOS");


    String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date todayDate = new Date();
    String highestYear = String.valueOf(1900 + todayDate.getYear() + 5);

    String formTitle = LM.getText(LC.JOB_EXAM_TYPE_FORM_NAME, loginDTO);
%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important; margin-bottom: -18px">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form id="doc-form" class="form-horizontal">

            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                        <span style="color: red;font-weight: bold;">*&nbsp;<%=UtilCharacter.getDataByLanguage(Language,
                                                "মৌখিক অথবা ব্যবহারিক পরীক্ষা পাশ ফেলের হলে, নম্বর এবং পাশের নম্বর উভয়ই ক্ষেত্রে ১ দিতে হবে","If Viva or practical test is pass fail test, you have to give 1 in both the marks and the pass marks")%> </span>
                                        <div class="table-responsive mb-4">
                                            <table class="table table-bordered table-striped text-nowrap">
                                                <thead class="thead-light">
                                                <tr class="">
                                                    <%--                                    <th scope="row">1</th>--%>
                                                    <th class="">

                                                    </th>
                                                    <th class="">

                                                        <b><%=LM.getText(LC.JOB_EXAM_TYPE_NAME, loginDTO)%>
                                                        </b>

                                                    </th>
                                                    <th class=""><b>
                                                        <%=LM.getText(LC.JOB_EXAM_TYPE_MARKS, loginDTO)%>
                                                    </b>
                                                    </th>
                                                    <th class=""><b>
                                                        <%=LM.getText(LC.JOB_EXAM_TYPE_PASS_MARKS, loginDTO)%>
                                                        <%--                                        Pass Marks--%>
                                                    </b>
                                                    </th>
                                                    <th>
                                                        <b><%=LM.getText(LC.JOB_EXAM_TYPE_TENTATIVE_START_DATE, loginDTO)%>
                                                        </b>
                                                    </th>
                                                    <th class="">
                                                        <b><%=LM.getText(LC.JOB_EXAM_TYPE_DESCRIPTION, loginDTO)%>
                                                        </b>
                                                    </th>
                                                    <th class="">
                                                        <b><%=LM.getText(LC.JOB_EXAM_TYPE_ORDER, loginDTO)%>
                                                        </b>
                                                    </th>
                                                    <%--										<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_MODIFIEDBY, loginDTO)%></th>--%>
                                                    <%--                            <th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_REMOVE, loginDTO)%></th>--%>


                                                </tr>
                                                </thead>
                                                <tbody id="">
                                                <%
                                                    for (int i = 0; i < recruitmentJobSpecificExamTypeDTOS.size(); i++) {
                                                        jobSpecificExamType = recruitmentJobSpecificExamTypeDTOS.get(i);


                                                %>
                                                <input type='hidden' name=''
                                                       id='recruitment_job_specific_exam_type_id_<%=i%>'
                                                       value="<%=jobSpecificExamType.iD%>">

                                                <tr>

                                                    <td class="">
                                                        <input type='checkbox' class='' name='is_selected_<%=i%>'
                                                               id='is_selected_<%=i%>'
                                                               value='true' <%=String.valueOf(jobSpecificExamType.isSelected).equals("true")?("checked"):""%>
                                                               tag='pb_html'><br>

                                                    </td>

                                                    <td class="">
                                                        <%
                                                            String name = "";
                                                            if (Language.equalsIgnoreCase("english")) {
                                                                name = jobSpecificExamType.jobExamTypeNameEn;
                                                            } else {
                                                                name = jobSpecificExamType.jobExamTypeNameBn;
                                                            }
                                                        %>
                                                        <%= name%>
                                                    </td>


                                                    <td>
                                                        <input type='number' class='form-control w-auto' name='marks_<%=i%>'
                                                               id='marks_<%=i%>' value='<%=jobSpecificExamType.marks%>'
                                                               min="0" tag='pb_html'>
                                                    </td>

                                                    <td>
                                                        <input type='number' class='form-control w-auto' name='pass_marks_<%=i%>'
                                                               id='pass_marks_<%=i%>'
                                                               value='<%=jobSpecificExamType.passMarks%>' min="0"
                                                               tag='pb_html'>
                                                    </td>

                                                    <td style="min-width: 30rem;">

                                                        <input type='hidden' class='form-control' readonly="readonly"
                                                               data-label="Document Date" id='exam_start_date_<%=i%>'
                                                               name='startApplicationDate'
                                                               value='' tag='pb_html'>
                                                        <%
                                                            String jsId = "date-of-exam-start-js_" + i;
//                                            System.out.println(highestYear);

                                                        %>

                                                        <jsp:include page="/date/date.jsp">
                                                            <jsp:param name="DATE_ID" value="<%=jsId%>"></jsp:param>
                                                            <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                            <jsp:param name="END_YEAR" value="<%=highestYear%>"></jsp:param>
                                                        </jsp:include>

                                                    </td>

                                                    <td>

                                                    <textarea class='form-control w-auto' name='description_<%=i%>'
                                                              id='description_<%=i%>'
                                                              tag='pb_html'><%=jobSpecificExamType.description%></textarea>


                                                    </td>

                                                    <td>
                                                        <input type='number' class='form-control w-auto' name='order_<%=i%>'
                                                               id='order_<%=i%>' value='<%=jobSpecificExamType.order%>'
                                                               min="1" tag='pb_html'>

                                                    </td>


                                                </tr>


                                                <%

                                                    }
                                                %>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-12 text-right">
                        <div class="form-actions mt-3">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_DESCRIPTION_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit" onclick="submitData()">
                                <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_DESCRIPTION_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<script type="text/javascript">

    let rowCount = 0;

    function init() {
        rowCount = <%=recruitmentJobSpecificExamTypeDTOS.size()%>;
        var today = new Date();
        var todayDate = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();


        <%
            for(int i = 0; i < recruitmentJobSpecificExamTypeDTOS.size(); i++){
                jobSpecificExamType = recruitmentJobSpecificExamTypeDTOS.get(i);
        %>
        setDateByTimestampAndId('date-of-exam-start-js_<%=i%>', <%=jobSpecificExamType.examStartDate%>);
        setMinDateById('date-of-exam-start-js_<%=i%>', todayDate);
        <%
            }
        %>
    }

    function back() {
        window.location = 'Recruitment_job_descriptionServlet?actionType=search';
    }

    function checkIfArrayIsUnique(myArray) {
        return myArray.length === new Set(myArray).size;
    }

    function checkGreaterThanZero(array) {
        return array.length === array.filter(j => j > 0).length;
    }

    function marksValidation(marks, passMarks) {
        // console.log({marks, passMarks})
        if (marks < 0) {
            toastr.error("Marks can't be negative");
            return false;
        }

        if (passMarks < 0) {
            toastr.error("Pass Marks can't be negative");
            return false;
        }

        if (passMarks > marks) {
            toastr.error("Pass Marks can't be greater than Marks");
            return false;
        }

        return true;
    }

    function dateValidation(){
        let v, vLength;
        let flag = true;
        <%
            for(int i = 0; i < recruitmentJobSpecificExamTypeDTOS.size(); i++){
        %>
        if (document.getElementById("is_selected_<%=i%>").checked){
            v = getDateTimestampById('date-of-exam-start-js_<%=i%>');
            vLength = v.toString().length;
            if(vLength == 0){
                toastr.error("Invalid date");
                flag = flag && false;
            }
        }

        <%
            }
        %>

        return flag;
    }

    function validation() {
        let id;
        let orders = [];
        for (let i = 0; i < rowCount; i++) {
            id = 'is_selected_' + i;
            if (document.getElementById(id).checked) {
                orders.push(document.getElementById('order_' + i).value / 1);

                let marks = document.getElementById('marks_' + i).value / 1;
                let passMarks = document.getElementById('pass_marks_' + i).value / 1;

                let markValid = marksValidation(marks, passMarks);
                if (!markValid) {
                    return false;
                }
            }
        }

        if (orders.length === 0) {
            toastr.error("No Item Selected");
            return false;
        }

        if (!checkGreaterThanZero(orders)) {
            toastr.error("Orders should be greater than zero");
            return false;
        }

        if (!checkIfArrayIsUnique(orders)) {
            toastr.error("Orders should be unique");
            return false;
        }

        return true;

    }

    function submitData() {
        event.preventDefault();
        if (validation() && dateValidation())  {
            let formData = new FormData();
            formData.append('rowCount', rowCount);

            for (let i = 0; i < rowCount; i++) {
                let id;
                let value;
                id = 'is_selected_' + i;
                formData.append(id, document.getElementById(id).checked);
                id = 'recruitment_job_specific_exam_type_id_' + i;
                formData.append(id, document.getElementById(id).value);
                id = 'marks_' + i;
                formData.append(id, document.getElementById(id).value);
                id = 'pass_marks_' + i;
                formData.append(id, document.getElementById(id).value);
                id = 'description_' + i;
                formData.append(id, document.getElementById(id).value);
                id = 'exam_start_date_' + i;
                value = getDateStringById('date-of-exam-start-js_' + i);
                // console.log(value)
                formData.append(id, value);
                id = 'order_' + i;
                formData.append(id, document.getElementById(id).value);
            }

            // alert()

            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    back();
                } else if (this.readyState == 4 && this.status != 200) {
                    //alert('failed ' + this.status);
                }
            };


            let params = "JobSpecificExamTypeServlet?actionType=edit";
            xhttp.open("POST", params, true);
            xhttp.send(formData);
        }
    }

    $(document).ready(function () {
        dateTimeInit("<%=Language%>");
        init();

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    });

</script>