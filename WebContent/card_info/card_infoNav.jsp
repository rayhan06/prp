<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LM" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="pb.*" %>
<%@ page import="user.UserDTO" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    RecordNavigator rn = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    String action = request.getParameter("url");
    String context = "../../.." + request.getContextPath() + "/";
    int year = Calendar.getInstance().get(Calendar.YEAR);
    String toolbarOpen=request.getParameter("toolbarOpen")==null?"0":request.getParameter("toolbarOpen");
%>


<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1" style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0' onKeyUp='allfield_changed("",0,false)' id='anyfield'
                       value = '<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="card_status_cat"><%=LM.getText(LC.CARD_INFO_ADD_CARDSTATUSCAT, loginDTO)%></label>
                        <div class="col-md-9">
                            <select class='form-control' name='card_status_cat' id='card_status_cat'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=CatRepository.getInstance().buildOptions("card_status", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="card_cat"><%=LM.getText(LC.CARD_INFO_ADD_CARDCAT, loginDTO)%></label>
                        <div class="col-md-9">
                            <select class='form-control' name='card_cat' id='card_cat' onSelect='setSearchChanged()'
                                    style="width: 100%">
                                <%=CatRepository.getInstance().buildOptions("card", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=LM.getText(LC.SEARCH_CARD_INFO_SEARCH_VALIDITY_FROM, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="validStartDate-js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                                <jsp:param name="END_YEAR"
                                           value="<%=year + 20%>"></jsp:param>
                            </jsp:include>
                            <input type='hidden' class='form-control' id='validation_from'
                                   name='validation_from' value=''
                                   tag='pb_html'/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=LM.getText(LC.SEARCH_CARD_INFO_SEARCH_VALIDITY_TO, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="validEndDate-js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                                <jsp:param name="END_YEAR"
                                           value="<%=year + 20%>"></jsp:param>
                            </jsp:include>
                            <input type='hidden' class='form-control' id='validation_to'
                                   name='validation_to' value=''
                                   tag='pb_html'/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0,true)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>

<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>
<script type="text/javascript">
    const cardStatusCatSelector = $('#card_status_cat');
    const cardCatSelector = $('#card_cat');
    const validationFromSelector = $('#validation_from');
    const validationToSelector = $('#validation_to');
    const anyFieldSelector = $('#anyfield');

    function resetInputs(){
        cardStatusCatSelector.select2("val", '-1');
        cardCatSelector.select2("val", '-1');
        anyFieldSelector.val('');
        resetDateById('validStartDate-js');
        resetDateById('validEndDate-js');
    }

    window.addEventListener('popstate',e=>{
        if(e.state){
            let params = e.state;
            dosubmit(params,false);
            resetInputs();
            let arr = params.split('&');
            arr.forEach(e=>{
                let item = e.split('=');
                if(item.length === 2){
                    switch (item[0]){
                        case 'card_status_cat':
                            cardStatusCatSelector.select2("val", item[1]);
                            break;
                        case 'card_cat':
                            cardCatSelector.select2("val", item[1]);
                            break;
                        case 'validation_from':
                            setDateByTimestampAndId('validStartDate-js',item[1]);
                            break;
                        case 'validation_to':
                            setDateByTimestampAndId('validEndDate-js',item[1]);
                            break;
                        case 'AnyField':
                            anyFieldSelector.val(item[1]);
                            break;
                        default:
                             setPaginationFields(item);
                    }
                }
            });
        }else{
            dosubmit(null,false);
            resetInputs();
            resetPaginationFields();
        }
    });

    function dosubmit(params,pushState=true) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if(pushState){
                    history.pushState(params,'','Card_infoServlet?actionType=search&'+params);
                }
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        let url = "<%=action%>&ajax=true&isPermanentTable=true";
        if(params){
            url+="&"+params;
        }
        xhttp.open("Get", url, false);
        xhttp.send();
    }

    function allfield_changed(go, pagination_number,pushState=true) {
        let params = 'search=true';
        if(anyFieldSelector.val()){
            params += '&AnyField=' + anyFieldSelector.val();
        }
        if (cardStatusCatSelector.val()) {
            params += '&card_status_cat=' + cardStatusCatSelector.val();
        }
        if (cardCatSelector.val()) {
            params += '&card_cat=' + cardCatSelector.val();
        }
        validationFromSelector.val(getDateTimestampById('validStartDate-js'));
        validationToSelector.val(getDateTimestampById('validEndDate-js'));
        if(validationFromSelector.val()){
            params+='&validation_from='+validationFromSelector.val();
        }
        if(validationToSelector.val()){
            params+='&validation_to='+validationToSelector.val();
        }
        if(document.getElementsByClassName("kt-portlet__body")[0].style['display']!='none'){
            params+='&toolbarOpen=1';
        }
        let extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        let pageNo = document.getElementsByName('pageno')[0].value;
        let rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;
        let totalRecords = 0;
        let lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }
        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params,pushState);
    }

    $(document).ready(() => {
        readyInit('Card_infoServlet');
        select2SingleSelector('#card_status_cat', '<%=Language%>');
        select2SingleSelector('#card_cat', '<%=Language%>');
        if('<%=toolbarOpen%>'=='1'){
            document.querySelector('.kt-portlet__body').style.display = 'block';
            document.querySelector('#kt_portlet_tools_1').classList.remove('kt-portlet--collapse')
        }
    });
</script>