<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="card_info.*" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ page import="util.StringUtils" %>
<%@ page import="card_approval_mapping.Card_approval_mappingDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="card_approval_mapping.Card_approval_mappingDAO" %>
<%@ page import="lost_card_info.Lost_card_infoDTO" %>
<%@ page import="lost_card_info.Lost_card_infoDAO" %>
<%@ page import="files.FilesDAO" %>
<%@ page import="files.FilesDTO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>


<%
    String fileColumnName = "";
    String servletName = "Lost_card_infoServlet";
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    Card_infoDTO card_infoDTO = (Card_infoDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    Lost_card_infoDTO lostCardInfoDTO = Lost_card_infoDAO.getInstance().getByReIssueCardInfoId(card_infoDTO.iD);
    CardEmployeeInfoDTO cardEmployeeInfoDTO = CardEmployeeInfoRepository.getInstance().getById(card_infoDTO.cardEmployeeInfoId);
    CardEmployeeOfficeInfoDTO cardEmployeeOfficeInfoDTO = CardEmployeeOfficeInfoRepository.getInstance().getById(card_infoDTO.cardEmployeeOfficeInfoId);
    CardEmployeeImagesDTO cardEmployeeImagesDTO = CardEmployeeImagesDAO.getInstance().getDTOFromID(card_infoDTO.cardEmployeeImagesId);
    byte[] encodeBase64Photo;
    List<Card_approval_mappingDTO> cardApprovalMappingDTOList = Card_approval_mappingDAO.getInstance().getAllApprovalDTOByCardInfoId(card_infoDTO.iD);
    List<Long> cardApprovalIds = cardApprovalMappingDTOList.stream()
            .map(e -> e.cardApprovalId)
            .collect(Collectors.toList());
    List<CardApprovalDTO> cardApprovalDTOList = CardApprovalRepository.getInstance().getByIds(cardApprovalIds);
    Map<Long, CardApprovalDTO> mapCardApprovalDTOById = cardApprovalDTOList.stream()
            .collect(Collectors.toMap(e -> e.iD, e -> e, (e1, e2) -> e1));
    Map<Boolean, List<Card_approval_mappingDTO>> groupByPendingStatus = cardApprovalMappingDTOList.stream()
            .collect(Collectors.partitioningBy(e -> e.cardApprovalStatusCat == ApprovalStatus.PENDING.getValue()));
    List<CardApprovalDTO> pendingCardApprovalDTOList = new ArrayList<>(groupByPendingStatus.get(true)
            .stream()
            .map(e -> mapCardApprovalDTOById.get(e.cardApprovalId))
            .collect(Collectors.toMap(e -> e.officeUnitId, e -> e, (e1, e2) -> e1))
            .values());
    FilesDAO filesDAO = new FilesDAO();
%>

<!-- <div class="modal-content viewmodal"> -->
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content" style="background: white">
    <div class="kt-portlet shadow-none">
        <div class="kt-portlet__body">
            <div class="office-div">
                <div>
                    <h3>
                        <%=LM.getText(LC.CARD_INFO_ADD_CARD_INFO_ADD_FORMNAME, loginDTO)%>
                    </h3>
                    <div class="table-responsive">
                        <table class="table table-bordered text-nowrap" style="border: 0">
                            <tr>
                                <td style="float: right">
                                    <%
                                        if (cardEmployeeImagesDTO != null && cardEmployeeImagesDTO.photo != null) {
                                            encodeBase64Photo = Base64.encodeBase64(cardEmployeeImagesDTO.photo);
                                        } else {
                                            encodeBase64Photo = null;
                                        }
                                    %>
                                    <img width="120px" height="120px"
                                         src='data:image/jpg;base64,<%=encodeBase64Photo != null ? new String(encodeBase64Photo) : ""%>'>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap">
                            <%
                                if (card_infoDTO.cardStatusCat == CardStatusEnum.REJECTED.getValue()) {
                            %>
                            <tr>
                                <td style="width:15%">
                                    <b><%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_REASON_FOR_DISSATISFACTION, loginDTO)%>
                                    </b></td>
                                <td style="color: red" colspan="3"><%=card_infoDTO.comment%>
                                </td>
                            </tr>
                            <%
                                }
                            %>
                            <tr>
                                <td style="width:15%"><b><%=LM.getText(LC.CARD_INFO_ADD_CARDSTATUSCAT, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                            <span class="btn btn-sm border-0 shadow"
                                  style="background-color: <%=CardStatusEnum.getColor(card_infoDTO.cardStatusCat)%>; color: white; border-radius: 8px;cursor: text">
                                        <%=CatRepository.getInstance().getText(Language, "card_status", card_infoDTO.cardStatusCat)%>
                            </span>
                                    <%
                                        if (lostCardInfoDTO != null) {
                                    %>
                                    &nbsp;&nbsp;
                                    <span class="btn btn-sm border-0 shadow"
                                          style="background-color: darkred; color: white; border-radius: 8px;cursor: text">
                                        <%=LM.getText(LC.LOST_CARD_INFO_ADD_REISSUE_CARD, loginDTO)%>
                            </span>
                                    <%
                                        }
                                    %>
                                </td>

                                <td style="width:15%"><b><%=LM.getText(LC.CARD_INFO_ADD_CARDCAT, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                            <span class="btn btn-sm border-0 shadow"
                                  style="background-color: <%=CardCategoryEnum.getColor(card_infoDTO.cardCat)%>; color: white; border-radius: 8px;cursor: text">
                                        <%=CatRepository.getInstance().getText(Language, "card", card_infoDTO.cardCat)%>
                            </span>
                                </td>

                            </tr>

                            <tr>
                                <td style="width:15%">
                                    <b><%=LM.getText(LC.CARD_INFO_ADD_ISPOLICEVERIFICATIONREQUIRED, loginDTO)%>
                                    </b></td>
                                <td style="width: 35%">
                                    <%=StringUtils.getYesNo(Language, card_infoDTO.isPoliceVerificationRequired)%>
                                </td>
                                <td style="width:15%"><b><%=LM.getText(LC.CARD_INFO_ADD_VALIDATIONFROM, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%=StringUtils.getFormattedDate(Language, card_infoDTO.validationFrom)%>&nbsp;
                                    <b><%=!StringUtils.getFormattedDate(Language, card_infoDTO.validationFrom).equals("") ? (isLanguageEnglish ? "to" : "থেকে") : ""%>
                                    </b>&nbsp;
                                    <%=StringUtils.getFormattedDate(Language, card_infoDTO.validationTo)%>
                                </td>
                            </tr>

                            <tr>
                                <td style="width:15%"><b><%=LM.getText(LC.CARD_INFO_ADD_APPLICANT_NAME, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%=isLanguageEnglish ? cardEmployeeInfoDTO.nameEn : cardEmployeeInfoDTO.nameBn%>
                                </td>

                                <td style="width:15%"><b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_DATEOFBIRTH, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%=StringUtils.getFormattedDate(Language, cardEmployeeInfoDTO.dob)%>
                                </td>
                            </tr>

                            <tr>
                                <td style="width:15%"><b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_FATHER_NAME, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%=isLanguageEnglish ? cardEmployeeInfoDTO.fatherNameEn : cardEmployeeInfoDTO.fatherNameBn%>
                                </td>

                                <td style="width:15%"><b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_MOTHER_NAME, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%=isLanguageEnglish ? cardEmployeeInfoDTO.motherNameEn : cardEmployeeInfoDTO.motherNameBn%>
                                </td>
                            </tr>

                            <tr>
                                <td style="width:15%"><b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_POST, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%=isLanguageEnglish ? cardEmployeeOfficeInfoDTO.organogramEng : cardEmployeeOfficeInfoDTO.organogramBng%>
                                </td>

                                <td style="width:15%"><b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_OFFICE, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%=isLanguageEnglish ? cardEmployeeOfficeInfoDTO.officeUnitEng : cardEmployeeOfficeInfoDTO.officeUnitBng%>
                                </td>
                            </tr>

                            <tr>
                                <td style="width:15%"><b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_PRESENTADDRESS, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%=isLanguageEnglish ? cardEmployeeInfoDTO.presentAddressEn : cardEmployeeInfoDTO.presentAddressBn%>
                                </td>

                                <td style="width:15%"><b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_PERMANENTADDRESS, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%=isLanguageEnglish ? cardEmployeeInfoDTO.permanentAddressEn : cardEmployeeInfoDTO.permanentAddressBn%>
                                </td>
                            </tr>

                            <tr>
                                <td style="width:15%"><b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_HEIGHT, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(cardEmployeeInfoDTO.height))%>
                                </td>

                                <td style="width:15%"><b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_IDENTIFICATION_MARK, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%=cardEmployeeInfoDTO.identificationSign%>
                                </td>
                            </tr>

                            <tr>
                                <td style="width:15%"><b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_BLOODGROUP, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%=CatRepository.getInstance().getText(Language, "blood_group", cardEmployeeInfoDTO.bloodGroup)%>
                                </td>

                                <td style="width:15%"><b><%=LM.getText(LC.HR_MANAGEMENT_OTHERS_OFFICE_PHONE_NUMBER, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, cardEmployeeInfoDTO.mobileNumber)%>
                                </td>
                            </tr>

                            <tr>
                                <td style="width:15%"><b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NID, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, cardEmployeeInfoDTO.nid)%>
                                </td>
                            </tr>

                            <tr>
                                <td style="width:15%">
                                    <b><%=LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_NID_FRONT_SIDE, loginDTO)%>
                                    </b></td>
                                <td style="width: 35%">
                                    <%
                                        if (cardEmployeeImagesDTO != null && cardEmployeeImagesDTO.nidFrontSide != null) {
                                            encodeBase64Photo = Base64.encodeBase64(cardEmployeeImagesDTO.nidFrontSide);
                                        } else {
                                            encodeBase64Photo = null;
                                        }
                                    %>
                                    <img width="400px" height="267px"
                                         src='data:image/jpg;base64,<%=encodeBase64Photo != null ? new String(encodeBase64Photo) : ""%>'>
                                </td>

                                <td style="width:15%"><b><%=LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_NID_BACK_SIDE, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%
                                        if (cardEmployeeImagesDTO != null && cardEmployeeImagesDTO.nidBackSide != null) {
                                            encodeBase64Photo = Base64.encodeBase64(cardEmployeeImagesDTO.nidBackSide);
                                        } else {
                                            encodeBase64Photo = null;
                                        }
                                    %>
                                    <img width="400px" height="267px"
                                         src='data:image/jpg;base64,<%=encodeBase64Photo != null ? new String(encodeBase64Photo) : ""%>'>
                                </td>
                            </tr>

                            <%
                                if (lostCardInfoDTO != null) {
                            %>
                            <tr>
                                <td style="width:15%"><b><%=LM.getText(LC.LOST_CARD_INFO_ADD_GD_COPY, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%
                                        {
                                            fileColumnName = "gdCopyFileDropzone";
                                            List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(lostCardInfoDTO.gdCopyFileDropzone);
                                    %>
                                    <%@include file="../pb/dropzoneEditor2.jsp" %>
                                    <%
                                        }
                                    %>
                                </td>

                                <td style="width:15%"><b><%=LM.getText(LC.LOST_CARD_INFO_ADD_MONEY_RECEIPT_COPY, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%
                                        {
                                            fileColumnName = "moneyReceiptFileDropzone";
                                            List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(lostCardInfoDTO.moneyReceiptFileDropzone);
                                    %>
                                    <%@include file="../pb/dropzoneEditor2.jsp" %>
                                    <%
                                        }
                                    %>
                                </td>
                            </tr>
                            <%
                                }
                            %>
                        </table>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered text-nowrap" style="border: 0">
                            <tr>
                                <td style="float: right">
                                    <%
                                        if (cardEmployeeImagesDTO != null && cardEmployeeImagesDTO.signature != null) {
                                            encodeBase64Photo = Base64.encodeBase64(cardEmployeeImagesDTO.signature);
                                        } else {
                                            encodeBase64Photo = null;
                                        }
                                    %>
                                    <img width="200px" height="54px"
                                         src='data:image/jpg;base64,<%=encodeBase64Photo != null ? new String(encodeBase64Photo) : ""%>'>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div id="card_requester_info">
                <div class="col-12">
                    <h3>
                        <%=LM.getText(LC.CARD_INFO_REQUESTER_INFORMATION, loginDTO)%>
                    </h3>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.CARD_INFO_NAME, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.CARD_INFO_DESIGNATION_OFFICE, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.CARD_INFO_REQUEST_CREATE_DATE_TIME, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td style="width: 35%"><%=isLanguageEnglish ? card_infoDTO.insertByNameEng : card_infoDTO.insertByNameBng%>
                                </td>
                                <td style="width: 35%">
                                    <b><%=isLanguageEnglish ? card_infoDTO.insertByOfficeUnitOrganogramEng : card_infoDTO.insertByOfficeUnitOrganogramBng%>
                                    </b>
                                    <br><%=isLanguageEnglish ? card_infoDTO.insertByOfficeUnitEng : card_infoDTO.insertByOfficeUnitBng%>
                                </td>
                                <td style="width: 35%"><%=StringUtils.convertToDateAndTime(isLanguageEnglish, card_infoDTO.insertionTime)%>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="approval_info_div">
                <div class="col-12">
                    <h3><%=LM.getText(LC.CARD_INFO_APPROVAL_INFORMATION, loginDTO)%>
                    </h3>
                   <div class="table-responsive">
                       <table class="table table-bordered table-striped text-nowrap">
                           <thead>
                           <tr>
                               <th><%=LM.getText(LC.CARD_INFO_APPROVER_OFFICE_INFORMATION, loginDTO)%>
                               </th>
                               <th><%=LM.getText(LC.CARD_INFO_APPROVAL_STATUS, loginDTO)%>
                               </th>
                               <th><%=LM.getText(LC.CARD_INFO_APPROVAL_DATE_TIME, loginDTO)%>
                               </th>
                           </tr>
                           </thead>
                           <tbody>
                           <%
                               for (CardApprovalDTO cardApprovalDTO : pendingCardApprovalDTOList) {
                           %>
                           <tr>
                               <td style="width: 35%">
                                   <b><%=isLanguageEnglish ? cardApprovalDTO.officeUnitEng : cardApprovalDTO.officeUnitBng%>
                                   </b></td>
                               <td style="width: 35%">
                                    <span class="btn btn-sm border-0 shadow"
                                          style="background-color: <%=ApprovalStatus.getColor(ApprovalStatus.PENDING.getValue())%>; color: white; border-radius: 8px;cursor: text">
                                        <%=CatRepository.getInstance().getText(Language, "card_approval_status", ApprovalStatus.PENDING.getValue())%>
                                    </span>
                               </td>
                               <td style="width: 35%"></td>
                           </tr>
                           <%
                               }
                           %>
                           <%
                               for (Card_approval_mappingDTO cardApprovalMappingDTO : groupByPendingStatus.get(false)) {
                                   CardApprovalDTO cardApprovalDTO = mapCardApprovalDTOById.get(cardApprovalMappingDTO.cardApprovalId);
                           %>
                           <tr>
                               <td style="width: 35%">
                                   <b><%=isLanguageEnglish ? cardApprovalDTO.officeUnitEng : cardApprovalDTO.officeUnitBng%>
                                   </b></td>
                               <td style="width: 35%">
                                    <span class="btn btn-sm border-0 shadow"
                                          style="background-color: <%=ApprovalStatus.getColor(cardApprovalMappingDTO.cardApprovalStatusCat)%>; color: white; border-radius: 8px;cursor: text">
                                        <%=CatRepository.getInstance().getText(Language, "card_approval_status", cardApprovalMappingDTO.cardApprovalStatusCat)%>
                                    </span>
                               </td>
                               <td style="width: 35%">
                                   <%=StringUtils.convertToDateAndTime(isLanguageEnglish, cardApprovalMappingDTO.lastModificationTime)%>
                               </td>
                           </tr>
                           <%
                               }
                           %>
                           </tbody>
                       </table>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>