<%@page pageEncoding="UTF-8" %>
<%@page import="language.LM" %>
<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="card_info.*" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="java.util.*" %>
<%@ page import="lost_card_info.Lost_card_infoDTO" %>
<%@ page import="lost_card_info.Lost_card_infoDAO" %>
<%@ page import="common.BaseServlet" %>


<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    RecordNavigator rn2 = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
%>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped" style="width: 100%">
        <thead class="text-nowrap">
            <tr>
                <th style="text-align: center"><%=LM.getText(LC.CARD_INFO_REQUESTER_INFORMATION_DESIGNATION_OFFICE, loginDTO)%></th>
                <th style="text-align: center"><%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARDINFOID, loginDTO)%></th>
                <th style="text-align: center"><%=LM.getText(LC.CARD_INFO_ADD_CARDSTATUSCAT, loginDTO)%></th>
                <th style="text-align: center" colspan="2"><%=LM.getText(LC.CARD_APPROVAL_MAPPING_CARDHOLDER_INFORMATION, loginDTO)%></th>
                <th style="text-align: center"><%=LM.getText(LC.CARD_INFO_ADD_ISPOLICEVERIFICATIONREQUIRED, loginDTO)%></th>
                <th style="text-align: center"><%=LM.getText(LC.CARD_INFO_ADD_VALIDATIONFROM, loginDTO)%></th>
                <th style="text-align: center"></th>
            </tr>
        </thead>
        <tbody class="text-nowrap">
        <%
            List<Card_infoDTO> data = (List<Card_infoDTO>) rn2.list;
            List<Long> employeeInfoIds = new ArrayList<>();
            List<Long> imageInfoIds = new ArrayList<>();
            List<Long> employeeOfficeIds = new ArrayList<>();
            List<Long> lostCardInfoIds = new ArrayList<>();
            if (data != null && data.size() > 0) {
                for(Card_infoDTO dto : data){
                    employeeInfoIds.add(dto.cardEmployeeInfoId);
                    imageInfoIds.add(dto.cardEmployeeImagesId);
                    employeeOfficeIds.add(dto.cardEmployeeOfficeInfoId);
                    if(dto.cardCat == CardCategoryEnum.PERMANENT.getValue() && dto.cardStatusCat ==  CardStatusEnum.LOST.getValue()){
                        lostCardInfoIds.add(dto.iD);
                    }
                }
                Map<Long,CardEmployeeInfoDTO> cardEmployeeInfoDTOMap = CardEmployeeInfoRepository.getInstance()
                        .getByIds(employeeInfoIds)
                        .stream()
                        .collect(Collectors.toMap(e->e.iD,e->e,(e1,e2)->e1));
                Map<Long,CardEmployeeOfficeInfoDTO> cardEmployeeOfficeInfoDTOMap = CardEmployeeOfficeInfoRepository.getInstance()
                        .getByIds(employeeOfficeIds)
                        .stream()
                        .collect(Collectors.toMap(e->e.iD,e->e,(e1,e2)->e1));
                Map<Long,CardEmployeeImagesDTO> cardEmployeeImagesDTOMap = CardEmployeeImagesDAO.getInstance().getPhotosByIds(imageInfoIds)
                        .stream()
                        .collect(Collectors.toMap(e->e.iD,e->e,(e1,e2)->e1));
                Map<Long,Lost_card_infoDTO> notIssuedMap = null;
                if(lostCardInfoIds.size()>0){
                    List<Lost_card_infoDTO> lostCardInfoDTOList = Lost_card_infoDAO.getInstance().getLostCardWhichNotIssued(lostCardInfoIds,HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO);
                    notIssuedMap = lostCardInfoDTOList.stream()
                            .collect(Collectors.toMap(e->e.cardInfoId,e->e));
                }
                if(notIssuedMap == null){
                    notIssuedMap = new HashMap<>();
                }
                for (int i = 0; i < data.size(); i++) {
                    Card_infoDTO card_infoDTO = data.get(i);
        %>
            <tr id='tr_<%=i%>'><%@include file="card_infoSearchRow.jsp" %></tr>
        <% } } %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>

<script>
    function markLost(cardInfoId, buttonElement, cardStatusId,actionTdId,isPermanentCard) {
        let message = '<%=isLanguageEnglish? "Are you sure to mark card as LOST?" : "আপনি কি কার্ডটি হারানো হিসেবে চিহ্নিত করতে নিশ্চিত?"%>';
        let title = '<%=isLanguageEnglish? "LOST" : "হারানো গিয়াছে"%>';
        let cancelBtnText = '<%=isLanguageEnglish? "CANCEL" : "বাতিল"%>';
        let cardStatusText = '<%=CatRepository.getInstance().getText(Language,"card_status",CardStatusEnum.LOST.getValue())%>';

        messageDialog('', message, 'error', true, title, cancelBtnText, async () => {
            const response = await fetch('Lost_card_infoServlet?actionType=ajax_reportLostCard', {
                method: 'post',
                body: 'cardInfoId=' + cardInfoId,
                headers: {'Content-type': 'application/x-www-form-urlencoded'}
            });
            const json = await response.json();
            if (json.success) {
                buttonElement.remove();
                let cardStatusIdSelector = '#' + cardStatusId;
                $(cardStatusIdSelector).children('span').text(cardStatusText);
                $(cardStatusIdSelector).children('span').css('background-color', '<%=CardStatusEnum.getColor(CardStatusEnum.LOST.getValue())%>');
                if(isPermanentCard){
                    let hLink = '\"location.href='+ "\'Lost_card_infoServlet?actionType=getEditPage&ID="+json.iD+ "\'" + '\"';
                    let addedIssuedButton = '<div style="height: 10px"></div>'+
                        ' <button type="button" class="btn btn-sm border-0 btn-success shadow"'+
                        ' style="color: white; border-radius: 8px"'+
                        ' onclick='+hLink+'>'
                        +'<%=LM.getText(LC.LOST_CARD_INFO_ADD_REISSUE_CARD, loginDTO)%>'+
                        '</button>';
                    $('#'+actionTdId).append(addedIssuedButton);
                }
            }
        });
    }
</script>