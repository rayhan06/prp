<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String formTitle = LM.getText(LC.CARD_INFO_ADD_CARD_INFO_ADD_FORMNAME, loginDTO);
    String Language = LM.getText(LC.CARD_INFO_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    String context = request.getContextPath() + "/";
%>
<style>
    .top-section-font {
        color: #00a1d4;
        font-weight: 500;
    }

    .page-bg {
        background-color: #f9f9fb;
    }
</style>

<!-- begin:: Subheader -->
<div class="ml-4 mt-4">
    <div class="kt-subheader__main">
        <h2 class="kt-subheader__title" style="color: #00a1d4;">
            <i class="far fa-address-card"></i> <%=formTitle%>
        </h2>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content" id="kt_content">
    <div class="row">
        <div class="kt-portlet" style="background-color: #F2F2F2!important;">
            <div class="kt-portlet__body m-4" style="background-color: #f6f9fb">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                            <img width="15%"
                                 src="<%=context%>assets/static/parliament_logo.png"
                                 alt="logo"
                                 class="logo-default"
                            />
                            <h2 class="text-center mt-3 top-section-font">
                                <%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
                            </h2>
                            <h4 class="text-center mt-2 top-section-font">
                                <%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                            </h4>
                        </div>
                    </div>
                </div>
                <form class="form-horizontal" id="temporaryCardForm" name="temporaryCardForm" enctype="multipart/form-data">
                    <div class="row my-5">
                        <div class="col-md-6 row ">
                            <div class="col-12">
                                <label class="h5">
                                    <%=LM.getText(LC.CARD_INFO_ADD_APPLICANT_NAME, loginDTO)%>
                                <span class="required"> * </span>
                            </label></div>
                            <div class="col-12">
                                <input type='text' class='form-control rounded-lg'
                                                       name='name'
                                                       id='name'
                                                       maxlength="64"
                                                       tag='pb_html'/>
                            </div>
                        </div>
                        <div class="col-md-6 row my-3">
                            <div class="col-12"><label
                                    class="h5"><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_DATEOFBIRTH, loginDTO)%>
                                <span class="required"> * </span>
                            </label></div>
                            <div class="col-12">
                                <jsp:include page="/date/date.jsp">
                                    <jsp:param name="DATE_ID" value="date-of-birth-js"/>
                                    <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                </jsp:include>
                                <input type='hidden'
                                       class='form-control'
                                       id='dateOfBirth'
                                       name='dateOfBirth'
                                       value='' tag='pb_html'/>
                            </div>
                        </div>
                        <div class="col-md-6 row my-3">
                            <div class="col-12"><label
                                    class="h5"><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_POST, loginDTO)%>
                                <span class="required"> * </span>
                            </label></div>
                            <div class="col-12"><input type='text' class='form-control rounded-lg'
                                                       name='currentPost'
                                                       id='currentPost'
                                                       maxlength="64"
                                                       tag='pb_html'/></div>
                        </div>
                        <div class="col-md-6 row my-3">
                            <div class="col-12"><label
                                    class="h5"><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_OFFICE, loginDTO)%>
                                <span class="required"> * </span>
                            </label></div>
                            <div class="col-12"><input type='text' class='form-control rounded-lg'
                                                       name='currentOffice'
                                                       id='currentOffice'
                                                       maxlength="64"
                                                       tag='pb_html'/></div>
                        </div>
                        <div class="col-md-6 row my-3">
                            <div class="col-12"><label
                                    class="h5"><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_FATHERNAMEENG, loginDTO).replaceAll("\\(.+\\)", "")%>
                                <span class="required"> * </span>
                            </label></div>
                            <div class="col-12"><input type='text' class='form-control rounded-lg'
                                                       name='fatherName'
                                                       id='fatherName'
                                                       maxlength="64"
                                                       tag='pb_html'/></div>
                        </div>
                        <div class="col-md-6 row my-3">
                            <div class="col-12"><label
                                    class="h5"><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_MOTHERNAMEENG, loginDTO).replaceAll("\\(.+\\)", "")%>
                                <span class="required"> * </span>
                            </label></div>
                            <div class="col-12"><input type='text' class='form-control rounded-lg'
                                                       name='motherName'
                                                       id='motherName'
                                                       maxlength="64"
                                                       tag='pb_html'/></div>
                        </div>
                        <div class="col-md-6 row my-3">
                            <div class="col-12"><label class="h5"><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NID, loginDTO)%>
                                <span class="required"> * </span>
                            </label></div>
                            <div class="col-12"><input maxlength="20" type='text'
                                                       class='form-control rounded-lg' required="required"
                                                       name='nid'
                                                       id='nid'
                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_NATIONAL_IDENTITY_NUMBER, loginDTO)%>'
                                                       value=''/></div>
                        </div>
                        <div class="col-md-6 row my-3">
                            <div class="col-12"><label
                                    class="h5"><%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PRESENTADDRESS_ENG, loginDTO).replaceAll("\\(.+\\)", "")%>
                                <span class="required"> * </span>
                            </label></div>
                            <div class="col-12">
                                <input type='text' class='form-control rounded-lg'
                                       name='presentAddress'
                                       id='presentAddress'
                                       maxlength="64"
                                       tag='pb_html'/>
                            </div>
                        </div>
                        <div class="col-md-6 row my-3">
                            <div class="col-12"><label
                                    class="h5"><%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PERMANENTADDRESS_ENG, loginDTO).replaceAll("\\(.+\\)", "")%>
                                <span class="required"> * </span>
                            </label></div>
                            <div class="col-12"><input type='text' class='form-control rounded-lg'
                                                       name='permanentAddress'
                                                       id='permanentAddress'
                                                       maxlength="64"
                                                       tag='pb_html'/></div>
                        </div>
                        <div class="col-md-6 row my-3">
                            <div class="col-12"><label
                                    class="h5"><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_HEIGHT, loginDTO)%>
                            </label></div>
                            <div class="col-12"><input class="form-control rounded" type="text"
                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_HEIGHT_IN_CM, loginDTO)%>'
                                                       name="height" id="height"
                                                       pattern="^\d*(\.\d{0,2})?$"
                                                       value=''/></div>
                        </div>
                        <div class="col-md-6 row my-3">
                            <div class="col-12"><label
                                    class="h5"><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_IDENTIFICATION_MARK, loginDTO)%>
                            </label></div>
                            <div class="col-12"><input type='text' class='form-control rounded-lg'
                                                       name='identificationMark'
                                                       id='identificationMark'
                                                       maxlength="64"
                                                       tag='pb_html'/></div>
                        </div>
                        <div class="col-md-6 row my-3">
                            <div class="col-12"><label
                                    class="h5"><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_BLOODGROUP, loginDTO)%>
                            </label></div>
                            <div class="col-12"><select class='form-control rounded' name='bloodGroup'
                                                        id='bloodGroup_select_'>
                                <%=CatRepository.getInstance().buildOptions("blood_group", Language, 0)%>
                            </select></div>
                        </div>
                        <div class="col-md-6 row my-3">
                            <div class="col-12"><label
                                    class="h5"><%=LM.getText(LC.EMPLOYEE_PERSONAL_MOBILE_NUMBER, loginDTO)%>
                            </label></div>
                            <div class="col-12">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">+88</div>
                                    </div>
                                    <input type='text' class='digitOnly form-control rounded'
                                           name='personalMobile' maxlength="11"
                                           id='personalMobile'

                                           placeholder='<%=LM.getText(LC.GLOBAL_MOBILE_PLACE_HOLDER, loginDTO)%>'
                                           title='<%=HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng?
                                                                "Personal mobile number must start with 01, then contain 9 digits"
                                                               :"ব্যক্তিগত মোবাইল নাম্বার 01 দিয়ে শুরু হবে, তারপর ৯টি সংখ্যা হবে"%>'
                                           tag='pb_html'/>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6 row my-3">
                        </div>
                        <div class="col-md-6 row my-3">
                            <div class="col-12"><label
                                    class="h5"><%=LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_PHOTO, loginDTO)%>
                                <span class="required"> * </span>
                            </label></div>
                            <div class="col-12">
                                <input type='file' accept="image/png, image/jpeg"
                                       class='form-control rounded'
                                       name='photo' id='photoImage_blob' tag='pb_html'/>
                            </div>
                        </div>
                        <div class="col-md-6 row my-3">
                            <div class="col-12"><label
                                    class="h5"><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_SIGNATURE, loginDTO)%>
                                <span class="required"> * </span>
                            </label></div>
                            <div class="col-12">
                                <input type='file' accept="image/png, image/jpeg"
                                       class='form-control rounded'
                                       name='signature' id='signature_blob' tag='pb_html'/>
                            </div>
                        </div>
                        <div class="col-md-6 row my-3">
                            <div class="col-12"><label
                                    class="h5"><%=LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_NID_FRONT_SIDE, loginDTO)%>
                                <span class="required"> * </span>
                            </label></div>
                            <div class="col-12">
                                <input type='file' accept="image/png, image/jpeg"
                                       class='form-control rounded'
                                       name='nid_front_photo' id='nidFrontPhoto_blob'
                                       tag='pb_html'/>
                            </div>
                        </div>
                        <div class="col-md-6 row my-3">
                            <div class="col-12"><label
                                    class="h5"><%=LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_NID_BACK_SIDE, loginDTO)%>
                                <span class="required"> * </span>
                            </label></div>
                            <div class="col-12">
                                <input type='file' accept="image/png, image/jpeg"
                                       class='form-control rounded'
                                       name='nid_back_photo' id='nidBackPhoto_blob'
                                       tag='pb_html'/>
                            </div>
                        </div>
                    </div>
                    <div class="row my-5">
                        <div class="col-12 text-right pr-md-5">
                            <button id="cancel_btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.CARD_INFO_ADD_CARD_INFO_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit_btn" class="btn-sm shadow text-white border-0 submit-btn ml-2 mr-3" type="button"
                                onclick="submitForm()">
                                <%=LM.getText(LC.CARD_INFO_ADD_CARD_INFO_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<%=context%>/assets/scripts/util1.js"></script>
<script src="<%=context%>/assets/scripts/input_validation.js"></script>
<script type="text/javascript">
    const form = $("#temporaryCardForm");
    function submitForm(){
        buttonStateChange(true);
        if(PreprocessBeforeSubmiting(form)){
            $.ajax({
                type : "POST",
                url : "Card_infoServlet?actionType=ajax_addTemporaryCard",
                cache: false,
                data : new FormData(form[0]),
                processData: false,
                contentType: false,
                dataType : 'JSON',
                success : function(response) {
                    if(response.responseCode === 0){
                        $('#toast_message').css('background-color','#ff6063');
                        showToastSticky(response.msg,response.msg);
                        buttonStateChange(false);
                    }else if(response.responseCode === 200){
                        window.location.replace(getContextPath()+response.msg);
                    }
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        }else{
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value){
        $('#cancel_btn').prop('disabled',value);
        $('#submit_btn').prop('disabled',value);
    }

    $(document).ready(function () {
        select2SingleSelector("#bloodGroup_select_", '<%=Language%>');
        $.validator.addMethod('nidFrontSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('nidBackSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('photoSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('signatureSelection', function (value, element) {
            return value != 0;
        });
        $("#temporaryCardForm").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                name:"required",
                fatherName:"required",
                motherName:"required",
                nid:"required",
                presentAddress:"required",
                permanentAddress:"required",
                currentOffice:"required",
                currentPost:"required",
                nid_front_photo: {
                    nidFrontSelection: true
                },
                nid_back_photo: {
                    nidBackSelection: true
                },
                photo:{
                    photoSelection:true
                },
                signature:{
                    signatureSelection:true
                }
            },
            messages: {
                name: "<%=LM.getText(LC.ERROR_MESSAGE_PLEASE_ENTER_YOUR_NAME, loginDTO)%>",
                fatherName: "<%=LM.getText(LC.ERROR_MESSAGE_PLEASE_ENTER_YOUR_FATHERS_NAME, loginDTO)%>",
                motherName: "<%=LM.getText(LC.ERROR_MESSAGE_PLEASE_ENTER_YOUR_MOTHERS_NAME, loginDTO)%>",
                nid: "<%=LM.getText(LC.ERROR_MESSAGE_PLEASE_ENTER_YOUR_NID, loginDTO)%>",
                presentAddress: "<%=LM.getText(LC.ERROR_MESSAGE_ENTER_PRESENT_ADDRESS, loginDTO)%>",
                permanentAddress: "<%=LM.getText(LC.ERROR_MESSAGE_ENTER_PERMANENT_ADDRESS, loginDTO)%>",
                photo: "<%=LM.getText(LC.ERROR_MESSAGE_UPLOAD_YOUR_PHOTO, loginDTO)%>",
                signature: "<%=LM.getText(LC.ERROR_MESSAGE_UPLOAD_YOUR_SIGNATURE, loginDTO)%>",
                nid_front_photo: "<%=LM.getText(LC.ERROR_MESSAGE_UPLOAD_NID_FRONT_SIDE, loginDTO)%>",
                nid_back_photo: "<%=LM.getText(LC.ERROR_MESSAGE_UPLOAD_NID_BACK_SIDE, loginDTO)%>",
                currentOffice : "<%=LM.getText(LC.ERROR_MESSAGE_OFFICE_NAME, loginDTO)%>",
                currentPost : "<%=LM.getText(LC.ERROR_MESSAGE_DESIGNATION, loginDTO)%>",
            }
        });
    });

    function PreprocessBeforeSubmiting($form) {
        $('#dateOfBirth').val(getDateStringById('date-of-birth-js'));
        console.log($('#temporaryCardForm'));
        const validDob = dateValidator('date-of-birth-js', true, {
            'errorEn': 'Enter Date of Birth!',
            'errorBn': 'জন্ম তারিখ প্রবেশ করুন!'
        });
        const jQueryValid = $form.valid();
        return jQueryValid && validDob;
    }

    $(document).ready(() => {
        $("#cancel_btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    });
</script>



