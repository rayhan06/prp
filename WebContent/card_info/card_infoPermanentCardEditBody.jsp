<%@page import="login.LoginDTO" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="user.UserDTO" %>
<%@ page import="common.RoleEnum" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String formTitle = LM.getText(LC.CARD_INFO_ADD_CARD_INFO_ADD_FORMNAME, loginDTO);
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isAdmin = userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId();
    String context = request.getContextPath() + "/";
    String action;
    long lostCardId = 0;
    if (request.getParameter("actionType").equalsIgnoreCase("reissue_card")) {
        lostCardId = Long.parseLong(request.getParameter("lostCardId"));
        action = "reissue_card";
    } else {
        action = "addPermanentCard";
    }
%>
<style>
    .fly-in-from-down {
        animation: flyFromDown 1s ease-out;
    }

    @keyframes flyFromDown {
        0% {
            transform: translateY(200%);
        }
        100% {
            transform: translateY(0%);
        }
    }

    .top-section-font {
        color: #00a1d4;
        font-weight: 500;
    }

    .page-bg {
        background-color: #f9f9fb;
    }
</style>

<!-- begin:: Subheader -->
<div class="ml-4 mt-4">
    <div class="kt-subheader__main">
        <h2 class="kt-subheader__title" style="color: #00a1d4;">
            <i class="far fa-address-card"></i> <%=formTitle%>
        </h2>
    </div>
    <%
        if (isAdmin) {
    %>
    <div class="text-center ml-4 mt-4">
        <%
            if (userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId()) {
        %>
        <button type="button" class="btn btn-primary shadow btn-border-radius" id="self-info-btn">
            <%=LM.getText(LC.CARD_INFO_ADD_SELF_INFORMATION, loginDTO)%>
        </button>
        <%
            }
        %>

        <button type="button" class="btn btn-info shadow btn-border-radius" id="select-employee-btn">
            <%=LM.getText(LC.CARD_INFO_ADD_SELECT_EMPLOYEE, loginDTO)%>
        </button>
        <div class="mt-4 mr-4" id="employee_record_id_div" style="display: none">
            <h3 id="employee_record_id_text"></h3>
        </div>
    </div>
    <%
        }
    %>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content fly-in-from-down" id="kt_content" style="display: none">
    <div class="kt-portlet" style="background-color: #F2F2F2!important;">
        <div class="kt-portlet__body m-4" style="background-color: #f6f9fb">
            <div class="row">
                <div class="col-12 row">
                    <div class="col-md-6 offset-md-3 text-center">
                        <img width="30%"
                             src="<%=context%>assets/static/parliament_logo.png"
                             alt="logo"
                             class="logo-default"
                        />
                        <h2 class="text-center mt-3 top-section-font">
                            <%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
                        </h2>
                        <h4 class="text-center mt-2 top-section-font">
                            <%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                        </h4>
                    </div>

                    <div class="col-md-3 text-right" id="photo_div my-4 my-md-0">
                        <img src="" alt="employee photo" width="45%" id="photo">
                    </div>

                    <div class="col-md-3 text-right" style="display: none" id="empty_photo_div">
                        <div style="float: right; width: 150px;height: 150px; border: red 2px solid;position: relative;">
                            <div style="position: absolute;top: 45%;width:100%;text-align: center;">
                                <h3 style="color: red"><%=LM.getText(LC.ERROR_MESSAGE_PHOTO_OF_CARDHOLDER, loginDTO)%>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h3 id="missing_info_div"
                style="color: red;text-align: center;width: 100%;display: none"><%=LM.getText(LC.ERROR_MESSAGE_PLEASE_FILL_PROFILE_MISSING_INFO, loginDTO)%>
            </h3>
            <div class="row my-5">
                <div class="col-md-6 row my-4 my-3">
                    <div class="col-12" id="name_label">
                        <label class="h5"><%=LM.getText(LC.CARD_INFO_ADD_APPLICANT_NAME, loginDTO)%>
                        </label>
                    </div>
                    <div class="col-12">
                        <label class="h5" id="name"></label>
                    </div>
                </div>

                <div class="col-md-6 row my-4 my-3">
                    <div class="col-12">
                        <label class="h5" id="dateOfBirth_label">
                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_DATEOFBIRTH, loginDTO)%>
                        </label>
                    </div>
                    <div class="col-12">
                        <label class="h5" id="dateOfBirth"></label>
                    </div>
                </div>

                <div class="col-md-6 row my-4 my-3">
                    <div class="col-12">
                        <label class="h5" id="post_label">
                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_POST, loginDTO)%>
                        </label>
                    </div>
                    <div class="col-12">
                        <label class="h5" id="post"></label>
                    </div>
                </div>

                <div class="col-md-6 row my-4 my-3">
                    <div class="col-12">
                        <label class="h5" id="office_label">
                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_OFFICE, loginDTO)%>
                        </label>
                    </div>
                    <div class="col-12">
                        <label id="office" class="h5"></label>
                    </div>
                </div>

                <div class="col-md-6 row my-4 my-3">
                    <div class="col-12">
                        <label class="h5" id="fatherName_label">
                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_FATHERNAMEENG, loginDTO).replaceAll("\\(.+\\)", "")%>
                        </label>
                    </div>
                    <div class="col-12">
                        <label class="h5" id="fatherName"></label>
                    </div>
                </div>

                <div class="col-md-6 row my-4 my-3">
                    <div class="col-12">
                        <label class="h5" id="motherName_label">
                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_MOTHERNAMEENG, loginDTO).replaceAll("\\(.+\\)", "")%>
                        </label>
                    </div>
                    <div class="col-12">
                        <label class="h5" id="motherName"></label>
                    </div>
                </div>

                <div class="col-md-6 row my-4 my-3">
                    <div class="col-12">
                        <label class="h5" id="nidNumber_label">
                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NID, loginDTO)%>
                        </label>
                    </div>
                    <div class="col-12">
                        <label class="h5" id="nidNumber"></label>
                    </div>
                </div>

                <div class="col-md-6 row my-4 my-3">
                    <div class="col-12">
                        <label class="h5" id="presentAddress_label">
                            <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PRESENTADDRESS_ENG, loginDTO)%>
                        </label>
                    </div>
                    <div class="col-12">
                        <label class="h5" id="presentAddress"></label>
                    </div>
                </div>

                <div class="col-md-6 row my-4 my-3">
                    <div class="col-12">
                        <label class="h5" id="permanentAddress_label">
                            <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PERMANENTADDRESS_ENG, loginDTO)%>
                        </label>
                    </div>
                    <div class="col-12">
                        <label class="h5" id="permanentAddress"></label>
                    </div>
                </div>

                <div class="col-md-6 row my-4 my-3">
                    <div class="col-12">
                        <label class="h5" id="height_label">
                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_HEIGHT, loginDTO)%>
                        </label>
                    </div>
                    <div class="col-12">
                        <label class="h5" id="height"></label>
                    </div>
                </div>

                <div class="col-md-6 row my-4 my-3">
                    <div class="col-12">
                        <label class="h5">
                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_IDENTIFICATION_MARK, loginDTO)%>
                        </label>
                    </div>
                    <div class="col-12">
                        <label class="h5" id="identificationSign"></label>
                    </div>
                </div>

                <div class="col-md-6 row my-4 my-3">
                    <div class="col-12">
                        <label class="h5">
                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_BLOODGROUP, loginDTO)%>
                        </label>
                    </div>
                    <div class="col-12">
                        <label class="h5" id="bloodGroup"></label>
                    </div>
                </div>

                <div class="col-md-6 row my-4 my-3">
                    <div class="col-12">
                        <label class="h5" id="mobileNumber_label">
                            <%=LM.getText(LC.HR_MANAGEMENT_OTHERS_OFFICE_PHONE_NUMBER, loginDTO)%>
                        </label>
                    </div>
                    <div class="col-12">
                        <label class="h5" id="mobileNumber"></label>
                    </div>
                </div>

                <div class="col-md-6 row my-4 my-3">
                    <div class="col-12">
                        <label class="h5" id="signature_label">
                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_SIGNATURE, loginDTO)%>
                        </label>
                    </div>
                    <div class="col-12" id="signature_div">
                        <img src="" alt="employee signature" id="signature" width="30%">
                    </div>
                </div>

                <div class="col-md-6 row my-4 my-3">
                    <div class="col-12">
                        <label class="h5" id="nidFront_label">
                            <%=LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_NID_FRONT_SIDE, loginDTO)%>
                        </label>
                    </div>
                    <div class="col-12" id="nidFront_div">
                        <img src="" alt="employee nid front side" id="nidFront" width="60%">
                    </div>
                </div>

                <div class="col-md-6 row my-4 my-3">
                    <div class="col-12">
                        <label class="h5" id="nidBack_label">
                            <%=LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_NID_BACK_SIDE, loginDTO)%>
                        </label>
                    </div>
                    <div class="col-12" id="nidBack_div">
                        <img src="" alt="employee nid back side" id="nidBack" width="60%">
                    </div>
                </div>

            </div>

            <div class="row my-5" id="action_button">
                <div class="col-12">
                    <form class="form-horizontal text-right" id="permanentCardForm" name="permanentCardForm"
                          action="Card_infoServlet?actionType=ajax_<%=action%>&lostCardId=<%=lostCardId%>"
                          enctype="multipart/form-data">
                        <input type="hidden" name="employeeRecordId" id="employee-record-id-form-input">
                        <a class="btn btn-danger shadow" style="border-radius: 8px;" id="cancel_btn"
                           href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.CARD_INFO_ADD_CARD_INFO_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button type="button" id="submit_btn" class="btn btn-success shadow ml-2"
                                style="border-radius: 8px;"
                                onclick="submitForm()"><%=LM.getText(LC.CARD_INFO_ADD_CARD_INFO_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<%if (isAdmin) {%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<%}%>

<script>
    let dataMissing = false;

    function submitForm() {
        if (dataMissing) return;
        let message = '<%=LM.getText(LC.CARD_INFO_ADD_DO_YOU_WANT_TO_SUBMIT_THIS_CARD, loginDTO)%>';
        let confirmBtnText = '<%=LM.getText(LC.CARD_INFO_ADD_YES_SUBMIT, loginDTO)%>';
        let cancelBtnText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARD_APPROVAL_MAPPING_CANCEL_BUTTON, loginDTO)%>';
        messageDialog('', message, 'success', true, confirmBtnText, cancelBtnText, () => {
            submitAjaxForm('permanentCardForm');
        });
    }

    function buttonStateChange(value) {
        $(':button').prop('disabled', value);
    }

    async function showCardInfo(employeeRecordId) {
        const mainContent = $('#kt_content');
        mainContent.hide();
        mainContent.removeClass('fly-in-from-down');

        const url = 'Card_infoServlet?actionType=ajax_getCardInfoModel&employeeRecordId=' + employeeRecordId + '&language=' + '<%=Language%>';
        const res = await fetch(url);
        const cardInfoModel = await res.json();

        document.getElementById('name').textContent = cardInfoModel.name;
        document.getElementById('post').textContent = cardInfoModel.post;
        document.getElementById('fatherName').textContent = cardInfoModel.fatherName;
        document.getElementById('motherName').textContent = cardInfoModel.motherName;
        document.getElementById('dateOfBirth').textContent = cardInfoModel.dateOfBirth;
        document.getElementById('nidNumber').textContent = cardInfoModel.nidNumber;
        document.getElementById('presentAddress').textContent = cardInfoModel.presentAddress;
        document.getElementById('permanentAddress').textContent = cardInfoModel.permanentAddress;
        document.getElementById('office').textContent = cardInfoModel.office;
        document.getElementById('height').textContent = cardInfoModel.height;
        document.getElementById('identificationSign').textContent = cardInfoModel.identificationSign;
        document.getElementById('bloodGroup').textContent = cardInfoModel.bloodGroup;
        document.getElementById('mobileNumber').textContent = cardInfoModel.mobileNumber;
        document.getElementById('signature').src = cardInfoModel.signatureSrc;
        document.getElementById('photo').src = cardInfoModel.photoSrc;
        document.getElementById('nidFront').src = cardInfoModel.nidFrontSrc;
        document.getElementById('nidBack').src = cardInfoModel.nidBackSrc;
        document.getElementById('employee-record-id-form-input').value = employeeRecordId;

        dataMissing = false;
        if (cardInfoModel.name === "") {
            $('#name_label').css('color', 'red');
            dataMissing = true;
        } else {
            $('#name_label').css('color', 'black');
        }

        if (cardInfoModel.post === "") {
            $('#post_label').css('color', 'red');
            dataMissing = true;
        } else {
            $('#post_label').css('color', 'black');
        }

        if (cardInfoModel.fatherName === "") {
            $('#fatherName_label').css('color', 'red');
            dataMissing = true;
        } else {
            $('#fatherName_label').css('color', 'black');
        }

        if (cardInfoModel.motherName === "") {
            $('#motherName_label').css('color', 'red');
            dataMissing = true;
        } else {
            $('#motherName_label').css('color', 'black');
        }

        if (cardInfoModel.dateOfBirth === "") {
            $('#dateOfBirth_label').css('color', 'red');
            dataMissing = true;
        } else {
            $('#dateOfBirth_label').css('color', 'black');
        }

        if (cardInfoModel.nidNumber === "") {
            $('#nidNumber_label').css('color', 'red');
            dataMissing = true;
        } else {
            $('#nidNumber_label').css('color', 'black');
        }

        if (cardInfoModel.presentAddress === "") {
            $('#presentAddress_label').css('color', 'red');
            dataMissing = true;
        } else {
            $('#presentAddress_label').css('color', 'black');
        }

        if (cardInfoModel.permanentAddress === "") {
            $('#permanentAddress_label').css('color', 'red');
            dataMissing = true;
        } else {
            $('#permanentAddress_label').css('color', 'black');
        }

        if (cardInfoModel.office === "") {
            $('#office_label').css('color', 'red');
            dataMissing = true;
        } else {
            $('#office_label').css('color', 'black');
        }

        if (cardInfoModel.photoSrc === "data:image/jpg;base64,") {
            $('#photo_div').hide();
            $('#empty_photo_div').show();
            dataMissing = true;
        } else {
            $('#photo_div').show();
            $('#empty_photo_div').hide();
        }

        if (cardInfoModel.signatureSrc === "data:image/jpg;base64,") {
            $('#signature_label').css('color', 'red');
            $('#signature_div').hide();
            dataMissing = true;
        } else {
            $('#signature_label').css('color', 'black');
            $('#signature_div').show();
        }

        if (cardInfoModel.nidFrontSrc === "data:image/jpg;base64,") {
            $('#nidFront_label').css('color', 'red');
            $('#nidFront_div').hide();
            dataMissing = true;
        } else {
            $('#nidFront_label').css('color', 'black');
            $('#nidFront_div').show();
        }

        if (cardInfoModel.nidBackSrc === "data:image/jpg;base64,") {
            $('#nidBack_label').css('color', 'red');
            $('#nidBack_div').hide();
            dataMissing = true;
        } else {
            $('#nidBack_label').css('color', 'black');
            $('#nidBack_div').show();
        }

        console.log("cardInfoModel : ", cardInfoModel);
        console.log("dataMissing : ", dataMissing);

        if (dataMissing) {
            $('#missing_info_div').show();
            $("#action_button").hide();
        } else {
            $('#missing_info_div').hide();
            $("#action_button").show();
        }
        mainContent.show();
        <%if(isAdmin){%>
        const empInfo = {
            employeeNameEn: cardInfoModel.name,
            employeeNameBn: cardInfoModel.name,
            organogramNameEn: cardInfoModel.post,
            organogramNameBn: cardInfoModel.post,
            officeUnitNameEn: cardInfoModel.office,
            officeUnitNameBn: cardInfoModel.office
        };
        viewEmployeeRecordIdInInput(empInfo);
        mainContent.addClass('fly-in-from-down');
        <%}%>
    }

    $('#self-info-btn').click(async () => {
        await showCardInfo(<%=userDTO.employee_record_id%>);
    });

    function viewEmployeeRecordIdInInput(empInfo) {
        $('#employee_record_id_modal_button').hide();
        $('#employee_record_id_div').show();

        let language = '<%=Language.toLowerCase()%>'
        let employeeView;
        if (language === 'english') {
            employeeView = empInfo.employeeNameEn + '<br>' + empInfo.organogramNameEn + ', ' + empInfo.officeUnitNameEn;
        } else {
            employeeView = empInfo.employeeNameBn + '<br>' + empInfo.organogramNameBn + ', ' + empInfo.officeUnitNameBn;
        }
        document.getElementById('employee_record_id_text').innerHTML = employeeView;
    }

    table_name_to_collcetion_map = new Map([
        ['employeeRecordId', {
            isSingleEntry: true,
            callBackFunction: empInfo => showCardInfo(empInfo.employeeRecordId)
        }]
    ]);
    // modal row button desatination table in the page
    modal_button_dest_table = 'none';
    // modal trigger button
    $('#select-employee-btn').click(() => {
        modal_button_dest_table = 'employeeRecordId';
        $('#search_emp_modal').modal();
    });

    $(async () => {
        <%if(!isAdmin){%>
        await showCardInfo(<%=userDTO.employee_record_id%>);
        <%}%>
    });
</script>