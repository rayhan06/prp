<%@page pageEncoding="UTF-8" %>
<%@page import="card_info.*" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.*" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%
    CardEmployeeInfoDTO cardEmployeeInfoDTO = cardEmployeeInfoDTOMap.get(card_infoDTO.cardEmployeeInfoId);
    CardEmployeeOfficeInfoDTO cardEmployeeOfficeInfoDTO = cardEmployeeOfficeInfoDTOMap.get(card_infoDTO.cardEmployeeOfficeInfoId);
    CardEmployeeImagesDTO cardEmployeeImagesDTO = cardEmployeeImagesDTOMap.get(card_infoDTO.cardEmployeeImagesId);
    byte[] encodeBase64Photo = null;
    if (cardEmployeeImagesDTO != null && cardEmployeeImagesDTO.photo != null) {
        encodeBase64Photo = Base64.encodeBase64(cardEmployeeImagesDTO.photo);
    }
%>
<td id='<%=i%>_requesterInfo' style="text-align: center">
    <%= isLanguageEnglish ? card_infoDTO.insertByNameEng : card_infoDTO.insertByNameBng%>
    <br><b><%= isLanguageEnglish ? card_infoDTO.insertByOfficeUnitOrganogramEng : card_infoDTO.insertByOfficeUnitOrganogramBng%>
</b>
    <br><%= isLanguageEnglish ? card_infoDTO.insertByOfficeUnitEng : card_infoDTO.insertByOfficeUnitBng%>
</td>
<td id='<%=i%>_cardCat' style="text-align: center">
    <span class="btn btn-sm border-0 shadow"
          style="background-color: <%=CardCategoryEnum.getColor(card_infoDTO.cardCat)%>; color: white; border-radius: 8px;cursor: text">
        <%=CatRepository.getInstance().getText(Language, "card", card_infoDTO.cardCat)%>
    </span><br>
    <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(card_infoDTO.iD))%>
</td>

<td id='<%=i%>_cardStatusCat' style="text-align: center">
    <span class="btn btn-sm border-0 shadow"
          style="background-color:<%=CardStatusEnum.getColor(card_infoDTO.cardStatusCat)%>; color: white; border-radius: 8px;cursor: text">
        <%=CatRepository.getInstance().getText(Language, "card_status", card_infoDTO.cardStatusCat)%>
    </span>
</td>

<td id='<%=i%>_cardEmployeeInfoId' style="text-align: center">
    <%
        String name = "";
        if (cardEmployeeInfoDTO != null) {
            name = isLanguageEnglish ? cardEmployeeInfoDTO.nameEn : cardEmployeeInfoDTO.nameBn;
        }
    %>
    <%=name == null ? "" : name%>
    <br>
    <%
        String officeOrganogramName = "";
        if (cardEmployeeOfficeInfoDTO != null) {
            officeOrganogramName = isLanguageEnglish ? "<b>" + cardEmployeeOfficeInfoDTO.organogramEng + "</b><br>" + cardEmployeeOfficeInfoDTO.officeUnitEng
                    : "<b>" + cardEmployeeOfficeInfoDTO.organogramBng + "</b><br>" + cardEmployeeOfficeInfoDTO.officeUnitBng;
        }
    %>
    <%=officeOrganogramName%>
</td>
<td style="text-align: center">
    <img width="75px" height="75px"
         src='data:image/jpg;base64,<%=encodeBase64Photo != null ? new String(encodeBase64Photo) : ""%>' alt="">
</td>

<td id='<%=i%>_isPoliceVerificationRequired' style="text-align: center">
    <%=StringUtils.getYesNo(Language, card_infoDTO.isPoliceVerificationRequired)%>
</td>

<td id='<%=i%>_validationFrom' style="text-align: center">
    <%=StringUtils.getFormattedDate(Language, card_infoDTO.validationFrom)%>
    <br>
    <b><%=!StringUtils.getFormattedDate(Language, card_infoDTO.validationFrom).equals("") ? (isLanguageEnglish ? "to" : "থেকে") : ""%>
    </b>
    <br>
    <%=StringUtils.getFormattedDate(Language, card_infoDTO.validationTo)%>
</td>

<td style="text-align: center" id="<%=i%>_action">
    <span class="btn btn-sm border-0 shadow" style="background-color: #22ccc1; color: white; border-radius: 8px">
        <a href='Card_infoServlet?actionType=view&iD=<%=card_infoDTO.iD%>'
           style="color: #FFFFFF"><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></a>
    </span>
    <%if (card_infoDTO.cardStatusCat == CardStatusEnum.APPROVED.getValue()) {%>
    <div style="height: 10px"></div>
    <button type="button" class="btn btn-sm btn-danger border-0 shadow" style="color: white; border-radius: 8px"
            onclick="markLost(<%=card_infoDTO.iD%>,this,'<%=i%>_cardStatusCat','<%=i%>_action',<%=card_infoDTO.cardCat == CardCategoryEnum.PERMANENT.getValue()%>);">
        <%=LM.getText(LC.CARD_INFO_ADD_LOST, loginDTO)%>
    </button>
    <%} else if (card_infoDTO.cardStatusCat == CardStatusEnum.LOST.getValue() && notIssuedMap.get(card_infoDTO.iD) != null) {%>
    <div style="height: 10px"></div>
    <button type="button" class="btn btn-sm border-0 btn-success shadow"
            style="color: white; border-radius: 8px"
            onclick="location.href='Lost_card_infoServlet?actionType=getEditPage&ID=<%=notIssuedMap.get(card_infoDTO.iD).iD%>'">
        <%=LM.getText(LC.LOST_CARD_INFO_ADD_REISSUE_CARD, loginDTO)%>
    </button>
    <% } %>
</td>