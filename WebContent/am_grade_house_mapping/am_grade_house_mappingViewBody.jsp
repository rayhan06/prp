

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="am_grade_house_mapping.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>
<%@ page import="java.util.stream.Collectors" %>


<%
String servletName = "Am_grade_house_mappingServlet";
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }

    long id = Long.parseLong(ID);
    Am_grade_house_mappingDTO am_grade_house_mappingDTO = Am_grade_house_mappingRepository.getInstance().
            getAm_grade_house_mappingDTOByID(id);
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;


%>



<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.AM_GRADE_HOUSE_MAPPING_ADD_AM_GRADE_HOUSE_MAPPING_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-md-8 offset-md-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.AM_GRADE_HOUSE_MAPPING_ADD_AM_GRADE_HOUSE_MAPPING_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_GRADE_HOUSE_MAPPING_ADD_AMHOUSECLASSCAT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">

											<%
                                                String value = CatRepository.getInstance().getText(Language, "am_house_class", am_grade_house_mappingDTO.amHouseClassCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_GRADE_HOUSE_MAPPING_ADD_JOBGRADECAT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
                                                StringBuilder text = new StringBuilder();
                                                List<Long> ids = new ArrayList<>();
                                                CategoryLanguageModel model;

                                                if (am_grade_house_mappingDTO.job_grade_ids != null) {
                                                    ids = Arrays.stream(am_grade_house_mappingDTO.job_grade_ids.split(","))
                                                            .map(String::trim)
                                                            .map(Long::parseLong)
                                                            .collect(Collectors.toList());
                                                }

                                                for(int i = 0; i < ids.size(); i++){
                                                    if(i != 0){
                                                       text.append(", ") ;
                                                    }

                                                    model = CatRepository.getInstance().getCategoryLanguageModel
                                                            ("job_grade_type", ids.get(i).intValue());
                                                    if(model != null){
                                                        text.append(isLanguageEnglish?model.englishText: model.banglaText);
                                                    }
                                                }
                                                %>
				
											<%=Utils.getDigits(text.toString(), Language)%>
				
			
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=UtilCharacter.getDataByLanguage(Language, "নূন্যতম মূল বেতন", "Minimum Basic Salary")%>
                                    </label>
                                    <div class="col-md-8 form-control">

                                        <%
                                            value = String.format("%.1f", am_grade_house_mappingDTO.minimum_basic_salary);
                                        %>

                                        <%=Utils.getDigits(value, Language)%>

                                    </div>
                                </div>
			
			
			
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>