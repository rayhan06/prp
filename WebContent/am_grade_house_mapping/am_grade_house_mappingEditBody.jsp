<%@page import="am_grade_house_mapping.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>
<%@ page import="login.LoginDTO" %>

<%

    String servletName = "Am_grade_house_mappingServlet";
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    String actionName;
    Am_grade_house_mappingDTO am_grade_house_mappingDTO;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        am_grade_house_mappingDTO = Am_grade_house_mappingRepository.getInstance().
                getAm_grade_house_mappingDTOByID(Long.parseLong(ID));
    } else {
        actionName = "add";
        am_grade_house_mappingDTO = new Am_grade_house_mappingDTO();
    }

    String formTitle = LM.getText(LC.AM_GRADE_HOUSE_MAPPING_ADD_AM_GRADE_HOUSE_MAPPING_ADD_FORMNAME, loginDTO);
    int i = 0;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              id="bigform" name="bigform" >
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=am_grade_house_mappingDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right">
                                                                <%=LM.getText(LC.AM_GRADE_HOUSE_MAPPING_ADD_AMHOUSECLASSCAT, loginDTO)%>
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-8">
																<select class='form-control'  name='amHouseClassCat' id = 'amHouseClassCat_category_<%=i%>'   tag='pb_html'>		
																<%
																	String Options = CatRepository.getInstance().buildOptions("am_house_class", Language, am_grade_house_mappingDTO.amHouseClassCat);
																%>
																<%=Options%>
																</select>
	
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_GRADE_HOUSE_MAPPING_ADD_JOBGRADECAT, loginDTO)%></label>
                                                            <div class="col-md-8">
																<select multiple="multiple" class='form-control'  name='jobGradeIds' id = 'jobGradeIds'   tag='pb_html'>
																<%
                                                                    Options = Am_grade_house_mappingDAO.getInstance().getBuildOptionsOfGrades(Language, am_grade_house_mappingDTO.job_grade_ids);
//																	Options = CatRepository.getInstance().buildOptions("job_grade", Language, am_grade_house_mappingDTO.jobGradeCat);
																%>
																<%=Options%>
																</select>
	
															</div>
                                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "নূন্যতম মূল বেতন", "Minimum Basic Salary")%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                String value = "";
                                                if (am_grade_house_mappingDTO.minimum_basic_salary > 0.0) {
                                                    value = am_grade_house_mappingDTO.minimum_basic_salary + "";
                                                }
                                            %>
                                            <input type='number' class='form-control'
                                                   name='minimumBasicSalary'
                                                   id='minimumBasicSalary' value='<%=value%>'
                                                   tag='pb_html'>

                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=am_grade_house_mappingDTO.insertedByUserId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value='<%=am_grade_house_mappingDTO.insertedByOrganogramId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=am_grade_house_mappingDTO.modifiedBy%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=am_grade_house_mappingDTO.insertionDate%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=am_grade_house_mappingDTO.searchColumn%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=am_grade_house_mappingDTO.isDeleted%>' tag='pb_html'/>
											
														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=am_grade_house_mappingDTO.lastModificationTime%>' tag='pb_html'/>
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.AM_GRADE_HOUSE_MAPPING_ADD_AM_GRADE_HOUSE_MAPPING_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="submit" onclick="submitForm()">
                                <%=LM.getText(LC.AM_GRADE_HOUSE_MAPPING_ADD_AM_GRADE_HOUSE_MAPPING_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

const houseForm = $("#bigform");

function buttonStateChange(value){
    $('#submit-btn').prop('disabled',value);
    $('#cancel-btn').prop('disabled',value);
}

function PreprocessBeforeSubmiting()
{
    processMultipleSelectBoxBeforeSubmit("jobGradeIds");
    houseForm.validate();
    return houseForm.valid();
}

function submitForm(){
    buttonStateChange(true);
    if(PreprocessBeforeSubmiting()){
        $.ajax({
            type : "POST",
            url : "Am_grade_house_mappingServlet?actionType=ajax_<%=actionName%>",
            data : houseForm.serialize(),
            dataType : 'JSON',
            success : function(response) {
                if(response.responseCode === 0){
                    $('#toast_message').css('background-color','#ff6063');
                    showToastSticky(response.msg,response.msg);
                    buttonStateChange(false);
                }else if(response.responseCode === 200){
                    window.location.replace(getContextPath()+response.msg);
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChange(false);
            }
        });
    }else{
        buttonStateChange(false);
    }
}



$(document).ready(function(){

	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	});

    $.validator.addMethod('validSelector', function (value, element) {
        return value && value !== -1 && value.toString().trim().length > 0;
    });

    let lang = '<%=Language%>';
    let classErr, salErr;
    if (lang.toUpperCase() === 'ENGLISH') {
        salErr = 'Minimum Basic Salary Required';
        classErr = 'House Class Required';
    } else {
        salErr = 'নূন্যতম মূল বেতন প্রয়োজনীয়';
        classErr = 'বাসার শ্রেণী প্রয়োজনীয়';
    }

    houseForm.validate({
        errorClass: 'error is-invalid',
        validClass: 'is-valid',
        rules: {
            minimumBasicSalary: {
                required: true,
            },
            amHouseClassCat: {
                validSelector: true,
            },
        },
        messages: {

            minimumBasicSalary: salErr,
            amHouseClassCat: classErr,
        }
    });

    select2MultiSelector("#jobGradeIds", '<%=Language%>');
});	





</script>






