<!-- Modal -->
<%@page import="workflow.WorkflowController" %>
<div id="approvalModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header text-left">
                <h4 class="modal-title">
                    <%=LM.getText(LC.HM_APPROVE, loginDTO)%>
                </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body text-left">
                <%
                    ApprovalPathDetailsDTO nextPath = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder + 1);
                    String employeeName;
                    if (nextPath != null) {
                        employeeName = WorkflowController.getNameFromOrganogramId(nextPath.organogramId, "English");
                %>
                Send to <%=employeeName%> for review?
                <%
                } else {
                %>
                Finally Approve?
                <%
                    }
                %>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info shadow btn-border-radius"
                        onclick='approve("<%=id%>", "<%=Message%>", <%=i%>, "<%=servletName%>", 1, true, true)'>Confirm
                </button>
            </div>
        </div>

    </div>
</div>