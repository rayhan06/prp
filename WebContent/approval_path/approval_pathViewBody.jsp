<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="approval_path.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>


<%
    String servletName = "Approval_pathServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.APPROVAL_PATH_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Approval_pathDAO approval_pathDAO = new Approval_pathDAO("approval_path");
    Approval_pathDTO approval_pathDTO = approval_pathDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <h5 class="table-title">
                <%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_ADD_FORMNAME, loginDTO)%>
            </h5>
            <div class="table-responsive">
                <table class="table table-bordered table-striped">


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.APPROVAL_PATH_ADD_NAMEEN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = approval_pathDTO.nameEn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.APPROVAL_PATH_ADD_NAMEBN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = approval_pathDTO.nameBn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.APPROVAL_PATH_ADD_OFFICEUNITID, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = WorkflowController.getOfficeUnitName(approval_pathDTO.officeUnitId, Language);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.APPROVAL_PATH_ADD_MODULECAT, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = approval_pathDTO.moduleCat + "";
                            %>
                            <%
                                value = CatRepository.getInstance().getText(Language, "module", approval_pathDTO.moduleCat);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.APPROVAL_PATH_ADD_ISVISIBLE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = approval_pathDTO.isVisible + "";
                            %>

                            <%=Utils.getYesNo(value, Language)%>


                        </td>

                    </tr>


                </table>
            </div>
            <h5 class="table-title mt-5">
                <%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_DETAILS, loginDTO)%>
            </h5>
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th><%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_DETAILS_APPROVALROLECAT, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_DETAILS_APPROVALORDER, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_DETAILS_ORGANOGRAMID, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_DETAILS_DAYSREQUIRED, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_DETAILS_PERMANENTORGANOGRAMID, loginDTO)%>
                        </th>
                    </tr>
                    <%
                        ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
                        List<ApprovalPathDetailsDTO> approvalPathDetailsDTOs = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathID(approval_pathDTO.iD);

                        for (ApprovalPathDetailsDTO approvalPathDetailsDTO : approvalPathDetailsDTOs) {
                    %>
                    <tr>
                        <td>
                            <%
                                value = approvalPathDetailsDTO.approvalRoleCat + "";
                            %>
                            <%
                                value = CatRepository.getInstance().getText(Language, "approval_role", approvalPathDetailsDTO.approvalRoleCat);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>
                        <td>
                            <%
                                value = approvalPathDetailsDTO.approvalOrder + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>
                        <td>
                            <%
                                value = approvalPathDetailsDTO.organogramId + "";
                            %>
                            <%
                                value = WorkflowController.getNameFromOrganogramId(approvalPathDetailsDTO.organogramId, Language) + ", " + WorkflowController.getOrganogramName(approvalPathDetailsDTO.organogramId, Language) + ", " + WorkflowController.getUnitNameFromOrganogramId(approvalPathDetailsDTO.organogramId, Language);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>
                        <td>
                            <%
                                value = approvalPathDetailsDTO.daysRequired + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>
                        <td>
                            <%
                                value = approvalPathDetailsDTO.permanentOrganogramId + "";
                            %>
                            <%
                                value = WorkflowController.getNameFromOrganogramId(approvalPathDetailsDTO.permanentOrganogramId, Language) + ", " + WorkflowController.getOrganogramName(approvalPathDetailsDTO.permanentOrganogramId, Language) + ", " + WorkflowController.getUnitNameFromOrganogramId(approvalPathDetailsDTO.permanentOrganogramId, Language);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>
                    </tr>
                    <%

                        }

                    %>
                </table>
            </div>
            <h5 class="table-title mt-5">
                <%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_NOTIFICATION_RECIPIENTS, loginDTO)%>
            </h5>
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th><%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_NOTIFICATION_RECIPIENTS_ORGANOGRAMID, loginDTO)%>
                        </th>
                    </tr>
                    <%
                        ApprovalNotificationRecipientsDAO approvalNotificationRecipientsDAO = new ApprovalNotificationRecipientsDAO();
                        List<ApprovalNotificationRecipientsDTO> approvalNotificationRecipientsDTOs = approvalNotificationRecipientsDAO.getApprovalNotificationRecipientsDTOListByApprovalPathID(approval_pathDTO.iD);

                        for (ApprovalNotificationRecipientsDTO approvalNotificationRecipientsDTO : approvalNotificationRecipientsDTOs) {
                    %>
                    <tr>
                        <td>
                            <%
                                value = approvalNotificationRecipientsDTO.organogramId + "";
                            %>
                            <%
                                value = WorkflowController.getNameFromOrganogramId(approvalNotificationRecipientsDTO.organogramId, Language) + ", " + WorkflowController.getOrganogramName(approvalNotificationRecipientsDTO.organogramId, Language) + ", " + WorkflowController.getUnitNameFromOrganogramId(approvalNotificationRecipientsDTO.organogramId, Language);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>
                    </tr>
                    <%

                        }

                    %>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- <div class="modal-content viewmodal"> -->
<%--<div class="menubottom">--%>
<%--    <div class="modal-header">--%>
<%--        <div class="col-md-12">--%>
<%--            <div class="row">--%>
<%--                <div class="col-md-9 col-sm-12">--%>
<%--                    <h5 class="modal-title">--%>
<%--                        <%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_ADD_FORMNAME, loginDTO)%>--%>
<%--                    </h5>--%>
<%--                </div>--%>
<%--                <div class="col-md-3 col-sm-12">--%>
<%--                    <div class="row">--%>
<%--                        <div class="col-md-6">--%>
<%--                            <a href="javascript:" style="display: none" class="btn btn-success app_register"--%>
<%--                               data-id="419637"> Register </a>--%>
<%--                        </div>--%>
<%--                        <div class="col-md-6">--%>
<%--                            <a href="javascript:" style="display: none" class="btn btn-danger app_reject"--%>
<%--                               data-id="419637"> Reject </a>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>


<%--    </div>--%>

<%--    <div class="modal-body container">--%>

<%--        <div class="row div_border office-div">--%>

<%--            <div class="col-md-12">--%>
<%--                <h3>--%>
<%--                    <%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_ADD_FORMNAME, loginDTO)%>--%>
<%--                </h3>--%>
<%--                <table class="table table-bordered table-striped">--%>


<%--                    <tr>--%>
<%--                        <td style="width:30%"><b><%=LM.getText(LC.APPROVAL_PATH_ADD_NAMEEN, loginDTO)%>--%>
<%--                        </b></td>--%>
<%--                        <td>--%>

<%--                            <%--%>
<%--                                value = approval_pathDTO.nameEn + "";--%>
<%--                            %>--%>

<%--                            <%=Utils.getDigits(value, Language)%>--%>


<%--                        </td>--%>

<%--                    </tr>--%>


<%--                    <tr>--%>
<%--                        <td style="width:30%"><b><%=LM.getText(LC.APPROVAL_PATH_ADD_NAMEBN, loginDTO)%>--%>
<%--                        </b></td>--%>
<%--                        <td>--%>

<%--                            <%--%>
<%--                                value = approval_pathDTO.nameBn + "";--%>
<%--                            %>--%>

<%--                            <%=Utils.getDigits(value, Language)%>--%>


<%--                        </td>--%>

<%--                    </tr>--%>


<%--                    <tr>--%>
<%--                        <td style="width:30%"><b><%=LM.getText(LC.APPROVAL_PATH_ADD_OFFICEUNITID, loginDTO)%>--%>
<%--                        </b></td>--%>
<%--                        <td>--%>

<%--                            <%--%>
<%--                                value = WorkflowController.getOfficeUnitName(approval_pathDTO.officeUnitId, Language);--%>
<%--                            %>--%>

<%--                            <%=Utils.getDigits(value, Language)%>--%>


<%--                        </td>--%>

<%--                    </tr>--%>


<%--                    <tr>--%>
<%--                        <td style="width:30%"><b><%=LM.getText(LC.APPROVAL_PATH_ADD_MODULECAT, loginDTO)%>--%>
<%--                        </b></td>--%>
<%--                        <td>--%>

<%--                            <%--%>
<%--                                value = approval_pathDTO.moduleCat + "";--%>
<%--                            %>--%>
<%--                            <%--%>
<%--                                value = CatRepository.getInstance().getText(Language, "module", approval_pathDTO.moduleCat);--%>
<%--                            %>--%>

<%--                            <%=Utils.getDigits(value, Language)%>--%>


<%--                        </td>--%>

<%--                    </tr>--%>


<%--                    <tr>--%>
<%--                        <td style="width:30%"><b><%=LM.getText(LC.APPROVAL_PATH_ADD_ISVISIBLE, loginDTO)%>--%>
<%--                        </b></td>--%>
<%--                        <td>--%>

<%--                            <%--%>
<%--                                value = approval_pathDTO.isVisible + "";--%>
<%--                            %>--%>

<%--                            <%=Utils.getYesNo(value, Language)%>--%>


<%--                        </td>--%>

<%--                    </tr>--%>


<%--                </table>--%>
<%--            </div>--%>


<%--        </div>--%>

<%--        <div class="row div_border attachement-div">--%>
<%--            <div class="col-md-12">--%>
<%--                <h5>--%>
<%--                    <%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_DETAILS, loginDTO)%>--%>
<%--                </h5>--%>
<%--                <table class="table table-bordered table-striped">--%>
<%--                    <tr>--%>
<%--                        <th><%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_DETAILS_APPROVALROLECAT, loginDTO)%>--%>
<%--                        </th>--%>
<%--                        <th><%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_DETAILS_APPROVALORDER, loginDTO)%>--%>
<%--                        </th>--%>
<%--                        <th><%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_DETAILS_ORGANOGRAMID, loginDTO)%>--%>
<%--                        </th>--%>
<%--                        <th><%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_DETAILS_DAYSREQUIRED, loginDTO)%>--%>
<%--                        </th>--%>
<%--                        <th><%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_DETAILS_PERMANENTORGANOGRAMID, loginDTO)%>--%>
<%--                        </th>--%>
<%--                    </tr>--%>
<%--                    <%--%>
<%--                        ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();--%>
<%--                        List<ApprovalPathDetailsDTO> approvalPathDetailsDTOs = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathID(approval_pathDTO.iD);--%>

<%--                        for (ApprovalPathDetailsDTO approvalPathDetailsDTO : approvalPathDetailsDTOs) {--%>
<%--                    %>--%>
<%--                    <tr>--%>
<%--                        <td>--%>
<%--                            <%--%>
<%--                                value = approvalPathDetailsDTO.approvalRoleCat + "";--%>
<%--                            %>--%>
<%--                            <%--%>
<%--                                value = CatRepository.getInstance().getText(Language, "approval_role", approvalPathDetailsDTO.approvalRoleCat);--%>
<%--                            %>--%>

<%--                            <%=Utils.getDigits(value, Language)%>--%>


<%--                        </td>--%>
<%--                        <td>--%>
<%--                            <%--%>
<%--                                value = approvalPathDetailsDTO.approvalOrder + "";--%>
<%--                            %>--%>

<%--                            <%=Utils.getDigits(value, Language)%>--%>


<%--                        </td>--%>
<%--                        <td>--%>
<%--                            <%--%>
<%--                                value = approvalPathDetailsDTO.organogramId + "";--%>
<%--                            %>--%>
<%--                            <%--%>
<%--                                value = WorkflowController.getNameFromOrganogramId(approvalPathDetailsDTO.organogramId, Language) + ", " + WorkflowController.getOrganogramName(approvalPathDetailsDTO.organogramId, Language) + ", " + WorkflowController.getUnitNameFromOrganogramId(approvalPathDetailsDTO.organogramId, Language);--%>
<%--                            %>--%>

<%--                            <%=Utils.getDigits(value, Language)%>--%>


<%--                        </td>--%>
<%--                        <td>--%>
<%--                            <%--%>
<%--                                value = approvalPathDetailsDTO.daysRequired + "";--%>
<%--                            %>--%>

<%--                            <%=Utils.getDigits(value, Language)%>--%>


<%--                        </td>--%>
<%--                        <td>--%>
<%--                            <%--%>
<%--                                value = approvalPathDetailsDTO.permanentOrganogramId + "";--%>
<%--                            %>--%>
<%--                            <%--%>
<%--                                value = WorkflowController.getNameFromOrganogramId(approvalPathDetailsDTO.permanentOrganogramId, Language) + ", " + WorkflowController.getOrganogramName(approvalPathDetailsDTO.permanentOrganogramId, Language) + ", " + WorkflowController.getUnitNameFromOrganogramId(approvalPathDetailsDTO.permanentOrganogramId, Language);--%>
<%--                            %>--%>

<%--                            <%=Utils.getDigits(value, Language)%>--%>


<%--                        </td>--%>
<%--                    </tr>--%>
<%--                    <%--%>

<%--                        }--%>

<%--                    %>--%>
<%--                </table>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--        <div class="row div_border attachement-div">--%>
<%--            <div class="col-md-12">--%>
<%--                <h5><%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_NOTIFICATION_RECIPIENTS, loginDTO)%>--%>
<%--                </h5>--%>
<%--                <table class="table table-bordered table-striped">--%>
<%--                    <tr>--%>
<%--                        <th><%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_NOTIFICATION_RECIPIENTS_ORGANOGRAMID, loginDTO)%>--%>
<%--                        </th>--%>
<%--                    </tr>--%>
<%--                    <%--%>
<%--                        ApprovalNotificationRecipientsDAO approvalNotificationRecipientsDAO = new ApprovalNotificationRecipientsDAO();--%>
<%--                        List<ApprovalNotificationRecipientsDTO> approvalNotificationRecipientsDTOs = approvalNotificationRecipientsDAO.getApprovalNotificationRecipientsDTOListByApprovalPathID(approval_pathDTO.iD);--%>

<%--                        for (ApprovalNotificationRecipientsDTO approvalNotificationRecipientsDTO : approvalNotificationRecipientsDTOs) {--%>
<%--                    %>--%>
<%--                    <tr>--%>
<%--                        <td>--%>
<%--                            <%--%>
<%--                                value = approvalNotificationRecipientsDTO.organogramId + "";--%>
<%--                            %>--%>
<%--                            <%--%>
<%--                                value = WorkflowController.getNameFromOrganogramId(approvalNotificationRecipientsDTO.organogramId, Language) + ", " + WorkflowController.getOrganogramName(approvalNotificationRecipientsDTO.organogramId, Language) + ", " + WorkflowController.getUnitNameFromOrganogramId(approvalNotificationRecipientsDTO.organogramId, Language);--%>
<%--                            %>--%>

<%--                            <%=Utils.getDigits(value, Language)%>--%>


<%--                        </td>--%>
<%--                    </tr>--%>
<%--                    <%--%>

<%--                        }--%>

<%--                    %>--%>
<%--                </table>--%>
<%--            </div>--%>
<%--        </div>--%>


<%--    </div>--%>
<%--</div>--%>