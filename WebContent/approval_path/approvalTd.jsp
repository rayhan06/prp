<td>
											<%
												
												Approval_execution_tableDTO approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId(tableName, commonDTO.iD);
												
												if(commonDTO.isDeleted == 0 && approval_execution_tableDTO != null)
												{
											%>
												<button
												class="btn btn-primary shadow border-0 btn-border-radius"
												style="background-color: #0098bf; border-radius: 6px"
												onclick="location.href='Approval_execution_tableServlet?actionType=search&tableName=<%=tableName%>&previousRowId=<%=approval_execution_tableDTO.previousRowId%>'"
												>
													<i class="fa fa-history"></i>
													<%=LM.getText(LC.HM_HISTORY, loginDTO)%>
												</button>
											<%
												}
												else
												{
											%>
												<%=LM.getText(LC.HM_NO_HISTORY_IS_AVAILABLE, loginDTO)%>
											<%
												}
											%>
											</td>
											
											<td>
											<%
											if(commonDTO.jobCat == SessionConstants.DEFAULT_JOB_CAT)
											{
											%>
												<button type="button" class="btn submit-btn text-white shadow btn-border-radius" data-toggle="modal" data-target="#sendToApprovalPathModal" >
													<%=LM.getText(LC.HM_SEND_TO_APPROVAL_PATH, loginDTO)%>
												</button>
												<%@include file="../inbox_internal/sendToApprovalPathModal.jsp"%>
											<%
											}
											else
											{
											%>
											<%=LM.getText(LC.HM_NO_ACTION_IS_REQUIRED, loginDTO)%>
											<%
											}
											%>
											</td>
											
											<td class="text-right">
											<%
											if(commonDTO.jobCat == SessionConstants.DEFAULT_JOB_CAT)
											{
											%>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=commonDTO.iD%>'/></span>
												</div>
												<%
											}
												%>
											</td>