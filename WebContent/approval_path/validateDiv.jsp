<div class="row div_border attachement-div">
    <div class="col-md-12">
        <h5 class="table-title mb-2">Attachments</h5>
        <%
            ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
        %>
        <div class="dropzone"
             action="Approval_execution_tableServlet?pageType=add&actionType=UploadFilesFromDropZone&columnName=approval_attached_fileDropzone&ColumnID=<%=ColumnID%>">
            <input type='file' style="display: none"
                   name='approval_attached_fileDropzoneFile'
                   id='approval_attached_fileDropzone_dropzone_File_<%=i%>'
                   tag='pb_html'/>
        </div>
        <input type='hidden'
               name='approval_attached_fileDropzoneFilesToDelete'
               id='approval_attached_fileDropzoneFilesToDelete_<%=i%>' value=''
               tag='pb_html'/> <input type='hidden'
                                      name='approval_attached_fileDropzone'
                                      id='approval_attached_fileDropzone_dropzone_<%=i%>' tag='pb_html'
                                      value='<%=ColumnID%>'/>
    </div>
    <div class="col-md-12 mt-4">
        <h5 class="table-title mb-2">Remarks</h5>
        <textarea class='form-control' name='remarks' id='<%=i%>_remarks' tag='pb_html'></textarea>
    </div>
</div>