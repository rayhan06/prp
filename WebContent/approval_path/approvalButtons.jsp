<div class="form-actions text-right">
                        <button type="button" class="btn btn-info shadow btn-border-radius" id='printer'
                                onclick="printAnyDiv('modalbody')">Print
                        </button>
                        <%
                            if (canApprove) {
                        %>
                        <button type="submit" class="btn btn-success shadow btn-border-radius"
                                id='approve_<%=id%>' data-toggle="modal"
                                data-target="#approvalModal"><%=LM.getText(LC.HM_APPROVE, loginDTO)%>
                        </button>
                        <%@include file="../approval_path/approvalModal.jsp" %>
                        <button type="submit" class="btn btn-danger shadow btn-border-radius"
                                id='reject_<%=id%>' data-toggle="modal"
                                data-target="#rejectionModal"><%=LM.getText(LC.HM_REJECT, loginDTO)%>
                        </button>
                        <%@include file="../approval_path/rejectionModal.jsp" %>
                        <%
                            }
                            if (canTerminate) {
                        %>
                        <button type="submit" class="btn btn-success shadow btn-border-radius"
                                id='approve_<%=id%>'
                                onclick='approve("<%=approval_execution_tableDTO.updatedRowId%>", "<%=Message%>", <%=i%>, "<%=servletName%>", 2, true, true)'><%=LM.getText(LC.HM_TERMINATE, loginDTO)%>
                        </button>
                        <%
                            }
                            if (canValidate) {
                        %>
                        <a type="submit" class="btn btn-success shadow btn-border-radius" id='validate_<%=id%>'
                           href='<%=servletName%>?actionType=getEditPage&ID=<%=id%>&isPermanentTable=<%=isPermanentTable%>'><%=LM.getText(LC.HM_VALIDATE, loginDTO)%>
                        </a>
                        <%
                            }
                        %>
                        <button type="submit" class="btn btn-success shadow btn-border-radius"
                                id='history_<%=id%>' data-toggle="modal"
                                data-target="#historyModal"><%=LM.getText(LC.HM_HISTORY, loginDTO)%>
                        </button>
                        <%@include file="../approval_path/historyModal.jsp" %>
                    </div>