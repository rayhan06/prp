<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="approval_path.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="approval_execution_table.*" %>
<%@ page import="approval_path.*" %>
<%@ page import="user.*" %>

<%@page import="workflow.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    Approval_pathDTO approval_pathDTO;
    approval_pathDTO = (Approval_pathDTO) request.getAttribute("approval_pathDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    if (approval_pathDTO == null) {
        approval_pathDTO = new Approval_pathDTO();

    }
    System.out.println("approval_pathDTO = " + approval_pathDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_ADD_FORMNAME, loginDTO);
    String servletName = "Approval_pathServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID = -1;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;
    if (request.getParameter("isPermanentTable") != null) {
        isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
    }

    Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
    ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
    Approval_execution_tableDTO approval_execution_tableDTO = null;
    Approval_execution_tableDTO approval_execution_table_initiationDTO = null;


    String tableName = "approval_path";


    String Language = LM.getText(LC.APPROVAL_PATH_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Approval_pathServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>

                                        <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                               value='<%=approval_pathDTO.iD%>' tag='pb_html'/>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.APPROVAL_PATH_ADD_NAMEEN, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='nameEn'
                                                       id='nameEn_text_<%=i%>' value='<%=approval_pathDTO.nameEn%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.APPROVAL_PATH_ADD_NAMEBN, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='nameBn'
                                                       id='nameBn_text_<%=i%>' value='<%=approval_pathDTO.nameBn%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                        <input type='hidden' class='form-control' name='insertedByUserId'
                                               id='insertedByUserId_hidden_<%=i%>'
                                               value='<%=approval_pathDTO.insertedByUserId%>' tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                               id='insertedByOrganogramId_hidden_<%=i%>'
                                               value='<%=approval_pathDTO.insertedByOrganogramId%>' tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='insertionDate'
                                               id='insertionDate_hidden_<%=i%>'
                                               value='<%=approval_pathDTO.insertionDate%>' tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='lastModifierUser'
                                               id='lastModifierUser_hidden_<%=i%>'
                                               value='<%=approval_pathDTO.lastModifierUser%>' tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='jobCat'
                                               id='jobCat_hidden_<%=i%>' value='<%=approval_pathDTO.jobCat%>'
                                               tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='officeUnitId'
                                               id='officeUnitId_hidden_<%=i%>'
                                               value='<%=approval_pathDTO.officeUnitId%>' tag='pb_html'/>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.APPROVAL_PATH_ADD_MODULECAT, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='moduleCat'
                                                        id='moduleCat_category_<%=i%>' tag='pb_html'>
                                                    <%
                                                        Options = CatRepository.getInstance().buildOptions("module", Language, approval_pathDTO.moduleCat);
                                                    %>
                                                    <%=Options%>
                                                </select>

                                            </div>
                                        </div>
                                        <input type='hidden' class='form-control' name='searchColumn'
                                               id='searchColumn_hidden_<%=i%>'
                                               value='<%=approval_pathDTO.searchColumn%>' tag='pb_html'/>

                                        <input type='hidden' class='form-control' name='isDeleted'
                                               id='isDeleted_hidden_<%=i%>' value='<%=approval_pathDTO.isDeleted%>'
                                               tag='pb_html'/>

                                        <input type='hidden' class='form-control' name='lastModificationTime'
                                               id='lastModificationTime_hidden_<%=i%>'
                                               value='<%=approval_pathDTO.lastModificationTime%>' tag='pb_html'/>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_DETAILS, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped text-nowrap">
                                <thead>
                                <tr>
                                    <th><%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_DETAILS_APPROVALROLECAT, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_DETAILS_APPROVALORDER, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_DETAILS_ORGANOGRAMID, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_DETAILS_DAYSREQUIRED, loginDTO)%>
                                    </th>

                                    <th><%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_DETAILS_REMOVE, loginDTO)%>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="field-ApprovalPathDetails">


                                <%
                                    if (actionName.equals("edit")) {
                                        int index = -1;


                                        for (ApprovalPathDetailsDTO approvalPathDetailsDTO : approval_pathDTO.approvalPathDetailsDTOList) {
                                            index++;

                                            System.out.println("index index = " + index);

                                %>

                                <tr id="ApprovalPathDetails_<%=index + 1%>">
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control' name='approvalPathDetails.iD'
                                               id='iD_hidden_<%=childTableStartingID%>'
                                               value='<%=approvalPathDetailsDTO.iD%>' tag='pb_html'/>

                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='approvalPathDetails.approvalPathId'
                                               id='approvalPathId_hidden_<%=childTableStartingID%>'
                                               value='<%=approvalPathDetailsDTO.approvalPathId%>' tag='pb_html'/>
                                    </td>
                                    <td>


                                        <select class='form-control' name='approvalPathDetails.approvalRoleCat'
                                                id='approvalRoleCat_category_<%=childTableStartingID%>' tag='pb_html'>
                                            <%
                                                Options = CatRepository.getInstance().buildOptions("approval_role", Language, approvalPathDetailsDTO.approvalRoleCat);
                                            %>
                                            <%=Options%>
                                        </select>

                                    </td>
                                    <td>


                                        <%
                                            value = "";
                                            if (approvalPathDetailsDTO.approvalOrder != -1) {
                                                value = approvalPathDetailsDTO.approvalOrder + "";
                                            }
                                        %>
                                        <input type='number' class='form-control' readonly
                                               name='approvalPathDetails.approvalOrder'
                                               id='approvalOrder_number_<%=childTableStartingID%>' value='<%=value%>'
                                               tag='pb_html'>
                                    </td>
                                    <td>


                                        <table class="table table-bordered table-striped">
                                            <tbody id="organogramId_table_<%=childTableStartingID%>" tag='pb_html'>
                                            <%
                                                if (approvalPathDetailsDTO.organogramId != -1) {
                                            %>
                                            <tr>
                                                <td style="width:20%"><%=WorkflowController.getUserNameFromOrganogramId(approvalPathDetailsDTO.organogramId)%>
                                                </td>
                                                <td style="width:40%"><%=WorkflowController.getNameFromOrganogramId(approvalPathDetailsDTO.organogramId, Language)%>
                                                </td>
                                                <td><%=WorkflowController.getOrganogramName(approvalPathDetailsDTO.organogramId, Language)%>
                                                    , <%=WorkflowController.getUnitNameFromOrganogramId(approvalPathDetailsDTO.organogramId, Language)%>
                                                </td>
                                            </tr>
                                            <%
                                                }
                                            %>
                                            </tbody>
                                        </table>
                                        <input type='hidden' class='form-control'
                                               name='approvalPathDetails.organogramId'
                                               id='organogramId_hidden_<%=childTableStartingID%>'
                                               value='<%=approvalPathDetailsDTO.organogramId%>' tag='pb_html'/>

                                    </td>
                                    <td>


                                        <%
                                            value = "";
                                            if (approvalPathDetailsDTO.daysRequired != -1) {
                                                value = approvalPathDetailsDTO.daysRequired + "";
                                            }
                                        %>
                                        <input type='number' class='form-control'
                                               name='approvalPathDetails.daysRequired'
                                               id='daysRequired_number_<%=childTableStartingID%>' value='<%=value%>'
                                               tag='pb_html'>
                                    </td>

                                    <td style="display: none;">


                                        <input type='hidden' class='form-control' name='approvalPathDetails.isDeleted'
                                               id='isDeleted_hidden_<%=childTableStartingID%>'
                                               value='<%=approvalPathDetailsDTO.isDeleted%>' tag='pb_html'/>

                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='approvalPathDetails.lastModificationTime'
                                               id='lastModificationTime_hidden_<%=childTableStartingID%>'
                                               value='<%=approvalPathDetailsDTO.lastModificationTime%>' tag='pb_html'/>
                                    </td>
                                    <td>
										<span id='chkEdit'>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
                                                   class="form-control-sm"/>
										</span>
                                    </td>
                                </tr>
                                <%
                                            childTableStartingID++;
                                        }
                                    }
                                %>

                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <div class=" text-right">
                                <button
                                        id="add-more-ApprovalPathDetails"
                                        name="add-moreApprovalPathDetails"
                                        type="button"
                                        class="btn btn-sm text-white add-btn shadow">
                                    <i class="fa fa-plus"></i>
                                    <%=LM.getText(LC.HM_ADD, loginDTO)%>
                                </button>
                                <button
                                        id="remove-ApprovalPathDetails"
                                        name="removeApprovalPathDetails"
                                        type="button"
                                        class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        </div>

                        <%ApprovalPathDetailsDTO approvalPathDetailsDTO = new ApprovalPathDetailsDTO();%>

                        <template id="template-ApprovalPathDetails">
                            <tr>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='approvalPathDetails.iD'
                                           id='iD_hidden_' value='<%=approvalPathDetailsDTO.iD%>' tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='approvalPathDetails.approvalPathId'
                                           id='approvalPathId_hidden_'
                                           value='<%=approvalPathDetailsDTO.approvalPathId%>' tag='pb_html'/>
                                </td>
                                <td>


                                    <select class='form-control' name='approvalPathDetails.approvalRoleCat'
                                            id='approvalRoleCat_category_' tag='pb_html'>
                                        <%
                                            Options = CatRepository.getInstance().buildOptions("approval_role", Language, approvalPathDetailsDTO.approvalRoleCat);
                                        %>
                                        <%=Options%>
                                    </select>

                                </td>
                                <td>


                                    <%
                                        value = "";
                                        if (approvalPathDetailsDTO.approvalOrder != -1) {
                                            value = approvalPathDetailsDTO.approvalOrder + "";
                                        }
                                    %>
                                    <input type='number' class='form-control' readonly
                                           name='approvalPathDetails.approvalOrder' id='approvalOrder_number_'
                                           value='<%=value%>' tag='pb_html'>
                                </td>
                                <td>


                                    <table class="table table-bordered table-striped">
                                        <tbody id="organogramId_table_" tag='pb_html'>
                                        <%
                                            if (approvalPathDetailsDTO.organogramId != -1) {
                                        %>
                                        <tr>
                                            <td style="width:20%"><%=WorkflowController.getUserNameFromOrganogramId(approvalPathDetailsDTO.organogramId)%>
                                            </td>
                                            <td style="width:40%"><%=WorkflowController.getNameFromOrganogramId(approvalPathDetailsDTO.organogramId, Language)%>
                                            </td>
                                            <td><%=WorkflowController.getOrganogramName(approvalPathDetailsDTO.organogramId, Language)%>
                                                , <%=WorkflowController.getUnitNameFromOrganogramId(approvalPathDetailsDTO.organogramId, Language)%>
                                            </td>
                                        </tr>
                                        <%
                                            }
                                        %>
                                        </tbody>
                                    </table>
                                    <input type='hidden' class='form-control' name='approvalPathDetails.organogramId'
                                           id='organogramId_hidden_' value='<%=approvalPathDetailsDTO.organogramId%>'
                                           tag='pb_html'/>

                                </td>
                                <td>


                                    <%
                                        value = "";
                                        if (approvalPathDetailsDTO.daysRequired != -1) {
                                            value = approvalPathDetailsDTO.daysRequired + "";
                                        }
                                    %>
                                    <input type='number' class='form-control' name='approvalPathDetails.daysRequired'
                                           id='daysRequired_number_' value='<%=value%>' tag='pb_html'>
                                </td>

                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='approvalPathDetails.isDeleted'
                                           id='isDeleted_hidden_' value='<%=approvalPathDetailsDTO.isDeleted%>'
                                           tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='approvalPathDetails.lastModificationTime'
                                           id='lastModificationTime_hidden_'
                                           value='<%=approvalPathDetailsDTO.lastModificationTime%>' tag='pb_html'/>
                                </td>
                                <td>
											<span id='chkEdit'>
												<input type='checkbox' name='checkbox' value='' deletecb='true'
                                                       class="form-control-sm"/>
											</span>
                                </td>
                            </tr>

                        </template>
                    </div>
                </div>
                <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_NOTIFICATION_RECIPIENTS, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped text-nowrap">
                                <thead>
                                <tr>
                                    <th><%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_NOTIFICATION_RECIPIENTS_ORGANOGRAMID, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_NOTIFICATION_RECIPIENTS_REMOVE, loginDTO)%>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="field-ApprovalNotificationRecipients">


                                <%
                                    if (actionName.equals("edit")) {
                                        int index = -1;


                                        for (ApprovalNotificationRecipientsDTO approvalNotificationRecipientsDTO : approval_pathDTO.approvalNotificationRecipientsDTOList) {
                                            index++;

                                            System.out.println("index index = " + index);

                                %>

                                <tr id="ApprovalNotificationRecipients_<%=index + 1%>">
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='approvalNotificationRecipients.iD'
                                               id='iD_hidden_<%=childTableStartingID%>'
                                               value='<%=approvalNotificationRecipientsDTO.iD%>' tag='pb_html'/>

                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='approvalNotificationRecipients.approvalPathId'
                                               id='approvalPathId_hidden_<%=childTableStartingID%>'
                                               value='<%=approvalNotificationRecipientsDTO.approvalPathId%>'
                                               tag='pb_html'/>
                                    </td>
                                    <td>


                                        <table class="table table-bordered table-striped">
                                            <tbody id="notiorganogramId_table_<%=childTableStartingID%>" tag='pb_html'>
                                            <%
                                                if (approvalNotificationRecipientsDTO.organogramId != -1) {
                                            %>
                                            <tr>
                                                <td style="width:20%"><%=WorkflowController.getUserNameFromOrganogramId(approvalNotificationRecipientsDTO.organogramId)%>
                                                </td>
                                                <td style="width:40%"><%=WorkflowController.getNameFromOrganogramId(approvalNotificationRecipientsDTO.organogramId, Language)%>
                                                </td>
                                                <td><%=WorkflowController.getOrganogramName(approvalNotificationRecipientsDTO.organogramId, Language)%>
                                                    , <%=WorkflowController.getUnitNameFromOrganogramId(approvalNotificationRecipientsDTO.organogramId, Language)%>
                                                </td>
                                            </tr>
                                            <%
                                                }
                                            %>
                                            </tbody>
                                        </table>
                                        <input type='hidden' class='form-control'
                                               name='approvalNotificationRecipients.organogramId'
                                               id='notiorganogramId_hidden_<%=childTableStartingID%>'
                                               value='<%=approvalNotificationRecipientsDTO.organogramId%>'
                                               tag='pb_html'/>

                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='approvalNotificationRecipients.isDeleted'
                                               id='isDeleted_hidden_<%=childTableStartingID%>'
                                               value='<%=approvalNotificationRecipientsDTO.isDeleted%>' tag='pb_html'/>

                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='approvalNotificationRecipients.lastModificationTime'
                                               id='lastModificationTime_hidden_<%=childTableStartingID%>'
                                               value='<%=approvalNotificationRecipientsDTO.lastModificationTime%>'
                                               tag='pb_html'/>
                                    </td>
                                    <td>
										<span id='chkEdit'>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
                                                   class="form-control-sm"/>
										</span>
                                    </td>
                                </tr>
                                <%
                                            childTableStartingID++;
                                        }
                                    }
                                %>

                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <div class="text-right">
                                <button
                                        id="add-more-ApprovalNotificationRecipients"
                                        name="add-moreApprovalNotificationRecipients"
                                        type="button"
                                        class="btn btn-sm text-white add-btn shadow">
                                    <i class="fa fa-plus"></i>
                                    <%=LM.getText(LC.HM_ADD, loginDTO)%>
                                </button>
                                <button
                                        id="remove-ApprovalNotificationRecipients"
                                        name="removeApprovalNotificationRecipients"
                                        type="button"
                                        class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        </div>

                        <%ApprovalNotificationRecipientsDTO approvalNotificationRecipientsDTO = new ApprovalNotificationRecipientsDTO();%>

                        <template id="template-ApprovalNotificationRecipients">
                            <tr>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='approvalNotificationRecipients.iD'
                                           id='iD_hidden_' value='<%=approvalNotificationRecipientsDTO.iD%>'
                                           tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='approvalNotificationRecipients.approvalPathId'
                                           id='approvalPathId_hidden_'
                                           value='<%=approvalNotificationRecipientsDTO.approvalPathId%>' tag='pb_html'/>
                                </td>
                                <td>


                                    <table class="table table-bordered table-striped">
                                        <tbody id="notiorganogramId_table_" tag='pb_html'>
                                        <%
                                            if (approvalNotificationRecipientsDTO.organogramId != -1) {
                                        %>
                                        <tr>
                                            <td style="width:20%"><%=WorkflowController.getUserNameFromOrganogramId(approvalNotificationRecipientsDTO.organogramId)%>
                                            </td>
                                            <td style="width:40%"><%=WorkflowController.getNameFromOrganogramId(approvalNotificationRecipientsDTO.organogramId, Language)%>
                                            </td>
                                            <td><%=WorkflowController.getOrganogramName(approvalNotificationRecipientsDTO.organogramId, Language)%>
                                                , <%=WorkflowController.getUnitNameFromOrganogramId(approvalNotificationRecipientsDTO.organogramId, Language)%>
                                            </td>
                                        </tr>
                                        <%
                                            }
                                        %>
                                        </tbody>
                                    </table>
                                    <input type='hidden' class='form-control'
                                           name='approvalNotificationRecipients.organogramId'
                                           id='notiorganogramId_hidden_'
                                           value='<%=approvalNotificationRecipientsDTO.organogramId%>' tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='approvalNotificationRecipients.isDeleted' id='isDeleted_hidden_'
                                           value='<%=approvalNotificationRecipientsDTO.isDeleted%>' tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='approvalNotificationRecipients.lastModificationTime'
                                           id='lastModificationTime_hidden_'
                                           value='<%=approvalNotificationRecipientsDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                </td>
                                <td>
											<span id='chkEdit'>
												<input type='checkbox' name='checkbox' value='' deletecb='true'
                                                       class="form-control-sm"/>
											</span>
                                </td>
                            </tr>

                        </template>
                    </div>
                </div>
                <div class="form-actions text-center my-5">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.APPROVAL_PATH_ADD_APPROVAL_PATH_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    var maxOrder = <%=approval_pathDTO.approvalPathDetailsDTOList.size()%> -1;

    function fixOrders() {
        $("[name = 'approvalPathDetails.approvalOrder']").each(function (index) {
            $(this).val(index);
            $(this).prop('readonly', 'true')
        });
    }

    function PreprocessBeforeSubmiting(row, validate) {


        for (i = 1; i < child_table_extra_id; i++) {
            if (document.getElementById("daysRequired_checkbox_" + i)) {
                if (document.getElementById("daysRequired_checkbox_" + i).getAttribute("processed") == null) {
                    preprocessCheckBoxBeforeSubmitting('daysRequired', i);
                    document.getElementById("daysRequired_checkbox_" + i).setAttribute("processed", "1");
                }
            }
        }
        for (i = 1; i < child_table_extra_id; i++) {
        }
        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Approval_pathServlet");
    }

    function init(row) {


        for (i = 1; i < child_table_extra_id; i++) {
        }
        for (i = 1; i < child_table_extra_id; i++) {
        }

    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-ApprovalPathDetails").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-ApprovalPathDetails");

            $("#field-ApprovalPathDetails").append(t.html());
            SetCheckBoxValues("field-ApprovalPathDetails");

            var tr = $("#field-ApprovalPathDetails").find("tr:last-child");

            tr.attr("id", "ApprovalPathDetails_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                console.log(index + ": " + $(this).attr('id'));
            });

            addEmployeeWithRow("organogramId_button_" + child_table_extra_id);
            fixOrders();

            child_table_extra_id++;

        });


    $("#remove-ApprovalPathDetails").click(function (e) {
        var tablename = 'field-ApprovalPathDetails';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[deletecb="true"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
        fixOrders();
    });
    $("#add-more-ApprovalNotificationRecipients").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-ApprovalNotificationRecipients");

            $("#field-ApprovalNotificationRecipients").append(t.html());
            SetCheckBoxValues("field-ApprovalNotificationRecipients");

            var tr = $("#field-ApprovalNotificationRecipients").find("tr:last-child");

            tr.attr("id", "ApprovalNotificationRecipients_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                console.log(index + ": " + $(this).attr('id'));
            });

            addEmployeeWithRow("notiorganogramId_button_" + child_table_extra_id);

            child_table_extra_id++;

        });


    $("#remove-ApprovalNotificationRecipients").click(function (e) {
        var tablename = 'field-ApprovalNotificationRecipients';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[deletecb="true"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
    });


</script>






