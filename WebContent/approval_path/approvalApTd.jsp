<%@page pageEncoding="UTF-8" %>

<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="files.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@ page import="approval_execution_table.*"%>
<%@ page import="approval_path.*"%>
<%@ page import="workflow.*"%>
<%@page import="dbm.*" %>
<%@page import="holidays.*" %>
<%@page import="approval_summary.*" %>


<%

System.out.println("isPermanentTable = " + isPermanentTable);

Approval_summaryDAO approval_summaryDAO = new Approval_summaryDAO();
ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
Approval_execution_tableDTO approval_execution_tableDTO = null;
ApprovalPathDetailsDTO approvalPathDetailsDTO =  null;
boolean canApprove = false, canValidate = false, isInitiator = false, canTerminate = false;
boolean isInPreviousOffice = false;
String Message = "Done";

approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId(tableName, commonDTO.iD);
Approval_summaryDTO approval_summaryDTO = approval_summaryDAO.getDTOByTableNameAndTableID(tableName, approval_execution_tableDTO.previousRowId);
System.out.println("approval_execution_tableDTO = " + approval_execution_tableDTO);
approvalPathDetailsDTO = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
if(approvalPathDetailsDTO!= null && approvalPathDetailsDTO.organogramId == userDTO.organogramID)
{
	canApprove = true;
	if(approvalPathDetailsDTO.approvalRoleCat == SessionConstants.VALIDATOR)
	{
		canValidate = true;
	}
}

isInitiator = WorkflowController.isInitiator(tableName, approval_execution_tableDTO.previousRowId, userDTO.organogramID);

canTerminate = isInitiator && commonDTO.isDeleted == 2;

Approval_pathDAO approval_pathDAO = new Approval_pathDAO();
Approval_pathDTO approval_pathDTO = approval_pathDAO.getDTOByID(approval_execution_tableDTO.approvalPathId);
	

System.out.println("commonDTO = " + commonDTO);

out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");




FilesDAO filesDAO = new FilesDAO();

SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

boolean isOverDue = false;

Date fromDate = new Date(approval_execution_tableDTO.lastModificationTime);
Date dueDate = null;
if(fromDate != null && approvalPathDetailsDTO!= null)
{
	dueDate = CalenderUtil.getDateAfter(fromDate, approvalPathDetailsDTO.daysRequired);
	long today = System.currentTimeMillis();
	boolean timeOver = today > dueDate.getTime();
	isOverDue  =  (approval_execution_tableDTO.approvalStatusCat == Approval_execution_tableDTO.PENDING) && timeOver;
	
	System.out.println("time dif = " + (today - dueDate.getTime()));
}
String formatted_dueDate;

System.out.println("i = " + i  + " OVERDUE = " + isOverDue);

if(isOverDue)
{
%>
<style>
#tr_<%=i%> {
  color: red;
}
</style>
<%
}
%>

											<td>
												<%
												value = approval_pathDTO.nameEn;
												%>											
															
												<%=value%>
											</td>
											
											<td>
												<%
												value = WorkflowController.getNameFromUserId(approval_summaryDTO.initiator, Language);
												%>											
															
												<%=value%>
											</td>
											
											<td id = '<%=i%>_dateOfInitiation'>
												<%
												value = approval_summaryDTO.dateOfInitiation + "";
												%>
												<%

												String formatted_dateOfInitiation = simpleDateFormat.format(new Date(Long.parseLong(value)));
												%>
												<%=Utils.getDigits(formatted_dateOfInitiation, Language)%>
				
			
											</td>
											
											
											<td>
												<%
												value = WorkflowController.getNameFromOrganogramId(approval_summaryDTO.assignedTo, Language);
												%>											
															
												<%=value%>
											</td>
											
											<td >
												<%
												String formatted_dateOfAssignment = simpleDateFormat.format(new Date(approval_execution_tableDTO.lastModificationTime));
												%>
												<%=Utils.getDigits(formatted_dateOfAssignment, Language)%>
				
			
											</td>
											
											<td>
											
												<%
												
												
												if(dueDate!= null)
												{
													formatted_dueDate = simpleDateFormat.format(dueDate);
												}
												else
												{
													formatted_dueDate = "";
												}
												%>
												<%=Utils.getDigits(formatted_dueDate, Language)%>
				
			
											</td>
		
											
											
											<td>
												<%
												if(isOverDue)
												{
													value = LM.getText(LC.HM_OVERDUE, loginDTO);
												}
												else
												{
													value = CatDAO.getName(Language, "approval_status", approval_summaryDTO.approvalStatusCat);
												}
												
												%>											
															
												<%=value%>
											</td>
	
											<td>
											<%
											if(canApprove || canTerminate)
											{
												%>
												<button
														type="button"
														class="btn-sm border-0 shadow bg-light btn-border-radius"
														style="color: #ff6b6b;"
														onclick="location.href='<%=servletName%>?actionType=view&ID=<%=commonDTO.iD%>&isPermanentTable=<%=isPermanentTable%>'"
												>
													<i class="fa fa-eye"></i>
												</button>
												
												<%
											}
											else
											{
											
											 	%>
											 	<%=LM.getText(LC.HM_NO_ACTION_IS_REQUIRED, loginDTO)%>
											 	<%
											}
											 %>																						
											</td>
											
											
											
											
											<td>
												<a
													class="btn btn-primary shadow border-0 btn-border-radius"
													style="background-color: #0098bf; border-radius: 6px"
													href='Approval_execution_tableServlet?actionType=search&tableName=<%=tableName%>&previousRowId=<%=approval_execution_tableDTO.previousRowId%>'
												>
												<i class="fa fa-history"></i>
												<%=LM.getText(LC.HM_HISTORY, loginDTO)%>
												</a>
											</td>
											

