<%
Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
Approval_execution_tableDTO approval_execution_tableDTO = null;
Approval_execution_tableDTO approval_execution_table_initiationDTO = null;
ApprovalPathDetailsDTO approvalPathDetailsDTO =  null;



boolean canApprove = false, canValidate = false, isInitiator = false, canTerminate = false;

if(!isPermanentTable)
{
	approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId(tableName, commonDTO.iD);
	System.out.println("approval_execution_tableDTO = " + approval_execution_tableDTO);
	approvalPathDetailsDTO = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
	approval_execution_table_initiationDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getInitiationDTOByUpdatedRowId(tableName, commonDTO.iD);
	if(approvalPathDetailsDTO!= null && approvalPathDetailsDTO.organogramId == userDTO.organogramID)
	{
		canApprove = true;
		if(approvalPathDetailsDTO.approvalRoleCat == SessionConstants.VALIDATOR)
		{
			canValidate = true;
		}
	}
	
	isInitiator = WorkflowController.isInitiator(tableName, approval_execution_tableDTO.previousRowId, userDTO.organogramID);
	
	canTerminate = isInitiator && commonDTO.isDeleted == 2;
}
%>