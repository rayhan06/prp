<%
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String Language = LM.getLanguage(loginDTO);

	UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

	String actionName = "edit";
	String failureMessage = (String)request.getAttribute("failureMessage");
	if(failureMessage == null || failureMessage.isEmpty())
	{
		failureMessage = "";
	}
	String value = "";


	boolean isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));


	String Value = "";
	int i = 0;
	FilesDAO filesDAO = new FilesDAO();
	
	

	Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
	ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
	Approval_execution_tableDTO approval_execution_tableDTO = null;
	Approval_execution_tableDTO approval_execution_table_initiationDTO = null;
	ApprovalPathDetailsDTO approvalPathDetailsDTO =  null;

	boolean canApprove = false, canValidate = false, isInitiator = false, canTerminate = false;
	boolean isInPreviousOffice = false;
	String Message = "Done";

	
	approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId(tableName, commonDTO.iD);
	System.out.println("approval_execution_tableDTO = " + approval_execution_tableDTO);
	approvalPathDetailsDTO = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
	approval_execution_table_initiationDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getInitiationDTOByPreviousRowId(tableName, approval_execution_tableDTO.previousRowId);
	if(approvalPathDetailsDTO!= null && approvalPathDetailsDTO.organogramId == userDTO.organogramID 
			&& approval_execution_tableDTO.iD == approval_execution_tableDAO.getMostRecentExecutionIDByPreviousRowID(tableName, approval_execution_tableDTO.previousRowId))
	{
		canApprove = true;
		if(approvalPathDetailsDTO.approvalRoleCat == SessionConstants.VALIDATOR)
		{
			canValidate = true;
		}
	}
	
	isInitiator = WorkflowController.isInitiator(tableName, approval_execution_tableDTO.previousRowId, userDTO.organogramID);
	
	canTerminate = isInitiator && commonDTO.isDeleted == 2;
		
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>