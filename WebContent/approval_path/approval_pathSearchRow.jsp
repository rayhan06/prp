<%@page pageEncoding="UTF-8" %>

<%@page import="approval_execution_table.Approval_execution_tableDAO" %>
<%@ page import="approval_execution_table.Approval_execution_tableDTO" %>
<%@ page import="approval_path.ApprovalPathDetailsDAO" %>
<%@page import="approval_path.Approval_pathDAO" %>

<%@ page import="approval_path.Approval_pathDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="pb.CatRepository" %>
<%@page import="pb.Utils" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="util.CommonDTO" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>
<%@page import="java.text.SimpleDateFormat" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.APPROVAL_PATH_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_APPROVAL_PATH;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Approval_pathDTO approval_pathDTO = (Approval_pathDTO) request.getAttribute("approval_pathDTO");
    CommonDTO commonDTO = approval_pathDTO;
    String servletName = "Approval_pathServlet";

    Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
    ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
    Approval_execution_tableDTO approval_execution_tableDTO = null;
    String Message = "Done";
    approval_execution_tableDTO = (Approval_execution_tableDTO) approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("approval_path", approval_pathDTO.iD);

    System.out.println("approval_pathDTO = " + approval_pathDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Approval_pathDAO approval_pathDAO = (Approval_pathDAO) request.getAttribute("approval_pathDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td id='<%=i%>_nameEn'>
    <%
        value = approval_pathDTO.nameEn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_nameBn'>
    <%
        value = approval_pathDTO.nameBn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_officeUnitId'>
    <%
        value = WorkflowController.getOfficeUnitName(approval_pathDTO.officeUnitId, Language);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_module'>
    <%
        value = approval_pathDTO.module + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_moduleCat'>
    <%
        value = approval_pathDTO.moduleCat + "";
    %>
    <%
        value = CatRepository.getInstance().getText(Language, "module", approval_pathDTO.moduleCat);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_isVisible'>
    <%
        value = approval_pathDTO.isVisible + "";
    %>

    <%=Utils.getYesNo(value, Language)%>


</td>


<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius"
			style="color: #ff6b6b;"
            onclick="location.href='Approval_pathServlet?actionType=view&ID=<%=approval_pathDTO.iD%>'">
		<i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
	<button
			type="button"
			class="btn-sm border-0 shadow btn-border-radius text-white"
			style="background-color: #ff6b6b;"
			onclick="location.href='Approval_pathServlet?actionType=getEditPage&ID=<%=approval_pathDTO.iD%>'"
	>
		<i class="fa fa-edit"></i>
	</button>
</td>

<td class="text-right" id='<%=i%>_checkbox'>
	<div class='checker'>
        <span class='chkEdit'>
            <input type='checkbox' name='ID' value='<%=approval_pathDTO.iD%>'/>
        </span>
	</div>
</td>
																						
											

