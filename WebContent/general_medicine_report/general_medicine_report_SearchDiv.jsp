<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page contentType="text/html;charset=utf-8" %>


<%@ page import="pb.*" %>
<%

    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EMPLOYEE_MEDICAL_REPORT_EDIT_LANGUAGE, loginDTO);
%>

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>
<div class="row mx-2">

    <div id="specialityType" class="search-criteria-div col-md-12">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.HM_REFERENCE_EMPLOYEE, loginDTO)%>
            </label>
            <div class="col-md-9">
                <button type="button" class="btn btn-block submit-btn btn-border-radius text-white"
                        onclick="addEmployee()"
                        id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
                </button>
                <table class="table table-bordered table-striped">
                    <tbody id="employeeToSet"></tbody>
                </table>
                <input class='form-control' type='hidden' name='userName' id='userName' value=''/>
            </div>
        </div>
    </div>
    

    <div class="search-criteria-div col-md-12">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.HM_USER_NAME, loginDTO)%>
            </label>
            <div class="col-md-9">              
                <input class='form-control' type='text' name='userNameRaw' id='userNameRaw' value='' onKeyUp="setEngUserName(this.value, 'userName')"/>
            </div>
        </div>
    </div>
    
    <div class="search-criteria-div col-md-12">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_EMPLOYEE_CLASS, loginDTO)%>
            </label>
            <div class="col-md-9">              
                <select class='form-control' name='employee_class_cat' id='employee_class_cat'>
                   <%=CatDAO.getOptions(Language, "employee_class", CatDTO.CATDEFAULT)%>
               </select>
            </div>
        </div>
    </div>
    
      <div class="search-criteria-div col-md-12">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_OFFICER_TYPE, loginDTO)%>
            </label>
            <div class="col-md-9">              
                <select class='form-control' name='emp_officer_cat' id='emp_officer_cat'>
                   <%=CatDAO.getOptions(Language, "emp_officer", CatDTO.CATDEFAULT)%>
               </select>
            </div>
        </div>
    </div>
    
     <div class="search-criteria-div col-md-12">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY, loginDTO)%>
            </label>
            <div class="col-md-9">              
                <select class='form-control' name='employment_cat' id='employment_cat'>
                   <%=CatDAO.getOptions(Language, "employment", CatDTO.CATDEFAULT)%>
               </select>
            </div>
        </div>
    </div>
    

     <div class="col-md-12">
        <%@include file="../pbreport/yearmonth.jsp" %>
    </div>
    <div class="col-md-12">
        <%@include file="../pbreport/calendar.jsp" %>
    </div>
</div>
<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script type="text/javascript">
    function init() {
        addables = [0, 0, 0, 0, 0, 1, 1];
        
        dateTimeInit($("#Language").val());
        $("#search_by_date").prop('checked', true);
        $("#search_by_date").trigger("change");
        setDateByStringAndId('startDate_js', '<%=datestr%>');
        setDateByStringAndId('endDate_js', '<%=datestr%>');
        add1WithEnd = false;
        processNewCalendarDateAndSubmit();
    }
    
    function setCDate(name) {
        console.log("calendar change called");
        $("#" + name).val(getDateTimestampById(name + '_js'));


        console.log("startdate = " + $("#" + name).val());


        //ajaxSubmit();
    }

    function PreprocessBeforeSubmiting() {
    	
    }

    function patient_inputted(userName, orgId) {
        console.log("patient_inputted " + userName);
        $("#userName").val(userName);
        $("#userNameRaw").val(userName);
    }
  
</script>