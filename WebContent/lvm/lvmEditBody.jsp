
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="lvm.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>


<%
LvmDTO lvmDTO;
lvmDTO = (LvmDTO)request.getAttribute("lvmDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(lvmDTO == null)
{
	lvmDTO = new LvmDTO();
	
}
System.out.println("lvmDTO = " + lvmDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.LVM_EDIT_LVM_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.LVM_ADD_LVM_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

boolean isPermanentTable = true;
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i>Leave Management</h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="LvmServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
				
				
				




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.LVM_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=lvmDTO.iD%>' tag='pb_html'/>
	
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.LVM_EDIT_EMPLOYEEINLEAVETYPE, loginDTO)):(LM.getText(LC.LVM_ADD_EMPLOYEEINLEAVETYPE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'employeeInLeaveType_div_<%=i%>'>	
		<select class='form-control'  name='employeeInLeaveType' id = 'employeeInLeaveType_select2_<%=i%>'   tag='pb_html'>
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "employee_records", "employeeInLeaveType_select2_" + i, "form-control", "employeeInLeaveType", lvmDTO.employeeInLeaveType + "");
}
else
{			
			//Options = CommonDAO.getOptions(Language, "select", "employee_records", "employeeInLeaveType_select2_" + i, "form-control", "employeeInLeaveType" );
			Options = CommonDAO.getOptions(Language, "select", "employee_records", "employeeInLeaveType_select2_" + i, "form-control", "employeeInLeaveType", "", "name_eng", "", "" );
			Options = "<option value='-1'>---select---</option>" + Options;
}
%>
<%=Options%>
		</select>
		
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='employeeInLeaveOrganogramId' id = 'employeeInLeaveOrganogramId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.employeeInLeaveOrganogramId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='employeeInLeaveName' id = 'employeeInLeaveName_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.employeeInLeaveName + "'"):("'" + "" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='employeeInLeaveDesignation' id = 'employeeInLeaveDesignation_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.employeeInLeaveDesignation + "'"):("'" + "" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='employeeInLeaveOfficeUnitId' id = 'employeeInLeaveOfficeUnitId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.employeeInLeaveOfficeUnitId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='employeeInLeaveOfficeUnitName' id = 'employeeInLeaveOfficeUnitName_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.employeeInLeaveOfficeUnitName + "'"):("'" + "" + "'")%> tag='pb_html'/>
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.LVM_EDIT_EMPLOYEEINCHARGETYPE, loginDTO)):(LM.getText(LC.LVM_ADD_EMPLOYEEINCHARGETYPE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'employeeInChargeType_div_<%=i%>'>	
		<select class='form-control'  name='employeeInChargeType' id = 'employeeInChargeType_select2_<%=i%>'   tag='pb_html'>
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "employee_records", "employeeInChargeType_select2_" + i, "form-control", "employeeInChargeType", lvmDTO.employeeInChargeType + "");
}
else
{			
			//Options = CommonDAO.getOptions(Language, "select", "employee_records", "employeeInChargeType_select2_" + i, "form-control", "employeeInChargeType" );
			Options = CommonDAO.getOptions(Language, "select", "employee_records", "employeeInChargeType_select2_" + i, "form-control", "employeeInChargeType", "", "name_eng", "", "" );
			Options = "<option value='-1'>---select---</option>" + Options;
}
%>
<%=Options%>
		</select>
		
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='employeeInChargeOrganogramId' id = 'employeeInChargeOrganogramId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.employeeInChargeOrganogramId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='employeeInChargeName' id = 'employeeInChargeName_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.employeeInChargeName + "'"):("'" + "" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='employeeInChargeDesignation' id = 'employeeInChargeDesignation_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.employeeInChargeDesignation + "'"):("'" + "" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='employeeInChargeOfficeUnitId' id = 'employeeInChargeOfficeUnitId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.employeeInChargeOfficeUnitId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='employeeInChargeOfficeUnitName' id = 'employeeInChargeOfficeUnitName_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.employeeInChargeOfficeUnitName + "'"):("'" + "" + "'")%> tag='pb_html'/>
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.LVM_EDIT_EMPLOYEEINCHARGEPOLICYROLESTYPE, loginDTO)):(LM.getText(LC.LVM_ADD_EMPLOYEEINCHARGEPOLICYROLESTYPE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'employeeInChargePolicyRolesType_div_<%=i%>'>	
		<select class='form-control'  name='employeeInChargePolicyRolesType' id = 'employeeInChargePolicyRolesType_select2_<%=i%>'   tag='pb_html'>
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "employee_policy_roles", "employeeInChargePolicyRolesType_select2_" + i, "form-control", "employeeInChargePolicyRolesType", lvmDTO.employeeInChargePolicyRolesType + "");
}
else
{			
			//Options = CommonDAO.getOptions(Language, "select", "employee_policy_roles", "employeeInChargePolicyRolesType_select2_" + i, "form-control", "employeeInChargePolicyRolesType" );
			Options = CommonDAO.getOptions(Language, "select", "employee_policy_roles", "employeeInChargePolicyRolesType_select2_" + i, "form-control", "employeeInChargePolicyRolesType", "", "name_eng", "", "" );
}
%>
<%=Options%>
		</select>
		
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.LVM_EDIT_LEAVESTARTDATE, loginDTO)):(LM.getText(LC.LVM_ADD_LEAVESTARTDATE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'leaveStartDate_div_<%=i%>'>	
		<input type='date' class='form-control'  name='leaveStartDate_Date_<%=i%>' id = 'leaveStartDate_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_leaveStartDate = new SimpleDateFormat("dd-MM-yyyy");
	String formatted_leaveStartDate = format_leaveStartDate.format(new Date(lvmDTO.leaveStartDate));
	%>
	'<%=formatted_leaveStartDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
		<input type='hidden' class='form-control'  name='leaveStartDate' id = 'leaveStartDate_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.leaveStartDate + "'"):("'" + "0" + "'")%>  tag='pb_html'>
		
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.LVM_EDIT_LEAVEENDDATE, loginDTO)):(LM.getText(LC.LVM_ADD_LEAVEENDDATE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'leaveEndDate_div_<%=i%>'>	
		<input type='date' class='form-control'  name='leaveEndDate_Date_<%=i%>' id = 'leaveEndDate_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_leaveEndDate = new SimpleDateFormat("dd-MM-yyyy");
	String formatted_leaveEndDate = format_leaveEndDate.format(new Date(lvmDTO.leaveEndDate));
	%>
	'<%=formatted_leaveEndDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
		<input type='hidden' class='form-control'  name='leaveEndDate' id = 'leaveEndDate_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.leaveEndDate + "'"):("'" + "0" + "'")%>  tag='pb_html'>
		
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='leaveEntryDate' id = 'leaveEntryDate_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.leaveEntryDate + "'"):("'" + "1970-01-01" + "'")%> tag='pb_html'/>

		<input type='hidden' class='form-control'  name='leaveStatusCat' id = 'leaveStatusCat_category_<%=i%>' value='1' tag='pb_html'/>
																					

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + lvmDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
					
	






				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						%>
						<%=LM.getText(LC.LVM_EDIT_LVM_CANCEL_BUTTON, loginDTO)%>
						<%
					}
					else
					{
						%>
						<%=LM.getText(LC.LVM_ADD_LVM_CANCEL_BUTTON, loginDTO)%>
						<%
					}
					
					%>
					</a>
					<button class="btn btn-success" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						%>
						<%=LM.getText(LC.LVM_EDIT_LVM_SUBMIT_BUTTON, loginDTO)%>
						<%
					}
					else
					{
						%>
						<%=LM.getText(LC.LVM_ADD_LVM_SUBMIT_BUTTON, loginDTO)%>
						<%
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">




function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}

	preprocessDateBeforeSubmitting('leaveStartDate', row);	
	preprocessDateBeforeSubmitting('leaveEndDate', row);	

	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "LvmServlet");	
}

function init(row)
{

			$("#employeeInLeaveType_select2_" + row).select2({
				dropdownAutoWidth: true
			});

			$("#employeeInChargeType_select2_" + row).select2({
				dropdownAutoWidth: true
			});

			$("#employeeInChargePolicyRolesType_select2_" + row).select2({
				dropdownAutoWidth: true
			});


	
}

var row = 0;
	
window.onload =function ()
{
	init(row);
}

var child_table_extra_id = <%=childTableStartingID%>;



</script>






