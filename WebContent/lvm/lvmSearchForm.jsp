
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="lvm.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}

String value = "";
String Language = LM.getText(LC.LVM_EDIT_LANGUAGE, loginDTO);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


LvmDAO lvmDAO = (LvmDAO)request.getAttribute("lvmDAO");


String navigator2 = SessionConstants.NAV_LVM;
System.out.println("navigator2 = " + navigator2);
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
System.out.println("rn2 = " + rn2);
String pageno2 = ( rn2 == null ) ? "1" : "" + rn2.getCurrentPageNo();
String totalpage2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalPages();
String totalRecords2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalRecords();
String lastSearchTime = ( rn2 == null ) ? "0" : "" + rn2.getSearchTime();
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

String successMessageForwarded = "Forwarded to your Senior Office";
String successMessageApproved = "Approval Done";
%>				
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.LVM_EDIT_EMPLOYEEINLEAVETYPE, loginDTO)%></th>
								<th><%=LM.getText(LC.LVM_EDIT_EMPLOYEEINLEAVEDESIGNATION, loginDTO)%></th>
								<th><%=LM.getText(LC.LVM_EDIT_EMPLOYEEINLEAVEOFFICEUNITNAME, loginDTO)%></th>
								<th><%=LM.getText(LC.LVM_EDIT_EMPLOYEEINCHARGETYPE, loginDTO)%></th>								
								<th><%=LM.getText(LC.LVM_EDIT_EMPLOYEEINCHARGEDESIGNATION, loginDTO)%></th>
								<th><%=LM.getText(LC.LVM_EDIT_EMPLOYEEINCHARGEOFFICEUNITNAME, loginDTO)%></th>
								<th><%=LM.getText(LC.LVM_EDIT_EMPLOYEEINCHARGEPOLICYROLESTYPE, loginDTO)%></th>
								<th><%=LM.getText(LC.LVM_EDIT_LEAVESTARTDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.LVM_EDIT_LEAVEENDDATE, loginDTO)%></th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_LVM);

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											LvmDTO lvmDTO = (LvmDTO) data.get(i);
																																
											
											%>
											<tr id = 'tr_<%=i%>'>
											<%
											
								%>
											
		
								<%  								
								    request.setAttribute("lvmDTO",lvmDTO);
								%>  
								
								 <jsp:include page="./lvmSearchRow.jsp">
								 		<jsp:param name="pageName" value="searchrow" />
								 		<jsp:param name="rownum" value="<%=i%>" />
								 </jsp:include>			

								
								<%

											%>
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


<script src="nicEdit.js" type="text/javascript"></script>

<script type="text/javascript">

function getOriginal(i, tempID, parentID, ServletName)
{
	console.log("getOriginal called");
	var idToSubmit;
	var isPermanentTable;
	var state = document.getElementById(i + "_original_status").value;
	if(state == 0)
	{
		idToSubmit = parentID;
		isPermanentTable = true;
	}
	else
	{
		idToSubmit = tempID;
		isPermanentTable = false;
	}
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{			
			var response = JSON.parse(this.responseText);
			document.getElementById(i + "_employeeInLeaveType").innerHTML = response.employeeInLeaveType;
			document.getElementById(i + "_employeeInLeaveOrganogramId").innerHTML = response.employeeInLeaveOrganogramId;
			document.getElementById(i + "_employeeInLeaveName").innerHTML = response.employeeInLeaveName;
			document.getElementById(i + "_employeeInLeaveDesignation").innerHTML = response.employeeInLeaveDesignation;
			document.getElementById(i + "_employeeInLeaveOfficeUnitId").innerHTML = response.employeeInLeaveOfficeUnitId;
			document.getElementById(i + "_employeeInLeaveOfficeUnitName").innerHTML = response.employeeInLeaveOfficeUnitName;
			document.getElementById(i + "_employeeInChargeType").innerHTML = response.employeeInChargeType;
			document.getElementById(i + "_employeeInChargeOrganogramId").innerHTML = response.employeeInChargeOrganogramId;
			document.getElementById(i + "_employeeInChargeName").innerHTML = response.employeeInChargeName;
			document.getElementById(i + "_employeeInChargeDesignation").innerHTML = response.employeeInChargeDesignation;
			document.getElementById(i + "_employeeInChargeOfficeUnitId").innerHTML = response.employeeInChargeOfficeUnitId;
			document.getElementById(i + "_employeeInChargeOfficeUnitName").innerHTML = response.employeeInChargeOfficeUnitName;
			document.getElementById(i + "_employeeInChargePolicyRolesType").innerHTML = response.employeeInChargePolicyRolesType;
			document.getElementById(i + "_leaveStartDate").innerHTML = response.leaveStartDate;
			document.getElementById(i + "_leaveEndDate").innerHTML = response.leaveEndDate;
			document.getElementById(i + "_leaveEntryDate").innerHTML = response.leaveEntryDate;
			document.getElementById(i + "_leaveStatusCat").innerHTML = response.leaveStatusCat;
					
			if(state == 0)
			{
				document.getElementById(i + "_getOriginal").innerHTML = "View Edited";
				state = 1;
			}
			else
			{
				document.getElementById(i + "_getOriginal").innerHTML = "View Original";
				state = 0;
			}
			
			document.getElementById(i + "_original_status").value = state;
		}
		else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	};
	xhttp.open("POST", ServletName + "?actionType=getDTO&ID=" + idToSubmit + "&isPermanentTable=" + isPermanentTable, true);
	xhttp.send();
}





function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}

	preprocessDateBeforeSubmitting('leaveStartDate', row);	
	preprocessDateBeforeSubmitting('leaveEndDate', row);	
	preprocessDateBeforeSubmitting('leaveEntryDate', row);	

	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "LvmServlet");	
}

function init(row)
{


	
}




function submitAjax(i, deletedStyle)
{
	console.log('submitAjax called');
	var isSubmittable = PreprocessBeforeSubmiting(i, "inplaceedit");
	if(isSubmittable == false)
	{
		return;
	}
	var formData = new FormData();
	var value;
	value = document.getElementById('iD_hidden_' + i).value;
	console.log('submitAjax i = ' + i + ' id = ' + value);
	formData.append('iD', value);
	formData.append("identity", value);
	formData.append("ID", value);
	formData.append('employeeInLeaveType', document.getElementById('employeeInLeaveType_select_' + i).value);
	formData.append('employeeInLeaveOrganogramId', document.getElementById('employeeInLeaveOrganogramId_hidden_' + i).value);
	formData.append('employeeInLeaveName', document.getElementById('employeeInLeaveName_text_' + i).value);
	formData.append('employeeInLeaveDesignation', document.getElementById('employeeInLeaveDesignation_text_' + i).value);
	formData.append('employeeInLeaveOfficeUnitId', document.getElementById('employeeInLeaveOfficeUnitId_hidden_' + i).value);
	formData.append('employeeInLeaveOfficeUnitName', document.getElementById('employeeInLeaveOfficeUnitName_text_' + i).value);
	formData.append('employeeInChargeType', document.getElementById('employeeInChargeType_select_' + i).value);
	formData.append('employeeInChargeOrganogramId', document.getElementById('employeeInChargeOrganogramId_hidden_' + i).value);
	formData.append('employeeInChargeName', document.getElementById('employeeInChargeName_text_' + i).value);
	formData.append('employeeInChargeDesignation', document.getElementById('employeeInChargeDesignation_text_' + i).value);
	formData.append('employeeInChargeOfficeUnitId', document.getElementById('employeeInChargeOfficeUnitId_hidden_' + i).value);
	formData.append('employeeInChargeOfficeUnitName', document.getElementById('employeeInChargeOfficeUnitName_text_' + i).value);
	formData.append('employeeInChargePolicyRolesType', document.getElementById('employeeInChargePolicyRolesType_select_' + i).value);
	formData.append('leaveStartDate', document.getElementById('leaveStartDate_date_' + i).value);
	formData.append('leaveEndDate', document.getElementById('leaveEndDate_date_' + i).value);
	formData.append('leaveEntryDate', document.getElementById('leaveEntryDate_date_' + i).value);
	formData.append('leaveStatusCat', document.getElementById('leaveStatusCat_category_' + i).value);
	formData.append('isDeleted', document.getElementById('isDeleted_hidden_' + i).value);
	formData.append('lastModificationTime', document.getElementById('lastModificationTime_hidden_' + i).value);

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{
			if(this.responseText !='')
			{				
				document.getElementById('tr_' + i).innerHTML = this.responseText ;
				ShowExcelParsingResult(i);
			}
			else
			{
				console.log("No Response");
				document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
			}
		}
		else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	  };
	xhttp.open("POST", 'LvmServlet?actionType=edit&inplacesubmit=true&isPermanentTable=<%=isPermanentTable%>&deletedStyle=' + deletedStyle + '&rownum=' + i, true);
	xhttp.send(formData);
}

window.onload =function ()
{
	ShowExcelParsingResult('general');
	}	
</script>
			