<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="lvm.LvmDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>

<%
LvmDTO lvmDTO = (LvmDTO)request.getAttribute("lvmDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(lvmDTO == null)
{
	lvmDTO = new LvmDTO();
	
}
System.out.println("lvmDTO = " + lvmDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.LVM_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=lvmDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeInLeaveType'>")%>
			
	
	<div class="form-inline" id = 'employeeInLeaveType_div_<%=i%>'>
		<select class='form-control'  name='employeeInLeaveType' id = 'employeeInLeaveType_select_<%=i%>'   tag='pb_html'>
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "employee_in_leave", "employeeInLeaveType_select_" + i, "form-control", "employeeInLeaveType", lvmDTO.employeeInLeaveType + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "employee_in_leave", "employeeInLeaveType_select_" + i, "form-control", "employeeInLeaveType" );			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeInLeaveOrganogramId'>")%>
			

		<input type='hidden' class='form-control'  name='employeeInLeaveOrganogramId' id = 'employeeInLeaveOrganogramId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.employeeInLeaveOrganogramId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeInLeaveName'>")%>
			
	
	<div class="form-inline" id = 'employeeInLeaveName_div_<%=i%>'>
		<input type='text' class='form-control'  name='employeeInLeaveName' id = 'employeeInLeaveName_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.employeeInLeaveName + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeInLeaveDesignation'>")%>
			
	
	<div class="form-inline" id = 'employeeInLeaveDesignation_div_<%=i%>'>
		<input type='text' class='form-control'  name='employeeInLeaveDesignation' id = 'employeeInLeaveDesignation_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.employeeInLeaveDesignation + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeInLeaveOfficeUnitId'>")%>
			

		<input type='hidden' class='form-control'  name='employeeInLeaveOfficeUnitId' id = 'employeeInLeaveOfficeUnitId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.employeeInLeaveOfficeUnitId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeInLeaveOfficeUnitName'>")%>
			
	
	<div class="form-inline" id = 'employeeInLeaveOfficeUnitName_div_<%=i%>'>
		<input type='text' class='form-control'  name='employeeInLeaveOfficeUnitName' id = 'employeeInLeaveOfficeUnitName_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.employeeInLeaveOfficeUnitName + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeInChargeType'>")%>
			
	
	<div class="form-inline" id = 'employeeInChargeType_div_<%=i%>'>
		<select class='form-control'  name='employeeInChargeType' id = 'employeeInChargeType_select_<%=i%>'   tag='pb_html'>
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "employee_in_charge", "employeeInChargeType_select_" + i, "form-control", "employeeInChargeType", lvmDTO.employeeInChargeType + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "employee_in_charge", "employeeInChargeType_select_" + i, "form-control", "employeeInChargeType" );			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeInChargeOrganogramId'>")%>
			

		<input type='hidden' class='form-control'  name='employeeInChargeOrganogramId' id = 'employeeInChargeOrganogramId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.employeeInChargeOrganogramId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeInChargeName'>")%>
			
	
	<div class="form-inline" id = 'employeeInChargeName_div_<%=i%>'>
		<input type='text' class='form-control'  name='employeeInChargeName' id = 'employeeInChargeName_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.employeeInChargeName + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeInChargeDesignation'>")%>
			
	
	<div class="form-inline" id = 'employeeInChargeDesignation_div_<%=i%>'>
		<input type='text' class='form-control'  name='employeeInChargeDesignation' id = 'employeeInChargeDesignation_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.employeeInChargeDesignation + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeInChargeOfficeUnitId'>")%>
			

		<input type='hidden' class='form-control'  name='employeeInChargeOfficeUnitId' id = 'employeeInChargeOfficeUnitId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.employeeInChargeOfficeUnitId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeInChargeOfficeUnitName'>")%>
			
	
	<div class="form-inline" id = 'employeeInChargeOfficeUnitName_div_<%=i%>'>
		<input type='text' class='form-control'  name='employeeInChargeOfficeUnitName' id = 'employeeInChargeOfficeUnitName_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.employeeInChargeOfficeUnitName + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeInChargePolicyRolesType'>")%>
			
	
	<div class="form-inline" id = 'employeeInChargePolicyRolesType_div_<%=i%>'>
		<select class='form-control'  name='employeeInChargePolicyRolesType' id = 'employeeInChargePolicyRolesType_select_<%=i%>'   tag='pb_html'>
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "employee_in_charge_policy_roles", "employeeInChargePolicyRolesType_select_" + i, "form-control", "employeeInChargePolicyRolesType", lvmDTO.employeeInChargePolicyRolesType + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "employee_in_charge_policy_roles", "employeeInChargePolicyRolesType_select_" + i, "form-control", "employeeInChargePolicyRolesType" );			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_leaveStartDate'>")%>
			
	
	<div class="form-inline" id = 'leaveStartDate_div_<%=i%>'>
		<input type='date' class='form-control'  name='leaveStartDate_Date_<%=i%>' id = 'leaveStartDate_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_leaveStartDate = new SimpleDateFormat("dd-MM-yyyy");
	String formatted_leaveStartDate = format_leaveStartDate.format(new Date(lvmDTO.leaveStartDate));
	%>
	'<%=formatted_leaveStartDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
		<input type='hidden' class='form-control'  name='leaveStartDate' id = 'leaveStartDate_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.leaveStartDate + "'"):("'" + "0" + "'")%>  tag='pb_html'>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_leaveEndDate'>")%>
			
	
	<div class="form-inline" id = 'leaveEndDate_div_<%=i%>'>
		<input type='date' class='form-control'  name='leaveEndDate_Date_<%=i%>' id = 'leaveEndDate_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_leaveEndDate = new SimpleDateFormat("dd-MM-yyyy");
	String formatted_leaveEndDate = format_leaveEndDate.format(new Date(lvmDTO.leaveEndDate));
	%>
	'<%=formatted_leaveEndDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
		<input type='hidden' class='form-control'  name='leaveEndDate' id = 'leaveEndDate_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.leaveEndDate + "'"):("'" + "0" + "'")%>  tag='pb_html'>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_leaveEntryDate'>")%>
			
	
	<div class="form-inline" id = 'leaveEntryDate_div_<%=i%>'>
		<input type='date' class='form-control'  name='leaveEntryDate_Date_<%=i%>' id = 'leaveEntryDate_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_leaveEntryDate = new SimpleDateFormat("dd-MM-yyyy");
	String formatted_leaveEntryDate = format_leaveEntryDate.format(new Date(lvmDTO.leaveEntryDate));
	%>
	'<%=formatted_leaveEntryDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
		<input type='hidden' class='form-control'  name='leaveEntryDate' id = 'leaveEntryDate_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + lvmDTO.leaveEntryDate + "'"):("'" + "0" + "'")%>  tag='pb_html'>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_leaveStatusCat'>")%>
			
	
	<div class="form-inline" id = 'leaveStatusCat_div_<%=i%>'>
		<select class='form-control'  name='leaveStatusCat' id = 'leaveStatusCat_category_<%=i%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "leave_status", lvmDTO.leaveStatusCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "leave_status", -1);			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + lvmDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=lvmDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
		