<%@page pageEncoding="UTF-8" %>

<%@page import="lvm.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.LVM_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_LVM;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
LvmDTO lvmDTO = (LvmDTO)request.getAttribute("lvmDTO");
CommonDTO commonDTO = lvmDTO;
String servletName = "LvmServlet";


System.out.println("lvmDTO = " + lvmDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


LvmDAO lvmDAO = (LvmDAO)request.getAttribute("lvmDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

											
		
											
											<td id = '<%=i%>_employeeInLeaveType'>
											<%
											value = lvmDTO.employeeInLeaveType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "employee_records", Language.equals("English")?"name_eng":"name_bng", "id");
											%>
														
											<%=value%>
				
			
											</td>
												
											
											<td id = '<%=i%>_employeeInLeaveDesignation'>
											<%
											value = lvmDTO.employeeInLeaveDesignation + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											
		
											
											<td id = '<%=i%>_employeeInLeaveOfficeUnitName'>
											<%
											value = lvmDTO.employeeInLeaveOfficeUnitName + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_employeeInChargeType'>
											<%
											value = lvmDTO.employeeInChargeType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "employee_records", Language.equals("English")?"name_eng":"name_bng", "id");
											%>
														
											<%=value%>
				
			
											</td>
		

											
											<td id = '<%=i%>_employeeInChargeDesignation'>
											<%
											value = lvmDTO.employeeInChargeDesignation + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											
											<td id = '<%=i%>_employeeInChargeOfficeUnitName'>
											<%
											value = lvmDTO.employeeInChargeOfficeUnitName + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_employeeInChargePolicyRolesType'>
											<%
											value = lvmDTO.employeeInChargePolicyRolesType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "employee_policy_roles", Language.equals("English")?"name_eng":"name_bng", "id");
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_leaveStartDate'>
											<%
											value = lvmDTO.leaveStartDate + "";
											%>
											<%
											String formatted_leaveStartDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_leaveStartDate%>
				
			
											</td>
		
											
											<td id = '<%=i%>_leaveEndDate'>
											<%
											value = lvmDTO.leaveEndDate + "";
											%>
											<%
											String formatted_leaveEndDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_leaveEndDate%>
				
			
											</td>
		
									
											

