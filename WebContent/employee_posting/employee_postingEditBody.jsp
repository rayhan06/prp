<%@page import="office_units.Office_unitsRepository" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="employee_posting.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="geolocation.GeoCountryRepository" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="employee_office_report.InChargeLevelEnum" %>
<%@include file="../pb/addInitializer2.jsp" %>

<%
    Employee_postingDTO employee_postingDTO;
    String data = request.getParameter("data"), formTitle;
    boolean isLocalPosting = false;
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    if (actionName.equals("ajax_add")) {
        employee_postingDTO = new Employee_postingDTO();
        if (data.equalsIgnoreCase("localPosting")) {
            formTitle = LM.getText(LC.EMPLOYEE_POSTING_ADD_LOCALPOSTING, loginDTO);
            isLocalPosting = true;
        } else {
            formTitle = LM.getText(LC.EMPLOYEE_POSTING_ADD_FOREIGNPOSTING, loginDTO);
        }
    } else {
        employee_postingDTO = (Employee_postingDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
        if (employee_postingDTO.localOrForeign == PostingTypeEnum.LOCAL.getValue()) {
            formTitle = LM.getText(LC.EMPLOYEE_POSTING_ADD_LOCALPOSTING, loginDTO);
            data = "localPosting";
            isLocalPosting = true;
        } else {
            formTitle = LM.getText(LC.EMPLOYEE_POSTING_ADD_FOREIGNPOSTING, loginDTO);
            data = "foreignPosting";
        }
    }
    String empId = request.getParameter("empId");
    String url = "Employee_postingServlet?actionType=" + actionName + "&isPermanentTable=true&empId="
            + empId + "&data=" + request.getParameter("data")
            + "&iD=" + employee_postingDTO.iD + "&localOrForeign=" + data;

    if (request.getParameter("tab") != null) {
        url += "&tab=" + request.getParameter("tab") + "&userId=" + request.getParameter("userId");
    }
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i><%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form" id="bigform" name="bigform" enctype="multipart/form-data"
              action="<%=url%>">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-8 offset-md-2 px-4 px-md-0">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <%--Input Fields--%>
                                    <%
                                        if (isLocalPosting) {
                                    %>
                                    
                                    <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                            </label>
                                            <div class="col-md-9" id='office-unit_button_<%=i%>'>
                                                <button type="button" class="btn btn-primary form-control"
                                                        id="addOffice_modal_button">
                                                    <%=isLanguageEnglish ? "Add Office and Designation" : "অফিস এবং পদবি যুক্ত করুন"%>
                                                </button>
                                            </div>
                                        </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="post_select">
                                            <%=LM.getText(LC.HM_OFFICE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9" id='post_div'>
                                        	<input type = "text" class='form-control' id = "post_tb" name = "post_tb" value = ""  readonly/>
                                            
                                        </div>
                                        <input type = "hidden" id = "post" name = "post" value = "<%=employee_postingDTO.post%>" />
                                    </div>

                                    <div class="form-group row designationDiv">
                                        <label class="col-md-3 col-form-label text-md-right" for="designation_select">
                                            <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_DESIGNATION, loginDTO)%>
                                        </label>
                                        <div class="col-md-9" id='designation_div'>
                                            <input type = "text" class='form-control' id = "designation_tb" name = "designation_tb" value = "" readonly/>
                                        </div>
                                        <input type = "hidden" id = "designation" name = "designation" value = "<%=employee_postingDTO.designation%>" />
                                    </div>
                                    <div class="form-group row oldData" style="display:none;">
                                        <label class="col-md-3 col-form-label text-md-right" for="oldOffice">
                                            <%=LM.getText(LC.HM_OFFICE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9" id='oldOffice_div_<%=i%>'>
                                            <input type='text' class='form-control'
                                                   name='oldOffice'
                                                   placeholder="<%=isLanguageEnglish?"Enter office":"অফিসের নাম লিখুন"%>"
                                                   id='oldOffice' tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row oldData" style="display:none;">
                                        <label class="col-md-3 col-form-label text-md-right" for="oldDesignation">
                                            <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_DESIGNATION, loginDTO)%>
                                        </label>
                                        <div class="col-md-9" id='oldDesignation_div_<%=i%>'>
                                            <input type='text' class='form-control'
                                                   name='oldDesignation'
                                                   placeholder="<%=isLanguageEnglish?"Enter designation":"পদবীর নাম লিখুন"%>"
                                                   id='oldDesignation' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="inChargeLevel">
                                            <%=(isLangEng ? "In charge Level" : "ইনচার্জ লেভেল")%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9" id='designation_div_<%=i%>'>
                                            <select class="form-control" id="inChargeLevel" name="inChargeLevel">
                                                <%=InChargeLevelEnum.getBuildOptions(Language)%>
                                            </select>
                                        </div>
                                    </div>

                                    <%
                                    } else {
                                    %>
                                    <div class="form-group row"
                                         id='isCurrentlyWorking_div_<%=i%>'>
                                        <label class="col-md-3 col-form-label text-md-right"
                                               for="isCurrentlyWorking_checkbox_<%=i%>">
                                            <%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_ISCURRENTLYWORKING, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='checkbox'
                                                   class='form-control-sm mt-1'
                                                   name='isCurrentlyWorking'
                                                   onchange="isCurrentlyWorkingChange()"
                                                   id='isCurrentlyWorking_checkbox_<%=i%>'
                                                   value='true' <%=(actionName.equals("edit") && employee_postingDTO.postingTo == SessionConstants.MIN_DATE)?"checked":""%>
                                                   tag='pb_html'><br>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="designation_foreign">
                                            <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_DESIGNATION, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control rounded' name='designation_foreign'
                                                   id='designation_foreign'
                                                   placeholder='<%=isLangEng?"Enter designation":"পদবী লিখুন"%>'
                                                   value='<%=employee_postingDTO.designationForeign!=null?employee_postingDTO.designationForeign:""%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="organization_text">
                                            <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_ORGANIZATION, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control rounded' name='organization'
                                                   id='organization_text'
                                                   placeholder='<%="English".equalsIgnoreCase(Language)?"Enter Organization Name":"সংস্থার নাম লিখুন"%>'
                                                   value='<%=employee_postingDTO.organization!=null?employee_postingDTO.organization:""%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="country">
                                            <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_COUNTRY, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9" id='country_div_<%=i%>'>
                                            <select class='form-control' name='country' id='country' tag='pb_html'>
                                                <%=GeoCountryRepository.getInstance().buildOptions(Language, employee_postingDTO.country)%>
                                            </select>
                                        </div>
                                    </div>

                                    <%
                                        }
                                    %>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.GLOBAL_FROM_DATE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9" id='postingFrom_div_<%=i%>'>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="posting-from-date-js"/>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                            </jsp:include>
                                            <input type='hidden' class='form-control' id='posting-from-date'
                                                   name='postingFrom' value=''
                                                   tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row" id="postingTo_div">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.GLOBAL_TO_DATE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9" id='postingTo_div_<%=i%>'>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="posting-to-date-js"/>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                            </jsp:include>
                                            <input type='hidden' class='form-control' id='posting-to-date'
                                                   name='postingTo' value=''
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-md-10 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                            <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_EMPLOYEE_POSTING_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                                onclick="submitForm()">
                            <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_EMPLOYEE_POSTING_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<%@include file="../office_assign/officeSearchModal.jsp" %>

<script type="text/javascript">
    const form = $("#bigform");
    let officeId = -1;
    let isShowTable = 1;
    let officeName = '';
    let officeUnitOrganogramId = -1;
    let officeUnitOrganogramName = '';

    $('#addOffice_modal_button').on('click', function () {
        //alert('CLICKED');
        $('#search_office_modal').modal();
    });


    $(document).ready(function () {
        <%
        if(isLocalPosting){
        %>
        select2SingleSelector("#post_select", "<%=Language%>");
        select2SingleSelector("#designation_select", "<%=Language%>");
        <%
        }else{
        %>
        select2SingleSelector("#country", "<%=Language%>");
        <%
        }
        %>
        form.validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                <%
                    if(isLocalPosting){
                %>
                post: "required",
                inChargeLevel: "required"
                <%
                }else{
                %>
                designation_foreign: "required",
                organization: "required",
                country: "required"
                <%
                }
                %>
            },

            messages: {
                organization: "<%=LM.getText(LC.EMPLOYEE_POSTING_ADD_ENTER_ORGANIZATION, loginDTO)%>",
                post: "<%=LM.getText(LC.EMPLOYEE_POSTING_ADD_ENTER_OFFICE, loginDTO)%>",
                designation: "<%=LM.getText(LC.EMPLOYEE_POSTING_ADD_ENTER_POST, loginDTO)%>",
                country: "<%=LM.getText(LC.EMPLOYEE_POSTING_ADD_SELECT_COUNTRY, loginDTO)%>",
                designation_foreign: '<%=isLangEng?"Enter designation":"পদবী লিখুন"%>',
                inChargeLevel: "<%=(isLangEng ? "Provide In charge Level": "ইনচার্জ লেভেল দিন") %>"
            }
        });

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    });
    $(function () {
        $('#posting-from-date-js').on('datepicker.change', () => {
            setMinDateById('posting-to-date-js', getDateStringById('posting-from-date-js'));
        });
        <%if (actionName.equalsIgnoreCase("edit") || actionName.equalsIgnoreCase("ajax_edit")) {%>
        fetchDesignation($('#post_select').val(), '<%=Language%>', '<%=employee_postingDTO.designation%>');
        setDateByTimestampAndId('posting-from-date-js', <%=employee_postingDTO.postingFrom%>);
        <%
          if(employee_postingDTO.postingTo!= SessionConstants.MIN_DATE){
        %>
        setDateByTimestampAndId('posting-to-date-js', <%=employee_postingDTO.postingTo%>);
        <%}else{%>
        $('#postingTo_div').hide();
        <%}}%>
    });


    function isFormValid() {
        const jQueryValid = form.valid();

        $('#posting-from-date').val(getDateStringById('posting-from-date-js'));
        $('#posting-to-date').val(getDateStringById('posting-to-date-js'));

        const fromDateValid = dateValidator('posting-from-date-js', true, {
            'errorEn': '<%=LM.getInstance().getText("English", LC.EMPLOYEE_POSTING_ADD_ENTER_VALID_POSTING_START_DATE)%>',
            'errorBn': '<%=LM.getInstance().getText("Bangla", LC.EMPLOYEE_POSTING_ADD_ENTER_VALID_POSTING_START_DATE)%>'
        })

        let toDateValid = true;

        if (!$('#isCurrentlyWorking_checkbox_0').is(":checked")) {
            toDateValid = dateValidator('posting-to-date-js', true, {
                'errorEn': '<%=LM.getInstance().getText("English", LC.EMPLOYEE_POSTING_ADD_ENTER_VALID_POSTING_TO_DATE)%>',
                'errorBn': '<%=LM.getInstance().getText("Bangla", LC.EMPLOYEE_POSTING_ADD_ENTER_VALID_POSTING_TO_DATE)%>'
            });
        }

        return jQueryValid && fromDateValid && toDateValid;
    }

    function fetchDesignation(officeUnitId, language, selectedId) {
        if (officeUnitId ==<%=Office_unitsRepository.OTHER_MAXIMUM_VALUE%>) {
            $('.oldData').show();
            $('.designationDiv').hide();
        } else {

            $('.oldData').hide();
            $('.designationDiv').show();
            let url = "Employee_postingServlet?actionType=getDesignationOpt&office_unit_id=" + officeUnitId + "&language=" + language + "&selectedId=" + selectedId;
            console.log("url : " + url);
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    document.getElementById('designation_select').innerHTML = fetchedData;
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

    }

    $("#post_select").change(function () {
        fetchDesignation($('#post_select').val(), '<%=Language%>', '<%=employee_postingDTO.designation%>');
    });

    function submitForm() {
        if (isFormValid()) {
            submitAjaxForm('bigform');
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function isCurrentlyWorkingChange() {
        if ($('#isCurrentlyWorking_checkbox_0').is(":checked")) {
            resetDateById('posting-to-date-js');
            $('#postingTo_div').hide();
        } else {
            $('#postingTo_div').show();
        }
    }
    
    function triggerOfficeUnitOrganogramId() {

        console.log(officeId + "  " + officeUnitOrganogramId);
        $("#post_tb").val(officeName)
        $("#designation_tb").val(officeUnitOrganogramName);
        
        $("#post").val(officeId);
        $("#designation").val(officeUnitOrganogramId);
    }
</script>