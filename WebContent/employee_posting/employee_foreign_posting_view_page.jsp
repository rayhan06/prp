<%@ page import="employee_posting.Employee_postingDTO" %>
<%@ page import="java.util.List" %>

<input type="hidden" data-ajax-marker="true">

<table class="table table-striped table-bordered" style="font-size: 14px">
    <thead>
    <tr>
        <th style="vertical-align: middle;text-align: left"><b><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_ORGANIZATION, loginDTO)%></b></th>
        <th style="vertical-align: middle;text-align: left"><b><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_DESIGNATION, loginDTO)%></b></th>
        <th style="vertical-align: middle;text-align: left"><b><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_COUNTRY, loginDTO)%></b></th>
        <th style="vertical-align: middle;text-align: left"><b><%=LM.getText(LC.GLOBAL_FROM_DATE, loginDTO)%></b></th>
        <th style="vertical-align: middle;text-align: left"><b><%=LM.getText(LC.GLOBAL_TO_DATE, loginDTO)%></b></th>
        <th style="vertical-align: middle;text-align: left"></th>
    </tr>
    </thead>
    <tbody>
    <%
        List<Employee_postingDTO> savedForeignPostingList = (List<Employee_postingDTO>) request.getAttribute("savedForeignPostingList");
        int foreignPostingIndex = 0;
        if (savedForeignPostingList != null && savedForeignPostingList.size() > 0) {
            for (Employee_postingDTO foreignPostingItem : savedForeignPostingList) {
                ++foreignPostingIndex;
    %>
    <tr>
        <%@include file="/employee_posting/employee_foreign_posting_item.jsp" %>
    </tr>
    <% } } %>
    </tbody>
</table>
<div class="row">
    <div class="col-12 text-right mt-3">
        <button class="btn btn-gray m-t-10 rounded-pill"
                onclick="location.href='<%=request.getContextPath()%>/Employee_postingServlet?actionType=getAddPage&tab=3&data=foreignPosting&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
            <i class="fa fa-plus"></i><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_ADD_FOREIGN_POSTING, loginDTO)%>
        </button>
    </div>
</div>