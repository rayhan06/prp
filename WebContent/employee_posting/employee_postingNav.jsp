<%@page contentType="text/html;charset=utf-8" %>
<%@page import="language.LC" %>

<%@page import="language.LM" %>
<%
    String url = "Employee_postingServlet?actionType=search";
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6" >
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_POSTINGFROM, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="posting_from_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>
                        </div>
                        <input type='hidden' id="posting_from" name="posting_from" value='' tag='pb_html'/>
                    </div>
                </div>
                <div class="col-md-6" >
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_POSTINGTO, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="posting_to_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>
                        </div>
                        <input type='hidden' id="posting_to" name="posting_to" value='' tag='pb_html'/>
                    </div>
                </div>

                <%--<div class="col-md-6" >
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="localOrForeign">
                            <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_LOCALORFOREIGN, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class="form-control" id="localOrForeign" name="localOrForeign"
                                    onChange='setSearchChanged()'>
                                <%=PostingTypeEnum.buildOptions(isLanguageEnglishNav)%>
                            </select>
                        </div>
                    </div>
                </div>--%>

                <div class="col-md-6" >
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="organization">
                            <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_ORGANIZATION, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="organization" placeholder="" name="organization"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit" class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->

<%@include file="../common/pagination_with_go2.jsp" %>

<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=url%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        var params = 'AnyField=' + document.getElementById('anyfield').value;

        params += '&organization=' + $('#organization').val();

        $("#posting_from").val(getDateStringById("posting_from_date_js"));
        if($('#posting_from').val()) {
            params += '&posting_from=' + getBDFormattedDate('posting_from');
        }

        $("#posting_to").val(getDateStringById("posting_to_date_js"));
        if($('#posting_to').val()) {
            params += '&posting_to=' + getBDFormattedDate('posting_to');
        }
        /*if($('#localOrForeign').val()){
            params += '&localOrForeign='+$('#localOrForeign').val();
        }*/

        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);
    }
</script>

