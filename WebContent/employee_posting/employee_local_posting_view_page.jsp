<%@ page import="employee_posting.Employee_local_posting_model" %>
<%@ page import="java.util.List" %>

<%
    List<Employee_local_posting_model> savedLocalPostingModels = (List<Employee_local_posting_model>) request.getAttribute("savedLocalPostingModels");
%>

<input type="hidden" data-ajax-marker="true">

<table class="table table-striped table-bordered" style="font-size: 14px">
    <thead>
    <tr>
        <th style="vertical-align: middle;text-align: left"><b><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_POST, loginDTO)%></b></th>
        <th style="vertical-align: middle;text-align: left"><b><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_PLACE, loginDTO)%></b></th>
        <th style="vertical-align: middle;text-align: left"><b><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_DESIGNATION, loginDTO)%></b></th>
        <th style="vertical-align: middle;text-align: left"><b><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_PAYSCALE, loginDTO)%></b></th>
        <th style="vertical-align: middle;text-align: left"><b><%=LM.getText(LC.GLOBAL_FROM_DATE, loginDTO)%></b></th>
        <th style="vertical-align: middle;text-align: left"><b><%=LM.getText(LC.GLOBAL_TO_DATE, loginDTO)%></b></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <%
        int localPostingIndex = 0;
        if (savedLocalPostingModels != null && savedLocalPostingModels.size() > 0) {
            for (Employee_local_posting_model localPostingItem : savedLocalPostingModels) {
                localPostingIndex++;
    %>
    <tr>
        <%@include file="/employee_posting/employee_local_posting_item.jsp" %>
    </tr>
    <% } } %>
    </tbody>
</table>

<div class="row">
    <div class="col-12 text-right mt-3">
        <button class="btn btn-gray m-t-10 rounded-pill"
                onclick="location.href='<%=request.getContextPath()%>/Employee_postingServlet?actionType=getAddPage&tab=3&data=localPosting&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
            <i class="fa fa-plus"></i><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_ADD_LOCAL_POSTING, loginDTO)%>
        </button>
    </div>
</div>