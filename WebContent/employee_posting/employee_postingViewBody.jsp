<%@page import="geolocation.GeoDistrictRepository" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="employee_posting.*" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="common.BaseServlet" %>
<%@page import="office_units.Office_unitsRepository" %>
<%@page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@page import="util.StringUtils" %>
<%@page import="geolocation.GeoCountryRepository" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@include file="../pb/viewInitializer.jsp" %>

<%

    Employee_postingDTO employee_postingDTO = (Employee_postingDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_EMPLOYEE_POSTING_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row">
                <div class="col-10 offset-1">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-10 offset-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background-color: white"><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_EMPLOYEE_POSTING_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped text-nowrap">
                                        <tr>
                                            <td>
                                                <b>
                                                    <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_LOCALORFOREIGN, loginDTO)%>
                                                </b>
                                            </td>
                                            <td>
                                                <%=PostingTypeEnum.getTextByValue(employee_postingDTO.localOrForeign, isLanguageEnglish)%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>
                                                    <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_POST, loginDTO)%>
                                                </b>
                                            </td>
                                            <td>
                                                <%=Office_unitsRepository.getInstance().geText(Language, employee_postingDTO.post)%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>
                                                    <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_DESIGNATION, loginDTO)%>
                                                </b>
                                            </td>
                                            <td>
                                                <%=OfficeUnitOrganogramsRepository.getInstance().getDesignation(Language, employee_postingDTO.designation)%>
                                            </td>
                                        </tr>
                                        <%
                                            if (employee_postingDTO.localOrForeign == PostingTypeEnum.FOREIGN.getValue()) {
                                        %>
                                        <tr>
                                            <td>
                                                <b>
                                                    <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_COUNTRY, loginDTO)%>
                                                </b>
                                            </td>
                                            <td>
                                                <%=GeoCountryRepository.getInstance().getText(Language, employee_postingDTO.country)%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <b>
                                                    <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_ORGANIZATION, loginDTO)%>
                                                </b>
                                            </td>
                                            <td>
                                                <%=employee_postingDTO.organization%>
                                            </td>
                                        </tr>
                                        <%
                                        } else {
                                        %>
                                        <tr>
                                            <td>
                                                <b>
                                                    <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_PLACE, loginDTO)%>
                                                </b>
                                            </td>
                                            <td>
                                                <%=GeoDistrictRepository.getInstance().getText(Language, employee_postingDTO.place)%>
                                            </td>
                                        </tr>
                                        <%
                                            }
                                        %>

                                        <tr>
                                            <td>
                                                <b>
                                                    <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_POSTINGFROM, loginDTO)%>
                                                </b>
                                            </td>
                                            <td>
                                                <%=StringUtils.getFormattedDate(Language, employee_postingDTO.postingFrom)%>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <b>
                                                    <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_POSTINGTO, loginDTO)%>
                                                </b>
                                            </td>
                                            <td>
                                                <%
                                                    if(employee_postingDTO.postingTo!= SessionConstants.MIN_DATE){
                                                %>
                                                <%=StringUtils.getFormattedDate(Language, employee_postingDTO.postingTo)%>
                                                <%
                                                    }else{
                                                %>
                                                    <%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_ISCURRENTLYWORKING, loginDTO)%>
                                                <%
                                                    }
                                                %>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>