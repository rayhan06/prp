<%@page import="employee_records.Employee_recordsRepository" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="util.StringUtils" %>
<%@ page import="geolocation.GeoCountryRepository" %>

<td>
    <%=Employee_recordsRepository.getInstance().getEmployeeName(employee_postingDTO.employeeRecordsId, Language)%>
</td>

<%--<td>
    <%=PostingTypeEnum.getTextByValue(employee_postingDTO.localOrForeign, Language)%>
</td>

<td>
    <%=Office_unitsRepository.getInstance().geText(Language, employee_postingDTO.post) %>
</td>--%>
<td>
    <%=employee_postingDTO.designationForeign%>
</td>


<td>
    <%=employee_postingDTO.organization%>
</td>


<td>
    <%=GeoCountryRepository.getInstance().getText(Language, employee_postingDTO.country)%>
</td>

<td>
    <%=StringUtils.getFormattedDate(Language, employee_postingDTO.postingFrom)%>
</td>

<td>
    <%=StringUtils.getFormattedDate(Language, employee_postingDTO.postingTo)%>
</td>

<%--<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Employee_postingServlet?actionType=view&ID=<%=employee_postingDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>--%>

<td>
    <button
            type="button" class="btn-sm border-0 shadow btn-border-radius text-white" style="background-color: #ff6b6b;"
            onclick="location.href='Employee_postingServlet?actionType=getEditPage&ID=<%=employee_postingDTO.iD%>&empId=<%=employee_postingDTO.employeeRecordsId%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td class="text-right">
    <div class='checker'>
        <span class='chkEdit'>
            <input type='checkbox' name='ID' value='<%=employee_postingDTO.iD%>'/>
        </span>
    </div>
</td>