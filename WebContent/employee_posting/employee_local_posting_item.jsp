<%@page import="geolocation.GeoDistrictRepository" %>

<td style="vertical-align: middle;text-align: left"><%=isLanguageEnglish ? localPostingItem.postEng : localPostingItem.postBng%></td>
<td style="vertical-align: middle;text-align: left"><%=GeoDistrictRepository.getInstance().getText(Language, localPostingItem.dto.place)%></td>
<td style="vertical-align: middle;text-align: left"><%=isLanguageEnglish ? localPostingItem.designationEng : localPostingItem.designationBng%></td>
<td style="vertical-align: middle;text-align: left"><%=isLanguageEnglish ? localPostingItem.payScaleEng : localPostingItem.payScaleBng%></td>
<td style="vertical-align: middle;text-align: left"><%=isLanguageEnglish ? localPostingItem.postingFromEng : localPostingItem.postingFromBng%></td>
<%
    if(localPostingItem.dto.postingTo == SessionConstants.MIN_DATE){
%>
<td style="vertical-align: middle;text-align: left"><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_ISCURRENTLYWORKING, loginDTO)%></td>
<%
    }else{
%>
<td style="vertical-align: middle;text-align: left"><%=isLanguageEnglish ? localPostingItem.postingToEng : localPostingItem.postingToBng%></td>
<%
    }
%>
<%
    if (!actionType.equals("viewSummary")) {
%>

<td style="text-align: center; vertical-align: middle;">
    <form action="Employee_postingServlet?isPermanentTable=true&actionType=delete&data=localPosting&tab=3&ID=<%=localPostingItem.dto.iD%>&empId=<%=empId%>"
          method="POST" id="tableForm_localPosting<%=localPostingIndex%>" enctype="multipart/form-data">
        <div class="btn-group" role="group" aria-label="Basic example">
            <button  class="btn-success" title="Edit" type="button" style="max-height: 30px"
                  onclick="location.href='<%=request.getContextPath()%>/Employee_postingServlet?actionType=getEditPage&data=localPosting&tab=3&ID=<%=localPostingItem.dto.iD%>&empId=<%=empId%>'"><i
                    class="fa fa-edit"></i></button>
            &nbsp;
            <button class="btn-danger" title="Delete" type="button" style="max-height: 30px"
                  onclick="deleteItem('tableForm_localPosting',<%=localPostingIndex%>)"><i
                    class="fa fa-trash"></i></button></div>
    </form>
</td>
<%
    }
%>



