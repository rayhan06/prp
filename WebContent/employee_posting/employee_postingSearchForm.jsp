<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="employee_posting.*" %>
<%@ page import="java.util.List" %>
<%@page pageEncoding="UTF-8" %>

<%
    String navigator2 = BaseServlet.RECORD_NAVIGATOR;
    String servletName = "";
%>
<%@include file="../pb/searchInitializer.jsp" %>

<%
    List<Employee_postingDTO> data = (List<Employee_postingDTO>) rn2.list;
    if (data != null && data.size() > 0) {
%>
<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead class="text-nowrap">
        <tr>
            <th><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_EMPLOYEERECORDSID, loginDTO)%>
            </th>
            <%--<th><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_LOCALORFOREIGN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_POST, loginDTO)%>
            </th>--%>
            <th><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_DESIGNATION, loginDTO)%></th>
            <th><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_ORGANIZATION, loginDTO)%></th>
            <th><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_COUNTRY, loginDTO)%>
            <th><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_POSTINGFROM, loginDTO)%></th>
            <th><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_POSTINGTO, loginDTO)%>
            <%--<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>--%>
            <th><%=LM.getText(LC.EMPLOYEE_POSTING_SEARCH_EMPLOYEE_POSTING_EDIT_BUTTON, loginDTO)%></th>
            <th>
                <div class="text-center">
                    <span><%=isLanguageEnglish?"All":"সকল"%></span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody class="text-nowrap">
        <%
            for (Employee_postingDTO employee_postingDTO : data) {
        %>
        <tr>
            <%@include file="employee_postingSearchRow.jsp" %>
        </tr>
        <% }
        %>
        </tbody>

    </table>
</div>

<%
} else {
%>
<label style="width: 100%;text-align: center;font-size: larger;font-weight: bold;color: red">
    <%=isLanguageEnglish ? "No information is found" : "কোন তথ্য পাওয়া যায় নি"%>
</label>
<%
    }
%>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>