<%@ page import="geolocation.GeoCountryRepository" %>
<%@ page import="util.StringUtils" %>

<td style="vertical-align: middle;text-align: left"><%=foreignPostingItem.organization%></td>
<td style="vertical-align: middle;text-align: left"><%=foreignPostingItem.designationForeign%></td>
<td style="vertical-align: middle;text-align: left"><%=GeoCountryRepository.getInstance().getText(Language, foreignPostingItem.country)%></td>
<td style="vertical-align: middle;text-align: left"><%=StringUtils.getFormattedDate(Language,foreignPostingItem.postingFrom)%></td>
<%
    if(foreignPostingItem.postingTo == SessionConstants.MIN_DATE){
%>
<td style="vertical-align: middle;text-align: left"><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_ISCURRENTLYWORKING, loginDTO)%></td>
<%
}else{
%>
<td style="vertical-align: middle;text-align: left"><%=StringUtils.getFormattedDate(Language,foreignPostingItem.postingTo)%></td>
<%
    }
%>
<%
    if (!actionType.equals("viewSummary")) {
%>
<td style="text-align: center; vertical-align: middle;">
    <form action="Employee_postingServlet?isPermanentTable=true&actionType=delete&tab=3&data=foreignPosting&ID=<%=foreignPostingItem.iD%>&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>"
          method="POST" id="tableForm_foreignPosting<%=foreignPostingIndex%>" enctype="multipart/form-data">
        <div class="btn-group" role="group" aria-label="Basic example">
            <button class="btn-success" title="Edit" type="button" style="max-height: 30px"
                  onclick="location.href='<%=request.getContextPath()%>/Employee_postingServlet?actionType=getEditPage&data=foreignPosting&tab=3&ID=<%=foreignPostingItem.iD%>&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'"><i
                    class="fa fa-edit"></i></button>
            &nbsp;
            <button class="btn-danger" title="Delete" type="button" style="max-height: 30px"
                  onclick="deleteItem('tableForm_foreignPosting',<%=foreignPostingIndex%>)"><i
                    class="fa fa-trash"></i></button></div>
    </form>
</td>
<%
    }
%>