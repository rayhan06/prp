<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="vm_fuel_request.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
Vm_fuel_requestDTO vm_fuel_requestDTO;
vm_fuel_requestDTO = (Vm_fuel_requestDTO)request.getAttribute("vm_fuel_requestDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(vm_fuel_requestDTO == null)
{
	vm_fuel_requestDTO = new Vm_fuel_requestDTO();
	
}
System.out.println("vm_fuel_requestDTO = " + vm_fuel_requestDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ADD_FORMNAME, loginDTO);
String servletName = "Vm_fuel_requestServlet";
String fileColumnName = "";

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

long ColumnID = -1;
FilesDAO filesDAO = new FilesDAO();
boolean isPermanentTable = true;
String Language = LM.getText(LC.VM_FUEL_REQUEST_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title prp-page-title">
                            <i class="fa fa-gift"></i>&nbsp;
                            <%=formTitle%>
                        </h3>
                    </div>
                </div>
                <form class="form-horizontal"
                      action="Vm_fuel_requestServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
                      id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
                      onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
                    <div class="kt-portlet__body form-body">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="onlyborder">
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="sub_title_top">
                                                        <div class="sub_title">
                                                            <h4 style="background: white"><%=formTitle%>
                                                            </h4>
                                                        </div>
                                                     </div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.iD%>' tag='pb_html'/>
	
														<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.insertedByUserId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.insertedByOrganogramId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.insertionDate%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.lastModificationTime%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.searchColumn%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VEHICLETYPE, loginDTO)%></label>
                                                            <div class="col-8">
																<select class='form-control'  name='vehicleType' id = 'vehicleType_select_<%=i%>'   tag='pb_html'>
																<%
																	Options = CatRepository.getOptions(Language, "vehicle_type", vm_fuel_requestDTO.vehicleType);
																%>
																<%=Options%>
																</select>
		
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='vehicleId' id = 'vehicleId_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.vehicleId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPLICATIONDATE, loginDTO)%></label>
                                                            <div class="col-8">
																<%value = "applicationDate_js_" + i;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='applicationDate' id = 'applicationDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(vm_fuel_requestDTO.applicationDate))%>' tag='pb_html'>
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='vehicleDriverAssignmentId' id = 'vehicleDriverAssignmentId_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.vehicleDriverAssignmentId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_LASTFUELDATE, loginDTO)%></label>
                                                            <div class="col-8">
																<%value = "lastFuelDate_js_" + i;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='lastFuelDate' id = 'lastFuelDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(vm_fuel_requestDTO.lastFuelDate))%>' tag='pb_html'>
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_STATUS, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																	value = "";
																	if(vm_fuel_requestDTO.status != -1)
																	{
																	value = vm_fuel_requestDTO.status + "";
																	}
																%>		
																<input type='number' class='form-control'  name='status' id = 'status_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='fiscalYearId' id = 'fiscalYearId_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.fiscalYearId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVEDDATE, loginDTO)%></label>
                                                            <div class="col-8">
																<%value = "approvedDate_js_" + i;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='approvedDate' id = 'approvedDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(vm_fuel_requestDTO.approvedDate))%>' tag='pb_html'>
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='vendorId' id = 'vendorId_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.vendorId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_SAROKNUMBER, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='sarokNumber' id = 'sarokNumber_text_<%=i%>' value='<%=vm_fuel_requestDTO.sarokNumber%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_FILESDROPZONE, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																fileColumnName = "filesDropzone";
																if(actionName.equals("edit"))
																{
																	List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(vm_fuel_requestDTO.filesDropzone);
																%>			
																	<%@include file="../pb/dropzoneEditor.jsp"%>
																<%
																}
																else
																{
																	ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
																	vm_fuel_requestDTO.filesDropzone = ColumnID;
																}
																%>
				
																<div class="dropzone" action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=vm_fuel_requestDTO.filesDropzone%>">
																	<input type='file' style="display:none"  name='<%=fileColumnName%>File' id = '<%=fileColumnName%>_dropzone_File_<%=i%>'  tag='pb_html'/>			
																</div>								
																<input type='hidden'  name='<%=fileColumnName%>FilesToDelete' id = '<%=fileColumnName%>FilesToDelete_<%=i%>' value=''  tag='pb_html'/>
																<input type='hidden' name='<%=fileColumnName%>' id = '<%=fileColumnName%>_dropzone_<%=i%>'  tag='pb_html' value='<%=vm_fuel_requestDTO.filesDropzone%>'/>		
		


															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTREMARKS, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='paymentRemarks' id = 'paymentRemarks_text_<%=i%>' value='<%=vm_fuel_requestDTO.paymentRemarks%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='requesterOrgId' id = 'requesterOrgId_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.requesterOrgId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='requesterOfficeId' id = 'requesterOfficeId_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.requesterOfficeId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='requesterOfficeUnitId' id = 'requesterOfficeUnitId_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.requesterOfficeUnitId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='requesterEmpId' id = 'requesterEmpId_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.requesterEmpId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTERPHONENUM, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='requesterPhoneNum' id = 'requesterPhoneNum_text_<%=i%>' value='<%=vm_fuel_requestDTO.requesterPhoneNum%>' 																<% 
																	if(!actionName.equals("edit"))
																	{
																%>
																		required="required"  pattern="880[0-9]{10}" title="requesterPhoneNum must start with 880, then contain 10 digits"
																<%
																	}
																%>
  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTERNAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='requesterNameEn' id = 'requesterNameEn_text_<%=i%>' value='<%=vm_fuel_requestDTO.requesterNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTERNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='requesterNameBn' id = 'requesterNameBn_text_<%=i%>' value='<%=vm_fuel_requestDTO.requesterNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTEROFFICENAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='requesterOfficeNameEn' id = 'requesterOfficeNameEn_text_<%=i%>' value='<%=vm_fuel_requestDTO.requesterOfficeNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTEROFFICENAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='requesterOfficeNameBn' id = 'requesterOfficeNameBn_text_<%=i%>' value='<%=vm_fuel_requestDTO.requesterOfficeNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTEROFFICEUNITNAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='requesterOfficeUnitNameEn' id = 'requesterOfficeUnitNameEn_text_<%=i%>' value='<%=vm_fuel_requestDTO.requesterOfficeUnitNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTEROFFICEUNITNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='requesterOfficeUnitNameBn' id = 'requesterOfficeUnitNameBn_text_<%=i%>' value='<%=vm_fuel_requestDTO.requesterOfficeUnitNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTEROFFICEUNITORGNAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='requesterOfficeUnitOrgNameEn' id = 'requesterOfficeUnitOrgNameEn_text_<%=i%>' value='<%=vm_fuel_requestDTO.requesterOfficeUnitOrgNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTEROFFICEUNITORGNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='requesterOfficeUnitOrgNameBn' id = 'requesterOfficeUnitOrgNameBn_text_<%=i%>' value='<%=vm_fuel_requestDTO.requesterOfficeUnitOrgNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='approverOrgId' id = 'approverOrgId_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.approverOrgId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='approverOfficeId' id = 'approverOfficeId_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.approverOfficeId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='approverOfficeUnitId' id = 'approverOfficeUnitId_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.approverOfficeUnitId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='approverEmpId' id = 'approverEmpId_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.approverEmpId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVERPHONENUM, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='approverPhoneNum' id = 'approverPhoneNum_text_<%=i%>' value='<%=vm_fuel_requestDTO.approverPhoneNum%>' 																<% 
																	if(!actionName.equals("edit"))
																	{
																%>
																		required="required"  pattern="880[0-9]{10}" title="approverPhoneNum must start with 880, then contain 10 digits"
																<%
																	}
																%>
  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVERNAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='approverNameEn' id = 'approverNameEn_text_<%=i%>' value='<%=vm_fuel_requestDTO.approverNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVERNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='approverNameBn' id = 'approverNameBn_text_<%=i%>' value='<%=vm_fuel_requestDTO.approverNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVEROFFICENAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='approverOfficeNameEn' id = 'approverOfficeNameEn_text_<%=i%>' value='<%=vm_fuel_requestDTO.approverOfficeNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVEROFFICENAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='approverOfficeNameBn' id = 'approverOfficeNameBn_text_<%=i%>' value='<%=vm_fuel_requestDTO.approverOfficeNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVEROFFICEUNITNAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='approverOfficeUnitNameEn' id = 'approverOfficeUnitNameEn_text_<%=i%>' value='<%=vm_fuel_requestDTO.approverOfficeUnitNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVEROFFICEUNITNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='approverOfficeUnitNameBn' id = 'approverOfficeUnitNameBn_text_<%=i%>' value='<%=vm_fuel_requestDTO.approverOfficeUnitNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVEROFFICEUNITORGNAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='approverOfficeUnitOrgNameEn' id = 'approverOfficeUnitOrgNameEn_text_<%=i%>' value='<%=vm_fuel_requestDTO.approverOfficeUnitOrgNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPROVEROFFICEUNITORGNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='approverOfficeUnitOrgNameBn' id = 'approverOfficeUnitOrgNameBn_text_<%=i%>' value='<%=vm_fuel_requestDTO.approverOfficeUnitOrgNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='paymentReceiverOrgId' id = 'paymentReceiverOrgId_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.paymentReceiverOrgId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='paymentReceiverOfficeId' id = 'paymentReceiverOfficeId_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.paymentReceiverOfficeId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='paymentReceiverOfficeUnitId' id = 'paymentReceiverOfficeUnitId_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.paymentReceiverOfficeUnitId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='paymentReceiverEmpId' id = 'paymentReceiverEmpId_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.paymentReceiverEmpId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVERPHONENUM, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='paymentReceiverPhoneNum' id = 'paymentReceiverPhoneNum_text_<%=i%>' value='<%=vm_fuel_requestDTO.paymentReceiverPhoneNum%>' 																<% 
																	if(!actionName.equals("edit"))
																	{
																%>
																		required="required"  pattern="880[0-9]{10}" title="paymentReceiverPhoneNum must start with 880, then contain 10 digits"
																<%
																	}
																%>
  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVERNAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='paymentReceiverNameEn' id = 'paymentReceiverNameEn_text_<%=i%>' value='<%=vm_fuel_requestDTO.paymentReceiverNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVERNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='paymentReceiverNameBn' id = 'paymentReceiverNameBn_text_<%=i%>' value='<%=vm_fuel_requestDTO.paymentReceiverNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVEROFFICENAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='paymentReceiverOfficeNameEn' id = 'paymentReceiverOfficeNameEn_text_<%=i%>' value='<%=vm_fuel_requestDTO.paymentReceiverOfficeNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVEROFFICENAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='paymentReceiverOfficeNameBn' id = 'paymentReceiverOfficeNameBn_text_<%=i%>' value='<%=vm_fuel_requestDTO.paymentReceiverOfficeNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVEROFFICEUNITNAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='paymentReceiverOfficeUnitNameEn' id = 'paymentReceiverOfficeUnitNameEn_text_<%=i%>' value='<%=vm_fuel_requestDTO.paymentReceiverOfficeUnitNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVEROFFICEUNITNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='paymentReceiverOfficeUnitNameBn' id = 'paymentReceiverOfficeUnitNameBn_text_<%=i%>' value='<%=vm_fuel_requestDTO.paymentReceiverOfficeUnitNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVEROFFICEUNITORGNAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='paymentReceiverOfficeUnitOrgNameEn' id = 'paymentReceiverOfficeUnitOrgNameEn_text_<%=i%>' value='<%=vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_PAYMENTRECEIVEROFFICEUNITORGNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='paymentReceiverOfficeUnitOrgNameBn' id = 'paymentReceiverOfficeUnitOrgNameBn_text_<%=i%>' value='<%=vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=vm_fuel_requestDTO.isDeleted%>' tag='pb_html'/>
											
					
														</div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>                               
                          </div>
                       </div>
					<div class="row">
                        <div class="col-lg-12">
                            <div class="kt-portlet shadow-none" style="margin-top: -20px">
                                <div class="kt-portlet__head border-bottom-0">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title prp-page-title"
                                            style="margin-bottom: -60px!important; font-size: 15px!important;">
                                            <%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM, loginDTO)%>
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body form-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
											<thead>
												<tr>
													<th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM_VEHICLEFUELCAT, loginDTO)%></th>
													<th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM_REQUESTEDAMOUNT, loginDTO)%></th>
													<th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM_LASTFUELAMOUNT, loginDTO)%></th>
													<th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM_LASTFUELDATE, loginDTO)%></th>
													<th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM_REMOVE, loginDTO)%></th>
												</tr>
											</thead>
											<tbody id="field-VmFuelRequestItem">
						
						
											<%
												if(actionName.equals("edit")){
													int index = -1;
													
													
													for(VmFuelRequestItemDTO vmFuelRequestItemDTO: vm_fuel_requestDTO.vmFuelRequestItemDTOList)
													{
														index++;
														
														System.out.println("index index = "+index);

											%>	
							
											<tr id = "VmFuelRequestItem_<%=index + 1%>">
												<td style="display: none;">





														<input type='hidden' class='form-control'  name='vmFuelRequestItem.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=vmFuelRequestItemDTO.iD%>' tag='pb_html'/>
	
												</td>
												<td style="display: none;">





														<input type='hidden' class='form-control'  name='vmFuelRequestItem.insertedByUserId' id = 'insertedByUserId_hidden_<%=childTableStartingID%>' value='<%=vmFuelRequestItemDTO.insertedByUserId%>' tag='pb_html'/>
												</td>
												<td style="display: none;">





														<input type='hidden' class='form-control'  name='vmFuelRequestItem.insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=childTableStartingID%>' value='<%=vmFuelRequestItemDTO.insertedByOrganogramId%>' tag='pb_html'/>
												</td>
												<td style="display: none;">





														<input type='hidden' class='form-control'  name='vmFuelRequestItem.insertionDate' id = 'insertionDate_hidden_<%=childTableStartingID%>' value='<%=vmFuelRequestItemDTO.insertionDate%>' tag='pb_html'/>
												</td>
												<td style="display: none;">





														<input type='hidden' class='form-control'  name='vmFuelRequestItem.lastModificationTime' id = 'lastModificationTime_hidden_<%=childTableStartingID%>' value='<%=vmFuelRequestItemDTO.lastModificationTime%>' tag='pb_html'/>
												</td>
												<td style="display: none;">





														<input type='hidden' class='form-control'  name='vmFuelRequestItem.searchColumn' id = 'searchColumn_hidden_<%=childTableStartingID%>' value='<%=vmFuelRequestItemDTO.searchColumn%>' tag='pb_html'/>
												</td>
												<td style="display: none;">





														<input type='hidden' class='form-control'  name='vmFuelRequestItem.vmFuelRequestId' id = 'vmFuelRequestId_hidden_<%=childTableStartingID%>' value='<%=vmFuelRequestItemDTO.vmFuelRequestId%>' tag='pb_html'/>
												</td>
												<td>										





																<select class='form-control'  name='vmFuelRequestItem.vehicleFuelCat' id = 'vehicleFuelCat_category_<%=childTableStartingID%>'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("vehicle_fuel", Language, vmFuelRequestItemDTO.vehicleFuelCat);
																%>
																<%=Options%>
																</select>
	
												</td>
												<td>										





																<input type='text' class='form-control'  name='vmFuelRequestItem.requestedAmount' id = 'requestedAmount_text_<%=childTableStartingID%>' value='<%=vmFuelRequestItemDTO.requestedAmount%>' 																  tag='pb_html'/>					
												</td>
												<td>										





																<input type='text' class='form-control'  name='vmFuelRequestItem.lastFuelAmount' id = 'lastFuelAmount_text_<%=childTableStartingID%>' value='<%=vmFuelRequestItemDTO.lastFuelAmount%>' 																  tag='pb_html'/>					
												</td>
												<td>										





																<%value = "lastFuelDate_js_" + childTableStartingID;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='vmFuelRequestItem.lastFuelDate' id = 'lastFuelDate_date_<%=childTableStartingID%>' value= '<%=dateFormat.format(new Date(vmFuelRequestItemDTO.lastFuelDate))%>' tag='pb_html'>
												</td>
												<td style="display: none;">





														<input type='hidden' class='form-control'  name='vmFuelRequestItem.isDeleted' id = 'isDeleted_hidden_<%=childTableStartingID%>' value= '<%=vmFuelRequestItemDTO.isDeleted%>' tag='pb_html'/>
											
												</td>
												<td><div class='checker'><span id='chkEdit' ><input type='checkbox' id='vmFuelRequestItem_cb_<%=index%>' name='checkbox' value=''/></span></div></td>
											</tr>								
											<%	
														childTableStartingID ++;
													}
												}
											%>						
						
										</tbody>
									</table>
								</div>
								<div class="form-group">
                                        <div class="col-xs-9 text-right">
                                            <button
                                                    id="add-more-VmFuelRequestItem"
                                                    name="add-moreVmFuelRequestItem"
                                                    type="button"
                                                    class="btn btn-sm text-white add-btn shadow">
                                                <i class="fa fa-plus"></i>
                                                <%=LM.getText(LC.HM_ADD, loginDTO)%>
                                            </button>
                                            <button
                                                    id="remove-VmFuelRequestItem"
                                                    name="removeVmFuelRequestItem"
                                                    type="button"
                                                    class="btn btn-sm remove-btn shadow ml-2">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </div>
                                    </div>
					
									<%VmFuelRequestItemDTO vmFuelRequestItemDTO = new VmFuelRequestItemDTO();%>
					
									<template id="template-VmFuelRequestItem" >						
										<tr>
											<td style="display: none;">





														<input type='hidden' class='form-control'  name='vmFuelRequestItem.iD' id = 'iD_hidden_' value='<%=vmFuelRequestItemDTO.iD%>' tag='pb_html'/>
	
											</td>
											<td style="display: none;">





														<input type='hidden' class='form-control'  name='vmFuelRequestItem.insertedByUserId' id = 'insertedByUserId_hidden_' value='<%=vmFuelRequestItemDTO.insertedByUserId%>' tag='pb_html'/>
											</td>
											<td style="display: none;">





														<input type='hidden' class='form-control'  name='vmFuelRequestItem.insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_' value='<%=vmFuelRequestItemDTO.insertedByOrganogramId%>' tag='pb_html'/>
											</td>
											<td style="display: none;">





														<input type='hidden' class='form-control'  name='vmFuelRequestItem.insertionDate' id = 'insertionDate_hidden_' value='<%=vmFuelRequestItemDTO.insertionDate%>' tag='pb_html'/>
											</td>
											<td style="display: none;">





														<input type='hidden' class='form-control'  name='vmFuelRequestItem.lastModificationTime' id = 'lastModificationTime_hidden_' value='<%=vmFuelRequestItemDTO.lastModificationTime%>' tag='pb_html'/>
											</td>
											<td style="display: none;">





														<input type='hidden' class='form-control'  name='vmFuelRequestItem.searchColumn' id = 'searchColumn_hidden_' value='<%=vmFuelRequestItemDTO.searchColumn%>' tag='pb_html'/>
											</td>
											<td style="display: none;">





														<input type='hidden' class='form-control'  name='vmFuelRequestItem.vmFuelRequestId' id = 'vmFuelRequestId_hidden_' value='<%=vmFuelRequestItemDTO.vmFuelRequestId%>' tag='pb_html'/>
											</td>
											<td>





																<select class='form-control'  name='vmFuelRequestItem.vehicleFuelCat' id = 'vehicleFuelCat_category_'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("vehicle_fuel", Language, vmFuelRequestItemDTO.vehicleFuelCat);
																%>
																<%=Options%>
																</select>
	
											</td>
											<td>





																<input type='text' class='form-control'  name='vmFuelRequestItem.requestedAmount' id = 'requestedAmount_text_' value='<%=vmFuelRequestItemDTO.requestedAmount%>' 																  tag='pb_html'/>					
											</td>
											<td>





																<input type='text' class='form-control'  name='vmFuelRequestItem.lastFuelAmount' id = 'lastFuelAmount_text_' value='<%=vmFuelRequestItemDTO.lastFuelAmount%>' 																  tag='pb_html'/>					
											</td>
											<td>





																<%value = "lastFuelDate_js_" + childTableStartingID;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='vmFuelRequestItem.lastFuelDate' id = 'lastFuelDate_date_' value= '<%=dateFormat.format(new Date(vmFuelRequestItemDTO.lastFuelDate))%>' tag='pb_html'>
											</td>
											<td style="display: none;">





														<input type='hidden' class='form-control'  name='vmFuelRequestItem.isDeleted' id = 'isDeleted_hidden_' value= '<%=vmFuelRequestItemDTO.isDeleted%>' tag='pb_html'/>
											
											</td>
											<td><div><span id='chkEdit' ><input type='checkbox' name='checkbox' value=''/></span></div></td>
										</tr>								
						
									</template>
                                </div>
                            </div>
                        </div>
                    </div>	
                       <div class="form-actions text-center mb-5">
	                        <a class="btn btn-danger" href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_CANCEL_BUTTON, loginDTO)%></a>										
							<button class="btn btn-success" type="submit"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_SUBMIT_BUTTON, loginDTO)%></button>
					   </div>
                   </form>
               </div>                      
          </div>
      </div>
 </div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, validate)
{


	preprocessDateBeforeSubmitting('applicationDate', row);
	preprocessDateBeforeSubmitting('lastFuelDate', row);
	preprocessDateBeforeSubmitting('approvedDate', row);

	for(i = 1; i < child_table_extra_id; i ++)
	{
		if(document.getElementById("lastFuelDate_date_" + i))
		{
			if(document.getElementById("lastFuelDate_date_" + i).getAttribute("processed") == null)
			{
				preprocessDateBeforeSubmitting('lastFuelDate', i);
				document.getElementById("lastFuelDate_date_" + i).setAttribute("processed","1");
			}
		}
	}
	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Vm_fuel_requestServlet");	
}

function init(row)
{

	setDateByStringAndId('applicationDate_js_' + row, $('#applicationDate_date_' + row).val());
	setDateByStringAndId('lastFuelDate_js_' + row, $('#lastFuelDate_date_' + row).val());
	setDateByStringAndId('approvedDate_js_' + row, $('#approvedDate_date_' + row).val());

	for(i = 1; i < child_table_extra_id; i ++)
	{
		setDateByStringAndId('lastFuelDate_js_' + i, $('#lastFuelDate_date_' + i).val());
	}
	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
});	

var child_table_extra_id = <%=childTableStartingID%>;

	$("#add-more-VmFuelRequestItem").click(
        function(e) 
		{
            e.preventDefault();
            var t = $("#template-VmFuelRequestItem");

            $("#field-VmFuelRequestItem").append(t.html());
			SetCheckBoxValues("field-VmFuelRequestItem");
			
			var tr = $("#field-VmFuelRequestItem").find("tr:last-child");
			
			tr.attr("id","VmFuelRequestItem_" + child_table_extra_id);
			
			tr.find("[tag='pb_html']").each(function( index ) 
			{
				var prev_id = $( this ).attr('id');
				$( this ).attr('id', prev_id + child_table_extra_id);
				console.log( index + ": " + $( this ).attr('id') );
			});
			
			setDateByStringAndId('lastFuelDate_js_' + child_table_extra_id, $('#lastFuelDate_date_' + child_table_extra_id).val());

			child_table_extra_id ++;

        });

    
      $("#remove-VmFuelRequestItem").click(function(e){    	
	    var tablename = 'field-VmFuelRequestItem';
	    var i = 0;
		console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
		var element = document.getElementById(tablename);

		var j = 0;
		for(i = document.getElementById(tablename).childNodes.length - 1; i >= 0 ; i --)
		{
			var tr = document.getElementById(tablename).childNodes[i];
			if(tr.nodeType === Node.ELEMENT_NODE)
			{
				console.log("tr.childNodes.length= " + tr.childNodes.length);
				var checkbox = tr.querySelector('input[type="checkbox"]');				
				if(checkbox.checked == true)
				{
					tr.remove();
				}
				j ++;
			}
			
		}    	
    });


</script>






