<%@page pageEncoding="UTF-8" %>

<%@page import="vm_fuel_request.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="files.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="vm_vehicle.Vm_vehicleDAO" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VM_FUEL_REQUEST_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_VM_FUEL_REQUEST;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Vm_fuel_requestDTO vm_fuel_requestDTO = (Vm_fuel_requestDTO) request.getAttribute("vm_fuel_requestDTO");
    CommonDTO commonDTO = vm_fuel_requestDTO;
    String servletName = "Vm_fuel_requestServlet";


    System.out.println("vm_fuel_requestDTO = " + vm_fuel_requestDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Vm_fuel_requestDAO vm_fuel_requestDAO = (Vm_fuel_requestDAO) request.getAttribute("vm_fuel_requestDAO");

    FilesDAO filesDAO = new FilesDAO();

    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    Employee_recordsDTO driver = Employee_recordsRepository.getInstance().getById(vm_fuel_requestDTO.vehicleDriverAssignmentId);
    String driverName = Language.equals("English") ? driver.nameEng : driver.nameBng;
%>


<td id='<%=i%>_cardInfoId'>
    <%
        Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().
                getVm_vehicleDTOByID(vm_fuel_requestDTO.vehicleId);

        String brand = "";
        String seats = "";
        String option = "";
        String numberOfSeats = "";
        String numOfSeatsStr = "";

        if(vm_vehicleDTO != null){
            brand = CatRepository.getName(Language, "vehicle_brand", vm_vehicleDTO.vehicleBrandCat);
            seats = Language.equals("English") ? "Number of seats: " : "আসন সংখ্যা: ";
            option = vm_vehicleDTO.regNo + " " + brand + " " + vm_vehicleDTO.modelNo;
            numberOfSeats = String.valueOf(vm_vehicleDTO.numberOfSeats);
            numOfSeatsStr = "(" + seats + numberOfSeats  + ")";
        }



        Options = Utils.getDigits(option + numOfSeatsStr, Language);
    %>
    <%=Options%>
</td>

<td id='<%=i%>_cardEmployeeId'>
    <%=driverName%>
</td>
<td id='<%=i%>_cardEmployeeImagesId'>
    <%
        value = vm_fuel_requestDTO.applicationDate + "";
    %>
    <%
        if (vm_fuel_requestDTO.applicationDate != -1 && vm_fuel_requestDTO.applicationDate != 0) {
            String formatted_lastFuelDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>

    <%=Utils.getDigits(formatted_lastFuelDate, Language)%>
    <%
        }
    %>
</td>

<td id='<%=i%>_cardApprovalStatusCat'>
    <span class="font-weight-bold"
          style="color: <%=ApprovalStatus.getColor(vm_fuel_requestDTO.status)%>;">
        <%=CatRepository.getInstance().getText(Language, "vm_fuel_request_approval_status", vm_fuel_requestDTO.status)%>
    </span>
</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Vm_fuel_requestServlet?actionType=view&ID=<%=vm_fuel_requestDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>

    <%
        if (vm_fuel_requestDTO.status == Vm_fuel_requestStatusEnum.WAITING_FOR_APPROVAL.getValue()) {
    %>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Vm_fuel_requestServlet?actionType=getEditPage&ID=<%=vm_fuel_requestDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
    <%
        }
    %>

</td>


<td id='<%=i%>_checkbox'>
    <%
        if (vm_fuel_requestDTO.status == Vm_fuel_requestStatusEnum.WAITING_FOR_APPROVAL.getValue()) {
    %>
    <div class='checker text-right'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=vm_fuel_requestDTO.iD%>'/></span>
    </div>
    <%
        }
    %>
</td>
																						
											

