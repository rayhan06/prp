<%@page pageEncoding="UTF-8" %>

<%@page import="vm_fuel_request.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="files.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="vm_vehicle.Vm_vehicleDAO" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VM_FUEL_REQUEST_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_VM_FUEL_REQUEST;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Vm_fuel_requestDTO vm_fuel_requestDTO = (Vm_fuel_requestDTO) request.getAttribute("vm_fuel_requestDTO");
    CommonDTO commonDTO = vm_fuel_requestDTO;
    String servletName = "Vm_fuel_requestServlet";


    System.out.println("vm_fuel_requestDTO = " + vm_fuel_requestDTO);


    double fuelRequestToPaidPrice = Double.parseDouble(request.getParameter("fuelRequestToPaidPrice")) * 1.075;
    double fuelRequestToPaidPriceAccumulated = Double.parseDouble(request.getParameter("fuelRequestToPaidPriceAccumulated")) * 1.075;

    fuelRequestToPaidPrice = (int) (Math.round(fuelRequestToPaidPrice * 100)) / 100.0;
    fuelRequestToPaidPriceAccumulated = (int) (Math.round(fuelRequestToPaidPriceAccumulated * 100)) / 100.0;

    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Vm_fuel_requestDAO vm_fuel_requestDAO = (Vm_fuel_requestDAO) request.getAttribute("vm_fuel_requestDAO");

    FilesDAO filesDAO = new FilesDAO();

    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_cardInfoId'>
    <%=Utils.getDigits(i + 1, Language)%>
</td>

<td id='<%=i%>_cardEmployeeId'>
    <%=Utils.getDigits(vm_fuel_requestDTO.sarokNumber, Language)%>
</td>
<td id='<%=i%>_cardEmployeeImagesId'>
    <%
        value = vm_fuel_requestDTO.lastModificationTime + "";
    %>
    <%
        String formatted_lastFuelDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>

    <%=Utils.getDigits(formatted_lastFuelDate, Language)%>
</td>

<td id='<%=i%>_cardApprovalStatusCat'>
    <%
        Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().
                getVm_vehicleDTOByID(vm_fuel_requestDTO.vehicleId);
        String option = "";
//		String brand = CatRepository.getInstance().getName(Language, "vehicle_brand", vm_vehicleDTO.vehicleBrandCat);
//		String seats = Language.equals("English") ? "Number of seats: " : "আসন সংখ্যা: ";
//		String option = vm_vehicleDTO.regNo + " " + brand + " " + vm_vehicleDTO.modelNo ;
        if(vm_vehicleDTO != null){
            option = vm_vehicleDTO.regNo;
        }

        Options = Utils.getDigits(option, Language);
    %>
    <%=Options%>
</td>


<td>
    <%=Utils.getDigits(0.0, Language)%>
</td>

<td id='<%=i%>_Edit'>
    <%=Utils.getDigits(fuelRequestToPaidPrice, Language)%>
</td>


<td id='<%=i%>_checkbox'>
    <%=Utils.getDigits(fuelRequestToPaidPriceAccumulated, Language)%>
</td>


<td>
    <%=Utils.getDigits(0.0, Language)%>
</td>
											

