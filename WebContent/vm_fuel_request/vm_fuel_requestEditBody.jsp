<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_fuel_request.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="vm_fuel_vendor.Vm_fuel_vendorDAO" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_assign.EmployeeSearchModel" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="employee_assign.EmployeeSearchIds" %>
<%@ page import="util.UtilCharacter" %>

<%
    Vm_fuel_requestDTO vm_fuel_requestDTO;
    vm_fuel_requestDTO = (Vm_fuel_requestDTO) request.getAttribute("vm_fuel_requestDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    EmployeeSearchModel own = new Gson().fromJson(EmployeeSearchModalUtil.getEmployeeSearchModelJson(userDTO.employee_record_id, userDTO.unitID, userDTO.organogramID), EmployeeSearchModel.class);
    if (vm_fuel_requestDTO == null) {
        vm_fuel_requestDTO = new Vm_fuel_requestDTO();

    }
    System.out.println("vm_fuel_requestDTO = " + vm_fuel_requestDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ADD_FORMNAME, loginDTO);
    String servletName = "Vm_fuel_requestServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID = -1;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_FUEL_REQUEST_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    List<CategoryLanguageModel> fuelList = CatRepository.getInstance().getCategoryLanguageModelList("vehicle_fuel");
    HashMap<Integer, String> fuelNameMap = new HashMap<>();
    fuelList
            .forEach(fuel -> {
                fuelNameMap.put(fuel.categoryValue, Language.equals("English") ? fuel.englishText : fuel.banglaText);
            });

%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Vm_fuel_requestServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD'
                                           id='iD_hidden_<%=i%>' value='<%=vm_fuel_requestDTO.iD%>'
                                           tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=vm_fuel_requestDTO.insertedByUserId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=vm_fuel_requestDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=vm_fuel_requestDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=vm_fuel_requestDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=vm_fuel_requestDTO.searchColumn%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REQUESTERNAMEBN, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <input type="hidden" class='form-control'
                                                   name='employeeRecordsId' id='employeeRecordsId'
                                                   value=''>
                                            <button type="button"
                                                    class="btn btn-primary btn-block shadow btn-border-radius mb-3"
                                                    id="tagRequester_modal_button">
                                                <%=LM.getText(LC.HM_SELECT, userDTO)%>
                                            </button>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped text-nowrap">
                                                    <thead></thead>
                                                    <tbody id="tagged_requester_table">
                                                    <tr style="display: none;">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <button type="button"
                                                                    class="btn cancel-btn text-white shadow btn-border-radius pl-4"
                                                                    style="margin-right: 14px"
                                                                    onclick="remove_containing_row(this,'tagged_requester_table');">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>

                                                    <% if (actionName.equals("edit")) { %>
                                                    <tr>
                                                        <td><%=Employee_recordsRepository.getInstance()
                                                                .getById(vm_fuel_requestDTO.requesterEmpId).employeeNumber%>
                                                        </td>
                                                        <td>
                                                            <%=isLanguageEnglish ? (vm_fuel_requestDTO.requesterNameEn)
                                                                    : (vm_fuel_requestDTO.requesterNameBn)%>
                                                        </td>

                                                        <%
                                                            String postName = isLanguageEnglish ? (vm_fuel_requestDTO.requesterOfficeUnitOrgNameEn + ", " + vm_fuel_requestDTO.requesterOfficeUnitNameEn)
                                                                    : (vm_fuel_requestDTO.requesterOfficeUnitOrgNameBn + ", " + vm_fuel_requestDTO.requesterOfficeUnitNameBn);
                                                        %>

                                                        <td><%=postName%>
                                                        </td>
                                                        <td id='<%=vm_fuel_requestDTO.requesterEmpId%>_td_button'>
                                                            <button type="button"
                                                                    class="btn  btn-sm delete-trash-btn"
                                                                    style="margin-right: 14px"
                                                                    onclick="remove_containing_row(this,'tagged_requester_table');">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <%} else {%>
                                                    <tr>
                                                        <td><%=own.employeeRecordId%>
                                                        </td>
                                                        <td>
                                                            <%=isLanguageEnglish ? (own.employeeNameEn)
                                                                    : (own.employeeNameBn)%>
                                                        </td>

                                                        <%
                                                            String postName = isLanguageEnglish ? (own.organogramNameEn + ", " + own.officeUnitNameEn)
                                                                    : (own.organogramNameBn + ", " + own.officeUnitNameBn);
                                                        %>

                                                        <td><%=postName%>
                                                        </td>
                                                        <td id='<%=own.employeeRecordId%>_td_button'>
                                                            <button type="button"
                                                                    class="btn btn-sm delete-trash-btn"
                                                                    style="margin-right: 14px"
                                                                    onclick="remove_containing_row(this,'tagged_requester_table');">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <%
                                                        }
                                                    %>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VEHICLETYPE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='vehicleType'
                                                    id='vehicleType_select_<%=i%>' tag='pb_html'
                                                    onchange="loadVehicleList()">
                                                <%
                                                    Options = CatRepository.getOptions(Language, "vehicle_type", vm_fuel_requestDTO.vehicleType);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row" id="vehicleIdDiv">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VEHICLEID, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='vehicleId' onchange="showFuelType()"
                                                    id='vehicleId_select_<%=i%>' tag='pb_html'>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row" id="vehicleIdDiv">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "জ্বালানীর ধরন", "Fuel Type")%>
                                        </label>
                                        <div class="col-md-9">
                                            <input class='form-control' type="text" id="fuelType" name="fuelType" value="" readonly>
                                            <input hidden class='form-control' type="text" id="fuelValue" name="fuelValue" value="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_APPLICATIONDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%value = "applicationDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID"
                                                           value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='applicationDate'
                                                   id='applicationDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(vm_fuel_requestDTO.applicationDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <%--													<div class="form-group row">--%>
                                    <%--														<label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VENDORID, loginDTO)%></label>--%>
                                    <%--														<div class="col-md-9">--%>
                                    <%--															<select class='form-control'  name='vendorId' id = 'vendorId_select_<%=i%>'   tag='pb_html'>--%>
                                    <%--																<%--%>
                                    <%--																	Vm_fuel_vendorDAO vm_fuel_vendorDAO = new Vm_fuel_vendorDAO();--%>
                                    <%--																	Options = vm_fuel_vendorDAO.getOptions(Language, userDTO, vm_fuel_requestDTO.vendorId);--%>
                                    <%--																%>--%>
                                    <%--																<%=Options%>--%>
                                    <%--															</select>--%>

                                    <%--														</div>--%>
                                    <%--													</div>--%>

                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value='<%=vm_fuel_requestDTO.isDeleted%>' tag='pb_html'/>
                                    <input type='text' style='display:none' class='form-control'
                                           name='requesterEmpId' id='requesterEmpId_hidden_<%=i%>'
                                           value='<%=vm_fuel_requestDTO.requesterEmpId%>'
                                           tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5">
                    <h5 class="table-title">
                        <%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM_VEHICLEFUELCAT, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM_REQUESTEDAMOUNT, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM_VEHICLEFUELCAT, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM_REQUESTEDAMOUNT, loginDTO)%>
                                </th>
                                <%--													<th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM_LASTFUELAMOUNT, loginDTO)%></th>--%>
                                <%--													<th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM_LASTFUELDATE, loginDTO)%></th>--%>
                                <%--													<th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_ITEM_REMOVE, loginDTO)%></th>--%>
                            </tr>
                            </thead>
                            <tbody id="field-VmFuelRequestItem">


                            <%
                                if (actionName.equals("edit")) {
                                    int index = -2;

                                    int fuelListSize = vm_fuel_requestDTO.vmFuelRequestItemDTOList.size();

                                    while (index + 2 < fuelListSize) {
                                        index += 2;

                            %>

                            <tr id="VmFuelRequestItem_<%=index + 1%>">
                                <td>

                                    <%
                                        if (index < fuelListSize) {
                                            VmFuelRequestItemDTO vmFuelRequestItemDTO = vm_fuel_requestDTO.vmFuelRequestItemDTOList.get(index);

                                            System.out.println("index index = " + index);
                                    %>

                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.isDeleted'
                                           id='isDeleted_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.isDeleted%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.iD'
                                           id='iD_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.iD%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.insertedByUserId'
                                           id='insertedByUserId_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.insertedByUserId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.insertionDate'
                                           id='insertionDate_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.insertionDate%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.lastFuelAmount'
                                           id='lastFuelAmount_text_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.lastFuelAmount%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.lastFuelDate'
                                           id='lastFuelDate_date_<%=childTableStartingID%>'
                                           value='<%=dateFormat.format(new Date(vmFuelRequestItemDTO.lastFuelDate))%>'
                                           tag='pb_html'>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.lastModificationTime'
                                           id='lastModificationTime_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.searchColumn'
                                           id='searchColumn_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.searchColumn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.vmFuelRequestId'
                                           id='vmFuelRequestId_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.vmFuelRequestId%>'
                                           tag='pb_html'/>

                                    <input type='hidden' name='vmFuelRequestItem.vehicleFuelCat'
                                           id='vehicleFuelCat_text_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.vehicleFuelCat%>'
                                           tag='pb_html'/>
                                    <input readonly type='text' class='form-control'
                                           value='<%=fuelNameMap.getOrDefault(vmFuelRequestItemDTO.vehicleFuelCat, "")%>'
                                           tag='pb_html'/>

                                    <%
                                        }
                                    %>

                                </td>
                                <td>

                                    <%
                                        if (index < fuelListSize) {
                                            VmFuelRequestItemDTO vmFuelRequestItemDTO = vm_fuel_requestDTO.vmFuelRequestItemDTOList.get(index);
                                            CategoryLanguageModel fuel = fuelList.get(index);
                                            String fuelName = fuel.englishText;
                                    %>

                                    <input type='number' class='form-control'
                                           name='vmFuelRequestItem.requestedAmount'
                                           id='requestedAmount_text_<%=fuelName%>'
                                           value='<%=vmFuelRequestItemDTO.requestedAmount%>'
                                           tag='pb_html'/>

                                    <%
                                            childTableStartingID++;
                                        }
                                    %>
                                </td>

                                <td>

                                    <%
                                        if (index + 1 < fuelListSize) {
                                            VmFuelRequestItemDTO vmFuelRequestItemDTO = vm_fuel_requestDTO.vmFuelRequestItemDTOList.get(index + 1);

                                            System.out.println("index index = " + index);
                                    %>

                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.isDeleted'
                                           id='isDeleted_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.isDeleted%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.iD'
                                           id='iD_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.iD%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.insertedByUserId'
                                           id='insertedByUserId_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.insertedByUserId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.insertionDate'
                                           id='insertionDate_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.insertionDate%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.lastFuelAmount'
                                           id='lastFuelAmount_text_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.lastFuelAmount%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.lastFuelDate'
                                           id='lastFuelDate_date_<%=childTableStartingID%>'
                                           value='<%=dateFormat.format(new Date(vmFuelRequestItemDTO.lastFuelDate))%>'
                                           tag='pb_html'>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.lastModificationTime'
                                           id='lastModificationTime_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.searchColumn'
                                           id='searchColumn_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.searchColumn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.vmFuelRequestId'
                                           id='vmFuelRequestId_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.vmFuelRequestId%>'
                                           tag='pb_html'/>

                                    <input type='hidden' name='vmFuelRequestItem.vehicleFuelCat'
                                           id='vehicleFuelCat_text_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.vehicleFuelCat%>'
                                           tag='pb_html'/>
                                    <input readonly type='text' class='form-control'
                                           value='<%=fuelNameMap.getOrDefault(vmFuelRequestItemDTO.vehicleFuelCat, "")%>'
                                           tag='pb_html'/>

                                    <%
                                        }
                                    %>

                                </td>
                                <td>

                                    <%
                                        if (index < fuelListSize) {
                                            VmFuelRequestItemDTO vmFuelRequestItemDTO = vm_fuel_requestDTO.vmFuelRequestItemDTOList.get(index + 1);
                                            CategoryLanguageModel fuel = fuelList.get(index+1);
                                            String fuelName = fuel.englishText;
                                    %>

                                    <input type='number' class='form-control'
                                           name='vmFuelRequestItem.requestedAmount'
                                           id='requestedAmount_text_<%=fuelName%>'
                                           value='<%=vmFuelRequestItemDTO.requestedAmount%>'
                                           tag='pb_html'/>

                                    <%
                                            childTableStartingID++;
                                        }
                                    %>
                                </td>
                            </tr>
                            <%
                                }
                            } else {
                                int index = -2;


                                int fuelListSize = fuelList.size();

                                while (index + 2 < fuelListSize) {
                                    index += 2;


                            %>

                            <tr id="VmFuelRequestItem_<%=index + 1%>">
                                <td>

                                    <%
                                        if (index < fuelListSize) {
                                            CategoryLanguageModel fuel = fuelList.get(index);

                                            String fuelName = Language.equals("English") ? fuel.englishText : fuel.banglaText;

                                            VmFuelRequestItemDTO vmFuelRequestItemDTO = new VmFuelRequestItemDTO();
                                            System.out.println("index index = " + index);
                                    %>

                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.iD'
                                           id='iD_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.iD%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.insertedByUserId'
                                           id='insertedByUserId_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.insertedByUserId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.insertionDate'
                                           id='insertionDate_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.insertionDate%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.lastFuelAmount'
                                           id='lastFuelAmount_text_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.lastFuelAmount%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.lastFuelDate'
                                           id='lastFuelDate_date_<%=childTableStartingID%>'
                                           value='<%=dateFormat.format(new Date(vmFuelRequestItemDTO.lastFuelDate))%>'
                                           tag='pb_html'>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.lastModificationTime'
                                           id='lastModificationTime_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.searchColumn'
                                           id='searchColumn_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.searchColumn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.vmFuelRequestId'
                                           id='vmFuelRequestId_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.vmFuelRequestId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.isDeleted'
                                           id='isDeleted_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.isDeleted%>' tag='pb_html'/>


                                    <input type='hidden' name='vmFuelRequestItem.vehicleFuelCat'
                                           id='vehicleFuelCat_text_<%=childTableStartingID%>'
                                           value='<%=fuel.categoryValue%>' tag='pb_html'/>
                                    <input readonly type='text' class='form-control'
                                           value='<%=fuelName%>' tag='pb_html'/>

                                    <%
                                        }
                                    %>

                                </td>
                                <td>
                                    <%
                                        if (index < fuelListSize) {
                                            CategoryLanguageModel fuel = fuelList.get(index);
                                            String fuelName = fuel.englishText;
                                    %>

                                    <input type='number' class='form-control'
                                           name='vmFuelRequestItem.requestedAmount'
                                           id='requestedAmount_text_<%=fuelName%>' value='0'
                                           tag='pb_html'/>
                                    <%
                                            childTableStartingID++;
                                        }
                                    %>
                                </td>


                                <td>

                                    <%
                                        if (index + 1 < fuelListSize) {
                                            CategoryLanguageModel fuel = fuelList.get(index + 1);

                                            String fuelName = Language.equals("English") ? fuel.englishText : fuel.banglaText;

                                            VmFuelRequestItemDTO vmFuelRequestItemDTO = new VmFuelRequestItemDTO();
                                            System.out.println("index index = " + index);
                                    %>

                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.iD'
                                           id='iD_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.iD%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.insertedByUserId'
                                           id='insertedByUserId_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.insertedByUserId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.insertionDate'
                                           id='insertionDate_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.insertionDate%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.lastFuelAmount'
                                           id='lastFuelAmount_text_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.lastFuelAmount%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.lastFuelDate'
                                           id='lastFuelDate_date_<%=childTableStartingID%>'
                                           value='<%=dateFormat.format(new Date(vmFuelRequestItemDTO.lastFuelDate))%>'
                                           tag='pb_html'>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.lastModificationTime'
                                           id='lastModificationTime_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.searchColumn'
                                           id='searchColumn_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.searchColumn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.vmFuelRequestId'
                                           id='vmFuelRequestId_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.vmFuelRequestId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='vmFuelRequestItem.isDeleted'
                                           id='isDeleted_hidden_<%=childTableStartingID%>'
                                           value='<%=vmFuelRequestItemDTO.isDeleted%>' tag='pb_html'/>


                                    <input type='hidden' name='vmFuelRequestItem.vehicleFuelCat'
                                           id='vehicleFuelCat_text_<%=childTableStartingID%>'
                                           value='<%=fuel.categoryValue%>' tag='pb_html'/>
                                    <input readonly type='text' class='form-control'
                                           value='<%=fuelName%>' tag='pb_html'/>

                                    <%
                                        }
                                    %>

                                </td>
                                <td>
                                    <%
                                        if (index < fuelListSize) {
                                            CategoryLanguageModel fuel = fuelList.get(index+1);

                                            String fuelName = fuel.englishText;
                                    %>

                                    <input type='number' class='form-control'
                                           name='vmFuelRequestItem.requestedAmount'
                                           id='requestedAmount_text_<%=fuelName%>' value='0'
                                           tag='pb_html'/>
                                    <%
                                            childTableStartingID++;
                                        }
                                    %>
                                </td>

                            </tr>
                            <%
                                    }
                                }
                            %>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-actions text-right mt-3">
                    <a class="btn btn-sm cancel-btn text-white shadow btn-border-radius"
                       href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_CANCEL_BUTTON, loginDTO)%>
                    </a>
                    <button class="btn  btn-sm submit-btn text-white shadow btn-border-radius ml-2"
                            onclick="event.preventDefault();submitBigForm()"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">

    function loadVehicleList() {

        var value = $("#vehicleType_select_0").val();
        let url = "Vm_vehicleServlet?actionType=getAllByVehicleType&ID=" + value + "&vehicleId=" + '<%=vm_fuel_requestDTO.vehicleId%>';
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                // const response = JSON.parse(fetchedData);
                $("#vehicleId_select_0").html(fetchedData);
            },
            error: function (error) {
                console.log(error);
            }
        });

    }

    function makeNonEditable(){
        try{
            document.querySelector("#requestedAmount_text_Petrol").readOnly = true;
            document.querySelector("#requestedAmount_text_Gas").readOnly = true;
            document.querySelector("#requestedAmount_text_Octane").readOnly = true;
            document.querySelector("#requestedAmount_text_Diesel").readOnly = true;
        }catch (e) {
            console.debug(e);
        }
    }

    function allowAuthorisedFuelToEdit(fuelType){
        try{
            if(fuelType === 'Petrol' || fuelType === 'পেট্রোল'){
                document.querySelector("#requestedAmount_text_Petrol").readOnly = false;
            }
            if(fuelType === 'Gas' || fuelType === 'গ্যাস'){
                document.querySelector("#requestedAmount_text_Gas").readOnly = false;
            }
            if(fuelType === 'Octane' || fuelType === 'অকটেন'){
                document.querySelector("#requestedAmount_text_Octane").readOnly = false;
            }
            if(fuelType === 'Diesel' || fuelType === 'ডিজেল'){
                document.querySelector("#requestedAmount_text_Diesel").readOnly = false;
            }
        }catch (e) {
            console.debug(e);
        }
    }

    function showFuelType(){
        let vehicleId = $("#vehicleId_select_0").val();
        let url = "Vm_vehicleServlet?actionType=getVehicleFuelType&vehicleId=" + vehicleId;
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                const fuel = JSON.parse(fetchedData);
                console.debug("show fuel type: ", fetchedData)
                $("#fuelType").val(fuel.name);
                $("#fuelValue").val(fuel.value);
                allowAuthorisedFuelToEdit(fuel.name.trim());
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function processResponse(data) {

        if (data.includes('No driver')) {
            var alertMsg = "<%=Language.equals("English") ? "Sorry, No driver found for this vehicle!" : "দুঃখিত, এই গাড়ির কোনো চালক পাওয়া যায়নি "%>";
            bootbox.alert(alertMsg
                , function (result) {
                });
        } else if (data.includes('Invalid Input')) {
            var alertMsg = "<%=Language.equals("English") ? "Sorry, missing required field!" : "দুঃখিত, আবশ্যকীয় তথ্য প্রদান করুন "%>";
            bootbox.alert(alertMsg
                , function (result) {
                });
        } else {
            window.location = 'Vm_fuel_requestServlet?actionType=search';
        }
    }

    function ajaxPost(url, accept, send, postData, onSuccess, onError) {
        $.ajax({
            type: "POST",
            url: url,
            dataType: accept,
            contentType: send,
            data: postData,
            success: onSuccess,
            error: onError,
            complete: function () {
                // $.unblockUI();
            }
        });
    }


    function submitBigForm() {

        if (PreprocessBeforeSubmiting(0, '<%=actionName%>')) {
            var form = $("#bigform");

            var actionUrl = form.attr("action");
            var postData = (form.serialize());

            ajaxPost(actionUrl, "json", "application/x-www-form-urlencoded", postData, processResponse, processResponse);
        }


    }

    function PreprocessBeforeSubmiting(row, validate) {

        preprocessDateBeforeSubmitting('applicationDate', row);

        var vehicle_id = $("#vehicleId_select_0").val();
        if (!(vehicle_id != undefined && vehicle_id != null && vehicle_id != -1)) {
            toastr.error(
                "<%=Language.equals("English") ? "Please Select vehicle" : "দয়া করে গাড়ি সিলেক্ট করুন"%>"
            );
            return false;
        }

        let data = added_requester_map.keys().next();
        if (!(data && data.value)) {
            toastr.error(
                "<%=Language.equals("English") ? "Please Select requester" : "দয়া করে অনুরোধকারী সিলেক্ট করুন"%>"
            );
            return false;
        }
        document.getElementById('requesterEmpId_hidden_0').value = JSON.stringify(
            Array.from(added_requester_map.values()));

        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Vm_fuel_requestServlet");
    }

    function init(row) {

        setDateByStringAndId('applicationDate_js_' + row, $('#applicationDate_date_' + row).val());
        loadVehicleList();
    }

    function convertToSelect2(){
        select2SingleSelector("#vehicleType_select_<%=i%>", '<%=Language%>');
        select2SingleSelector("#vehicleId_select_<%=i%>", '<%=Language%>');
    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        makeNonEditable();
        convertToSelect2();
        CKEDITOR.replaceAll();
    });

    var child_table_extra_id = <%=childTableStartingID%>;


    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button

    // map to store and send added employee data
    // map to store and send added employee data


    <%
    List<EmployeeSearchIds> addedEmpIdsList = null;
    if(actionName.equals("edit")) {
        addedEmpIdsList = Arrays.asList(new EmployeeSearchIds
        (vm_fuel_requestDTO.requesterEmpId,vm_fuel_requestDTO.requesterOfficeUnitId,vm_fuel_requestDTO.requesterOrgId));
    }
    else {
        addedEmpIdsList = Arrays.asList(new EmployeeSearchIds
        (own.employeeRecordId,own.officeUnitId,own.organogramId));
    }
    %>

    added_requester_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(addedEmpIdsList)%>);


    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_requester_table', {
                info_map: added_requester_map,
                isSingleEntry: true
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagRequester_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_requester_table';
        $('#search_emp_modal').modal();
    });


</script>


<style>
    .required {
        color: red;
    }
</style>






