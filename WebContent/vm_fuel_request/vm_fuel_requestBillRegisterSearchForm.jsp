<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_fuel_request.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.text.SimpleDateFormat" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="vm_fuel_vendor.VmFuelVendorItemDTO" %>
<%@ page import="vm_fuel_vendor.VmFuelVendorItemDAO" %>
<%@ page import="java.util.*" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.VM_FUEL_REQUEST_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    Vm_fuel_requestDAO vm_fuel_requestDAO = (Vm_fuel_requestDAO) request.getAttribute("vm_fuel_requestDAO");


    String navigator2 = SessionConstants.NAV_VM_FUEL_REQUEST;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<%

    if (hasAjax == false) {
        Enumeration<String> parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {

            String paramName = parameterNames.nextElement();

            if (!paramName.equalsIgnoreCase("actionType")) {
                String[] paramValues = request.getParameterValues(paramName);
                for (int i = 0; i < paramValues.length; i++) {
                    String paramValue = paramValues[i];

%>

<%

                }
            }


        }
    }

%>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_ID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_SAROKNUMBER, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_LASTMODIFICATIONTIME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VEHICLEID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_ALLOCATED, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_TOTAL_COST, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_TOTAL_EXPENSES, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_REMAINING, loginDTO)%>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            List<Vm_fuel_requestDTO> data = (List<Vm_fuel_requestDTO>) session.getAttribute(SessionConstants.VIEW_VM_FUEL_REQUEST);

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);

                    HashMap<Long, List<VmFuelVendorItemDTO>> vendorIdToItems = new HashMap<>();
                    HashMap<Long, Double> fuelRequestToPaidPrice = new HashMap<>();
                    HashMap<Long, Double> fuelRequestToPaidPriceAccumulated = new HashMap<>();
                    HashMap<Long, Double> vehicleToPaidPriceAccumulated = new HashMap<>();
                    StringBuilder filterBuilder = new StringBuilder();

                    data
                            .forEach(d -> {
                                filterBuilder.append("," + d.vendorId);
                            });

                    filterBuilder.replace(0, 1, "(");
                    filterBuilder.append(")");

                    String filterFuelRequest = " vm_fuel_vendor_id in " + filterBuilder;
                    List<VmFuelVendorItemDTO> vmFuelVendorItemDTOS = new VmFuelVendorItemDAO().getDTOs(null, -1, -1, true, userDTO, filterFuelRequest, false);

                    vmFuelVendorItemDTOS
                            .forEach(vmFuelVendorItemDTO -> {
                                List<VmFuelVendorItemDTO> existingItems = vendorIdToItems.getOrDefault(vmFuelVendorItemDTO.vmFuelVendorId, new ArrayList<>());
                                existingItems.add(vmFuelVendorItemDTO);
                                vendorIdToItems.put(vmFuelVendorItemDTO.vmFuelVendorId, existingItems);
                            });

                    data
                            .stream()
                            .forEach(d -> {
                                List<VmFuelVendorItemDTO> vendorItemDTOS = vendorIdToItems.getOrDefault(d.vendorId, new ArrayList<>());

                                HashMap<Integer, Double> fuelUnitPriceMap = new HashMap<>();
                                vendorItemDTOS
                                        .forEach(vendorItemDTO -> {
                                            fuelUnitPriceMap.put(vendorItemDTO.vehicleFuelCat, vendorItemDTO.price);
                                        });

                                List<VmFuelRequestItemDTO> vmFuelRequestItemDTOS = d.vmFuelRequestItemDTOList;

                                double totalPriceAtomic = 0.0;
                                for (VmFuelRequestItemDTO vmFuelRequestItemDTO : vmFuelRequestItemDTOS) {
                                    double price = fuelUnitPriceMap.getOrDefault(vmFuelRequestItemDTO.vehicleFuelCat, 0.0) * vmFuelRequestItemDTO.requestedAmount;
                                    totalPriceAtomic = (totalPriceAtomic + price);
                                }

                                fuelRequestToPaidPrice.put(d.iD, totalPriceAtomic);

                                double totalPrice = vehicleToPaidPriceAccumulated.getOrDefault(d.vehicleId, 0.0).doubleValue() + totalPriceAtomic;
                                fuelRequestToPaidPriceAccumulated.put(d.iD, totalPrice);
                                vehicleToPaidPriceAccumulated.put(d.vehicleId, totalPrice);
                            });


                    for (int i = 0; i < size; i++) {
                        Vm_fuel_requestDTO vm_fuel_requestDTO = data.get(i);


        %>
        <tr id='tr_<%=i%>'>
            <%

            %>


            <%
                request.setAttribute("vm_fuel_requestDTO", vm_fuel_requestDTO);
            %>

            <jsp:include page="./vm_fuel_requestBillRegisterSearchRow.jsp">
                <jsp:param name="pageName" value="searchrow"/>
                <jsp:param name="rownum" value="<%=i%>"/>
                <jsp:param name="fuelRequestToPaidPrice"
                           value="<%=fuelRequestToPaidPrice.getOrDefault(vm_fuel_requestDTO.iD, 0.0)%>"/>
                <jsp:param name="fuelRequestToPaidPriceAccumulated"
                           value="<%=fuelRequestToPaidPriceAccumulated.getOrDefault(vm_fuel_requestDTO.iD, 0.0)%>"/>
            </jsp:include>


            <%

            %>
        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			