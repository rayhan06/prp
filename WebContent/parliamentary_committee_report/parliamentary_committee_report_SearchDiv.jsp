<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="committees.CommitteesRepository" %>
<%

    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.PARLIAMENTARY_COMMITTEE_REPORT_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    boolean isLangEng = Language.equalsIgnoreCase("English");
%>

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>
<div class="row mx-2 mx-md-3">
    <div class="col-12">
        <div class="row">
            <div class="search-criteria-div col-md-6 ">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">
                        <%=LM.getText(LC.PARLIAMENTARY_COMMITTEE_REPORT_WHERE_COMMITTEESID, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control rounded' name='committeesId' id='committeesId' tag='pb_html'
                                onchange="changeElection()">
                            <%=CommitteesRepository.getInstance().buildOptions(Language, null)%>
                        </select>
                    </div>
                </div>
            </div>
            <div class="search-criteria-div col-md-6 ">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">
                        <%=LM.getText(LC.PARLIAMENTARY_COMMITTEE_REPORT_WHERE_ELECTIONDETAILSID, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control rounded' name='electionDetailsId' id='electionDetailsId'
                                tag='pb_html'
                                onchange="changeElection()">
                            <%=Election_detailsRepository.getInstance().buildOptions(Language, null)%>
                        </select>
                    </div>
                </div>
            </div>
            <div class="search-criteria-div col-md-6 ">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">
                        <%=LM.getText(LC.PARLIAMENTARY_COMMITTEE_REPORT_WHERE_ELECTIONCONSTITUENCYID, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control rounded' name='electionConstituencyId' id='electionConstituencyId'
                                tag='pb_html'></select>
                    </div>
                </div>
            </div>
            <div id="name_eng_div" class="search-criteria-div col-md-6 employeeNameClass"
            >
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="nameEng">
                        <%=isLangEng ? "Name(English)" : "নাম(ইংরেজি)"%>
                    </label>
                    <div class="col-md-9">
                        <input class="englishOnly form-control" type="text" name="nameEng" id="nameEng"
                               style="width: 100%"
                               placeholder="<%=isLangEng ? "Enter Employee Name in English" : "কর্মকর্তা/কর্মচারীর নাম ইংরেজিতে লিখুন"%>"
                               value="">
                    </div>
                </div>
            </div>

            <div id="name_bng_div" class="search-criteria-div col-md-6 employeeNameClass"
            >
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="nameBng">
                        <%=isLangEng ? "Name(Bangla)" : "নাম(বাংলা)"%>
                    </label>
                    <div class="col-md-9">
                        <input class="noEnglish form-control" type="text" name="nameBng" id="nameBng"
                               style="width: 100%"
                               placeholder="<%=isLangEng ? "Enter Employee Name in Bangla" : "কর্মকর্তা/কর্মচারীর নাম বাংলাতে লিখুন"%>"
                               value="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(() => {
        showFooter = false;

        init();
    });

    function init() {
        dateTimeInit($("#Language").val());
        select2SingleSelector('#electionDetailsId', '<%=Language%>');
        select2SingleSelector('#electionConstituencyId', '<%=Language%>');
        select2SingleSelector('#committeesId', '<%=Language%>');
    }

    function PreprocessBeforeSubmiting() {
    }

    function changeElection() {
        document.getElementById("electionConstituencyId").innerHTML = '';
        electionDetailsId = $('#electionDetailsId').val();
        if (!electionDetailsId) {
            return;
        }
        let url = "Election_constituencyServlet?actionType=buildElectionConstituency&election_id=" + electionDetailsId + "&language=" + language;
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                console.log(fetchedData);
                document.getElementById("electionConstituencyId").innerHTML = fetchedData;
                select2SingleSelector('#electionConstituencyId', '<%=Language%>');
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
</script>