<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="medical_inventory_lot.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page contentType="text/html;charset=utf-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.MEDICAL_INVENTORY_LOT_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    Medical_inventory_lotDAO medical_inventory_lotDAO = (Medical_inventory_lotDAO) request.getAttribute("medical_inventory_lotDAO");


    String navigator2 = SessionConstants.NAV_MEDICAL_INVENTORY_LOT;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>
<%

    if (hasAjax == false) {
        Enumeration<String> parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {

            String paramName = parameterNames.nextElement();

            if (!paramName.equalsIgnoreCase("actionType")) {
                String[] paramValues = request.getParameterValues(paramName);
                for (int i = 0; i < paramValues.length; i++) {
                    String paramValue = paramValues[i];

%>

<%

                }
            }


        }
    }

%>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
        	<th><%=LM.getText(LC.HM_SL, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_PARLIAMENTSUPPLIERTYPE, loginDTO)%>
            </th>
           
            <th><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_STOCKLOTNUMBER, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_REMARKS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_INSERTED_BY, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_INSERTION_DATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_RETURN_SEARCH_PI_RETURN_EDIT_BUTTON, loginDTO)%></th>
            <th><%=Language.equalsIgnoreCase("english")?"Add To Stock":"স্টকে নিন"%></th>
        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_MEDICAL_INVENTORY_LOT);

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Medical_inventory_lotDTO medical_inventory_lotDTO = (Medical_inventory_lotDTO) data.get(i);


        %>
        <tr id='tr_<%=i%>'>
            <%

            %>
			<td>
				<%=Utils.getDigits(i + 1 + ((rn2.getCurrentPageNo() - 1) * rn2.getPageSize()), Language) %>
			</td>

            <%
                request.setAttribute("medical_inventory_lotDTO", medical_inventory_lotDTO);
            %>

            <jsp:include page="./medical_inventory_lotSearchRow.jsp">
                <jsp:param name="pageName" value="searchrow"/>
                <jsp:param name="rownum" value="<%=i%>"/>
            </jsp:include>


            <%

            %>
        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


<script type ="text/javascript">
function addToStock(id)
{
	var result = confirm("Add to stock?");
	if (result) {
		location.href='Medical_inventory_lotServlet?actionType=finalize&ID=' + id;
	}
}
</script>

			