<%@page pageEncoding="UTF-8" %>

<%@page import="medical_inventory_lot.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.MEDICAL_INVENTORY_LOT_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_MEDICAL_INVENTORY_LOT;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Medical_inventory_lotDTO medical_inventory_lotDTO = (Medical_inventory_lotDTO) request.getAttribute("medical_inventory_lotDTO");
    CommonDTO commonDTO = medical_inventory_lotDTO;
    String servletName = "Medical_inventory_lotServlet";


    System.out.println("medical_inventory_lotDTO = " + medical_inventory_lotDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Medical_inventory_lotDAO medical_inventory_lotDAO = (Medical_inventory_lotDAO) request.getAttribute("medical_inventory_lotDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    MedicalInventoryInDAO medicalInventoryInDAO = new MedicalInventoryInDAO();
%>

<td id='<%=i%>_parliamentSupplierType'>
    <%
        value = medical_inventory_lotDTO.parliamentSupplierType + "";
    %>
    <%
        value = CommonDAO.getName(Integer.parseInt(value), "parliament_supplier", Language.equals("English") ? "name_en" : "name_bn", "id");
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_stockLotNumber'>
    <%
        value = medical_inventory_lotDTO.stockLotNumber + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_remarks'>
    <%
        value = medical_inventory_lotDTO.remarks + "";
    %>

    <%=Utils.getDigits(value, Language)%>

</td>
<td>
	<table class="table table-bordered table-striped text-nowrap">
	<%
	List<MedicalInventoryInDTO> medicalInventoryInDTOs = medicalInventoryInDAO.getMedicalInventoryInDTOListByMedicalInventoryLotID(medical_inventory_lotDTO.iD);
	if(medicalInventoryInDTOs != null)
	{
		for(MedicalInventoryInDTO medicalInventoryInDTO: medicalInventoryInDTOs)
		{
			%>
			<tr>
			    <td >

                     <%
                         if (medicalInventoryInDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_DRUG) {
                             value = CommonDAO.getName((int) medicalInventoryInDTO.drugInformationType, "drug_information", Language.equals("English") ? "name_en" : "name_bn", "id");
                         } else if (medicalInventoryInDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_REAGENT) {
                             value = CommonDAO.getName((int) medicalInventoryInDTO.medicalReagentNameType, "medical_reagent_name", Language.equals("English") ? "name_en" : "name_bn", "id");
                         } else if (medicalInventoryInDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_EQUIPMENT) {
                             value = CommonDAO.getName((int) medicalInventoryInDTO.medicalEquipmentNameType, "medical_equipment_name", Language.equals("English") ? "name_en" : "name_bn", "id");
                         }
                     %>

                     <%=value%>


                 </td>

                 <td style="width: 30px">
                     <%
                         value = medicalInventoryInDTO.stockInQuantity + "";
                     %>

                     <%=Utils.getDigits(value, Language)%>


                 </td>
			</tr>
			<%
		}
	}
	%>
    </table>
        	
</td>


<td id='<%=i%>_insertedByUserId'>
    <%
        value = medical_inventory_lotDTO.insertedByUserId + "";
    %>
    <%
        value = WorkflowController.getNameFromUserId(medical_inventory_lotDTO.insertedByUserId, Language);
        if (value.equalsIgnoreCase("")) {
            value = "superman";
        }
    %>

    <%=value%>


</td>


<td id='<%=i%>_insertionDate'>
    <%
        value = medical_inventory_lotDTO.insertionDate + "";
    %>
    <%
        String formatted_insertionDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>

    <%=Utils.getDigits(formatted_insertionDate, Language)%>


</td>


<td>
	<button
			type="button"
			class="btn-sm border-0 shadow bg-light btn-border-radius"
			style="color: #ff6b6b;"
			onclick="location.href='Medical_inventory_lotServlet?actionType=view&ID=<%=medical_inventory_lotDTO.iD%>'">
		<i class="fa fa-eye"></i>
	</button>
</td>

<td>
<%
if(!medical_inventory_lotDTO.isFinal)
{
	%>
	 <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Medical_inventory_lotServlet?actionType=getEditPage&ID=<%=medical_inventory_lotDTO.iD%>'"
    >
        <i class="fa fa-edit"></i>
    </button>
	<%
}
%>
</td>

<td>
<%
if(!medical_inventory_lotDTO.isFinal)
{
	%>
	 <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="addToStock(<%=medical_inventory_lotDTO.iD%>)"
    >
        <i class="fa fa-plus"></i>
    </button>
	<%
}
%>
</td>
	
																			
											

