<%@page import="drug_information.Drug_informationDTO"%>
<%@page import="drug_information.Drug_informationDAO"%>
<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>

<%@page import="medical_inventory_lot.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>


<%
    Medical_inventory_lotDTO medical_inventory_lotDTO;
    medical_inventory_lotDTO = (Medical_inventory_lotDTO) request.getAttribute("medical_inventory_lotDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (medical_inventory_lotDTO == null) {
        medical_inventory_lotDTO = new Medical_inventory_lotDTO();

    }
    System.out.println("medical_inventory_lotDTO = " + medical_inventory_lotDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_MEDICAL_INVENTORY_LOT_ADD_FORMNAME, loginDTO);
    String servletName = "Medical_inventory_lotServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.MEDICAL_INVENTORY_LOT_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
%>

<style>
    table input.form-control {
        width: auto;!important;
    }

    table select.form-control {
        /*min-width: 20rem;*/
        width: auto;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Medical_inventory_lotServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="sub_title_top">
                                                <div class="sub_title">
                                                    <h4 style="background: white"><%=formTitle%>
                                                    </h4>
                                                </div>
                                            </div>


                                            <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                                   value='<%=medical_inventory_lotDTO.iD%>' tag='pb_html'/>

                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_PARLIAMENTSUPPLIERTYPE, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <select class='form-control' name='parliamentSupplierType'
                                                            id='parliamentSupplierType_select_<%=i%>' tag='pb_html'>
                                                        <%
                                                            Options = CommonDAO.getOptions(Language, "parliament_supplier", medical_inventory_lotDTO.parliamentSupplierType);
                                                        %>
                                                        <%=Options%>
                                                    </select>

                                                </div>
                                            </div>
                                            <input type='hidden' class='form-control' name='relatedPoId'
                                                   id='relatedPoId_hidden_<%=i%>'
                                                   value='<%=medical_inventory_lotDTO.relatedPoId%>' tag='pb_html'/>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_STOCKLOTNUMBER, loginDTO)%>
                                                    *
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='stockLotNumber'
                                                           required
                                                           id='stockLotNumber_text_<%=i%>'
                                                           value='<%=medical_inventory_lotDTO.stockLotNumber%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_REMARKS, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='remarks'
                                                           id='remarks_text_<%=i%>'
                                                           value='<%=medical_inventory_lotDTO.remarks%>' tag='pb_html'/>
                                                </div>
                                            </div>
                                            <input type='hidden' class='form-control' name='insertedByUserId'
                                                   id='insertedByUserId_hidden_<%=i%>'
                                                   value='<%=medical_inventory_lotDTO.insertedByUserId%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                                   id='insertedByOrganogramId_hidden_<%=i%>'
                                                   value='<%=medical_inventory_lotDTO.insertedByOrganogramId%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertionDate'
                                                   id='insertionDate_hidden_<%=i%>'
                                                   value='<%=medical_inventory_lotDTO.insertionDate%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='searchColumn'
                                                   id='searchColumn_hidden_<%=i%>'
                                                   value='<%=medical_inventory_lotDTO.searchColumn%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='isDeleted'
                                                   id='isDeleted_hidden_<%=i%>'
                                                   value='<%=medical_inventory_lotDTO.isDeleted%>' tag='pb_html'/>

                                            <input type='hidden' class='form-control' name='lastModificationTime'
                                                   id='lastModificationTime_hidden_<%=i%>'
                                                   value='<%=medical_inventory_lotDTO.lastModificationTime%>'
                                                   tag='pb_html'/>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_MEDICAL_INVENTORY_IN, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table id="tableData" class="table table-bordered table-striped text-nowrap">
                                <thead>
                                <tr>
                                    <th style = "display:none"><%=LM.getText(LC.HM_DEPARTMENT, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_MEDICAL_INVENTORY_IN_MEDICALITEMCAT, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.HM_Item, loginDTO)%>
                                    </th>
                                    <th>
                                    <%=Language.equalsIgnoreCase("english")?"Current Stock":"বর্তমান স্টক"%>
                                    </th>
                                    <th>
                                    <%=Language.equalsIgnoreCase("english")?"Available Stock":"ব্যবহার্য স্টক"%>
                                    </th>

                                    <th><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_MEDICAL_INVENTORY_IN_STOCKINQUANTITY, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_MEDICAL_INVENTORY_IN_UNITPRICE, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_MEDICAL_INVENTORY_IN_SCANCODE, loginDTO)%>
                                    </th>
                                    <th style="min-width:25rem"><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_MEDICAL_INVENTORY_IN_SUPPLYDATE, loginDTO)%>
                                    </th>
                                    <th style="min-width:25rem"><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_MEDICAL_INVENTORY_IN_STOCKLOTEXPIRYDATE, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_MEDICAL_INVENTORY_IN_REMOVE, loginDTO)%>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="field-MedicalInventoryIn">


                                <%
                                    if (actionName.equals("edit")) {
                                        int index = -1;
                                        int lastAvailableStock = 0;
                                        int lastCurrentStock = 0;


                                        for (MedicalInventoryInDTO medicalInventoryInDTO : medical_inventory_lotDTO.medicalInventoryInDTOList) {
                                            index++;

                                            System.out.println("index index = " + index);

                                %>

                                <tr id="MedicalInventoryIn_<%=index + 1%>">
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control' name='medicalInventoryIn.iD'
                                               id='iD_hidden_<%=childTableStartingID%>'
                                               value='<%=medicalInventoryInDTO.iD%>' tag='pb_html'/>

                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='medicalInventoryIn.medicalInventoryLotId'
                                               id='medicalInventoryLotId_hidden_<%=childTableStartingID%>'
                                               value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.medicalInventoryLotId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                    </td>
                                    <td style = "display:none">


                                        <select class='form-control' name='medicalInventoryIn.departmentCat'
                                                id='departmentCat_category_<%=childTableStartingID%>' tag='pb_html'>
                                            <%
                                                if (actionName.equals("edit")) {
                                                    Options = CatDAO.getOptions(Language, "department", medicalInventoryInDTO.departmentCat);
                                                } else {
                                                    Options = CatDAO.getOptions(Language, "department", -1);
                                                }
                                            %>
                                            <%=Options%>
                                        </select>

                                    </td>
                                    <td>


                                        <input class='form-control' style="display:none" name='medicalInventoryIn.medicalItemCat'
                                                id='medicalItemCat_category_<%=childTableStartingID%>'
                                                tag='pb_html' value = "<%=medicalInventoryInDTO.medicalItemCat%>">
                                      
                                        
                                        <%=CatDAO.getName(Language, "medical_item", medicalInventoryInDTO.medicalItemCat) %>
                                        
                                        

                                    </td>
                                    <td>

										<input name='medicalInventoryIn.drugInformationType'
                                                   id='drugInformationType_select_<%=childTableStartingID%>' type="hidden"
                                                   tag='pb_html'
                                                   value='<%=medicalInventoryInDTO.drugInformationType%>'></input>
                                                   
                                                   
                                     <%
	                                    if (medicalInventoryInDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_DRUG) {
	                                        Drug_informationDTO drDTO = drug_informationDAO.getDrugInfoDetailsById(medicalInventoryInDTO.drugInformationType);
	                                        if(drDTO != null)
	                                        {
	                                        	value = drDTO.drugText;
	                                        	lastAvailableStock = drDTO.availableStock;
	                                        	lastCurrentStock = drDTO.currentStock;
	                                        }
	                                    } else if (medicalInventoryInDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_REAGENT) {
	                                        value = CommonDAO.getName((int) medicalInventoryInDTO.medicalReagentNameType, "medical_reagent_name", Language.equals("English") ? "name_en" : "name_bn", "id");
	                                    } else if (medicalInventoryInDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_EQUIPMENT) {
	                                        value = CommonDAO.getName((int) medicalInventoryInDTO.medicalEquipmentNameType, "medical_equipment_name", Language.equals("English") ? "name_en" : "name_bn", "id");
	                                    }
	                                %>
	
	                                <%=value%>

                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='medicalInventoryIn.medicineGenericNameId'
                                               id='medicineGenericNameId_hidden_<%=childTableStartingID%>'
                                               value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.medicineGenericNameId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='medicalInventoryIn.pharmaCompanyNameId'
                                               id='pharmaCompanyNameId_hidden_<%=childTableStartingID%>'
                                               value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.pharmaCompanyNameId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                    </td>
                                    <td style="display: none;">


                                        <select class='form-control mitem w-100'
                                                name='medicalInventoryIn.medicalReagentNameType'
                                                id='medicalReagentNameType_select_<%=childTableStartingID%>'
                                                tag='pb_html'>
                                            <%
                                                if (actionName.equals("edit")) {
                                                    Options = CommonDAO.getReagents(medicalInventoryInDTO.medicalReagentNameType);
                                                } else {
                                                    Options = CommonDAO.getReagents(-1);
                                                }
                                            %>
                                            <%=Options%>
                                        </select>

                                    </td>
                                    <td style="display: none;">


                                        <select class='form-control mitem'
                                                name='medicalInventoryIn.medicalEquipmentNameType'
                                                id='medicalEquipmentNameType_select_<%=childTableStartingID%>'
                                                tag='pb_html'>
                                            <%
                                                if (actionName.equals("edit")) {
                                                    Options = CommonDAO.getEquipments(medicalInventoryInDTO.medicalEquipmentNameType);
                                                } else {
                                                    Options = CommonDAO.getEquipments(-1);
                                                }
                                            %>
                                            <%=Options%>
                                        </select>

                                    </td>
                                    
                                    <td>
                                    	<input type='text' class='form-control' readonly
                                               name='medicalInventoryIn.currentStock'
                                               id='currentStock_text_<%=childTableStartingID%>'
                                               value=<%=lastCurrentStock%>' tag='pb_html'/>
     
                                    </td>
                                    
                                    <td>
                                    	<input type='text' class='form-control' readonly
                                               name='medicalInventoryIn.availableStock'
                                               id='availableStock_text_<%=childTableStartingID%>'
                                               value=<%=lastAvailableStock%>' tag='pb_html'/>
                                    </td>
                                    <td>


                                        <input type='number' class='form-control'
                                               name='medicalInventoryIn.stockInQuantity'
                                            <%=(userDTO.roleID != SessionConstants.ADMIN_ROLE)? "min='0'":"" %>
                                               id='stockInQuantity_text_<%=childTableStartingID%>'
                                               value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.stockInQuantity + "'"):("'" + "0" + "'")%>   tag='pb_html'/>
                                    </td>
                                    <td>


                                        <input type='number' class='form-control' name='medicalInventoryIn.unitPrice'
                                               step="0.01"
                                               id='unitPrice_text_<%=childTableStartingID%>'
                                               value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.unitPrice + "'"):("'" + "0.0" + "'")%>   tag='pb_html'/>
                                    </td>
                                    <td>


                                        <input type='text' class='form-control' name='medicalInventoryIn.scanCode'
                                               id='scanCode_text_<%=childTableStartingID%>'
                                               value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.scanCode + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                    </td>
                                    <td style="width:400px">


                                        <%value = "supplyDate_js_" + childTableStartingID;%>
                                        <jsp:include page="/date/date.jsp">
                                            <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                            <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                        </jsp:include>
                                        <input type='hidden' name='medicalInventoryIn.supplyDate'
                                               id='supplyDate_date_<%=childTableStartingID%>'
                                               value='<%=dateFormat.format(new Date(medicalInventoryInDTO.supplyDate))%>'
                                               tag='pb_html'>
                                    </td>
                                    <td style="width:400px">


                                        <%value = "stockLotExpiryDate_js_" + childTableStartingID;%>
                                        <jsp:include page="/date/date.jsp">
                                            <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                            <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                        </jsp:include>
                                        <input type='hidden' name='medicalInventoryIn.stockLotExpiryDate'
                                               id='stockLotExpiryDate_date_<%=childTableStartingID%>'
                                               value='<%=dateFormat.format(new Date(medicalInventoryInDTO.stockLotExpiryDate))%>'
                                               tag='pb_html'>
                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='medicalInventoryIn.insertedByUserId'
                                               id='insertedByUserId_hidden_<%=childTableStartingID%>'
                                               value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='medicalInventoryIn.insertedByOrganogramId'
                                               id='insertedByOrganogramId_hidden_<%=childTableStartingID%>'
                                               value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.insertedByOrganogramId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='medicalInventoryIn.insertionDate'
                                               id='insertionDate_hidden_<%=childTableStartingID%>'
                                               value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='medicalInventoryIn.isDeleted'
                                               id='isDeleted_hidden_<%=childTableStartingID%>'
                                               value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>

                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='medicalInventoryIn.lastModificationTime'
                                               id='lastModificationTime_hidden_<%=childTableStartingID%>'
                                               value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                    </td>
                                    <td>
                                        <div class='checker'><span id='chkEdit'><input type='checkbox'
                                                                                       id='medicalInventoryIn_cb_<%=index%>'
                                                                                       name='checkbox'
                                                                                       value=''/></span></div>
                                    </td>
                                </tr>
                                <%
                                            childTableStartingID++;
                                        }
                                    }
                                %>

                                </tbody>
                            </table>
                        </div>
                        <div class="form-group mt-4">
                            <div class="text-right">
                                <button
                                        id="add-more-MedicalInventoryIn"
                                        name="add-moreMedicalInventoryIn"
                                        type="button"
                                        class="btn btn-sm text-white add-btn shadow">
                                    <i class="fa fa-plus"></i>
                                    <%=LM.getText(LC.HM_ADD, loginDTO)%>
                                </button>
                                <button
                                        id="remove-MedicalInventoryIn"
                                        name="removeMedicalInventoryIn"
                                        type="button"
                                        class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        </div>
                        <%MedicalInventoryInDTO medicalInventoryInDTO = new MedicalInventoryInDTO();%>
                        <template id="template-MedicalInventoryIn">
                            <tr>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='medicalInventoryIn.iD'
                                           id='iD_hidden_' value='<%=medicalInventoryInDTO.iD%>' tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='medicalInventoryIn.medicalInventoryLotId'
                                           id='medicalInventoryLotId_hidden_'
                                           value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.medicalInventoryLotId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                </td>
                                <td style = "display:none">


                                    <select class='form-control' name='medicalInventoryIn.medicalDeptCat'
                                            id='departmentCat_category_' tag='pb_html'>
                                        <%
                                           
                                                Options = CatDAO.getOptions(Language, "medical_dept", medicalInventoryInDTO.medicalDeptCat);
                                            
                                        %>
                                        <%=Options%>
                                    </select>

                                </td>

                                <td>

                                    <select class='form-control'
                                            name='medicalInventoryIn.medicalItemCat' id='medicalItemCat_category_'
                                            onchange="catSelected(this.id);" tag='pb_html'>
                                        <%
                                            if (actionName.equals("edit")) {
                                                Options = CatDAO.getOptions(Language, "medical_item", medicalInventoryInDTO.medicalItemCat);
                                            } else {

                                                Options = CatDAO.getOptions(Language, "medical_item", -1);
                                            }
                                        %>
                                        <%=Options%>
                                    </select>

                                </td>
                                <td id='dummytd_' tag='pb_html'></td>
                                <td id='drugtd_' tag='pb_html' style='display:none'>
									<div class="input-group">
											        <input
											          class="form-control py-2  border searchBox w-auto"
											          type="search"
											          placeholder="Search"
											          name='suggested_drug'
                                                      id='suggested_drug_'
                                                      tag='pb_html'
                                                      onkeyup="getDrugs(this.id)"
                                                      autocomplete="off"
											        />
											        
											      </div>
											      <div
											        id="searchSgtnSection_"
											        class="search-sgtn-section shadow-sm bg-white"
													tag='pb_html'
											      >
											        <div class="">
											          <ul class="px-0" style="list-style: none" id="drug_ul_" tag='pb_html'>
											          </ul>
											        </div>
											      </div>
											      
											      <input name='formStr'
                                                   id='formStr_' type="hidden"
                                                   tag='pb_html'
                                                   value=''></input>
                                                   
                                     <input name='medicalInventoryIn.drugInformationType'
                                                   id='drugInformationType_select_' type="hidden"
                                                   tag='pb_html'
                                                   value='<%=medicalInventoryInDTO.drugInformationType%>'></input>

                                   

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='medicalInventoryIn.medicineGenericNameId'
                                           id='medicineGenericNameId_hidden_'
                                           value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.medicineGenericNameId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='medicalInventoryIn.pharmaCompanyNameId'
                                           id='pharmaCompanyNameId_hidden_'
                                           value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.pharmaCompanyNameId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                </td>
                                <td id='reagenttd_' tag='pb_html' style='display:none'>


                                    <select class='form-control mitem w-100'
                                            name='medicalInventoryIn.medicalReagentNameType'
                                            id='medicalReagentNameType_select_' tag='pb_html'>
                                        <%
                                            if (actionName.equals("edit")) {
                                                Options = CommonDAO.getReagents(medicalInventoryInDTO.medicalReagentNameType);
                                            } else {
                                                Options = CommonDAO.getReagents(-1);
                                            }
                                        %>
                                        <%=Options%>
                                    </select>

                                </td>
                                <td id='eqtd_' tag='pb_html' style='display:none'>

									<select class='form-control' name='medicalInventoryIn.medicalEquipmentCat'
                                            id='equipmentCat_category_' tag='pb_html' onchange = 'fillEquipemnts(this.id)'>
                                        <%
                                           
                                                Options = CatDAO.getOptions(Language, "medical_equipment", -1);
                                            
                                        %>
                                        <%=Options%>
                                    </select>

                                    <select class='form-control mitem w-100'
                                            name='medicalInventoryIn.medicalEquipmentNameType'
                                            id='medicalEquipmentNameType_select_' tag='pb_html'>
                                        <%
                                            if (actionName.equals("edit")) {
                                                Options = CommonDAO.getEquipments(medicalInventoryInDTO.medicalEquipmentNameType);
                                            } else {
                                                Options = CommonDAO.getEquipments(-1);
                                            }
                                        %>
                                        <%=Options%>
                                    </select>

                                </td>
                                
                                <td>
                                  	<input type='text' class='form-control' readonly
                                             name='medicalInventoryIn.currentStock'
                                             id='currentStock_text_'
                                              tag='pb_html'/>
   
                                  </td>
                                  
                                  <td>
                                  	<input type='text' class='form-control' readonly
                                             name='medicalInventoryIn.availableStock'
                                             id='availableStock_text_'
                                             tag='pb_html'/>
                                  </td>
                                <td>


                                    <input type='number' style="width:100px" class='form-control'
                                        <%=(userDTO.roleID != SessionConstants.ADMIN_ROLE)? "min='0'":"" %>
                                           name='medicalInventoryIn.stockInQuantity' id='stockInQuantity_text_'
                                           value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.stockInQuantity + "'"):("''")%>   tag='pb_html'/>
                                </td>
                                <td>


                                    <input type='number' style="width:100px" class='form-control'
                                           name='medicalInventoryIn.unitPrice' id='unitPrice_text_' step="0.01"
                                           value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.unitPrice + "'"):("''")%>   tag='pb_html'/>
                                </td>
                                <td>


                                    <input type='text' style="width:100px" class='form-control'
                                           name='medicalInventoryIn.scanCode' id='scanCode_text_'
                                           value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.scanCode + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                </td>
                                <td style="width:500px">


                                    <div id="supplyDate_div_" tag='pb_html'></div>

                                    <input type='hidden' name='medicalInventoryIn.supplyDate' id='supplyDate_date_'
                                           value='<%=dateFormat.format(new Date(medicalInventoryInDTO.supplyDate))%>'
                                           tag='pb_html'>
                                </td>
                                <td style="width:500px">


                                    <div id="stockLotExpiryDate_div_" tag='pb_html'></div>

                                    <input type='hidden' name='medicalInventoryIn.stockLotExpiryDate'
                                           id='stockLotExpiryDate_date_'
                                           value='<%=dateFormat.format(new Date(medicalInventoryInDTO.stockLotExpiryDate))%>'
                                           tag='pb_html'>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='medicalInventoryIn.insertedByUserId' id='insertedByUserId_hidden_'
                                           value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='medicalInventoryIn.insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_'
                                           value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.insertedByOrganogramId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='medicalInventoryIn.insertionDate' id='insertionDate_hidden_'
                                           value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='medicalInventoryIn.isDeleted'
                                           id='isDeleted_hidden_'
                                           value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='medicalInventoryIn.lastModificationTime'
                                           id='lastModificationTime_hidden_'
                                           value=<%=actionName.equals("edit")?("'" + medicalInventoryInDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                </td>
                                <td>
                                    <div><span id='chkEdit'><input type='checkbox' name='checkbox' value=''/></span>
                                    </div>
                                </td>
                            </tr>

                        </template>
                    </div>
                </div>
                <div class="form-actions text-center mb-5">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_MEDICAL_INVENTORY_LOT_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_MEDICAL_INVENTORY_LOT_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    function catSelected(id) {
        var rowId = id.split("_")[2];
        var val = $("#" + id).val();
        if (val == 0 || val == 1 || val == 2) {
            $("#dummytd_" + rowId).css("display", "none");
        }
        if (val == 0) {
            $("#drugtd_" + rowId).css("display", "block");
            $("#reagenttd_" + rowId).css("display", "none");
            $("#eqtd_" + rowId).css("display", "none");

        } else if (val == 1) {
            $("#reagenttd_" + rowId).css("display", "block");
            $("#drugtd_" + rowId).css("display", "none");
            $("#eqtd_" + rowId).css("display", "none");
        } else if (val == 2) {
            $("#eqtd_" + rowId).css("display", "block");
            $("#drugtd_" + rowId).css("display", "none");
            $("#reagenttd_" + rowId).css("display", "none");
        }
    }

    function PreprocessBeforeSubmiting(row, validate) {

    	var drugs = [];
        var duplicateFound = false;
         $( "[name= 'medicalInventoryIn.drugInformationType']" ).each(function( index )
         {
        	 if($( "[name= 'medicalInventoryIn.medicalItemCat']" )[index].val() == '0')
       		 {
        		 	console.log("alt val = " + $( "[name= 'patientPrescriptionMedicine2.alternateDrugName']" ).eq(index).val());
	     	        if ($(this).val() != -1 && drugs.includes($(this).val())) 
	     	        {
	     	            toastr.error("Duplicate/Invalid Medicine is not allowed");
	     	            duplicateFound = true;

	     	        }       
	     	        else if ($(this).val() == -1) 
	     	        {
	     	            toastr.error("Invalid medicine");
	     	            duplicateFound = true;

	     	        }
	
	     	        else 
	     	        {
	     	            drugs.push($(this).val());
	     	        }
       		 }
        	
	      });
         
         if(duplicateFound)
       	 {
       	 	return false;
       	 }
        for (i = 1; i < child_table_extra_id; i++) {
            if (document.getElementById("supplyDate_date_" + i)) {
                if (document.getElementById("supplyDate_date_" + i).getAttribute("processed") == null) {
                    preprocessDateBeforeSubmitting('supplyDate', i);
                    document.getElementById("supplyDate_date_" + i).setAttribute("processed", "1");
                }
            }
            if (document.getElementById("stockLotExpiryDate_date_" + i)) {
                if (document.getElementById("stockLotExpiryDate_date_" + i).getAttribute("processed") == null) {
                    preprocessDateBeforeSubmitting('stockLotExpiryDate', i);
                    document.getElementById("stockLotExpiryDate_date_" + i).setAttribute("processed", "1");
                }
            }
        }
        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Medical_inventory_lotServlet");
    }

    function init(row) {


        for (i = 1; i < child_table_extra_id; i++) {
            setDateByStringAndId('supplyDate_js_' + i, $('#supplyDate_date_' + i).val());
            setDateByStringAndId('stockLotExpiryDate_js_' + i, $('#stockLotExpiryDate_date_' + i).val());
        }

        $(".mitem").select2({
            dropdownAutoWidth: true
        });

    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-MedicalInventoryIn").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-MedicalInventoryIn");

            $("#field-MedicalInventoryIn").append(t.html());
            SetCheckBoxValues("field-MedicalInventoryIn");

            var tr = $("#field-MedicalInventoryIn").find("tr:last-child");

            tr.attr("id", "MedicalInventoryIn_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                //console.log( index + ": " + $( this ).attr('id') );
            });

            $(".mitem").select2({
                dropdownAutoWidth: true
            });


            createDatePicker("supplyDate_div_" + child_table_extra_id, {
                'DATE_ID': 'supplyDate_js_' + child_table_extra_id,
                'LANGUAGE': '<%=Language%>',
                'END_YEAR': new Date().getFullYear() + 5,
                'START_YEAR': new Date().getFullYear() - 5
            }, datesetter());

            //setDateByStringAndId('supplyDate_js_' + child_table_extra_id, $('#supplyDate_date_' + child_table_extra_id).val());
            //setDateByStringAndId('stockLotExpiryDate_js_' + child_table_extra_id, $('#stockLotExpiryDate_date_' + child_table_extra_id).val());


        });

    function datesetter() {
        console.log("date setting: " + $('#supplyDate_date_' + child_table_extra_id).val());
        setDateByStringAndId('supplyDate_js_' + child_table_extra_id, $('#supplyDate_date_' + child_table_extra_id).val());
        createDatePicker("stockLotExpiryDate_div_" + child_table_extra_id, {
            'DATE_ID': 'stockLotExpiryDate_js_' + child_table_extra_id,
            'LANGUAGE': '<%=Language%>',
            'END_YEAR': new Date().getFullYear() + 5,
            'START_YEAR': new Date().getFullYear() - 5
        }, childIncrementer());

    }

    function childIncrementer() {
    	setDateByStringAndId('stockLotExpiryDate_js_' + child_table_extra_id, $('#stockLotExpiryDate_date_' + child_table_extra_id).val());
        console.log("incrementing child");
        child_table_extra_id++;
    }


    $("#remove-MedicalInventoryIn").click(function (e) {
        var tablename = 'field-MedicalInventoryIn';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[type="checkbox"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
    });
    
    function drugClicked(rowId, drugText, drugId, form,
    		availableStock, len, showTick, currentStock)
    {
    	console.log("drugClicked with " + rowId + ", availableStock = " + availableStock + ", currentStock = " + currentStock);
    	$("#suggested_drug_" + rowId).val(drugText);
    	$("#suggested_drug_" + rowId).attr("style", "width: " + ($("#suggested_drug_" + rowId).val().length + 1)*8 + "px !important");
    	$("#drug_ul_" + rowId).html("");
		$("#drugInformationType_select_" + rowId).val(drugId);
		$("#formStr_" + rowId).val(form);
		$("#currentStock_text_" + rowId).val(currentStock);
		$("#availableStock_text_" + rowId).val(availableStock);
		<%
		if(userDTO.roleID == SessionConstants.ADMIN_ROLE)
		{
			%>
			$("#stockInQuantity_text_" + rowId).attr("min", parseInt(availableStock) - parseInt(currentStock));
			<%
		}
		%>
    }
	
	function fillEquipemnts(id)
	{
		var rowId = id.split("_")[2];
		var eqNameId = "medicalEquipmentNameType_select_" + rowId;
		var medicalDeptCat = $("#departmentCat_category_" + rowId).val();
		var medicalEquipmentCat = $("#" + id).val();
		
		console.log("medicalDeptCat = " + medicalDeptCat + " medicalEquipmentCat = " + medicalEquipmentCat )
		
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {
				$("#" + eqNameId).html(this.responseText);
			} else if (this.readyState == 4 && this.status != 200) {
				alert('failed ' + this.status);
			}
		};

		xhttp.open("GET",  "Medical_equipment_nameServlet?actionType=getEquipments&medicalDeptCat=" + medicalDeptCat + "&medicalEquipmentCat=" + medicalEquipmentCat, true);
		xhttp.send();
	}


</script>






