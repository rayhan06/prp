<%@page import="user.*"%>
<%@page import="drug_information.*"%>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="medical_inventory_lot.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>



<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.MEDICAL_INVENTORY_LOT_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Medical_inventory_lotDAO medical_inventory_lotDAO = new Medical_inventory_lotDAO("medical_inventory_lot");
    Medical_inventory_lotDTO medical_inventory_lotDTO = medical_inventory_lotDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
%>
<div class="ml-auto mr-5 mt-4 mb-2">
    
		    <button type="button" class="btn" id='printer2'
		            onclick="printAnyDiv('modalbody')">
		        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
		    </button>
		    
		</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
	
    <div class="kt-portlet" id = "modalbody">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_MEDICAL_INVENTORY_LOT_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div>
                <h5 class="table-title">
                    <%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_MEDICAL_INVENTORY_LOT_ADD_FORMNAME, loginDTO)%>
                </h5>
                
	                <div class="table-responsive">
	                    <table class="table table-bordered table-striped text-nowrap">
	                        <tr>
	                            <td>
	                                <b><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_PARLIAMENTSUPPLIERTYPE, loginDTO)%>
	                                </b></td>
	                            <td>
	
	                                <%
	                                    value = medical_inventory_lotDTO.parliamentSupplierType + "";
	                                %>
	                                <%
	                                    value = CommonDAO.getName(Integer.parseInt(value), "parliament_supplier", Language.equals("English") ? "name_en" : "name_bn", "id");
	                                %>
	
	                                <%=value%>
	
	
	                            </td>
	
	                        </tr>
	
	
	                        <tr>
	                            <td>
	                                <b><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_STOCKLOTNUMBER, loginDTO)%>
	                                </b></td>
	                            <td>
	
	                                <%
	                                    value = medical_inventory_lotDTO.stockLotNumber + "";
	                                %>
	
	                                <%=Utils.getDigits(value, Language)%>
	
	
	                            </td>
	
	                        </tr>
	
	
	                        <tr>
	                            <td><b><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_REMARKS, loginDTO)%>
	                            </b></td>
	                            <td>
	
	                                <%
	                                    value = medical_inventory_lotDTO.remarks + "";
	                                %>
	
	                                <%=value%>
	
	
	                            </td>
	
	                        </tr>
	
	
	                        <tr>
	                            <td><b><%=LM.getText(LC.HM_INSERTED_BY, loginDTO)%>
	                            </b></td>
	                            <td>
	
	                                <%
	                                    value = medical_inventory_lotDTO.insertedByUserId + "";
	                                %>
	                                <%
	                                    value = WorkflowController.getNameFromUserId(medical_inventory_lotDTO.insertedByUserId, Language);
	                                    if (value.equalsIgnoreCase("")) {
	                                        value = "superman";
	                                    }
	                                %>
	
	                                <%=value%>
	
	
	                            </td>
	
	                        </tr>
	
	
	                        <tr>
	                            <td><b><%=LM.getText(LC.HM_INSERTION_DATE, loginDTO)%>
	                            </b></td>
	                            <td>
	
	                                <%
	                                    value = medical_inventory_lotDTO.insertionDate + "";
	                                %>
	                                <%
	                                    String formatted_insertionDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
	                                %>
	                                <%=Utils.getDigits(formatted_insertionDate, Language)%>
	
	
	                            </td>
	
	                        </tr>
	
	
	                    </table>
	                </div>
            	
            </div>
            <div class="mt-5">
                <h5 class="table-title">
                    <%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_MEDICAL_INVENTORY_IN, loginDTO)%>
                </h5>
                <div >
                	<form action = "Drug_informationServlet?actionType=massEditAvailability" method="POST">
                		<%
                		if((userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE)
                				&& medical_inventory_lotDTO.isFinal)
                		{
                		%>
                		<input type = "hidden" name = "lotId" value = "<%=medical_inventory_lotDTO.iD%>" />
                		<div style = "float:right" class = "row">
                			<button type="button"
					                id = "makeEditableButton"
					                class="btn-sm border-0 shadow bg-light btn-border-radius"
					                style="color: #ff6b6b; display:block;"
					                onclick="makeEditable()">
					            <i class="fa fa-edit"></i>
					        </button>
					        <button type="button"
					                id = "makeViewableButton"
					                class="btn-sm border-0 shadow bg-light btn-border-radius"
					                style="color: #ff6b6b; display:none;"
					                onclick="makeViewable()">
					            <i class="fa fa-eye"></i>
					        </button>
					        <button type="submit"
					                id = "submitButton"
					                class="btn-sm border-0 shadow bg-light btn-border-radius"
					                style="color: #ff6b6b; display:none;"
					                >
					            <i class="fa fa-check"></i>
					        </button>
                		</div>
                		<%
                		}
                		%>
	                    <table class="table table-bordered table-striped ">
	                        <tr>
	                        	<th><%=LM.getText(LC.HM_SL, loginDTO)%>
	                            </th>
	                            <th style = "display:none"><%=LM.getText(LC.HM_DEPARTMENT, loginDTO)%>
	                            </th>
	                            <th><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_MEDICAL_INVENTORY_IN_MEDICALITEMCAT, loginDTO)%>
	                            </th>
	                            <th><%=LM.getText(LC.HM_NAME, loginDTO)%>
	                            </th>
	
	                            <th><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_MEDICAL_INVENTORY_IN_STOCKINQUANTITY, loginDTO)%>
	                            </th>
	                            <th><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_MEDICAL_INVENTORY_IN_UNITPRICE, loginDTO)%>
	                            </th>
	                            <th style = "display:none"><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_MEDICAL_INVENTORY_IN_SCANCODE, loginDTO)%>
	                            </th>
	                            <th style = "display:none"><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_MEDICAL_INVENTORY_IN_SUPPLYDATE, loginDTO)%>
	                            </th>
	                            <th style = "display:none"><%=LM.getText(LC.MEDICAL_INVENTORY_LOT_ADD_MEDICAL_INVENTORY_IN_STOCKLOTEXPIRYDATE, loginDTO)%>
	                            </th>
	                            
	                            <th><%=LM.getText(LC.DRUG_INFORMATION_ADD_CURRENTSTOCK, loginDTO)%>
	                            </th>
	                            
	                            <th ><%=Language.equalsIgnoreCase("english")?"Available Stock":"ব্যবহার্য স্টক"%>
	            				</th>
	                        </tr>
	                        <%
	                            MedicalInventoryInDAO medicalInventoryInDAO = new MedicalInventoryInDAO();
	                            List<MedicalInventoryInDTO> medicalInventoryInDTOs = medicalInventoryInDAO.getMedicalInventoryInDTOListByMedicalInventoryLotID(medical_inventory_lotDTO.iD);
	
	                            System.out.println("medicalInventoryInDTOs size = " + medicalInventoryInDTOs.size());
	                            int sl = 1;
	                            for (MedicalInventoryInDTO medicalInventoryInDTO : medicalInventoryInDTOs) 
	                            {
	                        %>
	                        <tr>
	                        	<td><%=Utils.getDigits(sl++, Language) %></td>
	                            <td style = "display:none">
	                                <%
	                                    value = CatDAO.getName(Language, "medical_dept", medicalInventoryInDTO.medicalDeptCat);
	                                %>
	
	                                <%=value%>
	
	
	                            </td>
	                            <td>
	                                <%
	                                    value = medicalInventoryInDTO.medicalItemCat + "";
	                                %>
	                                <%
	                                    value = CatDAO.getName(Language, "medical_item", medicalInventoryInDTO.medicalItemCat);
	                                %>
	
	                                <%=value%>
	
	
	                            </td>
	                            <td>
	
	                                <%
	                                    if (medicalInventoryInDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_DRUG) {
	                                        value = drug_informationDAO.getDrugInfoDetailsById(medicalInventoryInDTO.drugInformationType).drugText;
	                                    } else if (medicalInventoryInDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_REAGENT) {
	                                        value = CommonDAO.getName((int) medicalInventoryInDTO.medicalReagentNameType, "medical_reagent_name", Language.equals("English") ? "name_en" : "name_bn", "id");
	                                    } else if (medicalInventoryInDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_EQUIPMENT) {
	                                        value = CommonDAO.getName((int) medicalInventoryInDTO.medicalEquipmentNameType, "medical_equipment_name", Language.equals("English") ? "name_en" : "name_bn", "id");
	                                    }
	                                %>
	
	                                <%=value%>
	
	
	                            </td>
	
	                            <td>
	                                <%
	                                    value = medicalInventoryInDTO.stockInQuantity + "";
	                                %>
	
	                                <%=Utils.getDigits(value, Language)%>
	
	
	                            </td>
	                            <td>
	                                <%
	                                    value = medicalInventoryInDTO.unitPrice + "";
	                                %>
	
	                                <%=Utils.getDigits(value, Language)%>
	
	
	                            </td>
	                            <td style = "display:none">
	                                <%
	                                    value = medicalInventoryInDTO.scanCode + "";
	                                %>
	
	                                <%=value%>
	
	
	                            </td>
	                            <td style = "display:none">
	                                <%
	                                    value = medicalInventoryInDTO.supplyDate + "";
	                                %>
	                                <%
	                                    String formatted_supplyDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
	                                %>
	                                <%=Utils.getDigits(formatted_supplyDate, Language)%>
	
	
	                            </td>
	                            <td style = "display:none">
	                                <%
	                                    value = medicalInventoryInDTO.stockLotExpiryDate + "";
	                                %>
	                                <%
	                                    String formatted_stockLotExpiryDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
	                                %>
	                                <%=Utils.getDigits(formatted_stockLotExpiryDate, Language)%>
	
	
	                            </td>
	                            
	                            <td>
	                            <%
	                            	Drug_informationDTO drug_informationDTO = null;
	                            	if(medicalInventoryInDTO.medicalItemCat == SessionConstants.MEDICAL_ITEM_DRUG)
	                            	{
	                            		drug_informationDTO = drug_informationDAO.getDTOByID(medicalInventoryInDTO.drugInformationType);
	                            	}
	                            	if(drug_informationDTO != null)
	                            	{
	                            		%>
	                            		<%=Utils.getDigits(drug_informationDTO.currentStock, Language)%>
	                            		<%
	                            	}
	                            %>
	                            </td>
	                            <td>
	                            	<div name = "availableView" style = "display:block">
			                            <%
			                            if(drug_informationDTO != null)
			                        	{
			                        		%>
			                        		<%=Utils.getDigits(drug_informationDTO.availableStock, Language)%>
			                        		<%
			                        	}
			                            %>
		                            </div>
		                            <div name = "availableEdit" style = "display:none">
			                            <%
			                            if(drug_informationDTO != null)
			                        	{
			                        		%>
			                        		<input type = "number" name = "availableStock" id = "availableStock_<%=drug_informationDTO.iD%>" class = "form-control" 
						         			value = "<%=drug_informationDTO.availableStock%>" min = "0" max = "<%=drug_informationDTO.currentStock%>" style = "width:12rem" />
						         			
						         			<input type = "hidden" name = "currentStock" id = "currentStock_<%=drug_informationDTO.iD%>"
						         			value = "<%=drug_informationDTO.currentStock%>"  />
						         			
						         			<input type = "hidden" name = "id" id = "id_<%=drug_informationDTO.iD%>"
						         			value = "<%=drug_informationDTO.iD%>"  />
			                        		<%
			                        	}
			                            %>
		                            </div>
	                            </td>
	                        </tr>
	                        <%
	
	                            }
	
	                        %>
	                    </table>
                	</form>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

	function makeEditable()
	{
		$("#makeViewableButton").css("display", "block");
		$("#makeEditableButton").css("display", "none");
		$("#submitButton").css("display", "block");
		$("[name='availableView']").css("display", "none");
		$("[name='availableEdit']").css("display", "block");
	}
	
	function makeViewable()
	{
		$("#makeViewableButton").css("display", "none");
		$("#makeEditableButton").css("display", "block");
		$("#submitButton").css("display", "none");
		$("[name='availableView']").css("display", "block");
		$("[name='availableEdit']").css("display", "none");
	}
	
	$(document).ready(function () {
		if('${successMessage}' != '')
		{
			toastr.success('${successMessage}');
		}
		if('${failureMessage}' != '')
		{
			toastr.error('${failureMessage}');
		}
	});
	
</script>