const duplicate_servlet_info = {
    servlet: 'OfficeDuplicationServlet?',
    actionType: 'checkDuplication',
    table: 'offices',
    parent: null,
    parent_id: null
};

const property = {
    'iD': {},
    'officeOriginId': {
        'required': true
    },
    'officeNameEng': {
        'required': true,
        'max_length': 255,
        'min_length': 2,
        'duplicate': duplicate_servlet_info
    },
    'officeNameBng': {
        'required': true,
        'max_length': 255,
        'min_length': 2,
        'duplicate': duplicate_servlet_info
    },
    'officeCode': {
        'required': true,
        'is_integer': true,
        'max_value': 100000000,
        'min_value': 1,
        'duplicate': duplicate_servlet_info
    },
    'officeAddress': {
        'required': true,
        'max_length': 255,
        'min_length': 2
    }, 'digitalNothiCode': {
        'required': true,
        'max_length': 32,
        'min_length': 2
    }, 'referenceCode': {
        'required': true,
        'max_length': 32,
        'min_length': 2
    }, 'officePhone': {
        'required': true,
        'max_length': 20,
        'min_length': 4
    }, 'officeMobile': {
        'required': true,
        'max_length': 13,
        'min_length': 11
    }, 'officeFax': {
        'max_length': 255,
        'min_length': 0
    }, 'officeEmail': {
        'required': true,
        'max_length': 255,
        'min_length': 5
    }, 'officeWeb': {
        'max_length': 255,
        'min_length': 0
    }
};

class Offices {

    static add(post_url, request_method, form_data, labelName) {

        const model = {};
        decodeURI(form_data).split("&").forEach(function (x) {
            model[x.split("=")[0]] = x.split("=")[1];
        });

        const keys = Object.keys(property);
        for (let i = 0; i < keys.length; i++) {
            const key = keys[i];
            const value = property[key];

            const innerKeys = Object.keys(value);
            for (let j = 0; j < innerKeys.length; j++) {
                const method_name = innerKeys[j];
                const method_parm = value[method_name];

                if (!DataValidation[method_name](method_parm, model, key, labelName[i - 1])) {
                    console.log('Validation failed: ', method_name, keys, labelName[i - 1]);
                    return false;
                }
            }

            console.log(key, model[key], property[key]);
        }

        return true;
    }
}

