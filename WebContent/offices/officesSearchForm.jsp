
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="offices.OfficesDTO"%>
<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.OFFICES_EDIT_LANGUAGE, loginDTO);
%>				
				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.OFFICES_EDIT_OFFICEMINISTRYID, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_OFFICELAYERID, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_CUSTOMLAYERID, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_OFFICEORIGINID, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_OFFICENAMEENG, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_OFFICENAMEBNG, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_GEODIVISIONID, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_GEODISTRICTID, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_GEOUPAZILAID, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_GEOUNIONID, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_GEOCITYCORPORATIONID, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_GEOCITYCORPORATIONWARDID, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_GEOMUNICIPALITYID, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_GEOMUNICIPALITYWARDID, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_OFFICECODE, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_OFFICEADDRESS, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_DIGITALNOTHICODE, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_REFERENCECODE, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_OFFICEPHONE, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_OFFICEMOBILE, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_OFFICEFAX, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_OFFICEEMAIL, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_OFFICEWEB, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_PARENTOFFICEID, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_STATUS, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_CREATEDBY, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_MODIFIEDBY, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_CREATED, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_MODIFIED, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_GEOTHANAID, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_GEOPOSTOFFICEID, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_GEOCOUNTRYID, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_APPROVALPATHTYPE, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICES_EDIT_LASTMODIFICATIONTIME, loginDTO)%></th>
								<th><%out.print(LM.getText(LC.OFFICES_SEARCH_OFFICES_EDIT_BUTTON, loginDTO));%></th>
								<th><input type="submit" class="btn btn-xs btn-danger" value="
								<%out.print(LM.getText(LC.OFFICES_SEARCH_OFFICES_DELETE_BUTTON, loginDTO));%>
								" /></th>
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_OFFICES);

													try
													{

														if (data != null) 
														{
															int size = data.size();										
															System.out.println("data not null and size = " + size + " data = " + data);
															for (int i = 0; i < size; i++) 
															{
																OfficesDTO row = (OfficesDTO) data.get(i);
																String deletedStyle="color:red";
																if(!row.isDeleted)deletedStyle = "";
																out.println("<tr id = 'tr_" + i + "'>");
																

																
									
																
																out.println("<td id = '" + i + "_officeMinistryId'>");
																value = row.officeMinistryId + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_officeLayerId'>");
																value = row.officeLayerId + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_customLayerId'>");
																value = row.customLayerId + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_officeOriginId'>");
																value = row.officeOriginId + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_officeNameEng'>");
																value = row.officeNameEng + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_officeNameBng'>");
																value = row.officeNameBng + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_geoDivisionId'>");
																value = row.geoDivisionId + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_geoDistrictId'>");
																value = row.geoDistrictId + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_geoUpazilaId'>");
																value = row.geoUpazilaId + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_geoUnionId'>");
																value = row.geoUnionId + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_geoCityCorporationId'>");
																value = row.geoCityCorporationId + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_geoCityCorporationWardId'>");
																value = row.geoCityCorporationWardId + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_geoMunicipalityId'>");
																value = row.geoMunicipalityId + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_geoMunicipalityWardId'>");
																value = row.geoMunicipalityWardId + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_officeCode'>");
																value = row.officeCode + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_officeAddress'>");
																value = row.officeAddress + "";
																out.println(GeoLocationDAO2.parseEnText(value));
																{
																	String addressdetails = GeoLocationDAO2.parseDetails(value);
																	if(!addressdetails.equals(""))
																	{
																		out.println(", " + addressdetails);
																	}
																}
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_digitalNothiCode'>");
																value = row.digitalNothiCode + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_referenceCode'>");
																value = row.referenceCode + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_officePhone'>");
																value = row.officePhone + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_officeMobile'>");
																value = row.officeMobile + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_officeFax'>");
																value = row.officeFax + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_officeEmail'>");
																value = row.officeEmail + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_officeWeb'>");
																value = row.officeWeb + "";
																out.println("<a href='OfficesServlet?actionType=getURL&URL=" + value + "'>Link</a>");
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_parentOfficeId'>");
																value = row.parentOfficeId + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_status'>");
																value = row.status + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_createdBy'>");
																value = row.createdBy + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_modifiedBy'>");
																value = row.modifiedBy + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_created'>");
																value = row.created + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_modified'>");
																value = row.modified + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_geoThanaId'>");
																value = row.geoThanaId + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_geoPostofficeId'>");
																value = row.geoPostofficeId + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_geoCountryId'>");
																value = row.geoCountryId + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
																out.println("<td id = '" + i + "_approvalPathType'>");
																value = row.approvalPathType + "";
																
																value = CommonDAO.getName(Integer.parseInt(value), "approval_path", Language.equals("English")?"name_en":"name_bn", "id");
																			
																out.println(value);
									
								
																out.println("</td>");
									
																
									
																
																out.println("<td id = '" + i + "_lastModificationTime'>");
																value = row.lastModificationTime + "";
																			
																out.println(value);
									
								
																out.println("</td>");
									
								

								

																
																String onclickFunc = "\"fixedToEditable(" + i + ",'" + deletedStyle + "', '" + row.iD + "' )\"";										
								
																out.println("<td id = '" + i + "_Edit'>");										
																out.println("<a onclick=" + onclickFunc + ">" + LM.getText(LC.OFFICES_SEARCH_OFFICES_EDIT_BUTTON, loginDTO) + "</a>");
															
																out.println("</td>");
																
																
																
																out.println("<td>");
																out.println("<div class='checker'>");
																out.println("<span id='chkEdit' ><input type='checkbox' name='ID' value='" + row.iD + "'/></span>");
																out.println("</div");
																out.println("</td>");
																out.println("</tr>");
															}
															 
															System.out.println("printing done");
														}
														else
														{
															System.out.println("data  null");
														}
													}
													catch(Exception e)
													{
														System.out.println("JSP exception " + e);
													}
							%>



						</tbody>

					</table>
				</div>

<%
	String navigator2 = SessionConstants.NAV_OFFICES;
	System.out.println("navigator2 = " + navigator2);
	RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
	System.out.println("rn2 = " + rn2);
	String pageno2 = ( rn2 == null ) ? "1" : "" + rn2.getCurrentPageNo();
	String totalpage2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalPages();
	String totalRecords2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalRecords();
	String lastSearchTime = ( rn2 == null ) ? "0" : "" + rn2.getSearchTime();
%>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />


<script src="nicEdit.js" type="text/javascript"></script>

<script type="text/javascript">



function getOfficer(officer_id, officer_select)
{
	console.log("getting officer");
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{
			if(this.responseText.includes('option'))
			{
				console.log("got response for officer");
				document.getElementById(officer_select).innerHTML = this.responseText ;
				
				if(document.getElementById(officer_select).length > 1)
				{
					document.getElementById(officer_select).removeAttribute("disabled");
				}	
			}
			else
			{
				console.log("got errror response for officer");
			}
			
		}
		else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	};
	xhttp.open("POST", "OfficesServlet?actionType=getGRSOffice&officer_id=" + officer_id, true);
	xhttp.send();
}


function getLayer(layernum, layerID, childLayerID, selectedValue)
{
	console.log("getting layer");
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{
			if(this.responseText.includes('option'))
			{
				console.log("got response");
				document.getElementById(childLayerID).innerHTML = this.responseText ;
			}
			else
			{
				console.log("got errror response");
			}
			
		}
		else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	};
	xhttp.open("POST", "OfficesServlet?actionType=getGRSLayer&layernum=" + layernum + "&layerID=" 
			+ layerID + "&childLayerID=" + childLayerID + "&selectedValue=" + selectedValue, true);
	xhttp.send();
}

function layerselected(layernum, layerID, childLayerID, hiddenInput, hiddenInputForTopLayer, officerElement)
{
	var layervalue = document.getElementById(layerID).value;
	console.log("layervalue = " + layervalue);
	document.getElementById(hiddenInput).value = layervalue;
	if(layernum == 0)
	{
		document.getElementById(hiddenInputForTopLayer).value = layervalue;
	}
	if(layernum == 0 || (layernum == 1 && document.getElementById(hiddenInputForTopLayer).value == 3))
	{
		document.getElementById(childLayerID).setAttribute("style", "display: inline;");
		getLayer(layernum, layerID, childLayerID, layervalue);
	}
	
	if(officerElement !== null)
	{
		getOfficer(layervalue, officerElement);
	}
	
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;
		if(!document.getElementById('officeNameEng_text_' + row).checkValidity())
		{
			empty_fields += "'officeNameEng'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(!document.getElementById('officeNameBng_text_' + row).checkValidity())
		{
			empty_fields += "'officeNameBng'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(!document.getElementById('officeAddress_geolocation_' + row).checkValidity())
		{
			empty_fields += "'officeAddress'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(!document.getElementById('officePhone_text_' + row).checkValidity())
		{
			empty_fields += "'officePhone'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(!document.getElementById('officeEmail_text_' + row).checkValidity())
		{
			empty_fields += "'officeEmail'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}

	document.getElementById('officeAddress_geolocation_' + row).value = document.getElementById('officeAddress_geolocation_' + row).value + ":" + document.getElementById('officeAddress_geoTextField_' + row).value;
	console.log("geo value = " + document.getElementById('officeAddress_geolocation_' + row).value);
	return true;
}

function PostprocessAfterSubmiting(row)
{
	document.getElementById('officeAddress_geolocation_' + row).value = "1";
}

function addrselected(value, htmlID, selectedIndex, tagname, geodiv, hiddenfield)
{
	console.log('geodiv = ' + geodiv + ' hiddenfield = ' + hiddenfield);
	try 
	{
		var elements, ids;
		elements = document.getElementById(geodiv).children;
		
		document.getElementById(hiddenfield).value = value;
		
		ids = '';
		for(var i = elements.length - 1; i >= 0; i--) 
		{
			var elemID = elements[i].id;
			if(elemID.includes(htmlID) && elemID > htmlID)
			{
				ids += elements[i].id + ' ';
				
				for(var j = elements[i].options.length - 1; j >= 0; j--)
				{
				
					elements[i].options[j].remove();
				}
				elements[i].remove();
				
			}
		}
				

		var newid = htmlID + '_1';

		document.getElementById(geodiv).innerHTML += "<select class='form-control' name='" + tagname + "' id = '" + newid 
		+ "' onChange=\"addrselected(this.value, this.id, this.selectedIndex, this.name, '" + geodiv +"', '" + hiddenfield +"')\"></select>";
		//console.log('innerHTML= ' + document.getElementById(geodiv).innerHTML);
		document.getElementById(htmlID).options[0].innerHTML = document.getElementById(htmlID).options[selectedIndex].innerHTML;
		document.getElementById(htmlID).options[0].value = document.getElementById(htmlID).options[selectedIndex].value;
		//console.log('innerHTML again = ' + document.getElementById(geodiv).innerHTML);
		
		
		
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() 
		{
			if (this.readyState == 4 && this.status == 200) 
			{
				if(!this.responseText.includes('option'))
				{
					document.getElementById(newid).remove();
				}
				else
				{
					document.getElementById(newid).innerHTML = this.responseText ;
				}
				
			}
			else if(this.readyState == 4 && this.status != 200)
			{
				alert('failed ' + this.status);
			}
		};
		 
		xhttp.open("POST", "OfficesServlet?actionType=getGeo&myID="+value, true);
		xhttp.send();
	}
	catch(err) 
	{
		alert("got error: " + err);
	}	  

}

function addHTML(id, HTML)
{
	document.getElementById(id).innerHTML += HTML;
}

function getRequests() 
{
    var s1 = location.search.substring(1, location.search.length).split('&'),
        r = {}, s2, i;
    for (i = 0; i < s1.length; i += 1) {
        s2 = s1[i].split('=');
        r[decodeURIComponent(s2[0]).toLowerCase()] = decodeURIComponent(s2[1]);
    }
    return r;
}

function Request(name){
    return getRequests()[name.toLowerCase()];
}

function ShowExcelParsingResult(suffix)
{
	var failureMessage = document.getElementById("failureMessage_" + suffix);
	if(failureMessage == null)
	{
		console.log("failureMessage_" + suffix + " not found");
	}
	console.log("value = " + failureMessage.value);
	if(failureMessage != null &&  failureMessage.value != "")
	{
		alert("Excel uploading result:" + failureMessage.value);
	}
}

function init(row)
{
		
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{
	    	document.getElementById('officeAddress_geoSelectField_' + row).innerHTML = this.responseText ;
		}
	    else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	 };
	xhttp.open("POST", "OfficesServlet?actionType=getGeo&myID=1", true);
	xhttp.send();
}
function doEdit(params, i, id, deletedStyle)
{
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{
			if(this.responseText !='')
			{
				var onclickFunc = "submitAjax(" + i + ",'" + deletedStyle + "')";
				document.getElementById('tr_' + i).innerHTML = this.responseText ;
				document.getElementById('tr_' + i).innerHTML += "<td id = '" + i + "_Submit'></td>";
				document.getElementById(i + '_Submit').innerHTML += "<a onclick=\""+ onclickFunc +"\">Submit</a>";				
				document.getElementById('tr_' + i).innerHTML += "<td>"
					+ "<div class='checker'>"
					+ "<span class='' id='chkEdit'><input type='checkbox' name='ID' value='" + id + "'/></span>"
					+ "</td>";
				init(i);
			}
			else
			{
				document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
			}
		}
		else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	};
	  
	  xhttp.open("Get", "OfficesServlet?actionType=getEditPage" + params, true);
	  xhttp.send();	
}

function submitAjax(i, deletedStyle)
{
	console.log('submitAjax called');
	var isSubmittable = PreprocessBeforeSubmiting(i, "inplaceedit");
	if(isSubmittable == false)
	{
		return;
	}
	var formData = new FormData();
	var value;
	value = document.getElementById('iD_hidden_' + i).value;
	console.log('submitAjax i = ' + i + ' id = ' + value);
	formData.append('iD', value);
	formData.append("identity", value);
	formData.append("ID", value);
	formData.append('officeMinistryId', document.getElementById('officeMinistryId_hidden_' + i).value);
	formData.append('officeLayerId', document.getElementById('officeLayerId_hidden_' + i).value);
	formData.append('customLayerId', document.getElementById('customLayerId_hidden_' + i).value);
	formData.append('officeOriginId', document.getElementById('officeOriginId_hidden_' + i).value);
	formData.append('officeNameEng', document.getElementById('officeNameEng_text_' + i).value);
	formData.append('officeNameBng', document.getElementById('officeNameBng_text_' + i).value);
	formData.append('geoDivisionId', document.getElementById('geoDivisionId_hidden_' + i).value);
	formData.append('geoDistrictId', document.getElementById('geoDistrictId_hidden_' + i).value);
	formData.append('geoUpazilaId', document.getElementById('geoUpazilaId_hidden_' + i).value);
	formData.append('geoUnionId', document.getElementById('geoUnionId_hidden_' + i).value);
	formData.append('geoCityCorporationId', document.getElementById('geoCityCorporationId_hidden_' + i).value);
	formData.append('geoCityCorporationWardId', document.getElementById('geoCityCorporationWardId_hidden_' + i).value);
	formData.append('geoMunicipalityId', document.getElementById('geoMunicipalityId_hidden_' + i).value);
	formData.append('geoMunicipalityWardId', document.getElementById('geoMunicipalityWardId_hidden_' + i).value);
	formData.append('officeCode', document.getElementById('officeCode_text_' + i).value);
	formData.append('officeAddress', document.getElementById('officeAddress_geolocation_' + i).value);
	formData.append('digitalNothiCode', document.getElementById('digitalNothiCode_text_' + i).value);
	formData.append('referenceCode', document.getElementById('referenceCode_text_' + i).value);
	formData.append('officePhone', document.getElementById('officePhone_text_' + i).value);
	formData.append('officeMobile', document.getElementById('officeMobile_text_' + i).value);
	formData.append('officeFax', document.getElementById('officeFax_text_' + i).value);
	formData.append('officeEmail', document.getElementById('officeEmail_text_' + i).value);
	formData.append('officeWeb', document.getElementById('officeWeb_url_' + i).value);
	formData.append('parentOfficeId', document.getElementById('parentOfficeId_hidden_' + i).value);
	formData.append('status', document.getElementById('status_textarea_' + i).value);
	formData.append('createdBy', document.getElementById('createdBy_text_' + i).value);
	formData.append('modifiedBy', document.getElementById('modifiedBy_text_' + i).value);
	formData.append('created', document.getElementById('created_text_' + i).value);
	formData.append('modified', document.getElementById('modified_text_' + i).value);
	formData.append('geoThanaId', document.getElementById('geoThanaId_hidden_' + i).value);
	formData.append('geoPostofficeId', document.getElementById('geoPostofficeId_hidden_' + i).value);
	formData.append('geoCountryId', document.getElementById('geoCountryId_hidden_' + i).value);
	formData.append('approvalPathType', document.getElementById('approvalPathType_select_' + i).value);
	formData.append('isDeleted', document.getElementById('isDeleted_hidden_' + i).value);
	formData.append('lastModificationTime', document.getElementById('lastModificationTime_hidden_' + i).value);

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{
			if(this.responseText !='')
			{				
				document.getElementById('tr_' + i).innerHTML = this.responseText ;
				ShowExcelParsingResult(i);
			}
			else
			{
				console.log("No Response");
				document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
			}
		}
		else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	  };
	xhttp.open("POST", 'OfficesServlet?actionType=edit&inplacesubmit=true&deletedStyle=' + deletedStyle + '&rownum=' + i, true);
	xhttp.send(formData);
}

function fixedToEditable(i, deletedStyle, id)
{
	console.log('fixedToEditable called');
	var params = '&identity=' + id + '&inplaceedit=true' +  '&deletedStyle=' + deletedStyle + '&ID=' + id + '&rownum=' + i
	+ '&dummy=dummy';
	console.log('fixedToEditable i = ' + i + ' id = ' + id);
	doEdit(params, i, id, deletedStyle);

}
window.onload =function ()
{
	ShowExcelParsingResult('general');
}	
</script>
			