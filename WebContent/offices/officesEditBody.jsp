
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="offices.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="geolocation.GeoLocationDAO2"%>

<%
OfficesDTO officesDTO;
officesDTO = (OfficesDTO)request.getAttribute("officesDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(officesDTO == null)
{
	officesDTO = new OfficesDTO();
	
}
System.out.println("officesDTO = " + officesDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.OFFICES_EDIT_OFFICES_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.OFFICES_ADD_OFFICES_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";
OfficesDTO row = officesDTO;
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="OfficesServlet?actionType=<%=actionName%>&identity=<%=ID%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
				
				
				




























	














<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.OFFICES_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=ID%>'/>
	
												

		<input type='hidden' class='form-control'  name='officeMinistryId' id = 'officeMinistryId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officeMinistryId + "'"):("'" + "0" + "'")%>/>
												

		<input type='hidden' class='form-control'  name='officeLayerId' id = 'officeLayerId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officeLayerId + "'"):("'" + "0" + "'")%>/>
												

		<input type='hidden' class='form-control'  name='customLayerId' id = 'customLayerId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.customLayerId + "'"):("'" + "0" + "'")%>/>
												

		<input type='hidden' class='form-control'  name='officeOriginId' id = 'officeOriginId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officeOriginId + "'"):("'" + "0" + "'")%>/>
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.OFFICES_EDIT_OFFICENAMEENG, loginDTO)):(LM.getText(LC.OFFICES_ADD_OFFICENAMEENG, loginDTO))%>
	<span class="required"> * </span>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'officeNameEng_div_<%=i%>'>	
		<input type='text' class='form-control'  name='officeNameEng' id = 'officeNameEng_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officeNameEng + "'"):("''")%> required="required"
  />					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.OFFICES_EDIT_OFFICENAMEBNG, loginDTO)):(LM.getText(LC.OFFICES_ADD_OFFICENAMEBNG, loginDTO))%>
	<span class="required"> * </span>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'officeNameBng_div_<%=i%>'>	
		<input type='text' class='form-control'  name='officeNameBng' id = 'officeNameBng_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officeNameBng + "'"):("''")%> required="required"
  />					
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='geoDivisionId' id = 'geoDivisionId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoDivisionId + "'"):("'" + "0" + "'")%>/>
												

		<input type='hidden' class='form-control'  name='geoDistrictId' id = 'geoDistrictId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoDistrictId + "'"):("'" + "0" + "'")%>/>
												

		<input type='hidden' class='form-control'  name='geoUpazilaId' id = 'geoUpazilaId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoUpazilaId + "'"):("'" + "0" + "'")%>/>
												

		<input type='hidden' class='form-control'  name='geoUnionId' id = 'geoUnionId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoUnionId + "'"):("'" + "0" + "'")%>/>
												

		<input type='hidden' class='form-control'  name='geoCityCorporationId' id = 'geoCityCorporationId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoCityCorporationId + "'"):("'" + "0" + "'")%>/>
												

		<input type='hidden' class='form-control'  name='geoCityCorporationWardId' id = 'geoCityCorporationWardId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoCityCorporationWardId + "'"):("'" + "0" + "'")%>/>
												

		<input type='hidden' class='form-control'  name='geoMunicipalityId' id = 'geoMunicipalityId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoMunicipalityId + "'"):("'" + "0" + "'")%>/>
												

		<input type='hidden' class='form-control'  name='geoMunicipalityWardId' id = 'geoMunicipalityWardId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoMunicipalityWardId + "'"):("'" + "0" + "'")%>/>
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.OFFICES_EDIT_OFFICECODE, loginDTO)):(LM.getText(LC.OFFICES_ADD_OFFICECODE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'officeCode_div_<%=i%>'>	
		<input type='text' class='form-control'  name='officeCode' id = 'officeCode_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officeCode + "'"):("'" + " " + "'")%>   />					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.OFFICES_EDIT_OFFICEADDRESS, loginDTO)):(LM.getText(LC.OFFICES_ADD_OFFICEADDRESS, loginDTO))%>
	<span class="required"> * </span>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'officeAddress_div_<%=i%>'>	
		<div id ='officeAddress_geoDIV_<%=i%>'>
			<select class='form-control' name='officeAddress_active' id = 'officeAddress_geoSelectField_<%=i%>' onChange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'officeAddress_geoDIV_<%=i%>', 'officeAddress_geolocation_<%=i%>')" required="required"  pattern="^((?!select division).)*$" title="officeAddress must be selected"
></select>
		</div>
		<input type='text' class='form-control' onkeypress="return (event.charCode != 36 && event.keyCode != 36)" name='officeAddress_text' id = 'officeAddress_geoTextField_<%=i%>' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(officesDTO.officeAddress)  + "'"):("''")%> placeholder='Road Number, House Number etc'>
		<input type='hidden' class='form-control'  name='officeAddress' id = 'officeAddress_geolocation_<%=i%>' value=<%=actionName.equals("edit")?("'" + "1" + "'"):("''")%>>
						
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.OFFICES_EDIT_DIGITALNOTHICODE, loginDTO)):(LM.getText(LC.OFFICES_ADD_DIGITALNOTHICODE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'digitalNothiCode_div_<%=i%>'>	
		<input type='text' class='form-control'  name='digitalNothiCode' id = 'digitalNothiCode_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.digitalNothiCode + "'"):("'" + " " + "'")%>   />					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.OFFICES_EDIT_REFERENCECODE, loginDTO)):(LM.getText(LC.OFFICES_ADD_REFERENCECODE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'referenceCode_div_<%=i%>'>	
		<input type='text' class='form-control'  name='referenceCode' id = 'referenceCode_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.referenceCode + "'"):("'" + " " + "'")%>   />					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.OFFICES_EDIT_OFFICEPHONE, loginDTO)):(LM.getText(LC.OFFICES_ADD_OFFICEPHONE, loginDTO))%>
	<span class="required"> * </span>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'officePhone_div_<%=i%>'>	
		<input type='text' class='form-control'  name='officePhone' id = 'officePhone_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officePhone + "'"):("''")%> required="required"  pattern="880[0-9]{10}" title="officePhone must start with 880, then contain 10 digits"
  />					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.OFFICES_EDIT_OFFICEMOBILE, loginDTO)):(LM.getText(LC.OFFICES_ADD_OFFICEMOBILE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'officeMobile_div_<%=i%>'>	
		<input type='text' class='form-control'  name='officeMobile' id = 'officeMobile_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officeMobile + "'"):("'" + " " + "'")%> required="required"  pattern="880[0-9]{10}" title="officeMobile must start with 880, then contain 10 digits"
  />					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.OFFICES_EDIT_OFFICEFAX, loginDTO)):(LM.getText(LC.OFFICES_ADD_OFFICEFAX, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'officeFax_div_<%=i%>'>	
		<input type='text' class='form-control'  name='officeFax' id = 'officeFax_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officeFax + "'"):("'" + " " + "'")%>   />					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.OFFICES_EDIT_OFFICEEMAIL, loginDTO)):(LM.getText(LC.OFFICES_ADD_OFFICEEMAIL, loginDTO))%>
	<span class="required"> * </span>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'officeEmail_div_<%=i%>'>	
		<input type='text' class='form-control'  name='officeEmail' id = 'officeEmail_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officeEmail + "'"):("''")%> required="required"  pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$" title="officeEmail must be a of valid email address format"
  />					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.OFFICES_EDIT_OFFICEWEB, loginDTO)):(LM.getText(LC.OFFICES_ADD_OFFICEWEB, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'officeWeb_div_<%=i%>'>	
		<input type='url' class='form-control'  name='officeWeb' id = 'officeWeb_url_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officeWeb + "'"):("'" + "" + "'")%>   />					
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='parentOfficeId' id = 'parentOfficeId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.parentOfficeId + "'"):("'" + "0" + "'")%>/>
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.OFFICES_EDIT_STATUS, loginDTO)):(LM.getText(LC.OFFICES_ADD_STATUS, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'status_div_<%=i%>'>	
		<textarea class='form-control'  name='status' id = 'status_textarea_<%=i%>'  ><%=actionName.equals("edit")?(officesDTO.status):("false")%></textarea>		
											
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.OFFICES_EDIT_CREATEDBY, loginDTO)):(LM.getText(LC.OFFICES_ADD_CREATEDBY, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'createdBy_div_<%=i%>'>	
		<input type='text' class='form-control'  name='createdBy' id = 'createdBy_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.createdBy + "'"):("'" + "0" + "'")%>   />					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.OFFICES_EDIT_MODIFIEDBY, loginDTO)):(LM.getText(LC.OFFICES_ADD_MODIFIEDBY, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'modifiedBy_div_<%=i%>'>	
		<input type='text' class='form-control'  name='modifiedBy' id = 'modifiedBy_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.modifiedBy + "'"):("'" + "0" + "'")%>   />					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.OFFICES_EDIT_CREATED, loginDTO)):(LM.getText(LC.OFFICES_ADD_CREATED, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'created_div_<%=i%>'>	
		<input type='text' class='form-control'  name='created' id = 'created_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.created + "'"):("'" + " " + "'")%>   />					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.OFFICES_EDIT_MODIFIED, loginDTO)):(LM.getText(LC.OFFICES_ADD_MODIFIED, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'modified_div_<%=i%>'>	
		<input type='text' class='form-control'  name='modified' id = 'modified_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.modified + "'"):("'" + " " + "'")%>   />					
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='geoThanaId' id = 'geoThanaId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoThanaId + "'"):("'" + "0" + "'")%>/>
												

		<input type='hidden' class='form-control'  name='geoPostofficeId' id = 'geoPostofficeId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoPostofficeId + "'"):("'" + "0" + "'")%>/>
												

		<input type='hidden' class='form-control'  name='geoCountryId' id = 'geoCountryId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoCountryId + "'"):("'" + "0" + "'")%>/>
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.OFFICES_EDIT_APPROVALPATHTYPE, loginDTO)):(LM.getText(LC.OFFICES_ADD_APPROVALPATHTYPE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'approvalPathType_div_<%=i%>'>	
		<select class='form-control'  name='approvalPathType' id = 'approvalPathType_select_<%=i%>'  >
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "approval_path", "approvalPathType_select_" + i, "form-control", "approvalPathType", officesDTO.approvalPathType + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "approval_path", "approvalPathType_select_" + i, "form-control", "approvalPathType" );			
}
out.print(Options);
%>
		</select>
		
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + officesDTO.isDeleted + "'"):("'" + "false" + "'")%>/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.lastModificationTime + "'"):("'" + "0" + "'")%>/>
												
					
	







				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.OFFICES_EDIT_OFFICES_CANCEL_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.OFFICES_ADD_OFFICES_CANCEL_BUTTON, loginDTO));
					}
					
					%>
					</a>
					<button class="btn btn-success" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.OFFICES_EDIT_OFFICES_SUBMIT_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.OFFICES_ADD_OFFICES_SUBMIT_BUTTON, loginDTO));
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


function getOfficer(officer_id, officer_select)
{
	console.log("getting officer");
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{
			if(this.responseText.includes('option'))
			{
				console.log("got response for officer");
				document.getElementById(officer_select).innerHTML = this.responseText ;
				
				if(document.getElementById(officer_select).length > 1)
				{
					document.getElementById(officer_select).removeAttribute("disabled");
				}	
			}
			else
			{
				console.log("got errror response for officer");
			}
			
		}
		else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	};
	xhttp.open("POST", "OfficesServlet?actionType=getGRSOffice&officer_id=" + officer_id, true);
	xhttp.send();
}


function getLayer(layernum, layerID, childLayerID, selectedValue)
{
	console.log("getting layer");
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{
			if(this.responseText.includes('option'))
			{
				console.log("got response");
				document.getElementById(childLayerID).innerHTML = this.responseText ;
			}
			else
			{
				console.log("got errror response");
			}
			
		}
		else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	};
	xhttp.open("POST", "OfficesServlet?actionType=getGRSLayer&layernum=" + layernum + "&layerID=" 
			+ layerID + "&childLayerID=" + childLayerID + "&selectedValue=" + selectedValue, true);
	xhttp.send();
}

function layerselected(layernum, layerID, childLayerID, hiddenInput, hiddenInputForTopLayer, officerElement)
{
	var layervalue = document.getElementById(layerID).value;
	console.log("layervalue = " + layervalue);
	document.getElementById(hiddenInput).value = layervalue;
	if(layernum == 0)
	{
		document.getElementById(hiddenInputForTopLayer).value = layervalue;
	}
	if(layernum == 0 || (layernum == 1 && document.getElementById(hiddenInputForTopLayer).value == 3))
	{
		document.getElementById(childLayerID).setAttribute("style", "display: inline;");
		getLayer(layernum, layerID, childLayerID, layervalue);
	}
	
	if(officerElement !== null)
	{
		getOfficer(layervalue, officerElement);
	}
	
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;
		if(!document.getElementById('officeNameEng_text_' + row).checkValidity())
		{
			empty_fields += "'officeNameEng'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(!document.getElementById('officeNameBng_text_' + row).checkValidity())
		{
			empty_fields += "'officeNameBng'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(!document.getElementById('officeAddress_geolocation_' + row).checkValidity())
		{
			empty_fields += "'officeAddress'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(!document.getElementById('officePhone_text_' + row).checkValidity())
		{
			empty_fields += "'officePhone'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(!document.getElementById('officeEmail_text_' + row).checkValidity())
		{
			empty_fields += "'officeEmail'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}

	document.getElementById('officeAddress_geolocation_' + row).value = document.getElementById('officeAddress_geolocation_' + row).value + ":" + document.getElementById('officeAddress_geoTextField_' + row).value;
	console.log("geo value = " + document.getElementById('officeAddress_geolocation_' + row).value);
	return true;
}

function PostprocessAfterSubmiting(row)
{
	document.getElementById('officeAddress_geolocation_' + row).value = "1";
}

function addrselected(value, htmlID, selectedIndex, tagname, geodiv, hiddenfield)
{
	console.log('geodiv = ' + geodiv + ' hiddenfield = ' + hiddenfield);
	try 
	{
		var elements, ids;
		elements = document.getElementById(geodiv).children;
		
		document.getElementById(hiddenfield).value = value;
		
		ids = '';
		for(var i = elements.length - 1; i >= 0; i--) 
		{
			var elemID = elements[i].id;
			if(elemID.includes(htmlID) && elemID > htmlID)
			{
				ids += elements[i].id + ' ';
				
				for(var j = elements[i].options.length - 1; j >= 0; j--)
				{
				
					elements[i].options[j].remove();
				}
				elements[i].remove();
				
			}
		}
				

		var newid = htmlID + '_1';

		document.getElementById(geodiv).innerHTML += "<select class='form-control' name='" + tagname + "' id = '" + newid 
		+ "' onChange=\"addrselected(this.value, this.id, this.selectedIndex, this.name, '" + geodiv +"', '" + hiddenfield +"')\"></select>";
		//console.log('innerHTML= ' + document.getElementById(geodiv).innerHTML);
		document.getElementById(htmlID).options[0].innerHTML = document.getElementById(htmlID).options[selectedIndex].innerHTML;
		document.getElementById(htmlID).options[0].value = document.getElementById(htmlID).options[selectedIndex].value;
		//console.log('innerHTML again = ' + document.getElementById(geodiv).innerHTML);
		
		
		
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() 
		{
			if (this.readyState == 4 && this.status == 200) 
			{
				if(!this.responseText.includes('option'))
				{
					document.getElementById(newid).remove();
				}
				else
				{
					document.getElementById(newid).innerHTML = this.responseText ;
				}
				
			}
			else if(this.readyState == 4 && this.status != 200)
			{
				alert('failed ' + this.status);
			}
		};
		 
		xhttp.open("POST", "OfficesServlet?actionType=getGeo&myID="+value, true);
		xhttp.send();
	}
	catch(err) 
	{
		alert("got error: " + err);
	}	  

}

function addHTML(id, HTML)
{
	document.getElementById(id).innerHTML += HTML;
}

function getRequests() 
{
    var s1 = location.search.substring(1, location.search.length).split('&'),
        r = {}, s2, i;
    for (i = 0; i < s1.length; i += 1) {
        s2 = s1[i].split('=');
        r[decodeURIComponent(s2[0]).toLowerCase()] = decodeURIComponent(s2[1]);
    }
    return r;
}

function Request(name){
    return getRequests()[name.toLowerCase()];
}

function ShowExcelParsingResult(suffix)
{
	var failureMessage = document.getElementById("failureMessage_" + suffix);
	if(failureMessage == null)
	{
		console.log("failureMessage_" + suffix + " not found");
	}
	console.log("value = " + failureMessage.value);
	if(failureMessage != null &&  failureMessage.value != "")
	{
		alert("Excel uploading result:" + failureMessage.value);
	}
}

function init(row)
{
		
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{
	    	document.getElementById('officeAddress_geoSelectField_' + row).innerHTML = this.responseText ;
		}
	    else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	 };
	xhttp.open("POST", "OfficesServlet?actionType=getGeo&myID=1", true);
	xhttp.send();
}var row = 0;
bkLib.onDomLoaded(function() 
{	
});
	
window.onload =function ()
{
	init(row);
}



	function SetCheckBoxValues(tablename)
	{
		var i = 0;
		console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
		var element = document.getElementById(tablename);

		var j = 0;
		for(i = 0; i < document.getElementById(tablename).childNodes.length; i ++)
		{
			var tr = document.getElementById(tablename).childNodes[i];
			if(tr.nodeType === Node.ELEMENT_NODE)
			{
				console.log("tr.childNodes.length= " + tr.childNodes.length);
				var checkbox = tr.querySelector('input[type="checkbox"]');				
				checkbox.id = tablename + "_cb_" + j;
				j ++;
			}
			
		}
	}





</script>






