<%@page pageEncoding="UTF-8" %>

<%@page import="offices.OfficesDTO"%>
<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.OFFICES_EDIT_LANGUAGE, loginDTO);

OfficesDTO row = (OfficesDTO)request.getAttribute("officesDTO");
if(row == null)
{
	row = new OfficesDTO();
	
}
System.out.println("row = " + row);


int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";
}
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value='" + failureMessage + "'/></td>");

String value = "";


									
		
									
									out.println("<td id = '" + i + "_officeMinistryId'>");
									value = row.officeMinistryId + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_officeLayerId'>");
									value = row.officeLayerId + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_customLayerId'>");
									value = row.customLayerId + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_officeOriginId'>");
									value = row.officeOriginId + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_officeNameEng'>");
									value = row.officeNameEng + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_officeNameBng'>");
									value = row.officeNameBng + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_geoDivisionId'>");
									value = row.geoDivisionId + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_geoDistrictId'>");
									value = row.geoDistrictId + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_geoUpazilaId'>");
									value = row.geoUpazilaId + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_geoUnionId'>");
									value = row.geoUnionId + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_geoCityCorporationId'>");
									value = row.geoCityCorporationId + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_geoCityCorporationWardId'>");
									value = row.geoCityCorporationWardId + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_geoMunicipalityId'>");
									value = row.geoMunicipalityId + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_geoMunicipalityWardId'>");
									value = row.geoMunicipalityWardId + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_officeCode'>");
									value = row.officeCode + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_officeAddress'>");
									value = row.officeAddress + "";
									out.println(GeoLocationDAO2.parseEnText(value));
									{
										String addressdetails = GeoLocationDAO2.parseDetails(value);
										if(!addressdetails.equals(""))
										{
											out.println(", " + addressdetails);
										}
									}
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_digitalNothiCode'>");
									value = row.digitalNothiCode + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_referenceCode'>");
									value = row.referenceCode + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_officePhone'>");
									value = row.officePhone + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_officeMobile'>");
									value = row.officeMobile + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_officeFax'>");
									value = row.officeFax + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_officeEmail'>");
									value = row.officeEmail + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_officeWeb'>");
									value = row.officeWeb + "";
									out.println("<a href='OfficesServlet?actionType=getURL&URL=" + value + "'>Link</a>");
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_parentOfficeId'>");
									value = row.parentOfficeId + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_status'>");
									value = row.status + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_createdBy'>");
									value = row.createdBy + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_modifiedBy'>");
									value = row.modifiedBy + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_created'>");
									value = row.created + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_modified'>");
									value = row.modified + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_geoThanaId'>");
									value = row.geoThanaId + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_geoPostofficeId'>");
									value = row.geoPostofficeId + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_geoCountryId'>");
									value = row.geoCountryId + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
									
									out.println("<td id = '" + i + "_approvalPathType'>");
									value = row.approvalPathType + "";
									
									value = CommonDAO.getName(Integer.parseInt(value), "approval_path", Language.equals("English")?"name_en":"name_bn", "id");
												
									out.println(value);
		
	
									out.println("</td>");
		
									
		
									
									out.println("<td id = '" + i + "_lastModificationTime'>");
									value = row.lastModificationTime + "";
												
									out.println(value);
		
	
									out.println("</td>");
		
	

	

									
									String onclickFunc = "\"fixedToEditable(" + i + ",'" + deletedStyle + "', '" + row.iD + "' )\"";										
	
									out.println("<td id = '" + i + "_Edit'>");										
									out.println("<a onclick=" + onclickFunc + ">" + LM.getText(LC.OFFICES_SEARCH_OFFICES_EDIT_BUTTON, loginDTO) + "</a>");
								
									out.println("</td>");
									
									
									
									out.println("<td>");
									out.println("<div class='checker'>");
									out.println("<span id='chkEdit' ><input type='checkbox' name='ID' value='" + row.iD + "'/></span>");
									out.println("</div");
									out.println("</td>");
%>

