<%@page import="language.LC"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="language.LM"%>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator"%>
<%@ page import="java.util.Arrays"%>
<%@ page import="searchform.SearchForm"%>
<%@ page import="pb.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>


<%
	System.out.println("Inside nav.jsp");
	String url = request.getParameter("url");
	String navigator = request.getParameter("navigator");
	String pageName = request.getParameter("pageName");
	if (pageName == null)
		pageName = "Search";
	String pageno = "";
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
	pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

	System.out.println("rn " + rn);

	String action = url;
	String context = "../../.." + request.getContextPath() + "/";
	String link = context + url;
	String concat = "?";
	if (url.contains("?")) {
		concat = "&";
	}
	String[][] searchFieldInfo = rn.getSearchFieldInfo();
	String totalPage = "1";
	if (rn != null)
		totalPage = rn.getTotalPages() + "";
	int row = 0;

	String Language = LM.getText(LC.OFFICES_EDIT_LANGUAGE, loginDTO);
	String Options;
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	Date date = new Date();
	String datestr = dateFormat.format(date);
	int pagination_number = 0;
%>






<!-- search control -->
<div class="portlet box portlet-btcl">
	<div class="portlet-title">
		<div class="caption" style="margin-top: 5px;"><i class="fa fa-search-plus"  style="margin-top:-3px"></i><%=pageName%></div>
		<p class="desktop-only" style="float:right; margin:10px 5px !important;">Advanced Search</p>
		<div class="tools">
			<a class="expand" href="javascript:" data-original-title="" title=""></a>
		</div>
		
		<div class="col-xs-12 col-sm-5 col-md-4" style="margin-top:10px">
		<%
			
			out.println("<input type='text' class='form-control' onKeyUp='anyfield_changed(\"\")' id='anyfield'  name='"+  LM.getText(LC.OFFICES_SEARCH_ANYFIELD, loginDTO) +"' ");
			String value = (String)session.getAttribute(searchFieldInfo[searchFieldInfo.length - 1][1]);
			
			if( value != null)
			{
				out.println("value = '" + value + "'");
			}
			
			out.println ("/><br />");
		%> 
		</div>
		
		
		
	</div>

	<div class="portlet-body form collapse">
		<!-- BEGIN FORM-->
		<div class="container-fluid">
			<div class="row col-lg-offset-1">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_OFFICEMINISTRYID, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="office_ministry_id" placeholder="" name="office_ministry_id" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_OFFICELAYERID, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="office_layer_id" placeholder="" name="office_layer_id" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_CUSTOMLAYERID, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="custom_layer_id" placeholder="" name="custom_layer_id" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_OFFICEORIGINID, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="office_origin_id" placeholder="" name="office_origin_id" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_OFFICENAMEENG, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="office_name_eng" placeholder="" name="office_name_eng" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_OFFICENAMEBNG, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="office_name_bng" placeholder="" name="office_name_bng" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_GEODIVISIONID, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="geo_division_id" placeholder="" name="geo_division_id" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_GEODISTRICTID, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="geo_district_id" placeholder="" name="geo_district_id" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_GEOUPAZILAID, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="geo_upazila_id" placeholder="" name="geo_upazila_id" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_GEOUNIONID, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="geo_union_id" placeholder="" name="geo_union_id" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_GEOCITYCORPORATIONID, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="geo_city_corporation_id" placeholder="" name="geo_city_corporation_id" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_GEOCITYCORPORATIONWARDID, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="geo_city_corporation_ward_id" placeholder="" name="geo_city_corporation_ward_id" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_GEOMUNICIPALITYID, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="geo_municipality_id" placeholder="" name="geo_municipality_id" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_GEOMUNICIPALITYWARDID, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="geo_municipality_ward_id" placeholder="" name="geo_municipality_ward_id" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_OFFICECODE, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="office_code" placeholder="" name="office_code" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_OFFICEADDRESS, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="office_address" placeholder="" name="office_address" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_DIGITALNOTHICODE, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="digital_nothi_code" placeholder="" name="digital_nothi_code" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_REFERENCECODE, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="reference_code" placeholder="" name="reference_code" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_OFFICEPHONE, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="office_phone" placeholder="" name="office_phone" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_OFFICEMOBILE, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="office_mobile" placeholder="" name="office_mobile" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_OFFICEFAX, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="office_fax" placeholder="" name="office_fax" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_OFFICEEMAIL, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="office_email" placeholder="" name="office_email" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_OFFICEWEB, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="office_web" placeholder="" name="office_web" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_PARENTOFFICEID, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="parent_office_id" placeholder="" name="parent_office_id" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_STATUS, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="status" placeholder="" name="status" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_CREATEDBY, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="created_by" placeholder="" name="created_by" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_MODIFIEDBY, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="modified_by" placeholder="" name="modified_by" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_CREATED, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="created" placeholder="" name="created" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_MODIFIED, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="modified" placeholder="" name="modified" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_GEOTHANAID, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="geo_thana_id" placeholder="" name="geo_thana_id" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_GEOPOSTOFFICEID, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="geo_postoffice_id" placeholder="" name="geo_postoffice_id" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_GEOCOUNTRYID, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="geo_country_id" placeholder="" name="geo_country_id" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.OFFICES_SEARCH_APPROVALPATHTYPE, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<select class='form-control'  name='approval_path_type' id = 'approval_path_type' onSelect='setSearchChanged()'>
<%
			
			Options = CommonDAO.getOptions(Language, "select", "approval_path", "approvalPathType_select_" + row, "form-control", "approvalPathType", "any" );			
			out.print(Options);
%>
</select>
					</div>
				</div>

			</div>	


			<div class=clearfix></div>

			<div class="form-actions fluid" style="margin-top:10px">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-offset-3 col-xs-12 col-md-12  col-md-12 col-lg-9">							
							<div class="col-xs-4  col-sm-4  col-md-6">
								<input type="hidden" name="search" value="yes" />
								<!-- 				          	<input type="reset" class="btn  btn-sm btn btn-circle  grey-mint btn-outline sbold uppercase" value="Reset" > -->
								<input type="submit" onclick="allfield_changed('',0)"
									class="btn  btn-sm btn btn-circle btn-sm green-meadow btn-outline sbold uppercase"
									value="<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>">
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<!-- END FORM-->
	</div>

<%@include file="../common/pagination_with_go2.jsp"%>



<script type="text/javascript">

	var searchChanged = 0;

	function setValues(x, value)
	{
		for (i = 0; i < x.length; i++)
		{
			 x[i].value = value;
		}
	}

	function setPageNoInAllFields(value)
	{
		var x = document.getElementsByName("pageno");
		setValues(x, value);
	}
	
	function setRPPInAllFields(value)
	{
		var x = document.getElementsByName("RECORDS_PER_PAGE");
		setValues(x, value);
	}
	
	function setTotalPageInAllFields(value)
	{
		var x = document.getElementsByName("totalpage");
		setValues(x, value);
	}

	function setPageNo()
	{
		
		setPageNoInAllFields(document.getElementById('hidden_pageno').value);
		setTotalPageInAllFields(document.getElementById('hidden_totalpage').value);
		console.log("totalpage now is " + document.getElementById('hidden_totalpage').value);
		//document.getElementById('totalpage').innerHTML= document.getElementById('hidden_totalpage').value;
	}
	
	function setSearchChanged()
	{
		searchChanged = 1;
	}
	
	function dosubmit(params)
	{
		document.getElementById('tableForm').innerHTML = "Loading ...";
		//alert(params);
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) 
		    {
		    	document.getElementById('tableForm').innerHTML = this.responseText ;
				setPageNo();
				searchChanged = 0;
			}
		    else if(this.readyState == 4 && this.status != 200)
			{
				alert('failed ' + this.status);
			}
		  };
		  
		  xhttp.open("Get", "<%=action%>&" + params, true);
		  xhttp.send();
		
	}
	
	function anyfield_changed(go)
	{	
		var lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
		var totalRecords = document.getElementById('hidden_totalrecords').value;
		var params = 'AnyField=' + document.getElementById('anyfield').value 
		+ '&search=true&ajax=true&RECORDS_PER_PAGE=' + document.getElementsByName('RECORDS_PER_PAGE')[0].value
		+ '&pageno=' + document.getElementsByName('pageno')[0].value
		;
		params += '&TotalRecords=' + totalRecords;
		params += '&lastSearchTime=' + lastSearchTime;
		dosubmit(params);
	}
	function setPageNoAndSubmit(link)
	{
		var value = -1;
		if(link == 1) //next
		{
			value = parseInt(document.getElementsByName('pageno')[0].value, 10) + 1;
		}
		else if (link == 2) //previous
		{
			value = parseInt(document.getElementsByName('pageno')[0].value, 10) - 1;
		}
		else if (link == 3) //last
		{
			value = document.getElementById('hidden_totalpage').value;
		}
		else // 1st
		{
			value = 1
		}
		setPageNoInAllFields(value);
		console.log('pageno = ' + document.getElementsByName('pageno')[0].value);
		allfield_changed('go', 0);
	}
	function allfield_changed(go, pagination_number)
	{
		 var params = 'AnyField=' + document.getElementById('anyfield').value;
		  <%
		  for(int i = 0; i < searchFieldInfo.length - 1; i ++)
		  {
			  out.println("params += '&" +  searchFieldInfo[i][1] + "='+document.getElementById('" 
		  		+ searchFieldInfo[i][1] + "').value");
		  }
		  %>
		  params +=  '&search=true&ajax=true';
		  
		  var pageNo = document.getElementsByName('pageno')[0].value;
		  var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;
		  var totalRecords = document.getElementById('hidden_totalrecords').value;
		  var lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
			
		  if(go !== '' && searchChanged == 0)
		  {
			  console.log("go found");
			  params += '&go=1';
			  pageNo = document.getElementsByName('pageno')[pagination_number].value;
			  rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
			  setPageNoInAllFields(pageNo);
			  setRPPInAllFields(rpp);
		  }
		  params += '&pageno=' + pageNo;
		  params += '&RECORDS_PER_PAGE=' + rpp;
		  params += '&TotalRecords=' + totalRecords;
		  params += '&lastSearchTime=' + lastSearchTime;
		  dosubmit(params);
	
	}

</script>

