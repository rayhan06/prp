<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="offices.OfficesDTO"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="geolocation.GeoLocationDAO2"%>

<%
OfficesDTO officesDTO = (OfficesDTO)request.getAttribute("officesDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(officesDTO == null)
{
	officesDTO = new OfficesDTO();
	
}
System.out.println("officesDTO = " + officesDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";
OfficesDTO row = officesDTO;
%>




























	














<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.OFFICES_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=ID%>'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_officeMinistryId'>")%>
			

		<input type='hidden' class='form-control'  name='officeMinistryId' id = 'officeMinistryId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officeMinistryId + "'"):("'" + "0" + "'")%>/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_officeLayerId'>")%>
			

		<input type='hidden' class='form-control'  name='officeLayerId' id = 'officeLayerId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officeLayerId + "'"):("'" + "0" + "'")%>/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_customLayerId'>")%>
			

		<input type='hidden' class='form-control'  name='customLayerId' id = 'customLayerId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.customLayerId + "'"):("'" + "0" + "'")%>/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_officeOriginId'>")%>
			

		<input type='hidden' class='form-control'  name='officeOriginId' id = 'officeOriginId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officeOriginId + "'"):("'" + "0" + "'")%>/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_officeNameEng'>")%>
			
	
	<div class="form-inline" id = 'officeNameEng_div_<%=i%>'>
		<input type='text' class='form-control'  name='officeNameEng' id = 'officeNameEng_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officeNameEng + "'"):("''")%> required="required"
  />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_officeNameBng'>")%>
			
	
	<div class="form-inline" id = 'officeNameBng_div_<%=i%>'>
		<input type='text' class='form-control'  name='officeNameBng' id = 'officeNameBng_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officeNameBng + "'"):("''")%> required="required"
  />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_geoDivisionId'>")%>
			

		<input type='hidden' class='form-control'  name='geoDivisionId' id = 'geoDivisionId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoDivisionId + "'"):("'" + "0" + "'")%>/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_geoDistrictId'>")%>
			

		<input type='hidden' class='form-control'  name='geoDistrictId' id = 'geoDistrictId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoDistrictId + "'"):("'" + "0" + "'")%>/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_geoUpazilaId'>")%>
			

		<input type='hidden' class='form-control'  name='geoUpazilaId' id = 'geoUpazilaId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoUpazilaId + "'"):("'" + "0" + "'")%>/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_geoUnionId'>")%>
			

		<input type='hidden' class='form-control'  name='geoUnionId' id = 'geoUnionId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoUnionId + "'"):("'" + "0" + "'")%>/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_geoCityCorporationId'>")%>
			

		<input type='hidden' class='form-control'  name='geoCityCorporationId' id = 'geoCityCorporationId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoCityCorporationId + "'"):("'" + "0" + "'")%>/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_geoCityCorporationWardId'>")%>
			

		<input type='hidden' class='form-control'  name='geoCityCorporationWardId' id = 'geoCityCorporationWardId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoCityCorporationWardId + "'"):("'" + "0" + "'")%>/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_geoMunicipalityId'>")%>
			

		<input type='hidden' class='form-control'  name='geoMunicipalityId' id = 'geoMunicipalityId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoMunicipalityId + "'"):("'" + "0" + "'")%>/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_geoMunicipalityWardId'>")%>
			

		<input type='hidden' class='form-control'  name='geoMunicipalityWardId' id = 'geoMunicipalityWardId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoMunicipalityWardId + "'"):("'" + "0" + "'")%>/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_officeCode'>")%>
			
	
	<div class="form-inline" id = 'officeCode_div_<%=i%>'>
		<input type='text' class='form-control'  name='officeCode' id = 'officeCode_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officeCode + "'"):("'" + " " + "'")%>   />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_officeAddress'>")%>
			
	
	<div class="form-inline" id = 'officeAddress_div_<%=i%>'>
		<div id ='officeAddress_geoDIV_<%=i%>'>
			<select class='form-control' name='officeAddress_active' id = 'officeAddress_geoSelectField_<%=i%>' onChange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'officeAddress_geoDIV_<%=i%>', 'officeAddress_geolocation_<%=i%>')" required="required"  pattern="^((?!select division).)*$" title="officeAddress must be selected"
></select>
		</div>
		<input type='text' class='form-control' name='officeAddress_text' id = 'officeAddress_geoTextField_<%=i%>' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(officesDTO.officeAddress)  + "'"):("''")%> placeholder='Road Number, House Number etc'>
		<input type='hidden' class='form-control'  name='officeAddress' id = 'officeAddress_geolocation_<%=i%>' value=<%=actionName.equals("edit")?("'" + "1" + "'"):("''")%>>
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_digitalNothiCode'>")%>
			
	
	<div class="form-inline" id = 'digitalNothiCode_div_<%=i%>'>
		<input type='text' class='form-control'  name='digitalNothiCode' id = 'digitalNothiCode_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.digitalNothiCode + "'"):("'" + " " + "'")%>   />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_referenceCode'>")%>
			
	
	<div class="form-inline" id = 'referenceCode_div_<%=i%>'>
		<input type='text' class='form-control'  name='referenceCode' id = 'referenceCode_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.referenceCode + "'"):("'" + " " + "'")%>   />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_officePhone'>")%>
			
	
	<div class="form-inline" id = 'officePhone_div_<%=i%>'>
		<input type='text' class='form-control'  name='officePhone' id = 'officePhone_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officePhone + "'"):("''")%> required="required"  pattern="880[0-9]{10}" title="officePhone must start with 880, then contain 10 digits"
  />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_officeMobile'>")%>
			
	
	<div class="form-inline" id = 'officeMobile_div_<%=i%>'>
		<input type='text' class='form-control'  name='officeMobile' id = 'officeMobile_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officeMobile + "'"):("'" + " " + "'")%> required="required"  pattern="880[0-9]{10}" title="officeMobile must start with 880, then contain 10 digits"
  />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_officeFax'>")%>
			
	
	<div class="form-inline" id = 'officeFax_div_<%=i%>'>
		<input type='text' class='form-control'  name='officeFax' id = 'officeFax_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officeFax + "'"):("'" + " " + "'")%>   />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_officeEmail'>")%>
			
	
	<div class="form-inline" id = 'officeEmail_div_<%=i%>'>
		<input type='text' class='form-control'  name='officeEmail' id = 'officeEmail_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officeEmail + "'"):("''")%> required="required"  pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$" title="officeEmail must be a of valid email address format"
  />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_officeWeb'>")%>
			
	
	<div class="form-inline" id = 'officeWeb_div_<%=i%>'>
		<input type='url' class='form-control'  name='officeWeb' id = 'officeWeb_url_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.officeWeb + "'"):("'" + "" + "'")%>   />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_parentOfficeId'>")%>
			

		<input type='hidden' class='form-control'  name='parentOfficeId' id = 'parentOfficeId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.parentOfficeId + "'"):("'" + "0" + "'")%>/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_status'>")%>
			
	
	<div class="form-inline" id = 'status_div_<%=i%>'>
		<textarea class='form-control'  name='status' id = 'status_textarea_<%=i%>'  ><%=actionName.equals("edit")?(officesDTO.status):("false")%></textarea>		
											
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_createdBy'>")%>
			
	
	<div class="form-inline" id = 'createdBy_div_<%=i%>'>
		<input type='text' class='form-control'  name='createdBy' id = 'createdBy_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.createdBy + "'"):("'" + "0" + "'")%>   />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy'>")%>
			
	
	<div class="form-inline" id = 'modifiedBy_div_<%=i%>'>
		<input type='text' class='form-control'  name='modifiedBy' id = 'modifiedBy_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.modifiedBy + "'"):("'" + "0" + "'")%>   />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_created'>")%>
			
	
	<div class="form-inline" id = 'created_div_<%=i%>'>
		<input type='text' class='form-control'  name='created' id = 'created_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.created + "'"):("'" + " " + "'")%>   />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modified'>")%>
			
	
	<div class="form-inline" id = 'modified_div_<%=i%>'>
		<input type='text' class='form-control'  name='modified' id = 'modified_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.modified + "'"):("'" + " " + "'")%>   />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_geoThanaId'>")%>
			

		<input type='hidden' class='form-control'  name='geoThanaId' id = 'geoThanaId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoThanaId + "'"):("'" + "0" + "'")%>/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_geoPostofficeId'>")%>
			

		<input type='hidden' class='form-control'  name='geoPostofficeId' id = 'geoPostofficeId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoPostofficeId + "'"):("'" + "0" + "'")%>/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_geoCountryId'>")%>
			

		<input type='hidden' class='form-control'  name='geoCountryId' id = 'geoCountryId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.geoCountryId + "'"):("'" + "0" + "'")%>/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_approvalPathType'>")%>
			
	
	<div class="form-inline" id = 'approvalPathType_div_<%=i%>'>
		<select class='form-control'  name='approvalPathType' id = 'approvalPathType_select_<%=i%>'  >
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "approval_path", "approvalPathType_select_" + i, "form-control", "approvalPathType", officesDTO.approvalPathType + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "approval_path", "approvalPathType_select_" + i, "form-control", "approvalPathType" );			
}
out.print(Options);
%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + officesDTO.isDeleted + "'"):("'" + "false" + "'")%>/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + officesDTO.lastModificationTime + "'"):("'" + "0" + "'")%>/>
												
<%=("</td>")%>
					
		