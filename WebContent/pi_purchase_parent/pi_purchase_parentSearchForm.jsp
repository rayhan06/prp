<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="pi_purchase_parent.*" %>
<%@ page import="util.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page pageEncoding="UTF-8" %>

<%
    String navigator2 = "navPI_PURCHASE_PARENT";
    String servletName = "Pi_purchase_parentServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>
<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.PI_PURCHASE_ADD_FISCALYEARID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_PURCHASE_ADD_VENDORID, loginDTO)%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "ক্রয় আদেশ নম্বর", "Purchase Order Number")%>
            </th>
            <th><%=LM.getText(LC.PI_PURCHASE_ADD_PURCHASEDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<Pi_purchase_parentDTO>) rn2.list;
            try {
                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Pi_purchase_parentDTO pi_purchase_parentDTO = (Pi_purchase_parentDTO) data.get(i);
        %>
        <tr>
            <td>
                <%
                    value = CommonDAO.getName(Language, "fiscal_year", pi_purchase_parentDTO.fiscalYearId);
                %>
                <%=Utils.getDigits(value, Language)%>
            </td>
            <td>
                <%
                    value = CommonDAO.getName(pi_purchase_parentDTO.vendorId, "pi_vendor_auctioneer_details",
                            Language.equals("English") ? "name_en" : "name_bn", "id");
                %>
                <%=Utils.getDigits(value, Language)%>
            </td>
            <td>
                <%
                    value = pi_purchase_parentDTO.purchaseOrderNumber + "";
                %>
                <%=value%>
            </td>
            <td>
                <%
                    value = pi_purchase_parentDTO.purchaseDate + "";
                %>
                <%
                    String formatted_purchaseDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                %>
                <%=Utils.getDigits(formatted_purchaseDate, Language)%>
            </td>

            <%CommonDTO commonDTO = pi_purchase_parentDTO; %>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=view&ID=<%=commonDTO.iD%>'"
                >
                    <i class="fa fa-eye"></i>
                </button>
            </td>
        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>