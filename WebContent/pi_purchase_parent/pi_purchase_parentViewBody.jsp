<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="files.*" %>
<%@page import="util.*" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="procurement_package.Procurement_packageRepository" %>
<%@ page import="procurement_package.ProcurementGoodsTypeRepository" %>
<%@ page import="pi_purchase_parent.Pi_purchase_parentDTO" %>
<%@ page import="pi_purchase_parent.Pi_purchase_parentDAO" %>
<%@ page import="pi_unique_item.PiUniqueItemAssignmentDAO" %>
<%@ page import="pi_unique_item.PiActionTypeEnum" %>
<%@ page import="pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsRepository" %>
<%@ page import="pi_purchase.Pi_purchaseDTO" %>
<%@ page import="user.UserDTO" %>
<%@ page import="util.StringUtils" %>
<%@ page import="pi_purchase.Pi_purchaseDAO" %>
<%@ page import="pi_package_vendor_items.Pi_package_vendor_itemsDTO" %>
<%@ page import="pi_package_vendor_items.Pi_package_vendor_itemsDAO" %>
<%@ page import="procurement_goods.Procurement_goodsDTO" %>
<%@ page import="procurement_goods.Procurement_goodsDAO" %>
<%@ page import="procurement_package.ProcurementGoodsTypeDTO" %>
<%@ page import="procurement_package.Procurement_packageDTO" %>
<%@ page import="pi_unique_item.PiStageEnum" %>
<%@ page import="pi_package_final.Pi_package_finalRepository" %>
<%@ page import="pi_package_final.PiPackageLotFinalRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>

<%
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String servletName = "Pi_purchase_parentServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Pi_purchase_parentDTO mainPurchaseDTO = Pi_purchase_parentDAO.getInstance().getDTOByID(id);
    CommonDTO commonDTO = mainPurchaseDTO;
    String Options = "";
%>
<%@include file="../pb/viewInitializer.jsp" %>

<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PI_PURCHASE_ADD_PI_PURCHASE_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row">
                <div class="mt-5 col-12">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-12">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=getDataByLanguage(Language, "ক্রয় তথ্য", "Purchase Information")%>
                                        </h4>
                                    </div>
                                </div>
                                <input type='hidden' class='form-control' name='mainPurchaseId'
                                       id='mainPurchaseId_hidden_<%=i%>'
                                       value='<%=mainPurchaseDTO.iD%>' tag='pb_html'/>

                                <input type='hidden' class='form-control' name='officeUnitId'
                                       id='officeUnitId_hidden_<%=i%>' value='<%=userDTO.unitID%>'
                                       tag='pb_html'/>
                                <div class="form-group row">
                                    <div class="form-group row col-md-6">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PI_PURCHASE_ADD_FISCALYEARID, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <label class="col-md-4 col-form-label text-md-left">
                                                <input type='hidden' class='form-control' name='fiscalYearId'
                                                       id='fiscalYearId_select_<%=i%>'
                                                       value='<%=mainPurchaseDTO.fiscalYearId%>'
                                                       tag='pb_html'/>
                                                <%
                                                    Options = CommonDAO.getName(Language, "fiscal_year", mainPurchaseDTO.fiscalYearId);
                                                %>
                                                <%=Options%>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-6">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PI_PACKAGE_NEW_ADD_PI_PACKAGE_NEW_ADD_FORMNAME, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <label class="col-md-4 col-form-label text-md-left">
                                                <input type='hidden' class='form-control' name='packageId'
                                                       id='packageId_select_<%=i%>'
                                                       value='<%=mainPurchaseDTO.packageId%>'
                                                       tag='pb_html'/>
                                                <%
                                                    Options = Language.equals("English") ?
                                                            Pi_package_finalRepository.getInstance().getPi_package_finalDTOByiD(mainPurchaseDTO.packageId).packageNumberEn :
                                                            Pi_package_finalRepository.getInstance().getPi_package_finalDTOByiD(mainPurchaseDTO.packageId).packageNumberBn;
                                                %>
                                                <%=Options%>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="form-group row col-md-6">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_MODEL_ADD_LOT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <label class="col-md-4 col-form-label text-md-left">
                                                <input type='hidden' class='form-control' name='lotId'
                                                       id='lot_select_<%=i%>' value='<%=mainPurchaseDTO.lotId%>'
                                                       tag='pb_html'/>
                                                <%
                                                    if (mainPurchaseDTO.lotId != -1) {
                                                        Options = Language.equals("English") ?
                                                                PiPackageLotFinalRepository.getInstance().getPiPackageLotFinalDTOByiD(mainPurchaseDTO.lotId).lotNumberEn :
                                                                PiPackageLotFinalRepository.getInstance().getPiPackageLotFinalDTOByiD(mainPurchaseDTO.lotId).lotNumberBn;
                                                    } else {
                                                        Options = "";
                                                    }

                                                %>
                                                <%=Options%>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-6">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PI_PURCHASE_ADD_VENDORID, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <label class="col-md-4 col-form-label text-md-left">
                                                <input type='hidden' class='form-control' name='vendorId'
                                                       id='vendorId_select_<%=i%>' value='<%=mainPurchaseDTO.vendorId%>'
                                                       tag='pb_html'/>
                                                <%
                                                    Options = Language.equals("English") ?
                                                            Pi_vendor_auctioneer_detailsRepository.getInstance().getPi_vendor_auctioneer_detailsDTOByiD(mainPurchaseDTO.vendorId).nameEn :
                                                            Pi_vendor_auctioneer_detailsRepository.getInstance().getPi_vendor_auctioneer_detailsDTOByiD(mainPurchaseDTO.vendorId).nameBn;
                                                %>
                                                <%=Options%>
                                            </label>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <div class="form-group row col-md-6">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "ক্রয় আদেশ নম্বর", "Purchase Order Number")%>
                                        </label>
                                        <div class="col-md-8">
                                            <label class="col-md-4 col-form-label text-md-left">
                                                <%
                                                    Options = mainPurchaseDTO.purchaseOrderNumber;
                                                    ;
                                                %>
                                                <%=Options%>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-6">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PI_PURCHASE_ADD_SAROKNUMBER, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <label class="col-md-4 col-form-label text-md-left">
                                                <%
                                                    Options = mainPurchaseDTO.sarokNumber;
                                                    ;
                                                %>
                                                <%=Options%>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-6">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PI_PURCHASE_ADD_PURCHASEDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <label class="col-md-4 col-form-label text-md-left">
                                                <%
                                                    value = mainPurchaseDTO.purchaseDate + "";
                                                    String formatted_purchaseDate = StringUtils.convertToDateAndTime(isLanguageEnglish, mainPurchaseDTO.purchaseDate);
                                                %>
                                                <%=Utils.getDigits(formatted_purchaseDate, Language)%>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-6">
                                    </div>
                                </div>
                                <div class="form-group row">

                                    <div class="form-group row col-md-6">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_PIAUCTIONID, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <label class="col-md-4 col-form-label text-md-left">
                                                <%

                                                    Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(mainPurchaseDTO.officeUnitId);

                                                %>
                                                <%=office_unitsDTO != null ? isLanguageEnglish ? office_unitsDTO.unitNameEng : office_unitsDTO.unitNameBng : ""%>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group row col-md-6">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PI_PURCHASE_ADD_REMARKS, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <label class="col-md-4 col-form-label text-md-left">
                                                <%=mainPurchaseDTO.remarks%>
                                            </label>
                                        </div>
                                    </div>


                                </div>

                                <div class="from-group row">

                                    <div class="form-group row col-md-6">
                                        <label class="col-md-4 col-form-label text-md-right"><%=getDataByLanguage(Language, "ক্রয় পদ্ধতি", "Procurement Method")%>
                                        </label>
                                        <div class="col-md-8">
                                            <label class="col-md-4 col-form-label text-md-left">
                                                <%=CatRepository.getInstance().getText(Language, "procurement_method", mainPurchaseDTO.procurementMethod)%>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group row col-md-6">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_INCIDENT_ADD_FILESDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                {
                                                    List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(mainPurchaseDTO.filesDropzone);
                                            %>
                                            <%@include file="../pb/dropzoneViewer.jsp" %>
                                            <%
                                                }
                                            %>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="mt-4">
                <div class="form-body">
                    <h5 class="table-title">
                        <%=LM.getText(LC.PROCUREMENT_GOODS_ADD_GOODS_TYPE, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_PROCUREMENT_PACKAGE_ADD_FORMNAME, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_GOODS_TYPE, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PI_PURCHASE_ADD_ITEMID, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PI_PURCHASE_ADD_CURRENT_STOCK, loginDTO)%>
                                    <%=getDataByLanguage(Language, "(শুধুমাত্র সংশ্লিষ্ট ষ্টোরে)", "(Only In This Store)")%>
                                </th>
                                <th><%=LM.getText(LC.PI_PURCHASE_ADD_AMOUNT, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PI_PURCHASE_ADD_APP_ALLOCATED, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PI_PURCHASE_ADD_APP_REMAINING, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_UNITPRICE, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PI_PURCHASE_ADD_TOTALVALUE, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PI_PURCHASE_ADD_EXPIRYDATE, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="field-Purchase">

                            <%
                                List<Pi_package_vendor_itemsDTO> items = Pi_package_vendor_itemsDAO.getInstance().
                                        getPriceByFiscalYearIdAndOfficeUnitIdAndPackageIdAndVendorId(mainPurchaseDTO.fiscalYearId,
                                                mainPurchaseDTO.officeUnitId, mainPurchaseDTO.packageId, mainPurchaseDTO.vendorId);

                                List<Pi_purchaseDTO> purchaseDTOS = (List<Pi_purchaseDTO>) Pi_purchaseDAO.getInstance()
                                        .getDTOsByParent("pi_purchase_parent_id", mainPurchaseDTO.iD);

                                int childTableStartingID = 0;

                                HashMap<Long, Pi_purchaseDTO> itemIdToPurchaseMap = new HashMap<>();

                                purchaseDTOS
                                        .forEach(purchaseDTO -> {
                                            itemIdToPurchaseMap.put(purchaseDTO.itemId, purchaseDTO);
                                        });

                                for (Pi_purchaseDTO purchaseDTO : purchaseDTOS) {


                                    Procurement_goodsDTO itemDTO = Procurement_goodsDAO.getInstance().getDTOByID(purchaseDTO.itemId);
                                    ProcurementGoodsTypeDTO goodsTypeDTO = ProcurementGoodsTypeRepository.getInstance().getProcurementGoodsTypeDTOByID(itemDTO.procurementGoodsTypeId);
                                    Procurement_packageDTO packageDTO = Procurement_packageRepository.getInstance().getProcurement_packageDTOByID(goodsTypeDTO.procurementPackageId);
                                    String packageName = Language.equals("English") ?
                                            packageDTO.nameEn : packageDTO.nameBn;
                                    String typeName = Language.equals("English") ?
                                            goodsTypeDTO.nameEn : goodsTypeDTO.nameBn;
                                    String itemName = Language.equals("English") ?
                                            itemDTO.nameEn : itemDTO.nameBn;


                                    //  current stock calculation starts

                                    int initialStockInt = PiUniqueItemAssignmentDAO.getInstance().getTotalItemCountWithStageAllOver(purchaseDTO.officeUnitId, purchaseDTO.itemId,
                                            PiStageEnum.IN_STOCK.getValue());
                                    String remainingStockAmount = Utils.getDigits(initialStockInt, "English");

                                    //  current stock calculation ends


                                    //  APP calculation starts

                                    int currentYearStockInt = PiUniqueItemAssignmentDAO.getInstance().getItemsCountWithoutStore(purchaseDTO.fiscalYearId, purchaseDTO.itemId,
                                            PiActionTypeEnum.PURCHASE.getValue());
                                    double appAllocated = purchaseDTO.allocatedApp;
                                    double appAllocatedRemaining = appAllocated - (currentYearStockInt * 1.0);

                                    String appAllocatedString = Utils.getDigits(appAllocated, "English");
                                    String appAllocatedRemainingString = Utils.getDigits(appAllocatedRemaining, "English");

                                    //  APP calculation ends


                                    // price calculation starts

                                    double unitPrice = purchaseDTO.unitPrice;
                                    String unitPriceString = Utils.getDigits(unitPrice, "English");

                                    // price calculation ends


                                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                            %>
                            <tr>
                                <td>

                                    <%=packageName%>

                                </td>
                                <td>

                                    <%=typeName%>

                                </td>
                                <td>

                                    <%=itemName%>

                                </td>
                                <td>

                                    <%=Utils.getDigits(remainingStockAmount, Language)%>

                                </td>
                                <td>

                                    <%=Utils.getDigits(purchaseDTO.amount, Language)%>

                                </td>
                                <td>

                                    <%=Utils.getDigits(appAllocatedString, Language)%>

                                </td>
                                <td>

                                    <%=Utils.getDigits(appAllocatedRemainingString, Language)%>

                                </td>
                                <td>
                                    <%=Utils.getDigits(unitPriceString, Language)%>

                                </td>
                                <td>

                                    <%=Utils.getDigits(purchaseDTO.totalBill, Language)%>

                                </td>
                                <td>
                                    <%
                                        String formatted_expiryDateDate = simpleDateFormat.format(new Date(purchaseDTO.expiryDate));
                                    %>

                                    <%=Utils.getDigits(formatted_expiryDateDate, Language)%>

                                </td>
                            </tr>
                            <%

                                }

                            %>

                            </tbody>
                        </table>
                    </div>


                </div>
            </div>

        </div>
    </div>
</div>