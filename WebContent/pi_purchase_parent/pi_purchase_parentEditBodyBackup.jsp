<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="pi_purchase_parent.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
Pi_purchase_parentDTO pi_purchase_parentDTO = new Pi_purchase_parentDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	pi_purchase_parentDTO = Pi_purchase_parentDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = pi_purchase_parentDTO;
String tableName = "pi_purchase_parent";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.PI_PURCHASE_PARENT_ADD_PI_PURCHASE_PARENT_ADD_FORMNAME, loginDTO);
String servletName = "Pi_purchase_parentServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Pi_purchase_parentServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=pi_purchase_parentDTO.iD%>' tag='pb_html'/>
	
														<input type='hidden' class='form-control'  name='officeUnitId' id = 'officeUnitId_hidden_<%=i%>' value='<%=pi_purchase_parentDTO.officeUnitId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='fiscalYearId' id = 'fiscalYearId_hidden_<%=i%>' value='<%=pi_purchase_parentDTO.fiscalYearId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='packageId' id = 'packageId_hidden_<%=i%>' value='<%=pi_purchase_parentDTO.packageId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='vendorId' id = 'vendorId_hidden_<%=i%>' value='<%=pi_purchase_parentDTO.vendorId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_PARENT_ADD_SAROKNUMBER, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='sarokNumber' id = 'sarokNumber_text_<%=i%>' value='<%=pi_purchase_parentDTO.sarokNumber%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_PARENT_ADD_PURCHASEDATE, loginDTO)%></label>
                                                            <div class="col-8">
																<%value = "purchaseDate_js_" + i;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='purchaseDate' id = 'purchaseDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(pi_purchase_parentDTO.purchaseDate))%>' tag='pb_html'>
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_PARENT_ADD_REMARKS, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='remarks' id = 'remarks_text_<%=i%>' value='<%=pi_purchase_parentDTO.remarks%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_PARENT_ADD_FILESDROPZONE, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																fileColumnName = "filesDropzone";
																if(actionName.equals("ajax_edit"))
																{
																	List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(pi_purchase_parentDTO.filesDropzone);
																%>			
																	<%@include file="../pb/dropzoneEditor.jsp"%>
																<%
																}
																else
																{
																	ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
																	pi_purchase_parentDTO.filesDropzone = ColumnID;
																}
																%>
				
																<div class="dropzone" action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=pi_purchase_parentDTO.filesDropzone%>">
																	<input type='file' style="display:none"  name='<%=fileColumnName%>File' id = '<%=fileColumnName%>_dropzone_File_<%=i%>'  tag='pb_html'/>			
																</div>								
																<input type='hidden'  name='<%=fileColumnName%>FilesToDelete' id = '<%=fileColumnName%>FilesToDelete_<%=i%>' value=''  tag='pb_html'/>
																<input type='hidden' name='<%=fileColumnName%>' id = '<%=fileColumnName%>_dropzone_<%=i%>'  tag='pb_html' value='<%=pi_purchase_parentDTO.filesDropzone%>'/>		
		


															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_PARENT_ADD_INSERTEDBY, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='insertedBy' id = 'insertedBy_text_<%=i%>' value='<%=pi_purchase_parentDTO.insertedBy%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_PARENT_ADD_INSERTIONTIME, loginDTO)%></label>
                                                            <div class="col-8">
																<%value = "insertionTime_js_" + i;%>
																<jsp:include page="/time/time.jsp">
																   <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
																   <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																   <jsp:param name="IS_AMPM" value="true"></jsp:param>
																</jsp:include>
																<input type='hidden' value="<%=pi_purchase_parentDTO.insertionTime%>" name='insertionTime' id = 'insertionTime_time_<%=i%>'  tag='pb_html'/>			
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=pi_purchase_parentDTO.isDeleted%>' tag='pb_html'/>
											
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_PARENT_ADD_MODIFIEDBY, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='modifiedBy' id = 'modifiedBy_text_<%=i%>' value='<%=pi_purchase_parentDTO.modifiedBy%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=pi_purchase_parentDTO.lastModificationTime%>' tag='pb_html'/>
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.PI_PURCHASE_PARENT_ADD_PI_PURCHASE_PARENT_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.PI_PURCHASE_PARENT_ADD_PI_PURCHASE_PARENT_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);
	preprocessDateBeforeSubmitting('purchaseDate', row);
	preprocessTimeBeforeSubmitting('insertionTime', row);

	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Pi_purchase_parentServlet");	
}

function init(row)
{

	setDateByStringAndId('purchaseDate_js_' + row, $('#purchaseDate_date_' + row).val());
	setTimeById('insertionTime_js_' + row, $('#insertionTime_time_' + row).val(), true);

	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;



</script>






