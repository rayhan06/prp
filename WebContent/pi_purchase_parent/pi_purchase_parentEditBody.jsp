<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="pi_purchase.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="procurement_package.Procurement_packageRepository" %>
<%@ page import="procurement_package.ProcurementGoodsTypeRepository" %>
<%@ page import="pi_purchase_parent.Pi_purchase_parentDTO" %>
<%@ page import="pi_purchase_parent.Pi_purchase_parentDAO" %>
<%@ page import="pi_unique_item.PiUniqueItemAssignmentDAO" %>
<%@ page import="pi_unique_item.PiActionTypeEnum" %>
<%@ page import="pi_app_request_details.Pi_app_request_detailsDTO" %>
<%@ page import="pi_app_request.Pi_app_requestDAO" %>
<%@ page import="pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsRepository" %>
<%@ page import="pi_package_new.Pi_package_newRepository" %>
<%@ page import="pi_package_final.Pi_package_finalRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>

<%
    List<Pi_purchaseDTO> purchaseDTOS = new ArrayList<>();
    Pi_purchase_parentDTO mainPurchaseDTO = new Pi_purchase_parentDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        mainPurchaseDTO = Pi_purchase_parentDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = mainPurchaseDTO;
    String tableName = "pi_purchase";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    long officeUnitId = userDTO.unitID;


    if (actionName.equals("ajax_edit")) {
        purchaseDTOS = (List<Pi_purchaseDTO>) Pi_purchaseDAO.getInstance().getDTOsByParent("pi_purchase_parent_id", mainPurchaseDTO.iD);
        officeUnitId = mainPurchaseDTO.officeUnitId;
    }

    String formTitle = LM.getText(LC.PI_PURCHASE_ADD_PI_PURCHASE_ADD_FORMNAME, loginDTO);
    String servletName = "Pi_purchase_parentServlet";
    String context = request.getContextPath() + "/";

    Calendar c = Calendar.getInstance();
    c.setTime(new Date());
    c.add(Calendar.YEAR, i);
    Date today = c.getTime();
    SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
    String todayString = sf.format(today);
    int currentYear = Integer.parseInt(todayString.substring(6));
    int nextYear = currentYear + 1;
    String endYearForPurchaseDate = "2041";
    String endYearForExpiryDate = "2041";
//    int month = Integer.parseInt(todayString.substring(3,5));
//    endYearForPurchaseDate = month > 6 ? String.valueOf(nextYear) : String.valueOf(currentYear);
//    endYearForExpiryDate = month > 6 ? String.valueOf(nextYear+30) : String.valueOf(currentYear);

    List<Office_unitsDTO> office_unitsDTOS = Office_unitsRepository.getInstance().getOfficeUnitsDTOListAsInventoryStore();
%>

<style>
    .template-row {
        display: none;
    }

    .custom-modal {
        width: 300px !important; /* Use !important in case you want to override another val*/
        height: 200px;
        position: absolute; /*You can use fixed too*/
        top: 50%;
        left: 50%;
        margin-top: 30px;
        margin-left: 20px;
        display: none; /* You want it to be hidden, and show it using jquery*/
    }

    .form-control.is-invalid {
        /*background-image: none;*/
        background-position: center right calc(1.5em);
    }

    .hiddenPackage {
        display: none;
    }

</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Pi_purchase_parentServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="mt-5 col-12">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=getDataByLanguage(Language, "ক্রয় তথ্য", "Purchase Information")%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='mainPurchaseId'
                                           id='mainPurchaseId_hidden_<%=i%>'
                                           value='<%=mainPurchaseDTO.iD%>' tag='pb_html'/>

                                    <%--<input type='hidden' class='form-control' name='officeUnitId'
                                           id='officeUnitId_hidden_<%=i%>' value='<%=userDTO.unitID%>'
                                           tag='pb_html'/>--%>
                                    <div class="form-group row">
                                        <div class="form-group row col-md-6">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PI_PURCHASE_ADD_FISCALYEARID, loginDTO)%>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <%
                                                    if (!actionName.equals("ajax_edit")) {
                                                %>
                                                <select class='form-control' name='fiscalYearId'
                                                        onchange="loadData('packageId', '')"
                                                        id='fiscalYearId_select_<%=i%>' tag='pb_html'>
                                                    <%
                                                        Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();

                                                        if(mainPurchaseDTO.fiscalYearId == -1){
                                                            Fiscal_yearDTO fiscalYearDTO = fiscal_yearDAO.getFiscalYearBYDateLong(System.currentTimeMillis());
                                                            if(fiscalYearDTO != null) mainPurchaseDTO.fiscalYearId = fiscalYearDTO.id;
                                                        }
                                                        Options = CommonDAO.getOptions(Language, "fiscal_year", mainPurchaseDTO.fiscalYearId);
                                                    %>
                                                    <%=Options%>
                                                </select>
                                                <%
                                                } else {
                                                %>
                                                <label class="col-md-4 col-form-label text-md-left">
                                                    <input type='hidden' class='form-control' name='fiscalYearId'
                                                           id='fiscalYearId_select_<%=i%>'
                                                           value='<%=mainPurchaseDTO.fiscalYearId%>'
                                                           tag='pb_html'/>
                                                    <%
                                                        Options = CommonDAO.getName(Language, "fiscal_year", mainPurchaseDTO.fiscalYearId);
                                                    %>
                                                    <%=Options%>
                                                </label>
                                                <%
                                                    }
                                                %>

                                            </div>
                                        </div>

                                        <!--Procurement Method Start-->

                                        <div class="form-group row col-md-6">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=UtilCharacter.getDataByLanguage(Language, "ক্রয় পদ্ধতি", "Purchase procedure")%>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control'
                                                        name='procurement_method'
                                                        id='procurement_method_category_<%=i%>'
                                                        tag='pb_html'
                                                        onchange="loadData('packageId', '')">
                                                    <%
                                                        Options = CatRepository.getOptions(Language, "procurement_method", mainPurchaseDTO.procurementMethod);
                                                    %>
                                                    <%=Options%>
                                                </select>

                                            </div>
                                        </div>

                                        <!--Procurement Method end-->

                                        <div class="form-group row col-md-6 " id="packageSelectorDiv">
                                            <label class="col-md-4 col-form-label text-md-right"><%=UtilCharacter.getDataByLanguage(Language, "প্যাকেজ", "Package")%>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-8">

                                                <select class='form-control' name='packageId'
                                                        onchange="loadData('lotId', '')"
                                                        id='packageId_select_<%=i%>'
                                                        tag='pb_html'>
<%--                                                    <%= Pi_package_finalRepository.getInstance().buildOptions(Language, mainPurchaseDTO.piPackageNewId) %>--%>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row col-md-6 " id="lotSelectorDiv">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=Language.equals("English") ? "Select Lot" : "লট নির্বাচন করুন"%>

                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='piLotFinalId' id='piLotFinalId'
                                                        tag='pb_html' onchange="lotIdChanged(this)">
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="form-group row col-md-6 ">
                                            <!--store office start-->

                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_PIAUCTIONID, loginDTO)%>
                                                <span class="required" style="color: red;"> * </span>
                                            </label>
                                            <div class="col-md-8">

                                                <select class='form-control' name='officeUnitId'
                                                        id='officeUnitId'
                                                        tag='pb_html'>
                                                    <%= Office_unitsRepository.getInstance().buildOptions(Language, officeUnitId,false,office_unitsDTOS) %>
                                                </select>


                                            </div>
                                            <!--store office end-->
                                        </div>
                                        <div class="form-group row col-md-6">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PI_PURCHASE_ADD_VENDORID, loginDTO)%>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <%
                                                    if (!actionName.equals("ajax_edit")) {
                                                %>
                                                <select class='form-control' name='vendorId'
                                                        onchange="loadData('field-Purchase', '')"
                                                        id='vendorId_select_<%=i%>'
                                                        tag='pb_html'>

                                                </select>
                                                <%
                                                } else {
                                                %>
                                                <label class="col-md-4 col-form-label text-md-left">
                                                    <input type='hidden' class='form-control' name='vendorId'
                                                           id='vendorId_select_<%=i%>'
                                                           value='<%=mainPurchaseDTO.vendorId%>'
                                                           tag='pb_html'/>
                                                    <%
                                                        Options = Language.equals("English") ?
                                                                Pi_vendor_auctioneer_detailsRepository.getInstance().getPi_vendor_auctioneer_detailsDTOByiD(mainPurchaseDTO.vendorId).nameEn :
                                                                Pi_vendor_auctioneer_detailsRepository.getInstance().getPi_vendor_auctioneer_detailsDTOByiD(mainPurchaseDTO.vendorId).nameBn;
                                                    %>
                                                    <%=Options%>
                                                </label>
                                                <%
                                                    }
                                                %>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <div class="form-group row col-md-6">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PI_PURCHASE_ADD_PURCHASEDATE, loginDTO)%>
                                                <span class="required" style="color: red;"> * </span>
                                            </label>
                                            <div class="col-md-8">
                                                <%
                                                    if (!actionName.equals("ajax_edit")) {
                                                %>
                                                <%value = "purchaseDate_js_" + i;%>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="END_YEAR"
                                                               value="<%=endYearForPurchaseDate%>"></jsp:param>
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='purchaseDate' id='purchaseDate_date_<%=i%>'
                                                       value='<%=dateFormat.format(new Date(mainPurchaseDTO.purchaseDate))%>'
                                                       tag='pb_html'>
                                                <%
                                                } else {
                                                %>
                                                <label class="col-md-4 col-form-label text-md-left">
                                                    <%
                                                        value = mainPurchaseDTO.purchaseDate + "";
                                                        String formatted_purchaseDate = StringUtils.convertToDateAndTime(isLanguageEnglish, mainPurchaseDTO.purchaseDate);
                                                    %>
                                                    <%=Utils.getDigits(formatted_purchaseDate, Language)%>
                                                </label>
                                                <%
                                                    }
                                                %>
                                            </div>
                                        </div>
                                        <div class="form-group row col-md-6">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=UtilCharacter.getDataByLanguage(Language, "ক্রয় আদেশ নম্বর", "Purchase Order Number")%>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <%
                                                    if (!actionName.equals("ajax_edit")) {
                                                %>
                                                <input type='text' class='form-control' name='purchaseOrderNumber'
                                                       id='purchaseOrderNumber_text_<%=i%>'
                                                       value='<%=mainPurchaseDTO.purchaseOrderNumber%>'
                                                       tag='pb_html'/>
                                                <%
                                                } else {
                                                %>
                                                <label class="col-md-4 col-form-label text-md-left">
                                                    <%
                                                        Options = mainPurchaseDTO.purchaseOrderNumber;
                                                    %>
                                                    <%=Options%>
                                                </label>
                                                <%
                                                    }
                                                %>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="form-group row col-md-6">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=LM.getText(LC.PI_PURCHASE_ADD_SAROKNUMBER, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <%
                                                    if (!actionName.equals("ajax_edit")) {
                                                %>
                                                <input type='text' class='form-control' name='sarokNumber'
                                                       id='sarokNumber_text_<%=i%>'
                                                       value='<%=mainPurchaseDTO.sarokNumber%>'
                                                       tag='pb_html'/>
                                                <%
                                                } else {
                                                %>
                                                <label class="col-md-4 col-form-label text-md-left">
                                                    <%
                                                        Options = mainPurchaseDTO.sarokNumber;
                                                        ;
                                                    %>
                                                    <%=Options%>
                                                </label>
                                                <%
                                                    }
                                                %>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">



                                        <div class="form-group row col-md-6">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PI_PURCHASE_ADD_REMARKS, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <textarea class='form-control' name='remarks' rows="4"
                                                          style="resize: none"
                                                          id='remarks_text_<%=i%>'><%=mainPurchaseDTO.remarks%></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group row col-md-6">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.FILES_SEARCH_ANYFIELD, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <%
                                                    fileColumnName = "filesDropzone";
                                                    if (actionName.equals("ajax_edit")) {
                                                        List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(mainPurchaseDTO.filesDropzone);
                                                %>
                                                <%@include file="../pb/dropzoneEditor.jsp" %>
                                                <%
                                                    } else {
                                                        ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                        mainPurchaseDTO.filesDropzone = ColumnID;
                                                    }
                                                %>

                                                <div class="dropzone"
                                                     action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=mainPurchaseDTO.filesDropzone%>">
                                                    <input type='file' style="display:none"
                                                           name='<%=fileColumnName%>File'
                                                           id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                                </div>
                                                <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                       id='<%=fileColumnName%>FilesToDelete_<%=i%>' value=''
                                                       tag='pb_html'/>
                                                <input type='hidden' name='<%=fileColumnName%>'
                                                       id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                       value='<%=mainPurchaseDTO.filesDropzone%>'/>
                                            </div>
                                        </div>


                                    </div>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.PI_PURCHASE_ADD_ID, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table id="purchase-table" class="table table-bordered table-striped text-nowrap">
                                <thead>
                                <tr>
                                    <th><%=LM.getText(LC.PI_PACKAGE_NEW_ADD_PI_PACKAGE_NEW_ADD_FORMNAME, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_GOODS_TYPE, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.PI_PURCHASE_ADD_ITEMID, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.PI_PURCHASE_ADD_CURRENT_STOCK, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.PI_PURCHASE_ADD_AMOUNT, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.PI_PURCHASE_ADD_APP_ALLOCATED, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.PI_PURCHASE_ADD_APP_REMAINING, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_UNITPRICE, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.PI_PURCHASE_ADD_TOTALVALUE, loginDTO)%>
                                    </th>
                                    <th style="min-width: 25rem"><%=LM.getText(LC.PI_PURCHASE_ADD_EXPIRYDATE, loginDTO)%>
                                    </th>
                                    <th style="min-width: 25rem"><%=LM.getText(LC.APPROVAL_EXECUTION_TABLE_ADD_REMARKS, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.HM_SELECT, loginDTO)%>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="field-Purchase"></tbody>
                                <tr class="loading-gif" style="display: none;">
                                    <td class="text-center" colspan="100%">
                                        <img alt="" class="loading"
                                             src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
                                        <span><%=getDataByLanguage(Language, "লোড হচ্ছে...", "Loading...")%></span>
                                    </td>
                                </tr>
                            </table>
                        </div>


                    </div>
                </div>


                <div class="row mt-4">
                    <div class="col-12">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.PI_PURCHASE_ADD_PI_PURCHASE_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.PI_PURCHASE_ADD_PI_PURCHASE_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="" style="align-content: center">
                    <div class="custom-modal">
                        <div class="spinner-grow text-primary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-grow text-secondary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-grow text-success" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-grow text-danger" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-grow text-warning" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-grow text-info" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-grow text-light" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-grow text-dark" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<template id="template-expiryDate">
    <%value = "expiryDate_js_";%>
    <jsp:include page="/date/date.jsp">
        <jsp:param name="END_YEAR" value="<%=endYearForExpiryDate%>"></jsp:param>
        <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
        <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
    </jsp:include>
</template>

<jsp:include page="../procurement_goods/procurementGoodsModal.jsp">
    <jsp:param name="index" value="1"/>
</jsp:include>


<script type="text/javascript">

    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';
    const bigForm = $('#bigform');
    var child_table_extra_id = 0;

    function packageIdChanged(element){
        let packageId = element.value;
        let lotId = '';
        let lotOptions = document.getElementById('piLotFinalId');
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let res = JSON.parse(this.response)
                lotOptions.innerHTML = res.msg;
                makeLotVisibleIfRequired();
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Pi_package_finalServlet?actionType=getLotOptionsByPackage&packageId=" + packageId + "&piLotId=" + lotId, true);
        xhttp.send();
    }

    function lotIdChanged(){
        loadData('vendorId', '');
    }

    function preprocessPurchaseCheckBoxBeforeSubmitting(row) {
        if (document.getElementById('isSelected_checkbox_' + row).checked) {
            document.getElementById('selected_hidden_' + row).value = "true";
        } else {
            document.getElementById('selected_hidden_' + row).value = "false";
        }
    }


    function PreprocessBeforeSubmiting(row, action) {
        console.log("action = " + action);
        for (var j = 1; j <= child_table_extra_id; j++) {
            preprocessDateBeforeSubmitting('expiryDate', j);
        }
        preprocessDateBeforeSubmitting('purchaseDate', row);
        preprocessTimeBeforeSubmitting('purchaseTime', row);
        // for (var j=1; j<child_table_extra_id; j++) {
        //     preprocessDateBeforeSubmitting('expiryDate', row);
        // }
        for (var j = 1; j <= child_table_extra_id; j++) {
            preprocessPurchaseCheckBoxBeforeSubmitting(j);
        }

        submitAddForm2ForPurchase();
        return false;
    }

    function submitAddForm2ForPurchase() {
        event.preventDefault();
        let msg = '<%=isLanguageEnglish?"Do you want to purchase?":"ক্রয় করতে চান?"%>';
        let confirmButtonText = '<%=isLanguageEnglish?"Yes":"হ্যাঁ"%>';
        let cancelButtonText = '<%=isLanguageEnglish?"No":"না"%>';
        var form = $("#bigform");
        messageDialog('', msg, 'success', true, confirmButtonText, cancelButtonText, () => {
            var $myModal = $('.custom-modal');
            $('.form-actions').hide();
            $myModal.fadeIn();
            buttonStateChange(true);
            console.log("submitting");
            var form = $("#bigform");
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        console.log("Failed");
                        showError(response.msg, response.msg);
                        buttonStateChange(false);
                        $myModal.fadeOut();
                        $('.form-actions').show();
                    } else if (response.responseCode === 200) {
                        console.log("Successfully added");
                        $myModal.fadeOut();
                        window.location.replace(getContextPath() + response.msg);
                    } else {
                        console.log("Error: " + response.responseCode);
                        $myModal.fadeOut();
                        $('.form-actions').show();
                        buttonStateChange(false);
                    }
                }
                ,
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        }, () => {
        });
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Pi_purchase_parentServlet");
    }

    function init(row) {

        var actionName = '<%=actionName%>';
        if (actionName != 'ajax_edit') {
            setDateByStringAndId('purchaseDate_js_' + row, $('#purchaseDate_date_' + row).val());
            //setTimeById('purchaseTime_js_' + row, $('#purchaseTime_time_' + row).val());
        }
        select2SingleSelector("#vendorId_select_0", '<%=Language%>');
        select2SingleSelector("#packageId_select_0", '<%=Language%>');
        select2SingleSelector("#fiscalYearId_select_0", '<%=Language%>');
        select2SingleSelector("#procurement_method_category_0", '<%=Language%>');
        select2SingleSelector("#piLotFinalId", '<%=Language%>');
        select2SingleSelector("#officeUnitId", '<%=Language%>');
        $.validator.addMethod('vendorSelection', function (value, element) {
            return value !== 0 && value !== -1;
        });
        $.validator.addMethod('packageSelection', function (value, element) {
            return value != 0 && value != -1;
        });
        $.validator.addMethod('fiscalYearSelection', function (value, element) {
            return value != 0 && value != -1;
        });

        $.validator.addMethod('procurementMethod', function (value, element) {
            return value != 0 && value != -1;
        });
        bigForm.validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                vendorId: {
                    required: true,
                    vendorSelection: true
                },
                packageId: {
                    required: true,
                    packageSelection: true
                },
                fiscalYearId: {
                    required: true,
                    fiscalYearSelection: true
                },
                sarokNumber: {
                    required: false,
                },
                procurement_method: {
                    required: true,
                    procurementMethod: true
                }
            },

            messages: {
                vendorId: isLangEng ? "Please Select Vendor" : "অনুগ্রহ করে সরবরাহকারী নির্বাচন করুন",
                packageId: isLangEng ? "Please Select Package Type" : "অনুগ্রহ করে প্যাকেজ ধরণ প্রবেশ করুন",
                fiscalYearId: isLangEng ? "Please Enter Fiscal year" : "অনুগ্রহ করে অর্থবছর প্রবেশ করুন",
                sarokNumber: isLangEng ? "Please Enter sarok number" : "অনুগ্রহ করে স্মারক নম্বর প্রবেশ করুন",
                procurement_method: isLangEng ? "Please Enter procurement method number" : "অনুগ্রহ করে ক্রয় পদ্ধতি নির্বাচন করুন",
            }
        });

        if (actionName == 'ajax_edit') {
            <%--loadData('packageId', '&id=<%=mainPurchaseDTO.packageId%>');--%>
            loadData('field-Purchase', '&id=<%=mainPurchaseDTO.iD%>');
        }

        // loadData('field-Purchase', '&id=1108');

    }

    var actionName = '<%=actionName%>';

    var row = 0;
    $(document).ready(function () {
        init(row);
        <%--select2SingleSelector('#fiscalYearId_select_0', '<%=Language%>');--%>
        <%--select2SingleSelector('#procurement_method_category_0', '<%=Language%>');--%>
        <%--select2SingleSelector('#packageId_select_0', '<%=Language%>');--%>
        <%--select2SingleSelector('#vendorId_select_0', '<%=Language%>');--%>
        // CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    child_table_extra_id = <%=childTableStartingID%>;


    function loadData(name, urlExtra) {

        let officeUnitId = $("#officeUnitId").val();
        let fiscalYearId = $("#fiscalYearId_select_0").val();
        let procurementMethod = $("#procurement_method_category_0").val();
        let packageId = $("#packageId_select_0").val();
        let piLotFinalId = $("#piLotFinalId").val();
        let vendorId = $("#vendorId_select_0").val();


        let url = '';

        if(procurementMethod == null || procurementMethod === '' || procurementMethod === '-1'){
            showToast("ক্রয় পদ্ধতি বাছাই করুন","Select Procurement Method");
        }
        else if(fiscalYearId == null || fiscalYearId === '' || fiscalYearId === '-1'){
            showToast("অর্থ বছর বাছাই করুন","Select Fiscal Year");
        }

        else if (name === 'packageId' && fiscalYearId !== '' && procurementMethod !== '') {
            clearSelects(2);
            url = 'Pi_package_vendorServlet?actionType=getPackageByFiscalYearIdAndProcurementMethod&fiscalYearId=' +
                fiscalYearId + '&procurementMethod='+procurementMethod + urlExtra;

            if (urlExtra === '') ajaxGet(url, processPackageResponse, processPackageResponse);
            else ajaxGet(url, processPackageResponseForInitialEdit, processPackageResponseForInitialEdit);
        }

        if (name === 'lotId' && fiscalYearId !== '' && packageId !== '' && procurementMethod !== '' ) {
            clearSelects(3);
            url = 'Pi_package_vendorServlet?actionType=getLotByFiscalYearIdAndProcurementMethodAndPackageId&fiscalYearId=' +
                fiscalYearId + '&packageId=' + packageId+ '&procurementMethod='+procurementMethod + urlExtra;
            if (urlExtra === '') ajaxGet(url, processLotResponse, processLotResponse);
        }

        if (name === 'vendorId' && fiscalYearId !== '' && packageId !== '' && procurementMethod !== '' ) {
            clearSelects(4);
            url = 'Pi_package_vendorServlet?actionType=getVendorByFiscalYearIdAndPackageIdAndLotIdAndProcurementMethod&fiscalYearId=' +
                fiscalYearId  + '&packageId=' + packageId+'&piFinalLotId='+piLotFinalId+'&procurementMethod='+procurementMethod + urlExtra;
            if (urlExtra === '') ajaxGet(url, processVendorResponse, processVendorResponse);
            else ajaxGet(url, processVendorResponseForInitialEdit, processVendorResponseForInitialEdit);
        }


        if (name === 'field-Purchase' && fiscalYearId !== ''  && vendorId !== '') {
            clearSelects(5);
            showOrHideLoadingGif('purchase-table', true);
            url = 'Pi_package_vendorServlet?actionType=getPriceByFiscalYearIdAndVendorIdAndPackageIdAndLotId&fiscalYearId=' +
                fiscalYearId + '&officeUnitId=' + officeUnitId + '&packageId=' + packageId + '&vendorId=' +
                vendorId+'&piFinalLotId='+piLotFinalId+'&procurementMethod='+procurementMethod + urlExtra;
            ajaxGet(url, processTableResponse, processTableResponse);

        }

    }

    function processPackageResponse(data) {
        if (data.responseCode === 200) {
            $('#packageId_select_0').html(data.msg);
        }
    }
    function processLotResponse(data) {
        if (data.responseCode === 200) {
            if(data.msg !== null && data.msg!== ''){

                $('#piLotFinalId').html(data.msg);

                let SelectorOptionSize = $('#mySelectList > option').length;
                if(SelectorOptionSize <= 1) {
                     // hence the package has no lot. So we have to call vendor
                    loadData('vendorId','');
                }
            }

        }
    }

    function processPackageResponseForInitialEdit(data) {
        processPackageResponse(data);
        loadData('vendorId', '&id=<%=mainPurchaseDTO.vendorId%>');
    }

    function processVendorResponse(data) {
        if (data.responseCode === 200) {
            $('#vendorId_select_0').html(data.msg);
        }
    }

    function processVendorResponseForInitialEdit(data) {
        processVendorResponse(data);
        loadData('field-Purchase', '&id=<%=mainPurchaseDTO.iD%>');
    }

    function processTableResponse(data) {
        if (data.responseCode == 200) {
            $('#field-Purchase').html(JSON.parse(data.msg).itemsHtml);
            child_table_extra_id = JSON.parse(data.msg).itemsCount;
            var t = $("#template-expiryDate");

            var purchaseDateIndex = 1;
            $('#field-Purchase > tr').each(function () {
                var expiryDate = $(this).find('td:nth-last-child(9)');
                var prev_id = t.attr('id');
                expiryDate.append(t.html());

                expiryDate.find("[tag='pb_html']").each(function (index) {
                    var prev_id = $(this).attr('id');
                    $(this).attr('id', prev_id + purchaseDateIndex);
                });

                purchaseDateIndex++;
            });

            for (var j = 1; j <= child_table_extra_id; j++) {
                setDateByStringAndId('expiryDate_js_' + j, $('#expiryDate_date_' + j).val());
            }
        } else {
            noDataFound();
        }
        showOrHideLoadingGif('purchase-table', false);
    }

    function showOrHideLoadingGif(tableId, toShow) {
        const loadingGif = $('#' + tableId + ' tr.loading-gif');
        console.log('#' + tableId);
        if (toShow) {
            document.querySelector('#field-Purchase').innerHTML = '';
            document.querySelectorAll('#' + tableId + ' tfoot').forEach(tfoot => tfoot.remove());
            loadingGif.show();
        } else loadingGif.hide();
    }

    function noDataFound() {
        const tableBody = document.querySelector('#field-Purchase');
        const emptyTableText = '<%=getDataByLanguage(Language, "কোন তথ্য পাওয়া যায়নি", "No data found")%>';
        tableBody.innerHTML = '<tr class="text-center"><td colspan="100%">'
            + emptyTableText + '</td></tr>';

        document.querySelectorAll('#purchase-table tfoot')
            .forEach(tfoot => tfoot.remove());
    }

    function clearTable() {
        const tableBody = document.querySelector('#field-Purchase');
        tableBody.innerHTML = '';
        document.querySelectorAll('#purchase-table tfoot')
            .forEach(tfoot => tfoot.remove());
    }

    function loadTotalValue(index, language) {
        if ($("#isSelected_checkbox_" + index).is(':checked')) {
            var amounts = document.getElementsByName('amount');
            var unitPrices = document.getElementsByName('unitPrice');

            var amountEn = parseInt(amounts[index - 1].value.toString().toEng());
            var unitPriceEn = parseInt(unitPrices[index - 1].value.toString().toEng());

            if (isNaN(amountEn)) {
                amountEn = 0;
            }

            if (isNaN(unitPriceEn)) {
                unitPriceEn = 0;
            }

            $("#totalBill_number_" + index).val((amountEn * unitPriceEn).toString());
        }
    }

    function toggleRowEnabled(index) {
        if (!$("#isSelected_checkbox_" + index).is(':checked')) {
            $("#amount_number_" + index).attr('readonly', true);
            // $("#unitPrice_number_" + index).attr('readonly', true);
            // $("#totalBill_number_" + index).attr('readonly', true);

            $("#Purchase_" + index).css('background-color', '#B2BEB5')
        } else {
            $("#amount_number_" + index).attr('readonly', false);
            // $("#unitPrice_number_" + index).attr('readonly', false);
            // $("#totalBill_number_" + index).attr('readonly', false);

            $("#Purchase_" + index).css('background-color', '')
        }
    }

    function processRowsWhileAdding() {
        setDateByStringAndId('leaveDate_js_' + child_table_extra_id, $('#leaveDate_date_' + child_table_extra_id).val());
    }

    function resetInputFields() {
       let selectorArrIds = ['packageId_select_0','vendorId_select_0','field-Purchase'];
        selectorArrIds.forEach((id) => {
            if(id === 'packageId_select_0') document.querySelector("#"+id).value = '';
            else document.querySelector("#"+id).innerHTML = '';
       })
    }

    function clearSelects(startIndex) {
        const selectIds = ['fiscalYearId_select_0', 'procurement_method_category_0', 'packageId_select_0','piLotFinalId', 'vendorId_select_0','field-Purchase'];

        selectIds.forEach((item) => {
            if(selectIds.indexOf(item)>=startIndex ){
                $('#'+item).html('');
            }
        });
    }

</script>






