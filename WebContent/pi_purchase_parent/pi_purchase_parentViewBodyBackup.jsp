

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="pi_purchase_parent.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Pi_purchase_parentServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Pi_purchase_parentDTO pi_purchase_parentDTO = Pi_purchase_parentDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = pi_purchase_parentDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PI_PURCHASE_ADD_PI_PURCHASE_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.PI_PURCHASE_ADD_PI_PURCHASE_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>

								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_PURCHASE_PARENT_ADD_OFFICEUNITID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_purchase_parentDTO.officeUnitId + "";
											%>

											<%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_PURCHASE_PARENT_ADD_FISCALYEARID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_purchase_parentDTO.fiscalYearId + "";
											%>

											<%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_PURCHASE_PARENT_ADD_PACKAGEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_purchase_parentDTO.packageId + "";
											%>

											<%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_PURCHASE_PARENT_ADD_VENDORID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_purchase_parentDTO.vendorId + "";
											%>

											<%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_PURCHASE_PARENT_ADD_SAROKNUMBER, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_purchase_parentDTO.sarokNumber + "";
											%>

											<%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_PURCHASE_PARENT_ADD_PURCHASEDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_purchase_parentDTO.purchaseDate + "";
											%>
											<%
											String formatted_purchaseDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_purchaseDate, Language)%>


                                    </div>
                                </div>

								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_PURCHASE_PARENT_ADD_REMARKS, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_purchase_parentDTO.remarks + "";
											%>

											<%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_PURCHASE_PARENT_ADD_FILESDROPZONE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_purchase_parentDTO.filesDropzone + "";
											%>
											<%
											{
												List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(pi_purchase_parentDTO.filesDropzone);
											%>
												<%@include file="../pb/dropzoneViewer.jsp"%>
											<%
											}
											%>


                                    </div>
                                </div>

								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_PURCHASE_PARENT_ADD_INSERTEDBY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_purchase_parentDTO.insertedBy + "";
											%>

											<%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_PURCHASE_PARENT_ADD_INSERTIONTIME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_purchase_parentDTO.insertionTime + "";
											%>

											<%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>


								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_PURCHASE_PARENT_ADD_MODIFIEDBY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_purchase_parentDTO.modifiedBy + "";
											%>

											<%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>



							</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>