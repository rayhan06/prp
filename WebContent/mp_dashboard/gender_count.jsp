<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="kt-portlet__body" style="background-color: #F2F2F2">
    <div id="gender_count" style="height: 300px;"></div>
</div>

<script type="text/javascript">
    let genderCountData;

    function drawChartMPGender(fetchedData){
        genderCountData = fetchedData;
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawChartMPGenderChart);
    }

    function drawChartMPGenderChart() {
        const data = new google.visualization.DataTable();
        data.addColumn('string', '<%=isLangEng?"Gender":"লিঙ্গ"%>');
        data.addColumn('number', '<%=isLangEng?"Count":"সংখ্যা"%>');
        const LINK_INDEX = 2;
        data.addColumn('string', 'Link');

        if (genderCountData) {
            for (let key in genderCountData) {
                data.addRows([[genderCountData[key].title, genderCountData[key].count, genderCountData[key].link]]);
            }
        }

        const view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            {
                calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation"
            }
        ]);
        const options = {
            backgroundColor:'#F2F2F2',
            title: '<%=isLangEng?"Number of MPs by Gender":"লিঙ্গ অনুযায়ী এমপির সংখ্যা"%>',
            colors: ['#109618', '#006666', 'crimson'],
            is3D: true,
        };
        const chart = new google.visualization.PieChart(document.getElementById('gender_count'));

        function selectHandler() {
            const selectedItem = chart.getSelection()[0];
            if (selectedItem) {
                location.href = data.getValue(selectedItem.row, LINK_INDEX);
            }
        }
        google.visualization.events.addListener(chart, 'select', selectHandler);

        chart.draw(view, options);
    }
</script>