<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="kt-portlet__body" style="background-color: #F2F2F2">
    <div id="division_wise_count" style="height: 300px;"></div>
</div>

<script type="text/javascript">

    let divisionWiseCountData = [];

    function drawChartMPDivision(fetchedData){
        divisionWiseCountData = fetchedData;
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawChartMPDivisionChart);
    }

    function drawChartMPDivisionChart() {
        const data = new google.visualization.DataTable();
        data.addColumn('string', '<%=isLangEng?"Division":"বিভাগ"%>');
        data.addColumn('number', '<%=isLangEng?"Count":"সংখ্যা"%>');
        const LINK_INDEX = 3;
        data.addColumn({ role: 'style' });
        data.addColumn('string', 'Link');

        if (divisionWiseCountData) {
            for (let key in divisionWiseCountData) {
                if(key == <%=Long.MAX_VALUE%>){
                    data.addRows([[divisionWiseCountData[key].title, divisionWiseCountData[key].count,'color: crimson', divisionWiseCountData[key].link]]);
                }else{
                    data.addRows([[divisionWiseCountData[key].title, divisionWiseCountData[key].count,'color: #006666', divisionWiseCountData[key].link]]);
                }
            }
        }

        const view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            {
                calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation"
            },2
        ]);
        const options = {
            backgroundColor:'#F2F2F2',
            title: '<%=isLangEng?"Number of MPs by Division":"বিভাগ অনুযায়ী এমপির সংখ্যা"%>',
            legend: {position: "none"}
        };
        const chart = new google.visualization.ColumnChart(document.getElementById('division_wise_count'));

        function selectHandler() {
            const selectedItem = chart.getSelection()[0];
            if (selectedItem) {
                location.href = data.getValue(selectedItem.row, LINK_INDEX);
            }
        }

        google.visualization.events.addListener(chart, 'select', selectHandler);

        chart.draw(view, options);
    }

</script>