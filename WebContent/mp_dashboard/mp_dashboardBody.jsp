﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.*" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="election_details.Election_detailsRepository" %>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<%


    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;

%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=isLangEng?"Dashboard":"ড্যাশবোর্ড"%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row">
                <div class="col-12">
                    <div class="row" id='countDiv'>
                        <div class="col-md-6">
                            <label class="col-md-4" for="election_details_id">
                                <%=LM.getText(LC.MP_FUND_AMOUNT_ADD_ELECTIONDETAILSID, loginDTO)%>
                            </label>
                            <div class="col-md-12">
                                <select style='width: 100%' class='form-control'
                                        name='election_details_id' id='election_details_id'
                                        onchange='showCharts()'>
                                    <%=Election_detailsRepository.getInstance().buildOptions(language, Election_detailsRepository.getInstance().getRunningElectionDetailsDTO().iD)%>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-lg-6 mb-3">
                            <div class="shadow dashboard-card-border-radius overflow-hidden">
                                <div class="">
                                    <%@include file="party_count.jsp" %>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 mb-3">
                            <div class="shadow dashboard-card-border-radius overflow-hidden">
                                <div class="">
                                    <%@include file="division_wise_count.jsp" %>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 mb-3">
                            <div class="shadow dashboard-card-border-radius overflow-hidden">
                                <div class="">
                                    <%@include file="gender_count.jsp" %>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 mb-3">
                            <div class="shadow dashboard-card-border-radius overflow-hidden">
                                <div class="">
                                    <%@include file="age_count.jsp" %>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>


<script type="text/javascript">
    $(document).ready(() => {
        select2SingleSelector('#election_details_id', '<%=language%>');
        showCharts();
    });

    function showCharts() {
        $('#election_details_id').prop('disabled',true);
        $.ajax({
            url: "MpDashboardServlet?actionType=ajax_mp_dashboard&electionDetailsId=" + $("#election_details_id").val(),
            type: "GET",
            async: true,
            success: function (fetchedData) {
                console.log(fetchedData);
                drawChartMPParty(fetchedData.party);
                drawChartMPAge(fetchedData.age);
                drawChartMPGender(fetchedData.gender);
                drawChartMPDivision(fetchedData.division);
                $('#election_details_id').prop('disabled',false);
            },
            error: function (error) {
                console.log(error);
                $('#election_details_id').prop('disabled',false);
            }
        });
    }

</script>




