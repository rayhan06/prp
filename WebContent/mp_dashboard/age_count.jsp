<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="kt-portlet__body" style="background-color: #F2F2F2">
    <div id="age_count" style="height: 300px;"></div>
</div>

<script type="text/javascript">
    let ageCountData;

    function drawChartMPAge(fetchedData){
        ageCountData = fetchedData;
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawChartMPAgeChart);
    }

    function drawChartMPAgeChart() {
        const data = new google.visualization.DataTable();
        data.addColumn('string', '<%=isLangEng?"Age":"বয়স"%>');
        data.addColumn('number', '<%=isLangEng?"Count":"সংখ্যা"%>');
        const LINK_INDEX = 2;
        data.addColumn('string', 'Link');

        if (ageCountData) {
            for (let key in ageCountData) {
                data.addRows([[ageCountData[key].title, ageCountData[key].count, ageCountData[key].link]]);
            }
        }

        const view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            {
                calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation"
            }
        ]);
        const options = {
            backgroundColor:'#F2F2F2',
            colors:['#006666'],
            title: '<%=isLangEng?"Number of MPs by Age":"বয়স অনুযায়ী এমপির সংখ্যা"%>',
            legend: {position: "none"}
        };
        const chart = new google.visualization.ColumnChart(document.getElementById('age_count'));

        function selectHandler() {
            const selectedItem = chart.getSelection()[0];
            if (selectedItem) {
                location.href = data.getValue(selectedItem.row, LINK_INDEX);
            }
        }

        google.visualization.events.addListener(chart, 'select', selectHandler);

        chart.draw(view, options);
    }
</script>