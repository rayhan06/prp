<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<div class="kt-portlet__body" style="background-color: #F2F2F2">
    <div id="party_count" style="height: 300px;"></div>
</div>

<script type="text/javascript">

   let partyCountData;

    function drawChartMPParty(fetchedData){
        partyCountData = fetchedData;
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawChartMPPartyChart);
    }

    function drawChartMPPartyChart() {
        const data = new google.visualization.DataTable();
        data.addColumn('string', '<%=isLangEng?"Party":"পার্টি"%>');
        data.addColumn('number', '<%=isLangEng?"Count":"সংখ্যা"%>');
        const LINK_INDEX = 2;
        data.addColumn('string', 'Link');

        if (partyCountData) {
            for (let key in partyCountData) {
                data.addRows([[partyCountData[key].title, partyCountData[key].count, partyCountData[key].link]]);
            }
        }

        const view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            {
                calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation"
            }
        ]);
        const options = {
            backgroundColor:'#F2F2F2',
            colors:['#006666'],
            title: '<%=isLangEng?"Number of MPs by Party":"পার্টি অনুযায়ী এমপির সংখ্যা"%>',
            legend: {position: "none"}
        };
        const chart = new google.visualization.ColumnChart(document.getElementById('party_count'));

        function selectHandler() {
            const selectedItem = chart.getSelection()[0];
            if (selectedItem) {
                location.href = data.getValue(selectedItem.row, LINK_INDEX);
            }
        }

        google.visualization.events.addListener(chart, 'select', selectHandler);

        chart.draw(view, options);
    }

</script>