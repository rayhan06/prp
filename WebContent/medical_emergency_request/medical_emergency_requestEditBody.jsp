<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>

<%@page import="medical_emergency_request.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="family.*" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    Medical_emergency_requestDTO medical_emergency_requestDTO;
    medical_emergency_requestDTO = (Medical_emergency_requestDTO) request.getAttribute("medical_emergency_requestDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    if (medical_emergency_requestDTO == null) {
        medical_emergency_requestDTO = new Medical_emergency_requestDTO();

    }
    System.out.println("medical_emergency_requestDTO = " + medical_emergency_requestDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_MEDICAL_EMERGENCY_REQUEST_ADD_FORMNAME, loginDTO);
    String servletName = "Medical_emergency_requestServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Medical_emergency_requestServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="sub_title_top">
                                                <div class="sub_title">
                                                    <h4 style="background: white">
                                                        <%=formTitle%>
                                                    </h4>
                                                </div>
                                            </div>
                                            <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                                   value='<%=medical_emergency_requestDTO.iD%>' tag='pb_html'/>

                                            <input type='hidden' class='form-control' name='employeeId'
                                                   id='employeeId_hidden_<%=i%>'
                                                   value='<%=medical_emergency_requestDTO.employeeId%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='employeeUserId'
                                                   id='employeeUserId_hidden_<%=i%>'
                                                   value='<%=medical_emergency_requestDTO.employeeUserId%>'
                                                   tag='pb_html'/>

											<%
											if(userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE || userDTO.roleID == SessionConstants.ADMIN_ROLE 
											|| userDTO.roleID == SessionConstants.NURSE_ROLE ||  userDTO.roleID == SessionConstants.MEDICAL_RECEPTIONIST_ROLE)
											{
											%>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <button type="button" class="btn btn-block submit-btn text-white shadow btn-border-radius"
                                                            onclick="addEmployee()"
                                                            id="addToTrainee_modal_button"><%=LM.getText(LC.HM_ADD_EMPLOYEE, loginDTO)%>
                                                    </button>
                                                    <table class="table table-bordered table-striped">
                                                        <tbody id="employeeToSet"></tbody>
                                                    </table>
                                                    <input type='hidden' name='orgId' id='orgId' tag='pb_html'
                                                           value='<%=userDTO.organogramID%>'/>
                                                    <input type='hidden' name='userName' id='userName' tag='pb_html'
                                                           value='<%=userDTO.userName%>'/></div>
                                            </div>
                                            <%
											}
											else
											{
                                            %>
                                            <input type='hidden' name='orgId' id='orgId' tag='pb_html'
                                                           value='<%=userDTO.organogramID%>'/>
                                            <input type='hidden' name='userName' id='userName' tag='pb_html'
                                                   value='<%=userDTO.userName%>'/>
                                                   <%
											}
                                                   %>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_WHOISTHEPATIENTCAT, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <select class='form-control' required name='whoIsThePatientCat'
                                                            id='whoIsThePatientCat_category_<%=i%>'
                                                            onchange="getPatientInfo(this.value)" tag='pb_html'>
                                                        <%
                                                            Options = CatDAO.getOptions(Language, "who_is_the_patient", medical_emergency_requestDTO.whoIsThePatientCat);
                                                        %>
                                                        <option value=""><%=LM.getText(LC.HM_SELECT, loginDTO)%>
                                                        </option>
                                                        <%=Options%>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.HM_NAME, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type='text' class='form-control' name='patientName'
                                                           id='patientName_text_<%=i%>'
                                                           value='<%=medical_emergency_requestDTO.patientName%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.HM_PHONE, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
	                                                <div class="input-group mb-2">
							                            <div class="input-group-prepend">
							                                <div class="input-group-text"><%=Language.equalsIgnoreCase("english")? "+88" : "+৮৮"%></div>
							                            </div>
							                            <input type='text' class='form-control' name='phoneNumber' required
                                                           id='phoneNumber_text_<%=i%>'
                                                           value='<%=Utils.getPhoneNumberWithout88(medical_emergency_requestDTO.phoneNumber, Language)%>'
                                                           tag='pb_html'/>
										             </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.HM_DATE_OF_BIRTH, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <jsp:include page="/date/date.jsp">
                                                        <jsp:param name="DATE_ID"
                                                                   value="dateOfBirth_date_js"></jsp:param>
                                                        <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                    </jsp:include>

                                                    <input type='hidden' class='form-control formRequired datepicker'
                                                           readonly="readonly" data-label="Document Date"
                                                           id='dateOfBirth_date_<%=i%>' name='dateOfBirth' value=<%
																String formatted_dateOfBirth = dateFormat.format(new Date(medical_emergency_requestDTO.dateOfBirth));
																%>
                                                                   '<%=formatted_dateOfBirth%>'
                                                           tag='pb_html'></div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_GENDERCAT, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <select class='form-control' name='genderCat'
                                                            id='genderCat_category_<%=i%>' tag='pb_html'>
                                                        <%
                                                            Options = CatDAO.getOptions(Language, "gender", medical_emergency_requestDTO.genderCat);
                                                        %>
                                                        <%=Options%>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_SERVICECAT, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <select class='form-control' name='serviceCat'
                                                            id='serviceCat_category_<%=i%>'
                                                            onchange="serviceSelected(this.value)" tag='pb_html' required>
                                                        <%
                                                            Options = CatDAO.getOptions(Language, "service", -2);
                                                        %>
                                                        <%=Options%>
                                                    </select>

                                                </div>
                                            </div>
                                            <div id="fromAddressDiv" style="display:none">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_FROMADDRESS, loginDTO)%> *
                                                    </label>
                                                    <div class="col-md-9">
                                                        <div id='fromAddress_div_<%=i%>' tag='pb_html'>
                                                            <input type='text' class='form-control' name='fromAddress'
                                                                   id='fromAddress_text_<%=i%>'
                                                                   value='<%=medical_emergency_requestDTO.fromAddress%>'
                                                                   tag='pb_html'/></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="toAddressDiv" style="display:none">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-md-right" id = "toAddressLabel" style = "display:none">
                                                    <%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_TOADDRESS, loginDTO)%> *
                                                    </label>
                                                    <label class="col-md-3 col-form-label text-md-right" id = "justAddressLabel" style = "display:none">
                                                    <%=LM.getText(LC.REPORT_EDIT_ADDRESS, loginDTO)%> *
                                                    </label>
                                                    <div class="col-md-9">
                                                        <div id='toAddress_geoDIV_<%=i%>' tag='pb_html'>
                                                            <input type='text' class='form-control' name='toAddress'
                                                                   id='toAddress_text_<%=i%>'
                                                                   value='<%=medical_emergency_requestDTO.toAddress%>' required
                                                                   tag='pb_html'/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="serviceDateDiv" style="display:none">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_SERVICEDATE, loginDTO)%>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <jsp:include page="/date/date.jsp">
                                                            <jsp:param name="DATE_ID"
                                                                       value="serviceDate_date_js"></jsp:param>
                                                            <jsp:param name="LANGUAGE"
                                                                       value="<%=Language%>"></jsp:param>
                                                        </jsp:include>

                                                        <input type='hidden'
                                                               class='form-control formRequired datepicker'
                                                               readonly="readonly" data-label="Document Date"
                                                               id='serviceDate_date_<%=i%>' name='serviceDate' value=<%
																	String formatted_serviceDate = dateFormat.format(new Date(medical_emergency_requestDTO.serviceDate));
																%>
                                                                       '<%=formatted_serviceDate%>'/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="fromTimeDiv" style="display:none">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_FROMTIME, loginDTO)%>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <jsp:include page="/time/time.jsp">
                                                            <jsp:param name="TIME_ID"
                                                                       value="fromTime_time_js"></jsp:param>
                                                            <jsp:param name="LANGUAGE"
                                                                       value="<%=Language%>"></jsp:param>
                                                            <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                                        </jsp:include>

                                                        <input type='hidden' class="form-control"
                                                               value="<%=medical_emergency_requestDTO.fromTime%>"
                                                               id='fromTime_time_<%=i%>' name='fromTime' tag='pb_html'/>


                                                    </div>
                                                </div>
                                            </div>


                                            <div id="toTimeDiv" style="display:none">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_TOTIME, loginDTO)%>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <jsp:include page="/time/time.jsp">
                                                            <jsp:param name="TIME_ID"
                                                                       value="toTime_time_js"></jsp:param>
                                                            <jsp:param name="LANGUAGE"
                                                                       value="<%=Language%>"></jsp:param>
                                                            <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                                        </jsp:include>
                                                        <input type='hidden' class="form-control"
                                                               value="<%=medical_emergency_requestDTO.toTime%>"
                                                               id='toTime_time_<%=i%>' name='toTime' tag='pb_html'/>
                                                    </div>
                                                </div>
                                            </div>

                                         
                                            <input type='hidden' class='form-control' name='servicePersonUserId'
                                                   id='servicePersonUserId_hidden_<%=i%>'
                                                   value='<%=medical_emergency_requestDTO.servicePersonUserId%>'
                                                   tag='pb_html'/>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_INSTRUCTIONS, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type='text' class='form-control' name='instructions'
                                                           id='instructions_text_<%=i%>'
                                                           value='<%=medical_emergency_requestDTO.instructions%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                            <input type='hidden' class='form-control' name='insertedByUserId'
                                                   id='insertedByUserId_hidden_<%=i%>'
                                                   value='<%=medical_emergency_requestDTO.insertedByUserId%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                                   id='insertedByOrganogramId_hidden_<%=i%>'
                                                   value='<%=medical_emergency_requestDTO.insertedByOrganogramId%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertionDate'
                                                   id='insertionDate_hidden_<%=i%>'
                                                   value='<%=medical_emergency_requestDTO.insertionDate%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='lastModifierUser'
                                                   id='lastModifierUser_hidden_<%=i%>'
                                                   value='<%=medical_emergency_requestDTO.lastModifierUser%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='isDeleted'
                                                   id='isDeleted_hidden_<%=i%>'
                                                   value='<%=medical_emergency_requestDTO.isDeleted%>' tag='pb_html'/>

                                            <input type='hidden' class='form-control' name='lastModificationTime'
                                                   id='lastModificationTime_hidden_<%=i%>'
                                                   value='<%=medical_emergency_requestDTO.lastModificationTime%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='nurseUserId'
                                                   id='nurseUserId_hidden_<%=i%>'
                                                   value='<%=medical_emergency_requestDTO.nurseUserId%>' tag='pb_html'/>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_MEDICAL_EMERGENCY_REQUEST_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_MEDICAL_EMERGENCY_REQUEST_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    $(document).ready(function () {
        init(row);
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })

    });


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Medical_emergency_requestServlet");
    }

    function init(row) {
        setDateByTimestampAndId('serviceDate_date_js', '<%=medical_emergency_requestDTO.serviceDate%>');
        setTimeById('fromTime_time_js', $("#fromTime_time_0").val());
        setTimeById('toTime_time_js', $("#toTime_time_0").val());
        
        patient_inputted($("#userName").val(), $("#orgId").val());
    }

    function PreprocessBeforeSubmiting(row, validate) {
        var serviceDate = getDateStringById('serviceDate_date_js', 'DD/MM/YYYY');
        $("#serviceDate_date_<%=i%>").val(serviceDate);

        var birthDate = getDateStringById('dateOfBirth_date_js', 'DD/MM/YYYY');
        $("#dateOfBirth_date_0").val(birthDate);

        var fromTime = getTimeById('fromTime_time_js');
        console.log("fromTime = " + fromTime);
        $("#fromTime_time_0").val(getTimeById('fromTime_time_js', true));
        $("#toTime_time_0").val(getTimeById('toTime_time_js', true));
        $("select").prop("disabled", false);
        
        var convertedPhoneNumber = phoneNumberAdd88ConvertLanguage($('#phoneNumber_text_<%=i%>').val(), '<%=Language%>');
        $("#phoneNumber_text_<%=i%>").val(convertedPhoneNumber);

        return true;
    }


    var row = 0;


    var child_table_extra_id = <%=childTableStartingID%>;


    function patient_inputted(userName, orgId) {
        console.log('patient inputted value: ' + userName);

        $("#orgId").val(orgId);
        $("#userName").val(userName);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var options = this.responseText;
                $("#whoIsThePatientCat_category_0").html(options);
                //getPatientInfo($("#whoIsThePatientCat_category_0").val());

            } else {
                //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
            }
        };

        xhttp.open("POST", "FamilyServlet?actionType=getFamily&userName=" + userName + "&language=<%=Language%>&defaultOption=<%=medical_emergency_requestDTO.whoIsThePatientCat%>", true);
        xhttp.send();
    }

    function getPatientInfo(value) {
        console.log('changed value: ' + value);

        var familyMemberId = value;
        var userName = $("#userName").val();

        if (value == <%=FamilyDTO.OWN%> || value >= 0) {
            disableFields();
        } else {
            enableFields();
        }

        clearFields();

        if (value == <%=FamilyDTO.OWN%> || value >= 0) {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    var familyMemberDTO = JSON.parse(this.responseText);
                    <%
                    if(Language.equalsIgnoreCase("english"))
                    {
                    %>
                    $('#patientName_text_<%=i%>').val(familyMemberDTO.nameEn);
                    <%
                    }
                    else
                    {
                    %>
                    $('#patientName_text_<%=i%>').val(familyMemberDTO.nameBn);
                    <%
                    }
                    %>
                    $('#phoneNumber_text_<%=i%>').val(phoneNumberRemove88ConvertLanguage(familyMemberDTO.mobile, '<%=Language%>'));
                    $('#dateOfBirth_date_<%=i%>').val(familyMemberDTO.dateOfBirthFormatted);
                    setDateByStringAndId('dateOfBirth_date_js', familyMemberDTO.dateOfBirthFormatted);
                    $('#genderCat_category_<%=i%>').val(familyMemberDTO.genderCat);
                } else {
                    //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
                }
            };

            xhttp.open("POST", "FamilyServlet?actionType=getPatientDetails&familyMemberId=" + familyMemberId + "&userName=" + userName + "&language=<%=Language%>", true);
            xhttp.send();
        }

    }

    function disableFields() {
        console.log("disabling fields");
        $('#patientName_text_<%=i%>').prop("readonly", true);
        $('#phoneNumber_text_<%=i%>').prop("readonly", true);
        $('#genderCat_category_<%=i%>').prop("disabled", true);

    }

    function enableFields() {
        console.log("enabling fields");
        $('#patientName_text_<%=i%>').prop("readonly", false);
        $('#phoneNumber_text_<%=i%>').prop("readonly", false);
        $('#genderCat_category_<%=i%>').prop("disabled", false);


    }

    function clearFields() {
        $('#patientName_text_<%=i%>').val("");
        $('#phoneNumber_text_<%=i%>').val("");
        $('#dateOfBirth_date_<%=i%>').val("");
    }

    function serviceSelected(value) {
        if (value == -1) {
            $("#fromAddressDiv").css("display", "none");
            $("#toAddressDiv").css("display", "none");
            $("#serviceDateDiv").css("display", "none");
            $("#fromTimeDiv").css("display", "none");
            $("#toTimeDiv").css("display", "none");
            $("#nurseDiv").css("display", "none");
            $("#servicePersonDiv").css("display", "none");
            $("#toAddressLabel").css("display", "none");
            $("#justAddressLabel").css("display", "none");
            $("#fromAddress_text_<%=i%>").removeAttr("required");
        } else if (value == <%=SessionConstants.AMBULANCE_SERVICE%>) {
            $("#fromAddressDiv").css("display", "block");
            $("#toAddressDiv").css("display", "block");
            $("#serviceDateDiv").css("display", "block");
            $("#fromTimeDiv").css("display", "block");
            $("#toTimeDiv").css("display", "block");
            $("#nurseDiv").css("display", "none");
            $("#servicePersonDiv").css("display", "none");
            $("#toAddressLabel").css("display", "block");
            $("#justAddressLabel").css("display", "none");
            $("#fromAddress_text_<%=i%>").attr("required", "required");
        } else {
            $("#fromAddressDiv").css("display", "none");
            $("#toAddressDiv").css("display", "block");
            $("#serviceDateDiv").css("display", "block");
            $("#fromTimeDiv").css("display", "block");
            $("#toTimeDiv").css("display", "block");
            $("#nurseDiv").css("display", "block");
            $("#servicePersonDiv").css("display", "block");
            $("#toAddressLabel").css("display", "none");
            $("#justAddressLabel").css("display", "block");
            $("#fromAddress_text_<%=i%>").removeAttr("required");
        }
    }


</script>






