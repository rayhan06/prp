<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="medical_emergency_request.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>

<%
    String servletName = "Medical_emergency_requestServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Medical_emergency_requestDAO medical_emergency_requestDAO = new Medical_emergency_requestDAO("medical_emergency_request");
    Medical_emergency_requestDTO medical_emergency_requestDTO = medical_emergency_requestDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_MEDICAL_EMERGENCY_REQUEST_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div>
                <h5 class="table-title">
                    <%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_MEDICAL_EMERGENCY_REQUEST_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead></thead>
                        <tbody>
                        <tr>
                            <td>
                                <b>
                                    <%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%
                                    value = WorkflowController.getUserNameFromUserId(medical_emergency_requestDTO.employeeUserId) + "";
                                    if (medical_emergency_requestDTO.employeeUserId == -1) {
                                        value = "";
                                    }
                                %>
                                <%=Utils.getDigits(value, Language)%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <%=LM.getText(LC.HM_NAME, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%
                                    value = medical_emergency_requestDTO.patientName + "";
                                %>
                                <%=Utils.getDigits(value, Language)%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <%=LM.getText(LC.HM_PHONE, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%
                                    value = medical_emergency_requestDTO.phoneNumber + "";
                                %>
                                <%=Utils.getDigits(value, Language)%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <%=LM.getText(LC.HM_DATE_OF_BIRTH, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%
                                    value = medical_emergency_requestDTO.dateOfBirth + "";
                                %>
                                <%
                                    String formatted_dateOfBirth = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_dateOfBirth, Language)%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_GENDERCAT, loginDTO)%>
                                </b>
                            </td>
                            <td>
   
                                <%
                                    value = CatDAO.getName(Language, "gender", medical_emergency_requestDTO.genderCat);
                                %>
                                <%=Utils.getDigits(value, Language)%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_SERVICECAT, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%
                                    value = medical_emergency_requestDTO.serviceCat + "";
                                %>
                                <%
                                    value = CatDAO.getName(Language, "service", medical_emergency_requestDTO.serviceCat);
                                %>
                                <%=Utils.getDigits(value, Language)%>
                            </td>
                        </tr>
                        <%
                        if(medical_emergency_requestDTO.serviceCat == SessionConstants.AMBULANCE_SERVICE)
                        {
                        %>
                        
                        <tr>
                            <td>
                                <b>
                                    <%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_FROMADDRESS, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%
                                    value = medical_emergency_requestDTO.fromAddress + "";
                                %>
                                <%=Utils.getDigits(value, Language)%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_TOADDRESS, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%
                                    value = medical_emergency_requestDTO.toAddress + "";
                                %>
                                <%=Utils.getDigits(value, Language)%>
                            </td>
                        </tr>
                        <%
                        }
                        else
                        {
                        %>
                        <tr>
                            <td>
                                <b>
                                    <%=LM.getText(LC.REPORT_EDIT_ADDRESS, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%
                                    value = medical_emergency_requestDTO.toAddress + "";
                                %>
                                <%=Utils.getDigits(value, Language)%>
                            </td>
                        </tr>
                        <%
                        }
                        %>
                        <tr>
                            <td>
                                <b>
                                    <%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_SERVICEDATE, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%
                                    value = medical_emergency_requestDTO.serviceDate + "";
                                %>
                                <%
                                    String formatted_serviceDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_serviceDate, Language)%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_FROMTIME, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%
                                    value = medical_emergency_requestDTO.fromTime + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_TOTIME, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%
                                    value = medical_emergency_requestDTO.toTime + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>
                            </td>
                        </tr>
                        <%
                        if(medical_emergency_requestDTO.serviceCat == SessionConstants.HOME_SERVICE)
                        {
                        %>
                        <tr>
                            <td>
                                <b>
                                    <%=LM.getText(LC.HM_DOCTOR, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%
                                    value = WorkflowController.getNameFromUserId(medical_emergency_requestDTO.servicePersonUserId, Language);
                                %>

                                <%=Utils.getDigits(value, Language)%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <%=LM.getText(LC.HM_NURSE, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%
                                    value = WorkflowController.getNameFromUserId(medical_emergency_requestDTO.nurseUserId, Language);
                                %>

                                <%=Utils.getDigits(value, Language)%>
                            </td>
                        </tr>
                        <%
                        }
                        %>
                        <tr>
                            <td>
                                <b><%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_INSTRUCTIONS, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = medical_emergency_requestDTO.instructions + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>