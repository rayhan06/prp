<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="medical_emergency_request.*" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    Medical_emergency_requestDAO medical_emergency_requestDAO = (Medical_emergency_requestDAO) request.getAttribute("medical_emergency_requestDAO");


    String navigator2 = SessionConstants.NAV_MEDICAL_EMERGENCY_REQUEST;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>
<%

    if (hasAjax == false) {
        Enumeration<String> parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {

            String paramName = parameterNames.nextElement();

            if (!paramName.equalsIgnoreCase("actionType")) {
                String[] paramValues = request.getParameterValues(paramName);
                for (int i = 0; i < paramValues.length; i++) {
                    String paramValue = paramValues[i];

%>

<%

                }
            }


        }
    }

%>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
        	<th class="text-nowrap"><%=LM.getText(LC.HM_SL, loginDTO)%>
            </th>
            <th class="text-nowrap"><%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%>
            </th>

            <th class="text-nowrap"><%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_SERVICECAT, loginDTO)%>
            </th>
            <th class="text-nowrap"><%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_FROMADDRESS, loginDTO)%>
            </th>
            <th class="text-nowrap"><%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_TOADDRESS, loginDTO)%>
            </th>
            <th class="text-nowrap"><%=LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_SERVICEDATE, loginDTO)%>
            </th>
          
            <th class="text-nowrap"> <%=isLangEng ? "Service Person(s)" : "সেবাদানকারী"%>
            </th>
            

            <th class="text-nowrap"><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <%
            if(userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE)
            {
            %>
            <th class="">
                <div class="text-center">
                    <span>All</span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-2">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
            <%
            }
            %>
        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_MEDICAL_EMERGENCY_REQUEST);

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Medical_emergency_requestDTO medical_emergency_requestDTO = (Medical_emergency_requestDTO) data.get(i);


        %>
        <tr id='tr_<%=i%>'>
            <%

            %>


            <%
                request.setAttribute("medical_emergency_requestDTO", medical_emergency_requestDTO);
            %>

            <jsp:include page="./medical_emergency_requestSearchRow.jsp">
                <jsp:param name="pageName" value="searchrow"/>
                <jsp:param name="rownum" value="<%=i%>"/>
            </jsp:include>


            <%

            %>
        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			