<%@page pageEncoding="UTF-8" %>

<%@page import="medical_emergency_request.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_MEDICAL_EMERGENCY_REQUEST;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Medical_emergency_requestDTO medical_emergency_requestDTO = (Medical_emergency_requestDTO) request.getAttribute("medical_emergency_requestDTO");
    CommonDTO commonDTO = medical_emergency_requestDTO;
    String servletName = "Medical_emergency_requestServlet";


    System.out.println("medical_emergency_requestDTO = " + medical_emergency_requestDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Medical_emergency_requestDAO medical_emergency_requestDAO = new Medical_emergency_requestDAO();


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;

%>

<td>
	<b><%=Utils.getDigits(i + 1 + ((rn2.getCurrentPageNo() - 1) * rn2.getPageSize()), Language) %>:</b>
	<%=Utils.getDigits(medical_emergency_requestDTO.iD, Language)%>
</td>
<td class="text-nowrap" id='<%=i%>_employeeUserId'>

    <%
        value = WorkflowController.getUserNameFromUserId(medical_emergency_requestDTO.employeeUserId) + "";
        if (medical_emergency_requestDTO.employeeUserId == -1) {
            value = "";
        }
    %>

    <b><%=Utils.getDigits(value, Language)%></b><br>



    <%
        value = medical_emergency_requestDTO.patientName + "";
    %>

    <%=Utils.getDigits(value, Language)%><br>



    <%
        value = medical_emergency_requestDTO.phoneNumber + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td class="text-nowrap">
    <%
        value = medical_emergency_requestDTO.serviceCat + "";
    %>
    <%
        value = CatDAO.getName(Language, "service", medical_emergency_requestDTO.serviceCat);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_fromAddress' class="text-nowrap">
    <%
        value = medical_emergency_requestDTO.fromAddress + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_toAddress' class="text-nowrap">
    <%
        value = medical_emergency_requestDTO.toAddress + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td class="text-nowrap" id='<%=i%>_serviceDate'>
    <%
        value = medical_emergency_requestDTO.serviceDate + "";
    %>
    <%
        String formatted_serviceDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <b><%=Utils.getDigits(formatted_serviceDate, Language)%></b><br>



    <%
        value = medical_emergency_requestDTO.fromTime + "";
    %>

    <%=Utils.getDigits(value, Language)%>
	<%=value.equalsIgnoreCase("")?"":"-"%>

    <%
        value = medical_emergency_requestDTO.toTime + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td class="text-nowrap">

    <%
    	if(medical_emergency_requestDTO.servicePersonUserId != -1)
    	{
    		%>
    		<b><%=LM.getText(LC.HM_DOCTOR, loginDTO)%></b>:
    		<%=WorkflowController.getNameFromUserId(medical_emergency_requestDTO.servicePersonUserId, Language)%>
    		<br>
    		<%
    	}
        
    %>
    
     <%
    	if(medical_emergency_requestDTO.nurseUserId != -1)
    	{
    		%>
    		<b><%=LM.getText(LC.HM_NURSE, loginDTO)%></b>:
    		<%=WorkflowController.getNameFromUserId(medical_emergency_requestDTO.nurseUserId, Language)%>
    		<br>
    		<%
    	}
        
    %>
    
    <%
    	if(medical_emergency_requestDTO.driverUserName != null && !medical_emergency_requestDTO.driverUserName.equalsIgnoreCase(""))
    	{
    		%>
    		<b><%=isLangEng ? "Driver" : "ড্রাইভার"%>
            </b>:
    		<%=WorkflowController.getNameFromUserName(medical_emergency_requestDTO.driverUserName, Language)%>
    		<br>
    		<%
    	}
        
    %>



</td>









<td>
<%
if(userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE
|| userDTO.roleID == SessionConstants.NURSE_ROLE || userDTO.roleID == SessionConstants.MEDICAL_RECEPTIONIST_ROLE)
{
%>
	<button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            data-toggle="modal" data-target="#serviceModal_<%=medical_emergency_requestDTO.iD%>">
            
        <i class="fa fa-plus"></i>
    </button>
 <%
}
 %>   
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Medical_emergency_requestServlet?actionType=view&ID=<%=medical_emergency_requestDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
    
    <div id="serviceModal_<%=medical_emergency_requestDTO.iD%>" class="modal fade" role="dialog">
	    <div class="container">
	        <div class="">
	            <div class="modal-dialog">
	
	                <!-- Modal content-->
	                <div class="modal-content">
	                	<div id = "modalDiv_<%=medical_emergency_requestDTO.iD%>"
				              >
		                    <div class="modal-header">
		                        <button type="button" class="close" data-dismiss="modal"></button>
		                        <h4 class="modal-title">
		                        </h4>
		                    </div>
		                    <div class="modal-body">
								<input name = "erId" value = "<%=medical_emergency_requestDTO.iD%>" style = "display:none" />
		                        <%
		                        if(medical_emergency_requestDTO.serviceCat == SessionConstants.AMBULANCE_SERVICE)
		                        {
		                        	%>
		                        	
		                        	<div class="form-group row">
	                                        <label class="col-md-3 col-form-label text-md-right"><%=isLangEng ? "Driver" : "ড্রাইভের"%>
	                                        </label>
	                                        <div class="col-md-9">
	                                            <select class='form-control' name='driverOrganogramId'
	                                                     tag='pb_html'>
	                                                    <option value = "-1"><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
	                                                <%
	                                                	List<Long> drivers = medical_emergency_requestDAO.getAmbulanceDrivers();
	                                                	if(drivers != null)
	                                                	{
	                                                		for(long driver: drivers)
	                                                		{
	                                                			%>
	                                                			<option value = "<%=driver%>" 
	                                                			<%=driver == medical_emergency_requestDTO.driverOrganogramId?"selected":"" %>
	                                                			>
	                                                			<%=WorkflowController.getNameFromOrganogramId(driver, isLangEng)%>
	                                                			</option>
	                                                			<%
	                                                		}
	                                                	}
	                                                %>
	                                            </select>
	                                        </div>
	                                    </div>
	                               
		                        	<%
		                        }
		                        else
		                        {
		                        	%>
		                        	<div class="form-group row">
		                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.HM_DOCTOR, loginDTO)%>
		                                </label>
		                                <div class="col-md-9">
		                                    <select class='form-control' name='servicePersonOrganogramId'
		                                            tag='pb_html'>
		                                        <%
		
		                                            Options = CommonDAO.getDoctorsByOrganogramID(medical_emergency_requestDTO.servicePersonOrganogramId, "", CommonDAO.DR, true);
		
		                                        %>
		                                        <%=Options%>
		                                    </select>
		                                </div>
		                            </div>
		                        
		
		                        
		                            <div class="form-group row">
		                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.HM_NURSE, loginDTO)%>
		                                </label>
		                                <div class="col-md-9">
		                                    <select class='form-control' name='nurseOrganogramId'
		                                             tag='pb_html'>
		                                        <%
		                                            Options = CommonDAO.getEmployeesByOrganogramIDAndRole(medical_emergency_requestDTO.nurseOrganogramId, SessionConstants.NURSE_ROLE);
		                                        %>
		                                        <%=Options%>
		                                    </select>
		                                </div>
		                            </div>
		                            
		                            <div class="form-group row">
	                                        <label class="col-md-3 col-form-label text-md-right"><%=isLangEng ? "Driver" : "ড্রাইভের"%>
	                                        </label>
	                                        <div class="col-md-9">
	                                            <select class='form-control' name='driverOrganogramId'
	                                                     tag='pb_html'>
	                                                    <option value = "-1"><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
	                                                <%
	                                                	List<Long> drivers = medical_emergency_requestDAO.getAmbulanceDrivers();
	                                                	if(drivers != null)
	                                                	{
	                                                		for(long driver: drivers)
	                                                		{
	                                                			%>
	                                                			<option value = "<%=driver%>" 
	                                                			<%=driver == medical_emergency_requestDTO.driverOrganogramId?"selected":"" %>
	                                                			>
	                                                			<%=WorkflowController.getNameFromOrganogramId(driver, isLangEng)%>
	                                                			</option>
	                                                			<%
	                                                		}
	                                                	}
	                                                %>
	                                            </select>
	                                        </div>
	                                    </div>
		                        
		                        <%
		                        }
		                        %>
		
		
		                    </div>
		                    <div class="modal-footer">
		                        <button type="button" class="btn btn-success" onclick="submitDiv(<%=medical_emergency_requestDTO.iD%>)";
		                                ><%=LM.getText(LC.GLOBAL_SUBMIT, loginDTO)%>
		                        </button>
		                    </div>
	                    
	                    </div>
	                </div>
	
	            </div>
	        </div>
	    </div>
	</div>
    
</td>

 <%
            if(userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE)
            {
            %>
<td class="text-right" id='<%=i%>_checkbox'>
    <div class='checker'>
        <span class='chkEdit'>
            <input type='checkbox' name='ID' value='<%=medical_emergency_requestDTO.iD%>'/>
        </span>
    </div>
</td>
   <%
            }
            %>
																						
											

