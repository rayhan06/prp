<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="asset_type.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    Asset_typeDTO asset_typeDTO;
    asset_typeDTO = (Asset_typeDTO) request.getAttribute("asset_typeDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (asset_typeDTO == null) {
        asset_typeDTO = new Asset_typeDTO();

    }
    System.out.println("asset_typeDTO = " + asset_typeDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.ASSET_TYPE_ADD_ASSET_TYPE_ADD_FORMNAME, loginDTO);
    String servletName = "Asset_typeServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.ASSET_TYPE_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Asset_typeServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD'
                                           id='iD_hidden_<%=i%>' value='<%=asset_typeDTO.iD%>'
                                           tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.ASSET_TYPE_ADD_NAMEEN, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='nameEn'
                                                   id='nameEn_text_<%=i%>'
                                                   value='<%=asset_typeDTO.nameEn%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.ASSET_TYPE_ADD_NAMEBN, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='nameBn'
                                                   id='nameBn_text_<%=i%>'
                                                   value='<%=asset_typeDTO.nameBn%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=asset_typeDTO.searchColumn%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=asset_typeDTO.insertedByUserId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=asset_typeDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=asset_typeDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModifierUser'
                                           id='lastModifierUser_hidden_<%=i%>'
                                           value='<%=asset_typeDTO.lastModifierUser%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value='<%=asset_typeDTO.isDeleted%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=asset_typeDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 mt-3 text-right">
                        <a class="btn btn-sm cancel-btn text-white shadow btn-border-radius"
                           href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.ASSET_TYPE_ADD_ASSET_TYPE_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button class="btn  btn-sm submit-btn text-white shadow btn-border-radius ml-2"
                                type="submit"><%=LM.getText(LC.ASSET_TYPE_ADD_ASSET_TYPE_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Asset_typeServlet");
    }

    function init(row) {


    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






