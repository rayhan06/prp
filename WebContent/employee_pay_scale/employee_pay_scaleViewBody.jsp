<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="employee_pay_scale.*" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@ page import="util.StringUtils" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String Language = LM.getText(LC.EMPLOYEE_PAY_SCALE_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Employee_pay_scaleDTO employee_pay_scaleDTO = new Employee_pay_scaleDAO().getDTOByID(id);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.EMPLOYEE_PAY_SCALE_ADD_EMPLOYEE_PAY_SCALE_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div>
                <h5 class="table-title">
                    <%=LM.getText(LC.EMPLOYEE_PAY_SCALE_ADD_EMPLOYEE_PAY_SCALE_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">
                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_PAY_SCALE_ADD_JOBGRADECAT, loginDTO)%>
                            </b></td>
                            <td><%=CatRepository.getInstance().getText(Language, "job_grade_type", employee_pay_scaleDTO.jobGradeCat)%>
                            </td>
                        </tr>

                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.EMPLOYEE_PAY_SCALE_ADD_NATIONALPAYSCALECAT, loginDTO)%>
                                </b></td>
                            <td><%=CatRepository.getInstance().getText(Language, "national_pay_scale_type", employee_pay_scaleDTO.nationalPayScaleCat)%>
                            </td>
                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_EMPLOYEE_CLASS, loginDTO)%>
                            </b></td>
                            <td><%=CatRepository.getInstance().getText(Language, "employee_class", employee_pay_scaleDTO.employeeClassCat)%>
                            </td>
                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_PAY_SCALE_ADD_PAYSCALESHORT, loginDTO)%>
                            </b></td>
                            <td><%=employee_pay_scaleDTO.payScaleShort%>
                            </td>
                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_PAY_SCALE_ADD_PAYSCALEBRIEF, loginDTO)%>
                            </b></td>
                            <td><%=employee_pay_scaleDTO.payScaleBrief%>
                            </td>
                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_PAY_SCALE_ADD_ISACTIVE, loginDTO)%>
                            </b></td>
                            <td><%=StringUtils.getYesNo(Language, employee_pay_scaleDTO.isActive == 1)%>
                            </td>
                        </tr>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>