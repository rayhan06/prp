<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="employee_pay_scale.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>

<%
    Employee_pay_scaleDTO employee_pay_scaleDTO;
    employee_pay_scaleDTO = (Employee_pay_scaleDTO) request.getAttribute("employee_pay_scaleDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (employee_pay_scaleDTO == null) {
        employee_pay_scaleDTO = new Employee_pay_scaleDTO();

    }
    System.out.println("employee_pay_scaleDTO = " + employee_pay_scaleDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.EMPLOYEE_PAY_SCALE_ADD_EMPLOYEE_PAY_SCALE_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;
    String Language = LM.getText(LC.EMPLOYEE_PAY_SCALE_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i><%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form" id="bigform" name="bigform"
             action="Employee_pay_scaleServlet?actionType=ajax_<%=actionName%>&isPermanentTable=true" enctype="multipart/form-data">
            <!-- FORM BODY SKULL -->
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMPLOYEE_PAY_SCALE_ADD_JOBGRADECAT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='jobGradeCat_div_<%=i%>'>
                                            <select class='form-control' name='jobGradeCat'
                                                    id='jobGradeCat_select2_<%=i%>' tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("job_grade_type", Language, employee_pay_scaleDTO.jobGradeCat)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMPLOYEE_PAY_SCALE_ADD_NATIONALPAYSCALECAT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='nationalPayScaleCat_div_<%=i%>'>
                                            <select class='form-control' name='nationalPayScaleCat'
                                                    id='nationalPayScaleCat_select2_<%=i%>'
                                                    tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("national_pay_scale_type", Language, employee_pay_scaleDTO.nationalPayScaleCat)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMPLOYEE_EMPLOYEE_CLASS, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='employee_class_div_<%=i%>'>
                                            <select class='form-control' name='employeeClass'
                                                    id='employee_class_select2_<%=i%>' tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("employee_class", Language, employee_pay_scaleDTO.employeeClassCat)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMPLOYEE_PAY_SCALE_ADD_ISACTIVE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='isActive_div_<%=i%>'>
                                            <input type='checkbox' class='form-control-sm mt-1' name='isActive'
                                                   id='isActive_checkbox_<%=i%>'
                                                   value='true' <%=(actionName.equals("edit") && String.valueOf(employee_pay_scaleDTO.isActive).equals("1"))?("checked"):""%>
                                                   tag='pb_html'><br>
                                        </div>
                                        <div class="col-8"></div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMPLOYEE_PAY_SCALE_ADD_PAYSCALESHORT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='payScaleShort_div_<%=i%>'>
                                            <input type='text' class='form-control' name='payScaleShort'
                                                   id='payScaleShort_text_<%=i%>'
                                                   value='<%=employee_pay_scaleDTO.payScaleShort%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMPLOYEE_PAY_SCALE_ADD_PAYSCALEBRIEF, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='payScaleBrief_div_<%=i%>'>
                                            <textarea class='form-control' name='payScaleBrief'
                                                      id='payScaleBrief_textarea_<%=i%>'
                                                      style="resize: none" maxlength="1024" rows="4"
                                                      tag='pb_html'><%=employee_pay_scaleDTO.payScaleBrief%></textarea>
                                        </div>
                                    </div>
                                    <!-- Hidden Fields -->
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=employee_pay_scaleDTO.iD%>'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                type="button">
                            <%=LM.getText(LC.EMPLOYEE_PAY_SCALE_ADD_EMPLOYEE_PAY_SCALE_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                type="button" onclick="submitForm()">
                            <%=LM.getText(LC.EMPLOYEE_PAY_SCALE_ADD_EMPLOYEE_PAY_SCALE_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        select2SingleSelector("#jobGradeCat_select2_0", "<%=Language%>");
        select2SingleSelector("#nationalPayScaleCat_select2_0", "<%=Language%>");
        select2SingleSelector("#employee_class_select2_0", "<%=Language%>");
        dateTimeInit("<%=Language%>");

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    });

    const form = $('#bigform');

    function isFormValid() {
        preprocessCheckBoxBeforeSubmitting('isActive', 0 /* row */);
        return true;
    }

    function submitForm(){
        if(isFormValid()){
            submitAjaxForm();
        }
    }

    function buttonStateChange(value){
        $('#submit-btn').prop('disabled',value);
        $('#cancel-btn').prop('disabled',value);
    }
</script>