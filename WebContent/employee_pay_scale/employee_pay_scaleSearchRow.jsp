<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@ page import="util.StringUtils" %>


<td>
    <%=CatRepository.getInstance().getText(Language, "job_grade_type", employee_pay_scaleDTO.jobGradeCat)%>
</td>

<td>
    <%=CatRepository.getInstance().getText(Language, "national_pay_scale_type", employee_pay_scaleDTO.nationalPayScaleCat)%>
</td>
<td>
    <%=CatRepository.getInstance().getText(Language, "employee_class", employee_pay_scaleDTO.employeeClassCat)%>
</td>


<td>
    <%=employee_pay_scaleDTO.payScaleShort%>
</td>


<td>
    <%=employee_pay_scaleDTO.payScaleBrief%>
</td>


<td>
    <%=StringUtils.getYesNo(Language, employee_pay_scaleDTO.isActive == 1)%>
</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Employee_pay_scaleServlet?actionType=view&ID=<%=employee_pay_scaleDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Employee_pay_scaleServlet?actionType=getEditPage&ID=<%=employee_pay_scaleDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=employee_pay_scaleDTO.iD%>'/></span>
    </div>
</td>
																						
											

