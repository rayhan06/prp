<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="employee_pay_scale.*" %>
<%@ page import="util.RecordNavigator" %>

<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="java.util.List" %>
<%@page pageEncoding="UTF-8" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String Language = LM.getText(LC.EMPLOYEE_PAY_SCALE_EDIT_LANGUAGE, loginDTO);
    String navigator2 = SessionConstants.NAV_EMPLOYEE_PAY_SCALE;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();

%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.EMPLOYEE_PAY_SCALE_ADD_JOBGRADECAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_PAY_SCALE_ADD_NATIONALPAYSCALECAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_EMPLOYEE_CLASS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_PAY_SCALE_ADD_PAYSCALESHORT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_PAY_SCALE_ADD_PAYSCALEBRIEF, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_PAY_SCALE_ADD_ISACTIVE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_PAY_SCALE_SEARCH_EMPLOYEE_PAY_SCALE_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="text-center">
                <span><%="English".equalsIgnoreCase(Language) ? "All" : "সকল"%></span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick="">
                </div>
            </th>


        </tr>
        </thead>
        <tbody class="text-nowrap">
        <%
            List<Employee_pay_scaleDTO> data = (List<Employee_pay_scaleDTO>) session.getAttribute(SessionConstants.VIEW_EMPLOYEE_PAY_SCALE);

            if (data != null && data.size() > 0) {
                for (Employee_pay_scaleDTO employee_pay_scaleDTO : data) {
        %>
        <tr>
            <%@include file="employee_pay_scaleSearchRow.jsp" %>
        </tr>
        <% }
        } %>
        </tbody>


    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>


			