<%@page pageEncoding="UTF-8" %>

<%@page import="vm_fuel_vendor.*" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="files.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="static sessionmanager.SessionConstants.FuelStatusMap" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VM_FUEL_VENDOR_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_VM_FUEL_VENDOR;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Vm_fuel_vendorDTO vm_fuel_vendorDTO = (Vm_fuel_vendorDTO) request.getAttribute("vm_fuel_vendorDTO");
    CommonDTO commonDTO = vm_fuel_vendorDTO;
    String servletName = "Vm_fuel_vendorServlet";


    System.out.println("vm_fuel_vendorDTO = " + vm_fuel_vendorDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Vm_fuel_vendorDAO vm_fuel_vendorDAO = (Vm_fuel_vendorDAO) request.getAttribute("vm_fuel_vendorDAO");

    FilesDAO filesDAO = new FilesDAO();

    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td id='<%=i%>_nameBn'>
    <%
        value = vm_fuel_vendorDTO.nameBn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_nameEn'>
    <%
        value = vm_fuel_vendorDTO.nameEn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_mobile'>
    <%
        value = vm_fuel_vendorDTO.mobile + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_address'>
    <%
        value = vm_fuel_vendorDTO.address + "";
    %>
    <%=GeoLocationDAO2.getAddressToShow(value, Language)%>


</td>

<td id='<%=i%>_contractStartDate'>
    <%
        value = vm_fuel_vendorDTO.contractStartDate + "";
    %>
    <%
        String formatted_contractStartDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_contractStartDate, Language)%>


</td>

<td id='<%=i%>_contractEndDate'>
    <%
        value = vm_fuel_vendorDTO.contractEndDate + "";
    %>
    <%
        String formatted_contractEndDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_contractEndDate, Language)%>


</td>

<td id='<%=i%>_status'>

    <%
        value = "";

        value = LM.getText(vm_fuel_vendorDTO.status == 1 ? LC.GLOBAL_STATUS_ACTIVE : LC.GLOBAL_STATUS_INACTIVE, loginDTO);

    %>


    <%=value%>


</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Vm_fuel_vendorServlet?actionType=view&ID=<%=vm_fuel_vendorDTO.iD%>'"
    >
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Vm_fuel_vendorServlet?actionType=getEditPage&ID=<%=vm_fuel_vendorDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=vm_fuel_vendorDTO.iD%>'/></span>
    </div>
</td>
																						
											

