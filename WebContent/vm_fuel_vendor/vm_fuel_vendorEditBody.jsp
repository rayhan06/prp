<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="vm_fuel_vendor.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="static sessionmanager.SessionConstants.FuelStatusMap" %>

<%
	Vm_fuel_vendorDTO vm_fuel_vendorDTO;
	vm_fuel_vendorDTO = (Vm_fuel_vendorDTO)request.getAttribute("vm_fuel_vendorDTO");
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	if(vm_fuel_vendorDTO == null)
	{
		vm_fuel_vendorDTO = new Vm_fuel_vendorDTO();

	}
	System.out.println("vm_fuel_vendorDTO = " + vm_fuel_vendorDTO);

	String actionName;
	System.out.println("actionType = " + request.getParameter("actionType"));
	if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
	{
		actionName = "add";
	}
	else
	{
		actionName = "edit";
	}
	String formTitle = LM.getText(LC.VM_FUEL_VENDOR_ADD_ADD_NEW_VENDOR, loginDTO);
	String servletName = "Vm_fuel_vendorServlet";
	String fileColumnName = "";

	String ID = request.getParameter("ID");
	if(ID == null || ID.isEmpty())
	{
		ID = "0";
	}
	System.out.println("ID = " + ID);
	int i = 0;

	String value = "";

	int childTableStartingID = 1;

	long ColumnID = -1;
	FilesDAO filesDAO = new FilesDAO();
	boolean isPermanentTable = true;
	String Language = LM.getText(LC.VM_FUEL_VENDOR_EDIT_LANGUAGE, loginDTO);
	String Options;
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	Date date = new Date();
	String datestr = dateFormat.format(date);
	CommonDAO.language = Language;
	CatDAO.language = Language;

	CatRepository catRepository = CatRepository.getInstance();
	List<CategoryLanguageModel> modelList = catRepository.getCategoryLanguageModelList("vehicle_fuel");
	VmFuelVendorItemDAO vmFuelVendorItemDAO = new VmFuelVendorItemDAO();

	List<CategoryLanguageModel> unitModelList = catRepository.getCategoryLanguageModelList("vehicle_fuel_item");

%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
	<div class="kt-portlet">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title prp-page-title">
					<i class="fa fa-gift"></i>&nbsp;
					<%=formTitle%>
				</h3>
			</div>
		</div>

		<form class="form-horizontal"  id="bigform" name="bigform" >

			<div class="kt-portlet__body form-body">
				<div class="row mb-4">
					<div class="col-md-10 offset-md-1">
						<div class="onlyborder">
							<div class="row">
								<div class="col-md-10 offset-md-1">
									<div class="sub_title_top">
										<div class="sub_title">
											<h4 style="background: white"><%=formTitle%>
											</h4>
										</div>



										<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=vm_fuel_vendorDTO.iD%>' tag='pb_html'/>

										<div class="form-group row">
											<label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_FUEL_VENDOR_ADD_NAMEBN, loginDTO)%></label>
											<div class="col-md-9">
												<input type='text' class='form-control'  name='nameBn' id = 'nameBn_text_<%=i%>' value='<%=vm_fuel_vendorDTO.nameBn%>' 																  tag='pb_html'/>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_FUEL_VENDOR_ADD_NAMEEN, loginDTO)%></label>
											<div class="col-md-9">
												<input type='text' class='form-control'  name='nameEn' id = 'nameEn_text_<%=i%>' value='<%=vm_fuel_vendorDTO.nameEn%>' 																  tag='pb_html'/>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_FUEL_VENDOR_ADD_MOBILE, loginDTO)%></label>
											<div class="col-md-9">
												<input type='text' class='form-control'  name='mobile' id = 'mobile_text_<%=i%>' value='<%=vm_fuel_vendorDTO.mobile%>' 																<%
													if(!actionName.equals("edit"))
													{
												%>
													   required="required"  pattern="880[0-9]{10}" title="mobile must start with 880, then contain 10 digits"
														<%
															}
														%>
													   tag='pb_html'/>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_FUEL_VENDOR_ADD_ADDRESS, loginDTO)%></label>
											<div class="col-md-9">
												<div id ='address_geoDIV_<%=i%>' tag='pb_html'>
													<select class='form-control' name='address_active' id = 'address_geoSelectField_<%=i%>' onChange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'address', this.getAttribute('row'))" 																 tag='pb_html' row = '<%=i%>'></select>
												</div>
												<input type='text' onkeypress="return (event.charCode != 36 && event.keyCode != 36)" class='form-control' name='address_text' id = 'address_geoTextField_<%=i%>' value=																<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(vm_fuel_vendorDTO.address)  + "'"):("'" + "" + "'")%>
														placeholder='Road Number, House Number etc' tag='pb_html'>
												<input type='hidden' class='form-control'  name='address' id = 'address_geolocation_<%=i%>' value=																<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseID(vm_fuel_vendorDTO.address)  + "'"):("'" + "1" + "'")%>
														tag='pb_html'>
												<%
													if(actionName.equals("edit"))
													{
												%>
												<label class="control-label"><%=GeoLocationDAO2.parseText(vm_fuel_vendorDTO.address, Language) + "," + GeoLocationDAO2.parseDetails(vm_fuel_vendorDTO.address)%></label>
												<%
													}
												%>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_FUEL_VENDOR_ADD_CONTRACTSTARTDATE, loginDTO)%></label>
											<div class="col-md-9">
												<%value = "contractStartDate_js_" + i;%>
												<jsp:include page="/date/date.jsp">
													<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
													<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
												</jsp:include>
												<input type='hidden' name='contractStartDate' id = 'contractStartDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(vm_fuel_vendorDTO.contractStartDate))%>' tag='pb_html'>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_FUEL_VENDOR_ADD_CONTRACTENDDATE, loginDTO)%></label>
											<div class="col-md-9">
												<%value = "contractEndDate_js_" + i;%>
												<jsp:include page="/date/date.jsp">
													<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
													<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
												</jsp:include>
												<%@include file="/date/dateUtils.jsp"%>
												<script>
													resetMaxMinYearById("contractEndDate_js_0", new Date().getFullYear()+10, 1900);
												</script>

												<input type='hidden' name='contractEndDate' id = 'contractEndDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(vm_fuel_vendorDTO.contractEndDate))%>' tag='pb_html'>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_FUEL_VENDOR_ADD_STATUS, loginDTO)%></label>
											<div class="col-md-9">
												<%
													value = "";
													if(vm_fuel_vendorDTO.status != -1)
													{
														value = vm_fuel_vendorDTO.status + "";
													}
												%>

												<select name="status" style="width:100%;" class='form-control' id='status_number_<%=i%>' tag='pb_html'>

													<%
														value="";
														for (Map.Entry<Integer,String> entry : FuelStatusMap.entrySet())    {


															value=value+ "<option value="+entry.getKey()+" >"+LM.getText(entry.getKey()==1?LC.GLOBAL_STATUS_ACTIVE:LC.GLOBAL_STATUS_INACTIVE,loginDTO)+"</option>";

														}
													%>
													<%=value%>

												</select>

											</div>
										</div>
										<div class="form-group row">
											<label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_FUEL_VENDOR_ADD_FILESDROPZONE, loginDTO)%></label>
											<div class="col-md-9">
												<%
													fileColumnName = "filesDropzone";
													if(actionName.equals("edit"))
													{
														List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(vm_fuel_vendorDTO.filesDropzone);
												%>
												<%@include file="../pb/dropzoneEditor.jsp"%>
												<%
													}
													else
													{
														ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
														vm_fuel_vendorDTO.filesDropzone = ColumnID;
													}
												%>

												<div class="dropzone" action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=vm_fuel_vendorDTO.filesDropzone%>">
													<input type='file' style="display:none"  name='<%=fileColumnName%>File' id = '<%=fileColumnName%>_dropzone_File_<%=i%>'  tag='pb_html'/>
												</div>
												<input type='hidden'  name='<%=fileColumnName%>FilesToDelete' id = '<%=fileColumnName%>FilesToDelete_<%=i%>' value=''  tag='pb_html'/>
												<input type='hidden' name='<%=fileColumnName%>' id = '<%=fileColumnName%>_dropzone_<%=i%>'  tag='pb_html' value='<%=vm_fuel_vendorDTO.filesDropzone%>'/>



											</div>
										</div>
										<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=vm_fuel_vendorDTO.searchColumn%>' tag='pb_html'/>
										<div class="form-group row" style="display: none">
											<label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_FUEL_VENDOR_ADD_INSERTEDBY, loginDTO)%></label>
											<div class="col-md-9">
												<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_text_<%=i%>' value='<%=vm_fuel_vendorDTO.insertedBy%>' 																  tag='pb_html'/>
											</div>
										</div>
										<div class="form-group row" style="display: none">
											<label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_FUEL_VENDOR_ADD_MODIFIEDBY, loginDTO)%></label>
											<div class="col-md-9">
												<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_text_<%=i%>' value='<%=vm_fuel_vendorDTO.modifiedBy%>' 																  tag='pb_html'/>
											</div>
										</div>
										<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=vm_fuel_vendorDTO.insertionDate%>' tag='pb_html'/>
										<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=vm_fuel_vendorDTO.isDeleted%>' tag='pb_html'/>

										<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=vm_fuel_vendorDTO.lastModificationTime%>' tag='pb_html'/>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="mt-5">
					<div class="form-body">
						<h5 class="table-title">
							<%=LM.getText(LC.VM_FUEL_VENDOR_ADD_SUPPLY_OF_GOODS, loginDTO)%>
						</h5>
						<div class="table-responsive">
							<table class="table table-bordered table-striped">
								<thead>
								<tr>
									<th><%=LM.getText(LC.VM_FUEL_VENDOR_ADD_VM_FUEL_VENDOR_ITEM_VEHICLEFUELCAT, loginDTO)%></th>
									<th><%=LM.getText(LC.VM_FUEL_VENDOR_ADD_VM_FUEL_VENDOR_ITEM_UNIT, loginDTO)%></th>
									<th><%=LM.getText(LC.VM_FUEL_VENDOR_ADD_VM_FUEL_VENDOR_ITEM_PRICE, loginDTO)%></th>
									<%--										<th><%=LM.getText(LC.HM_UPDATE, loginDTO)%></th>--%>

								</tr>
								</thead>
								<tbody id="field-VmFuelVendorItem">


								<%
									if(actionName.equals("edit")){
										int index = -1;


										for(VmFuelVendorItemDTO vmFuelVendorItemDTO: vm_fuel_vendorDTO.vmFuelVendorItemDTOList)
										{
											index++;

											System.out.println("vmFuelVendorItemDTO jsp = "+vmFuelVendorItemDTO);

								%>

								<tr id = "VmFuelVendorItem_<%=index + 1%>">
									<td style="display: none;">





										<input type='hidden' class='form-control'  name='vmFuelVendorItem.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=vmFuelVendorItemDTO.iD%>' tag='pb_html'/>

									</td>
									<td style="display: none;">





										<input type='hidden' class='form-control'  name='vmFuelVendorItem.vmFuelVendorId' id = 'vmFuelVendorId_hidden_<%=childTableStartingID%>' value='<%=vmFuelVendorItemDTO.vmFuelVendorId%>' tag='pb_html'/>
									</td>
									<td>



										<input type="hidden" class='form-control'  name='vmFuelVendorItem.vehicleFuelCat' id = 'vehicleFuelCat_category_<%=childTableStartingID%>' value="<%=vmFuelVendorItemDTO.vehicleFuelCat%>"   tag='pb_html' >

										<%
											CategoryLanguageModel model1= vmFuelVendorItemDAO.getDTOFromModel(modelList, Long.valueOf(vmFuelVendorItemDTO.vehicleFuelCat));
										%>
										<%=Language.equalsIgnoreCase("English")?model1.englishText:model1.banglaText%>
									</td>
									<td>




										<select class='form-control'  name='vmFuelVendorItem.unit' id = 'vehicleFuelCat_category_<%=childTableStartingID%>'   tag='pb_html'>
											<%
												boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
												String unitOptions =  Utils.buildSelectOption(isLanguageEnglish);
												StringBuilder option = new StringBuilder();

												for (CategoryLanguageModel model2:unitModelList){
													if (vmFuelVendorItemDTO.unit==model2.categoryValue) {
														option.append("<option selected value = '").append(model2.categoryValue).append("'>");
													} else {
														option.append("<option value = '").append(model2.categoryValue).append("'>");
													}
													option.append(isLanguageEnglish ? model2.englishText : model2.banglaText).append("</option>");
												}
												unitOptions+=option.toString();
												//Options = CatRepository.getInstance().buildOptions("vehicle_fuel_item", Language, vmFuelVendorItemDTO.vehicleFuelCat);
											%>
											<%=unitOptions%>
										</select>
									</td>
									<td>





										<%
											value = "";
											if(vmFuelVendorItemDTO.price != -1)
											{
												value = vmFuelVendorItemDTO.price + "";
											}
										%>
										<input type='number' class='form-control'  name='vmFuelVendorItem.price' id = 'price_number_<%=childTableStartingID%>' value='<%=value%>'  tag='pb_html'>
									</td>
									<td style="display: none;">





										<input type='hidden' class='form-control'  name='vmFuelVendorItem.insertedBy' id = 'insertedBy_text_<%=childTableStartingID%>' value='<%=vmFuelVendorItemDTO.insertedBy%>' 																  tag='pb_html'/>
									</td>
									<td style="display: none;">





										<input type='hidden' class='form-control'  name='vmFuelVendorItem.modifiedBy' id = 'modifiedBy_text_<%=childTableStartingID%>' value='<%=vmFuelVendorItemDTO.modifiedBy%>' 																  tag='pb_html'/>
									</td>
									<td style="display: none;">





										<input type='hidden' class='form-control'  name='vmFuelVendorItem.insertionDate' id = 'insertionDate_hidden_<%=childTableStartingID%>' value='<%=vmFuelVendorItemDTO.insertionDate%>' tag='pb_html'/>
									</td>
									<td style="display: none;">





										<input type='hidden' class='form-control'  name='vmFuelVendorItem.isDeleted' id = 'isDeleted_hidden_<%=childTableStartingID%>' value= '<%=vmFuelVendorItemDTO.isDeleted%>' tag='pb_html'/>

									</td>
									<td style="display: none;">





										<input type='hidden' class='form-control'  name='vmFuelVendorItem.lastModificationTime' id = 'lastModificationTime_hidden_<%=childTableStartingID%>' value='<%=vmFuelVendorItemDTO.lastModificationTime%>' tag='pb_html'/>
									</td>

								</tr>
								<%
										childTableStartingID ++;
									}
								}
								else
								{


									int idCount = -1;
									for (CategoryLanguageModel model:modelList)
									{
										VmFuelVendorItemDTO vmFuelVendorItemDTO = new VmFuelVendorItemDTO();
										idCount++;
								%>

								<tr>
									<td style="display: none;">





										<input type='hidden' class='form-control'  name='vmFuelVendorItem.iD' id = 'iD_hidden_<%=idCount%>' value='<%=vmFuelVendorItemDTO.iD%>' tag='pb_html'/>

									</td>
									<td style="display: none;">





										<input type='hidden' class='form-control'  name='vmFuelVendorItem.vmFuelVendorId' id = 'vmFuelVendorId_hidden_<%=idCount%>' value='<%=vmFuelVendorItemDTO.vmFuelVendorId%>' tag='pb_html'/>
									</td>
									<td>
										<input type="hidden" class='form-control'  name='vmFuelVendorItem.vehicleFuelCat' id = 'vehicleFuelCat_category_<%=idCount%>' value="<%=model.categoryValue%>"   tag='pb_html' >
										<%=Language.equalsIgnoreCase("English")?model.englishText:model.banglaText%>
									</td>

									<td>



										<select class='form-control'  name='vmFuelVendorItem.unit' id = 'vehicleMaintenanceCat_category_<%=idCount%>'   tag='pb_html'>
											<%
												//VmFuelVendorItemDTO vmFuelVendorItemDTO2 = new VmFuelVendorItemDTO();
												Options = CatRepository.getInstance().buildOptions("vehicle_fuel_item", Language, vmFuelVendorItemDTO.vehicleFuelCat);
											%>
											<%=Options%>
										</select>

									</td>

									<td>





										<%
											value = "0";
											if(vmFuelVendorItemDTO.price != -1)
											{
												value = vmFuelVendorItemDTO.price + "";
											}
										%>
										<input type='number' class='form-control'  name='vmFuelVendorItem.price' id = 'price_number_<%=idCount%>' value='<%=value%>'  tag='pb_html'>
									</td>
									<td style="display: none">





										<input type='hidden' class='form-control'  name='vmFuelVendorItem.insertedBy' id = 'insertedBy_text_<%=idCount%>' value='<%=vmFuelVendorItemDTO.insertedBy%>' 																  tag='pb_html'/>
									</td>
									<td style="display: none">





										<input type='hidden' class='form-control'  name='vmFuelVendorItem.modifiedBy' id = 'modifiedBy_text_<%=idCount%>' value='<%=vmFuelVendorItemDTO.modifiedBy%>' 																  tag='pb_html'/>
									</td>
									<td style="display: none;">





										<input type='hidden' class='form-control'  name='vmFuelVendorItem.insertionDate' id = 'insertionDate_hidden_<%=idCount%>' value='<%=vmFuelVendorItemDTO.insertionDate%>' tag='pb_html'/>
									</td>
									<td style="display: none;">





										<input type='hidden' class='form-control'  name='vmFuelVendorItem.isDeleted' id = 'isDeleted_hidden_<%=idCount%>' value= '<%=vmFuelVendorItemDTO.isDeleted%>' tag='pb_html'/>

									</td>
									<td style="display: none;">





										<input type='hidden' class='form-control'  name='vmFuelVendorItem.lastModificationTime' id = 'lastModificationTime_hidden_<%=idCount%>' value='<%=vmFuelVendorItemDTO.lastModificationTime%>' tag='pb_html'/>
									</td>
								</tr>

								<%
										}
									}
								%>

								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="form-actions text-right mb-4">
					<button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
						<%=LM.getText(LC.VM_FUEL_VENDOR_ADD_VM_FUEL_VENDOR_CANCEL_BUTTON, loginDTO)%>
					</button>
					<button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button" onclick="submitForm()">
						<%=LM.getText(LC.VM_FUEL_VENDOR_ADD_VM_FUEL_VENDOR_SUBMIT_BUTTON, loginDTO)%>
					</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">





	function PreprocessBeforeSubmiting(row, validate)
	{



		preprocessDateBeforeSubmitting('contractStartDate', row);
		preprocessDateBeforeSubmitting('contractEndDate', row);

		return preprocessGeolocationBeforeSubmitting('address', row, false);



	}


	function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
	{
		addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Vm_fuel_vendorServlet");
	}

	function init(row)
	{

		initGeoLocation('address_geoSelectField_', row, "Vm_fuel_vendorServlet");
		setDateByStringAndId('contractStartDate_js_' + row, $('#contractStartDate_date_' + row).val());
		setDateByStringAndId('contractEndDate_js_' + row, $('#contractEndDate_date_' + row).val());

		for(i = 1; i < child_table_extra_id; i ++)
		{
		}

	}

	var row = 0;
	$(document).ready(function(){
		init(row);
		CKEDITOR.replaceAll();
		$("#cancel-btn").click(e => {
			e.preventDefault();
			location.href = "<%=request.getHeader("referer")%>";
		})
	});

	var child_table_extra_id = <%=childTableStartingID%>;


	// $("#address_geoTextField_0").keypress(function (evt) {
	//
	// 	let keycode = evt.charCode || evt.keyCode;
	// 	if (keycode  == 36) {
	// 		return false;
	// 	}
	// });

	$("#add-more-VmFuelVendorItem").click(
			function(e)
			{
				e.preventDefault();
				var t = $("#template-VmFuelVendorItem");

				$("#field-VmFuelVendorItem").append(t.html());
				SetCheckBoxValues("field-VmFuelVendorItem");

				var tr = $("#field-VmFuelVendorItem").find("tr:last-child");

				tr.attr("id","VmFuelVendorItem_" + child_table_extra_id);

				tr.find("[tag='pb_html']").each(function( index )
				{
					var prev_id = $( this ).attr('id');
					$( this ).attr('id', prev_id + child_table_extra_id);
					console.log( index + ": " + $( this ).attr('id') );
				});


				child_table_extra_id ++;

			});


	$("#remove-VmFuelVendorItem").click(function(e){
		var tablename = 'field-VmFuelVendorItem';
		var i = 0;
		console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
		var element = document.getElementById(tablename);

		var j = 0;
		for(i = document.getElementById(tablename).childNodes.length - 1; i >= 0 ; i --)
		{
			var tr = document.getElementById(tablename).childNodes[i];
			if(tr.nodeType === Node.ELEMENT_NODE)
			{
				console.log("tr.childNodes.length= " + tr.childNodes.length);
				var checkbox = tr.querySelector('input[deletecb="true"]');
				if(checkbox.checked == true)
				{
					tr.remove();
				}
				j ++;
			}

		}
	});
	

	const vehicleReceiveForm = $('#bigform');
	function submitForm(){
		$("#contractStartDate_date_0").val(getDateStringById('contractStartDate_js_0', 'DD/MM/YYYY'));
		$("#contractEndDate_date_0").val(getDateStringById('contractEndDate_js_0', 'DD/MM/YYYY'));

		preprocessGeolocationBeforeSubmitting('address', row, false);
		//if($("#bigform").valid()){
			$.ajax({
				type : "POST",
				url : "Vm_fuel_vendorServlet?actionType=<%=actionName%>",
				data : vehicleReceiveForm.serialize(),
				dataType : 'JSON',
				success : function(response) {

					if(response.responseCode === 0){
						showToastSticky(response.msg,response.msg);
					}else if(response.responseCode === 200){
						window.location.replace(getContextPath()+response.msg);
					}
				},
				error : function(jqXHR, textStatus, errorThrown) {

					toastr.error('Can not added data');
				}
			});
		// }else{
		//
		// }
	}

</script>








