<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_fuel_vendor.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>

<%@ page import="geolocation.*" %>


<%
    String servletName = "Vm_fuel_vendorServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.VM_FUEL_VENDOR_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Vm_fuel_vendorDAO vm_fuel_vendorDAO = new Vm_fuel_vendorDAO("vm_fuel_vendor");
//Vm_fuel_vendorDTO vm_fuel_vendorDTO = (Vm_fuel_vendorDTO)vm_fuel_vendorDAO.getDTOByID(id);
    Vm_fuel_vendorDTO vm_fuel_vendorDTO = Vm_fuel_vendorRepository.getInstance().getVm_fuel_vendorDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<!--new layout start-->


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important; margin-bottom: -18px">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.VM_FUEL_VENDOR_ADD_VM_FUEL_VENDOR_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div>
                <h5 class="table-title">
                    <%=LM.getText(LC.VM_FUEL_VENDOR_ADD_VM_FUEL_VENDOR_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td>
                                <b>
                                    <%=LM.getText(LC.VM_FUEL_VENDOR_ADD_NAMEBN, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%
                                    value = vm_fuel_vendorDTO.nameBn + "";
                                %>
                                <%=Utils.getDigits(value, Language)%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <%=LM.getText(LC.VM_FUEL_VENDOR_ADD_NAMEEN, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%
                                    value = vm_fuel_vendorDTO.nameEn + "";
                                %>
                                <%=Utils.getDigits(value, Language)%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <%=LM.getText(LC.VM_FUEL_VENDOR_ADD_MOBILE, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%
                                    value = vm_fuel_vendorDTO.mobile + "";
                                %>
                                <%=Utils.getDigits(value, Language)%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <%=LM.getText(LC.VM_FUEL_VENDOR_ADD_ADDRESS, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%
                                    value = vm_fuel_vendorDTO.address + "";
                                %>
                                <%=GeoLocationDAO2.getAddressToShow(value, Language)%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <%=LM.getText(LC.VM_FUEL_VENDOR_ADD_CONTRACTSTARTDATE, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%
                                    value = vm_fuel_vendorDTO.contractStartDate + "";
                                %>
                                <%
                                    String formatted_contractStartDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_contractStartDate, Language)%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <%=LM.getText(LC.VM_FUEL_VENDOR_ADD_CONTRACTENDDATE, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%
                                    value = vm_fuel_vendorDTO.contractEndDate + "";
                                %>
                                <%
                                    String formatted_contractEndDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_contractEndDate, Language)%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <%=LM.getText(LC.VM_FUEL_VENDOR_ADD_STATUS, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%
                                    value = "";
                                    value = LM.getText(vm_fuel_vendorDTO.status == 1 ? LC.GLOBAL_STATUS_ACTIVE : LC.GLOBAL_STATUS_INACTIVE, loginDTO);
                                %>
                                <%=value%>
                            </td>
                        </tr>
                    </table>
                </div>
                <h5  class="table-title mt-5">
                    <%=LM.getText(LC.VM_FUEL_VENDOR_ADD_SUPPLY_OF_GOODS, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th><%=LM.getText(LC.VM_FUEL_VENDOR_ADD_VM_FUEL_VENDOR_ITEM_VEHICLEFUELCAT, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.VM_FUEL_VENDOR_ADD_VM_FUEL_VENDOR_ITEM_UNIT, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.VM_FUEL_VENDOR_ADD_VM_FUEL_VENDOR_ITEM_PRICE, loginDTO)%>
                            </th>

                        </tr>
                        <%
                            VmFuelVendorItemDAO vmFuelVendorItemDAO = new VmFuelVendorItemDAO();
                            List<VmFuelVendorItemDTO> vmFuelVendorItemDTOs = vmFuelVendorItemDAO.getVmFuelVendorItemDTOListByVmFuelVendorID(vm_fuel_vendorDTO.iD);

                            CatRepository catRepository = CatRepository.getInstance();
                            List<CategoryLanguageModel> vehicleFueNamelList = catRepository.getCategoryLanguageModelList("vehicle_fuel");
                            List<CategoryLanguageModel> vehicleFuelUnitList = catRepository.getCategoryLanguageModelList("vehicle_fuel_item");
                            boolean isEnglish = Language.equalsIgnoreCase("english");

                            for (VmFuelVendorItemDTO vmFuelVendorItemDTO : vmFuelVendorItemDTOs) {
                        %>
                        <tr>
                            <td>

                                <%
                                    CategoryLanguageModel model1 = vmFuelVendorItemDAO.getDTOFromModelCatID(vehicleFueNamelList, Long.valueOf(vmFuelVendorItemDTO.vehicleFuelCat));
                                    value = isEnglish ? model1.englishText : model1.banglaText;
                                    //System.out.println("vmFuelVendorItemDTO.vehicleFuelCat: "+vmFuelVendorItemDTO.vehicleFuelCat);
                                %>

                                <%=value%>


                            </td>
                            <td>


                                <%

                                    CategoryLanguageModel model2 = vmFuelVendorItemDAO.getDTOFromModelCatID(vehicleFuelUnitList, (long) vmFuelVendorItemDTO.unit);
                                    if (model2 != null) {
                                        value = isEnglish ? model2.englishText : model2.banglaText;
                                    } else {
                                        value = "";
                                    }

                                %>
                                <%=value%>

                            </td>
                            <td>

                                <%
                                    value = String.format("%.1f", vmFuelVendorItemDTO.price);
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>
                        <%

                            }

                        %>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!--new layout end-->
