<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="util.*" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberInWord" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="pb.Utils" %>
<%@ page import="daily_basis_messenger_bill.Daily_basis_messenger_billModel" %>
<%@ page import="daily_basis_messenger_bill.Daily_basis_messenger_billDTO" %>
<%@ page import="budget_register.Budget_registerDTO" %>
<%@ page import="budget_register.Budget_registerDAO" %>
<%@page pageEncoding="UTF-8" %>

<%
    List<Daily_basis_messenger_billDTO> billDTOs = (List<Daily_basis_messenger_billDTO>) request.getAttribute("billDTOs");
    List<Daily_basis_messenger_billModel> billModels =
            billDTOs.stream()
                    .map(billDTO -> new Daily_basis_messenger_billModel(billDTO, "BANGLA"))
                    .collect(Collectors.toList());
    Daily_basis_messenger_billDTO aBillDTO = billDTOs.get(0);
    Budget_registerDTO budgetRegisterDTO = Budget_registerDAO.getInstance().getDTOFromID(aBillDTO.budgetRegisterId);
    long monthYear = System.currentTimeMillis();
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getLanguage(loginDTO);
    String pdfFileName = "Daily Basis Messenger Forwarding " + DateUtils.getMonthYear(monthYear, "English", " ");
%>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    #to-print-div * {
        font-size: 12px;
    }

    #to-print-div h1 {
        font-size: 16px;
        font-weight: bold;
    }

    #to-print-div h2 {
        font-size: 14px;
        font-weight: bold;
    }

    #to-print-div h3 {
        font-size: 13px;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .5in;
        background: white;
        margin-bottom: 10px;
    }

    .table-bordered > :not(caption) > * {
        border-color: black;
    }

    .table-bordered > :not(caption) > * > * {
        border-color: black;
    }

    .table-bordered td,
    .table-bordered th {
        padding: 4px;
    }

    .align-top {
        vertical-align: top;
    }

    thead th,
    thead td {
        text-align: center;
    }
</style>
<div class="kt-content" id="kt_content">
    <div class="row">
        <div class="kt-portlet">
            <div class="kt-portlet__body page-bg" id="bill-div">
                <div class="kt-subheader__main ml-4">
                    <label class="h2 kt-subheader__title" style="color: #00a1d4;">
                        <%=UtilCharacter.getDataByLanguage(
                                Language,
                                "দৈনিক ভিত্তিক বার্তাবাহক কর্মচারীদের বেতন ভাতার ব্যাংক স্টেটমেন্ট",
                                "Daily Basis Messenger's Wages Bank Statement"
                        )%>
                    </label>
                </div>
                <hr style="border-top: 1px solid rgba(0, 0, 0, 0.1); margin: 10px 10px">

                <div class="ml-auto m-3">
                    <button type="button" class="btn" id='download-pdf'
                            onclick="downloadTemplateAsPdf('to-print-div', '<%=pdfFileName%>');">
                        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                </div>

                <div style="margin: auto;">
                    <div class="container" id="to-print-div">
                        <%
                            long runningTotal = 0;
                            boolean isLastPage = false;
                            final int rowsPerPage = 20;
                            int index = 0;
                            while (index < billModels.size()) {
                                boolean isFirstPage = (index == 0);
                        %>
                        <section class="page shadow" data-size="A4">
                            <%if (isFirstPage) {%>
                            <div class="text-center">
                                <h1 style="font-weight: normal">বাংলাদেশ জাতীয় সংসদ সচিবালয়</h1>
                                <h2 style="display: inline-block; border-bottom: 1px solid black">অর্থ শাখা-১</h2>
                                <h2 style="font-weight: normal">
                                    <%=budgetRegisterDTO.description%> বাবদ বিল
                                </h2>
                            </div>
                            <%}%>

                            <div>
                                <div <%=isFirstPage ? "class=\"mt-4\"" : ""%>>
                                    <table class="table-bordered mt-2 w-100">
                                        <thead>
                                        <tr style="height: 50px;background-color:lightgrey">
                                            <th style="width:5%;">ক্র. নং</th>
                                            <th style="width:25%;">নাম</th>
                                            <th style="width:15%;">পদবী</th>
                                            <th style="width:10%;">মোবাইল নম্বর</th>
                                            <th style="width:10%;">সঞ্চয়ী হিসাব নম্বর</th>
                                            <th style="width:10%;">প্রাপ্য মজুরী</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <%if (!isFirstPage) {%>
                                        <tr>
                                            <td colspan="5" class="text-right">
                                                পূর্ব পৃষ্ঠার জের=
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        StringUtils.convertToBanNumber(String.valueOf(runningTotal))
                                                )%>
                                            </td>
                                        </tr>
                                        <%}%>

                                        <%
                                            int rowsInThisPage = 0;
                                            while (index < billModels.size() && rowsInThisPage < rowsPerPage) {
                                                isLastPage = (index == (billModels.size() - 1));
                                                rowsInThisPage++;
                                                Daily_basis_messenger_billModel model = billModels.get(index++);
                                                runningTotal += Long.parseLong(model.totalAmount);
                                        %>
                                        <tr style="height: 40px">
                                            <td>
                                                <%=Utils.getDigits(index, "BANGLA")%>
                                            </td>
                                            <td class="align-top">
                                                <%=model.name%>
                                            </td>
                                            <td class="align-top">
                                                <%=model.designation%>
                                            </td>
                                            <td class="text-center">
                                                <%=Utils.getDigits(model.mobileNumber, "BANGLA")%>
                                            </td>
                                            <td class="text-center">
                                                <%=Utils.getDigits(model.savingAccountNumber, "BANGLA")%>
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(Utils.getDigits(model.totalAmount, "BANGLA"))%>
                                            </td>
                                        </tr>
                                        <%
                                            }
                                        %>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="5" class="text-right">
                                                <%=isLastPage ? "সর্বমোট" : "উপমোট"%>=
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(Utils.getDigits(runningTotal, "BANGLA"))%>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                    <%if (isLastPage) {%>
                                    <div class="mt-2 offset-4 ml-auto text-center">
                                        কথায়:
                                        <strong><%=BangladeshiNumberInWord.convertToWord(Utils.getDigits(runningTotal, "Bangla"))%>
                                        </strong> টাকা মাত্র
                                    </div>
                                    <%}%>
                                </div>
                            </div>
                        </section>
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function downloadTemplateAsPdf(divId, fileName) {
        let content = document.getElementById(divId);
        const opt = {
            margin: 0.1,
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4'}
        };
        html2pdf().from(content).set(opt).save();
    }
</script>