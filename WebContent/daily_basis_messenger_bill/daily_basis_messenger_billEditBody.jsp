<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="payroll_daily_bill.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>
<%@ page import="election_details.Election_detailsRepository" %>

<%@include file="../pb/addInitializer2.jsp" %>

<%
	actionName = request.getParameter("actionType").equals("edit") ? "edit" : "add";
    String formTitle = getDataByLanguage(Language,"অধিবেশনকালীন বার্তাবাহক বিল","Parliament Session Messenger Bill");
	String context = request.getContextPath() + "/";
%>

<style>
	.template-row {
		display: none;
	}
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
	<div class="kt-portlet">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title prp-page-title">
					<i class="fa fa-gift"></i>&nbsp;
					<%=formTitle%>
				</h3>
			</div>
		</div>

        <form class="form-horizontal" id="bill-form" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
					<div class="col-12 row mt-2">
						<div class="col-lg-6 col-md-6 form-group">
							<label class="h5" for="electionDetailsId">
								<%=LM.getText(LC.ELECTION_REPORT_SELECT_ELECTIONDETAILSID, loginDTO)%>
							</label>
							<select id="electionDetailsId"
									name='electionDetailsId'
									class='form-control rounded shadow-sm'
									onchange="electionDetailsChanged();"
							>
								<%=Election_detailsRepository.getInstance().buildOptions(Language,null)%>
							</select>
						</div>

						<div class="col-lg-6 col-md-6 form-group">
							<label class="h5" for="parliamentSessionId">
								<%=getDataByLanguage(Language,"সংসদ অধিবেশন","Parliament Session")%>
							</label>
							<select id="parliamentSessionId" name='parliamentSessionId'
									class='form-control rounded shadow-sm'
									onchange="parliamentSessionChanged();">
								<%--Dynamically Added with AJAX--%>
							</select>
						</div>
					</div>
                </div>

				<div class="mt-4 table-responsive">
					<table id="allowance-table" class="table table-bordered table-striped text-nowrap mt-3">
						<thead>
						<tr>
							<th rowspan="2">
								<%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_EMPLOYEERECORDSID, loginDTO)%>
							</th>
							<th rowspan="2">
								<%=getDataByLanguage(Language,"পিতার নাম","Father's Name")%>
							</th>
							<th rowspan="2">
								<%=getDataByLanguage(
										Language,
										"মোবাইল নাম্বার",
										"Mobile Number"
								)%>
							</th>
							<th rowspan="2">
								<%=getDataByLanguage(
										Language,
										"সঞ্চয়ী হিসাব নাম্বার",
										"Savings Account Number"
								)%>
							</th>
							<th colspan="2" class="text-center">
								<%=getDataByLanguage(
										Language,
										"আদায়",
										"Collection"
								)%>
							</th>
							<th rowspan="2">
								<%=getDataByLanguage(
										Language,
										"মোট টাকার পরিমাণ",
										"Total Amount"
								)%>
							</th>
						</tr>
						<tr>
							<th>
								<%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_DAILYRATE, loginDTO)%>
							</th>
							<th>
								<%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_DAY, loginDTO)%>
							</th>
						</tr>
						</thead>
						<tbody class="main-tbody">
						</tbody>

						<%--don't put these tr inside tbody--%>
						<tr class="loading-gif" style="display: none;">
							<td class="text-center" colspan="100%">
								<img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
								<span><%=getDataByLanguage(Language, "লোড হচ্ছে...", "Loading...")%></span>
							</td>
						</tr>

						<tr class="template-row">
							<input type="hidden" name="dailyBasisMessengerId" value="-1">
							<input type="hidden" name="dailyBasisMessengerBillId" value="-1">
							<td class="row-data-name"></td>
							<td class="row-data-fatherName"></td>
							<td class="row-data-mobileNumber"></td>
							<td class="row-data-savingAccountNumber"></td>
							<td class="row-data-dailyRate"></td>
							<td class="row-data-day"></td>
							<td class="row-data-totalAmount"></td>
						</tr>
					</table>
				</div>

				<div class="row">
					<div class="col-12 mt-3 text-right">
						<button id="prepareSummary-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
								type="button" onclick="submitForm('prepareBankStatement')">
							<%=getDataByLanguage(Language, "ব্যাংক স্টেটমেন্ট প্রস্তুত করুন", "Prepare Bank Statement")%>
						</button>
						<button id="prepareBill-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
								type="button" onclick="submitForm('prepareBill')">
							<%=getDataByLanguage(Language, "বিল প্রস্তুত করুন", "Prepare Bill")%>
						</button>
					</div>
				</div>
            </div>
        </form>
    </div>
</div>

<%@include file="../common/table-sum-utils.jsp"%>
<script src="<%=context%>/assets/scripts/input_validation.js" type="text/javascript"></script>
<script src="<%=context%>/assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script type="text/javascript">
	let actionName = 'add';

	function showOrHideLoadingGif(tableId, toShow) {
		const loadingGif = $('#' + tableId + ' tr.loading-gif');
		if(toShow){
			document.querySelector('#' + tableId + ' tbody.main-tbody').innerHTML = '';
			document.querySelectorAll('#' + tableId + ' tfoot').forEach(tfoot => tfoot.remove());
			loadingGif.show();
		}
		else loadingGif.hide();
	}

	function clearParliamentSession() {
		document.getElementById('parliamentSessionId').innerHTML = '';
	}

	async function electionDetailsChanged() {
		clearNextLevels(0);
		let electionDetailsId = $("#electionDetailsId").val();
		if (electionDetailsId !== -1 && electionDetailsId !== "") {
			let url = 'Mp_travel_allowanceServlet?actionType=getParliamentSession&election_details_id=' + electionDetailsId;
			const response = await fetch(url);
			document.getElementById('parliamentSessionId').innerHTML = await response.text();
		}
	}

	function clearTable() {
		const tableBody = document.querySelector('#allowance-table tbody.main-tbody');
		const emptyTableText = '<%=getDataByLanguage(Language, "কোন তথ্য পাওয়া যায়নি", "No data found")%>';
		tableBody.innerHTML = '<tr class="text-center"><td colspan="100%">'
				+ emptyTableText + '</td></tr>';

		document.querySelectorAll('#allowance-table tfoot')
				.forEach(tfoot => tfoot.remove());
	}

	function clearNextLevels(startIndex) {
		const toClearFunctions = [
			clearParliamentSession, clearTable
		];
		for (let i = startIndex; i < toClearFunctions.length; i++) {
			toClearFunctions[i]();
		}
	}

	async function parliamentSessionChanged() {
		const parliamentSessionId = document.getElementById('parliamentSessionId').value;
		const electionDetailsId = document.getElementById('electionDetailsId').value;
		clearNextLevels(1);
		if (electionDetailsId === '' || parliamentSessionId === '') return;

		showOrHideLoadingGif('allowance-table', true);
		buttonStateChange(true);

		const url = 'Daily_basis_messenger_billServlet?actionType=ajax_getPayrollData'
				  + '&electionDetailsId=' + electionDetailsId
				  + '&parliamentSessionId=' + parliamentSessionId;
		const response = await fetch(url);

		const resJson = await response.json();
		console.log({resJson});

		const {isAlreadyAdded, dailyBillModels, budgetRegisterModel} = resJson;
		console.log("isAlreadyAdded", isAlreadyAdded);
		console.log("dailyBillModels", dailyBillModels);
		actionName = isAlreadyAdded === true ? 'edit' : 'add';

		const tableBody = document.querySelector('#allowance-table tbody');
		const templateRow = document.querySelector('#allowance-table tr.template-row');

		showOrHideLoadingGif('allowance-table', false);
		buttonStateChange(false);
		if(dailyBillModels.length === 0){
			clearTable();
		}
		else{
			dailyBillModels.forEach(model => showModelInTable(tableBody, templateRow, model));
			calculateAllRowTotal();
		}
	}

	function setRowData(templateRow, payrollDailyBillModel) {
		const rowDataPrefix = 'row-data-';
		for (const key in payrollDailyBillModel) {
			const td = templateRow.querySelector('.' + rowDataPrefix + key);
			if (!td) continue;
			if(actionName=="add" && key=="day")
				td.append(getInputElement(key + '_' + payrollDailyBillModel["dailyBasisMessengerId"], key, payrollDailyBillModel[key], onKeyUp));
			else
				td.innerText = payrollDailyBillModel[key];
		}
	}

	function showModelInTable(tableBody, templateRow, payrollDailyBillModel) {
		const modelRow = templateRow.cloneNode(true);
		modelRow.classList.remove('template-row');
		modelRow.querySelector('input[name="dailyBasisMessengerId"]').value = payrollDailyBillModel.dailyBasisMessengerId;
		modelRow.querySelector('input[name="dailyBasisMessengerBillId"]').value = payrollDailyBillModel.dailyBasisMessengerBillId;
		setRowData(modelRow, payrollDailyBillModel);
		tableBody.append(modelRow);
	}

	function calculateAllRowTotal() {
		const colIndicesToSum = [6];
		const totalTitleColSpan = 1;
		const totalTitle = '<%=getDataByLanguage(Language, "সর্বমোট", "Total")%>';
		setupTotalRow('allowance-table', totalTitle, totalTitleColSpan, colIndicesToSum);
	}


	function buttonStateChange(value){
		$('#prepareBill-btn').prop('disabled',value);
		$('#prepareSummary-btn').prop('disabled',value);
	}
	function keyDownEvent(e) {
		$(e.target).data('previousValue', $(e.target).val());
		let isvalid = inputValidationForIntValue(e, $(this), null);
		return true == isvalid;
	}

	function onKeyUp(e) {
		const previousData = $(e.target).data('previousValue');
		const currentData = $(e.target).val();
		const element = document.getElementById(e.target.id).parentElement.parentElement;
		const netAmountElement = element.querySelector('.row-data-totalAmount');
		const dailyRate = element.querySelector('.row-data-dailyRate');
		netAmountElement.innerText = Number(dailyRate.innerText) * Number(currentData);
		calculateAllRowTotal();
	}

	function getInputElement(id, name, value, onKeyUp) {
		const inputElement = document.createElement('input');
		inputElement.classList.add('form-control');
		inputElement.type = 'text';
		inputElement.id = id;
		inputElement.name = name;
		inputElement.value = value;
		inputElement.onkeydown = keyDownEvent;
		if (onKeyUp) inputElement.onkeyup = onKeyUp;
		return inputElement;
	}

	const form = $('#bill-form');
	function submitForm(source){
		if (!form.valid()) return;
		submitAjaxByData(form.serialize(),"Daily_basis_messenger_billServlet?actionType=ajax_" + actionName + "&source=" + source);
	}

    function init() {
		buttonStateChange(true);
		form.validate({
			rules: {
				electionDetailsId: "required",
				parliamentSessionId: "required"
			},
			messages: {
				electionDetailsId: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
				parliamentSessionId: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>'
			}
		});
    }

    $(document).ready(function () {
        init();
    });
</script>