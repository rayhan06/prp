<%@page import="category.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="common.BaseServlet" %>

<%@include file="../pb/addInitializer2.jsp" %>

<%
    CategoryDTO categoryDTO;
    String formTitle;
    if ("ajax_edit".equals(actionName)) {
        categoryDTO = (CategoryDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
        formTitle = LM.getText(LC.CATEGORY_EDIT_CATEGORY_EDIT_FORMNAME, loginDTO);
    } else {
        categoryDTO = new CategoryDTO();
        formTitle = LM.getText(LC.CATEGORY_ADD_CATEGORY_ADD_FORMNAME, loginDTO);
    }
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="CategoryServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>&iD=<%=categoryDTO.iD%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-1"></div>
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="sub_title_top">
                                                <div class="sub_title">
                                                    <h4 style="background: white"><%=formTitle%>
                                                    </h4>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right" for="domainName_text">
                                                    <%=LM.getText(LC.CATEGORY_ADD_DOMAINNAME, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='domainName'
                                                           id='domainName_text' required
                                                           value='<%=categoryDTO.domainName%>' tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right" for="nameEn_text">
                                                    <%=LM.getText(LC.GLOBAL_NAME_ENGLISH, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='nameEn'
                                                           id='nameEn_text' required
                                                           placeholder="<%=LM.getText(LC.CATEGORY_ADD_PLACE_HOLDER_DOMAIN_NAME_ENGLISH, loginDTO)%>"
                                                           value='<%=categoryDTO.nameEn%>' tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right" for="nameBn_text">
                                                    <%=LM.getText(LC.GLOBAL_NAME_BANGLA, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='nameBn'
                                                           id='nameBn_text' required
                                                           placeholder="<%=LM.getText(LC.CATEGORY_ADD_PLACE_HOLDER_DOMAIN_NAME_BANGLA, loginDTO)%>"
                                                           value='<%=categoryDTO.nameBn%>' tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right" for="value_number">
                                                    <%=LM.getText(LC.CATEGORY_ADD_VALUE, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='number' class='form-control' name='value'
                                                           id='value_number' required
                                                           placeholder="<%=LM.getText(LC.CATEGORY_ADD_PLACE_HOLDER_DOMAIN_VALUE, loginDTO)%>"
                                                           value='<%=categoryDTO.value%>'
                                                           tag='pb_html'>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right" for="value_number">
                                                    <%=Language.equalsIgnoreCase("english")?"Ordering":"ক্রম"%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='number' class='form-control' name='ordering'
                                                           id='ordering' 
                                                           
                                                           value='<%=categoryDTO.ordering%>'
                                                           tag='pb_html'>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right" for="value_number">
                                                    <%=Language.equalsIgnoreCase("english")?"Parent Domain":"প্যারেন্ট ডোমেইন"%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='parentDomain'
                                                           id='parentDomain' 
                                                           
                                                           value='<%=categoryDTO.parentDomain%>'
                                                           tag='pb_html'>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right" for="value_number">
                                                    <%=Language.equalsIgnoreCase("english")?"Parent Domain Value":"প্যারেন্ট ডোমেইনের মান"%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='number' class='form-control' name='parentDomainVal'
                                                           id='parentDomainVal' 
                                                           
                                                           value='<%=categoryDTO.parentDomainVal%>'
                                                           tag='pb_html'>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10 form-actions text-right">
                        <button class="btn btn-sm cancel-btn text-white shadow btn-border-radius"
                                href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.CATEGORY_ADD_CATEGORY_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button class="btn  btn-sm submit-btn text-white shadow btn-border-radius" onclick="submitForm()"
                                type="button"><%=LM.getText(LC.CATEGORY_ADD_CATEGORY_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<%=contextOfPath%>assets/scripts/input_validation.js"></script>
<script type="text/javascript">
    const form = $("#bigform");
    $(document).ready(() => {
        form.validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            messages: {
                domainName: '<%=LM.getText(LC.CATEGORY_ADD_PLACE_HOLDER_DOMAIN_NAME, loginDTO)%>',
                nameEn: '<%=LM.getText(LC.CATEGORY_ADD_PLACE_HOLDER_DOMAIN_NAME_ENGLISH, loginDTO)%>',
                nameBn: '<%=LM.getText(LC.CATEGORY_ADD_PLACE_HOLDER_DOMAIN_NAME_BANGLA, loginDTO)%>',
                value: '<%=LM.getText(LC.CATEGORY_ADD_PLACE_HOLDER_DOMAIN_VALUE, loginDTO)%>'
            }
        });
    });

    function submitForm() {
        buttonStateChange(true);
        if (form.valid()) {
            submitAjaxForm('bigform');
        } else {
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value) {
        $('.btn').prop('disabled', value);
    }
</script>