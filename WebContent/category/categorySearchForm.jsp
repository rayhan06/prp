<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="category.*" %>
<%@ page import="java.util.List" %>
<%@page pageEncoding="UTF-8" %>

<%
    String navigator2 = "";
    String servletName = "CategoryServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>

<%
    List<CategoryDTO> data = (List<CategoryDTO>) rn2.list;
    if (data != null && data.size() > 0) {
%>
<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.CATEGORY_ADD_DOMAINNAME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.GLOBAL_NAME_ENGLISH, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.GLOBAL_NAME_BANGLA, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.CATEGORY_ADD_VALUE, loginDTO)%>
            </th>
            <th><%=Language.equalsIgnoreCase("english")?"Ordering":"ক্রম"%>
            </th>
            <th><%=Language.equalsIgnoreCase("english")?"Parent Domain":"প্যারেন্ট ডোমেন"%>
            </th>
            <th><%=Language.equalsIgnoreCase("english")?"Parent Domain Value":"প্যারেন্ট ডোমেইনের মান"%>
            </th>
            <th><%=LM.getText(LC.CATEGORY_SEARCH_CATEGORY_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="text-center">
                <span><%="English".equalsIgnoreCase(Language)?"All":"সকল"%></span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            for (CategoryDTO categoryDTO : data) {
        %>
        <tr>
            <td>
                <%=categoryDTO.domainName%>
            </td>

            <td>
                <%=categoryDTO.nameEn%>
            </td>

            <td>
                <%=categoryDTO.nameBn%>
            </td>

            <td>
                <%=categoryDTO.value%>
            </td>
            
            <td>
                <%=categoryDTO.ordering%>
            </td>
            
            <td>
                <%=categoryDTO.parentDomain%>
            </td>
            
            <td>
                <%=categoryDTO.parentDomainVal%>
            </td>

            <td>
                <button type="button" class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #ff6b6b"
                        onclick="location.href='CategoryServlet?actionType=getEditPage&ID=<%=categoryDTO.iD%>'">
                    <i class="fa fa-edit"></i>
                </button>
            </td>

            <td>
                <div class='checker' style="text-align: right">
                    <span class='chkEdit'><input type='checkbox' name='ID' value='<%=categoryDTO.iD%>'/></span>
                </div>
            </td>
        </tr>
        <% } %>
        </tbody>
    </table>
</div>
<%
} else {
%>
<label style="width: 100%;text-align: center;font-size: larger;font-weight: bold;color: red">
    <%=isLanguageEnglish ? "No information is found" : "কোন তথ্য পাওয়া যায় নি"%>
</label>
<%
    }
%>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>