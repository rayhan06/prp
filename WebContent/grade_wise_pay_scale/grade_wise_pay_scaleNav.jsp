<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="national_pay_scale.National_pay_scaleRepository" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    System.out.println("Inside nav.jsp");
    String url = "Grade_wise_pay_scaleServlet?actionType=search";
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed(false)' id='anyfield' name='anyfield'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-group">
                <div class="tooltip tooltip-portlet tooltip bs-tooltip-top" role="tooltip" id="tooltip_p6zf7aqcpv"
                     aria-hidden="true" x-placement="top"
                     style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">
                    <div class="tooltip-arrow arrow" style="left: 34px;"></div>
                    <div class="tooltip-inner">Collapse</div>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">

            <div class="row">
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-3 col-form-label"><%=LM.getText(LC.GRADE_WISE_PAY_SCALE_ADD_NATIONAPAYSCALEID, loginDTO)%>
                        </label>
                        <div class="col-9">

                            <select class='form-control' name='nationalPayScaleID'
                                    id='nationalPayScaleID_select2' tag='pb_html' onChange='setSearchChanged()'>
                                <%=National_pay_scaleRepository.getInstance().buildOptions(Language, null)%>
                            </select>


                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-3 col-form-label"><%=LM.getText(LC.GLOBAL_Grade, loginDTO)%>
                        </label>
                        <div class="col-9">
                            <select class='form-control' name='gradeTypeId' onChange='setSearchChanged()'
                                    id='gradeTypeIdSelect' tag='pb_html'>
                                <%=CatRepository.getInstance().buildOptions("job_grade", Language, null)%>
                            </select>

                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-3 col-form-label"><%=LM.getText(LC.GRADE_WISE_PAY_SCALE_ADD_GRADEYEAR, loginDTO)%>
                        </label>
                        <div class="col-9">

                            <input type='text' class='form-control' name='grade_year'
                                   data-only-number="true" id="grade_year" placeholder=""
                                   onChange='setSearchChanged()'>

                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed(true)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>
<script type="text/javascript">
    const anyFieldSelector = $('#anyfield');
    function getParams() {
        var params = 'AnyField=' + document.getElementById('anyfield').value;

        params += '&gradeTypeId=' + $('#gradeTypeIdSelect').val();
        params += '&nationalPayScaleID=' + $('#nationalPayScaleID_select2').val();
        params += '&grade_year=' + $('#grade_year').val();

        params += '&search=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }

        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;

        return params;
    }

    function allfield_changed(setPushState) {
        var params = getParams();
        navsubmit(params, "Grade_wise_pay_scaleServlet", setPushState);
    }

    function resetFields() {
        $("#anyfield").val("");
        $("#gradeTypeId").val("");
        $("#nationalPayScaleID").val("");
        $("#grade_year").val("");
    }

    window.addEventListener('popstate', e => {
        if (e.state) {
            let params = e.state;
            navsubmit(params, "Test_moduleServlet", false);
            resetFields();
            let arr = params.split('&');
            arr.forEach(e => {
                let item = e.split('=');
                if (item.length === 2) {
                    //console.log("param " + item[0] + " = " + item[1]);
                    switch (item[0]) {
                        case 'anyfield':
                            $("#anyfield").val(item[1]);
                            break;
                        case 'gradeTypeId':
                            $("#anyfield").val(item[1]);
                            break;
                        case 'nationalPayScaleID':
                            $("#name_en").val(item[1]);
                            break;
                        case 'grade_year':
                            $("#grade_year").val(item[1]);
                            break;

                    }
                }
            });
        } else {
            navsubmit(null, "Test_moduleServlet", false);
        }
    });

    window.onbeforeunload = function () {
        var params = getParams();
        history.pushState(params, '', 'Grade_wise_pay_scaleServlet?actionType=search&' + params);
    };

    $(document).ready(() => {
        readyInit('Grade_wise_pay_scaleServlet');
        initNumberInput()

    });

    function keyDownEvent(e) {
        let isvalid = inputValidationForIntValue(e, $(this), 999999);
        return true == isvalid;
    }

    function initNumberInput() {
        document.querySelectorAll('[data-only-number="true"]')
            .forEach(inputField => inputField.onkeydown = keyDownEvent);
    }
</script>

