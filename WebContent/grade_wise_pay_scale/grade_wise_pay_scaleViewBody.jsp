<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="grade_wise_pay_scale.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>
<%@ page import="national_pay_scale.National_pay_scaleDTO" %>
<%@ page import="national_pay_scale.National_pay_scaleRepository" %>


<%
    String servletName = "Grade_wise_pay_scaleServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Grade_wise_pay_scaleDTO grade_wise_pay_scaleDTO = Grade_wise_pay_scaleDAO.getInstance().getDTOByID(id);
    CommonDTO commonDTO = grade_wise_pay_scaleDTO;
%>
<%@include file="../pb/viewInitializer.jsp" %>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.GRADE_WISE_PAY_SCALE_ADD_GRADE_WISE_PAY_SCALE_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.GRADE_WISE_PAY_SCALE_ADD_GRADE_WISE_PAY_SCALE_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.GRADE_WISE_PAY_SCALE_ADD_NATIONAPAYSCALEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            National_pay_scaleDTO national_pay_scaleDTO = National_pay_scaleRepository.getInstance().copyNational_pay_scaleDTOByiD(grade_wise_pay_scaleDTO.nationalPayScaleID);
                                            if (national_pay_scaleDTO != null) {
                                        %>
                                        <%=isLanguageEnglish ? national_pay_scaleDTO.nameEn :
                                                national_pay_scaleDTO.nameBn%>
                                        <%
                                            }
                                        %>
                                    </div>
                                </div>


                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.GLOBAL_Grade, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=CatRepository.getInstance().getText(Language, "job_grade", grade_wise_pay_scaleDTO.gradeTypeId)%>
                                    </div>
                                </div>


                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.GRADE_WISE_PAY_SCALE_ADD_GRADEYEAR, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=Utils.getDigits(grade_wise_pay_scaleDTO.gradeYear, Language)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.GRADE_WISE_PAY_SCALE_ADD_SALARY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=Utils.getDigits(grade_wise_pay_scaleDTO.salary, Language)%>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>