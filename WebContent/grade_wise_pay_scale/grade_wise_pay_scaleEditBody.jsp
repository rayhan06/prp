<%@page import="grade_wise_pay_scale.*" %>

<%@page pageEncoding="UTF-8" %>

<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@ page import="util.*" %>
<%@ page import="national_pay_scale.National_pay_scaleRepository" %>

<%
    Grade_wise_pay_scaleDTO grade_wise_pay_scaleDTO = new Grade_wise_pay_scaleDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        grade_wise_pay_scaleDTO = Grade_wise_pay_scaleDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = grade_wise_pay_scaleDTO;
    String tableName = "grade_wise_pay_scale";
    String context = request.getContextPath() + "/";

%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.GRADE_WISE_PAY_SCALE_ADD_GRADE_WISE_PAY_SCALE_ADD_FORMNAME, loginDTO);
    String servletName = "Grade_wise_pay_scaleServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Grade_wise_pay_scaleServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=grade_wise_pay_scaleDTO.iD%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.GRADE_WISE_PAY_SCALE_ADD_NATIONAPAYSCALEID, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <select class='form-control' name='nationalPayScaleID'
                                                    id='nationalPayScaleID_select2_<%=i%>' tag='pb_html'>
                                                <%=National_pay_scaleRepository.getInstance().buildOptions(Language, grade_wise_pay_scaleDTO.nationalPayScaleID)%>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="gradeTypeIdSelect"
                                               class="col-4 col-form-label text-right"><%=LM.getText(LC.GLOBAL_Grade, loginDTO)%>
                                          </label>
                                        <div class="col-8">
                                            <select class='form-control' name='gradeTypeId'
                                                    id='gradeTypeIdSelect' tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("job_grade", Language, grade_wise_pay_scaleDTO.gradeTypeId)%>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.GRADE_WISE_PAY_SCALE_ADD_GRADEYEAR, loginDTO)%>
                                        </label>
                                        <div class="col-8">

                                            <input type='text' class='form-control' name='gradeYear'
                                                   data-only-number="true"
                                                   id='gradeYear_number_<%=i%>'
                                                   value='<%=grade_wise_pay_scaleDTO.gradeYear%>' tag='pb_html'>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.GRADE_WISE_PAY_SCALE_ADD_SALARY, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='salary'
                                                   data-only-number="true"
                                                   id='salary_number_<%=i%>'
                                                   value='<%=grade_wise_pay_scaleDTO.salary%>' tag='pb_html'>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.GRADE_WISE_PAY_SCALE_ADD_GRADE_WISE_PAY_SCALE_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" id="submit-btn"
                                    type="submit">
                                <%=LM.getText(LC.GRADE_WISE_PAY_SCALE_ADD_GRADE_WISE_PAY_SCALE_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script type="text/javascript">
    const cancelBtn = $('#cancel-btn');
    const submitBtn = $('#submit-btn');
    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';

    function PreprocessBeforeSubmiting(row, action) {
        console.log("action = " + action);
        let msg = null;
        if (document.getElementById('nationalPayScaleID_select2_0').value === '') {
            msg = isLangEng ? "National pay scale is not selected" : "জাতীয় বেতন স্কেল বাছাই করা হয় নি";
        } else if (document.getElementById('gradeTypeIdSelect').value === '') {
            msg = isLangEng ? "Grade type is not selected" : "গ্রেড বাছাই করা হয় নি";
        } else if (document.getElementById('gradeYear_number_0').value === '') {
            msg = isLangEng ? "Grade year not found" : "বছরের স্তর পাওয়া যায় নি";
        } else if (document.getElementById('salary_number_0').value === '') {
            msg = isLangEng ? "Basic salary not found" : "মূল বেতন পাওয়া যায় নি";
        }

        if (msg) {
            $('#toast_message').css('background-color', '#ff6063');
            showToastSticky(msg, msg);
            buttonStateChange(false);
            return false;
        }

        submitAddForm2();
        return false;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Grade_wise_pay_scaleServlet");
    }

    function init(row) {

        $("#nationalPayScaleID_select2_" + row).select2({
            dropdownAutoWidth: true
        });
        $("#gradeTypeIdSelect").select2({
            dropdownAutoWidth: true
        });
        initNumberInput();

    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    function keyDownEvent(e) {
        let isvalid = inputValidationForIntValue(e, $(this), 999999);
        return true == isvalid;
    }

    function initNumberInput() {
        document.querySelectorAll('[data-only-number="true"]')
            .forEach(inputField => inputField.onkeydown = keyDownEvent);
    }

    function buttonStateChange(value) {
        cancelBtn.prop("disabled", value);
        submitBtn.prop("disabled", value);
    }
</script>






