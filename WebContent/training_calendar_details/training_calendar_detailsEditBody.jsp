<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="training_calendar_details.*" %>
<%@page import="java.util.*" %>
<%@ page import="java.text.SimpleDateFormat" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="training_calender.Training_calenderDAO" %>
<%@ page import="training_calender.Training_calenderDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="util.StringUtils" %>

<%
    Training_calendar_detailsDTO training_calendar_detailsDTO;
    training_calendar_detailsDTO = (Training_calendar_detailsDTO) request.getAttribute("training_calendar_detailsDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (training_calendar_detailsDTO == null) {
        training_calendar_detailsDTO = new Training_calendar_detailsDTO();

    }
    System.out.println("training_calendar_detailsDTO = " + training_calendar_detailsDTO);


    String formTitle = LM.getText(LC.TRAINING_CALENDAR_DETAILS_ADD_TRAINING_CALENDAR_DETAILS_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }

    System.out.println("ID = " + ID);
    long id = Long.parseLong(ID);
    Training_calenderDAO training_calenderDAO = Training_calenderDAO.getInstance();
    Training_calenderDTO training_calenderDTO = training_calenderDAO.getDTOFromID(id);
    int i = 0;

    String value;

    int childTableStartingID = 1;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Training_calendar_detailsDAO training_calendar_detailsDAO = new Training_calendar_detailsDAO();
    List<Training_calendar_detailsDTO> trainingCalendarDetailsDTOList = training_calendar_detailsDAO.getByForeignKey(String.valueOf(id));
%>

<%


    String Language = LM.getText(LC.TRAINING_CALENDAR_DETAILS_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form"
              action="Training_calendar_detailsServlet"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting()">
            <div class="kt-portlet__body form-body">
                <div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap">
                            <tr>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_NAMEEN, loginDTO)%>
                                    </b></td>
                                <td><%=training_calenderDTO.nameEn%>
                                </td>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_NAMEBN, loginDTO)%>
                                    </b></td>
                                <td><%=training_calenderDTO.nameBn%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_TRAININGTYPECAT, loginDTO)%>
                                    </b></td>
                                <td><%=CatRepository.getInstance().getText(Language, "training_type", training_calenderDTO.trainingTypeCat)%>
                                </td>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_TRAININGMODECAT, loginDTO)%>
                                    </b></td>
                                <td><%=CatRepository.getInstance().getText(Language, "training_mode", training_calenderDTO.trainingModeCat)%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_STARTDATE, loginDTO)%>
                                    </b></td>
                                <td>
                                    <%
                                        value = training_calenderDTO.startDate + "";
                                    %>
                                    <%
                                        String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                    %>
                                    <%=formatted_startDate%>
                                </td>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_ENDDATE, loginDTO)%>
                                    </b></td>
                                <td><%
                                    value = training_calenderDTO.endDate + "";
                                %>
                                    <%
                                        String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                    %>
                                    <%=formatted_endDate%>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_STARTTIME, loginDTO)%>
                                    </b></td>
                                <td>
                                    <%=training_calenderDTO.startTime%>
                                </td>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_ENDTIME, loginDTO)%>
                                    </b></td>
                                <td><%=training_calenderDTO.endTime%>
                                </td>
                            </tr>

                        </table>
                    </div>
                    <div class="mt-5">
                        <!-- Button trigger modal -->
                        <div class="row">
                            <div class="col-12 text-right">
                                <button type="button"
                                        class="btn btn-sm btn-primary shadow btn-border-radius mb-3"
                                        id="addToTable_modal_button">
                                    <%=LM.getText(LC.DISCIPLINARY_LOG_ADD_TAG_EMPLOYEE, userDTO)%>
                                </button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped text-nowrap">
                                <thead></thead>

                                <tbody id="tagged_emp_table">
                                <tr style="display: none;">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <button type="button"
                                                class="btn btn-sm delete-trash-btn"
                                                onclick="remove_containing_row(this,'tagged_emp_table');">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                <%if (trainingCalendarDetailsDTOList != null) {%>
                                <%for (Training_calendar_detailsDTO detailsDTO : trainingCalendarDetailsDTOList) {%>

                                <tr>
                                    <td>
                                        <%--employeeNumber and user name is same--%>
                                        <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(detailsDTO.employeeRecordsId))%>
                                        <%--                                        <%=detailsDTO.employeeRecordsId%>--%>
                                    </td>
                                    <td>
                                        <%=isLanguageEnglish ? detailsDTO.employeeNameEn : detailsDTO.employeeNameBn%>
                                    </td>
                                    <td>
                                        <%=isLanguageEnglish ? (detailsDTO.organogramNameEn + ", " + detailsDTO.officeNameEn)
                                                : (detailsDTO.organogramNameBn + ", " + detailsDTO.officeNameBn)%>
                                    </td>
                                    <td id='<%=detailsDTO.employeeRecordsId%>_td_button'>
                                        <button type="button"
                                                class="btn btn-sm delete-trash-btn"
                                                onclick="remove_containing_row(this,'tagged_emp_table');">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                <%}%>
                                <%}%>
                                </tbody>
                            </table>
                        </div>
                        <input type='hidden' class='form-control'
                               name='addedEmployees'
                               id='addedEmployees' value=''/>
                    </div>
                    <input type='hidden' class='form-control' name='id'
                           id='id_hidden_<%=i%>'
                           value='<%=training_calendar_detailsDTO.iD%>'/>
                    <input type='hidden' class='form-control'
                           name='trainingCalendarId'
                           id='trainingCalendarId_hidden_<%=i%>'
                           value=<%=id%>
                                   tag='pb_html'/>
                </div>
                <div class="text-right mt-3">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.TRAINING_CALENDAR_DETAILS_ADD_TRAINING_CALENDAR_DETAILS_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.TRAINING_CALENDAR_DETAILS_ADD_TRAINING_CALENDAR_DETAILS_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">
    language = "<%=Language%>";

    $(document).ready(function () {

        dateTimeInit("<%=Language%>");

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    });

    function PreprocessBeforeSubmiting() {
        document.getElementById("addedEmployees").value = JSON.stringify(
            Array.from(added_employee_info_map.values())
        );
        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Training_calendar_detailsServlet");
    }

    function init(row) {


    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;
    // modal on load event
    $('#tag_emp_modal').on('show.bs.modal', function () {
        // reset layer 1 to ""
        $('#office_layer_1').val("");
        // clear search table
        document.getElementById('search_table').innerHTML = "";

        // hide and clear layer 2 to 10
        for (let i = 2; i <= 10; i++) {
            document.getElementById("office_layer_div_" + i).style.display = 'none';
            document.getElementById("office_layer_" + i).innerHTML = '';
        }
    });

    function hide_selected_employees(table_name, selected_employees_map_or_set) {
        console.log("filtered");
        console.log("added employees");
        console.log(selected_employees_map_or_set);
        let table_obj = document.getElementById(table_name);
        let n_row = table_obj.rows.length;
        for (let i = 0; i < n_row; i++) {
            let row = table_obj.rows[i];
            let employee_record_id = row.id.split('_')[0];
            if (selected_employees_map_or_set.has(employee_record_id)) {
                hide(row);
            } else {
                un_hide(row);
            }
        }
    }

    added_employee_info_map = new Map(
        <%if(trainingCalendarDetailsDTOList != null){%>
        <%=trainingCalendarDetailsDTOList.stream()
                        .map(detailsDTO -> "['" + detailsDTO.employeeRecordsId + "'," + detailsDTO.getIdsStrInJsonString() + "]")
                        .collect(Collectors.joining(",","[","]"))
        %>
        <%}%>
    );
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_emp_table', {
                info_map: added_employee_info_map,
                isSingleEntry: false
            }]
        ]
    );
    modal_button_dest_table = 'none';

    $('#addToTable_modal_button').on('click', function () {
        //alert('CLICKED');
        modal_button_dest_table = 'tagged_emp_table';
        $('#search_emp_modal').modal();
    });

    function remove_containing_row(button, table_name) {
        let containing_row = button.parentNode.parentNode;
        let containing_table = containing_row.parentNode;
        containing_table.deleteRow(containing_row.rowIndex);

        // button id = "<employee record id>_button"
        let td_button = button.parentNode;
        let employee_record_id = td_button.id.split("_")[0];

        let added_info_map = table_name_to_collcetion_map.get(table_name).info_map;
        console.log("delete (employee_record_id)");
        added_info_map.delete(employee_record_id);
        console.log(added_info_map);
    }

</script>






