<%@ page import="training_calendar_details.Training_calendar_detailsDTO" %>
<%@ page import="training_calendar_details.Training_calendar_detailsDAO" %>
<%@ page import="training_calender.Training_calenderDTO" %>
<%@ page import="training_calender.TrainingCalendarStatusEnum" %>
<td style="vertical-align: middle;text-align: left"><%=trainingItem.trainingCalendarName%>
</td>
<td style="vertical-align: middle;text-align: left"><%=trainingItem.trainingType%>
</td>
<td style="vertical-align: middle;text-align: left"><%=trainingItem.startDateAndTime%>
</td>
<td style="vertical-align: middle;text-align: left"><%=trainingItem.endDateAndTime%>
</td>
<td style="vertical-align: middle;text-align: left"><%

    Training_calenderDTO training_calenderDTO = trainingItem.trainingCalenderDTO;
    Training_calendar_detailsDTO training_calendar_detailsDTO = new Training_calendar_detailsDAO().getDTOByCalendarIdAndEmployeeRecordId(training_calenderDTO.iD, empId);
    boolean isAbleToSubmitFeedback = training_calenderDTO.status == TrainingCalendarStatusEnum.COMPLETED.getValue()
            && !training_calendar_detailsDTO.isFeedBackDone
            && !training_calenderDTO.isPrivate;
    if (isAbleToSubmitFeedback) {
%>
    <a href='FeedbackQuestionAnswerServlet?trainCalId=<%=training_calenderDTO.iD%>&data=training'>Feedback</a>
    <%}%>
</td>
<%
    if (!actionType.equals("viewSummary")) {
%>
<td style="text-align: center; vertical-align: middle;">
    <div class="btn-group" role="group" aria-label="Basic example">
        <button style="max-height: 30px" class="btn-primary"
                title="<%=LM.getText(LC.HR_MANAGEMENT_BUTTON_VIEW_DETAILS, loginDTO)%>"
                onclick="location.href='Training_calenderServlet?actionType=view&ID=<%=trainingItem.trainingCalendarId%>&userId=<%=request.getParameter("userId")%>'">
            <i class="fa fa-eye"></i></button>
        <%if (training_calenderDTO.isPrivate) {%>
        <form action="Training_calenderServlet?isPermanentTable=true&actionType=delete&tab=4&data=trainingCalender&empId=<%=empId%>&ID=<%=trainingItem.trainingCalendarId%>"
              method="POST" id="tableForm_training<%=trainingItem.trainingCalendarId%>" enctype="multipart/form-data">
            <button type="button" class="btn-sm border-0 shadow btn-border-radius text-white"
                    style="background-color: #ff6b6b; margin-left: 10px;"
                    onclick="location.href='Training_calenderServlet?actionType=getEditPage&tab=4&data=trainingCalender&empId=<%=empId%>&ID=<%=trainingItem.trainingCalendarId%>&userId=<%=request.getParameter("userId")%>'">
                <i class="fa fa-edit"></i>
            </button>
            <button type="button" class="btn-sm border-0 shadow btn-border-radius text-white"
                    title="<%=isLangEng? "Delete":"ডিলিট"%>"
                    style="background-color: #ff6b6b; margin-left: 10px;"
                    onclick="deleteItem('tableForm_training','<%=trainingItem.trainingCalendarId%>')">
                <i class="fa fa-trash"></i>
            </button>
        </form>
        <%}%>
    </div>
</td>
<%
    }
%>