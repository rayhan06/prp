<%@page import="event_calendar.*" %>
<%@page import="files.FilesDAO" %>
<%@page import="files.FilesDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@page import="pb.CatDAO" %>

<%@ page language="java" %>
<%@ page import="pb.Utils" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="workflow.WorkflowController" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<%@page import="java.util.List" %>


<%
    String servletName = "Event_calendarServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.EVENT_CALENDAR_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Event_calendarDAO event_calendarDAO = new Event_calendarDAO("event_calendar");
    Event_calendarDTO event_calendarDTO = event_calendarDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENT_CALENDAR_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="div_border office-div">
                <h5 class="table-title">
                    <%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENT_CALENDAR_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                <table class="table table-bordered table-striped text-nowrap">
                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EVENT_CALENDAR_ADD_NAMEEN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = event_calendarDTO.nameEn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EVENT_CALENDAR_ADD_NAMEBN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = event_calendarDTO.nameBn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENTCAT, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = event_calendarDTO.eventCat + "";
                            %>
                            <%
                                value = CatDAO.getName(Language, "event", event_calendarDTO.eventCat);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENTLOCATION, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = event_calendarDTO.eventLocation + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENTDATE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = event_calendarDTO.eventDate + "";
                            %>
                            <%
                                String formatted_eventDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                            %>
                            <%=Utils.getDigits(formatted_eventDate, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENTSTARTTIME, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = event_calendarDTO.eventStartTime + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENTENDTIME, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = event_calendarDTO.eventEndTime + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENTDESCRIPTION, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = event_calendarDTO.eventDescription + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EVENT_CALENDAR_ADD_FILESDROPZONE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = event_calendarDTO.filesDropzone + "";
                            %>
                            <%
                                {
                                    List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(event_calendarDTO.filesDropzone);
                            %>
                            <%@include file="../pb/dropzoneViewer.jsp" %>
                            <%
                                }
                            %>


                        </td>

                    </tr>


                </table>
                    </div>
            </div>
            <div class="mt-5 div_border attachement-div">
                <h5 class="table-title">
                    <%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENT_CALENDER_NOTIFICATION, loginDTO)%>
                </h5>
                <div class="table-responsive">
                <table class="table table-bordered table-striped text-nowrap">
                    <tr>
                        <th><%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENT_CALENDER_NOTIFICATION_NOTIFICATIONCAT, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENT_CALENDER_NOTIFICATION_OCCURRENCECAT, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENT_CALENDER_NOTIFICATION_OCCURRENCE, loginDTO)%>
                        </th>
                    </tr>
                    <%
                        EventCalenderNotificationDAO eventCalenderNotificationDAO = new EventCalenderNotificationDAO();
                        List<EventCalenderNotificationDTO> eventCalenderNotificationDTOs = eventCalenderNotificationDAO.getEventCalenderNotificationDTOListByEventCalendarID(event_calendarDTO.iD);

                        for (EventCalenderNotificationDTO eventCalenderNotificationDTO : eventCalenderNotificationDTOs) {
                    %>
                    <tr>
                        <td>
                            <%
                                value = eventCalenderNotificationDTO.notificationCat + "";
                            %>
                            <%
                                value = CatDAO.getName(Language, "notification", eventCalenderNotificationDTO.notificationCat);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>
                        <td>
                            <%
                                value = eventCalenderNotificationDTO.occurrenceCat + "";
                            %>
                            <%
                                value = CatDAO.getName(Language, "occurrence", eventCalenderNotificationDTO.occurrenceCat);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>
                        <td>
                            <%
                                value = eventCalenderNotificationDTO.occurrence + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>
                    </tr>
                    <%

                        }

                    %>
                </table>
                    </div>
            </div>
            <div class="mt-5 div_border attachement-div">
                <h5 class="table-title">
                    <%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENT_MEMBERS, loginDTO)%>
                </h5>
                <div class="table-responsive">
                <table class="table table-bordered table-striped text-nowrap">
                    <tr>
                        <th><%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.HM_EMAIL, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.HM_PHONE, loginDTO)%>
                        </th>
                    </tr>
                    <%
                        EventMembersDAO eventMembersDAO = new EventMembersDAO();
                        List<EventMembersDTO> eventMembersDTOs = eventMembersDAO.getEventMembersDTOListByEventCalendarID(event_calendarDTO.iD);

                        for (EventMembersDTO eventMembersDTO : eventMembersDTOs) {
                    %>
                    <tr>
                        <td>

                            <%
                                String name = Language.equalsIgnoreCase("english") ? eventMembersDTO.nameEn : eventMembersDTO.nameBn;
                                value = name + ", " + WorkflowController.getOrganogramName(eventMembersDTO.organogramId, Language) + ", " + WorkflowController.getUnitNameFromOrganogramId(eventMembersDTO.organogramId, Language);
                            %>

                            <%=Utils.getDigits(value, Language)%>

                        </td>

                        <td>
                            <%=eventMembersDTO.email%>
                        </td>

                        <td>
                            <%=Utils.getDigits(eventMembersDTO.phone, Language)%>
                        </td>
                    </tr>
                    <%

                        }

                    %>
                </table>
                    </div>
            </div>
        </div>
    </div>
</div>