<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="event_calendar.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    Event_calendarDTO event_calendarDTO;
    event_calendarDTO = (Event_calendarDTO) request.getAttribute("event_calendarDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (event_calendarDTO == null) {
        event_calendarDTO = new Event_calendarDTO();

    }
    System.out.println("event_calendarDTO = " + event_calendarDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.EVENT_CALENDAR_ADD_EVENT_CALENDAR_ADD_FORMNAME, loginDTO);
    String servletName = "Event_calendarServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID = -1;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;
    String Language = LM.getText(LC.EVENT_CALENDAR_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<style>
    @media (max-width: 991.98px) {
        .table-section input.form-control{
            width: auto!important;
        }
    }
    @media (min-width: 992px) {
        .table-section input.form-control{
           width: 100%!important;
       }
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <jsp:include page="../employee_assign/employeeSearchModal.jsp">
        <jsp:param name="isHierarchyNeeded" value="false"/>
    </jsp:include>
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Event_calendarServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD'
                                           id='iD_hidden_<%=i%>' value='<%=event_calendarDTO.iD%>'
                                           tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.EVENT_CALENDAR_ADD_NAMEEN, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='nameEn'
                                                   id='nameEn_text_<%=i%>'
                                                   value='<%=event_calendarDTO.nameEn%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.EVENT_CALENDAR_ADD_NAMEBN, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='nameBn'
                                                   id='nameBn_text_<%=i%>'
                                                   value='<%=event_calendarDTO.nameBn%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENTCAT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='eventCat'
                                                    id='eventCat_category_<%=i%>' tag='pb_html'>
                                                <%
                                                    Options = CatDAO.getOptions(Language, "event", event_calendarDTO.eventCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENTLOCATION, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='eventLocation'
                                                   id='eventLocation_text_<%=i%>'
                                                   value='<%=event_calendarDTO.eventLocation%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENTDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%value = "eventDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID"
                                                           value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                                <jsp:param name="MIN_DATE"
                                                           value="<%=datestr%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='eventDate'
                                                   id='eventDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(event_calendarDTO.eventDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENTSTARTTIME, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%value = "eventStartTime_js_" + i;%>
                                            <jsp:include page="/time/time.jsp">
                                                <jsp:param name="TIME_ID"
                                                           value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                                <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden'
                                                   value="<%=event_calendarDTO.eventStartTime%>"
                                                   name='eventStartTime' id='eventStartTime_time_<%=i%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENTENDTIME, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%value = "eventEndTime_js_" + i;%>
                                            <jsp:include page="/time/time.jsp">
                                                <jsp:param name="TIME_ID"
                                                           value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                                <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden'
                                                   value="<%=event_calendarDTO.eventEndTime%>"
                                                   name='eventEndTime' id='eventEndTime_time_<%=i%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENTDESCRIPTION, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                                            <textarea class='form-control' name='eventDescription'
                                                                      id='eventDescription_text_<%=i%>'
                                                                      tag='pb_html'><%=event_calendarDTO.eventDescription%></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.EVENT_CALENDAR_ADD_FILESDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                fileColumnName = "filesDropzone";
                                                if (actionName.equals("edit")) {
                                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(event_calendarDTO.filesDropzone);
                                            %>
                                            <%@include file="../pb/dropzoneEditor.jsp" %>
                                            <%
                                                } else {
                                                    ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                    event_calendarDTO.filesDropzone = ColumnID;
                                                }
                                            %>

                                            <div class="dropzone"
                                                 action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=event_calendarDTO.filesDropzone%>">
                                                <input type='file' style="display:none"
                                                       name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>'
                                                       tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value=''
                                                   tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>'
                                                   tag='pb_html'
                                                   value='<%=event_calendarDTO.filesDropzone%>'/>


                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='isRecurring'
                                           id='isRecurring_checkbox_<%=i%>' value='false'
                                           tag='pb_html'>
                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=event_calendarDTO.insertedByUserId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=event_calendarDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=event_calendarDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModifierUser'
                                           id='lastModifierUser_hidden_<%=i%>'
                                           value='<%=event_calendarDTO.lastModifierUser%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value='<%=event_calendarDTO.isDeleted%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=event_calendarDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5 table-section">
                    <h5 class="table-title">
                        <%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENT_CALENDER_NOTIFICATION, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENT_CALENDER_NOTIFICATION_NOTIFICATIONCAT, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENT_CALENDER_NOTIFICATION_OCCURRENCECAT, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENT_CALENDER_NOTIFICATION_OCCURRENCE, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENT_CALENDER_NOTIFICATION_REMOVE, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="field-EventCalenderNotification">


                            <%
                                if (actionName.equals("edit")) {
                                    int index = -1;


                                    for (EventCalenderNotificationDTO eventCalenderNotificationDTO : event_calendarDTO.eventCalenderNotificationDTOList) {
                                        index++;

                                        System.out.println("index index = " + index);

                            %>

                            <tr id="EventCalenderNotification_<%=index + 1%>">
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='eventCalenderNotification.iD'
                                           id='iD_hidden_<%=childTableStartingID%>'
                                           value='<%=eventCalenderNotificationDTO.iD%>' tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='eventCalenderNotification.eventCalendarId'
                                           id='eventCalendarId_hidden_<%=childTableStartingID%>'
                                           value='<%=eventCalenderNotificationDTO.eventCalendarId%>'
                                           tag='pb_html'/>
                                </td>
                                <td>


                                    <select class='form-control'
                                            name='eventCalenderNotification.notificationCat'
                                            id='notificationCat_category_<%=childTableStartingID%>'
                                            tag='pb_html'>
                                        <%
                                            Options = CatDAO.getOptions(Language, "notification", eventCalenderNotificationDTO.notificationCat);
                                        %>
                                        <%=Options%>
                                    </select>

                                </td>
                                <td>


                                    <select class='form-control'
                                            name='eventCalenderNotification.occurrenceCat'
                                            id='occurrenceCat_category_<%=childTableStartingID%>'
                                            tag='pb_html'>
                                        <%
                                            Options = CatDAO.getOptions(Language, "occurrence", eventCalenderNotificationDTO.occurrenceCat);
                                        %>
                                        <%=Options%>
                                    </select>

                                </td>
                                <td>


                                    <%
                                        value = "";
                                        if (eventCalenderNotificationDTO.occurrence != -1) {
                                            value = eventCalenderNotificationDTO.occurrence + "";
                                        }
                                    %>
                                    <input type='number' class='form-control'
                                           name='eventCalenderNotification.occurrence'
                                           id='occurrence_number_<%=childTableStartingID%>'
                                           value='<%=value%>' tag='pb_html'>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='eventCalenderNotification.insertedByUserId'
                                           id='insertedByUserId_hidden_<%=childTableStartingID%>'
                                           value='<%=eventCalenderNotificationDTO.insertedByUserId%>'
                                           tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='eventCalenderNotification.insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=childTableStartingID%>'
                                           value='<%=eventCalenderNotificationDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='eventCalenderNotification.insertionDate'
                                           id='insertionDate_hidden_<%=childTableStartingID%>'
                                           value='<%=eventCalenderNotificationDTO.insertionDate%>'
                                           tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='eventCalenderNotification.lastModifierUser'
                                           id='lastModifierUser_hidden_<%=childTableStartingID%>'
                                           value='<%=eventCalenderNotificationDTO.lastModifierUser%>'
                                           tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='eventCalenderNotification.isDeleted'
                                           id='isDeleted_hidden_<%=childTableStartingID%>'
                                           value='<%=eventCalenderNotificationDTO.isDeleted%>'
                                           tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='eventCalenderNotification.lastModificationTime'
                                           id='lastModificationTime_hidden_<%=childTableStartingID%>'
                                           value='<%=eventCalenderNotificationDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                </td>
                                <td>
                                    <div class='checker'><span id='chkEdit'><input type='checkbox'
                                                                                   id='eventCalenderNotification_cb_<%=index%>'
                                                                                   name='checkbox'
                                                                                   value=''/></span>
                                    </div>
                                </td>
                            </tr>
                            <%
                                        childTableStartingID++;
                                    }
                                }
                            %>

                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-12 text-right">
                            <button
                                    id="add-more-EventCalenderNotification"
                                    name="add-moreEventCalenderNotification"
                                    type="button"
                                    class="btn btn-sm text-white add-btn shadow">
                                <i class="fa fa-plus"></i>
                                <%=LM.getText(LC.HM_ADD, loginDTO)%>
                            </button>
                            <button
                                    id="remove-EventCalenderNotification"
                                    name="removeEventCalenderNotification"
                                    type="button"
                                    class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </div>
                    <%EventCalenderNotificationDTO eventCalenderNotificationDTO = new EventCalenderNotificationDTO();%>
                    <template id="template-EventCalenderNotification">
                        <tr>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='eventCalenderNotification.iD' id='iD_hidden_'
                                       value='<%=eventCalenderNotificationDTO.iD%>' tag='pb_html'/>

                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='eventCalenderNotification.eventCalendarId'
                                       id='eventCalendarId_hidden_'
                                       value='<%=eventCalenderNotificationDTO.eventCalendarId%>'
                                       tag='pb_html'/>
                            </td>
                            <td>


                                <select class='form-control'
                                        name='eventCalenderNotification.notificationCat'
                                        id='notificationCat_category_' tag='pb_html'>
                                    <%
                                        Options = CatDAO.getOptions(Language, "notification", eventCalenderNotificationDTO.notificationCat);
                                    %>
                                    <%=Options%>
                                </select>

                            </td>
                            <td>


                                <select class='form-control'
                                        name='eventCalenderNotification.occurrenceCat'
                                        id='occurrenceCat_category_' tag='pb_html'>
                                    <%
                                        Options = CatDAO.getOptions(Language, "occurrence", eventCalenderNotificationDTO.occurrenceCat);
                                    %>
                                    <%=Options%>
                                </select>

                            </td>
                            <td>


                                <%
                                    value = "";
                                    if (eventCalenderNotificationDTO.occurrence != -1) {
                                        value = eventCalenderNotificationDTO.occurrence + "";
                                    }
                                %>
                                <input type='number' class='form-control'
                                       name='eventCalenderNotification.occurrence'
                                       id='occurrence_number_' value='<%=value%>' tag='pb_html'>
                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='eventCalenderNotification.insertedByUserId'
                                       id='insertedByUserId_hidden_'
                                       value='<%=eventCalenderNotificationDTO.insertedByUserId%>'
                                       tag='pb_html'/>
                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='eventCalenderNotification.insertedByOrganogramId'
                                       id='insertedByOrganogramId_hidden_'
                                       value='<%=eventCalenderNotificationDTO.insertedByOrganogramId%>'
                                       tag='pb_html'/>
                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='eventCalenderNotification.insertionDate'
                                       id='insertionDate_hidden_'
                                       value='<%=eventCalenderNotificationDTO.insertionDate%>'
                                       tag='pb_html'/>
                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='eventCalenderNotification.lastModifierUser'
                                       id='lastModifierUser_hidden_'
                                       value='<%=eventCalenderNotificationDTO.lastModifierUser%>'
                                       tag='pb_html'/>
                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='eventCalenderNotification.isDeleted' id='isDeleted_hidden_'
                                       value='<%=eventCalenderNotificationDTO.isDeleted%>'
                                       tag='pb_html'/>

                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='eventCalenderNotification.lastModificationTime'
                                       id='lastModificationTime_hidden_'
                                       value='<%=eventCalenderNotificationDTO.lastModificationTime%>'
                                       tag='pb_html'/>
                            </td>
                            <td>
                                <div><span id='chkEdit'><input type='checkbox' name='checkbox'
                                                               value=''/></span></div>
                            </td>
                        </tr>

                    </template>
                </div>
                <div class="mt-5">
                    <h5 class="table-title">
                        <%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENT_MEMBERS, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap">
                            <thead>
                            <tr>
                                <th>
                                    <%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%>
                                </th>
                               
                                <th>
                                    <%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENT_MEMBERS_REMOVE, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="field-EventMembers">
                            <%
                                if (actionName.equals("edit")) {
                                    int index = -1;


                                    for (EventMembersDTO eventMembersDTO : event_calendarDTO.eventMembersDTOList) {
                                        index++;

                                        System.out.println("index index = " + index);

                            %>

                            <tr id="EventMembers_<%=index + 1%>">
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='eventMembers.iD'
                                           id='iD_hidden_<%=childTableStartingID%>'
                                           value='<%=eventMembersDTO.iD%>' tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='eventMembers.eventCalendarId'
                                           id='eventCalendarId_hidden_<%=childTableStartingID%>'
                                           value='<%=eventMembersDTO.eventCalendarId%>' tag='pb_html'/>
                                </td>
                                <td id="organogramId_table_<%=childTableStartingID%>" tag='pb_html'>
                                    <%
                                        if (eventMembersDTO.organogramId != -1) {
                                    %>
                                </td>
                            <tr>
                                <td>
                                    <%=WorkflowController.getUserNameFromOrganogramId(eventMembersDTO.organogramId)%>
                                </td>
                                <td>
                                    <%=WorkflowController.getNameFromOrganogramId(eventMembersDTO.organogramId, Language)%>
                                </td>
                                <td>
                                    <%=WorkflowController.getOrganogramName(eventMembersDTO.organogramId, Language)%>
                                    , <%=WorkflowController.getUnitNameFromOrganogramId(eventMembersDTO.organogramId, Language)%>
                                </td>
                                <td>

                                </td>
                            </tr>
                            <%
                                }
                            %>

                            </tr>
                            <%
                                        childTableStartingID++;
                                    }
                                }
                            %>

                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-12 text-right">
                            <button
                                    id="add-more-EventMembers"
                                    name="add-moreEventMembers"
                                    type="button"
                                    class="btn btn-sm text-white add-btn shadow">
                                <i class="fa fa-plus"></i>
                                <%=LM.getText(LC.HM_ADD, loginDTO)%>
                            </button>
                            <button
                                    id="remove-EventMembers"
                                    name="removeEventMembers"
                                    type="button"
                                    class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </div>
                    <%EventMembersDTO eventMembersDTO = new EventMembersDTO();%>
                    <template id="template-EventMembers">
                        <tr>
                            <td style="display: none;">


                                <input type='hidden' class='form-control' name='eventMembers.iD'
                                       id='iD_hidden_' value='<%=eventMembersDTO.iD%>' tag='pb_html'/>

                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='eventMembers.eventCalendarId' id='eventCalendarId_hidden_'
                                       value='<%=eventMembersDTO.eventCalendarId%>' tag='pb_html'/>
                            </td>
                            <td>


                                <table class="table table-bordered table-striped">
                                    <tbody id="organogramId_table_" tag='pb_html'>
                                    <%
                                        if (eventMembersDTO.organogramId != -1) {
                                    %>
                                    <tr>
                                        <td style="width:20%"><%=WorkflowController.getUserNameFromOrganogramId(eventMembersDTO.organogramId)%>
                                        </td>
                                        <td style="width:40%"><%=WorkflowController.getNameFromOrganogramId(eventMembersDTO.organogramId, Language)%>
                                        </td>
                                        <td><%=WorkflowController.getOrganogramName(eventMembersDTO.organogramId, Language)%>
                                            , <%=WorkflowController.getUnitNameFromOrganogramId(eventMembersDTO.organogramId, Language)%>
                                        </td>
                                    </tr>
                                    <%
                                        }
                                    %>
                                    </tbody>
                                </table>
                                <input type='hidden' class='form-control'
                                       name='eventMembers.organogramId' id='organogramId_hidden_'
                                       value='<%=eventMembersDTO.organogramId%>' tag='pb_html'/>

                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control' name='eventMembers.isDeleted'
                                       id='isDeleted_hidden_' value='<%=eventMembersDTO.isDeleted%>'
                                       tag='pb_html'/>

                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='eventMembers.lastModificationTime'
                                       id='lastModificationTime_hidden_'
                                       value='<%=eventMembersDTO.lastModificationTime%>' tag='pb_html'/>
                            </td>
                            <td>
                                <div><span id='chkEdit'><input type='checkbox' name='checkbox'
                                                               value=''/></span></div>
                            </td>
                        </tr>

                    </template>
                </div>
                <div class="text-center mt-4">
                    <button type="button" id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENT_CALENDAR_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.EVENT_CALENDAR_ADD_EVENT_CALENDAR_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {


        preprocessDateBeforeSubmitting('eventDate', row);
        preprocessTimeBeforeSubmitting('eventStartTime', row);
        preprocessTimeBeforeSubmitting('eventEndTime', row);
        preprocessCheckBoxBeforeSubmitting('isRecurring', row);

        for (i = 1; i < child_table_extra_id; i++) {
        }
        for (i = 1; i < child_table_extra_id; i++) {
        }
        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Event_calendarServlet");
    }

    function init(row) {

        setDateByStringAndId('eventDate_js_' + row, $('#eventDate_date_' + row).val());
        setTimeById('eventStartTime_js_' + row, $('#eventStartTime_time_' + row).val());
        setTimeById('eventEndTime_js_' + row, $('#eventEndTime_time_' + row).val());

        for (i = 1; i < child_table_extra_id; i++) {
        }
        for (i = 1; i < child_table_extra_id; i++) {
        }

    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-EventCalenderNotification").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-EventCalenderNotification");

            $("#field-EventCalenderNotification").append(t.html());
            SetCheckBoxValues("field-EventCalenderNotification");

            var tr = $("#field-EventCalenderNotification").find("tr:last-child");

            tr.attr("id", "EventCalenderNotification_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                console.log(index + ": " + $(this).attr('id'));
            });


            child_table_extra_id++;

        });


    $("#remove-EventCalenderNotification").click(function (e) {
        var tablename = 'field-EventCalenderNotification';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[type="checkbox"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
    });
    $("#add-more-EventMembers").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-EventMembers");

            $("#field-EventMembers").append(t.html());
            SetCheckBoxValues("field-EventMembers");

            var tr = $("#field-EventMembers").find("tr:last-child");

            tr.attr("id", "EventMembers_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                console.log(index + ": " + $(this).attr('id'));
            });

            addEmployeeWithRow("organogramId_button_" + child_table_extra_id);

            child_table_extra_id++;

        });


    $("#remove-EventMembers").click(function (e) {
        var tablename = 'field-EventMembers';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[type="checkbox"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
    });


</script>






