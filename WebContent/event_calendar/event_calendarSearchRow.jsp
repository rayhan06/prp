<%@page pageEncoding="UTF-8" %>

<%@page import="event_calendar.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="files.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EVENT_CALENDAR_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_EVENT_CALENDAR;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Event_calendarDTO event_calendarDTO = (Event_calendarDTO) request.getAttribute("event_calendarDTO");
    CommonDTO commonDTO = event_calendarDTO;
    String servletName = "Event_calendarServlet";


    System.out.println("event_calendarDTO = " + event_calendarDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Event_calendarDAO event_calendarDAO = (Event_calendarDAO) request.getAttribute("event_calendarDAO");

    FilesDAO filesDAO = new FilesDAO();

    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td id='<%=i%>_nameEn'>
    <%
        value = event_calendarDTO.nameEn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_nameBn'>
    <%
        value = event_calendarDTO.nameBn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_eventCat'>
    <%
        value = event_calendarDTO.eventCat + "";
    %>
    <%
        value = CatDAO.getName(Language, "event", event_calendarDTO.eventCat);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_eventLocation'>
    <%
        value = event_calendarDTO.eventLocation + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_eventDate'>
    <%
        value = event_calendarDTO.eventDate + "";
    %>
    <%
        String formatted_eventDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_eventDate, Language)%>


</td>

<td id='<%=i%>_eventStartTime'>
    <%
        value = event_calendarDTO.eventStartTime + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_eventEndTime'>
    <%
        value = event_calendarDTO.eventEndTime + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Event_calendarServlet?actionType=view&ID=<%=event_calendarDTO.iD%>'"
    >
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;" type="button"
            onclick="location.href='Event_calendarServlet?actionType=getEditPage&ID=<%=event_calendarDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=event_calendarDTO.iD%>'/></span>
    </div>
</td>
																						
											

