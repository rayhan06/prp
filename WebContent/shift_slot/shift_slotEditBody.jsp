<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="shift_slot.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
Shift_slotDTO shift_slotDTO = new Shift_slotDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	shift_slotDTO = Shift_slotDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = shift_slotDTO;
String tableName = "shift_slot";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.SHIFT_SLOT_ADD_SHIFT_SLOT_ADD_FORMNAME, loginDTO);
String servletName = "Shift_slotServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Shift_slotServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=shift_slotDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.SHIFT_SLOT_ADD_SHIFTCAT, loginDTO)%>															</label>
                                                            <div class="col-8">
																<select class='form-control'  name='shiftCat' id = 'shiftCat_category_<%=i%>'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("shift", Language, shift_slotDTO.shiftCat);
																%>
																<%=Options%>
																</select>
	
															</div>
                                                      </div>									
											
                                                      
                                                      <div class="form-group row">
				                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.DOCTOR_TIME_SLOT_ADD_STARTTIME, loginDTO)%>
				                                        </label>
				                                        <div class="col-md-8">
				                                            <%value = "startTime_js_" + i;%>
				                                            <jsp:include page="/time/time.jsp">
				                                                <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
				                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
				                                                <jsp:param name="IS_AMPM" value="true"></jsp:param>
				                                            </jsp:include>
				                                            <input type='hidden'
				                                                   value="<%=TimeFormat.getInAmPmFormat(shift_slotDTO.startTime)%>"
				                                                   name='startTime' id='startTime_time_<%=i%>' tag='pb_html'/>
				                                        </div>
				                                    </div>
				                                    <div class="form-group row">
				                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.DOCTOR_TIME_SLOT_ADD_ENDTIME, loginDTO)%>
				                                        </label>
				                                        <div class="col-md-8">
				                                            <%value = "endTime_js_" + i;%>
				                                            <jsp:include page="/time/time.jsp">
				                                                <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
				                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
				                                                <jsp:param name="IS_AMPM" value="true"></jsp:param>
				                                            </jsp:include>
				                                            <input type='hidden'
				                                                   value="<%=TimeFormat.getInAmPmFormat(shift_slotDTO.endTime)%>"
				                                                   name='endTime' id='endTime_time_<%=i%>' tag='pb_html'/>
				                                        </div>
				                                    </div>									
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.SHIFT_SLOT_ADD_SHIFT_SLOT_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.SHIFT_SLOT_ADD_SHIFT_SLOT_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);
	preprocessTimeBeforeSubmitting('startTime', row);
    preprocessTimeBeforeSubmitting('endTime', row);

	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Shift_slotServlet");	
}

function init(row)
{

	setTimeById('startTime_js_' + row, $('#startTime_time_' + row).val(), true);
    setTimeById('endTime_js_' + row, $('#endTime_time_' + row).val(), true); 

	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	


</script>






