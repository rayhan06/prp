<%@page import="login.LoginDTO" %>
<%@page import="am_house_allocation.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="am_house_allocation_committee.Am_house_allocation_committeeDTO" %>
<%@ page import="am_house_allocation_committee.Am_house_allocation_committeeRepository" %>
<%@ page import="am_house_allocation_committee_member.Am_house_allocation_committee_memberDTO" %>
<%@ page import="am_house_allocation_committee_member.Am_house_allocation_committee_memberRepository" %>
<%@ page import="java.util.List" %>
<%@ page import="am_house_allocation_request.Am_house_allocation_requestDTO" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="am_grade_house_mapping.Am_grade_house_mappingRepository" %>
<%@ page import="files.FilesDTO" %>
<%@ page import="dbm.DBMW" %>
<%@ page import="files.FilesDAO" %>
<%@ page import="am_house_type_sub_category.Am_house_type_sub_categoryRepository" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    Am_house_allocationDTO am_house_allocationDTO;
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    String actionName;

    Am_house_allocation_committeeDTO committeeDTO = Am_house_allocation_committeeRepository.
            getInstance().getActiveCommittee();
    if (committeeDTO == null) {
        committeeDTO = new Am_house_allocation_committeeDTO();
    }

    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "ajax_edit";
        am_house_allocationDTO = Am_house_allocationDAO.getInstance().getDTOByID(Long.parseLong(ID));
    } else {
        actionName = "ajax_add";
        am_house_allocationDTO = new Am_house_allocationDTO();
        am_house_allocationDTO.committeeId = committeeDTO.iD;
    }


    //


    String formTitle = LM.getText(LC.AM_HOUSE_ALLOCATION_ADD_AM_HOUSE_ALLOCATION_ADD_FORMNAME, loginDTO);
    String servletName = "Am_house_allocationServlet";
    int i = 0;
    String value = "";
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    Am_house_allocation_requestDTO am_house_allocation_requestDTO = (Am_house_allocation_requestDTO) request.getAttribute("am_house_allocation_requestDTO");
    String msg = Am_grade_house_mappingRepository.getInstance().getMsg(Employee_recordsRepository.getInstance().getGradeByEmployeeRecordId(am_house_allocation_requestDTO.requesterEmpId), Language);

    String fileColumnName = "";
    FilesDAO filesDAO = new FilesDAO();
    long ColumnID = -1;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=am_house_allocationDTO.iD%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='amHouseAllocationRequestId'
                                           id='amHouseAllocationRequestId_hidden_<%=i%>'
                                           value='<%=am_house_allocation_requestDTO.iD%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <span class="col-12 text-center font-weight-bold" style="color: red"><%=msg%>
                                        </span>
                                    </div>
                                    <%--														<input type='hidden' class='form-control'  name='houseId' id = 'houseId_hidden_<%=i%>' value='<%=am_house_allocationDTO.houseId%>' tag='pb_html'/>--%>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_ADD_AMHOUSEOLDNEWCAT, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <select onchange="loadHouseList()" class='form-control'
                                                    name='amHouseOldNewCat' id='amHouseOldNewCat_category_<%=i%>'
                                                    tag='pb_html'>
                                                <%
                                                    int defaultValue = am_house_allocation_requestDTO.amHouseOldNewCat;
                                                    if ("edit".equals(request.getParameter("actionType"))) {
                                                        defaultValue = am_house_allocationDTO.amHouseOldNewCat;
                                                    }
                                                    String Options = CatRepository.getInstance().buildOptions("am_house_old_new", Language, defaultValue);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_ADD_AMHOUSELOCATIONCAT, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <select onchange="loadHouseList()" class='form-control'
                                                    name='amHouseLocationCat' id='amHouseLocationCat_category_<%=i%>'
                                                    tag='pb_html'>
                                                <%
                                                    defaultValue = am_house_allocation_requestDTO.amHouseLocationCat;
                                                    if ("edit".equals(request.getParameter("actionType"))) {
                                                        defaultValue = am_house_allocationDTO.amHouseLocationCat;
                                                    }
                                                    Options = CatRepository.getInstance().buildOptions("am_house_location", Language, defaultValue);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_ADD_AMHOUSECLASSCAT, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <select onchange="loadHouseSubClass();loadHouseList()" class='form-control'
                                                    name='amHouseClassCat' id='amHouseClassCat_category_<%=i%>'
                                                    tag='pb_html'>
                                                <%
                                                    defaultValue = am_house_allocation_requestDTO.amHouseClassCat;
                                                    if ("edit".equals(request.getParameter("actionType"))) {
                                                        defaultValue = am_house_allocationDTO.amHouseClassCat;
                                                    }
                                                    Options = CatRepository.getInstance().buildOptions("am_house_class", Language, defaultValue);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=UtilCharacter.getDataByLanguage(Language, "বাসার উপশ্রেণী", "House Sub Class")%>
                                        </label>
                                        <div class="col-8">
                                            <select class='form-control'
                                                    name='amHouseSubClassCat' id='amHouseSubClassCat_category_<%=i%>'
                                                    tag='pb_html'>
                                                <%
                                                    defaultValue = am_house_allocation_requestDTO.amHouseClassCat;
                                                    if ("edit".equals(request.getParameter("actionType"))) {
                                                        defaultValue = am_house_allocationDTO.amHouseClassCat;
                                                    }
                                                    Options = Am_house_type_sub_categoryRepository.getInstance().buildOptionsByHouseClass(Language,
                                                            am_house_allocation_requestDTO.amHouseClassCat, defaultValue);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=getDataByLanguage(Language, "বাসা নম্বর", "House Number")%>
                                        </label>
                                        <div class="col-8">
                                            <select class='form-control' name='houseId' id='houseId' tag='pb_html'>
                                            </select>
                                            <input type="hidden" id="houseIdHidden" value="<%=am_house_allocation_requestDTO.amHouseId%>">

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_ADD_ALLOCATIONDATE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%value = "allocationDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='allocationDate' id='allocationDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(am_house_allocationDTO.allocationDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=UtilCharacter.getDataByLanguage(Language, "বাসা বরাদ্দের কোটা",
                                                "House Allocation Quota")%>
                                        </label>
                                        <div class="col-8">
                                            <select  class='form-control'
                                                    name='amHouseAllocationQuota' id='amHouseAllocationQuota_<%=i%>'
                                                    tag='pb_html'>
                                                <%
                                                    defaultValue = am_house_allocationDTO.amHouseAllocationQuota;
                                                    if ("edit".equals(request.getParameter("actionType"))) {
                                                        defaultValue = am_house_allocationDTO.amHouseAllocationQuota;
                                                    }
                                                    Options = CatRepository.getInstance().buildOptions("asset_house_allocation_quota", Language, defaultValue);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_FILESDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%
                                                fileColumnName = "filesDropzone";
                                                if (actionName.equals("edit")) {
                                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(am_house_allocationDTO.filesDropzone);
                                            %>
                                            <%@include file="../pb/dropzoneEditor.jsp" %>
                                            <%
                                                } else {
                                                    ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                    am_house_allocationDTO.filesDropzone = ColumnID;
                                                }
                                            %>

                                            <div class="dropzone"
                                                 action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=am_house_allocationDTO.filesDropzone%>">
                                                <input type='file' style="display:none" name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value='' tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=am_house_allocationDTO.filesDropzone%>'/>


                                        </div>
                                    </div>


                                    <%
                                        if (committeeDTO != null) {
                                            List<Am_house_allocation_committee_memberDTO> memberDTOList =
                                                    Am_house_allocation_committee_memberRepository.getInstance().
                                                            getAm_house_allocation_committee_memberDTOByCom_id(committeeDTO.iD);
                                    %>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=getDataByLanguage(Language, "বাসা বরাদ্দ কমিটি", "House Allocation Committee")%>
                                        </label>

                                        <div class="col-8">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped">
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <%=getDataByLanguage(Language, "সভাপতি", "Chairman")%>
                                                        </td>
                                                        <td>
                                                            <%=getDataByLanguage(Language, committeeDTO.chairmanNameBn,
                                                                    committeeDTO.chairmanNameEn)%>

                                                        </td>
                                                        <td>
                                                            <%=getDataByLanguage(Language, committeeDTO.chairmanOfficeUnitOrgNameBn,
                                                                    committeeDTO.chairmanOfficeUnitOrgNameEn)%>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <%=getDataByLanguage(Language, "সদস্য সচিব", "Member Secretary")%>
                                                        </td>
                                                        <td>
                                                            <%=getDataByLanguage(Language, committeeDTO.secretaryNameBn,
                                                                    committeeDTO.secretaryNameEn)%>

                                                        </td>
                                                        <td>
                                                            <%=getDataByLanguage(Language, committeeDTO.secretaryOfficeUnitOrgNameBn,
                                                                    committeeDTO.secretaryOfficeUnitOrgNameEn)%>
                                                        </td>
                                                    </tr>

                                                    <%
                                                        for (Am_house_allocation_committee_memberDTO memberDTO : memberDTOList) {

                                                    %>
                                                    <tr>
                                                        <td>
                                                            <%=getDataByLanguage(Language, "সদস্য", "Member")%>
                                                        </td>
                                                        <td>
                                                            <%=getDataByLanguage(Language, memberDTO.memberNameBn,
                                                                    memberDTO.memberNameEn)%>

                                                        </td>
                                                        <td>
                                                            <%=getDataByLanguage(Language, memberDTO.memberOfficeUnitOrgNameBn,
                                                                    memberDTO.memberOfficeUnitOrgNameEn)%>
                                                        </td>
                                                    </tr>

                                                    <%

                                                        }
                                                    %>

                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>


                                    </div>

                                    <%
                                        }
                                    %>


                                    <input type='hidden' class='form-control' name='requestId'
                                           id='requestId_hidden_<%=i%>' value='<%=am_house_allocationDTO.requestId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='committeeId'
                                           id='committeeId_hidden_<%=i%>'
                                           value='<%=am_house_allocationDTO.committeeId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=am_house_allocationDTO.insertedByUserId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=am_house_allocationDTO.insertedByOrganogramId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=am_house_allocationDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=am_house_allocationDTO.searchColumn%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>' value='<%=am_house_allocationDTO.isDeleted%>'
                                           tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=am_house_allocationDTO.lastModificationTime%>' tag='pb_html'/>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.AM_HOUSE_ALLOCATION_ADD_AM_HOUSE_ALLOCATION_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <%--                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"--%>
                            <%--                                    type="submit" onclick="submitForm()">--%>
                            <%--                                <%=LM.getText(LC.AM_HOUSE_ALLOCATION_ADD_AM_HOUSE_ALLOCATION_SUBMIT_BUTTON, loginDTO)%>--%>
                            <%--                            </button>--%>
                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="button" onclick="approveApplication()">
                                <%=LM.getText(LC.AM_HOUSE_ALLOCATION_ADD_AM_HOUSE_ALLOCATION_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    const houseForm = $("#bigform");

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function PreprocessBeforeSubmiting() {
        preprocessDateBeforeSubmitting('allocationDate', row);
        houseForm.validate();
        return houseForm.valid();
    }

    function approveApplication() {
        let msg = '<%=getDataByLanguage(Language, "আপনি কি আবেদন অনুমোদনের ব্যাপারে নিশ্চিত?", "Are you sure to approve application?")%>';
        let confirmButtonText = '<%=StringUtils.getYesNo(Language, true)%>';
        let cancelButtonText = '<%=StringUtils.getYesNo(Language, false)%>';
        messageDialog('', msg, 'success', true, confirmButtonText, cancelButtonText, () => {
            $('#submit-btn').prop("disabled", true);
            submitForm();
        }, () => {
        });
    }


    function submitForm() {
        buttonStateChange(true);
        if (PreprocessBeforeSubmiting()) {
            $.ajax({
                type: "POST",
                url: "Am_house_allocationServlet?actionType=<%=actionName%>",
                data: houseForm.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        window.location.replace(getContextPath()+"Am_house_allocationServlet?actionType=search");
                        //updateApprovalMappingTable();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        } else {
            buttonStateChange(false);
        }
    }

    function updateApprovalMappingTable() {

        const url = 'Am_house_allocation_approval_mappingServlet?actionType=approveApplication&amHouseAllocationRequestId=<%=am_house_allocation_requestDTO.iD%>';
        $.ajax({
            url: url,
            type: "POST",
            async: false,
            success: function (fetchedData) {

                console.log('fetchdata: ' + fetchedData)
                const response = JSON.parse(fetchedData);
                console.log('response: ' + response)
                if (response && response.responseCode === 0) {

                    showToastSticky(response.msg, response.msg);
                } else if (response && response.responseCode === 200) {
                    //submitForm();
                    window.location.replace(getContextPath() + response.msg);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
            }
        });
    }

    function convertToSelect2(){
        select2SingleSelector("#amHouseOldNewCat_category_<%=i%>", '<%=Language%>>');
        select2SingleSelector("#amHouseLocationCat_category_<%=i%>", '<%=Language%>>');
        select2SingleSelector("#amHouseClassCat_category_<%=i%>", '<%=Language%>>');
        select2SingleSelector("#amHouseSubClassCat_category_<%=i%>", '<%=Language%>>');
        select2SingleSelector("#houseId", '<%=Language%>>');
        select2SingleSelector("#amHouseAllocationQuota_<%=i%>", '<%=Language%>>');
    }

    function init(row) {
        setDateByStringAndId('allocationDate_js_' + row, $('#allocationDate_date_' + row).val());
        loadHouseList();
        convertToSelect2();
    }

    let row = 0;
    $(document).ready(function () {
        init(row);
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        $.validator.addMethod('validSelector', function (value, element) {
            return value && value !== -1 && value.toString().trim().length > 0;
        });

        let lang = '<%=Language%>';
        let divErr, locErr, classErr, houseNumberErr;
        if (lang.toUpperCase() === 'ENGLISH') {
            houseNumberErr = 'House Number Required';
            divErr = 'New House/ Old House Required';
            locErr = 'House Location Required';
            classErr = 'House Class Required';
        } else {
            houseNumberErr = 'বাসার নম্বর প্রয়োজনীয়';
            divErr = 'নতুন বাসা/পুরাতন বাসা প্রয়োজনীয়';
            locErr = 'বাসার অবস্থান প্রয়োজনীয়';
            classErr = 'বাসার শ্রেণী প্রয়োজনীয়';
        }

        houseForm.validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                houseId: {
                    validSelector: true,
                },
                amHouseOldNewCat: {
                    validSelector: true,
                },
                amHouseLocationCat: {
                    validSelector: true,
                },
                amHouseClassCat: {
                    validSelector: true,
                },
            },
            messages: {
                houseId: houseNumberErr,
                amHouseOldNewCat: divErr,
                amHouseLocationCat: locErr,
                amHouseClassCat: classErr,
            }
        });
    });

    function loadHouseList() {

        let newCat = document.getElementById('amHouseOldNewCat_category_0').value;
        let locCat = document.getElementById('amHouseLocationCat_category_0').value;
        let classCat = document.getElementById('amHouseClassCat_category_0').value;

        if (newCat === '' || locCat === '' || classCat === '') {
            $("#houseId").html("");
            return;
        }


        let defaultValue = '<%=am_house_allocationDTO.houseId%>';
        let url = "Am_houseServlet?actionType=getActiveHouse&newCat=" + newCat
            + "&locCat=" + locCat
            + "&classCat=" + classCat
            + "&defaultValue=" + defaultValue;
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                const response = JSON.parse(fetchedData);
                if (response && response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                } else if (response && response.responseCode === 200) {
                    $("#houseId").html(response.msg);
                    setHouseId();
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
            }
        });

    }

    function setHouseId(){
        $("#houseId").val(document.querySelector('#houseIdHidden').value);
    }

    function loadHouseSubClass(){
        let $house = $("#houseId");
        let $houseSubClass = $("#amHouseSubClassCat_category_<%=i%>")
        let houseClass = document.getElementById('amHouseClassCat_category_0').value;
        if (houseClass === '') {
            $house.html("");
            return;
        }

        let url = "Am_house_type_sub_categoryServlet?actionType=getHouseSubClass&houseClass=" + houseClass;
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                const response = JSON.parse(fetchedData);
                if (response && response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                } else if (response && response.responseCode === 200) {
                    $houseSubClass.html(response.msg);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
            }
        });
    }
</script>






