<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="am_house_allocation.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>
<%@ page import="am_house.Am_houseRepository" %>
<%@ page import="am_house.Am_houseDTO" %>
<%@ page import="am_house_allocation_committee.Am_house_allocation_committeeDTO" %>
<%@ page import="am_house_allocation_committee.Am_house_allocation_committeeRepository" %>
<%@ page import="am_house_allocation_committee_member.Am_house_allocation_committee_memberDTO" %>
<%@ page import="am_house_allocation_committee_member.Am_house_allocation_committee_memberRepository" %>
<%@ page import="common.BaseServlet" %>


<%
    String servletName = "Am_house_allocationServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Am_house_allocationDTO am_house_allocationDTO = (Am_house_allocationDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);

    CommonDTO commonDTO = am_house_allocationDTO;
%>
<%@include file="../pb/viewInitializer.jsp" %>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.AM_HOUSE_ALLOCATION_ADD_AM_HOUSE_ALLOCATION_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8  offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-md-8 offset-md-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.AM_HOUSE_ALLOCATION_ADD_AM_HOUSE_ALLOCATION_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_ADD_HOUSEID, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = "";
                                            Am_houseDTO houseDTO = Am_houseRepository.getInstance().getAm_houseDTOByID
                                                    (am_house_allocationDTO.houseId);
                                            if (houseDTO != null) {
//													value = Utils.getDigits(houseDTO.houseNumber, Language);													value = Utils.getDigits(houseDTO.houseNumber, Language);
                                                value = houseDTO.houseNumber;


                                            }
                                        %>

                                        <%=value%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_ADD_AMHOUSEOLDNEWCAT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = am_house_allocationDTO.amHouseOldNewCat + "";
                                        %>
                                        <%
                                            value = CatRepository.getInstance().getText(Language, "am_house_old_new", am_house_allocationDTO.amHouseOldNewCat);
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_ADD_AMHOUSELOCATIONCAT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = am_house_allocationDTO.amHouseLocationCat + "";
                                        %>
                                        <%
                                            value = CatRepository.getInstance().getText(Language, "am_house_location", am_house_allocationDTO.amHouseLocationCat);
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_ADD_AMHOUSECLASSCAT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = am_house_allocationDTO.amHouseClassCat + "";
                                        %>
                                        <%
                                            value = CatRepository.getInstance().getText(Language, "am_house_class", am_house_allocationDTO.amHouseClassCat);
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=UtilCharacter.getDataByLanguage(Language, "বাসার উপশ্রেণী", "Sub Class of House")%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = am_house_allocationDTO.amHouseClassCat + "";
                                        %>
                                        <%
                                            value = CatRepository.getInstance().getText(Language, "am_house_class", am_house_allocationDTO.amHouseClassCat);
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_ADD_ALLOCATIONDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = am_house_allocationDTO.allocationDate + "";
                                        %>
                                        <%
                                            String formatted_allocationDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                        %>
                                        <%=Utils.getDigits(formatted_allocationDate, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=UtilCharacter.getDataByLanguage(Language, "বাসা বরাদ্দের কোটা",
                                                "House Allocation Quota")%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = am_house_allocationDTO.amHouseAllocationQuota + "";
                                        %>
                                        <%
                                            value = CatRepository.getInstance().getText(Language, "asset_house_allocation_quota", am_house_allocationDTO.amHouseAllocationQuota);
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_NECESSARY_DOCUMENTS, loginDTO)%>
                                    </label>
                                    <div class="col-8">

                                        <%
                                            String fileColumnName = "filesDropzone";
                                            FilesDAO filesDAO1 = new FilesDAO();
                                            if (true) {
                                                List<FilesDTO> fileList = filesDAO1.getMiniDTOsByFileID(am_house_allocationDTO.filesDropzone);
                                        %>
                                        <table>

                                            <%
                                                if (fileList != null) {
                                                    for (int j = 0; j < fileList.size(); j++) {
                                                        FilesDTO filesDTO = fileList.get(j);
                                                        byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                            %>
                                            <tr>
                                                <td id='<%=fileColumnName%>_td_<%=filesDTO.iD%>'>
                                                    <a href='<%=servletName%>?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                       download><%=filesDTO.fileTitle%>
                                                    </a>
                                                    <%
                                                        if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                    %>
                                                    <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)
						%>' style='width:100px'/>
                                                    <%
                                                        }
                                                    %>


                                                </td>
                                            </tr>
                                            <%
                                                    }
                                                }
                                            %>

                                        </table>
                                        <%
                                            }
                                        %>
                                    </div>
                                </div>


                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=UtilCharacter.getDataByLanguage(Language, "বাসা বরাদ্দ কমিটি", "House Allocation Committee")%>
                                    </label>
                                    <div class="col-md-8">
                                        <%
                                            Am_house_allocation_committeeDTO committeeDTO =
                                                    Am_house_allocation_committeeRepository.getInstance().
                                                            getAm_house_allocation_committeeDTOByID
                                                                    (am_house_allocationDTO.committeeId);

                                            if (committeeDTO != null) {
                                                List<Am_house_allocation_committee_memberDTO> memberDTOList =
                                                        Am_house_allocation_committee_memberRepository.getInstance().
                                                                getAm_house_allocation_committee_memberDTOByCom_id(committeeDTO.iD);
                                        %>


                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped">
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <%=UtilCharacter.getDataByLanguage(Language, "সভাপতি", "Chairman")%>
                                                    </td>
                                                    <td>
                                                        <%=UtilCharacter.getDataByLanguage(Language, committeeDTO.chairmanNameBn,
                                                                committeeDTO.chairmanNameEn)%>

                                                    </td>
                                                    <td>
                                                        <%=UtilCharacter.getDataByLanguage(Language, committeeDTO.chairmanOfficeUnitOrgNameBn,
                                                                committeeDTO.chairmanOfficeUnitOrgNameEn)%>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <%=UtilCharacter.getDataByLanguage(Language, "সদস্য সচিব", "Member Secretary")%>
                                                    </td>
                                                    <td>
                                                        <%=UtilCharacter.getDataByLanguage(Language, committeeDTO.secretaryNameBn,
                                                                committeeDTO.secretaryNameEn)%>

                                                    </td>
                                                    <td>
                                                        <%=UtilCharacter.getDataByLanguage(Language, committeeDTO.secretaryOfficeUnitOrgNameBn,
                                                                committeeDTO.secretaryOfficeUnitOrgNameEn)%>
                                                    </td>
                                                </tr>

                                                <%
                                                    for (Am_house_allocation_committee_memberDTO memberDTO : memberDTOList) {

                                                %>
                                                <tr>
                                                    <td>
                                                        <%=UtilCharacter.getDataByLanguage(Language, "সদস্য", "Member")%>
                                                    </td>
                                                    <td>
                                                        <%=UtilCharacter.getDataByLanguage(Language, memberDTO.memberNameBn,
                                                                memberDTO.memberNameEn)%>

                                                    </td>
                                                    <td>
                                                        <%=UtilCharacter.getDataByLanguage(Language, memberDTO.memberOfficeUnitOrgNameBn,
                                                                memberDTO.memberOfficeUnitOrgNameEn)%>
                                                    </td>
                                                </tr>

                                                <%

                                                    }
                                                %>

                                                </tbody>
                                            </table>
                                        </div>

                                        <%

                                            }
                                        %>


                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>