<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="am_house_allocation.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="am_house.Am_houseRepository" %>
<%@ page import="am_house.Am_houseDAO" %>
<%@ page import="am_house.Am_houseDTO" %>
<%@ page import="am_house_allocation_request.AmHouseAllocationRequestStatus" %>
<%@ page import="am_house_type_sub_category.Am_house_type_sub_categoryDTO" %>
<%@ page import="am_house_type_sub_category.Am_house_type_sub_categoryRepository" %>
<%@page contentType="text/html;charset=utf-8" %>


<%@ include file="/am_house_allocation/am_house_receive_date_picker.jsp" %>

<%
    String navigator2 = "navAM_HOUSE_ALLOCATION";
//String servletName = "Am_house_allocationServlet";
    String servletName = "Am_house_allocation_requestServlet";
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

%>




<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=UtilCharacter.getDataByLanguage(Language, "আবেদনকারীর নাম", "Requester Name")%>
            </th>
            <th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_ADD_HOUSEID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_ADD_AMHOUSEOLDNEWCAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_ADD_AMHOUSELOCATIONCAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_ADD_AMHOUSECLASSCAT, loginDTO)%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "বাসার উপশ্রেণী", "House Sub Class")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "বাসা বরাদ্দের কোটা",
                    "House Allocation Quota")%>
            </th>
            <th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_ADD_ALLOCATIONDATE, loginDTO)%>
            </th>
            <th class="text-center"><%=UtilCharacter.getDataByLanguage(Language, "বাসা গ্রহণ/দখল",
                    "House Receive")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "আবেদনের অবস্থা",
                    "Application Status")%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<Am_house_allocationDTO>) rn2.list;

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Am_house_allocationDTO am_house_allocationDTO = (Am_house_allocationDTO) data.get(i);


        %>
        <tr>
            <td style="display: none"><input class="amHouseAllocationId" hidden id="amHouseAllocationId" value="<%=am_house_allocationDTO.iD%>"></td>
            <td>
                <%
                    value = UtilCharacter.getNameOfEmployee(am_house_allocationDTO.requesterNameEn,
                            am_house_allocationDTO.requesterNameBn,
                            am_house_allocationDTO.requesterOfficeUnitNameEn,
                            am_house_allocationDTO.requesterOfficeUnitNameBn,
                            am_house_allocationDTO.requesterOfficeUnitOrgNameEn,
                            am_house_allocationDTO.requesterOfficeUnitOrgNameBn,
                            Language);

                %>

                <%=value%>
            </td>


            <td>
                <%
                    value = "";
                    Am_houseDTO houseDTO = Am_houseRepository.getInstance().getAm_houseDTOByID
                            (am_house_allocationDTO.houseId);
                    if (houseDTO != null) {
//													value = Utils.getDigits(houseDTO.houseNumber, Language);													value = Utils.getDigits(houseDTO.houseNumber, Language);
                        value = houseDTO.houseNumber;


                    }

                %>

                <%=value%>


            </td>

            <td>
                <%
                    value = am_house_allocationDTO.amHouseOldNewCat + "";
                %>
                <%
                    value = CatRepository.getInstance().getText(Language, "am_house_old_new", am_house_allocationDTO.amHouseOldNewCat);
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>

            <td>
                <%
                    value = am_house_allocationDTO.amHouseLocationCat + "";
                %>
                <%
                    value = CatRepository.getInstance().getText(Language, "am_house_location", am_house_allocationDTO.amHouseLocationCat);
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>

            <td>
                <%
                    value = am_house_allocationDTO.amHouseClassCat + "";
                %>
                <%
                    value = CatRepository.getInstance().getText(Language, "am_house_class", am_house_allocationDTO.amHouseClassCat);
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>

            <td>
                <%
                    Am_house_type_sub_categoryDTO dto = Am_house_type_sub_categoryRepository.getInstance()
                            .getAm_house_type_sub_categoryDTOByiD(am_house_allocationDTO.amHouseClassSubCat);
                    value = dto == null ? "" :
                            Language.equalsIgnoreCase("English") ?
                                    dto.amHouseSubCatNameEn : dto.amHouseSubCatNameBn;
                %>
                <%=value%>
            </td>

            <td>
                <%
                    value = am_house_allocationDTO.amHouseAllocationQuota + "";
                %>
                <%
                    value = CatRepository.getInstance().getText(Language, "asset_house_allocation_quota", am_house_allocationDTO.amHouseAllocationQuota);
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>

            <td>
                <%
                    value = am_house_allocationDTO.allocationDate + "";
                %>
                <%
                    String formatted_allocationDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                %>
                <%=Utils.getDigits(formatted_allocationDate, Language)%>


            </td>

            <td>

                <%
                    String jspParam = "house_receive_date_id_"+i;
                %>

                <div class="d-flex">
                    <div class="" style="width: 30rem">
                        <jsp:include page="/date/date.jsp">
                            <jsp:param name="DATE_ID" value="<%=jspParam%>"></jsp:param>
                            <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                        </jsp:include>
                        <input type="hidden" class="receiveDateCls" id='house_receive_date_<%=i%>' name='house_receive_date_<%=i%>' value="<%=am_house_allocationDTO.receiveDate%>">

                    </div>
                    <div class="ml-4">
                        <button
                                type="button"
                                class="btn-sm shadow text-white border-0 submit-btn ml-2 submit-date-btn"
                                style="color: #ff6b6b;"
                                onclick="saveReceiveDate(this,'<%=jspParam%>')"
                        >
                            <%=LM.getText(LC.ALLOWANCE_CONFIGURE_ADD_ALLOWANCE_CONFIGURE_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                    <div class="ml-4">
                        <button
                                type="button"
                                class="btn-sm shadow text-white border-0 btn btn-danger ml-2 edit-date-btn"
                                style="border-radius: 6px !important;"
                                onclick="editBtnClicked(this)"
                        >
                            <%=UtilCharacter.getDataByLanguage(Language,"সম্পাদনা","Edit")%>
                        </button>
                    </div>
                </div>

            </td>

            <td>


                 <span class="btn btn-sm border-0 shadow"
                       style="background-color: <%=AmHouseAllocationRequestStatus.getColor(am_house_allocationDTO.status)%>; color: white; border-radius: 8px;cursor: text">
                        <%=CatRepository.getInstance().getText(
                                Language, "am_house_allocation_status", am_house_allocationDTO.status
                        )%>
                </span>

            </td>

            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=view&ID=<%=am_house_allocationDTO.requestId%>'"
                >
                    <i class="fa fa-eye"></i>
                </button>
            </td>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>

<input type='hidden' class='form-control datepicker-language' name='day' id='day' value=''/>
<input type='hidden' class='form-control datepicker-language' name='month' id='month' value=''/>
<input type='hidden' class='form-control datepicker-language' name='year' id='year' value=''/>

<script>

	let language = '<%=Language%>';
	let isLanEng = (language === 'English' || language === 'english');

	function convertDigitToBangla(englishText) {
		if (isLanEng) return englishText;
		let banlgaText = "";
		for (let i = 0; i < englishText.length; i++) {
			if (englishText[i] === '0') banlgaText += '০';
			else if (englishText[i] === '1') banlgaText += '১';
			else if (englishText[i] === '2') banlgaText += '২';
			else if (englishText[i] === '3') banlgaText += '৩';
			else if (englishText[i] === '4') banlgaText += '৪';
			else if (englishText[i] === '5') banlgaText += '৫';
			else if (englishText[i] === '6') banlgaText += '৬';
			else if (englishText[i] === '7') banlgaText += '৭';
			else if (englishText[i] === '8') banlgaText += '৮';
			else if (englishText[i] === '9') banlgaText += '৯';
			else banlgaText += englishText[i];
		}
		return banlgaText;
	}


    $(document).ready(function () {
        setDate();
        disableDateAndSubmitBtn();
    });

	function editBtnClicked(selectedBtn){
	    let parent = selectedBtn.parentNode.parentNode;
	    let submitBtn = parent.querySelector('.submit-date-btn');
        $(submitBtn).prop('disabled', (i, v) => !v);

        let daySelector = parent.querySelectorAll('select[name^="daySelectionhouse_receive_date_"]');
        let monthSelector = parent.querySelectorAll('select[name^="monthSelectionhouse_receive_date_"]');
        let yearSelector = parent.querySelectorAll('select[name^="yearSelectionhouse_receive_date_"]');

        daySelector.forEach(e => $(e).prop('disabled', (i, v) => !v));
        monthSelector.forEach(e => $(e).prop('disabled', (i, v) => !v));
        yearSelector.forEach(e => $(e).prop('disabled', (i, v) => !v));
    }

	function disableDateAndSubmitBtn(){

        let daySelector = document.querySelectorAll('select[name^="daySelectionhouse_receive_date_"]');
        let monthSelector = document.querySelectorAll('select[name^="monthSelectionhouse_receive_date_"]');
        let yearSelector = document.querySelectorAll('select[name^="yearSelectionhouse_receive_date_"]');
        let submitDateButtons = document.querySelectorAll('.submit-date-btn');
        daySelector.forEach(e => $(e).prop('disabled', true));
        monthSelector.forEach(e => $(e).prop('disabled', true));
        yearSelector.forEach(e => $(e).prop('disabled', true));
        submitDateButtons.forEach(e => $(e).prop('disabled', true));
    }

    function setDate(){
        let dateIds = document.querySelectorAll('.receiveDateCls');
        dateIds.forEach((item) => {
            if(item.value !== '' && item.value !== '0'){
                let splittedIdsIndex = item.id.split('_');
                let datePickerId =  "house_receive_date_id_"+splittedIdsIndex[splittedIdsIndex.length-1];
                setDateByTimestampAndId(datePickerId, item.value);
            }

        });

    }


    function saveReceiveDate(selectedElement,datePickerId) {
        let date = getDateStringById(datePickerId, 'DD/MM/YYYY');
        let id = selectedElement.parentNode.parentNode.parentNode.parentNode.querySelector('.amHouseAllocationId').value;
        if(date !== null && date !== undefined && date !== ''){
            $.ajax({
                type : "GET",
                url : "Am_house_allocationServlet?actionType=updateHouseReceiveDate",
                data:{"houseReceiveDateUnixTime":date,"id":id},
                dataType : 'JSON',
                success : function(response) {
                    if(response.responseCode === 0){
                        $('#toast_message').css('background-color','#ff6063');
                        showToastSticky(response.msg,response.msg);
                        //buttonStateChange(false);
                    }else if(response.responseCode === 200){
                        showToastSticky("তারিখ সেভ করা হয়েছে", "Date saved");
                        setTimeout(() => {
                            window.location.replace(getContextPath() + response.msg);
                        }, 1500);
                    }
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                }
            });
        }
        else{
            showToastSticky("অবৈধ তারিখ","Date is not valid");
        }

    }



</script>