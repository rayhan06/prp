<%@ page import="java.util.ArrayList" %>
<%@ page import="static permission.MenuConstants.ASSET_MANAGEMENT" %>
<%@ page import="static permission.MenuConstants.AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_SEARCH" %>
<%@ page import="static permission.MenuConstants.*" %>
<%@ page import="java.util.Arrays" %>
<%

	request.setAttribute("menuIDPath", new ArrayList<>(Arrays.asList(ASSET_MANAGEMENT,
			AM_HOUSE_ALLOCATION,
			AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_SEARCH)));
%>

<jsp:include page="../common/layout.jsp" flush="true">
<jsp:param name="title" value="Edit User" /> 
	<jsp:param name="body" value="../am_house_allocation/am_house_allocationEditBody.jsp" />
</jsp:include> 