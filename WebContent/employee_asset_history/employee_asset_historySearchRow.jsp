<%@page pageEncoding="UTF-8" %>

<%@page import="employee_asset_history.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.EMPLOYEE_ASSET_HISTORY_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_EMPLOYEE_ASSET_HISTORY;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Employee_asset_historyDTO employee_asset_historyDTO = (Employee_asset_historyDTO)request.getAttribute("employee_asset_historyDTO");
CommonDTO commonDTO = employee_asset_historyDTO;
String servletName = "Employee_asset_historyServlet";


System.out.println("employee_asset_historyDTO = " + employee_asset_historyDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Employee_asset_historyDAO employee_asset_historyDAO = (Employee_asset_historyDAO)request.getAttribute("employee_asset_historyDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
		
											<td id = '<%=i%>_assetListId'>
											<%
											value = employee_asset_historyDTO.assetListId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_employeeRecordsId'>
											<%
											value = employee_asset_historyDTO.employeeRecordsId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_remarks'>
											<%
											value = employee_asset_historyDTO.remarks + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_assignOrRevoke'>
											<%
											value = employee_asset_historyDTO.assignOrRevoke + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
											<td id = '<%=i%>_insertedBy'>
											<%
											value = employee_asset_historyDTO.insertedBy + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_modifiedBy'>
											<%
											value = employee_asset_historyDTO.modifiedBy + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
	

											<td>
												<a href='Employee_asset_historyServlet?actionType=view&ID=<%=employee_asset_historyDTO.iD%>'><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></a>
										
											</td>
	
											<td id = '<%=i%>_Edit'>																																	
	
												<a href='Employee_asset_historyServlet?actionType=getEditPage&ID=<%=employee_asset_historyDTO.iD%>'><%=LM.getText(LC.EMPLOYEE_ASSET_HISTORY_SEARCH_EMPLOYEE_ASSET_HISTORY_EDIT_BUTTON, loginDTO)%></a>
																				
											</td>											
											
											
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=employee_asset_historyDTO.iD%>'/></span>
												</div>
											</td>
																						
											

