const duplicate_servlet_info = {
    servlet: 'OfficeDuplicationServlet?',
    actionType: 'checkDuplication',
    table: 'office_origin_unit_organograms',
    parent: null,
    parent_id: null
};

const property = {
    'id': {},
    'officeOriginUnitId': {
        'required': true,
    },
    'superiorUnitId': {},
    'superiorDesignationId': {},
    'designationEng': {
        'required': true,
        'max_length': 255,
        'min_length': 2
    },
    'designationBng': {
        'required': true,
        'max_length': 255,
        'min_length': 2
    },
    'designationLevel': {
        'required': true,
        'is_integer': true,
        'max_value': 100000000,
        'min_value': 0
    },
    'designationSequence': {
        'required': true,
        'is_integer': true,
        'max_value': 100000000,
        'min_value': 0
    }
};

class OfficeOriginUnitOrganogram {

    static add(post_url, request_method, form_data, labelName) {

        const model = {};
        decodeURI(form_data).split("&").forEach(function (x) {
            model[x.split("=")[0]] = x.split("=")[1];
        });

        const keys = Object.keys(property);
        for (let i = 0; i < keys.length; i++) {
            const key = keys[i];
            const value = property[key];

            const innerKeys = Object.keys(value);
            for (let j = 0; j < innerKeys.length; j++) {
                const method_name = innerKeys[j];
                const method_parm = value[method_name];

                if (!DataValidation[method_name](method_parm, model, key, labelName[i - 1])) {
                    console.log('Validation failed: ', method_name, keys, labelName[i - 1]);
                    return false;
                }
            }

            console.log(key, model[key], property[key]);
        }

        return true;
    }
}

