<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="office_origin_unit_organograms.Office_origin_unit_organogramsDTO" %>
<%@page import="pb.CommonDAO" %>
<%@ page import="sessionmanager.SessionConstants" %>

<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.ArrayList" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_LANGUAGE, loginDTO);
%>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_OFFICEORIGINUNITID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_SUPERIORUNITID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_SUPERIORDESIGNATIONID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_DESIGNATIONENG, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_DESIGNATIONBNG, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_DESIGNATIONLEVEL, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_DESIGNATIONSEQUENCE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_STATUS, loginDTO)%>
            </th>
            <th><%out.print(LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_SEARCH_OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_BUTTON, loginDTO));%></th>
            <th><input type="submit" class="btn btn-xs btn-danger" value="
								<%out.print(LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_SEARCH_OFFICE_ORIGIN_UNIT_ORGANOGRAMS_DELETE_BUTTON, loginDTO));%>"/>
            </th>
        </tr>

        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_OFFICE_ORIGIN_UNIT_ORGANOGRAMS);

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Office_origin_unit_organogramsDTO row = (Office_origin_unit_organogramsDTO) data.get(i);
                        String deletedStyle = "color:red";
                        if (!row.isDeleted) deletedStyle = "";
                        out.println("<tr id = 'tr_" + i + "'>");

                        out.println("<td id = '" + i + "_officeOriginUnitId'>");
                        value = row.officeOriginUnitId + "";
                        value = CommonDAO.getName(Integer.parseInt(value), "office_origin_unit_organograms", Language.equals("English") ? "name_en" : "name_bn", "id");
                        out.println(value);
                        out.println("</td>");

                        out.println("<td id = '" + i + "_superiorUnitId'>");
                        value = row.superiorUnitId + "";
                        value = CommonDAO.getName(Integer.parseInt(value), "office_origin_units", Language.equals("English") ? "name_en" : "name_bn", "id");
                        out.println(value);
                        out.println("</td>");


                        out.println("<td id = '" + i + "_superiorDesignationId'>");
                        value = row.superiorDesignationId + "";
                        value = CommonDAO.getName(Integer.parseInt(value), "office_origin_units", Language.equals("English") ? "name_en" : "name_bn", "id");
                        out.println(value);
                        out.println("</td>");


                        out.println("<td id = '" + i + "_designationEng'>");
                        value = row.designationEng + "";
                        out.println(value);
                        out.println("</td>");


                        out.println("<td id = '" + i + "_designationBng'>");
                        value = row.designationBng + "";
                        out.println(value);
                        out.println("</td>");


                        out.println("<td id = '" + i + "_designationLevel'>");
                        value = row.designationLevel + "";
                        out.println(value);
                        out.println("</td>");


                        out.println("<td id = '" + i + "_designationSequence'>");
                        value = row.designationSequence + "";
                        out.println(value);
                        out.println("</td>");


                        out.println("<td id = '" + i + "_status'>");
                        value = row.status ? LM.getText(LC.GLOBAL_STATUS_ACTIVE, loginDTO) : LM.getText(LC.GLOBAL_STATUS_INACTIVE, loginDTO);
                        out.println(value);
                        out.println("</td>");


                        String onclickFunc = "\"fixedToEditable(" + i + ",'" + deletedStyle + "', '" + row.id + "' )\"";
                        out.println("<td id = '" + i + "_Edit'>");
                        out.println("<a onclick=" + onclickFunc + ">" + LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_SEARCH_OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_BUTTON, loginDTO) + "</a>");
                        out.println("</td>");


                        out.println("<td>");
                        out.println("<div >");
                        out.println("<span id='chkEdit' ><input type='checkbox' name='ID' value='" + row.id + "'/></span>");
                        out.println("</div");
                        out.println("</td>");
                        out.println("</tr>");
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>

        </tbody>
    </table>
</div>

<%
    String navigator2 = SessionConstants.NAV_OFFICE_ORIGIN_UNIT_ORGANOGRAMS;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
%>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>


<script src="nicEdit.js" type="text/javascript"></script>

<script type="text/javascript">

    function getOfficer(officer_id, officer_select) {
        console.log("getting officer");
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText.includes('option')) {
                    console.log("got response for officer");
                    document.getElementById(officer_select).innerHTML = this.responseText;

                    if (document.getElementById(officer_select).length > 1) {
                        document.getElementById(officer_select).removeAttribute("disabled");
                    }
                }
                else {
                    console.log("got errror response for officer");
                }

            }
            else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("POST", "Office_origin_unit_organogramsServlet?actionType=getGRSOffice&officer_id=" + officer_id, true);
        xhttp.send();
    }

    function getLayer(layernum, layerID, childLayerID, selectedValue) {
        console.log("getting layer");
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText.includes('option')) {
                    console.log("got response");
                    document.getElementById(childLayerID).innerHTML = this.responseText;
                }
                else {
                    console.log("got errror response");
                }

            }
            else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("POST", "Office_origin_unit_organogramsServlet?actionType=getGRSLayer&layernum=" + layernum + "&layerID="
            + layerID + "&childLayerID=" + childLayerID + "&selectedValue=" + selectedValue, true);
        xhttp.send();
    }

    function layerselected(layernum, layerID, childLayerID, hiddenInput, hiddenInputForTopLayer, officerElement) {
        var layervalue = document.getElementById(layerID).value;
        console.log("layervalue = " + layervalue);
        document.getElementById(hiddenInput).value = layervalue;
        if (layernum == 0) {
            document.getElementById(hiddenInputForTopLayer).value = layervalue;
        }
        if (layernum == 0 || (layernum == 1 && document.getElementById(hiddenInputForTopLayer).value == 3)) {
            document.getElementById(childLayerID).setAttribute("style", "display: inline;");
            getLayer(layernum, layerID, childLayerID, layervalue);
        }

        if (officerElement !== null) {
            getOfficer(layervalue, officerElement);
        }

    }

    function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        }
        else {
            var empty_fields = "";
            var i = 0;
            if (!document.getElementById('designationEng_text_' + row).checkValidity()) {
                empty_fields += "'designationEng'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('designationBng_text_' + row).checkValidity()) {
                empty_fields += "'designationBng'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }

        if (document.getElementById('status_checkbox_' + row).checked) {
            document.getElementById('status_checkbox_' + row).value = "true";
        }
        else {
            document.getElementById('status_checkbox_' + row).value = "false";
        }
        return true;
    }

    function PostprocessAfterSubmiting(row) {
    }

    function addHTML(id, HTML) {
        document.getElementById(id).innerHTML += HTML;
    }

    function getRequests() {
        var s1 = location.search.substring(1, location.search.length).split('&'),
            r = {}, s2, i;
        for (i = 0; i < s1.length; i += 1) {
            s2 = s1[i].split('=');
            r[decodeURIComponent(s2[0]).toLowerCase()] = decodeURIComponent(s2[1]);
        }
        return r;
    }

    function Request(name) {
        return getRequests()[name.toLowerCase()];
    }

    function ShowExcelParsingResult(suffix) {
        var failureMessage = document.getElementById("failureMessage_" + suffix);
        if (failureMessage == null) {
            console.log("failureMessage_" + suffix + " not found");
        }
        console.log("value = " + failureMessage.value);
        if (failureMessage != null && failureMessage.value != "") {
            alert("Excel uploading result:" + failureMessage.value);
        }
    }

    function init(row) {
    }

    function doEdit(params, i, id, deletedStyle) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText != '') {
                    var onclickFunc = "submitAjax(" + i + ",'" + deletedStyle + "')";
                    document.getElementById('tr_' + i).innerHTML = this.responseText;
                    document.getElementById('tr_' + i).innerHTML += "<td id = '" + i + "_Submit'></td>";
                    document.getElementById(i + '_Submit').innerHTML += "<a onclick=\"" + onclickFunc + "\">Submit</a>";
                    document.getElementById('tr_' + i).innerHTML += "<td>"
                        + "<div >"
                        + "<label class='kt-checkbox kt-checkbox--brand' id='chkEdit'><input type='checkbox' class='checkbox' name='ID' value='" + id + "'/><span></span></label>"
                        + "</td>";
                    init(i);
                    select_2_call();
                }
                else {
                    document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
                }
            }
            else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "Office_origin_unit_organogramsServlet?actionType=getEditPage" + params, true);
        xhttp.send();
    }

    function submitAjax(i, deletedStyle) {
        console.log('submitAjax called');
        var isSubmittable = PreprocessBeforeSubmiting(i, "inplaceedit");
        if (isSubmittable == false) {
            return;
        }
        var formData = new FormData();
        var value;
        value = document.getElementById('id_hidden_' + i).value;
        console.log('submitAjax i = ' + i + ' id = ' + value);
        formData.append('id', value);
        formData.append("identity", value);
        formData.append("ID", value);
        formData.append('officeOriginUnitId', document.getElementById('officeOriginUnitId_select_' + i).value);
        formData.append('superiorUnitId', document.getElementById('superiorUnitId_select_' + i).value);
        formData.append('superiorDesignationId', document.getElementById('superiorDesignationId_select_' + i).value);
        formData.append('designationEng', document.getElementById('designationEng_text_' + i).value);
        formData.append('designationBng', document.getElementById('designationBng_text_' + i).value);
        formData.append('shortNameEng', document.getElementById('shortNameEng_hidden_' + i).value);
        formData.append('shortNameBng', document.getElementById('shortNameBng_hidden_' + i).value);
        formData.append('designationLevel', document.getElementById('designationLevel_text_' + i).value);
        formData.append('designationSequence', document.getElementById('designationSequence_text_' + i).value);
        formData.append('status', document.getElementById('status_checkbox_' + i).value);
        formData.append('createdBy', document.getElementById('createdBy_hidden_' + i).value);
        formData.append('modifiedBy', document.getElementById('modifiedBy_hidden_' + i).value);
        formData.append('created', document.getElementById('created_hidden_' + i).value);
        formData.append('modified', document.getElementById('modified_hidden_' + i).value);
        formData.append('isDeleted', document.getElementById('isDeleted_hidden_' + i).value);
        formData.append('lastModificationTime', document.getElementById('lastModificationTime_hidden_' + i).value);

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText != '') {
                    document.getElementById('tr_' + i).innerHTML = this.responseText;
                    ShowExcelParsingResult(i);
                }
                else {
                    console.log("No Response");
                    document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
                }
            }
            else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("POST", 'Office_origin_unit_organogramsServlet?actionType=edit&inplacesubmit=true&deletedStyle=' + deletedStyle + '&rownum=' + i, true);
        xhttp.send(formData);
    }

    function fixedToEditable(i, deletedStyle, id) {
        console.log('fixedToEditable called');
        var params = '&identity=' + id + '&inplaceedit=true' + '&deletedStyle=' + deletedStyle + '&ID=' + id + '&rownum=' + i
            + '&dummy=dummy';
        console.log('fixedToEditable i = ' + i + ' id = ' + id);
        doEdit(params, i, id, deletedStyle);

    }

    window.onload = function () {
        ShowExcelParsingResult('general');
    }
</script>
			