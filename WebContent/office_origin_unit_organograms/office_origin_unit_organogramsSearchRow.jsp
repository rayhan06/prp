<%@page pageEncoding="UTF-8" %>

<%@page import="office_origin_unit_organograms.Office_origin_unit_organogramsDTO"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_LANGUAGE, loginDTO);

Office_origin_unit_organogramsDTO row = (Office_origin_unit_organogramsDTO)request.getAttribute("office_origin_unit_organogramsDTO");
if(row == null)
{
	row = new Office_origin_unit_organogramsDTO();
	
}
System.out.println("row = " + row);


int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";
}
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value='" + failureMessage + "'/></td>");

String value = "";


											
		
											
											out.println("<td id = '" + i + "_officeOriginUnitId'>");
											value = row.officeOriginUnitId + "";
											
											value = CommonDAO.getName(Integer.parseInt(value), "office_origin_unit_organograms", Language.equals("English")?"name_en":"name_bn", "id");
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_superiorUnitId'>");
											value = row.superiorUnitId + "";
											
											value = CommonDAO.getName(Integer.parseInt(value), "office_origin_units", Language.equals("English")?"name_en":"name_bn", "id");
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_superiorDesignationId'>");
											value = row.superiorDesignationId + "";
											
											value = CommonDAO.getName(Integer.parseInt(value), "office_origin_units", Language.equals("English")?"name_en":"name_bn", "id");
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_designationEng'>");
											value = row.designationEng + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_designationBng'>");
											value = row.designationBng + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
		
											
		
											
											out.println("<td id = '" + i + "_designationLevel'>");
											value = row.designationLevel + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_designationSequence'>");
											value = row.designationSequence + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_status'>");
											value = row.status ? LM.getText(LC.GLOBAL_STATUS_ACTIVE, loginDTO) : LM.getText(LC.GLOBAL_STATUS_INACTIVE, loginDTO);
														
											out.println(value);
				
			
											out.println("</td>");
		
											
		
											
		
											
		
											
		
											
		
											
		
	

	

											
											String onclickFunc = "\"fixedToEditable(" + i + ",'" + deletedStyle + "', '" + row.id + "' )\"";										
	
											out.println("<td id = '" + i + "_Edit'>");										
											out.println("<a onclick=" + onclickFunc + ">" + LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_SEARCH_OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_BUTTON, loginDTO) + "</a>");
										
											out.println("</td>");
											
											
											
											out.println("<td>");
											out.println("<div >");
											out.println("<span id='chkEdit' ><input type='checkbox' name='ID' value='" + row.id + "'/></span>");
											out.println("</div");
											out.println("</td>");%>

