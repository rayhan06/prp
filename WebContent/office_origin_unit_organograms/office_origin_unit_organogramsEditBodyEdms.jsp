<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="office_origin_unit_organograms.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="OfficeUnitsEdms.OfficeUnitsEdmsRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%
    Office_origin_unit_organogramsDTO office_origin_unit_organogramsDTO;
    office_origin_unit_organogramsDTO = (Office_origin_unit_organogramsDTO) request.getAttribute("office_origin_unit_organogramsDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (office_origin_unit_organogramsDTO == null) {
        office_origin_unit_organogramsDTO = new Office_origin_unit_organogramsDTO();
    }
    System.out.println("office_origin_unit_organogramsDTO = " + office_origin_unit_organogramsDTO);
    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("addOriginUnitOrganograms")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String context = "../../.." + request.getContextPath() + "/";
    String Language = LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String selectLang = isLanguageEnglish ? "Select":"বাছাই করুন";
    
%>

<input type="hidden" id="actionName" value="<%=actionName%>"/>

<div class="kt-form" id="bigform" name="bigform" enctype="multipart/form-data">

    <div id="office_layer_div_1" class="form-group ">
        <label> <%=isLanguageEnglish ? "Office":"দপ্তর"%><span class="required"> * </span></label>
        <div>
            <select class='form-control formRequired' required data-label="Office" required="required"
                    name='office_layer_1' id = 'office_layer_1'   tag='pb_html' onchange="layerChange(1)">
                <%=Office_unitsRepository.getInstance().buildOptionsByParentId(Language,0L)%>
            </select>
        </div>
    </div>

    <div id="office_layer_div_2" class="form-group" style="display: none">
        <label> <%=isLanguageEnglish ? "Office":"দপ্তর"%></label>
        <div>
            <select class='form-control formRequired' name='office_layer_2' id = 'office_layer_2'  tag='pb_html' onchange="layerChange(2)"></select>
        </div>
    </div>

    <div id="office_layer_div_3" class="form-group" style="display: none">
        <label> <%=isLanguageEnglish ? "Office":"দপ্তর"%></label>
        <div>
            <select class='form-control formRequired' name='office_layer_3' id = 'office_layer_3'  tag='pb_html' onchange="layerChange(3)"></select>
        </div>
    </div>

    <div id="office_layer_div_4" class="form-group" style="display: none">
        <label> <%=isLanguageEnglish ? "Office":"দপ্তর"%></label>
        <div>
            <select class='form-control formRequired' name='office_layer_4' id = 'office_layer_4'  tag='pb_html' onchange="layerChange(4)"></select>
        </div>
    </div>

    <div id="office_layer_div_5" class="form-group" style="display: none">
        <label> <%=isLanguageEnglish ? "Office":"দপ্তর"%></label>
        <div>
            <select class='form-control formRequired' name='office_layer_5' id = 'office_layer_5'  tag='pb_html' onchange="layerChange(5)"></select>
        </div>
    </div>

    <div id="office_layer_div_6" class="form-group" style="display: none">
        <label> <%=isLanguageEnglish ? "Office":"দপ্তর"%></label>
        <div>
            <select class='form-control formRequired' name='office_layer_6' id = 'office_layer_6'  tag='pb_html' onchange="layerChange(6)"></select>
        </div>
    </div>

    <div id="office_layer_div_7" class="form-group" style="display: none">
        <label> <%=isLanguageEnglish ? "Office":"দপ্তর"%></label>
        <div>
            <select class='form-control formRequired' name='office_layer_7' id = 'office_layer_7'  tag='pb_html' onchange="layerChange(7)"></select>
        </div>
    </div>

    <div id="office_layer_div_8" class="form-group" style="display: none">
        <label> <%=isLanguageEnglish ? "Office":"দপ্তর"%></label>
        <div>
            <select class='form-control formRequired' name='office_layer_8' id = 'office_layer_8'  tag='pb_html' onchange="layerChange(8)"></select>
        </div>
    </div>

    <div id="office_layer_div_9" class="form-group" style="display: none">
        <label> <%=isLanguageEnglish ? "Office":"দপ্তর"%></label>
        <div>
            <select class='form-control formRequired' name='office_layer_9' id = 'office_layer_9'  tag='pb_html' onchange="layerChange(9)"></select>
        </div>
    </div>

    <div id="office_layer_div_10" class="form-group" style="display: none">
        <label> <%=isLanguageEnglish ? "Office":"দপ্তর"%></label>
        <div>
            <select class='form-control formRequired' name='office_layer_10' id = 'office_layer_10'  tag='pb_html' onchange="layerChange(10)"></select>
        </div>
    </div>

    <div class="form-group ">
        <label><%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_SUPERIORDESIGNATIONID, loginDTO)%><span class="required"> * </span></label>
        <div id='superiorDesignationId_div_<%=i%>'>
            <select class='form-control' name='superiorDesignationId' required="required"
                    id='superiorDesignationId_select_<%=i%>'>
            </select>
        </div>
    </div>

    <div class="form-group ">
        <label>
            <%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_DESIGNATIONENG, loginDTO)%><span class="required"> * </span></label>
        <div id='designationEng_div_<%=i%>'>
            <input type='text' class='form-control' name='designationEng' id='designationEng_text_<%=i%>'
                   value=<%=actionName.equals("edit")?("'" + office_origin_unit_organogramsDTO.designationEng + "'"):("''")%> required="required"
            />
        </div>
    </div>
    <div class="form-group ">
        <label><%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_DESIGNATIONBNG, loginDTO)%><span class="required"> * </span></label>
        <div id='designationBng_div_<%=i%>'>
            <input type='text' class='form-control' name='designationBng' id='designationBng_text_<%=i%>'
                   value=<%=actionName.equals("edit")?("'" + office_origin_unit_organogramsDTO.designationBng + "'"):("''")%> required="required"
            />
        </div>
    </div>
    <div class="form-group ">
        <label><%=LM.getText(LC.EMP_TRAINING_DETAILS_ADD_GRADE, loginDTO)%><span class="required"> * </span></label>
        <div id='superiorUnitId_div_<%=i%>'>
            <select class='form-control' name='job_grade_type_cat' id='job_grade_type_select2' required="required"
                    ><%=CatRepository.getInstance().buildOptions("job_grade_type",Language,null)%>
            </select>
        </div>
    </div>

    <div class="form-group ">
        <label><%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_DESIGNATIONSEQUENCE, loginDTO)%></label>
        <div id='designationSequence_div_<%=i%>'>
            <input type='text' class='form-control' name='designationSequence'
                   id='designationSequence_text_<%=i%>'
                   value='<%=office_origin_unit_organogramsDTO.designationSequence%>'/>
        </div>
    </div>

    <div class="kt-portlet__foot pl-0">
        <div class="kt-form__actions">
            <a class="btn btn-primary btn-hover-brand btn-square" href="<%=request.getHeader("referer")%>">
                <%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_OFFICE_ORIGIN_UNIT_ORGANOGRAMS_CANCEL_BUTTON, loginDTO)%>
            </a>
            <button class="btn btn-outline-brand btn-elevate-hover ml-2" type="submit"
                    onclick="onSubmit()">
                <%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_OFFICE_ORIGIN_UNIT_ORGANOGRAMS_SUBMIT_BUTTON, loginDTO)%>
            </button>
        </div>
    </div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script src="<%=context%>office_origin_unit_organograms/office_origin_unit_organograms.js" type="text/javascript" charset="utf-8"></script>
<script src="<%=context%>common/data-validation.js" type="text/javascript" charset="utf-8"></script>
<script src="<%=context%>common/validation-message.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript">
    let lastSelectedOfficeId;
    let language = '<%=Language%>';
    const officeMinistriesSelector = $('#select_office_ministries');
    const officeLayersSelector = $('#select_office_layers');
    const officeOriginSelector = $('#select_office_origins');
    const officeSelector = $('#select_offices');
    const superiorDesignationSelector = $('#superiorDesignationId_select_0');

    function getOfficer(officer_id, officer_select) {
        console.log("getting officer");
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (this.responseText.includes('option')) {
                    console.log("got response for officer");
                    document.getElementById(officer_select).innerHTML = this.responseText;
                    if (document.getElementById(officer_select).length > 1) {
                        document.getElementById(officer_select).removeAttribute("disabled");
                    }
                }
                else {
                    console.log("got errror response for officer");
                }
            }
            else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("POST", "Office_origin_unit_organogramsServlet?actionType=getGRSOffice&officer_id=" + officer_id, true);
        xhttp.send();
    }

    function getLayer(layernum, layerID, childLayerID, selectedValue) {
        console.log("getting layer");
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (this.responseText.includes('option')) {
                    console.log("got response");
                    document.getElementById(childLayerID).innerHTML = this.responseText;
                }
                else {
                    console.log("got errror response");
                }
            }
            else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("POST", "Office_origin_unit_organogramsServlet?actionType=getGRSLayer&layernum=" + layernum + "&layerID="
            + layerID + "&childLayerID=" + childLayerID + "&selectedValue=" + selectedValue, true);
        xhttp.send();
    }

    function layerselected(layernum, layerID, childLayerID, hiddenInput, hiddenInputForTopLayer, officerElement) {
        const layervalue = document.getElementById(layerID).value;
        console.log("layervalue = " + layervalue);
        document.getElementById(hiddenInput).value = layervalue;
        if (layernum === 0) {
            document.getElementById(hiddenInputForTopLayer).value = layervalue;
        }
        if (layernum === 0 || (layernum === 1 && document.getElementById(hiddenInputForTopLayer).value === 3)) {
            document.getElementById(childLayerID).setAttribute("style", "display: inline;");
            getLayer(layernum, layerID, childLayerID, layervalue);
        }
        if (officerElement !== null) {
            getOfficer(layervalue, officerElement);
        }
    }

    function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    function PostprocessAfterSubmiting(row) {
    }

    function addHTML(id, HTML) {
        document.getElementById(id).innerHTML += HTML;
    }

    function getRequests() {
        var s1 = location.search.substring(1, location.search.length).split('&'),
            r = {}, s2, i;
        for (i = 0; i < s1.length; i += 1) {
            s2 = s1[i].split('=');
            r[decodeURIComponent(s2[0]).toLowerCase()] = decodeURIComponent(s2[1]);
        }
        return r;
    }

    function Request(name) {
        return getRequests()[name.toLowerCase()];
    }

    function ShowExcelParsingResult(suffix) {
        const failureMessage = document.getElementById("failureMessage_" + suffix);
        if (failureMessage == null) {
            console.log("failureMessage_" + suffix + " not found");
        }
        console.log("value = " + failureMessage.value);
        if (failureMessage.value !== "") {
            alert("Excel uploading result:" + failureMessage.value);
        }
    }

    function init(row) {
    }

    var row = 0;
    bkLib.onDomLoaded(function () {
    });
    window.onload = function () {
        init(row);
    }

    function SetCheckBoxValues(tablename) {
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);
        var j = 0;
        for (i = 0; i < document.getElementById(tablename).childNodes.length; i++) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[type="checkbox"]');
                checkbox.id = tablename + "_cb_" + j;
                j++;
            }
        }
    }

    let edit_office_origin_unit_organogram = {};

    function getAllOfficeOriginUnit() {
        superiorUnitSelector.empty().append($('<option>', {value: 0, text: '<%=selectLang%>'}));
        superiorDesignationSelector.empty().append($('<option>', {value: 0, text: '<%=selectLang%>'}));

        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {

                const data = JSON.parse(this.responseText);
                for (let i = 0; i < data.length; i++) {
                    const d = data[i];
                    superiorUnitSelector.append($('<option>', {value: d.id, text: d.unit_name_bng}));
                }
            }
        };
        let url = "Office_unitsServlet?actionType=ajax_office_units";

        if(officeMinistriesSelector && officeMinistriesSelector.val()){
            url+=  "&office_ministry_id="+officeMinistriesSelector.val();
        }
        if(officeLayersSelector && officeLayersSelector.val()){
            url+= "&office_layer_id="+officeLayersSelector.val();
        }
        if(officeOriginSelector && officeOriginSelector.val()){
            url+= "&office_origin_id="+officeOriginSelector.val();
        }
        if(officeSelector && officeSelector.val()){
            url+= "&office_id="+officeSelector.val();
        }

        xhttp.open("Get", url, true);
        xhttp.send();
    }

    function upperLayerIdChange() {
        const upper_layer_id = superiorUnitSelector.val();
        superiorDesignationSelector.empty().append($('<option>', {value: 0, text: '<%=selectLang%>'}));

        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {

                const data = (this.responseText.length > 0 ? JSON.parse(this.responseText) : '');
                superiorDesignationSelector.empty().append($('<option>', {value: 0, text: '<%=selectLang%>'}));

                for (let i = 0; i < data.length; i++) {
                    const d = data[i];
                    superiorDesignationSelector.append($('<option>', {value: d.id, text: d.designation_bng}));
                }

                if ($('#actionName').val() === 'edit') {
                    superiorDesignationSelector.select2().select2('val', edit_office_origin_unit_organogram.superior_designation_id);
                }
            }
        };
        xhttp.open("Get", "office_unit_organograms_Servlet?actionType=office_unit_organograms" + "&office_unit_id=" + upper_layer_id, true);
        xhttp.send();
    }

    function extraLayerClick(showExtraLayerName, table_name, id, parentID, upperLayerId) {
        console.log(showExtraLayerName, ' ', table_name, ' ', id, ' parent_id: ', upperLayerId, ' upper_layer_id: ', upperLayerId);

        clear();
        $('#actionName').val('add');
        $('#edit_page').css('display', '');
        $('#officeOriginUnitId_0').val(upperLayerId);
    }

    /* Maintain the sequence account to form */
    const labelName = [
        '<%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_OFFICEORIGINUNITID, loginDTO)%>',
        '<%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_SUPERIORUNITID, loginDTO)%>',
        '<%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_SUPERIORDESIGNATIONID, loginDTO)%>',
        '<%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_DESIGNATIONENG, loginDTO)%>',
        '<%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_DESIGNATIONBNG, loginDTO)%>',
        '<%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_DESIGNATIONLEVEL, loginDTO)%>',
        '<%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_DESIGNATIONSEQUENCE, loginDTO)%>'
    ];

    function onSubmit() {
        const id = $('#id_hidden_0').val();
        const actionType = $('#actionName').val();
        const form_data = getFormData(id);
        const obj = $(this);
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (this.responseText !== '') {
                    showToast(success_message_bng, success_message_eng);
                    location.reload();
                }
            }
        };
        console.log(form_data);
        if (true) {
            xhttp.open("POST", "Office_origin_unit_organogramsServlet" + "?actionType=" + actionType + "&" + form_data, true);
            xhttp.send();
        }
    }

    function clear() {
        $('#officeOriginUnitId_0').val(0);
        $('#job_grade_type_select2').val(0);
        superiorUnitSelector.prop('selectedIndex', 0);
        superiorDesignationSelector.prop('selectedIndex', 0);
        $('#designationEng_text_0').val('');
        $('#designationBng_text_0').val('');
        $('#status_checkbox_0').prop('checked', false);
        //$('#designationLevel_text_0').val('0');
        $('#designationSequence_text_0').val('0');
        $('#tree_view').html();

        $('#select2-superiorUnitId_select_0-container').html('');
        $('#select2-superiorDesignationId_select_0-container').html('');
        superiorDesignationSelector.empty().append($('<option>', {value: 0, text: '<%=selectLang%>'}));
    }

    function getFormData(id) {
        const form_data = "identity=" + id
            + "&officeId=" + lastSelectedOfficeId
            + "&designationEng=" + $('#designationEng_text_0').val()
            + "&designationBng=" + $('#designationBng_text_0').val()
            + "&superiorDesignationId=" + superiorDesignationSelector.val()
            + "&designationSequence=" + $('#designationSequence_text_0').val()
            + "&jobGrade=" + $('#job_grade_type_select2').val()
            + "&status=" + $('#status_checkbox_0').is(":checked");
        return form_data;
    }

    function specialChecking() {
        if (parseInt($('#officeOriginUnitId_0').val()) === 0) {
            showError('শাখা সিলেক্ট করুন, অফিস ষ্টাফ এর প্লাস বাটনে ক্লিক দিন', 'Click plus button to select unit');
            return false;
        }

        if (superiorUnitSelector.val() <= 0 || superiorUnitSelector.val() === undefined || superiorUnitSelector.val() === null) {
            superiorUnitSelector.val(0);
        }
        if (superiorDesignationSelector.val() <= 0 || superiorDesignationSelector.val() === undefined || superiorDesignationSelector.val() === null) {
            superiorDesignationSelector.val(0);
        }

        if (superiorUnitSelector.val() !== 0) {
            property.superiorUnitId = {
                'from_dropdown': true,
                'required': true
            };
            property.superiorDesignationId = {
                'from_dropdown': true,
                'required': true
            }
        } else {
            property.superiorUnitId = {};
            property.superiorDesignationId = {}
        }

        return true;
    }

    function plusClicked(table, id, column_name) {
        $('#id_hidden_0').val(id);
        $('#actionName').val('edit');

        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                edit_office_origin_unit_organogram = JSON.parse(this.responseText);
                setFormData(JSON.parse(this.responseText));
            }
        };
        xhttp.open("Get", "Office_origin_unit_organogramsServlet?actionType=officeOriginUnitOrganogramsSingle&id=" + id, true);
        xhttp.send();
    }

    function setFormData(data) {
        $('#edit_page').css('display', '');

        $('#officeOriginUnitId_0').val(edit_office_origin_unit_organogram.office_origin_unit_id);
        superiorUnitSelector.select2().select2('val', edit_office_origin_unit_organogram.superior_unit_id);
        //superiorDesignationSelector.select2().select2('val', edit_office_origin_unit_organogram.superior_designation_id);
        $('#designationEng_text_0').val(edit_office_origin_unit_organogram.designation_eng);
        $('#designationBng_text_0').val(edit_office_origin_unit_organogram.designation_bng);
        $('#designationLevel_text_0').val(edit_office_origin_unit_organogram.designation_level);
        $('#designationSequence_text_0').val(edit_office_origin_unit_organogram.designation_sequence);
        $('#status_checkbox_0').prop('checked', edit_office_origin_unit_organogram.status);

        upperLayerIdChange();
    }

    function layerChange(layer){
        let layerid = "#office_layer_"+layer;
        let nextLayer = layer+1;
        for(let i=nextLayer;i<10;i++){
            document.getElementById("office_layer_div_"+i).style.display='none';
            document.getElementById("office_layer_"+i).innerHTML = '';
        }
        lastSelectedOfficeId = $(layerid).val();
        setSuperDesignation(lastSelectedOfficeId)
        let url = "Office_unitsServlet?actionType=ajax_office&parent_id=" + lastSelectedOfficeId + "&language=" + language;
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                console.log(fetchedData);
                if(fetchedData){
                    document.getElementById("office_layer_div_"+nextLayer).style.display='block';
                    document.getElementById("office_layer_"+nextLayer).innerHTML = fetchedData;
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function setSuperDesignation(id){
        document.getElementById("superiorDesignationId_select_0").innerHTML = '';
        let url = "Office_unitsServlet?actionType=ajax_designation&office_id=" + id + "&language=" + language;
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                console.log(fetchedData);
                if(fetchedData){
                    document.getElementById("superiorDesignationId_select_0").innerHTML = fetchedData;
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

</script>