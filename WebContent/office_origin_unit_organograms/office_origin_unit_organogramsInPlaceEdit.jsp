<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="office_origin_unit_organograms.Office_origin_unit_organogramsDTO"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>

<%
Office_origin_unit_organogramsDTO office_origin_unit_organogramsDTO = (Office_origin_unit_organogramsDTO)request.getAttribute("office_origin_unit_organogramsDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(office_origin_unit_organogramsDTO == null)
{
	office_origin_unit_organogramsDTO = new Office_origin_unit_organogramsDTO();
	
}
System.out.println("office_origin_unit_organogramsDTO = " + office_origin_unit_organogramsDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";
Office_origin_unit_organogramsDTO row = office_origin_unit_organogramsDTO;
%>




























	














<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_id" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='id' id = 'id_hidden_<%=i%>' value='<%=ID%>'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_officeOriginUnitId'>")%>
			
	
	<div class="form-inline" id = 'officeOriginUnitId_div_<%=i%>'>
		<select class='form-control'  name='officeOriginUnitId' id = 'officeOriginUnitId_select_<%=i%>'  >
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "office_origin_unit_organograms", "officeOriginUnitId_select_" + i, "form-control", "officeOriginUnitId", office_origin_unit_organogramsDTO.officeOriginUnitId + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "office_origin_unit_organograms", "officeOriginUnitId_select_" + i, "form-control", "officeOriginUnitId" );			
}
out.print(Options);
%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_superiorUnitId'>")%>
			
	
	<div class="form-inline" id = 'superiorUnitId_div_<%=i%>'>
		<select class='form-control'  name='superiorUnitId' id = 'superiorUnitId_select_<%=i%>'  >
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "office_origin_units", "superiorUnitId_select_" + i, "form-control", "superiorUnitId", office_origin_unit_organogramsDTO.superiorUnitId + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "office_origin_units", "superiorUnitId_select_" + i, "form-control", "superiorUnitId" );			
}
out.print(Options);
%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_superiorDesignationId'>")%>
			
	
	<div class="form-inline" id = 'superiorDesignationId_div_<%=i%>'>
		<select class='form-control'  name='superiorDesignationId' id = 'superiorDesignationId_select_<%=i%>'  >
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "office_origin_units", "superiorDesignationId_select_" + i, "form-control", "superiorDesignationId", office_origin_unit_organogramsDTO.superiorDesignationId + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "office_origin_units", "superiorDesignationId_select_" + i, "form-control", "superiorDesignationId" );			
}
out.print(Options);
%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_designationEng'>")%>
			
	
	<div class="form-inline" id = 'designationEng_div_<%=i%>'>
		<input type='text' class='form-control'  name='designationEng' id = 'designationEng_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + office_origin_unit_organogramsDTO.designationEng + "'"):("''")%> required="required"
  />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_designationBng'>")%>
			
	
	<div class="form-inline" id = 'designationBng_div_<%=i%>'>
		<input type='text' class='form-control'  name='designationBng' id = 'designationBng_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + office_origin_unit_organogramsDTO.designationBng + "'"):("''")%> required="required"
  />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_shortNameEng" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='shortNameEng' id = 'shortNameEng_hidden_<%=i%>' value='<%=office_origin_unit_organogramsDTO.shortNameEng%>'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_shortNameBng" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='shortNameBng' id = 'shortNameBng_hidden_<%=i%>' value='<%=office_origin_unit_organogramsDTO.shortNameBng%>'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_designationLevel'>")%>
			
	
	<div class="form-inline" id = 'designationLevel_div_<%=i%>'>
		<input type='text' class='form-control'  name='designationLevel' id = 'designationLevel_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + office_origin_unit_organogramsDTO.designationLevel + "'"):("'" + "0" + "'")%>   />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_designationSequence'>")%>
			
	
	<div class="form-inline" id = 'designationSequence_div_<%=i%>'>
		<input type='text' class='form-control'  name='designationSequence' id = 'designationSequence_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + office_origin_unit_organogramsDTO.designationSequence + "'"):("'" + "0" + "'")%>   />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_status'>")%>
			
	
	<div class="form-inline" id = 'status_div_<%=i%>'>
		<input type='checkbox' class='form-control'  name='status' id = 'status_checkbox_<%=i%>' value='true' <%=(actionName.equals("edit") && String.valueOf(office_origin_unit_organogramsDTO.status).equals("true"))?("checked"):""%>  ><br>						


	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_createdBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='createdBy' id = 'createdBy_hidden_<%=i%>' value='<%=office_origin_unit_organogramsDTO.createdBy%>'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=office_origin_unit_organogramsDTO.modifiedBy%>'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_created" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='created' id = 'created_hidden_<%=i%>' value='<%=office_origin_unit_organogramsDTO.created%>'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modified" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modified' id = 'modified_hidden_<%=i%>' value='<%=office_origin_unit_organogramsDTO.modified%>'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + office_origin_unit_organogramsDTO.isDeleted + "'"):("'" + "false" + "'")%>/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=office_origin_unit_organogramsDTO.lastModificationTime%>'/>
		
												
<%=("</td>")%>
					
		