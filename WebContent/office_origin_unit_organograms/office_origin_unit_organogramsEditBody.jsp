<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="office_origin_unit_organograms.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%
    Office_origin_unit_organogramsDTO office_origin_unit_organogramsDTO;
    office_origin_unit_organogramsDTO = (Office_origin_unit_organogramsDTO) request.getAttribute("office_origin_unit_organogramsDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (office_origin_unit_organogramsDTO == null) {
        office_origin_unit_organogramsDTO = new Office_origin_unit_organogramsDTO();
    }
    System.out.println("office_origin_unit_organogramsDTO = " + office_origin_unit_organogramsDTO);
    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("addOriginUnitOrganograms")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle;
    if (actionName.equals("edit")) {
        formTitle = LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_FORMNAME, loginDTO);
    } else {
        formTitle = LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_ADD_OFFICE_ORIGIN_UNIT_ORGANOGRAMS_ADD_FORMNAME, loginDTO);
    }
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;
    String value = "";
    Office_origin_unit_organogramsDTO row = office_origin_unit_organogramsDTO;

    String context = "../../.." + request.getContextPath() + "/";
%>

<input type="hidden" id="actionName" value="<%=actionName%>"/>

<div class="kt-form" id="bigform" name="bigform" enctype="multipart/form-data">
    <%@ page import="java.text.SimpleDateFormat" %>
    <%@ page import="java.util.Date" %>
    <%@ page import="pb.*" %>
    <%
        String Language = LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_LANGUAGE, loginDTO);
        String Options;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String datestr = dateFormat.format(date);
    %>
    <input type='hidden' class='form-control' name='id' id='id_hidden_<%=i%>' value='<%=ID%>'/>
    <div class="form-group " style="display: none">
        <label>
            <%=(LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_OFFICEORIGINUNITID, loginDTO))%>
        </label>
        <div id='officeOriginUnitId_Div_<%=i%>'>
            <input type='text' class='form-control' name='officeOriginUnitId'
                   id='officeOriginUnitId_<%=i%>'
                   value=<%=actionName.equals("edit")?("'" + office_origin_unit_organogramsDTO.officeOriginUnitId + "'"):("''")%> required="required"
            />
        </div>
    </div>
    <div class="form-group ">
        <label>
            <%=(LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_SUPERIORUNITID, loginDTO))%>
        </label>
        <div id='superiorUnitId_div_<%=i%>'>
            <select class='form-control' name='superiorUnitId' id='superiorUnitId_select_<%=i%>'
                    onchange="upperLayerIdChange()">
            </select>
        </div>
    </div>
    <div class="form-group ">
        <label>
            <%=(LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_SUPERIORDESIGNATIONID, loginDTO))%>
        </label>
        <div id='superiorDesignationId_div_<%=i%>'>
            <select class='form-control' name='superiorDesignationId'
                    id='superiorDesignationId_select_<%=i%>'>
            </select>
        </div>
    </div>
    <div class="form-group ">
        <label>
            <%=(LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_DESIGNATIONENG, loginDTO))%>
            <span class="required"> * </span>
        </label>
        <div id='designationEng_div_<%=i%>'>
            <input type='text' class='form-control' name='designationEng' id='designationEng_text_<%=i%>'
                   value=<%=actionName.equals("edit")?("'" + office_origin_unit_organogramsDTO.designationEng + "'"):("''")%> required="required"
            />
        </div>
    </div>
    <div class="form-group ">
        <label>
            <%=(LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_DESIGNATIONBNG, loginDTO))%>
            <span class="required"> * </span>
        </label>
        <div id='designationBng_div_<%=i%>'>
            <input type='text' class='form-control' name='designationBng' id='designationBng_text_<%=i%>'
                   value=<%=actionName.equals("edit")?("'" + office_origin_unit_organogramsDTO.designationBng + "'"):("''")%> required="required"
            />
        </div>
    </div>
    <input type='hidden' class='form-control' name='shortNameEng' id='shortNameEng_hidden_<%=i%>'
           value=<%=actionName.equals("edit") ? ("'" + office_origin_unit_organogramsDTO.shortNameEng + "'") : ("'" + " " + "'")%>/>
    <input type='hidden' class='form-control' name='shortNameBng' id='shortNameBng_hidden_<%=i%>'
           value=<%=actionName.equals("edit") ? ("'" + office_origin_unit_organogramsDTO.shortNameBng + "'") : ("'" + " " + "'")%>/>
    <div class="form-group ">
        <label>
            <%=(LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_DESIGNATIONLEVEL, loginDTO))%>
        </label>
        <div id='designationLevel_div_<%=i%>'>
            <input type='text' class='form-control' name='designationLevel'
                   id='designationLevel_text_<%=i%>'
                   value=<%=actionName.equals("edit") ? ("'" + office_origin_unit_organogramsDTO.designationLevel + "'") : ("'" + "0" + "'")%>/>
        </div>
    </div>
    <div class="form-group ">
        <label>
            <%=(LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_DESIGNATIONSEQUENCE, loginDTO))%>
        </label>
        <div id='designationSequence_div_<%=i%>'>
            <input type='text' class='form-control' name='designationSequence'
                   id='designationSequence_text_<%=i%>'
                   value=<%=actionName.equals("edit") ? ("'" + office_origin_unit_organogramsDTO.designationSequence + "'") : ("'" + "0" + "'")%>/>
        </div>
    </div>
    <div id='status_div_<%=i%>'>
        <label class="kt-checkbox kt-checkbox--brand">
            <%=(actionName.equals("edit")) ? (LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_STATUS, loginDTO)) : (LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_ADD_STATUS, loginDTO))%>
            <input type='checkbox' name='status' id='status_checkbox_<%=i%>'
                   value='true' <%=(actionName.equals("edit") && String.valueOf(office_origin_unit_organogramsDTO.status).equals("true"))?("checked"):""%>  >
            <span></span>
        </label>
    </div>
    <input type='hidden' class='form-control' name='createdBy' id='createdBy_hidden_<%=i%>'
           value=<%=actionName.equals("edit") ? ("'" + office_origin_unit_organogramsDTO.createdBy + "'") : ("'" + "0" + "'")%>/>
    <input type='hidden' class='form-control' name='modifiedBy' id='modifiedBy_hidden_<%=i%>'
           value=<%=actionName.equals("edit") ? ("'" + office_origin_unit_organogramsDTO.modifiedBy + "'") : ("'" + "0" + "'")%>/>
    <input type='hidden' class='form-control' name='created' id='created_hidden_<%=i%>'
           value=<%=actionName.equals("edit") ? ("'" + office_origin_unit_organogramsDTO.created + "'") : ("'" + "0" + "'")%>/>
    <input type='hidden' class='form-control' name='modified' id='modified_hidden_<%=i%>'
           value=<%=actionName.equals("edit") ? ("'" + office_origin_unit_organogramsDTO.modified + "'") : ("'" + "0" + "'")%>/>
    <input type='hidden' class='form-control' name='isDeleted' id='isDeleted_hidden_<%=i%>'
           value= <%=actionName.equals("edit") ? ("'" + office_origin_unit_organogramsDTO.isDeleted + "'") : ("'" + "false" + "'")%>/>
    <input type='hidden' class='form-control' name='lastModificationTime'
           id='lastModificationTime_hidden_<%=i%>'
           value=<%=actionName.equals("edit") ? ("'" + office_origin_unit_organogramsDTO.lastModificationTime + "'") : ("'" + "0" + "'")%>/>
    <div class="kt-portlet__foot pl-0">
        <div class="kt-form__actions">
            <a class="btn btn-primary btn-hover-brand btn-square" href="<%=request.getHeader("referer")%>">
                <%
                    if (actionName.equals("edit")) {
                        out.print(LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_OFFICE_ORIGIN_UNIT_ORGANOGRAMS_CANCEL_BUTTON, loginDTO));
                    } else {
                        out.print(LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_ADD_OFFICE_ORIGIN_UNIT_ORGANOGRAMS_CANCEL_BUTTON, loginDTO));
                    }

                %>
            </a>
            <button class="btn btn-outline-brand btn-elevate-hover ml-2" type="submit"
                    onclick="onSubmit(0,'<%=actionName%>')">
                <%
                    if (actionName.equals("edit")) {
                        out.print(LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_OFFICE_ORIGIN_UNIT_ORGANOGRAMS_SUBMIT_BUTTON, loginDTO));
                    } else {
                        out.print(LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_ADD_OFFICE_ORIGIN_UNIT_ORGANOGRAMS_SUBMIT_BUTTON, loginDTO));
                    }
                %>
            </button>
        </div>
    </div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">
    function getOfficer(officer_id, officer_select) {
        console.log("getting officer");
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText.includes('option')) {
                    console.log("got response for officer");
                    document.getElementById(officer_select).innerHTML = this.responseText;
                    if (document.getElementById(officer_select).length > 1) {
                        document.getElementById(officer_select).removeAttribute("disabled");
                    }
                }
                else {
                    console.log("got errror response for officer");
                }
            }
            else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("POST", "Office_origin_unit_organogramsServlet?actionType=getGRSOffice&officer_id=" + officer_id, true);
        xhttp.send();
    }

    function getLayer(layernum, layerID, childLayerID, selectedValue) {
        console.log("getting layer");
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText.includes('option')) {
                    console.log("got response");
                    document.getElementById(childLayerID).innerHTML = this.responseText;
                }
                else {
                    console.log("got errror response");
                }
            }
            else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("POST", "Office_origin_unit_organogramsServlet?actionType=getGRSLayer&layernum=" + layernum + "&layerID="
            + layerID + "&childLayerID=" + childLayerID + "&selectedValue=" + selectedValue, true);
        xhttp.send();
    }

    function layerselected(layernum, layerID, childLayerID, hiddenInput, hiddenInputForTopLayer, officerElement) {
        var layervalue = document.getElementById(layerID).value;
        console.log("layervalue = " + layervalue);
        document.getElementById(hiddenInput).value = layervalue;
        if (layernum == 0) {
            document.getElementById(hiddenInputForTopLayer).value = layervalue;
        }
        if (layernum == 0 || (layernum == 1 && document.getElementById(hiddenInputForTopLayer).value == 3)) {
            document.getElementById(childLayerID).setAttribute("style", "display: inline;");
            getLayer(layernum, layerID, childLayerID, layervalue);
        }
        if (officerElement !== null) {
            getOfficer(layervalue, officerElement);
        }
    }

    function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    function PostprocessAfterSubmiting(row) {
    }

    function addHTML(id, HTML) {
        document.getElementById(id).innerHTML += HTML;
    }

    function getRequests() {
        var s1 = location.search.substring(1, location.search.length).split('&'),
            r = {}, s2, i;
        for (i = 0; i < s1.length; i += 1) {
            s2 = s1[i].split('=');
            r[decodeURIComponent(s2[0]).toLowerCase()] = decodeURIComponent(s2[1]);
        }
        return r;
    }

    function Request(name) {
        return getRequests()[name.toLowerCase()];
    }

    function ShowExcelParsingResult(suffix) {
        var failureMessage = document.getElementById("failureMessage_" + suffix);
        if (failureMessage == null) {
            console.log("failureMessage_" + suffix + " not found");
        }
        console.log("value = " + failureMessage.value);
        if (failureMessage != null && failureMessage.value != "") {
            alert("Excel uploading result:" + failureMessage.value);
        }
    }

    function init(row) {
    }

    var row = 0;
    bkLib.onDomLoaded(function () {
    });
    window.onload = function () {
        init(row);
    }

    function SetCheckBoxValues(tablename) {
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);
        var j = 0;
        for (i = 0; i < document.getElementById(tablename).childNodes.length; i++) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[type="checkbox"]');
                checkbox.id = tablename + "_cb_" + j;
                j++;
            }
        }
    }
</script>

<script src="<%=context%>office_origin_unit_organograms/office_origin_unit_organograms.js" type="text/javascript"
        charset="utf-8"></script>
<script src="<%=context%>common/data-validation.js" type="text/javascript" charset="utf-8"></script>
<script src="<%=context%>common/validation-message.js" type="text/javascript" charset="utf-8"></script>

<%--Init--%>
<script>

    let edit_office_origin_unit_organogram = {};

    function getAllOfficeOriginUnit() {
        $('#superiorUnitId_select_0').empty().append($('<option>', {value: 0, text: '-- Please select one --'}));
        $('#superiorDesignationId_select_0').empty().append($('<option>', {value: 0, text: '-- Please select one --'}));

        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {

                const data = JSON.parse(this.responseText);
                for (let i = 0; i < data.length; i++) {
                    const d = data[i];
                    $('#superiorUnitId_select_0').append($('<option>', {value: d.id, text: d.unit_name_bng}));
                }
            }
        };

        const a = !$('#select_office_ministries') ? null : $('#select_office_ministries').val();
        const b = !$('#select_office_layers') ? null : $('#select_office_layers').val();
        const c = !$('#select_office_origins') ? null : $('#select_office_origins').val();

        xhttp.open("Get", "Office_origin_unit_organogramsServlet?actionType=officeOriginUnits" +
            "&office_ministry_id=" + a + "&office_layer_id=" + b + "&office_origin_id=" + c, true);
        xhttp.send();
    }

    function upperLayerIdChange() {
        const upper_layer_id = $('#superiorUnitId_select_0').val();
        $('#superiorDesignationId_select_0').empty().append($('<option>', {value: 0, text: '-- Please select one --'}));

        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {

                const data = (this.responseText.length > 0 ? JSON.parse(this.responseText) : '');
                $('#superiorDesignationId_select_0').empty().append($('<option>', {value: 0, text: '-- Please select one --'}));

                for (let i = 0; i < data.length; i++) {
                    const d = data[i];
                    $('#superiorDesignationId_select_0').append($('<option>', {value: d.id, text: d.designation_bng}));
                }

                if ($('#actionName').val() === 'edit') {
                    $('#superiorDesignationId_select_0').select2().select2('val', edit_office_origin_unit_organogram.superior_designation_id);
                }
            }
        };
        xhttp.open("Get", "Office_origin_unit_organogramsServlet?actionType=officeOriginUnitOrganograms" + "&office_origin_unit_id=" + upper_layer_id, true);
        xhttp.send();
    }

</script>

<script>
    function extraLayerClick(showExtraLayerName, table_name, id, parentID, upperLayerId) {
        console.log(showExtraLayerName, ' ', table_name, ' ', id, ' parent_id: ', upperLayerId, ' upper_layer_id: ', upperLayerId);

        clear();
        $('#actionName').val('add');
        $('#edit_page').css('display', '');
        $('#officeOriginUnitId_0').val(upperLayerId);
    }
</script>

<script type='text/javascript'>

    /* Maintain the sequence account to form */
    const labelName = [
        '<%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_OFFICEORIGINUNITID, loginDTO)%>',
        '<%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_SUPERIORUNITID, loginDTO)%>',
        '<%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_SUPERIORDESIGNATIONID, loginDTO)%>',
        '<%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_DESIGNATIONENG, loginDTO)%>',
        '<%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_DESIGNATIONBNG, loginDTO)%>',
        '<%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_DESIGNATIONLEVEL, loginDTO)%>',
        '<%=LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_DESIGNATIONSEQUENCE, loginDTO)%>'
    ];

    function onSubmit(row, validate) {
        const id = $('#id_hidden_0').val();
        const actionType = $('#actionName').val();

        if (!specialChecking()) return;

        // Get form serialize data
        const form_data = getFormData(id);

        const obj = $(this);
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (this.responseText !== '') {
                    clear();
                    fillNextElementWithID(1, previous_element);
                    showToast(success_message_bng, success_message_eng);
                }
            }
        };

        console.log(form_data);
        if (OfficeOriginUnitOrganogram.add(null, null, form_data, labelName)) {
            xhttp.open("POST", "Office_origin_unit_organogramsServlet" + "?actionType=" + actionType + "&" + form_data, true);
            xhttp.send();
        }
    }

    function clear() {
        $('#officeOriginUnitId_0').val(0);
        $('#superiorUnitId_select_0').prop('selectedIndex', 0);
        $('#superiorDesignationId_select_0').prop('selectedIndex', 0);
        $('#designationEng_text_0').val('');
        $('#designationBng_text_0').val('');
        $('#status_checkbox_0').prop('checked', false);
        $('#designationLevel_text_0').val('0');
        $('#designationSequence_text_0').val('0');
        $('#tree_view').html();

        $('#select2-superiorUnitId_select_0-container').html('');
        $('#select2-superiorDesignationId_select_0-container').html('');
        $('#superiorDesignationId_select_0').empty().append($('<option>', {value: 0, text: '-- Please select one --'}));
    }

    function getFormData(id) {

        const form_data = "identity=" + id
            + "&officeOriginUnitId=" + $('#officeOriginUnitId_0').val()
            + "&superiorUnitId=" + $('#superiorUnitId_select_0').val()
            + "&superiorDesignationId=" + $('#superiorDesignationId_select_0').val()
            + "&designationEng=" + $('#designationEng_text_0').val()
            + "&designationBng=" + $('#designationBng_text_0').val()
            + "&designationLevel=" + $('#designationLevel_text_0').val()
            + "&designationSequence=" + $('#designationSequence_text_0').val()
            + "&status=" + $('#status_checkbox_0').is(":checked");

        return form_data;
    }

    function specialChecking() {
        if (parseInt($('#officeOriginUnitId_0').val()) === 0) {
            showError('শাখা সিলেক্ট করুন, অফিস ষ্টাফ এর প্লাস বাটনে ক্লিক দিন', 'Click plus button to select unit');
            return false;
        }

        if ($('#superiorUnitId_select_0').val() <= 0 || $('#superiorUnitId_select_0').val() === undefined || $('#superiorUnitId_select_0').val() === null) {
            $('#superiorUnitId_select_0').val(0);
        }
        if ($('#superiorDesignationId_select_0').val() <= 0 || $('#superiorDesignationId_select_0').val() === undefined || $('#superiorDesignationId_select_0').val() === null) {
            $('#superiorDesignationId_select_0').val(0);
        }

        if ($('#superiorUnitId_select_0').val() != 0) {
            property.superiorUnitId = {
                'from_dropdown': true,
                'required': true
            };
            property.superiorDesignationId = {
                'from_dropdown': true,
                'required': true
            }
        } else {
            property.superiorUnitId = {};
            property.superiorDesignationId = {}
        }

        return true;
    }

</script>

<%--Edit, Plus click--%>
<script>

    function plusClicked(table, id, column_name) {
        $('#id_hidden_0').val(id);
        $('#actionName').val('edit');

        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                edit_office_origin_unit_organogram = JSON.parse(this.responseText);
                setFormData(JSON.parse(this.responseText));
            }
        };
        xhttp.open("Get", "Office_origin_unit_organogramsServlet?actionType=officeOriginUnitOrganogramsSingle&id=" + id, true);
        xhttp.send();
    }

    function setFormData(data) {
        $('#edit_page').css('display', '');

        $('#officeOriginUnitId_0').val(edit_office_origin_unit_organogram.office_origin_unit_id);
        $('#superiorUnitId_select_0').select2().select2('val', edit_office_origin_unit_organogram.superior_unit_id);
        //$('#superiorDesignationId_select_0').select2().select2('val', edit_office_origin_unit_organogram.superior_designation_id);
        $('#designationEng_text_0').val(edit_office_origin_unit_organogram.designation_eng);
        $('#designationBng_text_0').val(edit_office_origin_unit_organogram.designation_bng);
        $('#designationLevel_text_0').val(edit_office_origin_unit_organogram.designation_level);
        $('#designationSequence_text_0').val(edit_office_origin_unit_organogram.designation_sequence);
        $('#status_checkbox_0').prop('checked', edit_office_origin_unit_organogram.status);

        upperLayerIdChange();
    }

</script>