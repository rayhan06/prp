<%@page import="asset_model.AssetAssigneeDTO"%>
<%@ page import="dashboard.DashboardDTO" %>
<%@ page import="java.util.List" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="pb.*" %>
<%@page import="appointment.*" %>
<%@page import="workflow.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="com.google.gson.Gson" %>
<%@page pageEncoding="UTF-8" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String context = "../../.." + request.getContextPath() + "/";

    DashboardDTO dashboardDTO = (DashboardDTO) request.getAttribute("dashboardDTO");

    String Language = LM.getText(LC.SUPPORT_TICKET_EDIT_LANGUAGE, loginDTO);


%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=Language.equalsIgnoreCase("english")?"Asset Dashboard":"অ্যাসেট ড্যাশবোর্ড"%>
                </h3>
            </div>
            <button type="button" type="button" class="btn-sm border-0 shadow text-white btn-border-radius text-nowrap" 
            style="background-color: #bb336c;"
            onclick="location.href='DashboardServlet?actionType=ticket_admin'">
		        <div class="d-flex justify-content-center align-items-center">
		            <%=LM.getText(LC.HM_TICKET_DASHBOARD, loginDTO)%>
		        </div>
		    </button>
        </div>
        <div class="kt-portlet__body form-body  bg-light">
            <div class="row">
                <div class="col-lg-12 my-2">
                    <div class="kt-portlet shadow dashboard-card-border-radius overflow-hidden py-4"
                    >
                        <div class="row">
                            <div class="col-12 mb-1 mt-3 d-flex justify-content-between align-items-center px-4 mx-2">
                                                <span class="h3">
                                                     <%=LM.getText(LC.HM_ASSETS, loginDTO)%> <%=LM.getText(LC.HM_COUNT, loginDTO)%>
                                                </span>
                                <button
                                        onclick="location.href='Asset_modelServlet?actionType=searchAssetAssignee&filter=<%=AssetAssigneeDTO.ALLEXCEPTSOFT%>'"
                                        type="button"
                                        class="btn dashboard-count"
                                >
                                    <h3 class="mb-0">
                                        <%=Utils.getDigits(dashboardDTO.assetCount, Language)%>
                                    </h3>
                                </button>
                            </div>
                            
                               <div class="col-6 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                            <span class="h3 text-white">
                                                                <%=LM.getText(LC.HM_ASSIGNED, loginDTO)%>
                                                            </span>
                                            </div>
                                            <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        onclick="location.href='Asset_modelServlet?actionType=searchAssetAssignee&filter=<%=AssetAssigneeDTO.ASSIGNED%>'"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white d-flex align-items-center justify-content-center">
                                                                        <h3 class="mb-0"><%=Utils.getDigits(dashboardDTO.assignedAssetCount, Language)%></h3>
                                                                </button>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                            <span class="h3 text-white">
                                                                <%=LM.getText(LC.HM_FREE, loginDTO)%>
                                                            </span>
                                            </div>
                                            <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                       onclick="location.href='Asset_modelServlet?actionType=searchAssetAssignee&filter=<%=AssetAssigneeDTO.NOT_ASSIGNED%>'"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white d-flex align-items-center justify-content-center">
                                                                        <h3 class="mb-0">
                                                                        <%=Utils.getDigits(dashboardDTO.freeAssetCount, Language)%>
                                                                        </h3>
                                                                </button>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                            <span class="h3 text-white">
                                                                <%=Language.equalsIgnoreCase("english")?"Condemned":"কনডেমকৃত"%>
                                                            </span>
                                            </div>
                                            <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button 
                                                                		onclick="location.href='Asset_modelServlet?actionType=searchAssetAssignee&filter=<%=AssetAssigneeDTO.CONDEMNED%>'"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white d-flex align-items-center justify-content-center">
                                                            <h3 class="mb-0">
                                                            <%=Utils.getDigits(dashboardDTO.condemnedAssets, Language)%>
                                                            </h3>
                                                        </button>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-6 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                            <span class="h3 text-white">
                                                                <%=Language.equalsIgnoreCase("english")?"Others":"অন্যান্য"%>
                                                            </span>
                                            </div>
                                            <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button 
                                                                		onclick="location.href='Asset_modelServlet?actionType=searchAssetAssignee&filter=<%=AssetAssigneeDTO.OTHER%>'"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white d-flex align-items-center justify-content-center">
                                                            <h3 class="mb-0">
                                                            <%=Utils.getDigits(dashboardDTO.otherAssets, Language)%>
                                                            </h3>
                                                        </button>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    
                <div class="col-lg-12 my-2">
                    <div class="kt-portlet shadow dashboard-card-border-radius overflow-hidden py-4"
                    >
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <!--Div that will hold the pie chart-->
                        <div class="grid-item">
                            <div id="catwiseAsssetStatus" style="height: 400px;"></div>

                            <h3 style="text-align: center"><%=Language.equalsIgnoreCase("english")?"Category and Statuswise Assets":"সম্পদের ধরণ ও অবস্থা"%>
                            </h3>
                        </div>
                    </div>
                </div>
            
                <div class="col-lg-6 my-2">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden py-4"
                    >
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <!--Div that will hold the pie chart-->
                        <div class="grid-item">
                            <div id="wingwiseAsset"></div>

                            <h3 style="text-align: center"><%=Language.equalsIgnoreCase("english")?"Wingwise Assets":"উইংভিত্তিক সম্পদ"%>
                            </h3>                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-2">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden py-4"
                    >
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <!--Div that will hold the pie chart-->
                        <div class="grid-item">
                            <div id="keyedSoft"></div>

                            <h3 style="text-align: center">
                            <%=Language.equalsIgnoreCase("english")?"Licenced Software":"লাইসেন্সযুক্ত সফটওয়্যার"%>

                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var context = '${context}';
    var pluginsContext = '${pluginsContext}';

    var dashboardDTOJson = '<%=new Gson().toJson( request.getAttribute( "dashboardDTO"  ) )%>';
    var dashboardDTO = JSON.parse(dashboardDTOJson);

</script>
<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

    // Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages': ['corechart']});
   

    google.charts.setOnLoadCallback(keyedSoft);
    
    function keyedSoft() {


    	var data = google.visualization.arrayToDataTable(
    			[
    				['<%=LM.getText(LC.HM_CATEGORY, loginDTO)%>', '<%=LM.getText(LC.HM_ASSIGNED, loginDTO)%>',
    					'<%=LM.getText(LC.HM_FREE, loginDTO)%>'
					],

    				<%
    				for (long key : dashboardDTO.keyStatusCount.keySet()) 
    			    {
    					int[] catWiseCount = dashboardDTO.keyStatusCount.get(key);
    					%>
    					[
    						'<%=CatDAO.getName(Language, "software", key)%>',
    						<%=catWiseCount[DashboardDTO.ASSIGNED_POS]%>,
    						<%=catWiseCount[DashboardDTO.NOT_ASSIGNED_POS]%>
    						
    					],

    					<%
    				}
    				%>

    			]
    		);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Licensed Software Count":"লাইসেন্সযুক্ত সফটওয়্যারের সংখ্যা"%>',
            //curveType: 'function',
            legend: {position: 'bottom'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('keyedSoft'));

        chart.draw(data, options);
    }
    
    google.charts.setOnLoadCallback(drawCatwiseAsssetStatus);

    function drawCatwiseAsssetStatus() {


    	var data = google.visualization.arrayToDataTable(
    			[
    				['<%=LM.getText(LC.HM_CATEGORY, loginDTO)%>', '<%=LM.getText(LC.HM_ASSIGNED, loginDTO)%>',
    					'<%=LM.getText(LC.HM_FREE, loginDTO)%>',
    					'<%=Language.equalsIgnoreCase("english")?"Condemned":"কনডেমকৃত"%>',
						'<%=Language.equalsIgnoreCase("english")?"Warranty or Others":"ওয়ার‍্যান্টি বা অন্যান্য"%>'
					],

    				<%
    				for (long key : dashboardDTO.assetCategoryStatusCount.keySet()) 
    			    {
    					int[] catWiseCount = dashboardDTO.assetCategoryStatusCount.get(key);
    					%>
    					[
    						'<%=CommonDAO.getName(Language, "asset_category", key)%>',
    						<%=catWiseCount[DashboardDTO.ASSIGNED_POS]%>,
    						<%=catWiseCount[DashboardDTO.NOT_ASSIGNED_POS]%>,
    						<%=catWiseCount[DashboardDTO.CONDEMNED_POS]%>,
    						<%=catWiseCount[DashboardDTO.OTHER_POS]%>,
    					],

    					<%
    				}
    				%>

    			]
    		);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Category and Statuswise Assets":"সম্পদের ধরণ ও অবস্থা"%>',
            //curveType: 'function',
            legend: {position: 'bottom'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('catwiseAsssetStatus'));

        chart.draw(data, options);
    }

    google.charts.setOnLoadCallback(wingwiseAsset);

    function wingwiseAsset() {

    	 var data = google.visualization.arrayToDataTable(
    				[
    		            ['<%=LM.getText(LC.HM_WING, loginDTO)%>', '<%=LM.getText(LC.HM_COUNT, loginDTO)%>'],
    		            <%

    		            for(long key : dashboardDTO.assetWingCount.keySet())
    		            {
    					%>
    						[
    							'<%=WorkflowController.getOfficeUnitName(key, Language)%>',
    							<%=dashboardDTO.assetWingCount.get(key)%>
    						],
    		            <%
    					}
    					%>
    		        ]
    			);

        var options = {
            title: '<%=LM.getText(LC.HM_WINGWISE, loginDTO)%> <%=LM.getText(LC.HM_ASSET, loginDTO)%> <%=LM.getText(LC.HM_COUNT, loginDTO)%>',
            legend: {position: 'left'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('wingwiseAsset'));

        chart.draw(data, options);
    }

    
</script>