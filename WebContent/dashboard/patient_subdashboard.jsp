<div class="kt-portlet__head">
    <div class="kt-portlet__head-label">
        <h3 class="kt-portlet__head-title">
            <%=LM.getText(LC.HM_DOCTOR_VISITS_IN_THE_LAST_6_MONTHS, loginDTO)%>
        </h3>
    </div>
</div>

<div class="kt-portlet__body">
    <div id="monthWiseVisits" style="height: 300px;"></div>
</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

    var context = '${context}';
    var pluginsContext = '${pluginsContext}';

    var dashboardDTOJson = '<%=new Gson().toJson( request.getAttribute("dashboardDTO"))%>';
    var dashboardDTO = JSON.parse(dashboardDTOJson);


    google.charts.load('current', {'packages': ['corechart']});
    google.charts.setOnLoadCallback(monthWiseTicket);

    function monthWiseTicket() {
        var data = google.visualization.arrayToDataTable([
            ['Month', '<%=LM.getText(LC.HM_COUNT, loginDTO)%>'],
            <%

            for(int j = 0; j <= 6; j ++)
            {
                %>
            ['<%=Utils.getDigits(dashboardDTO.last6Months[j], Language)%>', dashboardDTO.last6MonthVisits[<%=j%>]],
            <%
        }
        %>
        ]);
        var options = {
            title: '',
            legend: {position: 'Top'}
        };
        var chart = new google.visualization.ColumnChart(document.getElementById('monthWiseVisits'));
        chart.draw(data, options);
    }
</script>