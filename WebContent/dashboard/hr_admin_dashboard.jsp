<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="util.HttpRequestUtils" %>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String language = LM.getText(LC.TRAINING_CALENDER_EDIT_LANGUAGE, loginDTO);
    boolean isLangEng = "English".equalsIgnoreCase(language);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.RECRUITMENT_DASHBOARD_DASHBOARD, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <%--Office Search modal--%>
            <div class="">
                <div class="d-flex justify-content-between align-items-center mb-4">
                    <div class="">
                        <h3 class="font-weight-normal table-title">
                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENTOFFICEUNITID, loginDTO)%>
                        </h3>
                    </div>
                    <div class="">
                        <input type="hidden" name='officeUnitId' id='office_units_id_input' value="1">
                        <button type="button" id="office_units_id_text"
                                class="btn btn-secondary shadow dashboard-card-border-radius overflow-hidden"
                                onclick="officeModalButtonClicked();">
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="shadow mb-4 dashboard-card-border-radius overflow-hidden"
                            >
                                <%@include file="hr/emp_count_by_office_unit.jsp" %>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="shadow mb-4 dashboard-card-border-radius overflow-hidden"
                            >
                                <%@include file="hr/emp_count_by_gender.jsp" %>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="shadow mb-4 dashboard-card-border-radius overflow-hidden"
                            >
                                <%@include file="hr/emp_count_by_employee_type.jsp" %>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="shadow mb-4 dashboard-card-border-radius overflow-hidden">
                                <%@include file="hr/emp_count_by_class.jsp" %>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="shadow mb-4 dashboard-card-border-radius overflow-hidden">
                                <%@include file="hr/monthwise_typewise_lpr_count.jsp" %>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="shadow mb-4 dashboard-card-border-radius overflow-hidden">
                                <%@include file="hr/emp_count_by_division.jsp" %>
                            </div>
                        </div>
                        
                        <div class="col-lg-12">
                            <div class="shadow mb-4 dashboard-card-border-radius overflow-hidden">
                                <%@include file="hr/monthwise_typewise_joining_count.jsp" %>
                            </div>
                        </div>
                        
                        <div class="col-lg-12">
                            <div class="shadow mb-4 dashboard-card-border-radius overflow-hidden">
                                <%@include file="hr/wingwise.jsp" %>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script>
    $(() => {
        viewOfficeIdInInput({
            name: '<%=language.equalsIgnoreCase("English")? "SPEAKER" : "মাননীয় স্পীকারের কার্যালয়"%>',
            id: 1
        });
    });
    
    var employeeTypes;

    function showCharts() {
        $('#office_units_id_modal_button').prop('disabled',true);
        $.ajax({
            url: "DashboardServlet?actionType=ajax_admin&officeUnitId=" + $("#office_units_id_input").val(),
            type: "GET",
            async: true,
            success: function (fetchedData) {
                let responseData = JSON.parse(fetchedData);
                drawGender(responseData.gender);
                drawEmpTypeCount(responseData.type);
                drawEmpClass(responseData.classResult);
                drawEmpDivision(responseData.division);
                drawEmpCount(responseData.designation);
                drawJoining6Months(responseData.joining6Count);
                drawLpr6Months(responseData.lpr6Count);
                drawWingWise(responseData.wingWiseCount);
                employeeTypes = responseData.employeeTypes;
                $('#office_units_id_modal_button').prop('disabled',false);
            },
            error: function (error) {
                console.log(error);
                $('#office_units_id_modal_button').prop('disabled',false);
            }
        });
    }

    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        console.log(selectedOffice);
        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id_input').val(selectedOffice.id);
        showCharts();
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    const officeUnitIdInput = $('#office_units_id_input');
    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        officeSearchSetSelectedOfficeLayers(officeUnitIdInput.val());
        $('#search_office_modal').modal();
    }
</script>