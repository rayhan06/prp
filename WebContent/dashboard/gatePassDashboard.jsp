<%@page pageEncoding="UTF-8" %>
<%@ page import="dashboard.DashboardDTO" %>
<%@ page import="java.util.List" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="com.google.gson.Gson" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String context = "../../.." + request.getContextPath() + "/";

    DashboardDTO dashboardDTO = (DashboardDTO) request.getAttribute("dashboardDTO");

    String Language = LM.getText(LC.GATE_PASS_EDIT_LANGUAGE, loginDTO);


%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.RECRUITMENT_DASHBOARD_DASHBOARD, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body" style="background: #f2f2f2">
            <div class="row">
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet shadow p-1 dashboard-card-border-radius overflow-hidden py-4">
                        <div class="row">
                            <div class="col-12 mb-1 mt-3 d-flex justify-content-between align-items-center px-4 mx-2">
                                                <span class="h3">
                                                    <%=Language.equalsIgnoreCase("English") ? "Gate Pass" : "প্রবেশ পাস"%>
                                                </span>
                                <button
                                        onclick="location.href='Gate_passServlet?actionType=search'"
                                        type="button"
                                        class="btn dashboard-count"
                                >
                                    <h2>
                                        <%=Utils.getDigits(dashboardDTO.gatePassDashboardDataModel.totalGatePassAppliedInThisMonth + "", Language)%>
                                    </h2>
                                </button>
                            </div>
                            <div class="col-12 text-center my-2">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <div class="d-flex justify-content-center align-items-center">
                                            <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                                <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                                    <div class="">
                                                            <span class="h3 text-white">
                                                                <%=Language.equalsIgnoreCase("English") ? "Issued" : "ইস্যুকৃত"%>
                                                            </span>
                                                    </div>
                                                    <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        onclick="location.href='Gate_passServlet?actionType=search&filter=issued&day=month'"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white pt-3">
                                                                        <h2>
                                                                            <%=Utils.getDigits(dashboardDTO.gatePassDashboardDataModel.processedPassAppliedInThisMonth + "", Language)%>
                                                                        </h2>
                                                                </button>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="d-flex justify-content-center align-items-center">
                                            <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                                <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                                    <div class="">
                                                            <span class="h3 text-white">
                                                                <%=Language.equalsIgnoreCase("English") ? "Approved" : "অনুমোদিত"%>
                                                            </span>
                                                    </div>
                                                    <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        onclick="location.href='Gate_passServlet?actionType=search&filter=approved&day=month'"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white pt-3">
                                                                        <h2>
                                                                            <%=Utils.getDigits(dashboardDTO.gatePassDashboardDataModel.approvedPassAppliedInThisMonth + "", Language)%>
                                                                        </h2>
                                                                </button>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-center my-2">

                                <div class="row">
                                    <%--                                    <div class="col-xl-6">--%>
                                    <%--                                        <div class="d-flex justify-content-center align-items-center">--%>
                                    <%--                                            <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">--%>
                                    <%--                                                <div class="d-flex justify-content-between align-items-center my-1 px-3">--%>
                                    <%--                                                    <div class="">--%>
                                    <%--                                                            <span class="h3 text-white">--%>
                                    <%--                                                                <%=Language.equalsIgnoreCase("English") ? "Processing" : "প্রক্রিয়াধীন"%>--%>
                                    <%--                                                            </span>--%>
                                    <%--                                                    </div>--%>
                                    <%--                                                    <div class="ml-5">--%>
                                    <%--                                                            <span class="patient_count2 text-white">--%>
                                    <%--                                                                <button--%>
                                    <%--                                                                        onclick="location.href='Gate_passServlet?actionType=search'"--%>
                                    <%--                                                                        type="button"--%>
                                    <%--                                                                        class="btn dashboard-count text-white pt-3">--%>
                                    <%--                                                                        <h2>--%>
                                    <%--                                                                            <%=Utils.getDigits(dashboardDTO.gatePassDashboardDataModel.processedPassAppliedInThisMonth + "", Language)%>--%>
                                    <%--                                                                        </h2>--%>
                                    <%--                                                                </button>--%>
                                    <%--                                                            </span>--%>
                                    <%--                                                    </div>--%>
                                    <%--                                                </div>--%>
                                    <%--                                            </div>--%>
                                    <%--                                        </div>--%>
                                    <%--                                    </div>--%>
                                    <div class="col-xl-6 mt-3 mt-xl-0">
                                        <div class="d-flex justify-content-center align-items-center">
                                            <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                                <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                                    <div class="">
                                                            <span class="h3 text-white">
                                                                <%=Language.equalsIgnoreCase("English") ? "Pending" : "অনিষ্পন্ন"%>
                                                            </span>
                                                    </div>
                                                    <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        onclick="location.href='Gate_passServlet?actionType=search&filter=requested&day=month'"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white pt-3">
                                                                        <h2>
                                                                            <%=Utils.getDigits(dashboardDTO.gatePassDashboardDataModel.pendingGatePassAppliedInThisMonth + "", Language)%>
                                                                        </h2>
                                                                </button>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 mt-3 mt-xl-0">
                                        <div class="d-flex justify-content-center align-items-center">
                                            <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                                <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                                    <div class="">
                                                            <span class="h3 text-white">
                                                                <%=Language.equalsIgnoreCase("English") ? "Dismissed" : "অননুমোদিত"%>
                                                            </span>
                                                    </div>
                                                    <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        onclick="location.href='Gate_passServlet?actionType=search&filter=rejected&day=month'"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white pt-3">
                                                                        <h2>
                                                                            <%=Utils.getDigits(dashboardDTO.gatePassDashboardDataModel.dismissedGatePassAppliedInThisMonth + "", Language)%>
                                                                        </h2>
                                                                </button>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet shadow p-1 dashboard-card-border-radius overflow-hidden py-4"
                    >
                        <div class="row">
                            <div class="col-12 mb-1 mt-3 d-flex justify-content-between align-items-center px-4 mx-2">
                                                <span class="h3">
                                                    <%=Language.equalsIgnoreCase("English") ? "Visitor Pass" : "সংসদ ভিজিট পাস"%>
                                                </span>
                                <button
                                        onclick="location.href='Visitor_passServlet?actionType=search&filter=rejected&day=month'"
                                        type="button"
                                        class="btn dashboard-count"
                                >
                                    <h2>
                                        <%=Utils.getDigits(dashboardDTO.gatePassDashboardDataModel.totalVisitorPassAppliedInThisMonth + "", Language)%>
                                    </h2>
                                </button>
                            </div>
                            <div class="col-12 text-center my-2">

                                <div class="row">
                                    <div class="col-xl-6">
                                        <div class="d-flex justify-content-center align-items-center">
                                            <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                                <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                                    <div class="">
                                                            <span class="h3 text-white">
                                                                <%=Language.equalsIgnoreCase("English") ? "Approved" : "অনুমোদিত"%>
                                                            </span>
                                                    </div>
                                                    <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        onclick="location.href='Visitor_passServlet?actionType=search'"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white pt-3">
                                                                        <h2><%=Utils.getDigits(dashboardDTO.gatePassDashboardDataModel.approvedVisitorPassAppliedInThisMonth + "", Language)%></h2>
                                                                </button>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 mt-3 mt-xl-0">
                                        <div class="d-flex justify-content-center align-items-center">
                                            <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                                <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                                    <div class="">
                                                            <span class="h3 text-white">
                                                                <%=Language.equalsIgnoreCase("English") ? "Pending" : "অনিষ্পন্ন"%>
                                                            </span>
                                                    </div>
                                                    <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        onclick="location.href='Visitor_passServlet?actionType=search'"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white pt-3">
                                                                        <h2><%=Utils.getDigits(dashboardDTO.gatePassDashboardDataModel.pendingVisitorPassAppliedInThisMonth + "", Language)%></h2>
                                                                </button>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-center my-2">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <div class="d-flex justify-content-center align-items-center">
                                            <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                                <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                                    <div class="">
                                                            <span class="h3 text-white">
                                                                <%=Language.equalsIgnoreCase("English") ? "Received" : "গ্রহণকৃত"%>
                                                            </span>
                                                    </div>
                                                    <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        onclick="location.href='Visitor_passServlet?actionType=search'"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white pt-3">
                                                                        <h2><%=Utils.getDigits(dashboardDTO.gatePassDashboardDataModel.receivedVisitorPassAppliedInThisMonth + "", Language)%></h2>
                                                                </button>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 mt-3 mt-xl-0">
                                        <div class="d-flex justify-content-center align-items-center">
                                            <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                                <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                                    <div class="">
                                                            <span class="h3 text-white">
                                                                <%=Language.equalsIgnoreCase("English") ? "Dismissed" : "অননুমোদিত"%>
                                                            </span>
                                                    </div>
                                                    <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        onclick="location.href='Visitor_passServlet?actionType=search'"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white pt-3">
                                                                        <h2><%=Utils.getDigits(dashboardDTO.gatePassDashboardDataModel.dismissedVisitorPassAppliedInThisMonth + "", Language)%></h2>
                                                                </button>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet shadow p-1 dashboard-card-border-radius overflow-hidden py-4"
                    >
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <!--Div that will hold the pie chart-->
                        <div class="grid-item">
                            <div id="typewiseGatePass"></div>

                            <h3 style="text-align: center">
                                <%=Language.equals("English") ? "Passwise number of gate pass" : "পাসের ধরণভিত্তিক প্রবেশ পাস"%>
                            </h3>
                        </div>

                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet shadow p-1 dashboard-card-border-radius overflow-hidden py-4"
                    >
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <!--Div that will hold the pie chart-->
                        <div class="grid-item">
                            <div id="typewiseVisitorPass"></div>

                            <h3 style="text-align: center">
                                <%=Language.equals("English") ? "Visitorwise number of visitor pass" : "দর্শনার্থীর ধরণভিত্তিক প্রবেশ পাস"%>
                            </h3>
                        </div>

                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet shadow p-1 dashboard-card-border-radius overflow-hidden py-4 px-5"
                    >
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <!--Div that will hold the pie chart-->
                        <div class="grid-item">
                            <div id="approvalwiseGatePass"></div>

                            <h3 style="text-align: center">
                                <%=Language.equals("English") ? "Month wise status of gate pass" : "অনুমোদনের অবস্থা ভিত্তিক প্রবেশ পাস"%>
                            </h3>
                        </div>

                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet shadow p-1 dashboard-card-border-radius overflow-hidden py-4"
                    >
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <!--Div that will hold the pie chart-->
                        <div class="grid-item">
                            <div id="approvalwiseVisitorPass"></div>

                            <h3 style="text-align: center">
                                <%=Language.equals("English") ? "Month wise number of visitor pass" : "অনুমোদনের অবস্থা ভিত্তিক সংসদ ভিজিট পাস"%>
                            </h3>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var context = '${context}';
    var pluginsContext = '${pluginsContext}';

    var dashboardDTOJson = '<%=new Gson().toJson( request.getAttribute( "dashboardDTO"  ) )%>';
    var dashboardDTO = JSON.parse(dashboardDTOJson);


    var isAdmin = request.getAttribute("isAdmin");
</script>
<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

    // Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages': ['corechart']});


    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Gate Pass Type', 'Number of applicants'],
            ['<%=Language.equals("English") ? "Parliament Area Pass (Visitor)" : "সংসদ এলাকা পাস (দর্শনার্থী)"%>', <%=dashboardDTO.gatePassDashboardDataModel.areaPassVisitor%>],
            ['<%=Language.equals("English") ? "Parliament Area Pass (Official)" : "সংসদ এলাকা পাস (দাপ্তরিক)"%>', <%=dashboardDTO.gatePassDashboardDataModel.areaPassOfficial%>],
            ['<%=Language.equals("English") ? "Visitor Pass (parliament building entry)" : "দর্শনার্থী পাস (সংসদ ভবন প্রবেশ)"%>', <%=dashboardDTO.gatePassDashboardDataModel.visitorPass%>],
            ['<%=Language.equals("English") ? "Parliament Gallery Pass" : "গ্যালারি পাস"%>', <%=dashboardDTO.gatePassDashboardDataModel.galleryPass%>],
            ['<%=Language.equals("English") ? "Meeting/Briefing Pass" : "মিটিং/ব্রিফিং পাস"%>', <%=dashboardDTO.gatePassDashboardDataModel.meetingBriefingPass%>]
        ]);

        var options = {
            // title: 'My Daily Activities'
        };

        var chart = new google.visualization.PieChart(document.getElementById('typewiseGatePass'));

        chart.draw(data, options);
    }

    google.charts.setOnLoadCallback(drawChart2);

    function drawChart2() {

        var data = google.visualization.arrayToDataTable([
            ['Gate Pass Type', 'Number of applicants'],
            ['<%=Language.equals("English") ? "Visitor Pass (Domestic)" : "স্থানীয় দর্শনার্থী"%>', <%=dashboardDTO.gatePassDashboardDataModel.visitorDomestic%>],
            ['<%=Language.equals("English") ? "Visitor Pass (Foreigner)" : "বিদেশী দর্শনার্থী"%>', <%=dashboardDTO.gatePassDashboardDataModel.visitorForeigner%>]
        ]);

        var options = {
            // title: 'My Daily Activities'
        };

        var chart = new google.visualization.PieChart(document.getElementById('typewiseVisitorPass'));

        chart.draw(data, options);
    }


    google.charts.load('current', {'packages': ['bar']});


    google.charts.setOnLoadCallback(drawChartBarGate);

    let today = new Date();

    const monthEn = {
        '1': 'January',
        '2': 'February',
        '3': 'March',
        '4': 'April',
        '5': 'May',
        '6': 'June',
        '7': 'July',
        '8': 'August',
        '9': 'September',
        '10': 'October',
        '11': 'November',
        '12': 'December'
    };
    const monthBn = {
        '1': "জানুয়ারী",
        '2': "ফেব্রুয়ারী",
        '3': "মার্চ",
        '4': "এপ্রিল",
        '5': "মে",
        '6': "জুন",
        '7': "জুলাই",
        '8': "আগস্ট",
        '9': "সেপ্টেম্বর",
        '10': "অক্টোবর",
        '11': "নভেম্বর",
        '12': "ডিসেম্বর"
    };
    let month;
    if ('<%=Language%>' == "English")
        month = monthEn;
    else
        month = monthBn;

    function drawChartBarGate() {
        let data = google.visualization.arrayToDataTable([
            ['Month', 'Processing', 'Dismissed', 'Pending', 'Approved'],
            [month[(today.getMonth() + 7) % 12 + 1], <%=dashboardDTO.gatePassDashboardDataModel.processedPassAppliedMonthWise.get(5)%>, <%=dashboardDTO.gatePassDashboardDataModel.dismissedGatePassAppliedMonthWise.get(5)%>, <%=dashboardDTO.gatePassDashboardDataModel.pendingGatePassAppliedMonthWise.get(5)%>, <%=dashboardDTO.gatePassDashboardDataModel.approvedPassAppliedMonthWise.get(5)%>],
            [month[(today.getMonth() + 8) % 12 + 1], <%=dashboardDTO.gatePassDashboardDataModel.processedPassAppliedMonthWise.get(4)%>, <%=dashboardDTO.gatePassDashboardDataModel.dismissedGatePassAppliedMonthWise.get(4)%>, <%=dashboardDTO.gatePassDashboardDataModel.pendingGatePassAppliedMonthWise.get(4)%>, <%=dashboardDTO.gatePassDashboardDataModel.approvedPassAppliedMonthWise.get(4)%>],
            [month[(today.getMonth() + 9) % 12 + 1], <%=dashboardDTO.gatePassDashboardDataModel.processedPassAppliedMonthWise.get(3)%>, <%=dashboardDTO.gatePassDashboardDataModel.dismissedGatePassAppliedMonthWise.get(3)%>, <%=dashboardDTO.gatePassDashboardDataModel.pendingGatePassAppliedMonthWise.get(3)%>, <%=dashboardDTO.gatePassDashboardDataModel.approvedPassAppliedMonthWise.get(3)%>],
            [month[(today.getMonth() + 10) % 12 + 1], <%=dashboardDTO.gatePassDashboardDataModel.processedPassAppliedMonthWise.get(2)%>, <%=dashboardDTO.gatePassDashboardDataModel.dismissedGatePassAppliedMonthWise.get(2)%>, <%=dashboardDTO.gatePassDashboardDataModel.pendingGatePassAppliedMonthWise.get(2)%>, <%=dashboardDTO.gatePassDashboardDataModel.approvedPassAppliedMonthWise.get(2)%>],
            [month[(today.getMonth() + 11) % 12 + 1], <%=dashboardDTO.gatePassDashboardDataModel.processedPassAppliedMonthWise.get(1)%>, <%=dashboardDTO.gatePassDashboardDataModel.dismissedGatePassAppliedMonthWise.get(1)%>, <%=dashboardDTO.gatePassDashboardDataModel.pendingGatePassAppliedMonthWise.get(1)%>, <%=dashboardDTO.gatePassDashboardDataModel.approvedPassAppliedMonthWise.get(1)%>],
            [month[today.getMonth() + 1], <%=dashboardDTO.gatePassDashboardDataModel.processedPassAppliedMonthWise.get(0)%>, <%=dashboardDTO.gatePassDashboardDataModel.dismissedGatePassAppliedMonthWise.get(0)%>, <%=dashboardDTO.gatePassDashboardDataModel.pendingGatePassAppliedMonthWise.get(0)%>, <%=dashboardDTO.gatePassDashboardDataModel.approvedPassAppliedMonthWise.get(0)%>]
        ]);

        let options = {};

        let chart = new google.charts.Bar(document.getElementById('approvalwiseGatePass'));

        chart.draw(data, options);
    }


    google.load('visualization', '1.0', {'packages': ['corechart']});

    google.charts.setOnLoadCallback(drawChartBarGateVisitor);

    function drawChartBarGateVisitor() {
        let data = google.visualization.arrayToDataTable([
            ['Month', 'Received', 'Dismissed', 'Pending', 'Approved'],
            [month[(today.getMonth() + 7) % 12 + 1], <%=dashboardDTO.gatePassDashboardDataModel.receivedVisitorPassAppliedMonthWise.get(5)%>, <%=dashboardDTO.gatePassDashboardDataModel.dismissedVisitorPassAppliedMonthWise.get(5)%>, <%=dashboardDTO.gatePassDashboardDataModel.pendingVisitorPassAppliedMonthWise.get(5)%>, <%=dashboardDTO.gatePassDashboardDataModel.approvedVisitorPassAppliedMonthWise.get(5)%>],
            [month[(today.getMonth() + 8) % 12 + 1], <%=dashboardDTO.gatePassDashboardDataModel.receivedVisitorPassAppliedMonthWise.get(4)%>, <%=dashboardDTO.gatePassDashboardDataModel.dismissedVisitorPassAppliedMonthWise.get(4)%>, <%=dashboardDTO.gatePassDashboardDataModel.pendingVisitorPassAppliedMonthWise.get(4)%>, <%=dashboardDTO.gatePassDashboardDataModel.approvedVisitorPassAppliedMonthWise.get(4)%>],
            [month[(today.getMonth() + 9) % 12 + 1], <%=dashboardDTO.gatePassDashboardDataModel.receivedVisitorPassAppliedMonthWise.get(3)%>, <%=dashboardDTO.gatePassDashboardDataModel.dismissedVisitorPassAppliedMonthWise.get(3)%>, <%=dashboardDTO.gatePassDashboardDataModel.pendingVisitorPassAppliedMonthWise.get(3)%>, <%=dashboardDTO.gatePassDashboardDataModel.approvedVisitorPassAppliedMonthWise.get(3)%>],
            [month[(today.getMonth() + 10) % 12 + 1], <%=dashboardDTO.gatePassDashboardDataModel.receivedVisitorPassAppliedMonthWise.get(2)%>, <%=dashboardDTO.gatePassDashboardDataModel.dismissedVisitorPassAppliedMonthWise.get(2)%>, <%=dashboardDTO.gatePassDashboardDataModel.pendingVisitorPassAppliedMonthWise.get(2)%>, <%=dashboardDTO.gatePassDashboardDataModel.approvedVisitorPassAppliedMonthWise.get(2)%>],
            [month[(today.getMonth() + 11) % 12 + 1], <%=dashboardDTO.gatePassDashboardDataModel.receivedVisitorPassAppliedMonthWise.get(1)%>, <%=dashboardDTO.gatePassDashboardDataModel.dismissedVisitorPassAppliedMonthWise.get(1)%>, <%=dashboardDTO.gatePassDashboardDataModel.pendingVisitorPassAppliedMonthWise.get(1)%>, <%=dashboardDTO.gatePassDashboardDataModel.approvedVisitorPassAppliedMonthWise.get(1)%>],
            [month[today.getMonth() + 1], <%=dashboardDTO.gatePassDashboardDataModel.receivedVisitorPassAppliedMonthWise.get(0)%>, <%=dashboardDTO.gatePassDashboardDataModel.dismissedVisitorPassAppliedMonthWise.get(0)%>, <%=dashboardDTO.gatePassDashboardDataModel.pendingVisitorPassAppliedMonthWise.get(0)%>, <%=dashboardDTO.gatePassDashboardDataModel.approvedVisitorPassAppliedMonthWise.get(0)%>]
        ]);

        let options = {
            seriesType: 'bars',
            series: {4: {type: 'line'}}
        };

        let chart = new google.visualization.ComboChart(document.getElementById('approvalwiseVisitorPass'));

        chart.draw(data, options);
    }

</script>