<%@page import="workflow.WorkflowController"%>
<%@ page import="dashboard.DashboardDTO" %>
<%@ page import="java.util.List" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="pb.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String context = "../../.." + request.getContextPath() + "/";

    DashboardDTO dashboardDTO = (DashboardDTO) request.getAttribute("dashboardDTO");
    String Language = LM.getText(LC.SUPPORT_TICKET_EDIT_LANGUAGE, loginDTO);

%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.HM_PHYSIOTHERAPY_DASHBOARD, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body bg-light">
            <div class="row">
                <div class="col-lg-6">
                    <div class="">
                        <a href="Physiotherapy_planServlet?actionType=search&filter=todaysUpcomingTherapies">
                            <div class="">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div class="shadow dashboard-card-border-radius overflow-hidden w-100 h-100"
                                         style="background: linear-gradient(to right, #9861C2, #6132B2);">
                                        <div class="d-flex justify-content-between align-items-center mx-2 my-4 px-4">
                                            <div class="">
                                                <span class="h3 text-white">
                                                    <%=LM.getText(LC.HM_TODAYS_UNPERFORMED_THERAPIES, loginDTO)%>
                                                </span>
                                            </div>
                                            <div class="ml-5">
                                                <span class="patient_count h1 text-white">
                                                    <%=Utils.getDigits(dashboardDTO.todaysTherapies + "", Language)%>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 mt-4 mt-lg-0">
                    <div class="">
                        <a href="AppointmentServlet?actionType=search&filter=getUpcomingAppointments">
                            <div class="">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div class="shadow dashboard-card-border-radius overflow-hidden w-100 h-100"
                                         style="background: linear-gradient(to right, #EE9048, #EE9048);">
                                        <div class="d-flex justify-content-between align-items-center mx-2 my-4 px-4">
                                            <div class="">
                                                <span class="h3 text-white">
                                                    <%=LM.getText(LC.HM_UPCOMING_APPOINTMENTS, loginDTO)%>
                                                </span>
                                            </div>
                                            <div class="ml-5">
                                                <span class="patient_count h1 text-white"
                                                      style="background-color: #FCB42E;">
                                                    <%=Utils.getDigits(dashboardDTO.upComingAppointments + "", Language)%>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 mt-4">
                    <div class="d-flex justify-content-center align-items-center">
                        <div class="shadow dashboard-card-border-radius overflow-hidden w-100 h-100 bg-white">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        <%=LM.getText(LC.HM_THERAPY_TYPES, loginDTO)%>
                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="kt_widget2_tab1_content">
                                        <div class="kt-widget2">
                                            <div id="chart2_div"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 mt-4">
                    <div class="d-flex justify-content-center align-items-center">
                        <div class="shadow dashboard-card-border-radius overflow-hidden w-100 h-100 bg-white">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        <%=LM.getText(LC.HM_THERAPIES_ON_LAST_7_DAYS, loginDTO)%>
                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="kt_widget2_tab1_content2">
                                        <div class="kt-widget2">
                                            <div id="char1_div"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                 <div class="col-lg-12 mt-4">
                    <div class="d-flex justify-content-center align-items-center">
                        <div class="shadow dashboard-card-border-radius overflow-hidden w-100 h-100 bg-white">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        <%=Language.equalsIgnoreCase("english")?"Last 6 Month Therapies":"গত ৬ মাসে থেরাপির সংখ্যা"%>
                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="kt_widget2_tab1_content2">
                                        <div class="kt-widget2">
                                            <div id="month6_div"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var context = '${context}';
    var pluginsContext = '${pluginsContext}';

    var dashboardDTOJson = '<%=new Gson().toJson( request.getAttribute( "dashboardDTO"  ) )%>';
    var dashboardDTO = JSON.parse(dashboardDTOJson);


    //var isAdmin = request.getAttribute("isAdmin");
</script>

<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

    // Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages': ['corechart']});
    google.charts.setOnLoadCallback(drawChartPatientMonth);

    function drawChartPatientMonth() {

        var data = google.visualization.arrayToDataTable([
            ['<%=LM.getText(LC.HM_DAY, loginDTO)%>',  '<%=LM.getText(LC.HM_PATIENTS, loginDTO)%>'],
            <%
            for(int j = 0; j <= 7; j ++)
            {
            	%>
            ['<%=Utils.getDigits(dashboardDTO.last7Days[j], Language)%>', <%=dashboardDTO.last7DayPatients[j]%>],
            <%
        	}
        %>
        ]);

        var options = {
            title: '<%=LM.getText(LC.HM_THERAPIES_ON_LAST_7_DAYS, loginDTO)%>',
            legend: {position: 'top'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('char1_div'));

        chart.draw(data, options);
    }

    google.charts.setOnLoadCallback(drawTherapyTypeChart);

    function drawTherapyTypeChart() {


        var data = google.visualization.arrayToDataTable([
            ['<%=LM.getText(LC.HM_THERAPY_TYPES, loginDTO)%>', '<%=LM.getText(LC.HM_COUNT, loginDTO)%>'],
            //[dashboardDTO.lastSevenDaysDocumentsCountDTO.last7thDayDate,  dashboardDTO.lastSevenDaysDocumentsCountDTO.last7thDayCount],
            <%
           for(int j = 0; j < dashboardDTO.therapyNames.length; j ++)
           {
        	   
               %>
            		['<%=dashboardDTO.therapyNames[j]%>', <%=dashboardDTO.therapyCount[j]%>],
            	<%
        	   
        	}
        %>

        ]);

        var options = {
            title: '<%=LM.getText(LC.HM_THERAPY_TYPES, loginDTO)%>',
            //curveType: 'function',
            legend: {position: 'left'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart2_div'));

        chart.draw(data, options);
    }
    
    
    google.charts.setOnLoadCallback(draw6);
    
    var therapyData;

    function draw6() {
    	var dataArray = [];
    	dataArray[0] = [];
    	dataArray[0].push('<%=LM.getText(LC.HM_MONTH, loginDTO)%>');
    	
    	<%
    	for(long physiotherapist: dashboardDTO.physiotherapists)
    	{
    		%>
    		dataArray[0].push('<%=WorkflowController.getNameFromOrganogramId(physiotherapist, Language)%>');
    		<%
    	}
    	%>
    	
    	<%
    	int i = 1;
    	for(long [] therapies: dashboardDTO.last6MonthsTherapies)
    	{
    		%>
    		dataArray[<%=i%>] = [];
    		dataArray[<%=i%>].push('<%=Utils.getDigits(dashboardDTO.last6Months[i - 1], Language)%>');
    		<%
    		for(long count: therapies)
    		{
    			%>
    			dataArray[<%=i%>].push(<%=count%>);
    			<%
    		}
    		i++;
    	}
    	%>


        var data = google.visualization.arrayToDataTable(dataArray);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Last 6 month Therapies":"গত ৬ মাসের থেরাপির সংখ্যা"%>',
            //curveType: 'function',
            legend: {position: 'left'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('month6_div'));

        chart.draw(data, options);
    }
</script>