<%@page import="workflow.WorkflowController"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="pb.*" %>
<%@ page import="dashboard.DashboardDTO" %>
<%@ page import="pb.*" %>
<%@ page import="appointment.*" %>


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = LM.getLanguage(userDTO);
    
    String dashboardPath = (String)request.getAttribute("dashboardPath");
    if(dashboardPath == null)
    {
    	System.out.println("null dashboardPath");
    }
    DashboardDTO dashboardDTO = (DashboardDTO) request.getAttribute("dashboardDTO");
    if(dashboardDTO == null)
    {
    	System.out.println("null dashboardDTO");
    }
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.HM_MEDICAL_ADMIN_DASHBOARD, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body bg-light">
            <div class="row">
            	<div class="col-lg-12 my-3">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden"
                    <%=dashboardDTO.doctorNamesToday.size() == 0?"style='display:none'":""%>
                         >
                        <div>
                            <div id="divDrToday" style="height: 600px;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 my-3">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden"
                    <%=dashboardDTO.receptionistWiseCountToday.size() == 0?"style='display:none'":""%>
                         >
                        <div>
                            <div id="divRec" style="height: 300px;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden"
                         >
                        <div>
                            <div id="div1" style="height: 300px;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden"
                         >
                        <div>
                            <div id="div2" style="height: 300px;"></div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-12 my-3">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden"
                         >
                        <div>
                            <div id="div3" style="height: 500px;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden"
                         >
                        <div>
                            <div id="div4" style="height: 300px;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden"
                         >
                        <div>
                            <div id="div5" style="height: 300px;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden"
                         >
                        <div>
                            <div id="div6" style="height: 300px;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden"
                         >
                        <div>
                            <div id="div7" style="height: 300px;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden"
                         >
                        <div>
                            <div id="div8" style="height: 500px;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden"
                         >
                        <div>
                            <div id="div9" style="height: 500px;"></div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden"
                         >
                        <div>
                            <div id="div10" style="height: 500px;"></div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden"
                         >
                        <div>
                            <div id="div11" style="height: 500px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

    var context = '${context}';
    var pluginsContext = '${pluginsContext}';

    var dashboardDTOJson = '<%=new Gson().toJson( request.getAttribute( "dashboardDTO"  ) )%>';
    var dashboardDTO = JSON.parse(dashboardDTOJson);


    google.charts.load('current', {'packages': ['corechart']});
    google.charts.setOnLoadCallback(drawChartPatientDays);

    function drawChartPatientDays() {


        var data = google.visualization.arrayToDataTable([
            ['<%=LM.getText(LC.HM_DAY, loginDTO)%>', '<%=LM.getText(LC.HM_APPOINTMENTS, loginDTO)%>', '<%=LM.getText(LC.HM_PATIENTS, loginDTO)%>'],
            //[dashboardDTO.lastSevenDaysDocumentsCountDTO.last7thDayDate,  dashboardDTO.lastSevenDaysDocumentsCountDTO.last7thDayCount],
            <%
           for(int j = 0; j <= 7; j ++)
           {
               %>
            ['<%=Utils.getDigits(dashboardDTO.last7Days[j], Language)%>', <%=dashboardDTO.last7DayAppointments[j]%>, <%=dashboardDTO.last7DayPatients[j]%>],
            <%
        }
        %>

        ]);

        var options = {
            title: '<%=LM.getText(LC.HM_PATIENTS_HANDLED_IN_LAST_7_DAYS, loginDTO)%>',
            //curveType: 'function',
            legend: {position: 'bottom'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('div1'));

        chart.draw(data, options);
    }

    google.charts.setOnLoadCallback(drawLabChart);

    function drawLabChart() {


        var data = google.visualization.arrayToDataTable([
            ['<%=LM.getText(LC.HM_DIAGNOSTIC_TYPES, loginDTO)%>', 'Patients'],
            <%
           for(int j = 0; j < dashboardDTO.labTestTypes.length; j ++)
           {
               %>
            ['<%=Utils.getDigits(dashboardDTO.labTestTypes[j], Language)%>', <%=dashboardDTO.typeWiseDiagnosticProcedure[j]%>],
            <%
            }
            %>

        ]);

        var options = {
            title: '<%=LM.getText(LC.HM_DIAGNOSTIC_TYPES, loginDTO)%>',
            //curveType: 'function',
            legend: {position: 'left'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('div2'));

        chart.draw(data, options);
    }
    
    google.charts.setOnLoadCallback(drawDrChartToday);

    function drawDrChartToday() {


        var data = google.visualization.arrayToDataTable([
            ['<%=LM.getText(LC.HM_DOCTOR, loginDTO)%>', '<%=LM.getText(LC.HM_APPOINTMENTS, loginDTO)%>', '<%=LM.getText(LC.HM_PRESCRIPTION, loginDTO)%>'],
            //[dashboardDTO.lastSevenDaysDocumentsCountDTO.last7thDayDate,  dashboardDTO.lastSevenDaysDocumentsCountDTO.last7thDayCount],
            <%
           for(int j = 0; j < dashboardDTO.doctorNamesToday.size(); j ++)
           {
        	   String drname = dashboardDTO.doctorNamesToday.get(j);
        	   if(drname.length() > 18)
        	   {
        		   drname = drname.substring(0, 18) + "..";
        	   }
               %>
            ['<%=dashboardDTO.drActiveToday.get(j)?"*":""%> <%=drname%>',
            	<%=dashboardDTO.doctorWiseAppointmentsToday.get(j)%>,
            	<%=dashboardDTO.doctorWiseServedPatientsToday.get(j)%>],
            <%
            }
            %>

        ]);

        var options = {
        	title: '<%=Language.equalsIgnoreCase("english")?"Doctorwise served patients today":"আজকের ডাক্তারভিত্তিক রোগীর সংখ্যা"%>',
            legend: {position: 'right'}
        };

        var chart = new google.visualization.BarChart(document.getElementById('divDrToday'));
        var chartContainer = document.getElementById('divDrToday');
        
        google.visualization.events.addListener(chart, 'ready', function () {
            var labels = chartContainer.getElementsByTagName('text');
            for (let i = 0; i < labels.length; i++) {
            	  if(labels[i].innerHTML.startsWith("*"))
           		  {
            		  labels[i].setAttribute('font-weight', 'Bold');
           		  }
            }
        
          
          });
        
        
        chart.draw(data, options);
        
        
      

    }

    google.charts.setOnLoadCallback(drawDrChart);

    function drawDrChart() {


        var data = google.visualization.arrayToDataTable([
            ['<%=LM.getText(LC.HM_DOCTOR, loginDTO)%>', '<%=LM.getText(LC.HM_APPOINTMENTS, loginDTO)%>', '<%=LM.getText(LC.HM_PRESCRIPTION, loginDTO)%>'],
            //[dashboardDTO.lastSevenDaysDocumentsCountDTO.last7thDayDate,  dashboardDTO.lastSevenDaysDocumentsCountDTO.last7thDayCount],
            <%
           for(int j = 0; j < dashboardDTO.doctorNames.size(); j ++)
           {
               %>
            ['<%=Utils.getDigits(dashboardDTO.doctorNames.get(j), Language)%>', <%=dashboardDTO.doctorWiseAppointments.get(j)%>, <%=dashboardDTO.doctorWiseServedPatients.get(j)%>],
            <%
            }
            %>

        ]);

        var options = {
            title: '<%=LM.getText(LC.HM_DOCTORWISE_SERVED_PATIENTS, loginDTO)%>',
            //curveType: 'function',
            legend: {position: 'left'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('div3'));

        chart.draw(data, options);
    }

    google.charts.setOnLoadCallback(drawDrugChart);

    function drawDrugChart() {


        var data = google.visualization.arrayToDataTable([
            ['Doctor', 'Patients'],
            //[dashboardDTO.lastSevenDaysDocumentsCountDTO.last7thDayDate,  dashboardDTO.lastSevenDaysDocumentsCountDTO.last7thDayCount],
            <%
           for(int j = 0; j < dashboardDTO.popularDrugNames.length; j ++)
           {
               %>
            ['<%=dashboardDTO.popularDrugNames[j]%>', <%=dashboardDTO.popularDrugCount[j]%>],
            <%
            }
            %>

        ]);

        var options = {
            title: '<%=LM.getText(LC.HM_MOST_USED_DRUGS, loginDTO)%>',
            //curveType: 'function',
            legend: {position: 'left'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('div4'));

        chart.draw(data, options);
    }
    
    google.charts.setOnLoadCallback(drawReceptionistsToday);

    function drawReceptionistsToday() {


        var data = google.visualization.arrayToDataTable([
        	
            <%
            if(dashboardDTO.receptionistWiseCountToday != null)
            {
            	%>
             	['<%=Language.equalsIgnoreCase("english")?"Receptionist":"রিসেপশনিস্ট"%>',
             		'<%=LM.getText(LC.HM_COUNT, loginDTO)%>'],
             <%
	           for(KeyCountDTO keyCountDTO: dashboardDTO.receptionistWiseCountToday)
	           {
	        	   
	                %>
	            		['<%=keyCountDTO.exists?"*":""%> <%=WorkflowController.getNameFromUserName(keyCountDTO.keyStr, Language)%>',
	            			<%=keyCountDTO.count%>],
	            	<%

	        	}
            }
        	%>

        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Receptionistwise Appointments Today":"আজকের রিসেপশনিস্টভিত্তিক অ্যাপয়েন্টমেন্ট সংখ্যা"%>',
            //curveType: 'function',
            legend: {position: 'left'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('divRec'));
        var chartContainer = document.getElementById('divRec');
        google.visualization.events.addListener(chart, 'ready', function () {
            var labels = chartContainer.getElementsByTagName('text');
            
            for (let i = 0; i < labels.length; i++) {
          	  if(labels[i].innerHTML.startsWith("*"))
         	  {
          		  labels[i].setAttribute('font-weight', 'Bold');
         	  }
          	}
          
          
          });

        chart.draw(data, options);
    }
    

    
    google.charts.setOnLoadCallback(drawReceptionists);

    function drawReceptionists() {


        var data = google.visualization.arrayToDataTable([
        	
            <%
            if(dashboardDTO.receptionistWiseCount != null)
            {
            	%>
             	['<%=Language.equalsIgnoreCase("english")?"Receptionist":"রিসেপশনিস্ট"%>',
             		'<%=LM.getText(LC.HM_COUNT, loginDTO)%>'],
             <%
	           for(KeyCountDTO keyCountDTO: dashboardDTO.receptionistWiseCount)
	           {
	        	   
	                %>
	            		['<%=WorkflowController.getNameFromUserName(keyCountDTO.keyStr, Language)%>',
	            			<%=keyCountDTO.count%>],
	            	<%

	        	}
            }
        %>

        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Receptionistwise Appointments":"রিসেপশনিস্টভিত্তিক অ্যাপয়েন্টমেন্ট সংখ্যা"%>',
            //curveType: 'function',
            legend: {position: 'left'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('div6'));

        chart.draw(data, options);
    }
    
    google.charts.setOnLoadCallback(drawOrgCost);

    function drawOrgCost() {


        var data = google.visualization.arrayToDataTable([
        	
            <%
            if(dashboardDTO.organizationWizeCountCost != null)
            {
            	%>
             	['<%=Language.equalsIgnoreCase("english")?"Organization":"সংগঠন"%>',
             		'<%=LM.getText(LC.HM_COUNT, loginDTO)%>',
             		'<%=LM.getText(LC.HM_COST, loginDTO)%>'],
             <%
             //dashboardDTO.organizationWizeCountCost.get(2).cost = 10000;
	           for(KeyCountDTO keyCountDTO: dashboardDTO.organizationWizeCountCost)
	           {
	        	   if(Language.equalsIgnoreCase("English"))
	        	   {
	        	   
	                %>
	            		['<%=keyCountDTO.nameEn%>', <%=keyCountDTO.count%>, <%=keyCountDTO.cost%>],
	            	<%
	        	   }
	        	   else
	        	   {
	        		   %>
	            		['<%=keyCountDTO.nameBn%>', <%=keyCountDTO.count%>, <%=keyCountDTO.cost%>],
	            	<%
	        	   }
	        	   
	        	}
            }
        %>

        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Organizationwise Medicine Count and Cost":"সংগঠনভিত্তিক ঔষধের সংখ্যা ও মূল্য"%>',
            //curveType: 'function',
            legend: {position: 'left'}
        };
        
        function selectHandler() {
            var selectedItem = chart.getSelection()[0];
            if (selectedItem) {
              	var topping = data.getValue(selectedItem.row, 0);
              
              	console.log('The user selected ' + topping);
              	if(topping == "OTHER" || topping == "অন্যান্য")
              	{
              		window.location ='Cum_report_Servlet?actionType=reportPage';
              	}
            }
          }

        var chart = new google.visualization.ColumnChart(document.getElementById('div7'));
        google.visualization.events.addListener(chart, 'select', selectHandler); 
        chart.draw(data, options);
    }
    
    google.charts.setOnLoadCallback(dragoholic);

    function dragoholic() {


        var data = google.visualization.arrayToDataTable([
        	['<%=Language.equalsIgnoreCase("english")?"Medicine Cost":"ঔষধের মূল্য"%>', '<%=LM.getText(LC.HM_COUNT, loginDTO)%>'],
            <%
            if(dashboardDTO.drugohololics != null)
            {
	           for(KeyCountDTO keyCountDTO: dashboardDTO.drugohololics)
	           {
	        	   
	                %>
	            		['<%=WorkflowController.getNameFromUserName(keyCountDTO.keyStr, Language)%>', <%=keyCountDTO.cost%>],
	            	<%
	        	  
	        	   
	        	}
            }
        %>

        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Top Medicine Users":"সর্বোচ্চ ঔষধ ব্যবহারকারীরা"%>',
            //curveType: 'function',
            legend: {position: 'left'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('div5'));

        chart.draw(data, options);
    }
    
    google.charts.setOnLoadCallback(drawCUMR);

    function drawCUMR() {


        var data = google.visualization.arrayToDataTable([
        	['<%=Language.equalsIgnoreCase("english")?"Office":"অফিস"%>', '<%=Language.equalsIgnoreCase("english")?"Visit Count (Multiplied by 100)":"ডাক্তার দেখানোর সংখ্যা (১০০ দিয়ে গুণ করা)"%>', '<%=Language.equalsIgnoreCase("english")?"Medicine Cost":"ঔষধের মূল্য"%>'],
            <%
            if(dashboardDTO.otherOfficeCountCost != null)
            {
	           for(KeyCountDTO keyCountDTO: dashboardDTO.otherOfficeCountCost)
	           {
					String officeName = CommonDAO.getName(Language, "other_office", keyCountDTO.key);
					if(!officeName.equalsIgnoreCase(""))
					{
					
	        	   
	                %>
	            		['<%=officeName%>', <%=keyCountDTO.count * 100%>, <%=keyCountDTO.cost%>],
	            	<%
					}
	        	   
	        	}
            }
        %>

        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Other Office Users":"অন্যান্য অফিসের ইউজাররা"%>',
            //curveType: 'function',
            legend: {position: 'bottom'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('div8'));

        chart.draw(data, options);
    }
    
    google.charts.setOnLoadCallback(drawSR);

    function drawSR() {


        var data = google.visualization.arrayToDataTable([
        	['<%=Language.equalsIgnoreCase("english")?"Office":"অফিস"%>', '<%=Language.equalsIgnoreCase("english")?"Medicine Cost":"ঔষধের মূল্য"%>'],
            <%
            if(dashboardDTO.specialCostCollections != null)
            {
	           for(KeyCountDTO keyCountDTO: dashboardDTO.specialCostCollections)
	           {
					if(Language.equalsIgnoreCase("english"))
					{
	                %>
	            		['<%=keyCountDTO.nameEn%>',  <%=keyCountDTO.cost%>],
	            	<%
					}
					else
					{
	                %>
	            		['<%=keyCountDTO.nameBn%>',  <%=keyCountDTO.cost%>],
	            	<%
					}
	        	   
	        	}
            }
        %>

        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Cost Summary":"খরচের সারসংক্ষেপ"%>',
            //curveType: 'function',
            legend: {position: 'bottom'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('div9'));

        chart.draw(data, options);
    }
    
    
    google.charts.setOnLoadCallback(drawDentalAll);

    function drawDentalAll() {


        var data = google.visualization.arrayToDataTable([
        	['<%=LM.getText(LC.HM_DENTAL_ACTION, loginDTO)%>', '<%=LM.getText(LC.HM_COUNT, loginDTO)%>'],
            <%
            if(dashboardDTO.dentalForever != null)
            {
	           for(KeyCountDTO keyCountDTO: dashboardDTO.dentalForever)
	           {
	        	   
	                %>
	            		['<%=CatDAO.getName(Language, "dental_action", keyCountDTO.key)%>', <%=keyCountDTO.count%>],
	            	<%
	        	}
            }
        %>

        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Dental Activities (All time)":"দন্ত বিষয়ক কার্যক্রম (সব সময়ের)"%>',
            //curveType: 'function',
            legend: {position: 'left'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('div10'));

        chart.draw(data, options);
    }
    
    google.charts.setOnLoadCallback(drawDentalToday);

    function drawDentalToday() {


        var data = google.visualization.arrayToDataTable([
        	['<%=LM.getText(LC.HM_DENTAL_ACTION, loginDTO)%>', '<%=LM.getText(LC.HM_COUNT, loginDTO)%>'],
            <%
            if(dashboardDTO.dentalDay != null)
            {
	           for(KeyCountDTO keyCountDTO: dashboardDTO.dentalDay)
	           {
	        	   
	                %>
	            		['<%=CatDAO.getName(Language, "dental_action", keyCountDTO.key)%>', <%=keyCountDTO.count%>],
	            	<%
	        	}
            }
        %>

        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Dental Activities (Today)":"দন্ত বিষয়ক কার্যক্রম (আজকের)"%>',
            //curveType: 'function',
            legend: {position: 'left'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('div11'));

        chart.draw(data, options);
    }
    

</script>