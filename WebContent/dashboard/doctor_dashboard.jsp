<%@ page import="dashboard.DashboardDTO" %>
<%@ page import="java.util.List" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="pb.*" %>
<%@ page import="appointment.*" %>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String context = "../../.." + request.getContextPath() + "/";

    DashboardDTO dashboardDTO = (DashboardDTO) request.getAttribute("dashboardDTO");
    String Language = LM.getText(LC.SUPPORT_TICKET_EDIT_LANGUAGE, loginDTO);

%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.HM_DOCTORS_DASHBOARD, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body bg-light">
            <div class="row">
                <div class="col-lg-6">
                    <a href="Prescription_detailsServlet?actionType=search&filter=patientHanDledInCurrentMonth">
                        <div class="shadow py-4 dashboard-card-border-radius"
                             style="background: linear-gradient(to right, #9861C2, #6132B2);">
                            <div class="d-flex justify-content-between align-items-center px-4">
                                <span class="h3 text-white">
                                    <%=LM.getText(LC.HM_PATIENTS_HANDLED_IN_CURRENT_MONTH, loginDTO)%>
                                </span>
                                <span class="patient_count text-white">
                                    <%=Utils.getDigits(dashboardDTO.patientHanDledInCurrentMonth + "", Language)%>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-6 mt-4 mt-lg-0">
                    <a href="AppointmentServlet?actionType=search&filter=getUpcomingAppointments">
                        <div class="shadow py-4 dashboard-card-border-radius"
                             style="background: linear-gradient(to right, #EE9048, #EE9048);">
                            <div class="d-flex justify-content-between align-items-center px-4">
                                <span class="h3 text-white">
                                    <%=LM.getText(LC.HM_UPCOMING_APPOINTMENTS, loginDTO)%>
                                </span>
                                <span class="patient_count text-white" style="background-color: #FCB42E;">
                                    <%=Utils.getDigits(dashboardDTO.upComingAppointments + "", Language)%>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-6 mt-4">
                    <div class="shadow overflow-hidden px-3 py-5 dashboard-card-border-radius"
                         style="background-color: #FFFFFF;">
                        <h3 class="table-title mb-4 ml-3">
                            <%=LM.getText(LC.HM_DAYWISE_NUMBER_OF_PATIENTS, loginDTO)%>
                        </h3>
                        <div id="patientDiv"></div>
                    </div>
                </div>
                <div class="col-lg-6 mt-4">
                    <div class="shadow overflow-hidden px-3 py-5 dashboard-card-border-radius"
                         style="background-color: #FFFFFF;">
                        <h3 class="table-title mb-4 ml-3">
                            <%=LM.getText(LC.HM_DIAGNOSTIC_TYPES, loginDTO)%>
                        </h3>
                        <div id="testDiv"></div>
                    </div>
                </div>
                
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden"
                         >
                        <div>
                            <div id="div10" style="height: 500px;"></div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden"
                         >
                        <div>
                            <div id="div11" style="height: 500px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    var context = '${context}';
    var pluginsContext = '${pluginsContext}';

    var dashboardDTOJson = '<%=new Gson().toJson( request.getAttribute( "dashboardDTO"  ) )%>';
    var dashboardDTO = JSON.parse(dashboardDTOJson);


    //var isAdmin = request.getAttribute("isAdmin");
</script>

<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

	google.charts.load('current', {'packages': ['corechart']});

	
	google.charts.setOnLoadCallback(drawLabChart);
	
	function drawLabChart() {
	
	
	    var data = google.visualization.arrayToDataTable([
	        ['<%=LM.getText(LC.HM_DIAGNOSTIC_TYPES, loginDTO)%>', 'Patients'],
	        <%
	       for(int j = 0; j < dashboardDTO.labTestTypes.length; j ++)
	       {
	    	   System.out.println("type = " + dashboardDTO.labTestTypes[j]);
	           %>
	        ['<%=Utils.getDigits(dashboardDTO.labTestTypes[j], Language)%>', <%=dashboardDTO.typeWiseDiagnosticProcedure[j]%>],
	        <%
	        }
	        %>
	
	    ]);
	
	    var options = {
	        title: '<%=LM.getText(LC.HM_DIAGNOSTIC_TYPES, loginDTO)%>',
	        //curveType: 'function',
	        legend: {position: 'left'}
	    };
	
	    var chart = new google.visualization.PieChart(document.getElementById('testDiv'));
	
	    chart.draw(data, options);
	}



    google.charts.setOnLoadCallback(drawChartPatientDays);

    function drawChartPatientDays() {


        var data = google.visualization.arrayToDataTable([
            ['<%=LM.getText(LC.HM_DAY, loginDTO)%>', '<%=LM.getText(LC.HM_APPOINTMENTS, loginDTO)%>', '<%=LM.getText(LC.HM_PATIENTS, loginDTO)%>'],
            //[dashboardDTO.lastSevenDaysDocumentsCountDTO.last7thDayDate,  dashboardDTO.lastSevenDaysDocumentsCountDTO.last7thDayCount],
            <%
           for(int j = 0; j <= 7; j ++)
           {
               %>
            ['<%=Utils.getDigits(dashboardDTO.last7Days[j], Language)%>', <%=dashboardDTO.last7DayAppointments[j]%>, <%=dashboardDTO.last7DayPatients[j]%>],
            <%
        	}
        %>

        ]);

        var options = {
            title: '<%=LM.getText(LC.HM_PATIENTS_HANDLED_IN_LAST_7_DAYS, loginDTO)%>',
            //curveType: 'function',
            legend: {position: 'top'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('patientDiv'));

        chart.draw(data, options);
    }
    
    
    google.charts.setOnLoadCallback(drawDentalAll);

    function drawDentalAll() {


        var data = google.visualization.arrayToDataTable([
        	['<%=LM.getText(LC.HM_DENTAL_ACTION, loginDTO)%>', '<%=LM.getText(LC.HM_COUNT, loginDTO)%>'],
            <%
            if(dashboardDTO.dentalForever != null)
            {
	           for(KeyCountDTO keyCountDTO: dashboardDTO.dentalForever)
	           {
	        	   
	                %>
	            		['<%=CatDAO.getName(Language, "dental_action", keyCountDTO.key)%>', <%=keyCountDTO.count%>],
	            	<%
	        	}
            }
        %>

        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Dental Activities (All time)":"দন্ত বিষয়ক কার্যক্রম (সব সময়ের)"%>',
            //curveType: 'function',
            legend: {position: 'left'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('div10'));

        chart.draw(data, options);
    }
    
    google.charts.setOnLoadCallback(drawDentalToday);

    function drawDentalToday() {


        var data = google.visualization.arrayToDataTable([
        	['<%=LM.getText(LC.HM_DENTAL_ACTION, loginDTO)%>', '<%=LM.getText(LC.HM_COUNT, loginDTO)%>'],
            <%
            if(dashboardDTO.dentalDay != null)
            {
	           for(KeyCountDTO keyCountDTO: dashboardDTO.dentalDay)
	           {
	        	   
	                %>
	            		['<%=CatDAO.getName(Language, "dental_action", keyCountDTO.key)%>', <%=keyCountDTO.count%>],
	            	<%
	        	}
            }
        %>

        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Dental Activities (Today)":"দন্ত বিষয়ক কার্যক্রম (আজকের)"%>',
            //curveType: 'function',
            legend: {position: 'left'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('div11'));

        chart.draw(data, options);
    }
    
</script>