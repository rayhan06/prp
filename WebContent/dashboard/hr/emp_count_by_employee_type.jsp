<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="kt-portlet__body" style="background-color: #F2F2F2">
    <div id="pie_chart_emp_type" style="height: 300px;"></div>
</div>

<script type="text/javascript">
    let empCountByTypeFetchedData = [];
    function getEmpCountByType(){
        const officeUnitId = $("#office_units_id_input").val();
        let url = "Employee_recordsServlet?actionType=ajax_emp_type_count&officeUnitId=" + officeUnitId + "&language=<%=language%>";
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                drawEmpTypeCount(fetchedData);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function drawEmpTypeCount(fetchedData){
        empCountByTypeFetchedData = fetchedData;
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawEmpTypeCountChart);
    }

    function drawEmpTypeCountChart() {
        const data = new google.visualization.DataTable();
        data.addColumn('string', 'Employee Type');
        data.addColumn('number', '<%=isLangEng?"Count":"সংখ্যা"%>');
        const LINK_INDEX = 3;
        data.addColumn({ role: 'style' });
        data.addColumn('string', 'Link');

        if(empCountByTypeFetchedData){
            for(let key in empCountByTypeFetchedData){
                if(key == <%=Integer.MIN_VALUE%>){
                    data.addRows([[empCountByTypeFetchedData[key].title, empCountByTypeFetchedData[key].count,'color: crimson', empCountByTypeFetchedData[key].link]]);
                }else{
                    data.addRows([
                        [empCountByTypeFetchedData[key].title, empCountByTypeFetchedData[key].count,'color: #006666', empCountByTypeFetchedData[key].link]
                    ]);
                }
            }
        }
        const view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            { calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation" },2
        ]);
        const options = {
            backgroundColor:'#F2F2F2',
            title: '<%=LM.getText(LC.HR_ADMIN_DASHBOARD_EMPLOYEE_TYPE_WISE_COUNT, loginDTO)%>',
            legend: { position: "none" }
        };
        const chart = new google.visualization.ColumnChart(document.getElementById('pie_chart_emp_type'));

        function selectHandler() {
            const selectedItem = chart.getSelection()[0];
            if (selectedItem && data.getValue(selectedItem.row, LINK_INDEX)) {
                location.href = data.getValue(selectedItem.row, LINK_INDEX);
            }
        }

        google.visualization.events.addListener(chart, 'select', selectHandler);

        chart.draw(view, options);
    }
</script>