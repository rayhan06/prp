<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="kt-portlet__body" style="background-color: #F2F2F2">
    <div id="pie_chart_emp_age_count_by_office_unit_id" style="height: 300px;"></div>
</div>

<script type="text/javascript">
    let empAgeCountFetchedData;
    function changeOfficeUnitForAgeCount(){
        const officeUnitId = $("#office_unit_for_age").val();
        if(officeUnitId){
            let url = "Office_unitsServlet?actionType=ajax_age_count&office_units_id=" + officeUnitId;
            console.log("url : " + url);
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    empAgeCountFetchedData = fetchedData;
                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawAgeCountChart);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }

    function drawAgeCountChart() {
        const data = new google.visualization.DataTable();
        data.addColumn('string', 'Age');
        data.addColumn('number', '<%=isLangEng?"Count":"সংখ্যা"%>');
        if(empAgeCountFetchedData){
            for(let i in empAgeCountFetchedData){
                data.addRows([[String(i),empAgeCountFetchedData[i]]]);
            }
        }
        const options = {
            backgroundColor:'#F2F2F2',
            title: 'Employee Count by age range'
        };
        const chart = new google.visualization.PieChart(document.getElementById('pie_chart_emp_age_count_by_office_unit_id'));
        chart.draw(data, options);
    }
</script>