<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="kt-portlet__body" style="background-color: #F2F2F2">
    <div id="bar_chart_wing" style="height: 300px;"></div>
</div>

<script type="text/javascript">
let wingWiseCount;

function drawWingWise(fetchedData)
{
	wingWiseCount = fetchedData;
	console.log("drawWingWise called");

	
	
	 google.charts.load('current', {'packages':['corechart']});
     google.charts.setOnLoadCallback(drawWingWiseReally);
}

var wingChart;
var wingData;
var wingLinks;
var isLangEng = <%=isLangEng%>;

function drawWingWiseReally() {

	var dataArray = [];
	wingLinks = [];
	dataArray[0] = [];    	
	wingLinks[0] = [];
	dataArray[0].push('<%=LM.getText(LC.HM_WING, loginDTO)%>');
	dataArray[0].push('<%=isLangEng?"Total Posts":"মোট পদ"%>');
	dataArray[0].push('<%=isLangEng?"Vacancies":"শূন্যপদ"%>');
	dataArray[0].push('<%=isLangEng?"Assigned Posts":"নিযুক্ত পদ"%>');
	
	var index = 0;
	for (let key in wingWiseCount) 
	{
		console.log("id = " + key + " nameEn = " + wingWiseCount[key].nameEn + " count = " + wingWiseCount[key].count);
		index ++;
		dataArray[index] = new Array(4);
		wingLinks[index] = new Array(4);
		if(isLangEng)
		{
			dataArray[index][0] = wingWiseCount[key].nameEn;
		}
		else
		{
			dataArray[index][0] = wingWiseCount[key].nameBn;
		}
		dataArray[index][1] = wingWiseCount[key].count;
		dataArray[index][2] = wingWiseCount[key].count2;
		dataArray[index][3] = wingWiseCount[key].count3;
		
		var winkLink = "Vacancy_report_Servlet?actionType=reportPage&officeToSet=" + wingWiseCount[key].key + "&name=";
		if(isLangEng)
		{
			winkLink += wingWiseCount[key].nameEn;
		}
		else
		{
			winkLink += wingWiseCount[key].nameBn;
		}
		wingLinks[index][1] = wingLinks[index][2] = wingLinks[index][3] = winkLink;
       
    }
	
	for (var i = 0; i < dataArray.length; i++) 
	{
		console.log(dataArray[i]);
	}
	

	wingData = google.visualization.arrayToDataTable(dataArray);

    var options = {
        backgroundColor:'#F2F2F2',
        title: '<%=isLangEng?"Wingwise Post Count":"শাখাভিত্তিক পদসংখ্যা"%>',
        //curveType: 'function',
        legend: {position: 'right'},
        allowHtml: true
    };

    wingChart = new google.visualization.ColumnChart(document.getElementById('bar_chart_wing'));
    google.visualization.events.addListener(wingChart, 'select', wingSelectHandler);
    wingChart.draw(wingData, options);
}

function wingSelectHandler() {
	var selection = wingChart.getSelection();
	var row = selection[0].row;
	console.log("row = " + row);
	var column = selection[0].column;
	var url =  wingLinks[row + 1][column];
	console.log('You selected ' + url);
	window.location = url;
}
</script>