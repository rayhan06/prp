<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="kt-portlet__body" style="background-color: #F2F2F2">
    <div id="pie_chart_emp_division" style="height: 300px;"></div>
</div>

<script type="text/javascript">
    let empCountByDivisionFetchedData;

    function drawEmpDivision(fetchedData){
        empCountByDivisionFetchedData = fetchedData;
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawEmpDivisionCountChart);
    }

    function drawEmpDivisionCountChart() {
        const data = new google.visualization.DataTable();
        data.addColumn('string', 'Employee Division');
        data.addColumn('number', '<%=isLangEng?"Count":"সংখ্যা"%>');
        const LINK_INDEX = 2;
        data.addColumn('string', 'Link');

        if(empCountByDivisionFetchedData){
            for(let key in empCountByDivisionFetchedData){
                data.addRows([
                    [empCountByDivisionFetchedData[key].title, empCountByDivisionFetchedData[key].count, empCountByDivisionFetchedData[key].link]
                ]);
            }
        }
        
        const view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            { calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation" }
        ]);
        const options = {
            backgroundColor:'#F2F2F2',
            colors: ['#109618', '#3366cc','#006666','#ff9900',"#800000",'#00cc99','#0033cc','#666633'],
            title: '<%=isLangEng?"Employee Division wise Count":"বিভাগের ভিত্তিতে কর্মকর্তা/কর্মচারি সংখ্যা"%>',
            is3D: true,
        };
        const chart = new google.visualization.PieChart(document.getElementById('pie_chart_emp_division'));

        function selectHandler() {
            const selectedItem = chart.getSelection()[0];
            if (selectedItem && data.getValue(selectedItem.row, LINK_INDEX)) {
                location.href = data.getValue(selectedItem.row, LINK_INDEX);
            }
        }

        google.visualization.events.addListener(chart, 'select', selectHandler);

        chart.draw(view, options);
    }
</script>
