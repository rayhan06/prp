<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="kt-portlet__body" style="background-color: #F2F2F2">
    <div id="pie_chart_emp_gender_count_by_office_unit_id" style="height: 300px;"></div>
</div>

<script type="text/javascript">
    let empGenderCountFetchedData;

    function changeOfficeUnitForEmpGenderCount() {
        const officeUnitId = $("#office_units_id_input").val();
        if (officeUnitId) {
            let url = "Office_unitsServlet?actionType=ajax_gender_count&office_units_id=" + officeUnitId + "&language=<%=language%>";
            console.log("url : " + url);
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    drawGender(fetchedData);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }

    function drawGender(fetchedData){
        empGenderCountFetchedData = fetchedData;
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawEmpGenderCountChart);
    }

    function drawEmpGenderCountChart() {
        const data = new google.visualization.DataTable();
        data.addColumn('string', 'Designation');
        data.addColumn('number', '<%=isLangEng?"Count":"সংখ্যা"%>');
        const LINK_INDEX = 2;
        data.addColumn('string', 'Link');

        if (empGenderCountFetchedData) {
            for (let key in empGenderCountFetchedData) {
                data.addRows([
                    [empGenderCountFetchedData[key].title, empGenderCountFetchedData[key].count, empGenderCountFetchedData[key].link]
                ]);
            }
        }

        const view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            {
                calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation"
            }
        ]);
        const options = {
            backgroundColor:'#F2F2F2',
            colors: ['#109618', '#006666', 'crimson'],
            title: '<%=LM.getText(LC.HR_ADMIN_DASHBOARD_EMPLOYEE_GENDER_WISE_COUNT, loginDTO)%>',
            is3D: true,
        };
        const chart = new google.visualization.PieChart(document.getElementById('pie_chart_emp_gender_count_by_office_unit_id'));

        function selectHandler() {
            const selectedItem = chart.getSelection()[0];
            if (selectedItem && data.getValue(selectedItem.row, LINK_INDEX)) {
                location.href = data.getValue(selectedItem.row, LINK_INDEX);
            }
        }

        google.visualization.events.addListener(chart, 'select', selectHandler);

        chart.draw(view, options);
    }
</script>