<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="kt-portlet__body" style="background-color: #F2F2F2">
    <div id="bar_chart_joining" style="height: 300px;"></div>
</div>

<script type="text/javascript">
    let joining6Months;
    function getJoining6Months(){
    	console.log("getJoining6Months called");
        let url = "Employee_recordsServlet?actionType=ajax_get_6moth_joining";
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
            	drawJoining6Months(fetchedData);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function drawJoining6Months(fetchedData){
    	console.log("drawJoining6Months called");
    	joining6Months = fetchedData;
    	/*for (var i = 0; i < joining6Months.length; i++) 
    	{
    		console.log(joining6Months[i].keyStr + " " + joining6Months[i].keyStr2 + " " + 
    				joining6Months[i].monthName + " " +
    				joining6Months[i].key + " " + joining6Months[i].count);
    	}*/
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawJoining6MonthsReally);
        
    }
    
    var joiningChart;
    var joiningData;
    var links;
    
    function drawJoining6MonthsReally() {

    	var dataArray = [];
    	links = [];
    	dataArray[0] = [];
    	links[0] = [];
    	
    	dataArray[0].push('<%=LM.getText(LC.HM_MONTH, loginDTO)%>');
    	//dataArray[0].push('');
    	if(<%=isLangEng%>)
   		{
    		for (var i = 0; i < employeeTypes.length; i++) 
        	{
    			dataArray[0].push(employeeTypes[i].nameEn + "");
        	}
   		}
    	else
    	{
    		for (var i = 0; i < employeeTypes.length; i++) 
        	{
    			dataArray[0].push(employeeTypes[i].nameBn + "");
        	}
    		
    	}
    	
    	
    	
    	var keyStr = "";
    	var index = 0;
    	var empIndex = 0;
    	for (var i = 0; i < joining6Months.length; i++) 
    	{
    		if(joining6Months[i].keyStr != keyStr)
    		{
    			keyStr = joining6Months[i].keyStr;
    			index ++;
    			dataArray[index] = new Array(employeeTypes.length + 1);
    			links[index] = new Array(employeeTypes.length + 1);
    			dataArray[index][0] = joining6Months[i].monthName;
    			links[index][0] = joining6Months[i].monthName;
    			for (var j = 1; j < dataArray[index].length ; j++)
   				{
    				dataArray[index][j] = 0;
    				links[index][j] = '';
   				}
    		}
    		
    		if(joining6Months[i].key > 0)
   			{
    			dataArray[index][joining6Months[i].key] = joining6Months[i].count;
        		links[index][joining6Months[i].key] = "Employee_info_report_Servlet?actionType=reportPage"
        				+"&startJoiningDate=" + joining6Months[i].startDate
        				+"&endJoiningDate=" + joining6Months[i].endDate
        				+"&employmentCat=" + joining6Months[i].key;
   			}
    		

    		
    	}
    	
    	/*for (var i = 0; i < dataArray.length; i++) 
    	{
    		console.log(dataArray[i]);
    	}*/
    	

    	joiningData = google.visualization.arrayToDataTable(dataArray);

        var options = {
            backgroundColor:'#F2F2F2',
            title: '<%=isLangEng?"Joining in last 6 months":"গত ৬ মাসে যোগদানের সংখ্যা"%>',
            //curveType: 'function',
            legend: {position: 'top'},
            allowHtml: true
        };

        joiningChart = new google.visualization.ColumnChart(document.getElementById('bar_chart_joining'));
        google.visualization.events.addListener(joiningChart, 'select', joinSelectHandler); 
        joiningChart.draw(joiningData, options);
    }
    
    function joinSelectHandler() {
    	var selection = joiningChart.getSelection();
    	var row = selection[0].row;
    	console.log("row = " + row);
    	var column = selection[0].column;
    	var url =  links[row + 1][column];
    	console.log('You selected ' + url);
    	window.location = url;
    }


</script>