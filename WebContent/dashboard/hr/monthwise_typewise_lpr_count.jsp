<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="kt-portlet__body" style="background-color: #F2F2F2">
    <div id="bar_chart_lpr" style="height: 300px;"></div>
</div>

<script type="text/javascript">
    let lpr6Months;
    function getLpr6Months(){
    	console.log("getLpr6Months called");
        let url = "Employee_recordsServlet?actionType=ajax_get_6moth_lpr";
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
            	drawLpr6Months(fetchedData);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function drawLpr6Months(fetchedData){
    	console.log("drawLpr6Months called");
    	lpr6Months = fetchedData;
    	/*for (var i = 0; i < lpr6Months.length; i++) 
    	{
    		console.log(lpr6Months[i].keyStr + " " + 
    				lpr6Months[i].monthName + " " +
    				lpr6Months[i].key + " " + lpr6Months[i].count);
    	}*/
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawLpr6MonthsReally);
        
    }
    
    var lprChart;
    var lprData;
    var lprLinks;
    
    function drawLpr6MonthsReally() {

    	var dataArray = [];
    	lprLinks = [];
    	dataArray[0] = [];    	
    	lprLinks[0] = [];
    	dataArray[0].push('<%=LM.getText(LC.HM_MONTH, loginDTO)%>');
    	dataArray[0].push('<%=LM.getText(LC.HM_COUNT, loginDTO)%>');
    	
    	var keyStr = "";
    	var index = 0;
    	var empIndex = 0;
    	for (var i = 0; i < lpr6Months.length; i++) 
    	{
    		if(lpr6Months[i].keyStr != keyStr)
    		{
    			keyStr = lpr6Months[i].keyStr;
    			index ++;
    			dataArray[index] = new Array(2);
    			lprLinks[index] = new Array(2);
    			dataArray[index][0] = lpr6Months[i].monthName;
    			
    		}
    		
    		dataArray[index][1] = lpr6Months[i].count;
    		lprLinks[index][1] = "Retirement_details_report_Servlet?actionType=reportPage"
				+"&startDate=" + lpr6Months[i].startDate
				+"&endDate=" + lpr6Months[i].endDate;
    		
    	}
    	
    	/*for (var i = 0; i < dataArray.length; i++) 
    	{
    		console.log(dataArray[i]);
    	}*/
    	

    	lprData = google.visualization.arrayToDataTable(dataArray);

        var options = {
            backgroundColor:'#F2F2F2',
            title: '<%=isLangEng?"LPR in next 6 months":"আগামী ৬ মাসে এল পি আরের সংখ্যা"%>',
            //curveType: 'function',
            legend: {position: 'top'},
            allowHtml: true
        };

        lprChart = new google.visualization.ColumnChart(document.getElementById('bar_chart_lpr'));
        google.visualization.events.addListener(lprChart, 'select', lprSelectHandler);
        lprChart.draw(lprData, options);
    }
    
    function lprSelectHandler() {
    	var selection = lprChart.getSelection();
    	var row = selection[0].row;
    	console.log("row = " + row);
    	var column = selection[0].column;
    	var url =  lprLinks[row + 1][column];
    	console.log('You selected ' + url);
    	window.location = url;
    }


</script>