<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="kt-portlet__body" style="background-color: #F2F2F2">
    <div id="pie_chart_emp_class" style="height: 300px;"></div>
</div>

<script type="text/javascript">
    let empCountByClassFetchedData;
    function getEmpCountByClass(){
        const officeUnitId = $("#office_units_id_input").val();
        let url = "Employee_recordsServlet?actionType=ajax_emp_class_count&officeUnitId=" + officeUnitId + "&language=<%=language%>";
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                drawEmpClass(fetchedData);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
    function drawEmpClass(fetchedData){
        empCountByClassFetchedData = fetchedData;
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawEmpClassCountChart);
    }

    function drawEmpClassCountChart() {
        const data = new google.visualization.DataTable();
        data.addColumn('string', 'Employee Class');
        data.addColumn('number', '<%=isLangEng?"Count":"সংখ্যা"%>');
        const LINK_INDEX = 2;
        data.addColumn('string', 'Link');

        if(empCountByClassFetchedData){
            for(let key in empCountByClassFetchedData){
                data.addRows([
                    [empCountByClassFetchedData[key].title, empCountByClassFetchedData[key].count, empCountByClassFetchedData[key].link]
                ]);
            }
        }
        
        const view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            { calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation" }
        ]);
        const options = {
            backgroundColor:'#F2F2F2',
            colors: ['#109618', '#3366cc','#006666','#ff9900',"#800000",'crimson'],
            title: '<%=LM.getText(LC.HR_ADMIN_DASHBOARD_EMPLOYEE_CLASS_WISE_COUNT, loginDTO)%>',
            is3D: true,
        };
        const chart = new google.visualization.PieChart(document.getElementById('pie_chart_emp_class'));

        function selectHandler() {
            const selectedItem = chart.getSelection()[0];
            if (selectedItem && data.getValue(selectedItem.row, LINK_INDEX)) {
                location.href = data.getValue(selectedItem.row, LINK_INDEX);
            }
        }

        google.visualization.events.addListener(chart, 'select', selectHandler);

        chart.draw(view, options);
    }
</script>
