<%@page import="ticket_issues.Ticket_issuesRepository"%>
<%@ page import="dashboard.DashboardDTO" %>
<%@ page import="java.util.*" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="user.*" %>
<%@page import="workflow.*" %>
<%@page import="ticket_issues.*" %>
<%@ page import="com.google.gson.Gson" %>
<%@page pageEncoding="UTF-8" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String context = "../../.." + request.getContextPath() + "/";
    DashboardDTO dashboardDTO = (DashboardDTO) request.getAttribute("dashboardDTO");
    String Language = LM.getText(LC.SUPPORT_TICKET_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);    
    long userOffice = WorkflowController.getOfficeIdFromOrganogramId(userDTO.organogramID);
    List<Ticket_issuesDTO> ticketIssues = Ticket_issuesRepository.getInstance().getTicket_issuesList();
    List<Long> userTicketTypes = new ArrayList<Long>();
    for(Ticket_issuesDTO ticket_issuesDTO: ticketIssues)
    {
    	long ticketOffice = WorkflowController.getOfficeIdFromOrganogramId(ticket_issuesDTO.adminOrganogramId);
    	if(userDTO.roleID == SessionConstants.ADMIN_ROLE)
    	{
    		userTicketTypes.add(ticket_issuesDTO.iD);
    	}
    	else if(userOffice == SessionConstants.MAINTENANCE1_OFFICE || userOffice == SessionConstants.MAINTENANCE2_OFFICE)
    	{
    		if(ticketOffice == SessionConstants.MAINTENANCE1_OFFICE || ticketOffice == SessionConstants.MAINTENANCE2_OFFICE)
    		{
    			userTicketTypes.add(ticket_issuesDTO.iD);
    		}
    	}
    	else if(userOffice == ticketOffice)
    	{
    		userTicketTypes.add(ticket_issuesDTO.iD);
    	}
    }
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.HM_TICKET_DASHBOARD, loginDTO)%>
                </h3>
            </div>
            <button type="button" type="button" class="btn-sm border-0 shadow text-white btn-border-radius text-nowrap" 
            style="background-color: #88336c;"
            onclick="location.href='DashboardServlet?actionType=asset_admin'">
		        <div class="d-flex justify-content-center align-items-center">
		            <%=Language.equalsIgnoreCase("english")?"Asset Dashboard":"অ্যাসেট ড্যাশবোর্ড"%>
		        </div>
		    </button>
        </div>
        <div class="kt-portlet__body form-body  bg-light">
            <div class="row">
                <div class="col-lg-12 my-2">
                    <div class="kt-portlet shadow dashboard-card-border-radius overflow-hidden py-4"
                    >
                        <div class="row">
                            <div class="col-12 mb-1 mt-3 d-flex justify-content-between align-items-center px-4 mx-2">
                                                <span class="h3">
                                                    <%=LM.getText(LC.HM_TICKET, loginDTO)%> <%=LM.getText(LC.HM_COUNT, loginDTO)%>
                                                </span>
                                <button
                                        onclick="location.href='Support_ticketServlet?actionType=search'"
                                        type="button"
                                        class="btn dashboard-count"
                                >
                                    <h3 class="mb-0">
                                        <%=Utils.getDigits(dashboardDTO.ticketCount + "", Language)%>
                                    </h3>
                                </button>
                            </div>
                            
                               <div class="col-6 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                            <span class="h3 text-white">
                                                                <%=Language.equalsIgnoreCase("english")?"Total":"মোট"%>
                                                            </span>
                                            </div>
                                            <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                		
                                                                       		onclick="location.href='Support_ticketServlet?actionType=search'"
                                                                        
                                                                        
                                                                        type="button"
                                                                        class="btn dashboard-count text-white d-flex align-items-center justify-content-center">
                                                                        <h3 class="mb-0"><%=Utils.getDigits(dashboardDTO.ticketCount + "", Language)%></h3>
                                                                </button>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                            <span class="h3 text-white">
                                                                <%=LM.getText(LC.HM_OPEN, loginDTO)%>
                                                            </span>
                                            </div>
                                            <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                		
                                                                        	onclick="location.href='Support_ticketServlet?actionType=OPEN_TICKET_COUNT_FILTER'"
                                                                        
                                                                        type="button"
                                                                        class="btn dashboard-count text-white d-flex align-items-center justify-content-center">
                                                                        <h3 class="mb-0"><%=Utils.getDigits(dashboardDTO.openTicketCount + "", Language)%></h3>
                                                                </button>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                            <span class="h3 text-white">
                                                                <%=LM.getText(LC.HM_CLOSED, loginDTO)%>
                                                            </span>
                                            </div>
                                            <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button 
                                                                		
                                                                			onclick="location.href='Support_ticketServlet?actionType=CLOSED_TICKET_COUNT_FILTER'"
                                                                		
                                                                        type="button"
                                                                        class="btn dashboard-count text-white d-flex align-items-center justify-content-center">
                                                            <h3 class="mb-0"><%=Utils.getDigits(dashboardDTO.closedTicketCount + "", Language)%></h3>
                                                        </button>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-6 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                            <span class="h3 text-white">
                                                                <%=Language.equalsIgnoreCase("english")?"Solved":"সমাধানকৃত"%>
                                                            </span>
                                            </div>
                                            <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button 
                                                                		
                                                                			onclick="location.href='Support_ticketServlet?actionType=SOLVED_TICKET_COUNT_FILTER'"
                                                                		
                                                                        type="button"
                                                                        class="btn dashboard-count text-white d-flex align-items-center justify-content-center">
                                                            <h3 class="mb-0"><%=Utils.getDigits(dashboardDTO.solvedTicketCount + "", Language)%></h3>
                                                        </button>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
   
                <div class="col-lg-6 my-2">
                    <div class="kt-portlet shadow dashboard-card-border-radius overflow-hidden py-4"
                    >
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <!--Div that will hold the pie chart-->
                        <div class="grid-item">
                            <div id="daywiseTicketStatus"></div>

                            <h3 style="text-align: center"><%=LM.getText(LC.HM_DAYWISE, loginDTO)%> <%=LM.getText(LC.HM_TICKET, loginDTO)%> <%=LM.getText(LC.HM_STATUS, loginDTO)%>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-2">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden py-4"
                    >
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <!--Div that will hold the pie chart-->
                        <div class="grid-item">
                            <div id="typewiseTicket"></div>

                            <h3 style="text-align: center"><%=LM.getText(LC.HM_TYPEWISE, loginDTO)%> <%=LM.getText(LC.HM_TICKET, loginDTO)%> <%=LM.getText(LC.HM_COUNT, loginDTO)%>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-2">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden py-4"
                    >
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <!--Div that will hold the pie chart-->
                        <div class="grid-item">
                            <div id="monthWiseTicketStatus"></div>

                            <h3 style="text-align: center"><%=LM.getText(LC.HM_MONTHWISE, loginDTO)%> <%=LM.getText(LC.HM_TICKET, loginDTO)%> <%=LM.getText(LC.HM_STATUS, loginDTO)%>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-2">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden py-4"
                    >
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <!--Div that will hold the pie chart-->
                        <div class="grid-item">
                            <div id="deptWiseTicket"></div>

                            <h3 style="text-align: center"><%=LM.getText(LC.HM_WINGWISE, loginDTO)%> <%=LM.getText(LC.HM_TICKET, loginDTO)%> <%=LM.getText(LC.HM_COUNT, loginDTO)%>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var context = '${context}';
    var pluginsContext = '${pluginsContext}';

    var dashboardDTOJson = '<%=new Gson().toJson( request.getAttribute( "dashboardDTO"  ) )%>';
    var dashboardDTO = JSON.parse(dashboardDTOJson);


</script>
<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

    // Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages': ['corechart']});
    google.charts.setOnLoadCallback(drawTypewiseTicket);

    function drawTypewiseTicket() {

        var data = google.visualization.arrayToDataTable([
            ['Type', '<%=LM.getText(LC.HM_TICKET, loginDTO)%>'],
            <%
            for(int j = 0; j < dashboardDTO.issueTypeCount; j ++)
            {
            	if(Language.equalsIgnoreCase("english"))
            	{
            		%>
	            	[dashboardDTO.ticketTypeEn[<%=j%>], dashboardDTO.typewiseCompplaintCount[<%=j%>]],
	            	<%
	            }
	            else
	            {
	                %>	
	            	[dashboardDTO.ticketTypeBn[<%=j%>], dashboardDTO.typewiseCompplaintCount[<%=j%>]],	
	           		<%
	        	}
    		}
    		%>
        ]);

        var options = {
            title: '<%=LM.getText(LC.HM_TYPEWISE, loginDTO)%> <%=LM.getText(LC.HM_TICKET, loginDTO)%> <%=LM.getText(LC.HM_COUNT, loginDTO)%>',
            legend: {position: 'top'},
            allowHtml: true
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('typewiseTicket'));
        
        function selectHandler() {
            var selectedItem = chart.getSelection()[0];
            if (selectedItem) {
              var topping = data.getValue(selectedItem.row, 0);
              var type = -1;
              <%
              for(int j = 0; j < dashboardDTO.issueTypeCount; j ++)
              {
              	if(Language.equalsIgnoreCase("english"))
              	{
              		%>
  	            	if(dashboardDTO.ticketTypeEn[<%=j%>] == topping)
            		{
  	            		console.log('The user selected <%=j%>');
  	            		type = dashboardDTO.ticketTypeId[<%=j%>];
            		}
  	            	<%
  	            }
  	            else
  	            {
  	            	%>
  	            	if(dashboardDTO.ticketTypeBn[<%=j%>] == topping)
            		{
  	            		console.log('The user selected <%=j%>');
  	            		type = dashboardDTO.ticketTypeId[<%=j%>];
            		}
  	            	<%
  	        	}
      		 }
      		 %>
              	console.log('The user selected ' + topping);
              	<%
              	for(Long ticketType: userTicketTypes)
              	{
              		%>
              		if(type == <%=ticketType%>)
           			{
              			window.location ='Support_ticketServlet?actionType=search&filter=typeWise&type=' + type;
           			}
              		<%
              	}
              	%>
            }
          }

        google.visualization.events.addListener(chart, 'select', selectHandler); 

        chart.draw(data, options);
    }

    google.charts.setOnLoadCallback(drawDaywiseTicketStatus);

    function drawDaywiseTicketStatus() {


        var data = google.visualization.arrayToDataTable([
            ['Date', '<%=LM.getText(LC.HM_OPEN, loginDTO)%>', '<%=LM.getText(LC.HM_CLOSED, loginDTO)%>'],
            <%
           for(int j = 0; j <= 7; j ++)
           {
               %>
            ['<%=Utils.getDigits(dashboardDTO.last7Days[j], Language)%>', dashboardDTO.last7DayOpenedTickets[<%=j%>], dashboardDTO.last7DayClosedTickets[<%=j%>]],
            <%
        }
        %>

        ]);

        var options = {
            title: '<%=LM.getText(LC.HM_DAYWISE, loginDTO)%> <%=LM.getText(LC.HM_TICKET, loginDTO)%> <%=LM.getText(LC.HM_STATUS, loginDTO)%>',
            //curveType: 'function',
            legend: {position: 'bottom'}
        };

        var chart = new google.visualization.LineChart(document.getElementById('daywiseTicketStatus'));

        chart.draw(data, options);
    }

    google.charts.setOnLoadCallback(monthWiseTicket);

    function monthWiseTicket() {

        var data = google.visualization.arrayToDataTable([
            ['Month', '<%=LM.getText(LC.HM_OPEN, loginDTO)%>', '<%=LM.getText(LC.HM_CLOSED, loginDTO)%>'],
            <%

            for(int j = 0; j <= 6; j ++)
            {
            	%>
            ['<%=Utils.getDigits(dashboardDTO.last6Months[j], Language)%>', dashboardDTO.last6MonthsOpenedTickets[<%=j%>], dashboardDTO.last6MonthsClosedTickets[<%=j%>]],
            <%
        }


        %>
        ]);

        var options = {
            title: '<%=LM.getText(LC.HM_MONTHWISE, loginDTO)%> <%=LM.getText(LC.HM_TICKET, loginDTO)%> <%=LM.getText(LC.HM_STATUS, loginDTO)%>',
            legend: {position: 'bottom'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('monthWiseTicketStatus'));

        chart.draw(data, options);
    }

    google.charts.setOnLoadCallback(deptWiseTicket);

    function deptWiseTicket() {

        var data = google.visualization.arrayToDataTable([
            ['Dept', 'Count'],
            <%

            for(int j = 0; j < dashboardDTO.unitWiseCompplaintCount.length; j ++)
            {
            	if(Language.equalsIgnoreCase("english"))
            	{
            	%>
            ['<%=dashboardDTO.unitNameEn[j]%>', <%=dashboardDTO.unitWiseCompplaintCount[j]%>],
            <%
            	}
	            else
	            {
                %>

            ['<%=dashboardDTO.unitNameBn[j]%>', <%=dashboardDTO.unitWiseCompplaintCount[j]%>],

            <%
        		}
    		}
    		%>
        ]);

        var options = {
            title: '<%=LM.getText(LC.HM_WINGWISE, loginDTO)%> <%=LM.getText(LC.HM_TICKET, loginDTO)%> <%=LM.getText(LC.HM_COUNT, loginDTO)%>',
            legend: {position: 'left'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('deptWiseTicket'));

        chart.draw(data, options);
    }
</script>