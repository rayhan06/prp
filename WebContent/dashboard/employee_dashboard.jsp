<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="dashboard.DashboardDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="pb.*" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="util.HttpRequestUtils" %>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String language = LM.getLanguage(userDTO);
    String Language = LM.getText(LC.SUPPORT_TICKET_EDIT_LANGUAGE, loginDTO);
    DashboardDTO dashboardDTO = (DashboardDTO) request.getAttribute("dashboardDTO");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__body form-body bg-light">
            <div class="row">
            	<div class=" col-lg-6">
                    <a href="Support_ticketServlet?actionType=OPEN_TICKET_COUNT_FILTER">
                        <div class="shadow py-4 dashboard-card-border-radius"
                             style="background: linear-gradient(to right, #9861C2, #6132B2);">
                            <div class="d-flex justify-content-between align-items-center px-4">
                                <span class="h3 text-white">
                                    <%=LM.getText(LC.HM_OPEN, loginDTO)%> <%=LM.getText(LC.HM_TICKET, loginDTO)%> <%=LM.getText(LC.HM_COUNT, loginDTO)%>
                                </span>
                                <span class="patient_count h1 text-white">
                                    <%=Utils.getDigits(dashboardDTO.openTicketCount + "", Language)%>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class=" col-lg-6 mt-3 mt-lg-0">
                    <a href="Support_ticketServlet?actionType=CLOSED_TICKET_COUNT_FILTER">
                        <div class="shadow py-4 dashboard-card-border-radius"
                             style="background: linear-gradient(to right, #EE9048, #EE9048);">
                            <div class="d-flex justify-content-between align-items-center px-4">
                                <span class="h3 text-white">
                                    <%=LM.getText(LC.HM_CLOSED, loginDTO)%> <%=LM.getText(LC.HM_TICKET, loginDTO)%> <%=LM.getText(LC.HM_COUNT, loginDTO)%>
                                </span>
                                <span class="patient_count h1 text-white" style="background-color: #FCB42E;">
                                    <%=Utils.getDigits(dashboardDTO.closedTicketCount + "", Language)%>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <input type="hidden" name='employeeRecordsId' id='employeeRecordsId'
                       value="<%=userDTO.employee_record_id%>">
                <div class=" col-lg-6 mt-4">
                    <div class="shadow overflow-hidden px-0 py-4 dashboard-card-border-radius"
                         style="background-color: #FFFFFF;">
                        <%@include file="patient_subdashboard.jsp" %>
                    </div>
                </div>
                <%-- as attendance module is not integrated, this graph is hidden
                <div class=" col-lg-6 mt-4">
                    <div class="shadow overflow-hidden px-0 py-4 dashboard-card-border-radius"
                         style="background-color: #FFFFFF;">
                        <%@include file="employee_attendance_history.jsp" %>
                    </div>
                </div>
                --%>

                <div class=" col-lg-6 mt-4">
                    <div class="shadow overflow-hidden px-0 py-4 dashboard-card-border-radius"
                         style="background-color: #FFFFFF;">
                        <%@include file="employee/emp_ot_bill_status.jsp" %>
                    </div>
                </div>

                <div class=" col-lg-6 mt-4">
                    <div class="shadow overflow-hidden px-0 py-4 dashboard-card-border-radius"
                         style="background-color: #FFFFFF;">
                        <%@include file="employee/emp_count_by_training.jsp" %>
                    </div>
                </div>
                <div class=" col-lg-6 mt-4">
                    <div class="shadow overflow-hidden px-0 py-4 dashboard-card-border-radius"
                         style="background-color: #FFFFFF;">
                        <%@include file="employee/leave_details_last_6_month.jsp" %>
                    </div>
                </div>
                <%--<div class=" col-lg-6 mt-4">
                    <div class="shadow overflow-hidden px-0 py-4 dashboard-card-border-radius"
                         style="background-color: #FFFFFF;">
                        <%@include file="asset_subdashboard.jsp" %>
                    </div>
                </div>--%>
                <div class=" col-lg-6 mt-4">
                    <div class="shadow overflow-hidden px-0 py-4 dashboard-card-border-radius"
                         style="background-color: #FFFFFF;">
                        <%@include file="ticket_subdashboard.jsp" %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(()=>{

    });
</script>