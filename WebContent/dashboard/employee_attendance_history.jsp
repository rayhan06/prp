﻿<div class="kt-portlet__head">
    <div class="kt-portlet__head-label">
        <h3 class="kt-portlet__head-title">
            <%=LM.getText(LC.EMPLOYEE_DASHBOARD_EMPLOYEE_ATTENDANCE_HISTORY_OF_LAST_MONTH, loginDTO)%>
        </h3>
    </div>
</div>

<div class="kt-portlet__body">
    <div id="employee_attendance_history" style="height: 300px;"></div>
</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    let employeeAttendanceData;
    let receivedData = [];

    $(document).ready(() => {
        getMonthlyAttendanceData();
    })

    function getMonthlyAttendanceData() {
        let url = "AttendanceDashboardServlet?actionType=employeeAttendanceHistory&language=<%=language%>";

        let wd = 'Working Days';
        let ab = 'Absent Days';
        let li = 'Late In';
        let eo = 'Early Out';

        if ('<%=language%>' == 'Bangla') {
            wd = 'কর্মদিবস';
            ab = 'অনুপস্থিত';
            li = 'দেরিতে উপস্থিত';
            eo = 'অগ্র প্রস্থান';
        }

        $.ajax({
            url: url,
            type: "GET",
            async: true,
            success: function (fetchedData) {

                employeeAttendanceData = fetchedData;
                receivedData = [];
                receivedData.push(['',
                    '',
                ]);
                receivedData.push([wd, employeeAttendanceData.workingDays]);
                receivedData.push([ab, employeeAttendanceData.absentDays]);
                receivedData.push([li, employeeAttendanceData.lateInDays]);
                receivedData.push([eo, employeeAttendanceData.earlyOutDays]);

                google.charts.load("current", {packages: ['corechart']});

                google.charts.setOnLoadCallback(drawChartEmployeeAttendanceHistory);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function drawChartEmployeeAttendanceHistory() {
        var data = google.visualization.arrayToDataTable(receivedData);

        var view = new google.visualization.DataView(data);

        view.setColumns([0, 1,
            {
                calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation"
            }
        ]);

        var options = {
            title: '',
            legend: {position: "none"}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById("employee_attendance_history"));

        chart.draw(view, options);
    }
</script>