<%@ page import="dashboard.DashboardDTO" %>
<%@ page import="java.util.List" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="pb.*" %>
<%@ page import="medical_equipment_name.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String context = "../../.." + request.getContextPath() + "/";

    DashboardDTO dashboardDTO = (DashboardDTO) request.getAttribute("dashboardDTO");
    String Language = LM.getText(LC.SUPPORT_TICKET_EDIT_LANGUAGE, loginDTO);

%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=Language.equalsIgnoreCase("english")?"X-Ray Technologist Dashboard":"এক্স রে টেকনোলজিস্ট ড্যাশবোর্ড"%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body bg-light">
            <div class="row">
                <div class="col-lg-6">
                    <div class="">
                        <a href="Prescription_detailsServlet?actionType=search&addLabTest=true">
                            <div class="">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div class="shadow dashboard-card-border-radius overflow-hidden w-100 h-100"
                                         style="background: linear-gradient(to right, #9861C2, #6132B2);">
                                        <div class="d-flex justify-content-between align-items-center mx-2 my-4 px-4">
                                            <div class="">
                                                <span class="h3 text-white">
                                                    <%=Language.equalsIgnoreCase("english")?"Pending Tests":"পেন্ডিং পরীক্ষাসমূহ"%>
                                                </span>
                                            </div>
                                            <div class="ml-5">
                                                <span class="patient_count h1 text-white">
                                                    <%=Utils.getDigits(dashboardDTO.testsPending, Language)%>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 mt-4 mt-lg-0">
                    <div class="">
                        <a href="Prescription_detailsServlet?actionType=search&addLabTest=true&myTests=true">
                            <div class="">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div class="shadow dashboard-card-border-radius overflow-hidden w-100 h-100"
                                         style="background: linear-gradient(to right, #9861C2, #6132B2);">
                                        <div class="d-flex justify-content-between align-items-center mx-2 my-4 px-4">
                                            <div class="">
                                                <span class="h3 text-white">
                                                    <%=Language.equalsIgnoreCase("english")?"Tests Performed Today":"আজ করা পরীক্ষাসমূহ"%>
                                                </span>
                                            </div>
                                            <div class="ml-5">
                                                <span class="patient_count h1 text-white">
                                                    <%=Utils.getDigits(dashboardDTO.testsToday, Language)%>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 mt-4">
                    <div class="d-flex justify-content-center align-items-center">
                        <div class="shadow dashboard-card-border-radius overflow-hidden w-100 h-100 bg-white">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        <%=Language.equalsIgnoreCase("english")?"Daywise Tests":"দিনভিত্তিক পরীক্ষাসমূহ"%>
                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="kt_widget2_tab1_content">
                                        <div class="kt-widget2">
                                            <div id="day7Div"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 mt-4">
                    <div class="d-flex justify-content-center align-items-center">
                        <div class="shadow dashboard-card-border-radius overflow-hidden w-100 h-100 bg-white">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        <%=Language.equalsIgnoreCase("english")?"Remaining Plates":"অবশিষ্ট প্লেটসমূহ" %>
                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="kt_widget2_tab1_content2">
                                        <div class="kt-widget2">
                                            <div id="plateDiv"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var context = '${context}';
    var pluginsContext = '${pluginsContext}';

    var dashboardDTOJson = '<%=new Gson().toJson( request.getAttribute( "dashboardDTO"  ) )%>';
    var dashboardDTO = JSON.parse(dashboardDTOJson);


    //var isAdmin = request.getAttribute("isAdmin");
</script>

<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

    // Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages': ['corechart']});
    google.charts.setOnLoadCallback(draw7DayChart);

    function draw7DayChart() {

    	var data = google.visualization.arrayToDataTable([
            ['<%=LM.getText(LC.HM_DAY, loginDTO)%>',  '<%=Language.equalsIgnoreCase("english")?"X Rays":"এক্স রে সমূহ"%>', '<%=Language.equalsIgnoreCase("english")?"Ultrasonograms":"আল্ট্রাসনোগ্রামসমূহ"%>'],
            <%
            for(int j = 0; j <= 7; j ++)
            {
            	%>
            ['<%=Utils.getDigits(dashboardDTO.last7Days[j], Language)%>', <%=dashboardDTO.last7DayTests1[j]%>, <%=dashboardDTO.last7DayTests2[j]%>],
            <%
        	}
        %>
        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Tests of Last 7 Days ":"গত সাত দিনের পরীক্ষাসমূহ"%>',
            legend: {position: 'top'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('day7Div'));

        chart.draw(data, options);
    }

    google.charts.setOnLoadCallback(drawPlates);

    function drawPlates() {

        var data = google.visualization.arrayToDataTable([
            ['<%=Language.equalsIgnoreCase("english")?"Remaining Plates":"অবশিষ্ট প্লেটসমূহ"%>',  '<%=Language.equalsIgnoreCase("english")?"Plates":"প্লেটসমূহ"%>'],
            <%
            for(Medical_equipment_nameDTO xRayDTO: dashboardDTO.xRayPlates)
            {
            	%>
            ["<%=xRayDTO.nameEn%>", <%=xRayDTO.currentStock%>],
            <%
        	}
        %>
        ]);

        var options = {
       		 title: '<%=Language.equalsIgnoreCase("english")?"Remaining Plates":"অবশিষ্ট প্লেটসমূহ"%>',
             legend: {position: 'left'}
       };
        var chart = new google.visualization.PieChart(document.getElementById('plateDiv'));

        chart.draw(data, options);
    }
</script>