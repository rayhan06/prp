<%

    String context = "../../.." + request.getContextPath() + "/";

%>
<section class="content position-fixed">
    <!-- Info boxes -->
    <div class="row">
        <div class="col-xs-12 col-sm-12  col-md-12  body-open">
            <div class="row banner_content">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <img class="banner_img" src="<%=context%>assets/images/5.png" style="">
                    <div class="banner-description">
                        Safe and Secure Animal Protein for all
                    </div>
                </div>

            </div>

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title uppercase">
                        <i class="fa fa-gift"></i>DLS Dashboard
                    </h3>
                </div>

                <div class="box-body">

                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h4 style="font-size: 25px;font-weight: bold;">Data Collections</h4>

                                    <h4><span id="TEXT_1_0_0">Actual Collection: </span>
                                        <span class="counter">0</span>
                                    </h4>

                                    <h4><span id="TEXT_2_0_1">Percentage Achieved: </span>
                                        <span class="counter">0%</span>
                                    </h4>
                                </div>
                                <div class="icon" style="top: 5px;">
                                    <img src="<%=context%>assets/images/grs/semen.png" alt="Chicago"
                                         style="height:50px;opacity: 0.4;">
                                </div>

                            </div>
                        </div>

                        <!-- ./col -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h4 style="font-size: 25px;font-weight: bold;">Artificial Data</h4>

                                    <h4><span id="TEXT_1_0_0">Actual AI: </span>
                                        <span class="counter">0</span>
                                    </h4>

                                    <h4><span id="TEXT_2_0_1">Percentage Achieved: </span>
                                        <span class="counter">0%</span>
                                    </h4>
                                </div>
                                <div class="icon" style="top: 5px;">
                                    <img src="<%=context%>assets/images/grs/ai.png" alt="Chicago"
                                         style="height:50px;opacity: 0.4;">
                                </div>

                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h4 style="font-size: 25px;font-weight: bold;">Productivity</h4>

                                    <h4><span id="TEXT_1_0_0">Actual : </span>
                                        <span class="counter">0</span>
                                    </h4>

                                    <h4><span id="TEXT_2_0_1">Percentage Achieved: </span>
                                        <span class="counter">0%</span>
                                    </h4>
                                </div>
                                <div class="icon" style="top: 5px;">
                                    <img src="<%=context%>assets/images/grs/progenyte.png" alt="Chicago"
                                         style="height:50px;opacity: 0.4;">
                                </div>

                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h4 style="font-size: 20px;font-weight: bold;"> Production</h4>

                                    <h4><span id="TEXT_1_0_0">Actual Production: </span>
                                        <span class="counter">0</span>
                                    </h4>

                                    <h4><span id="TEXT_2_0_1">Percentage Achieved: </span>
                                        <span class="counter">0%</span>
                                    </h4>
                                </div>
                                <div class="icon" style="top: 5px;">
                                    <img src="<%=context%>assets/images/grs/bull.png" alt="Chicago"
                                         style="height:50px;opacity: 0.4;">

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div clasas="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-sm-6">
                            <div class="chartjs-size-monitor">
                                <div class="chartjs-size-monitor-expand">
                                    <div class=""></div>
                                </div>
                                <div class="chartjs-size-monitor-shrink">
                                    <div class=""></div>
                                </div>
                            </div>
                            <canvas id="progenyBull" width="530" height="265" class="chartjs-render-monitor"
                                    style="display: block; width: 530px; height: 265px;"></canvas>

                            <h3 style="text-align:center">Most Productive </h3>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-sm-6">
                            <div class="chartjs-size-monitor">
                                <div class="chartjs-size-monitor-expand">
                                    <div class=""></div>
                                </div>
                                <div class="chartjs-size-monitor-shrink">
                                    <div class=""></div>
                                </div>
                            </div>

                            <canvas id="progenyCenter" width="529" height="264" class="chartjs-render-monitor"
                                    style="display: block; width: 529px; height: 264px;"></canvas>
                            <h3 style="text-align:center">Most Productive Centers </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    var canvas = document.getElementById('progenyBull');
    var data = {
        labels: ['null', 'null', 'null', 'null', 'null'],
        datasets: [
            {
                label: "Most Productive Bulls",
                backgroundColor: "#0d7c98",
                borderColor: "#ffffff",
                borderWidth: 2,
                hoverBackgroundColor: "#00a65a",
                hoverBorderColor: "#ffffff",
                data: [0, 0, 0, 0, 0],
            }
        ]
    };
    var option = {
        scales: {
            yAxes: [{
                stacked: true,
                gridLines: {
                    display: true,
                    color: "rgba(255,99,132,0.2)"
                }
            }],
            xAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    };

    var myBarChart = new Chart(canvas, {
        type: 'bar',
        data: {
            labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        }
    });
</script>

<script>
    var canvas2 = document.getElementById('progenyCenter');
    var data = {
        labels: ['null', 'null', 'null', 'null', 'null'],
        datasets: [
            {
                label: "Most Productive Centers",
                backgroundColor: "#00a65a",
                borderColor: "#ffffff",
                borderWidth: 2,
                hoverBackgroundColor: "#0d7c98",
                hoverBorderColor: "#ffffff",
                data: [0, 0, 0, 0, 0],
            }
        ]
    };
    var option = {
        scales: {
            yAxes: [{
                stacked: true,
                gridLines: {
                    display: true,
                    color: "rgba(255,99,132,0.2)"
                }
            }],
            xAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    };
    var myChart = new Chart(canvas2, {
        type: 'bar',
        data: {
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        }
    });
</script>