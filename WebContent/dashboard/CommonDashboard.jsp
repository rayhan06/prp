<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@ page import="common.RoleEnum" %>
<%@ page import="dashboard.DashboardDTO" %>
<%
DashboardDTO dashboardDTO = (DashboardDTO) request.getAttribute("dashboardDTO");
if(dashboardDTO == null)
{
	System.out.println("null dashboarddto in common dashboard");
}
else
{
	System.out.println("not null dashboarddto in common dashboard");
}
request.setAttribute("dontReplaceDashboardDTO", true);
%>
<jsp:include page="../common/dashboard_layout.jsp" flush="true">
    <jsp:param name="title" value="Dashboard"/>
    <jsp:param name="body" value="${dashboardPath}"/>
    <jsp:param name="dashboardDTO" value="${dashboardDTO}"/>
</jsp:include> 