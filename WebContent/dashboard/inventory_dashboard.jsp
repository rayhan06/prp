<%@ page import="dashboard.DashboardDTO" %>
<%@ page import="java.util.List" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="pb.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String context = "../../.." + request.getContextPath() + "/";

    DashboardDTO dashboardDTO = (DashboardDTO) request.getAttribute("dashboardDTO");
    String Language = LM.getText(LC.SUPPORT_TICKET_EDIT_LANGUAGE, loginDTO);

%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=Language.equalsIgnoreCase("english")?"Inventory Dashboard":"ইনভেন্টরি ড্যাশবোর্ড"%>                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body bg-light">
            <div class="row">
                
               <div class="col-lg-12 my-3">
                    <div class="kt-portlet  shadow overflow-hidden dashboard-card-border-radius py-4"
                    >
                        <div class="row">
                            <div class="col-12 mb-1 mt-3 px-4 mx-2">
                                <span class="h4">
                                    <%=Language.equalsIgnoreCase("english")?"Pending Prescriptions":"অপেক্ষারত প্রেসক্রিপশনসমূহ"%>
                                </span>
                            </div>
                            <div class="col-12 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                <span class="h4 text-white">
					                                    <%=Language.equalsIgnoreCase("english")?"Pending":"অপেক্ষারত"%>
					                                </span>
                                            </div>
                                            <div class="ml-5">
                                                       <span class="patient_count2 text-white">
                                                           <button
                                                                   
                                                                   onclick = "location.href='Prescription_detailsServlet?actionType=search&addMedicine=1'"
                                                                   type="button"
                                                                   class="btn dashboard-count text-white">
                                                                   <h4><%=Utils.getDigits(dashboardDTO.testsPending, Language)%></h4>
                                                           </button>
                                                       </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                       
                        </div>
                    </div>
                </div>
                
                
                
                
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow overflow-hidden dashboard-card-border-radius py-4"
                    >
                        <div class="row">
                            <div class="col-12 mb-1 mt-3 px-4 mx-2">
                                <span class="h4">
                                    <%=LM.getText(LC.HM_MEDICINE_DELIVERED_IN_CURRENT_MONTH, loginDTO)%>
                                </span>
                            </div>
                            <div class="col-12 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                <span class="h4 text-white">
                                                    <%=LM.getText(LC.HM_COUNT, loginDTO)%>
                                                </span>
                                            </div>
                                            <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        id="incomingLetterSummary"
                                                                       	style = "pointer-events: none; cursor: not-allowed;"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white">
                                                                        <h4><%=Utils.getDigits(dashboardDTO.soldCount, Language)%></h4>
                                                                </button>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                            <span class="h4 text-white">
                                                                <%=LM.getText(LC.HM_COST, loginDTO)%>
                                                            </span>
                                            </div>
                                            <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        id="TEXT_14_0_1"
                                                                        style = "pointer-events: none; cursor: not-allowed;"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white">
                                                            <h4><%=Utils.getDigits((int)dashboardDTO.totalCost, Language)%></h4>
                                                        </button>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow overflow-hidden dashboard-card-border-radius py-4"
                    >
                        <div class="row">
                            <div class="col-12 mb-1 mt-3 px-4 mx-2">
                                <span class="h4">
                                    <%=Language.equalsIgnoreCase("english")?"Service Count Today":"আজকের সেবার সংখ্যা"%>
                                </span>
                            </div>
                            <div class="col-12 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                <span class="h4 text-white">
                                                   <%=Language.equalsIgnoreCase("english")?"Total Prescriptions":"প্রেসক্রিপশনের সংখ্যা"%>

                                                </span>
                                            </div>
                                            <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        id="incomingLetterSummary"
                                                                        style = "pointer-events: none; cursor: not-allowed;"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white">
                                                                        <h4><%=Utils.getDigits(dashboardDTO.totalPatients, Language)%></h4>
                                                                </button>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                            <span class="h4 text-white">
                                                                <%=Language.equalsIgnoreCase("english")?"Total Medicines":"ঔষধের সংখ্যা"%>

                                                            </span>
                                            </div>
                                            <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        id="TEXT_14_0_1"
                                                                        style = "pointer-events: none; cursor: not-allowed;"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white">
                                                            <h4><%=Utils.getDigits(dashboardDTO.totalDrugs, Language)%></h4>
                                                        </button>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow overflow-hidden dashboard-card-border-radius py-4"
                    >
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <!--Div that will hold the pie chart-->
                        <div class="grid-item">
                            <div id="chart1_div"></div>
                            <h4 class="text-center my-2">
                                <%=LM.getText(LC.HM_MEDICINE_DISTRIBUTION_VS_COST_IN_LAST_7_DAYS, loginDTO)%>
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow overflow-hidden dashboard-card-border-radius py-4"
                    >
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <!--Div that will hold the pie chart-->
                        <div class="grid-item">
                            <div id="char2_div"></div>
                            <h4 class="text-center my-2">
                                <%=LM.getText(LC.HM_MEDICINE_DISTRIBUTION_VS_COST_IN_LAST_6_MONTHS, loginDTO)%>
                            </h4>
                        </div>
                    </div>
                </div>
                
               <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow overflow-hidden dashboard-card-border-radius py-4"
                    >
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <!--Div that will hold the pie chart-->
                        <div class="grid-item">
                            <div id="chart5_div"></div>
                            <h4 class="text-center my-2">
                               <%=Language.equalsIgnoreCase("english")?"Prescription Count in last 6 Months":"গত ৬ মাসে প্রেসক্রিপশনের সংখ্যা"%>
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow overflow-hidden dashboard-card-border-radius py-4"
                    >
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <!--Div that will hold the pie chart-->
                        <div class="grid-item">
                            <div id="chart6_div"></div>
                            <h4 class="text-center my-2">
                               <%=Language.equalsIgnoreCase("english")?"Prescription Count in last 7 Days":"গত ৭ দিনে প্রেসক্রিপশনের সংখ্যা"%>
                            </h4>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow overflow-hidden dashboard-card-border-radius py-4"
                    >
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <!--Div that will hold the pie chart-->
                        <div class="grid-item">
                            <div id="char3_div"></div>
                            <h4 class="text-center my-2">
                                <%=LM.getText(LC.HM_MOST_USED_DRUGS_BY_AMOUNT, loginDTO)%>
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow overflow-hidden dashboard-card-border-radius py-4"
                    >
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <!--Div that will hold the pie chart-->
                        <div class="grid-item">
                            <div id="char4_div"></div>
                            <h4 class="text-center my-2">
                                <%=LM.getText(LC.HM_MOST_USED_DRUGS_BY_COST, loginDTO)%>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var context = '${context}';
    var pluginsContext = '${pluginsContext}';

    var dashboardDTOJson = '<%=new Gson().toJson( request.getAttribute( "dashboardDTO"  ) )%>';
    var dashboardDTO = JSON.parse(dashboardDTOJson);


</script>
<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

    // Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages': ['corechart']});
    google.charts.setOnLoadCallback(drawChartPatientMonth);

    function drawChartPatientMonth() {
    	
    	<%
        for(int j = 0; j <= 6; j ++)
        {
        	%>
            console.log ('<%=Utils.getDigits(dashboardDTO.last6Months[j], Language)%>' + " " + 
            	dashboardDTO.last6MonthMedicineSold[<%=j%>] + " " +
            	dashboardDTO.last6MonthMedicineCost[<%=j%>] );
        	<%
    	}
       %>

        var data = google.visualization.arrayToDataTable([
            ['Month', '<%=LM.getText(LC.HM_COUNT, loginDTO)%>', '<%=LM.getText(LC.HM_COST, loginDTO)%>'
            ],
            <%
            for(int j = 0; j <= 6; j ++)
            {
            	%>
	            ['<%=Utils.getDigits(dashboardDTO.last6Months[j], Language)%>',
	            	dashboardDTO.last6MonthMedicineSold[<%=j%>],
	            	dashboardDTO.last6MonthMedicineCost[<%=j%>]],
            	<%
        	}
           %>
        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Last 6 Month Medicine Count, cost":"গত ৬ মাসের ঔষধের সংখ্যা, মূল্য"%>',
            legend: {position: 'bottom'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('char2_div'));

        chart.draw(data, options);
    }
    
    google.charts.setOnLoadCallback(drawChartPrescriptionMonth);

    function drawChartPrescriptionMonth() {

        var data = google.visualization.arrayToDataTable([
            ['Month', '<%=LM.getText(LC.HM_COUNT, loginDTO)%>'
            ],
            <%
            for(int j = 0; j <= 6; j ++)
            {
            	%>
	            ['<%=Utils.getDigits(dashboardDTO.last6Months[j], Language)%>',
	            	dashboardDTO.last6MonthPatients[<%=j%>]],
            	<%
        	}
        %>
        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Last 6 Month Prescriptions":"গত ৬ মাসের প্রেসক্রিপশনের সংখ্যা"%>',
            legend: {position: 'bottom'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart5_div'));

        chart.draw(data, options);
    }

    google.charts.setOnLoadCallback(drawChartMedicine7Days);

    function drawChartMedicine7Days() {


        var data = google.visualization.arrayToDataTable([
            ['Date', '<%=LM.getText(LC.HM_COUNT, loginDTO)%>', 
            	'<%=LM.getText(LC.HM_COST, loginDTO)%>'
            	],
            //[dashboardDTO.lastSevenDaysDocumentsCountDTO.last7thDayDate,  dashboardDTO.lastSevenDaysDocumentsCountDTO.last7thDayCount],
            <%
           for(int j = 0; j <= 7; j ++)
           {
               %>
	            ['<%=Utils.getDigits(dashboardDTO.last7Days[j], Language)%>',
	            	dashboardDTO.last7DayMedicineSold[<%=j%>],
	            	dashboardDTO.last7DayMedicineCost[<%=j%>]],
            	
            <%
        }
        %>

        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Last 7 day Medicine Count, cost":"গত ৭ দিনের ওয়ষধের সংখ্যা, মূল্য"%>',
            curveType: 'function',
            legend: {position: 'bottom'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart1_div'));

        chart.draw(data, options);
    }
    
    google.charts.setOnLoadCallback(drawChartPresription7Days);

    function drawChartPresription7Days() {


        var data = google.visualization.arrayToDataTable([
            ['Date', '<%=LM.getText(LC.HM_COUNT, loginDTO)%>', 
            	
            	],
            //[dashboardDTO.lastSevenDaysDocumentsCountDTO.last7thDayDate,  dashboardDTO.lastSevenDaysDocumentsCountDTO.last7thDayCount],
            <%
           for(int j = 0; j <= 7; j ++)
           {
               %>
	            ['<%=Utils.getDigits(dashboardDTO.last7Days[j], Language)%>',
	            	dashboardDTO.last7DayPatients[<%=j%>]],
            	
            <%
        }
        %>

        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Last 7 day Prescriptions":"গত ৭ দিনের প্রেসক্রিপশন"%>',
            curveType: 'function',
            legend: {position: 'bottom'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart6_div'));

        chart.draw(data, options);
    }


    google.charts.setOnLoadCallback(drawPopularDrugs);

    function drawPopularDrugs() {

        var data = google.visualization.arrayToDataTable([
            ['Name', '<%=LM.getText(LC.HM_COUNT, loginDTO)%>'],
            <%
            for(int j = 0; j <= 10; j ++)
            {
            	%>
            [dashboardDTO.popularDrugNames[<%=j%>], dashboardDTO.popularDrugCount[<%=j%>]],
            <%
        }
        %>
        ]);

        var options = {
            title: '<%=LM.getText(LC.HM_MOST_USED_DRUGS_BY_AMOUNT, loginDTO)%>',
            legend: {position: 'bottom'}
        };

        var chart = new google.visualization.BarChart(document.getElementById('char3_div'));

        chart.draw(data, options);


    }

    google.charts.setOnLoadCallback(drawProfitableDrugs);

    function drawProfitableDrugs() {

        var data = google.visualization.arrayToDataTable([
            ['Name', '<%=LM.getText(LC.HM_COST, loginDTO)%>'],
            <%
            for(int j = 0; j <= 10; j ++)
            {
            	%>
            [dashboardDTO.profitableDrugNames[<%=j%>], dashboardDTO.pritableDrugMoney[<%=j%>]],
            <%
        }
        %>
        ]);

        var options = {
            title: '<%=LM.getText(LC.HM_MOST_USED_DRUGS_BY_COST, loginDTO)%>',
            legend: {position: 'bottom'}
        };

        var chart = new google.visualization.BarChart(document.getElementById('char4_div'));

        chart.draw(data, options);
    }


</script>