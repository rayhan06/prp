<%@ page import="dashboard.DashboardDTO" %>
<%@ page import="java.util.List" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="pb.*" %>
<%@ page import="user.*" %>
<%@ page import="appointment.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String context = "../../.." + request.getContextPath() + "/";

    DashboardDTO dashboardDTO = (DashboardDTO) request.getAttribute("dashboardDTO");
    String Language = LM.getText(LC.SUPPORT_TICKET_EDIT_LANGUAGE, loginDTO);

%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=Language.equalsIgnoreCase("english")?"Nurse Dashboard":"নার্স ড্যাশবোর্ড"%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body bg-light">
            <div class="row">
                <div class="col-lg-6">
                    <div class="">
                        <a href="Patient_measurementServlet?actionType=search&filter=todaysMeasurements">
                            <div class="">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div class="shadow dashboard-card-border-radius overflow-hidden w-100 h-100"
                                         style="background: linear-gradient(to right, #9861C2, #6132B2);">
                                        <div class="d-flex justify-content-between align-items-center mx-2 my-4 px-4">
                                            <div class="">
                                                <span class="h3 text-white">
                                                    <%=Language.equalsIgnoreCase("english")?"Patients Measured Today":"আজ যতজন রোগীদের দেখা হয়েছে"%>
                                                </span>
                                            </div>
                                            <div class="ml-5">
                                                <span class="patient_count h1 text-white">
                                                    <%=Utils.getDigits(dashboardDTO.pmCount, Language)%>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 mt-4 mt-lg-0">
                    <div class="">
                        <a href="Nurse_actionServlet?actionType=search&todaysActions=true">
                            <div class="">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div class="shadow dashboard-card-border-radius overflow-hidden w-100 h-100"
                                         style="background: linear-gradient(to right, #EE9048, #EE9048);">
                                        <div class="d-flex justify-content-between align-items-center mx-2 my-4 px-4">
                                            <div class="">
                                                <span class="h3 text-white">
                                                    <%=Language.equalsIgnoreCase("english")?"Actions Performed Today":"আজ সম্পাদিত কর্মকাণ্ডের সংখ্যা"%>
                                                </span>
                                            </div>
                                            <div class="ml-5">
                                                <span class="patient_count h1 text-white"
                                                      style="background-color: #FCB42E;">
                                                    <%=Utils.getDigits(dashboardDTO.actionCount, Language)%>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 mt-4">
                    <div class="d-flex justify-content-center align-items-center">
                        <div class="shadow dashboard-card-border-radius overflow-hidden w-100 h-100 bg-white">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        <%=Language.equalsIgnoreCase("english")?"Actions Types":"কর্মকণ্ডের ধরণ"%>
                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="kt_widget2_tab1_content">
                                        <div class="kt-widget2">
                                            <div id="actionDiv"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 mt-4">
                    <div class="d-flex justify-content-center align-items-center">
                        <div class="shadow dashboard-card-border-radius overflow-hidden w-100 h-100 bg-white">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        <%=Language.equalsIgnoreCase("english")?"Last 7 Days Actions":"গত সাত দিনে কর্মকাণ্ডের সংখ্যা"%>
                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="kt_widget2_tab1_content2">
                                        <div class="kt-widget2">
                                            <div id="day7Div"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var context = '${context}';
    var pluginsContext = '${pluginsContext}';

    var dashboardDTOJson = '<%=new Gson().toJson( request.getAttribute( "dashboardDTO"  ) )%>';
    var dashboardDTO = JSON.parse(dashboardDTOJson);


    //var isAdmin = request.getAttribute("isAdmin");
</script>

<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

    // Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages': ['corechart']});
    google.charts.setOnLoadCallback(draw7DayChart);

    function draw7DayChart() {

        var data = google.visualization.arrayToDataTable([
            ['<%=LM.getText(LC.HM_DAY, loginDTO)%>',  '<%=Language.equalsIgnoreCase("english")?"Actions":"কর্মকণ্ড"%>'],
            <%
            for(int j = 0; j <= 7; j ++)
            {
            	%>
            ['<%=Utils.getDigits(dashboardDTO.last7Days[j], Language)%>', <%=dashboardDTO.last7DayPatients[j]%>],
            <%
        	}
        %>
        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Last 7 Days Actions":"গত সাত দিনে কর্মকাণ্ডের সংখ্যা"%>',
            legend: {position: 'top'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('day7Div'));

        chart.draw(data, options);
    }

    google.charts.setOnLoadCallback(drawActionTypeChart);

    function drawActionTypeChart() {


        var data = google.visualization.arrayToDataTable([
            ['<%=Language.equalsIgnoreCase("english")?"Actions Type":"কর্মকণ্ডের ধরণ"%>', '<%=LM.getText(LC.HM_COUNT, loginDTO)%>'],
            <%
            if(dashboardDTO.popularActioncCount != null)
            {
	           for(KeyCountDTO keyCountDTO: dashboardDTO.popularActioncCount)
	           {
	        	   
	               %>
	            		['<%=CatDAO.getName(Language, "nurse_action", keyCountDTO.key)%>', <%=keyCountDTO.count%>],
	            	<%
	        	   
	        	}
            }
        %>

        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Actions Types":"কর্মকণ্ডের ধরণ"%>',
            //curveType: 'function',
            legend: {position: 'left'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('actionDiv'));

        chart.draw(data, options);
    }
</script>