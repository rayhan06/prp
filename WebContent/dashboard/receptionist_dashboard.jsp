<%@ page import="dashboard.DashboardDTO" %>
<%@ page import="java.util.List" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="pb.*" %>
<%@ page import="appointment.*" %>
<%@ page import="workflow.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String context = "../../.." + request.getContextPath() + "/";

    DashboardDTO dashboardDTO = (DashboardDTO) request.getAttribute("dashboardDTO");
    String Language = LM.getText(LC.SUPPORT_TICKET_EDIT_LANGUAGE, loginDTO);

%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=Language.equalsIgnoreCase("english")?"Receptionist Dashboard":"রিসেপশনিস্ট ড্যাশবোর্ড"%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body bg-light">
            <div class="row">
                <div class="col-lg-6">
                    <div class="">
                        <a href="AppointmentServlet?actionType=getReceptionsAppointmentsToday">
                            <div class="">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div class="shadow dashboard-card-border-radius overflow-hidden w-100 h-100"
                                         style="background: linear-gradient(to right, #9861C2, #6132B2);">
                                        <div class="d-flex justify-content-between align-items-center mx-2 my-4 px-4">
                                            <div class="">
                                                <span class="h3 text-white">
                                                    <%=Language.equalsIgnoreCase("english")?"Appointments Today":"আজকের অ্যাপয়েন্টমেন্টসমূহ"%>
                                                </span>
                                            </div>
                                            <div class="ml-5">
                                                <span class="patient_count h1 text-white">
                                                    <%=Utils.getDigits(dashboardDTO.appointmentToday, Language)%>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 mt-4 mt-lg-0">
                    <div class="">
                        <a href="AppointmentServlet?actionType=getReceptionsAppointmentsThisMonth">
                            <div class="">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div class="shadow dashboard-card-border-radius overflow-hidden w-100 h-100"
                                         style="background: linear-gradient(to right, #9861C2, #6132B2);">
                                        <div class="d-flex justify-content-between align-items-center mx-2 my-4 px-4">
                                            <div class="">
                                                <span class="h3 text-white">
                                                    <%=Language.equalsIgnoreCase("english")?"Appointments This Month":"এ মাসের অ্যাপয়েন্টমেন্টসমূহ"%>
                                                </span>
                                            </div>
                                            <div class="ml-5">
                                                <span class="patient_count h1 text-white">
                                                    <%=Utils.getDigits(dashboardDTO.appointmentThisMonth, Language)%>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                
                <div class="col-lg-12 my-3">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden"
                    <%=dashboardDTO.doctorNamesToday.size() == 0?"style='display:none'":""%>
                         >
                        <div>
                            <div id="divDrToday" style="height: 600px;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 my-3">
                    <div class="kt-portlet  shadow dashboard-card-border-radius overflow-hidden"
                    <%=dashboardDTO.receptionistWiseCountToday.size() == 0?"style='display:none'":""%>
                         >
                        <div>
                            <div id="divRec" style="height: 300px;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 mt-4">
                    <div class="d-flex justify-content-center align-items-center">
                        <div class="shadow dashboard-card-border-radius overflow-hidden w-100 h-100 bg-white">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        <%=Language.equalsIgnoreCase("english")?"Daywise Appointments":"দিনভিত্তিক অ্যাপয়েন্টমেন্টসমূহ"%>
                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="kt_widget2_tab1_content">
                                        <div class="kt-widget2">
                                            <div id="day7Div"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 mt-4">
                    <div class="d-flex justify-content-center align-items-center">
                        <div class="shadow dashboard-card-border-radius overflow-hidden w-100 h-100 bg-white">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        <%=Language.equalsIgnoreCase("english")?"Monthwise Appointments":"মাসভিত্তিক অ্যাপয়েন্টমেন্টসমূহ"%>
                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="kt_widget2_tab1_content2">
                                        <div class="kt-widget2">
                                            <div id="month6Div"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var context = '${context}';
    var pluginsContext = '${pluginsContext}';

    var dashboardDTOJson = '<%=new Gson().toJson( request.getAttribute( "dashboardDTO"  ) )%>';
    var dashboardDTO = JSON.parse(dashboardDTOJson);


    //var isAdmin = request.getAttribute("isAdmin");
</script>

<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

    // Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages': ['corechart']});
    google.charts.setOnLoadCallback(draw7DayChart);

    function draw7DayChart() {

        var data = google.visualization.arrayToDataTable([
            ['<%=LM.getText(LC.HM_DAY, loginDTO)%>',  '<%=Language.equalsIgnoreCase("english")?"Appointments":"অ্যাপয়েন্টমেন্টসমূহ"%>'],
            <%
            for(int j = 0; j <= 7; j ++)
            {
            	%>
            ['<%=Utils.getDigits(dashboardDTO.last7Days[j], Language)%>', <%=dashboardDTO.last7DayAppointments[j]%>],
            <%
        	}
        %>
        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Appointments of Last 7 Days ":"গত সাত দিনের অ্যাপয়েন্টমেন্টসমূহ"%>',
            legend: {position: 'top'}
        };

        var chart = new google.visualization.LineChart(document.getElementById('day7Div'));

        chart.draw(data, options);
    }

    google.charts.setOnLoadCallback(draw6MonthChart);

    function draw6MonthChart() {

        var data = google.visualization.arrayToDataTable([
            ['<%=LM.getText(LC.HM_MONTH, loginDTO)%>',  '<%=Language.equalsIgnoreCase("english")?"Appointments":"অ্যাপয়েন্টমেন্টসমূহ"%>'],
            <%
            for(int j = 0; j <= 6; j ++)
            {
            	%>
            ['<%=Utils.getDigits(dashboardDTO.last6Months[j], Language)%>', <%=dashboardDTO.last6MonthAppointments[j]%>],
            <%
        	}
        %>
        ]);

        var options = {
       		 title: '<%=Language.equalsIgnoreCase("english")?"Appointments of Last 6 Months":"গত ছয় মাসের অ্যাপয়েন্টমেন্টসমূহ"%>',
             legend: {position: 'top'}
       };
        var chart = new google.visualization.ColumnChart(document.getElementById('month6Div'));

        chart.draw(data, options);
    }
    
    
    google.charts.setOnLoadCallback(drawDrChartToday);

    function drawDrChartToday() {


        var data = google.visualization.arrayToDataTable([
            ['<%=LM.getText(LC.HM_DOCTOR, loginDTO)%>', '<%=LM.getText(LC.HM_APPOINTMENTS, loginDTO)%>', '<%=LM.getText(LC.HM_PRESCRIPTION, loginDTO)%>'],
            //[dashboardDTO.lastSevenDaysDocumentsCountDTO.last7thDayDate,  dashboardDTO.lastSevenDaysDocumentsCountDTO.last7thDayCount],
            <%
           for(int j = 0; j < dashboardDTO.doctorNamesToday.size(); j ++)
           {
        	   System.out.println("Dr name today in jst = " + dashboardDTO.doctorNamesToday.get(j));
        	   String drname = dashboardDTO.doctorNamesToday.get(j);
        	   if(drname.length() > 18)
        	   {
        		   drname = drname.substring(0, 18) + "..";
        	   }
               %>
            ['<%=dashboardDTO.drActiveToday.get(j)?"*":""%> <%=drname%>',
            	<%=dashboardDTO.doctorWiseAppointmentsToday.get(j)%>,
            	<%=dashboardDTO.doctorWiseServedPatientsToday.get(j)%>],
            <%
            }
            %>

        ]);

        var options = {
        	title: '<%=Language.equalsIgnoreCase("english")?"Doctorwise served patients today":"আজকের ডাক্তারভিত্তিক রোগীর সংখ্যা"%>',
            legend: {position: 'right'}
        };

        var chart = new google.visualization.BarChart(document.getElementById('divDrToday'));
        var chartContainer = document.getElementById('divDrToday');
        
        google.visualization.events.addListener(chart, 'ready', function () {
            var labels = chartContainer.getElementsByTagName('text');
            for (let i = 0; i < labels.length; i++) {
            	  if(labels[i].innerHTML.startsWith("*"))
           		  {
            		  labels[i].setAttribute('font-weight', 'Bold');
           		  }
            }
        
          
          });
        
        
        chart.draw(data, options);
    }
    
    
    google.charts.setOnLoadCallback(drawReceptionistsToday);

    function drawReceptionistsToday() {


        var data = google.visualization.arrayToDataTable([
        	
            <%
            if(dashboardDTO.receptionistWiseCountToday != null)
            {
            	%>
             	['<%=Language.equalsIgnoreCase("english")?"Receptionist":"রিসেপশনিস্ট"%>',
             		'<%=LM.getText(LC.HM_COUNT, loginDTO)%>'],
             <%
	           for(KeyCountDTO keyCountDTO: dashboardDTO.receptionistWiseCountToday)
	           {
	        	   
	                %>
	            		['<%=keyCountDTO.exists?"*":""%> <%=WorkflowController.getNameFromUserName(keyCountDTO.keyStr, Language)%>',
	            			<%=keyCountDTO.count%>],
	            	<%

	        	}
            }
        	%>

        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Receptionistwise Appointments Today":"আজকের রিসেপশনিস্টভিত্তিক অ্যাপয়েন্টমেন্ট সংখ্যা"%>',
            //curveType: 'function',
            legend: {position: 'left'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('divRec'));
        var chartContainer = document.getElementById('divRec');
        google.visualization.events.addListener(chart, 'ready', function () {
            var labels = chartContainer.getElementsByTagName('text');
            
            for (let i = 0; i < labels.length; i++) {
          	  if(labels[i].innerHTML.startsWith("*"))
         	  {
          		  labels[i].setAttribute('font-weight', 'Bold');
         	  }
          	}
          
          
          });

        chart.draw(data, options);
    }
</script>