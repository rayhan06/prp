<div class="kt-portlet__head">
    <div class="kt-portlet__head-label">
        <h3 class="kt-portlet__head-title">
            <%=LM.getText(LC.HM_MONTHWISE, loginDTO)%> <%=LM.getText(LC.HM_TICKET, loginDTO)%> <%=LM.getText(LC.HM_STATUS, loginDTO)%>
        </h3>
    </div>
</div>

<div class="kt-portlet__body">
    <div id="monthWiseTicketStatus" style="height: 300px;"></div>
</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

	google.charts.load('current', {'packages': ['corechart']});
    google.charts.setOnLoadCallback(monthWiseTicket);

    function monthWiseTicket() {

        var data = google.visualization.arrayToDataTable([
            ['Month', '<%=LM.getText(LC.HM_OPEN, loginDTO)%>', '<%=LM.getText(LC.HM_CLOSED, loginDTO)%>'],
            <%

            for(int j = 0; j <= 6; j ++)
            {
            	%>
            ['<%=Utils.getDigits(dashboardDTO.last6Months[j], Language)%>', dashboardDTO.last6MonthsOpenedTickets[<%=j%>], dashboardDTO.last6MonthsClosedTickets[<%=j%>]],
            <%
        }


        %>
        ]);

        var options = {

            legend: {position: 'Top'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('monthWiseTicketStatus'));

        chart.draw(data, options);
    }
</script>