<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div class="kt-portlet__head">
    <div class="kt-portlet__head-label">
        <h3 class="kt-portlet__head-title">
            <%="english".equalsIgnoreCase(language)? "Overtime & Food Bill Status" : "অধিকাল ও খাবার ভাতার বিলের অবস্থা"%>
        </h3>
    </div>
</div>
<div class="kt-portlet__body">
    <div style="height: 150px;">
        <%="english".equalsIgnoreCase(language)? "Overtime Bill" : "অধিকাল ভাতা"%>
        <div id="ot-bill-status"></div>
    </div>
    <div style="height: 150px;">
        <%="english".equalsIgnoreCase(language)? "Food Bill" : "খাবার ভাতা"%>
        <div id="food-bill-status"></div>
    </div>
</div>

<script type="text/javascript">
    const $otBillStatusTable = $('#ot-bill-status');
    const $foodBillStatusTable = $('#food-bill-status');

    $(document).ready(() => {
        setOvertimeBillStatus();
        setFoodBillStatus();
    })

    function setOvertimeBillStatus() {
        let url = "OT_bill_submission_configServlet?actionType=ajax_getActiveBillStatusForEmployeeDashboard";
        $.ajax({
            url: url,
            type: "GET",
            async: true,
            success: function (fetchedData) {
                $otBillStatusTable.html(fetchedData);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function setFoodBillStatus() {
        let url = "Food_bill_submission_configServlet?actionType=ajax_getActiveBillStatusForEmployeeDashboard";
        $.ajax({
            url: url,
            type: "GET",
            async: true,
            success: function (fetchedData) {
                $foodBillStatusTable.html(fetchedData);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
</script>