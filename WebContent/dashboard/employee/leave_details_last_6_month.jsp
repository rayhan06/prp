<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div class="kt-portlet__head">
    <div class="kt-portlet__head-label">
        <h3 class="kt-portlet__head-title">
            <%=LM.getText(LC.EMPLOYEE_DASHBOARD_LEAVES_OF_LAST_SIX_MONTHS, loginDTO)%>
        </h3>
    </div>
</div>
<div class="kt-portlet__body">
    <div id="bar-chart-leave-stat" style="height: 300px;"></div>
</div>

<script type="text/javascript">
    $(document).ready(()=>{
        getCountByLeaveType();
    })
    let countByLeaveType;
    function getCountByLeaveType(){
        let url = "Employee_leave_detailsServlet?actionType=getLeaveStatistic";
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: true,
            success: function (fetchedData) {
                countByLeaveType = fetchedData;
                console.log('countByLeaveType', countByLeaveType);
                google.charts.load('current', {'packages':['corechart']});
                google.charts.setOnLoadCallback(drawLeaveTypeCountChart);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function drawLeaveTypeCountChart() {
        const data = new google.visualization.DataTable();
        const xLabel = '<%="English".equalsIgnoreCase(language) ? "Leave Type" : "ছুটির ধরণ"%>';
        const yLabel = '<%="English".equalsIgnoreCase(language) ? "Count" : "সংখ্যা"%>';

        data.addColumn('string', xLabel);
        data.addColumn('number', yLabel);

        if(countByLeaveType){
            for(let i in countByLeaveType){
                data.addRows([[String(i),countByLeaveType[i]]]);
            }
        }

        const view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            { calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation" }
        ]);
        const options = {
            title: '',
            legend: { position: "none" }
        };
        const chart = new google.visualization.ColumnChart(document.getElementById('bar-chart-leave-stat'));
        chart.draw(view, options);
    }
</script>