<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div class="kt-portlet__head">
    <div class="kt-portlet__head-label">
        <h3 class="kt-portlet__head-title">
            <%=LM.getText(LC.EMPLOYEE_DASHBOARD_TRAINING_OF_LAST_SIX_MONTHS, loginDTO)%>
        </h3>
    </div>
</div>
<div class="kt-portlet__body">
    <div id="bar-chart-training-stat" style="height: 300px;"></div>
</div>

<script type="text/javascript">
    $(document).ready(() => {
        EmpTrainingCount();
    })
    let empTrainingCountFetchedData;

    function EmpTrainingCount() {
        const employeeRecordsId = $("#employeeRecordsId").val();
        if (employeeRecordsId) {
            let url = "EnrollmentServlet?actionType=getChart&employeeRecordId="+employeeRecordsId;
            console.log("url : " + url);
            $.ajax({
                url: url,
                type: "GET",
                async: true,
                success: function (fetchedData) {
                    empTrainingCountFetchedData = fetchedData;
                    google.charts.load('current', {'packages': ['corechart']});
                    google.charts.setOnLoadCallback(drawEmpTrainingCountChart);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }

    function drawEmpTrainingCountChart() {
        const data = new google.visualization.DataTable();
        data.addColumn('string', '<%="English".equalsIgnoreCase(language) ? "Training Type" : "প্রশিক্ষণ ধরণ"%>');
        data.addColumn('number', '<%="English".equalsIgnoreCase(language) ? "Count" : "সংখ্যা"%>');

        if(empTrainingCountFetchedData){
            for(let i in empTrainingCountFetchedData){
                data.addRows([[String(i),empTrainingCountFetchedData[i]]]);
            }
        }

        const view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            { calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation" }
        ]);
        const options = {
            title: '',
            legend: { position: "none" }
        };
        const chart = new google.visualization.ColumnChart(document.getElementById('bar-chart-training-stat'));
        chart.draw(view, options);
    }
</script>