
<%@page import="asset_category.*"%>
<%@page import="asset_model.*"%>
<%@ page language="java"%>
<%@ page import="java.util.*"%>
<%@ page import="pb.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%
AssetAssigneeDAO assetAssigneeDAO = new AssetAssigneeDAO();
List<AssetAssigneeDTO> userAssets = assetAssigneeDAO.getByOrganogramId(userDTO.organogramID);
String value = "";
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-portlet__head">
    <div class="kt-portlet__head-label">
        <h3 class="kt-portlet__head-title">
            <%=LM.getText(LC.HM_ASSETS, loginDTO)%>
        </h3>
    </div>
</div>

<div class="kt-portlet__body">
    <div  style="height: 300px;">
    	<table class="table table-bordered table-striped">
			<tr>
				<th><%=LM.getText(LC.ASSET_MODEL_ADD_BRANDTYPE, loginDTO)%></th>
				<th><%=LM.getText(LC.HM_MODEL, loginDTO)%></th>
				<th><%=LM.getText(LC.HM_SL, loginDTO)%></th>
				<th><%=LM.getText(LC.HM_ASSIGNMENT_DATE, loginDTO)%></th>
			</tr>			
			<%
			for(AssetAssigneeDTO assetAssigneeDTO: userAssets)
			{
				Asset_modelDTO asset_modelDTO = Asset_modelRepository.getInstance().getAsset_modelDTOByID(assetAssigneeDTO.assetModelId);
				if(asset_modelDTO !=null){
			%>
				<tr>
					<td>
					<%
							value = asset_modelDTO.brandType + "";
							%>
							<%
							value = CommonDAO.getName(Integer.parseInt(value), "brand", Language.equals("English")?"name_en":"name_bn", "id");
							%>
							<%=value%>
					</td>
				
					<td><%=asset_modelDTO.nameEn%></td>								
					<td><%=assetAssigneeDTO.sl%></td>								
					<td><%=Utils.getDigits(simpleDateFormat.format(new Date(assetAssigneeDTO.assignmentDate)), Language)%></td>
				</tr>
			<%
			}}
			%>
			
		</table>
    </div>
</div>