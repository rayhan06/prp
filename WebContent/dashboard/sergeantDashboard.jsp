<%@page import="workflow.WorkflowController"%>
<%@ page import="dashboard.DashboardDTO" %>
<%@ page import="java.util.List" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="pb.*" %>
<%@ page import="appointment.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String context = "../../.." + request.getContextPath() + "/";

    DashboardDTO dashboardDTO = (DashboardDTO) request.getAttribute("dashboardDTO");
    String Language = LM.getText(LC.SUPPORT_TICKET_EDIT_LANGUAGE, loginDTO);

%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=Language.equalsIgnoreCase("english")?"Serjeant-at-Arms Dashboard":"সার্জেন্ট-অ্যাট-আর্মস  ড্যাশবোর্ড"%>                
                 </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body bg-light">
            <div class="row">
                <div class="col-lg-4 my-3">
                    <div class="kt-portlet  shadow overflow-hidden dashboard-card-border-radius py-4"
                    >
                        <div class="row">
                            <div class="col-12 mb-1 mt-3 px-4 mx-2">
                                <span class="h4">
                                    <%=Language.equalsIgnoreCase("english")?"Today":"আজ"%>
                                </span>
                            </div>
                            <div class="col-12 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                <span class="h4 text-white">
                                                    <%=Language.equalsIgnoreCase("english")?"Requested":"আবেদনকৃত"%>
                                                </span>
                                            </div>
                                            <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                       	onclick = "location.href='Gate_passServlet?actionType=armsOfficeSearch&filter=requested&day=today'"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white">
                                                                        <h4><%=Utils.getDigits(dashboardDTO.pendingPassCounts.count2, Language)%></h4>
                                                                </button>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                   <span class="h4 text-white">
                                                       <%=Language.equalsIgnoreCase("english")?"Approved":"অনুমোদিত"%>
                                                	</span>
                                            </div>
                                            <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        onclick = "location.href='Gate_passServlet?actionType=armsOfficeSearch&filter=approved&day=today'"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white">
                                                            <h4><%=Utils.getDigits(dashboardDTO.approvedPassCounts.count2, Language)%></h4>
                                                        </button>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="col-12 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                   <span class="h4 text-white">
                                                       <%=Language.equalsIgnoreCase("english")?"Not Approved":"অননুমোদিত"%>
                                                	</span>
                                            </div>
                                            <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        onclick = "location.href='Gate_passServlet?actionType=armsOfficeSearch&filter=unapproved&day=today'"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white">
                                                            <h4><%=Utils.getDigits((dashboardDTO.pendingPassCounts.count2 - dashboardDTO.approvedPassCounts.count2), Language)%></h4>
                                                        </button>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                     <div class="col-lg-4 my-3">
                    <div class="kt-portlet  shadow overflow-hidden dashboard-card-border-radius py-4"
                    >
                        <div class="row">
                            <div class="col-12 mb-1 mt-3 px-4 mx-2">
                                <span class="h4">
                                    <%=Language.equalsIgnoreCase("english")?"Current Month":"বর্তমান মাসে"%>
                                </span>
                            </div>
                            <div class="col-12 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                <span class="h4 text-white">
                                                    <%=Language.equalsIgnoreCase("english")?"Requested":"আবেদনকৃত"%>
                                                </span>
                                            </div>
                                            <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        onclick = "location.href='Gate_passServlet?actionType=armsOfficeSearch&filter=requested&day=month'"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white">
                                                                        <h4><%=Utils.getDigits(dashboardDTO.pendingPassCounts.count3, Language)%></h4>
                                                                </button>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                   <span class="h4 text-white">
                                                       <%=Language.equalsIgnoreCase("english")?"Approved":"অনুমোদিত"%>
                                                	</span>
                                            </div>
                                            <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        onclick = "location.href='Gate_passServlet?actionType=armsOfficeSearch&filter=approved&day=month'"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white">
                                                            <h4><%=Utils.getDigits(dashboardDTO.approvedPassCounts.count3, Language)%></h4>
                                                        </button>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                              <div class="col-12 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                   <span class="h4 text-white">
                                                       <%=Language.equalsIgnoreCase("english")?"Not Approved":"অননুমোদিত"%>
                                                	</span>
                                            </div>
                                            <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        onclick = "location.href='Gate_passServlet?actionType=armsOfficeSearch&filter=unapproved&day=month'"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white">
                                                            <h4><%=Utils.getDigits(dashboardDTO.pendingPassCounts.count3 - dashboardDTO.approvedPassCounts.count3, Language)%></h4>
                                                        </button>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 my-3">
                    <div class="kt-portlet  shadow overflow-hidden dashboard-card-border-radius py-4"
                    >
                        <div class="row">
                            <div class="col-12 mb-1 mt-3 px-4 mx-2">
                                <span class="h4">
                                    <%=Language.equalsIgnoreCase("english")?"Total":"মোট"%>
                                </span>
                            </div>
                            <div class="col-12 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                <span class="h4 text-white">
                                                    <%=Language.equalsIgnoreCase("english")?"Requested":"আবেদনকৃত"%>
                                                </span>
                                            </div>
                                            <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        onclick = "location.href='Gate_passServlet?actionType=armsOfficeSearch&filter=requested'"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white">
                                                                        <h4><%=Utils.getDigits(dashboardDTO.pendingPassCounts.count, Language)%></h4>
                                                                </button>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                   <span class="h4 text-white">
                                                       <%=Language.equalsIgnoreCase("english")?"Approved":"অনুমোদিত"%>
                                                	</span>
                                            </div>
                                            <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        onclick = "location.href='Gate_passServlet?actionType=armsOfficeSearch&filter=approved'"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white">
                                                            <h4><%=Utils.getDigits(dashboardDTO.approvedPassCounts.count, Language)%></h4>
                                                        </button>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                              <div class="col-12 text-center my-2">
                                <div class="d-flex justify-content-center align-items-center">
                                    <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                        <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                            <div class="">
                                                   <span class="h4 text-white">
                                                       <%=Language.equalsIgnoreCase("english")?"Not Approved":"অননুমোদিত"%>
                                                	</span>
                                            </div>
                                            <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        onclick = "location.href='Gate_passServlet?actionType=armsOfficeSearch&filter=unapproved'"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white">
                                                            <h4><%=Utils.getDigits(dashboardDTO.pendingPassCounts.count - dashboardDTO.approvedPassCounts.count, Language)%></h4>
                                                        </button>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                   
                   <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow overflow-hidden dashboard-card-border-radius py-4"
                    >
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <!--Div that will hold the pie chart-->
                        <div class="grid-item">
                            <div id="chart1_div"></div>
                            <h4 class="text-center my-2">
                                 <%=Language.equalsIgnoreCase("english")?"Passes Approved on This Week":"এই সপ্তাহে অনুমোদিত পাসসমূহ"%>
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow overflow-hidden dashboard-card-border-radius py-4"
                    >
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <!--Div that will hold the pie chart-->
                        <div class="grid-item">
                            <div id="char2_div"></div>
                            <h4 class="text-center my-2">
                                 <%=Language.equalsIgnoreCase("english")?"Applied vs Approved vs Issued passes on last 6 Months":"গত ছয় মাসের আবেদনকৃত বনাম অনুমোদিত বনাম ইস্যুকৃত পাসসমূহ"%>
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow overflow-hidden dashboard-card-border-radius py-4"
                    >
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <!--Div that will hold the pie chart-->
                        <div class="grid-item">
                            <div id="char3_div"></div>
                            <h4 class="text-center my-2">
                                <%=Language.equalsIgnoreCase("english")?"Gate Pass Type-wise Chart":"প্রবেশ পাসের ধরনভিত্তিক চার্ট"%>
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 my-3">
                    <div class="kt-portlet  shadow overflow-hidden dashboard-card-border-radius py-4"
                    >
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <!--Div that will hold the pie chart-->
                        <div class="grid-item">
                            <div id="char4_div"></div>
                            <h4 class="text-center my-2">
                                <%=Language.equalsIgnoreCase("english")?"Serjeant-wise Chart":"সার্জেন্ট ভিত্তিক চার্ট"%>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var context = '${context}';
    var pluginsContext = '${pluginsContext}';

    var dashboardDTOJson = '<%=new Gson().toJson( request.getAttribute( "dashboardDTO"  ) )%>';
    var dashboardDTO = JSON.parse(dashboardDTOJson);


    var isAdmin = request.getAttribute("isAdmin");
</script>
<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

    // Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages': ['corechart']});
    google.charts.setOnLoadCallback(draw6);

    function draw6() {

        var data = google.visualization.arrayToDataTable([
            ['Month', '<%=Language.equalsIgnoreCase("english")?"Applied":"আবেদনকৃত"%>', '<%=Language.equalsIgnoreCase("english")?"Approved":"অনুমোদিত"%>', '<%=Language.equalsIgnoreCase("english")?"Issued":"ইস্যুকৃত"%>'],
            <%
            for(int j = 0; j <= 6; j ++)
            {
            	%>
            ['<%=Utils.getDigits(dashboardDTO.last6Months[j], Language)%>', dashboardDTO.last6MonthRequestedPasses[<%=j%>], dashboardDTO.last6MonthApprovedPasses[<%=j%>], dashboardDTO.last6MonthIssuesPasses[<%=j%>]],
            <%
        }
        %>
        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Applied vs Approved vs Issued passes on last 6 Months":"গত ছয় মাসের আবেদনকৃত বনাম অনুমোদিত বনাম ইস্যুকৃত পাসসমূহ"%>',
            legend: {position: 'bottom'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('char2_div'));

        chart.draw(data, options);
    }

    google.charts.setOnLoadCallback(drawApprovedWeek);

    function drawApprovedWeek() {


        var data = google.visualization.arrayToDataTable([
            ['Date', '<%=LM.getText(LC.HM_COUNT, loginDTO)%>'],
            //[dashboardDTO.lastSevenDaysDocumentsCountDTO.last7thDayDate,  dashboardDTO.lastSevenDaysDocumentsCountDTO.last7thDayCount],
            <%
           for(int j = 0; j <= 7; j ++)
           {
               %>
            ['<%=Utils.getDigits(dashboardDTO.last7Days[j], Language)%>', dashboardDTO.last7DayApprovedPasses[<%=j%>]],
            <%
        }
        %>

        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Passes Approved on This Week":"এই সপ্তাহে অনুমোদিত পাসসমূহ"%>',
            curveType: 'function',
            legend: {position: 'bottom'}
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart1_div'));

        chart.draw(data, options);
    }


    google.charts.setOnLoadCallback(drawTypeWise);

    function drawTypeWise() {

        var data = google.visualization.arrayToDataTable([
            ['Name', '<%=LM.getText(LC.HM_COUNT, loginDTO)%>'],
            <%
            for(KeyCountDTO keyCountDTO: dashboardDTO.typeWisePasses)
            {
            	%>
            ['<%=CommonDAO.getName(Language, "gate_pass_type", keyCountDTO.key)%>', <%=keyCountDTO.count%>],
            <%
			}
        %>
        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Gate Pass Type-wise Chart":"প্রবেশ পাসের ধরনভিত্তিক চার্ট"%>',
            legend: {position: 'bottom'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('char3_div'));

        chart.draw(data, options);


    }

    google.charts.setOnLoadCallback(drawUserWise);

    function drawUserWise() {

        var data = google.visualization.arrayToDataTable([
            ['Name', '<%=LM.getText(LC.HM_COUNT, loginDTO)%>'],
            <%
            for(KeyCountDTO keyCountDTO: dashboardDTO.userWisePasses)
            {
            	%>
            ['<%=WorkflowController.getNameFromUserName(keyCountDTO.keyStr, Language)%>', <%=keyCountDTO.count%>],
            <%
			}
        %>
        ]);

        var options = {
            title: '<%=Language.equalsIgnoreCase("english")?"Serjeant-wise Chart":"সার্জেন্ট ভিত্তিক চার্ট"%>',
            legend: {position: 'bottom'}
        };

        var chart = new google.visualization.PieChart(document.getElementById('char4_div'));

        chart.draw(data, options);


    }


</script>