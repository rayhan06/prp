<%--
  Created by IntelliJ IDEA.
  User: Jahin
  Date: 3/18/2021
  Time: 10:59 AM
--%>
<%@ page import="pb.OptionDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="pb.Utils" %>
<%@ page import="util.StringUtils" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="time.JSTimeConfig" %>
<%@ page import="java.nio.charset.StandardCharsets" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    OptionDTO optionDTO;
    Date curDate = new Date();
    String language = "Bangla";
    if (request.getParameter(JSTimeConfig.LANGUAGE.toString()) != null) {
        language = request.getParameter(JSTimeConfig.LANGUAGE.toString());
    }

    String timeId = "jsTime";
    if (request.getParameter(JSTimeConfig.TIME_ID.toString()) != null) {
        timeId = request.getParameter(JSTimeConfig.TIME_ID.toString());
    }

    String maxTime = "23:60:00";
    if (request.getParameter(JSTimeConfig.MAX_TIME.toString()) != null) {
        maxTime = request.getParameter(JSTimeConfig.MAX_TIME.toString());
    }

    String minTime = "00:00:00";
    if (request.getParameter(JSTimeConfig.MIN_TIME.toString()) != null) {
        minTime = request.getParameter(JSTimeConfig.MIN_TIME.toString());
    }

    boolean isampm = true;
    if (request.getParameter(JSTimeConfig.IS_AMPM.toString()) != null) {
        isampm = request.getParameter(JSTimeConfig.IS_AMPM.toString()).equalsIgnoreCase("true");
    }

    int colLen = 4;
    if(!isampm) colLen = 6;

    List<OptionDTO> hourOptionDTOList = new ArrayList<>();
    optionDTO = new OptionDTO("Hour","ঘণ্টা","24");
    hourOptionDTOList.add(optionDTO);
    int hstart = 1, hupto = 12;
    if(!isampm) {
        hstart = 0;
        hupto = 23;
    }
    for(int x = hstart; x <= hupto; x++){
        String s = x < 10 ? "0" + x : String.valueOf(x);
        optionDTO = new OptionDTO(s,StringUtils.convertToBanNumber(s),String.valueOf(x));
        hourOptionDTOList.add(optionDTO);
    }

    List<OptionDTO> minuteOptionDTOList = new ArrayList<>();
    optionDTO = new OptionDTO("Minute","মিনিট","61");
    minuteOptionDTOList.add(optionDTO);
    for(int x = 0; x < 60; x+=1){
        String s = x < 10 ? "0" + x : String.valueOf(x);
        optionDTO = new OptionDTO(s,StringUtils.convertToBanNumber(s),String.valueOf(x));
        minuteOptionDTOList.add(optionDTO);
    }

    List<OptionDTO> ampmOptionDTOList = new ArrayList<>();
    optionDTO = new OptionDTO("AM","পূর্বাহ্ণ","0");
    ampmOptionDTOList.add(optionDTO);
    optionDTO = new OptionDTO("PM","অপরাহ্ণ","1");
    ampmOptionDTOList.add(optionDTO);
%>


<div class=" timepicker-container" id="<%=timeId%>">
    <div class="row">
        <div class="col-lg-<%=colLen%> " id='hourSelect_div_<%=timeId%>'>
            <select class='form-control hourSelection rounded' name='hourSelection<%=timeId%>' id='hourSelection<%=timeId%>' tag='pb_html'>
                <%=Utils.buildOptionsWithoutSelectWithSelectId(hourOptionDTOList,language,"24")%>
            </select>
        </div>

        <div class="col-lg-<%=colLen%> " id='minuteSelect_div_<%=timeId%>'>
            <select class='form-control minuteSelection rounded' name='minuteSelection<%=timeId%>' id='minuteSelection<%=timeId%>' tag='pb_html'>
                <%=Utils.buildOptionsWithoutSelectWithSelectId(minuteOptionDTOList,language,"61")%>
            </select>
        </div>

        <%
            if(isampm) {
        %>
        <div class="col-lg-4 " id='ampmSelect_div_<%=timeId%>'>
            <select class='form-control ampmSelection rounded' name='ampmSelection<%=timeId%>' id='ampmSelection<%=timeId%>' tag='pb_html'>
                <%=Utils.buildOptionsWithoutSelectWithSelectId(ampmOptionDTOList,language,"0")%>
            </select>
        </div>

        <%
            }
        %>

        <span id='error<%=timeId%>' style="color:#fd397a; text-align:center; margin-left:12px; margin-top: 1%; font-size: smaller"></span>
    </div>

    <input type='hidden' class='form-control timepicker-language' name='language_<%=timeId%>' id='language_<%=timeId%>' value ='<%=language%>' tag='pb_html'/>
    <input type='hidden' class='form-control timepicker-max-time' name='max_time_<%=timeId%>' id='max_time_<%=timeId%>' value ='<%=maxTime%>' tag='pb_html'/>
    <input type='hidden' class='form-control timepicker-min-time' name='min_time_<%=timeId%>' id='min_time_<%=timeId%>' value ='<%=minTime%>' tag='pb_html'/>

</div>

<script type="text/javascript">

    $(document).ready(function () {

    });

    function init<%=timeId%>(row) {
        $("#hourSelection<%=timeId%>").select({
            dropdownAutoWidth: true
        });
        $("#minuteSelection<%=timeId%>").select({
            dropdownAutoWidth: true
        });
        $("#ampmSelection<%=timeId%>").select({
            dropdownAutoWidth: true
        });
    }


    var row<%=timeId%> = 0;

    window.onload = function () {
        init<%=timeId%>(row<%=timeId%>);
    }
</script>
