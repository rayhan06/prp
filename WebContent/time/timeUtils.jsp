<%--
  Created by IntelliJ IDEA.
  User: Jahin
  Date: 3/18/2021
  Time: 12:30 PM
--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%--<script src="moment.js"></script>--%>
<%@page pageEncoding="UTF-8" %>
<script type="text/javascript">

    function timeNotEmptyValidator(id, obj){
        let error = '';
        let errorEn = 'Please select a time!', errorBn = 'দয়া করে সময় নির্ধারণ করুন!';
        const language = $('#language_'+id).prop('value');
        if(obj !== undefined){
            errorEn = obj.errorEn;
            errorBn = obj.errorBn;
        }
        let hour = parseInt($('#hourSelection'+id).prop('value'));
        let minute = parseInt($('#minuteSelection'+id).prop('value'));
        if(hour > 23 || minute > 60){
            error = language.localeCompare('English') == 0 ? errorEn : errorBn;
            $('#error'+id).text(error);
            return false;
        }
        else{
            $('#error'+id).empty();
        }
        return true;
    }

    function maxminValidator(id, obj){
        let error = '';
        const language = $('#language_'+id).prop('value');
        let errorEn = 'Please select a valid time!', errorBn = 'দয়া করে সঠিক সময় নির্ধারণ করুন!';
        let maxTime = $('#max_time_'+id).prop('value'), minTime = $('#min_time_'+id).prop('value');
        if(obj !== undefined){
            if(obj.errorEn != undefined) errorEn = obj.errorEn;
            if(obj.errorBn != undefined) errorBn = obj.errorBn;
            if(obj.maxTime != undefined) maxTime = obj.maxTime;
            if(obj.minTime != undefined) minTime = obj.minTime;
        }
        let hour = parseInt($('#hourSelection'+id).prop('value'));
        let minute = parseInt($('#minuteSelection'+id).prop('value'));
        if(document.getElementById('ampmSelection'+id) !== null){
            if(hour == 12) {
                hour = 0;
            }
            if($('#ampmSelection'+id).prop('value').localeCompare('1') == 0){
                hour += 12;
            }
        }
        if(parseInt($('#hourSelection'+id).prop('value')) < 24 &&  parseInt($('#minuteSelection'+id).prop('value')) < 61 &&
            (hour > parseInt(maxTime.split(':')[0]) || hour < parseInt(minTime.split(':')[0]) || (hour == parseInt(maxTime.split(':')[0]) && (minute > parseInt(maxTime.split(':')[1]) || minute < parseInt(minTime.split(':')[1]))))){
                error = language.localeCompare('English') == 0 ? errorEn : errorBn;
                $('#error'+id).text(error);
                return false;
            }
        else{
            $('#error'+id).empty();
        }
        return true;
    }

    function getTimeById(id, ampm = false) {
        let hour = parseInt($('#hourSelection'+id).prop('value'));
        let minute = parseInt($('#minuteSelection'+id).prop('value'));
        let  ampmstr = 'AM';
        if(isNaN(hour) || isNaN(minute) || hour > 23 || minute > 60) return '';
        if(document.getElementById('ampmSelection'+id) !== null){
            if(hour >= 12) {
                hour -= 12;
            }
            if($('#ampmSelection'+id).prop('value').localeCompare('1') == 0){
                hour += 12;
            }
        }

        if(ampm){
            if(hour >= 12)
                ampmstr = 'PM';

            if(hour == 0)
                hour = 12;

            if(hour > 12)
                hour -= 12;

            return (hour < 10 ? '0'+hour : hour) + ':' + (minute < 10 ? '0'+minute : minute) + " " + ampmstr;
        }

        return (hour < 10 ? '0'+hour : hour) + ':' + (minute < 10 ? '0'+minute : minute) + ':00';
    }

    function setTimeById(id, time, ampm = false){
    	if(time == '')
   		{
   			return;
   		}
    	if(time.includes('AM') || time.includes('PM'))
   		{
    		ampm = true;
   		}
        let hour = parseInt(time.split(' ')[0].split(':')[0]);
        let minute = parseInt(time.split(' ')[0].split(':')[1]);
        if(ampm) {
            document.getElementById('hourSelection'+id).value = hour.toString();
            document.getElementById('minuteSelection'+id).value = minute.toString();
            document.getElementById('ampmSelection' + id).value = time.split(' ')[1].toLowerCase().localeCompare('am');
        }

        else {
            if (document.getElementById('ampmSelection' + id) != null) {
                if (hour >= 12)
                    document.getElementById('ampmSelection' + id).value = '1';
                else
                    document.getElementById('ampmSelection' + id).value = '0';

                if (hour == 0)
                    hour = 12;
                if (hour > 12)
                    hour -= 12;
            }

            document.getElementById('hourSelection' + id).value = hour.toString();
            document.getElementById('minuteSelection' + id).value = minute.toString();
        }
    }

    // time should be "hh:mm:ss" in 24 hour format
    function setMaxTimeById(id, time){
        document.getElementById("max_time_"+id).value = time;
        // document.getElementById('hourSelection'+id).value = '24';
        // document.getElementById('minuteSelection'+id).value = '61';
        //refreshTimeOnMinMax(id);
        maxminValidator(id);
    }

    function setMinTimeById(id, time){
        document.getElementById("min_time_"+id).value = time;
        // document.getElementById('hourSelection'+id).value = '24';
        // document.getElementById('minuteSelection'+id).value = '61';
        //refreshTimeOnMinMax(id);
        maxminValidator(id);
    }

    function resetTimeById(id){
        document.getElementById('hourSelection'+id).value = '24';
        document.getElementById('minuteSelection'+id).value = '61';
        document.getElementById('ampmSelection'+id).value = '0';
    }

    function refreshTimeOnMinMax(id){
        let maxh = parseInt($('#max_time_'+id).prop('value').split(':')[0]);
        let maxm = parseInt($('#max_time_'+id).prop('value').split(':')[1]);
        let minh = parseInt($('#min_time_'+id).prop('value').split(':')[0]);
        let minm = parseInt($('#min_time_'+id).prop('value').split(':')[1]);
        const ampm = maxh < 12 ? '0' : '1';
        if(document.getElementById('ampmSelection'+id) !== null) {
            $($("#ampmSelection"+id+" option")[0]).prop('disabled',false);
            $($("#ampmSelection"+id+" option")[1]).prop('disabled',false);
            if(maxh < 12) $($("#ampmSelection"+id+" option")[1]).prop('disabled',true);
            if(minh >= 12) $($("#ampmSelection"+id+" option")[0]).prop('disabled',true);
            if($("#ampmSelection"+id).prop('value') == '1') {
                if(maxh >= 12) maxh -= 12;
                if(minh >= 12) minh -= 12;
            }

            for(let i = 1; i <= 12; i++)
                $("#hourSelection"+id+" option[value='"+i.toString()+"']").prop('disabled',false);

            for(let i = maxh+1; i <= 11; i++) {
                $("#hourSelection" + id + " option[value='" + i.toString() + "']").prop('disabled', true);
            }
            for(let i = minh-1; i >= 1; i--) {
                $("#hourSelection" + id + " option[value='" + i.toString() + "']").prop('disabled', true);
            }
            if(minh == 1) {
                $("#hourSelection" + id + " option[value='12']").prop('disabled', true);
            }

            for(let i = 0; i <= 60; i+=5)
                $("#minuteSelection"+id+" option[value='"+i.toString()+"']").prop('disabled',false);

            if((maxh != 12 && parseInt($('#hourSelection'+id).prop('value')) == maxh) ||
                (parseInt($('#hourSelection'+id).prop('value')) == '12' && maxh == 0 && $('#ampmSelection'+id).prop('value') == ampm)){
                for(let i = maxm+1; i <= 60; i++)
                    $("#minuteSelection"+id+" option[value='"+i.toString()+"']").prop('disabled',true);
            }
            if((minh != 12 && parseInt($('#hourSelection'+id).prop('value')) == minh) ||
                (parseInt($('#hourSelection'+id).prop('value')) == '12' && minh == 0 && $('#ampmSelection'+id).prop('value') == ampm)){
                for(let i = minm-1; i >= 0; i--)
                    $("#minuteSelection"+id+" option[value='"+i.toString()+"']").prop('disabled',true);
            }
        }
        else{
            for(let i = 0; i <= 23; i++)
                $("#hourSelection"+id+" option[value='"+i.toString()+"']").prop('disabled',false);

            for(let i = 0; i <= 60; i+=5)
                $("#minuteSelection"+id+" option[value='"+i.toString()+"']").prop('disabled',false);

            for(let i = maxh+1; i <= 23; i++) {
                $("#hourSelection" + id + " option[value='" + i.toString() + "']").prop('disabled', true);
            }

            for(let i = minh-1; i >= 0; i--) {
                $("#hourSelection" + id + " option[value='" + i.toString() + "']").prop('disabled', true);
            }

            if(parseInt($('#hourSelection'+id).prop('value')) == maxh){
                for(let i = maxm+1; i <= 60; i++)
                    $("#minuteSelection"+id+" option[value='"+i.toString()+"']").prop('disabled',true);
            }
            if(parseInt($('#hourSelection'+id).prop('value')) == minh){
                for(let i = minm-1; i >= 0; i--)
                    $("#minuteSelection"+id+" option[value='"+i.toString()+"']").prop('disabled',true);
            }
        }

    }

    function createTimePicker(divId, paramObject ){
        createDatePicker(divId, paramObject, function (){});
    }

    function createTimePicker(divId, paramObject, completeCallback){
        let url = "TimeServlet?";
        for(let key in paramObject){
            url += key + "=" + paramObject[key] + "&";
        }
        $.ajax({
            url: url,
            type: "GET",
            async: true,
            success: function (data) {
                $('#'+divId).append(data);
            },
            error: function (error) {
                console.log(error);
            },
            complete: completeCallback
        });
    }

    document.addEventListener('DOMContentLoaded', () => {
        $(document).on('change', '.hourSelection', function () {
            let timePickerDiv = $(this).closest("div.timepicker-container");
            //refreshTimeOnMinMax($(timePickerDiv).prop('id'));
            maxminValidator($(timePickerDiv).prop('id'));
            const id = $(this).prop('id');
            const hour = $(timePickerDiv).find('.hourSelection').prop('value');
            const minute = $(timePickerDiv).find('.minuteSelection').prop('value');
            let ampm = '';
            if(document.getElementById($(timePickerDiv).find('.ampmSelection').prop('id')) !== null)
                ampm = $(timePickerDiv).find('.ampmSelection').prop('value');
            $('#'+id).trigger('timepicker.change', [{
                'changed' : id.split('Selection')[0],
                'data' : {
                    'hour' : hour,
                    'minute' : minute,
                    'ampm' : ampm
                }
            }]);
        });

        $(document).on('change', '.minuteSelection', function () {
            let timePickerDiv = $(this).closest("div.timepicker-container");
            //refreshTimeOnMinMax($(timePickerDiv).prop('id'));
            maxminValidator($(timePickerDiv).prop('id'));
            const id = $(this).prop('id');
            const hour = $(timePickerDiv).find('.hourSelection').prop('value');
            const minute = $(timePickerDiv).find('.minuteSelection').prop('value');
            let ampm = '';
            if(document.getElementById($(timePickerDiv).find('.ampmSelection').prop('id')) !== null)
                ampm = $(timePickerDiv).find('.ampmSelection').prop('value');
            $('#'+id).trigger('timepicker.change', [{
                'changed' : id.split('Selection')[0],
                'data' : {
                    'hour' : hour,
                    'minute' : minute,
                    'ampm' : ampm
                }
            }]);
        });

        $(document).on('change', '.ampmSelection', function () {
            let timePickerDiv = $(this).closest("div.timepicker-container");
            //refreshTimeOnMinMax($(timePickerDiv).prop('id'));
            maxminValidator($(timePickerDiv).prop('id'));
            const id = $(this).prop('id');
            const hour = $(timePickerDiv).find('.hourSelection').prop('value');
            const minute = $(timePickerDiv).find('.minuteSelection').prop('value');
            let ampm = '';
            if(document.getElementById($(timePickerDiv).find('.ampmSelection').prop('id')) !== null)
                 ampm = $(timePickerDiv).find('.ampmSelection').prop('value');
            $('#'+id).trigger('timepicker.change', [{
                'changed' : id.split('Selection')[0],
                'data' : {
                    'hour' : hour,
                    'minute' : minute,
                    'ampm' : ampm
                }
            }]);
        });
    });
</script>