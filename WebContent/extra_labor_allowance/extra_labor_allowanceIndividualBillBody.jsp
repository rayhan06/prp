<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="util.*" %>
<%@ page import="static util.StringUtils.convertToBanNumber" %>
<%@ page import="static util.StringUtils.convertToBanNumber" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="extra_labor_allowance.Extra_labor_allowanceDTO" %>
<%@ page import="extra_labor_allowance.Extra_labor_allowanceModel" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberInWord" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getLanguage(loginDTO);

    Extra_labor_allowanceDTO allowanceDTO = (Extra_labor_allowanceDTO) request.getAttribute("allowanceDTO");
    Extra_labor_allowanceModel allowanceModel = new Extra_labor_allowanceModel(allowanceDTO, "Bangla");
    String pdfFileName = "Over time "
            + DateUtils.getMonthYear(allowanceDTO.monthYear, "English", " ") + " "
            + allowanceModel.name + " " + allowanceModel.organogramName;
%>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    .fix-fill {
        overflow: hidden;
        white-space: nowrap;
    }

    #to-print-div * {
        font-size: 12px;
    }

    #to-print-div h1 {
        font-size: 16px;
        font-weight: bold;
    }

    #to-print-div h2 {
        font-size: 14px;
        font-weight: bold;
    }

    #to-print-div h3 {
        font-size: 13px;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .25in .4in;
        background: white;
        margin-bottom: 10px;
    }

    .page[data-size="A4-landscape"] {
        width: 297mm;
        height: 210mm;
        padding: .4in;
        background: white;
        margin-bottom: 10px;
    }

    .foot-note {
        font-size: 11px !important;
    }

    .blank-to-fill {
        display: inline-block;
        border-bottom: 1px dotted black;
        width: inherit;
    }

    .table-bordered > :not(caption) > * {
        border-color: black;
    }

    .table-bordered > :not(caption) > * > * {
        border-color: black;
    }

    .table-bordered td,
    .table-bordered th {
        padding: 5px;
    }

    th {
        text-align: center;
    }

    table {
        table-layout: fixed;
    }
</style>

<div class="kt-content p-0" id="kt_content">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title prp-page-title">
                        <%=UtilCharacter.getDataByLanguage(Language, "খাটুনি ভাতা বিল", "Extra Labor Allowance Bill")%>
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body" id="bill-div">
                <div class="ml-auto m-5">
                    <button type="button" class="btn" id='download-pdf'
                            onclick="downloadTemplateAsPdf('to-print-div', '<%=pdfFileName%>');">
                        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                </div>

                <div style="margin: auto;">
                    <%--Reference Documents from Parliament\Kamal 01711009510\Over Time February 2021.docx--%>
                    <div class="container" id="to-print-div">
                        <section class="page shadow" data-size="A4-landscape">
                            <div class="text-center">
                                <h1>বাংলাদেশ জাতীয় সংসদ</h1>
                                <h2 class="mt-4">
                                    মাননীয় সংসদ
                                    সদস্য....................................................................................................................
                                    নির্বাচনী এলাকা
                                </h2>
                            </div>


                            <div class="mt-4">
                                <h3 class="text-center">
                                    <%=DateUtils.getMonthYear(allowanceDTO.monthYear, "Bangla", "/")%> মাসের অতিরিক্ত
                                    খাটুনি ভাতা বিল
                                </h3>

                                <p class="mt-3">
                                    বাংলাদেশ জাতীয় সংসদ সচিবালয়ের মানব সম্পদ শাখা-২ এর ২৮ জুন ২০১৮ তারিখের ১১.০০.০০০০.৮০৪.০২.০০২.১২.২৮৫&nbsp;
                                    নম্বর মঞ্জুরী আদেশ মোতাবেক ক-সংসদ অধিবেশন উপলক্ষে <%=allowanceModel.dailyRateKa%>/- এবং&nbsp;
                                    খ- কমিটি বৈঠক ও অন্যান্য অতিরিক্ত কাজের জন্য <%=allowanceModel.dailyRateKha%>/- টাকা হারে উপস্থিতির বিবরণী:
                                </p>

                                <table class="table-bordered mt-2 w-100 text-center">
                                    <thead>
                                    <tr>
                                        <th width="26%" rowspan="2">নাম ও পদবী</th>
                                        <th width="22%" colspan="2">(ক) সংসদ অধিবেশনের কাজ</th>
                                        <th width="22%" colspan="2">(খ) অন্যান্য জরুরি কাজ</th>
                                        <th width="17%" rowspan="2">সর্বমোট টাকা</th>
                                        <th width="13%" rowspan="2">প্রাপকের স্বাক্ষর</th>
                                    </tr>
                                    <tr>
                                        <th width="8.8%">ক দিন
                                        </th>
                                        <th width="13.2%">
                                            <%=allowanceModel.dailyRateKa%>/- হারে মোট টাকা
                                        </th>
                                        <th width="8.8%">খ দিন
                                        </th>
                                        <th width="13.2%">
                                            <%=allowanceModel.dailyRateKha%>/- হারে মোট টাকা
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr style="height: 150px;">
                                        <td width="26%">
                                            <%=allowanceModel.name%><br><br>
                                            <%=allowanceModel.organogramName%>
                                        </td>
                                        <td width="8.8%">
                                            <%=allowanceModel.dayKa%>
                                        </td>
                                        <td width="13.2%">
                                            <%=allowanceModel.dailyRateKa%> x <%=allowanceModel.dayKa%> = <br>
                                            <%=convertToBanNumber(String.valueOf(allowanceDTO.dailyRateKa * allowanceDTO.dayKa))%>/-
                                        </td>
                                        <td width="8.8%">
                                            <%=allowanceModel.dayKha%>
                                        </td>
                                        <td width="13.2%">
                                            <%=allowanceModel.dailyRateKha%> x <%=allowanceModel.dayKha%> = <br>
                                            <%=convertToBanNumber(String.valueOf(allowanceDTO.dailyRateKha * allowanceDTO.dayKha))%>/-
                                        </td>
                                        <td width="17%">
                                            <%=allowanceModel.totalAmount%>/-
                                        </td>
                                        <td width="13%"></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="text-center mt-4" style="width: 80%; margin: auto;">
                                    কথায়= <%=BangladeshiNumberInWord.convertToWord(allowanceModel.totalAmount)%>টাকা (মাত্র)
                                </div>
                            </div>

                            <p class="mt-4">
                                প্রত্যয়ন করা যাইতেছে যে, উপরোল্লিখিত কর্মকর্তা/কর্মচারীবৃন্দ জাতীয় সংসদ অধিবেশন/কমিটি
                                বৈঠক ও অন্যান্য
                                জরুরী কাজের জন্য জনস্বার্থে কর্তৃপক্ষের অনুমতিক্রমে অপর পৃষ্ঠায় বর্ণিত তারিখে অফিস সময়ের
                                পর অতিরিক্ত সময়
                                কর্মরত ছিলেন বিধায় অতিরিক্ত খাটুনি বিলটি পরিশোধের জন্য সুপারিশ করা হলো।
                            </p>

                            <div class="row">
                                <div class="col-9"></div>
                                <div class="col-3">
                                    প্রতিস্বাক্ষর
                                </div>
                            </div>

                            <div style="margin-top: 60px;">
                                <div class="row">
                                    <div class="col-3">
                                        <span style="border-bottom: 1px solid black;">
                                            <%=allowanceModel.organogramName%>
                                        </span>
                                    </div>
                                    <div class="col-6 text-center mt-4">
                                        সর্বমোট = <%=allowanceModel.totalAmount%>/-<br><br>
                                        কথায় = <%=BangladeshiNumberInWord.convertToWord(allowanceModel.totalAmount)%>টাকা (মাত্র)<br><br>
                                        বিলটি ঠিক আছে এবং পাশ করা হলো।
                                    </div>
                                    <div class="col-3 text-right">
                                        <span style="border-bottom: 1px solid black;">
                                            উপ-সচিব (অর্থ ও বাজেট ব্যবস্থাপনা)
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="foot-note text-center mt-5">
                                <div>
                                    সহকারী সচিব (অর্থ-১)<br>
                                    জাতীয় সংসদ সচিবালয়, ঢাকা।
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
</div>

<script>
    function downloadTemplateAsPdf(divId, fileName, orientation='landscape') {
        let content = document.getElementById(divId);
        const opt = {
            margin: 0.5,
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4', orientation: orientation}
        };
        html2pdf().from(content).set(opt).save();
    }
</script>