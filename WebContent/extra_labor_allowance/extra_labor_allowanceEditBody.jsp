<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="extra_labor_allowance.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="allowance_configure.AllowanceCatEnum" %>

<%
    String tableName = "extra_labor_allowance";
%>

<%@include file="../pb/addInitializer.jsp" %>

<%
    String formTitle = LM.getText(LC.EXTRA_LABOR_ALLOWANCE_ADD_EXTRA_LABOR_ALLOWANCE_ADD_FORMNAME, loginDTO);
    String servletName = "Extra_labor_allowanceServlet";
    String context = request.getContextPath() + "/";
    boolean isLangEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
%>

<style>
    .template-row {
        display: none;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal" id="extra-labor-allowance-form" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12 row mt-2">
                        <div class="col-lg-4 col-md-6 form-group">
                            <label class="h5" for="budgetInstitutionalGroup">
                                <%=LM.getText(LC.BUDGET_INSTITUTIONAL_GROUP, loginDTO)%>
                            </label>
                            <select id="budgetInstitutionalGroup"
                                    name='budgetInstitutionalGroupId'
                                    class='form-control rounded shadow-sm'
                                    onchange="institutionalGroupChanged(this);"
                            >
                                <%=Budget_institutional_groupRepository.getInstance().buildOptions(
                                        Language, 0L, false
                                )%>
                            </select>
                        </div>

                        <div class="col-lg-4 col-md-6 form-group">
                            <label class="h5" for="budgetOffice">
                                <%=LM.getText(LC.BUDGET_OFFICE, loginDTO)%>
                            </label>
                            <select id="budgetOffice" name='budgetOfficeId'
                                    class='form-control rounded shadow-sm'
                                    onchange="budgetOfficeChanged(this);">
                                <%--Dynamically Added with AJAX--%>
                            </select>
                        </div>

                        <div class="col-lg-4 col-md-6 form-group">
                            <label class="h5" for="budgetOffice">
                                <%=LM.getText(LC.EXTRA_LABOR_ALLOWANCE_ADD_MONTHYEAR, loginDTO)%>
                            </label>
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="monthYear_js"/>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                <jsp:param name="HIDE_DAY" value="true"/>
                            </jsp:include>
                            <input type='hidden' name='monthYear' id='monthYear' value=''>
                        </div>
                    </div>
                </div>

                <div class="mt-4 table-responsive">
                    <table id="allowance-table" class="table table-bordered table-striped text-nowrap">
                        <thead>
                        <tr>
                            <th rowspan="2">
                                <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_EMPLOYEERECORDSID, loginDTO)%>
                            </th>
                            <th rowspan="2">
                                <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_ORGANOGRAMID, loginDTO)%>
                            </th>
                            <th rowspan="2">
                                <%=isLangEnglish ?"Mobile Number" :"মোবাইল নাম্বার"%>
                            </th>
                            <th rowspan="2">
                                <%=isLangEnglish?"Savings Account Number":"সঞ্চয়ী হিসাব নাম্বার"%>
                            </th>
                            <th colspan="2" class="text-center">
                                <%=isLangEnglish?"KA - Parliament Session":"ক - সংসদ অধিবেশন"%>
                            </th>
                            <th colspan="2" class="text-center">
                                <%=isLangEnglish?"KHA - Committee Meeting":"খ - কমিটি বৈঠক"%>
                            </th>
                            <th rowspan="2">
                                <%=isLangEnglish?"Total Amount":"মোট টাকার পরিমাণ"%>
                            </th>
                            <th rowspan="2">
                                <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_REVENUESTAMPDEDUCTION, loginDTO)%>
                            </th>
                            <th rowspan="2">
                                <%=isLangEnglish?"Net Amount":"নীট দাবী"%>
                            </th>
                        </tr>
                        <tr>
                            <th>
                                <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_DAILYRATE, loginDTO)%>
                            </th>
                            <th>
                                <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_DAY, loginDTO)%>
                            </th>
                            <th>
                                <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_DAILYRATE, loginDTO)%>
                            </th>
                            <th>
                                <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_DAY, loginDTO)%>
                            </th>
                        </tr>
                        </thead>
                        <tbody class="main-tbody">
                        </tbody>

                        <%--don't put these tr inside tbody--%>
                        <tr class="loading-gif" style="display: none;">
                            <td class="text-center" colspan="100%">
                                <img alt="" class="loading"
                                     src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
                                <span><%=isLangEnglish?"Loading...":"লোড হচ্ছে..."%></span>
                            </td>
                        </tr>
                        <tr class="template-row">
                            <input type="hidden" name="employeeRecordsId" value="-1">
                            <input type="hidden" name="extraLaborAllowanceId" value="-1">
                            <td class="row-data-name"></td>
                            <td class="row-data-organogramName"></td>
                            <td class="row-data-mobileNumber"></td>
                            <td class="row-data-savingAccountNumber"></td>
                            <td class="row-data-dailyRateKa"></td>
                            <td class="row-data-dayKa"></td>
                            <td class="row-data-dailyRateKha"></td>
                            <td class="row-data-dayKha"></td>
                            <td class="row-data-totalAmount"></td>
                            <td class="row-data-revenueStampDeduction"></td>
                            <td class="row-data-netAmount"></td>
                        </tr>
                    </table>
                </div>

                <div class="row">
                    <div class="col-12 mt-3 text-right">
                        <button id="submit-btn"
                                class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                                type="button" onclick="submitForm()">
                            <%=isLangEnglish?"Prepare Bill":"বিল প্রস্তুত করুন"%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<%@include file="../common/table-sum-utils.jsp" %>

<script type="text/javascript">
    let actionName = 'add';
    const form = $('#extra-labor-allowance-form');

    function showOrHideLoadingGif(tableId, toShow) {
        const loadingGif = $('#' + tableId + ' tr.loading-gif');
        if (toShow) {
            document.querySelector('#' + tableId + ' tbody.main-tbody').innerHTML = '';
            document.querySelectorAll('#' + tableId + ' tfoot').forEach(tfoot => tfoot.remove());
            loadingGif.show();
        } else loadingGif.hide();
    }

    function clearOffice() {
        document.getElementById('budgetOffice').innerHTML = '';
    }

    function clearExtraLaborCat() {
        document.getElementById('extraLaborCat').value = '';
    }

    function clearMonthYear() {
        resetDateById('monthYear_js');
    }

    function clearTable() {
        const tableBody = document.querySelector('#allowance-table tbody.main-tbody');
        const emptyTableText = '<%=isLangEnglish?"No data found": "কোন তথ্য পাওয়া যায়নি"%>';
        tableBody.innerHTML = '<tr class="text-center"><td colspan="100%">'
            + emptyTableText + '</td></tr>';

        document.querySelectorAll('#allowance-table tfoot')
            .forEach(tfoot => tfoot.remove());
    }

    function clearNextLevels(startIndex) {
        const toClearFunctions = [
            clearOffice, clearMonthYear, clearTable
        ];
        for (let i = startIndex; i < toClearFunctions.length; i++) {
            toClearFunctions[i]();
        }
    }

    async function institutionalGroupChanged(selectElement) {
        const selectedInstitutionalGroupId = selectElement.value;
        clearNextLevels(0);
        if (selectedInstitutionalGroupId === '') return;
        buttonStateChange(true);

        const url = 'Budget_mappingServlet?actionType=getBudgetOfficeList&withCode=false'
            + '&budget_instituitional_group_id=' + selectedInstitutionalGroupId;

        const response = await fetch(url);
        document.getElementById('budgetOffice').innerHTML = await response.text();
        buttonStateChange(false);
    }

    function budgetOfficeChanged() {
        clearNextLevels(1);
    }

    async function monthYearChanged(monthYear) {
        const budgetOfficeId = document.getElementById('budgetOffice').value;
        if (budgetOfficeId === '') return;
        buttonStateChange(true);
        showOrHideLoadingGif('allowance-table', true);

        const url = 'Extra_labor_allowanceServlet?actionType=ajax_getExtraLaborAllowanceData'
            + '&monthYear=' + monthYear
            + '&budgetOfficeId=' + budgetOfficeId;
        const response = await fetch(url);

        const {isAlreadyAdded, extraLaborAllowanceModels} = await response.json();
        buttonStateChange(false);
        actionName = isAlreadyAdded ? 'edit' : 'add';

        const tableBody = document.querySelector('#allowance-table tbody');
        const templateRow = document.querySelector('#allowance-table tr.template-row');

        showOrHideLoadingGif('allowance-table', false);
        if (extraLaborAllowanceModels.length === 0) clearTable();

        extraLaborAllowanceModels.forEach(model => showModelInTable(tableBody, templateRow, model));
        calculateAllRowTotal();
    }

    function setRowData(templateRow, model) {
        const rowDataPrefix = 'row-data-';
        for (const key in model) {
            const td = templateRow.querySelector('.' + rowDataPrefix + key);
            if (!td) continue;
            td.innerText = model[key];
        }
    }

    function showModelInTable(tableBody, templateRow, model) {
        const modelRow = templateRow.cloneNode(true);
        modelRow.classList.remove('template-row');
        modelRow.querySelector('input[name="employeeRecordsId"]').value = model.employeeRecordsId;
        modelRow.querySelector('input[name="extraLaborAllowanceId"]').value = model.extraLaborAllowanceId;

        setRowData(modelRow, model);
        tableBody.append(modelRow);
    }

    function calculateAllRowTotal() {
        const colIndicesToSum = [10];
        const totalTitleColSpan = 1;
        const totalTitle = '<%=isLangEnglish?"Total":"সর্বমোট"%>';
        const language = '<%=Language%>';
        setupTotalRow('allowance-table', totalTitle, totalTitleColSpan, colIndicesToSum, 0, language);
    }

    function init() {
        $('#monthYear_js').on('datepicker.change', (event, param) => {
            const isValidDate = dateValidator('monthYear_js', true);
            const monthYear = getDateStringById('monthYear_js');
            if (isValidDate) {
                $('#monthYear').val(monthYear);
                monthYearChanged(monthYear);
            } else {
                clearTable();
            }
        });


        form.validate({
            rules: {
                budgetInstitutionalGroupId: "required",
                budgetOfficeId: "required",
                monthYear: "required"
            },
            messages: {
                budgetInstitutionalGroupId: '<%=isLangEnglish?"Mendatory":"আবশ্যক"%>',
                budgetOfficeId: '<%=isLangEnglish?"Mendatory":"আবশ্যক"%>',
                monthYear: '<%=isLangEnglish?"Mendatory":"আবশ্যক"%>'
            }
        });
    }

    function submitForm() {
        if (!form.valid()) return;
        submitAjaxByData(form.serialize(),"Extra_labor_allowanceServlet?actionType=ajax_" + actionName);
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
    }

    $(document).ready(function () {
        init();
    });
</script>