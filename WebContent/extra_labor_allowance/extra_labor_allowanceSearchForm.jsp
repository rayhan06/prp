<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="extra_labor_allowance.*" %>
<%@ page import="util.*" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="java.util.List" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="allowance_employee_info.AllowanceEmployeeInfoDTO" %>
<%@ page import="allowance_employee_info.AllowanceEmployeeInfoRepository" %>
<%@page pageEncoding="UTF-8" %>

<%
    String navigator2 = "navEXTRA_LABOR_ALLOWANCE";
    String servletName = "Extra_labor_allowanceServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
		<thead>
		<tr>
			<th rowspan="2">
				<%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_MONTHYEAR, loginDTO)%>
			</th>
			<th rowspan="2">
				<%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_EMPLOYEERECORDSID, loginDTO)%>
			</th>
			<th rowspan="2">
				<%=getDataByLanguage(Language, "মোবাইল নাম্বার", "Mobile Number")%>
			</th>
			<th rowspan="2">
				<%=getDataByLanguage(Language, "সঞ্চয়ী হিসাব নাম্বার", "Savings Account Number")%>
			</th>
			<th colspan="2" class="text-center">
				<%=getDataByLanguage(Language, "ক - সংসদ অধিবেশন", "KA - Parliament Session")%>
			</th>
			<th colspan="2" class="text-center">
				<%=getDataByLanguage(Language, "খ - কমিটি বৈঠক", "KHA - Committee Meeting")%>
			</th>
			<th rowspan="2">
				<%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_REVENUESTAMPDEDUCTION, loginDTO)%>
			</th>
			<th rowspan="2">
				<%=getDataByLanguage(Language, "নীট দাবী", "Net Amount")%>
			</th>
			<th rowspan="2"></th>
		</tr>
		<tr>
			<th>
				<%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_DAILYRATE, loginDTO)%>
			</th>
			<th>
				<%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_DAY, loginDTO)%>
			</th>
			<th>
				<%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_DAILYRATE, loginDTO)%>
			</th>
			<th>
				<%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_DAY, loginDTO)%>
			</th>
		</tr>
		</thead>
        <tbody>
        <%
			RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
			List<Extra_labor_allowanceDTO> data = (List<Extra_labor_allowanceDTO>) recordNavigator.list;
            try {
                if (data != null) {
                    for (Extra_labor_allowanceDTO extra_labor_allowanceDTO : data) {
        %>
        <tr>
			<td>
				<%=DateUtils.getMonthYear(extra_labor_allowanceDTO.monthYear, Language)%>
			</td>
			<td>
				<%
					AllowanceEmployeeInfoDTO employeeInfoDTO =
							AllowanceEmployeeInfoRepository.getInstance()
														   .getByEmployeeRecordId(extra_labor_allowanceDTO.employeeRecordsId);

				%>
				<%=getDataByLanguage(Language, employeeInfoDTO.nameBn, employeeInfoDTO.nameEn)%>
				<br>
				<b>
					<%=getDataByLanguage(Language, employeeInfoDTO.organogramNameBn, employeeInfoDTO.organogramNameEn)%>
				</b>
				<br>
				<%=getDataByLanguage(Language, employeeInfoDTO.officeNameBn, employeeInfoDTO.officeNameEn)%>
			</td>

			<td>
				<%=Utils.getDigits(employeeInfoDTO.mobileNumber, Language)%>
			</td>

			<td>
				<%=Utils.getDigits(employeeInfoDTO.savingAccountNumber, Language)%>
			</td>

			<td>
				<%=Utils.getDigits(extra_labor_allowanceDTO.dailyRateKa, Language)%>
			</td>

			<td>
				<%=Utils.getDigits(extra_labor_allowanceDTO.dayKa, Language)%>
			</td>

			<td>
				<%=Utils.getDigits(extra_labor_allowanceDTO.dailyRateKha, Language)%>
			</td>

			<td>
				<%=Utils.getDigits(extra_labor_allowanceDTO.dayKha, Language)%>
			</td>

			<td>
				<%=Utils.getDigits(extra_labor_allowanceDTO.revenueStampDeduction, Language)%>
			</td>

			<td>
				<%=Utils.getDigits(extra_labor_allowanceDTO.netAmount, Language)%>
			</td>
			<td>
				<a class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
				   href="Extra_labor_allowanceServlet?actionType=individualBill&iD=<%=extra_labor_allowanceDTO.iD%>">
					<%=getDataByLanguage(Language, "বিল প্রিভিউ", "Preview Bill")%>
				</a>
			</td>
        </tr>
        <%
                    }
                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>