<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="extra_labor_allowance.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="budget_mapping.Budget_mappingDTO" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="budget.BudgetCategoryEnum" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="java.util.stream.Collectors" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getLanguage(loginDTO);

    String formTitle = LM.getText(LC.EXTRA_LABOR_ALLOWANCE_ADD_EXTRA_LABOR_ALLOWANCE_ADD_FORMNAME, loginDTO);

    List<Extra_labor_allowanceDTO> allowanceDTOs = (List<Extra_labor_allowanceDTO>) request.getAttribute("allowanceDTOs");
    List<Extra_labor_allowanceModel> models =
            allowanceDTOs.stream()
                         .map(allowanceDTO -> new Extra_labor_allowanceModel(allowanceDTO, Language))
                         .collect(Collectors.toList());
    long monthYear = (long) request.getAttribute("monthYear");
    long budgetOfficeId = (long) request.getAttribute("budgetOfficeId");
    Budget_mappingDTO budgetMappingDTO = Budget_mappingRepository.getInstance()
                                                                 .getDTO(budgetOfficeId, BudgetCategoryEnum.OPERATIONAL.getValue());
%>

<style>
    .allowance-info {
        padding: 1rem 2rem;
        background: #f2fcff;
    }

    .allowance-info-heading {
        display: block;
        font-size: 1.25rem;
    }

    .allowance-info-data {
        color: #8bc9b0;
        font-weight: bold;
        font-size: 1.4rem;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%> <%=getDataByLanguage(Language, "বিল", "Bill")%>
                </h3>
            </div>
        </div>

        <div class="row mt-3 mx-4 allowance-info">
            <div class="col-md-6 col-lg-4 form-group">
                <label class="allowance-info-heading">
                    <%=LM.getText(LC.BUDGET_INSTITUTIONAL_GROUP, loginDTO)%>
                </label>
                <label class="allowance-info-data">
                    <%=Budget_institutional_groupRepository.getInstance().getText(
                            budgetMappingDTO.budgetInstitutionalGroupId,
                            false,
                            Language
                    )%>
                </label>
            </div>
            <div class="col-md-6 col-lg-4 form-group">
                <label class="allowance-info-heading">
                    <%=LM.getText(LC.BUDGET_OFFICE, loginDTO)%>
                </label>
                <label class="allowance-info-data">
                    <%=Budget_officeRepository.getInstance().getText(
                            budgetMappingDTO.budgetOfficeId,
                            Language
                    )%>
                </label>
            </div>
            <div class="col-md-6 col-lg-4 form-group">
                <label class="allowance-info-heading">
                    <%=LM.getText(LC.EXTRA_LABOR_ALLOWANCE_ADD_MONTHYEAR, loginDTO)%>
                </label>
                <label class="allowance-info-data">
                    <%=DateUtils.getMonthYear(monthYear, Language)%>
                </label>
            </div>
        </div>

        <div class="kt-portlet__body form-body">
            <div class="mt-4 table-responsive">
                <table id="allowance-table" class="table table-bordered table-striped text-nowrap">
                    <thead>
                    <tr>
                        <th>
                            <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_EMPLOYEERECORDSID, loginDTO)%>
                        </th>
                        <th>
                            <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_ORGANOGRAMID, loginDTO)%>
                        </th>
                        <th>
                            <%=getDataByLanguage(Language, "মোবাইল নাম্বার", "Mobile Number")%>
                        </th>
                        <th>
                            <%=getDataByLanguage(Language, "নীট দাবী", "Net Amount")%>
                        </th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody class="main-tbody">
                    <%for(Extra_labor_allowanceModel model : models){%>
                        <tr>
                            <td><%=model.name%></td>
                            <td><%=model.organogramName%></td>
                            <td><%=model.mobileNumber%></td>
                            <td><%=model.netAmount%></td>
                            <td>
                                <a class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                                    href="Extra_labor_allowanceServlet?actionType=individualBill&iD=<%=model.extraLaborAllowanceId%>">
                                    <%=getDataByLanguage(Language, "প্রিভিউ", "Preview")%>
                                </a>
                            </td>
                        </tr>
                    <%}%>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>