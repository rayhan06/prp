
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="am_house_allocation_committee_member.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
String navigator2 = "navAM_HOUSE_ALLOCATION_COMMITTEE_MEMBER";
String servletName = "Am_house_allocation_committee_memberServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_COMMITTEEID, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBERORGID, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICEID, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICEUNITID, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEREMPID, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBERPHONENUM, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBERNAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBERNAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICENAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICENAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICEUNITNAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICEUNITNAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICEUNITORGNAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICEUNITORGNAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MODIFIEDBY, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_SEARCH_AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_EDIT_BUTTON, loginDTO)%></th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Am_house_allocation_committee_memberDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Am_house_allocation_committee_memberDTO am_house_allocation_committee_memberDTO = (Am_house_allocation_committee_memberDTO) data.get(i);
																																
											
											%>
											<tr>
								
		
											<td>
											<%
											value = am_house_allocation_committee_memberDTO.committeeId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committee_memberDTO.memberOrgId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committee_memberDTO.memberOfficeId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committee_memberDTO.memberOfficeUnitId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committee_memberDTO.memberEmpId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committee_memberDTO.memberPhoneNum + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committee_memberDTO.memberNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committee_memberDTO.memberNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committee_memberDTO.memberOfficeNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committee_memberDTO.memberOfficeNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committee_memberDTO.memberOfficeUnitNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committee_memberDTO.memberOfficeUnitNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committee_memberDTO.memberOfficeUnitOrgNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committee_memberDTO.memberOfficeUnitOrgNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
											<td>
											<%
											value = am_house_allocation_committee_memberDTO.modifiedBy + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
		
		
	
											<%CommonDTO commonDTO = am_house_allocation_committee_memberDTO; %>
											<%@include file="../pb/searchAndViewButton.jsp"%>											
																						
											<td class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=am_house_allocation_committee_memberDTO.iD%>'/></span>
												</div>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			