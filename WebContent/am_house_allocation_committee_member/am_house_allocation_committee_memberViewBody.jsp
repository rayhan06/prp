

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="am_house_allocation_committee_member.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Am_house_allocation_committee_memberServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Am_house_allocation_committee_memberDTO am_house_allocation_committee_memberDTO = Am_house_allocation_committee_memberDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = am_house_allocation_committee_memberDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_COMMITTEEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committee_memberDTO.committeeId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBERORGID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committee_memberDTO.memberOrgId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committee_memberDTO.memberOfficeId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICEUNITID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committee_memberDTO.memberOfficeUnitId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEREMPID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committee_memberDTO.memberEmpId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBERPHONENUM, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committee_memberDTO.memberPhoneNum + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBERNAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committee_memberDTO.memberNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBERNAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committee_memberDTO.memberNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICENAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committee_memberDTO.memberOfficeNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICENAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committee_memberDTO.memberOfficeNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICEUNITNAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committee_memberDTO.memberOfficeUnitNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICEUNITNAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committee_memberDTO.memberOfficeUnitNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICEUNITORGNAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committee_memberDTO.memberOfficeUnitOrgNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICEUNITORGNAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committee_memberDTO.memberOfficeUnitOrgNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
			
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MODIFIEDBY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committee_memberDTO.modifiedBy + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>