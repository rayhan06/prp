<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="am_house_allocation_committee_member.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
Am_house_allocation_committee_memberDTO am_house_allocation_committee_memberDTO = new Am_house_allocation_committee_memberDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	am_house_allocation_committee_memberDTO = Am_house_allocation_committee_memberDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = am_house_allocation_committee_memberDTO;
String tableName = "am_house_allocation_committee_member";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_FORMNAME, loginDTO);
String servletName = "Am_house_allocation_committee_memberServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Am_house_allocation_committee_memberServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=am_house_allocation_committee_memberDTO.iD%>' tag='pb_html'/>
	
														<input type='hidden' class='form-control'  name='committeeId' id = 'committeeId_hidden_<%=i%>' value='<%=am_house_allocation_committee_memberDTO.committeeId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='memberOrgId' id = 'memberOrgId_hidden_<%=i%>' value='<%=am_house_allocation_committee_memberDTO.memberOrgId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='memberOfficeId' id = 'memberOfficeId_hidden_<%=i%>' value='<%=am_house_allocation_committee_memberDTO.memberOfficeId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='memberOfficeUnitId' id = 'memberOfficeUnitId_hidden_<%=i%>' value='<%=am_house_allocation_committee_memberDTO.memberOfficeUnitId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='memberEmpId' id = 'memberEmpId_hidden_<%=i%>' value='<%=am_house_allocation_committee_memberDTO.memberEmpId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBERPHONENUM, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='memberPhoneNum' id = 'memberPhoneNum_text_<%=i%>' value='<%=am_house_allocation_committee_memberDTO.memberPhoneNum%>'  required="required"  pattern="880[0-9]{10}" title="memberPhoneNum must start with 880, then contain 10 digits"   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBERNAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='memberNameEn' id = 'memberNameEn_text_<%=i%>' value='<%=am_house_allocation_committee_memberDTO.memberNameEn%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBERNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='memberNameBn' id = 'memberNameBn_text_<%=i%>' value='<%=am_house_allocation_committee_memberDTO.memberNameBn%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICENAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='memberOfficeNameEn' id = 'memberOfficeNameEn_text_<%=i%>' value='<%=am_house_allocation_committee_memberDTO.memberOfficeNameEn%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICENAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='memberOfficeNameBn' id = 'memberOfficeNameBn_text_<%=i%>' value='<%=am_house_allocation_committee_memberDTO.memberOfficeNameBn%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICEUNITNAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='memberOfficeUnitNameEn' id = 'memberOfficeUnitNameEn_text_<%=i%>' value='<%=am_house_allocation_committee_memberDTO.memberOfficeUnitNameEn%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICEUNITNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='memberOfficeUnitNameBn' id = 'memberOfficeUnitNameBn_text_<%=i%>' value='<%=am_house_allocation_committee_memberDTO.memberOfficeUnitNameBn%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICEUNITORGNAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='memberOfficeUnitOrgNameEn' id = 'memberOfficeUnitOrgNameEn_text_<%=i%>' value='<%=am_house_allocation_committee_memberDTO.memberOfficeUnitOrgNameEn%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MEMBEROFFICEUNITORGNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='memberOfficeUnitOrgNameBn' id = 'memberOfficeUnitOrgNameBn_text_<%=i%>' value='<%=am_house_allocation_committee_memberDTO.memberOfficeUnitOrgNameBn%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=am_house_allocation_committee_memberDTO.insertedByUserId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value='<%=am_house_allocation_committee_memberDTO.insertedByOrganogramId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_MODIFIEDBY, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='modifiedBy' id = 'modifiedBy_text_<%=i%>' value='<%=am_house_allocation_committee_memberDTO.modifiedBy%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=am_house_allocation_committee_memberDTO.insertionDate%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=am_house_allocation_committee_memberDTO.searchColumn%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=am_house_allocation_committee_memberDTO.isDeleted%>' tag='pb_html'/>
											
														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=am_house_allocation_committee_memberDTO.lastModificationTime%>' tag='pb_html'/>
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD_AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);

	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Am_house_allocation_committee_memberServlet");	
}

function init(row)
{


	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;



</script>






