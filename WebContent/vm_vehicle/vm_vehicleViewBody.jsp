<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_vehicle.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="asset_supplier.Asset_supplierRepository" %>

<%
    String servletName = "Vm_vehicleServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.VM_VEHICLE_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().
            getVm_vehicleDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important; margin-bottom: -18px">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.VM_VEHICLE_ADD_VM_VEHICLE_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div>
                <h5 class="table-title">
                    <%=LM.getText(LC.VM_VEHICLE_ADD_VM_VEHICLE_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_PURCHASEDATE, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicleDTO.purchaseDate + "";
                                %>
                                <%
                                    String formatted_purchaseDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_purchaseDate, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_VEHICLEBRANDCAT, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicleDTO.vehicleBrandCat + "";
                                %>
                                <%
                                    value = CatRepository.getName(Language, "vehicle_brand", vm_vehicleDTO.vehicleBrandCat);
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_VEHICLECOLORCAT, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicleDTO.vehicleColorCat + "";
                                %>
                                <%
                                    value = CatRepository.getName(Language, "vehicle_color", vm_vehicleDTO.vehicleColorCat);
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_VEHICLETYPECAT, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicleDTO.vehicleTypeCat + "";
                                %>
                                <%
                                    value = CatRepository.getName(Language, "vehicle_type", vm_vehicleDTO.vehicleTypeCat);
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_REGNO, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicleDTO.regNo + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_CHASISNO, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicleDTO.chasisNo + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_ENGINENO, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicleDTO.engineNo + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_NUMBEROFSEATS, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicleDTO.numberOfSeats + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_VEHICLEFUELCAT, loginDTO)%>
                                </b></td>
                            <td>



                                <%
                                    value = "";
                                    value = vm_vehicleDTO.vehicleFuelCat;
                                    StringTokenizer tokenizer = new StringTokenizer(value, ",");
                                    String separator = "";
                                    value = "";
                                    while (tokenizer.hasMoreTokens()) {
                                        String val = tokenizer.nextToken().trim();
                                        if(!val.equalsIgnoreCase("")){
                                            Long distNum = Long.valueOf(val);
                                            String advertiseMedium = CatRepository.getInstance().getText(Language, "vehicle_fuel", distNum);
                                            value = value + separator + advertiseMedium;
                                            separator = ", ";
                                        }

                                    }
                                %>

                                <%=value%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_SUPPLIERTYPE, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = Asset_supplierRepository.getInstance().getText(Language,
                                            vm_vehicleDTO.supplierType);
                                %>
                                <%
//                                    value = CommonDAO.getName(Integer.parseInt(value), "asset_supplier", Language.equals("English") ? "name_en" : "name_bn", "id");
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_MODELNO, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicleDTO.modelNo + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_PURCHASEAMOUNT, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicleDTO.purchaseAmount + "";
                                %>
                                <%
                                    value = String.format("%.1f", vm_vehicleDTO.purchaseAmount);
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_MANUFACTUREYEAR, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicleDTO.manufactureYear + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_FITNESSEXPIRYDATE, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicleDTO.fitnessExpiryDate + "";
                                %>
                                <%
                                    String formatted_fitnessExpiryDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_fitnessExpiryDate, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_TAXTOKENEXPIRYDATE, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicleDTO.taxTokenExpiryDate + "";
                                %>
                                <%
                                    String formatted_taxTokenExpiryDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_taxTokenExpiryDate, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_DIGITALPLATE, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = UtilCharacter.getBooleanReadable(vm_vehicleDTO.digitalPlate, Language);
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_STATUS, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = Vm_vehicleDAO.getStatus(vm_vehicleDTO.status, Language);
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_FILESDROPZONE, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = vm_vehicleDTO.filesDropzone + "";
                                %>
                                <%
                                    {
                                        List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(vm_vehicleDTO.filesDropzone);
                                %>
                                <%@include file="../pb/dropzoneViewer.jsp" %>
                                <%
                                    }
                                %>


                            </td>

                        </tr>


                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



