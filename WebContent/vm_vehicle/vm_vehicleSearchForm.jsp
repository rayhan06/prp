<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_vehicle.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.VM_VEHICLE_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    Vm_vehicleDAO vm_vehicleDAO = (Vm_vehicleDAO) request.getAttribute("vm_vehicleDAO");


    String navigator2 = SessionConstants.NAV_VM_VEHICLE;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<%

    if (hasAjax == false) {
        Enumeration<String> parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {

            String paramName = parameterNames.nextElement();

            if (!paramName.equalsIgnoreCase("actionType")) {
                String[] paramValues = request.getParameterValues(paramName);
                for (int i = 0; i < paramValues.length; i++) {
                    String paramValue = paramValues[i];

%>

<%

                }
            }


        }
    }

%>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>

            <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VEHICLETYPE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_VEHICLE_ADD_VEHICLEBRANDCAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_VEHICLE_ADD_REGNO, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_VEHICLE_ADD_PURCHASEDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_VEHICLE_ADD_STATUS, loginDTO)%>
            </th>
            <%--								<th><%=LM.getText(LC.VM_VEHICLE_ADD_VEHICLECOLORCAT, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_VEHICLE_ADD_VEHICLETYPECAT, loginDTO)%></th>--%>

            <th><%=LM.getText(LC.VM_VEHICLE_ADD_PURCHASEAMOUNT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_VEHICLE_ADD_SUPPLIERTYPE, loginDTO)%>
            </th>

            <%--								<th><%=LM.getText(LC.VM_VEHICLE_ADD_CHASISNO, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_VEHICLE_ADD_ENGINENO, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_VEHICLE_ADD_NUMBEROFSEATS, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_VEHICLE_ADD_VEHICLEFUELCAT, loginDTO)%></th>--%>

            <%--								<th><%=LM.getText(LC.VM_VEHICLE_ADD_MODELNO, loginDTO)%></th>--%>

            <%--								<th><%=LM.getText(LC.VM_VEHICLE_ADD_MANUFACTUREYEAR, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_VEHICLE_ADD_FITNESSEXPIRYDATE, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_VEHICLE_ADD_TAXTOKENEXPIRYDATE, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_VEHICLE_ADD_DIGITALPLATE, loginDTO)%></th>--%>

            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_VEHICLE_SEARCH_VM_VEHICLE_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="">
                <span class="ml-4">All</span>
                <div class="d-flex align-items-center mr-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_VM_VEHICLE);

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Vm_vehicleDTO vm_vehicleDTO = (Vm_vehicleDTO) data.get(i);


        %>
        <tr id='tr_<%=i%>'>
            <%

            %>


            <%
                request.setAttribute("vm_vehicleDTO", vm_vehicleDTO);
            %>

            <jsp:include page="./vm_vehicleSearchRow.jsp">
                <jsp:param name="pageName" value="searchrow"/>
                <jsp:param name="rownum" value="<%=i%>"/>
            </jsp:include>


            <%

            %>
        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			