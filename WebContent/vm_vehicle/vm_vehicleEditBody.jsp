<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_vehicle.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    Vm_vehicleDTO vm_vehicleDTO;
    vm_vehicleDTO = (Vm_vehicleDTO) request.getAttribute("vm_vehicleDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (vm_vehicleDTO == null) {
        vm_vehicleDTO = new Vm_vehicleDTO();

    }
    System.out.println("vm_vehicleDTO = " + vm_vehicleDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.VM_VEHICLE_ADD_VM_VEHICLE_ADD_FORMNAME, loginDTO);
    String servletName = "Vm_vehicleServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID = -1;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_VEHICLE_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    int year = Calendar.getInstance().get(Calendar.YEAR);


//	System.out.println(Vm_vehicleDAO.getStatus(0, "english"));


%>
<%@include file="vm_vehicleEditBodyForm.jsp" %>

<script type="text/javascript">

    let selectedFuelCat;
    let selectedFuelCatArr;
    var language = '<%=Language%>';

    function dateValidation() {
        return dateValidator('purchaseDate_js_0', true, {
                errorEn: "Please select purchase date",
                errorBn: "ক্রয়ের তারিখ নির্বাচন করুন",
            }
        ) && dateValidator('fitnessExpiryDate_js_0', true, {
                errorEn: "Please select fitness expiry date",
                errorBn: "ফিটনেস মেয়াদ নির্বাচন করুন",
            }
        ) && dateValidator('taxTokenExpiryDate_js_0', true, {
                errorEn: "Please select tax token expiry date",
                errorBn: "ট্যাক্স টোকেনের মেয়াদ নির্বাচন করুন",
            }
        );
    }

    function PreprocessBeforeSubmiting(row) {
        preprocessDateBeforeSubmitting('purchaseDate', row);
        preprocessDateBeforeSubmitting('fitnessExpiryDate', row);
        preprocessDateBeforeSubmitting('taxTokenExpiryDate', row);
        preprocessCheckBoxBeforeSubmitting('digitalPlate', row);
    }
    function processMultipleSelectBoxBeforeSubmit2(name)
    {

        $( "[name='" + name + "']" ).each(function( i )
        {
            var selectedInputs = $(this).val();
            var temp = "";
            if(selectedInputs != null){
                selectedInputs.forEach(function(value, index, array){
                    if(index > 0){
                        temp += ", ";
                    }
                    temp += value;
                });
            }
            if(temp.includes(',')){
                $(this).append('<option value="' + temp + '"></option>');
            }
            $(this).val(temp);

        });
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function formSubmitAfterValidation() {
        let form = $("#bigform");
        PreprocessBeforeSubmiting(0);
        processMultipleSelectBoxBeforeSubmit2("vehicleFuelCat");
        let url = form.attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            dataType: 'JSON',
            success: function (response) {
                if (response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                    buttonStateChange(false);
                } else if (response.responseCode === 200) {
                    window.location.replace(getContextPath() + response.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChange(false);
            }
        });
    }

    function formSubmit() {
        event.preventDefault();
        // let dateValid = dateValidation();
        // let form = $("#bigform");
        // form.validate();
        // let valid = form.valid();

        // if (valid && dateValid) {
        if (true) {
            let regNo = $("#regNo_text_0").val();
            let chasisNo = $("#chasisNo_text_0").val();
            let type = '<%=actionName%>'
            let id = '<%=vm_vehicleDTO.iD%>';
            let language = '<%=Language%>';
            let url = "Vm_vehicleServlet?actionType=checkRegistrationAndChasisValidation&regNo="
                + regNo + "&chasisNo=" + chasisNo + "&type=" + type + "&iD=" + id + "&language=" + language;
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    if (fetchedData && fetchedData.valid == true) {
                        formSubmitAfterValidation();
                    } else if (fetchedData && fetchedData.valid == false) {
                        toastr.error(fetchedData.errMsg);
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });

        }
    }


    function init(row) {

        setDateByStringAndId('purchaseDate_js_' + row, $('#purchaseDate_date_' + row).val());
        setDateByStringAndId('fitnessExpiryDate_js_' + row, $('#fitnessExpiryDate_date_' + row).val());
        setDateByStringAndId('taxTokenExpiryDate_js_' + row, $('#taxTokenExpiryDate_date_' + row).val());


    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $.validator.addMethod('validSelector', function (value, element) {
            return value && value != -1;
        });

        $.validator.addMethod('greaterThanZero', function (value, element) {
            return value && value > 0;
        });

        let lang = '<%=Language%>';
        let errVehicleBrandCat;
        let errvehicleColorCat;
        let errVehicleTypeCat;
        let errVehicleFuelCat;
        let errSupplierType;
        let errManufactureYear;
        let errStatus;

        let errRegNo;
        let errChasisNo;
        let errEngineNo;
        let errNumberOfSeats;
        let errModelNo;
        let errPurchaseAmount;


        if (lang == 'english') {
            errVehicleBrandCat = 'Please select Brand';
            errvehicleColorCat = 'Please select color';
            errVehicleTypeCat = 'Please select type';
            errVehicleFuelCat = 'Please select fuel type';
            errSupplierType = 'Please select supplier type';
            errManufactureYear = 'Please select manufacture year';
            errStatus = 'Please select status';
            errEngineNo = 'Required';
            errModelNo = 'Required';
            errRegNo = 'Required';
            errChasisNo = 'Required';
            errNumberOfSeats = ' Must be greater than zero';
            errPurchaseAmount = ' Must be greater than zero';

        } else {
            errVehicleBrandCat = 'ব্র্যান্ড নির্বাচন করুন';
            errvehicleColorCat = 'কালার নির্বাচন করুন';
            errVehicleTypeCat = 'ধরণ নির্বাচন করুন';
            errVehicleFuelCat = 'জ্বালানি ধরণ নির্বাচন করুন';
            errSupplierType = 'বিক্রয়কারী প্রতিষ্ঠান নির্বাচন করুন';
            errManufactureYear = 'ম্যানুফ্যাকচার বছর নির্বাচন করুন';
            errStatus = 'অবস্থা নির্বাচন করুন';
            errEngineNo = 'প্রয়োজনীয় ';
            errModelNo = 'প্রয়োজনীয় ';
            errRegNo = 'প্রয়োজনীয় ';
            errChasisNo = 'প্রয়োজনীয় ';
            errNumberOfSeats = 'শূন্য থেকে বড় হবে ';
            errPurchaseAmount = 'শূন্য থেকে বড় হবে ';

        }

        // $("#bigform").validate({
        //     errorClass: 'error is-invalid',
        //     validClass: 'is-valid',
        //     rules: {
        //         vehicleBrandCat: {
        //             required: true,
        //             validSelector: true,
        //         },
        //         vehicleColorCat: {
        //             required: true,
        //             validSelector: true,
        //         },
        //         vehicleTypeCat: {
        //             required: true,
        //             validSelector: true,
        //         },
        //         vehicleFuelCat: {
        //             required: true,
        //             validSelector: true,
        //         },
        //         supplierType: {
        //             required: true,
        //             validSelector: true,
        //         },
        //         manufactureYear: {
        //             required: true,
        //             validSelector: true,
        //         },
        //         status: {
        //             required: true,
        //             validSelector: true,
        //         },
        //         regNo: {
        //             required: true,
        //             // validSelector: true,
        //         },
        //         chasisNo: {
        //             required: true,
        //             // validSelector: true,
        //         },
        //         engineNo: {
        //             required: true,
        //             // validSelector: true,
        //         },
        //         numberOfSeats: {
        //             required: true,
        //             greaterThanZero: true,
        //         },
        //         modelNo: {
        //             required: true,
        //             // validSelector: true,
        //         },
        //         purchaseAmount: {
        //             required: true,
        //             greaterThanZero: true,
        //         },
        //     },
        //     messages: {
        //         vehicleBrandCat: errVehicleBrandCat,
        //         vehicleColorCat: errvehicleColorCat,
        //         vehicleTypeCat: errVehicleTypeCat,
        //         vehicleFuelCat: errVehicleFuelCat,
        //         supplierType: errSupplierType,
        //         manufactureYear: errManufactureYear,
        //         status: errStatus,
        //
        //         regNo: errRegNo,
        //         chasisNo: errChasisNo,
        //         engineNo: errEngineNo,
        //         numberOfSeats: errNumberOfSeats,
        //         modelNo: errModelNo,
        //         purchaseAmount: errPurchaseAmount,
        //
        //     }
        // });

        select2MultiSelector("#vehicleFuelCat",'<%=Language%>');
        <% if(actionName.equalsIgnoreCase("edit")){ %>

        selectedFuelCat = '<%=vm_vehicleDTO.vehicleFuelCat%>';
        selectedFuelCatArr = selectedFuelCat.split(',').map(function(item) {
            return item.trim();
        });
        <% }%>
        fetchVehicleFuelCat(selectedFuelCatArr);
    });
    function fetchVehicleFuelCat(selectedFuelCatArr) {

        let url = "Vm_vehicleServlet?actionType=getAllVehicleFuelCat";
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {

                $('#vehicleFuelCat').html("");
                let o;
                let str;

                $('#vehicleFuelCat').append(o);
                const response = JSON.parse(fetchedData);
                if (response && response.length > 0) {
                    for (let x in response) {
                        if (language === 'English') {
                            str = response[x].englishText;
                        } else {
                            str = response[x].banglaText;
                        }

                        if (selectedFuelCatArr && selectedFuelCatArr.length > 0) {
                            if (selectedFuelCatArr.includes(response[x].value + '')) {
                                o = new Option(str, response[x].value, false, true);
                            } else {
                                o = new Option(str, response[x].value);
                            }
                        } else {
                            o = new Option(str, response[x].value);
                        }
                        $(o).html(str);
                        $('#vehicleFuelCat').append(o);
                    }
                }


            },
            error: function (error) {
                console.log(error);
            }
        });
    }


</script>






