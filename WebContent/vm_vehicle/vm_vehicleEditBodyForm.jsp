<%@ page import="asset_supplier.Asset_supplierRepository" %>
<%@ page import="util.UtilCharacter" %>
<%@page pageEncoding="UTF-8" %>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Vm_vehicleServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="formSubmit()">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=vm_vehicleDTO.iD%>' tag='pb_html'/>
                                    <div class="row">
                                        <div class="col-md-6"
                                        >
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_ADD_PURCHASEDATE, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <%value = "purchaseDate_js_" + i;%>
                                                    <jsp:include page="/date/date.jsp">
                                                        <jsp:param name="DATE_ID"
                                                                   value="<%=value%>"></jsp:param>
                                                        <jsp:param name="LANGUAGE"
                                                                   value="<%=Language%>"></jsp:param>
                                                    </jsp:include>
                                                    <input type='hidden' name='purchaseDate'
                                                           id='purchaseDate_date_<%=i%>'
                                                           value='<%=dateFormat.format(new Date(vm_vehicleDTO.purchaseDate))%>'
                                                           tag='pb_html'>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6"
                                        >
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_ADD_VEHICLEBRANDCAT, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <select class='form-control' name='vehicleBrandCat'
                                                            id='vehicleBrandCat_category_<%=i%>' tag='pb_html'>
                                                        <%
                                                            Options = CatRepository.getOptions(Language, "vehicle_brand", vm_vehicleDTO.vehicleBrandCat);
                                                        %>
                                                        <%=Options%>
                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6"
                                        >
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_ADD_VEHICLECOLORCAT, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <select class='form-control' name='vehicleColorCat'
                                                            id='vehicleColorCat_category_<%=i%>' tag='pb_html'>
                                                        <%
                                                            Options = CatRepository.getOptions(Language, "vehicle_color", vm_vehicleDTO.vehicleColorCat);
                                                        %>
                                                        <%=Options%>
                                                    </select>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-6"
                                        >
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_ADD_VEHICLETYPECAT, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <select class='form-control' name='vehicleTypeCat'
                                                            id='vehicleTypeCat_category_<%=i%>' tag='pb_html'>
                                                        <%
                                                            Options = CatRepository.getOptions(Language, "vehicle_type", vm_vehicleDTO.vehicleTypeCat);
                                                        %>
                                                        <%=Options%>
                                                    </select>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-6"
                                        >
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_ADD_REGNO, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='regNo'
                                                           id='regNo_text_<%=i%>'
                                                           value='<%=vm_vehicleDTO.regNo%>' tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-6"
                                        >
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_ADD_CHASISNO, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='chasisNo'
                                                           id='chasisNo_text_<%=i%>'
                                                           value='<%=vm_vehicleDTO.chasisNo%>' tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-6"
                                        >
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_ADD_ENGINENO, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='engineNo'
                                                           id='engineNo_text_<%=i%>'
                                                           value='<%=vm_vehicleDTO.engineNo%>' tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-6"
                                        >
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_ADD_NUMBEROFSEATS, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <%
                                                        value = "";
                                                        if (vm_vehicleDTO.numberOfSeats != -1) {
                                                            value = vm_vehicleDTO.numberOfSeats + "";
                                                        }
                                                    %>
                                                    <input type='number' class='form-control'
                                                           name='numberOfSeats' id='numberOfSeats_number_<%=i%>'
                                                           value='<%=value%>' tag='pb_html'>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-6"
                                        >
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_ADD_VEHICLEFUELCAT, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">


                                                    <select class='form-control' name='vehicleFuelCat'
                                                            id='vehicleFuelCat' tag='pb_html'>

                                                    </select>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-6"
                                        >
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_ADD_SUPPLIERTYPE, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <select class='form-control' name='supplierType'
                                                            id='supplierType_select_<%=i%>' tag='pb_html'>

                                                        <%
                                                            String defaultOptions ="<option value=\"-1\">"+UtilCharacter.getDataByLanguage(Language, "বাছাই করুন", "Select")+"</option>";
                                                            Options = Asset_supplierRepository.getInstance().getBuildOptions
                                                                    (Language, vm_vehicleDTO.supplierType + "");
//                                                            Options = CommonDAO.getOptions(Language, "asset_supplier", vm_vehicleDTO.supplierType);
                                                            Options = defaultOptions + Options;

                                                        %>
                                                        <%=Options%>
                                                    </select>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-6"
                                        >
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_ADD_MODELNO, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='modelNo'
                                                           id='modelNo_text_<%=i%>'
                                                           value='<%=vm_vehicleDTO.modelNo%>' tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-6"
                                        >
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_ADD_PURCHASEAMOUNT, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <%
                                                        value = "";
                                                        if (vm_vehicleDTO.purchaseAmount != -1) {
                                                            value = vm_vehicleDTO.purchaseAmount + "";
                                                        }
                                                    %>
                                                    <input type='number' class='form-control'
                                                           name='purchaseAmount'
                                                           id='purchaseAmount_number_<%=i%>' value='<%=value%>'
                                                           tag='pb_html'>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-6"
                                        >
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_ADD_MANUFACTUREYEAR, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">

                                                    <select class='form-control' name='manufactureYear'
                                                            id='manufactureYear_select_<%=i%>' tag='pb_html'>
                                                        <%
                                                            Options = Vm_vehicleDAO.getYears(Language, (int) vm_vehicleDTO.manufactureYear);
                                                        %>
                                                        <%=Options%>
                                                    </select>

                                                    <%--																<%--%>
                                                    <%--																	value = "";--%>
                                                    <%--																	if(vm_vehicleDTO.manufactureYear != -1)--%>
                                                    <%--																	{--%>
                                                    <%--																	value = vm_vehicleDTO.manufactureYear + "";--%>
                                                    <%--																	}--%>
                                                    <%--																%>		--%>
                                                    <%--																<input type='number' class='form-control'  name='manufactureYear' id = 'manufactureYear_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		--%>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-6"
                                        >
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_ADD_FITNESSEXPIRYDATE, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <%value = "fitnessExpiryDate_js_" + i;%>
                                                    <jsp:include page="/date/date.jsp">
                                                        <jsp:param name="DATE_ID"
                                                                   value="<%=value%>"></jsp:param>
                                                        <jsp:param name="LANGUAGE"
                                                                   value="<%=Language%>"></jsp:param>
                                                        <jsp:param name="END_YEAR"
                                                                   value="<%=year + 10%>"></jsp:param>
                                                    </jsp:include>
                                                    <input type='hidden' name='fitnessExpiryDate'
                                                           id='fitnessExpiryDate_date_<%=i%>'
                                                           value='<%=dateFormat.format(new Date(vm_vehicleDTO.fitnessExpiryDate))%>'
                                                           tag='pb_html'>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-6"
                                        >
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_ADD_TAXTOKENEXPIRYDATE, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <%value = "taxTokenExpiryDate_js_" + i;%>
                                                    <jsp:include page="/date/date.jsp">
                                                        <jsp:param name="DATE_ID"
                                                                   value="<%=value%>"></jsp:param>
                                                        <jsp:param name="LANGUAGE"
                                                                   value="<%=Language%>"></jsp:param>
                                                        <jsp:param name="END_YEAR"
                                                                   value="<%=year + 10%>"></jsp:param>
                                                    </jsp:include>
                                                    <input type='hidden' name='taxTokenExpiryDate'
                                                           id='taxTokenExpiryDate_date_<%=i%>'
                                                           value='<%=dateFormat.format(new Date(vm_vehicleDTO.taxTokenExpiryDate))%>'
                                                           tag='pb_html'>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-6"
                                        >
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_ADD_DIGITALPLATE, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='checkbox' class='form-control-sm'
                                                           name='digitalPlate' id='digitalPlate_checkbox_<%=i%>'
                                                           value='true'                                                                <%=(String.valueOf(vm_vehicleDTO.digitalPlate).equals("true"))?("checked"):""%>
                                                           tag='pb_html'>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-6"
                                        >
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_ADD_STATUS, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">

                                                    <select class='form-control' name='status'
                                                            id='status_select_<%=i%>' tag='pb_html'>
                                                        <%
                                                            Options = Vm_vehicleDAO.getStatusList(Language, vm_vehicleDTO.status);
                                                        %>
                                                        <%=Options%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-6"
                                        >
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_ADD_FILESDROPZONE, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <%
                                                        fileColumnName = "filesDropzone";
                                                        if (actionName.equals("edit")) {
                                                            List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(vm_vehicleDTO.filesDropzone);
                                                    %>
                                                    <%@include file="../pb/dropzoneEditor.jsp" %>
                                                    <%
                                                        } else {
                                                            ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                            vm_vehicleDTO.filesDropzone = ColumnID;
                                                        }
                                                    %>

                                                    <div class="dropzone"
                                                         action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=vm_vehicleDTO.filesDropzone%>">
                                                        <input type='file' style="display:none"
                                                               name='<%=fileColumnName%>File'
                                                               id='<%=fileColumnName%>_dropzone_File_<%=i%>'
                                                               tag='pb_html'/>
                                                    </div>
                                                    <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                           id='<%=fileColumnName%>FilesToDelete_<%=i%>' value=''
                                                           tag='pb_html'/>
                                                    <input type='hidden' name='<%=fileColumnName%>'
                                                           id='<%=fileColumnName%>_dropzone_<%=i%>'
                                                           tag='pb_html'
                                                           value='<%=vm_vehicleDTO.filesDropzone%>'/>


                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=vm_vehicleDTO.insertedByUserId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=vm_vehicleDTO.insertedByOrganogramId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=vm_vehicleDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>' value='<%=vm_vehicleDTO.isDeleted%>'
                                           tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=vm_vehicleDTO.lastModificationTime%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=vm_vehicleDTO.searchColumn%>' tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 form-actions text-right mt-3">
                        <a id = "cancel-btn" class="btn btn-sm cancel-btn text-white shadow btn-border-radius"
                           href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.VM_VEHICLE_ADD_VM_VEHICLE_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button id = "submit-btn" class="btn  btn-sm submit-btn text-white shadow btn-border-radius ml-2"
                                type="submit"><%=LM.getText(LC.VM_VEHICLE_ADD_VM_VEHICLE_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
