<%@page pageEncoding="UTF-8" %>

<%@page import="vm_vehicle.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="files.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="asset_supplier.Asset_supplierRepository" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VM_VEHICLE_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_VM_VEHICLE;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Vm_vehicleDTO vm_vehicleDTO = (Vm_vehicleDTO) request.getAttribute("vm_vehicleDTO");
    CommonDTO commonDTO = vm_vehicleDTO;
    String servletName = "Vm_vehicleServlet";


    System.out.println("vm_vehicleDTO = " + vm_vehicleDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Vm_vehicleDAO vm_vehicleDAO = (Vm_vehicleDAO) request.getAttribute("vm_vehicleDAO");

    FilesDAO filesDAO = new FilesDAO();

    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
<td id='<%=i%>_vehicleTypeCat'>
    <%
        value = vm_vehicleDTO.vehicleTypeCat + "";
    %>
    <%
        value = CatRepository.getName(Language, "vehicle_type", vm_vehicleDTO.vehicleTypeCat);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>





<td id='<%=i%>_vehicleBrandCat'>
    <%
        value = vm_vehicleDTO.vehicleBrandCat + "";
    %>
    <%
        value = CatRepository.getName(Language, "vehicle_brand", vm_vehicleDTO.vehicleBrandCat);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_regNo'>
    <%
        value = vm_vehicleDTO.regNo + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_purchaseDate'>
    <%
        value = vm_vehicleDTO.purchaseDate + "";
    %>
    <%
        String formatted_purchaseDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_purchaseDate, Language)%>


</td>
<td id='<%=i%>_status'>
    <%
        value = Vm_vehicleDAO.getStatus(vm_vehicleDTO.status, Language);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<%--											<td id = '<%=i%>_vehicleColorCat'>--%>
<%--											<%--%>
<%--											value = vm_vehicleDTO.vehicleColorCat + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											value = CatDAO.getName(Language, "vehicle_color", vm_vehicleDTO.vehicleColorCat);--%>
<%--											%>	--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_vehicleTypeCat'>--%>
<%--											<%--%>
<%--											value = vm_vehicleDTO.vehicleTypeCat + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											value = CatDAO.getName(Language, "vehicle_type", vm_vehicleDTO.vehicleTypeCat);--%>
<%--											%>	--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>



<%--											<td id = '<%=i%>_chasisNo'>--%>
<%--											<%--%>
<%--											value = vm_vehicleDTO.chasisNo + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_engineNo'>--%>
<%--											<%--%>
<%--											value = vm_vehicleDTO.engineNo + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>

<%--											<td id = '<%=i%>_numberOfSeats'>--%>
<%--											<%--%>
<%--											value = vm_vehicleDTO.numberOfSeats + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_vehicleFuelCat'>--%>
<%--											<%--%>
<%--											value = vm_vehicleDTO.vehicleFuelCat + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											value = CatDAO.getName(Language, "vehicle_fuel", vm_vehicleDTO.vehicleFuelCat);--%>
<%--											%>	--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>

<%--</td>--%>

<td id='<%=i%>_purchaseAmount'>
    <%
        value = vm_vehicleDTO.purchaseAmount + "";
    %>
    <%
        value = String.format("%.1f", vm_vehicleDTO.purchaseAmount);

        if (vm_vehicleDTO.purchaseAmount != -1) {
    %>

    <%=Utils.getDigits(value, Language)%>

    <%
        }
    %>


</td>

<td id='<%=i%>_supplierType'>
    <%
        value = Asset_supplierRepository.getInstance().getText(Language,vm_vehicleDTO.supplierType) ;
    %>
    <%
//        value = CommonDAO.getName(Integer.parseInt(value), "asset_supplier", Language.equals("English") ? "name_en" : "name_bn", "id");
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<%--											<td id = '<%=i%>_modelNo'>--%>
<%--											<%--%>
<%--											value = vm_vehicleDTO.modelNo + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>

<%--		--%>
<%--											<td id = '<%=i%>_manufactureYear'>--%>
<%--											<%--%>
<%--											value = vm_vehicleDTO.manufactureYear + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_fitnessExpiryDate'>--%>
<%--											<%--%>
<%--											value = vm_vehicleDTO.fitnessExpiryDate + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											String formatted_fitnessExpiryDate = simpleDateFormat.format(new Date(Long.parseLong(value))).toString();--%>
<%--											%>--%>
<%--											<%=Utils.getDigits(formatted_fitnessExpiryDate, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_taxTokenExpiryDate'>--%>
<%--											<%--%>
<%--											value = vm_vehicleDTO.taxTokenExpiryDate + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											String formatted_taxTokenExpiryDate = simpleDateFormat.format(new Date(Long.parseLong(value))).toString();--%>
<%--											%>--%>
<%--											<%=Utils.getDigits(formatted_taxTokenExpiryDate, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_digitalPlate'>--%>
<%--											<%--%>
<%--											value = vm_vehicleDTO.digitalPlate + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>


<td>
	<button
			type="button"
			class="btn-sm border-0 shadow bg-light btn-border-radius"
			style="color: #ff6b6b;"
			onclick="view('<%=vm_vehicleDTO.iD%>')">
		<i class="fa fa-eye"></i>
	</button>
</td>

<td id='<%=i%>_Edit'>
	<button
			type="button"
			class="btn-sm border-0 shadow btn-border-radius text-white"
			style="background-color: #ff6b6b;"
			onclick="edit('<%=vm_vehicleDTO.iD%>')">
		<i class="fa fa-edit"></i>
	</button>
</td>


<td id='<%=i%>_checkbox'>
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=vm_vehicleDTO.iD%>'/></span>
    </div>
</td>
																						
											

