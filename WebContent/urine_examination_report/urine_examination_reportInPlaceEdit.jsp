<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="urine_examination_report.Urine_examination_reportDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Urine_examination_reportDTO urine_examination_reportDTO = (Urine_examination_reportDTO)request.getAttribute("urine_examination_reportDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(urine_examination_reportDTO == null)
{
	urine_examination_reportDTO = new Urine_examination_reportDTO();
	
}
System.out.println("urine_examination_reportDTO = " + urine_examination_reportDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=urine_examination_reportDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_commonLabReportId'>")%>
			

		<input type='hidden' class='form-control'  name='commonLabReportId' id = 'commonLabReportId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.commonLabReportId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_quantity'>")%>
			
	
	<div class="form-inline" id = 'quantity_div_<%=i%>'>
		<input type='text' class='form-control'  name='quantity' id = 'quantity_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.quantity + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_color'>")%>
			
	
	<div class="form-inline" id = 'color_div_<%=i%>'>
		<input type='text' class='form-control'  name='color' id = 'color_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.color + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_reaction'>")%>
			
	
	<div class="form-inline" id = 'reaction_div_<%=i%>'>
		<input type='text' class='form-control'  name='reaction' id = 'reaction_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.reaction + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_gravity'>")%>
			
	
	<div class="form-inline" id = 'gravity_div_<%=i%>'>
		<input type='text' class='form-control'  name='gravity' id = 'gravity_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.gravity + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_albumin'>")%>
			
	
	<div class="form-inline" id = 'albumin_div_<%=i%>'>
		<input type='text' class='form-control'  name='albumin' id = 'albumin_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.albumin + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_sugar'>")%>
			
	
	<div class="form-inline" id = 'sugar_div_<%=i%>'>
		<input type='text' class='form-control'  name='sugar' id = 'sugar_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.sugar + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_bileSalts'>")%>
			
	
	<div class="form-inline" id = 'bileSalts_div_<%=i%>'>
		<input type='text' class='form-control'  name='bileSalts' id = 'bileSalts_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.bileSalts + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_pussCells'>")%>
			
	
	<div class="form-inline" id = 'pussCells_div_<%=i%>'>
		<input type='text' class='form-control'  name='pussCells' id = 'pussCells_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.pussCells + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_epCells'>")%>
			
	
	<div class="form-inline" id = 'epCells_div_<%=i%>'>
		<input type='text' class='form-control'  name='epCells' id = 'epCells_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.epCells + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_rbc'>")%>
			
	
	<div class="form-inline" id = 'rbc_div_<%=i%>'>
		<input type='text' class='form-control'  name='rbc' id = 'rbc_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.rbc + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_rbcCast'>")%>
			
	
	<div class="form-inline" id = 'rbcCast_div_<%=i%>'>
		<input type='text' class='form-control'  name='rbcCast' id = 'rbcCast_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.rbcCast + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_crystals'>")%>
			
	
	<div class="form-inline" id = 'crystals_div_<%=i%>'>
		<input type='text' class='form-control'  name='crystals' id = 'crystals_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.crystals + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_otherThings'>")%>
			
	
	<div class="form-inline" id = 'otherThings_div_<%=i%>'>
		<input type='text' class='form-control'  name='otherThings' id = 'otherThings_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.otherThings + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedByUserId'>")%>
			

		<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedByOrganogramId'>")%>
			

		<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.insertedByOrganogramId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + urine_examination_reportDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=urine_examination_reportDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Urine_examination_reportServlet?actionType=view&ID=<%=urine_examination_reportDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Urine_examination_reportServlet?actionType=view&modal=1&ID=<%=urine_examination_reportDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	