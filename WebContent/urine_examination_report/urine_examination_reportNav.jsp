<%@page import="language.LC"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="language.LM"%>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator"%>
<%@ page import="java.util.Arrays"%>
<%@ page import="searchform.SearchForm"%>
<%@ page import="pb.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>


<%
	System.out.println("Inside nav.jsp");
	String url = request.getParameter("url");
	String navigator = request.getParameter("navigator");
	String pageName = request.getParameter("pageName");
	if (pageName == null)
		pageName = "Search";
	String pageno = "";
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
	pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

	System.out.println("rn " + rn);

	String action = url;
	String context = "../../.." + request.getContextPath() + "/";
	String link = context + url;
	String concat = "?";
	if (url.contains("?")) {
		concat = "&";
	}
	String[][] searchFieldInfo = rn.getSearchFieldInfo();
	String totalPage = "1";
	if (rn != null)
		totalPage = rn.getTotalPages() + "";
	int row = 0;

	String Language = LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_LANGUAGE, loginDTO);
	String Options;
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	Date date = new Date();
	String datestr = dateFormat.format(date);
	int pagination_number = 0;
	boolean isPermanentTable = rn.m_isPermanentTable;
	System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
%>






<!-- search control -->
<div class="portlet box portlet-btcl">
	<div class="portlet-title">
		<div class="caption" style="margin-top: 5px;"><i class="fa fa-search-plus"  style="margin-top:-3px"></i><%=pageName%></div>
		<p class="desktop-only" style="float:right; margin:10px 5px !important;">Advanced Search</p>
		<div class="tools">
			<a class="expand" href="javascript:" data-original-title="" title=""></a>
		</div>
		
		<div class="col-xs-12 col-sm-5 col-md-4" style="margin-top:10px">
		<%
			
			out.println("<input type='text' class='form-control' onKeyUp='allfield_changed(\"\",0)' id='anyfield'  name='"+  LM.getText(LC.URINE_EXAMINATION_REPORT_SEARCH_ANYFIELD, loginDTO) +"' ");
			String value = (String)session.getAttribute(searchFieldInfo[searchFieldInfo.length - 1][1]);
			
			if( value != null)
			{
				out.println("value = '" + value + "'");
			}
			
			out.println ("/><br />");
		%> 
		</div>
		
		
		
	</div>
	<div 
	class="portlet-body form collapse"
	>
		<!-- BEGIN FORM-->
		<div class="container-fluid">
			<div class="row col-lg-offset-1">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.URINE_EXAMINATION_REPORT_SEARCH_COMMONLABREPORTID, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="common_lab_report_id" placeholder="" name="common_lab_report_id" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.URINE_EXAMINATION_REPORT_SEARCH_QUANTITY, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="quantity" placeholder="" name="quantity" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.URINE_EXAMINATION_REPORT_SEARCH_COLOR, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="color" placeholder="" name="color" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.URINE_EXAMINATION_REPORT_SEARCH_REACTION, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="reaction" placeholder="" name="reaction" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.URINE_EXAMINATION_REPORT_SEARCH_GRAVITY, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="gravity" placeholder="" name="gravity" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.URINE_EXAMINATION_REPORT_SEARCH_ALBUMIN, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="albumin" placeholder="" name="albumin" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.URINE_EXAMINATION_REPORT_SEARCH_SUGAR, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="sugar" placeholder="" name="sugar" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.URINE_EXAMINATION_REPORT_SEARCH_BILESALTS, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="bile_salts" placeholder="" name="bile_salts" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.URINE_EXAMINATION_REPORT_SEARCH_PUSSCELLS, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="puss_cells" placeholder="" name="puss_cells" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.URINE_EXAMINATION_REPORT_SEARCH_EPCELLS, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="ep_cells" placeholder="" name="ep_cells" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.URINE_EXAMINATION_REPORT_SEARCH_RBC, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="rbc" placeholder="" name="rbc" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.URINE_EXAMINATION_REPORT_SEARCH_RBCCAST, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="rbc_cast" placeholder="" name="rbc_cast" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.URINE_EXAMINATION_REPORT_SEARCH_CRYSTALS, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="crystals" placeholder="" name="crystals" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.URINE_EXAMINATION_REPORT_SEARCH_OTHERTHINGS, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="other_things" placeholder="" name="other_things" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.URINE_EXAMINATION_REPORT_SEARCH_INSERTEDBYUSERID, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="inserted_by_user_id" placeholder="" name="inserted_by_user_id" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.URINE_EXAMINATION_REPORT_SEARCH_INSERTEDBYORGANOGRAMID, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="inserted_by_organogram_id" placeholder="" name="inserted_by_organogram_id" onChange='setSearchChanged()'>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
					<div class="col-xs-2 col-sm-4 col-md-4">
						<label for="" class="control-label pull-right"><%=LM.getText(LC.URINE_EXAMINATION_REPORT_SEARCH_INSERTIONDATE, loginDTO)%></label>
					</div>
					<div class="col-xs-10 col-sm-8 col-md-8">
						<input type="text" class="form-control" id="insertion_date" placeholder="" name="insertion_date" onChange='setSearchChanged()'>
					</div>
				</div>

			</div>	


			<div class=clearfix></div>

			<div class="form-actions fluid" style="margin-top:10px">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-offset-3 col-xs-12 col-md-12  col-md-12 col-lg-9">							
							<div class="col-xs-4  col-sm-4  col-md-6">
								<input type="hidden" name="search" value="yes" />
								<!-- 				          	<input type="reset" class="btn  btn-sm btn btn-circle  grey-mint btn-outline sbold uppercase" value="Reset" > -->
								<input type="submit" onclick="allfield_changed('',0)"
									class="btn  btn-sm btn btn-circle btn-sm green-meadow btn-outline sbold uppercase advanceseach"
									value="<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>">
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<!-- END FORM-->
	</div>


<%@include file="../common/pagination_with_go2.jsp"%>


<template id = "loader">
<div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
</div>
</template>


<script type="text/javascript">

	function dosubmit(params)
	{
		document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
		//alert(params);
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) 
		    {
		    	document.getElementById('tableForm').innerHTML = this.responseText ;
				setPageNo();
				searchChanged = 0;
			}
		    else if(this.readyState == 4 && this.status != 200)
			{
				alert('failed ' + this.status);
			}
		  };
		  
		  xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
		  xhttp.send();
		
	}

	function allfield_changed(go, pagination_number)
	{
		var params = 'AnyField=' + document.getElementById('anyfield').value;
		<%
			for(int i = 0; i < searchFieldInfo.length - 1; i ++)
			{
				out.println("params += '&" +  searchFieldInfo[i][1] + "='+document.getElementById('" 
					+ searchFieldInfo[i][1] + "').value");
			}
		%>
		params +=  '&search=true&ajax=true';
		
		var extraParams = document.getElementsByName('extraParam');
		extraParams.forEach((param) => {
			params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

		var pageNo = document.getElementsByName('pageno')[0].value;
		var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

		var totalRecords = 0;
		var lastSearchTime = 0;
		if(document.getElementById('hidden_totalrecords'))
		{
			totalRecords = document.getElementById('hidden_totalrecords').value;
			lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
		}


		if(go !== '' && searchChanged == 0)
		{
			console.log("go found");
			params += '&go=1';
			pageNo = document.getElementsByName('pageno')[pagination_number].value;
			rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
			setPageNoInAllFields(pageNo);
			setRPPInAllFields(rpp);
		}
		params += '&pageno=' + pageNo;
		params += '&RECORDS_PER_PAGE=' + rpp;
		params += '&TotalRecords=' + totalRecords;
		params += '&lastSearchTime=' + lastSearchTime;
		dosubmit(params);
	
	}

</script>

