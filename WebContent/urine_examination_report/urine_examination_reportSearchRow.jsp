<%@page pageEncoding="UTF-8" %>

<%@page import="urine_examination_report.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_URINE_EXAMINATION_REPORT;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Urine_examination_reportDTO urine_examination_reportDTO = (Urine_examination_reportDTO)request.getAttribute("urine_examination_reportDTO");
CommonDTO commonDTO = urine_examination_reportDTO;
String servletName = "Urine_examination_reportServlet";


System.out.println("urine_examination_reportDTO = " + urine_examination_reportDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Urine_examination_reportDAO urine_examination_reportDAO = (Urine_examination_reportDAO)request.getAttribute("urine_examination_reportDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

											
		
											
											<td id = '<%=i%>_commonLabReportId'>
											<%
											value = urine_examination_reportDTO.commonLabReportId + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_quantity'>
											<%
											value = urine_examination_reportDTO.quantity + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_color'>
											<%
											value = urine_examination_reportDTO.color + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_reaction'>
											<%
											value = urine_examination_reportDTO.reaction + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_gravity'>
											<%
											value = urine_examination_reportDTO.gravity + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_albumin'>
											<%
											value = urine_examination_reportDTO.albumin + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_sugar'>
											<%
											value = urine_examination_reportDTO.sugar + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_bileSalts'>
											<%
											value = urine_examination_reportDTO.bileSalts + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_pussCells'>
											<%
											value = urine_examination_reportDTO.pussCells + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_epCells'>
											<%
											value = urine_examination_reportDTO.epCells + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_rbc'>
											<%
											value = urine_examination_reportDTO.rbc + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_rbcCast'>
											<%
											value = urine_examination_reportDTO.rbcCast + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_crystals'>
											<%
											value = urine_examination_reportDTO.crystals + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_otherThings'>
											<%
											value = urine_examination_reportDTO.otherThings + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_insertedByUserId'>
											<%
											value = urine_examination_reportDTO.insertedByUserId + "";
											%>
											<%
											value = WorkflowController.getNameFromUserId(urine_examination_reportDTO.insertedByUserId, Language);
											if(value.equalsIgnoreCase(""))
											{
												value = "superman";
											}
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_insertedByOrganogramId'>
											<%
											value = urine_examination_reportDTO.insertedByOrganogramId + "";
											%>
											<%
											value = WorkflowController.getNameFromOrganogramId(urine_examination_reportDTO.insertedByOrganogramId, Language);
											if(value.equalsIgnoreCase(""))
											{
												value = "superman";
											}
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_insertionDate'>
											<%
											value = urine_examination_reportDTO.insertionDate + "";
											%>
											<%
											String formatted_insertionDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_insertionDate%>
				
			
											</td>
		
											
		
											
		
	

											<td>
												<a href='Urine_examination_reportServlet?actionType=view&ID=<%=urine_examination_reportDTO.iD%>'>View</a>
										
											</td>
	
											<td id = '<%=i%>_Edit'>																																	
	
												<a href='Urine_examination_reportServlet?actionType=getEditPage&ID=<%=urine_examination_reportDTO.iD%>'><%=LM.getText(LC.URINE_EXAMINATION_REPORT_SEARCH_URINE_EXAMINATION_REPORT_EDIT_BUTTON, loginDTO)%></a>
																				
											</td>											
											
											
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=urine_examination_reportDTO.iD%>'/></span>
												</div>
											</td>
																						
											

