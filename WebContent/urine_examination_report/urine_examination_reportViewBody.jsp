

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="urine_examination_report.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>




<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
Urine_examination_reportDAO urine_examination_reportDAO = new Urine_examination_reportDAO("urine_examination_report");
Urine_examination_reportDTO urine_examination_reportDTO = (Urine_examination_reportDTO)urine_examination_reportDAO.getDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<div class="modal-content viewmodal">
            <div class="modal-header">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9 col-sm-12">
                            <h5 class="modal-title">Urine Examination Report Details</h5>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="modal-body container">
			
			<div class="row div_border office-div">

                    <div class="col-md-12">
                        <h3>Urine Examination Report</h3>
						<table class="table table-bordered table-striped">
									

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_COMMONLABREPORTID, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.commonLabReportId + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_QUANTITY, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.quantity + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_COLOR, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.color + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_REACTION, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.reaction + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_GRAVITY, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.gravity + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_ALBUMIN, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.albumin + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_SUGAR, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.sugar + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_BILESALTS, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.bileSalts + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_PUSSCELLS, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.pussCells + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_EPCELLS, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.epCells + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_RBC, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.rbc + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_RBCCAST, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.rbcCast + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_CRYSTALS, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.crystals + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_OTHERTHINGS, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.otherThings + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_INSERTEDBYUSERID, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.insertedByUserId + "";
											%>
											<%
											value = WorkflowController.getNameFromUserId(urine_examination_reportDTO.insertedByUserId, Language);
											if(value.equalsIgnoreCase(""))
											{
												value = "superman";
											}
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_INSERTEDBYORGANOGRAMID, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.insertedByOrganogramId + "";
											%>
											<%
											value = WorkflowController.getNameFromOrganogramId(urine_examination_reportDTO.insertedByOrganogramId, Language);
											if(value.equalsIgnoreCase(""))
											{
												value = "superman";
											}
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_INSERTIONDATE, loginDTO)%></b></td>
								<td>
						
											<%
											value = urine_examination_reportDTO.insertionDate + "";
											%>
											<%
											String formatted_insertionDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_insertionDate%>
				
			
								</td>
						
							</tr>

				


			
			
			
		
						</table>
                    </div>
			






			</div>	

               


        </div>