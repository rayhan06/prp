
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="urine_examination_report.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="util.TimeFormat"%>

<%
Urine_examination_reportDTO urine_examination_reportDTO;
urine_examination_reportDTO = (Urine_examination_reportDTO)request.getAttribute("urine_examination_reportDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(urine_examination_reportDTO == null)
{
	urine_examination_reportDTO = new Urine_examination_reportDTO();
	
}
System.out.println("urine_examination_reportDTO = " + urine_examination_reportDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_URINE_EXAMINATION_REPORT_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.URINE_EXAMINATION_REPORT_ADD_URINE_EXAMINATION_REPORT_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

boolean isPermanentTable = true;
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="Urine_examination_reportServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
				
				
				




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=urine_examination_reportDTO.iD%>' tag='pb_html'/>
	
												

		<input type='hidden' class='form-control'  name='commonLabReportId' id = 'commonLabReportId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.commonLabReportId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_QUANTITY, loginDTO)):(LM.getText(LC.URINE_EXAMINATION_REPORT_ADD_QUANTITY, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'quantity_div_<%=i%>'>	
		<input type='text' class='form-control'  name='quantity' id = 'quantity_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.quantity + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_COLOR, loginDTO)):(LM.getText(LC.URINE_EXAMINATION_REPORT_ADD_COLOR, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'color_div_<%=i%>'>	
		<input type='text' class='form-control'  name='color' id = 'color_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.color + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_REACTION, loginDTO)):(LM.getText(LC.URINE_EXAMINATION_REPORT_ADD_REACTION, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'reaction_div_<%=i%>'>	
		<input type='text' class='form-control'  name='reaction' id = 'reaction_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.reaction + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_GRAVITY, loginDTO)):(LM.getText(LC.URINE_EXAMINATION_REPORT_ADD_GRAVITY, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'gravity_div_<%=i%>'>	
		<input type='text' class='form-control'  name='gravity' id = 'gravity_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.gravity + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_ALBUMIN, loginDTO)):(LM.getText(LC.URINE_EXAMINATION_REPORT_ADD_ALBUMIN, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'albumin_div_<%=i%>'>	
		<input type='text' class='form-control'  name='albumin' id = 'albumin_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.albumin + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_SUGAR, loginDTO)):(LM.getText(LC.URINE_EXAMINATION_REPORT_ADD_SUGAR, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'sugar_div_<%=i%>'>	
		<input type='text' class='form-control'  name='sugar' id = 'sugar_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.sugar + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_BILESALTS, loginDTO)):(LM.getText(LC.URINE_EXAMINATION_REPORT_ADD_BILESALTS, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'bileSalts_div_<%=i%>'>	
		<input type='text' class='form-control'  name='bileSalts' id = 'bileSalts_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.bileSalts + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_PUSSCELLS, loginDTO)):(LM.getText(LC.URINE_EXAMINATION_REPORT_ADD_PUSSCELLS, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'pussCells_div_<%=i%>'>	
		<input type='text' class='form-control'  name='pussCells' id = 'pussCells_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.pussCells + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_EPCELLS, loginDTO)):(LM.getText(LC.URINE_EXAMINATION_REPORT_ADD_EPCELLS, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'epCells_div_<%=i%>'>	
		<input type='text' class='form-control'  name='epCells' id = 'epCells_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.epCells + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_RBC, loginDTO)):(LM.getText(LC.URINE_EXAMINATION_REPORT_ADD_RBC, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'rbc_div_<%=i%>'>	
		<input type='text' class='form-control'  name='rbc' id = 'rbc_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.rbc + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_RBCCAST, loginDTO)):(LM.getText(LC.URINE_EXAMINATION_REPORT_ADD_RBCCAST, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'rbcCast_div_<%=i%>'>	
		<input type='text' class='form-control'  name='rbcCast' id = 'rbcCast_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.rbcCast + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_CRYSTALS, loginDTO)):(LM.getText(LC.URINE_EXAMINATION_REPORT_ADD_CRYSTALS, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'crystals_div_<%=i%>'>	
		<input type='text' class='form-control'  name='crystals' id = 'crystals_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.crystals + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_OTHERTHINGS, loginDTO)):(LM.getText(LC.URINE_EXAMINATION_REPORT_ADD_OTHERTHINGS, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'otherThings_div_<%=i%>'>	
		<input type='text' class='form-control'  name='otherThings' id = 'otherThings_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.otherThings + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.insertedByOrganogramId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + urine_examination_reportDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + urine_examination_reportDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
					
	






				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						%>
						<%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_URINE_EXAMINATION_REPORT_CANCEL_BUTTON, loginDTO)%>
						<%
					}
					else
					{
						%>
						<%=LM.getText(LC.URINE_EXAMINATION_REPORT_ADD_URINE_EXAMINATION_REPORT_CANCEL_BUTTON, loginDTO)%>
						<%
					}
					
					%>
					</a>
					<button class="btn btn-success" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						%>
						<%=LM.getText(LC.URINE_EXAMINATION_REPORT_EDIT_URINE_EXAMINATION_REPORT_SUBMIT_BUTTON, loginDTO)%>
						<%
					}
					else
					{
						%>
						<%=LM.getText(LC.URINE_EXAMINATION_REPORT_ADD_URINE_EXAMINATION_REPORT_SUBMIT_BUTTON, loginDTO)%>
						<%
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


$(document).ready( function(){

    dateTimeInit("<%=Language%>");
});

function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}


	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Urine_examination_reportServlet");	
}

function init(row)
{


	
}

var row = 0;
	
window.onload =function ()
{
	init(row);
	CKEDITOR.replaceAll();
}

var child_table_extra_id = <%=childTableStartingID%>;



</script>






