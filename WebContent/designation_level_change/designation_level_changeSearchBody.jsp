<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="util.RecordNavigator" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%
    String url = "Designation_level_changeServlet?actionType=search";
    String navigator2 = SessionConstants.NAV_DESIGNATION_LEVEL_CHANGE;
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String pageno = "";
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator2);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();
    boolean isPermanentTable = rn.m_isPermanentTable;
    System.out.println("rn " + rn);
    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    int pagination_number = 0;
%>
<jsp:include page="./designation_level_changeNav.jsp" flush="true">
    <jsp:param name="url" value="<%=url%>"/>
    <jsp:param name="navigator" value="<%=navigator2%>"/>
    <jsp:param name="pageName"
               value="<%=LM.getText(LC.DESIGNATION_LEVEL_CHANGE_SEARCH_DESIGNATION_LEVEL_CHANGE_SEARCH_FORMNAME, loginDTO)%>"/>
</jsp:include>
<div class="py-3">
    <form action="Designation_level_changeServlet?isPermanentTable=<%=isPermanentTable%>&actionType=delete"
          method="POST" id="tableForm" enctype="multipart/form-data">
        <jsp:include page="designation_level_changeSearchForm.jsp" flush="true">
            <jsp:param name="pageName"
                       value="<%=LM.getText(LC.DESIGNATION_LEVEL_CHANGE_SEARCH_DESIGNATION_LEVEL_CHANGE_SEARCH_FORMNAME, loginDTO)%>"/>
        </jsp:include>
    </form>
</div>
<div class="my-3">
    <input type="button" class="btn btn-primary btn-hover-brand btn-square" value="Submit" onclick="submitDesignationLevelChange()">
</div>
<div id="snackbar">Success</div>
<% pagination_number = 1;%>
<%@include file="../common/pagination_with_go2.jsp" %>
</div>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function submitDesignationLevelChange() {
        let changed_designation = [];
        for (let i = 0; ; i++) {
            try {
                let id = document.getElementById(i + '_id').value;
                let l = document.getElementById(i + '_level').value;
                let s = document.getElementById(i + '_sequence').value;
                changed_designation.push({id: id, level: l, sequence: s});
            } catch (e) {
                break;
            }
        }
        console.log(changed_designation);
        let data = "";
        changed_designation.forEach(function (value) {
            data += "&data=" + value.id + " " + value.level + " " + value.sequence;
        });
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                var x = document.getElementById("snackbar");
                x.className = "show";
                setTimeout(function () {
                    x.className = x.className.replace("show", "");
                }, 3000);
            }
        };
        xhttp.open("Post", "DesignationLabelChangeServlet?actionType=updateLevel&data=" + data, true);
        xhttp.send();
    }
</script>
<style>
    #snackbar {
        visibility: hidden;
        min-width: 500px;
        background-color: #00a65a;
        color: #fff;
        text-align: center;
        padding: 16px;
        position: fixed;
        z-index: 1;
        left: 40%;
        bottom: 30px;
    }
    #snackbar.show {
        visibility: visible;
        -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }
    @-webkit-keyframes fadein {
        from {
            bottom: 0;
            opacity: 0;
        }
        to {
            bottom: 30px;
            opacity: 1;
        }
    }
    @keyframes fadein {
        from {
            bottom: 0;
            opacity: 0;
        }
        to {
            bottom: 30px;
            opacity: 1;
        }
    }
    @-webkit-keyframes fadeout {
        from {
            bottom: 30px;
            opacity: 1;
        }
        to {
            bottom: 0;
            opacity: 0;
        }
    }
    @keyframes fadeout {
        from {
            bottom: 30px;
            opacity: 1;
        }
        to {
            bottom: 0;
            opacity: 0;
        }
    }
</style>