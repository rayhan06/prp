
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="designation_level_change.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%
Designation_level_changeDTO designation_level_changeDTO;
designation_level_changeDTO = (Designation_level_changeDTO)request.getAttribute("designation_level_changeDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(designation_level_changeDTO == null)
{
	designation_level_changeDTO = new Designation_level_changeDTO();
	
}
System.out.println("designation_level_changeDTO = " + designation_level_changeDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.DESIGNATION_LEVEL_CHANGE_EDIT_DESIGNATION_LEVEL_CHANGE_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.DESIGNATION_LEVEL_CHANGE_ADD_DESIGNATION_LEVEL_CHANGE_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";
Designation_level_changeDTO row = designation_level_changeDTO;
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="Designation_level_changeServlet?actionType=<%=actionName%>&identity=<%=ID%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
				
				
				




























	














<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.DESIGNATION_LEVEL_CHANGE_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
Date date = new Date();
String datestr = dateFormat.format(date);
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=ID%>'/>
	
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.DESIGNATION_LEVEL_CHANGE_EDIT_DESIGNATIONENG, loginDTO)):(LM.getText(LC.DESIGNATION_LEVEL_CHANGE_ADD_DESIGNATIONENG, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'designationEng_div_<%=i%>'>	
		<input type='text' class='form-control'  name='designationEng' id = 'designationEng_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + designation_level_changeDTO.designationEng + "'"):("'" + " " + "'")%>   />					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.DESIGNATION_LEVEL_CHANGE_EDIT_DESIGNATIONBNG, loginDTO)):(LM.getText(LC.DESIGNATION_LEVEL_CHANGE_ADD_DESIGNATIONBNG, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'designationBng_div_<%=i%>'>	
		<input type='text' class='form-control'  name='designationBng' id = 'designationBng_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + designation_level_changeDTO.designationBng + "'"):("'" + " " + "'")%>   />					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.DESIGNATION_LEVEL_CHANGE_EDIT_DESIGNATIONLEVEL, loginDTO)):(LM.getText(LC.DESIGNATION_LEVEL_CHANGE_ADD_DESIGNATIONLEVEL, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'designationLevel_div_<%=i%>'>	
		<input type='text' class='form-control'  name='designationLevel' id = 'designationLevel_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + designation_level_changeDTO.designationLevel + "'"):("'" + "0" + "'")%>   />					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.DESIGNATION_LEVEL_CHANGE_EDIT_DESIGNATIONSEQUENCE, loginDTO)):(LM.getText(LC.DESIGNATION_LEVEL_CHANGE_ADD_DESIGNATIONSEQUENCE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'designationSequence_div_<%=i%>'>	
		<input type='text' class='form-control'  name='designationSequence' id = 'designationSequence_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + designation_level_changeDTO.designationSequence + "'"):("'" + "0" + "'")%>   />					
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + designation_level_changeDTO.isDeleted + "'"):("'" + "false" + "'")%>/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + designation_level_changeDTO.lastModificationTime + "'"):("'" + "0" + "'")%>/>
												
					
	







				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.DESIGNATION_LEVEL_CHANGE_EDIT_DESIGNATION_LEVEL_CHANGE_CANCEL_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.DESIGNATION_LEVEL_CHANGE_ADD_DESIGNATION_LEVEL_CHANGE_CANCEL_BUTTON, loginDTO));
					}
					
					%>
					</a>
					<button class="btn btn-success" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.DESIGNATION_LEVEL_CHANGE_EDIT_DESIGNATION_LEVEL_CHANGE_SUBMIT_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.DESIGNATION_LEVEL_CHANGE_ADD_DESIGNATION_LEVEL_CHANGE_SUBMIT_BUTTON, loginDTO));
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">




function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}

	return true;
}

function PostprocessAfterSubmiting(row)
{
}


function init(row)
{
}var row = 0;
bkLib.onDomLoaded(function() 
{	
});
	
window.onload =function ()
{
	init(row);
}





</script>






