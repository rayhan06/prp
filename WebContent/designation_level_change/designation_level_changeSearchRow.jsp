<%@page pageEncoding="UTF-8" %>
<%@page import="designation_level_change.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.DESIGNATION_LEVEL_CHANGE_EDIT_LANGUAGE, loginDTO);
    Designation_level_changeDTO row = (Designation_level_changeDTO) request.getAttribute("designation_level_changeDTO");
    if (row == null) {
        row = new Designation_level_changeDTO();
    }
    System.out.println("row = " + row);
    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");
    String value = "";
    UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
    String navigator2 = SessionConstants.NAV_DESIGNATION_LEVEL_CHANGE;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;
    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";
    Designation_level_changeDAO designation_level_changeDAO = (Designation_level_changeDAO) request.getAttribute("designation_level_changeDAO");
    TempTableDTO tempTableDTO = null;
    if (!isPermanentTable) {
        tempTableDTO = designation_level_changeDAO.getTempTableDTOFromTableById(tableName, row.iD);
    }
    out.println("<input value='" + row.iD + "' type='hidden' id='" + i + "_id' />");
    out.println("<td id = '" + i + "_designationEng'>");
    value = row.designationEng + "";
    out.println(value);
    out.println("</td>");
    out.println("<td id = '" + i + "_designationBng'>");
    value = row.designationBng + "";
    out.println(value);
    out.println("</td>");
    out.println("<td id = '" + i + "_designationLevel'>");
    value = row.designationLevel + "";
    out.println("<input class='form-control' type='text' value='" + value + "' id='" + i + "_level'" + "/>");
    out.println("</td>");
    out.println("<td id = '" + i + "_designationSequence'>");
    value = row.designationSequence + "";
    out.println("<input class='form-control' type='text' value='" + value + "' id='" + i + "_sequence'" + "/>");
    out.println("</td>");
%>