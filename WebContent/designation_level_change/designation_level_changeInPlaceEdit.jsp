<%@page pageEncoding="UTF-8" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="designation_level_change.Designation_level_changeDTO" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%
    Designation_level_changeDTO designation_level_changeDTO = (Designation_level_changeDTO) request.getAttribute("designation_level_changeDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (designation_level_changeDTO == null) {
        designation_level_changeDTO = new Designation_level_changeDTO();
    }
    System.out.println("designation_level_changeDTO = " + designation_level_changeDTO);
    String actionName = "edit";
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = Integer.parseInt(request.getParameter("rownum"));
    String deletedStyle = request.getParameter("deletedstyle");
    String value = "";
    Designation_level_changeDTO row = designation_level_changeDTO;
%>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%
    String Language = LM.getText(LC.DESIGNATION_LEVEL_CHANGE_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Date date = new Date();
    String datestr = dateFormat.format(date);
%>
<%=("<td id = '" + i + "_iD" + "' style='display:none;'>")%>
<input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>' value='<%=ID%>'/>
<%=("</td>")%>
<%=("<td id = '" + i + "_designationEng'>")%>
<div class="form-inline" id='designationEng_div_<%=i%>'>
    <input type='text' class='form-control' name='designationEng' id='designationEng_text_<%=i%>'
           value=<%=actionName.equals("edit")?("'" + designation_level_changeDTO.designationEng + "'"):("'" + " " + "'")%>/>
</div>
<%=("</td>")%>
<%=("<td id = '" + i + "_designationBng'>")%>
<div class="form-inline" id='designationBng_div_<%=i%>'>
    <input type='text' class='form-control' name='designationBng' id='designationBng_text_<%=i%>'
           value=<%=actionName.equals("edit")?("'" + designation_level_changeDTO.designationBng + "'"):("'" + " " + "'")%>/>
</div>
<%=("</td>")%>
<%=("<td id = '" + i + "_designationLevel'>")%>
<div class="form-inline" id='designationLevel_div_<%=i%>'>
    <input type='text' class='form-control' name='designationLevel' id='designationLevel_text_<%=i%>'
           value=<%=actionName.equals("edit")?("'" + designation_level_changeDTO.designationLevel + "'"):("'" + "0" + "'")%>/>
</div>
<%=("</td>")%>
<%=("<td id = '" + i + "_designationSequence'>")%>
<div class="form-inline" id='designationSequence_div_<%=i%>'>
    <input type='text' class='form-control' name='designationSequence' id='designationSequence_text_<%=i%>'
           value=<%=actionName.equals("edit")?("'" + designation_level_changeDTO.designationSequence + "'"):("'" + "0" + "'")%>/>
</div>
<%=("</td>")%>
<%=("<td id = '" + i + "_isDeleted" + "' style='display:none;'>")%>
<input type='hidden' class='form-control' name='isDeleted' id='isDeleted_hidden_<%=i%>'
       value= <%=actionName.equals("edit") ? ("'" + designation_level_changeDTO.isDeleted + "'") : ("'" + "false" + "'")%>/>
<%=("</td>")%>
<%=("<td id = '" + i + "_lastModificationTime" + "' style='display:none;'>")%>
<input type='hidden' class='form-control' name='lastModificationTime' id='lastModificationTime_hidden_<%=i%>'
       value='<%=designation_level_changeDTO.lastModificationTime%>'/>
<%=("</td>")%>