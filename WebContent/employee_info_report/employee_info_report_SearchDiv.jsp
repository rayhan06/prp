<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="religion.ReligionRepository" %>
<%@ page import="user.*" %>
<%@ page import="login.*" %>
<%@ page import="sessionmanager.*" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="geolocation.GeoLocationRepository" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="role.PermissionRepository" %>
<%@ page import="employee_office_report.InChargeLevelEnum" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.stream.Collectors" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = LM.getText(LC.EMPLOYEE_INFO_REPORT_EDIT_LANGUAGE, loginDTO);
    String context = "../../.." + request.getContextPath() + "/assets/";
    int year = Calendar.getInstance().get(Calendar.YEAR);
    boolean isLangEng = Language.equalsIgnoreCase("English");
%>

<jsp:include page="../employee_assign/officeMultiSelectTagsUtil.jsp"/>

<script src="<%=context%>scripts/util1.js"></script>
<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>
<div class="row px-4">
    <div class="col-12">
        <div class="row">
            <div id="criteriaSelectionId" class="search-criteria-div col-md-6">
                <div class="form-group row">
                    <div class="col-md-3"></div>
                    <div class="col-md-9">
                        <button type="button"
                                class="btn btn-sm btn-info btn-info text-white shadow btn-border-radius"
                                data-toggle="modal" data-target="#select_criteria_div" id="select_criteria_btn"
                                onclick="beforeOpenCriteriaModal()">
                            <%=isLangEng ? "Criteria Select" : "ক্রাইটেরিয়া বাছাই"%>
                        </button>
                    </div>
                </div>
            </div>
            <div id="criteriaPaddingId" class="search-criteria-div col-md-6">
            </div>
            <div id="officeUnitId" class="search-criteria-div col-md-6 officeModalClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_OFFICEUNITID, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                id="office_units_id_modal_button"
                                onclick="officeModalButtonClicked();">
                            <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                        </button>
                        <input type="hidden" name='officeUnitIds' id='officeUnitIds_input' value="">
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6 officeModalClass" style="display: none">
                <div class="form-group row">
                    <label class="col-sm-4 col-xl-3 col-form-label text-md-right" for="onlySelectedOffice_checkbox">
                        <%=isLangEng ? "Only Selected Office" : "শুধুমাত্র নির্বাচিত অফিস" %>
                    </label>
                    <div class="col-1" id='onlySelectedOffice'>
                        <input type='checkbox' class='form-control-sm mt-1' name='onlySelectedOffice'
                               id='onlySelectedOffice_checkbox'
                               onchange="this.value = this.checked;" value='false'>
                    </div>
                    <div class="col-8"></div>
                </div>
            </div>

            <div class="search-criteria-div col-12" id="selected-offices" style="display: none;">
                <div class="form-group row">
                    <div class="offset-1 col-10 tag-container" id="selected-offices-tag-container">
                        <div class="tag template-tag">
                            <span class="tag-name"></span>
                            <i class="fas fa-times-circle tag-remove-btn"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div id="officeUnitOrganogramId" class="search-criteria-div col-md-6 employeeModalClass"
                 style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITORGANOGRAMID, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                id="organogram_id_modal_button"
                                onclick="organogramIdModalBtnClicked();">
                            <%=LM.getText(LC.LANGUAGE_SELECT, userDTO)%>
                        </button>
                        <div class="input-group" id="organogram_id_div" style="display: none">
                            <input type="hidden" name='officeUnitOrganogramId' id='organogram_id_input' value="">
                            <button type="button"
                                    class="btn btn-secondary btn-block shadow btn-border-radius form-control" disabled
                                    id="organogram_id_text"></button>
                            <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                            <button type="button" class="btn btn-outline-danger shadow btn-border-radius modalCross"
                                    onclick="crsBtnClicked('organogram_id');"
                                    id='organogram_id_crs_btn' tag='pb_html'>
                                x
                            </button>
                        </span>
                        </div>
                    </div>
                </div>
            </div>

            <div id="designation_select_div" class="search-criteria-div col-md-6 designationClass"
                 style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="designationNameSelect">
                        <%=isLangEng ? "Designation Name" : "পদবী নাম" %>
                    </label>
                    <div class="col-md-9">
                        <select multiple="multiple" class='form-control rounded' name='designationNameSelect'
                                id='designationNameSelect'
                        <%=Employee_recordsRepository.getInstance().buildOrganogramKeyOptionAll(null, Language)%>
                        </select>
                        <input type="hidden" name="designationName" id="designationName" value="">
                    </div>
                </div>
            </div>

            <div id="name_eng_div" class="search-criteria-div col-md-6 employeeNameClass"
                 style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="nameEng">
                        <%=isLangEng ? "Name(English)" : "নাম(ইংরেজি)"%>
                    </label>
                    <div class="col-md-9">
                        <input class="englishOnly form-control" type="text" name="nameEng" id="nameEng"
                               style="width: 100%"
                               placeholder="<%=isLangEng? "Enter Employee Name in English":"কর্মকর্তা/কর্মচারীর নাম ইংরেজিতে লিখুন"%>"
                               value="">
                    </div>
                </div>
            </div>

            <div id="name_bng_div" class="search-criteria-div col-md-6 employeeNameClass"
                 style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="nameBng">
                        <%=isLangEng ? "Name(Bangla)" : "নাম(বাংলা)"%>
                    </label>
                    <div class="col-md-9">
                        <input class="noEnglish form-control" type="text" name="nameBng" id="nameBng"
                               style="width: 100%"
                               placeholder="<%=isLangEng? "Enter Employee Name in Bangla":"কর্মকর্তা/কর্মচারীর নাম বাংলাতে লিখুন"%>"
                               value="">
                    </div>
                </div>
            </div>
            
            <div id="official_email_div" class="search-criteria-div col-md-6 officialEmailClass"
                 style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="official_email">
                        <%=isLangEng ? "Official Email" : "অফিসিয়াল ইমেইল"%>
                    </label>
                    <div class="col-md-9">
                        <input class="form-control" type="text" name="official_email" id="official_email"
                               style="width: 100%"
                               
                               value="">
                    </div>
                </div>
            </div>
            
             <div id="personal_email_div" class="search-criteria-div col-md-6 emailClass"
                 style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="personal_email">
                        <%=isLangEng ? "Personal Email" : "ব্যাক্তিগত ইমেইল"%>
                    </label>
                    <div class="col-md-9">
                        <input class="form-control" type="text" name="personal_email" id="personal_email"
                               style="width: 100%"
                               
                               value="">
                    </div>
                </div>
            </div>
            

            <div id="bloodGroup_div" class="search-criteria-div col-md-6 bloodGroupClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="bloodGroup">
                        <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_BLOODGROUP, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control' name='bloodGroup' id='bloodGroup'
                                style="width: 100%">
                            <%=CatRepository.getInstance().buildOptions("blood_group", Language, null)%>
                        </select>
                    </div>
                </div>
            </div>

            <div id="religion_div" class="search-criteria-div col-md-6 religionClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="religion">
                        <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_RELIGION, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control' name='religion' id='religion'
                                style="width: 100%">
                            <%=ReligionRepository.getInstance().buildOptions(Language, null)%>
                        </select>
                    </div>
                </div>
            </div>

            <div id="gender_div" class="search-criteria-div col-md-6 genderClass" style="display:none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="genderCat">
                        <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_GENDERCAT, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control' name='genderCat' id='genderCat'
                                style="width: 100%">
                            <%=CatRepository.getInstance().buildOptions("gender", Language, null)%>
                        </select>
                    </div>
                </div>
            </div>

            <div id="maritalStatus_div" class="search-criteria-div col-md-6 maritalStatusClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="maritalStatus">
                        <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_MARITALSTATUS, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control' name='maritalStatus' id='maritalStatus'
                                style="width: 100%">
                            <%=CatRepository.getInstance().buildOptions("marital_status", Language, null)%>
                        </select>
                    </div>
                </div>
            </div>

            <div id="employeeClass_div" class="search-criteria-div col-md-6 employeeClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="employeeClassCat">
                        <%=LM.getText(LC.EMPLOYEE_EMPLOYEE_CLASS, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control' name='employeeClassCat' id='employeeClassCat'
                                style="width: 100%">
                            <%=CatRepository.getInstance().buildOptions("employee_class", Language, null)%>
                        </select>
                    </div>
                </div>
            </div>

            <div id="employmentCat_div" class="search-criteria-div col-md-6 employmentCatClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="employmentCat">
                        <%=LM.getText(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control' name='employmentCat' id='employmentCat'
                                style="width: 100%">
                            <%=CatRepository.getInstance().buildOptions("employment", Language, null)%>
                        </select>
                    </div>
                </div>
            </div>

            <div id="emp_officer_div" class="search-criteria-div col-md-6 empOfficerClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="empOfficerCat">
                        <%=LM.getText(LC.EMPLOYEE_OFFICER_TYPE, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control' name='empOfficerCat' id='empOfficerCat'
                                style="width: 100%">
                            <%=CatRepository.getInstance().buildOptions("emp_officer", Language, null)%>
                        </select>
                    </div>
                </div>
            </div>

            <div id="permanent_district_div" class="search-criteria-div permanentDistrictClass col-md-6 present_division"
                 style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control' name='home_district' id='home_district'
                                style="width: 100%">
                            <%=GeoLocationRepository.getInstance().buildOptionByLevel(Language, 2, null)%>
                        </select>
                    </div>
                </div>
            </div>

            <div id="joiningDateStart_div" class="search-criteria-div col-md-6 joiningDateClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_JOININGDATE, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <jsp:include page="/date/date.jsp">
                            <jsp:param name="DATE_ID" value="joining-date-start-js"/>
                            <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            <jsp:param name="END_YEAR" value="<%=year + 50%>"/>
                        </jsp:include>
                        <input type='hidden' class='form-control' id='joiningDateStart'
                               name='joiningDateStart' value=''
                               tag='pb_html'/>
                    </div>
                </div>
            </div>

            <div id="joiningDateEnd_div" class="search-criteria-div col-md-6 joiningDateClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_JOININGDATE_10, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <jsp:include page="/date/date.jsp">
                            <jsp:param name="DATE_ID" value="joining-date-end-js"/>
                            <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            <jsp:param name="END_YEAR" value="<%=year + 50%>"/>
                        </jsp:include>
                        <input type='hidden' class='form-control' id='joiningDateEnd'
                               name='joiningDateEnd' value=''
                               tag='pb_html'/>
                    </div>
                </div>
            </div>

            <div id="parliamentJoiningDateStart_div" class="search-criteria-div col-md-6 parliamentJoiningDateClass"
                 style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_PARLIAMENT_JOINING_DATE_START, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <jsp:include page="/date/date.jsp">
                            <jsp:param name="DATE_ID" value="parliament-joining-date-start-js"/>
                            <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            <jsp:param name="END_YEAR" value="<%=year + 50%>"/>
                        </jsp:include>
                        <input type='hidden' class='form-control' id='parliamentJoiningDateStart'
                               name='parliamentJoiningDateStart' value=''
                               tag='pb_html'/>
                    </div>
                </div>
            </div>
            <div id="parliamentJoiningDateEnd_div" class="search-criteria-div col-md-6 parliamentJoiningDateClass"
                 style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_PARLIAMENT_JOINING_DATE_END, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <jsp:include page="/date/date.jsp">
                            <jsp:param name="DATE_ID" value="parliament-joining-date-end-js"/>
                            <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            <jsp:param name="END_YEAR" value="<%=year + 50%>"/>
                        </jsp:include>
                        <input type='hidden' class='form-control' id='parliamentJoiningDateEnd'
                               name='parliamentJoiningDateEnd' value=''
                               tag='pb_html'/>
                    </div>
                </div>
            </div>

            <div id="lprDateStart_div" class="search-criteria-div col-md-6 lprDateClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_LPRDATE, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <jsp:include page="/date/date.jsp">
                            <jsp:param name="DATE_ID" value="lpr-date-start-js"/>
                            <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            <jsp:param name="END_YEAR" value="<%=year + 50%>"/>
                        </jsp:include>
                        <input type='hidden' class='form-control' id='lprDateStart'
                               name='lprDateStart' value=''
                               tag='pb_html'/>
                    </div>
                </div>
            </div>

            <div id="lprDateEnd_div" class="search-criteria-div col-md-6 lprDateClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_LPRDATE_12, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <jsp:include page="/date/date.jsp">
                            <jsp:param name="DATE_ID" value="lpr-date-end-js"/>
                            <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            <jsp:param name="END_YEAR" value="<%=year + 50%>"/>
                        </jsp:include>
                        <input type='hidden' class='form-control' id='lprDateEnd'
                               name='lprDateEnd' value=''
                               tag='pb_html'/>
                    </div>
                </div>
            </div>

            <div id="age_div" class="search-criteria-div col-md-6 ageClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">
                        <%=LM.getText(LC.HR_REPORT_AGE_RANGE, loginDTO)%>
                    </label>
                    <div class="col-6 col-md-5">
                        <input class='form-control' type="text" name='age_start' id='age_start'
                               placeholder='<%=LM.getText(LC.HR_REPORT_FROM, loginDTO)%>' style="width: 100%">
                    </div>
                    <div class="col-6 col-md-4">
                        <input class='form-control' type="text" name='age_end' id='age_end'
                               placeholder='<%=LM.getText(LC.HR_REPORT_TO, loginDTO)%>' style="width: 100%">
                    </div>
                </div>
            </div>
            <div id="role_div" class="search-criteria-div col-md-6 roleClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="organogram_role_select">
                        <%=LM.getText(LC.USER_ADD_ROLE, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select multiple="multiple" class='form-control rounded' name='organogram_role_select'
                                id='organogram_role_select' style="width: 100%">
                            <%=PermissionRepository.getInstance().buildOptions(Language, null)%>
                        </select>
                        <input type="hidden" name="organogram_role" id="organogram_role" value="">
                    </div>
                </div>
            </div>

            <div id="incharge_label_div" class="search-criteria-div col-md-6 inchargeClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="incharge_label_select">
                        <%=isLangEng ? "In charge Level" : "ইনচার্জ লেভেল"%>
                    </label>
                    <div class="col-md-9">
                        <select multiple="multiple" class='form-control rounded' name='incharge_label_select'
                                id='incharge_label_select' style="width: 100%">
                                <%=InChargeLevelEnum.getBuildOptions(Language)%>
                            <input type="hidden" name="incharge_label" id="incharge_label" value="">
                    </div>
                </div>
            </div>
            <div class="search-criteria-div col-md-6 jobGradeClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="jobGradeTypeCat">
                        <%=LM.getText(LC.DESIGNATION_STATUS_REPORT_WHERE_JOBGRADETYPECAT, loginDTO)%>
                    </label>
                    <div class="col-md-5">
                        <select class='form-control' name='jobGradeTypeCat' id='jobGradeTypeCat'>
                            <%=CatRepository.getOptions(Language, "job_grade_type", CatDTO.CATDEFAULT)%>
                        </select>
                    </div>
                    <div class="col-md-4 form-group row">
                        <div class="col-md-4">
                            <button
                                    type="button" id="lessButton"
                                    class="btn shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                                    style="color: white;background-color: grey"
                                    onclick="lessButtonClicked()"
                            >
                                <
                            </button>
                        </div>
                        <div class="col-md-4">
                            <button
                                    type="button" id="equalButton"
                                    class="btn shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                                    style="color: white;background-color: lightgreen"
                                    onclick="equalButtonClicked()"
                            >
                                =
                            </button>
                        </div>
                        <div class="col-md-4">
                            <button
                                    type="button" id="moreButton"
                                    class="btn shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                                    style="color: white;background-color: grey"
                                    onclick="moreButtonClicked()"
                            >
                                >
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            
            <div  class="search-criteria-div col-md-6">
				<div class="form-group row">
					<label class="col-sm-3 control-label text-right">
						<%=Language.equalsIgnoreCase("english")?"Show Photo and Signature":"ছবি ও স্বাক্ষর দেখুন"%>
					</label>
					<div class="col-sm-9">
						
						<input class='form-control-sm'  name='showPic' id = 'showPic' value="false" onchange='setShowPic()' type="checkbox"/>							
					</div>
				</div>
			</div>
            <input type="hidden" id="jobGradeLess" name="jobGradeLess" value="0">
            <input type="hidden" id="jobGradeEqual" name="jobGradeEqual" value="1">
            <input type="hidden" id="jobGradeMore" name="jobGradeMore" value="0">
        </div>
    </div>
</div>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<%
    String modalTitle = Language.equalsIgnoreCase("English") ? "Find Designation" : "পদবী খুঁজুন";
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
    <jsp:param name="modalTitle" value="<%=modalTitle%>"/>
</jsp:include>

<script src="<%=context%>scripts/input_validation.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(() => {
        showFooter = false;
    });
    
    var isVisible = [true, true, true, true, true, true, true, true, true, true, 
    				true, true, true, true, true, true, true, true, true, true,
    				true, true, true, true, true, true, true, true, true, false, 
    				false
    				];
    function setShowPic()
    {
    	if($("#showPic").prop("checked"))
    	{
    		$("#showPic").val("true");
    		isVisible[29] = true;
    		isVisible[30] = true;
    	}
    	else
    	{
    		$("#showPic").val("false");
    		isVisible[29] = false;
    		isVisible[30] = false;
    	}
    }

    function init() {
        removeCross('select2-selection__choice');
        select2MultiSelector("#designationNameSelect", '<%=Language%>');
        select2MultiSelector('#organogram_role_select', '<%=Language%>');
        select2MultiSelector('#incharge_label_select', '<%=Language%>');
        select2SingleSelector('#bloodGroup', '<%=Language%>');
        select2SingleSelector('#religion', '<%=Language%>');
        select2SingleSelector('#maritalStatus', '<%=Language%>');
        select2SingleSelector('#genderCat', '<%=Language%>');
        select2SingleSelector('#employeeClassCat', '<%=Language%>');
        select2SingleSelector('#employmentCat', '<%=Language%>');
        select2SingleSelector('#empOfficerCat', '<%=Language%>');
        select2SingleSelector('#home_district', '<%=Language%>');
        select2SingleSelector('#jobGradeTypeCat', '<%=Language%>');
        dateTimeInit('<%=Language%>');
        document.getElementById('age_start').onkeydown = keyDownEvent;
        document.getElementById('age_end').onkeydown = keyDownEvent;
        criteriaArray = [{
            class: 'officeModalClass',
            title: '<%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_OFFICEUNITID, loginDTO)%>',
            show: false,
            specialCategory: 'officeModal'
        },
            {
                class: 'employeeModalClass',
                title: '<%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITORGANOGRAMID, loginDTO)%>',
                show: false,
                specialCategory: 'employeeModal'
            },
            {
                class: 'designationClass',
                title: '<%=isLangEng? "Designation Name": "পদবী নাম" %>',
                show: false
            },
            {
                class: 'employeeNameClass',
                title: '<%=isLangEng?   "Employee Name":"কর্মকর্তা/কর্মচারীর নাম"%>',
                show: false
            },
            {
                class: 'emailClass',
                title: '<%=isLangEng?   "Personal Email":"ব্যাক্তিগত ইমেইল"%>',
                show: false
            },
            {
                class: 'officialEmailClass',
                title: '<%=isLangEng?   "Official Email":"অফিসিয়াল ইমেইল"%>',
                show: false
            },
            {
                class: 'bloodGroupClass',
                title: '<%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_BLOODGROUP, loginDTO)%>',
                show: false
            },
            {
                class: 'religionClass',
                title: '<%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_RELIGION, loginDTO)%>',
                show: false
            },
            {
                class: 'genderClass',
                title: '<%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_GENDERCAT, loginDTO)%>',
                show: false
            },
            {
                class: 'maritalStatusClass',
                title: '<%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_MARITALSTATUS, loginDTO)%>',
                show: false
            },
            {class: 'employeeClass', title: '<%=LM.getText(LC.EMPLOYEE_EMPLOYEE_CLASS, loginDTO)%>', show: false},
            {
                class: 'employmentCatClass',
                title: '<%=LM.getText(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY, loginDTO)%>',
                show: false
            },
            {class: 'empOfficerClass', title: '<%=LM.getText(LC.EMPLOYEE_OFFICER_TYPE, loginDTO)%>', show: false},
            {
                class: 'permanentDistrictClass',
                title: '<%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT, loginDTO)%>',
                show: false
            },
            {
                class: 'joiningDateClass',
                title: '<%=isLangEng? "Joining Date":"যোগদান তারিখ"%>',
                show: false,
                specialCategory: 'date',
                datejsIds: ['joining-date-start-js', 'joining-date-end-js']
            },
            {
                class: 'parliamentJoiningDateClass',
                title: '<%=isLangEng?"Parliament Joining Date" :"পার্লামেন্ট যোগদান তারিখ"%>',
                show: false,
                specialCategory: 'date',
                datejsIds: ['parliament-joining-date-start-js', 'parliament-joining-date-end-js']
            },
            {
                class: 'lprDateClass',
                title: '<%=isLangEng?"LPR Date": "এলপিআর তারিখ"%>',
                show: false,
                specialCategory: 'date',
                datejsIds: ['lpr-date-start-js', 'lpr-date-end-js']
            },
            {class: 'ageClass', title: '<%=LM.getText(LC.HR_REPORT_AGE_RANGE, loginDTO)%>', show: false},
            {class: 'roleClass', title: '<%=LM.getText(LC.USER_ADD_ROLE, loginDTO)%>', show: false},
            {
                class: 'inchargeClass',
                title: '<%=isLangEng?"In charge Level": "ইনচার্জ লেভেল" %>',
                show: false
            },
            {
                class: 'jobGradeClass',
                title: '<%=LM.getText(LC.DESIGNATION_STATUS_REPORT_WHERE_JOBGRADETYPECAT, loginDTO)%>',
                show: false
            }]
        
        	var startJoiningDate = getParamValue("startJoiningDate");
        	console.log("startJoiningDate from param = " + startJoiningDate);
        	if(startJoiningDate != null)
        	{
        		$("#parliamentJoiningDateStart_div").show();
        		$("#c12").prop( "checked", true );
        		setDateByTimestampAndId('parliament-joining-date-start-js', startJoiningDate);
        		processNewCalendarDateAndSubmit();
        	}
        	
        	var endJoiningDate = getParamValue("endJoiningDate");
        	console.log("endJoiningDate from param = " + endJoiningDate);
        	if(endJoiningDate != null)
        	{
        		$("#parliamentJoiningDateEnd_div").show();
        		$("#c12").prop( "checked", true );
        		setDateByTimestampAndId('parliament-joining-date-end-js', endJoiningDate);
        		processNewCalendarDateAndSubmit();
        	}
        	
        	var employmentCat = getParamValue("employmentCat");
        	console.log("employmentCat from param = " + employmentCat);
        	if(employmentCat != null)
        	{
        		$("#employmentCat_div").show();
        		$("#c9").prop( "checked", true );
        		$("#employmentCat").val(employmentCat).trigger('change');
        		processNewCalendarDateAndSubmit();
        	}
        		

    }

    function PreprocessBeforeSubmiting() {
        $('#joiningDateStart').val(getDateTimestampById('joining-date-start-js'));
        $('#joiningDateEnd').val(getDateTimestampById('joining-date-end-js'));
        $('#parliamentJoiningDateStart').val(getDateTimestampById('parliament-joining-date-start-js'));
        $('#parliamentJoiningDateEnd').val(getDateTimestampById('parliament-joining-date-end-js'));
        $('#lprDateStart').val(getDateTimestampById('lpr-date-start-js'));
        $('#lprDateEnd').val(getDateTimestampById('lpr-date-end-js'));
        ProcessMultipleSelectBoxBeforeSubmit("designationNameSelect", "designationName");
        ProcessMultipleSelectBoxBeforeSubmit("organogram_role_select", "organogram_role");
        ProcessMultipleSelectBoxBeforeSubmit("incharge_label_select", "incharge_label");
        let labelValue = document.getElementById("incharge_label").value;

        if (labelValue.trim().length == 0) {
            <%
            String value = Arrays.stream(InChargeLevelEnum.values()).map(levelValue->levelValue.getValue()).collect(Collectors.joining(","));
            %>
            labelValue = '<%=value%>';
        }

        let newLabelValue = labelValue.split(",").map(value => {
            value = "\'" + value.trim() + "\'";
            return value;
        }).join(",");

        document.getElementById("incharge_label").value = newLabelValue;

        removeCross('select2-selection__choice');
    }

    function removeCross(className, rep, ffi) {
        if (ffi != null) {
            $("#designationNameSelect").select2("destroy");
            $("#designationNameSelect").select2();
        }
        let list = document.getElementsByClassName(className);
        for (let i = 0; i < list.length; i++) {
            console.log(list);
            let element = list[i];
            console.log(element);
            console.log(element.innerText);
            console.log(element.innerText == "×")
            if (element.innerText == "×") {
                element.remove();
            }
        }
        if (rep != null) {
            removeCross(className, null, null);
        }
    }

    function ProcessMultipleSelectBoxBeforeSubmit(name, hiddenFieldName) {
        $("[name='" + name + "']").each(function (i) {
            var selectedInputs = $(this).val();
            var temp = "";
            if (selectedInputs != null) {
                selectedInputs.forEach(function (value, index, array) {
                    if (index > 0) {
                        temp += ", ";
                    }
                    temp += value;
                });
            }
            document.getElementById(hiddenFieldName).value = temp;
        });
    }

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
    }

    function keyDownEvent(e) {
        let isvalid = inputValidationForIntValue(e, $(this), 200);
        return true == isvalid;
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInTags,
            isMultiSelect: true,
            keepLastSelectState: true
        }]
    ]);

    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function viewOgranogramIdInInput(empInfo) {
        console.log(empInfo);

        $('#organogram_id_modal_button').hide();
        $('#organogram_id_div').show();

        let language = '<%=Language.toLowerCase()%>'
        let designation;
        if (language === 'english') {
            designation = empInfo.organogramNameEn + ', ' + empInfo.officeUnitNameEn;
        } else {
            designation = empInfo.organogramNameBn + ', ' + empInfo.officeUnitNameBn;
        }
        document.getElementById('organogram_id_text').innerHTML = designation;
        $('#organogram_id_input').val(empInfo.organogramId);
    }

    table_name_to_collcetion_map = new Map([
        ['organogramId', {
            isSingleEntry: true,
            callBackFunction: viewOgranogramIdInInput
        }]
    ]);

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    function organogramIdModalBtnClicked() {
        modal_button_dest_table = 'organogramId';
        $('#search_emp_modal').modal();
    }

    function lessButtonClicked() {
        if (document.getElementById("jobGradeLess").value === "0")
            document.getElementById("lessButton").style.backgroundColor = "lightgreen";
        else
            document.getElementById("lessButton").style.backgroundColor = "grey";
        switchValue("jobGradeLess");
        if (document.getElementById("jobGradeMore").value == "1") {
            document.getElementById("jobGradeMore").value = "0";
            document.getElementById("moreButton").style.backgroundColor = "grey";
        }
    }

    function moreButtonClicked() {
        if (document.getElementById("jobGradeMore").value === "0")
            document.getElementById("moreButton").style.backgroundColor = "lightgreen";
        else
            document.getElementById("moreButton").style.backgroundColor = "grey";
        switchValue("jobGradeMore");
        if (document.getElementById("jobGradeLess").value == "1") {
            document.getElementById("jobGradeLess").value = "0";
            document.getElementById("lessButton").style.backgroundColor = "grey";
        }
    }

    function equalButtonClicked() {
        if (document.getElementById("jobGradeEqual").value === "0")
            document.getElementById("equalButton").style.backgroundColor = "lightgreen";
        else
            document.getElementById("equalButton").style.backgroundColor = "grey";
        switchValue("jobGradeEqual");
    }

    function switchValue(hiddenId) {
        if (document.getElementById(hiddenId).value === "0")
            document.getElementById(hiddenId).value = "1";
        else
            document.getElementById(hiddenId).value = "0";
    }
</script>