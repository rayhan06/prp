
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="pi_vendor_auctioneer_details.*"%>
<%@ page import="util.*"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="pb.*"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@ page import="java.util.StringTokenizer" %>


<%
String navigator2 = "navPI_VENDOR_AUCTIONEER_DETAILS";
String servletName = "Pi_vendor_auctioneer_detailsServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_NAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_NAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_ADDRESS, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_MOBILE, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_PIVENDORAUCTIONEERCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
								<th><%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_SEARCH_PI_VENDOR_AUCTIONEER_DETAILS_EDIT_BUTTON, loginDTO)%></th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Pi_vendor_auctioneer_detailsDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Pi_vendor_auctioneer_detailsDTO pi_vendor_auctioneer_detailsDTO = (Pi_vendor_auctioneer_detailsDTO) data.get(i);
																																
											
											%>
											<tr>
								
		
											<td>
											<%
											value = pi_vendor_auctioneer_detailsDTO.nameEn + "";
											%>
				
											<%=value%>
				
			
											</td>
		
											<td>
											<%
											value = pi_vendor_auctioneer_detailsDTO.nameBn + "";
											%>
				
											<%=value%>
				
			
											</td>
		
											<td>
											<%
											value = pi_vendor_auctioneer_detailsDTO.address + "";
											%>
											<%=value%>
				
			
											</td>
		
											<td>
											<%
											value = pi_vendor_auctioneer_detailsDTO.mobile + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											
												<%
													value = "";
													value = pi_vendor_auctioneer_detailsDTO.piVendorAuctioneerCat;
													StringTokenizer tokenizer = new StringTokenizer(value, ",");
													String separator = "";
													value = "";
													while (tokenizer.hasMoreTokens()) {
														String val = tokenizer.nextToken().trim();
														if(!val.equalsIgnoreCase("")){
															Long distNum = Long.valueOf(val);
															String advertiseMedium = CatRepository.getInstance().getText(Language, "pi_vendor_auctioneer", distNum);
															value = value + separator + advertiseMedium;
															separator = ", ";
														}

													}
												%>

												<%=value%>

				
			
											</td>
		
		
		
		
		
		
		
	
											<%CommonDTO commonDTO = pi_vendor_auctioneer_detailsDTO; %>
											<%@include file="../pb/searchAndViewButton.jsp"%>											
																						
											<td class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=pi_vendor_auctioneer_detailsDTO.iD%>'/></span>
												</div>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			