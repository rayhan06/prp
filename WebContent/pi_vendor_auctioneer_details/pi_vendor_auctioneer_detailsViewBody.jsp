
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="pi_vendor_auctioneer_details.*"%>
<%@ page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="util.*"%>
<%@ page import="common.BaseServlet" %>
<%
String servletName = "Pi_vendor_auctioneer_detailsServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Pi_vendor_auctioneer_detailsDTO pi_vendor_auctioneer_detailsDTO = (Pi_vendor_auctioneer_detailsDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
CommonDTO commonDTO = pi_vendor_auctioneer_detailsDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_PI_VENDOR_AUCTIONEER_DETAILS_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_PI_VENDOR_AUCTIONEER_DETAILS_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_NAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_vendor_auctioneer_detailsDTO.nameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_NAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_vendor_auctioneer_detailsDTO.nameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_ADDRESS, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_vendor_auctioneer_detailsDTO.address + "";
											%>
											<%=value%>
				
			
                                    </div>
                                </div>



                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=Language.equals("English")? "Email" : "ইমেইল"%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = pi_vendor_auctioneer_detailsDTO.email + "";
                                        %>

                                        <%=value%>


                                    </div>
                                </div>


								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_MOBILE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_vendor_auctioneer_detailsDTO.mobile + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>


                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=Language.equals("English")? "Website" : "ওয়েবসাইট"%>                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = pi_vendor_auctioneer_detailsDTO.website + "";
                                        %>

                                        <%=value%>


                                    </div>
                                </div>


                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=Language.equals("English")? "Contact Person" : "যোগাযোগ প্রতিনিধি"%>                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = pi_vendor_auctioneer_detailsDTO.contactPerson + "";
                                        %>

                                        <%=value%>


                                    </div>
                                </div>


								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_PIVENDORAUCTIONEERCAT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = "";
                                            value = pi_vendor_auctioneer_detailsDTO.piVendorAuctioneerCat;
                                            StringTokenizer tokenizer = new StringTokenizer(value, ",");
                                            String separator = "";
                                            value = "";
                                            while (tokenizer.hasMoreTokens()) {
                                                String val = tokenizer.nextToken().trim();
                                                if(!val.equalsIgnoreCase("")){
                                                    Long distNum = Long.valueOf(val);
                                                    String advertiseMedium = CatRepository.getInstance().getText(Language, "pi_vendor_auctioneer", distNum);
                                                    value = value + separator + advertiseMedium;
                                                    separator = ", ";
                                                }

                                            }
                                        %>

                                        <%=value%>
				
			
                                    </div>
                                </div>
			
			
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>