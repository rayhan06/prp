
<%@page import="pi_vendor_auctioneer_details.*"%>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@ page import="util.*"%>
<%@ page import="common.BaseServlet" %>
<%@ page import="javax.rmi.CORBA.Util" %>

<%
Pi_vendor_auctioneer_detailsDTO pi_vendor_auctioneer_detailsDTO = new Pi_vendor_auctioneer_detailsDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	pi_vendor_auctioneer_detailsDTO = (Pi_vendor_auctioneer_detailsDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = pi_vendor_auctioneer_detailsDTO;
String tableName = "pi_vendor_auctioneer_details";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_PI_VENDOR_AUCTIONEER_DETAILS_ADD_FORMNAME, loginDTO);
String servletName = "Pi_vendor_auctioneer_detailsServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=pi_vendor_auctioneer_detailsDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_NAMEEN, loginDTO)%><span class="required" style="color: red;"> * </span></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='nameEn' id = 'nameEn_text_<%=i%>' value='<%=pi_vendor_auctioneer_detailsDTO.nameEn%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_NAMEBN, loginDTO)%><span class="required" style="color: red;"> * </span></label>
                                                            <div class="col-8">
																<input type='text' class='form-control noEnglish'  name='nameBn' id = 'nameBn_text_<%=i%>' value='<%=pi_vendor_auctioneer_detailsDTO.nameBn%>'   tag='pb_html'/>
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
                                                                <%=UtilCharacter.getDataByLanguage(Language, "বর্তমান ঠিকানা", "Present Address")%>
                                                                <span class="required" style="color: red;"> * </span></label>
                                                            <div class="col-8">
                                                                <input type='text'
                                                                       class='form-control'
                                                                       name='address'
                                                                       id = 'address_geolocation_<%=i%>'
                                                                       value='<%=pi_vendor_auctioneer_detailsDTO.address%>'
                                                                       tag='pb_html'/>
															</div>
                                                      </div>

                                                    <div class="form-group row">
                                                        <label class="col-4 col-form-label text-right">
                                                            <%=UtilCharacter.getDataByLanguage(Language, "স্থায়ী ঠিকানা", "Permanent Address")%>
                                                            <span class="required" style="color: red;"> * </span></label>
                                                        <div class="col-8">
                                                            <input type='text'
                                                                   class='form-control'
                                                                   name='permanentAddress'
                                                                   id = 'permanent_address_geolocation_<%=i%>'
                                                                   value='<%=pi_vendor_auctioneer_detailsDTO.permanentAddress%>'
                                                                   tag='pb_html'/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-4 col-form-label text-right">
                                                            <%=UtilCharacter.getDataByLanguage(Language, "এন. আই. ডি.", "NID")%>
                                                            <span class="required" style="color: red;"> * </span></label>
                                                        <div class="col-8">
                                                            <input type='number' class='form-control'
                                                                   name='nid'
                                                                   id = 'nid_<%=i%>'
                                                                   value='<%=pi_vendor_auctioneer_detailsDTO.nid%>'
                                                                   tag='pb_html'/>
                                                        </div>
                                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=Language.equals("English")? "Email" : "ইমেইল"%><span class="required" style="color: red;"> * </span></label>
                                        <div class="col-8">
                                            <input type='text' class='form-control'
                                                   name='email'
                                                   id = 'email_text_<%=i%>'
                                                   value='<%=pi_vendor_auctioneer_detailsDTO.email%>'
                                                   tag='pb_html'
                                                   required/>
                                        </div>
                                    </div>



													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_MOBILE, loginDTO)%><span class="required" style="color: red;"> * </span></label>
                                                            <div class="col-8">
                                                                <input type='text' class='form-control'
                                                                       name='mobile'
                                                                       id = 'mobile_text_<%=i%>'
                                                                       required="required"
                                                                       pattern="^(01[3-9]{1}[0-9]{8})"
                                                                       value='<%=pi_vendor_auctioneer_detailsDTO.mobile ==null || pi_vendor_auctioneer_detailsDTO.mobile.length() == 0?"":
                                                                   (pi_vendor_auctioneer_detailsDTO.mobile.startsWith("88") ? pi_vendor_auctioneer_detailsDTO.mobile.substring(2) : pi_vendor_auctioneer_detailsDTO.mobile)%>'
                                                                       placeholder='<%=LM.getText(LC.GLOBAL_MOBILE_PLACE_HOLDER, loginDTO)%>'
                                                                       title='<%=isLanguageEnglish?"personal mobile number must start with 01, then contain 9 digits"
                                                               :"ব্যক্তিগত মোবাইল নাম্বার 01 দিয়ে শুরু হবে, তারপর ৯টি সংখ্যা হবে"%>'
                                                                       tag='pb_html'/>
															</div>
                                                      </div>



                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=Language.equals("English")? "Website" : "ওয়েবসাইট"%></label>
                                        <div class="col-8">
                                            <input type='text' class='form-control'
                                                   name='website'
                                                   id = 'website_text_<%=i%>'
                                                   value='<%=pi_vendor_auctioneer_detailsDTO.website%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>






                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=Language.equals("English")? "Contact Person" : "যোগাযোগ প্রতিনিধি"%></label>
                                        <div class="col-8">
                                            <input type='text' class='form-control'
                                                   name='contactPerson'
                                                   id = 'contactPerson_text_<%=i%>'
                                                   value='<%=pi_vendor_auctioneer_detailsDTO.contactPerson%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>


													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_PIVENDORAUCTIONEERCAT, loginDTO)%><span class="required" style="color: red;"> * </span></label>
                                                            <div class="col-8">

                                                                <select multiple="multiple" class='form-control' name='piVendorAuctioneerCat'
                                                                        id='piVendorAuctioneerCat' tag='pb_html'>

                                                                </select>
	
															</div>
                                                      </div>									

									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_PI_VENDOR_AUCTIONEER_DETAILS_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" id="submit-btn"  type="button" onclick="submitForm()">
                                <%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_PI_VENDOR_AUCTIONEER_DETAILS_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

const piVendorAuctioneerForm = $("#bigform");

function processMultipleSelectBoxBeforeSubmit2(name)
{

    $( "[name='" + name + "']" ).each(function( i )
    {
        var selectedInputs = $(this).val();
        var temp = "";
        if(selectedInputs != null){
            selectedInputs.forEach(function(value, index, array){
                if(index > 0){
                    temp += ", ";
                }
                temp += value;
            });
        }
        if(temp.includes(',')){
            $(this).append('<option value="' + temp + '"></option>');
        }
        $(this).val(temp);

    });
}

function PreprocessBeforeSubmiting(row, action)
{


    piVendorAuctioneerForm.validate();
    return piVendorAuctioneerForm.valid();
}

function submitForm(){
    buttonStateChange(true);
    if(PreprocessBeforeSubmiting(0)){
        processMultipleSelectBoxBeforeSubmit2("piVendorAuctioneerCat");
        //return false;
        let url =  "<%=servletName%>?actionType=<%=actionName%>";
        $.ajax({
            type : "POST",
            url : url,
            data : piVendorAuctioneerForm.serialize(),
            dataType : 'JSON',
            success : function(response) {
                if(response.responseCode === 0){
                    $('#toast_message').css('background-color','#ff6063');
                    showToastSticky(response.msg,response.msg);
                    buttonStateChange(false);
                }else if(response.responseCode === 200){
                    showToastSticky("সাবমিট সফল হয়েছে","Submit Successful");
                    setTimeout(() => {
                        window.location.replace(getContextPath() + response.msg);
                    }, 3000);
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChange(false);
            }
        });
    }else{
        buttonStateChange(false);
    }
}

function buttonStateChange(value){
    $('#submit-btn').prop('disabled',value);
    $('#cancel-btn').prop('disabled',value);
}



function init(row)
{

	
}

var row = 0;
let selectedpiVendorAuctioneerCat;
let selectedpiVendorAuctioneerCatArr;
var language = '<%=Language%>';
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	});

    $(".noEnglish").keypress(function(event){
        var allowed = "~`!@#$%^&*()_-+=|\'\"\\|:?/;<>,.}]{[";
        if (allowed.includes(event.key))return true;
        var ew = event.which;
        if(ew == 32)
            return true;
        if(48 <= ew && ew <= 57)
            return false;
        if(65 <= ew && ew <= 90)
            return false;
        if(97 <= ew && ew <= 122)
            return false;
        return true;
    });

    elements = document.getElementsByClassName("noEnglish");

    for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener('paste', onInputPasteNoEnglish);
    }

    select2MultiSelector("#piVendorAuctioneerCat",'<%=Language%>');
    <% if(actionName.equalsIgnoreCase("ajax_edit")){ %>

    selectedpiVendorAuctioneerCat = '<%=pi_vendor_auctioneer_detailsDTO.piVendorAuctioneerCat%>';
    selectedpiVendorAuctioneerCatArr = selectedpiVendorAuctioneerCat.split(',').map(function(item) {
        return item.trim();
    });
    <% }%>
    fetchVendorAuctioneerCat(selectedpiVendorAuctioneerCatArr);
});	

var child_table_extra_id = <%=childTableStartingID%>;

function fetchVendorAuctioneerCat(selectedTenderAdvertiseCattArr) {

    let url = "Pi_vendor_auctioneer_detailsServlet?actionType=getAllVendorAuctioneerCat";
    $.ajax({
        url: url,
        type: "GET",
        async: false,
        success: function (fetchedData) {

            $('#piVendorAuctioneerCat').html("");
            let o;
            let str;

            $('#piVendorAuctioneerCat').append(o);
            const response = JSON.parse(fetchedData);
            if (response && response.length > 0) {
                for (let x in response) {
                    if (language === 'English') {
                        str = response[x].englishText;
                    } else {
                        str = response[x].banglaText;
                    }

                    if (selectedTenderAdvertiseCattArr && selectedTenderAdvertiseCattArr.length > 0) {
                        if (selectedTenderAdvertiseCattArr.includes(response[x].value + '')) {
                            o = new Option(str, response[x].value, false, true);
                        } else {
                            o = new Option(str, response[x].value);
                        }
                    } else {
                        o = new Option(str, response[x].value);
                    }
                    $(o).html(str);
                    $('#piVendorAuctioneerCat').append(o);
                }
            }


        },
        error: function (error) {
            console.log(error);
        }
    });
}

function onInputPasteNoEnglish(event) {
    var clipboardData = event.clipboardData || window.clipboardData;
    var data = clipboardData.getData("Text");
    var valid = data.toString().split('').every(char => {
        var allowed = "~`!@#$%^&*()_-+=|\'\"\\|:?/;<>,.}]{[";
        if (allowed.includes(char))return true;
        if(char.toString().trim() == '')
            return true;
        if('0' <= char && char <= '9')
            return false;
        if('a' <= char && char <= 'z')
            return false;
        if('A' <= char && char <= 'Z')
            return false;
        return true;
    });
    if (valid)return;
    event.stopPropagation();
    event.preventDefault();
}



</script>






