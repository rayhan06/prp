<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    System.out.println("Inside nav.jsp");
    String url = "Pi_vendor_auctioneer_detailsServlet?actionType=search";
    String toggle = request.getParameter("toggle");
    boolean isExpanded = toggle != null && toggle.length() > 0 && toggle.equalsIgnoreCase("true");
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0,false)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-group">
                <div class="tooltip tooltip-portlet tooltip bs-tooltip-top" role="tooltip" id="tooltip_p6zf7aqcpv"
                     aria-hidden="true" x-placement="top"
                     style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">
                    <div class="tooltip-arrow arrow" style="left: 34px;"></div>
                    <div class="tooltip-inner">Collapse</div>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">

            <div class="row">
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-3 col-form-label"><%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_NAMEEN, loginDTO)%>
                        </label>
                        <div class="col-9">
                            <input type="text" class="form-control" id="name_en" placeholder="" name="name_en"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-3 col-form-label"><%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_NAMEBN, loginDTO)%>
                        </label>
                        <div class="col-9">
                            <input type="text" class="form-control" id="name_bn" placeholder="" name="name_bn"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-3 col-form-label"><%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_MOBILE, loginDTO)%>
                        </label>
                        <div class="col-9">
                            <input type="text" class="form-control" id="mobile" placeholder="" name="mobile"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-6" style="display: none;">
                    <div class="form-group row">
                        <label class="col-3 col-form-label"><%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_PIVENDORAUCTIONEERCAT, loginDTO)%>
                        </label>
                        <div class="col-9">
                            <select class='form-control' name='pi_vendor_auctioneer_cat' id='pi_vendor_auctioneer_cat'
                                    onSelect='setSearchChanged()'>
                                <%
                                    Options = CatRepository.getInstance().buildOptions("pi_vendor_auctioneer", Language, -1);
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0,true)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <div class="search-loader-container-circle ">
            <div class="search-loader-circle"></div>
        </div>
    </div>
</template>

<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>
<script type="text/javascript">
    const engNameInputField = $('#name_en');
    const banNameInputField = $('#name_bn');
    const mobile = $('#mobile');
    const typeSelector = $('#pi_vendor_auctioneer_cat');
    const anyFieldSelector = $('#anyfield');
    const servletName = "Pi_vendor_auctioneer_detailsServlet";


    $(document).ready(() => {

        readyInit(servletName);
        <%if(isExpanded){%>
        document.querySelector('.kt-portlet__body').style.display = 'block';
        document.querySelector('#kt_portlet_tools_1').classList.remove('kt-portlet--collapse');
        <%}%>

    });

    function dosubmit(params, pushState = true) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (pushState) {
                    history.pushState(params, '', servletName + '?actionType=search&' + params);
                }
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        let url = "<%=action%>&ajax=true&isPermanentTable=true";
        if (params) {
            url += "&" + params;
        }
        xhttp.open("Get", url, false);
        xhttp.send();

    }

    function resetInputs() {
        anyFieldSelector.val('');
        engNameInputField.val('');
        banNameInputField.val('');
        mobile.val('');
        typeSelector.select2("val", '-1');
    }

    window.addEventListener('popstate', e => {
        if (e.state) {

            let params = e.state;
            dosubmit(params, false);
            resetInputs();
            let arr = params.split('&');
            arr.forEach(e => {
                let item = e.split('=');
                if (item.length === 2) {
                    switch (item[0]) {

                        case 'name_en':
                            engNameInputField.val(item[1]);
                            break;
                        case 'name_bn':
                            banNameInputField.val(item[1]);
                            break;
                        case 'mobile':
                            mobile.val(item[1]);
                            //$('#building_type_cat').prop('selectedIndex', item[1]);
                            break;
                        case 'pi_vendor_auctioneer_cat':
                            typeSelector.select2("val", item[1]);
                            break;
                        case 'AnyField':
                            anyFieldSelector.val(item[1]);
                            break;
                        default:
                            setPaginationFields(item);
                    }
                }
            });
        } else {
            dosubmit(null, false);
            resetInputs();
            resetPaginationFields();
        }
    });

    function allfield_changed(go, pagination_number, pushState) {
        var params = 'search=true';
        let toggleFlag = 0;

        if (anyFieldSelector.val()) {
            params += '&AnyField=' + anyFieldSelector.val();
        }
        if (engNameInputField.val()) {
            params += '&name_en=' + $("#name_en").val();
            toggleFlag = 1;
        }

        if (banNameInputField.val()) {
            params += '&name_bn=' + $("#name_bn").val();
            toggleFlag = 1;
        }
        if (mobile.val()) {
            params += '&mobile=' + $("#mobile").val();
            toggleFlag = 1;
        }
        if ($("#pi_vendor_auctioneer_cat").val() != -1 && $("#pi_vendor_auctioneer_cat").val() != "") {
            params += '&pi_vendor_auctioneer_cat=' + $("#pi_vendor_auctioneer_cat").val();
            toggleFlag = 1;
        }

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        let toggleVal = toggleFlag===1?true:false;
        params += '&toggle='+toggleVal;
        dosubmit(params, pushState);

    }

</script>

