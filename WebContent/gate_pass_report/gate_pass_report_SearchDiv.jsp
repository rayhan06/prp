<%@page pageEncoding="UTF-8" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%@ page import="gate_pass_type.Gate_pass_typeRepository" %>
<%@ page import="com.google.gson.JsonArray" %>
<%@ page import="com.google.gson.JsonParser" %>
<%@ page import="gate_pass_sub_type.Gate_pass_sub_typeRepository" %>
<%

    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.GATE_PASS_REPORT_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    String jsonDTO = Gate_pass_sub_typeRepository.getInstance().buildJSON(Language);
    JsonArray jsonArray = new JsonParser().parse(jsonDTO).getAsJsonArray();
%>

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>
<div class="row mx-2 mx-md-4">
    <div class="col-12">
        <%@include file="../pbreport/yearmonth.jsp" %>
        <%@include file="../pbreport/calendar.jsp" %>
        <div class="search-criteria-div">
            <div class="form-group row">
                <label class="col-md-3 control-label text-md-right">
                    <%=LM.getText(LC.GATE_PASS_REPORT_WHERE_GATEPASSTYPEID, loginDTO)%>
                </label>
                <div class="col-md-9">
                    <%--					<input class='form-control'  name='gatePassTypeId' id = 'gatePassTypeId' value=""/>--%>
                    <select class='form-control' name='gatePassTypeId'
                            id='gatePassTypeId'
                            tag='pb_html'>
                        <%=Gate_pass_typeRepository.getInstance().buildOptions(Language, 0)%>
                    </select>
                </div>
            </div>
        </div>
        <div class="search-criteria-div" id='gatePassSubTypeId_div' style="display: none">
            <div class="form-group row">
                <label class="col-md-3 control-label text-md-right">
                    <%=LM.getText(LC.GATE_PASS_REPORT_WHERE_GATEPASSSUBTYPEID, loginDTO)%>
                </label>
                <div class="col-md-9">
                    <%--					<input class='form-control'  name='gatePassSubTypeId' id = 'gatePassSubTypeId' value=""/>--%>
                    <select class='form-control ignore-gate-pass-subtype-validation'
                            name='gatePassSubTypeId'
                            id='gatePassSubTypeId'
                            tag='pb_html'>
                    </select>
                </div>
            </div>
        </div>
        <div class="search-criteria-div">
            <div class="form-group row">
                <label class="col-md-3 control-label text-md-right">
                    <%=LM.getText(LC.GATE_PASS_REPORT_WHERE_REFERRERID, loginDTO)%>
                </label>
                <div class="col-md-9">
<%--                    <input class='form-control' name='referrerId' id='referrerId' value=""/>--%>
                    <div class="" id='select-referrer-div'>
                        <input type='hidden' class='form-control'
                               id="referrerId" name="referrerId"
                               tag='pb_html'/>
                        <button type="button"
                                class="btn btn-primary btn-block shadow btn-border-radius"
                                id="tagEmp_modal_button">
                            <%=LM.getText(LC.GATE_PASS_ADD_SELECT_REFERRER, loginDTO)%>
                        </button>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead></thead>
                                <tbody id="tagged_emp_table" class="rounded">
                                <tr style="display: none;">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <button type="button"
                                                class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4"
                                                style="padding-right: 14px"
                                                onclick="remove_containing_row(this,'tagged_emp_table');">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<%--        <div class="search-criteria-div" style="display: none">--%>
<%--            <div class="form-group row">--%>
<%--                <label class="col-md-3 control-label text-md-right">--%>
<%--                    <%=LM.getText(LC.GATE_PASS_REPORT_WHERE_ISDELETED, loginDTO)%>--%>
<%--                </label>--%>
<%--                <div class="col-md-9">--%>
<%--                    <input class='form-control' name='isDeleted' id='isDeleted' value=""/>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
    </div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script>
    const isVisible = [true, true, true, true, true, true, true, true, true, true, true, true, true, true, false];
    const obj = <%=jsonArray%>;
    language = '<%=Language%>';
    $('#gatePassTypeId').on('change', (event) => {
        const typeId = event.target.value;
        let count = 0;
        $('#gatePassSubTypeId').empty();
        $('#gatePassSubTypeId').append($('<option>', {
            text: (language == 'English' ? 'Select' : 'বাছাই করুন'),
            value: ''
        }));
        obj.forEach(elem => {
            if (elem.gatePassTypeId == typeId) {
                $('#gatePassSubTypeId').append($('<option>', {
                    text: (language == 'English' ? elem.nameEng : elem.nameBng),
                    value: elem.id
                }));
                count++;
            }
        });

        if (count > 0) {
            $('#gatePassSubTypeId_div').show();
            $("#gatePassSubTypeId").select2({
                dropdownAutoWidth: true
            });
        } else {
            $('#gatePassSubTypeId_div').hide();
        }

    });

    function init() {
        dateTimeInit($("#Language").val());
    }

    function PreprocessBeforeSubmiting() {

    }

    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button

    // map to store and send added employee data

    added_employee_info_map = new Map();

    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_emp_table', {
                info_map: added_employee_info_map,
                isSingleEntry: true,
                callBackFunction: function (empInfo) {

                    document.getElementById('referrerId').value = empInfo.employeeRecordId;
                    console.log('callBackFunction called and referrer Id is being populated!!');
                    // $('#referrerId-error').hide();
                }
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagEmp_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_emp_table';
        $('#search_emp_modal').modal();
    });


    function remove_containing_row(button, table_name) {
        let containing_row = button.parentNode.parentNode;
        let containing_table = containing_row.parentNode;
        containing_table.deleteRow(containing_row.rowIndex);

        // button id = "<employee record id>_button"
        let td_button = button.parentNode;
        let employee_record_id = td_button.id.split("_")[0];

        let added_info_map = table_name_to_collcetion_map.get(table_name).info_map;
        console.log("delete (employee_record_id)");
        added_info_map.delete(employee_record_id);
        console.log(added_info_map);

        $('#referrerId').val('');
    }

    function getLink(list)
    {
        var id = convertToEnglishNumber(list[14]);

        return "Gate_passServlet?actionType=view&ID=" + id;
    }
</script>