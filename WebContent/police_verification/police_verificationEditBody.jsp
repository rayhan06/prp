<%@page import="login.LoginDTO" %>
<%@page import="police_verification.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="pb.*" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    Police_verificationDTO police_verificationDTO;
    police_verificationDTO = (Police_verificationDTO) request.getAttribute("police_verificationDTO");
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    if (police_verificationDTO == null) {
        police_verificationDTO = new Police_verificationDTO();
    }
    String formTitle = LM.getText(LC.POLICE_VERIFICATION_ADD_POLICE_VERIFICATION_ADD_FORMNAME, loginDTO);
    String servletName = "Police_verificationServlet";
    String fileColumnName = "fileDropzone";
    int i = 0;
    long ColumnID;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    String cardInfoId = request.getParameter("cardInfoId");
    if (cardInfoId == null || cardInfoId.isEmpty()) {
        cardInfoId = "0";
    }
    police_verificationDTO.cardInfoId = Long.parseLong(cardInfoId);
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title prp-page-title">
                            <i class="fa fa-gift"></i>&nbsp;
                            <%=formTitle%>
                        </h3>
                    </div>
                </div>
                <form class="form-horizontal" id="bigform">
                    <div class="kt-portlet__body form-body">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="onlyborder">
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="sub_title_top">
                                                        <div class="sub_title">
                                                            <h4 style="background: white"><%=formTitle%>
                                                            </h4>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-4 col-form-label text-right" for="verificationCat_category">
                                                            <%=LM.getText(LC.POLICE_VERIFICATION_ADD_VERIFICATIONCAT, loginDTO)%>
                                                            <span class="required"> * </span></label>
                                                        <div class="col-8">
                                                            <select class='form-control' name='verificationCat'
                                                                    id='verificationCat_category' tag='pb_html'>
                                                                <%=CatRepository.getInstance().buildOptions("verification",Language,police_verificationDTO.verificationCat)%>
                                                            </select>

                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.POLICE_VERIFICATION_ADD_FILEDROPZONE, loginDTO)%>
                                                            <span class="required"> * </span> </label>
                                                        <div class="col-8">
                                                            <div id="file_missing_error" style="display: none">
                                                                <%=LM.getText(LC.POLICE_VERIFICATION_ADD_PLEASE_SUBMIT_POLICE_VERIFICATION_FILE, loginDTO)%>
                                                            </div>
                                                            <%
                                                                ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                                police_verificationDTO.fileDropzone = ColumnID;
                                                            %>

                                                            <div class="dropzone"
                                                                 action="<%=servletName%>?pageType=add&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=police_verificationDTO.fileDropzone%>">
                                                                <input type='file' style="display:none"
                                                                       name='<%=fileColumnName%>File'
                                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>'
                                                                       tag='pb_html'/>
                                                            </div>
                                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value=''
                                                                   tag='pb_html'/>
                                                            <input type='hidden' name='<%=fileColumnName%>'
                                                                   id='<%=fileColumnName%>_dropzone_<%=i%>'
                                                                   tag='pb_html'
                                                                   value='<%=police_verificationDTO.fileDropzone%>'/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions text-center mb-5">
                        <button class="btn btn-danger" id="reject_btn"
                           href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.POLICE_VERIFICATION_ADD_POLICE_VERIFICATION_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button class="btn btn-success" onclick="submitPoliceVerificationForm()" id="submit_btn"
                                type="button"><%=LM.getText(LC.POLICE_VERIFICATION_ADD_POLICE_VERIFICATION_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(()=>{
        select2SingleSelector('#verificationCat_category',"<%=Language%>");
        $.validator.addMethod('fileDropZoneSelection', function (value, element) {
            console.log($('.dz-image').length)
            return $('.dz-image').length > 0;
        });
        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                verificationCat: "required",
                fileDropzoneFile: {
                    fileDropZoneSelection: true
                }
            },
            messages: {
                verificationCat : "<%=LM.getText(LC.POLICE_VERIFICATION_ADD_PLEASE_SELECT_VERIFICATION_TYPE, loginDTO)%>",
                fileDropzoneFile : "<%=LM.getText(LC.POLICE_VERIFICATION_ADD_PLEASE_SUBMIT_POLICE_VERIFICATION_FILE, loginDTO)%>"
            }
        });
    });

    $(document).ready(function () {
        select2SingleSelector("#verificationCat_category_0", '<%=Language%>');
    });

    function submitPoliceVerificationForm(){
        console.log('val->'+$('#fileDropzone_dropzone_0').val());
        console.log($('#fileDropzone_dropzone_0'));
        buttonStateChange(true);
        if($("#bigform").valid()){
            $.ajax({
                type : "POST",
                url : "Police_verificationServlet?actionType=ajax_add&isPermanentTable=true&cardInfoId=<%=cardInfoId%>",
                data : {fileDropzone:$('#fileDropzone_dropzone_0').val(),verificationCat:$('#verificationCat_category').val()},
                dataType : 'JSON',
                success : function(response) {
                    if(response.responseCode === 0){
                        $('#toast_message').css('background-color','#ff6063');
                        showToastSticky(response.msg,response.msg);
                        buttonStateChange(false);
                    }else if(response.responseCode === 200){
                        window.location.replace(getContextPath()+response.msg);
                    }
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        }else{
            buttonStateChange(false);
        }
    }

    function buttonStateChange(flag){
        $('#submit_btn').prop("disabled",flag);
        $('#reject_btn').prop("disabled",flag);
    }

</script>