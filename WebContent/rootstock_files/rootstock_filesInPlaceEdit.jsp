<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="rootstock_files.Rootstock_filesDTO"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>

<%
Rootstock_filesDTO rootstock_filesDTO = (Rootstock_filesDTO)request.getAttribute("rootstock_filesDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(rootstock_filesDTO == null)
{
	rootstock_filesDTO = new Rootstock_filesDTO();
	
}
System.out.println("rootstock_filesDTO = " + rootstock_filesDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";
Rootstock_filesDTO row = rootstock_filesDTO;
%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.ROOTSTOCK_FILES_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=rootstock_filesDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_fileId'>")%>
			

		<input type='hidden' class='form-control'  name='fileId' id = 'fileId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + rootstock_filesDTO.fileId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_fileTypes'>")%>
			
	
	<div class="form-inline" id = 'fileTypes_div_<%=i%>'>
		<input type='text' class='form-control'  name='fileTypes' id = 'fileTypes_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + rootstock_filesDTO.fileTypes + "'"):("'" + " " + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_fileBlob'>")%>
			
	
	<div class="form-inline" id = 'fileBlob_div_<%=i%>'>
		<input type='file' class='form-control'  name='fileBlob' id = 'fileBlob_blob_jpg_<%=i%>' value=<%=actionName.equals("edit")?("'" + rootstock_filesDTO.fileBlob + "'"):("'" + "null" + "'")%>   tag='pb_html'/>
		<label class='control-label'><%=rootstock_filesDTO.fileBlob%></label>		
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_fileTag'>")%>
			
	
	<div class="form-inline" id = 'fileTag_div_<%=i%>'>
		<input type='text' class='form-control'  name='fileTag' id = 'fileTag_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + rootstock_filesDTO.fileTag + "'"):("'" + " " + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_fileTitle'>")%>
			
	
	<div class="form-inline" id = 'fileTitle_div_<%=i%>'>
		<input type='text' class='form-control'  name='fileTitle' id = 'fileTitle_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + rootstock_filesDTO.fileTitle + "'"):("'" + " " + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_userId'>")%>
			

		<input type='hidden' class='form-control'  name='userId' id = 'userId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + rootstock_filesDTO.userId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_source'>")%>
			
	
	<div class="form-inline" id = 'source_div_<%=i%>'>
		<input type='text' class='form-control'  name='source' id = 'source_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + rootstock_filesDTO.source + "'"):("'" + " " + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_createdAt'>")%>
			
	
	<div class="form-inline" id = 'createdAt_div_<%=i%>'>
		<input type='text' class='form-control'  name='createdAt' id = 'createdAt_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + rootstock_filesDTO.createdAt + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + rootstock_filesDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=rootstock_filesDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
		