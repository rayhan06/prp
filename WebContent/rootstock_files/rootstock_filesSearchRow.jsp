<%@page pageEncoding="UTF-8" %>

<%@page import="rootstock_files.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.ROOTSTOCK_FILES_EDIT_LANGUAGE, loginDTO);

Rootstock_filesDTO row = (Rootstock_filesDTO)request.getAttribute("rootstock_filesDTO");
if(row == null)
{
	row = new Rootstock_filesDTO();
	
}
System.out.println("row = " + row);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
String navigator2 = SessionConstants.NAV_ROOTSTOCK_FILES;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

String successMessageForwarded = "Forwarded to your Senior Office";
String successMessageApproved = "Approval Done";

Rootstock_filesDAO rootstock_filesDAO = (Rootstock_filesDAO)request.getAttribute("rootstock_filesDAO");
TempTableDTO tempTableDTO = null;
if(!isPermanentTable)
{
	tempTableDTO = rootstock_filesDAO.getTempTableDTOFromTableById(tableName, row.iD);
}



											
		
											
											out.println("<td id = '" + i + "_fileId'>");
											value = row.fileId + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_fileTypes'>");
											value = row.fileTypes + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_fileBlob'>");
											value = row.fileBlob + "";
											if(row.fileBlob != null)
											{
												byte[] encodeBase64 = Base64.encodeBase64(row.fileBlob);
												value = row.fileBlob + "";
												
												out.println("<img src='" + "data:image/jpg;base64," +  new String(encodeBase64) + "' style='width:100px' >");
												out.println("<a href = 'Rootstock_filesServlet?actionType=downloadBlob&name=fileBlob&id=" + row.iD + "' download='" + value + "'>Download</a>");
											}											
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_fileTag'>");
											value = row.fileTag + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_fileTitle'>");
											value = row.fileTitle + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_userId'>");
											value = row.userId + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_source'>");
											value = row.source + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_createdAt'>");
											
											SimpleDateFormat format_issueDate = new SimpleDateFormat("dd-MM-yyyy");
											value = format_issueDate.format(new Date(row.createdAt)) +  "";
											//value = row.createdAt + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
		
											
		
	

	
											out.println("<td id = '" + i + "_Edit'>");
											if(isPermanentTable || (!isPermanentTable && userDTO.approvalRole == SessionConstants.VALIDATOR && userDTO.approvalOrder == tempTableDTO.approval_order))
											{
																							
	
												out.println("<a href='Rootstock_filesServlet?actionType=getEditPage&ID=" +row.iD + "'>" + LM.getText(LC.ROOTSTOCK_FILES_SEARCH_ROOTSTOCK_FILES_EDIT_BUTTON, loginDTO) + "</a>");
																						
											}
											out.println("</td>");
											if(!isPermanentTable && userDTO.approvalOrder > -1)
											{
												String Successmessage = "";
												if(userDTO.approvalOrder < userDTO.maxApprovalOrder)
												{
													Successmessage = successMessageForwarded;
												}
												else
												{
													Successmessage= successMessageApproved;
												}
												out.println("<td id = 'tdapprove_" + row.iD + "'>");
												if(userDTO.approvalOrder == tempTableDTO.approval_order)
												{
													String onclickFunc = "\"approve('" + row.iD + "' , '" + Successmessage + "' , " + i + ", 'Rootstock_filesServlet')\"";	
													out.println("<a id = 'approve_" + row.iD + "' onclick=" + onclickFunc + "><%=LM.getText(LC.HM_APPROVE, loginDTO)%></a>");
												}
												else if(userDTO.approvalOrder > tempTableDTO.approval_order)
												{
													out.println("You cannot approve it yet");
												}
												else
												{
													if(userDTO.approvalOrder < userDTO.maxApprovalOrder)
													{
														out.println(Successmessage);
													}
													
												}
												out.println("</td>");
												
												
												out.println("<td id = 'tdoperation_" + row.iD + "'>");
												out.println(SessionConstants.operation_names[tempTableDTO.operation_type]);
												out.println("</td>");
												
												out.println("<td id = 'original_" + row.iD + "'>");
												if(tempTableDTO.operation_type == SessionConstants.UPDATE)
												{
													String onclickFunc = "\"getOriginal(" + i + ", " + row.iD + " , " + tempTableDTO.permanent_table_id + ", " + " 'Rootstock_filesServlet')\"";
													out.println("<a id = '" + i + "_getOriginal' onclick=" + onclickFunc + ">View Original</a>");
													out.println("<input type='hidden' id='" + i + "_original_status' value='0'/>");
												}
												out.println("</td>");
											}		
											
											
											out.println("<td id='" + i + "_checkbox'>");
											if(isPermanentTable || (!isPermanentTable && userDTO.approvalOrder == tempTableDTO.approval_order))
											{
												out.println("<div class='checker'>");
												out.println("<span id='chkEdit' ><input type='checkbox' name='ID' value='" + row.iD + "'/></span>");
												out.println("</div");
											}
											out.println("</td>");%>

