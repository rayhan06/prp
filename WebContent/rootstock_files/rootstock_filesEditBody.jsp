
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="rootstock_files.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>


<%
Rootstock_filesDTO rootstock_filesDTO;
rootstock_filesDTO = (Rootstock_filesDTO)request.getAttribute("rootstock_filesDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(rootstock_filesDTO == null)
{
	rootstock_filesDTO = new Rootstock_filesDTO();
	
}
System.out.println("rootstock_filesDTO = " + rootstock_filesDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.ROOTSTOCK_FILES_EDIT_ROOTSTOCK_FILES_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.ROOTSTOCK_FILES_ADD_ROOTSTOCK_FILES_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";
Rootstock_filesDTO row = rootstock_filesDTO;
int childTableStartingID = 1;
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal dropzone" action="Rootstock_filesServlet?actionType=uploadFiles"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
			
			<input type="file" name="file" />
				
			

<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.ROOTSTOCK_FILES_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=rootstock_filesDTO.iD%>' tag='pb_html'/>
	
												

		<input type='hidden' class='form-control'  name='fileId' id = 'fileId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + rootstock_filesDTO.fileId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
	
	
				
	

	
	
				
	
	
				

		
				
	
		
				

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + rootstock_filesDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + rootstock_filesDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
					
	







				
							
			</div>
		
		</form>
		
		<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.ROOTSTOCK_FILES_EDIT_ROOTSTOCK_FILES_CANCEL_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.ROOTSTOCK_FILES_ADD_ROOTSTOCK_FILES_CANCEL_BUTTON, loginDTO));
					}
					
					%>
					</a>
					<button class="btn btn-success" onclick="myfunction()" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.ROOTSTOCK_FILES_EDIT_ROOTSTOCK_FILES_SUBMIT_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.ROOTSTOCK_FILES_ADD_ROOTSTOCK_FILES_SUBMIT_BUTTON, loginDTO));
					}
					%>
					</button>
				</div>

	</div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">




function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}


	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Rootstock_filesServlet");	
}

function init(row)
{


	
}

var row = 0;
bkLib.onDomLoaded(function() 
{	
});
	
window.onload =function ()
{
	init(row);
}

function  myfunction() {
	
	
	document.getElementById("bigform").submit();
	

}

var child_table_extra_id = <%=childTableStartingID%>;



</script>






