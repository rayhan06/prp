

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="asset_clearance_letter.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>





<%
String servletName = "Asset_clearance_letterServlet";
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = "bangla";

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
Asset_clearance_letterDAO asset_clearance_letterDAO = new Asset_clearance_letterDAO("asset_clearance_letter");
Asset_clearance_letterDTO asset_clearance_letterDTO = asset_clearance_letterDAO.getDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
String context = request.getContextPath() + "/";


boolean isEdit = (request.getParameter("isEdit") != null);
Calendar requestCal = Calendar.getInstance();
requestCal.setTimeInMillis(asset_clearance_letterDTO.nocDate);
int month = requestCal.get(Calendar.MONTH);
String UOPrefix = "11,00,0000.924.19.001.15";
%>

<style>
    @media (max-width: 991.98px) {
        select.form-control{
            width: auto!important;
        }
    }
    @media (min-width: 992px) {
        select.form-control{
            width: 100%!important;
        }
    }
    
     .signature-image {
        background: #f3f6f9 !important;
      }

      .printer-icon {
        cursor: pointer;
      }

      .prp-website-link {
        color: black;
        font-size: 100%;
        font-weight: normal;
      }

      .application-main-text {
        line-height: 2;
        text-indent: 50px;
      }

      .a-prp-website-link:hover,
      .a-prp-website-link:focus,
      .a-prp-website-link:visited {
        color: black;
        text-decoration: none !important;
      }

      @-moz-document url-prefix() {
        .signature-image-container {
          margin-bottom: 1.5rem;
        }
      }

      @media print {
        html,
        body {
          width: 21cm;
          height: 29.7cm;
        }

        .contentSectionContainer {
          padding-left: 3rem;
          padding-right: 3rem;
        }

        .prp-website-link {
          color: black;
          text-decoration-color: black !important;
          text-underline-position: under;
        }

        .application-main-text {
          line-height: 2.5;
        }

        .signature-image {
          border: 1px solid #666 !important;
        }

        #contentSection {
          margin-top: 7rem !important;
          font-size: 1rem !important;
        }

        @page {
          size: A4;
          margin: 0;
        }

        .printer-icon {
          display: none;
        }
      }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <button type="button" class="btn btn-sm border-0 shadow"
                            style="background-color: #a3b122; color: white; border-radius: 8px"
                            onclick="printAnyDiv('modalbody')">
                        <%=LM.getText(LC.HM_PRINT, loginDTO)%>
                    </button>
                    <button type="button" class="btn btn-sm border-0 shadow"
                            style="background-color: #7351a2; color: white; border-radius: 8px"
                            onclick="location.href='Asset_clearance_letterServlet?actionType=search'">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%>
                    </button>
                  <%--   <%
                        if (assign_assetDTO != null) {
                    %>
                    <button type="button" class="btn btn-sm border-0 shadow"
                            style="background-color: #22ccc1; color: white; border-radius: 8px"
                            onclick="location.href='Assign_assetServlet?actionType=view&ID=<%=assign_assetDTO.iD%>'">
                        <%=LM.getText(LC.HM_ASSETS, loginDTO).toUpperCase()%>
                    </button>
                    <%
                        }
                    %> --%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body" id = 'modalbody'>
            <div class="mt-5 contentSectionContainer" id="contentSection">
		      <header class="px-5">
		        <div class="text-center my-3">
		          <p class="font-weight-bolder mb-1">
 						<img
                                class="parliament-logo"
                                width="8%"
                                src="<%=context%>assets/images/perliament_logo_final2.png"
                                alt=""
                        />
					</p>
				  <p class="font-weight-bolder mb-1">বাংলাদেশ জাতীয় সংসদ সচিবালয়</p>
		          <p class="mb-1  mb-1">
		          <%=WorkflowController.getOfficeNameFromOrganogramId(asset_clearance_letterDTO.signingOrganogramId, false)%>
		          </p>
		          <a
		            class="font-weight-normal a-prp-website-link"
		            href="https://www.parliament.gov.bd"
		            ><p class="prp-website-link">www.parliament.gov.bd</p></a
		          >
		        </div>
		      </header>
		      <main class="mx-5 my-4">
		        <div>
		          <div>
		            <table class="w-100">
		              <tr>
		                <td>
		                  <p class="font-weight-bold">
		                    বিষয়ঃ না-দাবি প্রত্যয়নপত্র প্রেরণ প্রসঙ্গে।
		                  </p>
		                </td>
		               
		              </tr>
		            </table>
		          </div>
		
		          <div class="my-4">
		            <p>
		              সূত্র: মাস- <%=Utils.getDigits(month, Language)%> এর পত্র নং-
 <%=Utils.getDigits(asset_clearance_letterDTO.letterNumber, Language)%>, 
 তারিখ: 
<%=Utils.getDigits(simpleDateFormat.format(new Date(asset_clearance_letterDTO.nocDate)), Language)%>
 খ্রিঃ।
		            </p>
		          </div>
		
		          <div class="pt-2">
		            <p class="application-main-text">
		              উপযুক্ত বিষয় ও সূত্রস্থ পত্রের প্রেক্ষিতে জানানো যাচ্ছে যে,
		              বাংলাদেশ জাতীয় সংসদ সচিবালয়ে প্রেষণে কর্মরত 
		              <%=WorkflowController.getOrganogramName(asset_clearance_letterDTO.organogramId, Language)%>
		              <%=WorkflowController.getNameFromOrganogramId(asset_clearance_letterDTO.organogramId, Language)%>
		               (কোড নং-
		               <%=Utils.getDigits(asset_clearance_letterDTO.codeNumber, Language)%>
		               ) 
		               এর নিকট আইটি অধিশাখার
		              সরকারী কোন পাওনাদি নেই। এমতাবস্থায়, 
		              <%=WorkflowController.getNameFromOrganogramId(asset_clearance_letterDTO.organogramId, Language)%>
		              (কোড
		              নং-
		              <%=Utils.getDigits(asset_clearance_letterDTO.codeNumber, Language)%>
		              ) -
		              কে অবমুক্তির লক্ষ্যে না-দাবি প্রত্যয়নপত্র প্রদান করা
		              হলাে।
		            </p>
		          </div>
		          <div class="my-5 d-flex justify-content-end">
		            <div class="person-info-container">
		              <div class="signature-image-container mb-2" style="display:none">
		                <img 
		                  class="
		                    signature-image
		                    text-nowrap
		                    rounded
		                    px-4
		                    pr-5
		                    pt-3
		                    pb-5
		                  "
		                  src=""
		                  alt=" Signature Image"
		                />
		              </div>
		              <p class="text-nowrap text-center mb-1"><%=WorkflowController.getNameFromOrganogramId(asset_clearance_letterDTO.signingOrganogramId, Language)%></p>
		              <p class="text-nowrap text-center mb-1">
		                <%=WorkflowController.getOrganogramName(asset_clearance_letterDTO.signingOrganogramId, Language)%>
		              </p>
		              <p class="text-nowrap text-center mb-1">
		                জাতীয় সংসদ সচিবালয়, ঢাকা
		              </p>
		              <p class="text-nowrap text-center mb-1">ফোন- ৮১৭১৩৫৯ (অফিস)।</p>
		            </div>
		          </div>
		          <div class="border-bottom border-dark pb-3">
		            <p class="mb-1">সিনিয়র সহকারী সচিব</p>
		            <p class="mb-1">মানব সম্পদ-১</p>
		            <p class="mb-1">জাতীয় সংসদ সচিবালয়, ঢাকা।</p>
		          </div>
		          <div class="pt-3">
		            <div class="row">
		              <div class="col-6">
		                <p>ইউ.ও.নোট নং-<%=Utils.getDigits(UOPrefix, Language)%><%=Utils.getDigits(asset_clearance_letterDTO.iD + 100, Language)%>
		                </p>
		              </div>
		              <div class="col-6 text-right">
		                <p>তারিখঃ <%=Utils.getDigits(simpleDateFormat.format(new Date(asset_clearance_letterDTO.requestDate)), Language)%> খ্রি:</p>
		              </div>
		            </div>
		          </div>
		        </div>
		      </main>
		    </div>
             
        </div>
    </div>
</div>
