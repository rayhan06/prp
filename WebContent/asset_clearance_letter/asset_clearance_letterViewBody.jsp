

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="asset_clearance_letter.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>





<%
String servletName = "Asset_clearance_letterServlet";
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.ASSET_CLEARANCE_LETTER_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
Asset_clearance_letterDAO asset_clearance_letterDAO = new Asset_clearance_letterDAO("asset_clearance_letter");
Asset_clearance_letterDTO asset_clearance_letterDTO = asset_clearance_letterDAO.getDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
String context = request.getContextPath() + "/";


boolean isEdit = (request.getParameter("isEdit") != null);

%>

<style>
    @media (max-width: 991.98px) {
        select.form-control{
            width: auto!important;
        }
    }
    @media (min-width: 992px) {
        select.form-control{
            width: 100%!important;
        }
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <button type="button" class="btn btn-sm border-0 shadow"
                            style="background-color: #a3b122; color: white; border-radius: 8px"
                            onclick="printAnyDiv('modalbody')">
                        <%=LM.getText(LC.HM_PRINT, loginDTO)%>
                    </button>
                    <button type="button" class="btn btn-sm border-0 shadow"
                            style="background-color: #7351a2; color: white; border-radius: 8px"
                            onclick="location.href='Asset_clearance_letterServlet?actionType=search'">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%>
                    </button>
                  <%--   <%
                        if (assign_assetDTO != null) {
                    %>
                    <button type="button" class="btn btn-sm border-0 shadow"
                            style="background-color: #22ccc1; color: white; border-radius: 8px"
                            onclick="location.href='Assign_assetServlet?actionType=view&ID=<%=assign_assetDTO.iD%>'">
                        <%=LM.getText(LC.HM_ASSETS, loginDTO).toUpperCase()%>
                    </button>
                    <%
                        }
                    %> --%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body" id = 'modalbody'>
            <div class="row">
                <div class="col-md-8">
                    <div class="d-flex align-items-center">
                        <div>
                            <img
                                    width="30%"
                                    src="<%=context%>assets/static/parliament_logo.png"
                                    alt="logo"
                                    class="logo-default"
                            />
                        </div>
                        <div class="prescription-parliament-info">
                            <h3 class="text-left">
                                <%=LM.getText(LC.HM_PARLIAMENT_SUPPORT_CENTRE, loginDTO)%>
                            </h3>
                            <h5 class="text-left">
                                <%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                            </h5>
                            <h5 class="text-left">
                                <%=LM.getText(LC.HM_PARLIAMENT_PHONE, loginDTO)%>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
             <%
            if(isEdit)
            {
            %>
            	<form class="form-horizontal"
              action="Asset_clearance_letterServlet?actionType=clear&id=<%=asset_clearance_letterDTO.iD%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data">
            <%
            }
            %>
            <div class="table-section">
           
                <div class="mt-5 table-responsive">

                    <table class="table table-bordered table-striped text-nowrap">
                    	 <tr>

                            <td><b><%=LM.getText(LC.HM_NAME, loginDTO)%>
                            </b></td>
                            <td>
                                <%
                                    value = WorkflowController.getNameFromOrganogramId(asset_clearance_letterDTO.organogramId, Language);

                                %>
                                <%=value%>
                            </td>

                          
                            <td><b><%=LM.getText(LC.ASSET_CLEARANCE_LETTER_ADD_REQUESTDATE, loginDTO)%>
                            </b></td>
                            <td>
                                <%
                                    String formatted_insertionDate = simpleDateFormat.format(new Date(asset_clearance_letterDTO.requestDate));
                                %>
                                <%=Utils.getDigits(formatted_insertionDate, Language)%>
                            </td>

                            <td><b><%=LM.getText(LC.ASSET_CLEARANCE_LETTER_ADD_RESPONSEREQUIREDDATE, loginDTO)%>
                            </b></td>
                            <td>
                                <%
                                	formatted_insertionDate = simpleDateFormat.format(new Date(asset_clearance_letterDTO.responseDate));
                                %>
                                <%=Utils.getDigits(formatted_insertionDate, Language)%>
                            </td>

                          
                        </tr>
                       
                    </table>

                </div>


            </div>
            <div class="table-section">
                <div class="mt-5 table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">
                        <tr>
                            <td style="width:20%"><b><%=Language.equalsIgnoreCase("English")?"IT Asset Handover Status":"আইটি সম্পদ হস্তান্ত্বরের অবস্থা"%>
                            </b></td>
                            <td style="width:30%">
                            	<%
                            	if(isEdit)
                            	{
                            		String Options = CatDAO.getOptions(Language, "basic_status", asset_clearance_letterDTO.itAssetHandoverStatusCat);
                            		%>
                            		<select class='form-control w-100' name='itAssetHandoverStatusCat' id='itAssetHandoverStatusCat'>
                            		<%=Options%>
                            		</select>
                            		<%
                            	}
                            	else
                            	{
                            	%>

                                <%
											value = CatRepository.getInstance().getText(Language, "basic_status", asset_clearance_letterDTO.itAssetHandoverStatusCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
								<%
                            	}
								%>
                            </td>
                        
                            <td style="width:20%">
                                <b>
                                 <%=Language.equalsIgnoreCase("English")?"Email Login Access Revoking":"ইমেইল  লগইন অ্যাক্সেস প্রত্যাহার"%>
                                
                                </b></td>
                            <td style="width:30%">
                            	<%
                            	if(isEdit)
                            	{
                            		String Options = CatDAO.getOptions(Language, "basic_status", asset_clearance_letterDTO.emailDeclarationStatusCat);
                            		%>
                            		<select class='form-control w-100' name='emailDeclarationStatusCat' id='emailDeclarationStatusCat'>
                            		<%=Options%>
                            		</select>
                            		<%
                            	}
                            	else
                            	{
                            	%>
                                <%
											value = CatRepository.getInstance().getText(Language, "basic_status", asset_clearance_letterDTO.emailDeclarationStatusCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
								<%
                            	}
								%>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:20%">
                                <b><%=LM.getText(LC.ASSET_CLEARANCE_LETTER_ADD_DIRECTORYLOGINSTATUSCAT, loginDTO)%>
                                <%=Language.equalsIgnoreCase("English")?"Directory Login Access Revoking":"ডিরেক্টরি লগইন অ্যাক্সেস প্রত্যাহার"%>
                            
                                </b></td>
                            <td style="width:30%">
                            <%
                            	if(isEdit)
                            	{
                            		String Options = CatDAO.getOptions(Language, "basic_status", asset_clearance_letterDTO.directoryLoginStatusCat);
                            		%>
                            		<select class='form-control' name='directoryLoginStatusCat' id='directoryLoginStatusCat'>
                            		<%=Options%>
                            		</select>
                            		<%
                            	}
                            	else
                            	{
                            	%>
                                <%
											value = CatRepository.getInstance().getText(Language, "basic_status", asset_clearance_letterDTO.directoryLoginStatusCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
											<%
                            	}
											%>

                            </td>
                        
                            <td style="width:20%"><b>
                             <%=Language.equalsIgnoreCase("English")?"Other Software Access Revoking":"অন্যান্য সফটওয়্যারের অ্যাক্সেস প্রত্যাহার"%>
                            
                            </b></td>
                            <td style="width:30%">
                            	<%
                            	if(isEdit)
                            	{
                            		String Options = CatDAO.getOptions(Language, "basic_status", asset_clearance_letterDTO.parliamentSoftwareLoginStatusCat);
                            		%>
                            		<select class='form-control' name='parliamentSoftwareLoginStatusCat' id='parliamentSoftwareLoginStatusCat'>
                            		<%=Options%>
                            		</select>
                            		<%
                            	}
                            	else
                            	{
                            	%>
                                <%
											value = CatRepository.getInstance().getText(Language, "basic_status", asset_clearance_letterDTO.parliamentSoftwareLoginStatusCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
								<%
                            	}
								%>
                            </td>
                        </tr>
                          <tr>
                            <td style="width:20%"><b>                            
                             <%=Language.equalsIgnoreCase("English")?"Report Server Login Access Revoking":"রিপোর্ট সার্ভারের লগইন অ্যাক্সেস প্রত্যাহার"%>
                            
                            
                            </b></td>
                            <td style="width:30%">
                            	<%
                            	if(isEdit)
                            	{
                            		String Options = CatDAO.getOptions(Language, "basic_status", asset_clearance_letterDTO.reportServerStatusCat);
                            		%>
                            		<select class='form-control' name='reportServerStatusCat' id='reportServerStatusCat'>
                            		<%=Options%>
                            		</select>
                            		<%
                            	}
                            	else
                            	{
                            	%>
                                <%
											value = CatRepository.getInstance().getText(Language, "basic_status", asset_clearance_letterDTO.reportServerStatusCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
								<%
                            	}
								%>
                            </td>
                            <td style="width:20%"><b><%=LM.getText(LC.ASSET_CLEARANCE_LETTER_ADD_CLEARANCESTATUSCAT, loginDTO)%>
                            </b></td>
                            <td style="width:30%">
                            	<%
                            	if(isEdit)
                            	{
                            		String Options = CatDAO.getOptions(Language, "basic_status", asset_clearance_letterDTO.clearanceStatusCat);
                            		%>
                            		<select class='form-control' name='clearanceStatusCat' id='clearanceStatusCat'>
                            		<%=Options%>
                            		</select>
                            		<%
                            	}
                            	else
                            	{
                            	%>
                                <%
											value = CatRepository.getInstance().getText(Language, "basic_status", asset_clearance_letterDTO.clearanceStatusCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
								<%
                            	}
								%>
                            </td>
                        	</tr>
                        	<tr>
                            <td style="width:20%"><b><%=LM.getText(LC.ASSET_CLEARANCE_LETTER_ADD_REMARKS, loginDTO)%>
                            </b></td>
                            <td colspan="3">
                            <%
                            	if(isEdit)
                            	{                            		
                            		%>
                            		<textarea class='form-control' name='remarks' id='remarks'><%=asset_clearance_letterDTO.remarks%></textarea>
                            		<%
                            	}
                            	else
                            	{
                            	%>
                                <%
                                    value = asset_clearance_letterDTO.remarks;
                                %>

                                <%=value%>
                                <%
                            	}
								%>
                            </td>

                        </tr>
                    </table>
                </div>
            </div>
            
            <%
            if(isEdit)
            {
            %>
            				<div class="text-right">
                                <button class="btn-sm shadow text-white border-0 submit-btn mt-3 mt-md-0" type="submit">
                                    <%=LM.getText(LC.ASSET_CLEARANCE_LETTER_ADD_ASSET_CLEARANCE_LETTER_SUBMIT_BUTTON, loginDTO)%>
                                </button>
                            </div>
            </form>
            <%
            }
            %>
             
        </div>
    </div>
</div>
