<%@page pageEncoding="UTF-8" %>

<%@page import="asset_clearance_letter.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@page import="support_ticket.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.ASSET_CLEARANCE_LETTER_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_ASSET_CLEARANCE_LETTER;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Asset_clearance_letterDTO asset_clearance_letterDTO = (Asset_clearance_letterDTO)request.getAttribute("asset_clearance_letterDTO");
CommonDTO commonDTO = asset_clearance_letterDTO;
String servletName = "Asset_clearance_letterServlet";


System.out.println("asset_clearance_letterDTO = " + asset_clearance_letterDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Asset_clearance_letterDAO asset_clearance_letterDAO = (Asset_clearance_letterDAO)request.getAttribute("asset_clearance_letterDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
		
											<td id = '<%=i%>_organogramId'>
											<%
											value = asset_clearance_letterDTO.userName + "";
											%>
				
											<b><%=Utils.getDigits(value, Language)%></b><br>
											<%
											value = asset_clearance_letterDTO.organogramId + "";
											%>
											<%
											value = WorkflowController.getNameFromOrganogramId(asset_clearance_letterDTO.organogramId, Language) + ", " + WorkflowController.getOrganogramName(asset_clearance_letterDTO.organogramId, Language) + ", " + WorkflowController.getUnitNameFromOrganogramId(asset_clearance_letterDTO.organogramId, Language);
											%>											
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
											
											<td>
											<%=WorkflowController.getNameFromOrganogramId(asset_clearance_letterDTO.signingOrganogramId, Language)%>
											</td>
		
											
		
											<td id = '<%=i%>_requestDate'>
											
											<%
											String formatted_requestDate = simpleDateFormat.format(new Date(asset_clearance_letterDTO.requestDate));
											%>
											<%=Utils.getDigits(formatted_requestDate, Language)%>
				
			
											</td>
		
											<td>
											
											<%
											if(asset_clearance_letterDTO.responseDate > 0)
											{
												String formatted_responseRequiredDate = simpleDateFormat.format(new Date(asset_clearance_letterDTO.responseDate));
											%>
											<%=Utils.getDigits(formatted_responseRequiredDate, Language)%>
											<%
											}
											%>
				
			
											</td>
		
											<td id = '<%=i%>_clearanceStatusCat'>
											
											<%=CatRepository.getName(Language, "noc_status", asset_clearance_letterDTO.clearanceStatusCat)%>
			
											</td>
		

		
	

											<td>
												<button
														type="button"
														class="btn-sm border-0 shadow bg-light btn-border-radius"
														style="color: #ff6b6b;"
														onclick="location.href='Support_ticketServlet?actionType=view&ID=<%=asset_clearance_letterDTO.supportTicketId%>'"
												>
													<i class="fa fa-eye"></i>
												</button>												
											</td>
	
											<td id = '<%=i%>_Edit' style = "display:none">
												<%
												if(asset_clearance_letterDTO.clearanceStatusCat != Asset_clearance_letterDTO.YES)
												{
												%>
												<button
														type="button"
														class="btn-sm border-0 shadow btn-border-radius text-white"
														style="background-color: #ff6b6b;"
														onclick="location.href='Asset_clearance_letterServlet?actionType=view&ID=<%=asset_clearance_letterDTO.iD%>&isEdit=1'"
												>
													<i class="fa fa-edit"></i>
												</button>
												<%
												}
												%>																			
											</td>
											
											<td>
											<%
												if(asset_clearance_letterDTO.clearanceStatusCat == Asset_clearance_letterDTO.NOC_APPROVED)
												{
												%>
												<button
														type="button"
														class="btn-sm border-0 shadow bg-light btn-border-radius"
														style="color: #ff6b6b;"
														onclick="location.href='Asset_clearance_letterServlet?actionType=certificate&ID=<%=asset_clearance_letterDTO.iD%>'"
												>
													<i class="fa fa-pen"></i>
												</button>
												<%
												}
												%>											
											</td>										
											
											
											<td id='<%=i%>_checkbox' class="text-right">
											<%
												if(asset_clearance_letterDTO.clearanceStatusCat != Asset_clearance_letterDTO.YES)
												{
												%>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=asset_clearance_letterDTO.iD%>'/></span>
												</div>
												<%
												}
												%>
											</td>
																						
											

