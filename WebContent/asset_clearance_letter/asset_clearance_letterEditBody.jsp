<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="asset_clearance_letter.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import="employee_offices.*" %>
<%@ page import="pb.*"%>

<%
Asset_clearance_letterDTO asset_clearance_letterDTO;
asset_clearance_letterDTO = (Asset_clearance_letterDTO)request.getAttribute("asset_clearance_letterDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(asset_clearance_letterDTO == null)
{
	asset_clearance_letterDTO = new Asset_clearance_letterDTO();
	
}
System.out.println("asset_clearance_letterDTO = " + asset_clearance_letterDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = LM.getText(LC.ASSET_CLEARANCE_LETTER_ADD_ASSET_CLEARANCE_LETTER_ADD_FORMNAME, loginDTO);
String servletName = "Asset_clearance_letterServlet";
String fileColumnName = "";

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

boolean isPermanentTable = true;
String Language = LM.getText(LC.ASSET_CLEARANCE_LETTER_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp" >
<jsp:param name="isHierarchyNeeded" value="false" />
</jsp:include>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Asset_clearance_letterServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=asset_clearance_letterDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_CLEARANCE_LETTER_ADD_ORGANOGRAMID, loginDTO)%>*</label>
                                                            <div class="col-md-8">
																<button type="button" class="btn btn-primary btn-block shadow btn-border-radius" onclick="addEmployeeWithRow(this.id)" id="organogramId_button_<%=i%>" tag='pb_html'><%=LM.getText(LC.HM_ADD_EMPLOYEE, loginDTO)%></button>
																<table class="table table-bordered table-striped">
																	<tbody id="organogramId_table_<%=i%>" tag='pb_html'>
																	<%
																	if(asset_clearance_letterDTO.organogramId != -1)
																	{
																		%>
																		<tr>
																			<td style="width:20%"><%=WorkflowController.getUserNameFromOrganogramId(asset_clearance_letterDTO.organogramId)%></td>
																			<td style="width:40%"><%=WorkflowController.getNameFromOrganogramId(asset_clearance_letterDTO.organogramId, Language)%></td>
																			<td><%=WorkflowController.getOrganogramName(asset_clearance_letterDTO.organogramId, Language)%>, <%=WorkflowController.getUnitNameFromOrganogramId(asset_clearance_letterDTO.organogramId, Language)%></td>
																		</tr>
																		<%
																	}
																	%>
																	</tbody>
																</table>
																<input type='hidden' class='form-control'  name = 'organogramId' id = 'organogramId_hidden_<%=i%>' value='<%=asset_clearance_letterDTO.organogramId%>' tag='pb_html'/>
			
															</div>
                                                      </div>
                                                      
                                                      <div class="form-group row" style = "display:none">
                                                            <label class="col-md-4 col-form-label text-md-right">
                                                            <%=Language.equalsIgnoreCase("English")?"Signing Employee":"সাক্ষরকারী কর্মকর্তা"%>
                                                            </label>
                                                            <div class="col-md-8">
																<select  class='form-control' name='signingOrganogramId'
					                                                   id='signingOrganogramId' 
					                                                   tag='pb_html' required>
					                                                   <option><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
					                                                   <%
					                                                   Set<Long> ticketAdmins = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.TICKET_ADMIN_ROLE);
					                                                   Set<Long> itEmployees = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.IT_EMPLOYEE_ROLE);
					                                                   ticketAdmins.addAll(itEmployees);
					                                                   for(Long ticketAdmin: ticketAdmins)
					                                                   {
					                                                   %>
					                                                   <option value = "<%=ticketAdmin%>" <%=asset_clearance_letterDTO.signingOrganogramId == ticketAdmin? "selected":"" %>>
					                                                   <%=WorkflowController.getNameFromOrganogramId(ticketAdmin, Language)%>
					                                                   </option>
					                                                   <%
					                                                   }
					                                                   %>
					                                             </select>
															</div>
                                                      </div>									
																					
													<div class="form-group row" style = "display:none">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_CLEARANCE_LETTER_ADD_RESPONSEREQUIREDDATE, loginDTO)%></label>
                                                            <div class="col-md-8">
																<%value = "responseRequiredDate_js_" + i;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='responseRequiredDate' id = 'responseRequiredDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(asset_clearance_letterDTO.responseDate))%>' tag='pb_html'>
															</div>
                                                      </div>
                                                      
                                                      <div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right">
                                                            <%=Language.equalsIgnoreCase("English")?"NOC Date":"না-দাবি প্রত্যয়নের তারিখ"%>
                                                            </label>
                                                            <div class="col-md-8">
																<%value = "nocDate_js_" + i;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='nocDate' id = 'nocDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(asset_clearance_letterDTO.nocDate))%>' tag='pb_html'>
															</div>
                                                      </div>
                                                      
                                                       <div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right">
                                                            <%=Language.equalsIgnoreCase("English")?"Letter Number":"পত্র নং"%>
                                                            *</label>
                                                            <div class="col-md-8">

																<input type='text' name='letterNumber' required id = 'letterNumber_text_<%=i%>'
																class='form-control' 
																value= '<%=asset_clearance_letterDTO.letterNumber%>' tag='pb_html'>		
															</div>
                                                      </div>
                                                      
                                                      <div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right">
                                                            <%=Language.equalsIgnoreCase("English")?"Code Number":"কোড নং"%>
                                                            *</label>
                                                            <div class="col-md-8">

																<input type='text' required name='codeNumber' id = 'codeNumber_text_<%=i%>'
																class='form-control' 
																value= '<%=asset_clearance_letterDTO.codeNumber%>' tag='pb_html'>		
															</div>
                                                      </div>
                                                      
                                                       <div class="form-group row" style = "display:none">
                                                            <label class="col-md-4 col-form-label text-md-right">
                                                            <%=Language.equalsIgnoreCase("English")?"U.O.Note Number":"ইউ.ও.নোট নং"%>
                                                            *</label>
                                                            <div class="col-md-8">

																<input type='text'  name='uONoteNumber' id = 'uONoteNumber_text_<%=i%>'
																class='form-control' 
																value= '<%=asset_clearance_letterDTO.uONoteNumber%>' tag='pb_html'>		
															</div>
                                                      </div>								
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-11">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.ASSET_CLEARANCE_LETTER_ADD_ASSET_CLEARANCE_LETTER_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.ASSET_CLEARANCE_LETTER_ADD_ASSET_CLEARANCE_LETTER_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, validate)
{

	if($("#organogramId_hidden_0").val() == '-1')
	{
		toastr.error("No employeee chosen");
		return false;
	}

	preprocessDateBeforeSubmitting('requestDate', row);
	preprocessDateBeforeSubmitting('responseRequiredDate', row);
	preprocessDateBeforeSubmitting('nocDate', row);

	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Asset_clearance_letterServlet");	
}

function init(row)
{

	setDateByStringAndId('requestDate_js_' + row, $('#requestDate_date_' + row).val());
	setDateByStringAndId('responseRequiredDate_js_' + row, $('#responseRequiredDate_date_' + row).val());
	setDateByStringAndId('nocDate_js_' + row, $('#nocDate_date_' + row).val());

	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;



</script>






