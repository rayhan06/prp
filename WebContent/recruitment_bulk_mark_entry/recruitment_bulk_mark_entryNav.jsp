<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDAO" %>


<%
    System.out.println("Inside nav.jsp");
    String url = request.getParameter("url");
    String navigator = request.getParameter("navigator");
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";
    String pageno = "";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String[][] searchFieldInfo = rn.getSearchFieldInfo();
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";
    int row = 0;

    String Language = LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    int pagination_number = 0;
    boolean isPermanentTable = rn.m_isPermanentTable;
    System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
    Recruitment_job_descriptionDAO jobDescriptionDAO = new Recruitment_job_descriptionDAO();
%>

<!-- search control -->

<div class="kt-content  kt-grid__item kt-grid__item--fluid" style="padding: 0px !important; margin-bottom: -18px">
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <div class="onlyborder">
                <div class="row mx-2 mx-md-0">
                    <div class="col-md-10 offset-md-1">
                        <div class="sub_title_top">
                            <div class="sub_title">
                                <h4 style="background: white">
                                    <%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_RECRUITMENT_BULK_MARK_ENTRY_SEARCH_FORMNAME, loginDTO)%>
                                </h4>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 control-label text-md-right">
                                <%=LM.getText(LC.ADMIT_CARD_ADD_RECRUITMENTJOBDESCRIPTIONTYPE, loginDTO)%>
                            </label>
                            <div class="col-md-9">
                                <select class='form-control' name='job_id' id='job_id' onchange='onJobChange()'
                                        onSelect='setSearchChanged()'>
                                    <%=jobDescriptionDAO.getJobListWithoutInternship("ALL", Language)%>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 control-label text-md-right">
                                <%=LM.getText(LC.CANDIDATE_LIST_LEVEL, loginDTO)%>
                            </label>
                            <div class="col-md-9">
                                <select class='form-control' name='level_id' id='level'
                                        onSelect='setSearchChanged()'>

                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 control-label text-md-right">
                                <%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_ROLL, loginDTO)%>
                            </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="roll" placeholder="" name="roll"
                                       onSelect='setSearchChanged()'>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <div class="form-actions text-right mt-3">
                <input type="hidden" name="search" value="yes"/>
                <input type="submit" onclick="allfield_changed('',0)"
                       class="btn btn-sm submit-btn text-white shadow btn-border-radius"
                       value="<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>"
                />
            </div>
        </div>
    </div>
    <div class="portlet-title">
        <!--<div class="caption" style="margin-top: 5px;"><i class="fa fa-search-plus"  style="margin-top:-3px"></i><%=pageName%></div> -->
        <p class="desktop-only" style="float:right; margin:10px 5px !important;"></p>
        <!--<div class="tools">
            <a class="expand" href="javascript:;" data-original-title="" title=""></a>
        </div>   -->
        <div class="col-xs-12 col-sm-5 col-md-4" style="margin-top:10px">
            <%

                out.println("<input type='hidden' class='form-control border-0' onKeyUp='allfield_changed(\"\",0)' id='anyfield'  name='" + LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_ANYFIELD, loginDTO) + "' ");
                String value = (String) session.getAttribute(searchFieldInfo[searchFieldInfo.length - 1][1]);

                if (value != null) {
                    out.println("value = '" + value + "'");
                }

                out.println("/><br />");
            %>
        </div>
    </div>
</div>


<!-- search control -->
<%--<div class="portlet box portlet-btcl">--%>
<%--	<div class="portlet-title">--%>
<%--		<!--<div class="caption" style="margin-top: 5px;"><i class="fa fa-search-plus"  style="margin-top:-3px"></i><%=pageName%></div> -->--%>
<%--		<p class="desktop-only" style="float:right; margin:10px 5px !important;">Advanced Search</p>--%>
<%--		<!--<div class="tools">--%>
<%--			<a class="expand" href="javascript:;" data-original-title="" title=""></a>--%>
<%--		</div>   -->--%>

<%--		<div class="col-xs-12 col-sm-5 col-md-4" style="margin-top:10px">--%>
<%--			<%--%>

<%--				out.println("<input type='hidden' class='form-control' onKeyUp='allfield_changed(\"\",0)' id='anyfield'  name='"+  LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_ANYFIELD, loginDTO) +"' ");--%>
<%--				String value = (String)session.getAttribute(searchFieldInfo[searchFieldInfo.length - 1][1]);--%>

<%--				if( value != null)--%>
<%--				{--%>
<%--					out.println("value = '" + value + "'");--%>
<%--				}--%>

<%--				out.println ("/><br />");--%>
<%--			%>--%>
<%--		</div>--%>


<%--	</div>--%>
<%--	<!--class="portlet-body form collapse"-->--%>
<%--	<div--%>
<%--			class="portlet-body form"--%>
<%--	>--%>
<%--		<!-- BEGIN FORM-->--%>
<%--		<div class="container-fluid">--%>
<%--			<div class="row col-lg-offset-1">--%>
<%--				<!--<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">--%>
<%--					<div class="col-xs-2 col-sm-4 col-md-4">--%>
<%--						<label for="" class="control-label pull-right"><%=LM.getText(LC.ADMIT_CARD_ADD_RECRUITMENTJOBDESCRIPTIONTYPE, loginDTO)%></label>--%>
<%--					</div>--%>
<%--					<div class="col-xs-10 col-sm-8 col-md-8">--%>

<%--						<select class='form-control'  name='job_id' id = 'job_id' onchange='onJobChange()' onSelect='setSearchChanged()'>--%>


<%--						</select>--%>
<%--					</div>--%>
<%--				</div>--%>
<%--				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">--%>
<%--					<div class="col-xs-2 col-sm-4 col-md-4">--%>
<%--						<label for="" class="control-label pull-right"><%=LM.getText(LC.CANDIDATE_LIST_LEVEL, loginDTO)%></label>--%>
<%--					</div>--%>
<%--					<div class="col-xs-10 col-sm-8 col-md-8">--%>

<%--						<select class='form-control'  name='level_id' id = 'level' onSelect='setSearchChanged()'>--%>

<%--							<option value=""></option>--%>
<%--						</select>--%>
<%--					</div>--%>
<%--				</div> -->--%>
<%--				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">--%>
<%--					<div class="col-xs-2 col-sm-4 col-md-4">--%>
<%--						<label for="" class="control-label pull-right"> <%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_ROLL, loginDTO)%></label>--%>
<%--					</div>--%>
<%--					<div class="col-xs-10 col-sm-8 col-md-8">--%>

<%--						<input type="text" class="form-control" id="roll" placeholder="" name="roll" onSelect='setSearchChanged()'>--%>
<%--					</div>--%>
<%--				</div>--%>
<%--				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">--%>
<%--					<div class="col-xs-2 col-sm-4 col-md-4">--%>
<%--						<label for="" class="control-label pull-right"> <%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_MARKS, loginDTO)%></label>--%>
<%--					</div>--%>
<%--					<div class="col-xs-10 col-sm-8 col-md-8">--%>

<%--						<input type="text" class="form-control" id="marks" placeholder="" name="marks" onSelect='setSearchChanged()'>--%>
<%--					</div>--%>
<%--				</div>--%>


<%--			</div>--%>


<%--			<div class=clearfix></div>--%>

<%--			<div class="form-actions fluid" style="margin-top:10px">--%>
<%--				<div class="container-fluid">--%>
<%--					<div class="row">--%>
<%--						<div class="col-lg-offset-3 col-xs-12 col-md-12  col-md-12 col-lg-9">--%>
<%--							<div class="col-xs-4  col-sm-4  col-md-6">--%>
<%--								<input type="hidden" name="search" value="yes" />--%>
<%--								<!-- 				          	<input type="reset" class="btn  btn-sm btn btn-circle  grey-mint btn-outline sbold uppercase" value="Reset" > -->--%>
<%--								<input type="submit" onclick="allfield_changed('',0)"--%>
<%--									   class="btn  btn-sm btn btn-circle btn-sm green-meadow btn-outline sbold uppercase advanceseach"--%>
<%--									   value="<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>">--%>
<%--							</div>--%>
<%--						</div>--%>
<%--					</div>--%>
<%--				</div>--%>
<%--			</div>--%>

<%--		</div>--%>
<%--		<!-- END FORM-->--%>
<%--	</div>--%>


<!-- search control -->
<!--<div class="box box-primary">

<div class="box-body">
<div class="form-horizontal">

<div class="form-body">

<div class="row">

<div class="col-lg-3">
<label for="" class="control-label col-4"><%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_ANYFIELD, loginDTO)%>:</label>
<div class="col-8">
<input type="text" class="form-control" id="anyfield"
placeholder="" name="anyfield" onKeyUp='allfield_changed("",0)'>
</div>
</div>
<div class="col-lg-3">
<label for="" class="control-label col-4"><%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_ROLL, loginDTO)%></label>
<div class="col-8">
<input type="text" class="form-control" id="roll" placeholder="" name="roll" onkeyup="allfield_changed('',0)">
</div>
</div>
<div class="col-lg-3">
<label for="" class="control-label col-4"><%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_MARKS, loginDTO)%></label>
<div class="col-8">
<input type="text" class="form-control" id="marks" placeholder="" name="marks" onkeyup="allfield_changed('',0)">
</div>
</div>
<div class="col-lg-3">
<label for="" class="control-label col-4"><%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_GRADE, loginDTO)%></label>
<div class="col-8">
<input type="text" class="form-control" id="grade" placeholder="" name="grade" onkeyup="allfield_changed('',0)">
</div>
</div>


</div>

</div>
</div>

</div>
</div> -->


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">


    var numbers = {
        "\u09E6": 0,
        "\u09E7": 1,
        "\u09E8": 2,
        "\u09E9": 3,
        "\u09EA": 4,
        "\u09EB": 5,
        "\u09EC": 6,
        "\u09ED": 7,
        "\u09EE": 8,
        "\u09EF": 9
    };

    function convertBNToEN(input) {

        let output = [];

        for (let i = 0; i < input.length; ++i) {
            if (numbers.hasOwnProperty(input[i])) {
                output.push(numbers[input[i]]);
            } else {
                output.push(input[i]);
            }
        }

        return output.join('');
    }

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        var params = 'AnyField=' + document.getElementById('anyfield').value;
        let roll = $('#roll').val();
        //let marks = $('#marks').val();
        let job_id = $('#job_id').val();
        let level_id = $('#level').val();
        params += '&roll=' + convertBNToEN(roll);
        //params +=  '&marks='+ convertBNToEN(marks);
        params += '&job_id=' + job_id;
        params += '&level_id=' + level_id;
        //params +=  '&grade='+ $('#grade').val();

        params += '&search=true&ajax=true';
        //console.log("params: "+params);

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

    function onJobChange() {
        let jobValue = document.getElementById('job_id').value;
        let language = '<%=Language%>';


        if (jobValue && jobValue.length > 0) {
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById('level').innerHTML = this.responseText;
                } else if (this.readyState == 4 && this.status != 200) {
                    document.getElementById('level').innerHTML = "";
                }
            };

            let params = "Recruitment_job_descriptionServlet?actionType=getExamTypesByJobIdForMarksEntry";
            params = params + "&language=" + language + "&jobId=" + jobValue;

            xhttp.open("Get", params, true);
            xhttp.send();

        } else {
            document.getElementById('level').innerHTML = '';
        }
    }

</script>

