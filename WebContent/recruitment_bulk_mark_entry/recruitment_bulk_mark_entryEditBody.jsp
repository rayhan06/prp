<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="recruitment_bulk_mark_entry.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>

<%
    Recruitment_bulk_mark_entryDTO recruitment_bulk_mark_entryDTO;
    recruitment_bulk_mark_entryDTO = (Recruitment_bulk_mark_entryDTO) request.getAttribute("recruitment_bulk_mark_entryDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (recruitment_bulk_mark_entryDTO == null) {
        recruitment_bulk_mark_entryDTO = new Recruitment_bulk_mark_entryDTO();

    }
    System.out.println("recruitment_bulk_mark_entryDTO = " + recruitment_bulk_mark_entryDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_ADD_RECRUITMENT_BULK_MARK_ENTRY_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
%>

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    String Language = LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<!--new layout start-->


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important; margin-bottom: -18px">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Recruitment_bulk_mark_entryServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-8 offset-md-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_ADD_ROLL, loginDTO)%>
                                        </label>
                                        <div class="col-md-9" id='roll_div_<%=i%>'>
                                            <input
                                                    type='text'
                                                    class='form-control'
                                                    name='roll'
                                                    id='roll_text_<%=i%>'
                                                    value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.roll + "'"):("'" + "0" + "'")%>
                                                            tag='pb_html'
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_ADD_MARKS, loginDTO)%>
                                        </label>
                                        <div class="col-md-9" id='status_div_<%=i%>'>
                                            <input type='text'
                                                   class='form-control'
                                                   name='marks'
                                                   id='marks_text_<%=i%>'
                                                   value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.marks + "'"):("'" + "0" + "'")%>
                                                           tag='pb_html'
                                            />
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='id'
                                           id='id_hidden_<%=i%>'
                                           value='<%=recruitment_bulk_mark_entryDTO.id%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='jobId'
                                           id='jobId_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.jobId + "'"):("'" + "0" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='levelId'
                                           id='levelId_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.levelId + "'"):("'" + "0" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='grade'
                                           id='grade_text_<%=i%>' value="0" tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.insertionDate + "'"):("'" + "0" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedBy'
                                           id='insertedBy_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.insertedBy + "'"):("'" + "" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='modifiedBy'
                                           id='modifiedBy_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.modifiedBy + "'"):("'" + "" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.isDeleted + "'"):("'" + "false" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.lastModificationTime + "'"):("'" + "0" + "'")%>
                                                   tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-actions text-right mt-3">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_ADD_RECRUITMENT_BULK_MARK_ENTRY_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_ADD_RECRUITMENT_BULK_MARK_ENTRY_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<!--new layout end-->


<%--<div class="box box-primary">--%>
<%--	<div class="box-header with-border">--%>
<%--		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>--%>
<%--	</div>--%>
<%--	<div class="box-body">--%>
<%--		<form class="form-horizontal" action="Recruitment_bulk_mark_entryServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"--%>
<%--		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"--%>
<%--		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">--%>
<%--			<div class="form-body">--%>


<%--		<input type='hidden' class='form-control'  name='id' id = 'id_hidden_<%=i%>' value='<%=recruitment_bulk_mark_entryDTO.id%>' tag='pb_html'/>--%>
<%--	--%>
<%--												--%>

<%--		<input type='hidden' class='form-control'  name='jobId' id = 'jobId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.jobId + "'"):("'" + "0" + "'")%>--%>
<%-- tag='pb_html'/>--%>
<%--												--%>

<%--		<input type='hidden' class='form-control'  name='levelId' id = 'levelId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.levelId + "'"):("'" + "0" + "'")%>--%>
<%-- tag='pb_html'/>--%>
<%--												--%>
<%--	--%>
<%--<label class="col-md-3 col-form-label text-md-right">--%>
<%--	<%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_ADD_ROLL, loginDTO)%>--%>
<%--</label>--%>
<%--<div class="form-group ">					--%>
<%--	<div class="col-lg-6 " id = 'roll_div_<%=i%>'>	--%>
<%--		<input type='text' class='form-control'  name='roll' id = 'roll_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.roll + "'"):("'" + "0" + "'")%>--%>
<%--   tag='pb_html'/>					--%>
<%--	</div>--%>
<%--</div>			--%>
<%--				--%>
<%--	--%>
<%--<label class="col-md-3 col-form-label text-md-right">--%>
<%--	<%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_ADD_MARKS, loginDTO)%>--%>
<%--</label>--%>
<%--<div class="form-group ">					--%>
<%--	<div class="col-lg-6 " id = 'marks_div_<%=i%>'>	--%>
<%--		<input type='text' class='form-control'  name='marks' id = 'marks_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.marks + "'"):("'" + "0" + "'")%>--%>
<%--   tag='pb_html'/>					--%>
<%--	</div>--%>
<%--</div>			--%>


<%--		<input type='hidden' class='form-control'  name='grade' id = 'grade_text_<%=i%>' value="0" tag='pb_html'/>--%>

<%--		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.insertionDate + "'"):("'" + "0" + "'")%>--%>
<%-- tag='pb_html'/>--%>
<%--												--%>

<%--		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.insertedBy + "'"):("'" + "" + "'")%>--%>
<%-- tag='pb_html'/>--%>
<%--												--%>

<%--		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.modifiedBy + "'"):("'" + "" + "'")%>--%>
<%-- tag='pb_html'/>--%>
<%--												--%>

<%--		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.isDeleted + "'"):("'" + "false" + "'")%>--%>
<%-- tag='pb_html'/>--%>
<%--											--%>
<%--												--%>

<%--		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.lastModificationTime + "'"):("'" + "0" + "'")%>--%>
<%-- tag='pb_html'/>--%>
<%--												--%>
<%--					--%>
<%--	--%>


<%--				<div class="form-actions text-center">--%>
<%--					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">					--%>
<%--						<%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_ADD_RECRUITMENT_BULK_MARK_ENTRY_CANCEL_BUTTON, loginDTO)%>						--%>
<%--					</a>--%>
<%--					<button class="btn btn-success" type="submit">--%>
<%--					--%>
<%--						<%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_ADD_RECRUITMENT_BULK_MARK_ENTRY_SUBMIT_BUTTON, loginDTO)%>						--%>
<%--					--%>
<%--					</button>--%>
<%--				</div>--%>
<%--							--%>
<%--			</div>--%>
<%--		--%>
<%--		</form>--%>

<%--	</div>--%>
<%--</div>--%>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


    $(document).ready(function () {

        dateTimeInit("<%=Language%>");

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    });

    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Recruitment_bulk_mark_entryServlet");
    }

    function init(row) {


    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






