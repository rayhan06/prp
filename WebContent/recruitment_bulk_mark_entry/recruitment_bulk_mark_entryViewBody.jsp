<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="recruitment_bulk_mark_entry.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Recruitment_bulk_mark_entryDAO recruitment_bulk_mark_entryDAO = new Recruitment_bulk_mark_entryDAO("recruitment_bulk_mark_entry");
    Recruitment_bulk_mark_entryDTO recruitment_bulk_mark_entryDTO = recruitment_bulk_mark_entryDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<!--new layout start-->

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important; margin-bottom: -18px">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title prp-page-title">
                            <i class="fa fa-gift"></i>&nbsp;
                            <%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_ADD_RECRUITMENT_BULK_MARK_ENTRY_ADD_FORMNAME, loginDTO)%>
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body form-body">
                    <div>
                        <h5 class="table-title">
                            <%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_ADD_RECRUITMENT_BULK_MARK_ENTRY_ADD_FORMNAME, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <td>
                                        <b>
                                            <%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_ADD_ROLL, loginDTO)%>
                                        </b>
                                    </td>
                                    <td>
                                        <%
                                            value = recruitment_bulk_mark_entryDTO.roll + "";
                                        %>
                                        <%=Utils.getDigits(value, Language)%>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:30%">
                                        <b><%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_ADD_MARKS, loginDTO)%>
                                        </b></td>
                                    <td>
                                        <%
                                            value = recruitment_bulk_mark_entryDTO.marks + "";
                                        %>
                                        <%=Utils.getDigits(value, Language)%>
                                    </td>

                                </tr>


                                <tr>
                                    <td style="width:30%">
                                        <b><%=LM.getText(LC.CANDIDATE_LIST_LEVEL, loginDTO)%>
                                        </b></td>
                                    <td>
                                        <%
                                            value = recruitment_bulk_mark_entryDTO.levelId + "";
                                        %>
                                        <%=Utils.getDigits(value, Language)%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--new layout end-->


<!-- <div class="modal-content viewmodal"> -->
<%--<div class="menubottom">--%>
<%--            <div class="modal-header">--%>
<%--                <div class="col-md-12">--%>
<%--                    <div class="row">--%>
<%--                        <div class="col-md-9 col-sm-12">--%>
<%--                            <h5 class="modal-title"><%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_ADD_RECRUITMENT_BULK_MARK_ENTRY_ADD_FORMNAME, loginDTO)%></h5>--%>
<%--                        </div>--%>
<%--                        <div class="col-md-3 col-sm-12">--%>
<%--                            <div class="row">--%>
<%--                                <div class="col-md-6">--%>
<%--                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>--%>
<%--                                </div>--%>
<%--                                <div class="col-md-6">--%>
<%--                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </div>--%>


<%--            </div>--%>

<%--            <div class="modal-body container">--%>
<%--			--%>
<%--			<div class="row div_border office-div">--%>

<%--                    <div class="col-md-12">--%>
<%--                        <h3><%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_ADD_RECRUITMENT_BULK_MARK_ENTRY_ADD_FORMNAME, loginDTO)%></h3>--%>
<%--						<table class="table table-bordered table-striped">--%>
<%--									--%>
<%--			--%>
<%--			--%>

<%--							<tr>--%>
<%--								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_ADD_ROLL, loginDTO)%></b></td>--%>
<%--								<td>--%>
<%--						--%>
<%--											<%--%>
<%--											value = recruitment_bulk_mark_entryDTO.roll + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--								</td>--%>
<%--						--%>
<%--							</tr>--%>

<%--				--%>


<%--			--%>

<%--							<tr>--%>
<%--								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_ADD_MARKS, loginDTO)%></b></td>--%>
<%--								<td>--%>
<%--						--%>
<%--											<%--%>
<%--											value = recruitment_bulk_mark_entryDTO.marks + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--								</td>--%>
<%--						--%>
<%--							</tr>--%>

<%--				--%>


<%--			--%>

<%--							<tr>--%>
<%--								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_ADD_GRADE, loginDTO)%></b></td>--%>
<%--								<td>--%>
<%--						--%>
<%--											<%--%>
<%--											value = recruitment_bulk_mark_entryDTO.grade + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--								</td>--%>
<%--						--%>
<%--							</tr>--%>

<%--				--%>


<%--			--%>
<%--			--%>
<%--			--%>
<%--			--%>
<%--			--%>
<%--			--%>
<%--		--%>
<%--						</table>--%>
<%--                    </div>--%>
<%--			--%>


<%--			</div>	--%>

<%--               --%>


<%--        </div>--%>
<%--	</div>--%>