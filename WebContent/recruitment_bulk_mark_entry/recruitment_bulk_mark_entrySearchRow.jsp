<%@page pageEncoding="UTF-8" %>

<%@page import="recruitment_bulk_mark_entry.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDAO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionRepository" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_RECRUITMENT_BULK_MARK_ENTRY;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Recruitment_bulk_mark_entryDTO recruitment_bulk_mark_entryDTO = (Recruitment_bulk_mark_entryDTO) request.getAttribute("recruitment_bulk_mark_entryDTO");
    CommonDTO commonDTO = recruitment_bulk_mark_entryDTO;
    String servletName = "Recruitment_bulk_mark_entryServlet";

    Recruitment_job_descriptionDAO recruitmentJobDescriptionDAO = new Recruitment_job_descriptionDAO("recruitment_job_description");
    List<Recruitment_job_descriptionDTO> recruitmentJobDescriptionDTOS = recruitmentJobDescriptionDAO.getAllRecruitment_job_description(true);


    System.out.println("recruitment_bulk_mark_entryDTO = " + recruitment_bulk_mark_entryDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Recruitment_bulk_mark_entryDAO recruitment_bulk_mark_entryDAO = (Recruitment_bulk_mark_entryDAO) request.getAttribute("recruitment_bulk_mark_entryDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_jobId'>
    <%
        //													value = + "";
        Recruitment_job_descriptionDTO jobDescriptionDTO = Recruitment_job_descriptionRepository.
                getInstance().getRecruitment_job_descriptionDTOByID(recruitment_bulk_mark_entryDTO.jobId);
        if(jobDescriptionDTO != null){
            value = UtilCharacter.getDataByLanguage(Language, jobDescriptionDTO.jobTitleBn, jobDescriptionDTO.jobTitleEn);
        }
    %>

    <%=value%>


</td>


<td id='<%=i%>_levelId'>
    <%
        value = recruitment_bulk_mark_entryDTO.levelId + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_roll'>
    <%
        value = recruitment_bulk_mark_entryDTO.roll + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_marks'>
    <%
        value = recruitment_bulk_mark_entryDTO.marks + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<!--<td id = '<%=i%>_grade'>
<%
    value = recruitment_bulk_mark_entryDTO.grade + "";
%>

<%=Utils.getDigits(value, Language)%>


</td> -->


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Recruitment_bulk_mark_entryServlet?actionType=view&ID=<%=recruitment_bulk_mark_entryDTO.id%>'"
    >
        <i class="fa fa-eye"></i>
    </button>
</td>

<%--											<td id = '<%=i%>_Edit'>																																	--%>
<%--	--%>
<%--&lt;%&ndash;												<a href='Recruitment_bulk_mark_entryServlet?actionType=getEditPage&ID=<%=recruitment_bulk_mark_entryDTO.id%>'><%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_RECRUITMENT_BULK_MARK_ENTRY_EDIT_BUTTON, loginDTO)%></a>&ndash;%&gt;--%>
<%--												<a href='Recruitment_bulk_mark_entryServlet?actionType=getEditPage&ID=<%=recruitment_bulk_mark_entryDTO.id%>'><i style="color: #ff6a6a" class="fa fa-edit"></i></a>--%>

<%--																				--%>
<%--											</td>--%>

<td id='<%=i%>_Edit'>
    <button
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Recruitment_bulk_mark_entryServlet?actionType=getEditPage&ID=<%=recruitment_bulk_mark_entryDTO.id%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>


<%--											<td id='<%=i%>_checkbox'>--%>
<%--												<div class='checker'>--%>
<%--													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=recruitment_bulk_mark_entryDTO.id%>'/></span>--%>
<%--												</div>--%>
<%--											</td>--%>

<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=recruitment_bulk_mark_entryDTO.id%>'/></span>
    </div>
</td>
																						
											

