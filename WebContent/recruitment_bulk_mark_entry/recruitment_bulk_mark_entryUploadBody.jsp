
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="recruitment_bulk_mark_entry.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%
Recruitment_bulk_mark_entryDTO recruitment_bulk_mark_entryDTO;
recruitment_bulk_mark_entryDTO = (Recruitment_bulk_mark_entryDTO)request.getAttribute("recruitment_bulk_mark_entryDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(recruitment_bulk_mark_entryDTO == null)
{
	recruitment_bulk_mark_entryDTO = new Recruitment_bulk_mark_entryDTO();
	
}
System.out.println("recruitment_bulk_mark_entryDTO = " + recruitment_bulk_mark_entryDTO);

String actionName = "upload";
System.out.println("actionType = " + request.getParameter("actionType"));

String formTitle = LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_UPLOAD_RECRUITMENT_BULK_MARK_ENTRY_UPLOAD_FORMNAME, loginDTO);;



String value = "";

%>


<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data">
			<div class="form-body">
			
					<div class="form-actions text-center">
					<label class="col-lg-3 control-label">
						<%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_UPLOAD_CHOOSE_FILE, loginDTO)%>
					</label>
					<div class="form-group ">					
						<div class="col-lg-6 " id = 'recruitment_bulk_mark_entryDatabase_div'>	
							<input type='file' class='form-control'  name='recruitment_bulk_mark_entryDatabase' id = 'recruitment_bulk_mark_entryDatabase' />	
											
						</div>
						<a class="btn btn-success" onclick = "uploadFile();">
							<%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_UPLOAD_UPLOAD, loginDTO)%>
						</a>
					</div>	
					
					
				</div>
							
			</div>
		
		</form>

	</div>
</div>

<form class="form-horizontal" action="Recruitment_bulk_mark_entryServlet?actionType=uploadConfirmed"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
<div id='reviewDiv'>
</div>

<button class='btn btn-success' type="submit" style="display:none" id="submitButton">
	<%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_UPLOAD_RECRUITMENT_BULK_MARK_ENTRY_SUBMIT_BUTTON, loginDTO)%>
</button>

</form>

<script type="text/javascript">


function uploadFile()
{
	console.log('submitAjax called');

	var formData = new FormData();
	var value;
	
	console.log('uploadFile called');

	formData.append('recruitment_bulk_mark_entryDatabase', document.getElementById('recruitment_bulk_mark_entryDatabase').files[0]);


	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{
			if(this.responseText !='')
			{				
				document.getElementById('reviewDiv').innerHTML = this.responseText ;
				document.getElementById('submitButton').style = "display:inline" ;
				ShowExcelParsingResult(i);
			}
			else
			{
				console.log("No Response");
				document.getElementById('reviewDiv').innerHTML = this.responseText ;
			}
		}
		else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	  };
	xhttp.open("POST", 'Recruitment_bulk_mark_entryServlet?actionType=upload', true);
	xhttp.send(formData);
}



$(document).ready( function(){

    dateTimeInit("<%=Language%>");
});

function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}


	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Recruitment_bulk_mark_entryServlet");	
}

function init(row)
{


	
}


</script>