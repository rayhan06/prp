<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="recruitment_bulk_mark_entry.Recruitment_bulk_mark_entryDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Recruitment_bulk_mark_entryDTO recruitment_bulk_mark_entryDTO = (Recruitment_bulk_mark_entryDTO)request.getAttribute("recruitment_bulk_mark_entryDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(recruitment_bulk_mark_entryDTO == null)
{
	recruitment_bulk_mark_entryDTO = new Recruitment_bulk_mark_entryDTO();
	
}
System.out.println("recruitment_bulk_mark_entryDTO = " + recruitment_bulk_mark_entryDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>

			
<%=("<td id = '" + i + "_id" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='id' id = 'id_hidden_<%=i%>' value='<%=recruitment_bulk_mark_entryDTO.id%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_jobId" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='jobId' id = 'jobId_hidden_<%=i%>' value='<%=recruitment_bulk_mark_entryDTO.jobId%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_levelId" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='levelId' id = 'levelId_hidden_<%=i%>' value='<%=recruitment_bulk_mark_entryDTO.levelId%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_roll'>")%>
			
	
	<div class="form-inline" id = 'roll_div_<%=i%>'>
		<input type='text' class='form-control'  name='roll' id = 'roll_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.roll + "'"):("'" + "0" + "'")%>
   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_marks'>")%>
			
	
	<div class="form-inline" id = 'marks_div_<%=i%>'>
		<input type='text' class='form-control'  name='marks' id = 'marks_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.marks + "'"):("'" + "0" + "'")%>
   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_grade'>")%>
			
	
	<div class="form-inline" id = 'grade_div_<%=i%>'>
		<input type='text' class='form-control'  name='grade' id = 'grade_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.grade + "'"):("'" + "" + "'")%>
   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=recruitment_bulk_mark_entryDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value='<%=recruitment_bulk_mark_entryDTO.insertedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=recruitment_bulk_mark_entryDTO.modifiedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.isDeleted + "'"):("'" + "false" + "'")%>
 tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=recruitment_bulk_mark_entryDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Recruitment_bulk_mark_entryServlet?actionType=view&ID=<%=recruitment_bulk_mark_entryDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Recruitment_bulk_mark_entryServlet?actionType=view&modal=1&ID=<%=recruitment_bulk_mark_entryDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	