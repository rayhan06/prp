
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="recruitment_bulk_mark_entry.Recruitment_bulk_mark_entryDTO"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.text.SimpleDateFormat"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>

<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="job_applicant_application.Job_applicant_applicationDTO" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDAO" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="java.util.List" %>
<%@ page import="geolocation.GeoDistrictDTO" %>
<%@ page import="geolocation.GeoDistrictDAO" %>
<%@ page import="java.util.*" %>
<%@ page import="static sessionmanager.SessionConstants.PassFailStatusMap" %>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_EDIT_LANGUAGE, loginDTO);


String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);

String parentTableName = "job_applicant_application";
Job_applicant_applicationDAO job_applicant_applicationDAO = new Job_applicant_applicationDAO(parentTableName);

//GeoDistrictDAO geoDistrictDAO = new GeoDistrictDAO();
//
//List<GeoDistrictDTO>getAllDistricts =  geoDistrictDAO.getAllDistricts();

%>
<div class="row">
	<div class="col-lg-12">
		<div class="kt-portlet shadow-none" style="margin-top: -20px">

			<div class="kt-portlet__body form-body">


<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.JOB_APPLICANT_DOCUMENTS_NAME, loginDTO)%></th>
								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_FATHERNAME, loginDTO)%></th>
								<th><%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_GEODISTRICTTYPE, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_EDIT_ROLL, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_EDIT_MARKS, loginDTO)%></th>

								<!--<th><%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_EDIT_GRADE, loginDTO)%></th> -->
															
							</tr>
						</thead>
						<tbody>
						<%
								ArrayList data = (ArrayList) session.getAttribute("recruitment_bulk_mark_entryDTOs");

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										List<Job_applicant_applicationDTO> job_applicant_applicationDTOS = job_applicant_applicationDAO.getIDDTOsByJobLevelID(Integer.parseInt(request.getParameter("job_id")),Integer.parseInt(request.getParameter("level_id")));
										for (int i = 0; i < size; i++) 
										{
											System.out.println("In jsp, parsed dto = " + data.get(i));
											Recruitment_bulk_mark_entryDTO recruitment_bulk_mark_entryDTO =  (Recruitment_bulk_mark_entryDTO)data.get(i);
											//Job_applicant_applicationDTO job_applicant_applicationDTO =  job_applicant_applicationDAO.getDTOByJobIdAndRollNumber(Long.parseLong(request.getParameter("job_id")), String.valueOf(Math.round(recruitment_bulk_mark_entryDTO.roll)));
											Job_applicant_applicationDTO job_applicant_applicationDTO =  UtilCharacter.getJobApplicantApplicationDTO(job_applicant_applicationDTOS,String.valueOf(Math.round(recruitment_bulk_mark_entryDTO.roll)));

											long  ID = recruitment_bulk_mark_entryDTO.iD;
											out.println("<tr id = 'tr_" + i + "'>");
						%>

<%=("<td id = '" + i + "_id" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='id' id = 'id_hidden_<%=i%>' value='<%=recruitment_bulk_mark_entryDTO.id%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_jobId" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='jobId' id = 'jobId_hidden_<%=i%>' value='<%=request.getParameter("job_id")%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_levelId" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='levelId' id = 'levelId_hidden_<%=i%>' value='<%=request.getParameter("level_id")%>' tag='pb_html'/>
		
												
<%=("</td>")%>

<%=("<td id = '" + i + "_identity'>")%>


<div class="form-inline" id = 'candidateName_div_<%=i%>'>
	<input type='text' style="width:100%;" class='form-control' id = 'candidateName_text<%=i%>' value=<%=actionName.equals("edit")?("'" +UtilCharacter.getDataByLanguage(Language,job_applicant_applicationDTO.applicant_name_bn,job_applicant_applicationDTO.applicant_name_en)  + "'"):("'" + "0" + "'")%>
			tag='pb_html'/>
</div>

<%=("</td>")%>

<%=("<td id = '" + i + "_fatherName'>")%>


<div class="form-inline" id = 'fatherName_div_<%=i%>'>
	<input type='text' style="width:100%;" class='form-control' id = 'fatherName_text<%=i%>' value=<%=actionName.equals("edit")?("'" +job_applicant_applicationDTO.father_name + "'"):("'" + "0" + "'")%>
			tag='pb_html'/>
</div>

<%=("</td>")%>

<%=("<td id = '" + i + "_district'>")%>

<%--	<%--%>

<%--		if(job_applicant_applicationDTO.district==null){--%>
<%--			job_applicant_applicationDTO.district="-1";--%>
<%--		}--%>
<%--	%>--%>

<div class="form-inline" id = 'district_div_<%=i%>'>
<%--	<input type='text' style="width:100%;" class='form-control' id = 'district_text<%=i%>' value=<%=actionName.equals("edit")?("'" +job_applicant_applicationDAO.JobApplicantDistrict(Language,Integer.parseInt(job_applicant_applicationDTO.district),getAllDistricts)  + "'"):("'" + "No district found" + "'")%>--%>
<%--			tag='pb_html'/>--%>

	<input type='text' style="width:100%;" class='form-control' id = 'district_text<%=i%>' value=<%=actionName.equals("edit")?("'" +UtilCharacter.getDataByLanguage(Language,job_applicant_applicationDTO.district_name_bn,job_applicant_applicationDTO.district_name_en)  + "'"):("'" + "No district found" + "'")%>
			tag='pb_html'/>
</div>

<%=("</td>")%>
			
<%=("<td id = '" + i + "_roll'>")%>
			
	
	<div class="form-inline" id = 'roll_div_<%=i%>'>
<%--		<input type='text' class='form-control'  name='roll' id = 'roll_text_<%=i%>' value=<%=actionName.equals("edit")?("'" +Utils.getDigits(Math.round(recruitment_bulk_mark_entryDTO.roll), Language)+ "'"):("'" + "0" + "'")%>--%>
<%--   tag='pb_html'/>					--%>

	<input type='text' style="width:100%;" class='form-control'  name='roll' id = 'roll_text_<%=i%>' value=<%=actionName.equals("edit")?("'" +Math.round(recruitment_bulk_mark_entryDTO.roll)+ "'"):("'" + "0" + "'")%>
			tag='pb_html'/>

	</div>
				
<%=("</td>")%>

<%if(request.getParameter("level_id").trim().equals("2")){%>
		<%=("<td id = '" + i + "_marks'>")%>


			<div class="form-inline" id = 'marks_div_<%=i%>'>
				<select name="marks" style="width:100%;" class='form-control' id='status_select2_<%=i%>' tag='pb_html'>
					<%
						for (java.util.Map.Entry<Integer,String> entry : PassFailStatusMap.entrySet())    {

							if(entry.getKey() == recruitment_bulk_mark_entryDTO.marks){
								out.println("<option value="+entry.getKey()+" selected>"+LM.getText(entry.getKey()==1?LC.PI_PACKAGE_VENDOR_ITEMS_ADD_PIPACKAGEVENDORCHILDRENID:LC.PI_PACKAGE_VENDOR_ITEMS_EDIT_PIPACKAGEVENDORID,loginDTO)+"</option>");
							}
							else{
								out.println("<option value="+entry.getKey()+" >"+LM.getText(entry.getKey()==1?LC.PI_PACKAGE_VENDOR_ITEMS_ADD_PIPACKAGEVENDORCHILDRENID:LC.PI_PACKAGE_VENDOR_ITEMS_EDIT_PIPACKAGEVENDORID,loginDTO)+"</option>");
							}

						}
					%>

				</select>
			</div>

		<%=("</td>")%>
<%}else{%>
	<%=("<td id = '" + i + "_marks'>")%>


		<div class="form-inline" id = 'marks_div_<%=i%>'>
			<input type='number' style="width:100%;" step='0.01' class='form-control marksClass'  name='marks' id = 'marks_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.marks + "'"):("'" + "0" + "'")%>
					tag='pb_html'/>
		</div>

	<%=("</td>")%>
<%}%>
			

			
<%--<%=("<td id = '" + i + "_grade'>")%>--%>
<%=("<td id = '" + i + "_grade" +  "' style='display:none;'>")%>
			
	
	<div class="form-inline" id = 'grade_div_<%=i%>'>
		<input type='hidden' class='form-control'  name='grade' id = 'grade_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.grade + "'"):("'" + "" + "'")%>
   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=recruitment_bulk_mark_entryDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value='<%=recruitment_bulk_mark_entryDTO.insertedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=recruitment_bulk_mark_entryDTO.modifiedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + recruitment_bulk_mark_entryDTO.isDeleted + "'"):("'" + "false" + "'")%>
 tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=recruitment_bulk_mark_entryDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
							<%					
											out.println("</tr>");
										}
									}
								}
								catch(Exception ex)
								{
									ex.printStackTrace();
								}
						%>
						</tbody>
					</table>
</div>

			</div>
		</div>
	</div>
</div>