﻿<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDAO" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDAO" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="dbm.DBMW" %>
<%@ page import="java.util.Map" %>
<%@ page import="static sessionmanager.SessionConstants.excelUploadChoice" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameRepository" %>


<%


    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);


    Recruitment_job_descriptionDAO jobDescriptionDAO = new Recruitment_job_descriptionDAO();
    Job_applicant_applicationDAO jobApplicantApplicationDAO = new Job_applicant_applicationDAO();

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date todayDate = new Date();

    int pagination_number = 0;
    RecordNavigator rn = new RecordNavigator();
    request.getSession().setAttribute(SessionConstants.NAV_JOB_APPLICANT_APPLICATION, rn);
    String context = "../../.." + request.getContextPath() + "/";

    String actionName;
    boolean isPermanentTable = true;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else if (request.getParameter("actionType").equalsIgnoreCase("getCandidateInfoExcelList")) {
        actionName = "DownloadTemplate";
    } else {
        actionName = "edit";
    }
    long ColumnID;
    int i = 0;

%>

<!--new layout start -->


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important; margin-bottom: -18px">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_UPLOAD_UPLOAD, loginDTO)%>
                </h3>
            </div>
        </div>
        <form id="candidate-form" name="candidate-form" method="POST" enctype="multipart/form-data"
              action="Recruitment_bulk_mark_entryServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_UPLOAD_UPLOAD, loginDTO)%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row" style="display: none;">


                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.CANDIDATE_LIST_STATUS, loginDTO)%>
                                        </label>

                                        <div class="col-md-9">

                                            <select onchange="statusChange()" class='form-control' name='status'
                                                    id='status' tag='pb_html'>

                                                <%=jobDescriptionDAO.getStatusList(Language)%>
                                            </select>

                                        </div>


                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.RECRUITMENT_TEST_NAME_ADD_RECRUITMENT_TEST_NAME_ADD_FORMNAME, loginDTO)%><span
                                                class="required">*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='recruitmentTestNameId'
                                                    id='recruitmentTestNameId'
                                                    onchange="onRecruitmentTestNameChange(this)" tag='pb_html'>
                                                <%= Recruitment_test_nameRepository.getInstance().buildOptions(Language, -1L) %>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">

                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.CANDIDATE_LIST_POST_NAME, loginDTO)%>
                                        </label>

                                        <div class="col-md-9" id='job_div'>

                                            <select onchange="onJobChange()" class='form-control' name='job'
                                                    id='job' tag='pb_html'>

                                                <%=jobDescriptionDAO.getJobListWithoutInternship("ALL", Language)%>
                                            </select>

                                        </div>


                                    </div>
                                    <div class="form-group row">

                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.CANDIDATE_LIST_LEVEL, loginDTO)%>
                                        </label>

                                        <div class="col-md-9">

                                            <select onchange="onLevelChange()" class='form-control' name='level'
                                                    id='level' tag='pb_html'>


                                            </select>

                                        </div>


                                    </div>
                                    <div class="committee-member mb-5" id="committee-member"></div>
                                    <div class="form-group row" id="excelUploadChoice" style="display: none;">

                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.CANDIDATE_LIST_EXCEL_UPLOAD_DECISION, loginDTO)%>
                                        </label>

                                        <div class="col-md-9">

                                            <select onchange="exlUpChoiceClick(this)" class='form-control'
                                                    name='exlUpChoice' id='exlUpChoice' tag='pb_html'>

                                                <%
                                                    String exlChoiceDropDown = "";
                                                    if (Language.equals("English")) {
                                                        exlChoiceDropDown = "<option value = ''>Select</option>";
                                                    } else {
                                                        exlChoiceDropDown = "<option value = ''>অনুগ্রহ করে নির্বাচন করুন</option>";
                                                    }
                                                    for (Map.Entry<Integer, String> entry : excelUploadChoice.entrySet()) {
                                                        String text = entry.getKey() == 1 ? LM.getText(LC.CANDIDATE_LIST_YES, loginDTO) : LM.getText(LC.CANDIDATE_LIST_NO, loginDTO);
                                                        exlChoiceDropDown = exlChoiceDropDown + "<option value=" + entry.getKey() + ">" + text + "</option>";
                                                    }
                                                %>
                                                <%=exlChoiceDropDown%>
                                            </select>
                                        </div>
                                    </div>

                                    <input type="hidden" id="job_name" name="job_name" value=""/>
                                    <input type="hidden" id="level_name" name="level_name" value=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-3">
                    <div class="row">
                        <div class="col-md-11">
                            <div class="form-actions text-right">
                                <button type="submit"
                                        class="btn btn-sm btn-success text-white shadow  btn-border-radius"
                                        id="download_excel" name="download_excel" style=""><i
                                        class="fas fa-file-excel"></i><%=LM.getText(LC.CANDIDATE_LIST_DOWNLOAD_EXCEL, loginDTO) %>
                                </button>
                                <%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>

                                <button type="button"
                                        class="btn btn-sm submit-btn text-white shadow ml-2  btn-border-radius"
                                        style="color: white;" id="loadFileXml"
                                        onclick="document.getElementById('testing_excelDatabase').click();"><i
                                        class="fas fa-cloud-upload-alt"></i><%=LM.getText(LC.CANDIDATE_LIST_UPLOAD_EXCEL, loginDTO) %>
                                </button>
                                <button type="button" class="btn btn-sm btn-danger text-white shadow btn-border-radius"
                                        style="color: white;text-align:center;display: inline-block;"
                                        id="load_data" name="load_data" onclick="loadDataClicked()"><i
                                        class="fas fa-spinner"></i><%=LM.getText(LC.CANDIDATE_LIST_LOAD_DATA, loginDTO) %>
                                </button>

                                <button type="button" class="btn btn-sm btn-primary text-white shadow btn-border-radius"
                                        style="color: white;text-align:center;display: none;"
                                        id="send_otp" name="send_otp" onclick="sendOTPClicked()"><i
                                        class="fas fa-paper-plane"></i><%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_UPLOAD_SEND_OTP, loginDTO) %>
                                </button>

                                <button type="button" class="btn btn-sm btn-success text-white shadow btn-border-radius"
                                        style="color: white;text-align:center;display: none;"
                                        id="submit_otp" name="submit_otp" onclick="submitOTPClicked()"><i
                                        class="fas fa-save"></i><%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_UPLOAD_SUBMIT_OTP, loginDTO) %>
                                </button>


                            </div>
                        </div>
                    </div>
                    <div class="dropzonesss" style="display: inline-block;margin-left: 40%;margin-top: 10px;">
                        <input
                                type='file' style="display:none;"
                                class='form-control'
                                name='testing_excelDatabase'
                                id='testing_excelDatabase'
                        />

                    </div>
                </div>
            </div>
        </form>
        <%--                <form class="form-horizontal" action="Recruitment_bulk_mark_entryServlet?actionType=uploadConfirmed"--%>
        <%--                      id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"--%>
        <%--                      onsubmit="return PreprocessBeforeSubmiting(0,'upload')" >--%>
        <form class="form-horizontal" action="Recruitment_bulk_mark_entryServlet?actionType=uploadConfirmed"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="formSubmit()">
            <div id='reviewDiv'>
            </div>

            <div class="my-5">

                <div class="form-actions text-center">
                    <button class='btn btn-sm submit-btn text-white shadow ml-2 btn btn-sm submit-btn text-white shadow ml-2 '
                            type="submit" style="display:none;border-radius: 8px" id="submitButton">
                        <i class="fas fa-cloud-upload-alt"></i>
                        <%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_UPLOAD_UPLOAD, loginDTO)%>
                    </button>

                </div>
            </div>


        </form>


    </div>
</div>


<!--new layout end-->

<%--<div class="box box-primary">--%>
<%--    <div class="box-header with-border">--%>
<%--        <h3 class="box-title"><i class="fa fa-gift"></i>--%>
<%--            <%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_UPLOAD_UPLOAD, loginDTO)%>--%>
<%--        </h3>--%>

<%--    </div>--%>

<%--    <div class="box-body">--%>
<%--        <form id = "candidate-form" name = "candidate-form" method="POST" enctype = "multipart/form-data" action="Recruitment_bulk_mark_entryServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>" >--%>
<%--            <div class="form-body">--%>
<%--                <div class="form-row">--%>


<%--                    <div class="form-group col-md-4">--%>
<%--                        <label class="col-lg-3 col-form-label" style="font-size: 12px; font-weight: bold">--%>
<%--                            &lt;%&ndash;                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%>&ndash;%&gt;--%>
<%--                                <%=LM.getText(LC.CANDIDATE_LIST_STATUS, loginDTO)%>--%>
<%--                            &lt;%&ndash;                        <span class="required"> * </span>&ndash;%&gt;--%>
<%--                        </label>--%>
<%--                        <div class="form-group ">--%>
<%--                            <div class="col-lg-9 " >--%>
<%--                                <select onchange="statusChange()" class='form-control'  name='status' id = 'status'   tag='pb_html'>--%>

<%--                                    <%=jobDescriptionDAO.getStatusList(Language)%>--%>
<%--                                </select>--%>

<%--                            </div>--%>
<%--                        </div>--%>


<%--                    </div>--%>


<%--                    <div class="form-group col-md-4">--%>
<%--                        <label class="col-lg-3 col-form-label" style="font-size: 12px; font-weight: bold">--%>
<%--                            &lt;%&ndash;                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%>&ndash;%&gt;--%>
<%--                                <%=LM.getText(LC.CANDIDATE_LIST_POST_NAME, loginDTO)%>--%>
<%--                            &lt;%&ndash;                        <span class="required"> * </span>&ndash;%&gt;--%>
<%--                        </label>--%>
<%--                        <div class="form-group ">--%>
<%--                            <div class="col-lg-9 " id = 'job_div'>--%>
<%--                                <select onchange="onJobChange()" class='form-control'  name='job' id = 'job'   tag='pb_html'>--%>

<%--                                    <%=jobDescriptionDAO.getJobListWithoutInternship( "ALL", Language)%>--%>
<%--                                </select>--%>

<%--                            </div>--%>
<%--                        </div>--%>


<%--                    </div>--%>


<%--                    <div class="form-group col-md-4">--%>
<%--                        <label class="col-lg-3 col-form-label" style="font-size: 12px; font-weight: bold">--%>
<%--                            &lt;%&ndash;                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%>&ndash;%&gt;--%>
<%--                                <%=LM.getText(LC.CANDIDATE_LIST_LEVEL, loginDTO)%>--%>
<%--                            &lt;%&ndash;                        <span class="required"> * </span>&ndash;%&gt;--%>
<%--                        </label>--%>
<%--                        <div class="form-group ">--%>
<%--                            <div class="col-lg-9 " >--%>
<%--                                <select onchange="onLevelChange()" class='form-control'  name='level' id = 'level'   tag='pb_html'>--%>


<%--                                </select>--%>

<%--                            </div>--%>
<%--                        </div>--%>


<%--                    </div>--%>

<%--                    <div class="form-group col-md-4" id="excelUploadChoice" >--%>
<%--                        <label class="col-lg-3 col-form-label" style="font-size: 12px; font-weight: bold">--%>
<%--                            &lt;%&ndash;                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%>&ndash;%&gt;--%>
<%--                            <%=LM.getText(LC.CANDIDATE_LIST_EXCEL_UPLOAD_DECISION, loginDTO)%>--%>
<%--                            &lt;%&ndash;                        <span class="required"> * </span>&ndash;%&gt;--%>
<%--                        </label>--%>
<%--                        <div class="form-group " >--%>
<%--                            <div class="col-lg-9 " >--%>
<%--                                <select onchange="exlUpChoiceClick(this)" class='form-control'  name='exlUpChoice' id = 'exlUpChoice'  tag='pb_html'>--%>


<%--                                    <%--%>
<%--                                        String exlChoiceDropDown = "";--%>
<%--                                        if (Language.equals("English")) {--%>
<%--                                            exlChoiceDropDown = "<option value = ''>Select</option>";--%>
<%--                                        } else {--%>
<%--                                            exlChoiceDropDown = "<option value = ''>অনুগ্রহ করে নির্বাচন করুন</option>";--%>
<%--                                        }--%>
<%--                                        for (Map.Entry<Integer, String> entry : excelUploadChoice.entrySet()) {--%>
<%--                                            String text = entry.getKey()==1?LM.getText(LC.CANDIDATE_LIST_YES,loginDTO):LM.getText(LC.CANDIDATE_LIST_NO,loginDTO);--%>
<%--                                            exlChoiceDropDown=exlChoiceDropDown+"<option value="+entry.getKey()+">"+text+"</option>";--%>
<%--                                        }--%>
<%--                                    %>--%>
<%--                                    <%=exlChoiceDropDown%>--%>
<%--                                </select>--%>

<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>


<%--                </div>--%>

<%--                <!--<div class="form-row">--%>
<%--                    <div class="form-group col-md-4">--%>
<%--                        <label class="col-lg-3 col-form-label" style="font-size: 12px; font-weight: bold">--%>
<%--                            &lt;%&ndash;                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%>&ndash;%&gt;--%>
<%--                                <%=LM.getText(LC.CANDIDATE_LIST_VIEW_STATUS, loginDTO)%>--%>
<%--                            &lt;%&ndash;                        <span class="required"> * </span>&ndash;%&gt;--%>
<%--                        </label>--%>
<%--                        <div class="form-group ">--%>
<%--                            <div class="col-lg-9 " id = ''>--%>
<%--                                <select onchange="" class='form-control'  name='view' id = 'view'   tag='pb_html'>--%>

<%--                                    <%=jobApplicantApplicationDAO.getViewList(Language)%>--%>
<%--                                </select>--%>

<%--                            </div>--%>
<%--                        </div>--%>


<%--                    </div>--%>
<%--                    <div class="form-group col-md-4">--%>
<%--                        <label class="col-lg-3 col-form-label" style="font-size: 12px; font-weight: bold">--%>
<%--                            &lt;%&ndash;                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%>&ndash;%&gt;--%>
<%--                                <%=LM.getText(LC.CANDIDATE_LIST_APPROVE_STATUS, loginDTO)%>--%>
<%--                            &lt;%&ndash;                        <span class="required"> * </span>&ndash;%&gt;--%>
<%--                        </label>--%>
<%--                        <div class="form-group ">--%>
<%--                            <div class="col-lg-9 " >--%>
<%--                                <select onchange="" class='form-control'  name='approve' id = 'approve'   tag='pb_html'>--%>

<%--                                    <%=jobApplicantApplicationDAO.getApproveList(Language)%>--%>
<%--                                </select>--%>

<%--                            </div>--%>
<%--                        </div>--%>


<%--                    </div>--%>
<%--                </div> -->--%>
<%--                </br></br>--%>

<%--                <input type="hidden" id="job_name" name="job_name" value="" />--%>
<%--                <input type="hidden" id="level_name" name="level_name" value="" />--%>

<%--                <center><input type="submit" class="btn-info" style="color: white;text-align:center;display: inline-block;float: center;" value="<%=LM.getText(LC.CANDIDATE_LIST_DOWNLOAD_EXCEL, loginDTO) %>" id="download_excel" name="download_excel">--%>

<%--                    <%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>--%>
<%--                    <div class="dropzonesss" style="display: inline-block;float: center;">--%>

<%--                        <input type='file' style="display:none" class='form-control'  name='testing_excelDatabase' id = 'testing_excelDatabase' />--%>
<%--                        <input type="button" class="btn-success" style="color: white;" id="loadFileXml" value="<%=LM.getText(LC.CANDIDATE_LIST_UPLOAD_EXCEL, loginDTO) %>"  onclick="document.getElementById('testing_excelDatabase').click();" />--%>


<%--                    </div>--%>
<%--                    <input type="button" class="btn-danger" style="color: white;text-align:center;display: inline-block;float: center;" value="<%=LM.getText(LC.CANDIDATE_LIST_LOAD_DATA, loginDTO) %>" id="load_data" name="load_data" onclick="loadDataClicked()">--%>
<%--   </center>--%>


<%--            </div>--%>
<%--        </form>--%>


<%--        <br>--%>

<%--        <div id = 'total-item-count-div' style="display: none">--%>

<%--            <%=LM.getText(LC.CANDIDATE_LIST_TOTAL, loginDTO)%>: <label id="total-count"></label>,--%>
<%--            <%=LM.getText(LC.CANDIDATE_LIST_VIEWED, loginDTO)%>: <label id="view-count"></label>,--%>
<%--            <%=LM.getText(LC.CANDIDATE_LIST_SHORTLISTED, loginDTO)%>: <label id="shortlisted-count"></label>,--%>
<%--            <%=LM.getText(LC.CANDIDATE_LIST_REJECTED, loginDTO)%>: <label id="rejected-count"></label>--%>

<%--        </div>--%>

<%--        <div id = 'data-div-parent' style="display: none">--%>
<%--            <div id = 'data-div' >--%>

<%--            </div>--%>
<%--            <% pagination_number = 1;%>--%>
<%--            <%@include file="../common/pagination_with_go2.jsp"%>--%>

<%--        </div>--%>


<%--    </div>--%>
<%--</div>--%>

<%--<form class="form-horizontal" action="Recruitment_bulk_mark_entryServlet?actionType=uploadConfirmed"--%>
<%--      id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"--%>
<%--      onsubmit="return PreprocessBeforeSubmiting(0,'upload')">--%>
<%--    <div id='reviewDiv'>--%>
<%--    </div>--%>

<%--    <button class='btn btn-success' type="submit" style="display:none" id="submitButton">--%>
<%--        <%=LM.getText(LC.RECRUITMENT_BULK_MARK_ENTRY_UPLOAD_UPLOAD, loginDTO)%>--%>
<%--    </button>--%>
<%--    --%>

<%--</form>--%>

<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">

    let language = '<%=Language%>';
    var downloadTimer;


    $(document).ready(function () {


        $('#testing_excelDatabase').on("change", function () {
            uploadFile();
        });
        select2SingleSelector('#recruitmentTestNameId', '<%=Language%>');
        select2SingleSelector('#job', '<%=Language%>');
        select2SingleSelector('#level', '<%=Language%>');
        select2SingleSelector('#exlUpChoice', '<%=Language%>');
    });

    $(document).ready(function () {

        CheckRequiredFields();

    });

    function formSubmit() {
        event.preventDefault();
        let form = $("#bigform");
        form.validate();
        let valid = form.valid();
        let language = '<%=Language%>';

        if (valid) {
            let jobId = $("#job").val();
            let order = $("#level").val();
            let url = "JobSpecificExamTypeServlet?actionType=getMark&jobId="
                + jobId + "&order=" + order;
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (marks) {
                    let elements = document.getElementsByClassName("marksClass");
                    let markFlag = 1;

                    Array.from(elements).forEach((el) => {

                        if (parseFloat(el.value) > parseFloat(marks) && parseFloat(marks) > 0) {
                            markFlag = 0;
                            toastr.error("Can not upload mark for exceeding examination total number");

                        }
                    });
                    if (markFlag == 1) {
                        formSubmitAfterValidation();
                    }

                },
                error: function (error) {
                    console.log(error);
                }
            });

        }
    }

    function formSubmitAfterValidation() {
        document.forms.bigform.submit();
    }

    function ReSendOTP() {
        $("#send_otp").text("Re-send OTP");
        document.getElementById('send_otp').style = "display:inline-block";
        document.getElementById('submit_otp').style = "display:none";
        // let length = document.getElementById("committeelength").innerText;
        //

    }

    function sendOTPClicked() {
        let committeeId = document.getElementById("committeeId").innerText;
        let length = document.getElementById("committeelength").innerText;
        //downloadTimer();

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('send_otp').style = "display:none";
                toastr.success("Sent OTP to corresponding phone");


                for (let i = 0; i < length; i++) {
                    let otp_name = "otp_" + (i + 1);
                    let otp_label = "otp_label_" + (i + 1);
                    document.getElementById(otp_label).style = "display:inline-block";
                    document.getElementById(otp_name).style = "display:inline-block";
                }

                clearInterval(downloadTimer);
                timer(300);

                document.getElementById('submit_otp').style = "display:inline-block";
            } else {
                document.getElementById('send_otp').style = "display:inline-block";
                //toastr.error("Can not send OTP");
                //document.getElementById('progressBar').innerHTML = "";
                document.getElementById('submit_otp').style = "display:none";
            }
        };
        xhttp.open("POST", "Recruitment_marks_committeeServlet?actionType=sendOTP&committeeId=" + committeeId, true);
        xhttp.send();

    }

    function showButtons() {
        //document.getElementById("progressBar").innerHTML = "";
        document.getElementById('submit_otp').style = "display:none";
        document.getElementById('committee-member').innerHTML = "";
        document.getElementById('submit_otp').style = "display:none";
        document.getElementById('excelUploadChoice').style = "display:flex";
        CheckRequiredFields();

    }

    function submitOTPClicked() {

        clearInterval(downloadTimer);
        document.getElementById("progressBar").innerHTML = "";

        let params = "Recruitment_marks_committeeServlet?actionType=checkOTP";
        let member_id_param = "";// params + "&language=" + language + "&status=" + statusValue;
        let otp_param = "";
        //xhttp.open("Get", params, true);
        //xhttp.send();


        let committeeId = document.getElementById("committeeId").innerText;
        let length = document.getElementById("committeelength").innerText;

        for (let i = 0; i < length; i++) {
            let otp = "otp_" + (i + 1);
            let member_id = "member_id_" + (i + 1);

            let member_id_val = document.getElementById(member_id).value;
            let otp_val = document.getElementById(otp).value;

            member_id_param = member_id_param + "&" + member_id + "=" + member_id_val;
            otp_param = otp_param + "&" + otp + "=" + otp_val;

        }

        params = params + member_id_param + otp_param + "&length=" + length;

        //console.log(' param val  submitOTPClicked: '+params);


        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('send_otp').style = "display:none";
                //document.getElementById('submit_otp').style = "display:none" ;
                //toastr.success("OTP verified successfully");


                let json_obj = $.parseJSON(this.responseText);

                if (json_obj.status) {
                    document.getElementById('submit_otp').style = "display:none";
                    document.getElementById('committee-member').innerHTML = "";
                    document.getElementById('submit_otp').style = "display:none";
                    document.getElementById('excelUploadChoice').style = "display:flex";
                    toastr.success("OTP verified successfully");
                    CheckRequiredFields();
                } else {
                    document.getElementById('submit_otp').style = "display:none";
                    document.getElementById('excelUploadChoice').style = "display:none";
                    //document.getElementById('progressBar').style = "display:none";
                    toastr.error("Cannot verify OTP");

                    for (let i = 0; i < length; i++) {
                        let otp_name = "otp_" + (i + 1);
                        $("#" + otp_name).val("");

                    }
                    //$('#progressBar').val("");

                    ReSendOTP();
                }


            } else {
                //document.getElementById('send_otp').style = "display:none" ;
                //document.getElementById('submit_otp').style = "display:none" ;
            }
        };
        xhttp.open("POST", params, true);
        xhttp.send();

    }


    function loadDataClicked() {
        var job = document.getElementById("job").value;
        var level = document.getElementById("level").value;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                toastr.success("Success");
                document.getElementById('reviewDiv').innerHTML = this.responseText;
                document.getElementById('submitButton').style = "display:inline";
                ShowExcelParsingResult(i);
            } else {
                //toastr.error("Failure");
            }
        };
        xhttp.open("POST", "Recruitment_bulk_mark_entryServlet?actionType=excelReviewWithoutUpload&job_id=" + job + "&level_id=" + level, true);
        xhttp.send();

    }


    function exlUpChoiceClick(nameSelect) {
        //var job = document.getElementById("job").value;
        //var level = document.getElementById("level").value;
        CheckRequiredFields();
    }

    function CheckRequiredFields() {
        //var status = document.getElementById("status").value;
        let job = document.getElementById("job").value;
        let level = document.getElementById("level").value;

        let excelUpChoice = document.getElementById("exlUpChoice").value;
        document.getElementById('reviewDiv').innerHTML = "";
        document.getElementById('submitButton').style = "display:none";

        if ( job == "" || level == "" || excelUpChoice == "" || $('#exlUpChoice').is(':hidden')) {
            $("#loadFileXml").hide();
            $("#download_excel").hide();
            $("#load_data").hide();
        } else {

            if (excelUpChoice == 1 || excelUpChoice == 2) {

                $("#loadFileXml").hide();
                $("#download_excel").hide();
                $("#load_data").hide();
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {

                        var response = this.responseText.split(",");
                        var download_allowed = response[0];
                        var upload_allowed = response[1];

                        if (download_allowed == 0 && excelUpChoice == 1) {
                            $("#load_data").hide();
                            $("#download_excel").show();
                            var job_name = $("#job>option:selected").text();
                            var level_name = $("#level>option:selected").text();
                            document.getElementById("job_name").value = job_name;
                            document.getElementById("level_name").value = level_name;

                            if (upload_allowed == 1 && excelUpChoice == 1) {
                                toastr.success("Data found");
                                $("#loadFileXml").show();
                            } else {
                                toastr.error("Already uploaded");
                                $("#loadFileXml").hide();
                            }

                        } else if (download_allowed == 0 && upload_allowed == 1 && excelUpChoice == 2) {
                            toastr.success("Data found");
                            $("#loadFileXml").hide();
                            $("#download_excel").hide();
                            $("#load_data").show();
                        } else {
                            toastr.error("No data found");
                        }
                    }
                };
                xhttp.open("POST", "Recruitment_bulk_mark_entryServlet?actionType=checkJobLevelAttExistence&job_id=" + job + "&level_id=" + level, true);
                xhttp.send();

            } else {
                $("#loadFileXml").hide();
                $("#download_excel").hide();
                $("#load_data").hide();
                toastr.error("No data found");
            }
        }
    }

    function CheckDataAvailability() {
        //let status = document.getElementById("status").value;
        let job = document.getElementById("job").value;
        let level = document.getElementById("level").value;
        let returnVal = false;

        if (job !== "" && level !== "") {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {

                    var response = this.responseText.split(",");
                    var download_allowed = response[0];
                    var upload_allowed = response[1];
                    if (download_allowed == 0 && upload_allowed == 1) {
                        returnVal = true;
                    } else {
                        returnVal = false;
                    }


                }
            };
            xhttp.open("POST", "Recruitment_bulk_mark_entryServlet?actionType=checkJobLevelAttExistence&job_id=" + job + "&level_id=" + level, false);
            xhttp.send();
        } else {
            returnVal = false;
        }
        return returnVal
    }

    function uploadFile() {
        console.log('submitAjax called');

        var formData = new FormData();
        var value;
        var job_id = document.getElementById('job').value;
        var level_id = document.getElementById('level').value;
        //console.log("job_id: "+job_id);

        console.log('uploadFile called');

        formData.append('testing_excelDatabase', document.getElementById('testing_excelDatabase').files[0]);


        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText != '') {
                    document.getElementById('reviewDiv').innerHTML = this.responseText;
                    document.getElementById('submitButton').style = "display:inline";
                    ShowExcelParsingResult(i);
                } else {
                    console.log("No Response");
                    document.getElementById('reviewDiv').innerHTML = this.responseText;
                }
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("POST", 'Recruitment_bulk_mark_entryServlet?actionType=upload&job_id=' + job_id + '&level_id=' + level_id, true);
        xhttp.send(formData);
    }

    function convertToBn(value) {
        if (language == 'English') {
            return value;
        }

        return toBn(value);
    }

    function showItemCountDiv(total, view, select, reject) {
        document.getElementById('total-count').innerText = convertToBn(total);
        document.getElementById('view-count').innerText = convertToBn(view);
        document.getElementById('shortlisted-count').innerText = convertToBn(select);
        document.getElementById('rejected-count').innerText = convertToBn(reject);
        document.getElementById('total-item-count-div').style.display = 'block';
    }

    function hideItemCountDiv() {
        document.getElementById('total-item-count-div').style.display = 'none';
    }

    function statusChange() {
        hideLoadButton();
        //CheckRequiredFields();
        let statusValue = document.getElementById('status').value;
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('job').innerHTML = this.responseText;
                CheckRequiredFields();
            } else if (this.readyState == 4 && this.status != 200) {
                CheckRequiredFields();
            }
        };

        let params = "Recruitment_job_descriptionServlet?actionType=getJobListByStatusWithoutInternship";
        params = params + "&language=" + language + "&status=" + statusValue;
        xhttp.open("Get", params, true);
        xhttp.send();
    }

    function showLoadButton() {
        //document.getElementById('submit-button-div').style.display = 'block';
    }

    function hideLoadButton() {
        //document.getElementById('submit-button-div').style.display = 'none';
    }

    function showDataDiv() {
        //document.getElementById('data-div-parent').style.display = 'block';
    }

    function hideDataDiv() {
        //document.getElementById('data-div-parent').style.display = 'none';
    }


    function timer(timeleft) {

        downloadTimer = setInterval(function () {
            if (timeleft <= 0) {
                clearInterval(downloadTimer);
                document.getElementById("progressBar").innerHTML = "OTP Expired";
                ReSendOTP();
            } else {

                let minutes = Math.floor(timeleft / 60);
                let seconds = Math.floor(timeleft % 60);
                document.getElementById("progressBar").innerHTML = "OTP expires in " + minutes + " min " + seconds + " seconds";
            }
            timeleft -= 1;
        }, 1000);

    }


    function onJobChange() {
        let jobValue = document.getElementById('job').value;
        document.getElementById('committee-member').innerHTML = "";
        document.getElementById('send_otp').style = "display:none";
        document.getElementById('submit_otp').style = "display:none";
        document.getElementById('excelUploadChoice').style = "display:none";

        //console.log("jobValue: "+jobValue);
        CheckRequiredFields();

        // $("#loadFileXml").hide();
        // $("#download_excel").hide();
        // $("#load_data").hide();
        if (jobValue && jobValue.length > 0) {
            showLoadButton();
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById('level').innerHTML = this.responseText;
                    CheckRequiredFields();

                    /*admit card existence start*/

                    if (CheckDataAvailability()) {
                        let admitCardXhttp = new XMLHttpRequest();
                        admitCardXhttp.onreadystatechange = function () {
                            if (this.readyState == 4 && this.status == 200) {
                                if (this.responseText == "true") {
                                    showButtons();
                                    toastr.success('Admit card exists');
                                    /*comitee member start*/

                                    <%--let committeeMemberXhttp = new XMLHttpRequest();--%>
                                    <%--committeeMemberXhttp.onreadystatechange = function () {--%>
                                    <%--    if (this.readyState == 4 && this.status == 200) {--%>
                                    <%--        //console.log('committee response: '+this.responseText+' jobID: '+jobValue);--%>
                                    <%--        let json_obj = $.parseJSON(this.responseText);--%>


                                    <%--        if (json_obj.status) {--%>

                                    <%--            toastr.success('Committee member found');--%>

                                    <%--            let output = '';--%>
                                    <%--            output += '<div style="display: none" id="committeeId">';--%>
                                    <%--            output += json_obj.committeeId;--%>
                                    <%--            output += '</div>';--%>

                                    <%--            output += '<div style="display: none" id="committeelength">';--%>
                                    <%--            output += json_obj.marks_committee_membersDTOS.length;--%>
                                    <%--            output += '</div>';--%>

                                    <%--            let j = 0;--%>
                                    <%--            for (let i = 0; i < json_obj.marks_committee_membersDTOS.length; i++) {--%>
                                    <%--                j = i + 1;--%>
                                    <%--                let otp_name = "otp_" + j;--%>
                                    <%--                let otp_label = "otp_label_" + j;--%>
                                    <%--                let member_id = "member_id_" + j;--%>
                                    <%--                let member = json_obj.marks_committee_membersDTOS[i];--%>
                                    <%--                output += '<div class="form-group row">';--%>
                                    <%--                output += '<label class="col-md-3 col-form-label text-md-right">';--%>

                                    <%--                output += '<%=LM.getText(LC.COMMITTEE_MEMBER_SEARCH_ANYFIELD, loginDTO)%>';--%>

                                    <%--                output += '</label>';--%>

                                    <%--                output += '<div class="col-sm-5">';--%>

                                    <%--                output += '<input type="text" class="form-control"   value="';--%>
                                    <%--                output += language == 'English' ? member.employeeRecordName : member.employeeRecordNameBn;--%>
                                    <%--                output += ", "--%>
                                    <%--                output += language == 'English' ? member.unitName : member.unitNameBn;--%>
                                    <%--                output += ", "--%>
                                    <%--                output += language == 'English' ? member.postName : member.postNameBn;--%>
                                    <%--                output += '" tag="pb_html" readonly/>';--%>

                                    <%--                output += '</div>';--%>

                                    <%--                output += '<label class="col-sm-2 col-form-label text-md-right" id="';--%>

                                    <%--                output += otp_label;--%>
                                    <%--                output += '" style="display: none;">';--%>

                                    <%--                output += '<%=LM.getText(LC.GLOBAL_OTP_VERIFICATION, loginDTO)%>';--%>

                                    <%--                output += '</label>';--%>
                                    <%--                output += '<div class="col-sm-2">';--%>

                                    <%--                output += '<input type="text" class="form-control" name="';--%>
                                    <%--                output += otp_name + '" id="';--%>
                                    <%--                output += otp_name + '"';--%>
                                    <%--                output += ' tag="pb_html" style="display:none;"/>';--%>

                                    <%--                output += '</div>';--%>


                                    <%--                output += '</div>';--%>

                                    <%--                output += '<input type="text" class="form-control" name="';--%>
                                    <%--                output += member_id + '" id="';--%>
                                    <%--                output += member_id + '" value="';--%>
                                    <%--                output += member.iD;--%>
                                    <%--                output += '" tag="pb_html" style="display:none;"/>';--%>

                                    <%--                output += '<div style="position: absolute; bottom: 8px; right: 1%; text-align:right; color: red; font-family: Roboto, SolaimanLipi; font-size: 1.1rem" id="progressBar"> </div>';--%>
                                    <%--            }--%>
                                    <%--            $('.committee-member').html(output);--%>
                                    <%--            document.getElementById('send_otp').style = "display:inline";--%>

                                    <%--        } else {--%>
                                    <%--            toastr.error('No committee member found');--%>
                                    <%--        }--%>


                                    <%--    } else if (this.readyState == 4 && this.status != 200) {--%>
                                    <%--    }--%>
                                    <%--};--%>

                                    <%--let CommitteeParams = "Recruitment_job_descriptionServlet?actionType=getMarksCommittee&jobId=" + jobValue;--%>
                                    <%--committeeMemberXhttp.open("Get", CommitteeParams, true);--%>
                                    <%--committeeMemberXhttp.send();--%>


                                    /*comitee member end*/
                                } else {
                                    toastr.error('Admit card issue');
                                }
                            } else if (this.readyState == 4 && this.status != 200) {
                                //console.log('admit exits: '+this.responseText);
                            }
                        };

                        let params = "Admit_cardServlet?actionType=admitCardExists";
                        params = params + "&jobId=" + jobValue;
                        //console.log("params: " + params);
                        admitCardXhttp.open("Get", params, true);
                        admitCardXhttp.send();
                    } else {
                        //toastr.error('Already uploaded');
                        if(document.getElementById('level').value!==""){
                            toastr.error('Already uploaded');
                        }
                        else{
                            toastr.error('No exam level found');
                        }
                    }


                    /*admit card existence end*/


                } else if (this.readyState == 4 && this.status != 200) {
                    CheckRequiredFields();
                }
            };

            let params = "Recruitment_job_descriptionServlet?actionType=getExamTypesByJobIdForMarksEntry";
            params = params + "&language=" + language + "&jobId=" + jobValue;

            //console.log("params: "+params);
            //let params = "CANDIDATE_LIST_LEVEL?actionType=getExamTypesByJobId";
            //params = params + "&language=" + language + "&jobId=" + jobValue;
            xhttp.open("Get", params, true);
            xhttp.send();

        } else {
            hideLoadButton();
            document.getElementById('level').innerHTML = '';
        }
    }

    function onLevelChange(){
        let job_id = document.getElementById('job').value ;
        let level_id = document.getElementById('level').value ;

        document.getElementById('committee-member').innerHTML = "";
        document.getElementById('send_otp').style = "display:none";
        document.getElementById('submit_otp').style = "display:none";
        document.getElementById('excelUploadChoice').style = "display:none";

        CheckRequiredFields();
        // //console.log("jobValue: "+jobValue);
        if(job_id && job_id.length > 0){

            if (CheckDataAvailability()) {
                let admitCardXhttp = new XMLHttpRequest();
                admitCardXhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        if (this.responseText == "true") {
                            showButtons();
                            toastr.success('Admit card exists');
                            /*comitee member start*/

                            <%--let committeeMemberXhttp = new XMLHttpRequest();--%>
                            <%--committeeMemberXhttp.onreadystatechange = function () {--%>
                            <%--    if (this.readyState == 4 && this.status == 200) {--%>
                            <%--        //console.log('committee response: '+this.responseText+' jobID: '+jobValue);--%>
                            <%--        let json_obj = $.parseJSON(this.responseText);--%>


                            <%--        if (json_obj.status) {--%>

                            <%--            toastr.success('Committee member found');--%>

                            <%--            let output = '';--%>
                            <%--            output += '<div style="display: none" id="committeeId">';--%>
                            <%--            output += json_obj.committeeId;--%>
                            <%--            output += '</div>';--%>

                            <%--            output += '<div style="display: none" id="committeelength">';--%>
                            <%--            output += json_obj.marks_committee_membersDTOS.length;--%>
                            <%--            output += '</div>';--%>

                            <%--            let j = 0;--%>
                            <%--            for (let i = 0; i < json_obj.marks_committee_membersDTOS.length; i++) {--%>
                            <%--                j = i + 1;--%>
                            <%--                let otp_name = "otp_" + j;--%>
                            <%--                let otp_label = "otp_label_" + j;--%>
                            <%--                let member_id = "member_id_" + j;--%>
                            <%--                let member = json_obj.marks_committee_membersDTOS[i];--%>
                            <%--                output += '<div class="form-group row">';--%>
                            <%--                output += '<label class="col-md-3 col-form-label text-md-right">';--%>

                            <%--                output += '<%=LM.getText(LC.COMMITTEE_MEMBER_SEARCH_ANYFIELD, loginDTO)%>';--%>

                            <%--                output += '</label>';--%>

                            <%--                output += '<div class="col-sm-5">';--%>

                            <%--                output += '<input type="text" class="form-control"   value="';--%>
                            <%--                output += language == 'English' ? member.employeeRecordName : member.employeeRecordNameBn;--%>
                            <%--                output += ", "--%>
                            <%--                output += language == 'English' ? member.unitName : member.unitNameBn;--%>
                            <%--                output += ", "--%>
                            <%--                output += language == 'English' ? member.postName : member.postNameBn;--%>
                            <%--                output += '" tag="pb_html" readonly/>';--%>

                            <%--                output += '</div>';--%>

                            <%--                output += '<label class="col-sm-2 col-form-label text-md-right" id="';--%>

                            <%--                output += otp_label;--%>
                            <%--                output += '" style="display: none;">';--%>

                            <%--                output += '<%=LM.getText(LC.GLOBAL_OTP_VERIFICATION, loginDTO)%>';--%>

                            <%--                output += '</label>';--%>
                            <%--                output += '<div class="col-sm-2">';--%>

                            <%--                output += '<input type="text" class="form-control" name="';--%>
                            <%--                output += otp_name + '" id="';--%>
                            <%--                output += otp_name + '"';--%>
                            <%--                output += ' tag="pb_html" style="display:none;"/>';--%>

                            <%--                output += '</div>';--%>


                            <%--                output += '</div>';--%>
                            <%--                output += '<input type="text" class="form-control" name="';--%>
                            <%--                output += member_id + '" id="';--%>
                            <%--                output += member_id + '" value="';--%>
                            <%--                output += member.iD;--%>
                            <%--                output += '" tag="pb_html" style="display:none;"/>';--%>

                            <%--                output += '<div style="position: absolute; bottom: 8px; right: 1%; text-align:right; color: red; font-family: Roboto, SolaimanLipi; font-size: 1.1rem" id="progressBar"> </div>';--%>



                            <%--            }--%>
                            <%--            $('.committee-member').html(output);--%>
                            <%--            document.getElementById('send_otp').style = "display:inline";--%>

                            <%--        } else {--%>
                            <%--            toastr.error('No committee member found');--%>
                            <%--        }--%>


                            <%--    } else if (this.readyState == 4 && this.status != 200) {--%>
                            <%--    }--%>
                            <%--};--%>

                            <%--let CommitteeParams = "Recruitment_job_descriptionServlet?actionType=getMarksCommittee&jobId=" + job_id;--%>
                            <%--committeeMemberXhttp.open("Get", CommitteeParams, true);--%>
                            <%--committeeMemberXhttp.send();--%>


                            /*comitee member end*/
                        } else {
                            toastr.error('Admit card issue');
                        }
                    } else if (this.readyState == 4 && this.status != 200) {
                        //console.log('admit exits: '+this.responseText);
                    }
                };

                let params = "Admit_cardServlet?actionType=admitCardExists";
                params = params + "&jobId=" + job_id;
                //console.log("params: " + params);
                admitCardXhttp.open("Get", params, true);
                admitCardXhttp.send();
            } else {
                toastr.error('Already uploaded');
            }

            // let xhttp = new XMLHttpRequest();
            // xhttp.onreadystatechange = function () {
            //     if (this.readyState == 4 && this.status == 200) {
            //          if(this.responseText==1){
            //              $('#loadFileXml').attr('disabled','disabled');
            //              toastr.error('Upload excel button disabled');
            //          }
            //          else{
            //              $('#loadFileXml').removeAttr('disabled');
            //              toastr.success('Upload excel button enabled');
            //          }
            //     }
            //     else if (this.readyState == 4 && this.status != 200) {
            //     }
            // };
            //
            // let params = "Recruitment_bulk_mark_entryServlet?actionType=CheckExistenceJobLevel";
            // params = params + "&language=" + language + "&job_id=" + job_id;
            // params = params +  "&level_id=" + level_id;
            //
            // xhttp.open("Get", params, true);
            // xhttp.send();

        } else {
            hideLoadButton();
            document.getElementById('level').innerHTML = '';
        }
    }

    function getAllDataCount() {
        let jobValue = document.getElementById('job').value;
        let levelValue = document.getElementById('level').value;

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                let responseCount = JSON.parse(this.responseText);
                showItemCountDiv
                (responseCount.totalCount, responseCount.viewCount, responseCount.selectedCount, responseCount.rejectedCount);

            } else if (this.readyState == 4 && this.status != 200) {
            }
        };

        let params = "&jobId=" + jobValue + "&level=" + levelValue;
        let prefixParam = "RecruitmentJobSpecificCandidateListServlet?actionType=getAllDataCount" + params;

        xhttp.open("Get", prefixParam, true);
        xhttp.send();

    }

    function onSubmit(params) {
        hideItemCountDiv();
        let jobValue = document.getElementById('job').value;
        //let statusValue = document.getElementById('status').value;
        let levelValue = document.getElementById('level').value;
        let viewValue = document.getElementById('view').value;
        let approveValue = document.getElementById('approve').value;
        // console.log({jobValue, statusValue, levelValue, viewValue, approveValue})

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('data-div').innerHTML = this.responseText;
                setPageNo();
                showDataDiv();
                getAllDataCount();

            } else if (this.readyState == 4 && this.status != 200) {
            }
        };

        params = params + "&jobId=" + jobValue
            + "&view=" + viewValue
            + "&approve=" + approveValue
            + "&level=" + levelValue;
        let prefixParam = "RecruitmentJobSpecificCandidateListServlet?actionType=getData" + params;
        // console.log(prefixParam)

        xhttp.open("Get", prefixParam, true);
        xhttp.send();
    }

    function allfield_changed(go, pagination_number) {
        // console.log('came here')


        let params = '&search=true&ajax=true';

        // var extraParams = document.getElementsByName('extraParam');
        // extraParams.forEach((param) => {
        //     params += "&" + param.getAttribute("tag") + "=" + param.value;
        // })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            // console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[0].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;

        // console.log(params)
        onSubmit(params);

    }

    function shortList(selectedId) {
        event.preventDefault();
        let remarksLabel = '<%=LM.getText(LC.CANDIDATE_LIST_REMARKS, loginDTO)%>' + ":";
        let saveLabel = '<%=LM.getText(LC.CANDIDATE_LIST_YES, loginDTO)%>';
        let cancelLabel = '<%=LM.getText(LC.CANDIDATE_LIST_NO, loginDTO)%>';
        let headingLabel = '<%=LM.getText(LC.CANDIDATE_LIST_SHOTLIST_REMARKS, loginDTO)%>';

        bootbox.dialog({
            message: "<div class='row'>" +
                "<div class='col-lg-2'>" + remarksLabel + "</div>" +
                "<textarea class='col-lg-8' style='border: 1px solid' id = 'shortlist-remarks'></textarea>" +
                "</div>",
            title: headingLabel,
            closeButton: false,
            buttons: [
                {
                    label: saveLabel,
                    className: "btn-success",
                    callback: function () {
                        let remarks = $('#shortlist-remarks').val();
                        makeShortlisted(selectedId, remarks);
                    }
                },
                {
                    label: cancelLabel,
                    className: "btn-danger",
                    callback: function () {
                        // console.log( $('#shortlist-remarks').val());
                        // console.log('nothing happened')
                    }
                }
            ]
        });
    }

    function reject(selectedId) {
        event.preventDefault();
        let remarksLabel = '<%=LM.getText(LC.CANDIDATE_LIST_REMARKS, loginDTO)%>' + ":";
        let saveLabel = '<%=LM.getText(LC.CANDIDATE_LIST_YES, loginDTO)%>';
        let cancelLabel = '<%=LM.getText(LC.CANDIDATE_LIST_NO, loginDTO)%>';
        let headingLabel = '<%=LM.getText(LC.CANDIDATE_LIST_REJECT_REMARKS, loginDTO)%>';

        bootbox.dialog({
            message: "<div class='row'>" +
                "<div class='col-lg-2'>" + remarksLabel + "</div>" +
                "<textarea class='col-lg-8' style='border: 1px solid' id = 'reject-remarks'></textarea>" +
                "</div>",
            title: headingLabel,
            closeButton: false,
            buttons: [
                {
                    label: saveLabel,
                    className: "btn-success",
                    callback: function () {
                        let remarks = $('#reject-remarks').val();
                        makeRejected(selectedId, remarks);
                    }
                },
                {
                    label: cancelLabel,
                    className: "btn-danger",
                    callback: function () {
                        // console.log( $('#shortlist-remarks').val());
                        // console.log('nothing happened')
                    }
                }
            ]
        });

    }

    function makeShortlisted(id, remarks) {
        let formData = new FormData();
        formData.append('id', id);
        formData.append('remarks', remarks);

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                // alert("updated");
                toastr.success("Success");
                onSubmit('');
            } else if (this.readyState == 4 && this.status != 200) {
            }
        };

        let params = "Job_applicant_applicationServlet?actionType=makeShortlist";
        xhttp.open("POST", params, true);
        xhttp.send(formData);
    }

    function makeRejected(id, remarks) {
        let formData = new FormData();
        formData.append('id', id);
        formData.append('remarks', remarks);

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                // alert("updated");
                toastr.success("Success");
                onSubmit('');
            } else if (this.readyState == 4 && this.status != 200) {
            }
        };

        let params = "Job_applicant_applicationServlet?actionType=makeRejected";
        xhttp.open("POST", params, true);
        xhttp.send(formData);
    }

    function makeViewed(id) {
        let formData = new FormData();
        formData.append('id', id);

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                // alert("updated");

            } else if (this.readyState == 4 && this.status != 200) {
            }
        };

        let params = "Job_applicant_applicationServlet?actionType=makeViewed";
        xhttp.open("POST", params, true);
        xhttp.send(formData);
    }

    let numberBangla = {
        '0': '০',
        '1': '১',
        '2': '২',
        '3': '৩',
        '4': '৪',
        '5': '৫',
        '6': '৬',
        '7': '৭',
        '8': '৮',
        '9': '৯'
    };

    function toBn(retStr) {
        retStr = retStr + "";
        for (let x in numberBangla) {
            retStr = retStr.replace(new RegExp(x, 'g'),
                numberBangla[x]);
        }
        return retStr;
    }

    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }


        return true;
    }

    function onRecruitmentTestNameChange(selectedVal) {

        let recruitmentTestNameId = selectedVal.value;

        if (recruitmentTestNameId !== null && recruitmentTestNameId > 0 && recruitmentTestNameId !== "") {
            const url = 'Recruitment_job_descriptionServlet?actionType=getJobDescriptionByRecTestName&recruitmentTestNameId=' + recruitmentTestNameId +
                '&selectedId=-1';

            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (response) {
                    document.getElementById('job').innerHTML = response;
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }

</script>
