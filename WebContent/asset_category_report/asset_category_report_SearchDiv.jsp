<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.ASSET_CATEGORY_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div>
    <div class="row mx-2 mx-md-0">
		
		<div  class="search-criteria-div col-md-6">
			<div class="form-group row">
				<label class="col-sm-3 col-form-label text-md-right">
					<%=LM.getText(LC.ASSET_CATEGORY_REPORT_WHERE_ASSETCATEGORYTYPE, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='assetCategoryType' id = 'assetCategoryType' >		
						<%		
						Options = CommonDAO.getOptions(Language, "asset_category", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">
function init()
{
    dateTimeInit($("#Language").val());
}
function PreprocessBeforeSubmiting()
{
}
function getcats(type)
{
	console.log("getting cats");
	fillDropDown(type, "assetCategoryType", "Asset_categoryServlet?actionType=getCatsWithType&type=" + type + "&language=<%=Language%>");
	
}
</script>