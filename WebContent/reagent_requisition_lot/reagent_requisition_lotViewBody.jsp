<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="reagent_requisition_lot.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>

<%
    String servletName = "Reagent_requisition_lotServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.REAGENT_REQUISITION_LOT_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Reagent_requisition_lotDAO reagent_requisition_lotDAO = new Reagent_requisition_lotDAO("reagent_requisition_lot");
    Reagent_requisition_lotDTO reagent_requisition_lotDTO = reagent_requisition_lotDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_REAGENT_REQUISITION_LOT_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div>
                <h5 class="table-title">
                    <%=LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_REAGENT_REQUISITION_LOT_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">
                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.MEDICAL_REQUISITION_LOT_ADD_REQUISITIONTOORGANOGRAMID, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = reagent_requisition_lotDTO.requisitionToOrganogramId + "";
                                %>
                                <%
                                    value = WorkflowController.getNameFromOrganogramId(reagent_requisition_lotDTO.requisitionToOrganogramId, Language) + ", " + WorkflowController.getOrganogramName(reagent_requisition_lotDTO.requisitionToOrganogramId, Language) + ", " + WorkflowController.getUnitNameFromOrganogramId(reagent_requisition_lotDTO.requisitionToOrganogramId, Language);
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.MEDICAL_REQUISITION_LOT_ADD_SUBJECT, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = reagent_requisition_lotDTO.subject + "";
                                %>

                                <%=value%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.HM_DATE, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = reagent_requisition_lotDTO.insertionDate + "";
                                %>
                                <%
                                    String formatted_insertionDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_insertionDate, Language)%>


                            </td>

                        </tr>


                    </table>
                </div>
            </div>
            <div class="mt-5">
                <h5 class="table-title">
                    <%=LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_REAGENT_REQUISITION_ITEM, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">
                        <tr>
                            <th>
                                <%=LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_REAGENT_REQUISITION_ITEM_DEPARTMENTCAT, loginDTO)%>
                            </th>
                            <th>
                                <%=LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_REAGENT_REQUISITION_ITEM_MEDICALREAGENTNAMETYPE, loginDTO)%>
                            </th>
                            <th>
                                <%=LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_REAGENT_REQUISITION_ITEM_TOTALCURRENTSTOCK, loginDTO)%>
                            </th>
                            <th>
                                <div class="d-flex justify-content-around">
                                    <div class=" text-danger">
                                        <%=LM.getText(LC.HM_EXPIRE_DATE, loginDTO)%>
                                    </div>
                                    <div class=" text-success ml-3 ml-md-0">
                                        <%=LM.getText(LC.HM_REMAINING_STOCK, loginDTO)%>
                                    </div>
                                </div>
                                <%--                                <table>--%>
                                <%--                                    <thead>--%>
                                <%--                                    <tr>--%>
                                <%--                                        <th style='width:100px'><%=LM.getText(LC.HM_EXPIRE_DATE, loginDTO)%>--%>
                                <%--                                        </th>--%>
                                <%--                                        <th style='width:100px'><%=LM.getText(LC.HM_REMAINING_STOCK, loginDTO)%>--%>
                                <%--                                        </th>--%>
                                <%--                                    </tr>--%>
                                <%--                                    </thead>--%>
                                <%--                                </table>--%>
                            </th>
                            <th>
                                <%=LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_REAGENT_REQUISITION_ITEM_QUANTITY, loginDTO)%>
                            </th>
                            <th>
                                <%=LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_REAGENT_REQUISITION_ITEM_REMARKS, loginDTO)%>
                            </th>
                        </tr>
                        <%
                            ReagentRequisitionItemDAO reagentRequisitionItemDAO = new ReagentRequisitionItemDAO();
                            List<ReagentRequisitionItemDTO> reagentRequisitionItemDTOs = reagentRequisitionItemDAO.getReagentRequisitionItemDTOListByReagentRequisitionLotID(reagent_requisition_lotDTO.iD);

                            int lastDept = -1;
                            for (ReagentRequisitionItemDTO reagentRequisitionItemDTO : reagentRequisitionItemDTOs) {
                        %>
                        <tr>
                            <td>

                                <%
                                    if (lastDept != reagentRequisitionItemDTO.departmentCat) {
                                        value = CatDAO.getName(Language, "department", reagentRequisitionItemDTO.departmentCat);
                                        lastDept = reagentRequisitionItemDTO.departmentCat;
                                    } else {
                                        value = "";
                                    }

                                %>

                                <b><%=value%>
                                </b>


                            </td>
                            <td>
                                <%
                                    value = reagentRequisitionItemDTO.medicalReagentNameType + "";
                                %>
                                <%
                                    value = CommonDAO.getName(Integer.parseInt(value), "medical_reagent_name", Language.equals("English") ? "name_en" : "name_bn", "id");
                                %>

                                <%=value%>


                            </td>
                            <td>
                                <%
                                    value = reagentRequisitionItemDTO.totalCurrentStock + " " + reagentRequisitionItemDTO.unit;
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>
                            <td>
                                <table>
                                    <%
                                        String[] eDates = reagentRequisitionItemDTO.expiryDateList.split(", ");
                                        String[] rStocks = reagentRequisitionItemDTO.currentStockList.split(", ");


                                        for (int j = 0; j < eDates.length; j++) {
                                            if (j == eDates.length - 1) {
                                                eDates[j] = eDates[j].split(",")[0];
                                                if (!rStocks[j].split(",")[0].equalsIgnoreCase("")) {
                                                    rStocks[j] = rStocks[j].split(",")[0] + " " + reagentRequisitionItemDTO.unit;
                                                }

                                            }
                                    %>

                                    <tr>
                                        <div class='row px-3'>
                                            <div class='col-6 border text-center rounded px-2 py-3'>
                                                <%=Utils.getDigits(eDates[j] + "", Language)%>
                                            </div>
                                            <div class='col-6 border text-center rounded px-2 py-3'>
                                                <%=Utils.getDigits(rStocks[j] + "", Language)%>
                                            </div>
                                        </div>
                                    </tr>
                                    <%
                                        }

                                    %>
                                </table>
                            </td>
                            <td>
                                <%
                                    value = reagentRequisitionItemDTO.quantity + " " + reagentRequisitionItemDTO.unit;
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>
                            <td>
                                <%
                                    value = reagentRequisitionItemDTO.remarks + "";
                                %>

                                <%=value%>


                            </td>

                        </tr>
                        <%

                            }

                        %>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>