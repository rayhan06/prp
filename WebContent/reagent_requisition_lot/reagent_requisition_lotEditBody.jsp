<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="reagent_requisition_lot.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="approval_execution_table.*" %>
<%@ page import="approval_path.*" %>
<%@ page import="user.*" %>

<%@page import="workflow.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    Reagent_requisition_lotDTO reagent_requisition_lotDTO;
    reagent_requisition_lotDTO = (Reagent_requisition_lotDTO) request.getAttribute("reagent_requisition_lotDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    boolean dtoFound = true;
    if (reagent_requisition_lotDTO == null) {
        reagent_requisition_lotDTO = new Reagent_requisition_lotDTO();
        dtoFound = false;
    }
    System.out.println("reagent_requisition_lotDTO = " + reagent_requisition_lotDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getEditPage")) {
        actionName = "edit";
    } else {
        actionName = "add";
    }
    String formTitle = LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_REAGENT_REQUISITION_LOT_ADD_FORMNAME, loginDTO);
    String servletName = "Reagent_requisition_lotServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID = -1;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;
    if (request.getParameter("isPermanentTable") != null) {
        isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
    }

    Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
    ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
    Approval_execution_tableDTO approval_execution_tableDTO = null;
    Approval_execution_tableDTO approval_execution_table_initiationDTO = null;
    ApprovalPathDetailsDTO approvalPathDetailsDTO = null;

    String tableName = "reagent_requisition_lot";

    boolean canApprove = false, canValidate = false, isInitiator = false, canTerminate = false;

    if (!isPermanentTable) {
        approval_execution_tableDTO = (Approval_execution_tableDTO) approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("reagent_requisition_lot", reagent_requisition_lotDTO.iD);
        System.out.println("approval_execution_tableDTO = " + approval_execution_tableDTO);
        approvalPathDetailsDTO = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
        approval_execution_table_initiationDTO = (Approval_execution_tableDTO) approval_execution_tableDAO.getInitiationDTOByUpdatedRowId("reagent_requisition_lot", reagent_requisition_lotDTO.iD);
        if (approvalPathDetailsDTO != null && approvalPathDetailsDTO.organogramId == userDTO.organogramID) {
            canApprove = true;
            if (approvalPathDetailsDTO.approvalRoleCat == SessionConstants.VALIDATOR) {
                canValidate = true;
            }
        }

        isInitiator = WorkflowController.isInitiator(tableName, approval_execution_tableDTO.previousRowId, userDTO.organogramID);

        canTerminate = isInitiator && reagent_requisition_lotDTO.isDeleted == 2;
    }
    String Language = LM.getText(LC.REAGENT_REQUISITION_LOT_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<style>
    table input.form-control {
        width: auto !important;
    }

    table select.form-control {
        min-width: 12rem !important;
    }

</style>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Reagent_requisition_lotServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD'
                                           id='iD_hidden_<%=i%>'
                                           value='<%=reagent_requisition_lotDTO.iD%>'
                                           tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-4  col-md-form-label text-md-right">
                                            <%=LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_REQUISITIONTOORGANOGRAMID, loginDTO)%>
                                        </label>
                                        <div class=" col-md-7">
                                            <button type="button"
                                                    class="btn btn-block submit-btn text-white shadow btn-border-radius mb-3"
                                                    onclick="addEmployeeWithRow(this.id)"
                                                    id="requisitionToOrganogramId_button_0"
                                                    tag='pb_html'><%=LM.getText(LC.HM_ADD_EMPLOYEE, loginDTO)%>
                                            </button>
                                            <table class="table table-bordered table-striped">
                                                <tbody id="requisitionToOrganogramId_table_<%=i%>"
                                                       tag='pb_html'>
                                                <%
                                                    if (reagent_requisition_lotDTO.requisitionToOrganogramId != -1) {
                                                %>
                                                <tr>
                                                    <td style="width:20%"><%=WorkflowController.getUserNameFromOrganogramId(reagent_requisition_lotDTO.requisitionToOrganogramId)%>
                                                    </td>
                                                    <td style="width:40%"><%=WorkflowController.getNameFromOrganogramId(reagent_requisition_lotDTO.requisitionToOrganogramId, Language)%>
                                                    </td>
                                                    <td><%=WorkflowController.getOrganogramName(reagent_requisition_lotDTO.requisitionToOrganogramId, Language)%>
                                                        , <%=WorkflowController.getUnitNameFromOrganogramId(reagent_requisition_lotDTO.requisitionToOrganogramId, Language)%>
                                                    </td>
                                                </tr>
                                                <%
                                                    }
                                                %>
                                                </tbody>
                                            </table>
                                            <input type='hidden' class='form-control'
                                                   name='requisitionToOrganogramId'
                                                   id='requisitionToOrganogramId_hidden_<%=i%>'
                                                   value='<%=reagent_requisition_lotDTO.requisitionToOrganogramId%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4  col-md-form-label text-md-right"><%=LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_SUBJECT, loginDTO)%>
                                        </label>
                                        <div class=" col-md-7">
                                            <input type='text' class='form-control' name='subject'
                                                   id='subject_text_<%=i%>'
                                                   value='<%=reagent_requisition_lotDTO.subject%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4  col-md-form-label text-md-right"><%=LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_DESCRIPTION, loginDTO)%>
                                        </label>
                                        <div class=" col-md-7">
                                            <input type='text' class='form-control'
                                                   name='description'
                                                   id='description_text_<%=i%>'
                                                   value='<%=reagent_requisition_lotDTO.description%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control'
                                           name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=reagent_requisition_lotDTO.insertedByUserId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=reagent_requisition_lotDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='jobCat'
                                           id='jobCat_hidden_<%=i%>'
                                           value='<%=reagent_requisition_lotDTO.jobCat%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=reagent_requisition_lotDTO.insertionDate%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=reagent_requisition_lotDTO.searchColumn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value='<%=reagent_requisition_lotDTO.isDeleted%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=reagent_requisition_lotDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-body mt-5">
                    <h5 class="table-title">
                        <%=LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_REAGENT_REQUISITION_ITEM, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap">
                            <thead>
                            <tr>
                                <th class=""><%=LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_REAGENT_REQUISITION_ITEM_DEPARTMENTCAT, loginDTO)%>
                                </th>
                                <th class=""><%=LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_REAGENT_REQUISITION_ITEM_MEDICALREAGENTNAMETYPE, loginDTO)%>
                                </th>
                                <th class=""><%=LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_REAGENT_REQUISITION_ITEM_TOTALCURRENTSTOCK, loginDTO)%>
                                </th>
                                <th class="px-5">
                                    <div class="d-flex justify-content-between mx-5">
                                        <div class=" text-danger pl-2 pr-3">
                                            <%=LM.getText(LC.HM_EXPIRE_DATE, loginDTO)%>
                                        </div>
                                        <div class=" text-success pl-5 ml-5">
                                            <%=LM.getText(LC.HM_REMAINING_STOCK, loginDTO)%>
                                        </div>
                                    </div>
                                    <%--                                    <table>--%>
                                    <%--                                        <thead>--%>
                                    <%--                                        <tr>--%>
                                    <%--                                            <th><%=LM.getText(LC.HM_EXPIRE_DATE, loginDTO)%>--%>
                                    <%--                                            </th>--%>
                                    <%--                                            <th><%=LM.getText(LC.HM_REMAINING_STOCK, loginDTO)%>--%>
                                    <%--                                            </th>--%>
                                    <%--                                        </tr>--%>
                                    <%--                                        </thead>--%>
                                    <%--                                        <tbody></tbody>--%>
                                    <%--                                    </table>--%>
                                </th>
                                <th class=""><%=LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_REAGENT_REQUISITION_ITEM_QUANTITY, loginDTO)%>
                                </th>
                                <th class=""><%=LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_REAGENT_REQUISITION_ITEM_REMARKS, loginDTO)%>
                                </th>
                                <th class=""><%=LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_REAGENT_REQUISITION_ITEM_REMOVE, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="field-ReagentRequisitionItem">


                            <%
                                if (dtoFound) {
                                    int index = -1;


                                    for (ReagentRequisitionItemDTO reagentRequisitionItemDTO : reagent_requisition_lotDTO.reagentRequisitionItemDTOList) {
                                        index++;

                                        System.out.println("index index = " + index);

                            %>

                            <tr id="ReagentRequisitionItem_<%=index + 1%>">
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='reagentRequisitionItem.iD'
                                           id='iD_hidden_<%=childTableStartingID%>'
                                           value='<%=reagentRequisitionItemDTO.iD%>' tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='reagentRequisitionItem.reagentRequisitionLotId'
                                           id='reagentRequisitionLotId_hidden_<%=childTableStartingID%>'
                                           value='<%=reagentRequisitionItemDTO.reagentRequisitionLotId%>'
                                           tag='pb_html'/>
                                </td>
                                <td>


                                    <select class='form-control'
                                            name='reagentRequisitionItem.departmentCat'
                                            id='departmentCat_category_<%=childTableStartingID%>'
                                            tag='pb_html'>
                                        <%
                                            if (dtoFound) {
                                                Options = CatDAO.getOptions(Language, "department", reagentRequisitionItemDTO.departmentCat);
                                            } else {
                                                Options = CatDAO.getOptions(Language, "department", -1);
                                            }
                                        %>
                                        <%=Options%>
                                    </select>

                                </td>
                                <td>
                                    <select class='js-example-basic-single form-control'
                                            onchange='reagentSelected(this.id)'
                                            name='reagentRequisitionItem.medicalReagentNameType'
                                            id='medicalReagentNameType_select_<%=childTableStartingID%>'
                                            tag='pb_html'>
                                        <%
                                            if (dtoFound) {
                                                Options = CommonDAO.getReagents(reagentRequisitionItemDTO.medicalReagentNameType);
                                            } else {
                                                Options = CommonDAO.getReagents(-1);
                                            }
                                        %>
                                        <%=Options%>
                                    </select>

                                </td>
                                <td>

                                    <div name="drug_modal_textdiv"
                                         id="drug_modal_textdiv_<%=childTableStartingID%>"
                                         tag='pb_html'><%=reagentRequisitionItemDTO.totalCurrentStock%>
                                    </div>
                                    <input type='hidden' class='form-control'
                                           name='reagentRequisitionItem.totalCurrentStock'
                                           id='totalCurrentStock_text_<%=childTableStartingID%>'
                                           value=<%=dtoFound?("'" + reagentRequisitionItemDTO.totalCurrentStock + "'"):("'" + "0" + "'")%>   tag='pb_html'/>
                                </td>
                                <td id="status_td_<%=childTableStartingID%>" tag='pb_html'>

                                </td>
                                <td>


                                    <input type='number' class='form-control'
                                           name='reagentRequisitionItem.quantity' min='0'
                                           id='quantity_text_<%=childTableStartingID%>'
                                           value=<%=dtoFound?("'" + reagentRequisitionItemDTO.quantity + "'"):("'" + "0" + "'")%>   tag='pb_html'/>
                                    <div name="drug_modal_textdiv"
                                         id="unit_textdiv_<%=childTableStartingID%>"
                                         tag='pb_html'><%=reagentRequisitionItemDTO.unit%>
                                    </div>
                                    <input type='hidden' class='form-control'
                                           name='reagentRequisitionItem.unit'
                                           id='unit_text_<%=childTableStartingID%>'
                                           value=<%=dtoFound?("'" + reagentRequisitionItemDTO.unit + "'"):("'" + "" + "'")%>   tag='pb_html'/>

                                </td>
                                <td>


                                    <input type='text' class='form-control'
                                           name='reagentRequisitionItem.remarks'
                                           id='remarks_text_<%=childTableStartingID%>'
                                           value=<%=dtoFound?("'" + reagentRequisitionItemDTO.remarks + "'"):("'" + "" + "'")%>   tag='pb_html'/>


                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='reagentRequisitionItem.isDeleted'
                                           id='isDeleted_hidden_<%=childTableStartingID%>'
                                           value=<%=dtoFound?("'" + reagentRequisitionItemDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='reagentRequisitionItem.lastModificationTime'
                                           id='lastModificationTime_hidden_<%=childTableStartingID%>'
                                           value=<%=dtoFound?("'" + reagentRequisitionItemDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                </td>
                                <td>
                                    <div class='checker'><span id='chkEdit'><input type='checkbox'
                                                                                   id='reagentRequisitionItem_cb_<%=index%>'
                                                                                   name='checkbox'
                                                                                   value=''/></span>
                                    </div>
                                </td>
                            </tr>
                            <%
                                        childTableStartingID++;
                                    }
                                }
                            %>

                            </tbody>
                        </table>


                    </div>
                    <div class="form-group">
                        <div class="col-md-12 text-right mt-3 mt-md-0">
                            <button
                                    id="add-more-ReagentRequisitionItem"
                                    name="add-moreReagentRequisitionItem"
                                    type="button"
                                    class="btn btn-sm text-white add-btn shadow">
                                <i class="fa fa-plus"></i>
                                <%=LM.getText(LC.HM_ADD, loginDTO)%>
                            </button>
                            <button
                                    id="remove-ReagentRequisitionItem"
                                    name="removeReagentRequisitionItem"
                                    type="button"
                                    class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </div>
                    <%ReagentRequisitionItemDTO reagentRequisitionItemDTO = new ReagentRequisitionItemDTO();%>
                    <template id="template-ReagentRequisitionItem">
                        <tr>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='reagentRequisitionItem.iD' id='iD_hidden_'
                                       value='<%=reagentRequisitionItemDTO.iD%>' tag='pb_html'/>

                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='reagentRequisitionItem.reagentRequisitionLotId'
                                       id='reagentRequisitionLotId_hidden_'
                                       value=<%=dtoFound?("'" + reagentRequisitionItemDTO.reagentRequisitionLotId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                            </td>
                            <td>


                                <select class='form-control' name='reagentRequisitionItem.departmentCat'
                                        id='departmentCat_category_' onchange='deptSelected(this.id)'
                                        tag='pb_html'>
                                    <%
                                        if (dtoFound) {
                                            Options = CatDAO.getOptions(Language, "department", reagentRequisitionItemDTO.departmentCat);
                                        } else {


                                            Options = CatDAO.getOptions(Language, "department", -1);
                                        }
                                    %>
                                    <%=Options%>
                                </select>

                            </td>
                            <td>


                                <select class='form-control' onchange='reagentSelected(this.id)'
                                        name='reagentRequisitionItem.medicalReagentNameType'
                                        id='medicalReagentNameType_select_' tag='pb_html'>
                                    <%
                                        Options = CommonDAO.getReagents(-1);
                                    %>
                                    <%=Options%>
                                </select>

                            </td>
                            <td>

                                <div name="drug_modal_textdiv" id="drug_modal_textdiv_"
                                     tag='pb_html'></div>

                                <input type='hidden' class='form-control'
                                       name='reagentRequisitionItem.totalCurrentStock'
                                       id='totalCurrentStock_text_'
                                       value=<%=dtoFound?("'" + reagentRequisitionItemDTO.totalCurrentStock + "'"):("'" + "0" + "'")%>   tag='pb_html'/>
                            </td>
                            <td id="status_td_" tag='pb_html'>

                            </td>
                            <td>


                                <input type='number' class='form-control'
                                       name='reagentRequisitionItem.quantity' id='quantity_text_' min='0'
                                       value=<%=dtoFound?("'" + reagentRequisitionItemDTO.quantity + "'"):("'" + "0" + "'")%>   tag='pb_html'/>
                                <div name="drug_modal_textdiv" id="unit_textdiv_" tag='pb_html'></div>
                                <input type='hidden' class='form-control'
                                       name='reagentRequisitionItem.unit' id='unit_text_'
                                       value=<%=dtoFound?("'" + reagentRequisitionItemDTO.unit + "'"):("'" + "" + "'")%>   tag='pb_html'/>

                            </td>
                            <td>


                                <input type='text' class='form-control'
                                       name='reagentRequisitionItem.remarks' id='remarks_text_'
                                       value=<%=dtoFound?("'" + reagentRequisitionItemDTO.remarks + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='reagentRequisitionItem.isDeleted' id='isDeleted_hidden_'
                                       value=<%=dtoFound?("'" + reagentRequisitionItemDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>

                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='reagentRequisitionItem.lastModificationTime'
                                       id='lastModificationTime_hidden_'
                                       value=<%=dtoFound?("'" + reagentRequisitionItemDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                            </td>
                            <td>
                                <div><span id='chkEdit'><input type='checkbox' name='checkbox'
                                                               value=''/></span></div>
                            </td>
                        </tr>

                    </template>
                </div>
                <div class="form-actions text-center mb-5 mt-4">
                    <button class="btn-sm shadow text-white border-0 cancel-btn btn-border-radius">
                        <%=LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_REAGENT_REQUISITION_LOT_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn btn-border-radius ml-2"
                            type="submit">
                        <%=LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_REAGENT_REQUISITION_LOT_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    var lastSelectedDept = -1;


    function PreprocessBeforeSubmiting(row, validate) {


        for (i = 1; i < child_table_extra_id; i++) {
        }
        return true;
    }

    function reagentSelected(id) {
        var drugModalRowId = id.split("_")[2];
        console.log("drugModalRowId " + drugModalRowId);
        var select = $('#' + id);
        var stock = $('option:selected', select).attr('stock');
        var unit = $('option:selected', select).attr('unit');
        var drugId = $('option:selected', select).val();
        $("#drug_modal_textdiv_" + drugModalRowId).html(stock + " " + unit + "");
        $("#totalCurrentStock_text_" + drugModalRowId).val(stock);
        $("#unit_textdiv_" + drugModalRowId).html(unit + "");
        $("#unit_text_" + drugModalRowId).val(unit);

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var list = JSON.parse(this.responseText);
                var i;
                var table = "<table>";
                var edateStr = "";
                var rstockStr = "";
                for (i = 0; i < list.length; i++) {
                    var medicalInventoryInDTO = list[i];
                    console.log("medicalInventoryInDTO.iD = " + medicalInventoryInDTO.iD);
                    table += "<div class='row px-3'>";
                    table += "<div class='col-md-6 border text-center rounded p-2'>" + medicalInventoryInDTO.expiryDateStr + "</td>";
                    table += "<div class='col-md-6 border text-center rounded p-2'>" + medicalInventoryInDTO.remainingStock + " " + unit + "</td>";
                    table += "</div>";

                    edateStr += medicalInventoryInDTO.expiryDateStr + ", ";
                    rstockStr += medicalInventoryInDTO.remainingStock + ", ";
                }
                table += "</table>";

                table += "<input type='hidden' name='reagentRequisitionItem.expiryDateList' value='" + edateStr + "'' />";
                table += "<input type='hidden' name='reagentRequisitionItem.currentStockList' value='" + rstockStr + "'' />";

                $("#status_td_" + drugModalRowId).html(table);
            } else {
                console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
                var table = "";
                table += "<input type='hidden' name='medicalRequisitionItem.expiryDateList' value='' />";
                table += "<input type='hidden' name='medicalRequisitionItem.currentStockList' value='' />";

                $("#status_td_" + drugModalRowId).html(table);
            }
        };

        xhttp.open("POST", "Medical_inventory_lotServlet?actionType=getReagentEdateList&reagentId=" + drugId, true);
        xhttp.send();

    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Reagent_requisition_lotServlet");
    }

    function init(row) {

        $("[name='reagentRequisitionItem.medicalReagentNameType']").select2({
            dropdownAutoWidth: true,
            width: '100%',
            // theme: 'classic'
        });

        for (i = 1; i < child_table_extra_id; i++) {
            reagentSelected("medicalReagentNameType_select_" + i);
        }

    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-ReagentRequisitionItem").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-ReagentRequisitionItem");

            $("#field-ReagentRequisitionItem").append(t.html());
            SetCheckBoxValues("field-ReagentRequisitionItem");

            var tr = $("#field-ReagentRequisitionItem").find("tr:last-child");

            tr.attr("id", "ReagentRequisitionItem_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                console.log(index + ": " + $(this).attr('id'));
            });

            $("#departmentCat_category_" + child_table_extra_id).val(lastSelectedDept);
            deptSelected("departmentCat_category_" + child_table_extra_id);
            child_table_extra_id++;

        });


    $("#remove-ReagentRequisitionItem").click(function (e) {
        var tablename = 'field-ReagentRequisitionItem';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[type="checkbox"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
    });

    function deptSelected(id) {
        var drugModalRowId = id.split("_")[2];
        console.log("drugModalRowId " + drugModalRowId);
        var select = $('#' + id);

        var deptId = $('option:selected', select).val();
        lastSelectedDept = deptId;

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                $("#medicalReagentNameType_select_" + drugModalRowId).html(this.responseText);
            } else {
                console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
            }
        };

        xhttp.open("GET", "Reagent_requisition_lotServlet?actionType=getReagentsByDept&dept=" + deptId, true);
        xhttp.send();

    }

    $(document).ready(() => {
        $(".cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });


</script>






