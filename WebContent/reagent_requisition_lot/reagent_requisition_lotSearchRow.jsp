<%@page pageEncoding="UTF-8" %>

<%@page import="reagent_requisition_lot.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="approval_execution_table.*" %>
<%@ page import="approval_path.*" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.REAGENT_REQUISITION_LOT_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_REAGENT_REQUISITION_LOT;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Reagent_requisition_lotDTO reagent_requisition_lotDTO = (Reagent_requisition_lotDTO) request.getAttribute("reagent_requisition_lotDTO");
    CommonDTO commonDTO = reagent_requisition_lotDTO;
    String servletName = "Reagent_requisition_lotServlet";

    Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
    ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
    Approval_execution_tableDTO approval_execution_tableDTO = null;
    String Message = "Done";
    approval_execution_tableDTO = (Approval_execution_tableDTO) approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("reagent_requisition_lot", reagent_requisition_lotDTO.iD);

    System.out.println("reagent_requisition_lotDTO = " + reagent_requisition_lotDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Reagent_requisition_lotDAO reagent_requisition_lotDAO = (Reagent_requisition_lotDAO) request.getAttribute("reagent_requisition_lotDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td id='<%=i%>_requisitionToOrganogramId' class="text-nowrap">
    <%
        value = reagent_requisition_lotDTO.requisitionToOrganogramId + "";
    %>
    <%
        value = WorkflowController.getNameFromOrganogramId(reagent_requisition_lotDTO.requisitionToOrganogramId, Language) + ", " + WorkflowController.getOrganogramName(reagent_requisition_lotDTO.requisitionToOrganogramId, Language) + ", " + WorkflowController.getUnitNameFromOrganogramId(reagent_requisition_lotDTO.requisitionToOrganogramId, Language);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_subject'>
    <%
        value = reagent_requisition_lotDTO.subject + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_description'>
    <%
        value = reagent_requisition_lotDTO.description + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Reagent_requisition_lotServlet?actionType=view&ID=<%=reagent_requisition_lotDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Reagent_requisition_lotServlet?actionType=getEditPage&ID=<%=reagent_requisition_lotDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td>
    <button
            class="btn btn-sm btn-primary shadow btn-border-radius pl-4 pb-2"
            onclick="location.href='Reagent_requisition_lotServlet?actionType=copy&ID=<%=reagent_requisition_lotDTO.iD%>'">
        <i class="fas fa-copy"></i>
    </button>
</td>


<td class="text-right" id='<%=i%>_checkbox'>
    <div class='checker'>
        <span class='chkEdit'>
			<input type='checkbox' name='ID' value='<%=reagent_requisition_lotDTO.iD%>'/>
		</span>
    </div>
</td>
											

