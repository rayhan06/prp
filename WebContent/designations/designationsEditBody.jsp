<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="designations.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>

<%
    DesignationsDTO designationsDTO = new DesignationsDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        designationsDTO = DesignationsDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = designationsDTO;
    String tableName = "designations";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.DESIGNATIONS_ADD_DESIGNATIONS_ADD_FORMNAME, loginDTO);
    String servletName = "DesignationsServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="DesignationsServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=designationsDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.GLOBAL_NAME_ENGLISH, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='nameEn'
                                                   id='nameEn_text_<%=i%>' value='<%=designationsDTO.nameEn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.GLOBAL_NAME_BANGLA, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='nameBn'
                                                   id='nameBn_text_<%=i%>' value='<%=designationsDTO.nameBn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.GLOBAL_Grade, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <select class='form-control' name='jobGradeCat'
                                                    id='jobGradeCat_category_<%=i%>' tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("job_grade", Language, designationsDTO.jobGradeCat)%>
                                            </select>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.PROJECT_TRACKER_ADD_PROJECT_TRACKER_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.PROJECT_TRACKER_ADD_PROJECT_TRACKER_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, action) {
        console.log("action = " + action);

        submitAddForm2();
        return false;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "DesignationsServlet");
    }

    function init(row) {


    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });


</script>






