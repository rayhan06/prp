<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="medicine_generic_name.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>


<%
    Medicine_generic_nameDTO medicine_generic_nameDTO;
    medicine_generic_nameDTO = (Medicine_generic_nameDTO) request.getAttribute("medicine_generic_nameDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (medicine_generic_nameDTO == null) {
        medicine_generic_nameDTO = new Medicine_generic_nameDTO();

    }
    System.out.println("medicine_generic_nameDTO = " + medicine_generic_nameDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle;
    if (actionName.equals("edit")) {
        formTitle = LM.getText(LC.MEDICINE_GENERIC_NAME_EDIT_MEDICINE_GENERIC_NAME_EDIT_FORMNAME, loginDTO);
    } else {
        formTitle = LM.getText(LC.MEDICINE_GENERIC_NAME_ADD_MEDICINE_GENERIC_NAME_ADD_FORMNAME, loginDTO);
    }

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
%>

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%
    String Language = LM.getText(LC.MEDICINE_GENERIC_NAME_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Medicine_generic_nameServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">

                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=medicine_generic_nameDTO.iD%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.MEDICINE_GENERIC_NAME_EDIT_NAMEEN, loginDTO)) : (LM.getText(LC.MEDICINE_GENERIC_NAME_ADD_NAMEEN, loginDTO))%>
                                        </label>
                                        <div class="col-md-9">
                                            <div id='nameEn_div_<%=i%>'>
                                                <input type='text' class='form-control' name='nameEn'
                                                       id='nameEn_text_<%=i%>'
                                                       value=<%=actionName.equals("edit")?("'" + medicine_generic_nameDTO.nameEn + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.MEDICINE_GENERIC_NAME_EDIT_NAMEBN, loginDTO)) : (LM.getText(LC.MEDICINE_GENERIC_NAME_ADD_NAMEBN, loginDTO))%>
                                        </label>
                                        <div class="col-md-9">
                                            <div id='nameBn_div_<%=i%>'>
                                                <input type='text' class='form-control' name='nameBn'
                                                       id='nameBn_text_<%=i%>'
                                                       value=<%=actionName.equals("edit")?("'" + medicine_generic_nameDTO.nameBn + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + medicine_generic_nameDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + medicine_generic_nameDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%
                                    if (actionName.equals("edit")) {
                                %>
                                <%=LM.getText(LC.MEDICINE_GENERIC_NAME_EDIT_MEDICINE_GENERIC_NAME_CANCEL_BUTTON, loginDTO)%>
                                <%
                                } else {
                                %>
                                <%=LM.getText(LC.MEDICINE_GENERIC_NAME_ADD_MEDICINE_GENERIC_NAME_CANCEL_BUTTON, loginDTO)%>
                                <%
                                    }

                                %>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%
                                    if (actionName.equals("edit")) {
                                %>
                                <%=LM.getText(LC.MEDICINE_GENERIC_NAME_EDIT_MEDICINE_GENERIC_NAME_SUBMIT_BUTTON, loginDTO)%>
                                <%
                                } else {
                                %>
                                <%=LM.getText(LC.MEDICINE_GENERIC_NAME_ADD_MEDICINE_GENERIC_NAME_SUBMIT_BUTTON, loginDTO)%>
                                <%
                                    }
                                %>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">
    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Medicine_generic_nameServlet");
    }

    function init(row) {


    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }
    $(document).ready(() => {
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    })
    var child_table_extra_id = <%=childTableStartingID%>;


</script>






