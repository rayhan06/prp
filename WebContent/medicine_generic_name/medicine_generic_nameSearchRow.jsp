<%@page pageEncoding="UTF-8" %>

<%@page import="medicine_generic_name.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.MEDICINE_GENERIC_NAME_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_MEDICINE_GENERIC_NAME;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Medicine_generic_nameDTO medicine_generic_nameDTO = (Medicine_generic_nameDTO) request.getAttribute("medicine_generic_nameDTO");
    CommonDTO commonDTO = medicine_generic_nameDTO;
    String servletName = "Medicine_generic_nameServlet";


    System.out.println("medicine_generic_nameDTO = " + medicine_generic_nameDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Medicine_generic_nameDAO medicine_generic_nameDAO = (Medicine_generic_nameDAO) request.getAttribute("medicine_generic_nameDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_nameEn' class="text-nowrap">
    <%
        value = medicine_generic_nameDTO.nameEn + "";
    %>

    <%=value%>


</td>


<td id='<%=i%>_nameBn' class="text-nowrap">
    <%
        value = medicine_generic_nameDTO.nameBn + "";
    %>

    <%=value%>


</td>


<td id='<%=i%>_Edit' class="text-nowrap">
    <a href="#"
       onclick='fixedToEditable(<%=i%>,"", "<%=medicine_generic_nameDTO.iD%>", <%=isPermanentTable%>, "Medicine_generic_nameServlet")'>
        <button
                class="btn-sm border-0 shadow btn-border-radius text-white"
                style="background-color: #ff6b6b;"
                type="button"
        >
            <i class="fa fa-edit"></i>
        </button>
    </a>

</td>

<td class="text-right" id='<%=i%>_checkbox'>
	<div class='checker'>
        <span class='chkEdit'>
            <input type='checkbox' name='ID' value='<%=medicine_generic_nameDTO.iD%>'/>
        </span>
	</div>
</td>
																						
											

