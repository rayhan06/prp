<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="medicine_generic_name.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.MEDICINE_GENERIC_NAME_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    Medicine_generic_nameDAO medicine_generic_nameDAO = (Medicine_generic_nameDAO) request.getAttribute("medicine_generic_nameDAO");


    String navigator2 = SessionConstants.NAV_MEDICINE_GENERIC_NAME;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<%

    if (hasAjax == false) {
        Enumeration<String> parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {

            String paramName = parameterNames.nextElement();

            if (!paramName.equalsIgnoreCase("actionType")) {
                String[] paramValues = request.getParameterValues(paramName);
                for (int i = 0; i < paramValues.length; i++) {
                    String paramValue = paramValues[i];

%>

<%

                }
            }


        }
    }

%>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th ><%=LM.getText(LC.MEDICINE_GENERIC_NAME_EDIT_NAMEEN, loginDTO)%>
            </th>
            <th ><%=LM.getText(LC.MEDICINE_GENERIC_NAME_EDIT_NAMEBN, loginDTO)%>
            </th>
            <th ><%=LM.getText(LC.MEDICINE_GENERIC_NAME_SEARCH_MEDICINE_GENERIC_NAME_EDIT_BUTTON, loginDTO)%>
            </th>
            <th>
<%--				<input type="submit" class="btn btn-xs btn-danger"--%>
				<%--                       value="<%=LM.getText(LC.MEDICINE_GENERIC_NAME_SEARCH_MEDICINE_GENERIC_NAME_DELETE_BUTTON, loginDTO)%>"/>--%>
				<div class="text-center">
					<span>All</span>
				</div>
				<div class="d-flex align-items-center justify-content-between mt-3">
					<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
						<i class="fa fa-trash"></i>
					</button>
					<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
				</div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_MEDICINE_GENERIC_NAME);

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Medicine_generic_nameDTO medicine_generic_nameDTO = (Medicine_generic_nameDTO) data.get(i);


        %>
        <tr id='tr_<%=i%>'>
            <%

            %>


            <%
                request.setAttribute("medicine_generic_nameDTO", medicine_generic_nameDTO);
            %>

            <jsp:include page="./medicine_generic_nameSearchRow.jsp">
                <jsp:param name="pageName" value="searchrow"/>
                <jsp:param name="rownum" value="<%=i%>"/>
            </jsp:include>


            <%

            %>
        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


<script src="nicEdit.js" type="text/javascript"></script>

<script type="text/javascript">

    function getOriginal(i, tempID, parentID, ServletName) {
        console.log("getOriginal called");
        var idToSubmit;
        var isPermanentTable;
        var state = document.getElementById(i + "_original_status").value;
        if (state == 0) {
            idToSubmit = parentID;
            isPermanentTable = true;
        } else {
            idToSubmit = tempID;
            isPermanentTable = false;
        }
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var response = JSON.parse(this.responseText);
                document.getElementById(i + "_nameEn").innerHTML = response.nameEn;
                document.getElementById(i + "_nameBn").innerHTML = response.nameBn;

                if (state == 0) {
                    document.getElementById(i + "_getOriginal").innerHTML = "View Edited";
                    state = 1;
                } else {
                    document.getElementById(i + "_getOriginal").innerHTML = "View Original";
                    state = 0;
                }

                document.getElementById(i + "_original_status").value = state;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("POST", ServletName + "?actionType=getDTO&ID=" + idToSubmit + "&isPermanentTable=" + isPermanentTable, true);
        xhttp.send();
    }


    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Medicine_generic_nameServlet");
    }

    function init(row) {


    }


    function submitAjax(i, deletedStyle) {
        console.log('submitAjax called');
        var isSubmittable = PreprocessBeforeSubmiting(i, "inplaceedit");
        if (isSubmittable == false) {
            return;
        }
        var formData = new FormData();
        var value;
        value = document.getElementById('iD_hidden_' + i).value;
        console.log('submitAjax i = ' + i + ' id = ' + value);
        formData.append('iD', value);
        formData.append("identity", value);
        formData.append("ID", value);
        formData.append('nameEn', document.getElementById('nameEn_text_' + i).value);
        formData.append('nameBn', document.getElementById('nameBn_text_' + i).value);
        formData.append('isDeleted', document.getElementById('isDeleted_hidden_' + i).value);
        formData.append('lastModificationTime', document.getElementById('lastModificationTime_hidden_' + i).value);

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText != '') {
                    document.getElementById('tr_' + i).innerHTML = this.responseText;
                    ShowExcelParsingResult(i);
                } else {
                    console.log("No Response");
                    document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
                }
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("POST", 'Medicine_generic_nameServlet?actionType=edit&inplacesubmit=true&isPermanentTable=<%=isPermanentTable%>&deletedStyle=' + deletedStyle + '&rownum=' + i, true);
        xhttp.send(formData);
    }

    window.onload = function () {
        ShowExcelParsingResult('general');
    }
</script>
			