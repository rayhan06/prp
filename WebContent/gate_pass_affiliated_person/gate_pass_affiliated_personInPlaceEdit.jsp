<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="gate_pass_affiliated_person.Gate_pass_affiliated_personDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Gate_pass_affiliated_personDTO gate_pass_affiliated_personDTO = (Gate_pass_affiliated_personDTO)request.getAttribute("gate_pass_affiliated_personDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(gate_pass_affiliated_personDTO == null)
{
	gate_pass_affiliated_personDTO = new Gate_pass_affiliated_personDTO();
	
}
System.out.println("gate_pass_affiliated_personDTO = " + gate_pass_affiliated_personDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.GATE_PASS_AFFILIATED_PERSON_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>

			
<%=("<td id = '" + i + "_id" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='id' id = 'id_hidden_<%=i%>' value='<%=gate_pass_affiliated_personDTO.id%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_gatePassId" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='gatePassId' id = 'gatePassId_hidden_<%=i%>' value='<%=gate_pass_affiliated_personDTO.gatePassId%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_credentialType" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='credentialType' id = 'credentialType_select_<%=i%>' value='<%=gate_pass_affiliated_personDTO.credentialType%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_credentialNo'>")%>
			
	
	<div class="form-inline" id = 'credentialNo_div_<%=i%>'>
		<input type='text' class='form-control'  name='credentialNo' id = 'credentialNo_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + gate_pass_affiliated_personDTO.credentialNo + "'"):("'" + "" + "'")%>
   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_mobileNumber'>")%>
			
	
	<div class="form-inline" id = 'mobileNumber_div_<%=i%>'>
		<input type='text' class='form-control'  name='mobileNumber' id = 'mobileNumber_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + gate_pass_affiliated_personDTO.mobileNumber + "'"):("'" + "880" + "'")%>
 <% 
	if(!actionName.equals("edit"))
	{
%>
		required="required"  pattern="880[0-9]{10}" title="mobileNumber must start with 880, then contain 10 digits"
<%
	}
%>
  tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=gate_pass_affiliated_personDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value='<%=gate_pass_affiliated_personDTO.insertedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=gate_pass_affiliated_personDTO.modifiedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + gate_pass_affiliated_personDTO.isDeleted + "'"):("'" + "false" + "'")%>
 tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=gate_pass_affiliated_personDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Gate_pass_affiliated_personServlet?actionType=view&ID=<%=gate_pass_affiliated_personDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Gate_pass_affiliated_personServlet?actionType=view&modal=1&ID=<%=gate_pass_affiliated_personDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	