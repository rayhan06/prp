<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="employee_office_hour.Employee_office_hourDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Employee_office_hourDTO employee_office_hourDTO = (Employee_office_hourDTO)request.getAttribute("employee_office_hourDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(employee_office_hourDTO == null)
{
	employee_office_hourDTO = new Employee_office_hourDTO();
	
}
System.out.println("employee_office_hourDTO = " + employee_office_hourDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=employee_office_hourDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_officeHourCat'>")%>
			
	
	<div class="form-inline" id = 'officeHourCat_div_<%=i%>'>
		<select class='form-control'  name='officeHourCat' id = 'officeHourCat_category_<%=i%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "office_hour", employee_office_hourDTO.officeHourCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "office_hour", -1);			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_startTime'>")%>
			
	
	<div class="form-inline" id = 'startTime_div_<%=i%>'>
			<%
                 value = "";
                 if(actionName.equals("edit")) {
                	 value = TimeFormat.getInAmPmFormat(employee_office_hourDTO.startTime);
                 }
             %>	
			<div class='input-group date edms-datetimepicker'>
                <input type='text' class="form-control" value="<%=value%>" id = 'startTime_time_<%=i%>' name='startTime'  tag='pb_html'/>
                <span class="input-group-addon" style="width:45px;">
                   <span><i class="fa fa-clock-o"></i></span>
                </span>
            </div>	
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_endTime'>")%>
			
	
	<div class="form-inline" id = 'endTime_div_<%=i%>'>
			<%
                 value = "";
                 if(actionName.equals("edit")) {
                	 value = TimeFormat.getInAmPmFormat(employee_office_hourDTO.endTime);
                 }
             %>	
			<div class='input-group date edms-datetimepicker'>
                <input type='text' class="form-control" value="<%=value%>" id = 'endTime_time_<%=i%>' name='endTime'  tag='pb_html'/>
                <span class="input-group-addon" style="width:45px;">
                   <span><i class="fa fa-clock-o"></i></span>
                </span>
            </div>	
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_startDate'>")%>
			
	
	<div class="form-inline" id = 'startDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'startDate_date_<%=i%>' name='startDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_startDate = dateFormat.format(new Date(employee_office_hourDTO.startDate));
	%>
	'<%=formatted_startDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_endDate'>")%>
			
	
	<div class="form-inline" id = 'endDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'endDate_date_<%=i%>' name='endDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_endDate = dateFormat.format(new Date(employee_office_hourDTO.endDate));
	%>
	'<%=formatted_endDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isActive'>")%>
			
	
	<div class="form-inline" id = 'isActive_div_<%=i%>'>
		<input type='checkbox' class='form-control'  name='isActive' id = 'isActive_checkbox_<%=i%>' value='true' <%=(actionName.equals("edit") && String.valueOf(employee_office_hourDTO.isActive).equals("true"))?("checked"):""%>   tag='pb_html'><br>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy'>")%>
			

		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + employee_office_hourDTO.insertedBy + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + employee_office_hourDTO.modifiedBy + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + employee_office_hourDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=employee_office_hourDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Employee_office_hourServlet?actionType=view&ID=<%=employee_office_hourDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Employee_office_hourServlet?actionType=view&modal=1&ID=<%=employee_office_hourDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	