<%@page pageEncoding="UTF-8" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page import="pb.*" %>
<%
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<td>
    <%= CatDAO.getName(Language, "office_hour", employee_office_hourDTO.officeHourCat)%>
</td>


<td>
    <%=employee_office_hourDTO.startTime%>
</td>


<td>
    <%=employee_office_hourDTO.endTime%>
</td>


<td>
    <%=simpleDateFormat.format(new Date(Long.parseLong(employee_office_hourDTO.startDate + "")))%>
</td>


<td>

    <%=simpleDateFormat.format(new Date(Long.parseLong(employee_office_hourDTO.endDate + "")))%>
</td>


<td>
    <%=employee_office_hourDTO.isActive%>
</td>


<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Employee_office_hourServlet?actionType=view&ID=<%=employee_office_hourDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Employee_office_hourServlet?actionType=getEditPage&ID=<%=employee_office_hourDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=employee_office_hourDTO.iD%>'/></span>
    </div>
</td>
																						
											

