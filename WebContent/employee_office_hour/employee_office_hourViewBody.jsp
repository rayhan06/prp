

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="employee_office_hour.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>

<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
Employee_office_hourDAO employee_office_hourDAO = new Employee_office_hourDAO("employee_office_hour");
Employee_office_hourDTO employee_office_hourDTO = (Employee_office_hourDTO)employee_office_hourDAO.getDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
	<div class="kt-portlet">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title prp-page-title">
					<i class="fa fa-gift"></i>&nbsp;
					Employee Office Hour Details
				</h3>
			</div>
		</div>
		<div class="kt-portlet__body form-body">
			<h5 class="table-title">
				Employee Office Hour
			</h5>
			<div class="table-responsive">
				<table class="table table-bordered table-striped">
					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_OFFICEHOURCAT, loginDTO)%></b></td>
						<td>

							<%
								value = employee_office_hourDTO.officeHourCat + "";
							%>
							<%
								value = CatDAO.getName(Language, "office_hour", employee_office_hourDTO.officeHourCat);
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_STARTTIME, loginDTO)%></b></td>
						<td>

							<%
								value = employee_office_hourDTO.startTime + "";
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_ENDTIME, loginDTO)%></b></td>
						<td>

							<%
								value = employee_office_hourDTO.endTime + "";
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_STARTDATE, loginDTO)%></b></td>
						<td>

							<%
								value = employee_office_hourDTO.startDate + "";
							%>
							<%
								String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
							%>
							<%=formatted_startDate%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_ENDDATE, loginDTO)%></b></td>
						<td>

							<%
								value = employee_office_hourDTO.endDate + "";
							%>
							<%
								String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
							%>
							<%=formatted_endDate%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_ISACTIVE, loginDTO)%></b></td>
						<td>

							<%
								value = employee_office_hourDTO.isActive + "";
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_INSERTEDBY, loginDTO)%></b></td>
						<td>

							<%
								value = employee_office_hourDTO.insertedBy + "";
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_MODIFIEDBY, loginDTO)%></b></td>
						<td>

							<%
								value = employee_office_hourDTO.modifiedBy + "";
							%>

							<%=value%>


						</td>

					</tr>








				</table>
			</div>
		</div>
	</div>
</div>