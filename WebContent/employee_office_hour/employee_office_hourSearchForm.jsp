<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="employee_office_hour.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String Language = LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_LANGUAGE, loginDTO);
    String navigator2 = SessionConstants.NAV_EMPLOYEE_OFFICE_HOUR;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_OFFICEHOURCAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_STARTTIME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_ENDTIME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_STARTDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_ENDDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_ISACTIVE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_OFFICE_HOUR_SEARCH_EMPLOYEE_OFFICE_HOUR_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="text-center">
                <span><%="English".equalsIgnoreCase(Language) ? "All" : "সকল"%></span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody class="text-nowrap">
        <%
            List<Employee_office_hourDTO> data = (List<Employee_office_hourDTO>) session.getAttribute(SessionConstants.VIEW_EMPLOYEE_OFFICE_HOUR);

            if (data != null && data.size() > 0) {
                for (Employee_office_hourDTO employee_office_hourDTO : data) {
        %>
        <tr>
            <%@include file="employee_office_hourSearchRow.jsp" %>
        </tr>
        <% }
        } %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>


			