<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="employee_office_hour.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.HttpRequestUtils" %>
<%
    Employee_office_hourDTO employee_office_hourDTO;
    employee_office_hourDTO = (Employee_office_hourDTO) request.getAttribute("employee_office_hourDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (employee_office_hourDTO == null) {
        employee_office_hourDTO = new Employee_office_hourDTO();

    }
    System.out.println("employee_office_hourDTO = " + employee_office_hourDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle;
    if (actionName.equals("edit")) {
        formTitle = LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_EMPLOYEE_OFFICE_HOUR_EDIT_FORMNAME, loginDTO);
    } else {
        formTitle = LM.getText(LC.EMPLOYEE_OFFICE_HOUR_ADD_EMPLOYEE_OFFICE_HOUR_ADD_FORMNAME, loginDTO);
    }

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);

    String URL = "Employee_office_hourServlet?actionType=ajax_" + actionName + "&isPermanentTable=true";
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
%>
<%--
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">
		<h3 class="kt-subheader__title"> Asset Management </h3>
	</div>
</div>

<!-- end:: Subheader -->--%>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=formTitle%>
                </h3> 
            </div>
        </div>
        <form class="form-horizontal kt-form" id="employee_office_hour_form" name="bigform"
              enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #ffffff">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=employee_office_hourDTO.iD%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_OFFICEHOURCAT, loginDTO)) : (LM.getText(LC.EMPLOYEE_OFFICE_HOUR_ADD_OFFICEHOURCAT, loginDTO))%>
                                        </label>
                                        <div class="col-md-9 " id='officeHourCat_div_<%=i%>'>
                                            <select class='form-control' name='officeHourCat'
                                                    id='officeHourCat_category_<%=i%>'
                                                    tag='pb_html'>
                                                <%
                                                    if (actionName.equals("edit")) {
                                                        Options = CatDAO.getOptions(Language, "office_hour", employee_office_hourDTO.officeHourCat);
                                                    } else {
                                                        Options = CatDAO.getOptions(Language, "office_hour", -1);
                                                    }
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_STARTTIME, loginDTO)) : (LM.getText(LC.EMPLOYEE_OFFICE_HOUR_ADD_STARTTIME, loginDTO))%>
                                        </label>
                                        <div class="col-md-9 " id='startTime_div_<%=i%>'>
                                            <%
                                                value = "";
                                                if (actionName.equals("edit")) {
                                                    value = TimeFormat.getInAmPmFormat(employee_office_hourDTO.startTime);
                                                }
                                            %>
                                            <div class='input-group date edms-datetimepicker'>
                                                <input
                                                        type='text'
                                                        class="form-control"
                                                        value="<%=value%>"
                                                        id='startTime_time_<%=i%>'
                                                        name='startTime' tag='pb_html'
                                                />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </span>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_ENDTIME, loginDTO)) : (LM.getText(LC.EMPLOYEE_OFFICE_HOUR_ADD_ENDTIME, loginDTO))%>
                                        </label>
                                        <div class="col-md-9 " id='endTime_div_<%=i%>'>
                                            <%
                                                value = "";
                                                if (actionName.equals("edit")) {
                                                    value = TimeFormat.getInAmPmFormat(employee_office_hourDTO.endTime);
                                                }
                                            %>
                                            <div class='input-group date edms-datetimepicker'>
                                                <input type='text' class="form-control" value="<%=value%>"
                                                       id='endTime_time_<%=i%>'
                                                       name='endTime' tag='pb_html'
                                                />
                                                <span class="input-group-addon">
                                                        <i class="fa fa-clock-o"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_STARTDATE, loginDTO)) : (LM.getText(LC.EMPLOYEE_OFFICE_HOUR_ADD_STARTDATE, loginDTO))%>
                                        </label>
                                        <div class="col-md-9 d-flex justify-content-between align-items-center"
                                             id='startDate_div_<%=i%>'>
                                            <input type='text' class='input-width92 form-control formRequired '
                                                   readonly="readonly"
                                                   data-label="Document Date" id='start-date' name='startDate'
                                                   value='' tag='pb_html'>
                                            <button type="button"
                                                    class="btn btn-danger shadow btn-border-radius office-hour-cross-picker"
                                                    id='start-date-crs-but'>X
                                            </button>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_ENDDATE, loginDTO)) : (LM.getText(LC.EMPLOYEE_OFFICE_HOUR_ADD_ENDDATE, loginDTO))%>
                                        </label>
                                        <div class="col-md-9 d-flex justify-content-between align-items-center"
                                             id='endDate_div_<%=i%>'>
                                            <input type='text' class='input-width92 form-control formRequired '
                                                   readonly="readonly"
                                                   data-label="Document Date" id='end-date' name='endDate'
                                                   value='' tag='pb_html'>
                                            <button type="button"
                                                    class="btn btn-danger shadow btn-border-radius office-hour-cross-picker"
                                                    id='end-date-crs-but'>X
                                            </button>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_ISACTIVE, loginDTO)) : (LM.getText(LC.EMPLOYEE_OFFICE_HOUR_ADD_ISACTIVE, loginDTO))%>
                                        </label>
                                        <div class="col-1 " id='isActive_div_<%=i%>'>
                                            <input type='checkbox' class='form-control-sm mt-1' name='isActive'
                                                   id='isActive_checkbox_<%=i%>'
                                                   value='true' <%=(actionName.equals("edit") && employee_office_hourDTO.isActive==1)?("checked"):""%>
                                                   tag='pb_html'><br>
                                        </div>
                                        <div class="col-8"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                type="button" onclick="location.href = '<%=request.getHeader("referer")%>'">
                            <%
                                if (actionName.equals("edit")) {
                            %>
                            <%=LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_EMPLOYEE_OFFICE_HOUR_CANCEL_BUTTON, loginDTO)%>
                            <%
                            } else {
                            %>
                            <%=LM.getText(LC.EMPLOYEE_OFFICE_HOUR_ADD_EMPLOYEE_OFFICE_HOUR_CANCEL_BUTTON, loginDTO)%>
                            <%
                                }

                            %>
                        </button>
                        <button class="btn-sm shadow text-white border-0 submit-btn ml-2" id="submit-btn"
                                type="button" onclick="submitItem('employee_office_hour_form')">
                            <%
                                if (actionName.equals("edit")) {
                            %>
                            <%=LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_EMPLOYEE_OFFICE_HOUR_SUBMIT_BUTTON, loginDTO)%>
                            <%
                            } else {
                            %>
                            <%=LM.getText(LC.EMPLOYEE_OFFICE_HOUR_ADD_EMPLOYEE_OFFICE_HOUR_SUBMIT_BUTTON, loginDTO)%>
                            <%
                                }
                            %>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    let isEnglish = <%=isLanguageEnglish%>;

    function submitItem(id) {
        let submitFunction = () => {
            let formName = '#' + id;
            const form = $(formName);

            if (isFormValid(form)) {
                submitAjaxByData("<%=URL%>",form.serialize());
            }
        }
        if (isEnglish) {
            messageDialog('Do you want to submit?', "You won't be able to revert this!", 'success', true, 'Submit', 'Cancel', submitFunction);
        } else {
            messageDialog('সাবমিট করতে চান?', "সাবমিটের পর পরিবর্তনযোগ্য না!", 'success', true, 'সাবমিট', 'বাতিল', submitFunction);
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    $(document).ready(function () {

        dateTimeInit("<%=Language%>");

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    });


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Employee_office_hourServlet");
    }

    function init(row) {


    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;

    function isFormValid($form) {

        const jQueryValid = $form.valid();
        preprocessCheckBoxBeforeSubmitting('isActive', row);
        return jQueryValid;
    }

    $(function () {

        $("#start-date").datepicker({
            dateFormat: 'dd/mm/yy',
            yearRange: '1900:2100',
            changeYear: true,
            changeMonth: true,
            buttonText: "<i class='fa fa-calendar'></i>",
            onSelect: function (dateText) {
                $("#end-date").datepicker('option', 'minDate', dateText);
            }
        });
        $("#end-date").datepicker({
            dateFormat: 'dd/mm/yy',
            yearRange: '1900:2100',
            changeYear: true,
            changeMonth: true,
            buttonText: "<i class='fa fa-calendar'></i>"
        });


        $('#start-date-crs-but').click(() => {
            $('#start-date').val('');
            $("#end-date").datepicker('option', 'minDate', null);
        });
        $('#end-date-crs-but').click(() => {
            $("#end-date").val('');
        });


        $("#start-date").datepicker('option', 'minDate', new Date());
        $("#end-date").datepicker('option', 'minDate', new Date());

        $("#start-date").val('');
        $("#end-date").val('');
        <%
     if(actionName.equals("edit")) {
    	String startDate= dateFormat.format(new Date(employee_office_hourDTO.startDate));
    	String endDate = dateFormat.format(new Date(employee_office_hourDTO.endDate));
    	%>
        $("#start-date").val('<%=startDate%>');
        $("#end-date").val('<%=endDate%>');
        <%}
        %>
    });

</script>

<style>
    .input-width92 {
        width: 92%;
        float: left;
    }
</style>




