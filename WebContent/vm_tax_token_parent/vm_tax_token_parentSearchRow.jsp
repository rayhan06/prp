<%@page pageEncoding="UTF-8" %>

<%@page import="vm_tax_token_parent.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.VM_TAX_TOKEN_PARENT_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_VM_TAX_TOKEN_PARENT;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Vm_tax_token_parentDTO vm_tax_token_parentDTO = (Vm_tax_token_parentDTO)request.getAttribute("vm_tax_token_parentDTO");
CommonDTO commonDTO = vm_tax_token_parentDTO;
String servletName = "Vm_tax_token_parentServlet";


System.out.println("vm_tax_token_parentDTO = " + vm_tax_token_parentDTO);


int i = Integer.parseInt(request.getParameter("rownum"));

out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Vm_tax_token_parentDAO vm_tax_token_parentDAO = (Vm_tax_token_parentDAO)request.getAttribute("vm_tax_token_parentDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
	Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();
	Fiscal_yearDTO fiscal_yearDTO ;
	List<Fiscal_yearDTO> fiscal_yearDTOS = (List<Fiscal_yearDTO>) request.getAttribute("fiscal_yearDTOS");

%>
		
											<td id = '<%=i%>_fiscalYearId'>
												<%
													fiscal_yearDTO = fiscal_yearDAO.getFiscalYearFromList(fiscal_yearDTOS,vm_tax_token_parentDTO.fiscalYearId);
													if(fiscal_yearDTO!=null){
														value = Language.equalsIgnoreCase("english")?fiscal_yearDTO.nameEn:fiscal_yearDTO.nameBn ;
													}
													else{
														value="";
													}

												%>

												<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_totalTaxTokenFees'>
											<%
											value = vm_tax_token_parentDTO.totalTaxTokenFees + "";
											%>
											<%
											value = String.format("%.1f", vm_tax_token_parentDTO.totalTaxTokenFees);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_totalFitnessFees'>
											<%
											value = vm_tax_token_parentDTO.totalFitnessFees + "";
											%>
											<%
											value = String.format("%.1f", vm_tax_token_parentDTO.totalFitnessFees);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_totalFees'>
											<%
											value = vm_tax_token_parentDTO.totalFees + "";
											%>
											<%
											value = String.format("%.1f", vm_tax_token_parentDTO.totalFees);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
		
		
		
		
	

											<td>
												<button
														type="button"
														class="btn-sm border-0 shadow bg-light btn-border-radius"
														style="color: #ff6b6b;"
														onclick="location.href='Vm_tax_token_parentServlet?actionType=AllVehicleSearch&fiscalYearID=<%=vm_tax_token_parentDTO.fiscalYearId%>'"
												>
													<i class="fa fa-eye"></i>
												</button>												
											</td>
	
											<td id = '<%=i%>_Edit'>
												<button
														type="button"
														class="btn-sm border-0 shadow btn-border-radius text-white"
														style="background-color: #ff6b6b;"
														onclick="location.href='Vm_tax_tokenServlet?actionType=getNewEditPage&fiscalYearID=<%=vm_tax_token_parentDTO.fiscalYearId%>'"
												>
													<i class="fa fa-edit"></i>
												</button>																			
											</td>											
											
											
											<td id='<%=i%>_checkbox' class="text-right">
												<div class='checker'>
<%--													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=vm_tax_token_parentDTO.iD%>'/></span>--%>
														<span class='chkEdit' ><input type='checkbox' name='fiscalYearId' value='<%=vm_tax_token_parentDTO.fiscalYearId%>'/></span>
												</div>
											</td>
																						
											

