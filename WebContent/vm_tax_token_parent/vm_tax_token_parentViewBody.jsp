<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_tax_token_parent.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>


<%
    String servletName = "Vm_tax_token_parentServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.VM_TAX_TOKEN_PARENT_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Vm_tax_token_parentDAO vm_tax_token_parentDAO = new Vm_tax_token_parentDAO("vm_tax_token_parent");
//Vm_tax_token_parentDTO vm_tax_token_parentDTO = (Vm_tax_token_parentDTO)vm_tax_token_parentDAO.getDTOByID(id);
    Vm_tax_token_parentDTO vm_tax_token_parentDTO = Vm_tax_token_parentRepository.getInstance().getVm_tax_token_parentDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.VM_TAX_TOKEN_PARENT_ADD_VM_TAX_TOKEN_PARENT_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="form-body">
                <h5 class="table-title">
                    <%=LM.getText(LC.VM_TAX_TOKEN_PARENT_ADD_VM_TAX_TOKEN_PARENT_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">


                        <tr>
                            <td><b><%=LM.getText(LC.VM_TAX_TOKEN_PARENT_ADD_FISCALYEARID, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = vm_tax_token_parentDTO.fiscalYearId + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td><b><%=LM.getText(LC.VM_TAX_TOKEN_PARENT_ADD_TOTALTAXTOKENFEES, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = vm_tax_token_parentDTO.totalTaxTokenFees + "";
                                %>
                                <%
                                    value = String.format("%.1f", vm_tax_token_parentDTO.totalTaxTokenFees);
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td><b><%=LM.getText(LC.VM_TAX_TOKEN_PARENT_ADD_TOTALFITNESSFEES, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = vm_tax_token_parentDTO.totalFitnessFees + "";
                                %>
                                <%
                                    value = String.format("%.1f", vm_tax_token_parentDTO.totalFitnessFees);
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td><b><%=LM.getText(LC.VM_TAX_TOKEN_PARENT_ADD_TOTALFEES, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = vm_tax_token_parentDTO.totalFees + "";
                                %>
                                <%
                                    value = String.format("%.1f", vm_tax_token_parentDTO.totalFees);
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                    </table>
                </div>
            </div>

        </div>
    </div>
</div>