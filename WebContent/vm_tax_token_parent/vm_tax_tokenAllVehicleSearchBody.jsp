<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="vm_tax_token.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="vm_fuel_vendor.VmFuelVendorItemDAO" %>

<%
    Vm_tax_tokenDTO vm_tax_tokenDTO;
    vm_tax_tokenDTO = (Vm_tax_tokenDTO)request.getAttribute("vm_tax_tokenDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if(vm_tax_tokenDTO == null)
    {
        vm_tax_tokenDTO = new Vm_tax_tokenDTO();

    }
    System.out.println("vm_tax_tokenDTO = " + vm_tax_tokenDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
    {
        actionName = "add";
    }
    else
    {
        actionName = "allVehicleDataSubmit";
    }
    actionName = "add";
    String formTitle = LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_ADD_FORMNAME, loginDTO);
    String servletName = "Vm_tax_tokenServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if(ID == null || ID.isEmpty())
    {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_TAX_TOKEN_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;



//    Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();
//    List<Fiscal_yearDTO> fiscal_yearDTOS = fiscal_yearDAO.getAllFiscal_year(true);

    VmTaxTokenItemDAO vmTaxTokenItemDAO = new VmTaxTokenItemDAO("vm_tax_token_item");
    List<VmTaxTokenItemDTO> vmTaxTokenItemDTOS = vmTaxTokenItemDAO.getAllVmTaxTokenItemByFiscalYearIDWithDeleted(Long.valueOf(request.getParameter("fiscalYearID")));

    VmFuelVendorItemDAO vmFuelVendorItemDAO = new VmFuelVendorItemDAO();

    Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();
    //Fiscal_yearDTO fiscal_yearDTO = fiscal_yearDAO.getDTOByID(Long.valueOf(request.getParameter("fiscalYearID")));
    Fiscal_yearDTO fiscal_yearDTO = Fiscal_yearRepository.getInstance().getFiscal_yearDTOByid(Long.valueOf(request.getParameter("fiscalYearID")));




%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">

                    <%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_FISCALYEARID, loginDTO)%> <%=Utils.getDigits(fiscal_yearDTO.nameEn, Language)%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Vm_tax_token_parentServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-12 ">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>

                                        <div class="mt-4">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped text-nowrap">
                                                    <thead>
                                                    <tr>
                                                        <th><%=LM.getText(LC.FEEDBACK_QUESTION_ANSWER_SERIAL_NO, loginDTO)%></th>
                                                        <th><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLE_NUMBER, loginDTO)%></th>
                                                        <th><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLETYPECAT, loginDTO)%></th>
                                                        <th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_TAXTOKENFEES, loginDTO)%></th>
                                                        <th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_FITNESSFEES, loginDTO)%></th>
                                                        <th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_DIGITALNUMBERFEES, loginDTO)%></th>
                                                        <th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_DIGITAL_REGISTRATION_FEES, loginDTO)%></th>
                                                        <th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_VAT, loginDTO)%></th>
                                                        <th><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_TOTAL, loginDTO)%></th>
                                                        <th><%=LM.getText(LC.REPORT_LANDING_COMMENT, loginDTO)%></th>
                                                        <%--								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>--%>
<%--                                                        <th><%=LM.getText(LC.USER_SEARCH_DELETE, loginDTO)%></th>--%>
                                                    </tr>
                                                    </thead>
                                                    <%
                                                        CatRepository catRepository = CatRepository.getInstance();
                                                        List<CategoryLanguageModel> modelList = catRepository.getCategoryLanguageModelList("vehicle_type");
                                                        int count=-1;
                                                        for (VmTaxTokenItemDTO vmTaxTokenItemDTO:vmTaxTokenItemDTOS)
                                                        {

                                                            ++count;

                                                    %>

                                                    <tr>
                                                        <input type='hidden' class='form-control'  name='fiscalYearId'  value='<%=fiscal_yearDTO.id%>' tag='pb_html'/>

                                                        <input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=count%>' value='<%=vmTaxTokenItemDTO.iD%>' tag='pb_html'/>

                                                        <td id = '<%=count%>_serialNo'>
                                                            <%
                                                                //int serial = i+1;
                                                                value=(count+1)+"";
                                                            %>

                                                            <%=Utils.getDigits(value, Language)%>


                                                        </td>

                                                        <td id = '<%=count%>_vehicleNumber'>

                                                            <%

                                                                value=vmTaxTokenItemDTO.vmRegNo+"";
                                                            %>
                                                            <%=Utils.getDigits(value, Language)%>

                                                        </td>
                                                        <td id = '<%=count%>_vehicleTypeCat'>
                                                            <input type='hidden' class='form-control'  name='vehicleTypeCat' value='<%=vmTaxTokenItemDTO.vehicleTypeCat%>' tag='pb_html'/>

                                                            <%

                                                                CategoryLanguageModel model = vmFuelVendorItemDAO.getDTOFromModelCatID(modelList, Long.valueOf(vmTaxTokenItemDTO.vehicleTypeCat));
                                                                value = Language.equalsIgnoreCase("English")?model.englishText:model.banglaText;

                                                            %>
                                                            <%=value%>

                                                        </td>
                                                        <td id = '<%=count%>_taxTokenFees'>

                                                            <%if(vmTaxTokenItemDTO.isDeleted==0){%>
                                                                <input type='hidden' class='form-control'  name='totalTaxTokenFees'  value='<%=vmTaxTokenItemDTO.taxTokenFees%>' tag='pb_html'/>
                                                                <%
                                                                    value=vmTaxTokenItemDTO.taxTokenFees+"";
                                                                %>
                                                                <%=Utils.getDigits(value, Language)%>

                                                            <%}else{%>

                                                            <%}%>


                                                        </td>

                                                        <td id = '<%=count%>_fitnessFees'>

                                                            <%if(vmTaxTokenItemDTO.isDeleted==0){%>
                                                            <input type='hidden' class='form-control'  name='totalFitnessFees'  value='<%=vmTaxTokenItemDTO.fitnessFees%>' tag='pb_html'/>
                                                            <%
                                                                value=vmTaxTokenItemDTO.fitnessFees+"";
                                                            %>
                                                            <%=Utils.getDigits(value, Language)%>

                                                            <%}else{%>

                                                            <%}%>


                                                        </td>

                                                        <td id = '<%=count%>_digitalNumberPlateFees'>


                                                            <%if(vmTaxTokenItemDTO.isDeleted==0){%>
                                                            <%
                                                                value=vmTaxTokenItemDTO.digitalNumberFees+"";
                                                            %>
                                                            <%=Utils.getDigits(value, Language)%>

                                                            <%}else{%>

                                                            <%}%>


                                                        </td>

                                                        <td id = '<%=count%>_digitalRegFees'>

                                                            <%if(vmTaxTokenItemDTO.isDeleted==0){%>
                                                            <%
                                                                value= (vmTaxTokenItemDTO.digitalRegFees<1)?"0": String.valueOf(vmTaxTokenItemDTO.digitalRegFees);
                                                                //value=vmTaxTokenItemDTO.digitalRegFees+"";
                                                            %>
                                                            <%=Utils.getDigits(value, Language)%>

                                                            <%}else{%>

                                                            <%}%>


                                                        </td>

                                                        <td id = '<%=count%>_vat'>

                                                            <%if(vmTaxTokenItemDTO.isDeleted==0){%>
                                                            <%
                                                                value=Utils.getDigits(vmTaxTokenItemDTO.totalVat, Language)+" ("+ Utils.getDigits(vmTaxTokenItemDTO.vat, Language) +"%)";
                                                            %>
                                                            <%=value%>

                                                            <%}else{%>

                                                            <%}%>


                                                        </td>

                                                        <td id = '<%=count%>_total'>

                                                            <%if(vmTaxTokenItemDTO.isDeleted==0){%>

                                                            <input type='hidden' class='form-control'  name='totalFees'  value='<%=vmTaxTokenItemDTO.total%>' tag='pb_html'/>
                                                            <%
                                                                value=vmTaxTokenItemDTO.total+"";
                                                                //value=vmTaxTokenItemDTO.digitalRegFees+"";
                                                            %>
                                                            <%=Utils.getDigits(value, Language)%>

                                                            <%}else{%>

                                                            <%}%>


                                                        </td>

                                                        <td id = '<%=count%>_comment'>

                                                            <%if(vmTaxTokenItemDTO.isDeleted==0){%>

                                                            <%=LM.getText(LC.VM_TAX_TOKEN_GIVEN, loginDTO)%>

                                                            <%}else{%>

                                                            <%=LM.getText(LC.VM_TAX_TOKEN_NOT_GIVEN, loginDTO)%>

                                                            <%}%>


                                                        </td>

<%--                                                        <td id = '<%=count%>_Delete'>--%>
<%--                                                            <button--%>
<%--                                                                    type="button"--%>
<%--                                                                    class="btn-sm border-0 shadow btn-border-radius text-white"--%>
<%--                                                                    style="background-color: #ff6b6b;"--%>
<%--                                                                    id="delete_row_<%=count%>"--%>
<%--                                                                    onclick="deleteRow(this)"--%>
<%--                                                            >--%>
<%--                                                                <i class="fa fa-trash"></i>--%>
<%--                                                            </button>--%>
<%--                                                        </td>--%>


                                                    </tr>


                                                    <%}%>

                                                </table>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions text-right mb-2 mt-4">

<%--                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">--%>
<%--                        <%=LM.getText(LC.CARD_INFO_ADD_CARD_INFO_SUBMIT_BUTTON, loginDTO)%>--%>
<%--                    </button>--%>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


   function deleteRow(selectedObject){
       let idSplit = selectedObject.id.split("_");
       let curFieldIdIndex = (idSplit[idSplit.length-1]);
       let hidden_id = "iD_hidden_"+curFieldIdIndex;
       let ID = document.getElementById(hidden_id).value;
       let language = '<%=Language%>';

       let url = "Vm_tax_tokenServlet?actionType=deletedSelectedRow&ID="
           + ID +"&language=" + language ;

       //console.log("url: "+url);
       $.ajax({
           url: url,
           type: "POST",
           async: false,
           success: function (fetchedData) {
               if(fetchedData && fetchedData.valid == true){
                   $(selectedObject).closest('tr').remove();
                   toastr.success(fetchedData.errMsg);
               } else if(fetchedData && fetchedData.valid == false){
                   toastr.error(fetchedData.errMsg);
               }
           },
           error: function (error) {
               console.log(error);
           }
       });
   }


    function PreprocessBeforeSubmiting(row, validate)
    {



        for(i = 1; i < child_table_extra_id; i ++)
        {
            if(document.getElementById("isDigital_checkbox_" + i))
            {
                if(document.getElementById("isDigital_checkbox_" + i).getAttribute("processed") == null)
                {
                    preprocessCheckBoxBeforeSubmitting('isDigital', i);
                    document.getElementById("isDigital_checkbox_" + i).setAttribute("processed","1");
                }
            }
        }
        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
    {
        addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Vm_tax_tokenServlet");
    }

    function init(row)
    {


        for(i = 1; i < child_table_extra_id; i ++)
        {
        }

    }

    var row = 0;
    $(document).ready(function(){
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-VmTaxTokenItem").click(
        function(e)
        {
            e.preventDefault();
            var t = $("#template-VmTaxTokenItem");

            $("#field-VmTaxTokenItem").append(t.html());
            SetCheckBoxValues("field-VmTaxTokenItem");

            var tr = $("#field-VmTaxTokenItem").find("tr:last-child");

            tr.attr("id","VmTaxTokenItem_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function( index )
            {
                var prev_id = $( this ).attr('id');
                $( this ).attr('id', prev_id + child_table_extra_id);
                console.log( index + ": " + $( this ).attr('id') );
            });


            child_table_extra_id ++;

        });


    $("#remove-VmTaxTokenItem").click(function(e){
        var tablename = 'field-VmTaxTokenItem';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for(i = document.getElementById(tablename).childNodes.length - 1; i >= 0 ; i --)
        {
            var tr = document.getElementById(tablename).childNodes[i];
            if(tr.nodeType === Node.ELEMENT_NODE)
            {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[deletecb="true"]');
                if(checkbox.checked == true)
                {
                    tr.remove();
                }
                j ++;
            }

        }
    });


</script>






