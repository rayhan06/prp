<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_tax_token_parent.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    Vm_tax_token_parentDTO vm_tax_token_parentDTO;
    vm_tax_token_parentDTO = (Vm_tax_token_parentDTO) request.getAttribute("vm_tax_token_parentDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (vm_tax_token_parentDTO == null) {
        vm_tax_token_parentDTO = new Vm_tax_token_parentDTO();

    }
    System.out.println("vm_tax_token_parentDTO = " + vm_tax_token_parentDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.VM_TAX_TOKEN_PARENT_ADD_VM_TAX_TOKEN_PARENT_ADD_FORMNAME, loginDTO);
    String servletName = "Vm_tax_token_parentServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_TAX_TOKEN_PARENT_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Vm_tax_token_parentServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-8 offset-md-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=vm_tax_token_parentDTO.iD%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='fiscalYearId'
                                           id='fiscalYearId_hidden_<%=i%>'
                                           value='<%=vm_tax_token_parentDTO.fiscalYearId%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-right"><%=LM.getText(LC.VM_TAX_TOKEN_PARENT_ADD_TOTALTAXTOKENFEES, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                value = "";
                                                if (vm_tax_token_parentDTO.totalTaxTokenFees != -1) {
                                                    value = vm_tax_token_parentDTO.totalTaxTokenFees + "";
                                                }
                                            %>
                                            <input type='number' class='form-control' name='totalTaxTokenFees'
                                                   id='totalTaxTokenFees_number_<%=i%>' value='<%=value%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-right"><%=LM.getText(LC.VM_TAX_TOKEN_PARENT_ADD_TOTALFITNESSFEES, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                value = "";
                                                if (vm_tax_token_parentDTO.totalFitnessFees != -1) {
                                                    value = vm_tax_token_parentDTO.totalFitnessFees + "";
                                                }
                                            %>
                                            <input type='number' class='form-control' name='totalFitnessFees'
                                                   id='totalFitnessFees_number_<%=i%>' value='<%=value%>' tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-right"><%=LM.getText(LC.VM_TAX_TOKEN_PARENT_ADD_TOTALFEES, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                value = "";
                                                if (vm_tax_token_parentDTO.totalFees != -1) {
                                                    value = vm_tax_token_parentDTO.totalFees + "";
                                                }
                                            %>
                                            <input type='number' class='form-control' name='totalFees'
                                                   id='totalFees_number_<%=i%>' value='<%=value%>' tag='pb_html'>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=vm_tax_token_parentDTO.searchColumn%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-right"><%=LM.getText(LC.VM_TAX_TOKEN_PARENT_ADD_INSERTEDBY, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='insertedBy'
                                                   id='insertedBy_text_<%=i%>'
                                                   value='<%=vm_tax_token_parentDTO.insertedBy%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-right"><%=LM.getText(LC.VM_TAX_TOKEN_PARENT_ADD_MODIFIEDBY, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='modifiedBy'
                                                   id='modifiedBy_text_<%=i%>'
                                                   value='<%=vm_tax_token_parentDTO.modifiedBy%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=vm_tax_token_parentDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>' value='<%=vm_tax_token_parentDTO.isDeleted%>'
                                           tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=vm_tax_token_parentDTO.lastModificationTime%>' tag='pb_html'/>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.VM_TAX_TOKEN_PARENT_ADD_VM_TAX_TOKEN_PARENT_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.VM_TAX_TOKEN_PARENT_ADD_VM_TAX_TOKEN_PARENT_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Vm_tax_token_parentServlet");
    }

    function init(row) {


    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






