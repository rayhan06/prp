
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="mps_attendance.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="java.util.List" %>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}

String value = "";
String Language = LM.getText(LC.MPS_ATTENDANCE_EDIT_LANGUAGE, loginDTO);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


Mps_attendanceDAO mps_attendanceDAO = (Mps_attendanceDAO)request.getAttribute("mps_attendanceDAO");
List<Mps_attendanceDetails> mps_attendanceDetails = (List<Mps_attendanceDetails>) request.getAttribute("mps_attendanceDetails");
String electionDetailsId = (String) request.getAttribute("electionDetailsId");
String electionConstituencyId = (String) request.getAttribute("electionConstituencyId");
String employeeRecordsId = (String) request.getAttribute("employeeRecordsId");
String fromDate = (String) request.getAttribute("fromDate");
String toDate = (String) request.getAttribute("toDate");
String date = (String) request.getAttribute("date");
String month = (String) request.getAttribute("month");
String year = (String) request.getAttribute("year");
String parliamentSessionId = (String) request.getAttribute("parliamentSessionId");
String reportType = (String) request.getAttribute("reportType");
String attendanceDeviceId = (String) request.getAttribute("attendanceDeviceId");

String navigator2 = "";
System.out.println("navigator2 = " + navigator2);
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
System.out.println("rn2 = " + rn2);
String pageno2 = ( rn2 == null ) ? "1" : "" + rn2.getCurrentPageNo();
String totalpage2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalPages();
String totalRecords2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalRecords();
String lastSearchTime = ( rn2 == null ) ? "0" : "" + rn2.getSearchTime();
boolean isPermanentTable = true;

String ajax = request.getParameter("ajax");
boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>	

<%

if(hasAjax == false)
{
	Enumeration<String> parameterNames = request.getParameterNames();

	while (parameterNames.hasMoreElements()) 
	{

		String paramName = parameterNames.nextElement();
	   
		if(!paramName.equalsIgnoreCase("actionType"))
		{
			String[] paramValues = request.getParameterValues(paramName);
			for (int i = 0; i < paramValues.length; i++) 
			{
				String paramValue = paramValues[i];
				
				%>
				
				<%
				
			}
		}
	   

	}
}

%>
<%if(mps_attendanceDetails != null && mps_attendanceDetails.size()>0) {%>
<a class="download-excel-link btn btn-sm btn-success shadow btn-border-radius mb-3 pull-right"
   href="Mps_attendanceServlet?actionType=downloadMPAttendanceReport&electionDetailsId=<%=electionDetailsId%>&attendanceDeviceId=<%=attendanceDeviceId%>&electionConstituencyId=<%=electionConstituencyId%>&employeeRecordsId=<%=employeeRecordsId%>&fromDate=<%=fromDate%>&toDate=<%=toDate%>&date=<%=date%>&month=<%=month%>&year=<%=year%>&parliamentSessionId=<%=parliamentSessionId%>&reportType=<%=reportType%>">
	<%=LM.getText(LC.ATTENDANCE_DOWNLOAD_EXCEL, loginDTO)%>
</a>

<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped text-nowrap">
						<thead>
							<tr>
								<th><%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_CONSTITUENCYNUMBER, loginDTO)%></th>
								<th><%=LM.getText(LC.MPS_ATTENDANCE_SEARCH_ELECTIONCONSTITUENCYID, loginDTO)%></th>
								<th><%=LM.getText(LC.MPS_ATTENDANCE_SEARCH_EMPLOYEERECORDSID, loginDTO)%></th>
								<%if(reportType!=null && reportType.equals("1")) {%>
								<th><%=LM.getText(LC.ATTENDANCE_TOTAL_SESSION, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_PRESENT, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_ABSENT, loginDTO)%></th>
								<%} else { %>
								<th><%=LM.getText(LC.HM_DATE, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_INTIME, loginDTO)%></th>
								<th><%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_OUTTIME, loginDTO)%></th>
								<%} %>

							</tr>
						</thead>
						<tbody>
							<%


								try
								{

										for (int i = 0; i < mps_attendanceDetails.size(); i++)
										{
											Mps_attendanceDetails mp_attendanceDetails = mps_attendanceDetails.get(i);
																																
											
											%>
											<tr id = 'tr_<%=i%>'>
											<%
											
								%>


								<%
								    request.setAttribute("mp_attendanceDetails",mp_attendanceDetails);
								    request.setAttribute("reportType", reportType);
								%>
								
								 <jsp:include page="./mps_attendanceSearchRow.jsp">
								 		<jsp:param name="pageName" value="searchrow" />
								 		<jsp:param name="rownum" value="<%=i%>" />
								 </jsp:include>			

								
								<%

											%>
											</tr>
											<%
										}



								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>
<%}%>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />