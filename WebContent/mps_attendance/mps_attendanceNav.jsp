<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="employee_records.Employee_recordsDAO" %>
<%@ page import="political_party.Political_partyRepository" %>
<%@ page import="mps_attendance.Mps_attendanceDAO" %>
<%@ page import="parliament_session.Parliament_sessionRepository" %>
<%@ page import="attendance_device.Attendance_deviceRepository" %>


<%
    System.out.println("Inside nav.jsp");
    String url = request.getParameter("url");
    String navigator = request.getParameter("navigator");
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";
    String pageno = "";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }

    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";
    int row = 0;

    String Language = LM.getText(LC.MPS_ATTENDANCE_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    int pagination_number = 0;
    boolean isPermanentTable = true;
    System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
%>


<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1" style="">
    <div class="kt-portlet__body">
        <!-- BEGIN FORM-->
        <div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%= LM.getText(LC.ELECTION_WISE_MP_ELECTION, loginDTO) %>
                        </label>
                        <div class="col-md-8">
                            <select onchange="fetchSelectors()" class='form-control' name='electionDetailsId'
                                    id='electionDetailsId' tag='pb_html'>
                                <%= Election_detailsRepository.getInstance().buildOptions(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%= LM.getText(LC.ATTENDANCE_PARLIAMENT_SESSION, loginDTO) %>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='parliamentSessionId' id='parliamentSessionId'
                                    tag='pb_html'>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%= LM.getText(LC.ELECTION_WISE_MP_ELECTION_CONSTITUENCY, loginDTO) %>
                        </label>
                        <div class="col-md-8">
                            <select onchange="openDateRange()" class='form-control'
                                    name='electionConstituencyId' id='electionConstituencyId'
                                    tag='pb_html'>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%= LM.getText(LC.ELECTION_WISE_MP_MEMBER_OF_PARLIAMENT, loginDTO) %>
                        </label>
                        <div class="col-md-8">
                            <select onchange="onSelectMP()" class='form-control'
                                    name='employeeRecordsId' id='employeeRecordsId'
                                    tag='pb_html'>
                                <%--<%=new Employee_recordsDAO().mpMemberBuildOption(Language, 0)%>--%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="enableCheckbox col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%= LM.getText(LC.MP_ENABLE_DATE_RANGE, loginDTO) %>
                        </label>
                        <div class="col-md-8">
                            <input onchange="openDateRangeHideMonthYear()" type='checkbox'
                                   class='form-control-sm' name='isDateRangeEnable'
                                   id='isDateRangeEnable'
                                   value='true'
                                   tag='pb_html'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%=LM.getText(LC.HM_DATE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="date_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type='hidden' class='form-control' name='date' id='date' value='' tag='pb_html'/>
                        </div>
                    </div>
                </div>
                <div class="dateRange col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%=LM.getText(LC.HM_FROM, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="from_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type='hidden' class='form-control' name='from_date' id='from_date' value=''
                                   tag='pb_html'/>
                        </div>
                    </div>
                </div>
                <div class="dateRange col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%=LM.getText(LC.HM_TO, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="to_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type='hidden' class='form-control' name='to_date' id='to_date' value=''
                                   tag='pb_html'/>
                        </div>
                    </div>
                </div>
                <div class="monthYear col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%=LM.getText(LC.FREQUENCY_MONTH, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='month' id='monthId' tag='pb_html'>
                                <%= new Mps_attendanceDAO().buildOptionMonth(Language)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="monthYear col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%=LM.getText(LC.FREQUENCY_YEAR, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='year' id='yearId' tag='pb_html'>
                                <%= new Mps_attendanceDAO().buildOptionYear(Language)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%=LM.getText(LC.ATTENDANCE_REPORT_TYPE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='reportType' id='reportTypeId' tag='pb_html'>
                                <%= new Mps_attendanceDAO().buildOptionReportType(Language)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_DEVICE_ADD_ATTENDANCEDEVICEID, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='attendanceDeviceId'
                                    id='attendanceDeviceId' tag='pb_html'>
                                <%=Attendance_deviceRepository.getInstance().buildOptions(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit" class="btn shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4; color: white; border-radius: 6px!important; border-radius: 8px;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">
    $('#date_js').on('datepicker.change', () => {
        setSearchChanged();
    });

    $('#from_date_js').on('datepicker.change', () => {
        setSearchChanged();
        setMinDateById('to_date_js', getDateStringById('from_date_js'));
    });

    $('#to_date_js').on('datepicker.change', () => {
        setSearchChanged();
    });

    let language = '<%=Language%>';

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        document.getElementById('date').value = getDateStringById('date_js');
        document.getElementById('from_date').value = getDateStringById('from_date_js');
        document.getElementById('to_date').value = getDateStringById('to_date_js');
        var params = '';
        params += '&election_details_id=' + document.getElementById('electionDetailsId').value;
        params += '&election_constituency_id=' + document.getElementById('electionConstituencyId').value;
        params += '&parliament_session_id=' + document.getElementById('parliamentSessionId').value;
        params += '&employee_records_id=' + document.getElementById('employeeRecordsId').value;
        params += '&attendance_device_id=' + document.getElementById('attendanceDeviceId').value;
        params += '&month=' + document.getElementById('monthId').value;
        params += '&year=' + document.getElementById('yearId').value;
        params += '&reportType=' + document.getElementById('reportTypeId').value;
        //params +=  '&political_party_id='+ document.getElementById('politicalPartyId').value;
        params += '&date=' + getBDFormattedDate('date');
        params += '&from_date=' + getBDFormattedDate('from_date');
        params += '&to_date=' + getBDFormattedDate('to_date');

        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })


        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';


            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

    function fetchSelectors() {
        fetchElectionConstituency();
        fetchMP();
        fetchParliamentSession();
    }

    function fetchElectionConstituency() {
        document.getElementById("electionConstituencyId").innerHTML = '';
        electionDetailsId = $('#electionDetailsId').val();
        if (!electionDetailsId) {
            return;
        }
        let url = "Election_constituencyServlet?actionType=buildElectionConstituency&election_id=" + electionDetailsId + "&language=" + language;
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                console.log(fetchedData);
                document.getElementById("electionConstituencyId").innerHTML = fetchedData;
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function fetchMP() {
        document.getElementById("employeeRecordsId").innerHTML = '';
        electionDetailsId = $('#electionDetailsId').val();
        if (!electionDetailsId) {
            return;
        }
        let url = "Employee_recordsServlet?actionType=buildMP&election_id=" + electionDetailsId + "&language=" + language;
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                console.log(fetchedData);
                document.getElementById("employeeRecordsId").innerHTML = fetchedData;
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function fetchParliamentSession() {
        electionDetailsId = $('#electionDetailsId').val();
        if (!electionDetailsId) {
            return;
        }
        let url = "Parliament_sessionServlet?actionType=buildParliamentSession&election_id=" + electionDetailsId + "&language=" + language;
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                console.log(fetchedData);
                document.getElementById("parliamentSessionId").innerHTML = fetchedData;
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    /*	window.onload = function () {
            $(".dateRange").hide();
            $(".monthYear").hide();
            $(".enableCheckbox").hide();
            init();
        }*/
    function onSelectMP() {
        openDateRange();
        $("#electionConstituencyId option:selected").each(function () {
            $(this).removeAttr("selected");
        });
    }

    function openDateRange() {
        if (document.getElementById('electionConstituencyId').value != 0 || document.getElementById('employeeRecordsId').value != 0) {
            $(".enableCheckbox").show();
            $(".monthYear").show();
            $(".dateRange").hide();
            $(".multipleMp").hide();
            // document.getElementById("date").value = "";
            resetDateById('date_js');
        } else {
            $(".enableCheckbox").hide();
            $(".dateRange").hide();
            $(".monthYear").hide();
            $(".multipleMp").show();
            // document.getElementById("from_date").value = "";
            // document.getElementById("to_date").value = "";
            resetDateById('from_date_js');
            resetDateById('to_date_js');
            $("#monthId option:selected").each(function () {
                $(this).removeAttr("selected");
            });
            $("#yearId option:selected").each(function () {
                $(this).removeAttr("selected");
            });
        }
    }

    function openDateRangeHideMonthYear() {
        /*if(document.getElementById('electionConstituencyId').value!=0 || document.getElementById('employeeRecordsId').value!=0) {*/
        var isChecked = document.getElementById("isDateRangeEnable").checked;
        if (isChecked) {
            $(".dateRange").show();
            $(".monthYear").hide();
            $(".multipleMp").hide();
        } else {
            $(".monthYear").show();
            $(".dateRange").hide();
            $(".multipleMp").hide();
        }
        /*	} else {
            //	$(".enableCheckbox").hide();
                $(".dateRange").hide();
                $(".monthYear").hide();
                $(".multipleMp").show();
            }*/
    }

    function init() {

        $("#electionDetailsId").select2({
            width: '100%',
        });

        $("#electionConstituencyId").select2({
            width: '100%',
        });

        $("#employeeRecordsId").select2({
            width: '100%',
        });


    }

    $(function () {

        // $("#from_date").datepicker({
        // 	dateFormat:'dd/mm/yy',
        // 	yearRange : '1900:2100',
        // 	changeYear : true,
        // 	changeMonth : true,
        // 	buttonText: "<i class='fa fa-calendar'></i>",
        // 	onSelect:function(dateText){
        // 		$("#to_date").datepicker('option','minDate',dateText);
        // 	}
        // });
        // $("#to_date").datepicker({
        // 	dateFormat:'dd/mm/yy',
        // 	yearRange : '1900:2100',
        // 	changeYear : true,
        // 	changeMonth : true,
        // 	buttonText: "<i class='fa fa-calendar'></i>"
        // });
        // $("#date").datepicker({
        // 	dateFormat:'dd/mm/yy',
        // 	yearRange : '1900:2100',
        // 	changeYear : true,
        // 	changeMonth : true,
        // 	buttonText: "<i class='fa fa-calendar'></i>"
        // });

        setMaxDateByTimestampAndId('from_date_js', (new Date).getTime());
        setMaxDateByTimestampAndId('to_date_js', (new Date).getTime());
        setMaxDateByTimestampAndId('date_js', (new Date).getTime());
        $(".dateRange").hide();
        //$(".monthYear").hide();
        //$(".enableCheckbox").hide();
        // $("#from_date").datepicker('option','maxDate',new Date());
        // $("#to_date").datepicker('option','maxDate',new Date());
        // $("#date").datepicker('option','maxDate',new Date());

        //$("#departure-date").val('');
        //	$("#return-date").val('');
    });
</script>

