<%@page pageEncoding="UTF-8" %>

<%@page import="mps_attendance.*"%>
<%@ page import="java.text.SimpleDateFormat"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@ page import="election_constituency.Election_constituencyRepository" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="util.StringUtils" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.MPS_ATTENDANCE_EDIT_LANGUAGE, loginDTO);
Mps_attendanceDetails mp_attendanceDetails = (Mps_attendanceDetails) request.getAttribute("mp_attendanceDetails");
String reportType = (String) request.getAttribute("reportType");
int i = Integer.parseInt(request.getParameter("rownum"));
String value = "";
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

											
		
											
											<td id = '<%=i%>_electionDetailsId'>
											<%
											value = Election_constituencyRepository.getInstance().getConstituencyNumber(mp_attendanceDetails.electionConstituencyId);
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_electionConstituencyId'>
											<%
												value = Election_constituencyRepository.getInstance().getText(mp_attendanceDetails.electionConstituencyId, Language);
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_employeeRecordsId'>
											<%
											value = Employee_recordsRepository.getInstance().getEmployeeName(mp_attendanceDetails.employeeRecordsId, Language);
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
<%if(reportType!=null && reportType.equals("1")) {%>
<td id = '<%=i%>date'>
	<%=Utils.getDigits(mp_attendanceDetails.sessionDays, Language)%>
</td>


<td id = '<%=i%>_politicalPartyId'>
	<%=Utils.getDigits(mp_attendanceDetails.presentDays, Language)%>
</td>


<td id = '<%=i%>_startDate'>
	<%=Utils.getDigits(mp_attendanceDetails.absentDays, Language)%>
</td>
<%} else { %>
<td id = '<%=i%>date'>
	<%=StringUtils.getFormattedDate(Language, mp_attendanceDetails.date)%>
</td>


<td id = '<%=i%>_politicalPartyId'>
	<%
		value = mp_attendanceDetails.inTime + "";
	%>

	<%=Utils.getDigits(value, Language)%>


</td>


<td id = '<%=i%>_startDate'>
	<%
		value = mp_attendanceDetails.outTime + "";
	%>
	<%=Utils.getDigits(value, Language)%>


</td>
<%} %>

		
