

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="pi_unique_item.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Pi_unique_itemServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Pi_unique_itemDTO pi_unique_itemDTO = Pi_unique_itemDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = pi_unique_itemDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_PI_UNIQUE_ITEM_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_PI_UNIQUE_ITEM_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
			
			
			
			
			
			
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_PI_UNIQUE_ITEM_ASSIGNMENT, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_PI_UNIQUE_ITEM_ASSIGNMENT_FROMDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_PI_UNIQUE_ITEM_ASSIGNMENT_TODATE, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_PI_UNIQUE_ITEM_ASSIGNMENT_ACTIONTYPE, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_PI_UNIQUE_ITEM_ASSIGNMENT_INSERTEDBY, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_PI_UNIQUE_ITEM_ASSIGNMENT_INSERTIONTIME, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_PI_UNIQUE_ITEM_ASSIGNMENT_MODIFIEDBY, loginDTO)%></th>
							</tr>
							<%
                        	PiUniqueItemAssignmentDAO piUniqueItemAssignmentDAO = new PiUniqueItemAssignmentDAO();
                         	List<PiUniqueItemAssignmentDTO> piUniqueItemAssignmentDTOs = (List<PiUniqueItemAssignmentDTO>)piUniqueItemAssignmentDAO.getDTOsByParent("pi_unique_item_id", pi_unique_itemDTO.iD);
                         	
                         	for(PiUniqueItemAssignmentDTO piUniqueItemAssignmentDTO: piUniqueItemAssignmentDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%
											value = piUniqueItemAssignmentDTO.fromDate + "";
											%>
											<%
											String formatted_fromDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_fromDate, Language)%>
				
			
										</td>
										<td>
											<%
											value = piUniqueItemAssignmentDTO.toDate + "";
											%>
											<%
											String formatted_toDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_toDate, Language)%>
				
			
										</td>
										<td>
											<%
											value = piUniqueItemAssignmentDTO.actionType + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = piUniqueItemAssignmentDTO.insertedBy + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = piUniqueItemAssignmentDTO.insertionTime + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = piUniqueItemAssignmentDTO.modifiedBy + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
        </div>
    </div>
</div>