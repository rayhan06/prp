<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="pi_unique_item.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
Pi_unique_itemDTO pi_unique_itemDTO = new Pi_unique_itemDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	pi_unique_itemDTO = Pi_unique_itemDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = pi_unique_itemDTO;
String tableName = "pi_unique_item";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.PI_UNIQUE_ITEM_ADD_PI_UNIQUE_ITEM_ADD_FORMNAME, loginDTO);
String servletName = "Pi_unique_itemServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Pi_unique_itemServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-11">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=pi_unique_itemDTO.iD%>' tag='pb_html'/>
	
														<input type='hidden' class='form-control'  name='officeUnitId' id = 'officeUnitId_hidden_<%=i%>' value='<%=pi_unique_itemDTO.officeUnitId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='fiscalYearId' id = 'fiscalYearId_hidden_<%=i%>' value='<%=pi_unique_itemDTO.fiscalYearId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='itemId' id = 'itemId_hidden_<%=i%>' value='<%=pi_unique_itemDTO.itemId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_TRACKINGNUMBER, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='trackingNumber' id = 'trackingNumber_text_<%=i%>' value='<%=pi_unique_itemDTO.trackingNumber%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='piPurchaseItemsId' id = 'piPurchaseItemsId_hidden_<%=i%>' value='<%=pi_unique_itemDTO.piPurchaseItemsId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_STATUS, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																	value = "";
																	if(pi_unique_itemDTO.status != -1)
																	{
																	value = pi_unique_itemDTO.status + "";
																	}
																%>		
																<input type='number' class='form-control'  name='status' id = 'status_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_INSERTEDBY, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='insertedBy' id = 'insertedBy_text_<%=i%>' value='<%=pi_unique_itemDTO.insertedBy%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_INSERTIONTIME, loginDTO)%></label>
                                                            <div class="col-8">
																<%value = "insertionTime_js_" + i;%>
																<jsp:include page="/time/time.jsp">
																   <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
																   <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																   <jsp:param name="IS_AMPM" value="true"></jsp:param>
																</jsp:include>
																<input type='hidden' value="<%=pi_unique_itemDTO.insertionTime%>" name='insertionTime' id = 'insertionTime_time_<%=i%>'  tag='pb_html'/>			
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=pi_unique_itemDTO.isDeleted%>' tag='pb_html'/>
											
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_MODIFIEDBY, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='modifiedBy' id = 'modifiedBy_text_<%=i%>' value='<%=pi_unique_itemDTO.modifiedBy%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=pi_unique_itemDTO.lastModificationTime%>' tag='pb_html'/>
					
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
               <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_PI_UNIQUE_ITEM_ASSIGNMENT, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
									<tr>
										<th><%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_PI_UNIQUE_ITEM_ASSIGNMENT_FROMDATE, loginDTO)%></th>
										<th><%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_PI_UNIQUE_ITEM_ASSIGNMENT_TODATE, loginDTO)%></th>
										<th><%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_PI_UNIQUE_ITEM_ASSIGNMENT_ACTIONTYPE, loginDTO)%></th>
										<th><%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_PI_UNIQUE_ITEM_ASSIGNMENT_INSERTEDBY, loginDTO)%></th>
										<th><%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_PI_UNIQUE_ITEM_ASSIGNMENT_INSERTIONTIME, loginDTO)%></th>
										<th><%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_PI_UNIQUE_ITEM_ASSIGNMENT_MODIFIEDBY, loginDTO)%></th>
										<th><%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_PI_UNIQUE_ITEM_ASSIGNMENT_REMOVE, loginDTO)%></th>
									</tr>
								</thead>
							<tbody id="field-PiUniqueItemAssignment">
						
						
								<%
									if(actionName.equals("ajax_edit")){
										int index = -1;
										
										
										for(PiUniqueItemAssignmentDTO piUniqueItemAssignmentDTO: pi_unique_itemDTO.piUniqueItemAssignmentDTOList)
										{
											index++;
											
											System.out.println("index index = "+index);

								%>	
							
								<tr id = "PiUniqueItemAssignment_<%=index + 1%>">
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='piUniqueItemAssignment.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=piUniqueItemAssignmentDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='piUniqueItemAssignment.piUniqueItemId' id = 'piUniqueItemId_hidden_<%=childTableStartingID%>' value='<%=piUniqueItemAssignmentDTO.piUniqueItemId%>' tag='pb_html'/>
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='piUniqueItemAssignment.officeUnitId' id = 'officeUnitId_hidden_<%=childTableStartingID%>' value='<%=piUniqueItemAssignmentDTO.officeUnitId%>' tag='pb_html'/>
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='piUniqueItemAssignment.fiscalYearId' id = 'fiscalYearId_hidden_<%=childTableStartingID%>' value='<%=piUniqueItemAssignmentDTO.fiscalYearId%>' tag='pb_html'/>
									</td>
									<td>										





																<%value = "fromDate_js_" + childTableStartingID;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='piUniqueItemAssignment.fromDate' id = 'fromDate_date_<%=childTableStartingID%>' value= '<%=dateFormat.format(new Date(piUniqueItemAssignmentDTO.fromDate))%>' tag='pb_html'>
									</td>
									<td>										





																<%value = "toDate_js_" + childTableStartingID;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='piUniqueItemAssignment.toDate' id = 'toDate_date_<%=childTableStartingID%>' value= '<%=dateFormat.format(new Date(piUniqueItemAssignmentDTO.toDate))%>' tag='pb_html'>
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='piUniqueItemAssignment.piRequisitionId' id = 'piRequisitionId_hidden_<%=childTableStartingID%>' value='<%=piUniqueItemAssignmentDTO.piRequisitionId%>' tag='pb_html'/>
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='piUniqueItemAssignment.orgId' id = 'orgId_hidden_<%=childTableStartingID%>' value='<%=piUniqueItemAssignmentDTO.orgId%>' tag='pb_html'/>
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='piUniqueItemAssignment.itemId' id = 'itemId_hidden_<%=childTableStartingID%>' value='<%=piUniqueItemAssignmentDTO.itemId%>' tag='pb_html'/>
									</td>
									<td>										





																<select class='form-control'  name='piUniqueItemAssignment.actionType' id = 'actionType_select_<%=childTableStartingID%>'   tag='pb_html'>
																</select>
		
									</td>
									<td>										





																<input type='text' class='form-control'  name='piUniqueItemAssignment.insertedBy' id = 'insertedBy_text_<%=childTableStartingID%>' value='<%=piUniqueItemAssignmentDTO.insertedBy%>'   tag='pb_html'/>					
									</td>
									<td>										





																<%value = "insertionTime_js_" + childTableStartingID;%>
																<jsp:include page="/time/time.jsp">
																   <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
																   <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																   <jsp:param name="IS_AMPM" value="true"></jsp:param>
																</jsp:include>
																<input type='hidden' value="<%=piUniqueItemAssignmentDTO.insertionTime%>" name='piUniqueItemAssignment.insertionTime' id = 'insertionTime_time_<%=childTableStartingID%>'  tag='pb_html'/>			
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='piUniqueItemAssignment.isDeleted' id = 'isDeleted_hidden_<%=childTableStartingID%>' value= '<%=piUniqueItemAssignmentDTO.isDeleted%>' tag='pb_html'/>
											
									</td>
									<td>										





																<input type='text' class='form-control'  name='piUniqueItemAssignment.modifiedBy' id = 'modifiedBy_text_<%=childTableStartingID%>' value='<%=piUniqueItemAssignmentDTO.modifiedBy%>'   tag='pb_html'/>					
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='piUniqueItemAssignment.lastModificationTime' id = 'lastModificationTime_hidden_<%=childTableStartingID%>' value='<%=piUniqueItemAssignmentDTO.lastModificationTime%>' tag='pb_html'/>
									</td>
									<td>
										<span id='chkEdit'>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
												   class="form-control-sm"/>
										</span>
									</td>
								</tr>								
								<%	
											childTableStartingID ++;
										}
									}
								%>						
						
								</tbody>
							</table>
						</div>
						<div class="form-group">
								<div class="col-xs-9 text-right">
									<button
											id="add-more-PiUniqueItemAssignment"
											name="add-morePiUniqueItemAssignment"
											type="button"
											class="btn btn-sm text-white add-btn shadow">
										<i class="fa fa-plus"></i>
										<%=LM.getText(LC.HM_ADD, loginDTO)%>
									</button>
									<button
											id="remove-PiUniqueItemAssignment"
											name="removePiUniqueItemAssignment"
											type="button"
											class="btn btn-sm remove-btn shadow ml-2 pl-4">
										<i class="fa fa-trash"></i>
									</button>
								</div>
							</div>
					
							<%PiUniqueItemAssignmentDTO piUniqueItemAssignmentDTO = new PiUniqueItemAssignmentDTO();%>
					
							<template id="template-PiUniqueItemAssignment" >						
								<tr>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='piUniqueItemAssignment.iD' id = 'iD_hidden_' value='<%=piUniqueItemAssignmentDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='piUniqueItemAssignment.piUniqueItemId' id = 'piUniqueItemId_hidden_' value='<%=piUniqueItemAssignmentDTO.piUniqueItemId%>' tag='pb_html'/>
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='piUniqueItemAssignment.officeUnitId' id = 'officeUnitId_hidden_' value='<%=piUniqueItemAssignmentDTO.officeUnitId%>' tag='pb_html'/>
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='piUniqueItemAssignment.fiscalYearId' id = 'fiscalYearId_hidden_' value='<%=piUniqueItemAssignmentDTO.fiscalYearId%>' tag='pb_html'/>
									</td>
									<td>





																<%value = "fromDate_js_" + childTableStartingID;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='piUniqueItemAssignment.fromDate' id = 'fromDate_date_' value= '<%=dateFormat.format(new Date(piUniqueItemAssignmentDTO.fromDate))%>' tag='pb_html'>
									</td>
									<td>





																<%value = "toDate_js_" + childTableStartingID;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='piUniqueItemAssignment.toDate' id = 'toDate_date_' value= '<%=dateFormat.format(new Date(piUniqueItemAssignmentDTO.toDate))%>' tag='pb_html'>
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='piUniqueItemAssignment.piRequisitionId' id = 'piRequisitionId_hidden_' value='<%=piUniqueItemAssignmentDTO.piRequisitionId%>' tag='pb_html'/>
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='piUniqueItemAssignment.orgId' id = 'orgId_hidden_' value='<%=piUniqueItemAssignmentDTO.orgId%>' tag='pb_html'/>
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='piUniqueItemAssignment.itemId' id = 'itemId_hidden_' value='<%=piUniqueItemAssignmentDTO.itemId%>' tag='pb_html'/>
									</td>
									<td>





																<select class='form-control'  name='piUniqueItemAssignment.actionType' id = 'actionType_select_'   tag='pb_html'>
																</select>
		
									</td>
									<td>





																<input type='text' class='form-control'  name='piUniqueItemAssignment.insertedBy' id = 'insertedBy_text_' value='<%=piUniqueItemAssignmentDTO.insertedBy%>'   tag='pb_html'/>					
									</td>
									<td>





																<%value = "insertionTime_js_" + childTableStartingID;%>
																<jsp:include page="/time/time.jsp">
																   <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
																   <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																   <jsp:param name="IS_AMPM" value="true"></jsp:param>
																</jsp:include>
																<input type='hidden' value="<%=piUniqueItemAssignmentDTO.insertionTime%>" name='piUniqueItemAssignment.insertionTime' id = 'insertionTime_time_'  tag='pb_html'/>			
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='piUniqueItemAssignment.isDeleted' id = 'isDeleted_hidden_' value= '<%=piUniqueItemAssignmentDTO.isDeleted%>' tag='pb_html'/>
											
									</td>
									<td>





																<input type='text' class='form-control'  name='piUniqueItemAssignment.modifiedBy' id = 'modifiedBy_text_' value='<%=piUniqueItemAssignmentDTO.modifiedBy%>'   tag='pb_html'/>					
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='piUniqueItemAssignment.lastModificationTime' id = 'lastModificationTime_hidden_' value='<%=piUniqueItemAssignmentDTO.lastModificationTime%>' tag='pb_html'/>
									</td>
									<td>
											<span id='chkEdit'>
												<input type='checkbox' name='checkbox' value='' deletecb='true'
													   class="form-control-sm"/>
											</span>
									</td>
								</tr>								
						
							</template>
                        </div>
                    </div>               
                <div class="form-actions text-right mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_PI_UNIQUE_ITEM_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.PI_UNIQUE_ITEM_ADD_PI_UNIQUE_ITEM_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>				

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);

	for(i = 1; i < child_table_extra_id; i ++)
	{
		if(document.getElementById("fromDate_date_" + i))
		{
			if(document.getElementById("fromDate_date_" + i).getAttribute("processed") == null)
			{
				preprocessDateBeforeSubmitting('fromDate', i);
				document.getElementById("fromDate_date_" + i).setAttribute("processed","1");
			}
		}
		if(document.getElementById("toDate_date_" + i))
		{
			if(document.getElementById("toDate_date_" + i).getAttribute("processed") == null)
			{
				preprocessDateBeforeSubmitting('toDate', i);
				document.getElementById("toDate_date_" + i).setAttribute("processed","1");
			}
		}
	}
	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Pi_unique_itemServlet");	
}

function init(row)
{


	for(i = 1; i < child_table_extra_id; i ++)
	{
		setDateByStringAndId('fromDate_js_' + i, $('#fromDate_date_' + i).val());
		setDateByStringAndId('toDate_js_' + i, $('#toDate_date_' + i).val());
		setTimeById('insertionTime_js_' + i, $('#insertionTime_time_' + i).val(), true);
	}
	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;

	$("#add-more-PiUniqueItemAssignment").click(
        function(e) 
		{
            e.preventDefault();
            var t = $("#template-PiUniqueItemAssignment");

            $("#field-PiUniqueItemAssignment").append(t.html());
			SetCheckBoxValues("field-PiUniqueItemAssignment");
			
			var tr = $("#field-PiUniqueItemAssignment").find("tr:last-child");
			
			tr.attr("id","PiUniqueItemAssignment_" + child_table_extra_id);
			
			tr.find("[tag='pb_html']").each(function( index ) 
			{
				var prev_id = $( this ).attr('id');
				$( this ).attr('id', prev_id + child_table_extra_id);
				console.log( index + ": " + $( this ).attr('id') );
			});
			
			setDateByStringAndId('fromDate_js_' + child_table_extra_id, $('#fromDate_date_' + child_table_extra_id).val());
			setDateByStringAndId('toDate_js_' + child_table_extra_id, $('#toDate_date_' + child_table_extra_id).val());
			setTimeById('insertionTime_js_' + child_table_extra_id, $('#insertionTime_time_' + child_table_extra_id).val());

			child_table_extra_id ++;

        });

    
      $("#remove-PiUniqueItemAssignment").click(function(e){    	
	    var tablename = 'field-PiUniqueItemAssignment';
	    var i = 0;
		console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
		var element = document.getElementById(tablename);

		var j = 0;
		for(i = document.getElementById(tablename).childNodes.length - 1; i >= 0 ; i --)
		{
			var tr = document.getElementById(tablename).childNodes[i];
			if(tr.nodeType === Node.ELEMENT_NODE)
			{
				console.log("tr.childNodes.length= " + tr.childNodes.length);
				var checkbox = tr.querySelector('input[deletecb="true"]');				
				if(checkbox.checked == true)
				{
					tr.remove();
				}
				j ++;
			}
			
		}    	
    });


</script>






