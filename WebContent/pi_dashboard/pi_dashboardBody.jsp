﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.*" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.Date" %>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<%


    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String language = LM.getText(LC.TRAINING_CALENDER_EDIT_LANGUAGE, loginDTO);


%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=UtilCharacter.getDataByLanguage(language, "ড্যাশবোর্ড", "Dashboard")%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body" style="background-color: #f2f2f2">
            <div class="d-flex justify-content-between align-items-center">
                <div class="">
                    <h3 class="font-weight-normal table-title">
                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENTOFFICEUNITID, loginDTO)%>
                    </h3>
                </div>
                <div class="">
                    <input type="hidden" name='officeUnitId' id='office_units_id_input' value="1">
                    <button type="button" id="office_units_id_text"
                            class="btn btn-secondary shadow dashboard-card-border-radius overflow-hidden"
                            onclick="officeModalButtonClicked();">
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="row mt-4">
                        <div class="col-lg-12 my-3">
                            <div class="kt-portlet  shadow overflow-hidden dashboard-card-border-radius py-4"
                            >
                                <div class="row">
                                    <div class="col-12 mb-2 mt-2 px-4 mx-2">
                                        <span class="h4">
                                            <%=UtilCharacter.getDataByLanguage(language, "পণ্য চাহিদার অবস্থা",
                                                    "Item Requisition Status")%>
                                        </span>
                                    </div>
                                    <div class="col-lg-6 col-xl-4 text-center my-2">
                                        <div class="d-flex justify-content-center align-items-center">
                                            <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                                <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                                    <div class="">
                                                <span id="label_1" class="h4 text-white">

                                                </span>
                                                    </div>
                                                    <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button

                                                                        onclick="redirectToSearchPage(1)"
                                                                        type="button"
                                                                        class="btn dashboard-count text-white d-flex align-items-center justify-content-center ">
                                                                        <h3 class="mb-0" id="value_1">
                                                                        </h3>
                                                                </button>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-xl-4 text-center my-2">
                                        <div class="d-flex justify-content-center align-items-center">
                                            <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                                <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                                    <div class="">
                                                            <span id="label_2" class="h4 text-white">

                                                            </span>
                                                    </div>
                                                    <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button

                                                                        onclick="redirectToSearchPage(2)" type="button"
                                                                        class="btn dashboard-count text-white d-flex align-items-center justify-content-center">
                                                                        <h3 class="mb-0" id="value_2">
                                                                        </h3>                                                        </button>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-xl-4 text-center my-2">
                                        <div class="d-flex justify-content-center align-items-center">
                                            <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                                <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                                    <div class="">
                                                <span id="label_3" class="h4 text-white">

                                                </span>
                                                    </div>
                                                    <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button

                                                                        onclick="redirectToSearchPage(3)" type="button"
                                                                        class="btn dashboard-count text-white d-flex align-items-center justify-content-center">
                                                                        <h3 class="mb-0" id="value_3">
                                                                        </h3>                                                                </button>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-xl-4 text-center my-2">
                                        <div class="d-flex justify-content-center align-items-center">
                                            <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                                <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                                    <div class="">
                                                            <span id="label_4" class="h4 text-white">

                                                            </span>
                                                    </div>
                                                    <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button

                                                                        onclick="redirectToSearchPage(4)" type="button"
                                                                        class="btn dashboard-count text-white d-flex align-items-center justify-content-center">
                                                                        <h3 class="mb-0" id="value_4">
                                                                        </h3>                                                                </button>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-xl-4 text-center my-2">
                                        <div class="d-flex justify-content-center align-items-center">
                                            <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                                <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                                    <div class="">
                                                <span id="label_5" class="h4 text-white">

                                                </span>
                                                    </div>
                                                    <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button

                                                                        onclick="redirectToSearchPage(5)" type="button"
                                                                        class="btn dashboard-count text-white d-flex align-items-center justify-content-center">
                                                                        <h3 class="mb-0" id="value_5">
                                                                        </h3>                                                                </button>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-xl-4 text-center my-2">
                                        <div class="d-flex justify-content-center align-items-center">
                                            <div style="width: 95%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                                <div class="d-flex justify-content-between align-items-center my-1 px-3">
                                                    <div class="">
                                                            <span id="label_6" class="h4 text-white">

                                                            </span>
                                                    </div>
                                                    <div class="ml-5">
                                                            <span class="patient_count2 text-white">
                                                                <button
                                                                        onclick="redirectToSearchPage(6)" type="button"
                                                                        class="btn dashboard-count text-white d-flex align-items-center justify-content-center">
                                                                    <h3 class="mb-0" id="value_6">
                                                                    </h3>
                                                                </button>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 mb-3">
                            <div class="shadow dashboard-card-border-radius overflow-hidden">
                                <div class="">
                                    <%@include file="last_six_months_purchase_amount.jsp" %>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 mb-3">
                            <div class="shadow dashboard-card-border-radius overflow-hidden">
                                <div class="">
                                    <%@include file="last_six_months_tender_count.jsp" %>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 mb-3">
                            <div class="shadow dashboard-card-border-radius overflow-hidden">
                                <div class="">
                                    <%@include file="last_twelve_months_top_five_item.jsp" %>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>


<script type="text/javascript">
    $(document).ready(() => {
        viewOfficeIdInInput({
            name: '<%=language.equalsIgnoreCase("English")? "SPEAKER" : "মাননীয় স্পীকারের কার্যালয়"%>',
            id: 1
        });
    });

    function showCharts() {
        $('#office_units_id_modal_button').prop('disabled', true);
        $.ajax({
            url: "ProcurementDashboardServlet?actionType=ajax_data&officeUnitId=" + $("#office_units_id_input").val(),
            type: "GET",
            async: true,
            success: function (fetchedData) {
                let responseData = fetchedData;
                setPurchaseData(responseData.lastSixMonthPurchaseAmount);
                setTenderData(responseData.lastSixMonthTenderAmount);
                setFirstItemValue(responseData.reqStatus);
                setTopfiveData(responseData.topFiveItems)
                $('#office_units_id_modal_button').prop('disabled', false);
            },
            error: function (error) {
                console.log(error);
                $('#office_units_id_modal_button').prop('disabled', false);
            }
        });
    }

    let redirectStatus = [];

    function setFirstItemValue(value) {
        if (value && value.length > 0) {
            redirectStatus = [];
            for (let i = 0; i < value.length; i++) {
                $("#label_" + (i + 1)).text(value[i].month);
                $("#value_" + (i + 1)).text(value[i].countInString);
                redirectStatus.push(value[i].redirectStatus);
            }
        }
    }

    function redirectToSearchPage(val) {
        event.preventDefault();
        location.href = 'Pi_requisitionServlet?actionType=search&officeUnitId=' + $("#office_units_id_input").val()
            + redirectStatus[val - 1];
    }


    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id_input').val(selectedOffice.id);
        showCharts();
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    const officeUnitIdInput = $('#office_units_id_input');

    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        officeSearchSetSelectedOfficeLayers(officeUnitIdInput.val());
        $('#search_office_modal').modal();
    }


</script>




