<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>


<div class="kt-portlet__body" style="background-color: white">
    <div id="top_five_chart" style="height: 300px;"></div>
</div>


<script type="text/javascript">
    let topfiveData;
    function setTopfiveData(data){
        topfiveData = data;
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(topFiveChart);
    }


    function topFiveChart() {
        const data = new google.visualization.DataTable();
        data.addColumn('string', 'Type');
        data.addColumn('number', 'Cost');
        if(topfiveData){
            for(let i in topfiveData){
                data.addRows([[topfiveData[i].month,topfiveData[i].amount]]);
            }
        }
        const view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            { calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation" }
        ]);
        const options = {
            title: '<%=UtilCharacter.getDataByLanguage(language, "বিগত ১২ মাসের প্রধান ৫ পণ্য ",
             "Last Twelve months Top Five Item ")%>',
            legend: { position: "none" }
        };
        const chart = new google.visualization.ColumnChart(document.getElementById('top_five_chart'));
        chart.draw(view, options);
    }


</script>