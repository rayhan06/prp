<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>


<div class="kt-portlet__body" style="background-color: white">
    <div id="purchase_amount_column_chart" style="height: 300px;"></div>
</div>


<script type="text/javascript">
    let purchaseData;
    function setPurchaseData(data){
        purchaseData = data;
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(purchaseChart);
    }


    function purchaseChart() {
        const data = new google.visualization.DataTable();
        data.addColumn('string', 'Type');
        data.addColumn('number', 'Count');
        if(purchaseData){
            for(let i in purchaseData){
                data.addRows([[purchaseData[i].month,purchaseData[i].amount]]);
            }
        }
        const view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            { calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation" }
        ]);
        const options = {
            title: '<%=UtilCharacter.getDataByLanguage(language, "বিগত ছয় মাসের ক্রয়ের পরিমান (টাকায়)",
             "Last Six Months Purchase Amount (In Taka)")%>',
            legend: { position: "none" }
        };
        const chart = new google.visualization.ColumnChart(document.getElementById('purchase_amount_column_chart'));
        chart.draw(view, options);
    }


</script>