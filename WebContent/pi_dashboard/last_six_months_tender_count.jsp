<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>


<div class="kt-portlet__body" style="background-color: white">
    <div id="tender_count_chart" style="height: 300px;"></div>
</div>


<script type="text/javascript">
    let tenderData;
    function setTenderData(data){
        tenderData = data;
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(tenderChart);
    }


    function tenderChart() {
        const data = new google.visualization.DataTable();
        data.addColumn('string', 'Type');
        data.addColumn('number', 'Count');
        if(tenderData){
            for(let i in tenderData){
                data.addRows([[tenderData[i].month,tenderData[i].count]]);
            }
        }
        const view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            { calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation" }
        ]);
        const options = {
            title: '<%=UtilCharacter.getDataByLanguage(language, "বিগত ছয় মাসের টেন্ডার সংখ্যা ",
             "Last Six Months Purchase Tender Count")%>',
            legend: { position: "none" }
        };
        const chart = new google.visualization.ColumnChart(document.getElementById('tender_count_chart'));
        chart.draw(view, options);
    }


</script>