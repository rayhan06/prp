<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@ page import="political_party.Political_partyRepository" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="util.StringUtils" %>
<%@ page import="committees.CommitteesRepository" %>
<%@ page import="ministry_office.Ministry_officeRepository" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.MP_REPORT_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    int year = Calendar.getInstance().get(Calendar.YEAR);
    String context = request.getContextPath() + "/";
    Long electionDetailsId = StringUtils.parseNullableLong(request.getParameter("electionDetailsId"));
    Long politicalPartyId = StringUtils.parseNullableLong(request.getParameter("politicalPartyId"));
    boolean isLangEng = Language.equalsIgnoreCase("English");

%>

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>
<div class="row mx-2">
    <div id="criteriaSelectionId" class="search-criteria-div col-md-6">
        <div class="form-group row">
            <div class="col-md-3"></div>
            <div class="col-md-9">
                <button type="button"
                        class="btn btn-sm btn-info btn-info text-white shadow btn-border-radius"
                        data-toggle="modal" data-target="#select_criteria_div" id="select_criteria_btn"
                        onclick="beforeOpenCriteriaModal()">
                    <%=isLangEng ? "Criteria Select" : "ক্রাইটেরিয়া বাছাই"%>
                </button>
            </div>
        </div>
    </div>
    <div id="criteriaPaddingId" class="search-criteria-div col-md-6">
    </div>
    <div class="search-criteria-div col-md-6 userNameClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.MP_REPORT_WHERE_USERNAME, loginDTO)%>
            </label>
            <div class="col-md-9">
                <input class='form-control' name='username' id='username' type="text"/>
            </div>
        </div>
    </div>
    <div id="political_party_div" class="search-criteria-div col-md-6 politicalPartyClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.MP_REPORT_WHERE_POLITICALPARTYID, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='politicalPartyId' id='politicalPartyId' style="width: 100%">
                    <%=Political_partyRepository.getInstance().buildOptions(Language, politicalPartyId)%>
                </select>
            </div>
        </div>
    </div>
    <div class="search-criteria-div col-md-6 electionIdClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.MP_REPORT_WHERE_ELECTIONDETAILSID, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control rounded' name='electionDetailsId' id='electionDetailsId' tag='pb_html'
                        onchange="changeElection()">
                    <%=Election_detailsRepository.getInstance().buildOptions(Language, electionDetailsId)%>
                </select>
            </div>
        </div>
    </div>
    <div class="search-criteria-div col-md-6 constituencyClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.MP_REPORT_WHERE_ELECTIONCONSTITUENCYID, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control rounded' name='electionConstituencyId' id='electionConstituencyId'
                        tag='pb_html'>
                </select>
            </div>
        </div>
    </div>
    <div id="name_eng_div" class="search-criteria-div col-md-6 mpNameClass"
         style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="nameEng">
                <%=isLangEng ? "Name(English)" : "নাম(ইংরেজি)"%>
            </label>
            <div class="col-md-9">
                <input class="englishOnly form-control" type="text" name="nameEng" id="nameEng"
                       style="width: 100%"
                       placeholder="<%=isLangEng ? "Enter MP Name in English" : "সংসদ সদস্যের নাম ইংরেজিতে লিখুন"%>"
                       value="">
            </div>
        </div>
    </div>

    <div id="name_bng_div" class="search-criteria-div col-md-6 mpNameClass"
         style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="nameBng">
                <%=isLangEng ? "Name(Bangla)" : "নাম(বাংলা)"%>
            </label>
            <div class="col-md-9">
                <input class="noEnglish form-control" type="text" name="nameBng" id="nameBng"
                       style="width: 100%"
                       placeholder="<%=isLangEng ? "Enter MP Name in Bangla" : "সংসদ সদস্যের নাম বাংলাতে লিখুন"%>"
                       value="">
            </div>
        </div>
    </div>

    <div id="age_div" class="search-criteria-div col-md-6 ageClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.HR_REPORT_AGE_RANGE, loginDTO)%>
            </label>
            <div class="col-6 col-md-5">
                <input class='form-control' type="text" name='startAge' id='startAge'
                       placeholder=<%=LM.getText(LC.HR_REPORT_FROM, loginDTO)%>>
            </div>
            <div class="col-6 col-md-4">
                <input class='form-control' type="text" name='endAge' id='endAge'
                       placeholder=<%=LM.getText(LC.HR_REPORT_TO, loginDTO)%>>
            </div>
        </div>
    </div>
    <div class="search-criteria-div col-md-6 mpStatusClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.MP_REPORT_WHERE_MPSTATUSCAT, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='mpStatusCat' id='mpStatusCat'>
                    <%=CatRepository.getInstance().buildOptions("mp_status", Language, null)%>
                </select>
            </div>
        </div>
    </div>
    <div id="startDate_div" class="search-criteria-div col-md-6 durationDateClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.MP_REPORT_SELECT_STARTDATE, loginDTO)%>
            </label>
            <div class="col-md-9">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID"
                               value="start-date-js"></jsp:param>
                    <jsp:param name="LANGUAGE"
                               value="<%=Language%>"></jsp:param>
                    <jsp:param name="END_YEAR"
                               value="<%=year + 50%>"></jsp:param>
                </jsp:include>
                <input type='hidden' class='form-control'
                       id='startDate'
                       name='startDate' value=''
                       tag='pb_html'/>
            </div>
        </div>
    </div>
    <div id="endDate_div" class="search-criteria-div col-md-6 durationDateClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.MP_REPORT_SELECT_ENDDATE, loginDTO)%>
            </label>
            <div class="col-md-9">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID"
                               value="end-date-js"></jsp:param>
                    <jsp:param name="LANGUAGE"
                               value="<%=Language%>"></jsp:param>
                    <jsp:param name="END_YEAR"
                               value="<%=year + 60%>"></jsp:param>
                </jsp:include>
                <input type='hidden' class='form-control'
                       id='endDate'
                       name='endDate' value=''
                       tag='pb_html'/>
            </div>
        </div>
    </div>
    <div class="search-criteria-div col-md-6 electedCountClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.PALCEHOLDER_HOW_MANY_TIMES_ELECTED, loginDTO)%>
            </label>
            <div class="col-md-9">
                <input class='form-control onlyBanglaAndEnglishDigitOnly' name='mpTerm' id='mpTerm' type="text"/>
            </div>
        </div>
    </div>
    <div class="search-criteria-div col-md-6 committeeClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.COMMITTEES_MAPPING_ADD_COMMITTEESID, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='parliamentaryCommittee' id='parliamentaryCommittee'>
                    <%=CommitteesRepository.getInstance().buildOptions(Language, null)%>
                </select>
            </div>
        </div>
    </div>
    <div class="search-criteria-div col-md-6 ministryOfficeClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.MINISTRY_OFFICE_MAPPING_ADD_MINISTRYOFFICEID, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='ministryOffice' id='ministryOffice'>
                    <%=Ministry_officeRepository.getInstance().buildOptions(Language, null)%>
                </select>
            </div>
        </div>
    </div>
</div>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script type="text/javascript">
    let language = '<%=Language%>';
    $(document).ready(() => {
        showFooter = false;
        notSeniorBase = false;
    });

    function init() {
        $('#startAge').keydown(function (e) {
            return inputValidationForIntValue(e, $(this), 999);
        });
        $('#endAge').keydown(function (e) {
            return inputValidationForIntValue(e, $(this), 999);
        });
        select2SingleSelector('#politicalPartyId', '<%=Language%>');
        select2SingleSelector('#electionDetailsId', '<%=Language%>');
        select2SingleSelector('#electionConstituencyId', '<%=Language%>');
        select2SingleSelector('#mpStatusCat', '<%=Language%>');
        select2SingleSelector('#parliamentaryCommittee', '<%=Language%>');
        select2SingleSelector('#ministryOffice', '<%=Language%>');
        changeElection();
        criteriaArray = [
            {
                class: 'userNameClass',
                title: '<%=LM.getText(LC.MP_REPORT_WHERE_USERNAME, loginDTO)%>',
                show: false
            },
            {
                class: 'politicalPartyClass',
                title: '<%=LM.getText(LC.MP_REPORT_WHERE_POLITICALPARTYID, loginDTO)%>',
                show: false
            },
            {
                class: 'electionIdClass',
                title: '<%=LM.getText(LC.MP_REPORT_WHERE_ELECTIONDETAILSID, loginDTO)%>',
                show: false
            },
            {
                class: 'constituencyClass',
                title: '<%=LM.getText(LC.MP_REPORT_WHERE_ELECTIONCONSTITUENCYID, loginDTO)%>',
                show: false
            },
            {
                class: 'mpNameClass',
                title: '<%=isLangEng ? "MP Name" : "সংসদ সদস্যের নাম"%>',
                show: false
            },
            {class: 'ageClass', title: '<%=LM.getText(LC.HR_REPORT_AGE_RANGE, loginDTO)%>', show: false},
            {
                class: 'mpStatusClass',
                title: '<%=LM.getText(LC.MP_REPORT_WHERE_MPSTATUSCAT, loginDTO)%>',
                show: false
            },
            {
                class: 'durationDateClass',
                title: '<%=isLangEng ? "Date Range" : "সময়সীমা"%>',
                show: false,
                specialCategory: 'date',
                datejsIds: ['start-date-js', 'end-date-js']
            },
            {
                class: 'electedCountClass',
                title: '<%=LM.getText(LC.PALCEHOLDER_HOW_MANY_TIMES_ELECTED, loginDTO)%>',
                show: false
            },
            {
                class: 'committeeClass',
                title: '<%=LM.getText(LC.COMMITTEES_MAPPING_ADD_COMMITTEESID, loginDTO)%>',
                show: false
            },
            {
                class: 'ministryOfficeClass',
                title: '<%=LM.getText(LC.MINISTRY_OFFICE_MAPPING_ADD_MINISTRYOFFICEID, loginDTO)%>',
                show: false
            }]
    }

    function PreprocessBeforeSubmiting() {
        $('mpTerm').val(convertToEnglishNumber($('mpTerm').val()));
        $('#startDate').val(getDateTimestampById('start-date-js'));
        $('#endDate').val(getDateTimestampById('end-date-js'));
    }

    function changeElection() {
        document.getElementById("electionConstituencyId").innerHTML = '';
        electionDetailsId = $('#electionDetailsId').val();
        if (!electionDetailsId) {
            return;
        }
        let url = "Election_constituencyServlet?actionType=buildElectionConstituency&election_id=" + electionDetailsId + "&language=" + language;
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                console.log(fetchedData);
                document.getElementById("electionConstituencyId").innerHTML = fetchedData;
                select2SingleSelector('#electionConstituencyId', '<%=Language%>');
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function convertToEnglishNumber(input) {
        let numbers = {
            '০': 0,
            '১': 1,
            '২': 2,
            '৩': 3,
            '৪': 4,
            '৫': 5,
            '৬': 6,
            '৭': 7,
            '৮': 8,
            '৯': 9
        };
        let output = [];
        for (let i = 0; i < input.length; ++i) {
            if (numbers.hasOwnProperty(input[i])) {
                output.push(numbers[input[i]]);
            } else {
                output.push(input[i]);
            }
        }
        return output.join('');
    }

    $(".onlyBanglaAndEnglishDigitOnly").keypress(event => {
        let ew = event.which;
        if (48 <= ew && ew <= 57) {
            return true;
        }
        return "০১২৩৪৫৬৭৮৯".includes(String.fromCharCode(ew));
    });
</script>