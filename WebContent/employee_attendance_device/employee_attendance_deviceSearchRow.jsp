<%@page import="attendance_device.Attendance_deviceDAO"%>
<%@page import="employee_records.Employee_recordsRepository"%>
<%@page pageEncoding="UTF-8" %>

<%@page import="employee_attendance_device.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="files.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EMPLOYEE_ATTENDANCE_DEVICE_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_EMPLOYEE_ATTENDANCE_DEVICE;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Employee_attendance_deviceDTO employee_attendance_deviceDTO = (Employee_attendance_deviceDTO) request.getAttribute("employee_attendance_deviceDTO");
    CommonDTO commonDTO = employee_attendance_deviceDTO;
    String servletName = "Employee_attendance_deviceServlet";


    System.out.println("employee_attendance_deviceDTO = " + employee_attendance_deviceDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Employee_attendance_deviceDAO employee_attendance_deviceDAO = (Employee_attendance_deviceDAO) request.getAttribute("employee_attendance_deviceDAO");

    FilesDAO filesDAO = new FilesDAO();

    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_employeeRecordsId'>
    <%=Employee_recordsRepository.getInstance().getEmployeeName(employee_attendance_deviceDTO.employeeRecordsId, Language)%>
</td>


<td id='<%=i%>_attendanceDeviceId'>

    <%=Attendance_deviceDAO.getInstance().getDeviceName(employee_attendance_deviceDTO.attendanceDeviceId, Language)%>


</td>


<td id='<%=i%>_cardNumber'>
    <%
        value = employee_attendance_deviceDTO.cardNumber + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_filesDropzone'>
    <%
        value = employee_attendance_deviceDTO.filesDropzone + "";
    %>
    <%
        {
            List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(employee_attendance_deviceDTO.filesDropzone);
    %>
    <table>
        <tr>
            <%
                if (FilesDTOList != null) {
                    for (int j = 0; j < FilesDTOList.size(); j++) {
                        FilesDTO filesDTO = FilesDTOList.get(j);
                        byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
            %>
            <td>
                <%
                    if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                %>
                <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px'/>
                <%
                    }
                %>
                <a href='Employee_attendance_deviceServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                   download><%=filesDTO.fileTitle%>
                </a>
            </td>
            <%
                    }
                }
            %>
        </tr>
    </table>
    <%
        }
    %>


</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Employee_attendance_deviceServlet?actionType=view&ID=<%=employee_attendance_deviceDTO.iD%>'"
    >
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button class="btn-sm border-0 shadow btn-border-radius text-white" style="background-color: #ff6b6b;"
            onclick="location.href='Employee_attendance_deviceServlet?actionType=getEditPage&ID=<%=employee_attendance_deviceDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=employee_attendance_deviceDTO.iD%>'/></span>
    </div>
</td>
																						
											

