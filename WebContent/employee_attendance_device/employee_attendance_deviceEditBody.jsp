<%@page import="attendance_device.Attendance_deviceRepository" %>
<%@page import="employee_records.Employee_recordsDAO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="employee_attendance_device.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="employee_records.EmployeeFlatInfoDAO" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="workflow.WorkflowController" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    Employee_attendance_deviceDTO employee_attendance_deviceDTO;
    employee_attendance_deviceDTO = (Employee_attendance_deviceDTO) request.getAttribute("employee_attendance_deviceDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (employee_attendance_deviceDTO == null) {
        employee_attendance_deviceDTO = new Employee_attendance_deviceDTO();

    }
    System.out.println("employee_attendance_deviceDTO = " + employee_attendance_deviceDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    Employee_recordsDTO employee_recordsDTO = null;
    if (actionName.equals("edit")) {
        employee_recordsDTO = new Employee_recordsDAO().getEmployee_recordsDTOByID(employee_attendance_deviceDTO.employeeRecordsId);
    }
    String formTitle = LM.getText(LC.EMPLOYEE_ATTENDANCE_DEVICE_ADD_EMPLOYEE_ATTENDANCE_DEVICE_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
%>
<%
    String Language = LM.getText(LC.EMPLOYEE_ATTENDANCE_DEVICE_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    boolean isLanguageEnglish = Language.equals("English");
%>

<%--
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">
		<h3 class="kt-subheader__title"> Asset Management </h3>
	</div>
</div>

<!-- end:: Subheader -->--%>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i> &nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form"
              action="Employee_attendance_deviceServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                   value=<%=actionName.equals("edit")?("'" + employee_attendance_deviceDTO.iD + "'"):("'" + "0" + "'")%>
                           tag='pb_html'/>
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row px-3 px-md-0">
                                        <label class="col-md-4 col-lg-3 text-md-right col-form-label">
                                            <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_DEVICE_ADD_EMPLOYEERECORDSID, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-lg-9">
                                            <div  id='employeeRecordsType_div_<%=i%>'>
                                                <input type='hidden' class='form-control'
                                                       name='employeeRecordsId' id='employeeRecordsId'
                                                       tag='pb_html'/>
                                                <button type="button"
                                                        class="btn btn-primary btn-block shadow btn-border-radius mb-3"
                                                        id="tagEmp_modal_button">
                                                    <%=LM.getText(LC.HM_SELECT, userDTO)%>
                                                </button>

                                                <table class="table table-bordered table-striped">
                                                    <thead></thead>

                                                    <tbody id="tagged_emp_table">
                                                    <tr style="display: none;">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <button type="button"
                                                                    class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4"
                                                                    onclick="remove_containing_row(this,'tagged_emp_table');">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <% if (employee_recordsDTO != null) { %>
                                                    <tr>
                                                        <td><%=employee_recordsDTO.employeeNumber%>
                                                        </td>
                                                        <td><%=employee_recordsDTO.nameEng%>
                                                        </td>

                                                        <td>
                                                            <%
                                                                EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employee_recordsDTO.iD);
                                                                Office_unitsDTO officeUnitsDTO = null;
                                                                if(employeeOfficeDTO!=null){
                                                                    officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
                                                                }
                                                            %>
                                                            <%=officeUnitsDTO == null ? "" :HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng?officeUnitsDTO.unitNameEng:officeUnitsDTO.unitNameBng%>

                                                        </td>
                                                        <td>
                                                            <button type="button"
                                                                    class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4"
                                                                    onclick="remove_containing_row(this,'tagged_emp_table');">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <%}%>
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group row px-3 px-md-0">
                                        <label class="col-md-4 col-lg-3 text-md-right col-form-label">
                                            <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_DEVICE_ADD_ATTENDANCEDEVICEID, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-lg-9">
                                            <div  id='attendanceDeviceType_div_<%=i%>'>
                                                <select class='form-control' name='attendanceDeviceId'
                                                        id='attendanceDeviceId' tag='pb_html'>
                                                    <%--<%=Attendance_deviceRepository.getInstance().buildOptions(Language, employee_attendance_deviceDTO.attendanceDeviceId)%>--%>
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row px-3 px-md-0">
                                        <label class="col-md-4 col-lg-3 text-md-right col-form-label">
                                            <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_DEVICE_ADD_CARDNUMBER, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-lg-9">
                                            <div  id='cardNumber_div_<%=i%>'>
                                                <input type='text' class='form-control' name='cardNumber'
                                                       id='cardNumber_text_<%=i%>'
                                                       value=<%=actionName.equals("edit")?("'" + employee_attendance_deviceDTO.cardNumber + "'"):("'" + "" + "'")%>
                                                               tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row px-3 px-md-0">
                                        <label class="col-md-4 col-lg-3 text-md-right col-form-label">
                                            <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_DEVICE_ADD_FILESDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-lg-9">
                                            <div  id='filesDropzone_div_<%=i%>'>
                                                <%
                                                    if (actionName.equals("edit")) {
                                                        List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(employee_attendance_deviceDTO.filesDropzone);
                                                %>
                                                <table>
                                                    <tr>
                                                        <%
                                                            if (filesDropzoneDTOList != null) {
                                                                for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                                                    FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                                                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                        %>
                                                        <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                                            <%
                                                                if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                            %>
                                                            <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)
						%>' style='width:100px'/>
                                                            <%
                                                                }
                                                            %>
                                                            <a href='Employee_attendance_deviceServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                               download><%=filesDTO.fileTitle%>
                                                            </a>
                                                            <a class='btn btn-danger'
                                                               onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>
                                                        </td>
                                                        <%
                                                                }
                                                            }
                                                        %>
                                                    </tr>
                                                </table>
                                                <%
                                                    }
                                                %>

                                                <%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>
                                                <div class="dropzone"
                                                     action="Employee_attendance_deviceServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?employee_attendance_deviceDTO.filesDropzone:ColumnID%>">
                                                    <input type='file' style="display:none"
                                                           name='filesDropzoneFile'
                                                           id='filesDropzone_dropzone_File_<%=i%>'
                                                           tag='pb_html'/>
                                                </div>
                                                <input type='hidden' name='filesDropzoneFilesToDelete'
                                                       id='filesDropzoneFilesToDelete_<%=i%>'
                                                       value='' tag='pb_html'/>
                                                <input type='hidden' name='filesDropzone'
                                                       id='filesDropzone_dropzone_<%=i%>' tag='pb_html'
                                                       value='<%=actionName.equals("edit")?employee_attendance_deviceDTO.filesDropzone:ColumnID%>'/>


                                            </div>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + employee_attendance_deviceDTO.insertionDate + "'"):("'" + "0" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedBy'
                                           id='insertedBy_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + employee_attendance_deviceDTO.insertedBy + "'"):("'" + "" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='modifiedBy'
                                           id='modifiedBy_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + employee_attendance_deviceDTO.modifiedBy + "'"):("'" + "" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + employee_attendance_deviceDTO.searchColumn + "'"):("'" + "" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + employee_attendance_deviceDTO.isDeleted + "'"):("'" + "false" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + employee_attendance_deviceDTO.lastModificationTime + "'"):("'" + "0" + "'")%>
                                                   tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-3 row">
                    <div class="col-md-11 text-right">
                        <a class="btn btn-sm cancel-btn text-white shadow btn-border-radius"
                           href="<%=request.getHeader("referer")%>">
                            <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_DEVICE_ADD_EMPLOYEE_ATTENDANCE_DEVICE_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button class="btn btn-sm submit-btn text-white shadow btn-border-radius ml-2" type="submit">
                            <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_DEVICE_ADD_EMPLOYEE_ATTENDANCE_DEVICE_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>

<%--Include with Param --%>
<%--isHierarchyNeeded--%>
<%--true : restrict employee to view other employee's data based on hierarchy--%>
<%--false: starting from top office show all employee--%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">


    $(document).ready(function () {

        dateTimeInit("<%=Language%>");

        $.validator.addMethod('deviceValidator', function (value, element) {
            return value != 0;

        });
        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                attendanceDeviceId: {
                    required: true,
                    deviceValidator: true
                }
            },
            messages: {
                attendanceDeviceId: '<%=Language.equalsIgnoreCase("English") ? "Kindly enter device!" : "অনুগ্রহ করে ডিভাইস প্রদান করুন!"%>'
            }
        });

    });

    function PreprocessBeforeSubmiting(row, validate) {
        document.getElementById('employeeRecordsId').value = +added_employee_info_map.keys().next().value;
        if (!document.getElementById('employeeRecordsId').value || document.getElementById('employeeRecordsId').value == 'NaN') {
            if ("<%=Language%>" == 'English') {
                alert('Please select employee !');
            } else {
                alert('কর্মকর্তা নির্ধারণ করুন');
            }

            return false;
        }

        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Employee_attendance_deviceServlet");
    }

    function init(row) {
        $("#attendanceDeviceId_select2_0").select2({
            dropdownAutoWidth: true,
            theme: "classic"
        });

    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;

    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button

    // map to store and send added employee data
    added_employee_info_map = new Map(
        <% if(actionName.equals("edit")) {%>
        [['<%=employee_attendance_deviceDTO.employeeRecordsId%>', '']]
        <%}%>
    );

    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_emp_table', {
                info_map: added_employee_info_map,
                isSingleEntry: true,
                callBackFunction: function (data) {
                    console.log(data);
                    fetchDevice(data.officeUnitId, data.employeeRecordId, '<%=Language%>', '<%=employee_attendance_deviceDTO.attendanceDeviceId%>');
                }
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagEmp_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_emp_table';
        $('#search_emp_modal').modal();
    });

    function fetchDevice(officeUnitId, employeeRecordId, language, selectedId) {
        let url = "Employee_attendance_deviceServlet?actionType=getDevice&office_unit_id=" + officeUnitId + "&language=" + language + "&selectedId=" + selectedId + "&employeeRecordId=" + employeeRecordId;
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                console.log(fetchedData);
                document.getElementById('attendanceDeviceId').innerHTML = fetchedData;
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
</script>






