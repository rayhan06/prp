<%@page import="util.StringUtils" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%
    Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employee_service_historyDTO.employeeRecordsId);
%>

<td>
    <%=employeeRecordsDTO == null ? "" :
            (HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng ? employeeRecordsDTO.nameEng : employeeRecordsDTO.nameBng)%>
</td>

<td>
    <%=employee_service_historyDTO.designation%>
</td>

<td>
    <%=employee_service_historyDTO.department%>
</td>

<td>
    <%=StringUtils.getFormattedDate(Language, employee_service_historyDTO.servingFrom)%>
</td>

<td>
    <%=StringUtils.getFormattedDate(Language, employee_service_historyDTO.servingTo)%>
</td>

<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Employee_service_historyServlet?actionType=view&ID=<%=employee_service_historyDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td>
    <button type="button" class="btn-sm border-0 shadow btn-border-radius text-white" style="background-color: #ff6b6b;"
            onclick="location.href='Employee_service_historyServlet?actionType=getEditPage&ID=<%=employee_service_historyDTO.iD%>&empId=<%=employee_service_historyDTO.employeeRecordsId%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=employee_service_historyDTO.iD%>'/></span>
    </div>
</td>