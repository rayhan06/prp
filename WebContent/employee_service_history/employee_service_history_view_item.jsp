<%@page contentType="text/html;charset=utf-8" %>
<%@ page import="util.StringUtils" %>
<%@ page import="java.time.LocalDate" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.time.ZoneId" %>
<%@ page import="java.time.Period" %>

<td style="vertical-align: middle;text-align: left">
    <%=dto.office%>
</td>

<td style="vertical-align: middle;text-align: left">
    <%=dto.department%>
</td>

<td style="vertical-align: middle;text-align: left">
    <%=dto.designation%>
</td>

<td style="vertical-align: middle;text-align: left">
    <%=StringUtils.getFormattedDate(Language, dto.servingFrom)%>
</td>

<td style="vertical-align: middle;text-align: left">
    <%=StringUtils.getFormattedDate(Language, dto.servingTo)%>
</td>
<td style="vertical-align: middle;text-align: left">
    <%
        LocalDate serviceFrom = new Date(dto.servingFrom).toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
        LocalDate servingTo = new Date(dto.servingTo).toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
        Period p = Period.between(serviceFrom, servingTo);
        String period;
        if ("English".equalsIgnoreCase(Language)) {
            period = p.getYears() + "y " + p.getMonths() + "m " + p.getDays() + "d";
        } else {
            period = StringUtils.convertToBanNumber(p.getYears() + "ব " + p.getMonths() + "মা " + p.getDays() + "দি");
        }
    %>
    <%=period%>
</td>

<%
    if (!actionName.equals("viewSummary")) {
%>
<td style="text-align: center; vertical-align: middle;">
    <form action="Employee_service_historyServlet?isPermanentTable=true&actionType=delete&tab=4&ID=<%=dto.iD%>&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>"
          method="POST" id="tableForm_service_history<%=serviceIndex%>" enctype="multipart/form-data">
        <div class="btn-group" role="group" aria-label="Basic example">
            <button type="button" class="btn-primary" style="max-height: 30px"
                    title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_VIEW_DETAILS, loginDTO) %>"
                    onclick="location.href='Employee_service_historyServlet?actionType=view&ID=<%=dto.iD%>&tab=4&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
                <i
                        class="fa fa-eye"></i>&nbsp;
            </button>&nbsp;
            <button style="max-height: 30px" type="button" class="btn-success"
                    title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_EDIT, loginDTO)%>"
                    onclick="location.href='<%=request.getContextPath()%>/Employee_service_historyServlet?actionType=getEditPage&tab=4&ID=<%=dto.iD%>&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
                <i
                        class="fa fa-edit"></i>&nbsp;
            </button>&nbsp;
            <button style="max-height: 30px" type="button" class="btn-danger"
                    title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_DELETE, loginDTO) %>"
                    onclick="deleteItem('tableForm_service_history',<%=serviceIndex%>)"><i
                    class="fa fa-trash"></i>&nbsp;
            </button>
        </div>
    </form>
</td>
<%
    }
%>