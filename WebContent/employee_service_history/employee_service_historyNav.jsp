<%@page contentType="text/html;charset=utf-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%
    String url = "Employee_service_historyServlet?actionType=search";
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label
                                class="col-md-2 col-form-label"><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_SERVINGFROM, loginDTO)%>
                        </label>
                        <div class="col-md-10">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="serving_from_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>
                        </div>
                        <input type='hidden'
                               class='form-control'
                               id='serving_from'
                               name='serving_from'
                               value='' tag='pb_html'/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label
                                class="col-md-2 col-form-label"><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_SERVINGTO, loginDTO)%>
                        </label>
                        <div class="col-md-10">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="serving_to_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>
                        </div>
                        <input type='hidden'
                               class='form-control'
                               id='serving_to'
                               name='serving_to'
                               value='' tag='pb_html'/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label
                                class="col-md-2 col-form-label"><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_EMPLOYEERECORDSID, loginDTO)%>
                        </label>
                        <div class="col-md-10">
                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius" id="search_emp_modal_button">
                                <%=LM.getText(LC.SEARCH_BY_EMPLOYEE, loginDTO)%>
                            </button>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap mb-2">
                            <thead></thead>
                            <tbody id="search_emp_acr_table">
                            <tr style="display: none;">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <button type="button" class="btn btn-sm delete-trash-btn"
                                            onclick="remove_containing_row(this,'search_emp_acr_table');">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->
<%@include file="../common/pagination_with_go2.jsp" %>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>

<script type="text/javascript">

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=true&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        var params = 'AnyField=' + document.getElementById('anyfield').value;
        $("#serving_from").val(getDateStringById("serving_from_date_js"));
        $("#serving_to").val(getDateStringById("serving_to_date_js"));

        if ($("#serving_from").val()) {
            params += '&serving_from=' + getBDFormattedDate('serving_from');
        }
        if ($("#serving_to").val()) {
            params += '&serving_to=' + getBDFormattedDate('serving_to');
        }

        if (search_emp_acr_map.size > 0) {
            let empId = search_emp_acr_map.values().next().value.employeeRecordId;
            params += '&employee_records_id=' + empId;
        }

        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

    <%--Search Emp ACR--%>
    search_emp_acr_map = new Map();
    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map([
        ['search_emp_acr_table', {
            info_map: search_emp_acr_map,
            isSingleEntry: true,
            callBackFunction: function (empInfo) {
                console.log('callBackFunction called with latest added emp info JSON');
                console.log(empInfo);
            }
        }]
    ]);
    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#search_emp_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'search_emp_acr_table';
        $('#search_emp_modal').modal();
    });
</script>

