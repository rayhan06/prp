<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="employee_service_history.*" %>
<%@ page import="java.util.List" %>

<%@page pageEncoding="UTF-8" %>

<%
    String navigator2 = "";
    String servletName = "Employee_service_historyServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>
<%
    List<Employee_service_historyDTO> data = (List<Employee_service_historyDTO>) rn2.list;
    if (data != null && data.size() > 0) {
%>
<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.OFFICE_AND_EMPLOYEE_MANAGEMENT_EMPLOYEE_NAME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_DESIGNATION, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_DEPARTMENT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_SERVINGFROM, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_SERVINGTO, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_EMPLOYEE_SERVICE_HISTORY_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="text-center">
                <span><%=isLanguageEnglish ? "All" : "সকল"%></span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            for (Employee_service_historyDTO employee_service_historyDTO : data) {
        %>
        <tr>
            <%@include file="employee_service_historySearchRow.jsp" %>
        </tr>
        <% } %>
        </tbody>
    </table>
</div>
<%
} else {
%>
<label style="width: 100%;text-align: center;font-size: larger;font-weight: bold;color: red">
    <%=isLanguageEnglish ? "No information is found" : "কোন তথ্য পাওয়া যায় নি"%>
</label>
<%
    }
%>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>