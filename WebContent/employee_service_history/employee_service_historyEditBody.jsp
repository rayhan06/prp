<%@page import="dbm.DBMW" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="employee_service_history.Employee_service_historyDTO" %>
<%@page import="files.FilesDAO" %>
<%@page import="files.FilesDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.util.List" %>
<%@ page import="common.BaseServlet" %>

<%@include file="../pb/addInitializer2.jsp" %>

<%
    Employee_service_historyDTO employee_service_historyDTO;
    String formTitle;
    if ("ajax_edit".equals(actionName)) {
        employee_service_historyDTO = (Employee_service_historyDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
        formTitle = LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_EMPLOYEE_SERVICE_HISTORY_EDIT_FORMNAME, loginDTO);
    } else {
        employee_service_historyDTO = new Employee_service_historyDTO();
        formTitle = LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_EMPLOYEE_SERVICE_HISTORY_ADD_FORMNAME, loginDTO);
    }
    String empId = request.getParameter("empId");
    if (empId == null) {
        empId = String.valueOf(request.getAttribute("empId"));
    }
    String url = "Employee_service_historyServlet?actionType=" + actionName + "&isPermanentTable=true&empId=" + empId;
    if (request.getParameter("tab") != null) {
        url += "&tab=" + request.getParameter("tab")+"&userId="+request.getParameter("userId");
    }
%>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="service-history-form" name="bigform" action="<%=url%>">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                        <input type='hidden' class='form-control' name='iD'
                                               id='iD_hidden_<%=i%>'
                                               value='<%=employee_service_historyDTO.iD%>' tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='employeeRecordsId'
                                               id='employeeRecordsId_hidden_<%=i%>'
                                               value=<%=empId%> tag='pb_html'/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-md-right"
                                                           for='isCadreCheckbox'>
                                                        <%=isLanguageEnglish ? "Cadre" : "ক্যাডার"%>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type='checkbox' class='form-control-sm mt-0'
                                                               name='isCadre' id='isCadreCheckbox'
                                                               onchange="isCadreCheckBoxChanged(this);"
                                                                <%=employee_service_historyDTO.isCadre ? "checked" : ""%>
                                                               tag='pb_html'/><br>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 cadre-info-div-opposite"></div>

                                            <div class="col-md-6 cadre-info-div">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-md-right">
                                                        <%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_ENCARDMENTDATE, loginDTO)%>
                                                        <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <jsp:include page="/date/date.jsp">
                                                            <jsp:param name="DATE_ID"
                                                                       value="encardment-date-js"/>
                                                            <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                                        </jsp:include>

                                                        <input type='hidden' class='form-control'
                                                               id='encardment-date'
                                                               name='encardmentDate' value=''
                                                               tag='pb_html'/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6 cadre-info-div">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-md-right"
                                                           for="cadreNumber">
                                                        <%=isLanguageEnglish ? "Cadre Number" : "ক্যাডার নম্বর"%>
                                                        <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class='form-control'
                                                               name='cadreNumber' id='cadreNumber'
                                                               placeholder='<%=isLanguageEnglish?"Enter cadre number":"ক্যাডার নম্বর লিখুন"%>'
                                                               value='<%=employee_service_historyDTO.cadreNumber != null ?employee_service_historyDTO.cadreNumber  : ""%>'
                                                        >
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6 cadre-info-div">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-md-right"
                                                           for="cadreBatch">
                                                        <%=isLanguageEnglish ? "Cadre Batch Number" : "ক্যাডার ব্যাচ নম্বর"%>
                                                        <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class='form-control' name='cadreBatch'
                                                               id='cadreBatch'
                                                               placeholder='<%=isLanguageEnglish?"Enter cadre batch number":"ক্যাডার ব্যাচ নম্বর লিখুন"%>'
                                                               value='<%=employee_service_historyDTO.cadreBatch>0 ? employee_service_historyDTO.cadreBatch : ""%>'
                                                        >
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-md-right">
                                                        <%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_GAZETTEDDATE, loginDTO)%>
                                                    </label>
                                                    <div class="col-md-8" id='gazettedDate_div_<%=i%>'>
                                                        <jsp:include page="/date/date.jsp">
                                                            <jsp:param name="DATE_ID" value="gazette-date-js"/>
                                                            <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                                        </jsp:include>

                                                        <input type='hidden' class='form-control'
                                                               id='gazette-date' name='gazettedDate'
                                                               value='' tag='pb_html'/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-md-right"
                                                           for='office'>
                                                        <%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_OFFICE, loginDTO)%>
                                                        <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-8" id="office_div">
                                                        <input type="text" class='form-control'
                                                               name='office' id='office'
                                                               placeholder='<%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_PLACEHOLDER_ENTER_OFFICE_NAME, loginDTO)%>'
                                                               value='<%=employee_service_historyDTO.office == null ? "" : employee_service_historyDTO.office%>'
                                                               tag='pb_html'>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-md-right"
                                                           for='department'>
                                                        <%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_DEPARTMENT, loginDTO)%>
                                                        <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-8" id='department_div'>
                                                        <input type="text" class='form-control'
                                                               name='department' id='department'
                                                               placeholder='<%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_PLACEHOLDER_ENTER_DEPARTMENT_NAME, loginDTO)%>'
                                                               value='<%=employee_service_historyDTO.department == null ? "" : employee_service_historyDTO.department%>'
                                                               tag='pb_html'>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-md-right">
                                                        <%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_DESIGNATION, loginDTO)%>
                                                        <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-8" id="designation_div">
                                                        <input type="text" class='form-control'
                                                               name='designation' id='designation'
                                                               placeholder='<%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_PLACEHOLDER_ENTER_DESIGNATION, loginDTO)%>'
                                                               value='<%=employee_service_historyDTO.designation == null ? "" : employee_service_historyDTO.designation%>'
                                                               tag='pb_html'>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 control-label text-md-right">
                                                        <%=LM.getText(LC.GLOBAL_FROM_DATE, loginDTO)%>
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8" id='servingFrom_div_<%=i%>'>
                                                        <jsp:include page="/date/date.jsp">
                                                            <jsp:param name="DATE_ID"
                                                                       value="serving-from-date-js"></jsp:param>
                                                            <jsp:param name="LANGUAGE"
                                                                       value="<%=Language%>"></jsp:param>
                                                        </jsp:include>
                                                        <input type='hidden'
                                                               class='form-control'
                                                               id='serving-from-date'
                                                               name='servingFrom'
                                                               value='' tag='pb_html'/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-md-right">
                                                        <%=LM.getText(LC.GLOBAL_TO_DATE, loginDTO)%>
                                                        <span id="serving_to_required_star"
                                                              class="required">*</span>
                                                    </label>
                                                    <div class="col-md-8" id="serving-to-date-div">
                                                        <jsp:include page="/date/date.jsp">
                                                            <jsp:param name="DATE_ID"
                                                                       value="serving-to-date-js"></jsp:param>
                                                            <jsp:param name="LANGUAGE"
                                                                       value="<%=Language%>"></jsp:param>
                                                        </jsp:include>
                                                    </div>
                                                    <input type='hidden' class='form-control'
                                                           id='serving-to-date' name='servingTo'
                                                           value=''
                                                           tag='pb_html'/>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-md-right"
                                                           for='office'>
                                                        <%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_SALARY, loginDTO)%>
                                                        <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-8" id="salary_div">
                                                        <input type="text" class='form-control'
                                                               name='salary' id='salary'
                                                               placeholder='<%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_PLACEHOLDER_SALARY, loginDTO)%>'
                                                               value='<%=employee_service_historyDTO.salary == 0 ? "" : employee_service_historyDTO.salary%>'
                                                               tag='pb_html'>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <label class="col-md-2 col-form-label text-md-right">
                                                        <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_FILESDROPZONE, loginDTO)%>
                                                    </label>
                                                    <div class="col-md-6" id='filesDropzone_div_<%=i%>'>
                                                        <%
                                                            if (actionName.equals("ajax_edit")) {
                                                                List<FilesDTO> filesDropzoneDTOList = new FilesDAO().getMiniDTOsByFileID(employee_service_historyDTO.filesDropzone);
                                                        %>
                                                        <table>
                                                            <tr>
                                                                <%
                                                                    if (filesDropzoneDTOList != null) {
                                                                        for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                                                            FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                                                            byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                                %>
                                                                <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                                                    <%
                                                                        if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                                    %>
                                                                    <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px'/>
                                                                    <%
                                                                        }
                                                                    %>
                                                                    <a href='Employee_education_infoServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                                       download><%=filesDTO.fileTitle%>
                                                                    </a>
                                                                    <a class='btn btn-danger'
                                                                       onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>
                                                                </td>
                                                                <%
                                                                        }
                                                                    }
                                                                %>
                                                            </tr>
                                                        </table>
                                                        <%
                                                            }
                                                        %>

                                                        <%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>
                                                        <div class="dropzone"
                                                             action="Employee_education_infoServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?employee_service_historyDTO.filesDropzone:ColumnID%>">
                                                            <input type='file' style="display:none"
                                                                   name='filesDropzoneFile'
                                                                   id='filesDropzone_dropzone_File_<%=i%>'
                                                                   tag='pb_html'/>
                                                        </div>
                                                        <input type='hidden'
                                                               name='filesDropzoneFilesToDelete'
                                                               id='filesDropzoneFilesToDelete_<%=i%>'
                                                               value='' tag='pb_html'/>
                                                        <input type='hidden' name='filesDropzone'
                                                               id='filesDropzone_dropzone_<%=i%>'
                                                               tag='pb_html'
                                                               value='<%=actionName.equals("ajax_edit")?employee_service_historyDTO.filesDropzone:ColumnID%>'/>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button">
                            <%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_EMPLOYEE_SERVICE_HISTORY_CANCEL_BUTTON, userDTO)%>
                        </button>
                        <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                                onclick="submitForm()">
                            <%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_EMPLOYEE_SERVICE_HISTORY_SUBMIT_BUTTON, userDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<%=contextOfPath%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script type="text/javascript">
    const serviceForm = $('#service-history-form');

    const cadreInfoDiv = $('.cadre-info-div');
    const cadreInfoDivOpposite = $('.cadre-info-div-opposite');
    const isCadreCheckbox = $('#isCadreCheckbox');

    function isCadreCheckBoxChanged(checkBoxElem) {
        if ($(checkBoxElem).is(":checked")) {
            cadreInfoDiv.show();
            cadreInfoDivOpposite.hide();
        } else {
            cadreInfoDiv.hide();
            cadreInfoDivOpposite.show();
            resetDateById('encardment-date-js');
        }
    }

    $(document).ready(function () {

        $('#serving-from-date-js').on('datepicker.change', () => {
            setMinDateById('serving-to-date-js', getDateStringById('serving-from-date-js'));
        });

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        $('#salary').keydown(e => {
            return inputValidationForIntValue(e, $('#salary'), 9999999999);
        });

        $('#cadreBatch').keydown(e => {
            return inputValidationForIntValue(e, $('#cadreBatch'), 9999999999);
        });


        <%if (actionName.equals("ajax_edit")) {%>
        setDateByTimestampAndId('serving-from-date-js', <%=employee_service_historyDTO.servingFrom%>);
        setDateByTimestampAndId('serving-to-date-js', <%=employee_service_historyDTO.servingTo%>);
        setMinDateById('serving-to-date-js', getDateStringById('serving-from-date-js'));
        setDateByTimestampAndId('gazette-date-js', <%=employee_service_historyDTO.gazettedDate%>);

        if (isCadreCheckbox.is(":checked")) {
            setDateByTimestampAndId('encardment-date-js', <%=employee_service_historyDTO.encardmentDate%>);
        }
        <%}%>

        isCadreCheckBoxChanged(isCadreCheckbox);

        $.validator.addMethod('mandatoryIfIsCadre', function (value, element) {
            if (!isCadreCheckbox.is(":checked")) return true;
            return value !== "";
        });

        $("#service-history-form").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                office: "required",
                department: "required",
                designation: "required",
                salary: "required",
                cadreNumber: {
                    mandatoryIfIsCadre: true
                },
                cadreBatch: {
                    mandatoryIfIsCadre: true
                }
            },

            messages: {
                office: "<%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_PLEASE_ENTER_OFFICE,userDTO)%>",
                department: "<%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_PLEASE_ENTER_DEPARTMENT,userDTO)%>",
                designation: "<%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_PLEASE_ENTER_DESIGNATION,userDTO)%>",
                salary: "<%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_ENTER_SALARY,userDTO)%>",
                cadreNumber: "<%=isLanguageEnglish?"Enter cadre number":"ক্যাডার নম্বর লিখুন"%>",
                cadreBatch: "<%=isLanguageEnglish?"Enter cadre batch number":"ক্যাডার ব্যাচ নম্বর লিখুন"%>"
            }
        });
    });

    function PreprocessBeforeSubmitting() {
        $("#service-history-form").validate();
        const jQueryValid = serviceForm.valid();
        let govtDatesValid = true;
        $('#serving-from-date').val(getDateStringById('serving-from-date-js'));
        $('#serving-to-date').val(getDateStringById('serving-to-date-js'));
        $('#gazette-date').val(getDateStringById('gazette-date-js'));

        let isCadreDateValid = true;
        if (isCadreCheckbox.is(":checked")) {
            $('#encardment-date').val(getDateStringById('encardment-date-js'));
            isCadreDateValid &&= dateValidator('encardment-date-js', true, {
                'errorEn': 'Enter encadrement date!',
                'errorBn': 'ক্যাডার হবার তারিখ তারিখ প্রবেশ করান!'
            });
            isCadreCheckbox.val('true');
        } else {
            $('#encardment-date').val('');
            isCadreCheckbox.val('false');
        }

        let workingDatesValid = dateValidator('serving-to-date-js', true, {
            'errorEn': 'Enter valid service end date!',
            'errorBn': 'চাকরি শেষ হওয়ার তারিখ প্রবেশ করান!'
        });

        let workingStartDatesValid = dateValidator('serving-from-date-js', true, {
            'errorEn': 'Enter valid service start date!',
            'errorBn': 'চাকরি শুরু করার তারিখ প্রবেশ করান!'
        });

        return jQueryValid && govtDatesValid && workingStartDatesValid && workingDatesValid && isCadreDateValid;
    }

    function submitForm() {
        buttonStateChange(true);
        if (PreprocessBeforeSubmitting()) {
            submitAjaxForm('service-history-form');
        } else {
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }
</script>