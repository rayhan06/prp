<%@page contentType="text/html;charset=utf-8" %>
<%@ page import="employee_service_history.EmployeeServiceModel" %>
<%@ page import="java.util.List" %>
<%@ page import="employee_service_history.Employee_service_historyDTO" %>

<input type="hidden" data-ajax-marker="true">

<table class="table table-striped table-bordered" style="font-size: 14px">
    <thead>
    <tr>
        <th style="vertical-align: middle;text-align: left">
            <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_OFFICE, loginDTO)%>
            </b></th>
        <th style="vertical-align: middle;text-align: left">
            <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_DEPARTMENT, loginDTO)%>
            </b></th>
        <th style="vertical-align: middle;text-align: left">
            <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_DESIGNATION, loginDTO)%>
            </b></th>
        <th style="vertical-align: middle;text-align: left"><b><%=LM.getText(LC.GLOBAL_FROM_DATE, loginDTO)%>
        </b></th>
        <th style="vertical-align: middle;text-align: left"><b><%=LM.getText(LC.GLOBAL_TO_DATE, loginDTO)%>
        </b></th>
        <th style="vertical-align: middle;text-align: left"><b><%=isLanguageEnglish ? "Period" : "সময়কাল"%>
        </b></th>
        <%
            if (!actionType.equals("viewSummary")) {
        %>
        <th><b></b></th>
        <%
            }
        %>
    </tr>
    </thead>
    <tbody>
    <%
        List<Employee_service_historyDTO> savedServiceModelList = (List<Employee_service_historyDTO>) request.getAttribute("savedServiceModelList");
        int serviceIndex = 0;
        if (savedServiceModelList != null && savedServiceModelList.size() > 0) {
            for (Employee_service_historyDTO dto : savedServiceModelList) {
                ++serviceIndex;
    %>

    <tr>
        <%@include file="/employee_service_history/employee_service_history_view_item.jsp" %>
    </tr>
    <%
            }
        }
    %>
    </tbody>
</table>

<div class="row">
    <div class="col-12 text-right mt-3">
        <button class="btn btn-gray m-t-10 rounded-pill"
                onclick="location.href='<%=request.getContextPath()%>/Employee_service_historyServlet?actionType=getAddPage&tab=4&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
            <i class="fa fa-plus"></i>&nbsp;<%= LM.getText(LC.HR_MANAGEMENT_POSTING_AND_TRAINING_ADD_SERVICE_HISTORY, loginDTO) %>
        </button>
    </div>
</div>