<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="employee_service_history.*" %>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="files.*" %>
<%@ page import="util.StringUtils" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>


<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    Employee_service_historyDTO employee_service_historyDTO = (Employee_service_historyDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_ANYFIELD, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-md-10 offset-md-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background-color: white"><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_ANYFIELD, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped text-nowrap">
                                        <tr>
                                            <td>
                                                <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_GAZETTEDDATE, loginDTO)%>
                                                </b></td>
                                            <td><%=StringUtils.getFormattedDate(Language, employee_service_historyDTO.gazettedDate)%>
                                            </td>
                                        </tr>

                                        <%
                                            if (employee_service_historyDTO.isCadre) {
                                        %>
                                        <tr>
                                            <td>
                                                <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_ENCARDMENTDATE, loginDTO)%>
                                                </b></td>
                                            <td><%=StringUtils.getFormattedDate(Language, employee_service_historyDTO.encardmentDate)%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <b><%=isLanguageEnglish ? "Cadre Number" : "ক্যাডার নম্বর"%>
                                                </b></td>
                                            <td><%=isLanguageEnglish ? employee_service_historyDTO.cadreNumber : StringUtils.convertToBanNumber(employee_service_historyDTO.cadreNumber)%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <b><%=isLanguageEnglish ? "Cadre Batch Number" : "ক্যাডার ব্যাচ নম্বর"%>
                                                </b></td>
                                            <td><%=isLanguageEnglish ? employee_service_historyDTO.cadreBatch : StringUtils.convertToBanNumber(String.valueOf(employee_service_historyDTO.cadreBatch))%>
                                            </td>
                                        </tr>

                                        <%
                                            }
                                        %>

                                        <tr>
                                            <td>
                                                <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_DESIGNATION, loginDTO)%>
                                                </b></td>
                                            <td><%=employee_service_historyDTO.designation%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_DEPARTMENT, loginDTO)%>
                                                </b></td>
                                            <td><%=employee_service_historyDTO.department%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <b><%=LM.getText(LC.GLOBAL_FROM_DATE, loginDTO)%>
                                                </b></td>
                                            <td><%=StringUtils.getFormattedDate(Language, employee_service_historyDTO.servingFrom)%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <b><%=LM.getText(LC.GLOBAL_TO_DATE, loginDTO)%>
                                                </b></td>
                                            <td><%=StringUtils.getFormattedDate(Language, employee_service_historyDTO.servingTo)%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_SALARY, loginDTO)%>
                                                </b></td>
                                            <td><%=employee_service_historyDTO.salary > 0 ? isLanguageEnglish? employee_service_historyDTO.salary
                                                    : StringUtils.convertToBanNumber(String.valueOf(employee_service_historyDTO.salary))
                                                    : ""%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <b><%=LM.getText(LC.VM_INCIDENT_ADD_FILESDROPZONE, loginDTO)%>
                                                </b></td>
                                            <td>
                                                <%
                                                    {
                                                        List<FilesDTO> FilesDTOList = new FilesDAO().getMiniDTOsByFileID(employee_service_historyDTO.filesDropzone);
                                                %>
                                                <table>
                                                    <tr>
                                                        <%
                                                            if (FilesDTOList != null) {
                                                                for (int j = 0; j < FilesDTOList.size(); j++) {
                                                                    FilesDTO filesDTO = FilesDTOList.get(j);
                                                                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                        %>
                                                        <td>
                                                            <%
                                                                if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                            %> <img
                                                                src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                                style='width: 100px'/>
                                                            <%
                                                                }
                                                            %>
                                                            <a href='Employee_service_historyServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                               download><%=filesDTO.fileTitle%>
                                                            </a>
                                                        </td>
                                                        <%
                                                                }
                                                            }
                                                        %>
                                                    </tr>
                                                </table>
                                                <% } %>

                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>