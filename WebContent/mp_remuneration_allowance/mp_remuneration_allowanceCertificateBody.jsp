<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.*" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="util.*" %>
<%@ page import="static util.StringUtils.convertToBanNumber" %>
<%@ page import="static util.StringUtils.convertToBanNumber" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="mp_payroll_bill.Mp_payroll_billDTO" %>
<%@ page import="mp_payroll_bill.Mp_payroll_billModel" %>
<%@ page import="mp_payroll_allowance_configuration.Mp_payroll_allowance_configurationModel" %>
<%@ page import="mp_payroll_deduction_configuration.Mp_payroll_deduction_configurationModel" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="allowance_employee_info.AllowanceEmployeeInfoRepository" %>
<%@ page import="allowance_employee_info.AllowanceEmployeeInfoDTO" %>
<%@ page import="bank_name.Bank_nameRepository" %>
<%@ page import="budget.BudgetUtils" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberInWord" %>
<%@ page import="budget.BudgetInfo" %>
<%@ page import="static budget.BudgetUtils.getEconomicYearBeginYear" %>
<%@ page import="language.LC" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="mp_fund_application.Mp_fund_applicationServlet" %>
<%@ page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getLanguage(loginDTO);
    String pdfFileName = "MP Remuneration and Allowance Certificate";
    int gender = 1;
    long startTime = System.currentTimeMillis();
    int currentYear = Calendar.getInstance().get(Calendar.YEAR);
%>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    .fix-fill {
        overflow: hidden;
        white-space: nowrap;
    }

    #to-print-div * {
        font-size: 12px;
    }

    #to-print-div h1 {
        font-size: 16px;
        font-weight: bold;
    }

    #to-print-div h2 {
        font-size: 14px;
        font-weight: bold;
    }

    #to-print-div h3 {
        font-size: 13px;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .5in;
        background: white;
        margin-bottom: 10px;
    }

    .foot-note {
        font-size: 11px !important;
    }

    .blank-to-fill {
        display: inline-block;
        border-bottom: 1px dotted black;
        width: inherit;
    }

    .table-bordered > :not(caption) > * {
        border-color: black;
    }

    .table-bordered > :not(caption) > * > * {
        border-color: black;
    }

    .table-bordered td,
    .table-bordered th {
        padding: 5px;
    }

    .align-top {
        vertical-align: top;
    }

    th {
        text-align: center;
    }

    /* class added dynamically: don't remove if shows unused */
    .fly-in-from-down {
        animation: flyFromDown 1s ease-out;
    }

    @keyframes flyFromDown {
        0% {
            transform: translateY(200%);
        }
        100% {
            transform: translateY(0%);
        }
    }
</style>

<div class="kt-content" style="padding-bottom: 0;" id="kt_content">
    <div class="kt-portlet" style="padding-bottom: 0;">
        <form id="search-param-form">
            <div class="kt-portlet__body form-body" style="padding-bottom: 0;">
                <div class="row">
                    <div class="col-12">
                        <div class="onlyborder">
                            <div class="row mx-2">
                                <div class="col-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=Budget_mappingRepository.getInstance().getOperationTextWithoutCode(
                                                        Language, Mp_fund_applicationServlet.MP_BUDGET_OPERATION_ID
                                                )%>
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="election_details_id">
                                                    <%=LM.getText(LC.MP_FUND_AMOUNT_ADD_ELECTIONDETAILSID, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <select style='width: 100%' class='form-control'
                                                            name='election_details_id'
                                                            id='election_details_id'
                                                            onSelect='setSearchChanged()'
                                                            onchange="electionDetailsChanged(this);">
                                                        <%=Election_detailsRepository.getInstance().buildOptions(Language, null)%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="election_constituency_id">
                                                    <%=LM.getText(LC.MP_FUND_AMOUNT_ADD_ELECTIONCONSTITUENCYID, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <select class='form-control' name='election_constituency_id'
                                                            id='election_constituency_id'
                                                            onSelect='setSearchChanged()'>
                                                        <%--Dependent on election_details_id--%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">
                                                    <%=getDataByLanguage(Language, "তারিখ হতে", "Date From")%>
                                                </label>
                                                <div class="col-md-9">
                                                    <jsp:include page="/date/date.jsp">
                                                        <jsp:param name="DATE_ID" value="startDate-js"/>
                                                        <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                                    </jsp:include>
                                                    <input type="hidden" name='startDate' id='startDate' value=""/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label">
                                                    <%=getDataByLanguage(Language, "তারিখ পর্যন্ত", "Date To")%>
                                                </label>
                                                <div class="col-md-9">
                                                    <jsp:include page="/date/date.jsp">
                                                        <jsp:param name="DATE_ID" value="endDate-js"/>
                                                        <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                                        <jsp:param name="END_YEAR" value="<%=currentYear + 5%>"/>
                                                    </jsp:include>
                                                    <input type="hidden" name='endDate' id='endDate' value=""/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mt-3">
                            <div class="col-12 text-right">
                                <button class="btn-sm shadow text-white border-0 submit-btn"
                                        type="submit">
                                    <%=getDataByLanguage(Language, "প্রত্যয়নপত্র তৈরি করুন", "Create Certificate")%>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="kt-content p-0" id="kt_content">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=getDataByLanguage(Language, "পারিতোষিক ও ভাতাদি প্রত্যয়নপত্র", "Remuneration and Allowance Certificate")%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body" id="bill-div">

            <div class="ml-auto m-3">
                <button type="button" class="btn" id='download-pdf'
                        onclick="downloadTemplateAsPdf('to-print-div', '<%=pdfFileName%>');">
                    <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                </button>
            </div>

            <div style="margin: auto;display: none;" id="certificate-page">
                <div class="container" id="to-print-div">
                    <section class="page shadow" data-size="A4">
                        <div class="text-center">
                            <h2>
                                অর্থ শাখা-২
                            </h2>
                            <h3 style="display: inline-block; border-bottom: 1px solid black">
                                www.parliament.gov.bd
                            </h3>
                        </div>

                        <div class="row mt-5">
                            <div class="col-8">
                                নম্বর-১১.০০.০০০০.৬৬৭.০২.০০১.১৯ (৯৬)/৯৪৭
                            </div>
                            <div class="col-4 text-right">
                                তারিখ: <span id="reportDate"></span>
                            </div>
                        </div>

                        <div class="text-center mt-5">
                            <h3 style="border-bottom: 1px solid black; display: inline-block">
                                প্রত্যয়নপত্র
                            </h3>
                        </div>

                        <p class="mt-4">
                            এ মর্মে প্রত্যয়ন করা যাচ্ছে যে, <span id="parliamentNumber"></span>-তম জাতীয় সংসদের <span
                                id="electionConstituency"></span>
                            আসন হতে নির্বাচিত মাননীয় সংসদ-সদস্য
                            <span id="mpName"></span> <span id="economicYear"></span>
                            অর্থ বছরে বাংলাদেশ জাতীয় সংসদ সচিবালয় হতে সংসদ-সদস্য হিসাবে নিম্নেবর্ণিত পারিতোষিক ও
                            ভাতাদি গ্রহণ করেছেন:
                        </p>

                        <table class="table-bordered w-100 mt-5">
                            <thead>
                            <tr>
                                <th>মেয়াদকাল</th>
                                <th>পারিতোষিক</th>
                                <th>ভাতাদি</th>
                                <th>মোট টাকা</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <span id="startDateView"></span> থেকে <span id="endDateView"></span> পর্যন্ত
                                </td>
                                <td>
                                    <span id="gratuity"></span>
                                </td>
                                <td>
                                    <span id="totalAllowance"></span>
                                </td>
                                <td>
                                    <span id="totalAmount"></span>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <div class="text-center mt-4">
                            কথায়ঃ <span id="totalInWord"></span>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    const searchParamForm = document.getElementById('search-param-form');

    $(() => {
        searchParamForm.addEventListener('submit', async (event) => {
            event.preventDefault();

            $('#startDate').val(getDateTimestampById('startDate-js'));
            $('#endDate').val(getDateTimestampById('endDate-js'));

            if (!$(searchParamForm).valid()) {
                hideCertificate();
                return;
            }
            if($('#startDate').val()>$('#endDate').val()){
                hideCertificate();
                showError("শেষ তারিখ শুরুর তারিখের চেয়ে বড় দিন","End date should be greater than start date");
                return;
            }

            const formElement = event.target;
            const formData = new FormData(formElement);
            const searchParamStr = new URLSearchParams(formData).toString();

            const url = "Mp_remuneration_allowanceServlet?actionType=ajax_getCertificateModel&" + searchParamStr;
            const response = await fetch(url);
            const model = await response.json();
            loadCertificate(model);
        });

        $(searchParamForm).validate({
            rules: {
                election_details_id: "required",
                election_constituency_id: "required",
                startDate: "required",
                endDate: "required"
            },
            messages: {
                election_details_id: '<%=getDataByLanguage(Language, "আবশ্যক", "Mandatory")%>',
                election_constituency_id: '<%=getDataByLanguage(Language, "আবশ্যক", "Mandatory")%>',
                startDate: '<%=getDataByLanguage(Language, "আবশ্যক", "Mandatory")%>',
                endDate: '<%=getDataByLanguage(Language, "আবশ্যক", "Mandatory")%>'
            }
        });
    });

    function hideCertificate(){
        const certificatePage = $('#certificate-page');
        certificatePage.hide();
        certificatePage.removeClass('fly-in-from-down');
    }
    function populateModelData(model) {
        document.getElementById("reportDate").innerText = model.reportDate;
        document.getElementById("parliamentNumber").innerText = model.parliamentNumber;
        document.getElementById("electionConstituency").innerText = model.electionConstituency;
        document.getElementById("mpName").innerText = model.mpName;
        document.getElementById("economicYear").innerText = model.economicYear;
        document.getElementById("startDateView").innerText = model.startDate;
        document.getElementById("endDateView").innerText = model.endDate;
        document.getElementById("gratuity").innerText = model.gratuity;
        document.getElementById("totalAllowance").innerText = model.totalAllowance;
        document.getElementById("totalAmount").innerText = model.totalAmount;
        document.getElementById("totalInWord").innerText = model.totalInWord;
    }

    function loadCertificate(model) {
        const certificatePage = $('#certificate-page');
        certificatePage.hide();
        certificatePage.removeClass('fly-in-from-down');

        populateModelData(model);

        certificatePage.show();
        certificatePage.addClass('fly-in-from-down');
    }

    const electionConstituencyInput = document.getElementById("election_constituency_id");

    async function electionDetailsChanged(selectElem) {
        const electionDetailsId = $(selectElem).val();

        electionConstituencyInput.innerHTML = '';
        $(electionConstituencyInput).val('');

        if (electionDetailsId === '') return;

        const url = "Election_constituencyServlet?actionType=buildElectionConstituency&election_id=" + electionDetailsId
            + "&language=" + '<%=Language%>';
        const res = await fetch(url);
        document.getElementById("election_constituency_id").innerHTML = await res.text();
        select2SingleSelector('#election_constituency_id', '<%=Language%>');
    }

    function downloadTemplateAsPdf(divId, fileName) {
        let content = document.getElementById(divId);
        const opt = {
            margin: 0.5,
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4'}
        };
        html2pdf().from(content).set(opt).save();
    }
</script>