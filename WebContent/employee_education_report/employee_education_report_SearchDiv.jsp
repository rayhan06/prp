<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="education_level.Education_levelRepository" %>
<%@ page import="result_exam.Result_examRepository" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@page pageEncoding="UTF-8" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = LM.getText(LC.EMPLOYEE_EDUCATION_REPORT_EDIT_LANGUAGE, loginDTO);
    boolean isLangEng = Language.equalsIgnoreCase("English");
    String context = "../../.." + request.getContextPath() + "/assets/";
%>

<jsp:include page="../employee_assign/officeMultiSelectTagsUtil.jsp"/>

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>

<div class="row mx-2">
    <div id="criteriaSelectionId" class="search-criteria-div col-md-6">
        <div class="form-group row">
            <div class="col-md-3"></div>
            <div class="col-md-9">
                <button type="button"
                        class="btn btn-sm btn-info btn-info text-white shadow btn-border-radius"
                        data-toggle="modal" data-target="#select_criteria_div" id="select_criteria_btn"
                        onclick="beforeOpenCriteriaModal()">
                    <%=isLangEng ? "Criteria Select" : "ক্রাইটেরিয়া বাছাই"%>
                </button>
            </div>
        </div>
    </div>
    <div id="criteriaPaddingId" class="search-criteria-div col-md-6">
    </div>
    <div id="officeUnitId_div" class="search-criteria-div col-md-6 officeModalClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_OFFICEUNITID, loginDTO)%>
            </label>
            <div class="col-md-9">
                <%--Office Unit Modal Trigger Button--%>
                <button type="button" class="btn btn-block shadow btn-primary text-white btn-border-radius"
                        id="office_units_id_modal_button" onclick="officeModalButtonClicked();">
                    <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                </button>
                <input type="hidden" name='officeUnitIds' id='officeUnitIds_input' value="">
            </div>
        </div>
    </div>
    <div class="search-criteria-div col-md-6 officeModalClass" style="display: none">
        <div class="form-group row">
            <label class="col-sm-4 col-xl-3 col-form-label text-md-right" for="onlySelectedOffice_checkbox">
                <%=isLangEng ? "Only Selected Office" : "শুধুমাত্র নির্বাচিত অফিস"%>
            </label>
            <div class="col-1" id='onlySelectedOffice'>
                <input type='checkbox' class='form-control-sm mt-1' name='onlySelectedOffice'
                       id='onlySelectedOffice_checkbox'
                       onchange="this.value = this.checked;" value='false'>
            </div>
            <div class="col-8"></div>
        </div>
    </div>
    <div class="search-criteria-div col-12" id="selected-offices" style="display: none;">
        <div class="form-group row">
            <div class="offset-1 col-10 tag-container" id="selected-offices-tag-container">
                <div class="tag template-tag">
                    <span class="tag-name"></span>
                    <i class="fas fa-times-circle tag-remove-btn"></i>
                </div>
            </div>
        </div>
    </div>
    <div id="officeUnitOrganogramId_div" class="search-criteria-div col-md-6 organogramModalClass"
         style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITORGANOGRAMID, loginDTO)%>
            </label>
            <div class="col-md-9">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                        id="organogram_id_modal_button"
                        onclick="organogramIdModalBtnClicked();">
                    <%=LM.getText(LC.LANGUAGE_SELECT, userDTO)%>
                </button>
                <div class="input-group" id="organogram_id_div" style="display: none">
                    <input type="hidden" name='officeUnitOrganogramId' id='organogram_id_input' value="">
                    <button type="button" class="btn btn-secondary form-control" disabled
                            id="organogram_id_text"></button>
                    <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                            <button type="button" class="btn btn-outline-danger modalCross"
                                    onclick="crsBtnClicked('organogram_id');"
                                    id='organogram_id_crs_btn' tag='pb_html'>
                                x
                            </button>
                        </span>
                </div>
            </div>
        </div>
    </div>
    <div id="employeeRecordId_div" class="search-criteria-div col-md-6 employeeModalClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_EMPLOYEERECORDID, loginDTO)%>
            </label>
            <div class="col-md-9">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                        id="employee_record_id_modal_button"
                        onclick="employeeRecordIdModalBtnClicked();">
                    <%=LM.getText(LC.LANGUAGE_SELECT, userDTO)%>
                </button>
                <div class="input-group" id="employee_record_id_div" style="display: none">
                    <input type="hidden" name='employeeRecordId' id='employee_record_id_input' value="">
                    <button type="button" class="btn btn-secondary form-control" disabled
                            id="employee_record_id_text"></button>
                    <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                            <button type="button" class="btn btn-outline-danger modalCross"
                                    onclick="crsBtnClicked('employee_record_id');"
                                    id='employee_record_id_crs_btn' tag='pb_html'>
                                x
                            </button>
                        </span>
                </div>
            </div>
        </div>
    </div>
    <div id="name_eng_div" class="search-criteria-div col-md-6 employeeNameClass"
         style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="nameEng">
                <%=isLangEng ? "Name(English)" : "নাম(ইংরেজি)"%>
            </label>
            <div class="col-md-9">
                <input class="englishOnly form-control" type="text" name="nameEng" id="nameEng"
                       style="width: 100%"
                       placeholder="<%=isLangEng ? "Enter Employee Name in English":"কর্মকর্তা/কর্মচারীর নাম ইংরেজিতে লিখুন"%>"
                       value="">
            </div>
        </div>
    </div>

    <div id="name_bng_div" class="search-criteria-div col-md-6 employeeNameClass"
         style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="nameBng">
                <%=isLangEng ? "Name(Bangla)" : "নাম(বাংলা)"%>
            </label>
            <div class="col-md-9">
                <input class="noEnglish form-control" type="text" name="nameBng" id="nameBng"
                       style="width: 100%"
                       placeholder="<%=isLangEng ? "Enter Employee Name in Bangla":"কর্মকর্তা/কর্মচারীর নাম বাংলাতে লিখুন"%>"
                       value="">
            </div>
        </div>
    </div>
    <div id="educationLevelType_div" class="search-criteria-div col-md-6 educationLevelClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="educationLevelType">
                <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_EDUCATIONLEVELTYPE, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='educationLevelType' id='educationLevelType'
                >
                    <%=Education_levelRepository.getInstance().buildOptions(Language, null)%>
                </select>
            </div>
        </div>
    </div>
    <div id="degreeExamType_div" class="search-criteria-div col-md-6 degreeExamClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="degreeExamType">
                <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_DEGREEEXAMTYPE, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='degreeExamType' id='degreeExamType'>
                </select>
            </div>
        </div>
    </div>
    <div id="result_exam_div" class="search-criteria-div col-md-6 resultExamClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="resultExamType">
                <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_RESULTEXAMTYPE, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='resultExamType' id='resultExamType'>
                    <%=Result_examRepository.getInstance().buildOptions(Language, null)%>
                </select>
            </div>
        </div>
    </div>
    <div id="major_div" class="search-criteria-div col-md-6 majorClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="major">
                <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_MAJOR, loginDTO)%>
            </label>
            <div class="col-md-9">
                <input class='form-control' name='major' id='major' value=""/>
            </div>
        </div>
    </div>
    <div id="gradePointCat_div" class="search-criteria-div col-md-6 gradePointClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="gradePointCat">
                <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_GRADEPOINTCAT, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='gradePointCat' id='gradePointCat'>
                    <%=CatRepository.getInstance().buildOptions("grade_point", Language, null)%>
                </select>
            </div>
        </div>
    </div>
    <div id="cgpaNumber_div" class="search-criteria-div col-md-6 cgpaClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="cgpaNumber">
                <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_CGPANUMBER, loginDTO)%>
            </label>
            <div class="col-md-9">
                <input class='form-control' name='cgpaNumber' id='cgpaNumber' value=""/>
            </div>
        </div>
    </div>
    <div id="institutionName_div" class="search-criteria-div col-md-6 institutionClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="institutionName">
                <%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_INSTITUTIONNAME, loginDTO)%>
            </label>
            <div class="col-md-9">
                <input class='form-control' name='institutionName' id='institutionName'
                       value=""/>
            </div>
        </div>
    </div>

    <div id="gender_div" class="search-criteria-div col-md-6 genderClass" style="display:none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="genderCat">
                <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_GENDERCAT, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='genderCat' id='genderCat'
                        style="width: 100%">
                    <%=CatRepository.getInstance().buildOptions("gender", Language, null)%>
                </select>
            </div>
        </div>
    </div>

    <div id="employeeClass_div" class="search-criteria-div col-md-6 employeeClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="employeeClassCat">
                <%=LM.getText(LC.EMPLOYEE_EMPLOYEE_CLASS, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='employeeClassCat' id='employeeClassCat'
                        style="width: 100%">
                    <%=CatRepository.getInstance().buildOptions("employee_class", Language, null)%>
                </select>
            </div>
        </div>
    </div>

    <div id="employmentCat_div" class="search-criteria-div col-md-6 employmentCatClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="employmentCat">
                <%=LM.getText(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='employmentCat' id='employmentCat'
                        style="width: 100%">
                    <%=CatRepository.getInstance().buildOptions("employment", Language, null)%>
                </select>
            </div>
        </div>
    </div>

    <div id="emp_officer_div" class="search-criteria-div col-md-6 empOfficerClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="empOfficerCat">
                <%=LM.getText(LC.EMPLOYEE_OFFICER_TYPE, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='empOfficerCat' id='empOfficerCat'
                        style="width: 100%">
                    <%=CatRepository.getInstance().buildOptions("emp_officer", Language, null)%>
                </select>
            </div>
        </div>
    </div>

    <div id="age_div" class="search-criteria-div col-md-6 ageClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.HR_REPORT_AGE_RANGE, loginDTO)%>
            </label>
            <div class="col-6 col-md-5">
                <input class='form-control' type="text" name='age_start' id='age_start'
                       placeholder='<%=LM.getText(LC.HR_REPORT_FROM, loginDTO)%>' style="width: 100%">
            </div>
            <div class="col-6 col-md-4">
                <input class='form-control' type="text" name='age_end' id='age_end'
                       placeholder='<%=LM.getText(LC.HR_REPORT_TO, loginDTO)%>' style="width: 100%">
            </div>
        </div>
    </div>
</div>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<%
    String modalTitle = Language.equalsIgnoreCase("English") ? "Find Designation" : "পদবী খুঁজুন";
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
    <jsp:param name="modalTitle" value="<%=modalTitle%>"/>
</jsp:include>
<script src="<%=context%>scripts/number_validation_1.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(() => {
        showFooter = false;
    });

    function keyDownEvent(e) {
        let isvalid = inputValidationForFloatValue(e, $(this), 5.00);
        return true == isvalid;
    }

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInTags,
            isMultiSelect: true,
            keepLastSelectState: true
        }]
    ]);

    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function officeModalEditButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        officeSearchSetSelectedOfficeLayers($('#office_units_id_input').val());
        $('#search_office_modal').modal();
    }

    function viewOgranogramIdInInput(empInfo) {
        $('#organogram_id_modal_button').hide();
        $('#organogram_id_div').show();

        let language = '<%=Language.toLowerCase()%>'
        let designation;
        if (language === 'english') {
            designation = empInfo.organogramNameEn + ', ' + empInfo.officeUnitNameEn;
        } else {
            designation = empInfo.organogramNameBn + ', ' + empInfo.officeUnitNameBn;
        }
        document.getElementById('organogram_id_text').innerHTML = designation;
        $('#organogram_id_input').val(empInfo.organogramId);
    }

    function viewEmployeeRecordIdInInput(empInfo) {
        $('#employee_record_id_modal_button').hide();
        $('#employee_record_id_div').show();

        let language = '<%=Language.toLowerCase()%>'
        let employeeView;
        if (language === 'english') {
            employeeView = empInfo.employeeNameEn + ', ' + empInfo.organogramNameEn + ', ' + empInfo.officeUnitNameEn;
        } else {
            employeeView = empInfo.employeeNameBn + ', ' + empInfo.organogramNameBn + ', ' + empInfo.officeUnitNameBn;
        }
        document.getElementById('employee_record_id_text').innerHTML = employeeView;
        $('#employee_record_id_input').val(empInfo.employeeRecordId);
    }


    table_name_to_collcetion_map = new Map([
        ['organogramId', {
            isSingleEntry: true,
            callBackFunction: viewOgranogramIdInInput
        }],
        ['employeeRecordId', {
            isSingleEntry: true,
            callBackFunction: viewEmployeeRecordIdInInput
        }]
    ]);
    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    function organogramIdModalBtnClicked() {
        modal_button_dest_table = 'organogramId';
        $('#search_emp_modal').modal();
    }

    function employeeRecordIdModalBtnClicked() {
        modal_button_dest_table = 'employeeRecordId';
        $('#search_emp_modal').modal();
    }

    $(document).ready(() => {
        select2SingleSelector("#educationLevelType", '<%=Language%>');
        select2SingleSelector("#degreeExamType", '<%=Language%>');
        select2SingleSelector("#universityInfoType", '<%=Language%>');
        select2SingleSelector("#gradePointCat", '<%=Language%>');
        select2SingleSelector("#resultExamType", '<%=Language%>');
        $('#educationLevelType').change(function () {
            fetchDegree($(this).val());
        });
        select2SingleSelector('#genderCat', '<%=Language%>');
        select2SingleSelector('#employeeClassCat', '<%=Language%>');
        select2SingleSelector('#employmentCat', '<%=Language%>');
        select2SingleSelector('#empOfficerCat', '<%=Language%>');
        dateTimeInit('<%=Language%>');
        document.getElementById('age_start').onkeydown = keyDownEvent;
        document.getElementById('age_end').onkeydown = keyDownEvent;
    });

    function init() {
        document.getElementById('cgpaNumber').onkeydown = keyDownEvent;
        criteriaArray = [{
            class: 'officeModalClass',
            title: '<%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_OFFICEUNITID, loginDTO)%>',
            show: false,
            specialCategory: 'officeModal'
        },
            {
                class: 'organogramModalClass',
                title: '<%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITORGANOGRAMID, loginDTO)%>',
                show: false,
                specialCategory: 'employeeModal'
            },
            {
                class: 'employeeModalClass',
                title: '<%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_EMPLOYEERECORDID, loginDTO)%>',
                show: false,
                specialCategory: 'employeeModal'
            },
            {
                class: 'employeeNameClass',
                title: '<%=isLangEng ? "Employee Name" : "কর্মকর্তা/কর্মচারীর নাম"%>',
                show: false
            },
            {
                class: 'educationLevelClass',
                title: '<%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_EDUCATIONLEVELTYPE, loginDTO)%>',
                show: false
            },
            {
                class: 'degreeExamClass',
                title: '<%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_DEGREEEXAMTYPE, loginDTO)%>',
                show: false
            },
            {
                class: 'resultExamClass',
                title: '<%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_RESULTEXAMTYPE, loginDTO)%>',
                show: false
            },
            {
                class: 'majorClass',
                title: '<%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_MAJOR, loginDTO)%>',
                show: false
            },
            {
                class: 'gradePointClass',
                title: '<%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_GRADEPOINTCAT, loginDTO)%>',
                show: false
            },
            {
                class: 'cgpaClass',
                title: '<%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_CGPANUMBER, loginDTO)%>',
                show: false
            },
            {
                class: 'institutionClass',
                title: '<%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_INSTITUTIONNAME, loginDTO)%>',
                show: false
            },
            {
                class: 'genderClass',
                title: '<%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_GENDERCAT, loginDTO)%>',
                show: false
            },
            {class: 'employeeClass',
                title: '<%=LM.getText(LC.EMPLOYEE_EMPLOYEE_CLASS, loginDTO)%>',
                show: false
            },
            {
                class: 'employmentCatClass',
                title: '<%=LM.getText(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY, loginDTO)%>',
                show: false
            },
            {class: 'empOfficerClass',
                title: '<%=LM.getText(LC.EMPLOYEE_OFFICER_TYPE, loginDTO)%>',
                show: false
            },
            {class: 'ageClass',
                title: '<%=LM.getText(LC.HR_REPORT_AGE_RANGE, loginDTO)%>',
                show: false
            },
            ]
    }

    function PreprocessBeforeSubmiting() {
    }

    function fetchDegree(educationLevelId) {
        let url = "option_Servlet?actionType=ajax_type&type=degree&education_level_id=" + educationLevelId;
        $('#degreeExamType').html("");
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                document.getElementById("degreeExamType").innerHTML = fetchedData;
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
</script>