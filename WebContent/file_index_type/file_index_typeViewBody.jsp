

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="file_index_type.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>




<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.FILE_INDEX_TYPE_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
File_index_typeDAO file_index_typeDAO = new File_index_typeDAO("file_index_type");
File_index_typeDTO file_index_typeDTO = (File_index_typeDTO)file_index_typeDAO.getDTOByID(id);
String Value = "";
int i = 0;
%>


<div class="modal-content viewmodal">
            <div class="modal-header">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9 col-sm-12">
                            <h5 class="modal-title"><%=LM.getText(LC.FILE_INDEX_TYPE_EDIT_FILE_INDEX_TYPE_EDIT_FORMNAME, loginDTO)%></h5>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="modal-body container">

			
                <div class="row div_border office-div">

                    <div class="col-md-12">
                        <h5>File Index Type</h5>
                    </div>
				
                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.FILE_INDEX_TYPE_EDIT_NAMEEN, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="nameEn">
						
											<%
											value = file_index_typeDTO.nameEn + "";
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				

			
				
                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.FILE_INDEX_TYPE_EDIT_NAMEBN, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="nameBn">
						
											<%
											value = file_index_typeDTO.nameBn + "";
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				

			
			
			
		

				</div>
	
				

                <div class="row div_border attachement-div">
                    <div class="col-md-12">
                        <h5>File Index Subtype</h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=(actionName.equals("edit"))?(LM.getText(LC.FILE_INDEX_TYPE_EDIT_FILE_INDEX_SUBTYPE_NAMEEN, loginDTO)):(LM.getText(LC.FILE_INDEX_TYPE_ADD_FILE_INDEX_SUBTYPE_NAMEEN, loginDTO))%></th>
								<th><%=(actionName.equals("edit"))?(LM.getText(LC.FILE_INDEX_TYPE_EDIT_FILE_INDEX_SUBTYPE_NAMEBN, loginDTO)):(LM.getText(LC.FILE_INDEX_TYPE_ADD_FILE_INDEX_SUBTYPE_NAMEBN, loginDTO))%></th>
							</tr>
							<%
                        	FileIndexSubtypeDAO fileIndexSubtypeDAO = new FileIndexSubtypeDAO();
                         	List<FileIndexSubtypeDTO> fileIndexSubtypeDTOs = fileIndexSubtypeDAO.getFileIndexSubtypeDTOListByFileIndexTypeID(file_index_typeDTO.iD);
                         	
                         	for(FileIndexSubtypeDTO fileIndexSubtypeDTO: fileIndexSubtypeDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%
											value = fileIndexSubtypeDTO.nameEn + "";
											%>
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = fileIndexSubtypeDTO.nameBn + "";
											%>
														
											<%=value%>
				
			
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
               

            <div class="modal-footer" style="justify-content:flex-start">



                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>

        </div>