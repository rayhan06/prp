<%@page pageEncoding="UTF-8" %>

<%@page import="file_index_type.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@ page import="approval_execution_table.*"%>
<%@ page import="approval_path.*"%>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.FILE_INDEX_TYPE_EDIT_LANGUAGE, loginDTO);

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_FILE_INDEX_TYPE;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
File_index_typeDTO file_index_typeDTO = (File_index_typeDTO)request.getAttribute("file_index_typeDTO");

Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
Approval_execution_tableDTO approval_execution_tableDTO = null;
ApprovalPathDetailsDTO approvalPathDetailsDTO =  null;
boolean canApprove = false, canValidate = false;
boolean isInPreviousOffice = false;
String Message = "Done";

if(!isPermanentTable)
{
	approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("file_index_type", file_index_typeDTO.iD);
	System.out.println("approval_execution_tableDTO = " + approval_execution_tableDTO);
	approvalPathDetailsDTO = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
	if(approvalPathDetailsDTO!= null && approvalPathDetailsDTO.organogramId == userDTO.organogramID)
	{
		canApprove = true;
		if(approvalPathDetailsDTO.approvalRoleCat == SessionConstants.VALIDATOR)
		{
			canValidate = true;
		}
	}
}	

System.out.println("file_index_typeDTO = " + file_index_typeDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";



String successMessageForwarded = "Forwarded to your Senior Office";
String successMessageApproved = "Approval Done";

File_index_typeDAO file_index_typeDAO = (File_index_typeDAO)request.getAttribute("file_index_typeDAO");


%>

											
		
											
											<td id = '<%=i%>_nameEn'>
											<%
											value = file_index_typeDTO.nameEn + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_nameBn'>
											<%
											value = file_index_typeDTO.nameBn + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
		
											
		
	

											<td>
											<a href='File_index_typeServlet?actionType=view&ID=<%=file_index_typeDTO.iD%>'>View</a>
											
											<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
											
											<div class='modal fade' id='viedFileModal_<%=i%>'>
											  <div class='modal-dialog modal-lg' role='document'>
											    <div class='modal-content'>
											      <div class='modal-body'>
											        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
											          <span aria-hidden='true'>&times;</span>
											        </button>											        
											        
											        <object type='text/html' data='File_index_typeServlet?actionType=view&modal=1&ID=<%=file_index_typeDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
											        
											      </div>
											    </div>
											  </div>
											</div>
											</td>
	
											<td id = '<%=i%>_Edit'>
											<%
											if(isPermanentTable || canValidate)
											{
											%>																						
	
												<a href='File_index_typeServlet?actionType=getEditPage&ID=<%=file_index_typeDTO.iD%>'><%=LM.getText(LC.FILE_INDEX_TYPE_SEARCH_FILE_INDEX_TYPE_EDIT_BUTTON, loginDTO)%></a>
									
											<%
											}
											else
											{
											%>
												You cannot Validate this.
											<%
											}
											%>
											</td>											
											<%
											if(!isPermanentTable)
											{
												%>
												<td><%=approval_execution_tableDTO !=null ? SessionConstants.operation_names[approval_execution_tableDTO.operation]: "Invalid Operation"%></td>
												<%
											}
											 %>
											 
											
											<%
											if(!isPermanentTable)
											{
											%>
											<td>
											<%
												if(approval_execution_tableDTO.operation == SessionConstants.UPDATE)
												{
													%>													
													<a href="#" id = '<%=i%>_getOriginal' onclick='getOriginal(<%=i%>, <%=file_index_typeDTO.iD%>, <%=approval_execution_tableDTO.previousRowId%>, "File_index_typeServlet")'>View Original</a>
													<input type='hidden' id='<%=i%>_original_status' value='0'/>
													<%
												}
												%>
											</td>
											<%
											}
											%>
											<%
											if(!isPermanentTable)
											{
												
												
												%>
												<td id = 'tdapprove_<%=file_index_typeDTO.iD%>'>
												<%
												if(canApprove)
												{
													%>													
													<a href="#"  id = 'approve_<%=file_index_typeDTO.iD%>' onclick='approve("<%=approval_execution_tableDTO.updatedRowId%>", "<%=Message%>", <%=i%>, "File_index_typeServlet", true)'><%=LM.getText(LC.HM_APPROVE, loginDTO)%></a>
												<%
												}
												else 
												{
												%>
													Not in your Office.
												<%
												}
												%>
												</td>
												
											<%
											}
											%>											
											
											
											<td id='<%=i%>_checkbox'>
											<%
											if(isPermanentTable)
											{
											%>
												
												<div class='checker'>
												<span id='chkEdit' ><input type='checkbox' name='ID' value='<%=file_index_typeDTO.iD%>'/></span>
												</div>
											<%
											}
											else
											{
											%>
												<a href="#"  id = 'reject_<%=file_index_typeDTO.iD%>' onclick='approve("<%=file_index_typeDTO.iD%>", "<%=Message%>", <%=i%>, "File_index_typeServlet", false)'><%=LM.getText(LC.HM_REJECT, loginDTO)%></a>
											<%
											}
											%>
											</td>



