<%@page pageEncoding="UTF-8" %>

<%@page import="office_shift_details.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="office_shift.Office_shiftDAO" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.OFFICE_SHIFT_DETAILS_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_OFFICE_SHIFT_DETAILS;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Office_shift_detailsDTO office_shift_detailsDTO = (Office_shift_detailsDTO) request.getAttribute("office_shift_detailsDTO");
    CommonDTO commonDTO = office_shift_detailsDTO;
    String servletName = "Office_shift_detailsServlet";


    System.out.println("office_shift_detailsDTO = " + office_shift_detailsDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Office_shift_detailsDAO office_shift_detailsDAO = (Office_shift_detailsDAO) request.getAttribute("office_shift_detailsDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_officeShiftId'>
    <%=Office_shiftDAO.geText(Language, office_shift_detailsDTO.officeShiftId)%>


</td>


<td id='<%=i%>_inTime'>

    <% value = TimeFormat.getInAmPmFormat(office_shift_detailsDTO.inTime);%>
    <%=Utils.getDigits(value, Language)%>
</td>


<td id='<%=i%>_outTime'>

    <% value = TimeFormat.getInAmPmFormat(office_shift_detailsDTO.outTime);%>
    <%=Utils.getDigits(value, Language)%>
</td>


<td id='<%=i%>_graceInPeriodMins'>
    <%
        value = office_shift_detailsDTO.graceInPeriodMins + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_graceOutPeriodMins'>
    <%
        value = office_shift_detailsDTO.graceOutPeriodMins + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_startDate'>
    <%
        value = office_shift_detailsDTO.startDate + "";
    %>
    <%
        String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_startDate, Language)%>


</td>


<td id='<%=i%>_endDate'>
    <%
        value = office_shift_detailsDTO.endDate + "";
    %>
    <%
        String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_endDate, Language)%>


</td>


<td id='<%=i%>_lateIn'>
    <%
        value = office_shift_detailsDTO.lateInAlertMinute + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_earlyOut'>
    <%
        value = office_shift_detailsDTO.earlyOutAlertMinute + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td>
    <%--    <a href='Office_shift_detailsServlet?actionType=view&ID=<%=office_shift_detailsDTO.id%>'><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
        </a>--%>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Office_shift_detailsServlet?actionType=view&ID=<%=office_shift_detailsDTO.id%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>

    <%--  <a href='Office_shift_detailsServlet?actionType=getEditPage&ID=<%=office_shift_detailsDTO.id%>'><%=LM.getText(LC.OFFICE_SHIFT_DETAILS_SEARCH_OFFICE_SHIFT_DETAILS_EDIT_BUTTON, loginDTO)%>
      </a>--%>
        <button
                class="btn-sm border-0 shadow btn-border-radius text-white"
                style="background-color: #ff6b6b;"
                onclick="location.href='Office_shift_detailsServlet?actionType=getEditPage&ID=<%=office_shift_detailsDTO.id%>'"
        >
            <i class="fa fa-edit"></i>
        </button>
</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=office_shift_detailsDTO.id%>'/></span>
    </div>
</td>
																						
											

