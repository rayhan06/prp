<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="office_shift_details.Office_shift_detailsDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Office_shift_detailsDTO office_shift_detailsDTO = (Office_shift_detailsDTO)request.getAttribute("office_shift_detailsDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(office_shift_detailsDTO == null)
{
	office_shift_detailsDTO = new Office_shift_detailsDTO();
	
}
System.out.println("office_shift_detailsDTO = " + office_shift_detailsDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.OFFICE_SHIFT_DETAILS_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>

			
<%=("<td id = '" + i + "_id" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='id' id = 'id_hidden_<%=i%>' value='<%=office_shift_detailsDTO.id%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_officeShiftId'>")%>
			
	
	<div class="form-inline" id = 'officeShiftId_div_<%=i%>'>
		<select class='form-control'  name='officeShiftId' id = 'officeShiftId_select2_<%=i%>'   tag='pb_html'>
			<option class='form-control'  value='0' <%=(actionName.equals("edit") && String.valueOf(office_shift_detailsDTO.officeShiftId).equals("0"))?("selected"):""%>>0<br>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_inTime'>")%>
			
	
	<div class="form-inline" id = 'inTime_div_<%=i%>'>
			<%
                 value = "";
                 if(actionName.equals("edit")) {
                	 value = TimeFormat.getInAmPmFormat(test_babaDTO.timeOfBirth);
                 }
             %>	
			<div class='input-group date edms-datetimepicker'>
                <input type='text' class="form-control" value="<%=value%>" id = 'inTime_time_<%=i%>' name='inTime'  tag='pb_html'/>
                <span class="input-group-addon" style="width:45px;">
                   <span><i class="fa fa-clock-o"></i></span>
                </span>
            </div>	
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_outTime'>")%>
			
	
	<div class="form-inline" id = 'outTime_div_<%=i%>'>
			<%
                 value = "";
                 if(actionName.equals("edit")) {
                	 value = TimeFormat.getInAmPmFormat(test_babaDTO.timeOfBirth);
                 }
             %>	
			<div class='input-group date edms-datetimepicker'>
                <input type='text' class="form-control" value="<%=value%>" id = 'outTime_time_<%=i%>' name='outTime'  tag='pb_html'/>
                <span class="input-group-addon" style="width:45px;">
                   <span><i class="fa fa-clock-o"></i></span>
                </span>
            </div>	
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_graceInPeriodMins'>")%>
			
	
	<div class="form-inline" id = 'graceInPeriodMins_div_<%=i%>'>
		<input type='text' class='form-control'  name='graceInPeriodMins' id = 'graceInPeriodMins_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + office_shift_detailsDTO.graceInPeriodMins + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_graceOutPeriodMins'>")%>
			
	
	<div class="form-inline" id = 'graceOutPeriodMins_div_<%=i%>'>
		<input type='text' class='form-control'  name='graceOutPeriodMins' id = 'graceOutPeriodMins_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + office_shift_detailsDTO.graceOutPeriodMins + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_startDate'>")%>
			
	
	<div class="form-inline" id = 'startDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'startDate_date_<%=i%>' name='startDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_startDate = dateFormat.format(new Date(office_shift_detailsDTO.startDate));
	%>
	'<%=formatted_startDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_endDate'>")%>
			
	
	<div class="form-inline" id = 'endDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'endDate_date_<%=i%>' name='endDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_endDate = dateFormat.format(new Date(office_shift_detailsDTO.endDate));
	%>
	'<%=formatted_endDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=office_shift_detailsDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value='<%=office_shift_detailsDTO.insertedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=office_shift_detailsDTO.modifiedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + office_shift_detailsDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=office_shift_detailsDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Office_shift_detailsServlet?actionType=view&ID=<%=office_shift_detailsDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Office_shift_detailsServlet?actionType=view&modal=1&ID=<%=office_shift_detailsDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	