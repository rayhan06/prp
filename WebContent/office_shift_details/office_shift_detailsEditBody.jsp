<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="office_shift_details.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>


<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="office_shift.Office_shiftDAO" %>
<%
    Office_shift_detailsDTO office_shift_detailsDTO;
    office_shift_detailsDTO = (Office_shift_detailsDTO) request.getAttribute("office_shift_detailsDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (office_shift_detailsDTO == null) {
        office_shift_detailsDTO = new Office_shift_detailsDTO();

    }
    System.out.println("office_shift_detailsDTO = " + office_shift_detailsDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_OFFICE_SHIFT_DETAILS_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
%>
<%
    String Language = LM.getText(LC.OFFICE_SHIFT_DETAILS_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i> &nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal kt-form"
              action="Office_shift_detailsServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">

            <input type='hidden' class='form-control' name='id' id='id_hidden_<%=i%>'
                   value='<%=office_shift_detailsDTO.id%>' tag='pb_html'/>

            <div class="kt-portlet__body form-body mt-5">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: white"><%=formTitle%>
                                            </h4>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_OFFICESHIFTID, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='officeShiftId_div_<%=i%>'>
                                                    <select class='form-control' name='officeShiftId'
                                                            id='officeShiftId' tag='pb_html'>
                                                        <%=Office_shiftDAO.buildOptionsNewShift(Language, office_shift_detailsDTO.officeShiftId)%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_INTIME, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='inTime_div_<%=i%>'>
                                                    <jsp:include page="/time/time.jsp">
                                                        <jsp:param name="TIME_ID" value="inTime_js"></jsp:param>
                                                        <jsp:param name="LANGUAGE"
                                                                   value="<%=Language%>"></jsp:param>
                                                    </jsp:include>

                                                    <input type='hidden' class='form-control' name='inTime'
                                                           id='inTime' value="" tag='pb_html'/>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 text-md-right col-form-label">
                                                <%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_OUTTIME, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='outTime_div_<%=i%>'>
                                                    <jsp:include page="/time/time.jsp">
                                                        <jsp:param name="TIME_ID" value="outTime_js"></jsp:param>
                                                        <jsp:param name="LANGUAGE"
                                                                   value="<%=Language%>"></jsp:param>
                                                    </jsp:include>

                                                    <input type='hidden' class='form-control' name='outTime'
                                                           id='outTime' value="" tag='pb_html'/>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 text-md-right col-form-label">
                                                <%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_GRACEINPERIODMINS, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='graceInPeriodMins_div_<%=i%>'>
                                                    <input type='number' class='form-control rounded'
                                                           name='graceInPeriodMins'
                                                           id='graceInPeriodMins_text_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + office_shift_detailsDTO.graceInPeriodMins + "'"):("'" + "0" + "'")%>   tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 text-md-right col-form-label">
                                                <%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_GRACEOUTPERIODMINS, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='graceOutPeriodMins_div_<%=i%>'>
                                                    <input type='number' class='form-control rounded'
                                                           name='graceOutPeriodMins'
                                                           id='graceOutPeriodMins_text_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + office_shift_detailsDTO.graceOutPeriodMins + "'"):("'" + "0" + "'")%>   tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 text-md-right col-form-label">
                                                <%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_STARTDATE, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='startDate_div_<%=i%>'>
                                                    <jsp:include page="/date/date.jsp">
                                                        <jsp:param name="DATE_ID" value="start_date_js"></jsp:param>
                                                        <jsp:param name="LANGUAGE"
                                                                   value="<%=Language%>"></jsp:param>
                                                        <jsp:param name="START_YEAR" value="1971"></jsp:param>
                                                    </jsp:include>

                                                    <input type='hidden' class='form-control' name='startDate'
                                                           id='start-date' value="" tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 text-md-right col-form-label">
                                                <%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_ENDDATE, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='endDate_div_<%=i%>'>
                                                    <jsp:include page="/date/date.jsp">
                                                        <jsp:param name="DATE_ID" value="end_date_js"></jsp:param>
                                                        <jsp:param name="LANGUAGE"
                                                                   value="<%=Language%>"></jsp:param>
                                                        <jsp:param name="START_YEAR" value="1971"></jsp:param>
                                                    </jsp:include>

                                                    <input type='hidden' class='form-control' name='endDate'
                                                           id='end-date' value="" tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 text-md-right col-form-label">
                                                <%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_IS_ACTIVE, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='isActive_div_<%=i%>'>
                                                    <input type='checkbox' class='form-control-sm mt-1'
                                                           name='isActive' id='isActive'
                                                           value='true' <%=(actionName.equals("edit") && office_shift_detailsDTO.isActive==1)?("checked"):""%>
                                                           tag='pb_html'><br>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 text-md-right col-form-label">
                                                <%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_LATE_IN_ALERT_MINUTE, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='lateIn_div_<%=i%>'>
                                                    <input type='number' class='form-control rounded'
                                                           name='lateInAlert' id='lateInAlert'
                                                           value=<%=actionName.equals("edit")?("'" + office_shift_detailsDTO.lateInAlertMinute + "'"):("'" + "0" + "'")%>   tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 text-md-right col-form-label">
                                                <%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_EARLY_OUT_ALERT_MINUTE, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='earlyOut_div_<%=i%>'>
                                                    <input type='number' class='form-control rounded'
                                                           name='earlyOutAlert' id='earlyOutAlert'
                                                           value=<%=actionName.equals("edit")?("'" + office_shift_detailsDTO.earlyOutAlertMinute + "'"):("'" + "0" + "'")%>   tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <input type='hidden' class='form-control' name='insertionDate'
                                               id='insertionDate_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + office_shift_detailsDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>


                                        <input type='hidden' class='form-control' name='insertedBy'
                                               id='insertedBy_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + office_shift_detailsDTO.insertedBy + "'"):("'" + "" + "'")%> tag='pb_html'/>


                                        <input type='hidden' class='form-control' name='modifiedBy'
                                               id='modifiedBy_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + office_shift_detailsDTO.modifiedBy + "'"):("'" + "" + "'")%> tag='pb_html'/>


                                        <input type='hidden' class='form-control' name='isDeleted'
                                               id='isDeleted_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + office_shift_detailsDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>


                                        <input type='hidden' class='form-control' name='lastModificationTime'
                                               id='lastModificationTime_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + office_shift_detailsDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 mt-3 text-right">
                        <a class="btn btn-sm cancel-btn text-white shadow" href="<%=request.getHeader("referer")%>">
                            <%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_OFFICE_SHIFT_DETAILS_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button class="btn btn-sm submit-btn text-white shadow ml-2" type="submit">
                            <%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_OFFICE_SHIFT_DETAILS_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">

    $('#start_date_js').on('datepicker.change', () => {
        setMinDateById('end_date_js', getDateStringById('start_date_js'));
    });

    $('#inTime_js').on('timepicker.change', () => {
        setMinTimeById('outTime_js', getTimeById('inTime_js'));
    });

    $("#bigform").on('submit', () => {
        $('#start-date').val(getDateStringById('start_date_js'));
        $('#end-date').val(getDateStringById('end_date_js'));
        $('#inTime').val(getTimeById('inTime_js', true));
        $('#outTime').val(getTimeById('outTime_js', true));

        const startDateValidator = dateValidator('start_date_js', true);
        const endDateValidator = dateValidator('end_date_js', true);
        const inTimeValidator = timeNotEmptyValidator('inTime');
        const outTimeValidator = timeNotEmptyValidator('outTime');


        return startDateValidator && endDateValidator && inTimeValidator && outTimeValidator;
    })

    $(document).ready(() => {
        basicInit();

        $.validator.addMethod('officeShiftSelection', function (value, element) {
            return value != 0;
        });

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                officeShiftId: {
                    required: true,
                    officeShiftSelection: true
                },
                inTime: "required",
                outTime: "required"
            },

            messages: {
                officeShiftId: "Please choose an office!",
                inTime: "Please enter time of entry!",
                outTime: "Please enter time of departure!"
            }
        });
    });

    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }

        // if (!checkInOutTime()) {
        //     $("#alert-in-out-time").show();
        //     $("#alert-in-out-time-label").show();
        // 	return false;
        // }

        return true;
    }


    function checkInOutTime() {
        let inT = ($("#inTime").val()).split(' ');
        let outT = ($("#outTime").val()).split(' ');
        let inMinute = 0;
        let outMinute = 0;


        if (inT[1] == 'PM') {
            inMinute += 720;
        }

        if (outT[1] == 'PM') {
            outMinute += 720;
        }

        inMinute = parseInt(inMinute) + parseInt(inT[0].split(':')[0] * 60) + parseInt(inT[0].split(':')[1]);

        outMinute = parseInt(outMinute) + parseInt(outT[0].split(':')[0] * 60) + parseInt(inT[0].split(':')[1]);

        if (outMinute < inMinute) {
            return false;
        } else {
            return true;
        }
    }


    $(function () {

        <%
            if(actionName.equals("edit")) {
        %>
        setDateByTimestampAndId('start_date_js', <%=office_shift_detailsDTO.startDate%>);
        setDateByTimestampAndId('end_date_js', <%=office_shift_detailsDTO.endDate%>);
        setTimeById('inTime_js', '<%=office_shift_detailsDTO.inTime%>');
        setTimeById('outTime_js', '<%=office_shift_detailsDTO.outTime%>');
        <%--$("#start-date").val('<%=office_shift_detailsDTO.startDate>SessionConstants.MIN_DATE ? dateFormat.format(new Date(office_shift_detailsDTO.startDate)) : ""%>');--%>
        <%--$("#end-date").val('<%=office_shift_detailsDTO.endDate>SessionConstants.MIN_DATE ? dateFormat.format(new Date(office_shift_detailsDTO.endDate)):""%>');--%>
        <%
            }
        %>
    });


    function init(row) {
        $("#officeShiftId").select2({
            dropdownAutoWidth: true
        });
    }

    var row = 0;
    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }
    var child_table_extra_id = <%=childTableStartingID%>;

</script>






