<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="office_shift_details.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="office_shift.Office_shiftDAO" %>
<%@ page import="util.TimeFormat" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.OFFICE_SHIFT_DETAILS_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Office_shift_detailsDAO office_shift_detailsDAO = new Office_shift_detailsDAO("office_shift_details");
    Office_shift_detailsDTO office_shift_detailsDTO = office_shift_detailsDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid p-0" id="kt_content">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_OFFICE_SHIFT_DETAILS_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <h5 class="table-title">
                <%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_OFFICE_SHIFT_DETAILS_ADD_FORMNAME, loginDTO)%>
            </h5>
            <div class="table-responsive">
                <table class="table table-bordered table-striped text-nowrap">
                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_OFFICESHIFTID, loginDTO)%>
                            </b></td>
                        <td>

                            <%=Office_shiftDAO.geText(Language, office_shift_detailsDTO.officeShiftId)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_INTIME, loginDTO)%>
                            </b></td>
                        <td>

                            <% value = TimeFormat.getInAmPmFormat(office_shift_detailsDTO.inTime);%>
                            <%=Utils.getDigits(value, Language)%>

                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_OUTTIME, loginDTO)%>
                            </b></td>
                        <td>

                            <%value = TimeFormat.getInAmPmFormat(office_shift_detailsDTO.outTime);%>
                            <%=Utils.getDigits(value, Language)%>

                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_GRACEINPERIODMINS, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = office_shift_detailsDTO.graceInPeriodMins + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_GRACEOUTPERIODMINS, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = office_shift_detailsDTO.graceOutPeriodMins + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_STARTDATE, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = office_shift_detailsDTO.startDate + "";
                            %>
                            <%
                                String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                            %>
                            <%=Utils.getDigits(formatted_startDate, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_ENDDATE, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = office_shift_detailsDTO.endDate + "";
                            %>
                            <%
                                String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                            %>
                            <%=Utils.getDigits(formatted_endDate, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_IS_ACTIVE, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = office_shift_detailsDTO.isActive == 1 ? "Yes" : "No";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_LATE_IN_ALERT_MINUTE, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = office_shift_detailsDTO.lateInAlertMinute + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_EARLY_OUT_ALERT_MINUTE, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = office_shift_detailsDTO.earlyOutAlertMinute + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                </table>
            </div>
        </div>
    </div>
</div>

