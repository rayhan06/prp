<%@ page language="java" %>
<%@ page import="java.util.*" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="treeView.*" %>
<%
    LoginDTO cc_loginDTO3 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String cc_Language = LM.getText(LC.CENTRE_EDIT_LANGUAGE, cc_loginDTO3);
    String cc_actionType = request.getParameter("actionType");
    String cc_servletType2 = request.getParameter("servletType");
    TreeDTO cc_treeDTO = (TreeDTO) request.getAttribute("treeDTO");

    ArrayList<Integer> cc_nodeIDs = (ArrayList<Integer>) request.getAttribute("nodeIDs");

    int cc_parentID = Integer.parseInt(request.getParameter("parentID"));
    int cc_myLayer = ((Integer) request.getAttribute("myLayer")).intValue();
    int cc_maxLayer = cc_treeDTO.parentChildMap.length;

    String cc_geoName = cc_treeDTO.parentChildMap[cc_myLayer];
   	String cc_englishNameColumn = cc_treeDTO.englishNameColumn[cc_myLayer];
	String cc_banglaNameColumn = cc_treeDTO.banglaNameColumn[cc_myLayer];
    String cc_treeName = cc_treeDTO.treeName;
    int cc_checkBoxType = cc_treeDTO.checkBoxType;
    int cc_plusButtonType = cc_treeDTO.plusButtonType;
    String cc_showName = cc_treeDTO.showNameMap[cc_myLayer];

   // System.out.println("In treeNode, myLayer = " + myLayer + " parentID = " + parentID);

    String cc_parentCheckBoxChecked = request.getParameter("checkBoxChecked");
    boolean cc_bParentCheckBoxChecked = false;
    if (cc_parentCheckBoxChecked != null && !cc_parentCheckBoxChecked.equals(""))
    {
        cc_bParentCheckBoxChecked = Boolean.parseBoolean(cc_parentCheckBoxChecked);
    }

  //  System.out.println("cccccccccccccc checkBoxType = " + checkBoxType + " bParentCheckBoxChecked = " + bParentCheckBoxChecked);
    String cc_nameText = "";

    ArrayList<Integer> cc_recursiveNodeTypes = null;

    int cc_isRecursive = 0;
    if (request.getAttribute("isRecursive") != null)
    {
        cc_isRecursive = ((Integer) request.getAttribute("isRecursive")).intValue();
        cc_recursiveNodeTypes = (ArrayList<Integer>) request.getAttribute("recursiveNodeTypes");
    }

    boolean[] cc_hasExtraLayerPlusButton = cc_treeDTO.hasExtraLayerPlusButton;
%>

<ul class="jstree-container-ul jstree-children">
    <%
        int cc_prevNodeType = 0;
        int cc_extraLayerIndex = 0;
        boolean alreadyDrawnAnExtraUL2 = false;
        int i4 = 0;
        for (i4 = 0; i4 < cc_nodeIDs.size(); i4++)
        {
            int id = cc_nodeIDs.get(i4);
            int recursiveNodeType = 0;
            int realMyLayer = cc_myLayer;

            boolean drawExtraUl = false;
            if (cc_isRecursive != 0)
            {
                recursiveNodeType = cc_recursiveNodeTypes.get(i4);
            }
            int nextLayer = realMyLayer + 1;
            if (recursiveNodeType == 1) //recursive node parent found, dont increment layer
            {
                nextLayer = realMyLayer;
            }
            else
            {
                realMyLayer++;
            }
            if (recursiveNodeType > 0) //recursive node parent found, dont increment layer
            {
                cc_geoName = cc_treeDTO.parentChildMap[realMyLayer];
                cc_englishNameColumn = cc_treeDTO.englishNameColumn[realMyLayer];
                cc_banglaNameColumn = cc_treeDTO.banglaNameColumn[realMyLayer];
            }

            if (cc_prevNodeType != recursiveNodeType && cc_treeDTO.drawExtraLayer)
            {
                drawExtraUl = true;
                cc_prevNodeType = recursiveNodeType;
                cc_extraLayerIndex = cc_myLayer + recursiveNodeType - 1;
                cc_showName = cc_treeDTO.showNameMap[cc_extraLayerIndex];
               
            }
            //System.out.println("realMyLayer = " + realMyLayer + " maxLayer = " + maxLayer);
            String text = "";
          //  if (cc_servletType2.equalsIgnoreCase("InboxInternalServlet"))
          //  {
                text = GenericTree.getName(cc_geoName, id, cc_Language, cc_englishNameColumn, cc_banglaNameColumn);
         //   }
         
          //  else
          //  {
          //      text = GeoTree.getName(cc_geoName, id, cc_Language);
          //  }
			String layer_id_pairs = cc_treeDTO.layer_id_pairs + "_" + realMyLayer + "_" + id;
            text = text.replace("'", "");
    %>

    <% if (drawExtraUl) {
        if (alreadyDrawnAnExtraUL2) //need to close li
        {
            System.out.println("Already drawn, closing ul and li");
    %>
</ul>
<%
        alreadyDrawnAnExtraUL2 = false;
    }
%>
<li role="treeitem" aria-expanded="true" id="cc_extra_li_<%=recursiveNodeType%>_<%=cc_geoName%>_<%=id%>"
    class="jstree-node  jstree-open" state="2">
    <i
            class="fa fa-arrows" id="cc_extra_plus_<%=recursiveNodeType%>_<%=cc_geoName%>_<%=id%>"
            onclick="getChildren2(0, 0, 'cc_extra_li_<%=recursiveNodeType%>_<%=cc_geoName%>_<%=id%>','', null, 0,'', '');">
    </i>
    <a class="jstree-anchor" href="#"></a>
    <i class="jstree-icon jstree-themeicon icon icon-arrow-right jstree-themeicon-custom"></i>
    <%=cc_showName%>
    <%
        if (cc_hasExtraLayerPlusButton != null && cc_hasExtraLayerPlusButton.length >= cc_extraLayerIndex && cc_hasExtraLayerPlusButton[cc_extraLayerIndex]) {
    %>
    <i class="glyphicon glyphicon-plus"
       onclick="extraLayerClick('<%=cc_showName%>', '<%=cc_geoName%>', '<%=id%>')"></i>
    <%
        }
    %>

    <ul class="jstree-container-ul jstree-children" style="display:none">
        <%
                alreadyDrawnAnExtraUL2 = true;
                System.out.println("Opened ul and li");
            }
        %>

        <%
            if(id != -1)
            {
        %>

        <li role="treeitem" aria-expanded="true" id="cc_<%=cc_treeName%>_div_<%=cc_geoName%>_<%=id%>"
            class="jstree-node  jstree-open" state="0">

            <%
                if ((cc_isRecursive == 1 && nextLayer < cc_maxLayer - 1) || (cc_isRecursive == 0 && nextLayer < cc_maxLayer)) //not leaf
                {
            %>
            <i
                    class="fa fa-arrows" id="plus_<%=cc_geoName%>_<%=id%>"
                    onclick="getChildren2(
										'<%=nextLayer%>',
										'<%=id%>',
										'cc_<%=cc_treeName%>_div_<%=cc_geoName%>_<%=id%>',
										'<%=cc_actionType%>',
										null,
										<%=cc_parentID%>,
										'&prevLayer=<%=cc_myLayer%>',
										'<%=cc_treeName%>_checkbox_<%=cc_geoName%>_<%=id%>',
										'<%=layer_id_pairs%>'
										);"
            >
            </i>
            <%
                }
                if ((cc_checkBoxType == 1 && realMyLayer == cc_maxLayer - 1) || cc_checkBoxType == 2)
                {
            %>
            <input type="hidden" name="treeCBrecursiveNodeType" value="<%=recursiveNodeType%>"/>
            <input type="hidden" name="treeCBLayer" value="<%=realMyLayer%>"/>
            <input type="hidden" name="treeCBType" value="<%=cc_checkBoxType%>"/>
            <input type="hidden" name="treeCBStatus" value="0" id="treeCBStatus_<%=id%>"/>
            <input type="hidden" name="treeCBChecked" value="-1" id="treeCBChecked_<%=id%>"/>
            <input type="checkbox" id="cc_<%=cc_treeName%>_checkbox_<%=cc_geoName%>_<%=id%>" name="treeCB"
                   value="<%=id%>" data="<%=layer_id_pairs%>"
                   onclick="checkbox_toggeled('<%=id%>','<%=text%>','cc_<%=cc_treeName%>_checkbox_<%=cc_geoName%>_<%=id%>',
                           '<%=GenericTree.getNameFromChildID(id, cc_Language, cc_geoName)%>', 'cc_<%=cc_treeName%>_div_<%=cc_geoName%>_<%=id%>')"
                <%=cc_bParentCheckBoxChecked?"checked":""%>>
            <%
                }
            %>
            <a class="jstree-anchor" href="#"></a>
            <i class="jstree-icon jstree-themeicon icon icon-arrow-right jstree-themeicon-custom"></i>
            <a href="javascript:" data-type="division" data-id="1"
               onclick="editNode('<%=cc_geoName%>', '<%=id%>');"><%=text %>
            </a>

            <%
                if ((cc_plusButtonType == 1 && realMyLayer == cc_maxLayer - 1) || cc_plusButtonType == 2)
                {
            %>

            <span class="glyphicon glyphicon-plus" aria-hidden="true"
                  onclick="plusClicked('<%=cc_geoName%>', '<%=id%>', '<%=text %>');"></span>

            <%
                }
            %>
        </li>

        <%
            }
        %>

        <%
            }
        %>
        <%
            if (alreadyDrawnAnExtraUL2 && i4 == cc_nodeIDs.size())
            {
                System.out.println("Already drawn, closing ul and li");
            }
        %>
    </ul>
</li>
</ul>