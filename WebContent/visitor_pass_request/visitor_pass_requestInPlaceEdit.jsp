<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="visitor_pass_request.Visitor_pass_requestDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Visitor_pass_requestDTO visitor_pass_requestDTO = (Visitor_pass_requestDTO)request.getAttribute("visitor_pass_requestDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(visitor_pass_requestDTO == null)
{
	visitor_pass_requestDTO = new Visitor_pass_requestDTO();
	
}
System.out.println("visitor_pass_requestDTO = " + visitor_pass_requestDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.VISITOR_PASS_REQUEST_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=visitor_pass_requestDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeRecordsId'>")%>
			

		<input type='hidden' class='form-control'  name='employeeRecordsId' id = 'employeeRecordsId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + visitor_pass_requestDTO.employeeRecordsId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_visitDate'>")%>
			
	
	<div class="form-inline" id = 'visitDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'visitDate_date_<%=i%>' name='visitDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_visitDate = dateFormat.format(new Date(visitor_pass_requestDTO.visitDate));
	%>
	'<%=formatted_visitDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_visitTime'>")%>
			
	
	<div class="form-inline" id = 'visitTime_div_<%=i%>'>
			<%
                 value = "";
                 if(actionName.equals("edit")) {
                	 value = TimeFormat.getInAmPmFormat(test_babaDTO.timeOfBirth);
                 }
             %>	
			<div class='input-group date edms-datetimepicker'>
                <input type='text' class="form-control" value="<%=value%>" id = 'visitTime_time_<%=i%>' name='visitTime'  tag='pb_html'/>
                <span class="input-group-addon" style="width:45px;">
                   <span><i class="fa fa-clock-o"></i></span>
                </span>
            </div>	
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_requestDate'>")%>
			
	
	<div class="form-inline" id = 'requestDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'requestDate_date_<%=i%>' name='requestDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_requestDate = dateFormat.format(new Date(visitor_pass_requestDTO.requestDate));
	%>
	'<%=formatted_requestDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_purpose'>")%>
			
	
	<div class="form-inline" id = 'purpose_div_<%=i%>'>
		<input type='text' class='form-control'  name='purpose' id = 'purpose_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + visitor_pass_requestDTO.purpose + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isKnown'>")%>
			
	
	<div class="form-inline" id = 'isKnown_div_<%=i%>'>
		<input type='checkbox' class='form-control'  name='isKnown' id = 'isKnown_checkbox_<%=i%>' value='true' <%=(actionName.equals("edit") && String.valueOf(visitor_pass_requestDTO.isKnown).equals("true"))?("checked"):""%>   tag='pb_html'><br>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_referenceEmployeeRecordsId'>")%>
			

		<input type='hidden' class='form-control'  name='referenceEmployeeRecordsId' id = 'referenceEmployeeRecordsId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + visitor_pass_requestDTO.referenceEmployeeRecordsId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isApproved'>")%>
			
	
	<div class="form-inline" id = 'isApproved_div_<%=i%>'>
		<input type='checkbox' class='form-control'  name='isApproved' id = 'isApproved_checkbox_<%=i%>' value='true' <%=(actionName.equals("edit") && String.valueOf(visitor_pass_requestDTO.isApproved).equals("true"))?("checked"):""%>   tag='pb_html'><br>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_jobCat" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='jobCat' id = 'jobCat_hidden_<%=i%>' value='<%=visitor_pass_requestDTO.jobCat%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedByUserId" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=visitor_pass_requestDTO.insertedByUserId%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=visitor_pass_requestDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=visitor_pass_requestDTO.modifiedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + visitor_pass_requestDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=visitor_pass_requestDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Visitor_pass_requestServlet?actionType=view&ID=<%=visitor_pass_requestDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Visitor_pass_requestServlet?actionType=view&modal=1&ID=<%=visitor_pass_requestDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	