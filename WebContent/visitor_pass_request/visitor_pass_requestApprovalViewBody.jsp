

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="visitor_pass_request.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@ page import="approval_execution_table.*"%>
<%@ page import="approval_path.*"%>
<%@page import="user.*"%>
<%@page import="workflow.*"%>
<%@ page import="util.CommonConstant" %>


<%
	String servletName = "Visitor_pass_requestServlet";
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String Language = LM.getText(LC.VISITOR_PASS_REQUEST_EDIT_LANGUAGE, loginDTO);

	UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

	String actionName = "edit";
	String failureMessage = (String)request.getAttribute("failureMessage");
	if(failureMessage == null || failureMessage.isEmpty())
	{
		failureMessage = "";
	}
	out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
	String value = "";


	String ID = request.getParameter("ID");
	boolean isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));

	if(ID == null || ID.isEmpty())
	{
		ID = "0";
	}
	long id = Long.parseLong(ID);
	System.out.println("ID = " + ID + " isPermanentTable = " + isPermanentTable);
	Visitor_pass_requestDAO visitor_pass_requestDAO = new Visitor_pass_requestDAO("visitor_pass_request");
	Visitor_pass_requestDTO visitor_pass_requestDTO = (Visitor_pass_requestDTO)visitor_pass_requestDAO.getDTOByID(id);
	String Value = "";
	int i = 0;
	FilesDAO filesDAO = new FilesDAO();
	
	String tableName = "visitor_pass_request";

	Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
	ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
	Approval_execution_tableDTO approval_execution_tableDTO = null;
	Approval_execution_tableDTO approval_execution_table_initiationDTO = null;
	ApprovalPathDetailsDTO approvalPathDetailsDTO =  null;

	boolean canApprove = false, canValidate = false, isInitiator = false, canTerminate = false;
	boolean isInPreviousOffice = false;
	String Message = "Done";

	if(!isPermanentTable)
	{
		approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("visitor_pass_request", visitor_pass_requestDTO.iD);
		System.out.println("approval_execution_tableDTO = " + approval_execution_tableDTO);
		approvalPathDetailsDTO = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
		approval_execution_table_initiationDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getInitiationDTOByPreviousRowId("visitor_pass_request", approval_execution_tableDTO.previousRowId);
		if(approvalPathDetailsDTO!= null && approvalPathDetailsDTO.organogramId == userDTO.organogramID 
				&& approval_execution_tableDTO.iD == approval_execution_tableDAO.getMostRecentExecutionIDByPreviousRowID("visitor_pass_request", approval_execution_tableDTO.previousRowId))
		{
			canApprove = true;
			if(approvalPathDetailsDTO.approvalRoleCat == SessionConstants.VALIDATOR)
			{
				canValidate = true;
			}
		}
		
		isInitiator = WorkflowController.isInitiator(tableName, approval_execution_tableDTO.previousRowId, userDTO.organogramID);
		
		canTerminate = isInitiator && visitor_pass_requestDTO.isDeleted == 2;
	}	
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<div class="modal-content viewmodal">
            <div class="modal-header">
                <div class="col-md-12">
                    <div class="row">
                        
                        <div class="col-md-3 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>
                                </div>
                            </div>
                        </div>
                    </div>
					<button type="button" class="btn btn-info" id = 'printer' onclick="printAnyDiv('modalbody')" >Print</button>
                </div>


            </div>

            <div class="modal-body container" id="modalbody">
			
				<div class="row div_border office-div">

                    <div class="col-md-12">
                        <h5><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_PASS_REQUEST_ADD_FORMNAME, loginDTO)%></h5>
                    </div>
			



			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD__EMPLOYEERECORDSID, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="employeeRecordsId">
						
											<%
											value = visitor_pass_requestDTO.employeeRecordsId + "";
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD__VISITDATE, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="visitDate">
						
											<%
											value = visitor_pass_requestDTO.visitDate + "";
											%>
											<%
											String formatted_visitDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_visitDate%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD__VISITTIME, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="visitTime">
						
											<%
											value = visitor_pass_requestDTO.visitTime + "";
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD__REQUESTDATE, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="requestDate">
						
											<%
											value = visitor_pass_requestDTO.requestDate + "";
											%>
											<%
											String formatted_requestDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_requestDate%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD__PURPOSE, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="purpose">
						
											<%
											value = visitor_pass_requestDTO.purpose + "";
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD__ISKNOWN, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="isKnown">
						
											<%
											value = visitor_pass_requestDTO.isKnown + "";
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD__REFERENCEEMPLOYEERECORDSID, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="referenceEmployeeRecordsId">
						
											<%
											value = visitor_pass_requestDTO.referenceEmployeeRecordsId + "";
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD__ISAPPROVED, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="isApproved">
						
											<%
											value = visitor_pass_requestDTO.isApproved + "";
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				


			
			
			
			
			
			
			
		

				<div class="col-md-6">
				<label class="col-md-5" style="padding-right: 0px;"><b><span>Initiator Instructions</span><span style="float:right;">:</span></b></label>
				<label id="referenceNo">

					<%
						value = approval_execution_table_initiationDTO.remarks + "";
					%>

					<%=value%>



				</label>
			</div>
			
			<div class="col-md-6">
				<label class="col-md-5" style="padding-right: 0px;"><b><span>Initiator Files</span><span style="float:right;">:</span></b></label>
				<label id="filesDropzone"></label> <br/>

				
				<%
					{
					List<FilesDTO>  FilesDTOList = filesDAO.getMiniDTOsByFileID(approval_execution_table_initiationDTO.fileDropzone);
						%>
						<table>
						<tr>
						<%
						if(FilesDTOList != null)
						{
							for(int j = 0; j < FilesDTOList.size(); j ++)
							{
								FilesDTO filesDTO = FilesDTOList.get(j);
								byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
								%>
								<td>
								<%
								if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)
								{
									%>
									<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px' />
									<%
								}
								%>
								<a href = 'Approval_execution_tableServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>
								</td>
							<%
							}
						}
						%>
						</tr>
						</table>
					<%												
					}
					%>
			</div>

			</div>
				

                <div class="row div_border attachement-div">
                    <div class="col-md-12">
                        <h5><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_VISITORNAME, loginDTO)%></th>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_VISTORFATHERNAME, loginDTO)%></th>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_IDENTIFICATIONCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_IDENTIFICATIONNUMBER, loginDTO)%></th>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_CONTACTNUMBER1, loginDTO)%></th>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_CONTACTNUMBER2, loginDTO)%></th>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_VISITORADDRESS, loginDTO)%></th>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_FILESDROPZONE, loginDTO)%></th>
							</tr>
							<%
                        	VisitorDetailsDAO visitorDetailsDAO = new VisitorDetailsDAO();
                         	List<VisitorDetailsDTO> visitorDetailsDTOs = visitorDetailsDAO.getVisitorDetailsDTOListByVisitorPassRequestID(visitor_pass_requestDTO.iD);
                         	
                         	for(VisitorDetailsDTO visitorDetailsDTO: visitorDetailsDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%
											value = visitorDetailsDTO.visitorName + "";
											%>
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = visitorDetailsDTO.vistorFatherName + "";
											%>
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = visitorDetailsDTO.identificationCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "identification", visitorDetailsDTO.identificationCat);
											%>																						
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = visitorDetailsDTO.identificationNumber + "";
											%>
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = visitorDetailsDTO.contactNumber1 + "";
											%>
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = visitorDetailsDTO.contactNumber2 + "";
											%>
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = visitorDetailsDTO.visitorAddress + "";
											%>
											<%=GeoLocationDAO2.parseEnText(value)%>
											<%
											{
												String addressdetails = GeoLocationDAO2.parseDetails(value);
												if(!addressdetails.equals(""))
												{
												%>
													, <%=addressdetails%>
												<%
												}
											}
											%>

				
			
										</td>
										<td>
											<%
											value = visitorDetailsDTO.filesDropzone + "";
											%>
											<%
											{
												List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(visitorDetailsDTO.filesDropzone);
												%>
												<table>
												<tr>
												<%
												if(FilesDTOList != null)
												{
													for(int j = 0; j < FilesDTOList.size(); j ++)
													{
														FilesDTO filesDTO = FilesDTOList.get(j);
														byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
														%>
														<td>
														<%
														if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)
														{
															%>
															<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px' />
															<%
														}
														%>
														<a href = 'Visitor_pass_requestServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>
														</td>
													<%
													}
												}
												%>
												</tr>
												</table>
											<%												
											}
											%>
				
			
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
                <div class="row div_border attachement-div">
                    <div class="col-md-12">
                        <h5><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_ALLOWED_BELONGINGS, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_ALLOWED_BELONGINGS_VISITORBELONGINGCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_ALLOWED_BELONGINGS_TOTALCOUNT, loginDTO)%></th>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_ALLOWED_BELONGINGS_REMARKS, loginDTO)%></th>
							</tr>
							<%
                        	VisitorAllowedBelongingsDAO visitorAllowedBelongingsDAO = new VisitorAllowedBelongingsDAO();
                         	List<VisitorAllowedBelongingsDTO> visitorAllowedBelongingsDTOs = visitorAllowedBelongingsDAO.getVisitorAllowedBelongingsDTOListByVisitorPassRequestID(visitor_pass_requestDTO.iD);
                         	
                         	for(VisitorAllowedBelongingsDTO visitorAllowedBelongingsDTO: visitorAllowedBelongingsDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%
											value = visitorAllowedBelongingsDTO.visitorBelongingCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "visitor_belonging", visitorAllowedBelongingsDTO.visitorBelongingCat);
											%>																						
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = visitorAllowedBelongingsDTO.totalCount + "";
											%>
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = visitorAllowedBelongingsDTO.remarks + "";
											%>
														
											<%=value%>
				
			
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
               


        

					<%if(!isPermanentTable && (canApprove || canTerminate)) {%>
				
						<div class="row div_border attachement-div">
							<div class="col-md-12">
								<h5><%=LM.getText(LC.HM_ATTACHMENTS, loginDTO )%></h5>
								<%
									long ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
								%>
								<div class="dropzone"
									 action="Approval_execution_tableServlet?pageType=add&actionType=UploadFilesFromDropZone&columnName=approval_attached_fileDropzone&ColumnID=<%=ColumnID%>">
									<input type='file' style="display: none"
										   name='approval_attached_fileDropzoneFile'
										   id='approval_attached_fileDropzone_dropzone_File_<%=i%>'
										   tag='pb_html' />
								</div>
								<input type='hidden'
									   name='approval_attached_fileDropzoneFilesToDelete'
									   id='approval_attached_fileDropzoneFilesToDelete_<%=i%>' value=''
									   tag='pb_html' /> <input type='hidden'
															   name='approval_attached_fileDropzone'
															   id='approval_attached_fileDropzone_dropzone_<%=i%>' tag='pb_html'
															   value='<%=ColumnID%>' />
							</div>
				
							<div class="col-md-12">
								<h5><%=LM.getText(LC.HM_REMARKS, loginDTO )%></h5>
				
								<textarea class='form-control'  name='remarks' id = '<%=i%>_remarks'   tag='pb_html'></textarea>
							</div>
						</div>
						<%}%>
						<div class="form-actions text-center">
						<%
							if(canApprove)
							{
							%>
							<button type="submit"  class="btn btn-success" id = 'approve_<%=id%>' data-toggle="modal" data-target="#approvalModal" ><%=LM.getText(LC.HM_APPROVE, loginDTO)%></button>
							<%@include file="../approval_path/approvalModal.jsp"%>
							<button type="submit" class="btn btn-danger" id = 'reject_<%=id%>' data-toggle="modal" data-target="#rejectionModal"><%=LM.getText(LC.HM_REJECT, loginDTO)%></button>
							<%@include file="../approval_path/rejectionModal.jsp"%>
							<%
							}
							if(canTerminate)
							{
							%>
							<button type="submit"  class="btn btn-success" id = 'approve_<%=id%>' onclick='approve("<%=approval_execution_tableDTO.updatedRowId%>", "<%=Message%>", <%=i%>, "Visitor_pass_requestServlet", 2, true, true)'><%=LM.getText(LC.HM_TERMINATE, loginDTO)%></button>
							<%
							}							
							if(canValidate)
							{
							%>
							<a type="submit"  class="btn btn-success" id = 'validate_<%=id%>' href='Visitor_pass_requestServlet?actionType=getEditPage&ID=<%=id%>&isPermanentTable=<%=isPermanentTable%>'><%=LM.getText(LC.HM_VALIDATE, loginDTO)%></a>
							<%
							}
							%>
							<button type="submit"  class="btn btn-success" id = 'history_<%=id%>' data-toggle="modal" data-target="#historyModal"><%=LM.getText(LC.HM_HISTORY, loginDTO)%></button>
							<%@include file="../approval_path/historyModal.jsp"%>	
						</div>
					
           

        </div>
        
        
 <script type="text/javascript">
 window.onload =function ()
 {
     console.log("using ckEditor");
     CKEDITOR.replaceAll();
 }
 </script>