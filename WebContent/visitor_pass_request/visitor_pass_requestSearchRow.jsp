<%@page pageEncoding="UTF-8" %>

<%@page import="visitor_pass_request.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@ page import="approval_execution_table.*"%>
<%@ page import="approval_path.*"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.VISITOR_PASS_REQUEST_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_VISITOR_PASS_REQUEST;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Visitor_pass_requestDTO visitor_pass_requestDTO = (Visitor_pass_requestDTO)request.getAttribute("visitor_pass_requestDTO");
CommonDTO commonDTO = visitor_pass_requestDTO;
String servletName = "Visitor_pass_requestServlet";

Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
Approval_execution_tableDTO approval_execution_tableDTO = null;
String Message = "Done";
approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("visitor_pass_request", visitor_pass_requestDTO.iD);

System.out.println("visitor_pass_requestDTO = " + visitor_pass_requestDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Visitor_pass_requestDAO visitor_pass_requestDAO = (Visitor_pass_requestDAO)request.getAttribute("visitor_pass_requestDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

											
		
											
											<td id = '<%=i%>_employeeRecordsId'>
											<%
											value = visitor_pass_requestDTO.employeeRecordsId + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_visitDate'>
											<%
											value = visitor_pass_requestDTO.visitDate + "";
											%>
											<%
											String formatted_visitDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_visitDate%>
				
			
											</td>
		
											
											<td id = '<%=i%>_visitTime'>
											<%
											value = visitor_pass_requestDTO.visitTime + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_requestDate'>
											<%
											value = visitor_pass_requestDTO.requestDate + "";
											%>
											<%
											String formatted_requestDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_requestDate%>
				
			
											</td>
		
											
											<td id = '<%=i%>_purpose'>
											<%
											value = visitor_pass_requestDTO.purpose + "";
											%>
														
											<%=value%>
				
											</td>

											<td>
												<a href='Visitor_pass_requestServlet?actionType=view&ID=<%=visitor_pass_requestDTO.iD%>'>View</a>
										
											</td>
	
											<td id = '<%=i%>_Edit'>																																	
	
												<a href='Visitor_pass_requestServlet?actionType=getEditPage&ID=<%=visitor_pass_requestDTO.iD%>'><%=LM.getText(LC.VISITOR_PASS_REQUEST_SEARCH_VISITOR_PASS_REQUEST_EDIT_BUTTON, loginDTO)%></a>
																				
											</td>											
											
											
											<td>
											<%
												if(visitor_pass_requestDTO.isDeleted == 0 && approval_execution_tableDTO != null)
												{
											%>
												<a href="Approval_execution_tableServlet?actionType=search&tableName=visitor_pass_request&previousRowId=<%=approval_execution_tableDTO.previousRowId%>"  ><%=LM.getText(LC.HM_HISTORY, loginDTO)%></a>
											<%
												}
												else
												{
											%>
												<%=LM.getText(LC.HM_NO_HISTORY_IS_AVAILABLE, loginDTO)%>
											<%
												}
											%>
											</td>
											
											<td>
											<%
											if(visitor_pass_requestDTO.jobCat == SessionConstants.DEFAULT_JOB_CAT)
											{
											%>
												<button type="button" class="btn btn-success" data-toggle="modal" data-target="#sendToApprovalPathModal" >
													<%=LM.getText(LC.HM_SEND_TO_APPROVAL_PATH, loginDTO)%>
												</button>
												<%@include file="../inbox_internal/sendToApprovalPathModal.jsp"%>
											<%
											}
											else
											{
											%>
											<%=LM.getText(LC.HM_NO_ACTION_IS_REQUIRED, loginDTO)%>
											<%
											}
											%>
											</td>
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=visitor_pass_requestDTO.iD%>'/></span>
												</div>
											</td>
																						
											
<script>
window.onload =function ()
{
    console.log("using ckEditor");
    CKEDITOR.replaceAll();
}
	
</script>	

