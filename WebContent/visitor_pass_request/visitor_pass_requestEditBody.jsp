
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="visitor_pass_request.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@ page import="approval_execution_table.*"%>
<%@ page import="approval_path.*"%>
<%@ page import="user.*"%>

<%@page import="workflow.*"%>
<%@page import="util.TimeFormat"%>
<%@page import="employee_records.*" %>
<%

List<Employee_recordsDTO> employeeRecordsDto =(List<Employee_recordsDTO>) request.getAttribute("employeeRecordsDto");
long currentEmployeeRecordsId = (long)request.getAttribute("currentEmployeeRecordsId");
Visitor_pass_requestDTO visitor_pass_requestDTO;
visitor_pass_requestDTO = (Visitor_pass_requestDTO)request.getAttribute("visitor_pass_requestDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
if(visitor_pass_requestDTO == null)
{
	visitor_pass_requestDTO = new Visitor_pass_requestDTO();
	
}
System.out.println("visitor_pass_requestDTO = " + visitor_pass_requestDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_PASS_REQUEST_ADD_FORMNAME, loginDTO);


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

long ColumnID;
FilesDAO filesDAO = new FilesDAO();
boolean isPermanentTable = true;
if(request.getParameter("isPermanentTable") != null)
{
	isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
}

Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
Approval_execution_tableDTO approval_execution_tableDTO = null;
Approval_execution_tableDTO approval_execution_table_initiationDTO = null;
ApprovalPathDetailsDTO approvalPathDetailsDTO =  null;

String tableName = "visitor_pass_request";

boolean canApprove = false, canValidate = false, isInitiator = false, canTerminate = false;

if(!isPermanentTable)
{
	approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("visitor_pass_request", visitor_pass_requestDTO.iD);
	System.out.println("approval_execution_tableDTO = " + approval_execution_tableDTO);
	approvalPathDetailsDTO = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
	approval_execution_table_initiationDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getInitiationDTOByUpdatedRowId("visitor_pass_request", visitor_pass_requestDTO.iD);
	if(approvalPathDetailsDTO!= null && approvalPathDetailsDTO.organogramId == userDTO.organogramID)
	{
		canApprove = true;
		if(approvalPathDetailsDTO.approvalRoleCat == SessionConstants.VALIDATOR)
		{
			canValidate = true;
		}
	}
	
	isInitiator = WorkflowController.isInitiator(tableName, approval_execution_tableDTO.previousRowId, userDTO.organogramID);
	
	canTerminate = isInitiator && visitor_pass_requestDTO.isDeleted == 2;
}
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="Visitor_pass_requestServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">

<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.VISITOR_PASS_REQUEST_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=visitor_pass_requestDTO.iD%>' tag='pb_html'/>
	
												

		<input type='hidden' class='form-control'  name='employeeRecordsId' id = 'employeeRecordsId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + visitor_pass_requestDTO.employeeRecordsId + "'"):("'" + currentEmployeeRecordsId + "'")%> tag='pb_html'/>
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.VISITOR_PASS_REQUEST_EDIT_VISITDATE, loginDTO)):(LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITDATE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'visitDate_div_<%=i%>'>	
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'visitDate_date_<%=i%>' name='visitDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_visitDate = dateFormat.format(new Date(visitor_pass_requestDTO.visitDate));
	%>
	'<%=formatted_visitDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.VISITOR_PASS_REQUEST_EDIT_VISITTIME, loginDTO)):(LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITTIME, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'visitTime_div_<%=i%>'>	
			<%
                 value = "";
                 if(actionName.equals("edit")) {
                	 value = TimeFormat.getInAmPmFormat(visitor_pass_requestDTO.visitTime);
                 }
             %>	
			<div class='input-group date edms-datetimepicker'>
                <input type='text' class="form-control" value="<%=value%>" id = 'visitTime_time_<%=i%>' name='visitTime'  tag='pb_html'/>
                <span class="input-group-addon" style="width:45px;">
                   <span><i class="fa fa-clock-o"></i></span>
                </span>
            </div>	
		
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.VISITOR_PASS_REQUEST_EDIT_REQUESTDATE, loginDTO)):(LM.getText(LC.VISITOR_PASS_REQUEST_ADD_REQUESTDATE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'requestDate_div_<%=i%>'>	
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'requestDate_date_<%=i%>' name='requestDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_requestDate = dateFormat.format(new Date(visitor_pass_requestDTO.requestDate));
	%>
	'<%=formatted_requestDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.VISITOR_PASS_REQUEST_EDIT_PURPOSE, loginDTO)):(LM.getText(LC.VISITOR_PASS_REQUEST_ADD_PURPOSE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'purpose_div_<%=i%>'>	
		<input type='text' class='form-control'  name='purpose' id = 'purpose_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + visitor_pass_requestDTO.purpose + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.VISITOR_PASS_REQUEST_EDIT_ISKNOWN, loginDTO)):(LM.getText(LC.VISITOR_PASS_REQUEST_ADD_ISKNOWN, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'isKnown_div_<%=i%>'>	
		<input type='checkbox' class='form-control'  name='isKnown' id = 'isKnown_checkbox_<%=i%>' value='true' <%=(actionName.equals("edit") && String.valueOf(visitor_pass_requestDTO.isKnown).equals("true"))?("checked"):""%>   tag='pb_html'><br>
	</div>
</div>
			
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.VISITOR_PASS_REQUEST_EDIT_REFERENCEEMPLOYEERECORDSID, loginDTO)):(LM.getText(LC.VISITOR_PASS_REQUEST_ADD_REFERENCEEMPLOYEERECORDSID, loginDTO))%>
</label>				
<div class="form-group ">					
	<div class="col-lg-6 " id = 'referenceEmployeeRecords'>	
		<select class='form-control'  name='referenceEmployeeRecordsId' id = 'referenceEmployeeRecordsId'   tag='pb_html'>
		    <%
		    for(int ind=0;ind<employeeRecordsDto.size();ind++) { %>
		    <option value="<%=employeeRecordsDto.get(ind).iD%>"> <%=employeeRecordsDto.get(ind).nameEng%>	</option>
		   <%}
		    %>
		</select>
		
	</div>
</div>	
		<input type='hidden' class='form-control'  name='referenceEmployeeRecordsId' id = 'referenceEmployeeRecordsId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + visitor_pass_requestDTO.referenceEmployeeRecordsId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.VISITOR_PASS_REQUEST_EDIT_ISAPPROVED, loginDTO)):(LM.getText(LC.VISITOR_PASS_REQUEST_ADD_ISAPPROVED, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'isApproved_div_<%=i%>'>	
		<input type='checkbox' class='form-control'  name='isApproved' id = 'isApproved_checkbox_<%=i%>' value='true' <%=(actionName.equals("edit") && String.valueOf(visitor_pass_requestDTO.isApproved).equals("true"))?("checked"):""%>   tag='pb_html'><br>
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='jobCat' id = 'jobCat_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + visitor_pass_requestDTO.jobCat + "'"):("'" + "-1" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + visitor_pass_requestDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + visitor_pass_requestDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + visitor_pass_requestDTO.modifiedBy + "'"):("'" + "" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + visitor_pass_requestDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + visitor_pass_requestDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
					
	





				<div class="col-md-12" style="padding-top: 20px;">
					<legend class="text-left content_legend"><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS, loginDTO)%></legend>
				</div>

				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_VISITORNAME, loginDTO)%></th>
										<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_VISTORFATHERNAME, loginDTO)%></th>
										<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_IDENTIFICATIONCAT, loginDTO)%></th>
										<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_IDENTIFICATIONNUMBER, loginDTO)%></th>
										<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_CONTACTNUMBER1, loginDTO)%></th>
										<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_CONTACTNUMBER2, loginDTO)%></th>
										<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_VISITORADDRESS, loginDTO)%></th>
										<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_FILESDROPZONE, loginDTO)%></th>
										<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_REMOVE, loginDTO)%></th>
									</tr>
								</thead>
								<tbody id="field-VisitorDetails">
						
						
<%
	if(actionName.equals("edit")){
		int index = -1;
		
		
		for(VisitorDetailsDTO visitorDetailsDTO: visitor_pass_requestDTO.visitorDetailsDTOList)
		{
			index++;
			
			System.out.println("index index = "+index);

%>	
							
									<tr id = "VisitorDetails_<%=index + 1%>">
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorDetails.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=visitorDetailsDTO.iD%>' tag='pb_html'/>
	
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorDetails.visitorPassRequestId' id = 'visitorPassRequestId_hidden_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + visitorDetailsDTO.visitorPassRequestId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
										</td>
										<td>										











		<input type='text' class='form-control'  name='visitorDetails.visitorName' id = 'visitorName_text_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + visitorDetailsDTO.visitorName + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
										</td>
										<td>										











		<input type='text' class='form-control'  name='visitorDetails.vistorFatherName' id = 'vistorFatherName_text_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + visitorDetailsDTO.vistorFatherName + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
										</td>
										<td>										











		<select class='form-control'  name='visitorDetails.identificationCat' id = 'identificationCat_category_<%=childTableStartingID%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "identification", visitorDetailsDTO.identificationCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "identification", -1);			
}
%>
<%=Options%>
		</select>
		
										</td>
										<td>										











		<input type='text' class='form-control'  name='visitorDetails.identificationNumber' id = 'identificationNumber_text_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + visitorDetailsDTO.identificationNumber + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
										</td>
										<td>										











		<input type='text' class='form-control'  name='visitorDetails.contactNumber1' id = 'contactNumber1_text_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + visitorDetailsDTO.contactNumber1 + "'"):("'" + "" + "'")%> <% 
	if(!actionName.equals("edit"))
	{
%>
		required="required" title="contactNumber1 must start with 880, then contain 10 digits"
<%
	}
%>
  tag='pb_html'/>					
										</td>
										<td>										
		<input type='text' class='form-control'  name='visitorDetails.contactNumber2' id = 'contactNumber2_text_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + visitorDetailsDTO.contactNumber2 + "'"):("'" + "" + "'")%>
  tag='pb_html'/>					
										</td>
										
										
										<td>										

		<div id ='visitorAddress_geoDIV_<%=childTableStartingID%>' tag='pb_html'>
			<select class='form-control' name='visitorAddress_active' id = 'visitorAddress_geoSelectField_<%=childTableStartingID%>' onChange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'visitorAddress', this.getAttribute('row'))"  tag='pb_html' row = '<%=i%>'></select>
		</div>
		<input type='text' class='form-control' onkeypress="return (event.charCode != 36 && event.keyCode != 36)" name='visitorAddress_text' id = 'visitorAddress_geoTextField_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(visitorDetailsDTO.visitorAddress)  + "'"):("'" + "" + "'")%> placeholder='Road Number, House Number etc' tag='pb_html'>
		<input type='hidden' class='form-control'  name='visitorDetails.visitorAddress' id = 'visitorAddress_geolocation_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseID(visitorDetailsDTO.visitorAddress)  + "'"):("'" + "1" + "'")%> tag='pb_html'>
		<%
		if(actionName.equals("edit"))
		{
		%>
		<label class="control-label"><%=GeoLocationDAO2.parseEnText(visitorDetailsDTO.visitorAddress) + "," + GeoLocationDAO2.parseDetails(visitorDetailsDTO.visitorAddress)%></label>
		<%
			}
		%>
			
						
										</td>
										<td>										











		<%
													if(actionName.equals("edit"))
														{
													List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(visitorDetailsDTO.filesDropzone);
												%>			
			<table>
				<tr>
			<%
				if(filesDropzoneDTOList != null)
				{
					for(int j = 0; j < filesDropzoneDTOList.size(); j ++)
					{
						FilesDTO filesDTO = filesDropzoneDTOList.get(j);
						byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
			%>
					<td id = 'filesDropzone_td_<%=filesDTO.iD%>'>
					<%
						if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)
								{
					%>
						<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px' />
						<%
							}
						%>
					<a href = 'Visitor_pass_requestServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>
					<a class='btn btn-danger' onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>		
					</td>
					<%
						}
						}
					%>
			</tr>
			</table>
			<%
				}
			%>
		
		<%
					ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
				%>			
		<div class="dropzone" action="Visitor_pass_requestServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?visitorDetailsDTO.filesDropzone:ColumnID%>">
			<input type='file' style="display:none"  name='filesDropzoneFile' id = 'filesDropzone_dropzone_File_<%=i%>'  tag='pb_html'/>			
		</div>								
		<input type='hidden'  name='filesDropzoneFilesToDelete' id = 'filesDropzoneFilesToDelete_<%=i%>' value=''  tag='pb_html'/>
		<input type='hidden' name='visitorDetails.filesDropzone' id = 'filesDropzone_dropzone_<%=i%>'  tag='pb_html' value='<%=actionName.equals("edit")?visitorDetailsDTO.filesDropzone:ColumnID%>'/>		


										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorDetails.insertedByUserId' id = 'insertedByUserId_hidden_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + visitorDetailsDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorDetails.insertionDate' id = 'insertionDate_hidden_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + visitorDetailsDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorDetails.modifiedBy' id = 'modifiedBy_hidden_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + visitorDetailsDTO.modifiedBy + "'"):("'" + "" + "'")%> tag='pb_html'/>
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorDetails.isDeleted' id = 'isDeleted_hidden_<%=childTableStartingID%>' value= <%=actionName.equals("edit")?("'" + visitorDetailsDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorDetails.lastModificationTime' id = 'lastModificationTime_hidden_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + visitorDetailsDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
										</td>
										<td><div class='checker'><span id='chkEdit' ><input type='checkbox' id='visitorDetails_cb_<%=index%>' name='checkbox' value=''/></span></div></td>
									</tr>								
<%
									childTableStartingID ++;
										}
									}
								%>						
						
								</tbody>
							</table>
						
						
						
					</div>
					<div class="form-group">
						<div class="col-xs-9 text-right">

							<button id="remove-VisitorDetails" name="removeVisitorDetails" type="button"
									class="btn btn-danger remove-me1"><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_REMOVE, loginDTO)%></button>

							<button id="add-more-VisitorDetails" name="add-moreVisitorDetails" type="button"
									class="btn btn-primary"><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_ADD_MORE, loginDTO)%></button>

						</div>
					</div>
					
					<%
											VisitorDetailsDTO visitorDetailsDTO = new VisitorDetailsDTO();
										%>
					
					<template id="template-VisitorDetails" >						
								<tr>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorDetails.iD' id = 'iD_hidden_' value='<%=visitorDetailsDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorDetails.visitorPassRequestId' id = 'visitorPassRequestId_hidden_' value=<%=actionName.equals("edit")?("'" + visitorDetailsDTO.visitorPassRequestId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
									</td>
									<td>











		<input type='text' class='form-control'  name='visitorDetails.visitorName' id = 'visitorName_text_' value=<%=actionName.equals("edit")?("'" + visitorDetailsDTO.visitorName + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
									</td>
									<td>











		<input type='text' class='form-control'  name='visitorDetails.vistorFatherName' id = 'vistorFatherName_text_' value=<%=actionName.equals("edit")?("'" + visitorDetailsDTO.vistorFatherName + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
									</td>
									<td>











		<select class='form-control'  name='visitorDetails.identificationCat' id = 'identificationCat_category_'   tag='pb_html'>		
<%
			if(actionName.equals("edit"))
		{
			Options = CatDAO.getOptions(Language, "identification", visitorDetailsDTO.identificationCat);
		}
		else
		{			
			Options = CatDAO.getOptions(Language, "identification", -1);			
		}
		%>
<%=Options%>
		</select>
		
									</td>
									<td>











		<input type='text' class='form-control'  name='visitorDetails.identificationNumber' id = 'identificationNumber_text_' value=<%=actionName.equals("edit")?("'" + visitorDetailsDTO.identificationNumber + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
									</td>
									<td>











		<input type='text' class='form-control'  name='visitorDetails.contactNumber1' id = 'contactNumber1_text_' value=<%=actionName.equals("edit")?("'" + visitorDetailsDTO.contactNumber1 + "'"):("'" + "" + "'")%> <%if(!actionName.equals("edit"))
	{%>
		required="required" title="contactNumber1 must start with 880, then contain 10 digits"
<%}%>
  tag='pb_html'/>					
									</td>
									<td>











		<input type='text' class='form-control'  name='visitorDetails.contactNumber2' id = 'contactNumber2_text_' value=<%=actionName.equals("edit")?("'" + visitorDetailsDTO.contactNumber2 + "'"):("'" + "" + "'")%>
  tag='pb_html'/>					
									</td>
									<td>











		<div id ='visitorAddress_geoDIV_' tag='pb_html'>
			<select class='form-control' name='visitorAddress_active' id = 'visitorAddress_geoSelectField_' onChange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'visitorAddress', this.getAttribute('row'))"  tag='pb_html' row = '<%=i%>'></select>
		</div>
		<input type='text' class='form-control' onkeypress="return (event.charCode != 36 && event.keyCode != 36)" name='visitorAddress_text' id = 'visitorAddress_geoTextField_' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(visitorDetailsDTO.visitorAddress)  + "'"):("'" + "" + "'")%> placeholder='Road Number, House Number etc' tag='pb_html'>
		<input type='hidden' class='form-control'  name='visitorDetails.visitorAddress' id = 'visitorAddress_geolocation_' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseID(visitorDetailsDTO.visitorAddress)  + "'"):("'" + "1" + "'")%> tag='pb_html'>
		<%
			if(actionName.equals("edit"))
				{
		%>
		<label class="control-label"><%=GeoLocationDAO2.parseEnText(visitorDetailsDTO.visitorAddress) + "," + GeoLocationDAO2.parseDetails(visitorDetailsDTO.visitorAddress)%></label>
		<%
		}
		%>
			
						
									</td>
									<td>











		<%
		if(actionName.equals("edit"))
		{
			List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(visitorDetailsDTO.filesDropzone);
			%>			
			<table>
				<tr>
			<%
			if(filesDropzoneDTOList != null)
			{
				for(int j = 0; j < filesDropzoneDTOList.size(); j ++)
				{
					FilesDTO filesDTO = filesDropzoneDTOList.get(j);
					byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
					%>
					<td id = 'filesDropzone_td_<%=filesDTO.iD%>'>
					<%
					if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)
					{
						%>
						<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64) 
						%>' style='width:100px' />
						<%
					}
					%>
					<a href = 'Visitor_pass_requestServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>
					<a class='btn btn-danger' onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>		
					</td>
					<%
				}
			}
			%>
			</tr>
			</table>
			<%
		}
		%>
		
		<%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>			
		<div class="dropzone" action="Visitor_pass_requestServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?visitorDetailsDTO.filesDropzone:ColumnID%>">
			<input type='file' style="display:none"  name='filesDropzoneFile' id = 'filesDropzone_dropzone_File_<%=i%>'  tag='pb_html'/>			
		</div>								
		<input type='hidden'  name='filesDropzoneFilesToDelete' id = 'filesDropzoneFilesToDelete_<%=i%>' value=''  tag='pb_html'/>
		<input type='hidden' name='visitorDetails.filesDropzone' id = 'filesDropzone_dropzone_<%=i%>'  tag='pb_html' value='<%=actionName.equals("edit")?visitorDetailsDTO.filesDropzone:ColumnID%>'/>		


									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorDetails.insertedByUserId' id = 'insertedByUserId_hidden_' value=<%=actionName.equals("edit")?("'" + visitorDetailsDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorDetails.insertionDate' id = 'insertionDate_hidden_' value=<%=actionName.equals("edit")?("'" + visitorDetailsDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorDetails.modifiedBy' id = 'modifiedBy_hidden_' value=<%=actionName.equals("edit")?("'" + visitorDetailsDTO.modifiedBy + "'"):("'" + "" + "'")%> tag='pb_html'/>
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorDetails.isDeleted' id = 'isDeleted_hidden_' value= <%=actionName.equals("edit")?("'" + visitorDetailsDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorDetails.lastModificationTime' id = 'lastModificationTime_hidden_' value=<%=actionName.equals("edit")?("'" + visitorDetailsDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
									</td>
									<td><div><span id='chkEdit' ><input type='checkbox' name='checkbox' value=''/></span></div></td>
								</tr>								
						
					</template>
				</div>		
				<div class="col-md-12" style="padding-top: 20px;">
					<legend class="text-left content_legend"><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_ALLOWED_BELONGINGS, loginDTO)%></legend>
				</div>

				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_ALLOWED_BELONGINGS_VISITORBELONGINGCAT, loginDTO)%></th>
										<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_ALLOWED_BELONGINGS_TOTALCOUNT, loginDTO)%></th>
										<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_ALLOWED_BELONGINGS_REMARKS, loginDTO)%></th>
										<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_ALLOWED_BELONGINGS_REMOVE, loginDTO)%></th>
									</tr>
								</thead>
								<tbody id="field-VisitorAllowedBelongings">
						
						
<%
	if(actionName.equals("edit")){
		int index = -1;
		
		
		for(VisitorAllowedBelongingsDTO visitorAllowedBelongingsDTO: visitor_pass_requestDTO.visitorAllowedBelongingsDTOList)
		{
			index++;
			
			System.out.println("index index = "+index);

%>	
							
									<tr id = "VisitorAllowedBelongings_<%=index + 1%>">
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorAllowedBelongings.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=visitorAllowedBelongingsDTO.iD%>' tag='pb_html'/>
	
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorAllowedBelongings.visitorPassRequestId' id = 'visitorPassRequestId_hidden_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + visitorAllowedBelongingsDTO.visitorPassRequestId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
										</td>
										<td>										











		<select class='form-control'  name='visitorAllowedBelongings.visitorBelongingCat' id = 'visitorBelongingCat_category_<%=childTableStartingID%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "visitor_belonging", visitorAllowedBelongingsDTO.visitorBelongingCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "visitor_belonging", -1);			
}
%>
<%=Options%>
		</select>
		
										</td>
										<td>										











		<input type='text' class='form-control'  name='visitorAllowedBelongings.totalCount' id = 'totalCount_text_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + visitorAllowedBelongingsDTO.totalCount + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
										</td>
										<td>										











		<input type='text' class='form-control'  name='visitorAllowedBelongings.remarks' id = 'remarks_text_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + visitorAllowedBelongingsDTO.remarks + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorAllowedBelongings.insertedByUserId' id = 'insertedByUserId_hidden_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + visitorAllowedBelongingsDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorAllowedBelongings.insertionDate' id = 'insertionDate_hidden_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + visitorAllowedBelongingsDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorAllowedBelongings.modifiedBy' id = 'modifiedBy_hidden_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + visitorAllowedBelongingsDTO.modifiedBy + "'"):("'" + "" + "'")%> tag='pb_html'/>
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorAllowedBelongings.isDeleted' id = 'isDeleted_hidden_<%=childTableStartingID%>' value= <%=actionName.equals("edit")?("'" + visitorAllowedBelongingsDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorAllowedBelongings.lastModificationTime' id = 'lastModificationTime_hidden_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + visitorAllowedBelongingsDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
										</td>
										<td><div class='checker'><span id='chkEdit' ><input type='checkbox' id='visitorAllowedBelongings_cb_<%=index%>' name='checkbox' value=''/></span></div></td>
									</tr>								
<%	
			childTableStartingID ++;
		}
	}
%>						
						
								</tbody>
							</table>
						
						
						
					</div>
					<div class="form-group">
						<div class="col-xs-9 text-right">

							<button id="remove-VisitorAllowedBelongings" name="removeVisitorAllowedBelongings" type="button"
									class="btn btn-danger remove-me1"><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_ALLOWED_BELONGINGS_REMOVE, loginDTO)%></button>

							<button id="add-more-VisitorAllowedBelongings" name="add-moreVisitorAllowedBelongings" type="button"
									class="btn btn-primary"><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_ALLOWED_BELONGINGS_ADD_MORE, loginDTO)%></button>

						</div>
					</div>
					
					<%VisitorAllowedBelongingsDTO visitorAllowedBelongingsDTO = new VisitorAllowedBelongingsDTO();%>
					
					<template id="template-VisitorAllowedBelongings" >						
								<tr>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorAllowedBelongings.iD' id = 'iD_hidden_' value='<%=visitorAllowedBelongingsDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorAllowedBelongings.visitorPassRequestId' id = 'visitorPassRequestId_hidden_' value=<%=actionName.equals("edit")?("'" + visitorAllowedBelongingsDTO.visitorPassRequestId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
									</td>
									<td>











		<select class='form-control'  name='visitorAllowedBelongings.visitorBelongingCat' id = 'visitorBelongingCat_category_'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "visitor_belonging", visitorAllowedBelongingsDTO.visitorBelongingCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "visitor_belonging", -1);			
}
%>
<%=Options%>
		</select>
		
									</td>
									<td>











		<input type='text' class='form-control'  name='visitorAllowedBelongings.totalCount' id = 'totalCount_text_' value=<%=actionName.equals("edit")?("'" + visitorAllowedBelongingsDTO.totalCount + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
									</td>
									<td>











		<input type='text' class='form-control'  name='visitorAllowedBelongings.remarks' id = 'remarks_text_' value=<%=actionName.equals("edit")?("'" + visitorAllowedBelongingsDTO.remarks + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorAllowedBelongings.insertedByUserId' id = 'insertedByUserId_hidden_' value=<%=actionName.equals("edit")?("'" + visitorAllowedBelongingsDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorAllowedBelongings.insertionDate' id = 'insertionDate_hidden_' value=<%=actionName.equals("edit")?("'" + visitorAllowedBelongingsDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorAllowedBelongings.modifiedBy' id = 'modifiedBy_hidden_' value=<%=actionName.equals("edit")?("'" + visitorAllowedBelongingsDTO.modifiedBy + "'"):("'" + "" + "'")%> tag='pb_html'/>
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorAllowedBelongings.isDeleted' id = 'isDeleted_hidden_' value= <%=actionName.equals("edit")?("'" + visitorAllowedBelongingsDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='visitorAllowedBelongings.lastModificationTime' id = 'lastModificationTime_hidden_' value=<%=actionName.equals("edit")?("'" + visitorAllowedBelongingsDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
									</td>
									<td><div><span id='chkEdit' ><input type='checkbox' name='checkbox' value=''/></span></div></td>
								</tr>								
						
					</template>
				</div>		

				<%if(canValidate)
				{
				%>	
				<div class="row div_border attachement-div">
							<div class="col-md-12">
								<h5>Attachments</h5>
								<%
									ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
								%>
								<div class="dropzone"
									 action="Approval_execution_tableServlet?pageType=add&actionType=UploadFilesFromDropZone&columnName=approval_attached_fileDropzone&ColumnID=<%=ColumnID%>">
									<input type='file' style="display: none"
										   name='approval_attached_fileDropzoneFile'
										   id='approval_attached_fileDropzone_dropzone_File_<%=i%>'
										   tag='pb_html' />
								</div>
								<input type='hidden'
									   name='approval_attached_fileDropzoneFilesToDelete'
									   id='approval_attached_fileDropzoneFilesToDelete_<%=i%>' value=''
									   tag='pb_html' /> <input type='hidden'
															   name='approval_attached_fileDropzone'
															   id='approval_attached_fileDropzone_dropzone_<%=i%>' tag='pb_html'
															   value='<%=ColumnID%>' />
							</div>
				
							<div class="col-md-12">
								<h5>Remarks</h5>
				
								<textarea class='form-control'  name='remarks' id = '<%=i%>_remarks'   tag='pb_html'></textarea>
							</div>
						</div>
				<%
				}
				%>
				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">					
						<%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_PASS_REQUEST_CANCEL_BUTTON, loginDTO)%>						
					</a>
					<button class="btn btn-success" type="submit">
					<%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_PASS_REQUEST_SUBMIT_BUTTON, loginDTO)%>						
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


$(document).ready( function(){

    dateTimeInit("<%=Language%>");
});

function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}

	preprocessCheckBoxBeforeSubmitting('isKnown', row);		
	preprocessCheckBoxBeforeSubmitting('isApproved', row);		

	for(i = 1; i < child_table_extra_id; i ++)
	{
		if(document.getElementById("visitorAddress_geolocation_" + i))
		{
			if(document.getElementById("visitorAddress_geolocation_" + i).getAttribute("processed") == null)
			{
				preprocessGeolocationBeforeSubmitting('visitorAddress', i, false);
				document.getElementById("visitorAddress_geolocation_" + i).setAttribute("processed","1");
			}
		}
	}
	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Visitor_pass_requestServlet");	
}

function init(row)
{


	for(i = 1; i < child_table_extra_id; i ++)
	{
			initGeoLocation('visitorAddress_geoSelectField_', i, "Visitor_pass_requestServlet");
	}
	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	
}

var row = 0;
	
window.onload =function ()
{
	init(row);
	CKEDITOR.replaceAll();
}

var child_table_extra_id = <%=childTableStartingID%>;

	$("#add-more-VisitorDetails").click(
        function(e) 
		{
            e.preventDefault();
            var t = $("#template-VisitorDetails");

            $("#field-VisitorDetails").append(t.html());
			SetCheckBoxValues("field-VisitorDetails");
			
			var tr = $("#field-VisitorDetails").find("tr:last-child");
			
			tr.attr("id","VisitorDetails_" + child_table_extra_id);
			
			tr.find("[tag='pb_html']").each(function( index ) 
			{
				var prev_id = $( this ).attr('id');
				$( this ).attr('id', prev_id + child_table_extra_id);
				console.log( index + ": " + $( this ).attr('id') );
			});
			
			$("#visitorAddress_geoSelectField_" + child_table_extra_id).attr("row",child_table_extra_id);
			initGeoLocation('visitorAddress_geoSelectField_', child_table_extra_id, "Visitor_pass_requestServlet");
			dateTimeInit("<%=Language%>");
			child_table_extra_id ++;

        });

    
      $("#remove-VisitorDetails").click(function(e){    	
	    var tablename = 'field-VisitorDetails';
	    var i = 0;
		console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
		var element = document.getElementById(tablename);

		var j = 0;
		for(i = document.getElementById(tablename).childNodes.length - 1; i >= 0 ; i --)
		{
			var tr = document.getElementById(tablename).childNodes[i];
			if(tr.nodeType === Node.ELEMENT_NODE)
			{
				console.log("tr.childNodes.length= " + tr.childNodes.length);
				var checkbox = tr.querySelector('input[type="checkbox"]');				
				if(checkbox.checked == true)
				{
					tr.remove();
				}
				j ++;
			}
			
		}    	
    });
	$("#add-more-VisitorAllowedBelongings").click(
        function(e) 
		{
            e.preventDefault();
            var t = $("#template-VisitorAllowedBelongings");

            $("#field-VisitorAllowedBelongings").append(t.html());
			SetCheckBoxValues("field-VisitorAllowedBelongings");
			
			var tr = $("#field-VisitorAllowedBelongings").find("tr:last-child");
			
			tr.attr("id","VisitorAllowedBelongings_" + child_table_extra_id);
			
			tr.find("[tag='pb_html']").each(function( index ) 
			{
				var prev_id = $( this ).attr('id');
				$( this ).attr('id', prev_id + child_table_extra_id);
				console.log( index + ": " + $( this ).attr('id') );
			});
			
			dateTimeInit("<%=Language%>");
			child_table_extra_id ++;

        });

    
      $("#remove-VisitorAllowedBelongings").click(function(e){    	
	    var tablename = 'field-VisitorAllowedBelongings';
	    var i = 0;
		console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
		var element = document.getElementById(tablename);

		var j = 0;
		for(i = document.getElementById(tablename).childNodes.length - 1; i >= 0 ; i --)
		{
			var tr = document.getElementById(tablename).childNodes[i];
			if(tr.nodeType === Node.ELEMENT_NODE)
			{
				console.log("tr.childNodes.length= " + tr.childNodes.length);
				var checkbox = tr.querySelector('input[type="checkbox"]');				
				if(checkbox.checked == true)
				{
					tr.remove();
				}
				j ++;
			}
			
		}    	
    });


</script>






