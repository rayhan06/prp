

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="visitor_pass_request.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>
<%@page import="geolocation.*" %>
<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>




<%
String requestedEmployeeNameEn = (String)request.getAttribute("requestedEmployeeNameEn");
String requestedEmployeeNameBn = (String)request.getAttribute("requestedEmployeeNameBn");
String referenceEmployeeNameEn = (String)request.getAttribute("referenceEmployeeNameEn");
String referenceEmployeeNameBn = (String)request.getAttribute("referenceEmployeeNameBn");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.VISITOR_PASS_REQUEST_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
Visitor_pass_requestDAO visitor_pass_requestDAO = new Visitor_pass_requestDAO("visitor_pass_request");
Visitor_pass_requestDTO visitor_pass_requestDTO = (Visitor_pass_requestDTO)visitor_pass_requestDAO.getDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<!-- <div class="modal-content viewmodal"> -->
<div class="menubottom">
            <div class="modal-header">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9 col-sm-12">
                            <h5 class="modal-title">Visitor Pass Request Details</h5>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-info" id = 'printer' onclick="printDiv('modalbody')" >Print</button>
                </div>


            </div>
            
            
            
            
            

            <div class="modal-body container">

			<div class="row div_border office-div">

                    <div class="col-md-12">
                    
                        <h3>Visitor Pass Request</h3>
						<table class="table table-bordered table-striped">
									

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_EMPLOYEERECORDSID, loginDTO)%></b></td>
								<td>
						
											<%
											if(Language.equals("English")) {
												value = requestedEmployeeNameEn + "";
											} else {
												value = requestedEmployeeNameBn + "";
											}
											
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITDATE, loginDTO)%></b></td>
								<td>
						
											<%
											value = visitor_pass_requestDTO.visitDate + "";
											%>
											<%
											String formatted_visitDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_visitDate%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITTIME, loginDTO)%></b></td>
								<td>
						
											<%
											value = visitor_pass_requestDTO.visitTime + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_REQUESTDATE, loginDTO)%></b></td>
								<td>
						
											<%
											value = visitor_pass_requestDTO.requestDate + "";
											%>
											<%
											String formatted_requestDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_requestDate%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_PURPOSE, loginDTO)%></b></td>
								<td>
						
											<%
											value = visitor_pass_requestDTO.purpose + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_ISKNOWN, loginDTO)%></b></td>
								<td>
						
											<%
											value = visitor_pass_requestDTO.isKnown + "";
											if(value.equals("true")) {
												value = "হ্যা";
											} else {
												value = "না";
											}
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_REFERENCEEMPLOYEERECORDSID, loginDTO)%></b></td>
								<td>
						
											<%
											if(Language.equals("English")) {
												value = referenceEmployeeNameEn + "";
											} else {
												value = referenceEmployeeNameBn + "";
											}
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_ISAPPROVED, loginDTO)%></b></td>
								<td>
						
											<%
											value = visitor_pass_requestDTO.isApproved + "";
											if(value.equals("true")) {
												value = "হ্যা";
											} else {
												value = "না";
											}
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			
			
			
			
			
			
			
		
						</table>
                    </div>
			






			</div>	

                <div class="row div_border attachement-div">
                    <div class="col-md-12">
                        <h5>Visitor Details</h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_VISITORNAME, loginDTO)%></th>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_VISTORFATHERNAME, loginDTO)%></th>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_IDENTIFICATIONCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_IDENTIFICATIONNUMBER, loginDTO)%></th>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_CONTACTNUMBER1, loginDTO)%></th>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_CONTACTNUMBER2, loginDTO)%></th>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_VISITORADDRESS, loginDTO)%></th>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_FILESDROPZONE, loginDTO)%></th>
							</tr>
							<%
                        	VisitorDetailsDAO visitorDetailsDAO = new VisitorDetailsDAO();
                         	List<VisitorDetailsDTO> visitorDetailsDTOs = visitorDetailsDAO.getVisitorDetailsDTOListByVisitorPassRequestID(visitor_pass_requestDTO.iD);
                         	
                         	for(VisitorDetailsDTO visitorDetailsDTO: visitorDetailsDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%
											value = visitorDetailsDTO.visitorName + "";
											%>
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = visitorDetailsDTO.vistorFatherName + "";
											%>
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = visitorDetailsDTO.identificationCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "identification", visitorDetailsDTO.identificationCat);
											%>																						
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = visitorDetailsDTO.identificationNumber + "";
											%>
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = visitorDetailsDTO.contactNumber1 + "";
											%>
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = visitorDetailsDTO.contactNumber2 + "";
											%>
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = visitorDetailsDTO.visitorAddress + "";
											%>
											<%=GeoLocationDAO2.parseEnText(value)%>
											<%
												{
																					String addressdetails = GeoLocationDAO2.parseDetails(value);
																					if(!addressdetails.equals(""))
																					{
											%>
													, <%=addressdetails%>
												<%
													}
																					}
												%>

				
			
										</td>
										<td>
											<%
												value = visitorDetailsDTO.filesDropzone + "";
											%>
											<%
												{
																					List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(visitorDetailsDTO.filesDropzone);
											%>
												<table>
												<tr>
												<%
													if(FilesDTOList != null)
																						{
																							for(int j = 0; j < FilesDTOList.size(); j ++)
																							{
																								FilesDTO filesDTO = FilesDTOList.get(j);
																								byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
												%>
														<td>
														<%
															if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)
																										{
														%>
															<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px' />
															<%
																}
															%>
														<a href = 'Visitor_pass_requestServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>
														</td>
													<%
														}
																							}
													%>
												</tr>
												</table>
											<%
												}
											%>
				
			
										</td>
                         			</tr>
                         		<%
                         			}
                         		%>
						</table>
                    </div>                    
                </div>
                <div class="row div_border attachement-div">
                    <div class="col-md-12">
                        <h5>Visitor Allowed Belongings</h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_ALLOWED_BELONGINGS_VISITORBELONGINGCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_ALLOWED_BELONGINGS_TOTALCOUNT, loginDTO)%></th>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_ALLOWED_BELONGINGS_REMARKS, loginDTO)%></th>
							</tr>
							<%
								VisitorAllowedBelongingsDAO visitorAllowedBelongingsDAO = new VisitorAllowedBelongingsDAO();
							                         	List<VisitorAllowedBelongingsDTO> visitorAllowedBelongingsDTOs = visitorAllowedBelongingsDAO.getVisitorAllowedBelongingsDTOListByVisitorPassRequestID(visitor_pass_requestDTO.iD);
							                         	
							                         	for(VisitorAllowedBelongingsDTO visitorAllowedBelongingsDTO: visitorAllowedBelongingsDTOs)
							                         	{
							%>
                         			<tr>
										<td>
											<%
												value = visitorAllowedBelongingsDTO.visitorBelongingCat + "";
											%>
											<%
												value = CatDAO.getName(Language, "visitor_belonging", visitorAllowedBelongingsDTO.visitorBelongingCat);
											%>																						
														
											<%=value%>
				
			
										</td>
										<td>
											<%
												value = visitorAllowedBelongingsDTO.totalCount + "";
											%>
														
											<%=value%>
				
			
										</td>
										<td>
											<%
												value = visitorAllowedBelongingsDTO.remarks + "";
											%>
														
											<%=value%>
				
			
										</td>
                         			</tr>
                         		<%
                         			}
                         		%>
						</table>
                    </div>                    
                </div>
               


        </div>
        
        
         <div class="modal-body container" id="modalbody" style="display:none">
         <style>
        @page {
            size: 21cm 29.7cm;
            margin: 0mm;
        }
       </style>
			<div class="parlamentprint">
			<h3></>বাংলাদেশ জাতীয় সংসদ সচিবালয়  </h3>
			<h4 ><u>শের-ই-বাংলা নগর, ঢাকা-১২০৭ । </u></h4>
			<h5>www.parliament.gov.bd</h5>
			<h4 class="visitor-pass-form-head"> <u>দর্শনার্থী পাসের অধিযাচন ফরম</u> </h4>
			  <%
			  	value = visitor_pass_requestDTO.visitDate + "";
			  %>
											<%
												formatted_visitDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
																				formatted_visitDate = visitorDetailsDAO.getBanglaDateFromEnglish(formatted_visitDate);
											%>
							
              <h4 class="request-text" >   নিন্মোক্ত ব্যাক্তি/ব্যাক্তিবর্গকে অদ্য <%=formatted_visitDate%> ইং তারিখ দর্শনার্থী পাস/ভিজিটর পাস ইস্যুর জন্য অনুরোধ করছি ঃ </h4>
			</div>
                                          
                <div class="row div_border attachement-div">
                    <div class="col-md-12">
						<table class="table table-bordered table-striped">
							<tr>
							<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_SERIAL_NUMBER, loginDTO)%></th>
							<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_FOR_WHOM_PASS_ISSUE, loginDTO)%></th>
							<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_FULL_ADDRESS, loginDTO)%></th>
							</tr>
							<%
								visitorDetailsDAO = new VisitorDetailsDAO();
							                         	 visitorDetailsDTOs = visitorDetailsDAO.getVisitorDetailsDTOListByVisitorPassRequestID(visitor_pass_requestDTO.iD);
							                         	int index =1 ;
							                         	for(VisitorDetailsDTO visitorDetailsDTO: visitorDetailsDTOs)
							                         	{
							%>
                         			<tr>
										<td>
											<%
												value =  visitorDetailsDAO.getDigitBanglaFromEnglish(index++);
											%>
											<%=value%>
			
										</td>
										<td>
										
										<%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_VISITORNAME, loginDTO)%>: <%
																					value=visitorDetailsDTO.visitorName;
																				%><%=value%> <br>
										<%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_VISTORFATHERNAME, loginDTO)%>: <%
											value=visitorDetailsDTO.vistorFatherName;
										%><%=value%><br>
										<%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_IDENTIFICATIONCAT, loginDTO)%>: <%
											value = CatDAO.getName(Language, "identification", visitorDetailsDTO.identificationCat);
										%><%=value%><br>
										<%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_IDENTIFICATIONNUMBER, loginDTO)%>: <%
											value=visitorDetailsDTO.identificationNumber;
										%><%=value%><br>
										<%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_DETAILS_CONTACTNUMBER1, loginDTO)%>: <%
											value=visitorDetailsDTO.contactNumber1;
										%><%=value%>
										</td>
										
										<td>
											<%
												value = visitorDetailsDTO.visitorAddress + "";
											%>
											<%=GeoLocationDAO2.parseEnText(value)%>
											<%
											{
												String addressdetails = GeoLocationDAO2.parseDetails(value);
												if(!addressdetails.equals(""))
												{
												%>
													, <%=addressdetails%>
												<%
												}
											}
											%>
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
                <div class="row div_border attachement-div">
                    <div class="col-md-12">
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_ALLOWED_BELONGINGS_VISITORBELONGINGCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_ALLOWED_BELONGINGS_TOTALCOUNT, loginDTO)%></th>
								<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_ADD_VISITOR_ALLOWED_BELONGINGS_REMARKS, loginDTO)%></th>
							</tr>
							<%
                        	 visitorAllowedBelongingsDAO = new VisitorAllowedBelongingsDAO();
                         	 visitorAllowedBelongingsDTOs = visitorAllowedBelongingsDAO.getVisitorAllowedBelongingsDTOListByVisitorPassRequestID(visitor_pass_requestDTO.iD);
                         	
                         	for(VisitorAllowedBelongingsDTO visitorAllowedBelongingsDTO: visitorAllowedBelongingsDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%
											value = visitorAllowedBelongingsDTO.visitorBelongingCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "visitor_belonging", visitorAllowedBelongingsDTO.visitorBelongingCat);
											%>																						
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = visitorAllowedBelongingsDTO.totalCount + "";
											%>
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = visitorAllowedBelongingsDTO.remarks + "";
											%>
														
											<%=value%>
				
			
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
               <div class="signaturearea" >
		               
		               	<div class="leftcontent">
		               	<h4>বর্ণিত ব্যক্তি/ব্যাক্তিবৃন্দ আমার পরিচিত । </h4>
		               	</div>
											
							<div class="rightcontent">
							               <%
											if(referenceEmployeeNameBn==null || referenceEmployeeNameBn.isEmpty() || referenceEmployeeNameBn.equals("Not Found")) {
												value = requestedEmployeeNameBn + "";
											} else {
												value = referenceEmployeeNameBn + "";
											}
											%>
							    <p>চাহিদাকারির স্বাক্ষর ............................  </p>
							    <p >চাহিদাকারির নাম :<%=value%> </p>
							    <p>নামসহ সীল ............................ </p>
							    <p>দপ্তর ............................ </p>	
							</div>							
			    	
		               </div>

        </div>
        
<script type="text/javascript">
function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;

    window.print();

    document.body.innerHTML = originalContents;
}
</script>