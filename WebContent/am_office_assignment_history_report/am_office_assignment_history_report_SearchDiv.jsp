<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%@ page import="am_parliament_building_level.Am_parliament_building_levelRepository" %>
<%@ page import="am_minister_hostel_block.Am_minister_hostel_blockDTO" %>
<%@ page import="am_minister_hostel_block.Am_minister_hostel_blockRepository" %>
<%@ page import="java.util.List" %>
<%@ page import="util.HttpRequestUtils" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.AM_OFFICE_ASSIGNMENT_HISTORY_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row mx-2 mx-md-0">
    <div class="col-12">
		<div id="calendardiv" >
			<div id="visitDate" class="search-criteria-div">
				<div class="form-group row">
					<label class="col-md-3 col-form-label text-md-right">
						<%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT_WHERE_STARTTIME, loginDTO)%>
					</label>
					<div class="col-md-9">
						<jsp:include page="/date/date.jsp">
							<jsp:param name="DATE_ID" value="startDate_js"></jsp:param>
							<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
						</jsp:include>
						<input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
							   data-label="Document Date" id='startDate' name='startDate' value=""
							   tag='pb_html'
						/>
					</div>
				</div>
			</div>
			<div id="visitDate_3" class="search-criteria-div">
				<div class="form-group row">
					<label class="col-md-3 col-form-label text-md-right">
						<%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT_WHERE_ENDTIME, loginDTO)%>
					</label>
					<div class="col-md-9">
						<jsp:include page="/date/date.jsp">
							<jsp:param name="DATE_ID" value="endDate_js"></jsp:param>
							<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
						</jsp:include>
						<input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
							   data-label="Document Date" id='endDate' name='endDate' value=""
							   tag='pb_html'
						/>
					</div>
				</div>
			</div>

			<div id="visitDate_4" class="search-criteria-div">
				<div class="form-group row">
					<label class="col-md-3 col-form-label text-md-right">
						<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_HISTORY_REPORT_WHERE_WITHDRAWALDATE, loginDTO)%>
					</label>
					<div class="col-md-9">
						<jsp:include page="/date/date.jsp">
							<jsp:param name="DATE_ID" value="withdrawalStartDate_js"></jsp:param>
							<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
						</jsp:include>
						<input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
							   data-label="Document Date" id='withdrawalStartDate' name='withdrawalStartDate' value=""
							   tag='pb_html'
						/>
					</div>
				</div>
			</div>

			<div id="visitDate_5" class="search-criteria-div">
				<div class="form-group row">
					<label class="col-md-3 col-form-label text-md-right">
						<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_HISTORY_REPORT_WHERE_WITHDRAWALDATE_3, loginDTO)%>
					</label>
					<div class="col-md-9">
						<jsp:include page="/date/date.jsp">
							<jsp:param name="DATE_ID" value="withdrawalStartDate_js"></jsp:param>
							<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
						</jsp:include>
						<input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
							   data-label="Document Date" id='withdrawalEndDate' name='withdrawalEndDate' value=""
							   tag='pb_html'
						/>
					</div>
				</div>
			</div>

		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_HISTORY_REPORT_WHERE_BUILDINGTYPECAT, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control' onchange='buildingTypeChanged(this)'  name='buildingTypeCat' id = 'buildingTypeCat' >
						<%		
						Options = CatDAO.getOptions(Language, "building_type", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>

		<div  class="search-criteria-div" style="display: none;" id="ParliamentBuildingContent">

				<div class="form-group row">
					<label class="col-sm-3 control-label text-right">
						<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_LEVEL, loginDTO)%>
					</label>
					<div class="col-sm-9">
						<select  class='form-control' onchange="parliamentBuildingLevelTypeChanged(this)"  name='level' id = 'parliamentBuildingLevel'   tag='pb_html'>
							<%
								String levelOptions = Am_parliament_building_levelRepository.getInstance().getOptions(Language, -1);
							%>
							<%=levelOptions%>
						</select>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-sm-3 control-label text-right">
						<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_BLOCK, loginDTO)%>
					</label>
					<div class="col-sm-9">
						<select id="parliamentBuildingBlock"
								class='form-control rounded shadow-sm w-100'   name="block"
								onchange="parliamentBuildingBlockChanged(this)"
						>
						</select>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-sm-3 control-label text-right">
						<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_ROOMNO, loginDTO)%>
					</label>
					<div class="col-sm-9">
						<select id="parliamentBuildingRoomNo"
								class='form-control rounded shadow-sm w-100'   name="roomNo"
						>
						</select>
					</div>
				</div>

		</div>

		<div  class="search-criteria-div" style="display: none;" id="MinisterHostelContent">

			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_BLOCK, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select  class='form-control' onchange="mpHostelBlockTypeChanged(this)"  name='block' id = 'mpHostelBlock'   tag='pb_html'>
						<%
							List<Am_minister_hostel_blockDTO> am_minister_hostel_blockDTOS = Am_minister_hostel_blockRepository.getInstance().getAm_minister_hostel_blockList();
							String blockOptions = Utils.buildSelectOption(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language.equalsIgnoreCase("english"));

							StringBuilder option = new StringBuilder();
							for(Am_minister_hostel_blockDTO am_minister_hostel_blockDTO:am_minister_hostel_blockDTOS){
								option.append("<option value = '").append(am_minister_hostel_blockDTO.iD).append("'>");
								option.append(Utils.getDigits(am_minister_hostel_blockDTO.blockNo,HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language)).append("</option>");
							}
							blockOptions+=option.toString();
						%>
						<%=blockOptions%>

					</select>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_SIDE, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select id="mpHostelSide"
							class='form-control rounded shadow-sm w-100'   name="side"
							onchange="mpHostelSideChanged(this)"
					>
					</select>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_UNIT, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select id="mpHostelUnit"
							class='form-control rounded shadow-sm w-100'   name="unit"
							onchange="mpHostelUnitChanged(this)"
					>
					</select>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_LEVEL, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select id="mpHostelLevel"
							class='form-control rounded shadow-sm w-100'   name="level"
					>
					</select>
				</div>
			</div>

		</div>


		<div  class="search-criteria-div" style = "display: none;">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_HISTORY_REPORT_WHERE_ISDELETED, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<input class='form-control'  name='isDeleted' id = 'isDeleted' value=""/>							
				</div>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">

const buildingTypeChanged = (selectedVal) => {
	switch(parseInt(selectedVal.value)) {
		case 1:
			$("#ParliamentBuildingContent").show();
			$("#MinisterHostelContent").hide();
			break;
		case 2:
			$("#ParliamentBuildingContent").hide();
			$("#MinisterHostelContent").show();
			break;
		default:
			$("#ParliamentBuildingContent").hide();
			$("#MinisterHostelContent").hide();
	}
}

const parliamentBuildingLevelTypeChanged = async (selectedVal) => {

	const levelId = selectedVal.value;
	clearSelects(0,1);
	if (levelId === '') return;

	const url = 'Am_parliament_building_blockServlet?actionType=getBlockByLevelId&levelId='
			+ levelId+'&defaultValue=-1';

	$.ajax({
		url: url,
		type: "GET",
		async: false,
		success: function (fetchedData) {
			const response = JSON.parse(fetchedData);
			if ( response && response.responseCode === 0) {
				$('#toast_message').css('background-color', '#ff6063');
				showToastSticky(response.msg, response.msg);
			}
			else if (response && response.responseCode === 200) {
				document.getElementById('parliamentBuildingBlock').innerHTML = response.msg;
			}

		},
		error: function (jqXHR, textStatus, errorThrown) {
			toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
					+ ", Message: " + errorThrown);
		}
	});
}

const parliamentBuildingBlockChanged = async (selectedVal) => {

	const block = selectedVal.value;
	clearSelects(0,2);
	if (block === '') return;

	const url = 'Am_parliament_building_roomServlet?actionType=getAllRoomByBlockId&blockId='
			+ block+'&defaultValue=-1';

	$.ajax({
		url: url,
		type: "GET",
		async: false,
		success: function (fetchedData) {
			const response = JSON.parse(fetchedData);
			document.getElementById('parliamentBuildingRoomNo').innerHTML = response.msg;
		},
		error: function (jqXHR, textStatus, errorThrown) {
			toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
					+ ", Message: " + errorThrown);
		}
	});
}

const mpHostelBlockTypeChanged = async (selectedVal) => {

	const blockType = selectedVal.value;
	clearSelects(1,1);
	if (blockType === '') return;

	const url = 'Am_minister_hostel_blockServlet?actionType=getSideOptionsByBlockId&blockId='
			+ blockType;
	ajaxGet(url, setD, setD )

}
const mpHostelSideChanged = async (selectedVal) => {

	const side = selectedVal.value;
	clearSelects(1,2);
	if (side === '') return;

	const url = 'Am_minister_hostel_unitServlet?actionType=getUnitOptionsBySideId&sideId='
			+ side;
	ajaxGet(url, setMpHostelUnit, setMpHostelUnit );

}
const mpHostelUnitChanged = async (selectedVal) => {

	const unit = selectedVal.value;
	clearSelects(1,3);
	if (unit === '') return;

	const url = 'Am_minister_hostel_levelServlet?actionType=getAllLevelOptionsByUnitId&unitId='
			+ unit;
	ajaxGet(url, setMpHostelLevel, setMpHostelLevel );


}

function setD(data) {
	document.getElementById('mpHostelSide').innerHTML = data;
}

function ajaxGet(url, onSuccess, onError) {
	$.ajax({
		type: "GET",
		url: getContextPath()+url,
		dataType: "json",
		success: onSuccess,
		error: onError,
		complete: function () {
			// $.unblockUI();
		}
	});
}

function setMpHostelUnit(data) {
	document.getElementById('mpHostelUnit').innerHTML = data;
}
function setMpHostelLevel(data) {
	document.getElementById('mpHostelLevel').innerHTML = data.responseText;
}

$(document).ready(() => {
	showFooter = false;
});

function init()
{
    dateTimeInit($("#Language").val());
}
function PreprocessBeforeSubmiting()
{
}

function clearSelects(arrayIndex,startIndex) {
	const selectIds = [['parliamentBuildingLevel', 'parliamentBuildingBlock', 'parliamentBuildingRoomNo'],['mpHostelBlock', 'mpHostelSide', 'mpHostelUnit','mpHostelLevel']];

	selectIds[arrayIndex].forEach((item) => {
		if(selectIds[arrayIndex].indexOf(item)>=startIndex ){
			$('#'+item).html('');
		}
	});

}
</script>