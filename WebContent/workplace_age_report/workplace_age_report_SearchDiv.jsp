<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="geolocation.GeoLocationRepository" %>
<%@page pageEncoding="UTF-8" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = LM.getText(LC.WORKPLACE_AGE_REPORT_EDIT_LANGUAGE, loginDTO);
    String context = request.getContextPath() + "/";
    boolean isLangEng = Language.equalsIgnoreCase("English");
%>

<jsp:include page="../employee_assign/officeMultiSelectTagsUtil.jsp"/>

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>

<div class="row mx-2">
    <div id="officeUnitId_div" class="search-criteria-div col-md-6">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_OFFICEUNITID, loginDTO)%>
            </label>
            <div class="col-md-9">
                <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                        id="office_units_id_modal_button"
                        onclick="officeModalButtonClicked();">
                    <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                </button>
                <input type="hidden" name='officeUnitIds' id='officeUnitIds_input' value="">
            </div>
        </div>
    </div>
    <div class="search-criteria-div col-md-6">
        <div class="form-group row">
            <label class="col-sm-4 col-xl-3 col-form-label text-md-right" for="onlySelectedOffice_checkbox">
                <%=isLangEng ? "Only Selected Office" : "শুধুমাত্র নির্বাচিত অফিস"%>
            </label>
            <div class="col-1" id='onlySelectedOffice'>
                <input type='checkbox' class='form-control-sm mt-1' name='onlySelectedOffice'
                       id='onlySelectedOffice_checkbox'
                       onchange="this.value = this.checked;" value='false'>
            </div>
            <div class="col-8"></div>
        </div>
    </div>
    <div class="search-criteria-div col-12" id="selected-offices" style="display: none;">
        <div class="form-group row">
            <div class="offset-1 col-10 tag-container" id="selected-offices-tag-container">
                <div class="tag template-tag">
                    <span class="tag-name"></span>
                    <i class="fas fa-times-circle tag-remove-btn"></i>
                </div>
            </div>
        </div>
    </div>
    <div id="officeUnitOrganogramId_div" class="search-criteria-div col-md-6">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITORGANOGRAMID, loginDTO)%>
            </label>
            <div class="col-md-9">
                <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                        id="organogram_id_modal_button"
                        onclick="organogramIdModalBtnClicked();">
                    <%=LM.getText(LC.LANGUAGE_SELECT, userDTO)%>
                </button>
                <div class="input-group" id="organogram_id_div" style="display: none">
                    <input type="hidden" name='officeUnitOrganogramId' id='organogram_id_input' value="">
                    <button type="button" class="btn btn-secondary form-control" disabled
                            id="organogram_id_text"></button>
                    <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                            <button type="button" class="btn btn-outline-danger"
                                    onclick="crsBtnClicked('organogram_id');"
                                    id='organogram_id_crs_btn' tag='pb_html'>
                                x
                            </button>
                        </span>
                </div>
            </div>
        </div>
    </div>
    <div id="employmentCat_div" class="search-criteria-div col-md-6">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.WORKPLACE_AGE_REPORT_WHERE_EMPLOYMENTCAT, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='employmentCat' id='employmentCat'>
                    <%=CatRepository.getInstance().buildOptions("employment", Language, null)%>
                </select>
            </div>
        </div>
    </div>
    <div id="name_eng_div" class="search-criteria-div col-md-6 employeeNameClass">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="nameEng">
                <%=isLangEng ? "Name(English)" : "নাম(ইংরেজি)"%>
            </label>
            <div class="col-md-9">
                <input class="englishOnly form-control" type="text" name="nameEng" id="nameEng" style="width: 100%"
                       placeholder="<%=isLangEng ? "Enter Employee Name in English" : "কর্মকর্তা/কর্মচারীর নাম ইংরেজিতে লিখুন"%>"
                       value="">
            </div>
        </div>
    </div>

    <div id="name_bng_div" class="search-criteria-div col-md-6 employeeNameClass">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="nameBng">
                <%=isLangEng ? "Name(Bangla)" : "নাম(বাংলা)"%>
            </label>
            <div class="col-md-9">
                <input class="noEnglish form-control" type="text" name="nameBng" id="nameBng" style="width: 100%"
                       placeholder="<%=isLangEng ? "Enter Employee Name in Bangla" : "কর্মকর্তা/কর্মচারীর নাম বাংলাতে লিখুন"%>"
                       value="">
            </div>
        </div>
    </div>
    <div id="age_div" class="search-criteria-div col-md-6">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.HR_REPORT_AGE_RANGE, loginDTO)%>
            </label>
            <div class="col-6 col-md-5">
                <input class='form-control' type="text" name='startAge' id='startAge'
                       placeholder=<%=LM.getText(LC.HR_REPORT_FROM, loginDTO)%>>
            </div>
            <div class="col-6 col-md-4">
                <input class='form-control' type="text" name='endAge' id='endAge'
                       placeholder=<%=LM.getText(LC.HR_REPORT_TO, loginDTO)%>>
            </div>
        </div>
    </div>
    <div id="permanent_district_div" class="search-criteria-div col-md-6">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='home_district' id='home_district' style="width: 100%">
                    <%=GeoLocationRepository.getInstance().buildOptionByLevel(Language, 2, null)%>
                </select>
            </div>
        </div>
    </div>
</div>

<%@include file="../common/hr_report_modal_util.jsp" %>
<script src="<%=context%>assets/scripts/input_validation.js" type="text/javascript"></script>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(() => {
        showFooter = false;
    });

    function init() {
        select2SingleSelector('#employmentCat', '<%=Language%>');
        select2SingleSelector('#home_district', '<%=Language%>');
        dateTimeInit($("#Language").val());
        document.getElementById('startAge').onkeydown = keyDownEvent;
        document.getElementById('endAge').onkeydown = keyDownEvent;
    }

    function PreprocessBeforeSubmiting() {
    }

    function AgeConvertAndAjaxCall(ageForm) {
        console.log(document.getElementById(ageForm + 'Form').value);
        document.getElementById(ageForm).value = Date.now() - document.getElementById(ageForm + 'Form').value * 365 * 24 * 60 * 60 * 1000;
        ajaxSubmit();
    }

    function keyDownEvent(e) {
        let isvalid = inputValidationForIntValue(e, $(this), 200);
        return true == isvalid;
    }
</script>