<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="deliver_or_return_medicine.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="sessionmanager.SessionManager.*" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="borrow_medicine.*" %>
<%@page import="drug_information.*" %>
<%@page import="medical_equipment_name.*" %>
<%@page import="other_office.*"%>

<%
    Deliver_or_return_medicineDTO deliver_or_return_medicineDTO = new Deliver_or_return_medicineDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        deliver_or_return_medicineDTO = Deliver_or_return_medicineDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = deliver_or_return_medicineDTO;
    String tableName = "deliver_or_return_medicine";
    
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = "";
    String servletName = "Deliver_or_return_medicineServlet";
    int medicalItemType = Integer.parseInt(request.getParameter("medicalItemType"));
    deliver_or_return_medicineDTO.medicalItemType = medicalItemType;
    if (deliver_or_return_medicineDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_DRUG) {
        formTitle = LM.getText(LC.HM_DELIVER, loginDTO) + "/" + LM.getText(LC.HM_RETURN, loginDTO) + " " + LM.getText(LC.HM_DRUGS, loginDTO);
    } else if (deliver_or_return_medicineDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_EQUIPMENT) {
        formTitle = LM.getText(LC.HM_RETURN, loginDTO) + " " + LM.getText(LC.HM_EQUIPMENT, loginDTO);
    }
    String userName = request.getParameter("userName");
    if(userName == null || userName.equalsIgnoreCase(""))
    {
    	userName = userDTO.userName;
    }
    UserDAO userDAO = new UserDAO();
    UserDTO employeeUserDTO  = UserRepository.getUserDTOByUserName(userName);
    if(employeeUserDTO == null)
    {
    	employeeUserDTO  = userDAO.getUserDTOByUsernameFromOnlyUsers(userName);
    }
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <jsp:include page="../employee_assign/employeeSearchModal.jsp">
            <jsp:param name="isHierarchyNeeded" value="false"/>
        </jsp:include>
        <form class="form-horizontal"
              action="Deliver_or_return_medicineServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>

                                        <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                               value='<%=deliver_or_return_medicineDTO.iD%>' tag='pb_html'/>


                                        <input type="hidden" name='doctorId' value="<%=employeeUserDTO.organogramID%>"/>
                                        <input type="hidden" name='doctorUserName' value="<%=employeeUserDTO.userName%>"/>
                                        <input type="hidden" name='medicalItemType' value="<%=medicalItemType%>"/>


                                        <div class="form-group row" style = "display:none">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.HM_LOT, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='stockLotNumber'
                                                       id='stockLotNumber_text_<%=i%>'
                                                       value='<%=deliver_or_return_medicineDTO.stockLotNumber%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row" >
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.HM_NAME, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='borrower'
                                                       id='borrower' readonly
                                                       value='<%=WorkflowController.getNameFromUserName(userName, Language)%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                        <%
                                        if (!deliver_or_return_medicineDTO.isDelivery)
                                        {
                                        	
                                        	%>
                                        	
                                        <div class="form-group row">
											<label class="col-md-4 col-form-label text-md-right"><%=Language.equalsIgnoreCase("english") ? "Returned By" : "ফেরতদাতা"%>
											</label>
											<div class="col-md-8">
												<button type="button"
													class="btn text-white shadow form-control btn-border-radius"
													style="background-color: #4a87e2"
													onclick="addEmployeeWithRow('employeeToSet_button_1')"
													id="employeeToSet_button_1"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
												</button>
												<table class="table table-bordered table-striped">
													<tbody id="employeeToSet_table_1"></tbody>
												</table>
												<input type='hidden' class='form-control'
													name='returnedByUserName' id='returnedByUserName' value=''
													tag='pb_html' />
											</div>
										</div>
						
										<div class="form-group row">
											<label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.NURSE_ACTION_ADD_NAME, loginDTO)%></label>
											<div class="col-md-8">
												<input type='text' class='form-control' name='name' id='name'
													value='' tag='pb_html' />
											</div>
										</div>
										<div class="form-group row">
											<label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.NURSE_ACTION_ADD_PHONE, loginDTO)%></label>
											<div class="col-md-8">
												<div class="input-group mb-2">
													<div class="input-group-prepend">
														<div class="input-group-text"><%=Language.equalsIgnoreCase("english") ? "+88" : "+৮৮"%></div>
													</div>
													<input type='text' class='form-control' required name='phone'
														id='phone' value='' tag='pb_html'>
												</div>
											</div>
										</div>
										<%
                                        }
                                        %>
                                        <%
                                            if (medicalItemType == SessionConstants.MEDICAL_ITEM_DRUG && userDTO.roleID ==SessionConstants.DOCTOR_ROLE) {
                                        %>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.DELIVER_OR_RETURN_MEDICINE_ADD_ISDELIVERY, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='checkbox' onchange="deliveryChecked()"
                                                       class='form-control-sm mt-1' name='isDelivery' id='isDelivery'
                                                       value='true'                                                                <%=(String.valueOf(deliver_or_return_medicineDTO.isDelivery).equals("true"))?("checked"):""%>
                                                       tag='pb_html'>
                                            </div>
                                        </div>
                                        <%
                                            }
                                        %>
                                        <div class="form-group row" id="patientDiv" style="display:none">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=LM.getText(LC.HM_PATIENT_ID, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <button type="button"
                                                        class="btn text-white shadow form-control btn-border-radius"
                                                        style="background-color: #4a87e2"
                                                        onclick="addEmployee()"
                                                        id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
                                                </button>
                                                <table class="table table-bordered table-striped">
                                                    <tbody id="employeeToSet"></tbody>
                                                </table>
                                                <input type="hidden" name='patientId' id='patientId' value=""/>
                                            </div>
                                        </div>
                                        
                                       <div class="form-group row" 
                                       <%=userName.equalsIgnoreCase(SessionConstants.OUTSIDER_PATIENT)?"":"style='display:none'"%>
                                       >
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=LM.getText(LC.HM_OFFICE, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='otherOfficeId'
                                                            id='otherOfficeId' tag='pb_html'>
                                                        <%=Other_officeRepository.getInstance().buildOptions(Language, null)%>
                                                    </select>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.HM_DATE, loginDTO)%></label>
                                                     
                                                    <div class="col-md-8">
                                                  <jsp:include page="/date/date.jsp">
                                                     <jsp:param name="DATE_ID"
                                                                value="transactionDate_date_js"></jsp:param>
                                                     <jsp:param name="LANGUAGE"
                                                                value="<%=Language%>"></jsp:param>
                                                		 </jsp:include>

                                                  <input type='hidden'
                                                    	     class='form-control formRequired datepicker'
                                                         readonly="readonly" data-label="Document Date"
                                                         id='transactionDate_date_<%=i%>' name='transactionDate' value=<%
														String formatted_transactionDate = dateFormat.format(new Date(deliver_or_return_medicineDTO.transactionDate));
													%>
                                                          '<%=formatted_transactionDate%>'/>
                                              </div>
                                               
                                                    
                                         </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.DELIVER_OR_RETURN_MEDICINE_ADD_REMARKS, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='remarks'
                                                       id='remarks_text_<%=i%>'
                                                       value='<%=deliver_or_return_medicineDTO.remarks%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%
                                if (deliver_or_return_medicineDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_DRUG) {
                            %>
                            <%=LM.getText(LC.HM_DRUGS, loginDTO)%>
                            <%
                            } else if (deliver_or_return_medicineDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_EQUIPMENT) {
                            %>
                            <%=LM.getText(LC.HM_EQUIPMENT, loginDTO)%>
                            <%
                                }
                            %>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped text-nowrap">
                                <thead>
                                <tr>
                                    <th><%=LM.getText(LC.DELIVER_OR_RETURN_MEDICINE_ADD_DELIVER_OR_RETURN_DETAILS_DRUGINFORMATIONTYPE, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.HM_REMAINING_STOCK, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.DELIVER_OR_RETURN_MEDICINE_ADD_DELIVER_OR_RETURN_DETAILS_QUANTITY, loginDTO)%>
                                    </th>

                                </tr>
                                </thead>
                                <tbody id="field-DeliverOrReturnDetails">


                                <%
                                    if (true) {
                                        PersonalStockDAO personalStockDAO = PersonalStockDAO.getInstance();
                                        List<PersonalStockDTO> personalStockDTOs = (List<PersonalStockDTO>) personalStockDAO.getDTOsByDrId(userName, medicalItemType);
                                        Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
                                        int index = -1;


                                        for (PersonalStockDTO personalStockDTO : personalStockDTOs) {
                                            index++;

                                            System.out.println("index index = " + index);

                                %>

                                <tr id="DeliverOrReturnDetails_<%=index + 1%>">


                                    <td>

                                        <input type="hidden" name="deliverOrReturnDetails.iD" value="-1">

                                        <%
                                            if (personalStockDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_DRUG) {
                                                Drug_informationDTO drug_informationDTO = Drug_informationRepository.getInstance().getDrug_informationDTOByID(personalStockDTO.drugInformationType);

                                                value = "<b>" + drug_informationDTO.nameEn + ":</b> " + drug_informationDTO.strength;
                                            } else if (personalStockDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_EQUIPMENT) {
                                                Medical_equipment_nameDTO medical_equipment_nameDTO = Medical_equipment_nameRepository.getInstance().getMedical_equipment_nameDTOByID(personalStockDTO.drugInformationType);
                                                value = medical_equipment_nameDTO.nameEn;
                                            }
                                        %>


                                        <%=value%>

                                        <input type="hidden" name='deliverOrReturnDetails.drugInformationType'
                                               id='drugInformationType_select_<%=childTableStartingID%>'
                                               value='<%=personalStockDTO.drugInformationType%>'>


                                    </td>
                                    <td><%=personalStockDTO.quantity%></td>

                                    <td>

                                        <input type='number' class='form-control' name='deliverOrReturnDetails.quantity'
                                               id='quantity_number_<%=childTableStartingID%>'
                                               value='0' min="0"
                                               max="<%=personalStockDTO.quantity%>" tag='pb_html'>
                                              
                                    </td>

                                </tr>
                                <%
                                            childTableStartingID++;
                                        }
                                    }
                                %>

                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
                <div class="form-actions text-right mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.DELIVER_OR_RETURN_MEDICINE_ADD_DELIVER_OR_RETURN_MEDICINE_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.DELIVER_OR_RETURN_MEDICINE_ADD_DELIVER_OR_RETURN_MEDICINE_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, action) {
        console.log("action = " + action);

        var transactionDate = getDateStringById('transactionDate_date_js', 'DD/MM/YYYY');
        $("#transactionDate_date_<%=i%>").val(transactionDate);
        submitAddForm2();
        return false;
    }

    function deliveryChecked() {
        if ($("#isDelivery").prop("checked")) {
            $("#patientDiv").css("display", "flex");
        } else {
            $("#patientDiv").css("display", "none");
        }


    }


    function init(row) {

    	setDateByTimestampAndId('transactionDate_date_js', '<%=deliver_or_return_medicineDTO.transactionDate%>');
    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;


    function employee_inputted(value) {
        console.log('changed value: ' + value);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
            	fillUserInfo(this.responseText)
            } else {
                //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
            }
        };

        xhttp.open("POST", "FamilyServlet?actionType=getEmployeeInfo&userName=" + value + "&language=<%=Language%>", true);
        xhttp.send();
    }


    function fillUserInfo(responseText)
    {
    	//$("#empdiv").css("display", "block");
        var userNameAndDetails = JSON.parse(responseText);
        console.log("name = " + userNameAndDetails.name);
        $("#name").val(userNameAndDetails.name);      
        $("#phone").val(phoneNumberRemove88ConvertLanguage(userNameAndDetails.phone, '<%=Language%>'));
       
    }

    function patient_inputted(userName, orgId) {
    	console.log("table = " + modal_button_dest_table + " username = " + userName);
    	if(modal_button_dest_table == "employeeToSet_table_1")
   		{
   			$("#returnedByUserName").val(userName);
   			employee_inputted(userName);
   		}
    	else
    	{
    		$("#patientId").val(orgId);
    	}
    }


</script>






