

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="deliver_or_return_medicine.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>
<%@page import="drug_information.*"%>
<%@page import="medical_equipment_name.*"%>



<%
String servletName = "Deliver_or_return_medicineServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Deliver_or_return_medicineDTO deliver_or_return_medicineDTO = Deliver_or_return_medicineDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = deliver_or_return_medicineDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>
<%
String formTitle = "";
if(deliver_or_return_medicineDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_DRUG)
{
	formTitle = LM.getText(LC.HM_DELIVER, loginDTO) + "/" + LM.getText(LC.HM_RETURN, loginDTO) + " " + LM.getText(LC.HM_DRUGS, loginDTO);
}
else if(deliver_or_return_medicineDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_EQUIPMENT)
{
	formTitle = LM.getText(LC.HM_RETURN, loginDTO) + " " + LM.getText(LC.HM_EQUIPMENT, loginDTO);
}
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-md-10 offset-md-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=formTitle%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.HM_FROM, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%=WorkflowController.getNameFromOrganogramId(deliver_or_return_medicineDTO.doctorId, Language)%>
				
			
                                    </div>
                                </div>
			
			
						
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.DELIVER_OR_RETURN_MEDICINE_ADD_ISDELIVERY, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = deliver_or_return_medicineDTO.isDelivery + "";
											%>
				
											<%=Utils.getYesNo(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<%
								if(deliver_or_return_medicineDTO.isDelivery)
								{
								%>
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.HM_TO, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%=WorkflowController.getNameFromUserName(deliver_or_return_medicineDTO.patientUserName, Language)%>
				
			
                                    </div>
                                </div>
                                <%
								}
                                %>
			
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.DELIVER_OR_RETURN_MEDICINE_ADD_REMARKS, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = deliver_or_return_medicineDTO.remarks + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>

		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5 class="table-title"><%=LM.getText(LC.DELIVER_OR_RETURN_MEDICINE_ADD_DELIVER_OR_RETURN_DETAILS, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.HM_NAME, loginDTO)%></th>
								<th><%=LM.getText(LC.DELIVER_OR_RETURN_MEDICINE_ADD_DELIVER_OR_RETURN_DETAILS_QUANTITY, loginDTO)%></th>
								<th><%=LM.getText(LC.DELIVER_OR_RETURN_MEDICINE_ADD_DELIVER_OR_RETURN_DETAILS_TRANSACTIONDATE, loginDTO)%></th>
							</tr>
							<%
                        	DeliverOrReturnDetailsDAO deliverOrReturnDetailsDAO = DeliverOrReturnDetailsDAO.getInstance();
                         	List<DeliverOrReturnDetailsDTO> deliverOrReturnDetailsDTOs = (List<DeliverOrReturnDetailsDTO>)deliverOrReturnDetailsDAO.getDTOsByParent("deliver_or_return_medicine_id", deliver_or_return_medicineDTO.iD);
                         	Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
                         	for(DeliverOrReturnDetailsDTO deliverOrReturnDetailsDTO: deliverOrReturnDetailsDTOs)
                         	{
                         		%>
                         			<tr>

										<td>
											<%
											if(deliverOrReturnDetailsDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_DRUG)
											{
												Drug_informationDTO drug_informationDTO = drug_informationDAO.getDrugInfoDetailsById(deliverOrReturnDetailsDTO.drugInformationType);
	
												value = drug_informationDTO.drugTextWithoutGName;
											}
											else if(deliverOrReturnDetailsDTO.medicalItemType == SessionConstants.MEDICAL_ITEM_EQUIPMENT)
											{
												Medical_equipment_nameDTO medical_equipment_nameDTO = Medical_equipment_nameRepository.getInstance().getMedical_equipment_nameDTOByID(deliverOrReturnDetailsDTO.drugInformationType);
												value = medical_equipment_nameDTO.nameEn;
											}
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = deliverOrReturnDetailsDTO.quantity + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = deliverOrReturnDetailsDTO.transactionDate + "";
											%>
											<%
											String formatted_transactionDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_transactionDate, Language)%>
				
			
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
        </div>
    </div>
</div>