
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="deliver_or_return_medicine.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page pageEncoding="UTF-8" %>

<%
String navigator2 = "navDELIVER_OR_RETURN_MEDICINE";
String servletName = "Deliver_or_return_medicineServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
			<input type = "hidden" id = "titleText"	value = "<%=LM.getText(LC.HM_DELIVER, loginDTO) + "/" + LM.getText(LC.HM_RETURN, loginDTO)%>" />
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped text-nowrap">
						<thead>
							<tr>
								<th><%=LM.getText(LC.HM_FROM, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_TO, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_DATE, loginDTO)%></th>
								<th><%=LM.getText(LC.DELIVER_OR_RETURN_MEDICINE_ADD_ISDELIVERY, loginDTO)%></th>

								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
							
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Deliver_or_return_medicineDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Deliver_or_return_medicineDTO deliver_or_return_medicineDTO = (Deliver_or_return_medicineDTO) data.get(i);
																																
											
											%>
											<tr>
								
		
											<td>
											
											<%=WorkflowController.getNameFromUserName(deliver_or_return_medicineDTO.doctorUserName, Language)%>
											
											<%
											if(deliver_or_return_medicineDTO.returnedByUserName != null && !deliver_or_return_medicineDTO.returnedByUserName.equalsIgnoreCase(""))
											{
												%>
												<br>
												<%=Language.equalsIgnoreCase("english")?"Returned By":"ফেরতদাতা"%>
												:<br>
												<%=deliver_or_return_medicineDTO.returnedByName%>
												<br>
												<%=Utils.getDigits(deliver_or_return_medicineDTO.returnedByPhone, Language)%>
											<%
											}
											%>
											<br>
											<%
											if(deliver_or_return_medicineDTO.doctorId != -1)
											{
											%>
											
											
										    
										    <button type="button" type="button" class="btn btn-sm cancel-btn text-white border-0 shadow btn-border-radius pl-4 ml-2"
										            onclick="location.href='Deliver_or_return_medicineServlet?actionType=getAddPage&userName=<%=deliver_or_return_medicineDTO.doctorUserName%>&medicalItemType=<%=SessionConstants.MEDICAL_ITEM_DRUG%>'">
										        <i class="fa fa-minus"></i>
										    </button> 
										    <%
											}
										    %>
											</td>
											
											<td>
											<%
											if(deliver_or_return_medicineDTO.patientId != -1)
											{
											%>
										    <button type="button" type="button" class="btn btn-sm cancel-btn text-white border-0 shadow btn-border-radius pl-4 ml-2"
										            onclick="location.href='Deliver_or_return_medicineServlet?actionType=getAddPage&userName=<%=deliver_or_return_medicineDTO.patientUserName%>&medicalItemType=<%=SessionConstants.MEDICAL_ITEM_DRUG%>'">
										        <i class="fa fa-minus"></i>
										    </button> 
										    <%
											}
										    %>
											<%=WorkflowController.getNameFromUserName(deliver_or_return_medicineDTO.patientUserName, Language)%>
			
											</td>
		
											
		
											<td>
											
				
											<%=Utils.getDigits(simpleDateFormat.format(deliver_or_return_medicineDTO.lastModificationTime), Language)%>
				
			
											</td>
		
											<td>
											<%
											value = deliver_or_return_medicineDTO.isDelivery + "";
											%>
				
											<%=Utils.getYesNo(value, Language)%>
				
			
											</td>
		
		
											
		
		
		
		
		
		
		
	
											<%CommonDTO commonDTO = deliver_or_return_medicineDTO; %>
											<%@page import="sessionmanager.SessionConstants" %>
											<td>
											    <button
											            type="button"
											            class="btn-sm border-0 shadow bg-light btn-border-radius"
											            style="color: #ff6b6b;"
											            onclick="location.href='<%=servletName%>?actionType=view&ID=<%=commonDTO.iD%>'"
											    >
											        <i class="fa fa-eye"></i>
											    </button>
											</td>
											
											
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />
<script type="text/javascript">
$(document).ready(function(){	
	$("#subHeader").html($("#titleText").val());
});

</script>

			