<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LM" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="geolocation.GeoDivisionRepository" %>
<%@ page import="geolocation.GeoDistrictRepository" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    RecordNavigator rn = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    String action = request.getParameter("url");
    String context = "../../.." + request.getContextPath() + "/";
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
%>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>

    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_ELECTIONDETAILSTYPE, loginDTO)%>
                        </label>
                        <div class="col-md-8 col-xl-9">
                            <select
                                    style='width: 100%'
                                    class='form-control'
                                    name='election_details_type'
                                    id='election_details_type'
                                    onSelect='setSearchChanged()'
                            >
                                <%=Election_detailsRepository.getInstance().buildOptions(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_CONSTITUENCYNUMBER, loginDTO)%>
                        </label>
                        <div class="col-md-8 col-xl-9">
                            <input
                                    type="text"
                                    class="form-control"
                                    id="constituency_number"
                                    name="constituency_number"
                                    onChange='setSearchChanged()'
                            >
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_GEODIVISIONTYPE, loginDTO)%>
                        </label>
                        <div class="col-md-8 col-xl-9">
                            <select
                                    style='width: 100%'
                                    class='form-control'
                                    name='geo_division_type'
                                    id='geo_division_type'
                                    onSelect='setSearchChanged()'
                            >
                                <%=GeoDivisionRepository.getInstance().getOptions(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_GEODISTRICTTYPE, loginDTO)%>
                        </label>
                        <div class="col-md-8 col-xl-9">
                            <select
                                    style='width: 100%'
                                    class='form-control'
                                    name='geo_district_type'
                                    id='geo_district_type'
                                    onSelect='setSearchChanged()'
                            >
                                <%=GeoDistrictRepository.getInstance().getDistrictOptionUpdated(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_CONSTITUENCYNAMEEN, loginDTO)%>
                        </label>
                        <div class="col-md-8 col-xl-9">
                            <input
                                    type="text"
                                    class="form-control"
                                    id="constituency_name_en"
                                    placeholder=""
                                    name="constituency_name_en"
                                    onChange='setSearchChanged()'
                            />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_CONSTITUENCYNAMEBN, loginDTO)%>
                        </label>
                        <div class="col-md-8 col-xl-9">
                            <input
                                    type="text"
                                    class="form-control"
                                    id="constituency_name_bn"
                                    placeholder=""
                                    name="constituency_name_bn"
                                    onChange='setSearchChanged()'
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->

<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>

<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<script type="text/javascript">
    let language='<%=Language%>';
    $(() => {
        select2SingleSelector('#election_details_type', '<%=Language%>');
        select2SingleSelector('#geo_division_type', '<%=Language%>');
        select2SingleSelector('#geo_district_type', '<%=Language%>');
        document.getElementById('constituency_number').onkeydown = keyDownEvent;
    });

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=true&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        var params = 'AnyField=' + document.getElementById('anyfield').value;

        if ($("#election_details_type").val()) {
            params += '&election_details_type=' + $("#election_details_type").val();
        }
        if ($("#geo_division_type").val()) {
            params += '&geo_division_type=' + $("#geo_division_type").val();
        }
        if ($("#geo_district_type").val()) {
            params += '&geo_district_type=' + $("#geo_district_type").val();
        }
        if ($('#constituency_number').val()) {
            params += '&constituency_number=' + $('#constituency_number').val();
        }
        if ($('#constituency_name_en').val()) {
            params += '&constituency_name_en=' + $('#constituency_name_en').val();
        }
        if ($('#constituency_name_bn').val()) {
            params += '&constituency_name_bn=' + $('#constituency_name_bn').val();
        }

        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

    function keyDownEvent(e) {
        let isvalid = inputValidationForPositiveValue(e, $(this), 1000,1);
        return true == isvalid;
    }

</script>

