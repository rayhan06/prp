<%@page pageEncoding="UTF-8" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="geolocation.*" %>
<%@ page import="util.StringUtils" %>

<td>
    <%=Election_detailsRepository.getInstance().getText(election_constituencyDTO.electionDetailsType, Language)%>
</td>

<td>
    <%=GeoDivisionRepository.getInstance().getText(Language, election_constituencyDTO.geoDivisionsType)%>
</td>

<td>
    <%=GeoDistrictRepository.getInstance().getText(Language, election_constituencyDTO.geoDistrictsType)%>
</td>

<td>
<%--    <%=GeoThanaRepository.getInstance().getThanaByIds(election_constituencyDTO.thana, Language)%>--%>
    <%=election_constituencyDTO.thana%>
</td>
<td>
    <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(election_constituencyDTO.constituencyNumber))%>
</td>

<td>
    <%=election_constituencyDTO.constituencyNameEn%>
</td>

<td>
    <%=election_constituencyDTO.constituencyNameBn%>
</td>

<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Election_constituencyServlet?actionType=view&ID=<%=election_constituencyDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Election_constituencyServlet?actionType=getEditPage&ID=<%=election_constituencyDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=election_constituencyDTO.iD%>'/></span>
    </div>
</td>