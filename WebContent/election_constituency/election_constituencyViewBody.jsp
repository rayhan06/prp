<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="election_constituency.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="geolocation.*" %>
<%@ page import="util.StringUtils" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>


<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    Election_constituencyDTO election_constituencyDTO = (Election_constituencyDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_ELECTION_CONSTITUENCY_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
           <div class="row">
               <div class="col-md-8 offset-md-2">
                   <div class="onlyborder">
                       <div class="row px-4 px-md-0">
                           <div class="col-md-10 offset-md-1">
                               <div class="sub_title_top">
                                   <div class="sub_title">
                                       <h4 style="background-color: #FFFFFF">
                                           <%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_ELECTION_CONSTITUENCY_ADD_FORMNAME, loginDTO)%>
                                       </h4>
                                   </div>
                               </div>
                               <div class="table-responsive">
                                   <table class="table table-bordered table-striped text-nowrap">
                                       <tr>
                                           <td style="width:30%">
                                               <b><%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_ELECTIONDETAILSTYPE, loginDTO)%>
                                               </b></td>
                                           <td><%=Election_detailsRepository.getInstance().getText(election_constituencyDTO.electionDetailsType, Language)%>
                                           </td>
                                       </tr>

                                       <tr>
                                           <td style="width:30%">
                                               <b><%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_GEODIVISIONTYPE, loginDTO)%>
                                               </b></td>
                                           <td><%=GeoDivisionRepository.getInstance().getText(Language, election_constituencyDTO.geoDivisionsType)%>
                                           </td>
                                       </tr>

                                       <tr>
                                           <td style="width:30%">
                                               <b><%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_GEODISTRICTTYPE, loginDTO)%>
                                               </b></td>
                                           <td><%=GeoDistrictRepository.getInstance().getText(Language, election_constituencyDTO.geoDistrictsType)%>
                                           </td>
                                       </tr>

                                       <tr>
                                           <td style="width:30%">
                                               <b><%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_GEOTHANATYPE, loginDTO)%>
                                               </b></td>
                                           <td>
<%--                                               <%=GeoThanaRepository.getInstance().getThanaByIds(election_constituencyDTO.thana, Language)%>--%>
                                               <%=election_constituencyDTO.thana%>
                                           </td>
                                       </tr>

                                       <tr>
                                           <td style="width:30%">
                                               <b><%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_CONSTITUENCYNUMBER, loginDTO)%>
                                               </b></td>
                                           <td><%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(election_constituencyDTO.constituencyNumber))%>
                                           </td>
                                       </tr>

                                       <tr>
                                           <td style="width:30%">
                                               <b><%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_CONSTITUENCYNAMEEN, loginDTO)%>
                                               </b></td>
                                           <td><%=election_constituencyDTO.constituencyNameEn%>
                                           </td>
                                       </tr>

                                       <tr>
                                           <td style="width:30%">
                                               <b><%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_CONSTITUENCYNAMEBN, loginDTO)%>
                                               </b></td>
                                           <td><%=election_constituencyDTO.constituencyNameBn%>
                                           </td>
                                       </tr>

                                       <tr>
                                           <td style="width:30%">
                                               <b><%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_BOUNDARYDETAILS, loginDTO)%>
                                               </b></td>
                                           <td><%=StringUtils.convertBanglaIfLanguageIsBangla(Language, election_constituencyDTO.boundaryDetails)%>
                                           </td>
                                       </tr>

                                       <tr>
                                           <td style="width:30%">
                                               <b><%=Language.equalsIgnoreCase("English") ? "Status" : "অবস্থা"%>
                                               </b></td>
                                           <td><%=election_constituencyDTO.isVacant==1 ? (Language.equalsIgnoreCase("English") ? "vacant":"খালি") :
                                                   (Language.equalsIgnoreCase("English") ? "occupied":"অধিকৃত")%>
                                           </td>
                                       </tr>

                                       <tr>
                                           <td style="width:30%">
                                               <b><%=Language.equalsIgnoreCase("English") ? "Reserved" : "সংরক্ষিত"%>
                                               </b></td>
                                           <td><%=election_constituencyDTO.isReserved==1 ? (Language.equalsIgnoreCase("English") ? "yes":"হ্যাঁ") :
                                                   (Language.equalsIgnoreCase("English") ? "no":"না")%>
                                           </td>
                                       </tr>
                                   </table>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
        </div>
    </div>
</div>