<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="election_constituency.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="geolocation.GeoDivisionRepository" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>

<%
    Election_constituencyDTO election_constituencyDTO;
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName;
    if (request.getParameter("actionType").equalsIgnoreCase("edit")) {
        actionName = "edit";
        election_constituencyDTO = (Election_constituencyDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    } else {
        actionName = "add";
        election_constituencyDTO = new Election_constituencyDTO();
    }
    String formTitle = LM.getText(LC.ELECTION_CONSTITUENCY_ADD_ELECTION_CONSTITUENCY_ADD_FORMNAME, loginDTO);
    int i = 0;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    String context = request.getContextPath() + "/";
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i><%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal kt-form" id="bigform" action="Election_constituencyServlet?actionType=ajax_<%=actionName%>">
            <!-- FORM BODY SKULL -->
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_ELECTIONDETAILSTYPE, loginDTO)%><span
                                                class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='electionDetailsType_div_<%=i%>'>
                                            <select class='form-control' name='electionDetailsType'
                                                    id='electionDetailsType_select' tag='pb_html'>
                                                <%= Election_detailsRepository.getInstance().buildOptions(Language, election_constituencyDTO.electionDetailsType)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_GEODIVISIONTYPE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='geoDivisionType_div_<%=i%>'>
                                            <select class='form-control' name='geoDivisionType'
                                                    id='geoDivisionType_select' tag='pb_html'>
                                                <%=GeoDivisionRepository.getInstance().getOptions(Language, election_constituencyDTO.geoDivisionsType)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_GEODISTRICTTYPE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='geoDistrictType_div_<%=i%>'>
                                            <select class='form-control' name='geoDistrictType'
                                                    id='geoDistrictType_select' tag='pb_html'>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_GEOTHANATYPE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='geoThanaType_div_<%=i%>'>
<%--                                            <select multiple="multiple" class='form-control'--%>
<%--                                                    name='geoThanaType' id='geoThanaType_select'--%>
<%--                                                    tag='pb_html'>--%>
<%--                                                <option value='-1'><%=Language.equals("English") ? "Select" : "বাছাই করুন"%>--%>
<%--                                                </option>--%>
<%--                                            </select>--%>
                                            <input type='text' class='form-control' name='geoThanaType' id='geoThanaType_select'
                                                   tag='pb_html' placeholder="<%=Language.equalsIgnoreCase("English") ? "Enter thana" : "থানার নাম লিখুন"%>"
                                                   value='<%=election_constituencyDTO.thana != null? election_constituencyDTO.thana:""%>'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_CONSTITUENCYNUMBER, loginDTO)%><span
                                                class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='constituencyNumber_div_<%=i%>'>
                                            <input type='text' class='form-control'
                                                   name='constituencyNumber'
                                                   id='constituencyNumber_text'
                                                   tag='pb_html' required="required"
                                                   value='<%=election_constituencyDTO.constituencyNumber>0?election_constituencyDTO.constituencyNumber:""%>'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_CONSTITUENCYNAMEEN, loginDTO)%><span
                                                class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='constituencyNameEn_div_<%=i%>'>
                                            <input type='text' class='englishOnly form-control'
                                                   name='constituencyNameEn'
                                                   id='constituencyNameEn_text_'
                                                   required="required" tag='pb_html'
                                                   placeholder='<%=Language.equalsIgnoreCase("English")?"Write election constituency in English":"নির্বাচনী এলাকার নাম ইংরেজিতে লিখুন"%>'
                                                   value='<%=election_constituencyDTO.constituencyNameEn == null ? "":election_constituencyDTO.constituencyNameEn%>'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_CONSTITUENCYNAMEBN, loginDTO)%><span
                                                class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='constituencyNameBn_div_<%=i%>'>
                                            <input type='text' class='noEnglish form-control'
                                                   name='constituencyNameBn'
                                                   id='constituencyNameBn_text'
                                                   required="required" tag='pb_html'
                                                   placeholder='<%=Language.equalsIgnoreCase("English")?"Write election constituency in Bangla":"নির্বাচনী এলাকার নাম বাংলায় লিখুন"%>'
                                                   value='<%=election_constituencyDTO.constituencyNameBn == null ? "" : election_constituencyDTO.constituencyNameBn%>'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_BOUNDARYDETAILS, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='boundaryDetails_div_<%=i%>'>
                                              <textarea type='text' class='form-control'
                                                        name='boundaryDetails' id='boundaryDetails_text'
                                                        style="resize: none" rows="4" maxlength="1024"
                                                        tag='pb_html'><%=election_constituencyDTO.boundaryDetails != null ? election_constituencyDTO.boundaryDetails : ""%></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right" for="isReserved_checkbox_<%=i%>">
                                            <%=Language.equalsIgnoreCase("English") ? "Reserved Seat" : "সংরক্ষিত আসন"%>
                                        </label>
                                        <div class="col-1 " id='isReserved_div_<%=i%>'>
                                            <input type='checkbox' class='form-control-sm mt-md-1 rounded-lg'
                                                   name='isReserved'
                                                   id='isReserved_checkbox_<%=i%>' <%=election_constituencyDTO.isReserved==1?("checked"):""%>
                                                   tag='pb_html'><br>
                                        </div>
                                    </div>
                                    <!-- Hidden Fields -->
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=election_constituencyDTO.iD%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isVacant' id='isVacant'
                                           value='<%=election_constituencyDTO.isVacant%>' tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                            <%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_ELECTION_CONSTITUENCY_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                type="button" onclick="submitForm()">
                            <%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_ELECTION_CONSTITUENCY_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<script type="text/javascript">
    let selectedDistrict = <%=election_constituencyDTO.geoDistrictsType%>;
    <%--let selectedThana;--%>
    <%--let selectedThanas = '<%=election_constituencyDTO.thana%>';--%>

    const language = "<%=Language%>";

    $(document).ready(function () {
        select2SingleSelector("#electionDetailsType_select", "<%=Language%>");
        select2SingleSelector("#geoDivisionType_select", "<%=Language%>");
        select2SingleSelector("#geoDistrictType_select", "<%=Language%>");
        <%--select2MultiSelector("#geoThanaType_select", "<%=Language%>");--%>
        dateTimeInit(language);
        basicInit();
        if ($('#geoDivisionType_select').val()) {
            fetchDistrict($('#geoDivisionType_select').val(), selectedDistrict);
        }
        $('#geoDivisionType_select').change(function () {
            fetchDistrict($(this).val(), null);
        });
        // if ($('#geoDistrictType_select').val()) {
        //     fetchThana($('#geoDistrictType_select').val(), selectedThanas);
        // }
        // $('#geoDistrictType_select').change(function () {
        //     fetchThana($(this).val(), null);
        // });
        document.getElementById('constituencyNumber_text').onkeydown = keyDownEvent;
    });

    // function fetchThana(districtId, selectedThanas) {
    //     let url = "option_Servlet?actionType=ajax_type&type=thanas&district=" + districtId;
    //     if (selectedThanas) {
    //         url += "&selectedValues=" + selectedThanas;
    //     }
    //     console.log("url : " + url);
    //     $('#geoThanaType_select').html("");
    //     $.ajax({
    //         url: url,
    //         type: "GET",
    //         async: false,
    //         success: function (fetchedData) {
    //             $('#geoThanaType_select').html(fetchedData);
    //         },
    //         error: function (error) {
    //             console.log(error);
    //         }
    //     });
    // }

    function fetchDistrict(divisionId, selectedDistrictValue) {
        let url = "option_Servlet?actionType=ajax_type&type=district&division=" + divisionId;
        if (selectedDistrictValue) {
            url += "&selectedValue=" + selectedDistrictValue;
        }
        $('#geoDistrictType_select').html("");
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                $('#geoDistrictType_select').html(fetchedData);
                // if (selectedDistrictValue) {
                //     fetchThana(selectedDistrictValue, null);
                // }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function PreprocessBeforeSubmiting() {
        preprocessCheckBoxBeforeSubmitting('isReserved', 0);
        // processMultipleSelectBoxBeforeSubmit("geoThanaType");
        return true;
    }

    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Election_constituencyServlet");
    }

    const form = $('#bigform');
    function submitForm(){
        if(PreprocessBeforeSubmiting() && form.valid()){
            submitAjaxForm();
        }
    }

    function buttonStateChange(value){
        $('#submit-btn').prop('disabled',value);
        $('#cancel-btn').prop('disabled',value);
    }

    function keyDownEvent(e) {
        let isvalid = inputValidationForPositiveValue(e, $(this), 1000,1);
        return true == isvalid;
    }
</script>