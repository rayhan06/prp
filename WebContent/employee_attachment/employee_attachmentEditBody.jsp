<%@page import="employee_attachment.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="util.UtilCharacter" %>

<%@include file="../pb/addInitializer2.jsp" %>
<%
    Employee_attachmentDTO employee_attachmentDTO;
    if (actionName.equals("ajax_add")) {
        employee_attachmentDTO = new Employee_attachmentDTO();
    } else {
        employee_attachmentDTO = (Employee_attachmentDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    }
    String formTitle = LM.getText(LC.EMPLOYEE_ATTACHMENT_ADD_EMPLOYEE_ATTACHMENT_ADD_FORMNAME, loginDTO);
    String today = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigForm">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"
                                               for="employeeRecordId_text_<%=i%>">
                                            <%=LM.getText(LC.HM_SELECT_EMPLOYEE, loginDTO)%>
                                            <span>*</span></label>
                                        <div class="col-md-8">
                                            <button type="button"
                                                    class="btn btn-primary btn-block shadow btn-border-radius"
                                                    id="employee_record_id_modal_button"
                                                    onclick="employeeRecordIdModalBtnClicked();">
                                                <%=LM.getText(LC.LANGUAGE_SELECT, userDTO)%>
                                            </button>
                                            <div class="input-group" id="employee_record_id_div" style="display: none">
                                                <input type="hidden" name='employeeRecordId'
                                                       id='employee_record_id_input' value="">
                                                <label class="form-control" id="employee_record_id_text"
                                                       style="height: 40px;width: 95%;text-align: center;border-radius: 0"></label>
                                                <span class="input-group-btn" style="width: 5%;height: 45px">
                                                    <button type="button" class="btn btn-outline-danger"
                                                            style="height: 40px;border-radius: 0"
                                                            onclick="crsBtnClicked('employee_record_id');"
                                                            id='employee_record_id_crs_btn'>
                                                        X
                                                    </button>
                                                </span>
                                            </div>
                                            <span id="employee_error_id"
                                                  style="display: none;color: #fd397a;text-align: center;margin-left: 12px;margin-top: 1%;font-size: smaller">
                                                <%=isLanguageEnglish ? "Please select employee" : "কর্মকর্তা বাছাই করুন"%>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"
                                               for="officeUnitId_text_<%=i%>">
                                            <%=LM.getText(LC.HM_OFFICE, loginDTO)%> <span>*</span>
                                        </label>
                                        <div class="col-md-8">
                                            <button type="button"
                                                    class="btn btn-block shadow btn-primary text-white btn-border-radius"
                                                    id="office_units_id_modal_button"
                                                    onclick="officeModalButtonClicked();">
                                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                                            </button>
                                            <div class="input-group" id="office_units_id_div" style="display: none">
                                                <input type="hidden" name='officeUnitId' id='office_units_id_input'
                                                       value="">
                                                <button type="button"
                                                        class="btn btn-secondary btn-block shadow-sm btn-border-radius form-control"
                                                        id="office_units_id_text"
                                                        onclick="officeModalEditButtonClicked()">
                                                </button>
                                                <span class="input-group-btn"
                                                      style="width: 5%;padding-right: 5px;height: 40px" tag='pb_html'>
                                                    <button type="button" class="btn btn-outline-danger"
                                                            style="height: 40px;border-radius: 0"
                                                            onclick="crsBtnClicked('office_units_id');"
                                                            id='office_units_id_crs_btn' tag='pb_html'>
                                                        X
                                                    </button>
                                                </span>
                                            </div>
                                            <span id="office_error_id"
                                                  style="display: none;color: #fd397a;text-align: center;margin-left: 12px;margin-top: 1%;font-size: smaller">
                                                <%=isLanguageEnglish ? "Please select office" : "দপ্তর বাছাই করুন"%>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMPLOYEE_ATTACHMENT_ADD_STARTDATE, loginDTO)%>
                                            <span>*</span>
                                        </label>
                                        <div class="col-md-8">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="startDate_js_0"/>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                            </jsp:include>
                                            <input type='hidden' name='startDate' id='startDate_date_<%=i%>' value=''
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <%
                                        if (!actionName.equals("ajax_add")) {
                                    %>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"
                                               for="status_select2_<%=i%>">
                                            <%=LM.getText(LC.GLOBAL_DELETE, loginDTO)%>
                                        </label>
                                        <div class="col-1">
                                            <input type='checkbox' class='form-control-sm mt-1' name='deleted'
                                                   id='deleted_checkbox_<%=i%>'
                                                   value='true' <%=employee_attachmentDTO.status == 2?"checked":""%>
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="endDate_div" style="display: none">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.EMPLOYEE_ATTACHMENT_ADD_ENDDATE, loginDTO)%>
                                            <span>*</span></label>
                                        <div class="col-md-8">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="endDate_js_0"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='endDate' id='endDate_date_<%=i%>' value=''
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <%
                                        }
                                    %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.EMPLOYEE_ATTACHMENT_ADD_EMPLOYEE_ATTACHMENT_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="button" onclick="submitEmployeeAttachmentForm()">
                                <%=LM.getText(LC.EMPLOYEE_ATTACHMENT_ADD_EMPLOYEE_ATTACHMENT_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">
    const isLangEng =
    <%=isLanguageEnglish%>
    const isAddPage = '<%=actionName%>' === "ajax_add";
    const employeeAttachmentForm = $('#bigForm');

    function submitEmployeeAttachmentForm() {
        buttonStateChange(true);
        if (isValid()) {
            let data = {
                employeeRecordId: $('#employee_record_id_input').val(),
                officeUnitId: $('#office_units_id_input').val(),
                startDate: $('#startDate_date_0').val()
            }
            if (!isAddPage) {
                const deletedCheckBox = $('#deleted_checkbox_0');
                data.deleted = deletedCheckBox.val();
                if (deletedCheckBox.val()) {
                    data.endDate = $('#endDate_date_0').val()
                }
            }

            let message = isLangEng ? "Do you want to submit?" : "আপনি কি সাবমিট করতে চান", title = "";
            let cancelBtnText = isLangEng ? "CANCEL" : "বাতিল";
            let submitBtnText = isLangEng ? "SUBMIT" : "সাবমিট"
            console.log(data);
            if (!isAddPage && data.deleted == 'true') {
                title = isLangEng ? "You won't be able to revert this!" : "সাবমিটের পর পরিবর্তনযোগ্য না!";
            }

            messageDialog(message, title, "success", true, submitBtnText, cancelBtnText, () => {
                $.ajax({
                    type: "POST",
                    url: "Employee_attachmentServlet?actionType=<%=actionName%>&isPermanentTable=true&iD=<%=employee_attachmentDTO.iD%>",
                    data: data,
                    dataType: 'JSON',
                    success: function (response) {
                        if (response.responseCode === 0) {
                            $('#toast_message').css('background-color', '#ff6063');
                            showToastSticky(response.msg, response.msg);
                            buttonStateChange(false);
                        } else if (response.responseCode === 200) {
                            window.location.replace(getContextPath() + response.msg);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                            + ", Message: " + errorThrown);
                        buttonStateChange(false);
                    }
                });
            }, () => {
                buttonStateChange(false);
            })
        } else {
            buttonStateChange(false);
        }
    }

    function isValid() {
        let inputValid = true;
        if (!$('#office_units_id_input').val()) {
            inputValid = false;
            $('#office_error_id').show();
        }
        if (!$('#employee_record_id_input').val()) {
            inputValid = false;
            $('#employee_error_id').show();
        }

        document.getElementById("startDate_date_0").value = (getDateStringById('startDate_js_0'));
        const validStartDate = dateValidator('startDate_js_0', true, {
            errorEn: '<%=LM.getText(LC.GLOBAL_ENTER_START_DATE, "English")%>',
            errorBn: '<%=LM.getText(LC.GLOBAL_ENTER_START_DATE, "Bangla")%>'
        });
        let validEndDate = true;
        if (!isAddPage) {
            preprocessCheckBoxBeforeSubmitting('deleted', 0);
            if (document.getElementById("deleted_checkbox_0").checked === true) {
                document.getElementById("endDate_date_0").value = (getDateStringById('endDate_js_0'));
            }
            if (document.getElementById("deleted_checkbox_0").checked === true) {
                validEndDate = dateValidator('endDate_js_0', true, {
                    errorEn: '<%=LM.getText(LC.GLOBAL_ENTER_END_DATE, "English")%>',
                    errorBn: '<%=LM.getText(LC.GLOBAL_ENTER_END_DATE, "Bangla")%>'
                });
            }
        }
        return inputValid && validStartDate && validEndDate;
    }

    function init() {
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
        $('#deleted_checkbox_0').change(function () {
            if (document.getElementById("deleted_checkbox_0").checked === true) {
                $('#endDate_div').show();
                setMinEndDate();
                setMaxDateById('endDate_js_0', '<%=today%>');
            } else {
                $('#endDate_div').hide();
            }
        });
        setMaxDateById('startDate_js_0', '<%=today%>');

        <%if(!actionName.equals("ajax_add")){%>
        setDateByTimestampAndId('startDate_js_0', <%=employee_attachmentDTO.startDate%>);
        <%if(employee_attachmentDTO.endDate!=SessionConstants.MIN_DATE){%>
        setDateByTimestampAndId('endDate_js_0', <%=employee_attachmentDTO.endDate%>);
        setMaxDateById('endDate_js_0', '<%=today%>');
        setMinEndDate();
        <%}%>
        $("#startDate_js_0").on('datepicker.change', () => setMinEndDate());

        const employeeSearchModel =
            JSON.parse('<%=EmployeeSearchModalUtil.getModelWithOnlyEmployeeInfo(employee_attachmentDTO.employeeRecordsId).getJSON()%>');
        viewEmployeeRecordIdInInput(employeeSearchModel);

        <%
            Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance()
                                                                   .getOffice_unitsDTOByID(employee_attachmentDTO.officeUnitId);
        %>
        const officeUnitModel = {
            name: '<%=isLanguageEnglish?officeUnitsDTO.unitNameEng: officeUnitsDTO.unitNameBng %>',
            id: <%=employee_attachmentDTO.officeUnitId%>
        };
        viewOfficeIdInInput(officeUnitModel);
        <%}%>
    }

    function setMinEndDate() {
        if (getDateStringById("startDate_js_0") !== '') {
            setMinDateById("endDate_js_0", getDateStringById("startDate_js_0"));
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
    }

    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        $('#office_units_id_modal_button').hide();
        $('#office_units_id_div').show();
        $('#office_error_id').hide();

        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id_input').val(selectedOffice.id);
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function officeModalEditButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        officeSearchSetSelectedOfficeLayers($('#office_units_id_input').val());
        $('#search_office_modal').modal();
    }

    function convertToBanglaNumber(str) {
        str = String(str);
        str = str.replaceAll('0', '০');
        str = str.replaceAll('1', '১');
        str = str.replaceAll('2', '২');
        str = str.replaceAll('3', '৩');
        str = str.replaceAll('4', '৪');
        str = str.replaceAll('5', '৫');
        str = str.replaceAll('6', '৬');
        str = str.replaceAll('7', '৭');
        str = str.replaceAll('8', '৮');
        str = str.replaceAll('9', '৯');
        return str;
    }

    function viewEmployeeRecordIdInInput(empInfo) {
        $('#employee_record_id_modal_button').hide();
        $('#employee_error_id').hide();
        $('#employee_record_id_div').show();

        let language = '<%=Language.toLowerCase()%>';
        let employeeView;
        if (language === 'english') {
            employeeView = empInfo.userName + ' - ' + empInfo.employeeNameEn;
        } else {
            employeeView = convertToBanglaNumber(empInfo.userName) + ' - ' + empInfo.employeeNameBn;
        }
        document.getElementById('employee_record_id_text').innerHTML = employeeView;
        $('#employee_record_id_input').val(empInfo.employeeRecordId);
    }


    table_name_to_collcetion_map = new Map([
        ['employeeRecordId', {
            isSingleEntry: true,
            callBackFunction: viewEmployeeRecordIdInInput
        }]
    ]);
    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    function organogramIdModalBtnClicked() {
        modal_button_dest_table = 'organogramId';
        $('#search_emp_modal').modal();
    }

    function employeeRecordIdModalBtnClicked() {
        modal_button_dest_table = 'employeeRecordId';
        $('#search_emp_modal').modal();
    }

    $(document).ready(init);
</script>