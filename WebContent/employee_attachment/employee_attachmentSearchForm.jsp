<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="employee_attachment.*" %>
<%@ page import="java.util.List" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="util.StringUtils" %>
<%@page pageEncoding="UTF-8" %>
<%
    String navigator2 = BaseServlet.RECORD_NAVIGATOR;
%>
<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.OFFICE_AND_EMPLOYEE_MANAGEMENT_EMPLOYEE_NAME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_USER_NAME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_OFFICE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_ATTACHMENT_ADD_STATUS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_ATTACHMENT_ADD_STARTDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_ATTACHMENT_ADD_ENDDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_ATTACHMENT_SEARCH_EMPLOYEE_ATTACHMENT_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span><%=isLanguageEnglish?"All":"সকল"%></span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            List<Employee_attachmentDTO> data = (List<Employee_attachmentDTO>) rn2.list;
            if (data != null && data.size() > 0) {
                for (Employee_attachmentDTO employee_attachmentDTO : data) {
                    Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employee_attachmentDTO.employeeRecordsId);
        %>
        <tr>
            <td>
                <%=employeeRecordsDTO == null ? "" : (isLanguageEnglish ? employeeRecordsDTO.nameEng : employeeRecordsDTO.nameBng)%>
            </td>
            <td>
                <%=employeeRecordsDTO == null ? "" : (isLanguageEnglish ? employeeRecordsDTO.employeeNumber : StringUtils.convertToBanNumber(employeeRecordsDTO.employeeNumber))%>
            </td>

            <td>
                <%
                    Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employee_attachmentDTO.officeUnitId);
                %>

                <%=officeUnitsDTO == null ? "" : (isLanguageEnglish ? officeUnitsDTO.unitNameEng : officeUnitsDTO.unitNameBng)%>
            </td>

            <td>
                <%
                    EmployeeAttachmentStatusEnum employeeAttachmentStatusEnum = EmployeeAttachmentStatusEnum.getByValue(employee_attachmentDTO.status);
                    if (employeeAttachmentStatusEnum != null) {
                %>
                <span class="btn btn-sm border-0 shadow"
                      style="background-color: <%=employeeAttachmentStatusEnum.getColor()%> ; color: white; border-radius: 8px;cursor: text">
        					<%=isLanguageEnglish ? employeeAttachmentStatusEnum.getEngText() : employeeAttachmentStatusEnum.getBngText()%>
    			</span>
                <%
                    }
                %>
            </td>

            <td>
                <%=StringUtils.getFormattedDate(Language, employee_attachmentDTO.startDate)%>
            </td>

            <td>
                <%=StringUtils.getFormattedDate(Language, employee_attachmentDTO.endDate)%>
            </td>

            <%
                if (employee_attachmentDTO.status == 1) {
            %>
                <td>
                    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
                            onclick="location.href='Employee_attachmentServlet?actionType=getEditPage&ID=<%=employee_attachmentDTO.iD%>'">
                        <i class="fa fa-edit"></i>
                    </button>
                </td>

                <td class="text-right">
                    <div class='checker'>
                        <span class='chkEdit'><input type='checkbox' name='ID'
                                                     value='<%=employee_attachmentDTO.iD%>'/></span>
                    </div>
                </td>
            <%
                }else{
            %>
            <td></td>
            <td></td>
            <%
                }
            %>
        </tr>
        <% } } %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>