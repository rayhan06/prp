<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="employee_attachment.*" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="common.BaseServlet" %>
<%@page import="employee_records.Employee_recordsRepository" %>
<%@page import="employee_records.Employee_recordsDTO" %>
<%@page import="util.StringUtils" %>
<%@page import="office_units.Office_unitsRepository" %>
<%@page import="office_units.Office_unitsDTO" %>


<%
    Employee_attachmentDTO employee_attachmentDTO = (Employee_attachmentDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
%>
<%@include file="../pb/viewInitializer.jsp" %>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.EMPLOYEE_ATTACHMENT_ADD_EMPLOYEE_ATTACHMENT_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.EMPLOYEE_ATTACHMENT_ADD_EMPLOYEE_ATTACHMENT_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.EMPLOYEE_ATTACHMENT_ADD_EMPLOYEERECORDID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employee_attachmentDTO.employeeRecordsId);
                                        %>
                                        <%=employeeRecordsDTO==null?"":isLanguageEnglish?employeeRecordsDTO.nameEng + " | "+employeeRecordsDTO.employeeNumber
                                                :employeeRecordsDTO.nameBng+ " | "+ StringUtils.convertToBanNumber(employeeRecordsDTO.employeeNumber)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.EMPLOYEE_ATTACHMENT_ADD_OFFICEUNITID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employee_attachmentDTO.officeUnitId);
                                        %>
                                        <%=officeUnitsDTO==null?"":isLanguageEnglish?officeUnitsDTO.unitNameEng:officeUnitsDTO.unitNameBng%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.EMPLOYEE_ATTACHMENT_ADD_STATUS, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            EmployeeAttachmentStatusEnum employeeAttachmentStatusEnum = EmployeeAttachmentStatusEnum.getByValue(employee_attachmentDTO.status);
                                        %>
                                        <%=employeeAttachmentStatusEnum == null?"": (isLanguageEnglish?employeeAttachmentStatusEnum.getEngText():employeeAttachmentStatusEnum.getBngText())%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.EMPLOYEE_ATTACHMENT_ADD_STARTDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=StringUtils.getFormattedDate(Language,employee_attachmentDTO.startDate)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.EMPLOYEE_ATTACHMENT_ADD_ENDDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=StringUtils.getFormattedDate(Language,employee_attachmentDTO.endDate)%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>