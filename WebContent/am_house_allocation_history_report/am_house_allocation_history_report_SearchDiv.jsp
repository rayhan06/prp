<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row mx-2 mx-md-0">
    <div class="col-12">
		<div id="calendardiv" >
			<div id="visitDate" class="search-criteria-div">
				<div class="form-group row">
					<label class="col-md-3 col-form-label text-md-right">
						<%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT_WHERE_STARTTIME, loginDTO)%>
					</label>
					<div class="col-md-9">
						<jsp:include page="/date/date.jsp">
							<jsp:param name="DATE_ID" value="startDate_js"></jsp:param>
							<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
						</jsp:include>
						<input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
							   data-label="Document Date" id='startDate' name='startDate' value=""
							   tag='pb_html'
						/>
					</div>
				</div>
			</div>
			<div id="visitDate_3" class="search-criteria-div">
				<div class="form-group row">
					<label class="col-md-3 col-form-label text-md-right">
						<%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT_WHERE_ENDTIME, loginDTO)%>
					</label>
					<div class="col-md-9">
						<jsp:include page="/date/date.jsp">
							<jsp:param name="DATE_ID" value="endDate_js"></jsp:param>
							<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
						</jsp:include>
						<input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
							   data-label="Document Date" id='endDate' name='endDate' value=""
							   tag='pb_html'
						/>
					</div>
				</div>
			</div>

			<div id="visitDate_4" class="search-criteria-div">
				<div class="form-group row">
					<label class="col-md-3 col-form-label text-md-right">
						<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_HISTORY_REPORT_WHERE_WITHDRAWALDATE, loginDTO)%>
					</label>
					<div class="col-md-9">
						<jsp:include page="/date/date.jsp">
							<jsp:param name="DATE_ID" value="withdrawalStartDate_js"></jsp:param>
							<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
						</jsp:include>
						<input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
							   data-label="Document Date" id='withdrawalStartDate' name='withdrawalStartDate' value=""
							   tag='pb_html'
						/>
					</div>
				</div>
			</div>

			<div id="visitDate_5" class="search-criteria-div">
				<div class="form-group row">
					<label class="col-md-3 col-form-label text-md-right">
						<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_HISTORY_REPORT_WHERE_WITHDRAWALDATE_3, loginDTO)%>
					</label>
					<div class="col-md-9">
						<jsp:include page="/date/date.jsp">
							<jsp:param name="DATE_ID" value="withdrawalStartDate_js"></jsp:param>
							<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
						</jsp:include>
						<input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
							   data-label="Document Date" id='withdrawalEndDate' name='withdrawalEndDate' value=""
							   tag='pb_html'
						/>
					</div>
				</div>
			</div>

		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_WHERE_AMHOUSEOLDNEWCAT, loginDTO)%>
				</label>
				<div class="col-md-9">
					<select class='form-control'  name='amHouseOldNewCat' id = 'amHouseOldNewCat' >		
						<%		
						Options = CatDAO.getOptions(Language, "am_house_old_new", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_WHERE_AMHOUSELOCATIONCAT, loginDTO)%>
				</label>
				<div class="col-md-9">
					<select class='form-control'  name='amHouseLocationCat' id = 'amHouseLocationCat' >		
						<%		
						Options = CatDAO.getOptions(Language, "am_house_location", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_WHERE_AMHOUSECLASSCAT, loginDTO)%>
				</label>
				<div class="col-md-9">
					<select class='form-control'  name='amHouseClassCat' id = 'amHouseClassCat' >		
						<%		
						Options = CatDAO.getOptions(Language, "am_house_class", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div" style = "display: none;">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_WHERE_HOUSEID, loginDTO)%>
				</label>
				<div class="col-md-9">
					<input class='form-control'  name='houseId' id = 'houseId' value=""/>							
				</div>
			</div>
		</div>
		<div  class="search-criteria-div" style = "display: none;">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_WHERE_ISDELETED, loginDTO)%>
				</label>
				<div class="col-md-9">
					<input class='form-control'  name='isDeleted' id = 'isDeleted' value=""/>							
				</div>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">

$(document).ready(() => {
	showFooter = false;
});

function init()
{
    dateTimeInit($("#Language").val());
}
function PreprocessBeforeSubmiting()
{
}
</script>