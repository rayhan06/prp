<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.util.*" %>


<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.*" %>
<%@ page import="am_house_allocation_request.Am_house_allocation_requestDTO" %>
<%@ page import="util.StringUtils" %>
<%@ page import="am_house.Am_houseRepository" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ page import="am_house.Am_houseDTO" %>
<%@ page import="employee_family_info.Employee_family_infoDTO" %>
<%@ page import="am_reliable_family_member.Am_reliable_family_memberDTO" %>
<%@ page import="employee_family_info.Employee_family_infoDAO" %>
<%@ page import="am_reliable_family_member.Am_reliable_family_memberDAO" %>
<%@ page import="relation.RelationRepository" %>
<%@ page import="am_house_type_sub_category.Am_house_type_sub_categoryRepository" %>
<%@ page import="am_house_type_sub_category.Am_house_type_sub_categoryDTO" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>


<%
    String servletName = "Am_house_allocation_approval_mappingServlet";

    Am_house_allocation_requestDTO am_house_allocation_requestDTO = (Am_house_allocation_requestDTO) request.getAttribute("am_house_allocation_requestDTO");

    String context = request.getContextPath() + "/";

    List<Employee_family_infoDTO> employee_family_infoDTOS = Employee_family_infoDAO.getInstance().getByEmployeeId(am_house_allocation_requestDTO.requesterEmpId);
    List<Am_reliable_family_memberDTO> am_reliable_family_memberDTOS = Am_reliable_family_memberDAO.getInstance().getByEmployeeRecordId(am_house_allocation_requestDTO.requesterEmpId);

    HashMap<Long, Am_reliable_family_memberDTO> familyInfoIdToReliableMap = new HashMap<>();

    am_reliable_family_memberDTOS
            .forEach(am_reliable_family_memberDTO -> {
                familyInfoIdToReliableMap.put(am_reliable_family_memberDTO.employeeFamilyInfoId, am_reliable_family_memberDTO);
            });
%>
<%@include file="../pb/viewInitializer.jsp" %>

<style>
    .family-member {
        margin-top: 10rem !important;
    }

    .approval-state {
        margin-top: 5rem !important;
    }

    /*table {*/
    /*    page-break-inside: avoid !important;*/
    /*}*/

    .family-member {
        margin-top: 10rem !important;
    }

    .page-bg {
        background-color: #f9f9fb;
    }

    .symbol-taka-amount {
        display: flex;
        justify-content: space-between;
    }

    .full-border {
        border: 2px solid black;
        padding: 5px;
    }

    .fix-fill {
        overflow: hidden;
        white-space: nowrap;
    }

    #to-print-div * {
        font-size: 12px;
    }

    #to-print-div h1 {
        font-size: 16px;
        font-weight: bold;
    }

    #to-print-div h2 {
        font-size: 14px;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .4in;
        background: white;
        margin-bottom: 10px;
    }

    .foot-note {
        font-size: 11px !important;
    }

    .blank-to-fill {
        display: inline-block;
        border-bottom: 1px dotted black;
        width: inherit;
    }

    .table-bordered > :not(caption) > * {
        border-color: black;
    }

    .table-bordered > :not(caption) > * > * {
        border-color: black;
    }

    .table-bordered td {
        padding: 5px;
    }

    .no-top-border {
        border-top-color: white !important;
    }

    .no-bottom-border {
        border-bottom-color: white !important;
    }

    span.tab {
        display: inline-block;
        width: 5ch;
    }

    .pl-4-5 {
        padding-left: 2rem;
    }

    .pl-1-3 {
        padding-left: 1.3rem;
    }
</style>

<div class="kt-content" id="kt_content">
    <div class="">
        <div class="kt-portlet">
            <div class="kt-portlet__body page-bg p-0" id="bill-div">
                <div class="ml-auto my-5">
                    <button type="button" class="btn" id='download-pdf'
                            onclick="downloadTemplateAsPdf('to-print-div', 'House Allocation');">
                        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                </div>

                <div style="margin: auto;">
                    <div class="container" id="to-print-div">
                        <section class="page shadow" data-size="A4">
                            <div class="row">
                                <div class="col-12 text-center">

                                    <h5 class="mt-2">
                                        <%=UtilCharacter.getDataByLanguage(Language, "বাংলাদেশ জাতীয় সংসদ সচিবালয়", "Bangladesh National Parliament Secretariat")%>
                                    </h5>
                                    <h5 class="text-dark">
                                        <%=UtilCharacter.getDataByLanguage(Language, "এস্টেট অফিস", "Estate Office")%>
                                    </h5>
                                    <h5 class="text-dark">
                                        <%=UtilCharacter.getDataByLanguage(Language, "শেরে বাংলা নগর,ঢাকা", "Sher-e-Bangla Nagar, Dhaka")%>
                                    </h5>
                                    <h4 class="text-dark mt-4 font-weight-bold">
                                        <%=UtilCharacter.getDataByLanguage(Language, "জাতীয় সংসদ সচিবালয়ের প্রশাসনিক নিয়ন্ত্রণাধীন সরকারী বাসা বরাদ্দের আবেদন ফরম",
                                                "Application Form for Allotment of Government Houses Under the Administrative Control of the National Parliament Secretariat")%>
                                    </h4>
                                    <h5 class="text-sm-center">
                                        <%=UtilCharacter.getDataByLanguage(Language, "[জাতীয় সংসদ সচিবালয়ের কর্মকর্তা ও কর্মচারীদের বাসা বরাদ্দ নীতিমালা, ২০০৯ এর ৭(১) অনুচ্ছেদ দ্রষ্টব্য]",
                                                "[See Section 7 (1) of the House Allocation Policy for Officers and Employees of the National Parliament Secretariat, 2009]")%>
                                    </h5>
                                </div>
                            </div>
                            <form class="form-horizontal">
                                <div class="row mt-5">
                                    <div class="col-12 row mx-0 py-2 align-items-center">
                                        <div class="col-8">
                                            <span><%=UtilCharacter.getDataByLanguage(Language, "১", "1")%>।</span>
                                            <span class="ml-5"><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_FULL_NAME_OF_GOVERNMENT_OFFICER__EMPLOYEE, loginDTO)%><%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                        </div>
                                        <div class="col-4"><%=UtilCharacter.getDataByLanguage(Language, am_house_allocation_requestDTO.requesterNameBn, am_house_allocation_requestDTO.approverNameEn)%>
                                        </div>
                                    </div>
                                    <div class="col-12 row mx-0 py-2 align-items-center">
                                        <div class="col-8">
                                            <span><%=UtilCharacter.getDataByLanguage(Language, "২", "2")%>।</span>
                                            <span class="ml-5"><%=LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO)%><%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                        </div>
                                        <div class="col-4"><%=UtilCharacter.getDataByLanguage(Language, am_house_allocation_requestDTO.requesterOfficeUnitOrgNameBn, am_house_allocation_requestDTO.requesterOfficeUnitOrgNameEn)%>
                                        </div>
                                    </div>
                                    <div class="col-12 row mx-0 py-2 align-items-center">
                                        <div class="col-8">
                                            <span><%=UtilCharacter.getDataByLanguage(Language, "৩", "3")%>।</span>
                                            <span class="ml-5"><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_SUB_DEPARTMENT__SUB_BRANCH__BRANCH, loginDTO)%><%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                        </div>
                                        <div class="col-4">
                                            ....................................................................
                                        </div>
                                    </div>
                                    <div class="col-12 row mx-0 py-2 align-items-center">
                                        <div class="col-8">
                                            <span><%=UtilCharacter.getDataByLanguage(Language, "৪", "4")%>।</span>
                                            <span class="ml-5"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_DATEOFBIRTH, loginDTO)%><%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                        </div>
                                        <div class="col-4"><%=StringUtils.getFormattedDate(Language, am_house_allocation_requestDTO.dateOfBirth)%>
                                        </div>
                                    </div>
                                    <div class="col-12 row mx-0 py-2">
                                        <div class="col-12">
                                            <span><%=UtilCharacter.getDataByLanguage(Language, "৫", "5")%>।</span>
                                            <span class="ml-5"><%=LM.getText(LC.PI_PACKAGE_VENDOR_EDIT_PI_PACKAGE_VENDOR_CHILDREN_PIPACKAGEVENDORID, loginDTO)%><%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                        </div>
                                        <div class="col-12 pl-5">
                                            <div class="row">
                                                <div class="col-8">
                                                    <div class="pl-4-5">
                                                        <div class="py-2">
                                                            <span><%=UtilCharacter.getDataByLanguage(Language, "(ক)", "(ka)")%> </span>
                                                            <span class="ml-3"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_EMPLOYEEPAYSCALEID, loginDTO)%><%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-4 px-0"><%=CatRepository.getInstance().getText(Language, "job_grade_type", am_house_allocation_requestDTO.employeePayScaleId)%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 pl-5">
                                            <div class="row">
                                                <div class="col-8">
                                                    <div class="pl-4-5">
                                                        <div class="py-2">
                                                            <span>*<%=UtilCharacter.getDataByLanguage(Language, "(খ)", "(kha)")%></span>
                                                            <span class="ml-3"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_BASICSALARYBN, loginDTO)%><%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-4 px-0">
                                                    <div><%=Utils.getDigits(am_house_allocation_requestDTO.basicSalaryEn, Language)%>
                                                    </div>
                                                    <div>
                                                        (<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_ATTACH_UPDATED_PAYROLL, loginDTO)%>
                                                        )
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 pl-5">
                                            <div class="row align-items-center">
                                                <div class="col-8">
                                                    <div class="pl-4-5">
                                                        <div class="py-2">
                                                            <span><%=UtilCharacter.getDataByLanguage(Language, "(গ)", "(Ga)")%></span>
                                                            <span class="ml-3"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HASSPECIALSALARY, loginDTO)%><%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-4 px-0"><%=CatRepository.getInstance().getText(Language, "yes_no", am_house_allocation_requestDTO.hasSpecialSalary ? 1 : 0)%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 pl-5">
                                            <div class="row align-items-center">
                                                <div class="col-8">
                                                    <div class="pl-4-5">
                                                        <div class="py-2">
                                                            <span><%=UtilCharacter.getDataByLanguage(Language, "(ঘ)", "(Gha)")%></span>
                                                            <span class="ml-3"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_SPECIALSALARYAMOUNT, loginDTO)%><%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-4 px-0"><%if (am_house_allocation_requestDTO.hasSpecialSalary) {%><%=Utils.getDigits(String.valueOf(am_house_allocation_requestDTO.specialSalaryAmount), Language)%><%} else {%>
                                                    ....................................................................<%}%></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 row mx-0 py-2">
                                        <div class="col-12">
                                            <span><%=UtilCharacter.getDataByLanguage(Language, "৬", "6")%>।</span>
                                        </div>
                                        <div class="col-12 pl-5">
                                            <div class="row align-items-center">
                                                <div class="col-8">
                                                    <div class="pl-4-5">
                                                        <div class="py-2">
                                                            <span><%=UtilCharacter.getDataByLanguage(Language, "(ক)", "(ka)")%></span>
                                                            <span class="ml-3"><%=UtilCharacter.getDataByLanguage(Language, "আবেদনাধীন বাসার শ্রেণী ও নম্বর (প্রাপ্যতা অনুযায়ী)",
                                                                    "Class and number of the house under application (According to availability)")%>&nbsp;<%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <%
                                                    Am_houseDTO am_houseDTO = Am_houseRepository.getInstance().getAm_houseDTOByID(am_house_allocation_requestDTO.amHouseId);
                                                    Am_house_type_sub_categoryDTO houseSubTypeDTO = Am_house_type_sub_categoryRepository.getInstance()
                                                            .getAm_house_type_sub_categoryDTOByiD(am_house_allocation_requestDTO.amHouseClassSubCat);
                                                    String houseSubType = houseSubTypeDTO == null ? "null" :
                                                            Language == "English" ? houseSubTypeDTO.amHouseSubCatNameEn : houseSubTypeDTO.amHouseSubCatNameBn;
                                                %>
                                                <div class="col-4 px-0">
                                                    <%=CatRepository.getInstance().getText(Language, "am_house_class", am_house_allocation_requestDTO.amHouseClassCat)%>
                                                    -
                                                    <%=houseSubType%> Sub
                                                    Type-<%=am_houseDTO != null ? am_houseDTO.houseNumber : ""%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 pl-5">
                                            <div class="row align-items-center">
                                                <div class="col-8">
                                                    <div class="pl-4-5">
                                                        <div class="py-2">
                                                            <span><%=UtilCharacter.getDataByLanguage(Language, "(খ)", "(kha)")%></span>
                                                            <span class="ml-3"><%=LM.getText(LC.AM_HOUSE_ADD_AMHOUSEOLDNEWCAT, loginDTO)%>&nbsp;<%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-4 px-0"><%=CatRepository.getInstance().getText(Language, "am_house_old_new", am_house_allocation_requestDTO.amHouseOldNewCat)%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 pl-5">
                                            <div class="row align-items-center">
                                                <div class="col-8">
                                                    <div class="pl-4-5">
                                                        <div class="py-2">
                                                            <span><%=UtilCharacter.getDataByLanguage(Language, "(গ)", "(Ga)")%></span>
                                                            <span class="ml-3"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSELOCATIONCAT, loginDTO)%><%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-4 px-0"><%=CatRepository.getInstance().getText(Language, "am_house_location", am_house_allocation_requestDTO.amHouseLocationCat)%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 row mx-0 py-2">
                                        <div class="col-8">
                                            <span><%=UtilCharacter.getDataByLanguage(Language, "৭", "7")%>।</span>
                                            <span class="ml-5"><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_MARITALSTATUS, loginDTO)%><%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                        </div>
                                        <div class="col-4"><%=CatRepository.getInstance().getText(Language, "marital_status", am_house_allocation_requestDTO.maritalStatusCat)%>
                                        </div>
                                    </div>
                                    <div class="col-12 row mx-0 py-2 align-items-center">
                                        <div class="col-12">
                                            <span><%=UtilCharacter.getDataByLanguage(Language, "৮", "8")%>।</span>
                                            <span class="ml-5"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_RELIABLE_FAMILY_MEMBERS, loginDTO)%><%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                        </div>
                                    </div>
                                    <table
                                            class="mt-3 w-100 rounded"
                                            border="1px solid #323233;"
                                            style="border-collapse: collapse"
                                    >
                                        <thead>
                                        <tr>
                                            <th style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "ক্রমিক নং", "Serial No")%>
                                            </th>
                                            <th style="padding: 5px 10px"><%=LM.getText(LC.AM_RELIABLE_FAMILY_MEMBER_ADD_EMPLOYEERECORDSID, loginDTO)%>
                                            </th>
                                            <th style="padding: 5px 10px"><%=LM.getText(LC.APPLICANT_PROFILE_ADD_AGE, loginDTO)%>
                                            </th>
                                            <th style="padding: 5px 10px"><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_RELATIONTYPE, loginDTO)%>
                                            </th>
                                            <th style="padding: 5px 10px"><%=LM.getText(LC.AM_RELIABLE_FAMILY_MEMBER_ADD_REMARKS, loginDTO)%>
                                            </th>
                                            <th style="padding: 5px 10px"><%=LM.getText(LC.AM_RELIABLE_FAMILY_MEMBER_SEARCH_ANYFIELD, loginDTO)%>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <%
                                            ArrayList data = (ArrayList<Employee_family_infoDTO>) employee_family_infoDTOS;

                                            try {

                                                if (data != null) {
                                                    int size = data.size();
                                                    for (int iReliable = 0; iReliable < size; iReliable++) {
                                                        Employee_family_infoDTO employee_family_infoDTO = (Employee_family_infoDTO) data.get(iReliable);


                                        %>
                                        <tr>
                                            <td style="padding: 5px 10px"><%=UtilCharacter.convertDataByLanguage(Language, String.valueOf(iReliable + 1))%>
                                            </td>
                                            <td style="padding: 5px 10px">
                                                <%
                                                    if (Language.equals("Bangla")) {
                                                        value = employee_family_infoDTO.firstNameBn + " " + employee_family_infoDTO.lastNameBn;
                                                    } else {
                                                        value = employee_family_infoDTO.firstNameEn + " " + employee_family_infoDTO.lastNameEn;
                                                    }
                                                %>

                                                <%=value%>


                                                <input type='hidden' class='form-control' name='familyMemberId'
                                                       id='familyMemberId_hidden_<%=iReliable%>'
                                                       value='<%=employee_family_infoDTO.iD%>' tag='pb_html'/>


                                                <input type='hidden' class='form-control'
                                                       name='isReliableValues'
                                                       id='isReliableValues_hidden_<%=iReliable%>'
                                                       tag='pb_html'/>

                                            </td>
                                            <td style="padding: 5px 10px">
                                                <%
                                                    value = Utils.calculateCompleteAgeInFullFormat(employee_family_infoDTO.dob, Language) + "";
                                                %>

                                                <%=value%>


                                            </td>
                                            <td style="padding: 5px 10px">
                                                <%
                                                    value = RelationRepository.getInstance().getText(Language, employee_family_infoDTO.relationType) + "";
                                                %>

                                                <%=value%>


                                            </td>
                                            <td style="padding: 5px 10px">
                                                <%
                                                    value = familyInfoIdToReliableMap.getOrDefault(employee_family_infoDTO.iD, new Am_reliable_family_memberDTO()).remarks + "";
                                                %>


                                                <%=value%>


                                            </td>
                                            <td style="padding: 5px 10px">
                                                <%=(familyInfoIdToReliableMap.containsKey(employee_family_infoDTO.iD)
                                                        && familyInfoIdToReliableMap.get(employee_family_infoDTO.iD).isReliable == 1) ?
                                                        LM.getText(LC.JOB_APPLICANT_DOCUMENTS_YES, Language) :
                                                        LM.getText(LC.JOB_APPLICANT_DOCUMENTS_NO, Language)%>
                                            </td>
                                        </tr>
                                        <%
                                                    }

                                                    System.out.println("printing done");
                                                } else {
                                                    System.out.println("data  null");
                                                }
                                            } catch (Exception e) {
                                                System.out.println("JSP exception " + e);
                                            }
                                        %>

                                        </tbody>
                                    </table>
                                </div>
                            </form>
                        </section>
                        <section class="page shadow" data-size="A4">
                            <form class="form-horizontal">
                                <div class="col-12 row py-2 align-items-center">
                                    <div class="col-8">
                                        <span><%=UtilCharacter.getDataByLanguage(Language, "৯", "9")%>।</span>
                                        <span class="ml-5"><%=UtilCharacter.getDataByLanguage(Language, "(ক)", "(ka)")%></span>
                                        <span class="ml-2"><%=UtilCharacter.getDataByLanguage(Language, "সরকারি চাকরিতে প্রথম নিয়োগের তারিখ ও ঐ তারিখে বেতন", "Appointment Date and Salary in the First Government Service")%><%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                    </div>
                                    <div class="col-4"><%=StringUtils.getFormattedDate(Language, am_house_allocation_requestDTO.govtFirstAppointmentDate)%><%=UtilCharacter.getDataByLanguage(Language, " ও ", " And ")%><%=Utils.getDigits(am_house_allocation_requestDTO.govtFirstSalary+" /-", Language)%>

                                    </div>
                                </div>
                                <div class="col-12 row py-2 align-items-center">
                                    <div class="col-8">
                                        <span class="ml-5 pl-1-3"><%=UtilCharacter.getDataByLanguage(Language, "(খ)", "(kha)")%></span>
                                        <span class="ml-2"><%=UtilCharacter.getDataByLanguage(Language, "সংসদ সচিবালয়ে প্রথম যোগদানের তারিখ ও পদের নাম", "Date of First Joining the Parliament Secretariat and Name of the Post")%><%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                    </div>
                                    <div class="col-4"><%=StringUtils.getFormattedDate(Language, am_house_allocation_requestDTO.prpFirstJoiningDate)%><%=UtilCharacter.getDataByLanguage(Language, " ও ", " And ")%><%=UtilCharacter.getDataByLanguage(Language, am_house_allocation_requestDTO.prpFirstJoiningDesignationBn, am_house_allocation_requestDTO.prpFirstJoiningDesignationEn)%>
                                    </div>
                                </div>
                                <div class="col-12 row py-2 align-items-center">
                                    <div class="col-8">
                                        <span class="ml-5 pl-1-3"><%=UtilCharacter.getDataByLanguage(Language, "(গ)", "(Ga)")%></span>
                                        <span class="ml-2"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_CURRENTDESIGNATIONJOININGDATE, loginDTO)%><%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                    </div>
                                    <div class="col-4"><%=StringUtils.getFormattedDate(Language, am_house_allocation_requestDTO.currentDesignationJoiningDate)%>
                                    </div>
                                </div>
                                <div class="col-12 row py-2">
                                    <div class="col-8" style="padding-left: 5rem">
                                        <span class="">*<%=UtilCharacter.getDataByLanguage(Language, "(ঘ)", "(Gha)")%></span>
                                        <span class=""><%=UtilCharacter.getDataByLanguage(Language, "আবেদিত বাসার প্রাপ্যতা অর্জনের তারিখ ও ঐ তারিখে মূল বেতন (প্রাপ্যতা অর্জনের তারিখের বেতন বিবরণী সংযুক্ত করতে হবে)", "Date and Basic Salary on the Date of Acquisition of the Requested House (The Salary Statement Of The Date Of Receipt Should Be Attached)")%>&nbsp;<%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                    </div>
                                    <div class="col-4"><%=StringUtils.getFormattedDate(Language, am_house_allocation_requestDTO.requestedHomeAvailablityDate)%><%=UtilCharacter.getDataByLanguage(Language, " ও ", " And ")%><%=Utils.getDigits(am_house_allocation_requestDTO.requestedHomeAvailablitySalaryEn+" /-", Language)%>

                                    </div>
                                </div>
                                <div class="col-12 row py-2">
                                    <div class="col-8" style="padding-left: 5rem">
                                        <span class="">*<%=UtilCharacter.getDataByLanguage(Language, "(ঙ)", "(ঙ)")%></span>
                                        <span class=""><%=UtilCharacter.getDataByLanguage(Language, "প্রাপ্যতার ঊর্ধ্বে বাসার জন্যে আবেদন করেছেন?করলে কয় ধাপ ঊর্ধ্বে আবেদন করেছেন?", "Have You Applied For Housing Beyond Availability? How Many Steps Above Have You Applied For?")%>&nbsp;<%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                    </div>
                                    <div class="col-4"><%=CatRepository.getInstance().getText(Language, "yes_no", am_house_allocation_requestDTO.isHigherAvailability ? 1 : 0)%><%if (am_house_allocation_requestDTO.isHigherAvailability) {%><%=UtilCharacter.getDataByLanguage(Language, ", ", ", ")%><%=Utils.getDigits(String.valueOf(am_house_allocation_requestDTO.higherStepNumber), Language)%><%}%></div>
                                </div>
                                <div class="col-12 row py-2 align-items-start">
                                    <div class="col-8">
                                        <span><%=UtilCharacter.getDataByLanguage(Language, "১০", "10")%>।</span>
                                        <span class="ml-5"><%=UtilCharacter.getDataByLanguage(Language, "(ক)", "(ka)")%></span>
                                        <span class="ml-2"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_CURRENTHOUSE, loginDTO)%><%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                    </div>
                                    <div class="col-4"><%=CatRepository.getInstance().getText(Language, "am_current_house", am_house_allocation_requestDTO.currentHouseCat)%>
                                    </div>
                                </div>
                                <div class="col-12 row py-2 align-items-start">
                                    <div class="col-8">
                                        <span class="ml-5"
                                              style="padding-left: 1.7rem"><%=UtilCharacter.getDataByLanguage(Language, "(খ)", "(kha)")%></span>
                                        <span class="ml-2"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_CURRENTHOUSEDESCRIPTION, loginDTO)%><%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                    </div>
                                    <div class="col-4"><%=am_house_allocation_requestDTO.currentHouseDescription%>
                                    </div>
                                </div>
                                <div class="col-12 row py-2 align-items-start" style="padding-left: 1.6rem">
                                    <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_CURRENT_HOUSE_MISCELLANEOUS, loginDTO)%>
                                    &nbsp;<%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%>
                                </div>
                                <div class="col-12 row py-2 align-items-start">
                                    <div class="col-8">
                                        <span><%=UtilCharacter.getDataByLanguage(Language, "১১", "11")%>।</span>
                                        <span class="ml-5"><%=UtilCharacter.getDataByLanguage(Language, "(ক)", "(ka)")%></span>
                                        <span class="ml-2"><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_IS_THERE_ANY_HOUSE_OF_APPLICANT_OR_HIS_FAMILY_MEMBERS_IN_DHAKA_CITY, loginDTO)%>&nbsp;<%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                    </div>
                                    <div class="col-4"><%=CatRepository.getInstance().getText(Language, "yes_no", am_house_allocation_requestDTO.hasHouseInDhaka ? 1 : 0)%>
                                    </div>
                                </div>
                                <%if (am_house_allocation_requestDTO.hasHouseInDhaka) {%>
                                <div class="col-12 row py-2 align-items-start">
                                    <div class="col-8">
                                        <span class="ml-5"
                                              style="padding-left: 1.7rem"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HOUSEINDHAKALOCATION, loginDTO)%></span>
                                    </div>
                                    <div class="col-4"><%if (am_house_allocation_requestDTO.hasHouseInDhaka) {%><%=am_house_allocation_requestDTO.houseInDhakaLocationDetails%><%}%></div>
                                </div>
                                <%}%>
                                <%if (am_house_allocation_requestDTO.hasHouseInDhaka) {%>
                                <div class="col-12 row py-2 align-items-start">
                                    <div class="col-8" style="padding-left: 2.3rem">
                                        <span class="ml-5"><%=UtilCharacter.getDataByLanguage(Language, "(খ)", "(kha)")%></span>
                                        <span class="ml-2"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HOUSEINDHAKACONSTRUCTIONDATE, loginDTO)%><%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                    </div>
                                    <div class="col-4"><%=StringUtils.getFormattedDate(Language, am_house_allocation_requestDTO.houseInDhakaConstructionDate)%>
                                    </div>
                                </div>
                                <%}%>
                                <div class="col-12 row py-2 align-items-center">
                                    <div class="col-8">
                                        <span><%=UtilCharacter.getDataByLanguage(Language, "১২", "12")%>।</span>
                                        <span class="ml-5"><%=UtilCharacter.getDataByLanguage(Language, "স্বামী /স্ত্রী /পিতা /মাতা (প্রযোজ্য ক্ষেত্রে) সরকারি চাকুরীজীবি হলে নিম্নবর্ণিত তথ্য দিনঃ", "If the husband / wife / father / mother (where applicable) is a government employee, give the following information:")%></span>
                                    </div>
                                    <div class="col-4"><%=CatRepository.getInstance().getText(Language, "yes_no", am_house_allocation_requestDTO.hasHouseInDhaka ? 1 : 0)%>
                                    </div>
                                </div>
                                <%
                                    if (am_house_allocation_requestDTO.hasRelativeGovtHouse) {
                                %>
                                <div class="col-12 row py-2 align-items-center">
                                    <div class="col-8">
                                        <span class="ml-5 pl-5"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_GOVTHOUSERELATIVENAME, loginDTO)%></span>
                                    </div>
                                    <div class="col-4"><%if (am_house_allocation_requestDTO.hasRelativeGovtHouse) {%><%=am_house_allocation_requestDTO.govtHouseRelativeName%><%}%></div>
                                </div>
                                <div class="col-12 row py-2 align-items-center">
                                    <div class="col-8">
                                        <span class="ml-5 pl-5"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_SEARCH_RELATIONSHIPCAT, loginDTO)%></span>
                                    </div>
                                    <div class="col-4"><%if (am_house_allocation_requestDTO.hasRelativeGovtHouse) {%><%=CatRepository.getInstance().getText(Language, "relationship", am_house_allocation_requestDTO.relationshipCat)%><%}%></div>
                                </div>
                                <div class="col-12 row py-2 align-items-center">
                                    <div class="col-8">
                                        <span class="ml-5 pl-5"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_RELATIVEDESIGNATION, loginDTO)%></span>
                                    </div>
                                    <div class="col-4"><%if (am_house_allocation_requestDTO.hasRelativeGovtHouse) {%><%=am_house_allocation_requestDTO.relativeDesignation%><%}%></div>
                                </div>
                                <div class="col-12 row py-2 align-items-center">
                                    <div class="col-8">
                                        <span class="ml-5 pl-5"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REALTIVEOFFICE, loginDTO)%></span>
                                    </div>
                                    <div class="col-4"><%if (am_house_allocation_requestDTO.hasRelativeGovtHouse) {%><%=am_house_allocation_requestDTO.realtiveOffice%><%}%></div>
                                </div>
                                <div class="col-12 row py-2 align-items-center">
                                    <div class="col-8">
                                        <span class="ml-5 pl-5"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_RELATIVESALARY, loginDTO)%></span>
                                    </div>
                                    <div class="col-4"><%if (am_house_allocation_requestDTO.hasRelativeGovtHouse) {%><%=Utils.getDigits(am_house_allocation_requestDTO.relativeSalary, Language)%><%}%></div>
                                </div>
                                <div class="col-12 row py-2 align-items-center">
                                    <div class="col-8">
                                        <span class="ml-5 pl-5"><%=UtilCharacter.getDataByLanguage(Language, "তার নামে সরকারী বাসা আছে কিনা?", "Is there a government house in his/her name?")%></span>
                                    </div>
                                    <div class="col-4"><%=CatRepository.getInstance().getText(Language, "yes_no", am_house_allocation_requestDTO.hasHouseInDhaka ? 1 : 0)%>
                                    </div>
                                </div>
                                <%}%>
                                <div class="col-12 row py-2 align-items-start">
                                    <div class="col-8">
                                        <span><%=UtilCharacter.getDataByLanguage(Language, "১৩", "13")%>।</span>
                                        <span class="ml-5"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HOUSENEEDEDDESCRIPTION, loginDTO)%><%=UtilCharacter.getDataByLanguage(Language, "ঃ", ":")%></span>
                                    </div>

                                </div>
                                <div class="col-12 py-2 " style="padding-left: 5.5rem;">
                                    <%=am_house_allocation_requestDTO.houseNeededDescription%>
                                </div>
                            </form>
                        </section>
                        <section class="page shadow" data-size="A4">
                            <form class="form-horizontal">
                                <div class="mt-5 mb-4 col-12">
                                    <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_I_CERTIFY_THAT_THE_ABOVE_INFORMATION_IS_TRUE_TO_MY_KNOWLEDGE_AND_BELIEF__IF_THE_INFORMATION_PROVIDED_IS_PROVED_TO_BE_UNTRUE__I_WILL_BE_RESPONSIBLE_ACCORDING_TO_THE_LAW, loginDTO)%>
                                </div>
                                <div class="col-12 row ">
                                    <div class="col-4 offset-8">
                                        <div class="">

                                            <%

                                                Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().
                                                        getById(am_house_allocation_requestDTO.requesterEmpId);
                                            %>
                                            <div>
                                                <%
                                                    byte[] encodeBase64Photo = Base64.encodeBase64(employee_recordsDTO.signature);
                                                %>
                                                <img
                                                        class="rounded"
                                                        src="data:image/jpg;base64,<%=encodeBase64Photo != null ? new String(encodeBase64Photo) : ""%>"
                                                        width="150"
                                                        height="100"
                                                        id="applicantSignature"
                                                        style="object-fit: contain;"
                                                >
                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <div class="col-12 row ">
                                    <div class="col-4 offset-8">
                                        <div class="pb-2"><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_SIGNATURE_OF_THE_APPLICANT, loginDTO)%>
                                        </div>
                                        <div class=""><%=UtilCharacter.getDataByLanguage(Language, "তারিখ", "Date")%>:
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 d-flex justify-content-between align-items-center mt-3">
                                    <div>
                                        <span><%=UtilCharacter.getDataByLanguage(Language, "নং", "No.")%>-</span>
                                        <span></span>
                                    </div>
                                    <div style="padding-right: 15.5rem;">
                                        <span><%=UtilCharacter.getDataByLanguage(Language, "তারিখ", "Date")%>:</span>
                                        <span></span>
                                    </div>
                                </div>
                                <table class="w-100 mt-3" style="margin-left: 3rem">
                                    <tr>
                                        <td><%=UtilCharacter.getDataByLanguage(Language, "বরাবর", "Along")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 10px">
                                            <%=UtilCharacter.getDataByLanguage(Language, "মাননীয় স্পীকার, বাংলাদেশ জাতীয় সংসদ/সচিব, জাতীয় সংসদ সচিবালয়/আহবায়ক/সভাপতি, বাসা বরাদ্দ কমিটি, জাতীয় সংসদ সচিবালয়।",
                                                    "Honorable Speaker, Bangladesh National Parliament / Secretary, National Parliament Secretariat / Convener / President, Home Allocation Committee, National Parliament Secretariat.")%>

                                        </td>
                                    </tr>
                                </table>
                                <%if (am_house_allocation_requestDTO.status == CommonApprovalStatus.SATISFIED.getValue()) {%>

                                <table class="w-100 mt-3" style="margin-left: 3rem">

                                    <tr>
                                        <td style="padding-top: 10px">

                                            <%=UtilCharacter.getDataByLanguage(Language,
                                                    "আবেদনকারী বাংলাদেশ জাতীয় সংসদ সচিবালয়ের একজন গেজেটেড/নন-গেজেটেড কর্মচারী। তার আবেদনটি অগ্রায়ন করা হলাে।",
                                                    "The applicant is a Gazetted / Non-Gazetted employee " +
                                                            "of the Secretariat of Bangladesh National Parliament. His application was forwarded.")%>
                                        </td>
                                    </tr>
                                </table>

                                <div class="col-12 row " style="padding-top: 20px">
                                    <div class="col-4 offset-8">
                                        <div class="">

                                            <%

                                                Employee_recordsDTO approverEmployee_recordsDTO = Employee_recordsRepository.getInstance().
                                                        getById(am_house_allocation_requestDTO.firstLayerApproverEmpId);
                                                byte[] encodeBase64Photo2 = approverEmployee_recordsDTO != null ? Base64.encodeBase64(approverEmployee_recordsDTO.signature) : "".getBytes();
                                            %>
                                            <div>

                                                <img
                                                        class="rounded mb-1"
                                                        src="data:image/jpg;base64,<%=encodeBase64Photo2 != null ? new String(encodeBase64Photo2) : ""%>"
                                                        width="150"
                                                        height="100"
                                                        id="firstApproverSignature"
                                                        style="object-fit: contain;"
                                                > <br>
                                                <%=UtilCharacter.getDataByLanguage(Language, "নিয়ন্ত্রণকারী কর্মকর্তার স্বাক্ষর", "Signature of the controlling officer")%>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <%}%>
                            </form>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions text-right mt-4">

        <button class="btn btn-sm btn-primary shadow btn-border-radius ml-2" onclick="history.back()"
        ><%=LM.getText(LC.GO_BACK_BACK, loginDTO)%>
        </button>

        <%if (am_house_allocation_requestDTO.status == CommonApprovalStatus.PENDING.getValue()) {%>
        <button type="button" class="btn btn-success btn-sm shadow ml-2" style="border-radius: 8px;" id="reject_btn"
                onclick="approveApplication()">
            <%=UtilCharacter.getDataByLanguage(Language, "অনুমোদন করুন", "APPROVE")%>
        </button>

        <button type="button" class="btn btn-danger btn-sm shadow ml-2" style="border-radius: 8px;" id="reject_btn"
                onclick="rejectApplication()">
            <%=LM.getText(LC.CARD_APPROVAL_MAPPING_DO_NOT_APPOVE, loginDTO)%>
        </button>
        <%}%>
    </div>
</div>

<div style="display: none">
    <form id="rejectForm" name="rejectForm">
        <input type="hidden" name="amHouseAllocationRequestId" value="<%=am_house_allocation_requestDTO.iD%>">
        <input type="hidden" name="reject_reason" id="reject_reason">
    </form>
</div>

<script type="text/javascript">
    function approveApplication() {
        let approveButtonUrl = "Am_house_allocation_requestServlet?actionType=giveFirstLayerApproval" +
            "&amHouseAllocationRequestId=" + '<%=am_house_allocation_requestDTO.iD%>';

        $.ajax({
            type: "POST",
            url: approveButtonUrl,
            data: '',
            dataType: 'JSON',
            success: function (response) {
                if (response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                    buttonStateChange(false);
                } else if (response.responseCode === 200) {
                    showToastSticky("বাসা অনুমোদন করা হয়েছে", "House Approved");
                    setTimeout(() => {
                        window.location.replace(getContextPath() + response.msg);
                    }, 3000);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChange(false);
            }
        });
    }

    function rejectApplication() {
        let msg = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_REASON_FOR_DISSATISFACTION, loginDTO)%>';
        let placeHolder = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_TYPE_DISSATISFACTION_REASON_HERE, loginDTO)%>';
        let confirmButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_YES_DISSATISFIED, loginDTO)%>';
        let cancelButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARD_APPROVAL_MAPPING_CANCEL_BUTTON, loginDTO)%>';
        let emptyReasonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_PLEASE_WRITE_DISSATISFACTION_REASON, loginDTO)%>';
        dialogMessageWithTextBoxWithoutAnimation(msg, placeHolder, confirmButtonText, cancelButtonText, emptyReasonText, (reason) => {
            $('#reject_btn').prop("disabled", true);
            $('#reject_reason').val(reason);
            doRejectionAjaxCall();
        }, () => {
        });
    }

    function doRejectionAjaxCall(){
        let rejectionUrl = "Am_house_allocation_requestServlet?actionType=giveFirstLayerRejection" +
            "&amHouseAllocationRequestId=" + '<%=am_house_allocation_requestDTO.iD%>';
        $.ajax({
            type: "POST",
            url: rejectionUrl,
            data: $('#rejectForm').serialize(),
            dataType: 'JSON',
            success: function (fetchedData) {
                showToastSticky("বাসা অনুমোদন বাতিল করা হয়েছে", "House Approval Rejected");
                setTimeout(() => {
                    window.location.replace(getContextPath());
                }, 3000);
            },
            error: function (error) {
                toastr.error(error);
            }
        });
    }

    function downloadTemplateAsPdf(divId, fileName) {
        let content = document.getElementById(divId);
        const opt = {
            margin: 0.5,
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4'}
        };
        html2pdf().from(content).set(opt).save();
    }
</script>