<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="util.UtilCharacter" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    System.out.println("Inside nav.jsp");
    String url = "Am_house_allocation_requestServlet?actionType=prePendingSearch";
    Employee_recordsDTO employeeRecordsDTO = null;
    String employeeRecordIdStr = request.getParameter("employeeRecordsId");
    if (employeeRecordIdStr != null && employeeRecordIdStr.length() > 0) {
        employeeRecordsDTO = Employee_recordsRepository.getInstance()
                .getById(Long.parseLong(employeeRecordIdStr));
    }
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__body">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=UtilCharacter.getDataByLanguage(Language, "কর্মকর্তা",
                                    "Employee")%>
                        </label>
                        <div class="col-md-9">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                    id="employeeRecordId_modal_button"
                                    onclick="employeeRecordIdModalBtnClicked();">
                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                            </button>
                            <div class="input-group" id="employeeRecordId_div" style="display: none">
                                <input type="hidden" name='employeeRecordId' id='employeeRecordId_input' value=""
                                       onchange='setSearchChanged()'>
                                <button type="button" class="btn btn-secondary form-control" disabled
                                        id="employeeRecordId_text"></button>
                                <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                                    <button type="button" class="btn btn-outline-danger"
                                            onclick="crsBtnClicked('employeeRecordId');"
                                            id='employeeRecordId_crs_btn' tag='pb_html'>
                                        x
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.FUND_APPROVAL_MAPPING_ADD_FUNDAPPLICATIONSTATUSCAT, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='fundApplicationStatusCat'
                                    id='fund_application_status_cat' onSelect='setSearchChanged()'>
                                <%=CatRepository.getInstance().buildOptions("am_house_allocation_status", Language, -1)%>
                            </select>
                        </div>
                    </div>
                </div>


            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>


<template id="loader">
    <div class="modal-body">
        <div class="search-loader-container-circle ">
            <div class="search-loader-circle"></div>
        </div>
    </div>
</template>

<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>
<script type="text/javascript">

    const requesterSelector = $('#employeeRecordId_input');
    const statusSelector = $('#fund_application_status_cat');
    const servletName = "Am_house_allocation_requestServlet";
    let employeeModel;

    $(document).ready(() => {

        readyInit(servletName);

        <%if(employeeRecordsDTO != null) {%>
        employeeModel = JSON.parse('<%=EmployeeSearchModalUtil.getEmployeeDefaultSearchModelJson(employeeRecordsDTO.iD)%>')
        employeeRecordIdInInput(employeeModel);
        <%}%>


    });

    function resetInputs() {
        crsBtnClicked('employeeRecordId');
        $('select').prop('selectedIndex', 0);
    }

    window.addEventListener('popstate', e => {
        if (e.state) {

            let params = e.state.params;
            employeeModel = e.state.employeeModel;
            if (employeeModel) {
                employeeRecordIdInInput(employeeModel);
            }
            dosubmit(params, false);
            //resetInputs();
            let arr = params.split('&');
            arr.forEach(e => {
                let item = e.split('=');
                if (item.length === 2) {
                    switch (item[0]) {

                        case 'employeeRecordsId':
                            if (item[1].length == 0) {
                                crsBtnClicked('employeeRecordId');
                            }
                            break;
                        case 'am_house_allocation_status_cat':
                            $('#fund_application_status_cat option[value="' + item[1] + '"]').prop('selected', 'selected').change();
                            break;
                        default:
                            setPaginationFields(item);
                    }
                }
            });
        } else {
            dosubmit(null, false);
            resetInputs();
            resetPaginationFields();
        }
    });


    function dosubmit(params, pushState = true) {
        document.getElementById('tableData').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (pushState) {
                    let stateParam = {
                        'params': params,
                        'employeeModel': employeeModel
                    }
                    history.pushState(stateParam, '', servletName + '?actionType=prePendingSearch&' + params);
                }
                setTimeout(() => {
                    document.getElementById('tableData').innerHTML = this.responseText;
                    setPageNo();
                    searchChanged = 0;
                }, 200);
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        let url = "Am_house_allocation_requestServlet?actionType=prePendingSearch&ajax=true&isPermanentTable=true";
        if (params) {
            url += "&" + params;
        }
        xhttp.open("Get", url, false);
        xhttp.send();


    }

    function allfield_changed(go, pagination_number, pushState) {
        let params = 'search=true';
        if (statusSelector.val()) {
            params += '&am_house_allocation_status_cat=' + $("#fund_application_status_cat").val();
        }
        if (requesterSelector.val()) {
            params += '&employeeRecordsId=' + $('#employeeRecordId_input').val();
        }

        let extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        let pageNo = document.getElementsByName('pageno')[0].value;
        let rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        let totalRecords = 0;
        let lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params, pushState);

    }

    function employeeRecordIdInInput(empInfo) {

        employeeModel = empInfo;
        $('#employeeRecordId_modal_button').hide();
        $('#employeeRecordId_div').show();

        let language = '<%=Language.toLowerCase()%>'
        let designation;
        if (language === 'english') {
            designation = empInfo.employeeNameEn + ' (' + empInfo.organogramNameEn + ', ' + empInfo.officeUnitNameEn + ')';
        } else {
            designation = empInfo.employeeNameBn + ' (' + empInfo.organogramNameBn + ', ' + empInfo.officeUnitNameBn + ')';
        }
        document.getElementById('employeeRecordId_text').innerHTML = designation;
        $('#employeeRecordId_input').val(empInfo.employeeRecordId);
    }

    table_name_to_collcetion_map = new Map([
        ['employeeRecordId', {
            isSingleEntry: true,
            callBackFunction: employeeRecordIdInInput
        }]
    ]);

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    function employeeRecordIdModalBtnClicked() {
        modal_button_dest_table = 'employeeRecordId';
        $('#search_emp_modal').modal();
    }

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
    }

</script>

