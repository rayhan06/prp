<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.util.List" %>
<%@ page import="am_house_allocation_request.Am_house_allocation_requestDTO" %>
<%@ page import="am_house.Am_houseDTO" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="am_house.Am_houseRepository" %>
<%@ page import="am_house_type_sub_category.Am_house_type_sub_categoryDTO" %>
<%@ page import="am_house_type_sub_category.Am_house_type_sub_categoryRepository" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>
<%@ page import="am_house_allocation_request.AmHouseAllocationRequestStatus" %>


<%
    String navigator2 = "navAM_HOUSE_ALLOCATION_APPROVAL_MAPPING";
    String servletName = "Am_house_allocation_requestServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.VM_FUEL_REQUEST_REPORT_WHERE_REQUESTEREMPID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_OFFICE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_FUEL_REQUEST_REPORT_WHERE_APPLICATIONDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_CLASS_OF_HOUSING_UNDER_APPLICATION, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.FUND_APPROVAL_MAPPING_ADD_FUNDAPPLICATIONSTATUSCAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th colspan="3"><%=LM.getText(LC.HM_UPDATE, loginDTO)%>
            </th>

        </tr>
        </thead>

        <tbody>

        <%
            List<Am_house_allocation_requestDTO> data = (List<Am_house_allocation_requestDTO>) rn2.list;
            int i = -1;
            if (data != null && data.size() > 0) {
                i++;
                for (Am_house_allocation_requestDTO model : data) {
        %>

        <tr>
            <td>
                <%=UtilCharacter.getDataByLanguage(Language, model.requesterNameBn, model.requesterNameEn)%>
            </td>

            <td>
                <%=UtilCharacter.getDataByLanguage(Language, model.requesterOfficeUnitOrgNameBn, model.requesterOfficeUnitOrgNameEn)%>
            </td>

            <td>
                <%=UtilCharacter.getDataByLanguage(Language, model.requesterOfficeUnitNameBn, model.requesterOfficeUnitNameEn)%>
            </td>

            <td>
                <%=StringUtils.getFormattedDate(Language, model.insertionDate)%>
            </td>

            <td>
                <%
                    String houseType = CatRepository.getInstance().getText(Language, "am_house_class", model.amHouseClassCat);
                    Am_houseDTO am_houseDTO = Am_houseRepository.getInstance().getAm_houseDTOByID(model.amHouseId);
                    Am_house_type_sub_categoryDTO houseSubTypeDTO = Am_house_type_sub_categoryRepository.getInstance()
                            .getAm_house_type_sub_categoryDTOByiD(model.amHouseClassSubCat);
                    String houseSubType = houseSubTypeDTO == null ? "null" :
                            Language == "English" ? houseSubTypeDTO.amHouseSubCatNameEn : houseSubTypeDTO.amHouseSubCatNameBn;
                    if (am_houseDTO != null) {
                        String houseNumber = am_houseDTO.houseNumber;
                        if (houseSubType.equals("null")) {%>
                            <%=houseType%>-<%= houseNumber%>
                        <%} else {%>
                            <%=houseType%>-<%=houseSubType%> Sub Type-<%= houseNumber%>
                        <%
                        }
                    }
                %>

            </td>

            <td>
    <span class="btn btn-sm border-0 shadow"
          style="background-color: <%=AmHouseAllocationRequestStatus.getColor(model.status)%>; color: white; border-radius: 8px;cursor: text">
            <%=CatRepository.getInstance().getText(
                    Language, "am_house_allocation_status", model.status
            )%>
    </span>
            </td>

            <%CommonDTO commonDTO = model; %>


            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=getApprovalPage&amHouseAllocationRequestId=<%=model.iD%>'"
                >
                    <i class="fa fa-eye"></i>
                </button>


            </td>

            <%if (model.status == CommonApprovalStatus.PENDING.getValue()) {%>

            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow "
                        style="background-color: #228B22;color: white;border-radius: 8px;cursor: pointer;"
                        onclick="location.href='<%=servletName%>?actionType=getApprovalPage&amHouseAllocationRequestId=<%=model.iD%>'"
                >
                    <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_SEARCH_APPROVE, loginDTO)%>
                </button>
            </td>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow "
                        style="background-color: #1E90FF;color: white;border-radius: 8px;cursor: pointer;"
                        onclick="location.href='<%=servletName%>?actionType=getApprovalPage&amHouseAllocationRequestId=<%=model.iD%>'"
                >
                    <%=LM.getText(LC.HM_PDF, loginDTO)%>
                </button>
            </td>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow "
                        style="background-color: #B22222;color: white;border-radius: 8px;cursor: pointer;"
                        onclick="rejectApplication(<%=model.iD%>);"
                        id="reject_btn_<%=i%>"
                >
                    <%=LM.getText(LC.USER_ADD_CANCEL, loginDTO)%>
                </button>
            </td>

            <%} else if (model.status == CommonApprovalStatus.SATISFIED.getValue()) {%>

            <td>
            </td>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow "
                        style="background-color: #1E90FF;color: white;border-radius: 8px;cursor: pointer;"
                        onclick="location.href='<%=servletName%>?actionType=getApprovalPage&amHouseAllocationRequestId=<%=model.iD%>'"
                >
                    <%=LM.getText(LC.HM_PDF, loginDTO)%>
                </button>
            </td>
            <td>

            </td>

            <%
            } else if (model.status == CommonApprovalStatus.DISSATISFIED.getValue() ||
                    model.status == CommonApprovalStatus.WITHDRAWN.getValue()) {
            %>
            <td>
            </td>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow "
                        style="background-color: #1E90FF;color: white;border-radius: 8px;cursor: pointer;"
                        onclick="location.href='<%=servletName%>?actionType=getApprovalPage&amHouseAllocationRequestId=<%=model.iD%>'"
                >
                    <%=LM.getText(LC.HM_PDF, loginDTO)%>
                </button>
            </td>

            <td>
            </td>

            <%}%>

            <% }
            } %>
        </tr>


        </tbody>


    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>

<script>


    function rejectApplication(amHouseAllocationRequestId) {


        let msg = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_REASON_FOR_DISSATISFACTION, loginDTO)%>';
        let placeHolder = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_TYPE_DISSATISFACTION_REASON_HERE, loginDTO)%>';
        let confirmButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_YES_DISSATISFIED, loginDTO)%>';
        let cancelButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARD_APPROVAL_MAPPING_CANCEL_BUTTON, loginDTO)%>';
        let emptyReasonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_PLEASE_WRITE_DISSATISFACTION_REASON, loginDTO)%>';

        dialogMessageWithTextBoxWithoutAnimation(msg, placeHolder, confirmButtonText, cancelButtonText, emptyReasonText, (reason) => {

            submitRejectForm(reason, amHouseAllocationRequestId);
        }, () => {
        });
    }

    function submitRejectForm(reason, amHouseAllocationRequestId) {

        console.log('here amOfficeAssignmentRequestId: ' + amHouseAllocationRequestId + " reason: " + reason);
        let rejectionUrl = "Am_house_allocation_requestServlet?actionType=giveFirstLayerRejection" +
            "&amHouseAllocationRequestId=" + amHouseAllocationRequestId;
        $.ajax({
            type: "POST",
            url: rejectionUrl,
            data: {"amHouseAllocationRequestId": amHouseAllocationRequestId, "reject_reason": reason},
            dataType: 'JSON',
            success: function (response) {
                if (response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                    //buttonStateChange(false);
                } else if (response.responseCode === 200) {
                    window.location.replace(getContextPath() + response.msg);
                    //updateApprovalMappingTable();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChange(false);
            }
        });

    }


</script>


			