<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="attendance_device.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%
    Attendance_deviceDTO attendance_deviceDTO;
    attendance_deviceDTO = (Attendance_deviceDTO) request.getAttribute("attendance_deviceDTO");
    String isDuplicate = request.getParameter("duplicate");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (attendance_deviceDTO == null) {
        attendance_deviceDTO = new Attendance_deviceDTO();

    }
    System.out.println("attendance_deviceDTO = " + attendance_deviceDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.ATTENDANCE_DEVICE_ADD_ATTENDANCE_DEVICE_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
%>
<%
    String Language = LM.getText(LC.ATTENDANCE_DEVICE_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i> &nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form"
              action="Attendance_deviceServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body mt-5">
                <input type='hidden' class='form-control' name='id' id='id_hidden_<%=i%>'
                       value='<%=attendance_deviceDTO.id%>' tag='pb_html'/>
                <div class="row">
                    <div class="col-md-10 offset-md-1 ">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_DEVICENAMEEN, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='deviceNameEn_div_<%=i%>'>
                                                    <input type='text' class='form-control' name='deviceNameEn'
                                                           id='deviceNameEn_text_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + attendance_deviceDTO.deviceNameEn + "'"):("'" + "" + "'")%>   tag='pb_html'/>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_DEVICENAMEBN, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='deviceNameBn_div_<%=i%>'>
                                                    <input type='text' class='form-control' name='deviceNameBn'
                                                           id='deviceNameBn_text_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + attendance_deviceDTO.deviceNameBn + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_DEVICECAT, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='deviceCat_div_<%=i%>'>
                                                    <select class='form-control' name='deviceCat'
                                                            id='deviceCat_category_<%=i%>' tag='pb_html'>
                                                        <%=CatRepository.getInstance().buildOptions("device", Language, attendance_deviceDTO.deviceCat)%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_DESCRIPTION, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='description_div_<%=i%>'>
                                                                <textarea class='form-control' name='description'
                                                                          id='description_text_<%=i%>'><%=actionName.equals("edit") ? ("" + attendance_deviceDTO.description + "") : ("")%></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_SYNCHRONIZATIONPROCESSCAT, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='synchronizationProcessCat_div_<%=i%>'>
                                                    <select class='form-control' name='synchronizationProcessCat'
                                                            id='synchronizationProcessCat_category_<%=i%>'
                                                            tag='pb_html'>
                                                        <%=CatRepository.getInstance().buildOptions("synchronization_process", Language, attendance_deviceDTO.synchronizationProcessCat)%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_SYNCHRONIZATIONDURATION, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='synchronizationDuration_div_<%=i%>'>
                                                    <input type='text' class='form-control'
                                                           name='synchronizationDuration'
                                                           id='synchronizationDuration_text_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + attendance_deviceDTO.synchronizationDuration + "'"):("'" + "0" + "'")%>   tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_DEVICEIPADDRESS, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='deviceIpAddress_div_<%=i%>'>
                                                    <input type='text' class='englishAddressValidation form-control'
                                                           name='deviceIpAddress' id='deviceIpAddress_text_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + attendance_deviceDTO.deviceIpAddress + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_FTPIPADDRESS, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='ftpIpAddress_div_<%=i%>'>
                                                    <input type='text' class='englishAddressValidation form-control'
                                                           name='ftpIpAddress' id='ftpIpAddress_text_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + attendance_deviceDTO.ftpIpAddress + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_FTPUSERNAME, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='ftpUsername_div_<%=i%>'>
                                                    <input type='text' class='form-control' name='ftpUsername'
                                                           id='ftpUsername_text_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + attendance_deviceDTO.ftpUsername + "'"):("'" + "" + "'")%>
                                                                   tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-lg-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_FTPPASSWORD, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-lg-9">
                                                <div id='ftpPassword_div_<%=i%>'>
                                                    <input type='text' class='form-control' name='ftpPassword'
                                                           id='ftpPassword_text_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + attendance_deviceDTO.ftpPassword + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>


                                        <input type='hidden' class='form-control' name='insertionDate'
                                               id='insertionDate_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + attendance_deviceDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>


                                        <input type='hidden' class='form-control' name='insertedBy'
                                               id='insertedBy_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + attendance_deviceDTO.insertedBy + "'"):("'" + "" + "'")%> tag='pb_html'/>


                                        <input type='hidden' class='form-control' name='modifiedBy'
                                               id='modifiedBy_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + attendance_deviceDTO.modifiedBy + "'"):("'" + "" + "'")%> tag='pb_html'/>


                                        <input type='hidden' class='form-control' name='isDeleted'
                                               id='isDeleted_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + attendance_deviceDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>


                                        <input type='hidden' class='form-control' name='lastModificationTime'
                                               id='lastModificationTime_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + attendance_deviceDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>


                                        <input type='hidden' class='form-control' name='searchColumn'
                                               id='searchColumn_hidden_<%=i%>'
                                               value=<%=actionName.equals("edit")?("'" + attendance_deviceDTO.searchColumn + "'"):("'" + "" + "'")%> tag='pb_html'/>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-11 text-right">
                        <a class="btn btn-sm cancel-btn text-white shadow" href="<%=request.getHeader("referer")%>">
                            <%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_ATTENDANCE_DEVICE_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button class="btn btn-sm submit-btn text-white shadow ml-2" type="submit">
                            <%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_ATTENDANCE_DEVICE_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


    $(document).ready(function () {

        basicInit();

        if (<%=isDuplicate%> +'' == 'true') {
            showToast("Device already found with same name !", "Device already found with same name !")
        }

        dateTimeInit("<%=Language%>");
        $.validator.addMethod('deviceCatValidator', function (value, element) {
            return value != 0;

        });
        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                deviceNameEn: "required",
                deviceNameBn: "required",
                deviceCat: {
                    required: true,
                    deviceCatValidator: true
                }
            },
            messages: {
                deviceNameEn: '<%=Language.equalsIgnoreCase("English") ? "Kindly enter device name in English!" : "অনুগ্রহ করে ইংরেজিতে ডিভাইসের নাম লিখুন!"%>',
                deviceNameBn: '<%=Language.equalsIgnoreCase("English") ? "Kindly enter device name in Bangla!" : "অনুগ্রহ করে বাংলায় ডিভাইসের নাম লিখুন!"%>',
                deviceCat: '<%=Language.equalsIgnoreCase("English") ? "Kindly select device type!" : "অনুগ্রহ করে ডিভাইস এর ধরন সিলেক্ট করুন!"%>'
            }
        });

    });

    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Attendance_deviceServlet");
    }

    function init(row) {

        $("#deviceCat_category_0").select2({
            dropdownAutoWidth: true,
            theme: "classic"
        });
        $("#synchronizationProcessCat_category_0").select2({
            dropdownAutoWidth: true,
            theme: "classic"
        });


    }

    var row = 0;

    window.onload = function () {
        init(row);
        //CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






