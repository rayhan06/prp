<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="attendance_device.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.ATTENDANCE_DEVICE_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Attendance_deviceDAO attendance_deviceDAO = Attendance_deviceDAO.getInstance();
    Attendance_deviceDTO attendance_deviceDTO = attendance_deviceDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_ATTENDANCE_DEVICE_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="div_border office-div table-responsive">
                <table class="table table-bordered table-striped text-nowrap">
                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_DEVICENAMEEN, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = attendance_deviceDTO.deviceNameEn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_DEVICENAMEBN, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = attendance_deviceDTO.deviceNameBn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_DEVICECAT, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = attendance_deviceDTO.deviceCat + "";
                            %>
                            <%
                                value = CatDAO.getName(Language, "device", attendance_deviceDTO.deviceCat);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_DESCRIPTION, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = attendance_deviceDTO.description + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_SYNCHRONIZATIONPROCESSCAT, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = attendance_deviceDTO.synchronizationProcessCat + "";
                            %>
                            <%
                                value = CatDAO.getName(Language, "synchronization_process", attendance_deviceDTO.synchronizationProcessCat);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_SYNCHRONIZATIONDURATION, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = attendance_deviceDTO.synchronizationDuration + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_DEVICEIPADDRESS, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = attendance_deviceDTO.deviceIpAddress + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_FTPIPADDRESS, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = attendance_deviceDTO.ftpIpAddress + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_FTPUSERNAME, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = attendance_deviceDTO.ftpUsername + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.ATTENDANCE_DEVICE_ADD_FTPPASSWORD, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = attendance_deviceDTO.ftpPassword + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>

                </table>
            </div>
        </div>
    </div>
</div>
