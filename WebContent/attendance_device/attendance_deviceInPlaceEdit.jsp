<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="attendance_device.Attendance_deviceDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Attendance_deviceDTO attendance_deviceDTO = (Attendance_deviceDTO)request.getAttribute("attendance_deviceDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(attendance_deviceDTO == null)
{
	attendance_deviceDTO = new Attendance_deviceDTO();
	
}
System.out.println("attendance_deviceDTO = " + attendance_deviceDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.ATTENDANCE_DEVICE_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>

			
<%=("<td id = '" + i + "_id" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='id' id = 'id_hidden_<%=i%>' value='<%=attendance_deviceDTO.id%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_deviceNameEn'>")%>
			
	
	<div class="form-inline" id = 'deviceNameEn_div_<%=i%>'>
		<input type='text' class='form-control'  name='deviceNameEn' id = 'deviceNameEn_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + attendance_deviceDTO.deviceNameEn + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_deviceNameBn'>")%>
			
	
	<div class="form-inline" id = 'deviceNameBn_div_<%=i%>'>
		<input type='text' class='form-control'  name='deviceNameBn' id = 'deviceNameBn_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + attendance_deviceDTO.deviceNameBn + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_deviceCat'>")%>
			
	
	<div class="form-inline" id = 'deviceCat_div_<%=i%>'>
		<select class='form-control'  name='deviceCat' id = 'deviceCat_category_<%=i%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "device", attendance_deviceDTO.deviceCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "device", -1);			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_description'>")%>
			
	
	<div class="form-inline" id = 'description_div_<%=i%>'>
		<input type='text' class='form-control'  name='description' id = 'description_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + attendance_deviceDTO.description + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_synchronizationProcessCat'>")%>
			
	
	<div class="form-inline" id = 'synchronizationProcessCat_div_<%=i%>'>
		<select class='form-control'  name='synchronizationProcessCat' id = 'synchronizationProcessCat_category_<%=i%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "synchronization_process", attendance_deviceDTO.synchronizationProcessCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "synchronization_process", -1);			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_synchronizationDuration'>")%>
			
	
	<div class="form-inline" id = 'synchronizationDuration_div_<%=i%>'>
		<input type='text' class='form-control'  name='synchronizationDuration' id = 'synchronizationDuration_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + attendance_deviceDTO.synchronizationDuration + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_deviceIpAddress'>")%>
			
	
	<div class="form-inline" id = 'deviceIpAddress_div_<%=i%>'>
		<input type='text' class='form-control'  name='deviceIpAddress' id = 'deviceIpAddress_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + attendance_deviceDTO.deviceIpAddress + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_ftpIpAddress'>")%>
			
	
	<div class="form-inline" id = 'ftpIpAddress_div_<%=i%>'>
		<input type='text' class='form-control'  name='ftpIpAddress' id = 'ftpIpAddress_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + attendance_deviceDTO.ftpIpAddress + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_ftpUsername'>")%>
			
	
	<div class="form-inline" id = 'ftpUsername_div_<%=i%>'>
		<input type='text' class='form-control'  name='ftpUsername' id = 'ftpUsername_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + attendance_deviceDTO.ftpUsername + "'"):("'" + "" + "'")%> <% 
	if(!actionName.equals("edit"))
	{
%>
		required="required"  pattern="^[A-Za-z0-9]{5,}" title="ftpUsername must contain at least 5 letters"
<%
	}
%>
  tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_ftpPassword'>")%>
			
	
	<div class="form-inline" id = 'ftpPassword_div_<%=i%>'>
		<input type='text' class='form-control'  name='ftpPassword' id = 'ftpPassword_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + attendance_deviceDTO.ftpPassword + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=attendance_deviceDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value='<%=attendance_deviceDTO.insertedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=attendance_deviceDTO.modifiedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + attendance_deviceDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=attendance_deviceDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_searchColumn" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=attendance_deviceDTO.searchColumn%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Attendance_deviceServlet?actionType=view&ID=<%=attendance_deviceDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Attendance_deviceServlet?actionType=view&modal=1&ID=<%=attendance_deviceDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	