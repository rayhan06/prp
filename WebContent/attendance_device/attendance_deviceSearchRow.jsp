<%@page pageEncoding="UTF-8" %>

<%@page import="attendance_device.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.ATTENDANCE_DEVICE_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_ATTENDANCE_DEVICE;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Attendance_deviceDTO attendance_deviceDTO = (Attendance_deviceDTO) request.getAttribute("attendance_deviceDTO");
    CommonDTO commonDTO = attendance_deviceDTO;
    String servletName = "Attendance_deviceServlet";


    System.out.println("attendance_deviceDTO = " + attendance_deviceDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Attendance_deviceDAO attendance_deviceDAO = (Attendance_deviceDAO) request.getAttribute("attendance_deviceDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_deviceNameEn'>
    <%
        value = attendance_deviceDTO.deviceNameEn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_deviceNameBn'>
    <%
        value = attendance_deviceDTO.deviceNameBn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_deviceCat'>
    <%
        value = attendance_deviceDTO.deviceCat + "";
    %>
    <%
        value = CatDAO.getName(Language, "device", attendance_deviceDTO.deviceCat);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_description'>
    <%
        value = attendance_deviceDTO.description + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_synchronizationProcessCat'>
    <%
        value = attendance_deviceDTO.synchronizationProcessCat + "";
    %>
    <%
        value = CatDAO.getName(Language, "synchronization_process", attendance_deviceDTO.synchronizationProcessCat);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_synchronizationDuration'>
    <%
        value = attendance_deviceDTO.synchronizationDuration + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_deviceIpAddress'>
    <%
        value = attendance_deviceDTO.deviceIpAddress + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_ftpIpAddress'>
    <%
        value = attendance_deviceDTO.ftpIpAddress + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_ftpUsername'>
    <%
        value = attendance_deviceDTO.ftpUsername + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_ftpPassword'>
    <%
        value = attendance_deviceDTO.ftpPassword + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td>
	<button
			type="button"
			class="btn-sm border-0 shadow bg-light btn-border-radius"
			style="color: #ff6b6b;"
			onclick="location.href='Attendance_deviceServlet?actionType=view&ID=<%=attendance_deviceDTO.id%>'">
		<i class="fa fa-eye"></i>
	</button>
</td>

<td id='<%=i%>_Edit'>
	<button
			class="btn-sm border-0 shadow btn-border-radius text-white"
			style="background-color: #ff6b6b;"
			onclick="location.href='Attendance_deviceServlet?actionType=getEditPage&ID=<%=attendance_deviceDTO.id%>'">
		<i class="fa fa-edit"></i>
	</button>
</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=attendance_deviceDTO.id%>'/></span>
    </div>
</td>
																						
											

