<%@page pageEncoding="UTF-8" %>

<%@page import="ticket_forward_history.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@page import="files.*" %>
<%@page import="employee_records.Employee_recordsRepository"%>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.TICKET_FORWARD_HISTORY_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

    String navigator2 = SessionConstants.NAV_TICKET_FORWARD_HISTORY;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Ticket_forward_historyDTO ticket_forward_historyDTO = (Ticket_forward_historyDTO) request.getAttribute("ticket_forward_historyDTO");
    Ticket_forward_historyDTO prev_ticket_forward_historyDTO = (Ticket_forward_historyDTO) request.getAttribute("prev_ticket_forward_historyDTO");
    CommonDTO commonDTO = ticket_forward_historyDTO;
    String servletName = "Ticket_forward_historyServlet";


    System.out.println("ticket_forward_historyDTO = " + ticket_forward_historyDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Ticket_forward_historyDAO ticket_forward_historyDAO = (Ticket_forward_historyDAO) request.getAttribute("ticket_forward_historyDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    FilesDAO filesDAO = new FilesDAO();

%>


<td id='<%=i%>_supportTicketId'>
    <%
        value = ticket_forward_historyDTO.supportTicketId + "";

    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td>
<%=CatRepository.getName(Language, "assignment_status", ticket_forward_historyDTO.forwardActionCat)%>
</td>

<td id='<%=i%>_forwardFromUserId'>
    <%
        if (ticket_forward_historyDTO.forwardFromOrganogramId != -1) {
            value = WorkflowController.getNameFromOrganogramId(ticket_forward_historyDTO.forwardFromOrganogramId, Language);
        } else {
            value = "";
        }

    %>

    <%=value%>


</td>




<td id='<%=i%>_forwardToUserId'>
    <%
        if (ticket_forward_historyDTO.forwardToOrganogramId != -1) {
            value = WorkflowController.getNameFromOrganogramId(ticket_forward_historyDTO.forwardToOrganogramId, Language);
        } else {
            value = "";
        }

    %>

    <%=value%>
    <%
		UserDTO assignedUserDTO = UserRepository.getUserDTOByOrganogramID(ticket_forward_historyDTO.forwardToOrganogramId);
		if(assignedUserDTO != null && assignedUserDTO.roleID == SessionConstants.SUPPORT_ENGINEER_ROLE)
		{
		%>
		 <br><%=Utils.getDigits(Employee_recordsRepository.getInstance().getPhoneByUserName(assignedUserDTO.userName), Language)%>
		<%
		}
		%>


</td>


<td id='<%=i%>_comments'>
    <%
        value = ticket_forward_historyDTO.comments + "";
    %>

    <%=value%>


</td>


<td id='<%=i%>_receiveTime'>
        <%
											
			String formatted_forwardDate = simpleDateFormat.format(new Date(ticket_forward_historyDTO.forwardDate));
			
			
			%>

        <%=Utils.getDigits(formatted_forwardDate, Language)%>


</td>

<td>
    <%

        List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(ticket_forward_historyDTO.filesDropzone);
        if (FilesDTOList != null && FilesDTOList.size() > 0) {
    %>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><%=LM.getText(LC.HM_FILE_NAME, loginDTO)%>
            </th>

            <th><%=LM.getText(LC.HM_DOWNLOAD, loginDTO)%>
            </th>

        </tr>
        </thead>
        <tbody>

        <%
            if (FilesDTOList != null) {
                for (int j = 0; j < FilesDTOList.size(); j++) {
                    FilesDTO filesDTO = FilesDTOList.get(j);
                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
        %>
        <tr>
            <td>
                <%
                    if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                %>
                <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px'/><br>
                <%
                    }
                %>
                <%=filesDTO.fileTitle%>
            </td>

            <td>
                <a href='Support_ticketServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                   download><i class="fa fa-download" aria-hidden="true"></i>
                </a>
            </td>

        </tr>
        <%
                }
            }
        %>
        </tbody>
    </table>
    <%
        }
    %>
</td>
											
		
											
		
	

											
																						
											

