<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="ticket_forward_history.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.TICKET_FORWARD_HISTORY_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Ticket_forward_historyDAO ticket_forward_historyDAO = new Ticket_forward_historyDAO("ticket_forward_history");
    Ticket_forward_historyDTO ticket_forward_historyDTO = ticket_forward_historyDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<!-- <div class="modal-content viewmodal"> -->
<div class="menubottom">
    <div class="modal-header">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-9 col-sm-12">
                    <h5 class="modal-title">Ticket Forward History Details</h5>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-success app_register"
                               data-id="419637"> Register </a>
                        </div>
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-danger app_reject"
                               data-id="419637"> Reject </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <div class="modal-body container">

        <div class="row div_border office-div">

            <div class="col-md-12">
                <h5 class="table-title">Ticket Forward History</h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.TICKET_FORWARD_HISTORY_ADD_SUPPORTTICKETID, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = ticket_forward_historyDTO.supportTicketId + "";
                                %>

                                <%=value%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.TICKET_FORWARD_HISTORY_ADD_FORWARDFROMUSERID, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = ticket_forward_historyDTO.forwardFromUserId + "";
                                %>

                                <%=value%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.TICKET_FORWARD_HISTORY_ADD_FORWARDTOUSERID, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = ticket_forward_historyDTO.forwardToUserId + "";
                                %>

                                <%=value%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.TICKET_FORWARD_HISTORY_ADD_COMMENTS, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = ticket_forward_historyDTO.comments + "";
                                %>

                                <%=value%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.TICKET_FORWARD_HISTORY_ADD_FORWARDDATE, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = ticket_forward_historyDTO.forwardDate + "";
                                %>
                                <%
                                    String formatted_forwardDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=formatted_forwardDate%>


                            </td>

                        </tr>


                    </table>
                </div>
            </div>


        </div>


    </div>