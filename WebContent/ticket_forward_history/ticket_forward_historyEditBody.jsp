<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="ticket_forward_history.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>

<%
    Ticket_forward_historyDTO ticket_forward_historyDTO;
    ticket_forward_historyDTO = (Ticket_forward_historyDTO) request.getAttribute("ticket_forward_historyDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (ticket_forward_historyDTO == null) {
        ticket_forward_historyDTO = new Ticket_forward_historyDTO();

    }
    System.out.println("ticket_forward_historyDTO = " + ticket_forward_historyDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.TICKET_FORWARD_HISTORY_ADD_TICKET_FORWARD_HISTORY_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
%>


<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%>
        </h3>
    </div>
    <div class="box-body">
        <form class="form-horizontal"
              action="Ticket_forward_historyServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="form-body">


                <%@ page import="java.text.SimpleDateFormat" %>
                <%@ page import="java.util.Date" %>

                <%@ page import="pb.*" %>

                <%
                    String Language = LM.getText(LC.TICKET_FORWARD_HISTORY_EDIT_LANGUAGE, loginDTO);
                    String Options;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    Date date = new Date();
                    String datestr = dateFormat.format(date);
                %>


                <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                       value='<%=ticket_forward_historyDTO.iD%>' tag='pb_html'/>


                <input type='hidden' class='form-control' name='supportTicketId' id='supportTicketId_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + ticket_forward_historyDTO.supportTicketId + "'"):("'" + "0" + "'")%> tag='pb_html'/>


                <input type='hidden' class='form-control' name='forwardFromUserId' id='forwardFromUserId_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + ticket_forward_historyDTO.forwardFromUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>


                <input type='hidden' class='form-control' name='forwardToUserId' id='forwardToUserId_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + ticket_forward_historyDTO.forwardToUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>


                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.TICKET_FORWARD_HISTORY_ADD_COMMENTS, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='comments_div_<%=i%>'>
                        <input type='text' class='form-control' name='comments' id='comments_text_<%=i%>'
                               value=<%=actionName.equals("edit")?("'" + ticket_forward_historyDTO.comments + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                    </div>
                </div>


                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.TICKET_FORWARD_HISTORY_ADD_FORWARDDATE, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='forwardDate_div_<%=i%>'>
                        <input type='text' class='form-control formRequired datepicker' readonly="readonly"
                               data-label="Document Date" id='forwardDate_date_<%=i%>' name='forwardDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_forwardDate = dateFormat.format(new Date(ticket_forward_historyDTO.forwardDate));
	%>
                                       '<%=formatted_forwardDate%>'
                        <%
                        } else {
                        %>
                        '<%=datestr%>'
                        <%
                            }
                        %>
                        tag='pb_html'>
                    </div>
                </div>


                <input type='hidden' class='form-control' name='insertedByUserId' id='insertedByUserId_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + ticket_forward_historyDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>


                <input type='hidden' class='form-control' name='insertionDate' id='insertionDate_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + ticket_forward_historyDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>


                <input type='hidden' class='form-control' name='modifiedBy' id='modifiedBy_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + ticket_forward_historyDTO.modifiedBy + "'"):("'" + "" + "'")%> tag='pb_html'/>


                <input type='hidden' class='form-control' name='isDeleted' id='isDeleted_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + ticket_forward_historyDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>


                <input type='hidden' class='form-control' name='lastModificationTime'
                       id='lastModificationTime_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + ticket_forward_historyDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>


                <div class="form-actions text-center">
                    <a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
                        <%=LM.getText(LC.TICKET_FORWARD_HISTORY_ADD_TICKET_FORWARD_HISTORY_CANCEL_BUTTON, loginDTO)%>
                    </a>
                    <button class="btn btn-success" type="submit">

                        <%=LM.getText(LC.TICKET_FORWARD_HISTORY_ADD_TICKET_FORWARD_HISTORY_SUBMIT_BUTTON, loginDTO)%>

                    </button>
                </div>

            </div>

        </form>

    </div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


    $(document).ready(function () {

        basicInit();
    });

    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Ticket_forward_historyServlet");
    }

    function init(row) {


    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






