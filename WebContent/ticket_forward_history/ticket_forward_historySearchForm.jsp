
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="ticket_forward_history.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}

String value = "";
String Language = LM.getText(LC.TICKET_FORWARD_HISTORY_EDIT_LANGUAGE, loginDTO);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


Ticket_forward_historyDAO ticket_forward_historyDAO = (Ticket_forward_historyDAO)request.getAttribute("ticket_forward_historyDAO");

long supportTicketId = (long) request.getAttribute("supportTicketId");

String navigator2 = SessionConstants.NAV_TICKET_FORWARD_HISTORY;
System.out.println("navigator2 = " + navigator2);
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
System.out.println("rn2 = " + rn2);
String pageno2 = ( rn2 == null ) ? "1" : "" + rn2.getCurrentPageNo();
String totalpage2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalPages();
String totalRecords2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalRecords();
String lastSearchTime = ( rn2 == null ) ? "0" : "" + rn2.getSearchTime();
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

String successMessageForwarded = "Forwarded to your Senior Office";
String successMessageApproved = "Approval Done";

String ajax = request.getParameter("ajax");
boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>	

<%

if(hasAjax == false)
{
	Enumeration<String> parameterNames = request.getParameterNames();

	while (parameterNames.hasMoreElements()) 
	{

		String paramName = parameterNames.nextElement();
	   
		if(!paramName.equalsIgnoreCase("actionType"))
		{
			String[] paramValues = request.getParameterValues(paramName);
			for (int i = 0; i < paramValues.length; i++) 
			{
				String paramValue = paramValues[i];
				
				%>
				
				<%
				
			}
		}
	   

	}
}

%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped text-nowrap">
						<thead>
							<tr>
								<th><%=LM.getText(LC.SUPPORT_TICKET_ADD_ID, loginDTO)%></th>
								<th>Action</th>
								<th><%=LM.getText(LC.TICKET_FORWARD_HISTORY_ADD_FORWARDFROMUSERID, loginDTO)%></th>
								<th><%=LM.getText(LC.TICKET_FORWARD_HISTORY_ADD_FORWARDTOUSERID, loginDTO)%></th>
								<th><%=LM.getText(LC.TICKET_FORWARD_HISTORY_ADD_COMMENTS, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_DATE, loginDTO)%></th>

								<th><%=LM.getText(LC.HM_ATTACHMENTS, loginDTO)%></th>
									
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_TICKET_FORWARD_HISTORY);

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Ticket_forward_historyDTO ticket_forward_historyDTO = (Ticket_forward_historyDTO) data.get(i);
											if(supportTicketId != -1000 && ticket_forward_historyDTO.supportTicketId != supportTicketId)
												continue;
											Ticket_forward_historyDTO prev_ticket_forward_historyDTO = (Ticket_forward_historyDTO) data.get(i);
											for(int j=i;j<size;j++) {
												Ticket_forward_historyDTO temp = (Ticket_forward_historyDTO) data.get(j);
												if(ticket_forward_historyDTO.forwardFromUserId == temp.forwardToUserId) {
													prev_ticket_forward_historyDTO = (Ticket_forward_historyDTO) data.get(j);
													break;
												}
											}
											
											%>
											<tr id = 'tr_<%=i%>'>
											<%
											
								%>
											
		
								<%  								
								    request.setAttribute("ticket_forward_historyDTO",ticket_forward_historyDTO);
								    request.setAttribute("prev_ticket_forward_historyDTO",prev_ticket_forward_historyDTO);
								%>  
								
								 <jsp:include page="./ticket_forward_historySearchRow.jsp">
								 		<jsp:param name="pageName" value="searchrow" />
								 		<jsp:param name="rownum" value="<%=i%>" />
								 </jsp:include>			

								
								<%

											%>
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			