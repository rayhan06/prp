<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%@page import="employee_offices.*" %>
<%@ page import="pb.*" %>
<%@page import="workflow.WorkflowController"%>
<%@ page import="java.util.*" %>
<%@page contentType="text/html;charset=utf-8" %>
<%

    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.THERAPY_REPORT_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>
<div class="row mx-2">
    <div class="col-md-12">
        <%@include file="../pbreport/yearmonth.jsp" %>
    </div>
    <div class="col-md-12">
        <%@include file="../pbreport/calendar.jsp" %>
    </div>
    <div class="search-criteria-div col-md-12">
        
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.HM_REFERENCE_EMPLOYEE, loginDTO)%>
            </label>
            <div class="col-md-9">
                <button type="button" class="btn btn-block submit-btn btn-border-radius text-white"
                        onclick="addEmployee()"
                        id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
                </button>
                <table class="table table-bordered table-striped">
                    <tbody id="employeeToSet"></tbody>
                </table>
                <input class='form-control' type='hidden' name='userName' id='userName' value=''/>
            </div>
        </div>
    </div>
    <div class="search-criteria-div col-md-12">
	        <div class="form-group row">
	            <label class="col-md-3 col-form-label text-md-right">
	                <%=LM.getText(LC.HM_USER_NAME, loginDTO)%>
	            </label>
	            <div class="col-md-9">              
	                <input class='form-control' type='text' name='userNameRaw' id='userNameRaw' value='' onKeyUp="setEngUserName(this.value, 'userName')"/>
	            </div>
	        </div>
	    </div>
    <div  class="search-criteria-div col-md-12">
			<div class="form-group row">
				<label class="col-md-3 col-form-label text-md-right">
					<%=Language.equalsIgnoreCase("english")?"Therapist":"থেরাপিদাদাতা"%>
				</label>
				<div class="col-md-9">
					<select  class='form-control' name='nurse_organogram_id'
                          id='nurse_organogram_id' 
                          tag='pb_html' >
                          <option value=""><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
                          <%
                          Set<Long> emps = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.PHYSIOTHERAPIST_ROLE);
                          for(Long em: emps)
                          {
                          %>
                          <option value = "<%=em%>">
                          <%=WorkflowController.getNameFromOrganogramId(em, Language)%>
                          </option>
                          <%
                          }
                          %>
                    </select>							
				</div>
			</div>
		</div>
    <div class="search-criteria-div col-md-12">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.THERAPY_REPORT_WHERE_THERAPYCAT, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='therapyCat' id='therapyCat'>
                    <%
                        Options = CatDAO.getOptions(Language, "therapy_treatment", CatDTO.CATDEFAULT);
                    %>
                    <%=Options%>
                </select>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function init() {
    	dateTimeInit($("#Language").val());
        $("#search_by_date").prop('checked', true);
        $("#search_by_date").trigger("change");
        setDateByStringAndId('startDate_js', '<%=datestr%>');
        setDateByStringAndId('endDate_js', '<%=datestr%>');
        add1WithEnd = false;
        processNewCalendarDateAndSubmit();
    }

    function PreprocessBeforeSubmiting() {
    }
    
    function patient_inputted(userName, orgId) {
        console.log("patient_inputted " + userName);
        $("#userName").val(userName);
        $("#userNameRaw").val(userName);
    }
</script>