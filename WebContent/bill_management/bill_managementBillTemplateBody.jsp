<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="bill_management.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@ page import="util.StringUtils" %>
<%@ page import="java.math.BigDecimal" %>
<%@ page import="java.util.stream.IntStream" %>
<%@ page import="java.util.stream.Stream" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="static java.util.stream.Collectors.joining" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>
<%@ page import="budget_mapping.Budget_mappingDTO" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupDTO" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="budget_operation.Budget_operationRepository" %>
<%@ page import="static util.StringUtils.convertToBanNumber" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberInWord" %>
<%@ page import="static util.StringUtils.convertBanglaIfLanguageIsBangla" %>
<%@ page import="budget.BudgetUtils" %>
<%@ page import="budget.BudgetInfo" %>
<%@ page import="supplier_institution.Supplier_institutionRepository" %>


<%
    String servletName = "Bill_managementServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Bill_managementDTO bill_managementDTO = (Bill_managementDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    Budget_mappingDTO budgetMappingDTO = Budget_mappingRepository.getInstance().getById(bill_managementDTO.budgetMappingId);
    String formattedBillAmount = BangladeshiNumberFormatter.getFormattedNumber(
            convertToBanNumber(String.valueOf(bill_managementDTO.billAmount))
    );

    String formattedSupplierTotalBill = BangladeshiNumberFormatter.getFormattedNumber(convertToBanNumber(
            String.format("%d", bill_managementDTO.supplierTotalBill)
    ));

    String dueVatText = Bill_managementDTO.getDueAmountBanglaText(
            bill_managementDTO.getVatAmount(),
            bill_managementDTO.dueVatAmount,
            bill_managementDTO.dueVatDescription
    );

    String dueTaxText = Bill_managementDTO.getDueAmountBanglaText(
            bill_managementDTO.getTaxAmount(),
            bill_managementDTO.dueTaxAmount,
            bill_managementDTO.dueTaxDescription
    );

    String dueServiceChargeText = Bill_managementDTO.getDueAmountBanglaText(
            bill_managementDTO.getServiceChargeAmount(),
            bill_managementDTO.dueServiceChargeAmount,
            bill_managementDTO.dueServiceChargeDescription
    );
%>

<%@include file="../pb/viewInitializer.jsp" %>

<%
    String dots = Stream.generate(() -> ".").limit(1000).collect(joining());
    long deductionAmount, dueAmount, runningTotal = bill_managementDTO.billAmount;
%>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    .symbol-taka-amount {
        display: flex;
        justify-content: space-between;
    }

    .full-border {
        border: 2px solid black;
        padding: 5px;
    }

    .fix-fill {
        overflow: hidden;
        white-space: nowrap;
    }

    #to-print-div * {
        font-size: 12px;
    }

    #to-print-div h1 {
        font-size: 16px;
        font-weight: bold;
    }

    #to-print-div h2 {
        font-size: 14px;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .4in;
        background: white;
        margin-bottom: 10px;
    }

    .foot-note {
        font-size: 11px !important;
    }

    .blank-to-fill {
        display: inline-block;
        border-bottom: 1px dotted black;
        width: inherit;
    }

    .table-bordered > :not(caption) > * {
        border-color: black;
    }

    .table-bordered > :not(caption) > * > * {
        border-color: black;
    }

    .table-bordered td {
        padding: 5px;
    }

    .no-top-border {
        border-top-color: white !important;
    }

    .no-bottom-border {
        border-bottom-color: white !important;
    }

    span.tab {
        display: inline-block;
        width: 5ch;
    }
</style>

<div class="kt-content" id="kt_content">
    <div class="row">
        <div class="kt-portlet">
            <div class="kt-portlet__body page-bg" id="bill-div">
                <div class="kt-subheader__main ml-4">
                    <label class="h2 kt-subheader__title" style="color: #00a1d4;">
                        ক্রয়, সরবরাহ ও সেবা বাবদ ব্যয়ের বিল
                    </label>
                </div>
                <hr style="border-top: 1px solid rgba(0, 0, 0, 0.1); margin: 10px 10px">

                <div class="ml-auto m-3">
                    <button type="button" class="btn" id='download-pdf'
                            onclick="downloadTemplateAsPdf('to-print-div', 'Forwarding ' + '<%=bill_managementDTO.budgetRegisterId%>');">
                        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                </div>

                <div style="margin: auto;">
                    <div class="container" id="to-print-div">
                        <section class="page shadow" data-size="A4">
                            <div>
                                টি. আর ফর্ম নং ২১<br>
                                (এস.আর ১৮৩ দ্রষ্টব্য)
                            </div>

                            <div class="text-center">
                                <h1>বাংলাদেশ জাতীয় সংসদ সচিবালয়</h1>
                                <h2>ক্রয়, সরবরাহ ও সেবা বাবদ ব্যায়ের বিল</h2>

                                <div class="mt-3">
                                    <span class="full-border">কোড নং</span>
                                    <span class="full-border" style="margin-left : 0;">১০২</span>
                                    &nbsp;-&nbsp;<span class="full-border">
                                        <%=Budget_institutional_groupRepository.getInstance().getCode(
                                                budgetMappingDTO.budgetInstitutionalGroupId,
                                                "Bangla"
                                        )%>
                                      </span>
                                    &nbsp;-&nbsp;<span class="full-border">
                                        <%=Budget_officeRepository.getInstance().getCode(
                                                budgetMappingDTO.budgetOfficeId,
                                                "Bangla"
                                        )%>
                                      </span>
                                    &nbsp;-&nbsp;<span class="full-border">
                                            <%=Budget_operationRepository.getInstance().getCode(
                                                    budgetMappingDTO.budgetOperationId,
                                                    "Bangla"
                                            )%>
                                      </span>
                                    &nbsp;-&nbsp;<span class="full-border">
                                            <%=Economic_sub_codeRepository.getInstance().getCode(
                                                    bill_managementDTO.economicSubCodeId,
                                                    "Bangla"
                                            )%>
                                      </span>
                                </div>
                            </div>

                            <div>
                                <div class="mt-5">
                                    <div class="row">
                                        <div class="col-3 fix-fill">
                                            টোকেন নং
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="col-3 fix-fill">
                                            তারিখ
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="col-3 fix-fill">
                                            ভাউচার নং
                                            <div class="blank-to-fill">
                                                &nbsp;&nbsp;<%=StringUtils.convertToBanNumber(String.format("%d", bill_managementDTO.budgetRegisterId))%>
                                            </div>
                                        </div>
                                        <div class="col-3 fix-fill">
                                            তারিখ
                                            <div class="blank-to-fill">
                                                <%=StringUtils.getFormattedDate(Language, bill_managementDTO.insertionTime)%>
                                            </div>
                                        </div>
                                    </div>

                                    <table class="table-bordered mt-2">
                                        <thead class="text-center">
                                        <tr>
                                            <td width="20%" rowspan="2">অর্থনৈতিক কোড</td>
                                            <td width="55%" rowspan="2">সরবরাহকৃত দ্রব্যের বিবরণ</td>
                                            <td width="25%" colspan="2">পরিমাণ</td>
                                        </tr>
                                        <tr>
                                            <td width="70%">টাকা</td>
                                            <td width="30%">পয়সা</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td style="width: 20%; vertical-align: top;" rowspan="100%">
                                                <%=Economic_sub_codeRepository.getInstance().getText(
                                                        "Bangla",
                                                        bill_managementDTO.economicSubCodeId
                                                )%><br><br>
                                                <%=dueVatText%><br><br>
                                                <%=dueTaxText%><br><br>
                                                <%=dueServiceChargeText%>
                                            </td>
                                            <td class="text-center no-top-border no-bottom-border"
                                                style="text-align: center;">
                                                <div>
                                                    <div class="row">
                                                        <div class="col-5 fix-fill">
                                                            নং
                                                            <div class="blank-to-fill">
                                                                <%=bill_managementDTO.officeLetterNumber%>
                                                            </div>
                                                        </div>
                                                        <div class="col-5 fix-fill">
                                                            তাং
                                                            <div class="blank-to-fill">
                                                                <%=StringUtils.getFormattedDate(Language, bill_managementDTO.letterDate)%>
                                                            </div>
                                                        </div>
                                                        <div class="col-2 text">
                                                            খ্রি.
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-right no-top-border no-bottom-border"></td>
                                            <td class="text-right no-top-border no-bottom-border"></td>
                                        </tr>
                                        <tr>
                                            <td class="no-top-border no-bottom-border">
                                                <%=bill_managementDTO.suppliedItemDescription%> বাবদ
                                                <span style="font-weight: bold;">"<%=Supplier_institutionRepository.getInstance().getIssuedToText(bill_managementDTO.supplierInstitutionId, Language)%>"</span>
                                                এর অনুকূলে চেক দেয়
                                            </td>
                                            <td class="text-right no-top-border no-bottom-border">
                                                <%=formattedBillAmount%>/-
                                            </td>
                                            <td class="no-top-border no-bottom-border"></td>
                                        </tr>
                                        <%
                                            if (bill_managementDTO.hasVat()) {
                                                dueAmount = Math.max(0L, bill_managementDTO.dueVatAmount);
                                                deductionAmount =
                                                        dueAmount
                                                        + Bill_managementDTO.getPercentageOfAmount(
                                                                bill_managementDTO.billAmount, bill_managementDTO.vatPercent
                                                        );
                                        %>
                                        <tr>
                                            <td class="no-top-border no-bottom-border">
                                                <%=convertToBanNumber(String.format("%.2f", bill_managementDTO.vatPercent))%>
                                                % ভ্যাট=
                                            </td>
                                            <td class="text-right no-top-border">
                                                <div class="symbol-taka-amount">
                                                    <div class="font-weight-bold">
                                                        (-)
                                                    </div>
                                                    <div>
                                                        <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                                convertToBanNumber(String.valueOf(deductionAmount))
                                                        )%>/-
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="no-top-border no-bottom-border"></td>
                                        </tr>
                                        <tr>
                                            <td class="no-top-border no-bottom-border"></td>
                                            <td class="no-top-border no-bottom-border">
                                                <div class="symbol-taka-amount">
                                                    <div class="font-weight-bold">
                                                        =
                                                    </div>
                                                    <div>
                                                        <%
                                                            runningTotal -= deductionAmount;
                                                        %>
                                                        <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                                convertToBanNumber(String.valueOf(runningTotal))
                                                        )%>/-
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="no-top-border no-bottom-border"></td>
                                        </tr>
                                        <%}%>

                                        <%
                                            if (bill_managementDTO.hasTax()) {
                                                dueAmount = Math.max(0L, bill_managementDTO.dueTaxAmount);
                                                deductionAmount =
                                                        dueAmount
                                                        + Bill_managementDTO.getPercentageOfAmount(
                                                                bill_managementDTO.billAmount, bill_managementDTO.taxPercent
                                                        );
                                        %>
                                        <tr>
                                            <td class="no-top-border no-bottom-border">
                                                <%=convertToBanNumber(String.format("%.2f", bill_managementDTO.taxPercent))%>
                                                % কর=
                                            </td>
                                            <td class="no-top-border">
                                                <div class="symbol-taka-amount">
                                                    <div class="font-weight-bold">
                                                        (-)
                                                    </div>
                                                    <div>
                                                        <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                                convertToBanNumber(String.valueOf(deductionAmount))
                                                        )%>/-
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="no-top-border no-bottom-border"></td>
                                        </tr>
                                        <tr>
                                            <td class="no-top-border no-bottom-border"></td>
                                            <td class="no-top-border no-bottom-border">
                                                <div class="symbol-taka-amount">
                                                    <div class="font-weight-bold">
                                                        =
                                                    </div>
                                                    <div>
                                                        <%
                                                            runningTotal -= deductionAmount;
                                                        %>
                                                        <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                                convertToBanNumber(String.valueOf(runningTotal))
                                                        )%>/-
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="no-top-border no-bottom-border"></td>
                                        </tr>
                                        <%}%>

                                        <%
                                            if (bill_managementDTO.hasServiceCharge()) {
                                                dueAmount = Math.max(0L, bill_managementDTO.dueServiceChargeAmount);
                                                deductionAmount =
                                                        dueAmount
                                                        + Bill_managementDTO.getPercentageOfAmount(
                                                                bill_managementDTO.billAmount, bill_managementDTO.serviceChargePercent
                                                        );
                                        %>
                                        <tr>
                                            <td class="no-top-border no-bottom-border">
                                                <%=convertToBanNumber(String.format("%.2f", bill_managementDTO.serviceChargePercent))%>
                                                % সার্ভিস চার্জ=
                                            </td>
                                            <td class="no-top-border">
                                                <div class="symbol-taka-amount">
                                                    <div class="font-weight-bold">
                                                        (-)
                                                    </div>
                                                    <div>
                                                        <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                                convertToBanNumber(String.valueOf(deductionAmount))
                                                        )%>/-
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="no-top-border no-bottom-border"></td>
                                        </tr>
                                        <tr>
                                            <td class="no-top-border no-bottom-border"></td>
                                            <td class="no-top-border no-bottom-border">
                                                <div class="symbol-taka-amount">
                                                    <div class="font-weight-bold">
                                                        =
                                                    </div>
                                                    <div>
                                                        <%
                                                            runningTotal -= deductionAmount;
                                                        %>
                                                        <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                                convertToBanNumber(String.valueOf(runningTotal))
                                                        )%>/-
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="no-top-border no-bottom-border"></td>
                                        </tr>
                                        <%}%>

                                        <tr>
                                            <td class="no-top-border">প্রত্যয়ন করা যাইতেছে যে, মঞ্জুরীপত্র ও বিল ভাউচার
                                                সংযুক্ত।
                                            </td>
                                            <td class="no-top-border"></td>
                                            <td class="no-top-border"></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <div class="symbol-taka-amount">
                                                    <div class="font-weight-bold">
                                                        =
                                                    </div>
                                                    <div>
                                                        <%
                                                            String netTotal = convertToBanNumber(String.valueOf(runningTotal));
                                                        %>
                                                        <%=BangladeshiNumberFormatter.getFormattedNumber(netTotal)%>/-
                                                    </div>
                                                </div>
                                            </td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="text-right mt-4 fix-fill">
                                কথায়=&nbsp;&nbsp;
                                <div class="blank-to-fill">
                                    <%
                                        String netTotalInWord = BangladeshiNumberInWord.convertToWord(netTotal);
                                    %>
                                    <%=netTotalInWord%>
                                    টাকা (মাত্র)
                                </div>
                            </div>

                            <div class="mt-2">
                                <p>
                                    ১। প্রত্যায়ন করা যাইতেছে যে, জনস্বার্থে এই ব্যয় অপরিহার্য ছিল। আমি আরও প্রত্যায়ন
                                    করিতেছি যে, আমার
                                    জ্ঞান
                                    ও বিশ্বাস মতে, এই বিলে উল্লিখিত প্রদত্ত অর্থ নিম্নবর্ণিত ক্ষেত্র ব্যতীত প্রকৃত
                                    প্রাপককে প্রদান করা
                                    হইয়াছে,
                                    তবে স্থায়ী অগ্রিমের টাকা অপেক্ষা দায় বেশী হওয়ায়, অবশিষ্ট পাওনা এই বিলে দাবিকৃত টাকা
                                    প্রাপ্তির পর
                                    প্রদান করা হইবে। আমি যথাসম্ভব সকল ভাউচার গ্রহণ করিয়াছি এবং সেগুলি এমনভাবে বাতিল করা
                                    হইয়াছে যেন
                                    পুনরায় ব্যবহার
                                    করা না যায়। ২৫ টাকার উর্ধ্বের সকল ভাউচারসমূহ এমনভাবে সংরক্ষণ করা হইয়াছে, যেন
                                    প্রয়োজনমত ৩ বৎসরের
                                    মধ্যে
                                    এইগুলি পেশ করা যায়। সকল পূর্তকর্মের বিল সঙ্গে সংযুক্ত করা হইল।
                                </p>


                                <p>
                                    ২। প্রত্যায়ন করা যাইতেছে যে, যে সকল দ্রব্যের জন্য স্টোর একাউন্টস সংরক্ষণ করা হয় সে
                                    সব দ্রব্যাদি স্টক
                                    রেজিস্টারে অর্ন্তভূক্ত করা হইয়াছে।
                                </p>

                                <p>
                                    ৩। প্রত্যায়ন করা যাইতেছে যে, যে সব দ্রব্যাদি ক্রয়ের বিল পেশ করা হইয়াছে, সে সব
                                    দ্রব্যের পরিমাণ সঠিক,
                                    গুণগতমান
                                    ভাল, যে দরে ক্রয় করা হইয়াছে, তাহা বাজার দরের অধিক নহে এবং কার্যাদেশ বা চালান
                                    (ইনভয়েস) এর যথাস্থানে
                                    লিপিবদ্ধ
                                    করা হইয়াছে। যাহাতে একই দ্রব্যের জন্য দ্বিতীয় (ডুপ্লিকেট) অর্থ প্রদান এড়ান যায়।
                                </p>

                                <p>
                                    ৪। প্রত্যায়ন করা যাইতেছে যে- <br>
                                    (ক) এই বিলে দাবিকৃত পরিবহন ভাড়া প্রকৃতপক্ষে দেওয়া হইয়াছে এবং ইহা অপরিহার্য ছিল এবং
                                    ভাড়ার হার প্রচলিত
                                    যানবাহন
                                    ভাড়ার হারের মধ্যেই; এবং <br>

                                    (খ) সংশ্লিষ্ট সরকারী কর্মচারী সাধারণ বিধিবলে এই ভ্রমণের জন্য ভ্রমণ ব্যয় প্রাপ্য হন
                                    না, এবং এর
                                    অতিরিক্ত
                                    কোন
                                    বিশেষ পারিশ্রমিক, এই দায়িত্ব পালনের জন্য প্রাপ্য হইবেন না।
                                </p>
                            </div>
                        </section>

                        <section class="page shadow" data-size="A4">
                            <div class="text-center">
                                <h2>বাংলাদেশ জাতীয় সংসদ সচিবালয়</h2>
                            </div>

                            <div class="mt-3">
                                <p>
                                    ৫। প্রত্যায়ন করা যাইতেছে যে, যে সকল অধঃস্তন কর্মচারীর বেতন বিলে দাবী করা হইয়াছে
                                    তাহারা ঐ সময়ে
                                    প্রকৃতই
                                    সরকারী
                                    কাজে নিয়োজিত ছিলেন (এস, আর, ১৭১)।
                                </p>

                                <p>
                                    ৬। প্রত্যায়ন করা যাইতেছে যে- <br>

                                    (ক) মনোহারী দ্রব্য বা স্ট্যাম্প বাবত ২০ টাকার অধিক কোন ক্রয় স্থানীয়ভাবে করা হয় নাই।
                                    <br>

                                    (খ) ব্যক্তিগত কাজে ব্যবহৃত তাঁবু বহনের কোন খরচ এই বিলে অন্তর্ভূক্ত করা হয় নাই। <br>

                                    (গ) আবাসিক ভবনে ব্যবহৃত কোন বিদ্যুৎ বাবদ খরচ এই বিলে অন্তর্ভূক্ত করা হয় নাই। <br>

                                    (ঘ) এই বৎসরে প্রসেস সার্ভারদের প্রদত্ত পারিতোষিক টাকা
                                    .................................... (যা গত ৩
                                    বৎসরের
                                    জরিমানা বাবদ প্রাপ্তির গড় টাকার সামান্য অধিক হইবে না)। <br>
                                </p>

                                <p>
                                    ৭। যাহার নামে চেক ইস্যু করা হইবে (প্রযোজ্য ক্ষেত্রে) <span
                                        style="font-weight: bold;">"<%=Supplier_institutionRepository.getInstance().getIssuedToText(bill_managementDTO.supplierInstitutionId, Language)%>"</span>,
                                    এর অনুকুলে চেক প্রদেয়।
                                </p>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div style="margin: 0 20px;">
                                        <div>
                                            *নিয়ন্ত্রণকারী/প্রতিস্বাক্ষরকারী কর্মকর্তার স্বাক্ষর
                                        </div>
                                        <div class="fix-fill mt-4 w-100">
                                            &nbsp;
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            নাম
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            পদবী
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-5 w-100">
                                            সীল
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            স্থান
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            তারিখ
                                            <div class="blank-to-fill"></div>
                                        </div>
                                    </div>
                                    <table class="table-bordered mt-2">
                                        <thead>
                                        <tr>
                                            <td style="width: 55%;">বরাদ্দের হিসাব</td>
                                            <td style="width: 45%;">টাকা</td>
                                            <td style="width: 5%;">প.</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>১। শেষ বিলের টাকার অংক</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="fix-fill">
                                                ২। এ যাবত অতিরিক্ত বরাদ্দ<br><br>
                                                (পত্র নং............................................)
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        convertBanglaIfLanguageIsBangla(Language, String.valueOf(bill_managementDTO.allocatedBudget))
                                                )%>/-
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="fix-fill">
                                                ৩। এ যাবত যে অংকের বরাদ্দ কমানো হয়েছে<br><br>
                                                (পত্র নং............................................)
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                ৪। নীট মোট<br>
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-6">
                                    <div style="margin: 0 20px;">
                                        <div class="fix-fill w-100">
                                            বুঝিয়া পাইয়াছি
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-4 w-100">
                                            আয়ন কর্মকর্তার স্বাক্ষর
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            নাম
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            পদবী
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-5 w-100">
                                            সীল
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            স্থান
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            তারিখ
                                            <div class="blank-to-fill"></div>
                                        </div>
                                    </div>

                                    <table class="table-bordered mt-2">
                                        <thead>
                                        <tr>
                                            <td style="width: 53%;">বরাদ্দের হিসাব</td>
                                            <td style="width: 37%;">টাকা</td>
                                            <td style="width: 8%;">প.</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>গত বিলের মোট জের (+)</td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        convertBanglaIfLanguageIsBangla(Language, String.valueOf(bill_managementDTO.uptoLastBillTotal))
                                                )%>/-
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                এই বিলের মোট (+)<br><br><br>
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        convertBanglaIfLanguageIsBangla(Language, String.valueOf(bill_managementDTO.billAmount))
                                                )%>/-
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                সংযুক্ত ---- পূর্তকর্মের বিলের টাকা <br><br><br>
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                মোট (পরবর্তী বিলে জের টেনে নেয়া হবে)
                                            </td>
                                            <td class="text-right">
                                                <%
                                                    long toBeCarried = bill_managementDTO.billAmount + bill_managementDTO.uptoLastBillTotal;
                                                %>
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        convertBanglaIfLanguageIsBangla(Language, String.valueOf(toBeCarried))
                                                )%>/-
                                            </td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="mt-3" style="border-top: 5px solid black;">
                                <div class="text-center mt-2">
                                    <h2>হিসাবরক্ষণ অফিসে ব্যবহারের জন্য</h2>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-4 fix-fill">
                                        প্রদানের জন্য পাস করা হল টাকা&nbsp;&nbsp;
                                        <div class="blank-to-fill">
                                            <%=BangladeshiNumberFormatter.getFormattedNumber(netTotal)%>/-
                                        </div>
                                    </div>
                                    <div class="col-8 fix-fill">
                                        কথায়&nbsp;&nbsp;
                                        <div class="blank-to-fill">
                                            <%=netTotalInWord%> টাকা (মাত্র)
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-4">
                                        <div>
                                            <strong>অডিটর (স্বাক্ষর)</strong>
                                        </div>
                                        <div class="fix-fill mt-3">
                                            নাম<%=dots%>
                                        </div>
                                        <div class="fix-fill mt-3">
                                            তাং<%=dots%>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div>
                                            <strong>সুপার (স্বাক্ষর)</strong>
                                        </div>
                                        <div class="fix-fill mt-3">
                                            নাম<%=dots%>
                                        </div>
                                        <div class="fix-fill mt-3">
                                            তাং<%=dots%>
                                        </div>
                                        <div class="fix-fill mt-4">
                                            চেক নং<%=dots%>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div>
                                            <strong>হিসাবরক্ষণ অফিসার (স্বাক্ষর)</strong>
                                        </div>
                                        <div class="fix-fill mt-3">
                                            নাম<%=dots%>
                                        </div>
                                        <div class="fix-fill mt-3">
                                            তাং<%=dots%>
                                        </div>
                                        <div class="fix-fill mt-4">
                                            তারিখ<%=dots%>
                                        </div>
                                        <div class="fix-fill mt-4">
                                            চেক প্রদানকারীর স্বাক্ষর
                                        </div>
                                        <div class="fix-fill mt-4">
                                            নাম<%=dots%>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mt-3 foot-note" style="border-top: 1px solid black;">
                                <div>
                                    * কেবল মাত্র প্রযোজ্য ক্ষেত্রে
                                </div>

                                <div class="mt-2">
                                    বি. দ্র. - ইহা স্পষ্ঠভাবে স্মরণ রাখিতে হইবে যে, বরাদ্দের অতিরিক্ত ব্যয়ের জন্য আয়ন
                                    কর্মকর্তা
                                    ব্যক্তিগতভাবে দায়ী থাকিবেন। বরাদ্দের অতিরিক্ত ব্যয়ের বিপরীতে যদি তিনি অতিরিক্ত
                                    বরাদ্দ মঞ্জুর করাইতে
                                    না পারেন, তবে অতিরিক্ত ব্যয়িত অর্থ তাহার ব্যক্তিগত ভাতাদি হইতে আদায় করা হইবে।
                                </div>

                                <div class="mt-1">
                                    বা.নি.মু. ডন-১০/২০১০-১১/৫,০০,০০০ কপি,মুঃ আঃ নং-৮১/০৯-১০ তাং ২৯/৬/২০১০ইং।
                                </div>
                            </div>
                        </section>

                        <%--VAT CERTIFICATE--%>
                        <%if (bill_managementDTO.hasVat()) {%>
                        <section class="page shadow" data-size="A4" style="line-height: 2;">
                            <div class="text-center">
                                <div>
                                    [মূসক-৬.৬]
                                </div>
                                <div class="mt-2">
                                    গণপ্রজাতন্ত্রী বাংলাদেশ সরকার<br>
                                    জাতীয় রাজস্ব বোর্ড, ঢাকা।
                                </div>
                                <div class="mt-4">
                                    <span style="border-bottom: 1px solid black;font-weight: bold;">
                                        উৎসে কর কর্তন সনদপত্র
                                    </span><br>
                                    [ বিধি ৪০ এর উপ-বিধি (১) এর দফা (চ) দ্রষ্টব্য ]
                                </div>
                            </div>

                            <div class="row mt-4">
                                <div class="col-12 row">
                                    <div class="col-4">
                                        উৎসে কর কর্তনকারী সত্তার নাম:
                                    </div>
                                    <div class="col-8">
                                        বাংলাদেশ জাতীয় সংসদ সচিবালয়।
                                    </div>
                                </div>
                                <div class="col-12 row">
                                    <div class="col-4">
                                        উৎসে কর কর্তনকারী সত্তার ঠিকানা:
                                    </div>
                                    <div class="col-8">
                                        সংসদ ভবন, শেরে বাংলা নগর, ঢাকা-১২০৭।
                                    </div>
                                </div>
                                <div class="col-12 row">
                                    <div class="col-4">
                                        উৎসে কর কর্তনকারী সত্তার বিআইএন:
                                    </div>
                                    <div class="col-8">
                                        (প্রযোজ্য নয়)
                                    </div>
                                </div>
                                <div class="col-12 row mt-4">
                                    <div class="col-4">
                                        উৎসে কর কর্তন সনদপত্র নং:
                                    </div>
                                    <div class="col-8 text-right">
                                        জারির
                                        তারিখ: <%=StringUtils.getFormattedDate("Bangla", bill_managementDTO.insertionTime)%>
                                    </div>
                                </div>
                            </div>

                            <p class="mt-5">
                                এই মর্মে প্রত্যয়ন করা যাইতেসে যে, আইনের ধারা ৪৯ অনুযায়ী উৎসে কর কর্তনযোগ্য সরবরাহ হইতে
                                প্রযোজ্য মূল্য সংযোজন কর বাবদ
                                উৎসে কর কর্তন করা হইল। কর্তনকৃত মূল্য সংযোজন করের অর্থ বুক ট্রান্সফার/ট্রেজারি
                                চালান/দাখিল্পত্রে বৃদ্ধিকারী সমন্বয়ের মাধ্যমে সরকারি
                                কোষাগারে জমা প্রদান করা হইয়াছে। কপি এতদসংগে সংযুক্ত করা হইল (প্রযোজ্য নয়)।
                            </p>

                            <table class="table-bordered mt-4 w-100">
                                <thead class="text-center" style="background: #ddd;">
                                <tr>
                                    <td rowspan="2" style="width: 5%;">ক্রমিক নং</td>
                                    <td colspan="2" style="width: 40%;">সরবরাহকারী</td>
                                    <td colspan="2" style="width: 10%;">সংশ্লিষ্ট কর চালান পত্র</td>
                                    <td rowspan="2" style="width: 20%;">মোট সরবরাহমূল্য (টাকা)</td>
                                    <td rowspan="2" style="width: 12%;">মূসকের পরিমাণ (টাকা)</td>
                                    <td rowspan="2" style="width: 13%;">উৎসে কর্তনকৃত মূসকের পরিমাণ (টাকা)</td>
                                </tr>
                                <tr>
                                    <td style="width: 32%;">নাম</td>
                                    <td style="width: 8%;">বিআইএন</td>
                                    <td style="width: 5%;">নম্বর</td>
                                    <td style="width: 5%;">ইস্যুর তারিখ</td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>১.</td>
                                    <td>
                                        <%=Supplier_institutionRepository.getInstance().getIssuedToText(bill_managementDTO.supplierInstitutionId, Language)%>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-center">
                                        <%=formattedBillAmount%>/-
                                    </td>
                                    <td class="text-center">
                                        <%=convertToBanNumber(String.format("%.2f", bill_managementDTO.vatPercent))%>%
                                    </td>
                                    <td class="text-center">
                                        <%=BangladeshiNumberFormatter.getFormattedNumber(convertToBanNumber(String.format(
                                                "%d",
                                                Bill_managementDTO.getPercentageOfAmount(bill_managementDTO.billAmount, bill_managementDTO.vatPercent)
                                        )))%>/-
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                            <div class="row" style="margin-top: 10rem;">
                                <div class="col-4 offset-8 text-center">
                                    সহকারী সচিব (অর্থ-১)<br>
                                    বাংলাদেশ জাতীয় সংসদ সচিবালয়
                                </div>
                            </div>
                        </section>
                        <%}%>

                        <%--TAX CERTIFICATE--%>
                        <%if (bill_managementDTO.hasTax()) {%>
                        <section class="page shadow" data-size="A4" style="line-height: 2;">
                            <div class="text-center">
                                বাংলাদেশ জাতীয় সংসদ সচিবালয়<br>
                                <span style="border-bottom: 1px solid black;">
                                    অর্থ শাখা-১
                                </span><br>
                            </div>

                            <div class="text-center mt-4">
                                <span style="border-bottom: 1px solid black;">
                                    কর কর্তনের প্রত্যয়ন পত্র
                                </span>
                            </div>

                            <p class="mt-5">
                                <span class="tab"></span> প্রত্যয়ন করা যাচ্ছে যে
                                <%=Supplier_institutionRepository.getInstance().getIssuedToText(bill_managementDTO.supplierInstitutionId, Language)%>&nbsp;
                                ঠিকাদার/সরবরাহকারী প্রতিষ্ঠানের বর্তমান <%=formattedBillAmount%>/- টাকার বিল হতে
                                <%=convertToBanNumber(String.format("%.2f", bill_managementDTO.taxPercent))%>%
                                হারে কর বাবদ <%=BangladeshiNumberFormatter.getFormattedNumber(convertToBanNumber(
                                    String.format(
                                            "%d",
                                            Bill_managementDTO.getPercentageOfAmount(bill_managementDTO.billAmount, bill_managementDTO.taxPercent)
                                    )
                            ))%>/- টাকা কর্তন করা হয়েছে। <%=dueTaxText.isEmpty() ? "" : "(" + dueTaxText + ")"%>
                                বর্তমান <%=BudgetUtils.getEconomicYearText(bill_managementDTO.insertionTime, "Bangla")%>
                                অর্থ বছরে অত্র সচিবালয় হতে এ প্রতিষ্ঠানকে প্রদানকৃত মোট <%=formattedSupplierTotalBill%>
                                /- টাকার
                                বিলের উপর দেয়া মোট কর হতে ইতোপূর্বে কর্তনকৃত কর বাদ দিয়ে অবশিষ্ট কর বর্তমান বিল হতে
                                কর্তন করা হয়েছে।
                            </p>

                            <div class="row" style="margin-top: 8rem;">
                                <div class="col-4 offset-8 text-center">
                                    (আয়ন ও ব্যয়ন কর্মকর্তা)
                                </div>
                            </div>

                            <p class="mt-5">
                                <span class="tab">২।</span> .......................... তারিখের
                                ...........................
                                নম্বর টোকেন ও উপরোল্লিখিত কর কর্তন করা হয়েছে।
                            </p>

                            <div class="row" style="margin-top: 8rem;">
                                <div class="col-4 offset-8 text-center">
                                    নিরীক্ষা ও হিসাবরক্ষণ কর্মকর্তা<br>
                                    বাংলাদেশ জাতীয় সংসদ সচিবালয়<br>
                                    সেগুনবাগিচা, ঢাকা
                                </div>
                            </div>
                        </section>
                        <%}%>

                        <%--ServiceCharge CERTIFICATE--%>
                        <%if (bill_managementDTO.hasServiceCharge()) {%>
                        <section class="page shadow" data-size="A4" style="line-height: 2;">
                            <div class="text-center">
                                বাংলাদেশ জাতীয় সংসদ সচিবালয়<br>
                                <span style="border-bottom: 1px solid black;">
                                    অর্থ শাখা-১
                                </span><br>
                            </div>

                            <div class="text-center mt-4">
                                <span style="border-bottom: 1px solid black;">
                                    সার্ভিস চার্জ কর্তনের প্রত্যয়ন পত্র
                                </span>
                            </div>

                            <p class="mt-5">
                                <span class="tab"></span> প্রত্যয়ন করা যাচ্ছে
                                যে <%=Supplier_institutionRepository.getInstance().getIssuedToText(bill_managementDTO.supplierInstitutionId, Language)%>
                                ,&nbsp;
                                ঠিকাদার/সরবরাহকারী প্রতিষ্ঠানের বর্তমান <%=formattedBillAmount%>/- টাকার বিল হতে&nbsp;
                                <%=convertToBanNumber(String.format("%.2f", bill_managementDTO.serviceChargePercent))%>%&nbsp;
                                হারে সার্ভিস চার্জ
                                বাবদ <%=BangladeshiNumberFormatter.getFormattedNumber(convertToBanNumber(
                                    String.format(
                                            "%d",
                                            Bill_managementDTO.getPercentageOfAmount(bill_managementDTO.billAmount, bill_managementDTO.serviceChargePercent)
                                    )
                            ))%>/- টাকা কর্তন করা হয়েছে। <%=dueServiceChargeText.isEmpty() ? "" : "(" + dueServiceChargeText + ")"%>
                                বর্তমান <%=BudgetUtils.getEconomicYearText(bill_managementDTO.insertionTime, "Bangla")%>
                                অর্থ বছরে অত্র সচিবালয় হতে এ প্রতিষ্ঠানকে প্রদানকৃত মোট <%=formattedSupplierTotalBill%>
                                /- টাকার বিলের উপর
                                দেয়া মোট সার্ভিস চার্জ হতে ইতোপূর্বে কর্তনকৃত সার্ভিস চার্জ বাদ দিয়ে অবশিষ্ট সার্ভিস
                                চার্জ বর্তমান বিল হতে কর্তন করা হয়েছে।
                            </p>

                            <div class="row" style="margin-top: 8rem;">
                                <div class="col-4 offset-8 text-center">
                                    (আয়ন ও ব্যয়ন কর্মকর্তা)
                                </div>
                            </div>

                            <p class="mt-5">
                                <span class="tab">২।</span> .......................... তারিখের
                                ...........................
                                নম্বর টোকেন ও উপরোল্লিখিত সার্ভিস চার্জ কর্তন করা হয়েছে।
                            </p>

                            <div class="row" style="margin-top: 8rem;">
                                <div class="col-4 offset-8 text-center">
                                    নিরীক্ষা ও হিসাবরক্ষণ কর্মকর্তা<br>
                                    বাংলাদেশ জাতীয় সংসদ সচিবালয়<br>
                                    সেগুনবাগিচা, ঢাকা
                                </div>
                            </div>
                        </section>
                        <%}%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function downloadTemplateAsPdf(divId, fileName) {
        let content = document.getElementById(divId);
        const opt = {
            margin: 0.5,
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4'}
        };
        html2pdf().from(content).set(opt).save();
    }
</script>