<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="bill_management.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="economic_operation_mapping.Economic_operation_mappingRepository" %>
<%@ page import="economic_code.Economic_codeRepository" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="budget.BudgetCategoryEnum" %>
<%@ page import="supplier_institution.Supplier_institutionRepository" %>

<%
    Bill_managementDTO bill_managementDTO;
    bill_managementDTO = (Bill_managementDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    CommonDTO commonDTO = bill_managementDTO;
    if (bill_managementDTO == null) {
        bill_managementDTO = new Bill_managementDTO();
    }
    String tableName = "bill_management";
%>

<%@include file="../pb/addInitializer.jsp" %>

<%
    actionName = request.getParameter("actionType").equals("edit") ? "edit" : "add";
    String formTitle = LM.getText(LC.BILL_MANAGEMENT_ADD_BILL_MANAGEMENT_ADD_FORMNAME, loginDTO);
    String servletName = "Bill_managementServlet";
    boolean isEditPage = "edit".equalsIgnoreCase(actionName);
    String context = request.getContextPath() + "/";
%>

<style>
    .required {
        font-size: 1.5rem;
        color: red;
    }

    input[readonly],
    textarea[readonly] {
        background-color: rgba(231, 231, 231, .5) !important;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=bill_managementDTO.iD%>'>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="officeLetterNumber">
                                            <%=LM.getText(LC.BILL_MANAGEMENT_ADD_OFFICELETTERNUMBER, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='officeLetterNumber'
                                                   id='officeLetterNumber'
                                                   value='<%=bill_managementDTO.officeLetterNumber%>'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.BILL_MANAGEMENT_ADD_LETTERDATE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="letterDate_js"/>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                            </jsp:include>
                                            <input type='hidden' name='letterDate' id='letterDate'
                                                   value='<%=bill_managementDTO.letterDate == SessionConstants.MIN_DATE
                                                             ? "" : dateFormat.format(new Date(bill_managementDTO.letterDate))%>'
                                            >
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="budgetInstitutionalGroup">
                                            <%=LM.getText(LC.BUDGET_INSTITUTIONAL_GROUP, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <select id="budgetInstitutionalGroup"
                                                    name='budgetInstitutionalGroupId'
                                                    class='form-control rounded shadow-sm'
                                                    onchange="institutionalGroupChanged(this);"
                                            >
                                                <%=Budget_institutional_groupRepository.getInstance().buildOptions(
                                                        Language, bill_managementDTO.budgetInstitutionalGroupId, true
                                                )%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="budgetOffice">
                                            <%=LM.getText(LC.BUDGET_OFFICE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <select id="budgetOffice" name='budgetOfficeId'
                                                    class='form-control rounded shadow-sm'
                                                    onchange="budgetOfficeChanged(this);">
                                                <%--Dynamically Added with AJAX--%>
                                                <%if (isEditPage) {%>
                                                <%=Budget_mappingRepository.getInstance().buildBudgetOffice(
                                                        Language,
                                                        bill_managementDTO.budgetInstitutionalGroupId,
                                                        BudgetCategoryEnum.OPERATIONAL.getValue(),
                                                        bill_managementDTO.budgetOfficeId,
                                                        true
                                                )%>
                                                <%}%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="budgetMapping">
                                            <%=LM.getText(LC.BUDGET_OPERATION_CODE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <select id="budgetMapping" name='budgetMappingId'
                                                    class='form-control rounded shadow-sm'
                                                    onchange="budgetMappingChanged(this);">
                                                <%--Dynamically Added with AJAX--%>
                                                <%if (isEditPage) {%>
                                                <%=Budget_mappingRepository.getInstance().buildOperationDropDown(
                                                        Language,
                                                        bill_managementDTO.budgetInstitutionalGroupId,
                                                        BudgetCategoryEnum.OPERATIONAL.getValue(),
                                                        bill_managementDTO.budgetOfficeId,
                                                        bill_managementDTO.budgetMappingId,
                                                        true
                                                )%>
                                                <%}%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="economic-group-select">
                                            <%=LM.getText(LC.BUDGET_ECONOMIC_GROUP, userDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control rounded shadow-sm'
                                                    id='economic-group-select'
                                                    name='economicGroupId'
                                                    onchange="economicGroupChanged(this);">
                                                <%--Dynamically added with ajax--%>
                                                <%if (isEditPage) {%>
                                                <%=Economic_operation_mappingRepository.getInstance().buildEconomicGroupOption(
                                                        Language,
                                                        bill_managementDTO.budgetMappingId,
                                                        bill_managementDTO.economicGroupId,
                                                        Bill_managementServlet.selectedEconomicGroups
                                                )%>
                                                <%}%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="economic-code-select">
                                            <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, userDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control rounded shadow-sm'
                                                    id='economic-code-select'
                                                    name='economicCodeId'
                                                    onchange="economicCodeChanged(this);">
                                                <%--Dynamically added with ajax--%>
                                                <%if (isEditPage) {%>
                                                <%=Economic_codeRepository.getInstance().buildEconomicCodeOptionFromEconomicGroup(
                                                        Language,
                                                        bill_managementDTO.economicGroupId,
                                                        bill_managementDTO.economicCodeId
                                                )%>
                                                <%}%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="economic-sub-code-select">
                                            <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, userDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control rounded shadow-sm'
                                                    id='economic-sub-code-select'
                                                    name='economicSubCodeId'
                                                    onchange="economicSubCodeChanged(this);">
                                                <%--Dynamically added with ajax--%>
                                                <%if (isEditPage) {%>
                                                <%=Economic_sub_codeRepository.getInstance().buildOption(
                                                        Language,
                                                        bill_managementDTO.economicCodeId,
                                                        bill_managementDTO.economicSubCodeId
                                                )%>
                                                <%}%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="uptoLastBillTotal">
                                            <%=LM.getText(LC.BILL_MANAGEMENT_ADD_BILLRUNNINGTOTAL, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                value = "";
                                                if (bill_managementDTO.uptoLastBillTotal != -1)
                                                    value = bill_managementDTO.uptoLastBillTotal + "";
                                            %>
                                            <input type='text' class='form-control' name='uptoLastBillTotal' readonly
                                                   id='uptoLastBillTotal'
                                                   value='<%=value%>'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="allocatedBudget">
                                            <%=LM.getText(LC.BILL_MANAGEMENT_ADD_ALLOCATEDBUDGET, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                value = "";
                                                if (bill_managementDTO.allocatedBudget != -1)
                                                    value = bill_managementDTO.allocatedBudget + "";
                                            %>
                                            <input type='text' class='form-control' name='allocatedBudget' readonly
                                                   id='allocatedBudget'
                                                   value='<%=value%>'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="remainingBudget">
                                            <%=UtilCharacter.getDataByLanguage(Language, "অবশিষ্ট", "Remaining")%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                value = "" + Bill_managementDTO.getRemainingBudget(
                                                        bill_managementDTO.allocatedBudget,
                                                        bill_managementDTO.uptoLastBillTotal
                                                );
                                            %>
                                            <input type='text' class='form-control' readonly id='remainingBudget'
                                                   value='<%=value%>'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="billAmount_text">
                                            <%=LM.getText(LC.BILL_MANAGEMENT_ADD_BILLAMOUNT, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                value = "";
                                                if (bill_managementDTO.billAmount != -1) {
                                                    value = bill_managementDTO.billAmount + "";
                                                }
                                            %>
                                            <input type='text' class='form-control' name='billAmount'
                                                   data-only-integer="true"
                                                   id='billAmount_text'
                                                   value='<%=value%>'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="vatPercent_number">
                                            <%=LM.getText(LC.BILL_MANAGEMENT_ADD_VATPERCENT, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                value = "";
                                                if (bill_managementDTO.vatPercent != -1) {
                                                    value = bill_managementDTO.vatPercent + "";
                                                }
                                            %>
                                            <input type='text' class='form-control' name='vatPercent'
                                                   data-only-decimal="true"
                                                   id='vatPercent_number' value='<%=value%>'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="taxPercent_number">
                                            <%=LM.getText(LC.BILL_MANAGEMENT_ADD_TAXPERCENT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                value = "";
                                                if (bill_managementDTO.taxPercent != -1) {
                                                    value = bill_managementDTO.taxPercent + "";
                                                }
                                            %>
                                            <input type='text' class='form-control' name='taxPercent'
                                                   id='taxPercent_number'
                                                   data-only-decimal="true" value='<%=value%>'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="serviceChargePercent">
                                            <%=LM.getText(LC.BILL_MANAGEMENT_ADD_SERVICECHARGEPERCENT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                value = "";
                                                if (bill_managementDTO.serviceChargePercent != -1) {
                                                    value = bill_managementDTO.serviceChargePercent + "";
                                                }
                                            %>
                                            <input type='text' class='form-control' name='serviceChargePercent'
                                                   id='serviceChargePercent'
                                                   data-only-decimal="true" value='<%=value%>'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="dueBillAmount">
                                            <%=LM.getText(LC.BILL_MANAGEMENT_ADD_DUEBILLAMOUNT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                value = "";
                                                if (bill_managementDTO.dueBillAmount != -1)
                                                    value = bill_managementDTO.dueBillAmount + "";
                                            %>
                                            <input type='text' class='form-control' name='dueBillAmount'
                                                   id='dueBillAmount'
                                                   data-only-integer="true" value='<%=value%>'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="dueBillDescription">
                                            <%=LM.getText(LC.BILL_MANAGEMENT_ADD_DUEBILLDESCRIPTION, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <textarea class='form-control' name='dueBillDescription'
                                                      id='dueBillDescription'
                                                      rows="5" maxlength="1024"
                                                      style="resize: none;"><%=bill_managementDTO.dueBillDescription%></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="dueVatAmount">
                                            <%=UtilCharacter.getDataByLanguage(Language, "বকেয়া ভ্যাটের পরিমাণ", "Due Vat Amount")%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                value = "";
                                                if (bill_managementDTO.dueVatAmount != -1)
                                                    value = bill_managementDTO.dueVatAmount + "";
                                            %>
                                            <input type='text' class='form-control' name='dueVatAmount'
                                                   id='dueVatAmount'
                                                   data-only-integer="true" value='<%=value%>'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="dueVatDescription">
                                            <%=UtilCharacter.getDataByLanguage(Language, "বকেয়া ভ্যাটের বিবরণ", "Due Vat Description")%>
                                        </label>
                                        <div class="col-md-8">
                                            <textarea class='form-control' name='dueVatDescription'
                                                      id='dueVatDescription'
                                                      rows="5" maxlength="1024"
                                                      style="resize: none;"><%=bill_managementDTO.dueVatDescription%></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="dueTaxAmount">
                                            <%=UtilCharacter.getDataByLanguage(Language, "বকেয়া করের পরিমাণ", "Due Tax Amount")%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                value = "";
                                                if (bill_managementDTO.dueTaxAmount != -1)
                                                    value = bill_managementDTO.dueTaxAmount + "";
                                            %>
                                            <input type='text' class='form-control' name='dueTaxAmount'
                                                   id='dueTaxAmount'
                                                   data-only-integer="true" value='<%=value%>'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="dueTaxDescription">
                                            <%=UtilCharacter.getDataByLanguage(Language, "বকেয়া করের বিবরণ", "Due Tax Description")%>
                                        </label>
                                        <div class="col-md-8">
                                            <textarea class='form-control' name='dueTaxDescription'
                                                      id='dueTaxDescription'
                                                      rows="5" maxlength="1024"
                                                      style="resize: none;"><%=bill_managementDTO.dueTaxDescription%></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="dueServiceChargeAmount">
                                            <%=UtilCharacter.getDataByLanguage(Language, "বকেয়া সার্ভিস চার্জের পরিমাণ", "Due Service Charge Amount")%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                value = "";
                                                if (bill_managementDTO.dueServiceChargeAmount != -1)
                                                    value = bill_managementDTO.dueServiceChargeAmount + "";
                                            %>
                                            <input type='text' class='form-control' name='dueServiceChargeAmount'
                                                   id='dueServiceChargeAmount'
                                                   data-only-integer="true" value='<%=value%>'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="dueServiceChargeDescription">
                                            <%=UtilCharacter.getDataByLanguage(Language, "বকেয়া সার্ভিস চার্জের বিবরণ", "Due Service Charge Description")%>
                                        </label>
                                        <div class="col-md-8">
                                            <textarea class='form-control' name='dueServiceChargeDescription'
                                                      id='dueServiceChargeDescription'
                                                      rows="5" maxlength="1024"
                                                      style="resize: none;"><%=bill_managementDTO.dueServiceChargeDescription%></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="supplierInstitutionId">
                                            <%=LM.getText(LC.BILL_MANAGEMENT_ADD_ISSUEDTONAME, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control rounded shadow-sm' id='supplierInstitutionId'
                                                    name='supplierInstitutionId'>
                                                <%=Supplier_institutionRepository.getInstance().buildOptions(
                                                        Language,
                                                        bill_managementDTO.supplierInstitutionId
                                                )%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="suppliedItemDescription">
                                            <%=LM.getText(LC.BILL_MANAGEMENT_ADD_SUPPLIEDITEMDESCRIPTION, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <textarea class='form-control' name='suppliedItemDescription'
                                                      id='suppliedItemDescription'
                                                      rows="7" maxlength="4096"
                                                      style="resize: none;"><%=bill_managementDTO.suppliedItemDescription%></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.BILL_MANAGEMENT_ADD_FILEDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                fileColumnName = "fileDropzone";
                                                if (actionName.equals("edit")) {
                                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(bill_managementDTO.fileDropzone);
                                            %>
                                            <%@include file="../pb/dropzoneEditor.jsp" %>
                                            <%
                                                } else {
                                                    ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                    bill_managementDTO.fileDropzone = ColumnID;
                                                }
                                            %>
                                            <div class="dropzone"
                                                 action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=bill_managementDTO.fileDropzone%>">
                                                <input type='file' style="display:none" name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value='' tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=bill_managementDTO.fileDropzone%>'/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-md-11">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                    type="button" onclick="location.href='<%=request.getHeader("referer")%>'">
                                <%=LM.getText(LC.BILL_MANAGEMENT_ADD_BILL_MANAGEMENT_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="preview-btn"
                                    class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                                    type="button" onclick="submitForm('preview')">
                                <%=getDataByLanguage(Language, "প্রিভিউ", "Preview")%>
                            </button>
                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="button" onclick="submitForm('submit')">
                                <%=LM.getText(LC.BILL_MANAGEMENT_ADD_BILL_MANAGEMENT_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script type="text/javascript">
    function clearSelects(startIndex) {
        const selectIds = [
            'budgetOffice', 'budgetMapping',
            'economic-group-select', 'economic-code-select',
            'economic-sub-code-select'
        ];
        for (let i = startIndex; i < selectIds.length; i++) {
            const selectElement = document.getElementById(selectIds[i]);
            selectElement.innerHTML = '';
        }
    }

    async function institutionalGroupChanged(selectElement) {
        const selectedInstitutionalGroupId = selectElement.value;
        clearSelects(0);
        if (selectedInstitutionalGroupId === '') return;

        const url = 'Budget_mappingServlet?actionType=getBudgetOfficeList&withCode=true'
                    + '&budget_instituitional_group_id=' + selectedInstitutionalGroupId
                    + '&budget_cat=' + '<%=BudgetCategoryEnum.OPERATIONAL.getValue()%>';

        const response = await fetch(url);
        document.getElementById('budgetOffice').innerHTML = await response.text();
    }

    async function budgetOfficeChanged(selectElement) {
        const selectedBudgetOffice = selectElement.value;
        clearSelects(1);
        if (selectedBudgetOffice === '') return;

        const selectedInstitutionalGroup = document.getElementById('budgetInstitutionalGroup').value;

        const url = 'Budget_mappingServlet?actionType=getOperationCodeList&withCode=true'
                    + '&budget_instituitional_group_id=' + selectedInstitutionalGroup
                    + '&budget_office_id=' + selectedBudgetOffice
                    + '&budget_cat=' + '<%=BudgetCategoryEnum.OPERATIONAL.getValue()%>';
        const response = await fetch(url);
        document.getElementById('budgetMapping').innerHTML = await response.text();
    }

    async function budgetMappingChanged(selectElement) {
        const selectedBudgetOperationId = selectElement.value;
        clearSelects(2);
        if (selectedBudgetOperationId === '') return;

        const url = 'Bill_managementServlet?actionType=ajax_buildEconomicGroup&budget_operation_id=' + selectedBudgetOperationId;
        const data = await fetch(url);
        document.getElementById('economic-group-select').innerHTML = await data.text();
    }

    async function economicGroupChanged(selectElement) {
        const selectedEconomicGroup = selectElement.value;
        clearSelects(3);
        if (selectedEconomicGroup === '') return;

        const url = 'Budget_mappingServlet?actionType=buildEconomicCode&economic_group_type=' + selectedEconomicGroup;
        const data = await fetch(url);
        document.getElementById('economic-code-select').innerHTML = await data.text();
    }

    async function economicCodeChanged(selectElement) {
        const selectedEconomicCode = selectElement.value;
        clearSelects(4);
        if (selectedEconomicCode === '') return;

        const url = 'Budget_mappingServlet?actionType=getEconomicSubCodeOption'
                    + '&economicCode=' + selectedEconomicCode;
        const response = await fetch(url);
        document.getElementById('economic-sub-code-select').innerHTML = await response.text();
    }

    async function economicSubCodeChanged(selectElement) {
        document.getElementById('uptoLastBillTotal').value = '';
        document.getElementById('allocatedBudget').value = '';

        const selectedEconomicSubCode = selectElement.value;
        if (selectedEconomicSubCode === '') return;

        const budgetMappingId = document.getElementById('budgetMapping').value;

        const url = 'Bill_managementServlet?actionType=ajax_getDataForSubCode'
                    + '&budgetMappingId=' + budgetMappingId
                    + '&economicSubCodeId=' + selectedEconomicSubCode
                    + '&budgetRegisterId=' + '<%=bill_managementDTO.budgetRegisterId%>'
                    + '&letterDate=' + getDateStringById('letterDate_js');
        const response = await fetch(url);

        const data = await response.json();
        document.getElementById('uptoLastBillTotal').value = data.uptoLastBillTotal;
        document.getElementById('uptoLastBillTotal').readonly = true;
        document.getElementById('allocatedBudget').value = data.allocatedBudget;
        document.getElementById('allocatedBudget').readonly = true;
        document.getElementById('remainingBudget').value = data.remainingBudget;
        document.getElementById('remainingBudget').readonly = true;
    }

    function isFormValid() {
        $('#letterDate').val(getDateStringById('letterDate_js'));

        let dateErrorMessage = {
            'errorEn': '<%=LM.getInstance().getText("English", LC.EMPLOYEE_ACR_ADD_ENTER_VALID_DATE)%>',
            'errorBn': '<%=LM.getInstance().getText("Bangla", LC.EMPLOYEE_ACR_ADD_ENTER_VALID_DATE)%>'
        };
        let isDateValid = dateValidator('letterDate_js', true, dateErrorMessage);
        return form.valid() && isDateValid;
    }

    function init() {
        select2SingleSelector('#supplierInstitutionId', '<%=Language%>');
        <%if(isEditPage){%>
        setDateByStringAndId('letterDate_js', $('#letterDate').val());
        <%}%>
    }

    const form = $('#bigform');

    function typeOnlyInteger(e) {
        return true === inputValidationForIntValue(e, $(this));
    }

    function typeOnlyDecimal(e) {
        return true === inputValidationForFloatValue(e, $(this), 4);
    }

    $(document).ready(function () {
        init();
        document.querySelectorAll('[data-only-integer="true"]')
                .forEach(inputField => inputField.onkeydown = typeOnlyInteger);

        document.querySelectorAll('[data-only-decimal="true"]')
                .forEach(inputField => inputField.onkeydown = typeOnlyDecimal);

        $.validator.addMethod('textValidator', function (value, element) {
            return value.trim() !== '';
        });

        form.validate({
            rules: {
                officeLetterNumber: {
                    required: true,
                    textValidator: true
                },
                budgetInstitutionalGroupId: "required",
                budgetOfficeId: "required",
                budgetMappingId: "required",
                economicGroupId: "required",
                economicCodeId: "required",
                economicSubCodeId: "required",
                vatPercent: {
                    required: true,
                    min: 0
                },
                billAmount: {
                    required: true,
                    min: 1
                },
                supplierInstitutionId: "required"
            },
            messages: {
                officeLetterNumber: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                budgetInstitutionalGroupId: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                budgetOfficeId: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                budgetMappingId: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                economicGroupId: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                economicCodeId: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                economicSubCodeId: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                vatPercent: '<%=getDataByLanguage(Language, "ভ্যাট(%) দিন (প্রযোজ্য না হলে 0 দিন)", "Enter VAT(%) (If not applicable neter 0)")%>',
                billAmount: '<%=getDataByLanguage(Language, "সঠিক সংখ্যা দিন", "Enter valid number")%>',
                supplierInstitutionId: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>'
            }
        });
    });

    function submitForm(source) {
        if (isFormValid()) {
            submitAjaxByData(form.serialize(),"Bill_managementServlet?actionType=ajax_<%=actionName%>&source=" + source);
        }
    }

    function buttonStateChange(value) {
        $(':button').prop('disabled', value);
    }
</script>