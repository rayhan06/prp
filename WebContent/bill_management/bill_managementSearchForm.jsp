<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="bill_management.*" %>
<%@ page import="util.*" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoRepository" %>
<%@ page import="util.StringUtils" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@ page import="supplier_institution.Supplier_institutionRepository" %>


<%
    String navigator2 = "navBILL_MANAGEMENT";
    String servletName = "Bill_managementServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.BUDGET_ECONOMIC_YEAR, loginDTO)%></th>
            <th><%=LM.getText(LC.BILL_MANAGEMENT_ADD_VOUCHERNUMBER, loginDTO)%></th>
            <th><%=LM.getText(LC.BILL_MANAGEMENT_ADD_VOUCHERDATE, loginDTO)%></th>
            <th><%=LM.getText(LC.BILL_MANAGEMENT_ADD_OFFICELETTERNUMBER, loginDTO)%></th>
            <th><%=LM.getText(LC.BILL_MANAGEMENT_ADD_BUDGETMAPPINGID, loginDTO)%></th>
            <th><%=LM.getText(LC.BILL_MANAGEMENT_ADD_BUDGETOFFICEID, loginDTO)%></th>
            <th><%=LM.getText(LC.BILL_MANAGEMENT_ADD_ECONOMICSUBCODEID, loginDTO)%></th>
            <th><%=LM.getText(LC.BILL_MANAGEMENT_ADD_BILLAMOUNT, loginDTO)%></th>
            <th><%=LM.getText(LC.BILL_MANAGEMENT_ADD_ISSUEDTONAME, loginDTO)%></th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>
            <th><%=LM.getText(LC.BILL_MANAGEMENT_SEARCH_BILL_MANAGEMENT_EDIT_BUTTON, loginDTO)%></th>
            <th class="">
                <div class="text-center">
                    <span>All</span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Bill_managementDTO> data = (List<Bill_managementDTO>) recordNavigator.list;
            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (Bill_managementDTO bill_managementDTO : data) {
        %>
                        <tr>
                            <td>
                                <%=BudgetSelectionInfoRepository.getInstance().getEconomicYearById(Language, bill_managementDTO.budgetSelectionInfoId)%>
                            </td>
                            <td>
                                <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.format("%d", bill_managementDTO.budgetRegisterId))%>
                            </td>
                            <td>
                                <%=StringUtils.getFormattedDate(Language, bill_managementDTO.insertionTime)%>
                            </td>
                            <td>
                                <%=StringUtils.convertBanglaIfLanguageIsBangla(
                                        Language,
                                        String.valueOf(bill_managementDTO.officeLetterNumber)
                                )%>
                            </td>
                            <td>
                                <%=Budget_mappingRepository.getInstance().getOperationText(Language, bill_managementDTO.budgetMappingId)%>
                            </td>
                            <td>
                                <%=Budget_officeRepository.getInstance().getText(bill_managementDTO.budgetOfficeId, Language)%>
                            </td>
                            <td>
                                <%=Economic_sub_codeRepository.getInstance().getText(Language, bill_managementDTO.economicSubCodeId)%>
                            </td>
                            <td>
                                <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(bill_managementDTO.billAmount))%>
                            </td>
                            <td>
                                <%=Supplier_institutionRepository.getInstance().getIssuedToText(bill_managementDTO.supplierInstitutionId, Language)%>
                            </td>

                            <%CommonDTO commonDTO = bill_managementDTO; %>
                            <%@include file="../pb/searchAndViewButton.jsp" %>

                            <td class="text-right">
                                <div class='checker'>
                                    <span class='chkEdit'><input type='checkbox' name='ID' value='<%=bill_managementDTO.iD%>'/></span>
                                </div>
                            </td>
                        </tr>
        <%
                    }
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>">
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>">
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>">
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>">
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>">