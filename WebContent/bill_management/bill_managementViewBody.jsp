<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="bill_management.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@ page import="util.StringUtils" %>
<%@ page import="java.math.BigDecimal" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="static util.StringUtils.convertToBanNumber" %>
<%@ page import="static util.StringUtils.convertBanglaIfLanguageIsBangla" %>
<%@ page import="supplier_institution.Supplier_institutionRepository" %>


<%
    String servletName = "Bill_managementServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Bill_managementDTO bill_managementDTO = (Bill_managementDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
%>

<%@include file="../pb/viewInitializer.jsp" %>

<%
    boolean isEnglish = "english".equalsIgnoreCase(Language);
    boolean isBangla = "bangla".equalsIgnoreCase(Language);
    long deductionAmount, runningTotal = bill_managementDTO.billAmount;
%>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    .symbol-taka-amount {
        display: flex;
        justify-content: space-between;
    }
</style>

<div class="kt-content p-0" id="kt_content">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=getDataByLanguage(Language, "ক্রয়,সরবরাহ ও সেবা বাবদ বিল", "Purchase, Supply & Service Bill")%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body " id="bill-div">
            <table class="table mt-3">
                <thead>
                <tr>
                    <th rowspan="2">
                        <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, loginDTO)%>
                    </th>
                    <th rowspan="2">
                        <%=LM.getText(LC.BILL_MANAGEMENT_ADD_SUPPLIEDITEMDESCRIPTION, loginDTO)%>
                    </th>
                    <th class="text-center" colspan="2">
                        <%=LM.getText(LC.PAYMENT_ADD_AMOUNT, loginDTO)%>
                    </th>
                </tr>
                <tr>
                    <th class="text-center"><%=getDataByLanguage(Language, "টাকা", "Taka")%>
                    </th>
                    <th class="text-center"><%=getDataByLanguage(Language, "পয়সা", "Poysha")%>
                    </th>
                </tr>
                </thead>

                <tbody>
                <%--Bill Amount--%>
                <tr>
                    <td>
                        <%=Economic_sub_codeRepository.getInstance().getText(Language, bill_managementDTO.economicSubCodeId)%>
                    </td>
                    <td>
                        <%=isEnglish ? "On account of " : ""%>
                        <%=bill_managementDTO.suppliedItemDescription%>
                        <%=isBangla ? " বাবদ " : ""%>
                        <%=isEnglish ? " provides checks to " : ""%>
                        <span style="color: #008cd4">
                            "<%=Supplier_institutionRepository.getInstance().getIssuedToText(bill_managementDTO.supplierInstitutionId, Language)%>"
                        </span>
                        <%=isBangla ? " এর অনুকূলে চেক দেয় " : ""%>
                    </td>
                    <td class="text-right">
                        <%=BangladeshiNumberFormatter.getFormattedNumber(
                                convertBanglaIfLanguageIsBangla(Language, String.valueOf(bill_managementDTO.billAmount))
                        )%>/-
                    </td>
                    <td></td>
                </tr>

                <%--VAT--%>
                <%
                    if (bill_managementDTO.hasVat()) {
                        deductionAmount = Bill_managementDTO.getPercentageOfAmount(bill_managementDTO.billAmount, bill_managementDTO.vatPercent);
                %>
                <tr>
                    <td></td>
                    <td class="text-right">
                        <%
                            value = String.format("%.2f", bill_managementDTO.vatPercent);
                        %>
                        <%=convertBanglaIfLanguageIsBangla(Language, value)%> %
                        <%=getDataByLanguage(Language, " ভ্যাট", " VAT")%>
                    </td>
                    <td>
                        <div class="symbol-taka-amount">
                            <div class="text-danger font-weight-bold">
                                (-)
                            </div>
                            <div>
                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                        convertBanglaIfLanguageIsBangla(Language, String.valueOf(deductionAmount))
                                )%>/-
                            </div>
                        </div>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                        <div class="symbol-taka-amount">
                            <div class="font-weight-bold">
                                =
                            </div>
                            <div>
                                <%
                                    runningTotal -= deductionAmount;
                                %>
                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                        convertBanglaIfLanguageIsBangla(Language, String.valueOf(runningTotal))
                                )%>/-
                            </div>
                        </div>
                    </td>
                    <td></td>
                </tr>
                <%}%>

                <%--TAX--%>
                <%
                    if (bill_managementDTO.hasTax()) {
                        deductionAmount = Bill_managementDTO.getPercentageOfAmount(bill_managementDTO.billAmount, bill_managementDTO.taxPercent);
                %>
                <tr>
                    <td></td>
                    <td class="text-right">
                        <%
                            value = String.format("%.2f", bill_managementDTO.taxPercent);
                        %>
                        <%=convertBanglaIfLanguageIsBangla(Language, value)%> %
                        <%=getDataByLanguage(Language, " কর", " Tax")%>
                    </td>
                    <td>
                        <div class="symbol-taka-amount">
                            <div class="text-danger font-weight-bold">
                                (-)
                            </div>
                            <div>
                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                        convertBanglaIfLanguageIsBangla(Language, String.valueOf(deductionAmount))
                                )%>/-
                            </div>
                        </div>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                        <div class="symbol-taka-amount">
                            <div class="font-weight-bold">
                                =
                            </div>
                            <div>
                                <%
                                    runningTotal -= deductionAmount;
                                %>
                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                        convertBanglaIfLanguageIsBangla(Language, String.valueOf(runningTotal))
                                )%>/-
                            </div>
                        </div>
                    </td>
                    <td></td>
                </tr>
                <%}%>

                <%--SERVICE CHARGE--%>
                <%
                    if (bill_managementDTO.hasServiceCharge()) {
                        deductionAmount = Bill_managementDTO.getPercentageOfAmount(bill_managementDTO.billAmount, bill_managementDTO.serviceChargePercent);
                %>
                <tr>
                    <td></td>
                    <td class="text-right">
                        <%
                            value = String.format("%.2f", bill_managementDTO.serviceChargePercent);
                        %>
                        <%=convertBanglaIfLanguageIsBangla(Language, value)%> %
                        <%=getDataByLanguage(Language, " সার্ভিস চার্জ", " Service Charge")%>
                    </td>
                    <td>
                        <div class="symbol-taka-amount">
                            <div class="text-danger font-weight-bold">
                                (-)
                            </div>
                            <div>
                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                        convertBanglaIfLanguageIsBangla(Language, String.valueOf(deductionAmount))
                                )%>/-
                            </div>
                        </div>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                        <div class="symbol-taka-amount">
                            <div class="font-weight-bold">
                                =
                            </div>
                            <div>
                                <%
                                    runningTotal -= deductionAmount;
                                %>
                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                        convertBanglaIfLanguageIsBangla(Language, String.valueOf(runningTotal))
                                )%>/-
                            </div>
                        </div>
                    </td>
                    <td></td>
                </tr>
                <%}%>


                <%--Allocated Budget--%>
                <tr>
                    <td></td>
                    <td class="text-right">
                        <%=LM.getText(LC.BILL_MANAGEMENT_ADD_ALLOCATEDBUDGET, loginDTO)%>
                    </td>
                    <td class="text-right">
                        <%=BangladeshiNumberFormatter.getFormattedNumber(
                                convertBanglaIfLanguageIsBangla(Language, String.valueOf(bill_managementDTO.allocatedBudget))
                        )%>/-
                    </td>
                    <td></td>
                </tr>

                <%--upto last bill total--%>
                <tr>
                    <td></td>
                    <td class="text-right">
                        <%=LM.getText(LC.BILL_MANAGEMENT_ADD_BILLRUNNINGTOTAL, loginDTO)%>
                    </td>
                    <td class="text-right">
                        <div class="symbol-taka-amount">
                            <div class="text-success font-weight-bold">
                                (+)
                            </div>
                            <div>
                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                        convertBanglaIfLanguageIsBangla(Language, String.valueOf(bill_managementDTO.uptoLastBillTotal))
                                )%>/-
                            </div>
                        </div>
                    </td>
                    <td></td>
                </tr>

                <%--This Bill Total--%>
                <tr>
                    <td></td>
                    <td class="text-right">
                        <%=getDataByLanguage(
                                Language,
                                "এই বিলের মোট জের",
                                "This Bill Total"
                        )%>
                    </td>
                    <td class="text-right">
                        <div class="symbol-taka-amount">
                            <div class="text-success font-weight-bold">
                                (+)
                            </div>
                            <div>
                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                        convertBanglaIfLanguageIsBangla(Language, String.valueOf(bill_managementDTO.billAmount))
                                )%>/-
                            </div>
                        </div>
                    </td>
                    <td></td>
                </tr>
                </tbody>

                <tfoot>
                <%--Total--%>
                <tr>
                    <td></td>
                    <td class="text-right">
                        <%=getDataByLanguage(
                                Language,
                                "মোট (পরবর্তী বিলে জের টেনে নেয়া হবে)",
                                "Total (will be carried forward in next bill)"
                        )%>
                    </td>
                    <td class="text-right">
                        <%
                            long toBeCarried = bill_managementDTO.billAmount + bill_managementDTO.uptoLastBillTotal;
                        %>
                        <%=BangladeshiNumberFormatter.getFormattedNumber(
                                convertBanglaIfLanguageIsBangla(Language, String.valueOf(toBeCarried))
                        )%>/-
                    </td>
                    <td></td>
                </tr>
                </tfoot>
            </table>
            <div class="row mt-5">
                <div class="col-12">
                    <div class="form-actions text-right">
                        <a class="btn-sm border-0 shadow btn-border-radius text-white mr-2"
                           style="background-color: #ff6b6b;cursor: pointer;"
                           href='<%=servletName%>?actionType=getEditPage&ID=<%=bill_managementDTO.iD%>'>
                            <%=getDataByLanguage(Language, "এডিট করুন", "Edit")%>
                        </a>

                        <a class="btn-sm shadow text-white border-0 submit-btn ml-2" style="cursor: pointer;"
                           href="Bill_managementServlet?actionType=downloadBill&ID=<%=bill_managementDTO.iD%>">
                            <%=getDataByLanguage(Language, "বিল প্রস্তুত করুন", "Prepare Bill")%>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>