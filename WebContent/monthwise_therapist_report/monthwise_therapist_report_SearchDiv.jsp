<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ page import="pb.*" %>
<%
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.DOCTOR_PERFORMANCE_REPORT_EDIT_LANGUAGE, loginDTO);

%>

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>
<div class="row mx-2">

    <div id="doctorType" class="search-criteria-div col-md-12">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=Language.equalsIgnoreCase("english")?"Therapist":"থেরাপিদাতা"%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='dr_user_name' id='dr_user_name'
                        tag='pb_html'>
                    <%
                        Options = CommonDAO.getTherapistsByUserName();
                        out.print(Options);
                    %>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <%@include file="../pbreport/yearmonth.jsp" %>
    </div>
    <div class="col-md-12">
        <%@include file="../pbreport/calendar.jsp" %>
    </div>
</div>
<script type="text/javascript">

	var ordIgColumn = 5;
	var isVisible = [true, true, true, true, true, true, false];

    function init() {
        
        $("#doctorOrganogramId").select2({
        	dropdownAutoWidth: true,
            theme: "classic"
        });
        
        dateTimeInit($("#Language").val());
        /*$("#search_by_date").prop('checked', true);
        $("#search_by_date").trigger("change");
        setDateByStringAndId('startDate_js', '<%=datestr%>');
        setDateByStringAndId('endDate_js', '<%=datestr%>');*/
        add1WithEnd = false;
        processNewCalendarDateAndSubmit();
    }

    function PreprocessBeforeSubmiting() {
    }

  
    


</script>
