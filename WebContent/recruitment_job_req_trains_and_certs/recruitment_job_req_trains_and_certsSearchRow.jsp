<%@page pageEncoding="UTF-8" %>

<%@page import="recruitment_job_req_trains_and_certs.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Recruitment_job_req_trains_and_certsDTO recruitment_job_req_trains_and_certsDTO = (Recruitment_job_req_trains_and_certsDTO) request.getAttribute("recruitment_job_req_trains_and_certsDTO");
    CommonDTO commonDTO = recruitment_job_req_trains_and_certsDTO;
    String servletName = "Recruitment_job_req_trains_and_certsServlet";


    System.out.println("recruitment_job_req_trains_and_certsDTO = " + recruitment_job_req_trains_and_certsDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Recruitment_job_req_trains_and_certsDAO recruitment_job_req_trains_and_certsDAO = (Recruitment_job_req_trains_and_certsDAO) request.getAttribute("recruitment_job_req_trains_and_certsDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_nameBn'>
    <%
        value = recruitment_job_req_trains_and_certsDTO.nameBn + "";
    %>

    <%=value%>


</td>


<td id='<%=i%>_nameEn'>
    <%
        value = recruitment_job_req_trains_and_certsDTO.nameEn + "";
    %>

    <%=value%>


</td>


<td>
    <button type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Recruitment_job_req_trains_and_certsServlet?actionType=view&ID=<%=recruitment_job_req_trains_and_certsDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit' class="text-center">
    <button
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="edit('<%=recruitment_job_req_trains_and_certsDTO.iD%>')">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'>
            <input type='checkbox' name='ID' value='<%=recruitment_job_req_trains_and_certsDTO.iD%>'/>
        </span>
    </div>
</td>


<%--<script>--%>

<%--</script>--%>