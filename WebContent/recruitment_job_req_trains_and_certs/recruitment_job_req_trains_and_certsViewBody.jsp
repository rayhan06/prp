<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="recruitment_job_req_trains_and_certs.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Recruitment_job_req_trains_and_certsDTO recruitment_job_req_trains_and_certsDTO = Recruitment_job_req_trains_and_certsRepository.getInstance()
            .getRecruitment_job_req_trains_and_certsDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important; margin-bottom: -18px">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS_ADD_RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row">
                <h5  class="table-title">
                    <%=LM.getText(LC.RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS_ADD_RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS_ADD_NAMEBN, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = recruitment_job_req_trains_and_certsDTO.nameBn + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%">
                                <b><%=LM.getText(LC.RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS_ADD_NAMEEN, loginDTO)%>
                                </b></td>
                            <td>

                                <%
                                    value = recruitment_job_req_trains_and_certsDTO.nameEn + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                    </table>
                </div>
            </div>
        </div>
    </div>
</div>