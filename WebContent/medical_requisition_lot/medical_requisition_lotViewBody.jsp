<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="medical_requisition_lot.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>

<%
    String servletName = "Medical_requisition_lotServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.MEDICAL_REQUISITION_LOT_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Medical_requisition_lotDAO medical_requisition_lotDAO = new Medical_requisition_lotDAO("medical_requisition_lot");
    Medical_requisition_lotDTO medical_requisition_lotDTO = medical_requisition_lotDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<!-- begin:: Subheader -->
<div class="text-right mr-2 mt-4">
    <%
        if (true) {
    %>
    <button
            type="button"
            class="btn"
            id='printer'
            onclick="printAnyDiv('modalbody')">
        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
    <%
        }
    %>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content" id="kt_content">
    <div class="kt-portlet" id="modalbody">
        <div class="kt-portlet__body px-0">
            <h5 class="table-title">
                <%=LM.getText(LC.MEDICAL_REQUISITION_LOT_ADD_MEDICAL_REQUISITION_LOT_ADD_FORMNAME, loginDTO)%>
            </h5>
            <div class="table-responsive">
                <table class="table table-bordered table-striped text-nowrap">
                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.MEDICAL_REQUISITION_LOT_ADD_REQUISITIONTOORGANOGRAMID, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = WorkflowController.getNameFromOrganogramId(medical_requisition_lotDTO.requisitionToOrganogramId, Language);
                            %>

                            <%=value%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.MEDICAL_REQUISITION_LOT_EDIT_SUBJECT, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = medical_requisition_lotDTO.subject + "";
                            %>

                            <%=value%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.HM_DATE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = medical_requisition_lotDTO.insertionDate + "";
                            %>
                            <%
                                String formatted_insertionDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                            %>

                            <%=Utils.getDigits(formatted_insertionDate, Language)%>


                        </td>

                    </tr>


                </table>
            </div>
            <h5 class="table-title mt-5">
                <%=LM.getText(LC.REAGENT_REQUISITION_LOT_ADD_REAGENT_REQUISITION_ITEM, loginDTO)%>
            </h5>
            <div class="table-responsive">
                <table class="table table-bordered table-striped text-nowrap">
                    <tr>
                        <th><%=LM.getText(LC.MEDICAL_REQUISITION_LOT_ADD_MEDICAL_REQUISITION_ITEM_DRUGINFORMATIONTYPE, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.MEDICAL_REQUISITION_LOT_ADD_MEDICAL_REQUISITION_ITEM_TOTALCURRENTSTOCK, loginDTO)%>
                        </th>
                        <th>
                            <div class="d-flex justify-content-around">
                                <div class=" text-danger">
                                    <%=LM.getText(LC.HM_EXPIRE_DATE, loginDTO)%>
                                </div>
                                <div class=" text-success ml-3 ml-lg-0">
                                    <%=LM.getText(LC.HM_REMAINING_STOCK, loginDTO)%>
                                </div>
                            </div>
                        </th>
                        <th><%=(actionName.equals("edit")) ? (LM.getText(LC.MEDICAL_REQUISITION_LOT_EDIT_MEDICAL_REQUISITION_ITEM_QUANTITY, loginDTO)) : (LM.getText(LC.MEDICAL_REQUISITION_LOT_ADD_MEDICAL_REQUISITION_ITEM_QUANTITY, loginDTO))%>
                        </th>
                        <th><%=(actionName.equals("edit")) ? (LM.getText(LC.MEDICAL_REQUISITION_LOT_EDIT_MEDICAL_REQUISITION_ITEM_REMARKS, loginDTO)) : (LM.getText(LC.MEDICAL_REQUISITION_LOT_ADD_MEDICAL_REQUISITION_ITEM_REMARKS, loginDTO))%>
                        </th>
                    </tr>
                    <%
                        MedicalRequisitionItemDAO medicalRequisitionItemDAO = new MedicalRequisitionItemDAO();
                        List<MedicalRequisitionItemDTO> medicalRequisitionItemDTOs = medicalRequisitionItemDAO.getMedicalRequisitionItemDTOListByMedicalRequisitionLotID(medical_requisition_lotDTO.iD);

                        for (MedicalRequisitionItemDTO medicalRequisitionItemDTO : medicalRequisitionItemDTOs) {
                    %>
                    <tr>
                        <td>
                            <%
                                value = medicalRequisitionItemDTO.drugInformationType + "";
                            %>
                            <%
                                value = CommonDAO.getName(Integer.parseInt(value), "drug_information", "name_en", "id");
                            %>

                            <%=value%>


                        </td>
                        <td>
                            <%
                                value = medicalRequisitionItemDTO.totalCurrentStock + "";
                            %>


                            <%=Utils.getDigits(value, Language)%>


                        </td>
                        <td>
                            <table>
                                <%
                                    String[] eDates = medicalRequisitionItemDTO.expiryDateList.split(", ");
                                    String[] rStocks = medicalRequisitionItemDTO.currentStockList.split(", ");


                                    for (int j = 0; j < eDates.length; j++) {
                                        if (j == eDates.length - 1) {
                                            eDates[j] = eDates[j].split(",")[0];
                                            rStocks[j] = rStocks[j].split(",")[0];
                                        }
                                %>
                                <tr>
                                    <div class='row px-3'>
                                        <div class='col-6 border text-center rounded px-2 py-3'>
                                            <%=Utils.getDigits(eDates[j] + "", Language)%>
                                        </div>
                                        <div class='col-6 border text-center rounded p-2'>
                                            <%=Utils.getDigits(rStocks[j] + "", Language)%>
                                        </div>
                                    </div>
                                </tr>

                                <%
                                    }

                                %>
                            </table>

                        </td>
                        <td>
                            <%
                                value = medicalRequisitionItemDTO.quantity + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>
                        <td>
                            <%
                                value = medicalRequisitionItemDTO.remarks + "";
                            %>

                            <%=value%>


                        </td>
                    </tr>
                    <%

                        }

                    %>
                </table>
            </div>
        </div>
    </div>
</div>