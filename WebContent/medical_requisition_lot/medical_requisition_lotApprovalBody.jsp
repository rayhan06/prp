
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%
String url = "Medical_requisition_lotServlet?actionType=search";
String navigator = SessionConstants.NAV_MEDICAL_REQUISITION_LOT;
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.MEDICAL_REQUISITION_LOT_EDIT_LANGUAGE, loginDTO);

String pageno = "";

RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();
boolean isPermanentTable = rn.m_isPermanentTable;

System.out.println("rn " + rn);

String action = url;
String context = "../../.." + request.getContextPath() + "/";
String link = context + url;
String concat = "?";
if (url.contains("?")) {
	concat = "&";
}
int pagination_number = 0;
%>
	<jsp:include page="../pb/approvalNav.jsp" flush="true">
		<jsp:param name="url" value="<%=url%>" />
		<jsp:param name="navigator" value="<%=navigator%>" />
		<jsp:param name="pageName" value="<%=LM.getText(LC.MEDICAL_REQUISITION_LOT_SEARCH_MEDICAL_REQUISITION_LOT_SEARCH_FORMNAME, loginDTO)%>" />
		<jsp:param name="action" value="Medical_requisition_lot_testServlet?actionType=getApprovalPage" />
		<jsp:param name="Language" value="<%=LM.getText(LC.MEDICAL_REQUISITION_LOT_EDIT_LANGUAGE, loginDTO)%>" />
	</jsp:include>


<div class="portlet box">
	<div class="portlet-body">
		<form action="Medical_requisition_lotServlet?isPermanentTable=<%=isPermanentTable%>&actionType=delete" method="POST" id="tableForm" enctype = "multipart/form-data">
			<jsp:include page="medical_requisition_lotApprovalForm.jsp" flush="true">
				<jsp:param name="pageName" value="<%=LM.getText(LC.MEDICAL_REQUISITION_LOT_SEARCH_MEDICAL_REQUISITION_LOT_SEARCH_FORMNAME, loginDTO)%>" />
			</jsp:include>	
		</form>
	</div>
</div>

<% pagination_number = 1;%>
<%@include file="../common/pagination_with_go2.jsp"%>
<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function(){
	dateTimeInit("<%=Language%>");
	$("select").select2({
		dropdownAutoWidth: true,
	    theme: "classic"
	});
});


$(document).on("click",'#chkEdit',function(){
	debugger;
	$("#chkEdit").toggleClass("checked");
});
$(document).on("click",'input[type="checkbox"]',function(e){
	debugger;
	e.classList.toggle("checked");
});

</script>


