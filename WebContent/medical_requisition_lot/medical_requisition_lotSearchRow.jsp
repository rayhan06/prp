<%@page pageEncoding="UTF-8" %>

<%@page import="medical_requisition_lot.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="approval_execution_table.*" %>
<%@ page import="approval_path.*" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.MEDICAL_REQUISITION_LOT_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_MEDICAL_REQUISITION_LOT;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Medical_requisition_lotDTO medical_requisition_lotDTO = (Medical_requisition_lotDTO) request.getAttribute("medical_requisition_lotDTO");
    CommonDTO commonDTO = medical_requisition_lotDTO;
    String servletName = "Medical_requisition_lotServlet";

    Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
    ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
    Approval_execution_tableDTO approval_execution_tableDTO = null;
    String Message = "Done";
    approval_execution_tableDTO = (Approval_execution_tableDTO) approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("medical_requisition_lot", medical_requisition_lotDTO.iD);

    System.out.println("medical_requisition_lotDTO = " + medical_requisition_lotDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Medical_requisition_lotDAO medical_requisition_lotDAO = (Medical_requisition_lotDAO) request.getAttribute("medical_requisition_lotDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_requisitionToOrganogramId'>
    <%
        value = WorkflowController.getNameFromOrganogramId(medical_requisition_lotDTO.requisitionToOrganogramId, Language);
    %>

    <%=value%>


</td>


<td id='<%=i%>_subject'>
    <%
        value = medical_requisition_lotDTO.subject + "";
    %>

    <%=value%>


</td>

<td id='<%=i%>_insertedByUserId'>
    <%
        value = medical_requisition_lotDTO.insertedByUserId + "";
    %>
    <%
        value = WorkflowController.getNameFromUserId(medical_requisition_lotDTO.insertedByUserId, Language);
        if (value.equalsIgnoreCase("")) {
            value = "superman";
        }
    %>

    <%=value%>


</td>


<td id='<%=i%>_insertionDate'>
    <%
        value = medical_requisition_lotDTO.insertionDate + "";
    %>
    <%
        String formatted_insertionDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=formatted_insertionDate%>


</td>


<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Medical_requisition_lotServlet?actionType=view&ID=<%=medical_requisition_lotDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button
            class="btn-sm border-0 shadow btn-border-radius text-white" type="button"
            style="background-color: #ff6b6b;"
            onclick="location.href='Medical_requisition_lotServlet?actionType=getEditPage&ID=<%=medical_requisition_lotDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td>
    <button class="btn btn-sm btn-primary shadow btn-border-radius pl-4 pb-2" type="button"
            onclick="location.href='Medical_requisition_lotServlet?actionType=copy&ID=<%=medical_requisition_lotDTO.iD%>'">
        <i class="fas fa-copy"></i>
    </button>
</td>
<td class="text-right" id='<%=i%>_checkbox'>
    <div class='checker'>
        <span class='chkEdit'>
            <input type='checkbox' name='ID' value='<%=medical_requisition_lotDTO.iD%>'/>
        </span>
    </div>
</td>
											
											
											
																						
											

