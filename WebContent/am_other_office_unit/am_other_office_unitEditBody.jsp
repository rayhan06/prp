<%@page import="am_other_office_unit.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.*" %>
<%@ page import="common.BaseServlet" %>

<%
    Am_other_office_unitDTO am_other_office_unitDTO = new Am_other_office_unitDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        am_other_office_unitDTO = (Am_other_office_unitDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
        ;
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = am_other_office_unitDTO;
    String tableName = "am_other_office_unit";


%>
<%@include file="../pb/addInitializer2.jsp" %>
<%

    String formTitle = LM.getText(LC.AM_OTHER_OFFICE_UNIT_ADD_AM_OTHER_OFFICE_UNIT_ADD_FORMNAME, loginDTO);
    String servletName = "Am_other_office_unitServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=am_other_office_unitDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_OTHER_OFFICE_UNIT_ADD_OFFICENAMEEN, loginDTO)%>
                                            <span style="color: red">*</span></label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='officeNameEn'
                                                   id='officeNameEn_text_<%=i%>'
                                                   value='<%=am_other_office_unitDTO.officeNameEn%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_OTHER_OFFICE_UNIT_ADD_OFFICENAMEBN, loginDTO)%>
                                            <span style="color: red">*</span></label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control noEnglish' name='officeNameBn'
                                                   id='officeNameBn_text_<%=i%>'
                                                   value='<%=am_other_office_unitDTO.officeNameBn%>' tag='pb_html'/>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn"
                                    class="btn-sm shadow text-white border-0 cancel-btn btn-border-radius">
                                <%=LM.getText(LC.AM_OTHER_OFFICE_UNIT_ADD_AM_OTHER_OFFICE_UNIT_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn"
                                    class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                                    type="button" onclick="submitForm()">
                                <%=LM.getText(LC.AM_OTHER_OFFICE_UNIT_ADD_AM_OTHER_OFFICE_UNIT_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    const amOfficeRequestForm = $("#bigform");

    function PreprocessBeforeSubmiting(row, validate) {

        amOfficeRequestForm.validate();
        return amOfficeRequestForm.valid();

    }


    function submitForm() {
        buttonStateChange(true);
        if (PreprocessBeforeSubmiting(0)) {
            let url = "<%=servletName%>?actionType=<%=actionName%>";
            $.ajax({
                type: "POST",
                url: url,
                data: amOfficeRequestForm.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        } else {
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function onInputPasteNoEnglish(event) {
        var clipboardData = event.clipboardData || window.clipboardData;
        var data = clipboardData.getData("Text");
        var valid = data.toString().split('').every(char => {
            var allowed = "~`!@#$%^&*()_-+=|\'\"\\|:?/;<>,.}]{[";
            if (allowed.includes(char)) return true;
            if (char.toString().trim() == '')
                return true;
            if ('0' <= char && char <= '9')
                return false;
            if ('a' <= char && char <= 'z')
                return false;
            if ('A' <= char && char <= 'Z')
                return false;
            return true;
        });
        if (valid) return;
        event.stopPropagation();
        event.preventDefault();
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Am_other_office_unitServlet");
    }

    function init(row) {


    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        $(".noEnglish").keypress(function (event) {
            var allowed = "~`!@#$%^&*()_-+=|\'\"\\|:?/;<>,.}]{[";
            if (allowed.includes(event.key)) return true;
            var ew = event.which;
            if (ew == 32)
                return true;
            if (48 <= ew && ew <= 57)
                return false;
            if (65 <= ew && ew <= 90)
                return false;
            if (97 <= ew && ew <= 122)
                return false;
            return true;
        });

        elements = document.getElementsByClassName("noEnglish");

        for (var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('paste', onInputPasteNoEnglish);
        }
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






