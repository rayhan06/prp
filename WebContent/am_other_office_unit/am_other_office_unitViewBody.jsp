<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="am_other_office_unit.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.*" %>
<%@ page import="common.BaseServlet" %>


<%
    String servletName = "Am_other_office_unitServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Am_other_office_unitDTO am_other_office_unitDTO = (Am_other_office_unitDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    CommonDTO commonDTO = am_other_office_unitDTO;
%>
<%@include file="../pb/viewInitializer.jsp" %>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.OFFICE_ORIGIN_LABEL, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-md-10 offset-md-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.OFFICE_ORIGIN_LABEL, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_OTHER_OFFICE_UNIT_ADD_OFFICENAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = am_other_office_unitDTO.officeNameEn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_OTHER_OFFICE_UNIT_ADD_OFFICENAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = am_other_office_unitDTO.officeNameBn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>