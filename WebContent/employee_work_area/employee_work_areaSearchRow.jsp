<%@page pageEncoding="UTF-8" %>
<td>
    <%=employee_work_areaDTO.nameEn%>
</td>

<td>
    <%=employee_work_areaDTO.nameBn%>
</td>
<td>
    <%=employee_work_areaDTO.description%>
</td>

<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Employee_work_areaServlet?actionType=view&ID=<%=employee_work_areaDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td>
    <button type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Employee_work_areaServlet?actionType=getEditPage&ID=<%=employee_work_areaDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=employee_work_areaDTO.iD%>'/></span>
    </div>
</td>
																						
											

