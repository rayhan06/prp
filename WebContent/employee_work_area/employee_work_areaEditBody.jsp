<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="employee_work_area.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.HttpRequestUtils" %>


<%
    Employee_work_areaDTO employee_work_areaDTO;
    employee_work_areaDTO = (Employee_work_areaDTO) request.getAttribute("employee_work_areaDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (employee_work_areaDTO == null) {
        employee_work_areaDTO = new Employee_work_areaDTO();
    }
    System.out.println("employee_work_areaDTO = " + employee_work_areaDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }

    String formTitle;
    if (actionName.equals("edit")) {
        formTitle = LM.getText(LC.EMPLOYEE_WORK_AREA_EDIT_EMPLOYEE_WORK_AREA_EDIT_FORMNAME, loginDTO);
    } else {
        formTitle = LM.getText(LC.EMPLOYEE_WORK_AREA_ADD_EMPLOYEE_WORK_AREA_ADD_FORMNAME, loginDTO);
    }

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isEnglish  = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form" id="bigform" name="bigform"
              action="Employee_work_areaServlet?actionType=ajax_<%=actionName%>" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-8 offset-md-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD'
                                           id='iD_hidden_<%=i%>'
                                           value='<%=employee_work_areaDTO.iD%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_WORK_AREA_EDIT_NAMEEN, loginDTO)) : (LM.getText(LC.EMPLOYEE_WORK_AREA_ADD_NAMEEN, loginDTO))%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9" id='nameEn_div_<%=i%>'>
                                            <input type='text' class='englishOnly form-control'
                                                   name='nameEn' id='nameEn_text_<%=i%>'
                                                   required="required"
                                                   placeholder='<%=Language.equalsIgnoreCase("English")?"Write Name English":"নাম (ইংরেজি) লিখুন"%>'
                                                   value=<%=actionName.equals("edit")?("'" + employee_work_areaDTO.nameEn + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_WORK_AREA_EDIT_NAMEBN, loginDTO)) : (LM.getText(LC.EMPLOYEE_WORK_AREA_ADD_NAMEBN, loginDTO))%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9" id='nameBn_div_<%=i%>'>
                                            <input type='text' class='noEnglish form-control'
                                                   name='nameBn' id='nameBn_text_<%=i%>'
                                                   required="required"
                                                   placeholder='<%=Language.equalsIgnoreCase("English")?"Write Name Bangla":"নাম (বাংলা) লিখুন"%>'
                                                   value=<%=actionName.equals("edit")?("'" + employee_work_areaDTO.nameBn + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_WORK_AREA_EDIT_DESCRIPTION, loginDTO)) : (LM.getText(LC.EMPLOYEE_WORK_AREA_ADD_DESCRIPTION, loginDTO))%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9" id='description_div_<%=i%>'>
                                             <textarea type='text' class='form-control'
                                                       name='description'
                                                       id='description_text_<%=i%>' required="required"
                                                       style="text-align: left"><%=actionName.equals("edit") ? (employee_work_areaDTO.description) : ("")%></textarea>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                type="button" onclick="location.href = '<%=request.getHeader("referer")%>'">
                            <%=LM.getText(LC.EMPLOYEE_WORK_AREA_EDIT_EMPLOYEE_WORK_AREA_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button class="btn-sm shadow text-white border-0 submit-btn ml-2" id="submit-btn"
                                type="button" onclick="submitForm()">
                            <%=LM.getText(LC.EMPLOYEE_WORK_AREA_EDIT_EMPLOYEE_WORK_AREA_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    const form = $("#bigform");

    $(document).ready(function () {
        basicInit();
        dateTimeInit("<%=Language%>");

        form.validate({
            nameEn: "required",
            nameBn: "required",
            description: "required",
            messages: {
                nameEn: '<%=isEnglish? "Please insert English Name" : "ইংরেজি নাম দিন"%>',
                nameBn: '<%=isEnglish? "Please insert Bangla Name" : "বাংলা নাম দিন"%>',
                description: '<%=isEnglish? "Please insert Description" : "বিবরণ দিন"%>'
            }
        });
    });

    function submitForm(){
        if(form.valid()){
            submitAjaxForm();
        }
    }

    function buttonStateChange(value){
        $('#submit-btn').prop('disabled',value);
        $('#cancel-btn').prop('disabled',value);
    }
</script>