<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="employee_work_area.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.EMPLOYEE_WORK_AREA_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Employee_work_areaDAO employee_work_areaDAO = new Employee_work_areaDAO("employee_work_area");
    Employee_work_areaDTO employee_work_areaDTO = (Employee_work_areaDTO) employee_work_areaDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.EMPLOYEE_WORK_AREA_SEARCH_ANYFIELD, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <h5 class="table-title">
                <%=LM.getText(LC.EMPLOYEE_WORK_AREA_SEARCH_ANYFIELD,loginDTO)%>
            </h5>
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_WORK_AREA_EDIT_NAMEEN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = employee_work_areaDTO.nameEn + "";
                            %>

                            <%=value%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_WORK_AREA_EDIT_NAMEBN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = employee_work_areaDTO.nameBn + "";
                            %>

                            <%=value%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_WORK_AREA_EDIT_DESCRIPTION, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = employee_work_areaDTO.description + "";
                            %>

                            <%=value%>


                        </td>

                    </tr>


                </table>
            </div>
        </div>
    </div>
</div>