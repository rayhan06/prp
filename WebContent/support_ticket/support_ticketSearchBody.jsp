
<%@page import="support_ticket.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@ page import="util.RecordNavigator"%>

<%@ page import="java.util.*"%>
<%@ page import="appointment.*"%>
<%@page pageEncoding="UTF-8" %>
<%@ page import = "util.HttpRequestUtils" %>
<%@ page import = "user.*" %>

<%-- <%@ page errorPage="failure.jsp"%> --%>
<%
String url = "Support_ticketServlet?actionType=search";
String navigator = SessionConstants.NAV_SUPPORT_TICKET;
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.APPOINTMENT_EDIT_LANGUAGE, loginDTO);

String pageno = "";

RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();
boolean isPermanentTable = rn.m_isPermanentTable;

System.out.println("rn " + rn);

String action = url;
String context = "../../.." + request.getContextPath() + "/";
String link = context + url;
String concat = "?";
if (url.contains("?")) {
	concat = "&";
}
int pagination_number = 0;
Support_ticketDAO support_ticketDAO = new Support_ticketDAO();
%>
<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <i class="fa fa-search fa-2x" style="color:#0098bf !important;"></i>&nbsp;
        <h3 class="kt-subheader__title">
            &nbsp; <%=LM.getText(LC.SUPPORT_TICKET_SEARCH_SUPPORT_TICKET_SEARCH_FORMNAME, loginDTO)%>
        </h3>
        
    </div>
    <%
    int open = 0, closed = 0, solved = 0, total = 0;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    List<KeyCountDTO> ticketStatusCounts = support_ticketDAO.getTicketStatusCount(userDTO);
    for(KeyCountDTO ticketStatusCount: ticketStatusCounts)
    {
    	if(ticketStatusCount.key == Support_ticketDTO.OPEN)
    	{
    		open = ticketStatusCount.count;
    	}
    	else if(ticketStatusCount.key == Support_ticketDTO.CLOSED)
    	{
    		closed = ticketStatusCount.count;
    	}
    	else if(ticketStatusCount.key == Support_ticketDTO.SOLVED)
    	{
    		solved = ticketStatusCount.count;
    	}
    }
    total = open + closed + solved;
    %>
    <div class="col-lg-6">
    	<table class="table table-bordered table-striped">
        	<tr>
        		<td><%=Language.equalsIgnoreCase("english")?"Total":"মোট"%>
				</td> 
				<td><a href = 'Support_ticketServlet?actionType=search'><%=total%></a></td>
        		<td><%=LM.getText(LC.HM_OPEN, loginDTO)%></td> 
        		<td><a href = 'Support_ticketServlet?actionType=OPEN_TICKET_COUNT_FILTER'><%=open%></a></td>
        		<td><%=LM.getText(LC.HM_CLOSED, loginDTO)%></td> 
        		<td><a href = 'Support_ticketServlet?actionType=CLOSED_TICKET_COUNT_FILTER'><%=closed%></a></td>
        		<td><%=Language.equalsIgnoreCase("english")?"Solved":"সমাধানকৃত"%>
				</td> 
				<td><a href = 'Support_ticketServlet?actionType=SOLVED_TICKET_COUNT_FILTER'><%=solved%></a></td>
        	</tr>
        </table>
    </div>
</div>
<!-- end:: Subheader -->


<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content" style="background: white">
    <div class="row shadow-none border-0">
        <div class="col-lg-12">
            <jsp:include page="./support_ticketNav.jsp" flush="true">
                <jsp:param name="url" value="<%=url%>"/>
                <jsp:param name="navigator" value="<%=navigator%>"/>
                <jsp:param name="pageName"
                           value="<%=LM.getText(LC.SUPPORT_TICKET_SEARCH_SUPPORT_TICKET_SEARCH_FORMNAME, loginDTO)%>"/>
            </jsp:include>
            <div style="height: 1px; background: #ecf0f5"></div>
            <div class="kt-portlet shadow-none">
                <div class="kt-portlet__body">
                    <form action="Support_ticketServlet?isPermanentTable=<%=isPermanentTable%>&actionType=delete"
                          method="POST"
                          id="tableForm" enctype="multipart/form-data">
                        <jsp:include page="support_ticketSearchForm.jsp" flush="true">
                            <jsp:param name="pageName"
                                       value="<%=LM.getText(LC.SUPPORT_TICKET_SEARCH_SUPPORT_TICKET_SEARCH_FORMNAME, loginDTO)%>"/>
                        </jsp:include>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <% pagination_number = 1;%>
    <%@include file="../common/pagination_with_go2.jsp" %>
</div>

<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	initDeleteCheckBoxes();	
	dateTimeInit("<%=Language%>");
	
	select2SingleSelector("#current_assigned_organogram_id", "<%=Language%>");
	
	
});

</script>


