<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="support_ticket.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.List" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="java.text.SimpleDateFormat" %>


<%
    String navigator2 = SessionConstants.NAV_SUPPORT_TICKET;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String Language = "bangla";
    if(isLangEng)
    {
    	Language = "english";
    }
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
    Support_ticketDAO support_ticketDAO = new Support_ticketDAO();
%>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead class="text-nowrap">
        <tr>
            <th><%=isLangEng ? "Ticket#" : "টিকেট#"%>
            </th>
            <th><%=isLangEng ? "Client/User" : "ক্লায়েন্ট/ইউজার"%>
            </th>
            <th><%=isLangEng ? "Problem Details" : "সমস্যার বিবরণ"%>
            </th>
            <th><%=isLangEng ? "Problem Type" : "সমস্যার ধরণ"%>
            </th>
            <th><%=isLangEng ? "Branch/Wing" : "শাখা"%></th>
            <th><%=isLangEng ? "Priority" : "গুরুত্ব"%>
            </th>
            <th><%=isLangEng ? "Status" : "অবস্থা"%>
            </th>
            <th><%=isLangEng ? "Assigned To" : "নিযুক্ত"%>
            </th>

        </tr>
        </thead>
        <tbody>
        <%
            List<Support_ticketDTO> data = (List<Support_ticketDTO>) session.getAttribute(SessionConstants.VIEW_SUPPORT_TICKET);
            if (data != null && data.size()>0) {
                    for (Support_ticketDTO support_ticketDTO : data) {
        %>
        <tr onclick="location.href='Support_ticketServlet?actionType=view&ID=<%=support_ticketDTO.iD%>'">
            <%@include file="support_ticketSearchRow.jsp"%>
        </tr>
        <%
                    }
                }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>


			