<%@page import="support_engineers_diary.*"%>
<%@page import="ticket_issues.*"%>

<%@page import="user.UserRepository"%>
<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="support_ticket.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="employee_offices.*"%>
<%@ page import="user_expertise_map.*" %>
<%@ page import="ticket_forward_history.*" %>
<%@page import="office_unit_organograms.*" %>
<%@page import="employee_offices.*" %>
<%@page import="user.*" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="util.StringUtils" %>

<%
Forward_ticketDTO forward_ticketDTO;
forward_ticketDTO = (Forward_ticketDTO)request.getAttribute("forward_ticketDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
Support_ticketDAO support_ticketDAO = new Support_ticketDAO();
long supportTicketId = -1;
boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
Support_ticketDTO  support_ticketDTO = null;
if(request.getParameter("supportTicketId") != null)
{
	supportTicketId = Long.parseLong(request.getParameter("supportTicketId"));
	support_ticketDTO = support_ticketDAO.getDTOByID(supportTicketId);
}
if(forward_ticketDTO == null)
{
	forward_ticketDTO = new Forward_ticketDTO();
	if(support_ticketDTO != null)
	{
		forward_ticketDTO.iD = supportTicketId;
		forward_ticketDTO.ticketStatusCat = support_ticketDTO.ticketStatusCat;
		if(support_ticketDTO.dueDate != 0)
		{
			forward_ticketDTO.dueDate = support_ticketDTO.dueDate;
		}
	}
	
}
System.out.println("forward_ticketDTO = " + forward_ticketDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = isLangEng ? "TICKET ACTIONS" : "টিকেটের পদক্ষেপসমূহ";
String servletName = "Forward_ticketServlet";
String fileColumnName = "";

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

long ColumnID = -1;
FilesDAO filesDAO = new FilesDAO();
boolean isPermanentTable = true;
String Language = LM.getText(LC.FORWARD_TICKET_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;

long issueRaiserUserId = WorkflowController.getUserIDFromOrganogramId(support_ticketDTO.issueRaiserOrganoramId);

User_expertise_mapDAO user_expertise_mapDAO = new User_expertise_mapDAO();
User_expertise_mapDTO user_expertise_mapDTO = user_expertise_mapDAO.getDTOByUserId(issueRaiserUserId);

boolean dtoFound = true;
if(user_expertise_mapDTO == null)
{
	dtoFound = false;
	user_expertise_mapDTO = new User_expertise_mapDTO();
}
boolean isBoss = userDTO.roleID == SessionConstants.TICKET_ADMIN_ROLE || userDTO.roleID == SessionConstants.ADMIN_ROLE;
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp" >
<jsp:param name="isHierarchyNeeded" value="false" />
</jsp:include>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title prp-page-title">
                            <i class="fa fa-gift"></i>&nbsp;
                            <%=formTitle%>
                        </h3>
                    </div>
                </div>
                
                <form class="form-horizontal"
                      action="Support_ticketServlet?actionType=forwardTicket&id=<%=support_ticketDTO.iD%>"
                      id="bigform" name="bigform" method="POST" 
                      enctype="multipart/form-data" onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
                    <div class="kt-portlet__body form-body">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="onlyborder">
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="sub_title_top">
                                                        <div class="sub_title">
                                                            <h4 style="background: white"><%=formTitle%>
                                                            </h4>
                                                        </div>
                                                     </div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=forward_ticketDTO.iD%>' tag='pb_html'/>
	
													<%
													if(
															support_ticketDTO.ticketStatusCat != Support_ticketDTO.CLOSED &&
															(
															userDTO.roleID == SessionConstants.SUPPORT_ENGINEER_ROLE 
															|| isBoss ))
													{
													%>
													
													<%
													if(support_ticketDTO.ticketIssuesType == Ticket_issuesDTO.ESERVICE && userDTO.roleID == SessionConstants.SUPPORT_ENGINEER_ROLE)
													{
														%>
														<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=isLangEng ? "Task" : "কাজ"%>
															</label>
                                                            <div class="col-8">
																<select class='form-control'  name='solvedJob' id = 'solvedJob'  tag='pb_html'>		
																	<%
																	List<Support_engineers_diaryDTO> diaries = Support_engineers_diaryDAO.getInstance().getPending(support_ticketDTO.iD, userDTO.employee_record_id);
																	for(Support_engineers_diaryDTO support_engineers_diaryDTO: diaries)
																	{
																		%>
																		<option value='<%=support_engineers_diaryDTO.supportJobCat%>' ><%=CatDAO.getName(Language, "eservice", support_engineers_diaryDTO.supportJobCat)%></option>
																		<%
																	}
																	%>
																</select>
	
															</div>
                                                      </div>
														<%
													}
													%>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.SUPPORT_TICKET_ADD_TICKETSTATUSCAT, loginDTO)%></label>
                                                            <div class="col-8">
																<select class='form-control'  name='ticketStatusCat' id = 'ticketStatusCat_category_<%=i%>' onchange="hideThingy(this.value)"  tag='pb_html'>		
																	
																	
																	
																	<%
																	if(userDTO.roleID == SessionConstants.SUPPORT_ENGINEER_ROLE)
																	{
																		%>
																	<option value='<%=Support_ticketDTO.SOLVED%>' <%=support_ticketDTO.ticketStatusCat == Support_ticketDTO.SOLVED? "selected":"" %>><%=Language.equalsIgnoreCase("English")?"Solved":"সমাধান হয়েছে" %></option>
																	<%
																	}
																	%>
																	
																	<%
																	if(userDTO.roleID == SessionConstants.TICKET_ADMIN_ROLE 
																			|| userDTO.roleID == SessionConstants.ADMIN_ROLE )
																	{
																		%>
																		<option value='<%=Support_ticketDTO.OPEN%>' <%=support_ticketDTO.ticketStatusCat == Support_ticketDTO.OPEN? "selected":"" %> ><%=Language.equalsIgnoreCase("English")?"Open":"উন্মুক্ত" %></option>
																		<option value='<%=Support_ticketDTO.CLOSED%>' <%=support_ticketDTO.ticketStatusCat == Support_ticketDTO.CLOSED? "selected":"" %>><%=Language.equalsIgnoreCase("English")?"Closed":"নিষ্পন্ন" %></option>
																		<%
																	}
																	%>
																	
																	
																</select>
	
															</div>
                                                      </div>
                                                      <%
													}
													else
													{
														%>
														<input type="hidden"  name='ticketStatusCat' id = 'ticketStatusCat_category_<%=i%>'  tag='pb_html' value = '<%=support_ticketDTO.ticketStatusCat%>' />		
																	
														<%
													}
                                                      %>
                                                      
                                                      <%
                                                      if(isBoss)
                                                      {
                                                      %>
                                                       <div class="form-group row">
					                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.SUPPORT_TICKET_ADD_TICKETISSUESTYPE, loginDTO)%>
					                                        </label>
					                                        <div class="col-md-8">
					                                            <select class='form-control' required
					                                                    name='ticketIssuesType'
					                                                    id='ticketIssuesType_select_<%=i%>'
					                                                    onchange="getSubType('ticketIssuesSubtypeType_select_<%=i%>', this.value)"
					                                                    tag='pb_html'>
					                                                <%
					                                                    if (actionName.equals("edit")) {
					                                                        Options = CommonDAO.getOptions(Language, "select", "ticket_issues", "ticketIssuesType_select_" + i, "form-control", "ticketIssuesType", support_ticketDTO.ticketIssuesType + "");
					                                                    } else {
					                                                %>
					                                                <option value=""></option>
					                                                <%
					                                                        Options = CommonDAO.getOptions(Language, "select", "ticket_issues", "ticketIssuesType_select_" + i, "form-control", "ticketIssuesType");
					                                                    }
					                                                %>
					                                                <%=Options%>
					                                            </select>
					
					                                        </div>
					                                    </div>
                                    
				                                      <div class="form-group row" id = "subTypeDiv" > 
				                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.SUPPORT_TICKET_ADD_TICKETISSUESSUBTYPETYPE, loginDTO)%>
				                                        </label>
				                                        <div class="col-md-8">
				                                            <select class='form-control' 
				                                                    name='ticketIssuesSubtypeType'
				                                                    id='ticketIssuesSubtypeType_select_<%=i%>'
				                                                    tag='pb_html'>
				                                                <%
				                                                    if (actionName.equals("edit")) {
				                                                        Options = CommonDAO.getOptions(Language, "select", "ticket_issues_subtype", "ticketIssuesSubtypeType_select_" + i, "form-control", "ticketIssuesSubtypeType", support_ticketDTO.ticketIssuesSubtypeType + "");
				                                                    } else {
				                                                    	Options = "<option value = '-1'>" + LM.getText(LC.HM_SELECT, Language) + "</option>";
				                                                    }
				                                                %>
				                                                <%=Options%>
				                                            </select>
				
				                                        </div>
				                                    </div>
				                                    <%
                                                      }
				                                    %>
                                                      									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.HM_REMARKS, loginDTO)%></label>
                                                            <div class="col-8">
																<textarea class='form-control'  name='remarks' id = 'remarks_text_<%=i%>'    tag='pb_html'><%=forward_ticketDTO.remarks%></textarea>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.SUPPORT_TICKET_ADD_DUEDATE, loginDTO)%></label>
                                                            <div class="col-8">
																<%value = "dueDate_js_" + i;%>
																<jsp:include page="/date/date.jsp">
															   			<jsp:param name="DATE_ID" value="dueDate_date_js"></jsp:param>
															   			<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																	</jsp:include>	
																	<input type='hidden' class='form-control' readonly="readonly" data-label="Document Date" id = 'dueDate_date_<%=i%>' name='dueDate' value=	<%
																String formatted_dueDate = dateFormat.format(new Date(forward_ticketDTO.dueDate));
																%>
																'<%=formatted_dueDate%>'
															   tag='pb_html'>		
															</div>
                                                      </div>
                                                      
                                                      
	                                                <div class="form-group row">
	                                                    <label class="col-4 col-form-label text-right"><%=isLangEng?"Due Time":"ডিউর সময়"%>
	                                                    </label>
	                                                    <div class="col-8">
	                                                        <jsp:include page="/time/time.jsp">
	                                                            <jsp:param name="TIME_ID"
	                                                                       value="dueTime_time_js"></jsp:param>
	                                                            <jsp:param name="LANGUAGE"
	                                                                       value="<%=Language%>"></jsp:param>
	                                                            <jsp:param name="IS_AMPM" value="true"></jsp:param>
	                                                        </jsp:include>
	
	                                                        <input type='hidden' class="form-control"
	                                                               value="<%=timeFormat.format(new Date(support_ticketDTO.dueTime))%>"
	                                                               id='dueTime_time_<%=i%>' name='dueTime' tag='pb_html'/>
	
	
	                                                    </div>
	                                                </div>
		                                            
		                                            									
													<input type='hidden' class='form-control'  name='configuration' id = 'configuration_text_<%=i%>' value='<%=forward_ticketDTO.configuration%>'   tag='pb_html'/>										
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.SUPPORT_TICKET_ADD_FILESDROPZONE, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																fileColumnName = "filesDropzone";
																if(actionName.equals("edit"))
																{
																	List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(forward_ticketDTO.filesDropzone);
																%>			
																	<%@include file="../pb/dropzoneEditor.jsp"%>
																<%
																}
																else
																{
																	ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
																	forward_ticketDTO.filesDropzone = ColumnID;
																}
																%>
				
																<div class="dropzone" action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=forward_ticketDTO.filesDropzone%>">
																	<input type='file' style="display:none"  name='<%=fileColumnName%>File' id = '<%=fileColumnName%>_dropzone_File_<%=i%>'  tag='pb_html'/>			
																</div>								
																<input type='hidden'  name='<%=fileColumnName%>FilesToDelete' id = '<%=fileColumnName%>FilesToDelete_<%=i%>' value=''  tag='pb_html'/>
																<input type='hidden' name='<%=fileColumnName%>' id = '<%=fileColumnName%>_dropzone_<%=i%>'  tag='pb_html' value='<%=forward_ticketDTO.filesDropzone%>'/>		
		


															</div>
                                                      </div>
				
														

                                                      <div id = "empDiv">
                                                      
                                                      	 <div class="form-group row">
					                                        <label class="col-4 col-form-label text-md-right"><%=LM.getText(LC.SUPPORT_TICKET_ADD_PRIORITYCAT, loginDTO)%>
					                                        </label>
					                                        <div class="col-8">
					                                        <%
																if(isBoss)
																{
																%>
					                                            <select class='form-control' name='priorityCat'
					                                                    id='priorityCat_category_<%=i%>' tag='pb_html'>
					                                                <%
					                                                    Options = CatDAO.getOptions(Language, "priority", support_ticketDTO.priorityCat);
					                                                %>
					                                                <%=Options%>
					                                            </select>
																<%
																}
																else
																{
					                                    		%>
					                                    		<input type="text" readonly class='form-control'  
					                                    		value='<%=CatDAO.getName(Language, "priority", support_ticketDTO.priorityCat)%>' 
					                                    		 tag='pb_html' />																						
					                                    		
					                                    		<%
																}
					                                    		%>
					                                        </div>
					                                    </div>
					                                    
                                                      	<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.HM_ACTION, loginDTO)%></label>
                                                            <div class="col-8">
																
																<select name='forwardActionCat' id = 'forwardActionCat'	class='form-control'  onchange = 'fillOptions()'>
																<%
																if(isBoss)
																{
																%>
																<option value='<%=Ticket_forward_historyDTO.TICKET_ASSIGN%>'><%=Language.equalsIgnoreCase("English")?"Assign":"দায়িত্ব দিন" %></option>
																<option value='<%=Ticket_forward_historyDTO.FORWARD%>'><%=Language.equalsIgnoreCase("English")?"Forward":"ফরওয়ার্ড করুন" %></option>
																<%
																}
																
																%>
																</select>					
															</div>
                                                   		</div>	
                                                   		
                                                   		<div id = "bossOptions" style = "display:none">
                                                   			<option value = "-1"></option>
																 <%
				                                                    Set<Long> admins = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.TICKET_ADMIN_ROLE);
																 	
				                                                    for(Long employee: admins)
				                                                    {
				                                                    	boolean employeCanBeAdded = true;
				                                                    	
				                                                    	if(employeCanBeAdded)
				                                                    	{
				                                                    	
				                                                    %>
				                                                    <option value = "<%=employee%>" >
				                                                     <%=WorkflowController.getNameFromOrganogramId(employee, Language)%>,
				                                                     <%=WorkflowController.getOfficeNameFromOrganogramId(employee, Language)%>
				                                                    </option>
				                                                    <%
				                                                    	}
				                                                    }
				                                                    %>
                                                   		</div>	
                                                   		
                                                   		<div id = "employeeOptions" style = "display:none">
                                                   			
																<option value = "-1"></option>
																 <%
																 	Set<Long> engineers = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.SUPPORT_ENGINEER_ROLE);
																 	List<Long> addables = new ArrayList<Long>();
																 	
																   
																 	Ticket_issuesDTO ticket_issuesDTO = Ticket_issuesRepository.getInstance().getById(support_ticketDTO.ticketIssuesType);
		                                                    		long admin = ticket_issuesDTO.adminOrganogramId;
		                                                    		long adminOffice = WorkflowController.getOfficeIdFromOrganogramId(admin);
		                                                    		System.out.println("adminOffice = " + adminOffice);
				                                                    for(Long employee: engineers)
				                                                    {
				                                                    	boolean employeCanBeAdded = false;
				                                                    	if(userDTO.roleID == SessionConstants.ADMIN_ROLE)
				                                                    	{
				                                                    		employeCanBeAdded = true;
				                                                    	}
				                                                    	else
				                                                    	{
				                                                    		
				                                                    		long employeeOffice = WorkflowController.getOfficeIdFromOrganogramId(employee);
				                                                    		if(adminOffice == SessionConstants.MAINTENANCE1_OFFICE || adminOffice == SessionConstants.MAINTENANCE2_OFFICE)
				                                                    		{
				                                                    			if(employeeOffice == SessionConstants.MAINTENANCE1_OFFICE || employeeOffice == SessionConstants.MAINTENANCE2_OFFICE)
				                                                    			{
				                                                    				employeCanBeAdded = true;
				                                                    			}
				                                                    			
				                                                    		}
				                                                    		else if(adminOffice == employeeOffice)
				                                                    		{
				                                                    			employeCanBeAdded = true;
				                                                    		}
				                                                    		else
				                                                    		{
				                                                    			System.out.println("employeeOffice = " + employeeOffice);
				                                                    		}
				                                                    	}
				                                                    	if(employeCanBeAdded)
				                                                    	{
				                                                    		System.out.println("adding " + employee);
				                                                    		addables.add(employee);			                                                   
				                                                    	}
				                                                    	else
				                                                    	{
				                                                    		System.out.println("not adding " + employee);
				                                                    	}
				                                                    }
				                                                    for(Long employee: addables)
				                                                    {
				                                                    	 %>
						                                                    <option value = "<%=employee%>" >
						                                                     <%=WorkflowController.getNameFromOrganogramId(employee, Language)%>
						                                                    </option>
						                                                    <%
				                                                    }
				                                                    %>
                                                   		</div>
                                                   		
                                                   		<div class="form-group row" id = "eServiceDiv" style = "display:none">
                                                   			<label class="col-4 col-form-label text-right"><%=Language.equalsIgnoreCase("english")?"Employee Name":"কর্মকর্তার নাম"%></label>
                                                            <div class="col-8">
																<table id="esTable" class="table table-bordered table-striped">
																<%
																if(support_ticketDTO.supportJobsArray != null && support_ticketDTO.ticketIssuesType == Ticket_issuesDTO.ESERVICE)
																{
																	for(long es: support_ticketDTO.supportJobsArray)
																	{
																		%>
																		<tr>
																			<td><%=CatDAO.getName(Language, "eservice", (int)es)%></td>
																			<td>
																				<select name='orgId' 	class='form-control'>
																					<option value = "-1"></option>
																					<%
																					 	for(Long employee: addables)
									                                                    {
									                                                    	 %>
											                                                    <option value = "<%=employee%>" >
											                                                     <%=WorkflowController.getNameFromOrganogramId(employee, Language)%>
											                                                    </option>
											                                                    <%
									                                                    }
								
																					%>
																				</select>
																			</td>
																		</tr>
																		<%
																	}
																}
																
																%>
                                        						</table>					
															</div>
                                                   		</div>	
                                                   		
                                                   									
														<div class="form-group row" id = "normalServiceDiv">
                                                            <label class="col-4 col-form-label text-right"><%=Language.equalsIgnoreCase("english")?"Employee Name":"কর্মকর্তার নাম"%></label>
                                                            <div class="col-8">
																
																<select name='orgId' id = 'orgId'	class='form-control' style="width: 100%">
																	<option value = "-1"></option>
																</select>					
															</div>
                                                   		</div>
                                                      </div>
                                                      
                                                      <div id="expertiseDiv" style="display:none">
                                                      	<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.HM_EXPERTISE, loginDTO)%></label>
                                                            <div class="col-8">
																	<select class='form-control'  name='expertise' id = 'expertise'  tag='pb_html'>		
																	<%
																	Options = CatDAO.getOptions(Language, "user_expertise", user_expertise_mapDTO.userExpertiseCat);
																	%>
																	<%=Options%>
																</select>					
															</div>
                                                   		</div>
                                                   		<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.HM_REMARKS_ABOUT_EXPERTISE, loginDTO)%></label>
                                                            <div class="col-8">
																	<textarea 	class='form-control'  name='expertiseRemarks' id = 'expertiseRemarks'><%=user_expertise_mapDTO.remarks%></textarea>	
															</div>
                                                   		</div>
                                                      </div>
                                                      
                                                      <div id="expertiseViewDiv" style="display:block">
                                                      	<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.HM_EXPERTISE, loginDTO)%></label>
                                                            <div class="col-8">
																	<input type="text" readonly class='form-control'  name='expertiseView' id = 'expertiseView' value='<%=CatRepository.getInstance().getText(Language, "user_expertise", user_expertise_mapDTO.userExpertiseCat) %>'  tag='pb_html' />																						
															</div>
                                                   		</div>
                                                   		<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.HM_REMARKS_ABOUT_EXPERTISE, loginDTO)%></label>
                                                            <div class="col-8">
																	<input type="text" readonly class='form-control'  name='expertiseRemarksView' id = 'expertiseRemarksView' value='<%=user_expertise_mapDTO.remarks%>'  tag='pb_html' />																						
															</div>
                                                   		</div>
                                                      </div>
                                                      
                                                  	
													</div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>                               
                          </div>
                       </div>
                       <div class="form-actions text-center mb-5">
	                        <a class="btn btn-danger" href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.FORWARD_TICKET_ADD_FORWARD_TICKET_CANCEL_BUTTON, loginDTO)%></a>										
							<button class="btn btn-success" type="submit"><%=LM.getText(LC.FORWARD_TICKET_ADD_FORWARD_TICKET_SUBMIT_BUTTON, loginDTO)%></button>
					   </div>
                   </form>
               </div>                      
          </div>
      </div>
 </div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, validate)
{
	var dueDate = getDateStringById('dueDate_date_js', 'DD/MM/YYYY');
	$("#dueDate_date_0").val(dueDate);
	
	var dueTime = getTimeById('dueTime_time_js');
    console.log("dueTime = " + dueTime);
    $("#dueTime_time_0").val(getTimeById('dueTime_time_js', true));
    console.log($("#dueTime_time_0").val());
	
	if($("#ticketStatusCat_category_0").val() == <%=Support_ticketDTO.OPEN%> && $("#orgId").val() == -1 && <%=support_ticketDTO.ticketIssuesType != Ticket_issuesDTO.ESERVICE%>)
	{
		toastr.error("Please select an employee to forward");
		return false;
	}
	else if($("#ticketStatusCat_category_0").val() == <%=Support_ticketDTO.OPEN%> &&  <%=support_ticketDTO.ticketIssuesType == Ticket_issuesDTO.ESERVICE%>)
	{
		var taskCount = <%=support_ticketDTO.supportJobsArray.size()%>;
		var chosenCount = 0;
		$('[name="orgId"]').each(function(){
	        if(parseInt($(this).val()) != -1)
	        {
	        	 chosenCount ++;
	        }
		});
		if(chosenCount != taskCount)
		{
			toastr.error("Please select " + taskCount + " employee(s). You have selected only " + chosenCount);
			return false;
		}
		
	}

	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Forward_ticketServlet");	
}

function init(row)
{
	setDateByStringAndId('dueDate_date_js', $("#dueDate_date_<%=i%>").val());
	setTimeById('dueTime_time_js', $("#dueTime_time_0").val());

	
}

var row = 0;
$(document).ready(function(){
	init(row);
	
	fillOptions();
	/*$("#orgId").select2({
        dropdownAutoWidth: true,
        theme: "classic"
    });*/
    
    hideThingy($("#ticketStatusCat_category_<%=i%>").val());

});	

var child_table_extra_id = <%=childTableStartingID%>;

function patient_inputted(userName, orgId)
{
	$("#orgId").val(orgId);
}

function hideThingy(value)
{
	if(value == <%=Support_ticketDTO.CLOSED%> || value == <%=Support_ticketDTO.SOLVED%>)//ending
	{
		$("#dateDiv").css("display", "none");
		$("#empDiv").css("display", "none");
		$("#expertiseDiv").css("display", "block");
		$("#expertiseViewDiv").css("display", "none");
	}
	else
	{
		$("#dateDiv").css("display", "block");
		$("#empDiv").css("display", "block");
		$("#expertiseDiv").css("display", "none");
		$("#expertiseViewDiv").css("display", "block");
	}
}

function getSubType(childElement, value) {
    console.log("getting Element " + value);
    $("#" + childElement).val(-1);


    

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById(childElement).innerHTML = this.responseText;
            var count = (this.responseText.match("</option>") || []).length;
            //console.log("this.responseText = " + this.responseText);
            if(count >= 1)
           	{
            	$("#subTypeDiv").removeAttr("style");
           	}
            else
           	{
            	$("#subTypeDiv").css("display", "none");
           	}
           
        } else if (this.readyState == 4 && this.status != 200) {
            alert('failed ' + this.status);
        }
    };

    console.log("selected value = " + value);

    xhttp.open("POST", "Support_ticketServlet?actionType=getSubType&type="
        + value + "&language=<%=Language%>", true);
    xhttp.send();


}

function fillOptions()
{
	if($("#forwardActionCat").val() == '<%=Ticket_forward_historyDTO.TICKET_ASSIGN%>')
	{
		if(<%=support_ticketDTO.ticketIssuesType == Ticket_issuesDTO.ESERVICE%>)
		{
			$("#normalServiceDiv").hide();
			$("#eServiceDiv").show();
			
		}
		else
		{
			$("#orgId").html($("#employeeOptions").html());
			$("#orgId").attr("multiple","multiple");
			$("#orgId").select2({
		        dropdownAutoWidth: true,
		        theme: "classic"
		    });
			$("#normalServiceDiv").show();
			$("#eServiceDiv").hide();
		}
		

	}
	else
	{
		$("#orgId").html($("#bossOptions").html());
		$("#orgId").removeAttr("multiple");
		$("#orgId").select2({
	        dropdownAutoWidth: true,
	        theme: "classic"
	    });
		$("#normalServiceDiv").show();
		$("#eServiceDiv").hide();
	}
	
	
}
</script>






