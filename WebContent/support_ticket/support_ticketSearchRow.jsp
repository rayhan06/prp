<%@page import="support_ticket.Support_ticketDAO"%>
<%@page import="user.UserRepository"%>
<%@page import="support_ticket.Support_ticketDTO"%>
<%@page pageEncoding="UTF-8" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="workflow.OrganogramDetails" %>
<%@ page import="ticket_issues.*" %>

<%@ page import="util.StringUtils" %>

<%
String style = "";
if(support_ticketDTO.priorityCat == Support_ticketDTO.HIGH)
{
	style = "style = 'color:red'";
}
long now = System.currentTimeMillis();
%>

<td <%=style%>>
    <b><%=Utils.getDigits(support_ticketDTO.iD, isLangEng)%>
    </b>
    <br>
     <%=Utils.getDigits(simpleDateFormat.format(new Date(support_ticketDTO.insertionDate)), Language)%>
</td>

<td <%=style%>>

    <%
    if (support_ticketDTO.issueRaiserOrganoramId >= SessionConstants.OFFICE_OFFSET) 
    {
    	%>
    	<b><%=isLangEng ? "Office" : "অফিস"%>:
        <%=WorkflowController.getNameFromOrganogramId(support_ticketDTO.issueRaiserOrganoramId , isLangEng)%>
    	</b>
    	<%
    }
    else if (support_ticketDTO.roomNoCat == -1) 
    {
            OrganogramDetails organogramDetails = WorkflowController.getOrganogramDetailsByOrganogramId(support_ticketDTO.issueRaiserOrganoramId, isLangEng);
    %>
    <b><%=organogramDetails.empName%></b>
    <%
    if(!organogramDetails.organogramName.equalsIgnoreCase(""))
    {
    %>
    <br><%=organogramDetails.organogramName%>,<%=organogramDetails.officeName%>
    <%
    }
    %>
    <br><%=organogramDetails.empAddress%>
    <br><%=organogramDetails.empMobileNo%>
    <br><%=organogramDetails.username%>
    <%
    } else {
    %>
    <b><%=isLangEng ? "Room" : "ঘর"%>:
        <%=CatRepository.getInstance().getText(isLangEng, "room_no", support_ticketDTO.roomNoCat)%>
    </b>
    <%
        }
    %>


</td>

<td <%=style%>>
    <%=support_ticketDTO.description%>
</td>


<td <%=style%>>
    <%=Ticket_issuesRepository.getInstance().getText(support_ticketDTO.ticketIssuesType,isLangEng)%><br>
    <%=TicketIssueSubTypeRepository.getInstance().getText(support_ticketDTO.ticketIssuesSubtypeType,isLangEng)%><br>
</td>


<td <%=style%>>
    <%=WorkflowController.getWingNameFromOrganogramId(support_ticketDTO.issueRaiserOrganoramId, isLangEng)%>
</td>

<td <%=style%>>
    <%=CatRepository.getInstance().getText(isLangEng, "priority", support_ticketDTO.priorityCat)%>
</td>


<td <%=style%>>
    <%=CatRepository.getInstance().getText(isLangEng, "ticket_status", support_ticketDTO.ticketStatusCat)%>
    <%
    if(support_ticketDTO.ticketStatusCat == Support_ticketDTO.CLOSED)
    {
    	%>
    	<br><%=Utils.getDigits(simpleDateFormat.format(new Date(support_ticketDTO.closingDate)), Language)%>
    	<%
    }
    %>
    <%
    if(support_ticketDTO.ticketStatusCat == Support_ticketDTO.OPEN && support_ticketDTO.dueDate != 0)
    {
    	%>
    	<br><b><%=isLangEng?"Deadline:":"ডেডলাইন :"%></b> <%=dateFormat.format(new Date(support_ticketDTO.dueDate))%> <%=timeFormat.format(new Date(support_ticketDTO.dueTime))%>
    	<%
    }
    %>
     <%
    if(support_ticketDTO.ticketStatusCat == Support_ticketDTO.OPEN && now > support_ticketDAO.getCrossTime(support_ticketDTO) && support_ticketDTO.dueDate != 0)
    {
    	%>
    	<br><b><%=isLangEng?"Deadline Crossed":"ডেডলাইন অতিক্রান্ত"%></b>
    	
    	<%
    }
    %>
    
    <%
    if(support_ticketDTO.ticketIssuesType == Ticket_issuesDTO.NOC_ISSUE)
    {
    	%>
    	<br><b><%=isLangEng?"NOC Status":"এন ও স্যার অবস্থা"%></b>:
    	<%=CatRepository.getInstance().getText(isLangEng, "noc_status", support_ticketDTO.nocStatusCat)%>
    	<%
    }
    %>
</td>


<td <%=style%>>
    <%
        if (support_ticketDTO.currentAssignedOrganoramId != -1) 
        {
        	
            OrganogramDetails organogramDetails = WorkflowController.getOrganogramDetailsByOrganogramId(support_ticketDTO.currentAssignedOrganoramId,isLangEng);
            if(!organogramDetails.empName.equalsIgnoreCase(""))
            {
		    %>
		    <b><%=organogramDetails.empName%>, <%=organogramDetails.username%></b>
		    <%
            }
        }
    %>
    
    <%
    if(support_ticketDTO.origSupportOrgsArray != null && !support_ticketDTO.origSupportOrgsArray.isEmpty())
	{
    	%>
    <br><br><b><%=isLangEng ? "Assigned Engineers" : " নিযুক্ত ইঞ্জিনিয়ার"%>
    :</b><br>
   		<%
   		
   		for(long org: support_ticketDTO.origSupportOrgsArray)
   		{
   			OrganogramDetails orgD = WorkflowController.getOrganogramDetailsByOrganogramId(org, isLangEng);
   			%>
   			<b><%=orgD.empName%>  , <%=orgD.username%>:</b> <%=orgD.empMobileNo%> <br>
   			<%
   		}
    	
   		%>
    	<%
	}
    %>
    
    <%
    if(support_ticketDTO.supportOrgsArray != null  && !support_ticketDTO.origSupportOrgsArray.isEmpty())
	{
    	%>
    <br><b><%=isLangEng ? "Active Engineers" : "কর্মরত ইঞ্জিনিয়ার"%>
    :</b><br>
   		<%
   		
   		for(long org: support_ticketDTO.supportOrgsArray)
   		{
   			OrganogramDetails orgD = WorkflowController.getOrganogramDetailsByOrganogramId(org, isLangEng);
   			%>
   			<b><%=orgD.empName%>  , <%=orgD.username%>:</b> <%=orgD.empMobileNo%> <br>
   			<%
   		}
    	
   		%>
    	<%
	}
    %>
    

    
   

</td>