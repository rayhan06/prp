<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@ page import="ticket_forward_history.Ticket_forward_historyDAO" %>
<%@ page import="ticket_forward_history.Ticket_forward_historyDTO" %>

<div class="table-section">
    <div class="mt-5">
        <h5 class="table-title">
            <%=LM.getText(LC.HM_HISTORY, userDTO)%>
        </h5>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th><%=LM.getText(LC.HM_DATE, userDTO)%>
                    </th>
                    <th><%=LM.getText(LC.HM_ACTION, userDTO)%>
                    </th>
                    <th><%=LM.getText(LC.HM_STATUS, userDTO)%>
                    </th>
                    <th><%=isLangEng ? "Action Taker" : "পদক্ষেপ গ্রহীতা"%>
                    </th>
                    <th><%=LM.getText(LC.TICKET_FORWARD_HISTORY_ADD_FORWARDTOUSERID, userDTO)%>
                    </th>
                    <th><%=LM.getText(LC.SUPPORT_TICKET_ADD_TICKETISSUESTYPE, userDTO)%>
                    </th>
                    <th><%=isLangEng ? "Task" : "কাজ"%>
                    </th>
                    <th><%=LM.getText(LC.TICKET_FORWARD_HISTORY_ADD_COMMENTS, userDTO)%>
                    </th>


                    <th><%=LM.getText(LC.HM_ATTACHMENTS, userDTO)%>
                    </th>


                </tr>
                </thead>
                <tbody>
                <%
                    List<Ticket_forward_historyDTO> ticket_forward_historyDTOs = new Ticket_forward_historyDAO().getAllByTicketId(support_ticketDTO.iD);
                    for (Ticket_forward_historyDTO ticket_forward_historyDTO : ticket_forward_historyDTOs) {
                %>
                <tr>
                    <td>
                        <%=Utils.getDigits(simpleDateFormat.format(new Date(ticket_forward_historyDTO.forwardDate)), Language)%>
                    </td>

                    <td>
                        <%=CatRepository.getName(Language, "assignment_status", ticket_forward_historyDTO.forwardActionCat)%>
                    </td>

                    <td>
                    <%
                    if(!isNOC)
                    {
                    %>
                        <%=CatRepository.getName(Language, "ticket_status", ticket_forward_historyDTO.ticketStatusCat)%>
                    <%
                    }
                    else
                    {
                    	%>
                    	<%=CatRepository.getName(Language, "noc_status", ticket_forward_historyDTO.nocStatusCat)%>
                    	<%
                    }
                    %>
                    </td>

                    <td>
                        <%
                            OrganogramDetails forwardOrgDetails = WorkflowController.getOrganogramDetailsByOrganogramId(ticket_forward_historyDTO.forwardFromOrganogramId,isLangEng);
                        %>
                        <%=forwardOrgDetails.empName%>
                    </td>


                    <td>
                    
                    	<%
                        	List<Long> forwardTos = Utils.StringToArrayList(ticket_forward_historyDTO.forwardToOrganogramId);
	                        {
	                        	int i = 0;
	                        	for(long org: forwardTos)
	                        	{
	                        		%>
	                        		<%=i>0?", ":"" %>
	                        		<%=WorkflowController.getNameFromOrganogramId(org, isLangEng) %>
	                        		
	                        		<%
	                        		i++;
	                        	}
	                        }
                            
                        %>
                       
                    </td>

                    <td>
                        <%=Ticket_issuesRepository.getInstance().getText(ticket_forward_historyDTO.ticketIssuesType,isLangEng)%><br>
                        <%=TicketIssueSubTypeRepository.getInstance().getText(ticket_forward_historyDTO.ticketIssuesSubtypeType,isLangEng)%><br>
                    </td>
                    
                    <td>
                    <%=CatDAO.getName(Language, "eservice", ticket_forward_historyDTO.taskDone)%>
                    </td>

                    <td>
                        <%=ticket_forward_historyDTO.comments%>
                    </td>


                    <td>
                        <%

                            FilesDTOList = filesDAO.getMiniDTOsByFileID(ticket_forward_historyDTO.filesDropzone);
                            if (FilesDTOList != null && FilesDTOList.size() > 0) {
                        %>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.HM_FILE_NAME, userDTO)%>
                                </th>

                                <th><%=LM.getText(LC.HM_DOWNLOAD, userDTO)%>
                                </th>

                            </tr>
                            </thead>
                            <tbody>

                            <%
                                if (FilesDTOList != null) {
                                    for (int j = 0; j < FilesDTOList.size(); j++) {
                                        FilesDTO filesDTO = FilesDTOList.get(j);
                                        byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                            %>
                            <tr>
                                <td>
                                    <%
                                        if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                    %>
                                    <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                         style='width:100px'/><br>
                                    <%
                                        }
                                    %>
                                    <%=filesDTO.fileTitle%>
                                </td>

                                <td>
                                    <a href='Support_ticketServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                       download><i class="fa fa-download" aria-hidden="true"></i>
                                    </a>
                                </td>

                            </tr>
                            <%
                                    }
                                }
                            %>
                            </tbody>
                        </table>
                        <%
                            }
                        %>
                    </td>
                </tr>
                <%
                    }
                %>
                </tbody>
            </table>
        </div>
    </div>
</div>