<%@page import="employee_records.Employee_recordsRepository"%>
<%@page import="user.*"%>
<%@page import="support_ticket.Support_ticketDAO"%>
<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page import="ticket_issues.Ticket_issuesRepository" %>
<%@ page import="ticket_issues.TicketIssueSubTypeRepository" %>
<%@page import="employee_offices.*" %>
<%@page contentType="text/html;charset=utf-8" %>

<%
    System.out.println("Inside nav.jsp");
    String url = request.getParameter("url");
    String navigator = request.getParameter("navigator");
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";
    String pageno = "";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String[][] searchFieldInfo = rn.getSearchFieldInfo();
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";
    int row = 0;

    String Language = LM.getText(LC.SUPPORT_TICKET_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    int pagination_number = 0;
    boolean isPermanentTable = rn.m_isPermanentTable;
    System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
    Support_ticketDAO support_ticketDAO = new Support_ticketDAO();
    boolean isLangEng = Language.equalsIgnoreCase("english");
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <%
                    out.println("<input placeholder='অনুসন্ধান করুন' autocomplete='off' type='text' class='form-control border-0' onKeyUp='allfield_changed(\"\",0)' id='anyfield'  name='" + LM.getText(LC.ASSET_MANUFACTURER_SEARCH_ANYFIELD, loginDTO) + "' ");
                    String value = (String) session.getAttribute(searchFieldInfo[searchFieldInfo.length - 1][1]);

                    if (value != null) {
                        out.println("value = '" + value + "'");
                    } else {
                        out.println("value=''");
                    }

                    out.println("/><br />");
                %>
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>

    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.SUPPORT_TICKET_ADD_TICKETISSUESTYPE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='ticket_issues_type' id='ticket_issues_type'
                                    onchange="getSubType(this.value)">
                                <%=Ticket_issuesRepository.getInstance().buildOptions(Language,-1)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.SUPPORT_TICKET_ADD_TICKETISSUESSUBTYPETYPE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='ticket_issues_subtype_type'
                                    id='ticket_issues_subtype_type' onSelect='setSearchChanged()'>
                                <%=TicketIssueSubTypeRepository.getInstance().buildOptions(Language,-1,-1)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.SUPPORT_TICKET_ADD_TICKETSTATUSCAT, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='ticket_status_cat' id='ticket_status_cat'
                                    onSelect='setSearchChanged()'>
                                <%=CatRepository.getInstance().buildOptions("ticket_status",Language,-1)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_ISSUE_RAISER, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                        	<button type="button" class="btn btn-primary form-control shadow btn-border-radius mb-3"
                                                    onclick="addEmployee()"
                                                    id="addToTrainee_modal_button"><%=LM.getText(LC.HM_ADD_EMPLOYEE, loginDTO)%>
                                            </button>
                                            <table class="table table-bordered table-striped text-nowrap">
                                                <tbody id="employeeToSet"></tbody>
                                            </table>
                                            <input type='hidden' name='issue_raiser_organogram_id'
                                                   id='issue_raiser_organogram_id' tag='pb_html' value='-1'/>
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.SUPPORT_TICKET_ADD_CURRENTASSIGNEDUSERID, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='current_assigned_organogram_id' id='current_assigned_organogram_id'
                                    onSelect='setSearchChanged()'>
                                <option value='-1'><%=LM.getText(LC.HM_SELECT, loginDTO) %></option>
                                <%
                                Set<Long> engineersAndAdmins = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.TICKET_ADMIN_ROLE);
                                engineersAndAdmins.addAll(EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.SUPPORT_ENGINEER_ROLE));
                                for(Long assignedTo: engineersAndAdmins)
                                {
                                	UserDTO assignedToDTO = UserRepository.getUserDTOByOrganogramID(assignedTo);
                                	if(assignedToDTO != null)
                                	{
                                	%>
                                	<option value='<%=assignedToDTO.organogramID%>'>
                                	<%=Employee_recordsRepository.getInstance().getByUserName(assignedToDTO.userName, Language) %>
                                	</option>
                                	<%
                                	}
                                }
                                %>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" >
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                        <%=Language.equalsIgnoreCase("english")?"Ticket#":"টিকেট#"%>
                        </label>
                        <div class="col-md-8">
                        	<input type="text" id="ticket_id" name="ticket_id" value = '' class='form-control'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" style="display:none">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.SUPPORT_TICKET_ADD_DUEDATE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="due_date_start_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type="hidden" id="due_date_start" name="due_date_start">
                        </div>
                    </div>
                </div>
                <div class="col-md-6" style="display:none">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.SUPPORT_TICKET_ADD_DUEDATE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="due_date_end_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type="hidden" id="due_date_end" name="due_date_end">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=Language.equalsIgnoreCase("english")?"From Opening Date":"খোলার সময় থেকে"%> 
                        </label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="insertion_date_start_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type="hidden" id="insertion_date_start" name="insertion_date_start">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=Language.equalsIgnoreCase("english")?"To Opening Date":"খোলার সময় পর্যন্ত"%>
                        </label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="insertion_date_end_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type="hidden" id="insertion_date_end" name="insertion_date_end">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.SUPPORT_TICKET_ADD_CLOSINGDATE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="closing_date_start_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type="hidden" id="closing_date_start" name="closing_date_start">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.SUPPORT_TICKET_ADD_CLOSINGDATE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="closing_date_end_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type="hidden" id="closing_date_end" name="closing_date_end">
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                        <%=isLangEng ? "Room" : "কক্ষ"%>

                        </label>
                        <div class="col-md-8">
                            <select class='form-control'  name='room_no_cat' id = 'room_no_cat' >		
								<%		
								Options = CatDAO.getOptions(Language, "room_no", -1);								
								%>
								<%=Options%>
							</select>
                        </div>
                    </div>
                </div>                
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                        <%=isLangEng ? "Deadline Status" : "ডেডলাইনের অবস্থা"%>

                        </label>
                        <div class="col-md-8">
                            <select class='form-control'  name='deadline' id = 'deadline' >		
								<option value = "-1"><%=LM.getText(LC.HM_SELECT, loginDTO) %></option>
								<option value = "1">
									<%=isLangEng ? "Deadline Crossed" : "ডেডলাইন অতিক্রান্ত"%>								
								</option>
								<option value = "0">
									<%=isLangEng ? "Deadline Not Crossed" : "ডেডলাইন অনতিক্রান্ত"%>								
								</option>
							</select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->

<div style="padding-right: 20px; padding-bottom: 20px">
	<button type="button"
	        class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
	        onclick="allfield_changed('',0, 1)"
	        style="background-color: #a8b194; float: right;">
	    <%=LM.getText(LC.HM_EXCEL, loginDTO)%>
	</button>
</div>


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">


    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        console.log(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number, type) {
        var params = 'AnyField=' + document.getElementById('anyfield').value;

        if ($("#ticket_issues_type").val() != -1) {
            params += '&ticket_issues_type=' + $("#ticket_issues_type").val();
        }
        if ($("#ticket_issues_subtype_type").val() != -1) {
            params += '&ticket_issues_subtype_type=' + $("#ticket_issues_subtype_type").val();
        }
        
        if ($("#ticket_id").val() != "") {
            params += '&ticket_id=' + $("#ticket_id").val();
        }

        if ($("#ticket_status_cat").val() != -1) {
            params += '&ticket_status_cat=' + $("#ticket_status_cat").val();
        }
        if ($("#room_no_cat").val() != -1) {
            params += '&room_no_cat=' + $("#room_no_cat").val();
        }
        if ($("#deadline").val() != -1) {
            params += '&deadline=' + $("#deadline").val();
        }
        $("#due_date_start").val(getDateStringById('due_date_start_js', 'DD/MM/YYYY'));
        params += '&due_date_start=' + getBDFormattedDate('due_date_start');
        $("#due_date_end").val(getDateStringById('due_date_end_js', 'DD/MM/YYYY'));
        params += '&due_date_end=' + getBDFormattedDate('due_date_end');

        if ($("#current_assigned_organogram_id").val() != -1) {
            params += '&current_assigned_organogram_id=' + $("#current_assigned_organogram_id").val();
        }
        if ($("#issue_raiser_organogram_id").val() != -1) {
            params += '&issue_raiser_organogram_id=' + $("#issue_raiser_organogram_id").val();
        }

       
        var bdfDate;
        
        $("#insertion_date_start").val(getDateStringById('insertion_date_start_js', 'DD/MM/YYYY'));
        bdfDate = getBDFormattedDate('insertion_date_start');
        if(bdfDate != '')
       	{
        	params += '&insertion_date_start=' + bdfDate;
       	}
        
        
        $("#insertion_date_end").val(getDateStringById('insertion_date_end_js', 'DD/MM/YYYY'));
        bdfDate = getBDFormattedDate('insertion_date_end');
        if(bdfDate != '')
       	{
        	params += '&insertion_date_end=' + (parseInt(bdfDate) + 86400000);
       	}
        


        $("#closing_date_start").val(getDateStringById('closing_date_start_js', 'DD/MM/YYYY'));
        bdfDate = getBDFormattedDate('closing_date_start');
        if(bdfDate != '')
       	{
        	params += '&closing_date_start=' + bdfDate;
       	}

        $("#closing_date_end").val(getDateStringById('closing_date_end_js', 'DD/MM/YYYY'));
        bdfDate = getBDFormattedDate('closing_date_end');
        if(bdfDate != '')
       	{
        	params += '&closing_date_end=' + (parseInt(bdfDate) + 86400000);
       	}
       

        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        if(type == 0 || typeof(type) == "undefined")
		{
			dosubmit(params);
		}
		else
		{
			var url =  "Support_ticketServlet?actionType=xl&isPermanentTable=<%=isPermanentTable%>&" + params;
			window.location.href = url;
		}

    }
    
    function patient_inputted(userName, orgId) {
        console.log('patient inputted value: ' + userName);

        $("#issue_raiser_organogram_id").val(orgId);
    }

    function getSubType(value) {
        document.getElementById('ticket_issues_subtype_type').innerHTML = "";
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('ticket_issues_subtype_type').innerHTML = this.responseText;

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("GET", "option_Servlet?actionType=ticket_issues_subtype&ticket_issues="
            + value + "&ticket_issues_sub_type=-1", true);
        xhttp.send();
    }

</script>

