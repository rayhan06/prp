<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>

<%@page import="support_ticket.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="ticket_issues.*" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    Support_ticketDTO support_ticketDTO;
    support_ticketDTO = (Support_ticketDTO) request.getAttribute("support_ticketDTO");
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    if (support_ticketDTO == null) {
        support_ticketDTO = new Support_ticketDTO();

    }

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.SUPPORT_TICKET_ADD_SUPPORT_TICKET_ADD_FORMNAME, userDTO);
    String servletName = "Support_ticketServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    int i = 0;

    long ColumnID = -1;
    FilesDAO filesDAO = new FilesDAO();
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    Ticket_issuesDTO eserviceDTO = Ticket_issuesRepository.getInstance().getById(Ticket_issuesDTO.ESERVICE);
%>
<div style = "display:none" id = "esTemplate">
<%
eserviceDTO.ticketIssuesSubtypeDTOList = TicketIssueSubTypeRepository.getInstance().getByTicketIssuesId(eserviceDTO.iD);
for(TicketIssuesSubtypeDTO ticketIssuesSubtypeDTO: eserviceDTO.ticketIssuesSubtypeDTOList)
{
	%>
	<table id = "es_<%=ticketIssuesSubtypeDTO.iD%>">
		
		<%
		for (long es: ticketIssuesSubtypeDTO.eserviceCatsArray)
		{
			%>
			<tr>
			<td><%=CatDAO.getName(Language, "eservice", (int)es)%></td>
			<td>
			<input type='checkbox' class='form-control-sm'
              name='es'
                                           	
              value='<%=es%>' tag='pb_html'/>
			</td>
			</tr>
			<%
		}
		%>
		
	</table>
	<%
}
%>
</div>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Support_ticketServlet?actionType=<%=actionName%>&isPermanentTable=true"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting()">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row mx-2">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD'
                                           id='iD_hidden_<%=i%>' value='<%=support_ticketDTO.iD%>'
                                           tag='pb_html'/>


                                    <input type='hidden' class='form-control'
                                           name='departmentDatetimeId'
                                           id='departmentDatetimeId_hidden_<%=i%>'
                                           value='<%=support_ticketDTO.departmentDatetimeId%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="roomCB">
                                            <%=isLangEng ? "Room Related Issue" : "কক্ষ সংক্রান্ত সমস্যা"%>
                                        </label>
                                        
                                         <input type='checkbox' class='form-control-sm mt-1' name='roomCB'
                                                id='roomCB'
                                                onchange="showRoom()"
                                                tag='pb_html'/>
                                         <label class="col-md-4 col-form-label text-md-right" for="roomCB">
                                           <%=isLangEng ? "Office Related Issue" : "অফিস সংক্রান্ত সমস্যা"%>
                                       </label>
                                       <input type='checkbox' class='form-control-sm mt-1' name='officeCB'
                                                 id='officeCB'
                                                 onchange="showOffice()"
                                                 tag='pb_html'/>
                                        
                                    </div>

                                    <div class="form-group row" id="roomDiv" style="display:none">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=isLangEng ? "Room" : "কক্ষ"%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='roomNoCat'
                                                    id='roomNoCat' tag='pb_html' onchange = "setOrgFromRoom(this.value)">
                                                <%=CatRepository.getInstance().buildOptions("room_no", Language, support_ticketDTO.roomNoCat)%>
                                            </select>

                                        </div>
                                    </div>
                                    
                                    <div class="form-group row" id="officeDiv" style="display:none">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=isLangEng ? "Office" : "অফিস"%>
                                        </label>
                                        <div class="col-md-8">
                                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
						                            id="officeUnitId_modal_button"
						                            onclick="officeModalButtonClicked();">
						                        <%=LM.getText(LC.LANGUAGE_SELECT, userDTO)%>
						                    </button>
						
						                    <div class="input-group" id="officeUnitId_div" style="display: none">
						                        <input type="hidden" name='officeUnitId'
						                               id='office_units_id_input' value="">
						                        <button type="button" class="btn btn-secondary form-control shadow btn-border-radius"
						                                disabled id="office_units_id_text"></button>
						                        <span class="input-group-btn" style="width: 5%" tag='pb_html'>
													<button type="button" class="btn btn-outline-danger"
															onclick="crsBtnClicked('officeUnitId');"
															id='officeUnitId_crs_btn' tag='pb_html'>
														x
													</button>
												</span>
						                    </div>

                                        </div>
                                    </div>
                                    <div class="form-group row" id="employeeDiv">
                                        <label class="col-md-4 col-form-label text-md-right" for="orgId">
                                            <%=LM.getText(LC.HM_ISSUE_RAISER, userDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                if (userDTO.roleID != SessionConstants.ADMIN_ROLE && userDTO.roleID != SessionConstants.TICKET_ADMIN_ROLE) {
                                            %>
                                            <select class='form-control' required
                                                    name='issueRaiserOrganoramId' id='orgId'
                                                    onchange='fillAssetAssignee(this.value);'
                                                    tag='pb_html'>
                                                <%
                                                    System.out.println("orgId = " + userDTO.organogramID);
                                                    String filter = " office_units.id = " + userDTO.unitID
                                                            + " and (office_unit_organograms.org_tree like '%" + userDTO.organogramID + "%'"
                                                            + " or office_unit_organograms.id = " + userDTO.organogramID + ")";
                                                    if (actionName.equals("edit")) {
                                                        Options = CommonDAO.getOrganogramsByOrganogramID(support_ticketDTO.issueRaiserOrganoramId, filter);
                                                    } else {
                                                        Options = CommonDAO.getOrganogramsByOrganogramID(userDTO.organogramID, filter);
                                                    }
                                                %>
                                                <%=Options%>
                                            </select>
                                            <%
                                            } else {
                                            %>
                                            <button type="button"
                                                    class="btn btn-primary form-control shadow btn-border-radius mb-3"
                                                    onclick="addEmployee()"
                                                    id="addToTrainee_modal_button"><%=LM.getText(LC.HM_ADD_EMPLOYEE, userDTO)%>
                                            </button>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped text-nowrap">
                                                    <tbody id="employeeToSet"></tbody>
                                                </table>
                                            </div>
                                            <input type='hidden' name='issueRaiserOrganoramId'
                                                   id='orgId' tag='pb_html' value='-1'/>
                                            <%
                                                }
                                            %>

                                        </div>
                                    </div>
                                    
                                    

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="ticketIssuesType_select_<%=i%>">
                                            <%=LM.getText(LC.SUPPORT_TICKET_ADD_TICKETISSUESTYPE, userDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' required
                                                    name='ticketIssuesType'
                                                    id='ticketIssuesType_select_<%=i%>'
                                                    onchange="getSubType('ticketIssuesSubtypeType_select_<%=i%>', this.value,-1)"
                                                    tag='pb_html'>
                                                <%=Ticket_issuesRepository.getInstance().buildOptions(Language, support_ticketDTO.ticketIssuesType)%>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row" id="subTypeDiv" style="display:none" onchange = "getEs()">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="ticketIssuesSubtypeType_select_<%=i%>">
                                            <%=LM.getText(LC.SUPPORT_TICKET_ADD_TICKETISSUESSUBTYPETYPE, userDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control'
                                                    name='ticketIssuesSubtypeType'
                                                    id='ticketIssuesSubtypeType_select_<%=i%>'
                                                    tag='pb_html'>
                                            </select>

                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                              
                                            <%=isLangEng?"Office Location":"অফিসের ঠিকানা"%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' 
                                                    name='officeBuildingId'
                                                    id='officeBuildingId'
                                                    tag='pb_html'>
                                                <option value '-1'></option>
                                            </select>

                                        </div>
                                    </div>
                                    
                                    <div class="form-group row" id="esDiv" style="display:none">
										<label class="col-md-4 col-form-label text-md-right">
                                            
                                        </label>
                                        <div class="col-md-8">
                                            <table id="esTable" class="table table-bordered table-striped">
                                        	</table>

                                        </div>
                                        
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="description_textarea_<%=i%>">
                                            <%=LM.getText(LC.SUPPORT_TICKET_ADD_DESCRIPTION, userDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                              <textarea class='form-control' name='description' id='description_textarea_<%=i%>'
                                                        tag='pb_html'><%=support_ticketDTO.description%></textarea>
                                        </div>
                                    </div>


                                    <div id="othersDiv" style="display:none">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right" for="issueText_text_<%=i%>">
                                                <%=LM.getText(LC.HM_OTHERS, userDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='issueText'
                                                       id='issueText_text_<%=i%>'
                                                       value='<%=support_ticketDTO.issueText%>'   tag='pb_html'/>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="assetAssigneeId">
                                            <%=LM.getText(LC.HM_ASSETS, userDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control'
                                                    name=assetAssigneeId
                                                    id='assetAssigneeId'
                                                    tag='pb_html' multiple>
                                                <option value='-1'><%=LM.getText(LC.HM_SELECT, userDTO)%>
                                                </option>
                                            </select>

                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='configuration'
                                           id='configuration_text_<%=i%>'
                                           value='<%=support_ticketDTO.configuration%>' tag='pb_html'/>

                                    <input type="hidden" name='ticketStatusCat'
                                           id='ticketStatusCat_category_<%=i%>' value='1'/>

                                    <div class="form-group row" style="display:none">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.SUPPORT_TICKET_ADD_DUEDATE, userDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID"
                                                           value="dueDate_date_js"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' class='form-control'
                                                   readonly="readonly" data-label="Document Date"
                                                   id='dueDate_date_<%=i%>' name='dueDate' value=<%
																	if(actionName.equals("edit"))
																	{
																		String formatted_dueDate = dateFormat.format(new Date(support_ticketDTO.dueDate));
																		%>
                                                           '<%=formatted_dueDate%>'
                                            <%
                                            } else {
                                            %>
                                            '<%=datestr%>'
                                            <%
                                                }
                                            %>
                                            tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.SUPPORT_TICKET_ADD_FILESDROPZONE, userDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                fileColumnName = "filesDropzone";
                                                if (actionName.equals("edit")) {
                                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(support_ticketDTO.filesDropzone);
                                            %>
                                            <%@include file="../pb/dropzoneEditor.jsp" %>
                                            <%
                                                } else {
                                                    ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                    support_ticketDTO.filesDropzone = ColumnID;
                                                }
                                            %>

                                            <div class="dropzone"
                                                 action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=support_ticketDTO.filesDropzone%>">
                                                <input type='file' style="display:none"
                                                       name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>'
                                                       tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value=''
                                                   tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>'
                                                   tag='pb_html'
                                                   value='<%=support_ticketDTO.filesDropzone%>'/>


                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control'
                                           name='currentAssignedUserId'
                                           id='currentAssignedUserId_hidden_<%=i%>'
                                           value='<%=support_ticketDTO.currentAssignedUserId%>'
                                           tag='pb_html'/>
                                    <div class="form-group row" style="display:none">
                                        <label class="col-md-4 col-form-label text-md-right" for="priorityCat_category_<%=i%>">
                                            <%=LM.getText(LC.SUPPORT_TICKET_ADD_PRIORITYCAT, userDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='priorityCat'
                                                    id='priorityCat_category_<%=i%>' tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("priority", Language, support_ticketDTO.priorityCat)%>
                                            </select>

                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 mt-3 text-right">
                        <a class="btn btn-sm cancel-btn text-white shadow btn-border-radius"
                           href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.SUPPORT_TICKET_ADD_SUPPORT_TICKET_CANCEL_BUTTON, userDTO)%>
                        </a>
                        <button class="btn  btn-sm submit-btn text-white shadow btn-border-radius ml-2"
                                type="submit"><%=LM.getText(LC.SUPPORT_TICKET_ADD_SUPPORT_TICKET_SUBMIT_BUTTON, userDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">


    $(document).ready(function () {
        basicInit();
        $.validator.addMethod('ticketIssueSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('prioritySelection', function (value, element) {
            return value != 0;
        });

        $("#support-ticket-id").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                ticketIssuesType: {
                    required: true,
                    ticketIssueSelection: true
                },
                priorityCat: {
                    required: true,
                    prioritySelection: true
                }
            },
            messages: {
                ticketIssuesType: {
                    required: "Please select ticket issue",
                    ticketIssueSelection: "Please select ticket issue"
                },
                priorityCat: {
                    required: "Please select priority",
                    prioritySelection: "Please select priority"
                }
            }
        });

        init();
        getLocationAndFillAsset($("#orgId").val());

        $("#assetAssigneeId").select2({
            dropdownAutoWidth: true,
            theme: "classic"
        });


    });

    function PreprocessBeforeSubmiting() {
        $("#dueDate_date_0").val(getDateStringById('dueDate_date_js', 'DD/MM/YYYY'));
        $("select").prop("disabled", false);
        return true;
    }

    function init() {
        setDateByStringAndId('dueDate_date_js', $("#dueDate_date_<%=i%>").val());
        <%
            if(actionName.equals("edit")){
        %>
        getSubType('ticketIssuesSubtypeType_select_<%=i%>', <%=support_ticketDTO.ticketIssuesType%>, <%=support_ticketDTO.ticketIssuesSubtypeType%>)
        <%
        }
        %>
        
        $("#ticketIssuesType_select_<%=i%> option[value='<%=Ticket_issuesDTO.NOC_ISSUE%>']").each(function() {
            $(this).remove();
        });
    }


    function getSubType(childElement, value, subType) {
        document.getElementById(childElement).innerHTML = "";
        $("#subTypeDiv").css("display", "none");
        $("#esDiv").hide();
		$("#esTable").html("");
        if (!value) {
            return;
        }
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById(childElement).innerHTML = this.responseText;
                $("#subTypeDiv").removeAttr("style");
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("GET", "option_Servlet?actionType=ticket_issues_subtype&ticket_issues="
            + value + "&ticket_issues_sub_type=" + subType, true);
        xhttp.send();
    }


    function fillAssetAssignee(orgId) {
    	if(orgId != null)
   		{
    		console.log("fillAssetAssignee with " + orgId);
            fillDropDown(orgId, "assetAssigneeId",
                "Support_ticketServlet?actionType=getUserAssets&orgId="
                + orgId
                + "&assetAssigneeId=" + $("#assetAssigneeId").val());
   		}
    	
    }
    
    function getLocationAndFillAsset(orgId)
    {
    	var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
            	$("#officeBuildingId").html(this.responseText);
            	fillAssetAssignee(orgId);
            } else {
                //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
            }
        };

        xhttp.open("GET", "Office_unitsServlet?actionType=ajax_buildingByOrg&orgId=" + orgId 
        		, true);
        xhttp.send();
    }


    function patient_inputted(userName, orgId) {
        console.log('patient inputted value: ' + userName);

        $("#orgId").val(orgId);
        getLocationAndFillAsset(orgId);
    }
    
    function setOrgFromRoom(roomNo)
    {
    	if(roomNo == -1)
   		{

   		}
    	else
   		{
   			var orgId = <%=SessionConstants.ROOM_OFFSET%> + parseInt(roomNo);
   			fillAssetAssignee(orgId);
   		}
    }

    function showRoom() {
        if ($("#roomCB").prop("checked")) {
            $("#roomDiv").removeAttr("style");
            $("#employeeDiv").css("display", "none");
            $("#officeDiv").css("display", "none");
        } else {
            $("#employeeDiv").removeAttr("style");
            $("#roomDiv").css("display", "none");
            $("#officeDiv").css("display", "none");
        }
    }
    
    function showOffice() {
        if ($("#officeCB").prop("checked")) {
            $("#roomDiv").css("display", "none");
            $("#employeeDiv").css("display", "none");
            $("#officeDiv").removeAttr("style");
        } else {
        	 $("#employeeDiv").removeAttr("style");
             $("#roomDiv").css("display", "none");
             $("#officeDiv").css("display", "none");
        }
    }
    
    
    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        $('#officeUnitId_modal_button').hide();
        $('#officeUnitId_div').show();
        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id_input').val(selectedOffice.id);
        
        var orgId = <%=SessionConstants.OFFICE_OFFSET%> + parseInt(selectedOffice.id);
        $("#orgId").val(orgId);
		fillAssetAssignee(orgId);
        
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    function officeModalButtonClicked() {
        // console.log('Button Clicked!');
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
    }
	
	function getEs()
	{
		if($("#ticketIssuesType_select_<%=i%>").val() == '<%=Ticket_issuesDTO.ESERVICE%>')
		{
			$("#esDiv").show();
			var subtype = $("#ticketIssuesSubtypeType_select_<%=i%>").val();
			console.log("subtype = " + subtype);
			console.log($("#es_" + subtype).html());
			$("#esTable").html($("#es_" + subtype).html());
		}
		else
		{
			$("#esDiv").hide();
			$("#esTable").html("");
		}
	}

</script>
