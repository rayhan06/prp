<%@page import="office_building.Office_buildingRepository"%>
<%@page import="asset_clearance_status.Asset_clearance_statusDAO"%>
<%@page import="asset_clearance_status.Asset_clearance_statusDTO"%>
<%@page import="ticket_issues.Ticket_issuesDTO"%>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="support_ticket.*" %>
<%@page import="workflow.WorkflowController" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="files.*" %>
<%@page import="user.*" %>
<%@page import="asset_model.*" %>
<%@ page import="workflow.OrganogramDetails" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="util.StringUtils" %>
<%@ page import="ticket_issues.Ticket_issuesRepository" %>
<%@ page import="ticket_issues.TicketIssueSubTypeRepository" %>

<%
    String value = "";
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Support_ticketDTO support_ticketDTO = new Support_ticketDAO().getDTOByID(id);
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
    String context = request.getContextPath() + "/";
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    AssetAssigneeDAO assetAssigneeDAO = new AssetAssigneeDAO();
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    boolean isNOC = support_ticketDTO.ticketIssuesType  == Ticket_issuesDTO.NOC_ISSUE;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <button type="button" class="btn btn-sm border-0 shadow text-white btn-border-radius my-2 my-md-0"
                            style="background-color: #a3b122;"
                            onclick="printAnyDiv('modalbody')">
                        <%=LM.getText(LC.HM_PRINT, userDTO)%>
                    </button>
                    <%
                        if (userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.TICKET_ADMIN_ROLE) {
                    %>
                    <button type="button" class="btn btn-sm border-0 shadow text-white btn-border-radius my-2 my-md-0"
                            style="background-color: #338cbb;"
                            onclick="location.href='Support_ticketServlet?actionType=search&filter=getHistory&issuer=<%=support_ticketDTO.issueRaiserOrganoramId%>'">
                        <%=LM.getText(LC.HM_ISSUE_RAISER_HISTORY, userDTO)%>
                    </button>
                    <%
                        }
                    %>

                    <%
                        if (
                                support_ticketDTO.ticketStatusCat != Support_ticketDTO.CLOSED &&
                                        (support_ticketDTO.currentAssignedOrganoramId == userDTO.organogramID
                                        || support_ticketDTO.supportOrgsArray.contains(userDTO.organogramID)
                                                || userDTO.roleID == SessionConstants.ADMIN_ROLE
                                                || userDTO.roleID == SessionConstants.TICKET_ADMIN_ROLE)) {
                    %>
                    <button type="button" class="btn btn-sm border-0 shadow text-white btn-border-radius my-2 my-md-0"
                            style="background-color: #8233c1;"
                            onclick="location.href='Support_ticketServlet?actionType=getForwardPage&supportTicketId=<%=support_ticketDTO.iD%>'">
                         <%=isLangEng ? "TICKET ACTIONS" : "টিকেটের পদক্ষেপসমূহ"%>
                    </button>
                    <%
                        }
                    %>


                    <button type="button" class="btn btn-sm border-0 shadow text-white btn-border-radius my-2 my-md-0"
                            style="background-color: #22ccc1;"
                            onclick="location.href='Asset_modelServlet?actionType=getUserAsset&organogramId=<%=support_ticketDTO.issueRaiserOrganoramId%>'">
                        <%=LM.getText(LC.HM_ASSET, userDTO).toUpperCase()%>
                    </button>


                    <button type="button" class="btn btn-sm border-0 shadow text-white btn-border-radius my-2 my-md-0"
                            style="background-color: #772233;"
                            onclick="location.href='Support_ticketServlet?actionType=search'">
                        <%=isLangEng ? "SEARCH PAGE" : "সার্চ পেইজ"%>
                    </button>

                </h3>
            </div>
        </div>
        <div id="commentModal" class="modal fade" role="dialog">
            <div class="container">
                <div>
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content modal-xl">
                            <div class="modal-header d-flex justify-content-between">
                                <h4 class="modal-title"><%=LM.getText(LC.HM_REMARKS, userDTO)%>
                                </h4>
                                <button type="button" class="close text-danger"
                                        data-dismiss="modal"></button>
                            </div>
                            <div class="modal-body">
                                <form class="form-horizontal"
                                      action="Support_ticketServlet?actionType=addComment&id=<%=support_ticketDTO.iD%>"
                                      method="POST" enctype="multipart/form-data">
                                    <div class="form-group row" >
                                        <label class="col-2 col-form-label text-right" for="remarks">
                                            <%=LM.getText(LC.HM_REMARKS, userDTO)%>
                                        </label>
                                        <div class="col-10">
											<textarea class='form-control' name='remarks'
                                                      id='remarks' tag='pb_html'></textarea>
                                        </div>
                                    </div>
                                    <div class="form-actions text-center mb-5">
                                        <button class="btn btn-success"
                                                type="submit"><%=LM.getText(LC.FORWARD_TICKET_ADD_FORWARD_TICKET_SUBMIT_BUTTON, userDTO)%>
                                        </button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body form-body" id="modalbody">
            <div class="d-flex flex-column flex-md-row align-items-center">
                <div class="text-center text-md-left">
                    <img width="30%"
                         src="<%=context%>assets/static/parliament_logo.png" alt="logo"
                         class="logo-default"/>
                </div>
                <div class="prescription-parliament-info">
                    <h4 class="text-center text-md-left">
                        <%=LM.getText(LC.HM_PARLIAMENT_SUPPORT_CENTRE, userDTO)%>
                    </h4>
                    <h5 class="text-center text-md-left">
                        <%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, userDTO)%>
                    </h5>
                    <h5 class="text-center text-md-left">
                        <%=LM.getText(LC.HM_PARLIAMENT_PHONE, userDTO)%>
                    </h5>
                </div>
            </div>
            <div class="table-section">
                <div class="mt-5 table-responsive">
                    <table class="table table-bordered table-striped">

                        <%
                            if (support_ticketDTO.roomNoCat == -1) {
                        %>
                        <tr>

                            <td><%=LM.getText(LC.HM_DATE, userDTO)%>
                            </td>
                            <td>
                                <%=Utils.getDigits(simpleDateFormat.format(new Date(support_ticketDTO.insertionDate)), Language)%>
                            </td>


                            <td><%=LM.getText(LC.HM_NAME, userDTO)%>
                            </td>
                            <td>
                                <%
                                    OrganogramDetails issueRaiserOrganogramDetails = WorkflowController.getOrganogramDetailsByOrganogramId(support_ticketDTO.issueRaiserOrganoramId, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng);
                                %>
                                <b>
                                <%
                                if (support_ticketDTO.issueRaiserOrganoramId >= SessionConstants.OFFICE_OFFSET) 
                                {
                                	%>
                                	<%=isLangEng ? "Office" : "অফিস"%>
                                	<%
                                }
                                else if (support_ticketDTO.issueRaiserOrganoramId >= SessionConstants.ROOM_OFFSET) 
                                {
                                	%>
                                	<%=isLangEng ? "Room" : "কক্ষ"%>
                                	<%
                                }
                                %>
                                <%=WorkflowController.getNameFromOrganogramId(support_ticketDTO.issueRaiserOrganoramId , isLangEng)%>
                                </b>
                            </td>

                            <td><%=LM.getText(LC.HM_PHONE, userDTO)%>
                            </td>
                            <td>
                                <%=issueRaiserOrganogramDetails.empMobileNo%>
                            </td>
                        </tr>
                        <tr>
                            <td><%=isLangEng ? "Office Address" : "অফিসের ঠিকানা"%>
                            </td>
                            <td>
                                <%=Office_buildingRepository.getInstance().getName(support_ticketDTO.officeBuildingId, isLangEng)%>
                            </td>
                            <td><%=LM.getText(LC.HM_SECTION, userDTO)%>
                            </td>
                            <td>
                                <%=issueRaiserOrganogramDetails.officeName%>
                            </td>

                            <td><%=LM.getText(LC.HM_WING, userDTO)%>
                            </td>
                            <td>
                                <%
                                    if (issueRaiserOrganogramDetails.wingModel != null) {
                                %>
                                <%=isLangEng ? issueRaiserOrganogramDetails.wingModel.nameEn : issueRaiserOrganogramDetails.wingModel.nameBn%>
                                <%
                                    }
                                %>
                            </td>
                        </tr>
                        <tr>
                        	<td><%=isLangEng ? "Ticket Creator" : "টিকেট তৈরিকারী"%>
                            </td>
                            <td>
                                <%=WorkflowController.getNameFromUserId(support_ticketDTO.insertedByUserId, isLangEng)%>
                            </td>
                            
                            <td><%=isLangEng ? "Designation" : "পদবী"%>
                            </td>
                            <td>
                                <%=WorkflowController.getOrganogramName(support_ticketDTO.insertedByOrganogramId, isLangEng)%>
                            </td>
                            
                            <td><%=isLangEng ? "Office" : "অফিস"%>
                            </td>
                            <td>
                                <%=WorkflowController.getOfficeNameFromOrganogramId(support_ticketDTO.insertedByOrganogramId, isLangEng)%>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                        <%
                            if (support_ticketDTO.roomNoCat != -1) {
                        %>
                        <tr>
                            <td><%=LM.getText(LC.HM_DATE, userDTO)%>
                            </td>
                            <td>
                                <%=Utils.getDigits(simpleDateFormat.format(new Date(support_ticketDTO.insertionDate)), Language)%>
                            </td>
                            <td><%=isLangEng ? "Room" : "কক্ষ"%>
                            </td>
                            <td>
                                <%=CatRepository.getInstance().getText(isLangEng, "room_no", support_ticketDTO.roomNoCat)%>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                    </table>
                </div>
            </div>
            <div class="table-section">
                <div class="mt-5 table-responsive">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td style="width: 30%"><b><%=LM.getText(LC.SUPPORT_TICKET_ADD_ID, userDTO)%>
                            </b></td>
                            <td><%=Utils.getDigits(support_ticketDTO.iD, Language)%>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%"><b><%=LM.getText(LC.SUPPORT_TICKET_ADD_TICKETISSUESTYPE, userDTO)%>
                            </b></td>
                            <td>
                                <b><%=Ticket_issuesRepository.getInstance().getText(support_ticketDTO.ticketIssuesType, isLangEng)%></b>
                                <%
                                    if (support_ticketDTO.ticketIssuesSubtypeType != -1) {
                                %>
                                : <%=TicketIssueSubTypeRepository.getInstance().getText(support_ticketDTO.ticketIssuesSubtypeType, isLangEng)%> <%
                                }
                            %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b><%=isLangEng ? "Priority" : "গুরুত্ব"%>
                                </b>
                            </td>
                            <td>
                                <b><%=CatRepository.getInstance().getText(isLangEng, "priority", support_ticketDTO.priorityCat)%>
                                </b>
                            </td>
                        </tr>
                        
                       <tr>
                            <td>
                                <b><%=isLangEng ? "Ticket Status" : "টিকেটের অবস্থা"%>
                                </b>
                            </td>
                            <td>
                                <b><%=CatRepository.getInstance().getText(isLangEng, "ticket_status", support_ticketDTO.ticketStatusCat)%>
                                </b>
                            </td>
                        </tr>
                        
                        <%
                        if(isNOC)
                        {
                        %>
                        <tr>
                            <td>
                                <b><%=isLangEng ? "NOC Status" : "এন ও সি অবস্থা"%>
                                </b>
                            </td>
                            <td>
                                <b><%=CatRepository.getInstance().getText(isLangEng, "noc_status", support_ticketDTO.nocStatusCat)%>
                                </b>
                            </td>
                        </tr>
                        <%
                        }
                        %>

                        <%
                            if (!support_ticketDTO.assetAssigneeId.equalsIgnoreCase("") && !support_ticketDTO.assetAssigneeId.equalsIgnoreCase("-1")) {
                        %>
                        <tr>
                            <td style="width: 30%"><b><%=isLangEng ? "Assets with Issue" : "সমস্যাগ্রস্থ সম্পদ"%>

                            </b></td>
                            <td>
                                <%
                                    String[] assignees = support_ticketDTO.assetAssigneeId.split(",");
                                    if (assignees != null) {
                                        boolean b1st = true;
                                        for (String assignee : assignees) {
                                            if (!assignee.equalsIgnoreCase("") && !assignee.equalsIgnoreCase("-1")) {
                                                if (!b1st) {
                                %>
                                ,
                                <%

                                    }
                                %>
                                <%=assetAssigneeDAO.getName(Long.parseLong(assignee))%>
                                <%
                                                b1st = false;
                                            }
                                        }
                                    }

                                %>

                            </td>
                        </tr>
                        <%
                            }
                        %>
                        
                           <%
                            if (!isNOC) {
                        %>
                        <tr>
                            <td style="width: 30%"><b><%=isLangEng ? "User Assets" : "ইউজারের সম্পদসমূহ"%>

                            </b></td>
                            <td>
                                <%
                                List <AssetAssigneeDTO> assetAssigneeDTOs = assetAssigneeDAO.getByOrganogramId(support_ticketDTO.issueRaiserOrganoramId);
                                   
                                if (assetAssigneeDTOs != null) 
                                {
                                        boolean b1st = true;
                                        for (AssetAssigneeDTO assetAssigneeDTO : assetAssigneeDTOs)
                                        { 
	                                        if (!b1st) 
	                                        {
					                             %>
					                             ,
					                             <%
	                            			}
			                                %>
			                                <%=assetAssigneeDAO.getName(assetAssigneeDTO.iD)%>
			                                <%
                                            b1st = false;
                                            
                                        }
                                    }

                                %>

                            </td>
                        </tr>
                        <%
                            }
                        %>

                        <%
                            if (!support_ticketDTO.description.equalsIgnoreCase("")) {
                        %>
                        <tr>
                            <td style="width: 30%"><b><%=LM.getText(LC.SUPPORT_TICKET_ADD_DESCRIPTION, userDTO)%>
                            </b></td>
                            <td>
                                <%
                                    value = support_ticketDTO.description;
                                %> <%=Utils.getDigits(value, Language)%>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                        <%
                            if (support_ticketDTO.ticketStatusCat == Support_ticketDTO.CLOSED) {
                        %>
                        <tr>
                            <td style="width: 30%"><b><%=LM.getText(LC.SUPPORT_TICKET_ADD_CLOSINGDATE, userDTO)%>
                            </b></td>
                            <td>
                                <%=Utils.getDigits(simpleDateFormat.format(new Date(support_ticketDTO.closingDate)), Language)%>

                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%"><b><%=LM.getText(LC.SUPPORT_TICKET_ADD_CLOSEDBYID, userDTO)%>
                            </b></td>
                            <td>
                                <%= WorkflowController.getNameFromOrganogramId(support_ticketDTO.closedById, Language)%>

                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%"><b><%=LM.getText(LC.SUPPORT_TICKET_ADD_CLOSINGCOMMENTS, userDTO)%>
                            </b></td>
                            <td>
                                <%
                                    value = support_ticketDTO.closingComments;

                                %> <%=value%>
                            </td>
                        </tr>
                        <%
                        } else {
                        %>
                        <tr>
                            <td style="width: 30%"><b><%=isLangEng ? "Currently Assigned To" : "বর্তমানে নিযুক্ত"%>
                            </b></td>
                            <td>
                                <%
                                    OrganogramDetails organogramDetails = WorkflowController.getOrganogramDetailsByOrganogramId(support_ticketDTO.currentAssignedOrganoramId, isLangEng);
                                if(!organogramDetails.empName.equalsIgnoreCase(""))
                                {
                                %>
                                <b><%=organogramDetails.empName%>  , <%=organogramDetails.username%></b>
                                <%
                                }
                                %>
                               
                            </td>
                        </tr>
                        
                        <tr>
                            <td style="width: 30%"><b><%=isLangEng ? "Assigned Engineers" : " নিযুক্ত ইঞ্জিনিয়ার"%>
                            </b></td>
                            <td>
                                <%
                                	if(support_ticketDTO.origSupportOrgsArray != null && !support_ticketDTO.origSupportOrgsArray.isEmpty())
                                	{
                                		%>
                                		<table>
                                		<tr>
                                				<td><%=LM.getText(LC.HM_NAME, userDTO)%></td>
                                				<td><%=LM.getText(LC.HM_USER_NAME, userDTO)%></td>
                                				<td><%=LM.getText(LC.HM_PHONE, userDTO)%></td>
                               				<%
                               				if(support_ticketDTO.ticketIssuesType == Ticket_issuesDTO.ESERVICE)
                               				{
                               					%>
                               					<td><%=isLangEng ? "Task" : "কাজ"%>
												</td>
                               					<%
                               				}
                               				%>
                                		</tr>
                                		<%
                                		int j = 0;
                                		for(long org: support_ticketDTO.origSupportOrgsArray)
                                		{
                                			OrganogramDetails orgD = WorkflowController.getOrganogramDetailsByOrganogramId(org, isLangEng);
                                			%>
                                			<tr>
                                			<td><b><%=orgD.empName%></b></td>
                                			<td><%=orgD.username%></td>
                                			<td><%=orgD.empMobileNo%></td>
                                			<%
                               				if(support_ticketDTO.ticketIssuesType == Ticket_issuesDTO.ESERVICE)
                               				{
                               					%>
                               					<td><%=CatDAO.getName(Language, "eservice", support_ticketDTO.supportJobsArray.get(j))%>
												</td>
                               					<%
                               				}
                               				%>
                                			<tr>
                                			<%
                                			j++;
                                		}
                                		%>
                                		</table>
                                		<%
                                	}
                                    
                                %>
                                
                               
                            </td>
                        </tr>
                        
                         <tr>
                            <td style="width: 30%"><b><%=isLangEng ? "Currently Working" : "বর্তমানে কর্মরত"%>
                            </b></td>
                            <td>
                                <%
                                	if(support_ticketDTO.supportOrgsArray != null && !support_ticketDTO.supportOrgsArray.isEmpty())
                                	{
                                		%>
                                		<table>
                                			<tr>
                                				<td><%=LM.getText(LC.HM_NAME, userDTO)%></td>
                                				<td><%=LM.getText(LC.HM_USER_NAME, userDTO)%></td>
                                				<td><%=LM.getText(LC.HM_PHONE, userDTO)%></td>
                                			</tr>
                                		<%
                                		for(long org: support_ticketDTO.supportOrgsArray)
                                		{
                                			OrganogramDetails orgD = WorkflowController.getOrganogramDetailsByOrganogramId(org, isLangEng);
                                			%>
                                			<tr>
                                			<td><b><%=orgD.empName%></b></td>
                                			<td><%=orgD.username%></td>
                                			<td><%=orgD.empMobileNo%></td>
                                			<tr>
                                			<%
                                		}
                                		%>
                                		</table>
                                		<%
                                	}
                                    
                                %>
                                
                               
                            </td>
                        </tr>
                        
                        <tr>
                            <td style="width: 30%"><b><%=isLangEng ? "Due Time" : "ডিউর সময়"%>
                            </b></td>
                            <td>
                                <%=dateFormat.format(new Date(support_ticketDTO.dueDate))%> <%=timeFormat.format(new Date(support_ticketDTO.dueTime))%>
                                
                               
                            </td>
                        </tr>
                        <%
                            }
                        %>
                    </table>
                </div>
                <div class="mt-5 table-responsive" style="display: none">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td style="width: 30%"><b><%=LM.getText(LC.HM_LAST_PROBLEM_DATE, userDTO)%>
                            </b></td>
                            <td>
                                <%
                                    value = support_ticketDTO.lastDTOText;
                                %> <%=Utils.getDigits(value, Language)%>
                            </td>

                        </tr>
                    </table>
                </div>
            </div>
            <%

                List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(support_ticketDTO.filesDropzone);
                if (FilesDTOList.size() > 0) {
            %>
            <div class="table-section">
                <div class="mt-5">
                    <h5 class="table-title"><%=LM.getText(LC.HM_ATTACHMENTS, userDTO)%>
                    </h5>

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.HM_FILE_NAME, userDTO)%>
                                </th>

                                <th><%=LM.getText(LC.HM_DOWNLOAD, userDTO)%>
                                </th>

                            </tr>
                            </thead>
                            <tbody>

                            <%
                                if (FilesDTOList != null) {
                                    for (int j = 0; j < FilesDTOList.size(); j++) {
                                        FilesDTO filesDTO = FilesDTOList.get(j);
                                        byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                            %>
                            <tr>
                                <td>
                                    <%
                                        if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                    %> <img
                                        src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                        style='width: 100px'/><br> <%
                                    }
                                %> <%=filesDTO.fileTitle%>
                                </td>

                                <td><a
                                        href='Support_ticketServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                        download><i class="fa fa-download" aria-hidden="true"></i>
                                </a></td>

                            </tr>
                            <%
                                    }
                                }
                            %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <%
                }
            %>
            
            <%
            if(isNOC)
            {
            	%>
            <div class="table-section">
			    <div class="mt-5">
			        <h5 class="table-title">
			            <%=isLangEng?"Asset Status":"সম্পদের অবস্থা"%>
			        </h5>
			        <div class="table-responsive">
			            <table class="table table-bordered table-striped">
			                <thead>
				                <tr>
					                <th><%=LM.getText(LC.HM_ASSET, userDTO)%>
				                    </th>
				                    <th><%=LM.getText(LC.HM_STATUS, userDTO)%>
				                    </th>
				                    <th><%=LM.getText(LC.HM_REMARKS, userDTO)%>
				                    </th>
				                </tr>
			                </thead>
			                	<%
			                	List<Asset_clearance_statusDTO> asset_clearance_statusDTOs = Asset_clearance_statusDAO.getInstance().getByTicketId(support_ticketDTO.iD);
			                	if(asset_clearance_statusDTOs != null)
			                	{
			                		for(Asset_clearance_statusDTO asset_clearance_statusDTO: asset_clearance_statusDTOs)
			                		{
			                			%>
			                			<tr>
			                				<td><%=asset_clearance_statusDTO.assetName%></td>
			                				<td><%=CatRepository.getInstance().getText(Language, "basic_status", asset_clearance_statusDTO.assetClearanceStatusCat)%></td>
			                				<td><%=asset_clearance_statusDTO.clearingRemarks%>
			                			</tr>
			                			<%
			                		}
			                	}
			                	%>
			                
			                <tbody>
			                </tbody>
			             </table>
			         </div>
			      </div>
			   </div>
            	<%
            }
            %>
            <%@include file="./history.jsp" %>
            <%
            if(!isNOC)
            {
            %>
            <div class="d-flex justify-content-end" >
                <button type="button" class="btn btn-sm border-0 shadow text-white btn-border-radius my-2 my-md-0"
                        style="background-color: #441971;" data-toggle='modal' 
                        data-target="#commentModal">
                    <%=LM.getText(LC.HM_REMARKS, userDTO).toUpperCase()%>
                </button>
            </div>
            <%
            }
            %>
        </div>
    </div>
</div>