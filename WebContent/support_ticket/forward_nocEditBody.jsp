<%@page import="asset_clearance_letter.Asset_clearance_letterDTO"%>
<%@page import="ticket_issues.Ticket_issuesRepository"%>
<%@page import="ticket_issues.Ticket_issuesDTO"%>
<%@page import="user.UserRepository"%>
<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="support_ticket.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="employee_offices.*"%>
<%@ page import="user_expertise_map.*" %>
<%@ page import="ticket_forward_history.*" %>
<%@page import="office_unit_organograms.*" %>
<%@page import="employee_offices.*" %>
<%@page import="user.*" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="util.StringUtils" %>
<%@ page import="asset_clearance_status.*" %>

<%
Forward_ticketDTO forward_ticketDTO;
forward_ticketDTO = (Forward_ticketDTO)request.getAttribute("forward_ticketDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
Support_ticketDAO support_ticketDAO = new Support_ticketDAO();
long supportTicketId = -1;
boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
Support_ticketDTO  support_ticketDTO = null;
if(request.getParameter("supportTicketId") != null)
{
	supportTicketId = Long.parseLong(request.getParameter("supportTicketId"));
	support_ticketDTO = support_ticketDAO.getDTOByID(supportTicketId);
}
if(forward_ticketDTO == null)
{
	forward_ticketDTO = new Forward_ticketDTO();
	if(support_ticketDTO != null)
	{
		forward_ticketDTO.iD = supportTicketId;
		forward_ticketDTO.ticketStatusCat = support_ticketDTO.ticketStatusCat;
		if(support_ticketDTO.dueDate != 0)
		{
			forward_ticketDTO.dueDate = support_ticketDTO.dueDate;
		}
	}
	
}
System.out.println("forward_ticketDTO = " + forward_ticketDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = isLangEng ? "NOC ACTIONS" : "এন ও সি পদক্ষেপসমূহ";
String servletName = "Forward_ticketServlet";
String fileColumnName = "";

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

long ColumnID = -1;
FilesDAO filesDAO = new FilesDAO();
boolean isPermanentTable = true;
String Language = LM.getText(LC.FORWARD_TICKET_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;

long issueRaiserUserId = WorkflowController.getUserIDFromOrganogramId(support_ticketDTO.issueRaiserOrganoramId);

User_expertise_mapDAO user_expertise_mapDAO = new User_expertise_mapDAO();
User_expertise_mapDTO user_expertise_mapDTO = user_expertise_mapDAO.getDTOByUserId(issueRaiserUserId);

boolean dtoFound = true;
if(user_expertise_mapDTO == null)
{
	dtoFound = false;
	user_expertise_mapDTO = new User_expertise_mapDTO();
}
boolean isBoss = userDTO.roleID == SessionConstants.TICKET_ADMIN_ROLE || userDTO.roleID == SessionConstants.ADMIN_ROLE;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title prp-page-title">
                            <i class="fa fa-gift"></i>&nbsp;
                            <%=formTitle%>
                        </h3>
                    </div>
                </div>
                
                <form class="form-horizontal"
                      action="Support_ticketServlet?actionType=forwardTicket&id=<%=support_ticketDTO.iD%>"
                      id="bigform" name="bigform" method="POST" 
                      enctype="multipart/form-data" onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
                    <div class="kt-portlet__body form-body">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="onlyborder">
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="sub_title_top">
                                                        <div class="sub_title">
                                                            <h4 style="background: white"><%=formTitle%>
                                                            </h4>
                                                        </div>
                                                     </div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=forward_ticketDTO.iD%>' tag='pb_html'/>
	
													<div class="table-section">
													    <div class="mt-5">
													        <h5 class="table-title">
													            <%=isLangEng?"Asset Status":"সম্পদের অবস্থা"%>
													        </h5>
													        <div class="table-responsive">
													            <table class="table table-bordered table-striped">
													                <thead>
														                <tr>
															                <th><%=LM.getText(LC.HM_ASSET, userDTO)%>
														                    </th>
														                    <th><%=LM.getText(LC.HM_STATUS, userDTO)%>
														                    </th>
														                    <th><%=LM.getText(LC.HM_REMARKS, userDTO)%>
														                    </th>
														                </tr>
													                </thead>
													                	<%
													                	List<Asset_clearance_statusDTO> asset_clearance_statusDTOs = Asset_clearance_statusDAO.getInstance().getByTicketId(support_ticketDTO.iD);
													                	if(asset_clearance_statusDTOs != null)
													                	{
													                		for(Asset_clearance_statusDTO asset_clearance_statusDTO: asset_clearance_statusDTOs)
													                		{
													                			%>
													                			<tr>
													                				<td><%=asset_clearance_statusDTO.assetName%></td>
													                				<%
													                				if(support_ticketDTO.nocStatusCat != Asset_clearance_letterDTO.NOC_VERIFICATION_STARTED)
													                				{
													                				%>
													                				<td><%=CatRepository.getInstance().getText(Language, "basic_status", asset_clearance_statusDTO.assetClearanceStatusCat)%></td>
													                				<td><%=asset_clearance_statusDTO.remarks%></td>
													                				<%
													                				}
													                				else
													                				{
													                					%>
													                				<td>
													                				 <select class='form-control' name='assetClearanceStatusCat' style = "width:80px"
										                                                    tag='pb_html'>
										                                                <%
										                                                    Options = CatDAO.getOptions(Language, "basic_status", asset_clearance_statusDTO.assetClearanceStatusCat);
										                                                %>
										                                                <%=Options%>
										                                             </select>
													                				</td>
													                				
													                				<td>
													                				 <input class='form-control' name='clearingRemarks' type = 'text' value = '<%=asset_clearance_statusDTO.clearingRemarks%>'
										                                                    tag='pb_html'>
										                                               
													                				</td>
													                					<%
													                				}
													                				%>
													                			</tr>
													                			<%
													                		}
													                	}
													                	%>
													                
													                <tbody>
													                </tbody>
													             </table>
													         </div>
													      </div>
													   </div>
													   
													   <br>
                                                      									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.HM_REMARKS, loginDTO)%></label>
                                                            <div class="col-8">
																<textarea class='form-control'  name='remarks' id = 'remarks_text_<%=i%>'    tag='pb_html'><%=forward_ticketDTO.remarks%></textarea>					
															</div>
                                                      </div>
                                                      
                                                      
                                                      <%
					                                    if(isBoss)
					                                    {
					                                    	if(support_ticketDTO.nocStatusCat == Asset_clearance_letterDTO.PENDING)
					                                    	{
					                                    %>
                                                      									
														<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.SUPPORT_TICKET_ADD_DUEDATE, loginDTO)%></label>
                                                            <div class="col-8">
																<%value = "dueDate_js_" + i;%>
																<jsp:include page="/date/date.jsp">
															   			<jsp:param name="DATE_ID" value="dueDate_date_js"></jsp:param>
															   			<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																	</jsp:include>	
																	<input type='hidden' class='form-control' readonly="readonly" data-label="Document Date" id = 'dueDate_date_<%=i%>' name='dueDate' value=	<%
																String formatted_dueDate = dateFormat.format(new Date(forward_ticketDTO.dueDate));
																%>
																'<%=formatted_dueDate%>'
															   tag='pb_html'>		
															</div>
                                                      </div>
                                                      <%
					                                    	}
                                                      %>									


                                                      <div id = "empDiv">
                                                      
                                                      <%
                                                      if(support_ticketDTO.nocStatusCat == Asset_clearance_letterDTO.PENDING)
				                                    	{
                                                      %>
                                                      	 <div class="form-group row">
					                                        <label class="col-4 col-form-label text-md-right"><%=LM.getText(LC.SUPPORT_TICKET_ADD_PRIORITYCAT, loginDTO)%>
					                                        </label>
					                                        <div class="col-8">
					                                        
					                                            <select class='form-control' name='priorityCat'
					                                                    id='priorityCat_category_<%=i%>' tag='pb_html'>
					                                                <%
					                                                    Options = CatDAO.getOptions(Language, "priority", support_ticketDTO.priorityCat);
					                                                %>
					                                                <%=Options%>
					                                            </select>
																
					                                        </div>
					                                    </div>
					                                   
					                                   
					                                    <%
				                                    	}
					                                    %>
					                                    
					                                    
					                                    <%
					                                    if(support_ticketDTO.nocStatusCat == Asset_clearance_letterDTO.PENDING 
							                                    || support_ticketDTO.nocStatusCat == Asset_clearance_letterDTO.NOC_VERIFIED)
					                                    {
					                                    %>
                                                      	<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.HM_ACTION, loginDTO)%></label>
                                                            <div class="col-8">
																
																<select name='forwardActionCat' id = 'forwardActionCat'	class='form-control' onchange = "showOrg()">
																	<%
																	if(support_ticketDTO.nocStatusCat == Asset_clearance_letterDTO.PENDING)
																	{
																	%>
																	<option value='<%=Ticket_forward_historyDTO.TICKET_ASSIGN%>'><%=Language.equalsIgnoreCase("English")?"Assign":"দায়িত্ব দিন" %></option>
																	<%
																	}
																	else if(support_ticketDTO.nocStatusCat == Asset_clearance_letterDTO.NOC_VERIFIED)
																	{
																	%>
																	<option value='<%=Ticket_forward_historyDTO.NOC_APPROVAL%>'><%=Language.equalsIgnoreCase("English")?"Approve":"অনুমোদন করুন" %></option>
																	<option value='<%=Ticket_forward_historyDTO.NOC_REJECTION%>'><%=Language.equalsIgnoreCase("English")?"Reject":"বাতিল করুন" %></option>
																	<option value='<%=Ticket_forward_historyDTO.FORWARD%>'><%=Language.equalsIgnoreCase("English")?"Forward":"ফরওয়ার্ড করুন" %></option>
																	<%
																	}
																	%>
																	<option value='<%=Ticket_forward_historyDTO.DELETE%>'><%=Language.equalsIgnoreCase("English")?"Delete":"ডিলিট" %></option>
																</select>					
															</div>
                                                   		</div>									
														<div class="form-group row" id = "orgDiv" style = "display:none">
                                                            <label class="col-4 col-form-label text-right"><%=Language.equalsIgnoreCase("english")?"Employee Name":"কর্মকর্তার নাম"%></label>
                                                            <div class="col-8">
																
																<select name='orgId' id = 'orgId'	class='form-control'>
																<option value = "-1"></option>
																 <%
				                                                    Set<Long> engineersAndAdmins = new HashSet<Long>();
																 	if(support_ticketDTO.nocStatusCat == Asset_clearance_letterDTO.PENDING)
																 	{
																		engineersAndAdmins.addAll(EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.SUPPORT_ENGINEER_ROLE));
																 	}
																 	else if(support_ticketDTO.nocStatusCat == Asset_clearance_letterDTO.NOC_VERIFIED)
																	{
																 		engineersAndAdmins.addAll(EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.TICKET_ADMIN_ROLE));
																	}
																   
																 	Ticket_issuesDTO ticket_issuesDTO = Ticket_issuesRepository.getInstance().getById(support_ticketDTO.ticketIssuesType);
		                                                    		long admin = ticket_issuesDTO.adminOrganogramId;
		                                                    		long adminOffice = WorkflowController.getOfficeIdFromOrganogramId(admin);
		                                                    		System.out.println("adminOffice = " + adminOffice);
				                                                    for(Long employee: engineersAndAdmins)
				                                                    {
				                                                    	boolean employeCanBeAdded = false;
				                                                    	if(userDTO.roleID == SessionConstants.ADMIN_ROLE)
				                                                    	{
				                                                    		employeCanBeAdded = true;
				                                                    	}
				                                                    	else
				                                                    	{
				                                                    		
				                                                    		long employeeOffice = WorkflowController.getOfficeIdFromOrganogramId(employee);
				                                                    		if(adminOffice == SessionConstants.MAINTENANCE1_OFFICE || adminOffice == SessionConstants.MAINTENANCE2_OFFICE)
				                                                    		{
				                                                    			if(employeeOffice == SessionConstants.MAINTENANCE1_OFFICE || employeeOffice == SessionConstants.MAINTENANCE2_OFFICE)
				                                                    			{
				                                                    				employeCanBeAdded = true;
				                                                    			}
				                                                    			
				                                                    		}
				                                                    		else if(adminOffice == employeeOffice)
				                                                    		{
				                                                    			System.out.println("adminOffice = employeeOffice");
				                                                    			employeCanBeAdded = true;
				                                                    		}
				                                                    	}
				                                                    	if(employeCanBeAdded)
				                                                    	{
				                                                    	
				                                                    %>
				                                                    <option value = "<%=employee%>" >
				                                                     <%=WorkflowController.getNameFromOrganogramId(employee, Language)%>,
				                                                     <%=WorkflowController.getOfficeNameFromOrganogramId(employee, Language)%>
				                                                    </option>
				                                                    <%
				                                                    	}
				                                                    }
				                                                    %>
																</select>					
															</div>
                                                   		</div>
                                                   		<%
					                                    }
                                                   		%>
                                                   		
                                                   		
                                                   		
                                                      </div>
                                                      
                                                      <%
					                                    }
                                                   		%>

                                                  	
													</div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>                               
                          </div>
                       </div>
                       <div class="form-actions text-center mb-5">
	                        <a class="btn btn-danger" href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.FORWARD_TICKET_ADD_FORWARD_TICKET_CANCEL_BUTTON, loginDTO)%></a>										
							<button class="btn btn-success" type="submit"><%=LM.getText(LC.FORWARD_TICKET_ADD_FORWARD_TICKET_SUBMIT_BUTTON, loginDTO)%></button>
					   </div>
                   </form>
               </div>                      
          </div>
      </div>
 </div>

<script type="text/javascript">

function showOrg()
{
	var action = $("#forwardActionCat").val();
	if(action == <%=Ticket_forward_historyDTO.NOC_APPROVAL%> || action == <%=Ticket_forward_historyDTO.NOC_REJECTION%>)
	{
		$("#orgDiv").css("display", "none");
	}
	else
	{
		$("#orgDiv").removeAttr("style");
	}
}

function PreprocessBeforeSubmiting(row, validate)
{
	
	
	<%
	if(support_ticketDTO.nocStatusCat == Asset_clearance_letterDTO.PENDING)
	{
	%>
		var dueDate = getDateStringById('dueDate_date_js', 'DD/MM/YYYY');
		$("#dueDate_date_0").val(dueDate);
		if($("#orgId").val() == -1 && $("#forwardActionCat").val() != '<%=Ticket_forward_historyDTO.DELETE%>')
		{
			toastr.error("Please select an employee to forward");
			return false;
		}
	<%
	}
	%>

	return true;
}




function init(row)
{
	<%
	if(support_ticketDTO.nocStatusCat == Asset_clearance_letterDTO.PENDING)
	{
	%>
	setDateByStringAndId('dueDate_date_js', $("#dueDate_date_<%=i%>").val());
	<%
	}
	%>
	showOrg();
}

var row = 0;
$(document).ready(function(){
	init(row);
	<%
	if(isBoss)
	{
	%>
		/*$("#orgId").select2({
	        dropdownAutoWidth: true,
	        theme: "classic"
	    });*/
	<%
	}
	%>

});	



</script>






