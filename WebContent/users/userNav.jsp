<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="role.*" %>
<%@ page import="java.util.*" %>
<%
    System.out.println("Inside nav.jsp");
    String url = request.getParameter("url");
    String navigator = request.getParameter("navigator");
    String pageName = request.getParameter("pageName");
    if (pageName == null) pageName = "Search";
    String pageno = "";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String[][] searchFieldInfo = rn.getSearchFieldInfo();
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";

    System.out.println("action  " + action);
    String Language = LM.getLanguage(loginDTO);

%>
<style>
    .kay-group > input {
        margin: 5px;
    }
</style>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=pageName%>
                </h3>
            </div>
        </div>
        <form action="<%=action%>" method="POST" class="form-horizontal">
        <div class="kt-portlet__body form-body">
                <div>
                    
                    
                    
                    <div class="row">
                        
                        <div class="col-md-6">
                            
                            <div class="form-group row">
                                
                                <label  class="col-md-3 col-form-label">
                                    <%=LM.getText(LC.HM_USER_NAME, loginDTO)%>
                                </label>
                                <%
                                String value = (String)session.getAttribute("userName");%>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="" placeholder="" name="userName"
                                    value="<%=value!=null?value:""%>"
                                    >
                                </div>
                            </div>
                            
                        </div>
                        

                    
                    
                        <div class="col-md-6">
                            
                            <div class="form-group row">
                                
                                <label  class="col-md-3 col-form-label">
                                    <%=LM.getText(LC.HM_NAME, loginDTO)%>
                                </label>
                                <%value = (String)session.getAttribute("fullName");%>
                                <div class="col-md-9">
                                    <input type="text" class="form-control"  placeholder="" name="fullName"
                                    value="<%=value!=null?value:""%>"
                                    >
                                </div>
                            </div>
                            
                        </div>
                        
                        <div class="col-md-6">
                            
                            <div class="form-group row">
                                
                                <label  class="col-md-3 col-form-label">
                                    <%=LM.getText(LC.ROLE_SEARCH_ROLE_NAME, loginDTO)%>
                                </label>
                                <%
                                value = (String)session.getAttribute("roleToSearch");
                                if(value == null)
                                {
                                	value = "";
                                }
                                System.out.println("roleToSearch in jsp = " + value);
                                %>
                                <div class="col-md-9">
                                    <select name = "roleToSearch" id = "roleToSearch" class="form-control">
                                    	<option value = ""
                                    	<%=(value == null || value.equalsIgnoreCase(""))?"selected":"" %>
                                    	>
                                    	<%=LM.getText(LC.HM_SELECT, loginDTO)%>
                                    	</option>
                                    	<%
                                    	ArrayList<RoleDTO> roles= PermissionRepository.getAllRoles();
                                		for(RoleDTO roleDTO: roles)
                                		{
                                			if(roleDTO == null)
                                			{
                                				continue;
                                			}
                                			%>
                                			<option value = '<%=roleDTO.ID%>' 
                                			<%=value.equalsIgnoreCase(roleDTO.ID + "")?"selected":"" %>
                                			>
                                			<%=Language.equalsIgnoreCase("english")?roleDTO.roleName:roleDTO.roleBn%>
                                			</option>
                                			<%
                                		}
                                    	%>
                                    </select>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                    

                    
                    
                    <div class="row">
                        
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group row pr-0">
                        <label  class="col-md-3 col-form-label">Record Per Page
                        </label>
                        <div class="col-md-9 pr-0">
                            <input type="text" class="form-control" name="RECORDS_PER_PAGE" placeholder="" value="10">
                        </div>
                    </div>
                    <div class="col-md-12 form-group text-right">
                        <input type="hidden" name="search" value="yes">
                        <!-- 				          	<input type="reset" class="btn  btn-sm btn btn-circle  grey-mint btn-outline sbold uppercase" value="Reset" > -->
                        <input type="submit" class="btn  btn-primary shadow btn-border-radius" value="Search">
                    </div>
                </div>
            </div>
         </form>
        <div class="kt-portlet__body form-body">
            <form action="<%=action%>" method="POST" class="form-inline ml-auto">
                <nav aria-label="Page navigation">
                    <ul class="pagination d-flex align-items-center justify-content-end">
                        <li class="page-item">
                            <a class="page-link" style="border-radius: 6px 0px 0px 6px"
                               href="<%=link%><%=concat%>id=first" aria-label="First"
                               title="Left">
                                <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                                <span class="sr-only"><%=LM.getText(LC.NAVIGATION_FIRST, loginDTO)%></span>
                            </a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="<%=link%><%=concat%>id=previous"
                               aria-label="Previous"
                               title="Previous">
                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                <span class="sr-only"><%=LM.getText(LC.NAVIGATION_PREVIOUS, loginDTO)%></span>
                            </a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="<%=link%><%=concat%>id=next" aria-label="Next"
                               title="Next">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <span class="sr-only"><%=LM.getText(LC.NAVIGATION_NEXT, loginDTO)%></span>
                            </a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" style="border-radius: 0px 6px 6px 0px"
                               href="<%=link%><%=concat%>id=last"
                               aria-label="Last"
                               title="Last">
                                <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                <span class="sr-only"><%=LM.getText(LC.NAVIGATION_LAST, loginDTO)%></span>
                            </a>
                        </li>
                        <li class="d-flex align-items-center justify-content-end w-50">
                            &nbsp;&nbsp;<span class="hidden-xs mx-3">
                                <%=LM.getText(LC.GLOBAL_PAGE, loginDTO) %>
                            </span>
                            <input
                                    type="text"
                                    class="form-control mx-3 text-center w-25"
                                    name="pageno"
                                    value='<%=pageno%>'
                            <%--                                    size="15"--%>
                                    style="height: 35px; border-radius: 6px"
                            />
                            <span class="hidden-xs mx-2">
                                <%=LM.getText(LC.GLOBAL_OF, loginDTO) %>
                            </span>
                            <%=rn.getTotalPages()%>
                            <input type="hidden" name="go" value="yes"/>
                            <input type="hidden" name="mode" value="search"/>
                            <button
                                    type="submit"
                                    class="btn ml-3 mt-1 pr-0"
                                    value="<%=LM.getText(LC.GLOBAL_GO, loginDTO)%>"
                            >
                                <i class="fa fa-arrow-alt-circle-right fa-2x bg-light text-success mb-2"
                                   style="cursor: pointer"></i>
                            </button>
                        </li>
                    </ul>
                </nav>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
    	console.log("ready called");
    	  $("#roleToSearch").select2({
              dropdownAutoWidth: true,
              theme: "classic"
          });
    	
    })
    
    
</script>

