<jsp:include page="../common/layout.jsp" flush="true">
<jsp:param name="title" value="User Change Password" /> 
	<jsp:param name="body" value="../users/userChangePasswordBody.jsp" />
	<jsp:param name="js" value="assets/global/plugins/jquery.sparkline.min.js" />
	<jsp:param name="js" value="assets/global/plugins/pwstrength.js" />
</jsp:include> 