<%@page import="config.GlobalConfigConstants" %>
<%@page import="config.GlobalConfigurationRepository" %>
<%@page import="user.UserRepository" %>
<%@page import="permission.MenuConstants" %>
<%@page import="role.PermissionRepository" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LM" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="role.RoleDTO" %>
<%@page import="user.UserTypeDTO" %>
<%@page import="user.UserTypeRepository" %>
<%@page import="java.util.ArrayList" %>
<%@page import="user.UserDTO" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>


<%
    String context = "../../.." + request.getContextPath() + "/";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO loginUserDTO = UserRepository.getUserDTOByUserID(loginDTO);
    UserDTO userDTO = (UserDTO) request.getAttribute(ServletConstant.USER_DTO);
    ArrayList<RoleDTO> roleList = (ArrayList<RoleDTO>) request.getAttribute(ServletConstant.ROLE_LIST);
    String action = JSPConstant.USER_SERVLET + "?" + ActionTypeConstant.ACTION_TYPE + "=";
    String formTitle = "";
    String password = "";

    if (userDTO == null) {
        formTitle = LM.getText(LC.GLOBAL_CHANGE_PASSWORD, loginDTO);
        action = action + ActionTypeConstant.USER_CHANGE_PASSWORD;
        userDTO = (UserDTO) request.getSession().getAttribute(ServletConstant.USER_DTO);
        if (userDTO == null) userDTO = new UserDTO();
        else request.getSession().removeAttribute(ServletConstant.USER_DTO);

    } else {
        formTitle = LM.getText(LC.GLOBAL_CHANGE_PASSWORD, loginDTO);
        action = action + ActionTypeConstant.USER_CHANGE_PASSWORD;
        password = ServletConstant.DEFAULT_PASSWORD;
    }
    System.out.println("#################action  = " + action);

    System.out.println("#################action 2  = " + ActionTypeConstant.USER_CHANGE_PASSWORD);
    String actionName = request.getParameter("actionType");
/*
if (request.getParameter("actionType").equalsIgnoreCase("getChangePassword"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}

*/
    String fieldError = "";
%>
<%
//    String Language = "English";//LM.getText(LC.BULL_TRANSFER_EDIT_LANGUAGE, loginDTO);
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    System.out.println("Language = " + Language);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    date.setYear(date.getYear() + 2);
    String futuredatestr = dateFormat.format(date);
    String passMessage = (String)request.getSession().getAttribute(ServletConstant.PASSWORD);
%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" action="<%=action%>" method="POST" onsubmit="return validate();">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type="hidden" name="ID" value="<%=userDTO.ID%>"/>
                                    <jsp:include page='../common/flushActionStatus.jsp'/>
                                    <div class="form-group row">

                                        <label for="password" class="col-md-4 col-form-label text-md-right">
                                            <%--<%=LM.getText(LC.USER_ADD_PASSWORD, loginDTO)%><span class="required"> * </span>--%>
                                            <%=Language.equalsIgnoreCase("English")?"Old Password": "পুরানো পাসওয়ার্ড"%><span class="required"> * </span>
                                        </label>

                                        <div class="col-md-8">
                                            <input type="password" class="form-control" name="oldPassword"
                                                   id="oldPassword"/>
                                            <div class="pwstrength_viewport_progress" style = "display:none"></div>
                                        </div>
                                    </div>
                                    <div class="form-group row">

                                        <label for="password" class="col-md-4 col-form-label text-md-right">
                                            <%--<%=LM.getText(LC.USER_ADD_PASSWORD, loginDTO)%><span class="required"> * </span>--%>
                                                <%=Language.equalsIgnoreCase("English")?"New Password": "নতুন পাসওয়ার্ড"%><span class="required"> * </span>
                                        </label>

                                        <div class="col-md-8">
                                            <input type="password" class="form-control" name="password" id="password"
                                                   value="<%=password%>"/>
                                            <div class="pwstrength_viewport_progress" style = "display:none"></div>
                                        </div>
                                        <%
                                            fieldError = (String) request.getSession().getAttribute(ServletConstant.PASSWORD);
                                            if (fieldError != null) {
                                                request.getSession().removeAttribute(ServletConstant.PASSWORD);
                                        %>
                                        <span class="label label-danger" style = "display:none"><%=fieldError%></span>
                                        <%}%>
                                    </div>
                                    <div class="form-group row ">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%--<%=LM.getText(LC.USER_ADD_CONFIRM_PASSWORD, loginDTO)%><span class="required"> * </span>--%>
                                                <%=Language.equalsIgnoreCase("English")?"Confirm New Password": "নতুন পাসওয়ার্ড নিশ্চিত করুন"%><span class="required"> * </span>
                                        </label>

                                        <div class="col-md-8">
                                            <input type="password" class="form-control" name="repassword"
                                                   value="<%=password%>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 text-right">
                        <%if (true) {%>
                        <a class="btn btn-sm cancel-btn text-white shadow btn-border-radius"
                           href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.USER_ADD_CANCEL, loginDTO)%>
                        </a>
                        <button class="btn btn-sm submit-btn text-white shadow btn-border-radius ml-2"
                                type="submit"><%=LM.getText(LC.USER_ADD_SUBMIT, loginDTO)%>
                        </button>
                        <%} %>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<script src="<%=context%>scripts/util.js"></script>
<%--<script  src="<%=context%>users/user.js"></script>--%>
<%-- <script src="<%=context%>assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script> --%>
<script>
    $(document).ready(function () {
    	<%
    	if(passMessage!= null && !passMessage.isEmpty())
    	{
    		%>
    		toastr.error("<%=passMessage%>");
    		<%
    	}
    	%>
        $("input[name='userName']").keyup(function () {
            if ($(this).val().length > 0) {
                data = {};
                data.userName = $(this).val();
                callAjax(context + "UtilServlet?actionType=checkUserAvailable&username=" + data.userName, data, (data) => {
                    $('#usernameError').hide();
                    if (data == true) {
                        $('#availability').html('available');
                        $('#availability').css('color', 'green');
                    } else if (data == false) {
                        $('#availability').html('unavailable');
                        $('#availability').css('color', 'red');
                    } else {
                        toastr.error("Error");
                    }
                }, "POST")
            } else {
                $('#availability').html('');
            }
        });

        "use strict";
        var options = {};
        options.ui = {
            container: "#pwd-container",
            showVerdictsInsideProgressBar: true,
            showStatus: true,
            viewports: {
                progress: ".pwstrength_viewport_progress"
            },
            progressBarExtraCssClasses: "progress-bar-striped active",
            showPopover: false,
            showErrors: true,
            showProgressBar: true
        };
        options.rules = {
            activated: {
                wordTwoCharacterClasses: true,
                wordRepetitions: true
            }
        };
        options.common = {};
        $('#password').pwstrength(options);

    })
</script>


