<%@page import="workflow.WorkflowController"%>
<%@page import="config.GlobalConfigConstants" %>
<%@page import="config.GlobalConfigurationRepository" %>
<%@page import="user.UserRepository" %>
<%@page import="util.CommonConstant" %>
<%@page import="permission.MenuConstants" %>
<%@page import="role.PermissionRepository" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="util.ServletConstant" %>
<%@page import="util.ActionTypeConstant" %>
<%@page import="util.JSPConstant" %>
<%@page import="language.LM" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="role.RoleDTO" %>
<%@page import="user.*" %>
<%@page import="java.util.*" %>
<%@page import="java.util.ArrayList" %>
<%@page import="user.UserDTO" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>


<%
    String context = "../../.." + request.getContextPath() + "/";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO loginUserDTO = UserRepository.getUserDTOByUserID(loginDTO);
    UserDTO userDTO = (UserDTO) request.getAttribute(ServletConstant.USER_DTO);
    ArrayList<RoleDTO> roleList = (ArrayList<RoleDTO>) request.getAttribute(ServletConstant.ROLE_LIST);
    String action = JSPConstant.USER_SERVLET + "?" + ActionTypeConstant.ACTION_TYPE + "=";
    String formTitle = "";
    String password = "";

    if (userDTO == null) {
        formTitle = LM.getText(LC.USER_ADD_USER_ADD, loginDTO);
        action = action + ActionTypeConstant.USER_ADD;
        userDTO = (UserDTO) request.getSession().getAttribute(ServletConstant.USER_DTO);
        if (userDTO == null) userDTO = new UserDTO();
        else request.getSession().removeAttribute(ServletConstant.USER_DTO);

    } else {
        formTitle = LM.getText(LC.USER_ADD_USER_EDIT, loginDTO) +": " + WorkflowController.getNameFromUserId(userDTO.ID, true);
        action = action + ActionTypeConstant.USER_EDIT;
        password = ServletConstant.DEFAULT_PASSWORD;
    }
    System.out.println("actionType = " + request.getParameter("actionType"));
    String actionName;
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String fieldError = "";
%>
<%
    String Language = "English";//LM.getText(LC.BULL_TRANSFER_EDIT_LANGUAGE, loginDTO);
    System.out.println("Language = " + Language);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    date.setYear(date.getYear() + 2);
    String futuredatestr = dateFormat.format(date);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" action="<%=action%>" method="POST" onsubmit="return validate();">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type="hidden" name="ID" value="<%=userDTO.ID%>"/>
                                    <jsp:include page='../common/flushActionStatus.jsp'/>
                                    <div class="form-group row">
                                        <label class="col-md-3  col-from-label text-md-right">
                                            <%=LM.getText(LC.USER_ADD_USER_NAME, loginDTO)%><span
                                                class="required"> * </span>
                                        </label>

                                        <div class="col-md-9 ">
                                            <input type="text" class="form-control" name="userName"
                                                   value='<%=userDTO.userName%>'
                                                   readonly="readonly"/>
                                            <input type="hidden" class="form-control" name="orgId"
                                                   value='<%=userDTO.organogramID%>'
                                                   readonly="readonly"/>
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3  col-from-label text-md-right">
                                            <%=LM.getText(LC.USER_ADD_USER_TYPE, loginDTO) %>
                                        </label>

                                        <div class="col-md-9 ">
                                            <select class="form-control pull-right" name="userType">
                                                <%
                                                    for (UserTypeDTO userTypeDTO : UserTypeRepository.getInstance().getAllUserType()) {
                                                %>
                                                <option value="<%=userTypeDTO.ID%>" <%if (userTypeDTO.ID == userDTO.userType) {%>
                                                        selected <%}%>><%=(loginUserDTO.languageID == CommonConstant.Language_ID_Bangla) ? userTypeDTO.name_bn : userTypeDTO.name_en%>
                                                </option>
                                                <%
                                                    }
                                                %>

                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="col-md-3  col-from-label text-md-right">
                                            <%=Language.equalsIgnoreCase("english")?"Primary Role":"প্রাথমিক রোল"%>
                                       		
                                        </label>

                                        <div class="col-md-9 ">
                                            <select class="form-control pull-right" name="roleName"  id = "roleName">
                                                <%
                                                    for (RoleDTO roleDTO : roleList) {%>
                                                <option value="<%=roleDTO.ID%>" <%if (roleDTO.ID == userDTO.roleID) {%>
                                                        selected <%}%>><%=roleDTO.roleName%>
                                                </option>
                                                <%
                                                    }
                                                %>

                                            </select>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="form-group row">
                                        <label class="col-md-3  col-from-label text-md-right">
                                            <%=Language.equalsIgnoreCase("english")?"Additional Role":"অন্যান্য রোল"%>
                                        </label>
                                        <%
                                        List<UserDTO> otherUserDTOs = new ArrayList<UserDTO>();
                                        if (userDTO.ID != -1) {
                                            otherUserDTOs = new UserDAO().getNonDefaultUserDTOsByUserIdAndOrgId(userDTO.ID, userDTO.organogramID);
                                        }
                                        %>

                                        <div class="col-md-9 ">
                                            <select class="form-control pull-right" name="moreRole"  id = "moreRole" multiple="muiltiple">
                                                <%
                                                    for (RoleDTO roleDTO : roleList) 
                                                    {
                                                    	if(roleDTO.ID != SessionConstants.DOCTOR_ROLE && roleDTO.ID != SessionConstants.PHYSIOTHERAPIST_ROLE)
                                                    	{
                                                    	%>
                                                    
		                                                <option value="<%=roleDTO.ID%>" 
		                                                <%
		                                                
		                                                for(UserDTO otherDTO: otherUserDTOs)
		                                                {
		                                                	if(roleDTO.ID == otherDTO.roleID)
		                                                	{
		                                                		%>
		                                                		selected
		                                                		<%
		                                                		break;
		                                                	}
		                                                }
		                                                %>
                                                        >
                                                        <%=roleDTO.roleName%>
                                                		</option>
                                                		<%
                                                    	}
                                                    }
                                                %>

                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="col-md-12 col-from-label text-md-right">
                                            *<%=Language.equalsIgnoreCase("english")?"Dr or Physiotherapist cannot be in additional role.":"ডাক্তার বা ফিজিওথেরাপিস্ট অন্যান্য রোলে থাকতে পারবে না"%>
                                       		
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%if (PermissionRepository.checkPermissionByRoleIDAndMenuID(loginUserDTO.roleID, MenuConstants.USER_ADD)) {%>
                <div class="row">
                    <div class="col-md-10 text-right">
                        <a class="btn btn-sm cancel-btn text-light shadow btn-border-radius"
                           href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.USER_ADD_CANCEL, loginDTO)%>
                        </a>
                        <button class="btn btn-sm submit-btn text-light shadow btn-border-radius ml-2"
                                type="submit"><%=LM.getText(LC.USER_ADD_SUBMIT, loginDTO)%>
                        </button>
                    </div>
                </div>
                <%} %>
            </div>
        </form>
    </div>
</div>

<script src="<%=context%>scripts/util.js"></script>
<script src="<%=context%>assets/vendors/general/select2/dist/js/select2.full.js" type="text/javascript"></script>
<%--<script  src="<%=context%>users/user.js"></script>--%>
<%-- <script src="<%=context%>assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script> --%>
<script>
    $(document).ready(function () {
    	console.log("ready called");
    	  $("#roleName").select2({
              dropdownAutoWidth: true,
              theme: "classic"
          });
    	  $("#moreRole").select2({
              dropdownAutoWidth: true,
              theme: "classic"
          });  
    })
    
    function validate()
    {
    	console.log("valicate called");
    	var primaryRole = $("#roleName").val();
    	var noDuplicate = true;
    	
    	$("#moreRole :selected").map(function(i, el) {
    	    if($(el).val() == primaryRole)
   	    	{
    	    	 toastr.error("Primary and Secondary roles cannot be same " + primaryRole);
    	    	 noDuplicate= false;

   	    	}
    	}).get();
    	
    	return noDuplicate;
    }

    
    
</script>


