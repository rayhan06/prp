<%@page import="permission.MenuConstants" %>
<%@page import="user.UserRepository" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.ActionTypeConstant" %>
<%@page import="util.JSPConstant" %>
<%@page import="role.PermissionRepository" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="user.UserDTO" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@page import="workflow.*" %>
<%@page import="pb.*" %>
<%@page import="role.*" %>
<%@page import="user.*" %>
<%@page import="java.util.*" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = "English";


    UserDTO loggedInUserDTO = UserRepository.getUserDTOByUserID(loginDTO);
    if (loggedInUserDTO.languageID != 1) {
        Language = "Bangla";
    }
    String url = "UserServlet?actionType=search";
    String navigator = SessionConstants.NAV_USER;
    String pageName = LM.getText(LC.USER_SEARCH_USER_SEARCH, loginDTO);
%>

<%
    String context = "../../.." + request.getContextPath() + "/";
    String editPageAction = JSPConstant.USER_SERVLET + "?" + ActionTypeConstant.ACTION_TYPE + "=" + ActionTypeConstant.USER_GET_EDIT_PAGE;
    String deleteAction = JSPConstant.USER_SERVLET + "?" + ActionTypeConstant.ACTION_TYPE + "=" + ActionTypeConstant.USER_DELETE;
    boolean isLangEng = Language.equalsIgnoreCase("english");
%>


<!-- begin:: Subheader -->
<%--<div class="kt-subheader  kt-grid__item" id="kt_subheader">--%>
<%--	<div class="kt-subheader__main">--%>
<%--		<i class="fa fa-search fa-2x" style="color:#0098bf !important;"></i>&nbsp;--%>
<%--		<h3 class="kt-subheader__title">--%>
<%--			&nbsp; <%=LM.getText(LC.APPOINTMENT_SEARCH_APPOINTMENT_SEARCH_FORMNAME, loginDTO)%>--%>
<%--		</h3>--%>
<%--	</div>--%>
<%--</div>--%>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid p-0" id="kt_content" style="background: white">
    <div class="shadow-none border-0">
        <jsp:include page="../users/userNav.jsp" flush="true">
            <jsp:param name="url" value="<%=url%>"/>
            <jsp:param name="pageName" value="<%=pageName%>"/>
            <jsp:param name="navigator" value="<%=navigator%>"/>
        </jsp:include>
        <div style="height: 1px; background: #ecf0f5"></div>
        <div class="kt-portlet shadow-none">
            <div class="kt-portlet__body">
                <form action="<%=deleteAction%>" method="POST" id="tableForm">
                    <jsp:include page='../common/flushActionStatus.jsp'/>
                    <div class="table-responsive">
                        <table id="tableData" class="table table-bordered table-striped text-nowrap">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.USER_SEARCH_USER_ID, loginDTO) %>
                                </th>
                                <th><%=LM.getText(LC.USER_SEARCH_FULL_NAME, loginDTO) %>
                                </th>
                                <th><%=LM.getText(LC.USER_SEARCH_ROLE, loginDTO) %>
                                </th>
                                <th><%=LM.getText(LC.HM_ORGANOGRAM, loginDTO) %>
                                </th>
                                <th>
                                    <%=LM.getText(LC.USER_SEARCH_EDIT, loginDTO) %>
                                </th>
                                <%if (PermissionRepository.checkPermissionByRoleIDAndMenuID(loggedInUserDTO.ID, MenuConstants.USER_DELETE)) { %>
                                <th>
                                    <input
                                            type="submit"
                                            class="btn btn-primary shadow btn-border-radius"
                                            value="<%=LM.getText(LC.USER_SEARCH_DELETE, loginDTO) %>"
                                    />
                                </th>
                                <%}%>
                            </tr>
                            </thead>
                            <tbody>
                            <%
                                ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_USER);
                                if (data != null) {
                                    int size = data.size();
                                    for (int i = 0; i < size; i++) {
                                        UserDTO userDTO = (UserDTO) data.get(i);
                            %>
                            <%
                                if (userDTO != null) {
                                    RoleDTO RoleDTO = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
                                   
                            %>
                            <tr>

                                <td><%=userDTO.userName%>
                                </td>
                                <td><%=WorkflowController.getNameFromUserName(userDTO.userName, Language)%>
                                </td>
                                <td>
                                <%
                                List<UserDTO> otherUserDTOs = null;
                                if (userDTO.ID != -1) {
                                    otherUserDTOs = new UserDAO().getrUserDTOsByUserIdAndOrgId(userDTO.ID, userDTO.organogramID);
                                }
                                if (otherUserDTOs != null) 
                                {
                                	int j = 0;
                                    for (UserDTO otherUserDTO : otherUserDTOs) 
                                    {
                                    	if(j != 0)
                                    	{
                                    		%>
                                    		, 
                                    		<%
                                    	}
                                    	%>
                                    	 <%=CommonDAO.getName(otherUserDTO.roleID, "role", "roleName", "id")%>
                                    	 
                                    	<%
                                    	j++;
                                    }
                                }
                                %>
                                </td>
                                <td>
                                	
                                    <%=WorkflowController.getOfficeNameFromOrganogramId(userDTO.organogramID, isLangEng)%>,
                                    <%=WorkflowController.getOrganogramName(userDTO.organogramID, isLangEng)%>
                                </td>
                                <td>
                                    <button
                                            type="button"
                                            class="btn-sm border-0 shadow btn-border-radius text-white"
                                            style="background-color: #ff6b6b;"
                                            onclick="location.href='<%=context%><%=editPageAction%>&orgId=<%=userDTO.organogramID%>'">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </td>
                                <%if (PermissionRepository.checkPermissionByRoleIDAndMenuID(loggedInUserDTO.ID, MenuConstants.USER_DELETE)) { %>
                                <td><input type="checkbox" name="ID" value="<%=userDTO.ID%>"/></td>
                                <%}%>
                            </tr>
                            <%
                                }
                            %>

                            <%
                                }
                            %>
                            </tbody>
                            <%
                                }
                            %>

                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- begin:: Content -->


<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#tableForm').submit(function (e) {
            var currentForm = this;
            var selected = false;
            e.preventDefault();
            var set = $('#tableData').find('tbody > tr > td:last-child input[type="checkbox"]');
            $(set).each(function () {
                if ($(this).prop('checked')) {
                    selected = true;
                }
            });
            if (!selected) {
                bootbox.alert("Select user to delete!", function () {
                });
            } else {
                bootbox.confirm("Are you sure you want to delete the record(s)?", function (result) {
                    if (result) {
                        currentForm.submit();
                    }
                });
            }
        });
    })

</script>


