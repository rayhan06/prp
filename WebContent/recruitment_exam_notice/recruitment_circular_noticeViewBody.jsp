<%@page import="util.*" %>
<%@ page import="user.UserServlet" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page import="pb.*" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionRepository" %>
<%@ page import="admit_card.Admit_cardDAO" %>
<%@ page import="bangla_date_converter.BanglaDateConverter" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="recruitment_seat_plan.Recruitment_seat_planDTO" %>
<%@ page import="recruitment_seat_plan.Recruitment_seat_planRepository" %>
<%@ page import="recruitment_exam_venue.Recruitment_exam_venueDTO" %>
<%@ page import="recruitment_exam_venue.Recruitment_exam_venueRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="recruitment_seat_plan.RecruitmentSeatPlanChildDTO" %>
<%@ page import="recruitment_seat_plan.RecruitmentSeatPlanChildRepository" %>
<%@ page import="recruitment_job_specific_exam_type.RecruitmentJobSpecificExamTypeDTO" %>
<%@ page import="recruitment_job_specific_exam_type.JobSpecificExamTypeRepository" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameDTO" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameDAO" %>
<%@ page import="employee_pay_scale.Employee_pay_scaleDTO" %>
<%@ page import="employee_pay_scale.Employee_pay_scaleRepository" %>

<%
    String context_folder = request.getContextPath();
    String Language = "Bangla";
    int my_language = 1;
%>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/css/all.min.css"
          integrity="sha512-BnbUDfEUfV0Slx6TunuB042k9tuKe3xrD6q4mg5Ed72LTgzDIcLPxg6yI2gcMFRyomt+yJJxE+zJwNmxki6/RA=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link rel="stylesheet" href="https://fonts.maateen.me/solaiman-lipi/font.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.js"></script>

    <title>View Notice</title>

</head>
<body>


<%
    SimpleDateFormat format_dateOfExam = new SimpleDateFormat("dd MMMM yyyy");
    String examType = "";

    Recruitment_test_nameDTO recruitment_test_nameDTO = Recruitment_test_nameDAO.getInstance().getLatestRecruitmentTestExam();
    List<Recruitment_job_descriptionDTO> recruitmentJobDescriptionDTOS = new ArrayList<>();
    if (recruitment_test_nameDTO != null && recruitment_test_nameDTO.recruitmentTestNamePublishCat == 2) { // 2 means published
        recruitmentJobDescriptionDTOS = Recruitment_job_descriptionRepository.getInstance().getRecruitment_job_descriptionListByRecTestNameId(recruitment_test_nameDTO.iD);
    }


    String noticeHeaderBN = "বাংলাদেশ জাতীয় সংসদ সচিবালয়ের নিম্নবর্ণিত পদসমূহে লােক নিয়ােগের নিমিত্ত প্রকৃত বাংলাদেশী নাগরিকদের নিকট থেকে দরখাস্ত আহবান করা যাচ্ছেঃ ";


%>

<style>

    /* ==================== General Style =========== */

    * {
        margin: 0;
        padding: 0;
        outline: none;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        text-decoration: none;
        list-style: none;
    }

    /* ================== Body =================== */
    body {
    <%--background: url(<%=context_folder%>/images/pin_retrieve/BG.png) no-repeat;--%> background: rgb(0, 159, 201);
        background: radial-gradient(circle, rgba(0, 159, 201, 1) 6%, rgba(0, 54, 115, 1) 60%, rgba(0, 27, 93, 1) 91%);
        max-width: 100vw;
        height: 100vh;
        background-position: center center;
        font-family: 'SolaimanLipi', sans-serif;
        -webkit-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        transition: all 0.3s ease;
        background-size: cover;
        background-attachment: fixed;
    }

    /* ======================== Top Logo area ================== */
    .leftlogo img {
        width: 159px;
        height: 113px;
        margin-top: 30px;
        margin-left: 30px;
    }

    .rightlogo img {
        width: 175px;
        height: 83px;
        margin-top: 60px;
        float: right;
        margin-right: 60px;
    }


    .formarea {
        display: block;
        float: left;
        width: 90%;
        margin: 0 5%;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        height: auto;
        background-color: rgba(255, 255, 255, 0.15);
        border-radius: 20px;
        color: #fff;
        /*padding: 26px 47px 37px 47px;*/
    }

    .formarea h2 {
        margin-bottom: 24px;
    }

    .rightallbutton {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-box-pack: end;
        -ms-flex-pack: end;
        justify-content: flex-end;
        color: #fff;
    }

    .rightallbutton button {
        margin: 0px 0px 0px 10px;
    }

    .rightallbutton .btnleft {
        color: #000;
        background-color: #FFE600;
        border: none;
        border-radius: 8px;
    }

    .rightallbutton .btnright {
        color: #fff;
        background-color: #00D4FF;
        border: none;
        border-radius: 8px;
    }

    .maintitle h2 {
        font-size: 2.25rem;
        margin-bottom: 25px;
    }


    input[type='text']::-webkit-input-placeholder {
        color: #B1CADB;
        font-size: 14px;
    }

    input[type='text']::-moz-placeholder {
        color: #B1CADB;
        font-size: 14px;
    }

    input[type='text']:-ms-input-placeholder {
        color: #B1CADB;
        font-size: 14px;
    }

    input[type='text']::-ms-input-placeholder {
        color: #B1CADB;
        font-size: 14px;
    }

    input[type='text']::placeholder {
        color: #B1CADB;
        font-size: 14px;
    }

    input[type='password']::-webkit-input-placeholder {
        color: #B1CADB;
        font-size: 14px;
    }

    input[type='password']::-moz-placeholder {
        color: #B1CADB;
        font-size: 14px;
    }

    input[type='password']:-ms-input-placeholder {
        color: #B1CADB;
        font-size: 14px;
    }

    input[type='password']::-ms-input-placeholder {
        color: #B1CADB;
        font-size: 14px;
    }

    input[type='password']::placeholder {
        color: #B1CADB;
        font-size: 14px;
    }


    .leftnews img {
        width: 256px;
        height: 154px;
        padding-bottom: 10px;
    }

    img.img-fluid.circularimage {
        margin-bottom: 15px;
    }

    img.img-fluid.admincardimage {
        margin-bottom: 15px;
    }

    .rightnews img {
        width: 256px;
        height: 154px;
        padding-bottom: 10px;
    }

    .maintitle h2 {
        font-size: 1.5rem;
        margin-bottom: 0px;
        padding: 12px 0px;
    }

    .leftlogo img {
        width: 130px;
        height: 90px;
        margin-top: 20px;
        margin-left: 30px;
    }

    .rightlogo img {
        width: 120px;
        height: 57px;
        margin-top: 50px;
        float: right;
        margin-right: 60px;
    }

    .mainlogo.text-center img {
        width: 90px;
        height: 90px;
        line-height: 90px;
    }

    .leftnews {
        padding-top: 140px;
    }

    .rightnews {
        padding-top: 140px;
    }

    h2.title {
        font-size: 25px;
    }

    .formarea h2 {
        margin-bottom: 10px;
    }

    /*.formarea {*/
    /*    padding: 24px 47px 25px 47px;*/
    /*}*/

    .leftnews img {
        width: 220px;
        height: 132px;
    }

    .rightnews img {
        width: 220px;
        height: 132px;
    }

    img.img-fluid.circularimage {
        margin-bottom: 20px;
    }

    img.img-fluid.admincardimage {
        margin-bottom: 20px;
    }

    .songshodimage img {
        width: 100%;
        height: 100%;
        margin-top: 30px;
    }

    .btn-border-radius {
        border-radius: 6px !important;
    }

    /* ======================== Responsive Area ================== */

    @media screen and (max-width: 1537px) {
        .songshodimage img {
            width: 78%;
            height: 100%;
            margin-top: 15px;
        }

        .leftnews {
            padding-top: 30%;
        }

        .rightnews {
            padding-top: 30%;
        }

    }

    @media screen and (max-width: 1480px) {

        .songshodimage img {
            width: 78%;
            height: 100%;
            margin-top: 15px;
        }

        .leftnews {
            padding-top: 140px;
        }

        .rightnews {
            padding-top: 140px;
        }


        .maintitle h2 {
            font-size: 1.25rem;
            margin-bottom: 0px;
            padding: 12px 0px;
        }

        /*.formarea {*/
        /*    padding: 24px 47px 26px 47px;*/
        /*}*/
    }


    @media screen and (max-width: 1366px) {

        .songshodimage img {
            width: 70%;
            height: 100%;
            margin-top: 0px;
        }

        .leftnews {
            padding-top: 140px;
        }

        .rightnews {
            padding-top: 140px;
        }


        .maintitle h2 {
            font-size: 1.25rem;
            margin-bottom: 0px;
            padding: 12px 0px;
        }

        /*.formarea {*/
        /*    padding: 24px 47px 26px 47px;*/
        /*}*/
        .leftlogo img {

            margin-top: 10px;

        }

        .rightlogo img {
            margin-top: 40px;

        }

    }


    @media screen and (max-width: 1100px) {
        .maintitle h2 {
            font-size: 1.15rem;
            margin-bottom: 0px;
            padding: 12px 0px;
        }

        /*.formarea {*/
        /*    padding: 24px 47px 26px 47px;*/
        /*}*/
        .rightallbutton .btnleft {
            font-size: 12px;
        }

        .rightallbutton .btnright {
            font-size: 12px;
        }

        /*.formarea {*/
        /*    padding: 24px 20px 26px 20px;*/
        /*}*/
        .formarea {
            display: block;
            float: left;
            width: 100%;
            margin: 0;
        }

        img.img-fluid.circularimage {
            margin-bottom: 10px;
        }

        img.img-fluid.admincardimage {
            margin-bottom: 10px;
        }

        .songshodimage img {
            width: 100%;
        }

    }

    @media screen and (max-width: 980px) {
        .maintitle h2 {
            font-size: 1rem;

        }

        .leftnews {
            padding-top: 135px;
        }

        .rightnews {
            padding-top: 135px;
        }

        img.img-fluid.circularimage {
            margin-bottom: 11px;
        }

        img.img-fluid.admincardimage {
            margin-bottom: 11px;
        }

    }

    @media screen and (max-width: 790px) {
        .maintitle h2 {
            font-size: 0.90rem;

        }

        .leftnews {
            padding-top: 135px;
        }

        .rightnews {
            padding-top: 135px;
        }

        img.img-fluid.circularimage {
            margin-bottom: 11px;
        }

        img.img-fluid.admincardimage {
            margin-bottom: 11px;
        }

    }

    @media screen and (max-width: 767px) {

        .leftnews {
            padding-top: 30px;
            padding-bottom: 20px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            width: 100%;
        }

        .rightnews {
            padding-top: 20px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            width: 100%;
        }

        .leftlogo img {
            margin-left: 6px;
        }

        .rightlogo img {
            margin-right: 8px;
        }

        .mainlogo.text-center img {
            width: 25%;
            height: auto;
        }

        img.img-fluid.circularimage {
            margin-bottom: 0px;
        }

        .leftnews img {
            padding-bottom: 0px;
        }

        .songshodimage img {
            margin-top: 0px;
        }

    }

    @media screen and (max-width: 464px) {

        .leftnews {
            padding-top: 30px;
            padding-bottom: 20px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            width: 100%;
        }

        .rightnews {
            padding-top: 20px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            width: 100%;
        }

        .leftnews img {
            width: 195px;
            height: 132px;
        }

        .rightnews img {
            width: 195px;
            height: 132px;
        }

        .leftlogo img {
            margin-left: 6px;

        }

        .rightlogo img {
            margin-right: 8px;
        }

        .mainlogo.text-center img {
            width: 25%;
            height: auto;
        }

        img.img-fluid.circularimage {
            margin-bottom: 0px;
        }

        .leftnews img {
            padding-bottom: 0px;
        }

        .songshodimage img {
            margin-top: 0px;
        }

    }


    @media screen and (max-width: 420px) {

        .leftnews {
            padding-top: 10px;
            padding-bottom: 10px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            width: 100%;
        }

        .rightnews {
            padding-top: 10px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            width: 100%;
        }

        .leftnews img {
            width: 170px;
            height: 132px;
        }

        .rightnews img {
            width: 170px;
            height: 132px;
        }

        .leftlogo img {
            margin-left: 6px;

        }

        .rightlogo img {
            margin-right: 8px;
        }

        .mainlogo.text-center img {
            width: 25%;
            height: auto;
        }

        img.img-fluid.circularimage {
            margin-bottom: 0px;
        }

        .leftnews img {
            padding-bottom: 0px;
        }

        .songshodimage img {
            margin-top: 0px;
        }

        img.img-fluid.admincardimage {
            margin-bottom: 0px;
        }

    }

    @media screen and (max-width: 370px) {

        .leftnews {
            padding-top: 10px;
            padding-bottom: 10px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            width: 100%;
        }

        .rightnews {
            padding-top: 10px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            width: 100%;
        }

        .leftnews img {
            width: 105px;
            height: 70px;
        }

        .rightnews img {
            width: 105px;
            height: 70px;
        }

        .leftlogo img {
            margin-left: 6px;

        }

        .rightlogo img {
            margin-right: 8px;
        }

        .mainlogo.text-center img {
            width: 25%;
            height: auto;
        }

        img.img-fluid.circularimage {
            margin-bottom: 0px;
        }

        .leftnews img {
            padding-bottom: 0px;
        }

        .songshodimage img {
            margin-top: 0px;
        }

        img.img-fluid.admincardimage {
            margin-bottom: 0px;
        }

    }

</style>

<style>

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .4in;
        background: white;
        margin-bottom: 10px;
    }

</style>

<div class="kt-content p-0" id="kt_content">
    <div class="">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title prp-page-title">

                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body" id="bill-div">


                <div style="margin: auto;">
                    <div class="container" id="to-print-div">
                        <%
                            int rowsPerPage = 5;
                            int index = 0;

                            while (index < recruitmentJobDescriptionDTOS.size()) {
                                boolean isFirstPage = (index == 0);

                        %>
                        <section class="page shadow" data-size="A4">

                            <%if (isFirstPage) {%>
                            <div class="col-12 text-right py-1" data-html2canvas-ignore="true">
                                <button type="button" class="btn" id='download-pdf'
                                        onclick="downloadTemplateAsPdf('to-print-div', 'Circular');">
                                    <i class="fa fa-file-download fa-2x" style="color: gray" aria-hidden="true"></i>
                                </button>
                            </div>
                            <div class="text-center">
                                <h5 class="mt-2 font-weight-bold">
                                    <%=UtilCharacter.getDataByLanguage(Language, "বাংলাদেশ জাতীয় সংসদ সচিবালয়", "Bangladesh National Parliament Secretariat")%>
                                </h5>
                                <h5 class="text-dark">
                                    <%=UtilCharacter.getDataByLanguage(Language, "মানব সম্পদ উইং", "Human Resources Wing")%>
                                </h5>
                                <h5 class="text-dark">
                                    <%=UtilCharacter.getDataByLanguage(Language, "মানব সম্পদ শাখা-৩", "Human Resources Wing")%>
                                </h5>
                                <a class="text-dark" href="https://www.Parliament.gov.bd" target="_blank">
                                    <u><%=UtilCharacter.getDataByLanguage(Language, "www.Parliament.gov.bd", "www.Parliament.gov.bd")%>
                                    </u>
                                </a>
                            </div>

                            <div class="row mt-2 ">
                                <div class="col-12 " style="display: flex;justify-content: space-between;">

                                    <span>
                                        <%=UtilCharacter.getDataByLanguage(Language, "নংঃ- ", "Subject:- ")%>&nbsp;
                                    </span>

                                    <span>
                                        <% String formattedBillBanglaDateInWord = BanglaDateConverter.getFormattedBanglaDate(recruitmentJobDescriptionDTOS.get(0).onlineJobPostingDate, true);
                                            String formattedBillDateInWord = DateUtils.getDateInWord("bangla", recruitmentJobDescriptionDTOS.get(0).onlineJobPostingDate);

                                        %>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td rowspan="2">তারিখ:</td>
                                                <td style="text-align: center;"><%=formattedBillBanglaDateInWord%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="border-top: 1px solid black; text-align: center;">
                                                    <%=formattedBillDateInWord%>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </span>
                                </div>
                            </div>

                            <div class=" mt-5 mx-2" style="text-indent: 3rem;">

                                <%=UtilCharacter.getDataByLanguage(Language, noticeHeaderBN, "")%>

                            </div>


                            <%}%>
                            <div class="row mx-2 mt-1">
                                <table
                                        class="mt-3 w-100 rounded"
                                        border="1px solid #323233;"
                                        style="border-collapse: collapse"
                                >
                                    <thead>
                                    <tr>

                                        <th class="text-center"
                                            style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "ক্রমিক নং", "Serial No.")%>
                                        </th>

                                        <th class="text-center"
                                            style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "পদের নাম", "Position Name")%>
                                        </th>
                                        <th class="text-center"
                                            style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "জাতীয় বেতন স্কেল", "National Pay Scale")%>
                                        </th>
                                        <th class="text-center"
                                            style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "পদ সংখ্যা ", "Number Of positions")%>
                                        </th>

                                        <th class="text-center"
                                            style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "যােগ্যতা", "Qualification")%>
                                        </th>


                                    </tr>
                                    </thead>

                                    <%
                                        int rowsInThisPage = 0;
                                        String rows = "";
                                        while (index < recruitmentJobDescriptionDTOS.size() && rowsInThisPage < rowsPerPage) {

                                            rowsInThisPage++;


                                            Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = recruitmentJobDescriptionDTOS.get(index++);

                                            String positionName = recruitment_job_descriptionDTO.jobTitleBn;
                                            String payScale = "";
                                            Employee_pay_scaleDTO employee_pay_scaleDTO = Employee_pay_scaleRepository.getInstance().getEmployee_pay_scaleDTOByID(recruitment_job_descriptionDTO.employeePayScaleType);
                                            if (employee_pay_scaleDTO != null) {
                                                payScale = CatRepository.getName(Language, "national_pay_scale_type", employee_pay_scaleDTO.nationalPayScaleCat);

                                            }
                                            String numberOfPositions = String.valueOf(recruitment_job_descriptionDTO.numberOfVacancy);
                                            numberOfPositions = Utils.getDigits(numberOfPositions, Language);
                                            String qualifications = recruitment_job_descriptionDTO.jobPurpose;
                                            qualifications = Utils.getDigits(qualifications, Language);

                                    %>
                                    <tr>
                                        <td class="p-2"><%=UtilCharacter.convertDataByLanguage(Language, String.valueOf(index))%>
                                        </td>
                                        <td class="p-2"><%=positionName%>
                                        </td>
                                        <td class="p-2"><%=payScale%>
                                        </td>
                                        <td class="p-2"><%=numberOfPositions%>
                                        </td>
                                        <td class="p-2"><%=qualifications%>
                                        </td>


                                    </tr>


                                    <%
                                        } %>
                                </table>
                            </div>

                        </section>
                        <% }
                        %>

                        <section class="page shadow" data-size="A4">
                            <h1 class="text-center mb-4"> শর্তাবলী </h1>

                            <div class="row mx-2 mt-1">
                                <table
                                        class="mt-3 w-100 rounded"
                                        border="1px solid #323233;"
                                        style="border-collapse: collapse"
                                >

                                    <tr>
                                        <td class="text-center px-3">১</td>
                                        <td class="p-2">আবেদনকারীকে (ক) পূর্ণ নাম (খ) পিতার নাম (গ) মাতার নাম (ঘ)
                                            স্থায়ী ঠিকানা (ঙ)
                                            বর্তমান ঠিকানা (চ) জন্ম তারিখ ও ২৮/১১/২০১৯ তারিখে সঠিক বয়স (ছ) শিক্ষাগত
                                            যােগ্যতা (জ) জাতীয়তা (ঝ) ধর্ম (ঞ) অভিজ্ঞতা (যদি থাকে) ইত্যাদি উল্লেখপূর্বক
                                            উপ-সচিব, মানব সম্পদ শাখা-২, বাংলাদেশ জাতীয় সংসদ সচিবালয়, শেরে বাংলা নগর ,
                                            ঢাকা-১২০৭ এই ঠিকানায় ডাকযােগে আবেদনপত্র আগামী ২৮/১১/২০১৯ তারিখের মধ্যে
                                            পৌছাতে হবে। ডাকযােগে ব্যতীত প্রাপ্ত আবেদনপত্র বাতিল বলে গণ্য হবে এবং
                                            নির্ধারিত তারিখের পর কোন আবেদনপত্র গ্রহণযােগ্য হবে না।
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center px-3">২</td>
                                        <td class="p-2">প্রার্থীর বয়সসীমা ২৮/১১/২০১৯ তারিখে ১৮-৩০ বছরের মধ্যে হতে হবে।
                                            বয়স
                                            সম্পর্কে কোনাে এফিডেভিট গ্রহণযােগ্য হবে না। মুক্তিযােদ্ধাদের পুত্র-কন্যা এবং
                                            শারীরিক প্রতিবন্ধীদের জন্য বয়সসীমা ৩২ বছর।
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center px-3">৩</td>
                                        <td class="p-2">আবেদনপত্রের সাথে (সত্যায়নকারী কর্মকর্তার নাম ও পদবিযুক্ত সিলসহ)
                                            (ক) প্রথম
                                            শ্রেণীর গেজেটেড কর্মকর্তা কর্তৃক সত্যায়িত শিক্ষাগত যােগ্যতার সকল সনদপত্রের
                                            কপি (খ) প্রথম শ্রেণীর গেজেটেড কর্মকর্তা কর্তৃক প্রদত্ত চারিত্রিক সনদপত্র (গ)
                                            ইউনিয়ন পরিষদের চেয়ারম্যান/সিটি কর্পোরেশনের ওয়ার্ড কাউন্সিলর /পৌরসভার
                                            মেয়র/কাউন্সিলর কর্তৃক প্রদত্ত নাগরিকত্বের সনদপত্র (ঘ) প্রথম শ্রেণীর গেজেটেড
                                            কর্মকর্তা কর্তৃক সত্যায়িত সদ্য তােলা পাসপাের্ট সাইজের ০৩ কপি ছবি (ঙ)
                                            অভিজ্ঞতার সনদপত্রের সত্যায়িত কপি
                                            (প্রযােজ্য ক্ষেত্রে) এবং (চ) জাতীয় পরিচয় পত্র/জন্ম নিবন্ধন পত্রের
                                            সত্যায়িত কপি সংযুক্ত করতে হবে।
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center px-3">৪</td>
                                        <td class="p-2"> সরকারি, আধা-সরকারি ও স্বায়ত্তশাসিত প্রতিষ্ঠানসমূহে কর্মরত
                                            প্রার্থীদের
                                            যথাযথ কর্তৃপক্ষের মাধ্যমে নির্ধারিত তারিখের মধ্যে আবেদন।
                                            করতে হবে (অগ্রিম কপি গ্রহণযােগ্য হবে না)।
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center px-3">৫</td>
                                        <td class="p-2">আবেদনকৃত খামের ওপর পদের নাম ও নিজ জেলার নাম উল্লেখ করতে হবে এবং
                                            অব্যবহৃত
                                            ৬/-(ছয়) টাকার ডাকটিকেট সংযুক্ত অপর একটি খামের ওপর প্রার্থীর পূর্ণ ঠিকানা
                                            (যােগাযােগের ঠিকানা) উল্লেখপূর্বক আবেদনপত্রের সাথে সংযুক্ত করতে হবে ।
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-center px-3">৬</td>
                                        <td class="p-2"> কোনাে ব্যক্তিকে জাতীয় সংসদ সচিবালয়ের সাংবাৎসরিক দৈনিকভিত্তিক
                                            পদে নিয়ােগ করা হয়ে থাকলে উল্লিখিত ব্যক্তির সাংবাৎসরিক | ভিত্তিতে
                                            নিযুক্তকালীন সংসদ সচিবালয়ের কোনাে শূন্য পদে সরাসরি নিয়ােগের ক্ষেত্রে
                                            তফসিলে বর্ণিত সর্বোচ্চ বয়ঃসীমা ও প্রশিক্ষণ
                                            সংক্রান্ত যােগ্যতা শিথিল করা যেতে পারে।
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center px-3">৭</td>
                                        <td class="p-2"> মাননীয় স্পীকার, ডেপুটি স্পীকার , চীফ হুইপ, সংসদ উপনেতা,
                                            বিরােধীদলীয় নেতা, বিরােধীদলীয় উপনেতা ও হুইপগণের কার্যালয়ে কর্মরত কোনাে
                                            বহিরাগত কর্মকর্তা বা কর্মচারী উক্ত পদের জন্য নির্ধারিত শিক্ষাগত যােগ্যতা ও
                                            বয়সসীমার মধ্যে নিয়ােগপ্রাপ্ত হয়ে থাকলে তার নিযুক্তকালীন কার্যকালের জন্য
                                            অন্যান্য যােগ্যতা সাপেক্ষে সংসদ সচিবালয়ের কোনাে শূন্য পদে সরাসরি নিয়ােগের
                                            ক্ষেত্রে
                                            তফসিলে বর্ণিত সর্বোচ্চ বয়ঃসীমা শিথিল করা যেতে পারে। প্রমাণক হিসেবে
                                            নিয়ােগপত্র দাখিল করতে হবে।
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center px-3">৮</td>
                                        <td class="p-2"> অসমাপ্ত ও ত্রুটিপূর্ণ আবেদন সরাসরি বাতিল বলে গণ্য হবে।
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center px-3">৯</td>

                                        <td class="p-2">
                                            নিয়ােগের ক্ষেত্রে সরকারের বিদ্যমান বিধি-বিধান এবং পরবর্তীতে সংশ্লিষ্ট
                                            বিধি-বিধানে কোন সংশােধন হলে তা অনুসরণ করা হবে। |
                                        </td>
                                    </tr>

                                    

                                </table>

                            </div>

                        </section>
                        <section class="page shadow" data-size="A4">


                            <div class="row mx-2 mt-1">
                                <table
                                        class="mt-3 w-100 rounded"
                                        border="1px solid #323233;"
                                        style="border-collapse: collapse"
                                >


                                    <tr>
                                        <td class="text-center px-3">১০</td>
                                        <td class="p-2">প্রার্থী নির্বাচনের ক্ষেত্রে:
                                            (ক) সরকারি বিধি-বিধান মােতাবেক সরকার নির্ধারিত জেলা ও অন্যান্য কোটা সংরক্ষণ
                                            করা হবে। মুক্তিযােদ্ধা ও শহীদ মুক্তিযােদ্ধাদের
                                            সন্তানদের ক্ষেত্রে মুক্তিযুদ্ধবিষয়ক মন্ত্রণালয় কর্তৃক প্রদত্ত পিতা/মাতার
                                            মুক্তিযুদ্ধের সনদপত্র এবং মুক্তিযােদ্ধার সন্তান হিসেবে প্রমাণপত্রের
                                            সত্যায়িত ফটোকপি আবেদনপত্রের সাথে অবশ্যই সংযুক্ত করতে হবে। এক জেলার প্রার্থী
                                            অন্য জেলা হতে আবেদন
                                            করতে পারবে না।
                                            (খ) মুক্তিযােদ্ধা/শহীদ মুক্তিযােদ্ধার পুত্র-কন্যার
                                            পুত্র-কন্যা হিসেবে চাকরি প্রার্থীকে চাকরির আবেদনপত্রের সাথে তাদের পিতার
                                            পিতা/পিতার মাতা/মাতার পিতা/মাতার মাতা (প্রযােজ্য ক্ষেত্রে) এর মুক্তিযােদ্ধা
                                            সার্টিফিকেট, যা যথাযথভাবে উপযুক্ত কর্তৃপক্ষ
                                            কর্তৃক স্বাক্ষরিত ও প্রতিস্বাক্ষরিত হতে হবে এবং এর সত্যায়িত কপি আবেদনপত্রের
                                            সাথে সংযুক্ত করতে হবে।
                                            (গ) আবেদনকারী মুক্তিযােদ্ধা/শহীদ মুক্তিযােদ্ধার
                                            পুত্র-কন্যার পুত্র-কন্যা হলে আবেদনকারী যে মুক্তিযােদ্ধা/শহীদ মুক্তিযােদ্ধার
                                            পুত্র
                                            কন্যার পুত্র-কন্যা এই মর্মে সংশ্লিষ্ট ইউনিয়ন পরিষদের চেয়ারম্যান/সিটি
                                            কর্পোরেশনের ওয়ার্ড কাউন্সিলর/পৌরসভার
                                            মেয়র/কাউন্সিলর কর্তৃক প্রদত্ত সার্টিফিকেট আবেদনপত্রের সাথে সংযুক্ত করতে
                                            হবে।
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center px-3">১১</td>
                                        <td class="p-2"> আবেদনকারীগণকে নির্বাচনী পরীক্ষায় অংশগ্রহণের জন্য কোনাে টিএ/ডিএ
                                            প্রদান করা হবে না। প্রার্থী নির্বাচনে কর্তৃপক্ষের সিদ্ধান্তই
                                            চূড়ান্ত বলে বিবেচিত হবে।
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center px-3">১২</td>
                                        <td class="p-2"> আবেদনপত্রের সাথে সিনিয়র সচিব, বাংলাদেশ জাতীয় সংসদ সচিবালয়,
                                            শেরেবাংলা নগর, ঢাকা-১২০৭ এর অনুকূলে ট্রেজারি চালানের মাধ্যমে ১০০/-(একশত)
                                            টাকা (অফেরতযােগ্য) ১-০২০১-০০০১-২০৩১ নম্বর কোডে জমাদানপূর্বক চালানের মূল কপি
                                            আবেদনপত্রের
                                            সাথে সংযুক্ত করতে হবে।
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center px-3">১৩</td>
                                        <td class="p-2"> কর্তৃপক্ষ পদের সংখ্যা হ্রাস/বৃদ্ধি এবং বিজ্ঞপ্তি বাতিল করার
                                            ক্ষমতা সংরক্ষণ করেন।
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center px-3">১৪</td>
                                        <td class="p-2"> নিয়ােগ বিজ্ঞপ্তিটি বাংলাদেশ জাতীয় সংসদ সচিবালয়ের
                                            ১WM8.parliament.gov.bd এই ওয়েব সাইটে পাওয়া যাবে এবং পরবর্তীতে
                                            এ সংক্রান্ত তথ্য এই ওয়েব সাইটে পাওয়া যাবে।
                                        </td>
                                    </tr>



                                </table>

                            </div>

                        </section>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>

<script src="<%=context_folder%>/assets/backup/js/jquery-3.5.1.slim.min.js"></script>
<script src="<%=context_folder%>/assets/backup/js/bootstrap.min.js"></script>
<script src="<%=context_folder%>/assets/backup/js/popper.min.js"></script>
<script src="<%=context_folder%>/assets/backup/scripts/login/main.js"></script>
<script src="<%=context_folder%>/assets/backup/scripts/util1.js"></script>
<script src="<%=context_folder%>/assets/backup/scripts/pb.js"></script>
<script src="<%=context_folder%>/assets/backup/global/plugins/jquery.min.js"></script>
<script src="<%=context_folder%>/assets/backup/global/plugins/jquery-validation/js/jquery.validate.js"></script>
<script src="<%=context_folder%>/assets/backup/global/plugins/jquery-ui/jquery-ui.min.js"></script>

<script>

    function downloadTemplateAsPdf(divId, fileName) {
        let content = document.getElementById(divId);
        const opt = {
            margin: 0.5,
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4'}
        };
        html2pdf().from(content).set(opt).save();
    }
</script>
</body>
</html>