<%@page import="util.*" %>
<%@ page import="user.UserServlet" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_maintenance.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentDTO" %>
<%@ page import="vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentRepository" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="vm_vehicle_parts.Vm_vehicle_partsRepository" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionRepository" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDTO" %>
<%@ page import="job_applicant_application.Job_applicant_applicationRepository" %>
<%@ page import="admit_card.Admit_cardDTO" %>
<%@ page import="admit_card.Admit_cardRepository" %>
<%@ page import="admit_card.Admit_cardDAO" %>
<%@ page import="recruitment_job_specific_exam_type.RecruitmentJobSpecificExamTypeDTO" %>
<%@ page import="recruitment_job_specific_exam_type.JobSpecificExamTypeRepository" %>
<%@ page import="bangla_date_converter.BanglaDateConverter" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="recruitment_seat_plan.Recruitment_seat_planDTO" %>
<%@ page import="recruitment_seat_plan.Recruitment_seat_planRepository" %>
<%@ page import="recruitment_exam_venue.Recruitment_exam_venueDTO" %>
<%@ page import="recruitment_exam_venue.Recruitment_exam_venueRepository" %>
<%@ page import="recruitment_seat_plan.RecruitmentSeatPlanChildDTO" %>
<%@ page import="recruitment_seat_plan.RecruitmentSeatPlanChildRepository" %>

<%
    String context_folder = request.getContextPath();
    String Language = "Bangla";
    int my_language = 1;
%>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/css/all.min.css"
          integrity="sha512-BnbUDfEUfV0Slx6TunuB042k9tuKe3xrD6q4mg5Ed72LTgzDIcLPxg6yI2gcMFRyomt+yJJxE+zJwNmxki6/RA=="
          crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://fonts.maateen.me/solaiman-lipi/font.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.js"></script>

    <title>View Notice</title>
    <style>

        .custom-toast {
            visibility: hidden;
            width: 300px;
            min-height: 60px;
            color: #fff;
            text-align: center;
            padding: 20px 30px;
            position: fixed;
            z-index: 1;
            right: 23px;
            bottom: 60px;
            font-size: 13pt;
        }

        #toast_message {
            background-color: #008aa6;
        }

        #error_message {
            background-color: #ca5e59;
        }

        #toast_message.show {
            visibility: visible;
            -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
            animation: fadein 0.5s, fadeout 0.5s 2.5s;
        }

        #error_message.show {
            visibility: visible;
            -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
            animation: fadein 0.5s, fadeout 0.5s 2.5s;
        }

        @-webkit-keyframes fadein {
            from {
                right: -38%;
                opacity: 0;
            }
            to {
                right: 23px;
                opacity: 1;
            }
        }

        @keyframes fadein {
            from {
                right: -38%;
                opacity: 0;
            }
            to {
                right: 23px;
                opacity: 1;
            }
        }

        @-webkit-keyframes fadeout {
            from {
                right: 23px;
                opacity: 1;
            }
            to {
                right: -38%;
                opacity: 0;
            }
        }

        @keyframes fadeout {
            from {
                right: 23px;
                opacity: 1;
            }
            to {
                right: -38%;
                opacity: 0;
            }
        }


        /* ==================== General Style =========== */

        * {
            margin: 0;
            padding: 0;
            outline: none;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            text-decoration: none;
            list-style: none;
        }

        /* ================== Body =================== */
        body {
        <%--background: url(<%=context_folder%>/images/pin_retrieve/BG.png) no-repeat;--%> background: rgb(0, 159, 201);
            background: radial-gradient(circle, rgba(0, 159, 201, 1) 6%, rgba(0, 54, 115, 1) 60%, rgba(0, 27, 93, 1) 91%);
            max-width: 100vw;
            height: 100vh;
            background-position: center center;
            font-family: 'SolaimanLipi', sans-serif;
            -webkit-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease;
            background-size: cover;
            background-attachment: fixed;
        }

        /* ======================== Top Logo area ================== */
        .leftlogo img {
            width: 159px;
            height: 113px;
            margin-top: 30px;
            margin-left: 30px;
        }

        .rightlogo img {
            width: 175px;
            height: 83px;
            margin-top: 60px;
            float: right;
            margin-right: 60px;
        }

        .leftnews {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-box-pack: end;
            -ms-flex-pack: end;
            justify-content: flex-end;
            justify-items: flex-end;
            float: right;
            padding-top: 35%;
        }


        .rightnews {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-box-pack: start;
            -ms-flex-pack: start;
            justify-content: flex-start;
            justify-items: flex-start;
            float: left;
            padding-top: 35%;
        }

        .formrelative {
            position: relative;
        }

        .bottomrightcontent {
            position: relative;
        }

        .formarea {
            display: block;
            float: left;
            width: 90%;
            margin: 0 5%;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            height: auto;
            background-color: rgba(255, 255, 255, 0.15);
            border-radius: 20px;
            color: #fff;
            /*padding: 26px 47px 37px 47px;*/
        }

        .formarea h2 {
            margin-bottom: 24px;
        }

        .rightallbutton {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-box-pack: end;
            -ms-flex-pack: end;
            justify-content: flex-end;
            color: #fff;
        }

        .rightallbutton button {
            margin: 0px 0px 0px 10px;
        }

        .rightallbutton .btnleft {
            color: #000;
            background-color: #FFE600;
            border: none;
            border-radius: 8px;
        }

        .rightallbutton .btnright {
            color: #fff;
            background-color: #00D4FF;
            border: none;
            border-radius: 8px;
        }

        .maintitle h2 {
            font-size: 2.25rem;
            margin-bottom: 25px;
        }


        input[type='text']::-webkit-input-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='text']::-moz-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='text']:-ms-input-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='text']::-ms-input-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='text']::placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='password']::-webkit-input-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='password']::-moz-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='password']:-ms-input-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='password']::-ms-input-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='password']::placeholder {
            color: #B1CADB;
            font-size: 14px;
        }


        .leftnews img {
            width: 256px;
            height: 154px;
            padding-bottom: 10px;
        }

        img.img-fluid.circularimage {
            margin-bottom: 15px;
        }

        img.img-fluid.admincardimage {
            margin-bottom: 15px;
        }

        .rightnews img {
            width: 256px;
            height: 154px;
            padding-bottom: 10px;
        }

        .maintitle h2 {
            font-size: 1.5rem;
            margin-bottom: 0px;
            padding: 12px 0px;
        }

        .leftlogo img {
            width: 130px;
            height: 90px;
            margin-top: 20px;
            margin-left: 30px;
        }

        .rightlogo img {
            width: 120px;
            height: 57px;
            margin-top: 50px;
            float: right;
            margin-right: 60px;
        }

        .mainlogo.text-center img {
            width: 90px;
            height: 90px;
            line-height: 90px;
        }

        .leftnews {
            padding-top: 140px;
        }

        .rightnews {
            padding-top: 140px;
        }

        h2.title {
            font-size: 25px;
        }

        .formarea h2 {
            margin-bottom: 10px;
        }

        /*.formarea {*/
        /*    padding: 24px 47px 25px 47px;*/
        /*}*/

        .leftnews img {
            width: 220px;
            height: 132px;
        }

        .rightnews img {
            width: 220px;
            height: 132px;
        }

        img.img-fluid.circularimage {
            margin-bottom: 20px;
        }

        img.img-fluid.admincardimage {
            margin-bottom: 20px;
        }

        .songshodimage img {
            width: 100%;
            height: 100%;
            margin-top: 30px;
        }

        .btn-border-radius {
            border-radius: 6px !important;
        }

        /* ======================== Responsive Area ================== */

        @media screen and (max-width: 1537px) {
            .songshodimage img {
                width: 78%;
                height: 100%;
                margin-top: 15px;
            }

            .leftnews {
                padding-top: 30%;
            }

            .rightnews {
                padding-top: 30%;
            }

        }

        @media screen and (max-width: 1480px) {

            .songshodimage img {
                width: 78%;
                height: 100%;
                margin-top: 15px;
            }

            .leftnews {
                padding-top: 140px;
            }

            .rightnews {
                padding-top: 140px;
            }


            .maintitle h2 {
                font-size: 1.25rem;
                margin-bottom: 0px;
                padding: 12px 0px;
            }

            /*.formarea {*/
            /*    padding: 24px 47px 26px 47px;*/
            /*}*/
        }


        @media screen and (max-width: 1366px) {

            .songshodimage img {
                width: 70%;
                height: 100%;
                margin-top: 0px;
            }

            .leftnews {
                padding-top: 140px;
            }

            .rightnews {
                padding-top: 140px;
            }


            .maintitle h2 {
                font-size: 1.25rem;
                margin-bottom: 0px;
                padding: 12px 0px;
            }

            /*.formarea {*/
            /*    padding: 24px 47px 26px 47px;*/
            /*}*/
            .leftlogo img {

                margin-top: 10px;

            }

            .rightlogo img {
                margin-top: 40px;

            }

        }


        @media screen and (max-width: 1100px) {
            .maintitle h2 {
                font-size: 1.15rem;
                margin-bottom: 0px;
                padding: 12px 0px;
            }

            /*.formarea {*/
            /*    padding: 24px 47px 26px 47px;*/
            /*}*/
            .rightallbutton .btnleft {
                font-size: 12px;
            }

            .rightallbutton .btnright {
                font-size: 12px;
            }

            /*.formarea {*/
            /*    padding: 24px 20px 26px 20px;*/
            /*}*/
            .formarea {
                display: block;
                float: left;
                width: 100%;
                margin: 0;
            }

            img.img-fluid.circularimage {
                margin-bottom: 10px;
            }

            img.img-fluid.admincardimage {
                margin-bottom: 10px;
            }

            .songshodimage img {
                width: 100%;
            }

        }

        @media screen and (max-width: 980px) {
            .maintitle h2 {
                font-size: 1rem;

            }

            .leftnews {
                padding-top: 135px;
            }

            .rightnews {
                padding-top: 135px;
            }

            img.img-fluid.circularimage {
                margin-bottom: 11px;
            }

            img.img-fluid.admincardimage {
                margin-bottom: 11px;
            }

        }

        @media screen and (max-width: 790px) {
            .maintitle h2 {
                font-size: 0.90rem;

            }

            .leftnews {
                padding-top: 135px;
            }

            .rightnews {
                padding-top: 135px;
            }

            img.img-fluid.circularimage {
                margin-bottom: 11px;
            }

            img.img-fluid.admincardimage {
                margin-bottom: 11px;
            }

        }

        @media screen and (max-width: 767px) {

            .leftnews {
                padding-top: 30px;
                padding-bottom: 20px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .rightnews {
                padding-top: 20px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .leftlogo img {
                margin-left: 6px;
            }

            .rightlogo img {
                margin-right: 8px;
            }

            .mainlogo.text-center img {
                width: 25%;
                height: auto;
            }

            img.img-fluid.circularimage {
                margin-bottom: 0px;
            }

            .leftnews img {
                padding-bottom: 0px;
            }

            .songshodimage img {
                margin-top: 0px;
            }

        }

        @media screen and (max-width: 464px) {

            .leftnews {
                padding-top: 30px;
                padding-bottom: 20px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .rightnews {
                padding-top: 20px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .leftnews img {
                width: 195px;
                height: 132px;
            }

            .rightnews img {
                width: 195px;
                height: 132px;
            }

            .leftlogo img {
                margin-left: 6px;

            }

            .rightlogo img {
                margin-right: 8px;
            }

            .mainlogo.text-center img {
                width: 25%;
                height: auto;
            }

            img.img-fluid.circularimage {
                margin-bottom: 0px;
            }

            .leftnews img {
                padding-bottom: 0px;
            }

            .songshodimage img {
                margin-top: 0px;
            }

        }


        @media screen and (max-width: 420px) {

            .leftnews {
                padding-top: 10px;
                padding-bottom: 10px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .rightnews {
                padding-top: 10px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .leftnews img {
                width: 170px;
                height: 132px;
            }

            .rightnews img {
                width: 170px;
                height: 132px;
            }

            .leftlogo img {
                margin-left: 6px;

            }

            .rightlogo img {
                margin-right: 8px;
            }

            .mainlogo.text-center img {
                width: 25%;
                height: auto;
            }

            img.img-fluid.circularimage {
                margin-bottom: 0px;
            }

            .leftnews img {
                padding-bottom: 0px;
            }

            .songshodimage img {
                margin-top: 0px;
            }

            img.img-fluid.admincardimage {
                margin-bottom: 0px;
            }

        }

        @media screen and (max-width: 370px) {

            .leftnews {
                padding-top: 10px;
                padding-bottom: 10px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .rightnews {
                padding-top: 10px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .leftnews img {
                width: 105px;
                height: 70px;
            }

            .rightnews img {
                width: 105px;
                height: 70px;
            }

            .leftlogo img {
                margin-left: 6px;

            }

            .rightlogo img {
                margin-right: 8px;
            }

            .mainlogo.text-center img {
                width: 25%;
                height: auto;
            }

            img.img-fluid.circularimage {
                margin-bottom: 0px;
            }

            .leftnews img {
                padding-bottom: 0px;
            }

            .songshodimage img {
                margin-top: 0px;
            }

            img.img-fluid.admincardimage {
                margin-bottom: 0px;
            }

        }

    </style>
</head>
<body>


<%


    String jobDescriptionId = request.getParameter("jobDescriptionId");
    String seatPlan = request.getParameter("seatPlanId");


    long jobId = Long.parseLong(jobDescriptionId);
    long seatPlanId = Long.parseLong(seatPlan);
    //long approveId = Long.parseLong(approve);

    Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = Recruitment_job_descriptionRepository.getInstance().getRecruitment_job_descriptionDTOByID(jobId);
    Recruitment_seat_planDTO recruitment_seat_planDTO = Recruitment_seat_planRepository.getInstance().getRecruitment_seat_planDTOByiD(seatPlanId);

    String examType = "লিখিত";

    SimpleDateFormat format_dateOfExam = new SimpleDateFormat("dd MMMM yyyy");
    String formatted_dateOfExam = format_dateOfExam.format(new Date(Long.parseLong(recruitment_seat_planDTO.examDate + "")));
    String formattedDate = Admit_cardDAO.getDateBanglaFromEnglish(formatted_dateOfExam);
    String finalDate = UtilCharacter.newDateFormat(UtilCharacter.convertNumberEnToBn(formattedDate));

    String grade = CatRepository.getInstance().getText(Language, "job_grade", recruitment_job_descriptionDTO.jobGradeCat);
    String postName = UtilCharacter.getDataByLanguage(Language, recruitment_job_descriptionDTO.jobTitleBn, recruitment_job_descriptionDTO.jobTitleEn);
    String weekDay = TimeFormat.weekDayInBangla[new Date(recruitment_seat_planDTO.examDate).getDay()] ;
    String examTime = recruitment_seat_planDTO.examTime;

    //no need recruitment test name id. Since every recruitment job description id is unique
    List<Recruitment_seat_planDTO> recruitment_seat_planDTOS = Recruitment_seat_planRepository.getInstance().getRecruitmentSeatPlanDTOByRecruitmentJobDescriptionId(jobId);

    recruitment_seat_planDTOS = recruitment_seat_planDTOS.stream()
            .filter(e -> e.recruitmentJobSpecificExamTypeId == recruitment_seat_planDTO.recruitmentJobSpecificExamTypeId)
            .collect(Collectors.toList());

    String noticeHeaderBN = "বাংলাদেশ জাতীয় সংসদ সচিবালয়ের " + grade + " গ্রেডভুক্ত " + postName + " পদে জনবল নিয়োগের লক্ষ্যে আবেদনকৃত প্রার্থীদের <b>লিখিত</b> পরীক্ষা আগামী " + finalDate + ", " + weekDay + " " + examTime + "  নিম্নোক্ত ভেন্যুসমূহে অনুষ্ঠিত হবে।";


%>

<style>

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .4in;
        background: white;
        margin-bottom: 10px;
    }

</style>

<div class="kt-content p-0" id="kt_content">
    <div class="">
        <div class="kt-portlet">
            <div class="kt-portlet__body page-bg p-0" id="bill-div">

                <div class="mx-1">
                    <div class="container d-flex justify-content-center" id="to-print-div">
                        <section class="page shadow" data-size="A4">
                            <div class="row">
                                <div class="col-12 text-right py-1" data-html2canvas-ignore="true">
                                    <button type="button" class="btn" id='download-pdf'
                                            onclick="downloadTemplateAsPdf('to-print-div', 'Written Exam Notice');">
                                        <i class="fa fa-file-download fa-2x" style="color: gray" aria-hidden="true"></i>
                                    </button>
                                </div>
                                <div class="col-12 text-center">

                                    <h5 class="mt-2 font-weight-bold">
                                        <%=UtilCharacter.getDataByLanguage(Language, "বাংলাদেশ জাতীয় সংসদ সচিবালয়", "Bangladesh National Parliament Secretariat")%>
                                    </h5>
                                    <h5 class="text-dark">
                                        <%=UtilCharacter.getDataByLanguage(Language, "মানব সম্পদ উইং", "Human Resources Wing")%>
                                    </h5>
                                    <h5 class="text-dark">
                                        <%=UtilCharacter.getDataByLanguage(Language, "মানব সম্পদ শাখা-৩", "Human Resources Wing")%>
                                    </h5>
                                    <a class="text-dark" href="https://www.Parliament.gov.bd" target="_blank">
                                        <u><%=UtilCharacter.getDataByLanguage(Language, "www.Parliament.gov.bd", "www.Parliament.gov.bd")%>
                                        </u>
                                    </a>

                                </div>
                            </div>

                            <div class="row mt-2 ">
                                <div class="col-12 " style="display: flex;justify-content: space-between;">

                                    <span>
                                        <%=UtilCharacter.getDataByLanguage(Language, "নংঃ- ", "Subject:- ")%>&nbsp;
                                    </span>

                                    <span>
                                        <% String formattedBillBanglaDateInWord = BanglaDateConverter.getFormattedBanglaDate(recruitment_seat_planDTO.examDate, true);
                                            String formattedBillDateInWord = DateUtils.getDateInWord("bangla", recruitment_seat_planDTO.examDate);

                                        %>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td rowspan="2">তারিখ:</td>
                                                <td style="text-align: center;"><%=formattedBillBanglaDateInWord%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="border-top: 1px solid black; text-align: center;">
                                                    <%=formattedBillDateInWord%>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </span>
                                </div>
                            </div>

                            <div class=" mt-10 mb-3 text-center">

                                <u style="text-underline-position: under;"><%=UtilCharacter.getDataByLanguage(Language, "বিজ্ঞপ্তি", "Notice")%>
                                </u>

                            </div>

                            <div class=" mt-10 mx-2" style="text-indent: 3rem;">

                                <%=UtilCharacter.getDataByLanguage(Language, noticeHeaderBN, "")%>

                            </div>


                            <div class="row mx-2 mt-1">
                                <table
                                        class="mt-3 w-100 rounded"
                                        border="1px solid #323233;"
                                        style="border-collapse: collapse"
                                >
                                    <thead>
                                    <tr>
                                        <th class="text-center"
                                            style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "কেন্দ্রের নাম", "Venue Name")%>
                                        </th>
                                        <th class="text-center"
                                            style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "প্রার্থীদের রোল নম্বর", "Roll Number")%>
                                        </th>


                                    </tr>
                                    </thead>

                                    <tbody>

                                    <%
                                        String rows = "";
                                        if (recruitment_seat_planDTOS != null && recruitment_seat_planDTOS.size() > 0) {

                                            for (Recruitment_seat_planDTO recruitment_seat_planDTO1 : recruitment_seat_planDTOS) {
                                                Recruitment_exam_venueDTO recruitment_exam_venueDTO = Recruitment_exam_venueRepository.getInstance()
                                                        .getRecruitment_exam_venueDTOByiD(recruitment_seat_planDTO1.recruitmentExamVenueId);

                                                List<RecruitmentSeatPlanChildDTO> recruitmentSeatPlanChildDTOS = RecruitmentSeatPlanChildRepository.getInstance()
                                                        .getRecruitmentSeatPlanChildDTOByrecruitmentSeatPlanId(recruitment_seat_planDTO1.iD);

                                                String rollRange = "";
                                                if(recruitmentSeatPlanChildDTOS != null && recruitmentSeatPlanChildDTOS.size() > 0){
                                                    Long startRoll = recruitmentSeatPlanChildDTOS.stream()
                                                            .filter(e -> !e.startRoll.isEmpty())
                                                            .mapToLong(e ->  Long.parseLong(e.startRoll)).min().orElse(-1);

                                                    Long endRoll = recruitmentSeatPlanChildDTOS.stream()
                                                            .filter(e -> !e.endRoll.isEmpty())
                                                            .mapToLong(e -> Long.parseLong(e.endRoll)).max().orElse(-1);

                                                    String sRoll1 = startRoll.equals("-1")? "":String.valueOf(startRoll);
                                                    String eRoll1 = endRoll.equals("-1")? "":String.valueOf(endRoll);
                                                    sRoll1 = UtilCharacter.convertNumberEnToBn(sRoll1);
                                                    eRoll1 = UtilCharacter.convertNumberEnToBn(eRoll1);
                                                    rollRange = sRoll1+" - "+eRoll1;
                                                }




                                                rows += "<tr>";
                                                rows += "<td class=\"p-2\">" + UtilCharacter.getDataByLanguage(Language, recruitment_exam_venueDTO.nameBn, recruitment_exam_venueDTO.nameEn) + "</td>";
                                                rows += "<td class=\"p-2\">"+rollRange+"</td>";
                                                rows += "</tr>";
                                            }
                                        }
                                    %>
                                    <%=rows%>
                                    </tbody>

                                </table>
                            </div>


                        </section>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>

<script src="<%=context_folder%>/assets/backup/js/jquery-3.5.1.slim.min.js"></script>
<script src="<%=context_folder%>/assets/backup/js/bootstrap.min.js"></script>
<script src="<%=context_folder%>/assets/backup/js/popper.min.js"></script>
<script src="<%=context_folder%>/assets/backup/scripts/login/main.js"></script>
<script src="<%=context_folder%>/assets/backup/scripts/util1.js"></script>
<script src="<%=context_folder%>/assets/backup/scripts/pb.js"></script>
<script src="<%=context_folder%>/assets/backup/global/plugins/jquery.min.js"></script>
<script src="<%=context_folder%>/assets/backup/global/plugins/jquery-validation/js/jquery.validate.js"></script>
<script src="<%=context_folder%>/assets/backup/global/plugins/jquery-ui/jquery-ui.min.js"></script>

<script>
    let downloadTimer;

    $(document).ready(function () {
        $("#password_submit").prop("disabled", true);
        $("#resend_otp").prop("disabled", true);

    });

    const phoneNoInput = document.getElementById("phoneNo");
    const emailInput = document.getElementById("email");
    const validChars = new Set([
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯',
    ]);

    const validOTPChars = new Set([
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
    ]);

    phoneNoInput.addEventListener("keypress", function (event) {
        if (!validChars.has(event.key)) {
            event.preventDefault();
        }
    });

    document.getElementById('otp_input').addEventListener("keypress", function (event) {
        if (!validOTPChars.has(event.key)) {
            event.preventDefault();
        }
    });

    phoneNoInput.addEventListener("keyup", function (event) {
        if (event.key === 'Enter') {
            event.preventDefault();
            resetCitizenPinCode();
        }
    });

    function sendOTP(isResend) {
        const pinCodeRetrieveForm = $("#pinCodeRetrieveForm");
        const url = "UserServlet?actionType=sendOTP";
        $.ajax({
            type: "POST",
            url: url,
            data: pinCodeRetrieveForm.serialize(),
            dataType: 'text',
            success: function (data) {
                const response = JSON.parse(data);
                if (response.success) {
                    showToast(response.messageBn, response.messageEn);
                    if (!isResend) {
                        $("#mobile_submit").prop("disabled", true);
                        $("#mobile_submit_div").removeClass('d-flex');
                        $("#mobile_submit_div").hide();
                        document.getElementById("phoneNo").readOnly = true;
                        document.getElementById("email").readOnly = true;
                        $("#methodDiv").removeClass('d-flex');
                        $("#methodDiv").hide();
                        $("#otp_password_div").show();
                    } else {
                        $("#resend_otp").prop("disabled", true);
                        $("#resend_otp_div").hide();
                        clearOTPPasswords();
                        clearInterval(downloadTimer);
                    }
                    $("#password_submit").prop("disabled", false);
                    $("#password_submit_div").show();
                    timer(response.TimeRemaining);
                } else {
                    showError(response.messageBn, response.messageEn);
                }
            },
            error: function (req, err) {
                showError("ওটিপি প্রদান করা সম্ভব হয়নি", "Could not send OTP");
            }
        });
    }

    function clearOTPPasswords() {
        $("#otp_input").val("");
        $("#password").val("");
        $("#confirmPassword").val("");
    }

    function submitPassword() {
        const pinCodeRetrieveForm = $("#pinCodeRetrieveForm");
        if ($("#otp_input").val() === "") {
            showToast("ওটিপি ইনপুট দিন", "Please Insert OTP");
            return;
        }

        const passwordVal = $("#password").val();
        if (passwordVal === "") {
            showToast("পাসওয়ার্ড ইনপুট দিন", "Please Insert Password");
            return;
        }

        const confirmPasswordVal = $("#confirmPassword").val();
        if (confirmPasswordVal === "") {
            showToast("পাসওয়ার্ড কনফার্ম করুন", "Please Confirm Password");
            return;
        }

        if (passwordVal.length < <%=UserServlet.MIN_PASSWORD_LENGTH%>) {
            showToast("পাসওয়ার্ড ন্যূনতম দৈর্ঘ্যের চেয়ে ছোট হয়েছে", "Password is too short");
            return;
        }

        if (confirmPasswordVal !== passwordVal) {
            showToast("একই পাসওয়ার্ড দিয়ে নিশ্চিত করুন", "Confirm with same password");
            return;
        }

        const url = "UserServlet?actionType=validatePasswordOTP";
        $.ajax({
            type: "POST",
            url: url,
            data: pinCodeRetrieveForm.serialize(),
            dataType: 'text',
            success: function (data) {
                console.log("data", data);
                const response = JSON.parse(data);
                if (response.success) {
                    showToast(response.messageBn, response.messageEn);
                    setTimeout(
                        () => window.location.replace("<%=request.getContextPath()%>" + response.redirectPath),
                        3000
                    );
                } else {
                    showError(response.messageBn, response.messageEn);
                }
            },
            error: function (req, err) {
                showError("পাসওয়ার্ড পরিবর্তন ব্যর্থ হয়েছে!", "Password change failed!");
            }
        });
    }

    function timer(timeLeft) {
        downloadTimer = setInterval(function () {
            if (timeLeft <= 0) {
                clearInterval(downloadTimer);
                document.getElementById("p_message").innerHTML = "ওটিপি মেয়াদোত্তীর্ণ হয়েছে";
                timerOut();
            } else {
                let minutes = Math.floor(timeLeft / 60);
                let seconds = Math.floor(timeLeft % 60);
                document.getElementById("p_message").innerHTML = "ওটিপি মেয়াদোত্তীর্ণ হবে "
                    + convertToBangla(minutes) + " মিনিট "
                    + convertToBangla(seconds) + " সেকেন্ড";
            }
            timeLeft -= 1;
        }, 1000);
    }

    function timerOut() {
        $("#password_submit").prop("disabled", true);
        $("#password_submit_div").hide();
        $("#resend_otp").prop("disabled", false);
        $("#resend_otp_div").show();
        clearOTPPasswords();
    }

    function showToast(bn, en) {
        const toastMessageDiv = document.getElementById("toast_message_div");
        toastMessageDiv.innerHTML = '';
        const my_language = document.getElementById('my_language').value;
        toastMessageDiv.innerHTML = parseInt(my_language) === 1 ? bn : en;

        document.getElementById('toast_message').classList.add("show");

        setTimeout(() => document.getElementById('toast_message').classList.remove("show"), 3000);
    }

    function showError(bn, en) {
        const toastMessageDiv = document.getElementById("error_message_div");
        toastMessageDiv.innerHTML = '';
        const my_language = document.getElementById('my_language').value;
        toastMessageDiv.innerHTML = parseInt(my_language) === 1 ? bn : en;

        document.getElementById('error_message').classList.add("show");

        setTimeout(() => document.getElementById('error_message').classList.remove("show"), 3000);
    }

    function convertToBangla(str) {
        str = String(str);
        str = str.replaceAll('0', '০');
        str = str.replaceAll('1', '১');
        str = str.replaceAll('2', '২');
        str = str.replaceAll('3', '৩');
        str = str.replaceAll('4', '৪');
        str = str.replaceAll('5', '৫');
        str = str.replaceAll('6', '৬');
        str = str.replaceAll('7', '৭');
        str = str.replaceAll('8', '৮');
        str = str.replaceAll('9', '৯');
        return str;
    }

    let currentValue = 'mobile';

    function handleMethodRadioClick(methodRadio) {
        currentValue = methodRadio.value;
        if (currentValue == 'mobile') {
            document.getElementById('p_message').innerText = 'পাসওয়ার্ড রিসেটের জন্য ওটিপি পেতে আপনার মোবাইল নম্বর (বাংলায়/ইংরেজিতে)টি দিন';
            $("#mobileNoDiv").show();
            $("#emailDiv").hide();
        } else {
            document.getElementById('p_message').innerText = 'পাসওয়ার্ড রিসেটের জন্য ওটিপি পেতে আপনার ইমেইল আইডিটি দিন';
            $("#mobileNoDiv").hide();
            $("#emailDiv").show();
        }

    }
    function downloadTemplateAsPdf(divId, fileName) {
        let content = document.getElementById(divId);
        const opt = {
            margin: 0.5,
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4'}
        };
        html2pdf().from(content).set(opt).save();
    }
</script>
</body>
</html>