<%@page import="util.*" %>
<%@ page import="user.UserServlet" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page import="pb.*" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionRepository" %>
<%@ page import="admit_card.Admit_cardDAO" %>
<%@ page import="bangla_date_converter.BanglaDateConverter" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="recruitment_seat_plan.Recruitment_seat_planDTO" %>
<%@ page import="recruitment_seat_plan.Recruitment_seat_planRepository" %>
<%@ page import="recruitment_exam_venue.Recruitment_exam_venueDTO" %>
<%@ page import="recruitment_exam_venue.Recruitment_exam_venueRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="recruitment_seat_plan.RecruitmentSeatPlanChildDTO" %>
<%@ page import="recruitment_seat_plan.RecruitmentSeatPlanChildRepository" %>
<%@ page import="recruitment_job_specific_exam_type.RecruitmentJobSpecificExamTypeDTO" %>
<%@ page import="recruitment_job_specific_exam_type.JobSpecificExamTypeRepository" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDTO" %>
<%@ page import="job_applicant_application.Job_applicant_applicationRepository" %>

<%
    String context_folder = request.getContextPath();
    String Language = "Bangla";
    int my_language = 1;
%>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/css/all.min.css"
          integrity="sha512-BnbUDfEUfV0Slx6TunuB042k9tuKe3xrD6q4mg5Ed72LTgzDIcLPxg6yI2gcMFRyomt+yJJxE+zJwNmxki6/RA=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link rel="stylesheet" href="https://fonts.maateen.me/solaiman-lipi/font.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.js"></script>

    <title>View Notice</title>

</head>
<body>


<%
    SimpleDateFormat format_dateOfExam = new SimpleDateFormat("dd MMMM yyyy");
    String examType = "";

    String jobDescriptionId = request.getParameter("jobDescriptionId");
    String seatPlan = request.getParameter("seatPlanId");


    long jobId = Long.parseLong(jobDescriptionId);
    long seatPlanId = Long.parseLong(seatPlan);

    Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = Recruitment_job_descriptionRepository.getInstance().getRecruitment_job_descriptionDTOByID(jobId);
    Recruitment_seat_planDTO recruitment_seat_planDTO = Recruitment_seat_planRepository.getInstance().getRecruitment_seat_planDTOByiD(seatPlanId);

    if (recruitment_job_descriptionDTO != null) {
        List<RecruitmentJobSpecificExamTypeDTO> recruitmentJobSpecificExamTypeDTOS = JobSpecificExamTypeRepository.getInstance()
                .getRecruitmentJobSpecificExamTypeDTOByjob_id(recruitment_job_descriptionDTO.iD);
        RecruitmentJobSpecificExamTypeDTO jobSpecificExamTypeDTO = recruitmentJobSpecificExamTypeDTOS.stream()
                .filter(e -> e.order == recruitment_seat_planDTO.recruitmentJobSpecificExamTypeId && e.isSelected)
                .findFirst()
                .orElse(null);
        if(jobSpecificExamTypeDTO != null){
            examType = CatRepository.getName(Language, "job_exam_type", jobSpecificExamTypeDTO.jobExamTypeCat);
        }
        else{
            examType = "";
        }
    }

    //no need recruitment test name id. Since every recruitment job description id is unique
    List<RecruitmentSeatPlanChildDTO> recruitmentSeatPlanChildDTOS = RecruitmentSeatPlanChildRepository.getInstance()
            .getRecruitmentSeatPlanChildDTOByrecruitmentJobDescriptionId(jobId);

//    recruitmentSeatPlanChildDTOS = recruitmentSeatPlanChildDTOS.stream()
//            .filter(e -> e.recruitmentJobSpecificExamTypeId == recruitment_seat_planDTO.recruitmentJobSpecificExamTypeId)
//            .collect(Collectors.toList());

    recruitmentSeatPlanChildDTOS = recruitmentSeatPlanChildDTOS.stream()
            .filter(e -> e.recruitmentSeatPlanId == recruitment_seat_planDTO.iD)
            .collect(Collectors.toList());

    String postName = UtilCharacter.getDataByLanguage(Language, recruitment_job_descriptionDTO.jobTitleBn, recruitment_job_descriptionDTO.jobTitleEn);
    String noticeHeaderBN = "বাংলাদেশ জাতীয় সংসদ সচিবালয়ের " + postName + " পদে লিখিত পরীক্ষায় উর্ত্তীর্ণ প্রার্থীদের নিম্নবর্ণিত সময়সূচী অনুযায়ী <b>"+examType+"</b> পরীক্ষায় উপস্থিত থাকার জন্য অনুরোধ করা হলোঃ ";


%>

<style>

    /* ==================== General Style =========== */

    * {
        margin: 0;
        padding: 0;
        outline: none;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        text-decoration: none;
        list-style: none;
    }

    /* ================== Body =================== */
    body {
    <%--background: url(<%=context_folder%>/images/pin_retrieve/BG.png) no-repeat;--%> background: rgb(0, 159, 201);
        background: radial-gradient(circle, rgba(0, 159, 201, 1) 6%, rgba(0, 54, 115, 1) 60%, rgba(0, 27, 93, 1) 91%);
        max-width: 100vw;
        height: 100vh;
        background-position: center center;
        font-family: 'SolaimanLipi', sans-serif;
        -webkit-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        transition: all 0.3s ease;
        background-size: cover;
        background-attachment: fixed;
    }

    /* ======================== Top Logo area ================== */
    .leftlogo img {
        width: 159px;
        height: 113px;
        margin-top: 30px;
        margin-left: 30px;
    }

    .rightlogo img {
        width: 175px;
        height: 83px;
        margin-top: 60px;
        float: right;
        margin-right: 60px;
    }


    .formarea {
        display: block;
        float: left;
        width: 90%;
        margin: 0 5%;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        height: auto;
        background-color: rgba(255, 255, 255, 0.15);
        border-radius: 20px;
        color: #fff;
        /*padding: 26px 47px 37px 47px;*/
    }

    .formarea h2 {
        margin-bottom: 24px;
    }

    .rightallbutton {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-box-pack: end;
        -ms-flex-pack: end;
        justify-content: flex-end;
        color: #fff;
    }

    .rightallbutton button {
        margin: 0px 0px 0px 10px;
    }

    .rightallbutton .btnleft {
        color: #000;
        background-color: #FFE600;
        border: none;
        border-radius: 8px;
    }

    .rightallbutton .btnright {
        color: #fff;
        background-color: #00D4FF;
        border: none;
        border-radius: 8px;
    }

    .maintitle h2 {
        font-size: 2.25rem;
        margin-bottom: 25px;
    }


    input[type='text']::-webkit-input-placeholder {
        color: #B1CADB;
        font-size: 14px;
    }

    input[type='text']::-moz-placeholder {
        color: #B1CADB;
        font-size: 14px;
    }

    input[type='text']:-ms-input-placeholder {
        color: #B1CADB;
        font-size: 14px;
    }

    input[type='text']::-ms-input-placeholder {
        color: #B1CADB;
        font-size: 14px;
    }

    input[type='text']::placeholder {
        color: #B1CADB;
        font-size: 14px;
    }

    input[type='password']::-webkit-input-placeholder {
        color: #B1CADB;
        font-size: 14px;
    }

    input[type='password']::-moz-placeholder {
        color: #B1CADB;
        font-size: 14px;
    }

    input[type='password']:-ms-input-placeholder {
        color: #B1CADB;
        font-size: 14px;
    }

    input[type='password']::-ms-input-placeholder {
        color: #B1CADB;
        font-size: 14px;
    }

    input[type='password']::placeholder {
        color: #B1CADB;
        font-size: 14px;
    }


    .leftnews img {
        width: 256px;
        height: 154px;
        padding-bottom: 10px;
    }

    img.img-fluid.circularimage {
        margin-bottom: 15px;
    }

    img.img-fluid.admincardimage {
        margin-bottom: 15px;
    }

    .rightnews img {
        width: 256px;
        height: 154px;
        padding-bottom: 10px;
    }

    .maintitle h2 {
        font-size: 1.5rem;
        margin-bottom: 0px;
        padding: 12px 0px;
    }

    .leftlogo img {
        width: 130px;
        height: 90px;
        margin-top: 20px;
        margin-left: 30px;
    }

    .rightlogo img {
        width: 120px;
        height: 57px;
        margin-top: 50px;
        float: right;
        margin-right: 60px;
    }

    .mainlogo.text-center img {
        width: 90px;
        height: 90px;
        line-height: 90px;
    }

    .leftnews {
        padding-top: 140px;
    }

    .rightnews {
        padding-top: 140px;
    }

    h2.title {
        font-size: 25px;
    }

    .formarea h2 {
        margin-bottom: 10px;
    }

    /*.formarea {*/
    /*    padding: 24px 47px 25px 47px;*/
    /*}*/

    .leftnews img {
        width: 220px;
        height: 132px;
    }

    .rightnews img {
        width: 220px;
        height: 132px;
    }

    img.img-fluid.circularimage {
        margin-bottom: 20px;
    }

    img.img-fluid.admincardimage {
        margin-bottom: 20px;
    }

    .songshodimage img {
        width: 100%;
        height: 100%;
        margin-top: 30px;
    }

    .btn-border-radius {
        border-radius: 6px !important;
    }

    /* ======================== Responsive Area ================== */

    @media screen and (max-width: 1537px) {
        .songshodimage img {
            width: 78%;
            height: 100%;
            margin-top: 15px;
        }

        .leftnews {
            padding-top: 30%;
        }

        .rightnews {
            padding-top: 30%;
        }

    }

    @media screen and (max-width: 1480px) {

        .songshodimage img {
            width: 78%;
            height: 100%;
            margin-top: 15px;
        }

        .leftnews {
            padding-top: 140px;
        }

        .rightnews {
            padding-top: 140px;
        }


        .maintitle h2 {
            font-size: 1.25rem;
            margin-bottom: 0px;
            padding: 12px 0px;
        }

        /*.formarea {*/
        /*    padding: 24px 47px 26px 47px;*/
        /*}*/
    }


    @media screen and (max-width: 1366px) {

        .songshodimage img {
            width: 70%;
            height: 100%;
            margin-top: 0px;
        }

        .leftnews {
            padding-top: 140px;
        }

        .rightnews {
            padding-top: 140px;
        }


        .maintitle h2 {
            font-size: 1.25rem;
            margin-bottom: 0px;
            padding: 12px 0px;
        }

        /*.formarea {*/
        /*    padding: 24px 47px 26px 47px;*/
        /*}*/
        .leftlogo img {

            margin-top: 10px;

        }

        .rightlogo img {
            margin-top: 40px;

        }

    }


    @media screen and (max-width: 1100px) {
        .maintitle h2 {
            font-size: 1.15rem;
            margin-bottom: 0px;
            padding: 12px 0px;
        }

        /*.formarea {*/
        /*    padding: 24px 47px 26px 47px;*/
        /*}*/
        .rightallbutton .btnleft {
            font-size: 12px;
        }

        .rightallbutton .btnright {
            font-size: 12px;
        }

        /*.formarea {*/
        /*    padding: 24px 20px 26px 20px;*/
        /*}*/
        .formarea {
            display: block;
            float: left;
            width: 100%;
            margin: 0;
        }

        img.img-fluid.circularimage {
            margin-bottom: 10px;
        }

        img.img-fluid.admincardimage {
            margin-bottom: 10px;
        }

        .songshodimage img {
            width: 100%;
        }

    }

    @media screen and (max-width: 980px) {
        .maintitle h2 {
            font-size: 1rem;

        }

        .leftnews {
            padding-top: 135px;
        }

        .rightnews {
            padding-top: 135px;
        }

        img.img-fluid.circularimage {
            margin-bottom: 11px;
        }

        img.img-fluid.admincardimage {
            margin-bottom: 11px;
        }

    }

    @media screen and (max-width: 790px) {
        .maintitle h2 {
            font-size: 0.90rem;

        }

        .leftnews {
            padding-top: 135px;
        }

        .rightnews {
            padding-top: 135px;
        }

        img.img-fluid.circularimage {
            margin-bottom: 11px;
        }

        img.img-fluid.admincardimage {
            margin-bottom: 11px;
        }

    }

    @media screen and (max-width: 767px) {

        .leftnews {
            padding-top: 30px;
            padding-bottom: 20px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            width: 100%;
        }

        .rightnews {
            padding-top: 20px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            width: 100%;
        }

        .leftlogo img {
            margin-left: 6px;
        }

        .rightlogo img {
            margin-right: 8px;
        }

        .mainlogo.text-center img {
            width: 25%;
            height: auto;
        }

        img.img-fluid.circularimage {
            margin-bottom: 0px;
        }

        .leftnews img {
            padding-bottom: 0px;
        }

        .songshodimage img {
            margin-top: 0px;
        }

    }

    @media screen and (max-width: 464px) {

        .leftnews {
            padding-top: 30px;
            padding-bottom: 20px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            width: 100%;
        }

        .rightnews {
            padding-top: 20px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            width: 100%;
        }

        .leftnews img {
            width: 195px;
            height: 132px;
        }

        .rightnews img {
            width: 195px;
            height: 132px;
        }

        .leftlogo img {
            margin-left: 6px;

        }

        .rightlogo img {
            margin-right: 8px;
        }

        .mainlogo.text-center img {
            width: 25%;
            height: auto;
        }

        img.img-fluid.circularimage {
            margin-bottom: 0px;
        }

        .leftnews img {
            padding-bottom: 0px;
        }

        .songshodimage img {
            margin-top: 0px;
        }

    }


    @media screen and (max-width: 420px) {

        .leftnews {
            padding-top: 10px;
            padding-bottom: 10px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            width: 100%;
        }

        .rightnews {
            padding-top: 10px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            width: 100%;
        }

        .leftnews img {
            width: 170px;
            height: 132px;
        }

        .rightnews img {
            width: 170px;
            height: 132px;
        }

        .leftlogo img {
            margin-left: 6px;

        }

        .rightlogo img {
            margin-right: 8px;
        }

        .mainlogo.text-center img {
            width: 25%;
            height: auto;
        }

        img.img-fluid.circularimage {
            margin-bottom: 0px;
        }

        .leftnews img {
            padding-bottom: 0px;
        }

        .songshodimage img {
            margin-top: 0px;
        }

        img.img-fluid.admincardimage {
            margin-bottom: 0px;
        }

    }

    @media screen and (max-width: 370px) {

        .leftnews {
            padding-top: 10px;
            padding-bottom: 10px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            width: 100%;
        }

        .rightnews {
            padding-top: 10px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            width: 100%;
        }

        .leftnews img {
            width: 105px;
            height: 70px;
        }

        .rightnews img {
            width: 105px;
            height: 70px;
        }

        .leftlogo img {
            margin-left: 6px;

        }

        .rightlogo img {
            margin-right: 8px;
        }

        .mainlogo.text-center img {
            width: 25%;
            height: auto;
        }

        img.img-fluid.circularimage {
            margin-bottom: 0px;
        }

        .leftnews img {
            padding-bottom: 0px;
        }

        .songshodimage img {
            margin-top: 0px;
        }

        img.img-fluid.admincardimage {
            margin-bottom: 0px;
        }

    }

</style>

<style>

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .4in;
        background: white;
        margin-bottom: 10px;
    }

</style>

<div class="kt-content p-0" id="kt_content">
    <div class="">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title prp-page-title">

                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body" id="bill-div">



                <div style="margin: auto;">
                    <div class="container d-flex justify-content-center " id="to-print-div">
                        <%
                            int rowsPerPage = 4;
                            int index = 0;

                            while (index < recruitmentSeatPlanChildDTOS.size()) {
                                boolean isFirstPage = (index == 0);

                        %>
                        <section class="page shadow" data-size="A4">

                            <%if (isFirstPage) {%>
                            <div class="col-12 text-right py-1" data-html2canvas-ignore="true">
                                <button type="button" class="btn" id='download-pdf'
                                        onclick="downloadTemplateAsPdf('to-print-div', 'Practical Exam Notice');">
                                    <i class="fa fa-file-download fa-2x" style="color: gray" aria-hidden="true"></i>
                                </button>
                            </div>
                            <div class="text-center">
                                <h5 class="mt-2 font-weight-bold">
                                    <%=UtilCharacter.getDataByLanguage(Language, "বাংলাদেশ জাতীয় সংসদ সচিবালয়", "Bangladesh National Parliament Secretariat")%>
                                </h5>
                                <h5 class="text-dark">
                                    <%=UtilCharacter.getDataByLanguage(Language, "মানব সম্পদ উইং", "Human Resources Wing")%>
                                </h5>
                                <h5 class="text-dark">
                                    <%=UtilCharacter.getDataByLanguage(Language, "মানব সম্পদ শাখা-৩", "Human Resources Wing")%>
                                </h5>
                                <a class="text-dark" href="https://www.Parliament.gov.bd" target="_blank">
                                    <u><%=UtilCharacter.getDataByLanguage(Language, "www.Parliament.gov.bd", "www.Parliament.gov.bd")%>
                                    </u>
                                </a>
                            </div>

                            <div class=" mt-5 mx-2" style="text-indent: 3rem;">

                                <%=UtilCharacter.getDataByLanguage(Language, noticeHeaderBN, "")%>

                            </div>


                            <%}%>
                            <div class="row mx-2 mt-1">
                                <table
                                        class="mt-3 w-100 rounded"
                                        border="1px solid #323233;"
                                        style="border-collapse: collapse"
                                >
                                    <thead>
                                    <tr>

                                        <th class="text-center"
                                            style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "পদের নাম", "Position Name")%>
                                        </th>
                                        <th class="text-center"
                                            style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "প্রার্থীদের রোল নম্বর ও সংখ্যা", "Roll Number and Number of Applicant")%>
                                        </th>
                                        <th class="text-center"
                                            style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "পরীক্ষার ভেন্যু", "Exam Venue")%>
                                        </th>
                                        <th class="text-center"
                                            style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "তারিখ ও সময়", "Date and Time")%>
                                        </th>


                                    </tr>
                                    </thead>

                                    <%
                                        int rowsInThisPage = 0;
                                        String rows = "";
                                        while (index < recruitmentSeatPlanChildDTOS.size() && rowsInThisPage < rowsPerPage) {

                                            rowsInThisPage++;


                                            RecruitmentSeatPlanChildDTO recruitmentSeatPlanChildDTO = recruitmentSeatPlanChildDTOS.get(index++);

                                            String formatted_dateOfExam = format_dateOfExam.format(new Date(Long.parseLong(recruitmentSeatPlanChildDTO.examDate + "")));
                                            String formattedDate = Admit_cardDAO.getDateBanglaFromEnglish(formatted_dateOfExam);
                                            String finalDate = UtilCharacter.newDateFormat(UtilCharacter.convertNumberEnToBn(formattedDate));
                                            String weekDay = TimeFormat.weekDayInBangla[new Date(recruitmentSeatPlanChildDTO.examDate).getDay()];

                                            String buildingRoomFloor = recruitmentSeatPlanChildDTO.building + ", " + recruitmentSeatPlanChildDTO.floor + ", " + recruitmentSeatPlanChildDTO.roomNo;

                                            String examInfo = finalDate + ", " + weekDay +", "+recruitmentSeatPlanChildDTO.examTime+ ", " + buildingRoomFloor;

                                            Recruitment_exam_venueDTO recruitment_exam_venueDTO = Recruitment_exam_venueRepository.getInstance()
                                                    .getRecruitment_exam_venueDTOByiD(recruitmentSeatPlanChildDTO.recruitmentExamVenueId);

                                            List<Job_applicant_applicationDTO> job_applicant_applicationDTOS = Job_applicant_applicationRepository.getInstance()
                                                    .getJob_applicant_applicationDTOByJobIdAndLevel(recruitmentSeatPlanChildDTO.recruitmentJobDescriptionId,
                                                            recruitmentSeatPlanChildDTO.recruitmentJobSpecificExamTypeId).stream().
                                                            filter(f -> f.seatPlanChildId == recruitmentSeatPlanChildDTO.iD).collect(Collectors.toList());

                                            String selectedRolls = "";
                                            String totalSelected = "";
                                            String totalStr = "";
                                            if(!job_applicant_applicationDTOS.isEmpty()){
                                                selectedRolls = job_applicant_applicationDTOS.stream().map(e -> e.rollNumber).collect(Collectors.joining(","));
                                                totalSelected = String.valueOf(job_applicant_applicationDTOS.size());
                                                totalStr = selectedRolls + " = " +totalSelected + " জন ";
                                            }


                                    %>
                                    <%if(!job_applicant_applicationDTOS.isEmpty()){%>
                                        <tr>
                                            <td class="p-2"><%=recruitment_job_descriptionDTO.jobTitleBn%></td>
                                            <td class="p-2"><%=StringUtils.convertToBanNumber(totalStr)%>
                                            <td class="p-2"><%=UtilCharacter.getDataByLanguage(Language, recruitment_exam_venueDTO.nameBn, recruitment_exam_venueDTO.nameEn)%>
                                            </td>
                                            <td class="p-2"><%=examInfo%>
                                            </td>

                                        </tr>
                                    <%}%>


                                    <%
                                        } %>
                                </table>
                            </div>

                        </section>
                        <% }
                        %>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>

<script src="<%=context_folder%>/assets/backup/js/jquery-3.5.1.slim.min.js"></script>
<script src="<%=context_folder%>/assets/backup/js/bootstrap.min.js"></script>
<script src="<%=context_folder%>/assets/backup/js/popper.min.js"></script>
<script src="<%=context_folder%>/assets/backup/scripts/login/main.js"></script>
<script src="<%=context_folder%>/assets/backup/scripts/util1.js"></script>
<script src="<%=context_folder%>/assets/backup/scripts/pb.js"></script>
<script src="<%=context_folder%>/assets/backup/global/plugins/jquery.min.js"></script>
<script src="<%=context_folder%>/assets/backup/global/plugins/jquery-validation/js/jquery.validate.js"></script>
<script src="<%=context_folder%>/assets/backup/global/plugins/jquery-ui/jquery-ui.min.js"></script>

<script>

    function downloadTemplateAsPdf(divId, fileName) {
        let content = document.getElementById(divId);
        const opt = {
            margin: 0.5,
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4'}
        };
        html2pdf().from(content).set(opt).save();
    }
</script>
</body>
</html>