<%@ page import="job_applicant_application.Job_applicant_applicationDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="job_applicant_application.Job_applicant_applicationRepository" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="admit_card.Admit_cardDTO" %>
<%@ page import="admit_card.Admit_cardDAO" %>
<%@ page import="pb.Utils" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="recruitment_job_specific_exam_type.RecruitmentJobSpecificExamTypeDTO" %>
<%@ page import="recruitment_job_specific_exam_type.JobSpecificExamTypeRepository" %>
<%@ page import="recruitment_seat_plan.RecruitmentSeatPlanChildDTO" %>
<%@ page import="recruitment_seat_plan.RecruitmentSeatPlanChildRepository" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameDTO" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameRepository" %>
<%@ page import="java.util.Map" %>
<%@ page import="recruitment_seat_plan.Recruitment_seat_planDTO" %>
<%@ page import="recruitment_seat_plan.Recruitment_seat_planRepository" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="java.util.Comparator" %><%--
  Created by IntelliJ IDEA.
  User: Nafees
  Date: 3/8/2022
  Time: 2:50 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    String Language = "Bangla";
    List<Recruitment_test_nameDTO> recruitment_test_nameDTOS = Recruitment_test_nameRepository.getInstance().
            getRecruitment_test_nameList().stream().
            sorted(Comparator.comparing(c -> c.iD)).
            filter(e ->  e.recruitmentTestNamePublishCat == 2 ).
            collect(Collectors.toList());


%>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>View Exam Notice</title>
    <style>
        @import url('https://fonts.maateen.me/solaiman-lipi/font.css');

        body {
            /*background: #eee;*/
            background: linear-gradient(90deg, rgba(16, 66, 106, 1) 1%, rgba(7, 106, 154, 1) 32%, rgba(10, 67, 113, 1) 67%);
            padding: 3rem;
            font-family: 'SolaimanLipi', sans-serif;
            font-weight: 400;
            color: #dadada;
        }
    </style>
</head>
<body>
<%
    if (recruitment_test_nameDTOS != null && recruitment_test_nameDTOS.size() > 0) {
        for (Recruitment_test_nameDTO recruitment_test_nameDTO : recruitment_test_nameDTOS) {  %>


    <%
            List<Recruitment_job_descriptionDTO> recruitmentJobDescriptionDTOS = Recruitment_job_descriptionRepository.getInstance()
                    .getRecruitment_job_descriptionListByRecTestNameId(recruitment_test_nameDTO.iD);

            if (recruitmentJobDescriptionDTOS != null && recruitmentJobDescriptionDTOS.size() > 0) {
    %>

                <h1 style="display: flex;justify-content: center"><%=UtilCharacter.getDataByLanguage(Language, recruitment_test_nameDTO.nameBn,
                        recruitment_test_nameDTO.nameEn)%></h1>
                <h3 style="display: flex;justify-content: center"><%=UtilCharacter.getDataByLanguage(Language, "(পরীক্ষা সংক্রান্ত নোটিশ)",
                        "(Exam related notice)")%></h3>
                <div class="table-responsive mt-5 mb-5" >
                    <table id="tableData" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th><%=UtilCharacter.getDataByLanguage(Language, "ক্রমিক নং", "Serial No.")%>
                            </th>
                            <th><%=UtilCharacter.getDataByLanguage(Language, "পদের নাম", "Position name")%>
                            </th>
                            <th><%=UtilCharacter.getDataByLanguage(Language, "লিখিত পরীক্ষা সংক্রান্ত", "Concerning the written test")%>
                            </th>
                            <th><%=UtilCharacter.getDataByLanguage(Language, "ব্যবহারিক পরীক্ষা সংক্রান্ত", "Concerning the practical test")%>
                            </th>
                            <th><%=UtilCharacter.getDataByLanguage(Language, "মৌখিক পরীক্ষা সংক্রান্ত", "Concerning the viva test")%>
                            </th>
                            <th><%=UtilCharacter.getDataByLanguage(Language, "স্বাস্থ্য পরীক্ষা সংক্রান্ত", "Concerning the health test")%>
                            </th>
                        </tr>
                        </thead>

        <%
            int i = 1;
            for(Recruitment_job_descriptionDTO recruitment_job_descriptionDTO : recruitmentJobDescriptionDTOS){


                String postName = "";
                String writtenExamDetails="X";
                String practicalExamDetails="X";
                String vivaExamDetails="X";
                String healthExamDetails="X";

                if (recruitment_job_descriptionDTO != null) {
                    postName = recruitment_job_descriptionDTO.jobTitleBn;

                    //no need recruitment test name id. Since every recruitment job description id is unique
                    List<RecruitmentJobSpecificExamTypeDTO> recruitmentJobSpecificExamTypeDTOS = JobSpecificExamTypeRepository.getInstance()
                            .getRecruitmentJobSpecificExamTypeDTOByjob_id(recruitment_job_descriptionDTO.iD);

                    List<Recruitment_seat_planDTO> recruitment_seat_planDTOS = Recruitment_seat_planRepository.getInstance()
                            .getRecruitmentSeatPlanDTOByRecruitmentJobDescriptionId(recruitment_job_descriptionDTO.iD);


                    for(RecruitmentJobSpecificExamTypeDTO jobSpecificExamTypeDTO : recruitmentJobSpecificExamTypeDTOS){
                        String jobExamType = CatRepository.getName(Language, "job_exam_type", jobSpecificExamTypeDTO.jobExamTypeCat);

                        if(jobSpecificExamTypeDTO.isSelected){

                            if(recruitment_seat_planDTOS != null){
                                Recruitment_seat_planDTO recruitment_seat_planDTO = recruitment_seat_planDTOS.stream()
                                        .filter(e -> e.recruitmentJobSpecificExamTypeId == jobSpecificExamTypeDTO.order).findFirst().orElse(null);

                                    if(jobExamType.equals("লিখিত")){

                                        if(recruitment_seat_planDTO != null){
                                            String writtenExamPath = request.getContextPath()+"/PublicServlet?actionType=viewWrittenExamNotice&jobDescriptionId="+recruitment_job_descriptionDTO.iD+"&seatPlanId="+recruitment_seat_planDTO.iD;
                                            writtenExamDetails ="<a class=\"text-light\" href="+writtenExamPath+" target=\"_blank\" ><u>বিস্তারিত</u></a>";
                                        }
                                        else{
                                            writtenExamDetails="এখনো প্রকাশ করা হয়নি";
                                        }


                                    }

                                    else if(jobExamType.equals("ব্যবহারিক")){
                                        if(recruitment_seat_planDTO != null){
                                            String practicalExamPath = request.getContextPath()+"/PublicServlet?actionType=viewPracticalExamNotice&jobDescriptionId="+recruitment_job_descriptionDTO.iD+"&seatPlanId="+recruitment_seat_planDTO.iD;
                                            practicalExamDetails ="<a class=\"text-light\" href="+practicalExamPath+" target=\"_blank\" ><u>বিস্তারিত</u></a>";
                                        }
                                        else{
                                            practicalExamDetails="এখনো প্রকাশ করা হয়নি";
                                        }

                                    }

                                    else if(jobExamType.equals("মৌখিক")){

                                        if(recruitment_seat_planDTO != null){
                                            String vivaExamPath = request.getContextPath()+"/PublicServlet?actionType=viewPracticalExamNotice&jobDescriptionId="+recruitment_job_descriptionDTO.iD+"&seatPlanId="+recruitment_seat_planDTO.iD;
                                            vivaExamDetails ="<a class=\"text-light\" href="+vivaExamPath+" target=\"_blank\" ><u>বিস্তারিত</u></a>";
                                        }
                                        else{
                                            vivaExamDetails="এখনো প্রকাশ করা হয়নি";
                                        }

                                    }
                                    else if(jobExamType.equals("স্বাস্থ্য")){

                                        if(recruitment_seat_planDTO != null){

                                        }
                                        else{
                                            healthExamDetails="এখনো প্রকাশ করা হয়নি";
                                        }

                                    }

                                }
                            }




                    }%>

                    <tr>
                        <td><%=UtilCharacter.convertDataByLanguage(Language,String.valueOf(i++))%></td>
                        <td><%=postName%></td>
                        <td><%=writtenExamDetails%></td>
                        <td><%=practicalExamDetails%></td>
                        <td><%=vivaExamDetails%></td>
                        <td><%=healthExamDetails%></td>

                    </tr>


                <%}
            }   %>


<%}%>
                    </table>
                </div>
 <%}%>
<%} else {%>
<p style="display: flex;justify-content: center;align-items: center;font-weight: bold;font-size: larger;color: red;">
    কোনো বিজ্ঞপ্তির তথ্য খুঁজে পাওয়া যায়নি</p>
<%}%>

<marquee
        width="100%"
        class=""
        style="position: fixed; bottom: 0;left: 0;"
        direction="left"
        height="auto"
        behavior="scroll"
        onmouseover="this.stop()"
        onmouseout="this.start()"
>
    <ul class="d-flex">
        <li class="mr-5"><a class="text-white" href="#">লিখিত পরীক্ষার নোটিশ</a></li>
        <li class="mr-5"><a class="text-white" href="#">মৌখিক পরীক্ষার নোটিশ</a></li>
        <li class="mr-5"><a class="text-white" href="#">ব্যবহারিক পরীক্ষার নোটিশ</a></li>
        <li class="mr-5"><a class="text-white" href="#">স্বাস্থ্য পরীক্ষার নোটিশ</a></li>
        <li class="mr-5"><a class="text-white" href="#">লিখিত পরীক্ষার নোটিশ</a></li>
    </ul>
</marquee>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script>
    $(document).ready(function () {
        $(document).on("click", ".notification-container", function () {

            let content = this;
            let job = content.querySelector('.job').value;
            let level = content.querySelector('.level').value;
            let approve = content.querySelector('.approve').value;
            let url = 'PublicServlet?actionType=getResultNotice&job=' + job + '&level=' + level + '&approve=' + approve;
            window.open(url, "_blank");
        });
    });
</script>
</body>
</html>
