<%@page import="dbm.*" %>
<%@page import="permission.MenuUtil" %>
<%@page import="permission.MenuRepository" %>
<%@page import="permission.MenuDTO" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.*,sessionmanager.SessionConstants" %>
<%!
    MenuUtil menuUtil = new MenuUtil();
%>
<%
    List<MenuDTO> allMenuList = menuUtil.getAlignMenuListAllSeparated();
%>
<%
    String menuID = (String) session.getAttribute("menuID");
    if (menuID == null) {
        menuID = "";
    }
    System.out.println("menuID=" + menuID);
%>
<div class="form-group row">
    <label for="" class="col-3 col-form-label">Menu</label>
    <div class="col-9">
        <select class="form-control" size="1" name="menuID">
            <option value="" <%if (menuID.equals("")) {%> selected='selected' <%}%>>All</option>
            <%for (MenuDTO menuDTO : allMenuList) {%>
            <option value="<%=menuDTO.menuID%>" <%if (menuID.equals("" + menuDTO.menuID)) {%>
                    selected <%}%>><%=menuDTO.menuName%>
            </option>
            <%}%>
        </select>
    </div>
</div>