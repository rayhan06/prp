<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%
    System.out.println("Inside nav.jsp");
    String url = request.getParameter("url");
    String navigator = request.getParameter("navigator");
    String pageName = request.getParameter("pageName");
    if (pageName == null) pageName = "Search";
    String pageno = "";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String[][] searchFieldInfo = rn.getSearchFieldInfo();
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";

    System.out.println("action  " + action);

%>
<style>
    .kay-group > input {
        margin: 5px;
    }
</style>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=pageName%>
                </h3>
            </div>
        </div>
        <form action="<%=action%>" method="POST" class="form-horizontal">
            <div class="kt-portlet__body form-body">
                <div>
                    <%if (searchFieldInfo != null && searchFieldInfo.length > 0) {%>
                    <%for (int i = 0; i < searchFieldInfo.length; i++) {%>
                    <%if (i % 2 == 0) {%>
                    <div class="row">
                        <%}%>
                        <div class="col-md-6">
                            <%if (searchFieldInfo[i][0].endsWith(".jsp")) {%>
                            <jsp:include page="<%=searchFieldInfo[i][0]%>" flush="true"/>
                            <%} else {%>
                            <div class="form-group row">
                                <%
                                    String fieldName = searchFieldInfo[i][0];
                                    if (StringUtils.isNumeric(fieldName))
                                        fieldName = LM.getText(Long.parseLong(searchFieldInfo[i][0]), loginDTO);
                                %>
                                <label for="" class="col-md-3 col-form-label">
                                    <%=fieldName%>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="" placeholder=""
                                           name="<%=searchFieldInfo[i][1]%>"
                                           <%String value = (String)session.getAttribute(searchFieldInfo[i][1]);
                                           //session.removeAttribute(searchFieldInfo[i][1]);
                                            if( value != null){%>value="<%=value%>"<%}%>>
                                </div>
                            </div>
                            <%}%>
                        </div>
                        <%if (i % 2 == 1) {%>
                    </div>
                    <%}%>

                    <%
                            }
                        }
                    %>
                    <%if (searchFieldInfo != null && searchFieldInfo.length % 2 == 0) {%>
                    <div class="row">
                        <%}%>
                    </div>
                    <div class=clearfix></div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group row pr-0">
                        <label for=""
                               class="col-md-3 col-form-label"><%=LM.getText(LC.GLOBAL_RECORD_PER_PAGE, loginDTO)%>
                        </label>
                        <div class="col-md-9 pr-0">
                            <input type="text" class="form-control" name="<%=SessionConstants.RECORDS_PER_PAGE %>"
                                   placeholder="" value="<%=rn.getPageSize()%>">
                        </div>
                    </div>
                    <div class="col-md-12 form-group text-right">
                        <input type="hidden" name="search" value="yes"/>
                        <!-- 				          	<input type="reset" class="btn  btn-sm btn btn-circle  grey-mint btn-outline sbold uppercase" value="Reset" > -->
                        <input type="submit" class="btn  btn-primary shadow btn-border-radius"
                               value="<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>">
                    </div>
                </div>
            </div>
        </form>
        <div class="kt-portlet__body form-body">
            <form action="<%=action%>" method="POST" class="form-inline ml-auto">
                <nav aria-label="Page navigation">
                    <ul class="pagination d-flex align-items-center justify-content-end">
                        <li class="page-item">
                            <a class="page-link" style="border-radius: 6px 0px 0px 6px"
                               href="<%=link%><%=concat%>id=first" aria-label="First"
                               title="Left">
                                <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                                <span class="sr-only"><%=LM.getText(LC.NAVIGATION_FIRST, loginDTO)%></span>
                            </a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="<%=link%><%=concat%>id=previous"
                               aria-label="Previous"
                               title="Previous">
                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                <span class="sr-only"><%=LM.getText(LC.NAVIGATION_PREVIOUS, loginDTO)%></span>
                            </a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="<%=link%><%=concat%>id=next" aria-label="Next"
                               title="Next">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <span class="sr-only"><%=LM.getText(LC.NAVIGATION_NEXT, loginDTO)%></span>
                            </a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" style="border-radius: 0px 6px 6px 0px"
                               href="<%=link%><%=concat%>id=last"
                               aria-label="Last"
                               title="Last">
                                <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                <span class="sr-only"><%=LM.getText(LC.NAVIGATION_LAST, loginDTO)%></span>
                            </a>
                        </li>
                        <li class="d-flex align-items-center justify-content-end w-50">
                            &nbsp;&nbsp;<span class="hidden-xs mx-3">
                                <%=LM.getText(LC.GLOBAL_PAGE, loginDTO) %>
                            </span>
                            <input
                                    type="text"
                                    class="form-control mx-3 text-center w-25"
                                    name="pageno"
                                    value='<%=pageno%>'
                            <%--                                    size="15"--%>
                                    style="height: 35px; border-radius: 6px"
                            />
                            <span class="hidden-xs mx-2">
                                <%=LM.getText(LC.GLOBAL_OF, loginDTO) %>
                            </span>
                            <%=rn.getTotalPages()%>
                            <input type="hidden" name="go" value="yes"/>
                            <input type="hidden" name="mode" value="search"/>
                            <button
                                    type="submit"
                                    class="btn ml-3 mt-1 pr-0"
                                    value="<%=LM.getText(LC.GLOBAL_GO, loginDTO)%>"
                            >
                                <i class="fa fa-arrow-alt-circle-right fa-2x bg-light text-success mb-2"
                                   style="cursor: pointer"></i>
                            </button>
                        </li>
                    </ul>
                </nav>
            </form>
        </div>
    </div>
</div>

