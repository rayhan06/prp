function buildParamMap() {
    let arr = location.href.split('?');
    if (arr.length > 1) {
        let paramStr = arr[1];
        if (paramStr) {
            return actualBuildParamMap(paramStr);
        }
    }
    return new Map();
}

function actualBuildParamMap(paramStr){
    let paramMap = new Map();
    paramStr = paramStr.split('&');
    paramStr.forEach(param => {
        let item = param.split('=');
        if (item.length > 1) {
            paramMap.set(item[0], item[1]);
        }
    });
    return paramMap;
}

function buildParamStr(anyFieldVal, paramMap) {
    let params = 'actionType=search';
    if (anyFieldVal) {
        params += '&AnyField=' + anyFieldVal;
    }
    paramMap.forEach((value, key) => {
        if (key !== 'actionType' && key !== 'AnyField') {
            params += '&' + key + '=' + value;
        }
    });
    return params;
}

function readyInit(servletName) {
    if (location.href.includes('AnyField=')) {
        let paramMap = buildParamMap();
        if (paramMap.has("AnyField")) {
            anyFieldSelector.val(decodeURIComponent(paramMap.get("AnyField")));
        }
    }

    window.addEventListener('beforeunload', () => {
        let paramMap = buildParamMap();
        let anyFieldVal = anyFieldSelector.val();
        let params;
        if (paramMap.has('AnyField')) {
            if (anyFieldVal !== paramMap.get('AnyField')) {
                params = buildParamStr(anyFieldVal, paramMap);
            }
        } else {
            if (anyFieldVal) {
                params = buildParamStr(anyFieldVal, paramMap);
            }
        }
        if (params) {
            history.pushState(params, '', servletName+'?' + params);
        }
    });
}

function resetPaginationFields(){
    document.querySelectorAll('input[name="RECORDS_PER_PAGE"]').forEach(input => {
        input.value="10";
    })
    document.querySelectorAll('input[name="pageno"]').forEach(input => {
        input.value="1";
    })
}

function setPaginationFields(items){
    switch (items[0]) {
        case "RECORDS_PER_PAGE":
            document.querySelectorAll('input[name="RECORDS_PER_PAGE"]').forEach(input => {
                input.value=items[1];
            })
            break;
        case "pageno":
            document.querySelectorAll('input[name="pageno"]').forEach(input => {
                input.value=items[1];
            })
            break;
    }
}