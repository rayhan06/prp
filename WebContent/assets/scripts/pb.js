function convertToEnglishNumber(str) {
    str = String(str);
    str = str.replaceAll('০', '0');
    str = str.replaceAll('১', '1');
    str = str.replaceAll('২', '2');
    str = str.replaceAll('৩', '3');
    str = str.replaceAll('৪', '4');
    str = str.replaceAll('৫', '5');
    str = str.replaceAll('৬', '6');
    str = str.replaceAll('৭', '7');
    str = str.replaceAll('৮', '8');
    str = str.replaceAll('৯', '9');
    return str;
}

function convertToBanglaNumber(str) {
    str = String(str);
    str = str.replaceAll('0', '০');
    str = str.replaceAll('1', '১');
    str = str.replaceAll('2', '২');
    str = str.replaceAll('3', '৩');
    str = str.replaceAll('4', '৪');
    str = str.replaceAll('5', '৫');
    str = str.replaceAll('6', '৬');
    str = str.replaceAll('7', '৭');
    str = str.replaceAll('8', '৮');
    str = str.replaceAll('9', '৯');
    return str;
}

function setValueToSelect(selectId, val, text) {
    if ($("#" + selectId + " option[value='" + val + "']").length <= 0) {
        $("#" + selectId).append("<option value='" + val + "'></option>");
    }
    $("#" + selectId).val(val);
}

function createTemp(id)
{
	console.log("createTemp called");
	var data = $("#modalDiv_" + id ).find('select, textarea, input').serialize();
	console.log("data = " +  data);
	if($("#designationEng_" + id).val() == "" || $("#designationBng_" + id).val() == "")
	{
		toastr.error("Designation name cannot be empty.");
		return;
	}
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            location.href = 'Office_unit_organogramServlet?actionType=search';


        } else if (this.readyState == 4 && this.status != 200) {
            alert('failed ' + this.status);
        }
    };
    var url = "Office_unit_organogramServlet?actionType=createTemp&ID=" + id + "&"
        + data;
    xhttp.open("POST", url, false);
    xhttp.send();
}

function activateEmployee(id, activate)
{
	console.log("createTemp called");
	var data = $("#modalDiv_" + id ).find('select, textarea, input').serialize();
	console.log("data = " +  data);
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            location.href = 'Employee_managementServlet?actionType=search';


        } else if (this.readyState == 4 && this.status != 200) {
            alert('failed ' + this.status);
        }
    };
    var url = "Employee_recordsServlet?actionType=activate&ID=" + id + "&activate=" + activate + "&" 
        + data;
    xhttp.open("POST", url, false);
    xhttp.send();
}

function destroySearchForm()
{
	$("#tableForm").replaceWith($('<div>' + $("#tableForm").innerHTML + '</div>'));
}


function makeAvailable(id) {
    console.log("makeAvailable " + id);
    var availableStock = $("#availableStock_" + id).val();
	var currentStock = $("#currentStock_" + id).val();
	
	if(parseInt(availableStock) < 0)
	{
		toastr.error("Invalid quantity");
		return;
	}
	
	if(parseInt(availableStock) > parseInt(currentStock))
	{
		toastr.error("Input: " + availableStock + ".Choose a value less than or equal to " + currentStock);
		return;
	}
	$("#makeAvailableButton_" + id).prop("disabled", true);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            location.replace("Drug_informationServlet?actionType=search");
        }

    };
    var url = "Drug_informationServlet?actionType=editAvailability&id=" + id + "&availableStock=" + availableStock;

    xhttp.open("POST", url, true);
    xhttp.send();

}

function editAvailability(id)
{
	$("#tdStock_" + id).css("display", "none");
	$("#tdEdit_" + id).css("padding", "0px");
	$("#tdEdit_" + id).css("border", "0px");
	$("#tdEdit_" + id).css("margin", "0px");
	$("#makeAvailable_" + id).css("display", "none");
	$("#stockStatus_" + id).css("display", "none");
	$("#divStatus_" + id).css("display", "block");
}

var currentRequest = null; 



function getDrugs(suggestedDrugsId, orgId) {

    var suggestion = $("#" + suggestedDrugsId).val();
	var rowId = suggestedDrugsId.split("_")[2];
	$("#drug_ul_" + rowId).html("");
	if(suggestion.length <= 2)
	{
		console.log("Too small suggestion");
		
		return;
	}
	
	var orgToFilter = -1;
	if(orgId !== undefined)
	{
		orgToFilter = orgId;
	}
    
	
    console.log("calling getDrugs with " + suggestion);

    //$("#alternateDrugName_text_" + rowId).val(suggestion);

	
	if(currentRequest != null)
	{
		console.log("aborting prev request");
		currentRequest.abort();
	}
		
    currentRequest = new XMLHttpRequest();
    currentRequest.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log("Got response with " + suggestion + " " + orgToFilter);
			
			//console.log(this.responseText)
            var drugs = JSON.parse(this.responseText);
			if(drugs.length > 0)
			{
				$("#searchSgtnSection_"+ rowId).css("display", "block");
			}
            $("#drug_ul_" + rowId).html("");
            var text = "";
            for (var i = 0; i < drugs.length; i++) {
                // Iterate over numeric indexes from 0 to 5, as everyone expects.
                //console.log(drugs[i].nameEn);
                var drugText = drugs[i].nameEn + " | " + drugs[i].strength + " | " + drugs[i].formName + " | " + drugs[i].pharmaName + " | "+ drugs[i].genericNameEn;
				var len = ((drugs[i].nameEn + " | " + drugs[i].strength + " | " + drugs[i].formName + " | " ).length + 1) * 8;
                text += "<a class='search-item-a' onclick='drugClicked(" + rowId + ", \"" + drugText + "\", " + drugs[i].iD + ", \"" + drugs[i].formName + "\", " + drugs[i].availableStock + "," + len + ", " + drugs[i].showTick + "," + drugs[i].currentStock + ")'>"
                    + "<li class='search-list-item'>";
				if(drugs[i].showTick == false)
				{
					text += "<div class='d-flex align-items-baseline px-4 pt-2 pb-1' style='color:red'>";
					text += "<i class='fa fa-times'></i>";
				}
				else
				{
					text += "<div class='d-flex align-items-baseline px-4 pt-2 pb-1' style='color:green'>";
					text += "<i class='fa fa-check'></i>";
				}
                
                
               text += "<h5 class='text-capitalize font-weight-normal mr-2 ml-3'>"
                    + drugs[i].nameEn + " | " + drugs[i].strength 
                    + "</h5>"
                    + "<small>" + drugs[i].formName + " | " + drugs[i].pharmaName + " | " + drugs[i].genericNameEn + "</small>"
                    + "</div>"
                    + "</li>"
                    + "</a>";


            }
            $("#drug_ul_" + rowId).html(text);

        } else {
            //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
        }
    };

	
    currentRequest.open("GET", "Drug_informationServlet?actionType=getSuggested&suggestion=" + suggestion + "&orgToFilter=" + orgToFilter, true);
    currentRequest.send();

}

function navsubmit(params, servletName, pushState = true) {
    document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
    //alert(params);
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (pushState) {
                history.pushState(params, '', servletName + '?actionType=search&' + params);
            }
            document.getElementById('tableForm').innerHTML = this.responseText;
            setPageNo();
            searchChanged = 0;
        } else if (this.readyState == 4 && this.status != 200) {
            alert('failed ' + this.status);
        }
    };

    xhttp.open("GET", servletName + "?actionType=search&isPermanentTable=true&ajax=true&" + params, true);
    xhttp.send();

}

function showKey(selectId, prefix) {
    console.log("showKey called with id = " + selectId);
    var rowId = selectId.split("_")[2];
    var value = $("#" + selectId).val();
    var hidden = prefix + "_text_" + rowId;
    if (value == 2) {
        $("#" + hidden).attr("type", "text");
    } else {
        $("#" + hidden).attr("type", "hidden");
    }
}


function childAdded(e, childName) {
    e.preventDefault();
    var t = $("#template-" + childName);

    $("#field-" + childName).append(t.html());
    SetCheckBoxValues("field-" + childName);

    var tr = $("#field-" + childName).find("tr:last-child");

    tr.attr("id", childName + "Child_" + child_table_extra_id);

    tr.find("[tag='pb_html']").each(function (index) {
        var prev_id = $(this).attr('id');
        $(this).attr('id', prev_id + child_table_extra_id);
        console.log(index + ": " + $(this).attr('id'));
    });

    processRowsWhileAdding(childName);


    child_table_extra_id++;

}

function childRemoved(e, childName) {
    var tablename = "field-" + childName;
    var i = 0;
    //console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
    var element = document.getElementById(tablename);

    var j = 0;
    for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
        var tr = document.getElementById(tablename).childNodes[i];
        if (tr.nodeType === Node.ELEMENT_NODE) {
            console.log("tr.childNodes.length= " + tr.childNodes.length);
            var checkbox = tr.querySelector('input[deletecb="true"]');
            if (checkbox.checked == true) {
                tr.remove();
            }
            j++;
        }

    }
}

function phoneNumberRemove88ConvertLanguage(phone, language) {
    phone = phone.substring(2, phone.length);
    if (language == "bangla" || language == "Bangla") {
        phone = convertToBanglaNumber(phone);
    }
    return phone;
}

function getJsError(type, language, errorType) {
    if (type == "phone") {
        if (language.toLowerCase() == "english") {
            return "Invalid Phone Number";
        } else {
            return "ফোন নাম্বারের ফর্ম্যাট ভুল";
        }
    }
}

function processPhoneById(phoneId, language)
{
	var convertedPhoneNumber = phoneNumberAdd88ConvertLanguage($(phoneId).val(), language);


    var isValid = false;
    if (convertedPhoneNumber.startsWith("880") && convertedPhoneNumber.length == 13) {
        isValid = true;
    }
    console.log("isValid = " + isValid);

    if ($(phoneId).prop('required') && !isValid) {
        return false;
    } else if ($(phoneId).prop('required')) {
        console.log("ok1");
        $(phoneId).val(convertedPhoneNumber);
        return true;
    }

    if (!$(phoneId).prop('required') && (isValid || convertedPhoneNumber.length == 2)) {
        console.log("ok2");
        $(phoneId).val(convertedPhoneNumber);
        return true;
    } else if (!$(phoneId).prop('required')) {
        return false;
    }
    return false;
	
}

function preprocessPhoneBeforeSubmitting(phoneElement, row, language) {
    var phoneId = "#" + phoneElement + "_phone_" + row;
	return processPhoneById(phoneId, language);
    
}

function phoneNumberAdd88ConvertLanguage(phone, language) {
    phone = "88" + phone;
    if (language == "bangla" || language == "Bangla") {
        phone = convertToEnglishNumber(phone);
    }
    return phone;
}

function convertNumberToLanguage(number, language) {
    if (language == "bangla" || language == "Bangla") {
        number = convertToBanglaNumber(number);
    } else {
        number = convertToEnglishNumber(number);
    }
    return number;
}

function viewHistory() {
    console.log("viewhistory called");
    if ($("#historyDropdown").css("display") == "none") {
        console.log("making block");
        $("#historyDropdown").css("display", "block");
    } else {
        console.log("making none");
        $("#historyDropdown").css("display", "none");
    }
}

var substringMatcher = function (strs) {
    return function findMatches(q, cb) {
        var matches, substringRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function (i, str) {
            if (substrRegex.test(str)) {
                matches.push(str);
            }
        });

        cb(matches);
    };
};


function buttonStateChange(value) {
    if ($('.submit-btn') != undefined) {
        $('.btn-sm').prop('disabled', value);
    }
    if ($('.cancel-btn') != undefined) {
        $('.cancel-btn').prop('disabled', value);
    }
}


function ajaxGet(url, onSuccess, onError) {
    $.ajax({
        type: "GET",
        url: getContextPath() + url,
        dataType: "json",
        success: onSuccess,
        error: onError,
        complete: function () {
            // $.unblockUI();
        }
    });
}


function submitAddForm2() {
    buttonStateChange(true);
    console.log("submitting");
    var form = $("#bigform");
    $.ajax({
        type: "POST",
        url: form.attr('action'),
        data: form.serialize(),
        dataType: 'JSON',
        success: function (response) {
            if (response.responseCode === 0) {
                console.log("Failed");
                showToastSticky(response.msg, response.msg);
                buttonStateChange(false);
            } else if (response.responseCode === 200) {
                showToastSticky("সাবমিট সফল হয়েছে","Submit Successful");
                setTimeout(() => {
                    window.location.replace(getContextPath() + response.msg);
                }, 3000);
            } else {
                console.log("Error: " + response.responseCode);
                buttonStateChange(false);
            }
        }
        ,
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                + ", Message: " + errorThrown);
            buttonStateChange(false);
        }
    });
}

function submitAddForm() {
    buttonStateChange(true);
    console.log("submitting");
    var form = $("#bigform");
    $.ajax({
        type: "POST",
        url: form.attr('action'),
        data: form.serialize(),
        dataType: 'JSON',
        success: function (response) {
            if (response.responseCode === 0) {
                console.log("Failed");
                toastr.error(response.msg);
                buttonStateChange(false);
            } else if (response.responseCode === 200) {
                showToastSticky("সাবমিট সফল হয়েছে","Submit Successful");
                setTimeout(() => {
                    location.href = response.msg;
                    // window.location.replace(getContextPath() + response.msg);
                }, 3000);
                buttonStateChange(false);
            }
        }
    });
}

function submitAddFormWithUrl(url, type, data) {
    $.ajax({
        type: type,
        url: url,
        data: data,
        dataType: 'JSON',
        success: function (response) {
            if (response.responseCode === 0) {
                console.log("Failed");
                toastr.error(response.msg);
            } else if (response.responseCode === 200) {
                console.log("Successfully added");
                location.href = response.msg;
            }
        }
    });
}

function showApprovalPathdetails(i) {
    var jobcat = $("#modal_jobCat_category_" + i).val();
    console.log("getting ap, jobcat = " + jobcat);
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("ap_details").innerHTML = this.responseText;
            if (this.responseText.includes("<table")) {
                document.getElementById("submitButton").style.visibility = "visible";
            }


        } else if (this.readyState == 4 && this.status != 200) {
            alert('failed ' + this.status);
        }
    };
    var url = "Approval_pathServlet?actionType=getAPDetailsPlain&jobCat="
        + jobcat;
    console.log("url = " + url);
    xhttp.open("POST", url, true);
    xhttp.send();
}

function initDeleteAll() {
    $('#deleteAll').click(function (e) {
        if ($('#deleteAll').prop("checked")) {
            // console.log("deleteall checked");
            $("input[name='ID']").prop("checked", true);
        } else {
            // console.log("deleteall not checked");
            $("input[name='ID']").prop("checked", false);
        }
    });

    $("input[name='ID']").click(x => {
        let allSelected = true;
        let set = $('#tableData').find('tbody > tr > td:last-child input[type="checkbox"]');
        $(set).each(function () {
            if (!$(this).prop('checked')) {

                allSelected = false;
                return false;
            }
        });

        $("#deleteAll").prop("checked", allSelected);
    });
}

function initDeleteCheckBoxes() {
    initDeleteAll();
    $('#tableForm').submit(function (e) {
        var currentForm = this;
        var selected = false;
        e.preventDefault();
        var set = $('#tableData').find('tbody > tr > td:last-child input[type="checkbox"]');
        $(set).each(function () {
            if ($(this).prop('checked')) {
                selected = true;
            }
        });
        if (!selected) {
            deleteWarningDialog();
        } else {
            deleteDialog(null, () => {
                currentForm.submit();
            });
        }
    });


    $(document).on("click", '.chkEdit', function () {

        $(this).toggleClass("checked");
    });
}


function processMultipleSelectBoxBeforeSubmit(name) {

    $("[name='" + name + "']").each(function (i) {
        var selectedInputs = $(this).val();
        var temp = "";
        if (selectedInputs != null) {
            selectedInputs.forEach(function (value, index, array) {
                if (value && value != '') {
                    if (index > 0) {
                        temp += ", ";
                    }
                    temp += value;
                }

            });
        }
        $(this).append('<option value="' + temp + '"></option>');
        console.log("temp = " + temp + " id = " + $(this).attr("id"));
        $(this).val(temp);
        var hiddenId = "hidden_" + $(this).attr("id");
        if ($("#" + hiddenId)) {
            $("#" + hiddenId).val(temp);
            console.log("hiddenval = " + $("#" + hiddenId).val());
        }
        console.log("val = " + $(this).val());
    });
}

function downloadPdf(fileName, selectorId) {
    const content = document.getElementById(selectorId);
    var opt = {
        margin: 0.5,
        filename: fileName,
        image: {type: 'jpeg', quality: 2},
        html2canvas: {scale: 2},
        jsPDF: {unit: 'in', format: 'A4', orientation: 'portrait'}
    };
    html2pdf().from(content).set(opt).save();
}

window.onafterprint = function () {
    location.reload();
}

function printAnyDiv(divId) {
    var printContents = document.getElementById(divId).innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;

    window.print();

    document.body.innerHTML = originalContents;


    //$.print("#pritDiv");
    /*w=window.open();
    w.document.write($('#' + divId).html());
    w.print();
    w.close();*/
}

function initCkEditor(id) {
    CKEDITOR.replace(id, {
        toolbar: [
            ['Cut', 'Copy', 'Paste', 'Undo', 'Redo'],			// Defines toolbar group without name.
            {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline']},
            {name: 'morestyles', items: ['Strike', 'Subscript', 'Superscript']},
            {
                name: 'paragraph',
                items: ['NumberedList', 'BulletedList']
            },
            {name: 'justify', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
            {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
            {name: 'colors', items: ['TextColor', 'BGColor']}
        ],
        height: '5em',
        width: 600
    });
}

function birthDateTimeInit(language) {
    $('.edms-datetimepicker').datetimepicker({
        format: 'LT'
    });


    $('.datepicker').datepicker({

        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        yearRange: '-150:+0',
        maxDate: 'now',
        todayHighlight: true,
        showButtonPanel: true,
        onSelect: function (dateText) {
            console.log("Selected date: " + dateText + "; input's current value: " + this.value);
        }
    });

}

function dateTimeInit(language) {
    console.log("initing date time, language = " + language);
    $('.edms-datetimepicker').datetimepicker({
        format: 'LT'
    });


    $('.datepicker').datepicker({

        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        yearRange: '-99:+100',
        todayHighlight: true,
        showButtonPanel: true
    });

}

function basicInit() {
    $('.edms-datetimepicker').datetimepicker({
        format: 'LT'
    });
    $(".banglaDigitOnly").keypress(function (event) {
        var ew = event.which;
        if (ew == 32)
            return true;
        var charStr = String.fromCharCode(ew);

        if (charStr == "০") {
            return true;
        } else if (charStr == "১") {
            return true;
        } else if (charStr == "২") {
            return true;
        } else if (charStr == "৩") {
            return true;
        } else if (charStr == "৪") {
            return true;
        } else if (charStr == "৫") {
            return true;
        } else if (charStr == "৬") {
            return true;
        } else if (charStr == "৭") {
            return true;
        } else if (charStr == "৮") {
            return true;
        } else if (charStr == "৯") {
            return true;
        }
        return false;
    });
    $(".englishDigitOnly").keypress(function (event) {
        var ew = event.which;
        if (ew == 32)
            return true;
        var charStr = String.fromCharCode(ew);

        if (charStr == "0") {
            return true;
        } else if (charStr == "1") {
            return true;
        } else if (charStr == "2") {
            return true;
        } else if (charStr == "3") {
            return true;
        } else if (charStr == "4") {
            return true;
        } else if (charStr == "5") {
            return true;
        } else if (charStr == "6") {
            return true;
        } else if (charStr == "7") {
            return true;
        } else if (charStr == "8") {
            return true;
        } else if (charStr == "9") {
            return true;
        }
        return false;
    });
    $(".englishOnly").keypress(function (event) {
        var ew = event.which;
        if (ew === 32)
            return true;
        if (48 <= ew && ew <= 57)
            return true;
        if (65 <= ew && ew <= 90)
            return true;
        if (97 <= ew && ew <= 122)
            return true;
        return "#,.;:(){}/-&|+-".includes(String.fromCharCode(ew));
    });
    $(".digitOnly").keypress(event => {
        let ew = event.which;
        return 48 <= ew && ew <= 57;
    });
    $(".englishAddressValidation").keypress(function (event) {
        var ew = event.which;
        if (ew == 32)
            return true;
        if (48 <= ew && ew <= 57)
            return true;
        if (65 <= ew && ew <= 90)
            return true;
        if (97 <= ew && ew <= 122)
            return true;
        var charStr = String.fromCharCode(ew);
        var allowedChar = "#,.;:(){}/-&";
        if (allowedChar.includes(charStr)) {
            return true;
        }
        return false;
    });
    $(".noEnglish").keypress(function (event) {
        var ew = event.which;
        if (ew == 32)
            return true;
        if (48 <= ew && ew <= 57)
            return false;
        if (65 <= ew && ew <= 90)
            return false;
        if (97 <= ew && ew <= 122)
            return false;
        return true;
    });
    /*$('.datepicker').datepicker({

        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        todayHighlight:	true,
        showButtonPanel: true
    });*/


}

function unit_selected(selectedElement, participant) {
    console.log("selected " + selectedElement.value + " id = " + selectedElement.getAttribute("id"));
    var id = selectedElement.getAttribute("id");
    var option = $("#" + id).val();
    var text = $("#" + id + " option:selected").text();
    console.log("selected value = " + option + " text = " + text);
    var splitted = id.split("_");
    var rowID = splitted[splitted.length - 1];
    console.log("rowID = " + rowID);


    $("#" + participant + rowID + "> option").each(function () {
        if ($(this).attr("unitname") != text && $(this).attr("value") && $(this).attr("value") != '-1') {
            $(this).remove();
        }

    });

    $("#" + id).prop("disabled", true);
}

function unit_selected_nosplit(selectedElement, participant) {
    console.log("selected " + selectedElement.value + " id = " + selectedElement.getAttribute("id"));
    var id = selectedElement.getAttribute("id");
    var option = $("#" + id).val();
    var text = $("#" + id + " option:selected").text();
    console.log("selected value = " + option + " text = " + text);

    console.log("department = " + dept);

    if (deptChanged == 0) {
        origHtml = $("#" + participant).html();
        deptChanged = 1;
        console.log("deptChanged = 1 now");
    } else {
        console.log("original html");
        $("#" + participant).html(origHtml);
    }


    $("#" + participant + "> option").each(function () {
        if ($(this).attr("unitname") != text && $(this).attr("value") && $(this).attr("value") != '-1' && $(this).attr("value") != '') {
            $(this).remove();
        }

    });

    //$( "#" + id ).prop( "disabled", true );
}

var origHtml;
var deptChanged = 0;

function dept_selected(selectedElement, participant) {
    console.log("selected " + selectedElement.value + " id = " + selectedElement.getAttribute("id"));
    var id = selectedElement.getAttribute("id");
    var option = $("#" + id).val();
    var dept = $("#" + id).val();

    console.log("department = " + dept);

    if ($("#therapyNeeded_cb_0").length) {
        if (dept == 3)//physiotherapy
        {
            $("#therapyNeeded_cb_0").val("true");
        } else {
            $("#therapyNeeded_cb_0").val("false");
        }

        console.log($("#therapyNeeded_cb_0").val());
    }


    if (deptChanged == 0) {
        origHtml = $("#" + participant).html();
        deptChanged = 1;
        console.log("deptChanged = 1 now");
    } else {
        console.log("original html");
        $("#" + participant).html(origHtml);
    }

    $("#" + participant + "> option").each(function () {
        if ($(this).attr("dept") != dept && $(this).attr("value") && $(this).attr("dept") != "") {
            $(this).remove();
        }

    });

    $("#" + participant).prop("disabled", false);
}

function deletefile(id, tdId, hiddenvar) {
    $("#" + tdId).remove();
    var newvalue = $("#" + hiddenvar).attr('value') + "," + id;
    $("#" + hiddenvar).attr('value', newvalue);
    hiddenvar
}

//*** This code is copyright 2002-2016 by Gavin Kistner, !@phrogz.net
//*** It is covered under the license viewable at http://phrogz.net/JS/_ReuseLicense.txt
Date.prototype.customFormat = function (formatString) {
	console.log("date = " + this);
    var YYYY, YY, MMMM, MMM, MM, M, DDDD, DDD, DD, D, hhhh, hhh, hh, h, mm, m, ss, s, ampm, AMPM, dMod, th;
    YY = ((YYYY = this.getFullYear()) + "").slice(-2);
    MM = (M = this.getMonth() + 1) < 10 ? ('0' + M) : M;
    MMM = (MMMM = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][M - 1]).substring(0, 3);
    DD = (D = this.getDate()) < 10 ? ('0' + D) : D;
    DDD = (DDDD = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"][this.getDay()]).substring(0, 3);
    th = (D >= 10 && D <= 20) ? 'th' : ((dMod = D % 10) == 1) ? 'st' : (dMod == 2) ? 'nd' : (dMod == 3) ? 'rd' : 'th';
    formatString = formatString.replace("#YYYY#", YYYY).replace("#YY#", YY).replace("#MMMM#", MMMM).replace("#MMM#", MMM).replace("#MM#", MM).replace("#M#", M).replace("#DDDD#", DDDD).replace("#DDD#", DDD).replace("#DD#", DD).replace("#D#", D).replace("#th#", th);
    h = (hhh = this.getHours());
    if (h == 0) h = 24;
    if (h > 12) h -= 12;
    hh = h < 10 ? ('0' + h) : h;
    hhhh = hhh < 10 ? ('0' + hhh) : hhh;
    AMPM = (ampm = hhh < 12 ? 'am' : 'pm').toUpperCase();
    mm = (m = this.getMinutes()) < 10 ? ('0' + m) : m;
    ss = (s = this.getSeconds()) < 10 ? ('0' + s) : s;
    return formatString.replace("#hhhh#", hhhh).replace("#hhh#", hhh).replace("#hh#", hh).replace("#h#", h).replace("#mm#", mm).replace("#m#", m).replace("#ss#", ss).replace("#s#", s).replace("#ampm#", ampm).replace("#AMPM#", AMPM);
};

function fillDropDown(value, dropdown, qs) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText.includes('option')) {
                //console.log(this.responseText);
                $("#" + dropdown).html(this.responseText);
            } else {
                console.log("got errror");
            }

        } else if (this.readyState == 4 && this.status != 200) {
            alert('failed ' + this.status);
        }
    };
    xhttp.open("POST", qs, true);
    xhttp.send();
}

function fillSelect(dropdown, qs) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText.includes('option')) {
                $("#" + dropdown).html(this.responseText);
            } else {
                console.log("got errror");
            }

        } else if (this.readyState == 4 && this.status != 200) {
            alert('failed ' + this.status);
        }
    };
    xhttp.open("GET", qs, true);
    xhttp.send();
}

function getOfficer(officer_id, officer_select, ServetName) {
    console.log("getting officer");
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText.includes('option')) {
                console.log("got response for officer");
                document.getElementById(officer_select).innerHTML = this.responseText;

                if (document.getElementById(officer_select).length > 1) {
                    document.getElementById(officer_select).removeAttribute(
                        "disabled");
                }
            } else {
                console.log("got errror response for officer");
            }

        } else if (this.readyState == 4 && this.status != 200) {
            alert('failed ' + this.status);
        }
    };
    xhttp.open("POST", ServetName + "?actionType=getGRSOffice&officer_id="
        + officer_id, true);
    xhttp.send();
}

function fillDependentDiv(parentelement, dependentElement, ServetName) {
    console.log("getting Element");
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById(dependentElement).innerHTML = this.responseText;
        } else if (this.readyState == 4 && this.status != 200) {
            alert('failed ' + this.status);
        }
    };
    var value = document.getElementById(parentelement).value;
    console.log("selected value = " + value);

    xhttp.open("POST", ServetName + "?actionType=getDependentDiv&Value="
        + value, true);
    xhttp.send();
}

function getLayer(layernum, layerID, childLayerID, selectedValue, ServetName) {
    console.log("getting layer");
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText.includes('option')) {
                console.log("got response");
                document.getElementById(childLayerID).innerHTML = this.responseText;
            } else {
                console.log("got errror response");
            }

        } else if (this.readyState == 4 && this.status != 200) {
            alert('failed ' + this.status);
        }
    };
    xhttp.open("POST", ServetName + "?actionType=getGRSLayer&layernum="
        + layernum + "&layerID=" + layerID + "&childLayerID="
        + childLayerID + "&selectedValue=" + selectedValue, true);
    xhttp.send();
}

function layerselected(layernum, layerID, childLayerID, hiddenInput,
                       hiddenInputForTopLayer, officerElement, ServetName) {
    var layervalue = document.getElementById(layerID).value;
    console.log("layervalue = " + layervalue);
    document.getElementById(hiddenInput).value = layervalue;
    if (layernum == 0) {
        document.getElementById(hiddenInputForTopLayer).value = layervalue;
    }
    if (layernum == 0
        || (layernum == 1 && document
            .getElementById(hiddenInputForTopLayer).value == 3)) {
        document.getElementById(childLayerID).setAttribute("style",
            "display: inline;");
        getLayer(layernum, layerID, childLayerID, layervalue, ServetName);
    }

    if (officerElement !== null) {
        getOfficer(layervalue, officerElement, ServetName);
    }

}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function addHTML(id, HTML) {
    document.getElementById(id).innerHTML += HTML;
}

function getRequests() {
    var s1 = location.search.substring(1, location.search.length).split('&'), r = {}, s2, i;
    for (i = 0; i < s1.length; i += 1) {
        s2 = s1[i].split('=');
        r[decodeURIComponent(s2[0]).toLowerCase()] = decodeURIComponent(s2[1]);
    }
    return r;
}

function Request(name) {
    return getRequests()[name.toLowerCase()];
}

function ShowExcelParsingResult(suffix) {
    var failureMessage = document.getElementById("failureMessage_" + suffix);
    if (failureMessage == null) {
        console.log("failureMessage_" + suffix + " not found");
    }
    console.log("value = " + failureMessage.value);
    if (failureMessage != null && failureMessage.value != "") {
        alert("Excel uploading result:" + failureMessage.value);
    }
}

function removeElement(elementId) {
    // Removes an element from the document
    var element = document.getElementById(elementId);
    element.parentNode.removeChild(element);
}

function disableApprovalButtons(i) {
    $("#approveButton").css("display", "none");
    $("#rejectButton").css("display", "none");
    $('a[name="sendToApprovalPathButton_' + i + '"]').each(function () {

        $(this).css("display", "none");
    });
}

function forward(id, ServletName) {
    console.log("forwarding");
    var employee = $("#employee").val();
    var remarks_val = CKEDITOR.instances["0_remarks"].getData();
    var file_id_val = $("#approval_attached_fileDropzone_dropzone_0").val();
    console.log("after getting remarks");

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            location.replace(ServletName + "?actionType=getApprovalPage");
        }

    };

    var url = ServletName + "?actionType=forward"
        + "&id=" + id + "&fileID="
        + file_id_val
        + "&employee=" + employee;

    var formData = new FormData();
    formData.append("remarks", remarks_val);
    xhttp.open("POST", url, ajax);
    xhttp.send(formData);

}

function giveBack(id, ServletName) {
    console.log("returning");
    var remarks_val = CKEDITOR.instances["0_remarks"].getData();
    var file_id_val = $("#approval_attached_fileDropzone_dropzone_0").val();

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            location.replace(ServletName + "?actionType=getApprovalPage");
        }

    };

    var url = ServletName + "?actionType=giveBack"
        + "&id=" + id + "&fileID="
        + file_id_val;

    var formData = new FormData();
    formData.append("remarks", remarks_val);
    xhttp.open("POST", url, ajax);
    xhttp.send(formData);

}

function approve(row_id, successMessage, i, ServletName, approveOrReject, redirect, isTextArea) {

    disableApprovalButtons(i);
    console.log("approving");

    $("#approveButton").css("display", "none");
    $("#rejectButton").css("display", "none");
    $('a[name="sendToApprovalPathButton_' + i + '"]').each(function () {
        $(this).css("display", "none");
        console.log("found one");
    });
    console.log("approve 2");

    // removeElement("approve_" + row_id);
    var element = document.getElementById("tdapprove_" + row_id);
    var reject_element = document.getElementById("tdreject_" + row_id);
    var edit = document.getElementById(i + "_Edit");
    var cb_td = document.getElementById(i + "_checkbox");
    var xhttp = new XMLHttpRequest();
    var jobCat = -1;

    console.log("i = " + i);
    console.log("ServletName = " + ServletName);


    var remarks_val;
    if (isTextArea == true) {
        remarks_val = CKEDITOR.instances[i + "_remarks"].getData();
    } else {
        remarks_val = $("#" + i + "_remarks").val();
    }

    console.log("isTextArea = " + isTextArea + " remarks_val = " + remarks_val);


    var file_id_val = document
        .getElementById("approval_attached_fileDropzone_dropzone_" + i).value;
    console.log("remarks = " + remarks_val + " redirect = " + redirect);


    var actionType;
    var rejectTo = -1;
    if (approveOrReject == 1) {
        actionType = "approve";
    } else if (approveOrReject == 0) {
        actionType = "reject";
        rejectTo = document.getElementById('rejectTo').value;
        console.log("rejectTo = " + rejectTo);
    } else if (approveOrReject == 2) {
        actionType = "terminate";
    } else if (approveOrReject == 3) {
        actionType = "SendToApprovalPath";
        var e = document.getElementById("modal_jobCat_category_" + i);
        jobCat = e.options[e.selectedIndex].value;
    }

    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (!redirect) {
                if (approveOrReject == 1) {
                    //element.innerHTML = successMessage;
                    //reject_element.innerHTML = "Not in your office";
                } else if (approveOrReject == 0) {
                    //element.innerHTML = "Not in your office";
                    //reject_element.innerHTML = successMessage;
                }

                edit.innerHTML = "";
                cb_td.innerHTML = "";
            } else {
                location.replace(ServletName + "?actionType=getApprovalPage");
            }

        } else if (this.readyState == 4 && this.status != 200) {
            if (approveOrReject == 1) {
                //element.innerHTML = "Approval Failed";
            } else if (approveOrReject == 0) {
                //reject_element.innerHTML = "Rejection Failed";
            }

        }
    };

    var url = ServletName + "?actionType=" + actionType
        + "&idToApprove=" + row_id + "&fileID="
        + file_id_val + "&jobCat=" + jobCat
        + "&rejectTo=" + rejectTo;

    console.log(url);
    var ajax;
    if (redirect == true) {
        url += "&redirect=" + redirect;
        ajax = false;
        var formData = new FormData();
        formData.append("remarks", remarks_val);
        xhttp.open("POST", url, ajax);
        xhttp.send(formData);

    } else {
        url += "&remarks=" + remarks_val;
        ajax = true;
        xhttp.open("POST", url, ajax);
        xhttp.send();
    }


}

function doEdit(params, i, id, deletedStyle, isPermanentTable, ServletName) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText != '') {
                var onclickFunc = "submitAjax(" + i + ",'" + deletedStyle
                    + "')";
                document.getElementById('tr_' + i).innerHTML = this.responseText;

                document.getElementById('tr_' + i).innerHTML += "<td id = '"
                    + i + "_Submit'></td>";

                if (document.getElementById('isPermanentTable').value !== 'true') {
                    document.getElementById('tr_' + i).innerHTML += "<td></td><td></td><td></td>";
                }
                document.getElementById(i + '_Submit').innerHTML += "<a href='#' onclick=\""
                    + onclickFunc + "\">Submit</a>";
                document.getElementById('tr_' + i).innerHTML += "<td>"
                    + "<div class='checker text-right'>"
                    + "<span class='' id='chkEdit'><input type='checkbox' name='ID' value='"
                    + id + "'/></span>" + "</td>";
                init(i);
            } else {
                document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
            }
        } else if (this.readyState == 4 && this.status != 200) {
            alert('failed ' + this.status);
        }
    };

    xhttp.open("Get", ServletName + "?isPermanentTable=" + isPermanentTable
        + "&actionType=getEditPage" + params, true);
    xhttp.send();
}

function fixedToEditable(i, deletedStyle, id, isPermanentTable, ServletName) {
    console.log('fixedToEditable called, isPermanentTable = '
        + isPermanentTable);
    var params = '&identity=' + id + '&inplaceedit=true' + '&deletedStyle='
        + deletedStyle + '&ID=' + id + '&rownum=' + i + '&dummy=dummy';
    console.log('fixedToEditable i = ' + i + ' id = ' + id);
    doEdit(params, i, id, deletedStyle, isPermanentTable, ServletName);

}

function SetCheckBoxValues(tablename) {
    var i = 0;
    console.log("document.getElementById(tablename).childNodes.length = "
        + document.getElementById(tablename).childNodes.length);
    var element = document.getElementById(tablename);

    var j = 0;
    for (i = 0; i < document.getElementById(tablename).childNodes.length; i++) {
        var tr = document.getElementById(tablename).childNodes[i];
        if (tr.nodeType === Node.ELEMENT_NODE) {
            console.log("tr.childNodes.length= " + tr.childNodes.length);
            var checkbox = tr.querySelector('input[type="checkbox"]');
            if (checkbox.name == "checkbox") {
                checkbox.id = tablename + "_cb_" + j;
                j++;
            }

        }

    }
}

var searchChanged = 0;

function setValues(x, value) {
    for (i = 0; i < x.length; i++) {
        x[i].value = value;
    }
}

function setInnerHTML(x, value) {
    for (i = 0; i < x.length; i++) {
        x[i].innerHTML = value;
    }
}

function setPageNoInAllFields(value) {
    var x = document.getElementsByName("pageno");
    setValues(x, value);
}

function setRPPInAllFields(value) {
    var x = document.getElementsByName("RECORDS_PER_PAGE");
    setValues(x, value);
}

function setTotalPageInAllFields(value) {
    var x = document.getElementsByName("totalpage");
    setInnerHTML(x, value);
}

function setPageNo() {

    initDeleteAll();//This is the only global method where this can be done.
    setPageNoInAllFields(document.getElementById('hidden_pageno').value);
    setTotalPageInAllFields(document.getElementById('hidden_totalpage').value);
	$('input[name="trData"]').val($("#hidden_totalrecords").val());
    console.log("totalpage now is "
        + document.getElementById('hidden_totalpage').value);
    console.log("totalpage now is "
        + document.getElementsByName('totalpage')[0].value);
    // document.getElementById('totalpage').innerHTML=
    // document.getElementById('hidden_totalpage').value;
}

function setSearchChanged() {
    searchChanged = 1;
}

function setPageNoAndSubmit(link) {
    var value = -1;
    if (link == 1) // next
    {
        value = parseInt(document.getElementsByName('pageno')[0].value, 10) + 1;
    } else if (link == 2) // previous
    {
        value = parseInt(document.getElementsByName('pageno')[0].value, 10) - 1;
    } else if (link == 3) // last
    {
        value = document.getElementById('hidden_totalpage').value;
    } else // 1st
    {
        value = 1
    }
    setPageNoInAllFields(value);
    console.log('pageno = ' + document.getElementsByName('pageno')[0].value);
    allfield_changed('go', 0);
}

function preprocessCheckBoxBeforeSubmitting(fieldname, row) {
    if (document.getElementById(fieldname + '_checkbox_' + row).checked) {
        document.getElementById(fieldname + '_checkbox_' + row).value = "true";
    } else {
        document.getElementById(fieldname + '_checkbox_' + row).value = "false";
    }
}

function preprocessDateBeforeSubmitting(fieldname, row) {
    $('#' + fieldname + '_date_' + row).val(getDateStringById(fieldname + '_js_' + row, 'DD/MM/YYYY'));
    console.log("setting date = " + $('#' + fieldname + '_date_' + row).val());
}

function preprocessTimeBeforeSubmitting(fieldname, row) {
    $('#' + fieldname + '_time_' + row).val(getTimeById(fieldname + '_js_' + row, true));
    console.log("setting time = " + $('#' + fieldname + '_time_' + row).val());
}

function getFormattedDate(id) {

    var foundDate = document.getElementById(id).value;
    console.log("found date = "
        + foundDate);
    if (foundDate === null || foundDate === '') {
        return '';
    }
    var date = new Date(
        document.getElementById(id).value)
        .getTime();

    console.log("date = " + date);
    return date;
}

function getBDFormattedDate(id) {

    var foundDate = document.getElementById(id).value;
    console.log("found date = "
        + foundDate);
    if (foundDate === null || foundDate === '') {
        return '';
    }
    var df = foundDate.split("/");
    var year = df[2];
    var month = df[1] - 1;
    var day = df[0];
    var date = (new Date(year, month, day).getTime());

    console.log("date = " + date);
    return date;
}

function getBDFormattedDateWithOneDayAddition(id) {

    var foundDate = document.getElementById(id).value;
    if (foundDate === null || foundDate === '') {
        return '';
    }
    var df = foundDate.split("/");
    var year = df[2];
    var month = df[1] - 1;
    var day = df[0];
    var date = new Date(year, month, day)
        .getTime() + 86399999;// 86399999 means 1 day in milliseconds - 1
    return date;
}

function getBDFormattedDateByStr(foundDate) {

    console.log("found date = "
        + foundDate);
    if (foundDate === null || foundDate === '') {
        return '';
    }
    var df = foundDate.split("/");
    var year = df[2];
    var month = df[1] - 1;
    var day = df[0];
    var date = new Date(year, month, day)
        .getTime();

    console.log("date = " + date);
    return date;
}

function preprocessGeolocationBeforeSubmitting(fieldname, row, isReport) {
    if (isReport == true) {
        /*
         * if(document.getElementById(fieldname + '_geolocation_' + row).value ==
         * "1") { document.getElementById(fieldname + '_geolocation_' +
         * row).value = ""; } if(document.getElementById(fieldname +
         * '_geolocation_' + row).value != "" &&
         * document.getElementById(fieldname + '_geoTextField_' + row).value !=
         * "") { document.getElementById(fieldname + '_geolocation_' +
         * row).value = document.getElementById(fieldname + '_geolocation_' +
         * row).value + ":" + document.getElementById(fieldname +
         * '_geoTextField_' + row).value; } else {
         * document.getElementById(fieldname + '_geolocation_' + row).value =
         * document.getElementById(fieldname + '_geolocation_' + row).value +
         * document.getElementById(fieldname + '_geoTextField_' + row).value; }
         * document.getElementById('address_geolocation_' + row).value =
         * document.getElementById('address_geolocation_' + row).value.replace("<br>","");
         * var lfcrRegexp = /\n\r?/g;
         * document.getElementById('address_geolocation_' + row).value =
         * document.getElementById('address_geolocation_' +
         * row).value.replace(lfcrRegexp,"");
         */

        var hiddenfield = document.getElementById(fieldname + '_geolocation_'
            + row);
        var value = "%";

        $("#" + fieldname + "_geoDIV_" + row).find("select").each(
            function (index) {
                var id = $(this).attr('id');
                var option = $("#" + id + " option:selected").html();
                console.log("id = " + id + " option = " + option);
                if (option && !option.includes("Select")) {
                    value += option + "%";
                }

            });
        value += document.getElementById(fieldname + '_geoTextField_' + row).value
            + "%";
        hiddenfield.value = value;
    } else {


        let textValue = document.getElementById(fieldname + '_geoTextField_' + row).value;
        if (textValue.toString().includes('$')) {
            toastr.error("$ sign not allowed");
            return false;
        }

        document.getElementById(fieldname + '_geolocation_' + row).value = document
                .getElementById(fieldname + '_geolocation_' + row).value
            + "$"
            + document.getElementById(fieldname + '_geoTextField_' + row).value;
    }
    console.log("geo value = "
        + document.getElementById(fieldname + '_geolocation_' + row).value);
    return true;
}

function initGeoLocation(idPrefix, row, ServletName) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById(idPrefix + row).innerHTML = this.responseText;
        } else if (this.readyState == 4 && this.status != 200) {
            alert('failed ' + this.status);
        }
    };
    xhttp.open("POST", ServletName + "?actionType=getGeo&myID=1", true);
    xhttp.send();
}

function initGeoLocationByLang(idPrefix, row, ServletName, language) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
			if(document.getElementById(idPrefix + row) != null)
			{
				document.getElementById(idPrefix + row).innerHTML = this.responseText;
			}
            
        } else if (this.readyState == 4 && this.status != 200) {
            alert('failed ' + this.status);
        }
    };
    xhttp.open("POST", ServletName + "?actionType=getGeo&myID=1&language=" + language, true);
    xhttp.send();
}

function addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName,
                          row, isReport, ServletName) {
    var geodiv = fieldName + "_geoDIV_" + row;
    var hiddenfield = fieldName + "_geolocation_" + row;
    console.log('geodiv = ' + geodiv + ' hiddenfield = ' + hiddenfield);
    try {
        var elements, ids;
        elements = document.getElementById(geodiv).children;

        if (!isReport) {
            document.getElementById(hiddenfield).value = value;
        }

        ids = '';
        for (var i = elements.length - 1; i >= 0; i--) {
            var elemID = elements[i].id;
            if (elemID.includes(htmlID) && elemID > htmlID) {
                ids += elements[i].id + ' ';

                for (var j = elements[i].options.length - 1; j >= 0; j--) {

                    elements[i].options[j].remove();
                }
                elements[i].remove();

            }
        }

        var newid = htmlID + '_1';

        document.getElementById(geodiv).innerHTML += "<select class='form-control' name='"
            + tagname
            + "' id = '"
            + newid
            + "' onChange=\"addrselected(this.value, this.id, this.selectedIndex, this.name, '"
            + fieldName + "', '" + row + "')\"></select>";

        document.getElementById(htmlID).options[0].innerHTML = document
            .getElementById(htmlID).options[selectedIndex].innerHTML;
        document.getElementById(htmlID).options[0].value = document
            .getElementById(htmlID).options[selectedIndex].value;

        if (isReport) {
            elements = document.getElementById(geodiv).children;
            var geoText = '';
            var j = 0;
            for (var i = 0; i < elements.length; i++) {
                var elemID = elements[i].id;

                if (elements[i].options[0]
                    && elements[i].options[0].innerHTML != "") {
                    if (j > 0) {
                        geoText += ", ";
                    }
                    geoText += elements[i].options[0].innerHTML;
                    j++;
                }

            }
            document.getElementById(hiddenfield).value = geoText;
            console.log("geoText now = " + geoText);
        }

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (!this.responseText.includes('option')) {
                    document.getElementById(newid).remove();
                } else {
                    document.getElementById(newid).innerHTML = this.responseText;
                }

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("POST", ServletName + "?actionType=getGeo&myID=" + value,
            true);

        xhttp.send();
    } catch (err) {
        alert("got error: " + err);
    }

}

function addrselectedFuncByLang(value, htmlID, selectedIndex, tagname, fieldName,
                                row, isReport, ServletName, language) {
    var geodiv = fieldName + "_geoDIV_" + row;
    var hiddenfield = fieldName + "_geolocation_" + row;
    console.log('geodiv = ' + geodiv + ' hiddenfield = ' + hiddenfield);
    try {
        var elements, ids;
        elements = document.getElementById(geodiv).children;

        if (!isReport) {
            document.getElementById(hiddenfield).value = value;
        }

        ids = '';
        for (var i = elements.length - 1; i >= 0; i--) {
            var elemID = elements[i].id;
            if (elemID.includes(htmlID) && elemID > htmlID) {
                ids += elements[i].id + ' ';

                for (var j = elements[i].options.length - 1; j >= 0; j--) {

                    elements[i].options[j].remove();
                }
                elements[i].remove();

            }
        }

        var newid = htmlID + '_1';

        document.getElementById(geodiv).innerHTML += "<select class='form-control' name='"
            + tagname
            + "' id = '"
            + newid
            + "' onChange=\"addressSelectedByLanguage(this.value, this.id, this.selectedIndex, this.name, '"
            + fieldName + "', '" + row + "', '" + language + "')\"></select>";

        document.getElementById(htmlID).options[0].innerHTML = document.getElementById(htmlID).options[selectedIndex].innerHTML;
        document.getElementById(htmlID).options[0].value = document.getElementById(htmlID).options[selectedIndex].value;

        if (isReport) {
            elements = document.getElementById(geodiv).children;
            var geoText = '';
            var j = 0;
            for (var i = 0; i < elements.length; i++) {
                var elemID = elements[i].id;

                if (elements[i].options[0]
                    && elements[i].options[0].innerHTML != "") {
                    if (j > 0) {
                        geoText += ", ";
                    }
                    geoText += elements[i].options[0].innerHTML;
                    j++;
                }

            }
            document.getElementById(hiddenfield).value = geoText;
            console.log("geoText now = " + geoText);
        }

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (!this.responseText.includes('option')) {
                    document.getElementById(newid).remove();
                } else {
                    document.getElementById(newid).innerHTML = this.responseText;
                }

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("POST", ServletName + "?actionType=getGeo&myID=" + value + "&language=" + language,
            true);

        xhttp.send();
    } catch (err) {
        alert("got error: " + err);
    }

}

if (document.getElementById('header_language_id') != null) {
    let headerLang = $('#header_language_id').val();
    if (headerLang && headerLang.toLowerCase() === 'bangla') {
        Dropzone.prototype.defaultOptions.dictRemoveFile = 'ফাইল সরান';
        Dropzone.prototype.defaultOptions.dictCancelUpload = 'আপলোড বাতিল';
        Dropzone.prototype.defaultOptions.fileRemoveText = 'আপনার ফাইল সরানো হয়েছে';
        Dropzone.prototype.defaultOptions.uploadSuccessText = 'সফলভাবে আপলোড করা হয়েছে';
        Dropzone.prototype.defaultOptions.uploadFailedText = 'আপলোড ব্যর্থ হয়েছে';
        Dropzone.prototype.defaultOptions.maxSizeExceeds = 'ফাইলের আকার সর্বোচ্চ 1 মেগাবাইট হতে পারে';
        Dropzone.prototype.defaultOptions.dictDefaultMessage = 'ফাইল আপলোড করার জন্য এখানে ফাইল ড্রপ করুন';
    }
}
