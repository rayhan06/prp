$(".englishOnly").keypress(function(event){
    var ew = event.which;
    if(ew === 32)
        return true;
    if(48 <= ew && ew <= 57)
        return true;
    if(65 <= ew && ew <= 90)
        return true;
    if(97 <= ew && ew <= 122)
        return true;
    return "#,.;:(){}/-&|+-".includes(String.fromCharCode(ew));
});
$(".digitOnly").keypress(event=>{
    let ew = event.which;
    return 48 <= ew && ew <= 57;
});
$(".noEnglish").keypress(function(event){
    var ew = event.which;
    if(ew === 32)
        return true;
    if(48 <= ew && ew <= 57)
        return false;
    if(65 <= ew && ew <= 90)
        return false;
    return !(97 <= ew && ew <= 122);
});

$(".onlyBanglaAndEnglishDigitOnly").keypress(event=>{
    let ew = event.which;
    if(48 <= ew && ew <= 57){
        return true;
    }
    return "০১২৩৪৫৬৭৮৯".includes(String.fromCharCode(ew));
});