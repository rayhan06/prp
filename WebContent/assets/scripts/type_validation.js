const isLanguageEnglish = '<%=Language%>'.toLowerCase() === 'english';
const nameEngTextSelector = $('#nameEn_text');
const nameBngTextSelector = $('#nameBn_text');
const submitBtn = $('#submit-btn');
const cancelBtn = $('#cancel-btn');
const typeForm = $('#bigform');
function submitForm(){
    console.log('.............');
    buttonStateChange(true);
    if(!nameEngTextSelector.val()){
        $('#toast_message').css('background-color', '#ff6063');
        showToastSticky('অনুগ্রহ করে ইংরেজির নাম লিখুন','Please enter english name');
        buttonStateChange(false);
        return;
    }
    if(!nameBngTextSelector.val()){
        $('#toast_message').css('background-color', '#ff6063');
        showToastSticky('অনুগ্রহ করে বাংলা নাম লিখুন','Please enter bangla name');
        buttonStateChange(false);
        return;
    }
    $.ajax({
        type : "POST",
        url : typeForm.attr('action'),
        data : typeForm.serialize(),
        dataType : 'JSON',
        success : function(response) {
            if(response.responseCode === 0){
                $('#toast_message').css('background-color','#ff6063');
                showToastSticky(response.msg,response.msg);
                buttonStateChange(false);
            }else if(response.responseCode === 200){
                window.location.replace(getContextPath()+response.msg);
            }
        },
        error : function(jqXHR, textStatus, errorThrown) {
            toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus + ", Message: " + errorThrown);
            buttonStateChange(false);
        }
    });
}

function buttonStateChange(value){
    submitBtn.prop('disabled',value);
    cancelBtn.prop('disabled',value);
}