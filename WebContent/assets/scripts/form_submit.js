function submitAjaxForm(formName = 'bigform', method = 'POST', successFunction, failureFunction) {
    let form = $("#" + formName);
    submitAjaxByData(form.serialize(), form.attr('action'), method, successFunction, failureFunction);
}

function submitAjaxByData(data, url, method = 'POST', successFunction, failureFunction) {
    buttonStateChangeFunction(true);
    $.ajax({
        type: method,
        url: url,
        data: data,
        dataType: 'JSON',
        success: function (response) {
            console.log(response);
            if (successFunction && typeof successFunction == 'function') {
                buttonStateChangeFunction(false);
                successFunction(response);
            } else if (response.responseCode === 0) {
                $('#toast_message').css('background-color', '#ff6063');
                showToastSticky(response.msg, response.msg);
                buttonStateChangeFunction(false);
            } else if (response.responseCode === 302 || response.responseCode === 200) {
                window.location.replace(getContextPath() + response.msg);
            } else {
                console.log("Error: " + response.responseCode);
                buttonStateChangeFunction(false);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            buttonStateChangeFunction(false);
            if (failureFunction && typeof failureFunction == 'function') {
                failureFunction(jqXHR, textStatus, errorThrown);
            } else {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
            }
        }
    });
}

function buttonStateChangeFunction(flag) {
    if (buttonStateChange && typeof (buttonStateChange) == 'function') {
        buttonStateChange(flag);
    }
}