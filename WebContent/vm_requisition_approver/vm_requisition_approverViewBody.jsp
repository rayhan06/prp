

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="vm_requisition_approver.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>




<%
String servletName = "Vm_requisition_approverServlet";
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.VM_REQUISITION_APPROVER_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
Vm_requisition_approverDAO vm_requisition_approverDAO = new Vm_requisition_approverDAO("vm_requisition_approver");
Vm_requisition_approverDTO vm_requisition_approverDTO = Vm_requisition_approverRepository.getInstance().getVm_requisition_approverDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<!-- <div class="modal-content viewmodal"> -->
<div class="menubottom">
            <div class="modal-header">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9 col-sm-12">
                            <h5 class="modal-title"><%=LM.getText(LC.VM_REQUISITION_APPROVER_ADD_VM_REQUISITION_APPROVER_ADD_FORMNAME, loginDTO)%></h5>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="modal-body container">
			
			<div class="row div_border office-div">

                    <div class="col-md-12">
                        <h3><%=LM.getText(LC.VM_REQUISITION_APPROVER_ADD_VM_REQUISITION_APPROVER_ADD_FORMNAME, loginDTO)%></h3>
						<table class="table table-bordered table-striped">








							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_APPROVER_ADD_APPROVERNAMEEN, loginDTO)%></b></td>
								<td>

									<%
										value = vm_requisition_approverDTO.approverNameEn + ", " + vm_requisition_approverDTO.approverOfficeUnitOrgNameEn;
									%>

									<%=Utils.getDigits(value, Language)%>


								</td>

							</tr>






							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_APPROVER_ADD_APPROVERNAMEBN, loginDTO)%></b></td>
								<td>

									<%
										value = vm_requisition_approverDTO.approverNameBn + ", " + vm_requisition_approverDTO.approverOfficeUnitOrgNameBn;
									%>

									<%=Utils.getDigits(value, Language)%>


								</td>

							</tr>

				


			
		
						</table>
                    </div>
			






			</div>	

               


        </div>
	</div>