<%@page pageEncoding="UTF-8" %>

<%@page import="vm_requisition_approver.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.VM_REQUISITION_APPROVER_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_VM_REQUISITION_APPROVER;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Vm_requisition_approverDTO vm_requisition_approverDTO = (Vm_requisition_approverDTO)request.getAttribute("vm_requisition_approverDTO");
CommonDTO commonDTO = vm_requisition_approverDTO;
String servletName = "Vm_requisition_approverServlet";


System.out.println("vm_requisition_approverDTO = " + vm_requisition_approverDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Vm_requisition_approverDAO vm_requisition_approverDAO = (Vm_requisition_approverDAO)request.getAttribute("vm_requisition_approverDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
		
		
		
		
		
		
		
											<td id = '<%=i%>_approverOrgId'>
												<%
													value = Language.equals("English") ?
															vm_requisition_approverDTO.approverNameEn + ", " + vm_requisition_approverDTO.approverOfficeUnitOrgNameEn
															: vm_requisition_approverDTO.approverNameBn + ", " + vm_requisition_approverDTO.approverOfficeUnitOrgNameBn
													;
												%>

												<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
	

											<td>
												<button class="btn btn-sm border-0 shadow" style="background-color: #22ccc1; color: white; border-radius: 8px"
											            onclick="event.preventDefault();location.href='Vm_requisition_approverServlet?actionType=view&ID=<%=vm_requisition_approverDTO.iD%>'">
											        <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
											    </button>												
											</td>
	
<%--											<td id = '<%=i%>_Edit'>																																	--%>
<%--	--%>
<%--												<button class="btn btn-sm border-0 shadow" style="background-color: #cc22c1; color: white; border-radius: 8px"--%>
<%--											            onclick="location.href='Vm_requisition_approverServlet?actionType=getEditPage&ID=<%=vm_requisition_approverDTO.iD%>'">--%>
<%--											        <%=LM.getText(LC.VM_REQUISITION_APPROVER_SEARCH_VM_REQUISITION_APPROVER_EDIT_BUTTON, loginDTO)%>--%>
<%--											    </button>--%>
<%--																				--%>
<%--											</td>											--%>
											
											
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=vm_requisition_approverDTO.iD%>'/></span>
												</div>
											</td>
																						
											

