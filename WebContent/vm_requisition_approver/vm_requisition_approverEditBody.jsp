<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_requisition_approver.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="employee_assign.EmployeeSearchIds" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="vm_requisition.Vm_requisitionDAO" %>
<%@ page import="role.RoleDTO" %>
<%@ page import="role.PermissionRepository" %>

<%
    Vm_requisition_approverDTO vm_requisition_approverDTO;
    vm_requisition_approverDTO = (Vm_requisition_approverDTO) request.getAttribute("vm_requisition_approverDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

    RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
    boolean isAdmin = role.ID == SessionConstants.VEHICLE_ADMIN_ROLE || role.ID == SessionConstants.ADMIN_ROLE;

    if (vm_requisition_approverDTO == null) {
        vm_requisition_approverDTO = new Vm_requisition_approverDTO();

    }
    System.out.println("vm_requisition_approverDTO = " + vm_requisition_approverDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.VM_REQUISITION_APPROVER_ADD_VM_REQUISITION_APPROVER_ADD_FORMNAME, loginDTO);
    String servletName = "Vm_requisition_approverServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_REQUISITION_APPROVER_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Vm_requisition_approverServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD'
                                           id='iD_hidden_<%=i%>'
                                           value='<%=vm_requisition_approverDTO.iD%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=vm_requisition_approverDTO.insertedByUserId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=vm_requisition_approverDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=vm_requisition_approverDTO.insertionDate%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value='<%=vm_requisition_approverDTO.isDeleted%>'
                                           tag='pb_html'/>

                                    <input type='hidden' class='form-control'
                                           name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=vm_requisition_approverDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=vm_requisition_approverDTO.searchColumn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='approverOrgId'
                                           id='approverOrgId_hidden_<%=i%>'
                                           value='<%=vm_requisition_approverDTO.approverOrgId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='approverOfficeId'
                                           id='approverOfficeId_hidden_<%=i%>'
                                           value='<%=vm_requisition_approverDTO.approverOfficeId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='approverOfficeUnitId'
                                           id='approverOfficeUnitId_hidden_<%=i%>'
                                           value='<%=vm_requisition_approverDTO.approverOfficeUnitId%>'
                                           tag='pb_html'/>
                                    <input type='text' style='display:none' class='form-control'
                                           name='approverEmpId' id='approverEmpId_hidden_<%=i%>'
                                           value='<%=vm_requisition_approverDTO.approverEmpId%>'
                                           tag='pb_html'/>


                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.VM_REQUISITION_APPROVER_ADD_APPROVERNAMEBN, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type="hidden" class='form-control'
                                                   name='employeeRecordsId' id='employeeRecordsId'
                                                   value=''>
                                            <%
                                                if (isAdmin) {
                                            %>
                                            <button type="button" class="btn btn-primary form-control shadow btn-border-radius mb-3"
                                                    id="tagApprover_modal_button">
                                                <%=LM.getText(LC.HM_SELECT, userDTO)%>
                                            </button>
                                            <%
                                            } else {
                                            %>
                                            <input type="hidden" class='form-control'
                                                   id='tagApprover_modal_button'
                                                   value=''>
                                            <%
                                                }
                                            %>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped">
                                                    <thead></thead>

                                                    <tbody id="tagged_approver_table">
                                                    <tr style="display: none;">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <button type="button"
                                                                    class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4"
                                                                    style="padding-right: 15px"
                                                                    onclick="remove_containing_row(this,'tagged_approver_table');">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>

                                                    <%
                                                        Vm_requisition_approverDAO vm_requisition_approverDAO = new Vm_requisition_approverDAO();
                                                        List<Vm_requisition_approverDTO> vm_requisition_approverDTOS = vm_requisition_approverDAO.getAllVm_requisition_approver(true);
                                                        for (Vm_requisition_approverDTO dto : vm_requisition_approverDTOS) {
                                                    %>
                                                    <tr>
                                                        <td><%=Employee_recordsRepository.getInstance()
                                                                .getById(dto.approverEmpId).employeeNumber%>
                                                        </td>
                                                        <td>
                                                            <%=isLanguageEnglish ? (dto.approverNameEn)
                                                                    : (dto.approverNameBn)%>
                                                        </td>

                                                        <%
                                                            String postName = isLanguageEnglish ? (dto.approverOfficeUnitOrgNameEn + ", " + dto.approverOfficeUnitNameEn)
                                                                    : (dto.approverOfficeUnitOrgNameBn + ", " + dto.approverOfficeUnitNameBn);
                                                        %>

                                                        <td><%=postName%>
                                                        </td>
                                                        <td id='<%=dto.approverEmpId%>_td_button'>
                                                            <%
                                                                if (isAdmin) {
                                                            %>

                                                            <button type="button"
                                                                    class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4"
                                                                    style="padding-right: 15px"
                                                                    onclick="remove_containing_row(this,'tagged_approver_table');">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                            <%
                                                                }
                                                            %>
                                                        </td>
                                                    </tr>
                                                    <%
                                                        }
                                                    %>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <%
                        if (isAdmin) {
                    %>
                    <div class="col-md-10 form-actions text-right mt-3">
                        <a class="btn btn-sm cancel-btn text-white shadow btn-border-radius"
                           href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.VM_REQUISITION_APPROVER_ADD_VM_REQUISITION_APPROVER_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button class="btn  btn-sm submit-btn text-white shadow btn-border-radius ml-2"
                                type="submit"><%=LM.getText(LC.VM_REQUISITION_APPROVER_ADD_VM_REQUISITION_APPROVER_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                    <%
                        }
                    %>
                </div>
            </div>
        </form>
    </div>
</div>


<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>


<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {

        var existingData = <%=vm_requisition_approverDTOS.size()%>;

        let data = added_approver_map.keys().next();
        if (!(data && data.value) && existingData) {
            toastr.error("Please Select Person");
            return false;
        }
        document.getElementById('approverEmpId_hidden_0').value = JSON.stringify(
            Array.from(added_approver_map.values()));

        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Vm_requisition_approverServlet");
    }

    function init(row) {


    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
    });

    var child_table_extra_id = <%=childTableStartingID%>;


    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button

    // map to store and send added employee data
    // map to store and send added employee data


    <%
    List<EmployeeSearchIds> addedEmpIdsList = new ArrayList<>();
    if(true) {
        for (Vm_requisition_approverDTO dto: vm_requisition_approverDTOS) {
            addedEmpIdsList.add(new EmployeeSearchIds
        (dto.approverEmpId,dto.approverOfficeUnitId,dto.approverOrgId));
        }
    }
    %>

    added_approver_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(addedEmpIdsList)%>);


    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_approver_table', {
                info_map: added_approver_map,
                isSingleEntry: false
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagApprover_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_approver_table';
        $('#search_emp_modal').modal();
    });


</script>


<style>
    .required {
        color: red;
    }
</style>



