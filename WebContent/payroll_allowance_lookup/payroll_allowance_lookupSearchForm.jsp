<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="payroll_allowance_lookup.*" %>
<%@ page import="util.*" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="payroll_allowance_configuration.Payroll_allowance_configurationRepository" %>

<%
    String navigator2 = "navPAYROLL_ALLOWANCE_LOOKUP";
    String servletName = "Payroll_allowance_lookupServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th>
				<%=LM.getText(LC.PAYROLL_ALLOWANCE_CONFIGURATION_ADD_EMPLOYMENTCAT, loginDTO)%>

            </th>
			<th>
				<%=LM.getText(LC.PAYROLL_ALLOWANCE_LOOKUP_ADD_ORGANOGRAMKEY, loginDTO)%>
			</th>
            <th>
				<%=LM.getText(LC.PAYROLL_ALLOWANCE_LOOKUP_ADD_PAYROLLALLOWANCECONFIGID, loginDTO)%>
            </th>
            <th>
				<%=LM.getText(LC.PAYROLL_ALLOWANCE_LOOKUP_ADD_AMOUNT, loginDTO)%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
			List<Payroll_allowance_lookupDTO> data = (ArrayList<Payroll_allowance_lookupDTO>) rn2.list;
            try {
                if (data != null) {
                    for (Payroll_allowance_lookupDTO payroll_allowance_lookupDTO : data) {
        %>
        <tr>
            <td>
                <%=CatRepository.getInstance().getText(Language, "employment", payroll_allowance_lookupDTO.employmentCat)%>
            </td>

			<td>
				<%=OfficeUnitOrganogramsRepository.getInstance().getDesignation(Language, payroll_allowance_lookupDTO.organogramId)%>
			</td>

            <td>
                <%=Payroll_allowance_configurationRepository.getInstance().getAllowanceName(
						payroll_allowance_lookupDTO.payrollAllowanceConfigId,
						Language
				)%>
            </td>

            <td>
                <%=Utils.getDigits(String.valueOf(payroll_allowance_lookupDTO.amount), Language)%>
            </td>
        </tr>
        <%
                    }
                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>