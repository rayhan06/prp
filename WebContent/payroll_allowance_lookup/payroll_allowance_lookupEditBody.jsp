<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="employee_records.EmploymentEnum" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>

<%@include file="../pb/addInitializer2.jsp" %>

<%
    String formTitle = LM.getText(LC.PAYROLL_ALLOWANCE_LOOKUP_ADD_PAYROLL_ALLOWANCE_LOOKUP_ADD_FORMNAME, loginDTO);
    String servletName = "Payroll_allowance_lookupServlet";
    int PRIVILEGED_EMPLOYEE_CAT = EmploymentEnum.PRIVILEGED.getValue();
    String context = request.getContextPath() + "/";

    // always add action expected
    actionName = "add";
%>

<style>
    input[readonly],
    textarea[readonly] {
        font-weight: bold;
        background-color: rgba(231, 231, 231, .5) !important;
    }

    .template-row {
        display: none;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="lookup-amount-form" enctype="multipart/form-data"
            action="Payroll_allowance_lookupServlet?actionType=ajax_<%=actionName%>">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.PAYROLL_ALLOWANCE_CONFIGURATION_ADD_EMPLOYMENTCAT, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type="text" class='form-control' readonly
                                                   value="<%=CatRepository.getName(Language, "employment", PRIVILEGED_EMPLOYEE_CAT)%>">
                                            <input type='hidden' name='employmentCat' id="employmentCat"
                                                   value='<%=PRIVILEGED_EMPLOYEE_CAT%>'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="organogramId">
                                            <%=LM.getText(LC.PAYROLL_ALLOWANCE_LOOKUP_ADD_ORGANOGRAMKEY, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='organogramId' id='organogramId'
                                                    onchange="organogramIdChanged(this);">
                                                <%=Employee_recordsRepository.getInstance().buildDesignationOption(
                                                        PRIVILEGED_EMPLOYEE_CAT,
                                                        null,
                                                        Language
                                                )%>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" mt-5">
                    <div class="table-responsive">
                        <input type="hidden" name="economicSubCodes" id="economicSubCodes-input">
                        <table class="table table-striped table-bordered text-nowrap" id="lookup-amount-table">
                            <thead>
                            <tr>
                                <th class="row-data-code" style="width: 60%;">
                                    <%=LM.getText(LC.PAYROLL_ALLOWANCE_LOOKUP_ADD_PAYROLLALLOWANCECONFIGID, loginDTO)%>
                                </th>
                                <th class="row-data-name" style="width: 40%;">
                                    <%=LM.getText(LC.PAYROLL_ALLOWANCE_LOOKUP_ADD_AMOUNT, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="main-tbody">
                            </tbody>

                            <%--don't put these tr inside tbody--%>
                            <tr class="loading-gif" style="display: none;">
                                <td class="text-center" colspan="100%">
                                    <img alt="" class="loading"
                                         src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
                                    <span><%=getDataByLanguage(Language, "লোড হচ্ছে...", "Loading...")%></span>
                                </td>
                            </tr>
                            <tr class="template-row">
                                <input type="hidden" name="lookupDTOId"/>
                                <input type="hidden" name="configId" value="-1"/>
                                <td class="row-data-name">
                                    শিক্ষা ভাতা
                                </td>
                                <td>
                                    <input type='text' class='form-control' name='amount' data-only-integer="true"/>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class=" mt-3">
                    <div class="">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button"
                                    onclick="location.href = '<%=request.getHeader("referer")%>'">
                                <%=LM.getText(LC.PAYROLL_ALLOWANCE_LOOKUP_ADD_PAYROLL_ALLOWANCE_LOOKUP_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                                    onclick="submitForm();">
                                <%=LM.getText(LC.PAYROLL_ALLOWANCE_LOOKUP_ADD_PAYROLL_ALLOWANCE_LOOKUP_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script type="text/javascript">
    const tableId = 'lookup-amount-table';
    const tableBody = document.querySelector('#lookup-amount-table tbody.main-tbody');
    const templateRow = document.querySelector('#lookup-amount-table tr.template-row');

    function clearTable(tableId, putEmptyTableText) {
        const tableBody = document.querySelector('#' + tableId + ' tbody.main-tbody');

        const emptyTableText = '<%=getDataByLanguage(Language, "কোন তথ্য পাওয়া যায়নি", "No data found")%>';

        if(putEmptyTableText)
            tableBody.innerHTML = '<tr class="text-center"><td colspan="100%">'
                                + emptyTableText + '</td></tr>';
        else
            tableBody.innerHTML = '';
        document.querySelectorAll('#' + tableId + ' tfoot').forEach(tfoot => tfoot.remove());
    }

    function showOrHideLoadingGif(tableId, toShow) {
        const loadingGif = $('#' + tableId + ' tr.loading-gif');
        if (toShow) {
            clearTable(tableId, false);
            loadingGif.show();
        } else loadingGif.hide();
    }

    async function organogramIdChanged(selectElement) {
        const organogramId = selectElement.value;
        clearTable(tableId, true);
        if (organogramId === '') return;
        const employmentCat = $('#employmentCat').val();
        const url = 'Payroll_allowance_lookupServlet?actionType=ajax_getAllowanceLookupModels'
            + '&organogramId=' + organogramId + '&employmentCat=' + employmentCat;

        showOrHideLoadingGif(tableId, true);

        const response = await fetch(url);
        const allowanceLookupModels = await response.json();

        showOrHideLoadingGif(tableId, false);

        const isDataAvailable = Array.isArray(allowanceLookupModels) && (allowanceLookupModels.length > 0);
        if (isDataAvailable) {
            allowanceLookupModels.forEach(model => addModelToTable(model, tableBody, templateRow));
        } else {
            clearTable(tableId, true);
        }
    }

    function addModelToTable(lookupModel, tableBody, templateRow) {
        const newRow = templateRow.cloneNode(true);
        newRow.classList.remove('template-row');
        newRow.querySelector('input[name="lookupDTOId"]').value = lookupModel.lookupDTOId;
        newRow.querySelector('input[name="configId"]').value = lookupModel.payrollAllowanceConfigId;
        newRow.querySelector('input[name="amount"]').value = lookupModel.amount;
        newRow.querySelector('.row-data-name').textContent = lookupModel.allowanceName;
        tableBody.append(newRow);
    }

    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Payroll_allowance_lookupServlet");
    }

    function typeOnlyInteger(e) {
        return true === inputValidationForIntValue(e, $(this));
    }

    function init() {
        select2SingleSelector("#payrollAllowanceConfigId", '<%=Language%>');
        $('body').delegate('input[data-only-integer="true"]', 'keydown', typeOnlyInteger);
    }

    $(document).ready(function () {
        init();
    });

    function submitForm(){
        submitAjaxForm('lookup-amount-form');
    }

    function buttonStateChange(value){
        $('#cancel-btn').prop('disabled',value);
        $('#submit-btn').prop('disabled',value);
    }
</script>