<%@page import="login.LoginDTO"%>
<%@page import="am_parliament_building_level.*"%>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@ page import="util.*"%>
<%@ page import="common.BaseServlet" %>

<%

    String servletName = "Am_parliament_building_levelServlet";
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    String actionName;
    Am_parliament_building_levelDTO am_parliament_building_levelDTO;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        am_parliament_building_levelDTO = (Am_parliament_building_levelDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    } else {
        actionName = "add";
        am_parliament_building_levelDTO = new Am_parliament_building_levelDTO();
    }

    String formTitle = LM.getText(LC.AM_PARLIAMENT_BUILDING_LEVEL_ADD_AM_PARLIAMENT_BUILDING_LEVEL_ADD_FORMNAME, loginDTO);
    int i = 0;


%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              id="bigform" name="bigform" >
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=am_parliament_building_levelDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right">
                                                                <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_LEVEL_ADD_LEVELNO, loginDTO)%>
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-8">
																<input type='text' class='form-control'  name='levelNo' id = 'levelNo_text_<%=i%>' value='<%=am_parliament_building_levelDTO.levelNo%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=am_parliament_building_levelDTO.insertedByUserId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value='<%=am_parliament_building_levelDTO.insertedByOrganogramId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=am_parliament_building_levelDTO.insertionDate%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=am_parliament_building_levelDTO.isDeleted%>' tag='pb_html'/>
											
														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=am_parliament_building_levelDTO.lastModificationTime%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=am_parliament_building_levelDTO.searchColumn%>' tag='pb_html'/>
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <a class="btn btn-sm cancel-btn text-white shadow btn-border-radius" id="cancel-btn"
                               href="<%=request.getHeader("referer")%>">
                                <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_LEVEL_ADD_AM_PARLIAMENT_BUILDING_LEVEL_CANCEL_BUTTON, loginDTO)%>
                            </a>
                            <button id="submit-btn"  class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit"
                                    onclick="event.preventDefault();submitForm()">
                                <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_LEVEL_ADD_AM_PARLIAMENT_BUILDING_LEVEL_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
const levelForm = $("#bigform");

function buttonStateChange(value){
    $('#submit-btn').prop('disabled',value);
    $('#cancel-btn').prop('disabled',value);
}

function submitForm(){
    buttonStateChange(true);
    if(PreprocessBeforeSubmiting()){
        $.ajax({
            type : "POST",
            url : "Am_parliament_building_levelServlet?actionType=ajax_<%=actionName%>",
            data : levelForm.serialize(),
            dataType : 'JSON',
            success : function(response) {
                if(response.responseCode === 0){
                    $('#toast_message').css('background-color','#ff6063');
                    showToastSticky(response.msg,response.msg);
                    buttonStateChange(false);
                }else if(response.responseCode === 200){
                    window.location.replace(getContextPath()+response.msg);
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChange(false);
            }
        });
    }else{
        buttonStateChange(false);
    }
}

function PreprocessBeforeSubmiting()
{
    levelForm.validate();
    return levelForm.valid();
}

$(document).ready(function(){
    let lang = '<%=Language%>';
    let levelErr;
    if (lang.toUpperCase() === 'ENGLISH') {
        levelErr = 'Level Number Required';

    } else {
        levelErr = 'লেভেল নম্বর প্রয়োজনীয়';
    }

    levelForm.validate({
        errorClass: 'error is-invalid',
        validClass: 'is-valid',
        rules: {
            levelNo: {
                required: true,
            },
        },
        messages: {
            levelNo: levelErr,
        }
    });

});	




</script>






