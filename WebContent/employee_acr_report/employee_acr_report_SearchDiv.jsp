<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="pb.*" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="util.StringUtils" %>
<%@ page import="common.RoleEnum" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EMPLOYEE_ACR_REPORT_EDIT_LANGUAGE, loginDTO);
    boolean isLangEng = Language.equalsIgnoreCase("English");
    CommonDAO.language = Language;
    CatDAO.language = Language;
    int year = Calendar.getInstance().get(Calendar.YEAR);
%>

<jsp:include page="../employee_assign/officeMultiSelectTagsUtil.jsp"/>

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>

<div class="row">
    <div class="col-12">
        <div class="row mx-2 mx-md-0">
            <div id="criteriaSelectionId" class="search-criteria-div col-md-6">
                <div class="form-group row">
                    <div class="col-md-3"></div>
                    <div class="col-md-9">
                        <button type="button"
                                class="btn btn-sm btn-info btn-info text-white shadow btn-border-radius"
                                data-toggle="modal" data-target="#select_criteria_div" id="select_criteria_btn"
                                onclick="beforeOpenCriteriaModal()">
                            <%=isLangEng ? "Criteria Select" : "ক্রাইটেরিয়া বাছাই"%>
                        </button>
                    </div>
                </div>
            </div>
            <div id="criteriaPaddingId" class="search-criteria-div col-md-6">
            </div>
            <div id="officeUnitId" class="search-criteria-div col-md-6 officeModalClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_OFFICEUNITID, loginDTO)%>
                    </label>
                    <div class="col-md-9">

                        <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                id="office_units_id_modal_button"
                                onclick="officeModalButtonClicked();">
                            <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                        </button>
                        <input type="hidden" name='officeUnitIds' id='officeUnitIds_input' value="">
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6 officeModalClass" style="display: none">
                <div class="form-group row">
                    <label class="col-sm-4 col-xl-3 col-form-label text-md-right" for="onlySelectedOffice_checkbox">
                        <%=isLangEng ? "Only Selected Office" : "শুধুমাত্র নির্বাচিত অফিস" %>
                    </label>
                    <div class="col-1" id='onlySelectedOffice'>
                        <input type='checkbox' class='form-control-sm mt-1' name='onlySelectedOffice'
                               id='onlySelectedOffice_checkbox'
                               onchange="this.value = this.checked;" value='false'>
                    </div>
                    <div class="col-8"></div>
                </div>
            </div>

            <div class="search-criteria-div col-12" id="selected-offices" style="display: none;">
                <div class="form-group row">
                    <div class="offset-1 col-10 tag-container" id="selected-offices-tag-container">
                        <div class="tag template-tag">
                            <span class="tag-name"></span>
                            <i class="fas fa-times-circle tag-remove-btn"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6 checkBoxClass" style="display: none;">
                <div class="form-group row">
                    <label class="col-sm-4 col-xl-3 col-form-label text-md-right" for="isPartial_checkbox">
                        <%=LM.getText(LC.EMPLOYEE_ACR_REPORT_WHERE_NOT_SUBMITTED, loginDTO)%>
                    </label>
                    <div class="col-1" id='isPartialDiv'>
                        <input type='checkbox' class='form-control-sm mt-1' name='getNotSubmittedAcrReport'
                               id='isPartial_checkbox'
                               onchange="this.value = this.checked;" value='false'>
                    </div>
                    <div class="col-8"></div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6 yearClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_ACR_REPORT_WHERE_YEAR, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control' name='year' id='year-select'>
                            <option value=""><%=LM.getText(LC.HM_SELECT, loginDTO)%>
                                    <%for(int yearOpt = year + 5; yearOpt >= 1900;yearOpt--){%>
                            <option value="<%=yearOpt%>">
                                <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(yearOpt))%>
                            </option>
                            <%}%>
                        </select>
                    </div>
                </div>
            </div>
            <div id="name_eng_div" class="search-criteria-div col-md-6 employeeNameClass"
                 style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="nameEng">
                        <%=isLangEng ? "Name(English)" : "নাম(ইংরেজি)"%>
                    </label>
                    <div class="col-md-9">
                        <input class="englishOnly form-control" type="text" name="nameEng" id="nameEng"
                               style="width: 100%"
                               placeholder="<%=isLangEng?"Enter Employee Name in English":"কর্মকর্তা/কর্মচারীর নাম ইংরেজিতে লিখুন"%>"
                               value="">
                    </div>
                </div>
            </div>

            <div id="name_bng_div" class="search-criteria-div col-md-6 employeeNameClass"
                 style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="nameBng">
                        <%=isLangEng ? "Name(Bangla)" : "নাম(বাংলা)"%>
                    </label>
                    <div class="col-md-9">
                        <input class="noEnglish form-control" type="text" name="nameBng" id="nameBng"
                               style="width: 100%"
                               placeholder="<%=isLangEng?"Enter Employee Name in Bangla":"কর্মকর্তা/কর্মচারীর নাম বাংলাতে লিখুন"%>"
                               value="">
                    </div>
                </div>
            </div>
            <div class="search-criteria-div col-md-6 partialClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_ACR_REPORT_WHERE_ISPARTIAL, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select name="isPartial" id="isPartial-select" class='form-control'>
                            <%=CatRepository.getInstance().buildOptions("is_partial", Language, 0)%>
                        </select>
                    </div>
                </div>
            </div>
            <div class="search-criteria-div col-md-6 statusClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_ACR_REPORT_WHERE_STATUS, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control' name='status' id='status-select'>
                            <%=CatRepository.getInstance().buildOptions("status", Language, null)%>
                        </select>
                    </div>
                </div>
            </div>
            <div class="search-criteria-div col-md-6 acrFromClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_ACR_REPORT_WHERE_ACRFROM, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <jsp:include page="/date/date.jsp">
                            <jsp:param name="DATE_ID" value="acrFromStart-js"/>
                            <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            <jsp:param name="END_YEAR" value="<%=year + 5%>"/>
                        </jsp:include>
                        <input type="hidden" name='acrFromStart' id='acrFromStart' value=""/>
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6 acrFromClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_ACR_REPORT_WHERE_ACRFROM_8, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <jsp:include page="/date/date.jsp">
                            <jsp:param name="DATE_ID" value="acrFromEnd-js"/>
                            <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            <jsp:param name="END_YEAR" value="<%=year + 5%>"/>
                        </jsp:include>
                        <input type="hidden" name='acrFromEnd' id='acrFromEnd' value=""/>
                    </div>
                </div>
            </div>


            <div class="search-criteria-div col-md-6 submissionClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_ACR_REPORT_WHERE_INSERTIONDATE, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <jsp:include page="/date/date.jsp">
                            <jsp:param name="DATE_ID" value="submissionStartDate-js"/>
                            <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            <jsp:param name="END_YEAR" value="<%=year + 5%>"/>
                        </jsp:include>
                        <input type="hidden" name='submissionStartDate' id='submissionStartDate' value=""/>
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6 submissionClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_ACR_REPORT_WHERE_INSERTIONDATE_3, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <jsp:include page="/date/date.jsp">
                            <jsp:param name="DATE_ID" value="submissionEndDate-js"/>
                            <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            <jsp:param name="END_YEAR" value="<%=year + 5%>"/>
                        </jsp:include>
                        <input type="hidden" name='submissionEndDate' id='submissionEndDate' value=""/>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<%
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    boolean isHierarchyNeeded =
            userDTO != null
                    && userDTO.roleID == RoleEnum.EMPLOYEE_OF_PARLIAMENT.getRoleId();
%>
<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="<%=isHierarchyNeeded%>"/>
</jsp:include>

<script type="text/javascript">
    $(document).ready(() => {
        showFooter = false;
    });

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInTags,
            isMultiSelect: true,
            keepLastSelectState: true
        }]
    ]);

    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function init() {
        dateTimeInit($("#Language").val());
        select2SingleSelector('#status-select', '<%=Language%>');
        select2SingleSelector('#isPartial-select', '<%=Language%>');
        select2SingleSelector('#year-select', '<%=Language%>');

        criteriaArray = [{
            class: 'officeModalClass',
            title: '<%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_OFFICEUNITID, loginDTO)%>',
            show: false,
            specialCategory: 'officeModal'
        },
            {
                class: 'checkBoxClass',
                title: '<%=LM.getText(LC.EMPLOYEE_ACR_REPORT_WHERE_NOT_SUBMITTED, loginDTO)%>',
                show: false,
                specialCategory: 'checkBox'
            },
            {
                class: 'yearClass',
                title: '<%=LM.getText(LC.EMPLOYEE_ACR_REPORT_WHERE_YEAR, loginDTO)%>',
                show: false,
            },
            {
                class: 'employeeNameClass',
                title: '<%=isLangEng?"Employee Name": "কর্মকর্তা/কর্মচারীর নাম"%>',
                show: false
            },
            {
                class: 'partialClass',
                title: '<%=LM.getText(LC.EMPLOYEE_ACR_REPORT_WHERE_ISPARTIAL, loginDTO)%>',
                show: false
            },

            {
                class: 'acrFromClass',
                title: '<%=isLangEng?"ACR Date":"এসিআর তারিখ"%>',
                show: false,
                specialCategory: 'date',
                datejsIds: ['acrFromStart-js', 'acrFromEnd-js']
            },
            {
                class: 'submissionClass',
                title: '<%=isLangEng?"ACR Submission Date":"এসিআর জমা তারিখ"%>',
                show: false,
                specialCategory: 'date',
                datejsIds: ['submissionStartDate-js', 'submissionEndDate-js']
            }, {
                class: 'statusClass',
                title: '<%=LM.getText(LC.EMPLOYEE_ACR_REPORT_WHERE_STATUS, loginDTO)%>',
                show: false
            }
        ]

    }

    function PreprocessBeforeSubmiting() {
        $('#acrFromStart').val(getDateTimestampById('acrFromStart-js'));
        $('#acrFromEnd').val(getDateTimestampById('acrFromEnd-js'));
        $('#submissionStartDate').val(getDateTimestampById('submissionStartDate-js'));
        $('#submissionEndDate').val(getDateTimestampById('submissionEndDate-js'));


    }
</script>