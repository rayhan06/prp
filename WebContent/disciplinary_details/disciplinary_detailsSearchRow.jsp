<%@page import="employee_records.Employee_recordsRepository" %>
<%@page pageEncoding="UTF-8" %>

<%@page import="disciplinary_details.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.DISCIPLINARY_DETAILS_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_DISCIPLINARY_DETAILS;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Disciplinary_detailsDTO disciplinary_detailsDTO = (Disciplinary_detailsDTO) request.getAttribute("disciplinary_detailsDTO");
    CommonDTO commonDTO = disciplinary_detailsDTO;
    String servletName = "Disciplinary_detailsServlet";


    System.out.println("disciplinary_detailsDTO = " + disciplinary_detailsDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Disciplinary_detailsDAO disciplinary_detailsDAO = (Disciplinary_detailsDAO) request.getAttribute("disciplinary_detailsDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_disciplinaryLogId'>
    <%
        value = disciplinary_detailsDTO.disciplinaryLogId + "";
    %>

    <%=value%>


</td>


<td id='<%=i%>_employeeRecordsId'>

    <%=Employee_recordsRepository.getInstance().getEmployeeName(disciplinary_detailsDTO.employeeRecordsId, Language)%>

</td>


<td id='<%=i%>_suspensionDays'>
    <%
        value = disciplinary_detailsDTO.suspensionDays + "";
    %>

    <%=value%>


</td>


<td id='<%=i%>_complainActionType'>
    <%
        value = disciplinary_detailsDTO.complainActionType + "";
    %>
    <%
        value = CommonDAO.getName(Integer.parseInt(value), "complain_action", Language.equals("English") ? "name_en" : "name_bn", "id");
    %>

    <%=value%>


</td>


<td id='<%=i%>_complianOther'>
    <%
        value = disciplinary_detailsDTO.complianOther + "";
    %>

    <%=value%>


</td>


<td id='<%=i%>_remarks'>
    <%
        value = disciplinary_detailsDTO.remarks + "";
    %>

    <%=value%>


</td>


<td>
    <a href='Disciplinary_detailsServlet?actionType=view&ID=<%=disciplinary_detailsDTO.iD%>'>View</a>

</td>

<td id='<%=i%>_Edit'>

    <a href='Disciplinary_detailsServlet?actionType=getEditPage&ID=<%=disciplinary_detailsDTO.iD%>'><%=LM.getText(LC.DISCIPLINARY_DETAILS_SEARCH_DISCIPLINARY_DETAILS_EDIT_BUTTON, loginDTO)%>
    </a>

</td>


<td id='<%=i%>_checkbox'>
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=disciplinary_detailsDTO.iD%>'/></span>
    </div>
</td>
																						
											

