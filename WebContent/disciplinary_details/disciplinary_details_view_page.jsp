<%@ page import="disciplinary_details.DisciplinaryDetailsModelWithLogModel" %>
<%@ page import="java.util.List" %>
<input type="hidden" data-ajax-marker="true">
<table class="table table-bordered table-striped" style="font-size: 14px">
    <thead>
    <tr>
        <th><b><%= LM.getText(LC.DISCIPLINARY_LOG_ADD_COMPLAINTSOURCECAT, loginDTO) %></b></th>
        <th><b><%= LM.getText(LC.DISCIPLINARY_DETAILS_ADD_COMPLAINACTIONTYPE, loginDTO) %></b></th>
        <th><b><%= LM.getText(LC.DISCIPLINARY_LOG_ADD_INCIDENTTYPE, loginDTO) %></b></th>
        <th><b><%= LM.getText(LC.DISCIPLINARY_LOG_ADD_COMPLAINTSTATUSCAT, loginDTO) %></b></th>
        <th><b><%= LM.getText(LC.DISCIPLINARY_LOG_ADD_REPORTEDDATE, loginDTO) %></b></th>
        <th><b><%= LM.getText(LC.DISCIPLINARY_LOG_ADD_COMPLAINTRESOLVEDDATE, loginDTO) %></b></th>
        <th><b></b></th>
    </tr>
    </thead>
    <tbody>
    <%
        List<DisciplinaryDetailsModelWithLogModel> savedDisciplinaryList = (List<DisciplinaryDetailsModelWithLogModel>) request.getAttribute("savedDisciplinaryList");
        if(savedDisciplinaryList !=null && savedDisciplinaryList.size()>0){
            int disciplinaryDetailsIndex = 0;
            for(DisciplinaryDetailsModelWithLogModel disciplinaryDetailsItem: savedDisciplinaryList){
                ++disciplinaryDetailsIndex;
    %>
    <tr><%@include file="/disciplinary_details/disciplinary_details_view_item.jsp"%></tr>
    <%} }%>
    </tbody>
</table>