<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="disciplinary_details.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="complain_action.Complain_actionRepository" %>

<%
    Disciplinary_detailsDTO disciplinary_detailsDTO;
    disciplinary_detailsDTO = (Disciplinary_detailsDTO) request.getAttribute("disciplinary_detailsDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (disciplinary_detailsDTO == null) {
        disciplinary_detailsDTO = new Disciplinary_detailsDTO();
    }
    System.out.println("disciplinary_detailsDTO = " + disciplinary_detailsDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle;
    if (actionName.equals("edit")) {
        formTitle = LM.getText(LC.DISCIPLINARY_DETAILS_EDIT_DISCIPLINARY_DETAILS_EDIT_FORMNAME, loginDTO);
    } else {
        formTitle = LM.getText(LC.DISCIPLINARY_DETAILS_ADD_DISCIPLINARY_DETAILS_ADD_FORMNAME, loginDTO);
    }

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.DISCIPLINARY_LOG_EDIT_LANGUAGE, loginDTO);
    String Options;
    String empId = request.getParameter("empId");
    if (empId == null) {
        empId = String.valueOf(request.getAttribute("empId"));
    }
%>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%>
        </h3>
    </div>
    <div class="box-body">
        <form class="form-horizontal"
              action="Disciplinary_detailsServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="disciplinary_detail_form" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="form-body">

                <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                       value='<%=disciplinary_detailsDTO.iD%>' tag='pb_html'/>

                <div id='disciplinaryLogId_div'>
                    <label class="col-lg-3 control-label">
                        <%=(actionName.equals("edit")) ? (LM.getText(LC.DISCIPLINARY_DETAILS_EDIT_DISCIPLINARYLOGID, loginDTO)) : (LM.getText(LC.DISCIPLINARY_DETAILS_ADD_DISCIPLINARYLOGID, loginDTO))%>
                        <span class="required"> * </span>
                    </label>
                    <div class="form-group ">
                        <div class="col-lg-6">

                            <select class='form-control rounded' name='disciplinaryLogId'
                                    id='disciplinaryLogId_text<%=i%>' tag='pb_html'>
                                <%=Disciplinary_detailsDAO.getIncidentNumberOption(Language, disciplinary_detailsDTO.disciplinaryLogId)%>
                            </select>
                        </div>
                    </div>
                </div>

                <input type='hidden' class='form-control' name='employeeRecordsId' id='employeeRecordsId_hidden_<%=i%>'
                       value='<%=empId%>' tag='pb_html'/>


                <div id='suspensionDays_div'>
                    <label class="col-lg-3 control-label">
                        <%=(actionName.equals("edit")) ? (LM.getText(LC.DISCIPLINARY_DETAILS_EDIT_SUSPENSIONDAYS, loginDTO)) : (LM.getText(LC.DISCIPLINARY_DETAILS_ADD_SUSPENSIONDAYS, loginDTO))%>
                    </label>
                    <div class="form-group ">
                        <div class="col-lg-6">
                            <input type='text' class='form-control' name='suspensionDays' id='suspensionDays_text'
                                   value=<%=actionName.equals("edit")?("'" + disciplinary_detailsDTO.suspensionDays + "'"):("''")%>   tag='pb_html'/>
                        </div>
                    </div>
                </div>

                <div>
                    <label class="col-lg-3 control-label">
                        <%=(actionName.equals("edit")) ? (LM.getText(LC.DISCIPLINARY_DETAILS_EDIT_COMPLAINACTIONTYPE, loginDTO)) : (LM.getText(LC.DISCIPLINARY_DETAILS_ADD_COMPLAINACTIONTYPE, loginDTO))%>
                        <span class="required"> * </span>
                    </label>
                    <div class="form-group ">
                        <div class="col-lg-6">
                            <select class='form-control' name='complainActionType' id='complainActionType_select'
                                    tag='pb_html'>
                                <%=Complain_actionRepository.getInstance().buildOptions(Language, disciplinary_detailsDTO.complainActionType)%>
                            </select>
                        </div>
                    </div>
                </div>


                <div id='complianOther_div' style="display: none">
                    <label class="col-lg-3 control-label">
                        <%=(actionName.equals("edit")) ? (LM.getText(LC.DISCIPLINARY_DETAILS_EDIT_COMPLIANOTHER, loginDTO)) : (LM.getText(LC.DISCIPLINARY_DETAILS_ADD_COMPLIANOTHER, loginDTO))%>
                        <span class="required"> * </span>
                    </label>
                    <div class="form-group ">
                        <div class="col-lg-6">
                            <input type='text' class='form-control' name='complianOther' id='complianOther_text'
                                   value=<%=actionName.equals("edit")?("'" + disciplinary_detailsDTO.complianOther + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                        </div>
                    </div>
                </div>

                <div id='remarks_div'>
                    <label class="col-lg-3 control-label">
                        <%=(actionName.equals("edit")) ? (LM.getText(LC.DISCIPLINARY_DETAILS_EDIT_REMARKS, loginDTO)) : (LM.getText(LC.DISCIPLINARY_DETAILS_ADD_REMARKS, loginDTO))%>
                    </label>
                    <div class="form-group ">
                        <div class="col-lg-6">
                            <textarea class='form-control' name='remarks' id='remarks_text'
                                      tag='pb_html'><%=actionName.equals("edit") ? (disciplinary_detailsDTO.remarks) : ("")%></textarea>
                        </div>
                    </div>
                </div>

                <input type='hidden' class='form-control' name='insertionDate' id='insertionDate_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + disciplinary_detailsDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                <input type='hidden' class='form-control' name='insertedBy' id='insertedBy_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + disciplinary_detailsDTO.insertedBy + "'"):("'" + "" + "'")%> tag='pb_html'/>
                <input type='hidden' class='form-control' name='modifiedBy' id='modifiedBy_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + disciplinary_detailsDTO.modifiedBy + "'"):("'" + "" + "'")%> tag='pb_html'/>
                <input type='hidden' class='form-control' name='isDeleted' id='isDeleted_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + disciplinary_detailsDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
                <input type='hidden' class='form-control' name='lastModificationTime'
                       id='lastModificationTime_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + disciplinary_detailsDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                <div class="form-actions text-center">
                    <a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
                        <%
                            if (actionName.equals("edit")) {
                        %>
                        <%=LM.getText(LC.DISCIPLINARY_DETAILS_EDIT_DISCIPLINARY_DETAILS_CANCEL_BUTTON, loginDTO)%>
                        <%
                        } else {
                        %>
                        <%=LM.getText(LC.DISCIPLINARY_DETAILS_ADD_DISCIPLINARY_DETAILS_CANCEL_BUTTON, loginDTO)%>
                        <%
                            }

                        %>
                    </a>
                    <button class="btn btn-success" type="submit">
                        <%
                            if (actionName.equals("edit")) {
                        %>
                        <%=LM.getText(LC.DISCIPLINARY_DETAILS_EDIT_DISCIPLINARY_DETAILS_SUBMIT_BUTTON, loginDTO)%>
                        <%
                        } else {
                        %>
                        <%=LM.getText(LC.DISCIPLINARY_DETAILS_ADD_DISCIPLINARY_DETAILS_SUBMIT_BUTTON, loginDTO)%>
                        <%
                            }
                        %>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">
    const language = "<%=Language%>";
    const other = language === 'English' ? 'Other' : 'অন্যান্য'
    const please_select = language === 'English' ? 'Please Select' : 'অনুগ্রহ করে নির্বাচন করুন'

    $(document).ready(function () {
        showOrHideOtherAction();
        $('#complainActionType_select').change(function () {
            showOrHideOtherAction();
        });
        dateTimeInit("<%=Language%>");
        initValidation();
    });

    function showOrHideOtherAction() {
        if ($('#complainActionType_select option:selected').text() === other) {
            $('#complianOther_div').show();
        } else {
            $('#complianOther_div').hide();
        }
    }

    function initValidation() {
        $.validator.addMethod('complainActionTypeSelection', function (value, element) {
            return value != 0;
        });
        $("#disciplinary_detail_form").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                disciplinaryLogId: {
                    required: true,
                },
                complianOther: {
                    required: function () {
                        if ($('#complianOther_div').is(':visible')) {
                            return true
                        }
                        return false;
                    },
                },
                complainActionType: {
                    required: true,
                    complainActionTypeSelection: true
                }
            },

            messages: {
                disciplinaryLogId: "Please enter disciplinary id",
                complainActionType: "Please select a complain action",
                complianOther: "Please enter other complain",
            }
        });
    }

    function PreprocessBeforeSubmiting(row, validate) {
        $("#disciplinary_detail_form").validate();
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;
            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }
        }
        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Disciplinary_detailsServlet");
    }

    function init(row) {
        $("#disciplinaryLogId_text0").select2({
            dropdownAutoWidth: true
        });
        $("#complainActionType_select").select2({
            dropdownAutoWidth: true
        });
    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;

</script>






