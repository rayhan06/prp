<%@page import="complain_action.Complain_actionRepository" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="disciplinary_details.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%--<%@page import="disciplinary_details.Disciplinary_detailsRepository" %>--%>
<%@ page import="common.NameDTO" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.DISCIPLINARY_DETAILS_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    Disciplinary_detailsDAO disciplinary_detailsDAO = new Disciplinary_detailsDAO();
    Disciplinary_detailsDTO disciplinary_detailsDTO = disciplinary_detailsDAO.getById(id);
    NameDTO complainActionDTO = Complain_actionRepository.getInstance().getDTOByID(disciplinary_detailsDTO.complainActionType);

    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<div class="modal-content viewmodal">
    <div class="modal-header">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-9 col-sm-12">
                    <h5 class="modal-title">Disciplinary Details Details</h5>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-success app_register"
                               data-id="419637"> Register </a>
                        </div>
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-danger app_reject"
                               data-id="419637"> Reject </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <div class="modal-body container">

        <div class="row div_border office-div">

            <div class="col-md-12">
                <h3>Disciplinary Details</h3>
                <table class="table table-bordered table-striped">


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.DISCIPLINARY_DETAILS_EDIT_DISCIPLINARYLOGID, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = disciplinary_detailsDTO.disciplinaryLogId + "";
                            %>

                            <%=value%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.DISCIPLINARY_DETAILS_EDIT_EMPLOYEERECORDSID, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = disciplinary_detailsDTO.employeeRecordsId + "";
                            %>

                            <%=value%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.DISCIPLINARY_DETAILS_EDIT_SUSPENSIONDAYS, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = disciplinary_detailsDTO.suspensionDays + "";
                            %>

                            <%=value%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.DISCIPLINARY_DETAILS_EDIT_COMPLAINACTIONTYPE, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = disciplinary_detailsDTO.complainActionType + "";
                            %>
                            <%
                                value = CommonDAO.getName(Integer.parseInt(value), "complain_action", Language.equals("English") ? "name_en" : "name_bn", "id");
                            %>

                            <%=value%>


                        </td>

                    </tr>


                    <%if (complainActionDTO.nameEn.equals("Other")) {%>
                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.DISCIPLINARY_DETAILS_EDIT_COMPLIANOTHER, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = disciplinary_detailsDTO.complianOther + "";
                            %>

                            <%=value%>


                        </td>

                    </tr>
                    <%}%>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.DISCIPLINARY_DETAILS_EDIT_REMARKS, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = disciplinary_detailsDTO.remarks + "";
                            %>

                            <%=value%>


                        </td>

                    </tr>


                </table>
            </div>


        </div>


    </div>
</div>