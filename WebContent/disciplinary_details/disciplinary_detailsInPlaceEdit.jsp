<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="disciplinary_details.Disciplinary_detailsDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Disciplinary_detailsDTO disciplinary_detailsDTO = (Disciplinary_detailsDTO)request.getAttribute("disciplinary_detailsDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(disciplinary_detailsDTO == null)
{
	disciplinary_detailsDTO = new Disciplinary_detailsDTO();
	
}
System.out.println("disciplinary_detailsDTO = " + disciplinary_detailsDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.DISCIPLINARY_DETAILS_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=disciplinary_detailsDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_disciplinaryLogId'>")%>
			
	
	<div class="form-inline" id = 'disciplinaryLogId_div_<%=i%>'>
		<input type='text' class='form-control'  name='disciplinaryLogId' id = 'disciplinaryLogId_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + disciplinary_detailsDTO.disciplinaryLogId + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeRecordsId'>")%>
			
	
	<div class="form-inline" id = 'employeeRecordsId_div_<%=i%>'>
		<input type='text' class='form-control'  name='employeeRecordsId' id = 'employeeRecordsId_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + disciplinary_detailsDTO.employeeRecordsId + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_suspensionDays'>")%>
			
	
	<div class="form-inline" id = 'suspensionDays_div_<%=i%>'>
		<input type='text' class='form-control'  name='suspensionDays' id = 'suspensionDays_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + disciplinary_detailsDTO.suspensionDays + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_complainActionType'>")%>
			
	
	<div class="form-inline" id = 'complainActionType_div_<%=i%>'>
		<select class='form-control'  name='complainActionType' id = 'complainActionType_select_<%=i%>'   tag='pb_html'>
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "complain_action", "complainActionType_select_" + i, "form-control", "complainActionType", disciplinary_detailsDTO.complainActionType + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "complain_action", "complainActionType_select_" + i, "form-control", "complainActionType" );			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_complianOther'>")%>
			
	
	<div class="form-inline" id = 'complianOther_div_<%=i%>'>
		<input type='text' class='form-control'  name='complianOther' id = 'complianOther_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + disciplinary_detailsDTO.complianOther + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_remarks'>")%>
			
	
	<div class="form-inline" id = 'remarks_div_<%=i%>'>
		<input type='text' class='form-control'  name='remarks' id = 'remarks_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + disciplinary_detailsDTO.remarks + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=disciplinary_detailsDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value='<%=disciplinary_detailsDTO.insertedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=disciplinary_detailsDTO.modifiedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + disciplinary_detailsDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=disciplinary_detailsDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Disciplinary_detailsServlet?actionType=view&ID=<%=disciplinary_detailsDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Disciplinary_detailsServlet?actionType=view&modal=1&ID=<%=disciplinary_detailsDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	