
<td style="vertical-align: middle;"><%=isLanguageEnglish ? disciplinaryDetailsItem.logModel.complaintSourceEng : disciplinaryDetailsItem.logModel.complaintSourceBan%></td>
<td style="vertical-align: middle;"><%=isLanguageEnglish ? disciplinaryDetailsItem.detailsModel.complainEngText : disciplinaryDetailsItem.detailsModel.complainBangText%></td>
<td style="vertical-align: middle;"><%=isLanguageEnglish ? disciplinaryDetailsItem.logModel.incidentEngText : disciplinaryDetailsItem.logModel.incidentBangText%></td>
<td style="vertical-align: middle;"><%=isLanguageEnglish ? disciplinaryDetailsItem.logModel.complaintSourceEng : disciplinaryDetailsItem.logModel.complaintSourceBan%></td>
<td style="vertical-align: middle;"><%=isLanguageEnglish ? disciplinaryDetailsItem.logModel.reportedDateEng : disciplinaryDetailsItem.logModel.reportedDateBan%></td>
<td style="vertical-align: middle;"><%=isLanguageEnglish ? disciplinaryDetailsItem.logModel.complaintResolvedDateEng : disciplinaryDetailsItem.logModel.complaintResolvedDateBan%></td>
<td style="vertical-align: middle;text-align: center"><button class="btn-primary" title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_VIEW_DETAILS, loginDTO) %>"
                     onclick="location.href='Disciplinary_logServlet?actionType=view&ID=<%=disciplinaryDetailsItem.logModel.dto.iD%>&userId=<%=request.getParameter("userId")%>'"><i class="fa fa-eye"></i></button></td>