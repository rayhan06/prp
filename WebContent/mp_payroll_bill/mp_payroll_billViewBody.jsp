<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="mp_payroll_bill.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="allowance_employee_info.AllowanceEmployeeInfoDTO" %>
<%@ page import="allowance_employee_info.AllowanceEmployeeInfoRepository" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="election_constituency.Election_constituencyRepository" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="mp_payroll_allowance_configuration.Mp_payroll_allowance_configurationModel" %>
<%@ page import="mp_payroll_deduction_configuration.Mp_payroll_deduction_configurationModel" %>
<%@ page import="java.util.stream.Collectors" %>
<%
    String servletName = "Mp_payroll_billServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Mp_payroll_billDTO mp_payroll_billDTO = Mp_payroll_billDAO.getInstance().getDTOByID(id);
%>
<%@include file="../pb/viewInitializer.jsp" %>
<%
    Mp_payroll_billModel billModel = new Mp_payroll_billModel(mp_payroll_billDTO, Language);
    List<Mp_payroll_allowance_configurationModel> allowanceModels = new ArrayList<>();
    List<Mp_payroll_deduction_configurationModel> deductionModels = new ArrayList<>();
    if (billModel != null) {
        allowanceModels = billModel.allowanceDTOList.stream()
                .map(dto -> new Mp_payroll_allowance_configurationModel(dto, Language))
                .sorted(Comparator.comparing(dto -> dto.configurationId))
                .collect(Collectors.toList());
        deductionModels = billModel.deductionDTOList.stream()
                .map(dto -> new Mp_payroll_deduction_configurationModel(dto, Language))
                .sorted(Comparator.comparing(dto -> dto.configurationId))
                .collect(Collectors.toList());
    }
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.MP_PAYROLL_BILL_ADD_MP_PAYROLL_BILL_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-md-10 offset-md-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=getDataByLanguage(Language, "সংসদ সদস্য তথ্য", "MP Information")%>
                                        </h4>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=getDataByLanguage(Language, "সংসদ সদস্য", "Parliamnent Member")%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            AllowanceEmployeeInfoDTO employeeInfoDTO = AllowanceEmployeeInfoRepository.getInstance().getById(mp_payroll_billDTO.allowanceEmployeeInfoId);
                                        %>
                                        <b>
                                            <%=isLanguageEnglish ? employeeInfoDTO.nameEn : employeeInfoDTO.nameBn%>
                                        </b>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.MP_PAYROLL_BILL_ADD_ELECTIONDETAILSID, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=Election_detailsRepository.getInstance().getText(mp_payroll_billDTO.electionDetailsId, Language)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.MP_PAYROLL_BILL_ADD_ELECTIONCONSTITUENCYID, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=Election_constituencyRepository.getInstance().getText(mp_payroll_billDTO.electionConstituencyId, Language)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.MP_PAYROLL_BILL_ADD_MONTHYEAR, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=DateUtils.getMonthYear(mp_payroll_billDTO.monthYear, Language)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=getDataByLanguage(Language, "পারিতোষিক ও ভাতা (টাকা)", "Gratuity and Allowance (Taka)")%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=billModel.totalAllowance%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=getDataByLanguage(Language, "কর্তন ও আদায় (টাকা)", "Deduction and Collection (Taka)")%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=billModel.totalDeduction%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=getDataByLanguage(Language, "নীট দাবি (টাকা)", "Net Amount (Taka)")%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=billModel.netAmount%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-5">
                <div class="row">
                    <div class="col-md-6 form-group">
                        <div class="border">
                            <div class="text-center" style="margin-top: 10px">
                                <h5>
                                    <%=getDataByLanguage(
                                            Language,
                                            "পারিতোষিক ও ভাতা",
                                            "Gratuity and Allowance"
                                    )%>
                                </h5>
                            </div>
                            <div class="mt-3 table-responsive">
                                <table id="allowance-table" class="table">
                                    <thead>
                                    <tr>
                                        <th>
                                            <%=getDataByLanguage(
                                                    Language,
                                                    "অর্থনৈতিক গ্রুপ",
                                                    "Economic Group"
                                            )%>
                                        </th>
                                        <th>
                                            <%=getDataByLanguage(
                                                    Language,
                                                    "বিবরণ",
                                                    "Description"
                                            )%>
                                        </th>
                                        <th>
                                            <%=getDataByLanguage(
                                                    Language,
                                                    "টাকার পরিমাণ",
                                                    "Amount(Taka)"
                                            )%>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody class="main-tbody">
                                    <%
                                        for (Mp_payroll_allowance_configurationModel allowance_configurationModel : allowanceModels) {
                                    %>
                                    <tr>
                                        <td>
                                            <%=allowance_configurationModel.code%>
                                        </td>
                                        <td>
                                            <%=allowance_configurationModel.name%>
                                        </td>
                                        <td>
                                            <%=allowance_configurationModel.amount%>
                                        </td>
                                    </tr>
                                    <%
                                        }
                                    %>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="2">
                                            <b>
                                                <%=getDataByLanguage(Language, "সর্বমোট ভাতা", "Total Allowance")%>
                                            </b>
                                        </td>
                                        <td>
                                            <%=billModel.totalAllowance%>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        <div class="border">
                            <div class="text-center" style="margin-top: 10px">
                                <h5>
                                    <%=getDataByLanguage(
                                            Language,
                                            "কর্তন ও আদায়",
                                            "Deduction and Collection"
                                    )%>
                                </h5>
                            </div>
                            <div class="mt-3 table-responsive">
                                <table id="deduction-table" class="table">
                                    <thead>
                                    <tr>
                                        <th>
                                            <%=getDataByLanguage(
                                                    Language,
                                                    "অর্থনৈতিক গ্রুপ",
                                                    "Economic Group"
                                            )%>
                                        </th>
                                        <th>
                                            <%=getDataByLanguage(
                                                    Language,
                                                    "বিবরণ",
                                                    "Description"
                                            )%>
                                        </th>
                                        <th>
                                            <%=getDataByLanguage(
                                                    Language,
                                                    "Amount(Taka)",
                                                    "টাকার পরিমাণ"
                                            )%>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody class="main-tbody">
                                    <%
                                        for (Mp_payroll_deduction_configurationModel deduction_configurationModel : deductionModels) {
                                    %>
                                    <tr>
                                        <td>
                                            <%=deduction_configurationModel.code%>
                                        </td>
                                        <td>
                                            <%=deduction_configurationModel.name%>
                                        </td>
                                        <td>
                                            <%=deduction_configurationModel.amount%>
                                        </td>
                                    </tr>
                                    <%
                                        }
                                    %>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="2">
                                            <b>
                                                <%=getDataByLanguage(Language, "সর্বমোট কর্তন", "Total Deduction")%>
                                            </b>
                                        </td>
                                        <td>
                                            <%=billModel.totalDeduction%>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions text-right mb-2 mt-3">
                <button id="cancel-btn" class="btn-border-radius-10 shadow text-white border-12 submit-btn"
                        type="button"
                        onclick="location.href='<%=servletName%>?actionType=generateBill&ID=<%=mp_payroll_billDTO.iD%>'">
                    <%=LM.getText(LC.MEDICAL_ALLOWANCE_SEARCH_BILL_GENERATE, loginDTO)%>
                </button>
            </div>
        </div>
    </div>
</div>