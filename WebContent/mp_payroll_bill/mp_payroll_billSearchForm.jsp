<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="mp_payroll_bill.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="allowance_employee_info.AllowanceEmployeeInfoDTO" %>
<%@ page import="allowance_employee_info.AllowanceEmployeeInfoRepository" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="election_constituency.Election_constituencyRepository" %>


<%
    String navigator2 = "navMP_PAYROLL_BILL";
    String servletName = "Mp_payroll_billServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.MP_PAYROLL_BILL_ADD_MONTHYEAR, loginDTO)%>
            </th>
            <th><%=getDataByLanguage(Language, "সংসদ সদস্য", "Parliament Member")%>
            </th>
            <th><%=LM.getText(LC.MP_PAYROLL_BILL_ADD_ELECTIONDETAILSID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.MP_PAYROLL_BILL_ADD_ELECTIONCONSTITUENCYID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.MEDICAL_ALLOWANCE_SEARCH_BILL_GENERATE, loginDTO)%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<Mp_payroll_billDTO>) rn2.list;
            try {
                if (data != null) {
                    int size = data.size();
                    for (int i = 0; i < size; i++) {
                        Mp_payroll_billDTO mp_payroll_billDTO = (Mp_payroll_billDTO) data.get(i);
        %>
        <tr>
            <td>
                <%=DateUtils.getMonthYear(mp_payroll_billDTO.monthYear, Language)%>
            </td>
            <td>
                <%
                    AllowanceEmployeeInfoDTO employeeInfoDTO =
                            AllowanceEmployeeInfoRepository.getInstance()
                                    .getById(mp_payroll_billDTO.allowanceEmployeeInfoId);

                %>
                <%=getDataByLanguage(Language, employeeInfoDTO.nameBn, employeeInfoDTO.nameEn)%>
            </td>
            <td>
                <%=Election_detailsRepository.getInstance().getText(mp_payroll_billDTO.electionDetailsId, Language)%>
            </td>

            <td>
                <%=Election_constituencyRepository.getInstance().getText(mp_payroll_billDTO.electionConstituencyId, Language)%>
            </td>
            <%CommonDTO commonDTO = mp_payroll_billDTO; %>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=view&ID=<%=commonDTO.iD%>'"
                >
                    <i class="fa fa-eye"></i>
                </button>
            </td>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ea07d4;"
                        onclick="location.href='<%=servletName%>?actionType=generateBill&ID=<%=mp_payroll_billDTO.iD%>'"
                >
                    <%=LM.getText(LC.MEDICAL_ALLOWANCE_SEARCH_BILL_GENERATE, loginDTO)%>
                </button>
            </td>

        </tr>
        <%
                    }
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			