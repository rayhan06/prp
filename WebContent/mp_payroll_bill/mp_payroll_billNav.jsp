<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="election_constituency.Election_constituencyRepository" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    System.out.println("Inside nav.jsp");
    String url = "Mp_payroll_billServlet?actionType=search";
    String toolbarOpen = request.getParameter("toolbarOpen") == null ? "0" : request.getParameter("toolbarOpen");
    int year = Calendar.getInstance().get(Calendar.YEAR);
    String electionId = request.getParameter("electionDetailsId");
    String electionConstituencyId = request.getParameter("electionConstituencyId");
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0,false)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
        <%--        <div class="kt-portlet__head-toolbar">--%>
        <%--            <div class="kt-portlet__head-group">--%>
        <%--                <div class="tooltip tooltip-portlet tooltip bs-tooltip-top" role="tooltip" id="tooltip_p6zf7aqcpv"--%>
        <%--                     aria-hidden="true" x-placement="top"--%>
        <%--                     style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">--%>
        <%--                    <div class="tooltip-arrow arrow" style="left: 34px;"></div>--%>
        <%--                    <div class="tooltip-inner">Collapse</div>--%>
        <%--                </div>--%>
        <%--            </div>--%>
        <%--        </div>--%>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label ">
                            <%=LM.getText(LC.MP_REPORT_WHERE_ELECTIONDETAILSID, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control rounded' name='electionDetailsId' id='electionDetailsId'
                                    tag='pb_html' onchange="changeElection()">
                                <%=Election_detailsRepository.getInstance().buildOptions(Language, (electionId == null || electionId.length() == 0) ? null : Long.parseLong(electionId))%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label ">
                            <%=LM.getText(LC.MP_PAYROLL_BILL_ADD_ELECTIONCONSTITUENCYID, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control rounded' name='electionConstituencyId'
                                    id='electionConstituencyId' tag='pb_html'>
                                <%
                                    if (electionId != null && electionId.length() != 0) {
                                %>
                                <%=Election_constituencyRepository.getInstance().buildOptions(Language, (electionConstituencyId == null || electionConstituencyId.length() == 0) ? null : Long.parseLong(electionConstituencyId), Long.parseLong(electionId))%>
                                <%
                                    }
                                %>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label ">
                            <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_MONTHYEAR, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="monthYear_js"/>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                <jsp:param name="HIDE_DAY" value="true"/>
                                <jsp:param name="END_YEAR" value="<%=year+5%>"/>
                            </jsp:include>
                            <input type='hidden' id='monthYear' value=''>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0,true)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <div class="search-loader-container-circle ">
            <div class="search-loader-circle"></div>
        </div>
    </div>
</template>

<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>
<script type="text/javascript">
    let language = '<%=Language%>';
    const electionDetailsIdSelector = $('#electionDetailsId');
    const electionConstituencyIdSelector = $('#electionConstituencyId');
    const monthYearSelector = $('#monthYear');
    const anyFieldSelector = $('#anyfield');
    let previousConstituencyVal = null;


    function resetInputs() {
        electionDetailsIdSelector.select2("val", '-1');
        electionConstituencyIdSelector.select2("val", '-1');
        resetDateById('monthYear_js');
        previousConstituencyVal = null;
    }

    window.addEventListener('popstate', e => {
        if (e.state) {
            let params = e.state;
            dosubmit(params, false);

            let arr = params.split('&');
            arr.forEach(e => {
                let item = e.split('=');
                if (item.length === 2) {
                    switch (item[0]) {
                        case 'electionDetailsId':
                            electionDetailsIdSelector.val(item[1]).trigger('change');
                            break;
                        case 'electionConstituencyId':
                            previousConstituencyVal = item[1];
                            electionConstituencyIdSelector.val(item[1]).trigger('change');
                            break;
                        case 'monthYear':
                            setDateByTimestampAndId('monthYear_js', item[1]);
                            break;
                        case 'AnyField':
                            anyFieldSelector.val(item[1]);
                            break;
                        default:
                            setPaginationFields(item);
                    }
                }
            });
        } else {
            dosubmit(null, false);
            resetInputs();
            resetPaginationFields();
        }
    });


    $(document).ready(() => {
        readyInit('Mp_payroll_billServlet');
        if ('<%=toolbarOpen%>' == '1') {
            document.querySelector('.kt-portlet__body').style.display = 'block';
            document.querySelector('#kt_portlet_tools_1').classList.remove('kt-portlet--collapse')
        }
        select2SingleSelector('#electionDetailsId', '<%=Language%>');
        select2SingleSelector('#electionConstituencyId', '<%=Language%>');
    });

    function dosubmit(params, pushState = true) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (pushState) {
                    history.pushState(params, '', 'Mp_payroll_billServlet?actionType=search&' + params);
                }
                setTimeout(() => {
                    document.getElementById('tableForm').innerHTML = this.responseText ;
                    setPageNo();
                    searchChanged = 0;
                }, 500);
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Mp_payroll_billServlet?actionType=search&ajax=true&isPermanentTable=<%=isPermanentTable%>&" + params, false);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number, pushState = true) {
        let params = 'search=true';
        if (document.getElementById('anyfield').value) {
            params += '&AnyField=' + document.getElementById('anyfield').value
        }
        params += '&electionDetailsId=' + document.getElementById('electionDetailsId').value;
        params += '&electionConstituencyId=' + document.getElementById('electionConstituencyId').value;
        $('#monthYear').val(getDateTimestampById('monthYear_js'));
        const errorEn = 'Enter all Date Fields!';
        const errorBn = 'সকল তারিখের ঘর দিন!';
        let errorText = language.localeCompare('English') == 0 ? errorEn : errorBn;
        const dateId = 'monthYear_js';
        const month = parseInt($('#' + dateId).find('.monthSelection').prop('value'));
        const year = parseInt($('#' + dateId).find('.yearSelection').prop('value'));
        if ((month == 0 && year != 0) || ((month != 0 && year == 0))) {
            $('#error' + dateId).text(errorText);
            $('#monthYear').val('<%=SessionConstants.MIN_DATE%>');
        }
        params += '&monthYear=' + $('#monthYear').val();
        if (document.getElementsByClassName("kt-portlet__body")[0].style['display'] != 'none') {
            params += '&toolbarOpen=1';
        }
        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params, pushState);

    }

    function changeElection() {
        // console.log(document.getElementById('electionConstituencyId').value)
        document.getElementById("electionConstituencyId").innerHTML = '';
        electionDetailsId = $('#electionDetailsId').val();
        if (!electionDetailsId) {
            return;
        }
        let url = "Election_constituencyServlet?actionType=buildElectionConstituency&election_id=" + electionDetailsId + "&language=" + language;
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                // console.log(fetchedData);
                document.getElementById("electionConstituencyId").innerHTML = fetchedData;
                if (previousConstituencyVal != null) {
                    electionConstituencyInput.val(previousConstituencyVal).trigger('change');
                }
                previousConstituencyVal = null;
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

</script>

