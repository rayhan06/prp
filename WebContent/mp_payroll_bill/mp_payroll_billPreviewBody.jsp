<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.*" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="util.*" %>
<%@ page import="static util.StringUtils.convertToBanNumber" %>
<%@ page import="static util.StringUtils.convertToBanNumber" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="mp_payroll_bill.Mp_payroll_billDTO" %>
<%@ page import="mp_payroll_bill.Mp_payroll_billModel" %>
<%@ page import="mp_payroll_allowance_configuration.Mp_payroll_allowance_configurationModel" %>
<%@ page import="mp_payroll_deduction_configuration.Mp_payroll_deduction_configurationModel" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="allowance_employee_info.AllowanceEmployeeInfoRepository" %>
<%@ page import="allowance_employee_info.AllowanceEmployeeInfoDTO" %>
<%@ page import="bank_name.Bank_nameRepository" %>
<%@ page pageEncoding="UTF-8" %>

<%
    String servletName = "Mp_payroll_billServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Mp_payroll_billDTO mp_payroll_billDTO = (Mp_payroll_billDTO) request.getAttribute("payrollBillDTO");
    if (mp_payroll_billDTO == null) {
        mp_payroll_billDTO = new Mp_payroll_billDTO();
    }
    Mp_payroll_billModel billModel = new Mp_payroll_billModel(mp_payroll_billDTO, "BANGLA");
    List<Mp_payroll_allowance_configurationModel> allowanceModels = new ArrayList<>();
    List<Mp_payroll_deduction_configurationModel> deductionModels = new ArrayList<>();
    if (billModel != null) {
        allowanceModels = billModel.allowanceDTOList.stream()
                .map(dto -> new Mp_payroll_allowance_configurationModel(dto, "BANGLA"))
                .sorted(Comparator.comparing(dto -> dto.configurationId))
                .collect(Collectors.toList());
        deductionModels = billModel.deductionDTOList.stream()
                .map(dto -> new Mp_payroll_deduction_configurationModel(dto, "BANGLA"))
                .sorted(Comparator.comparing(dto -> dto.configurationId))
                .collect(Collectors.toList());
    }
    AllowanceEmployeeInfoDTO employeeInfoDTO = AllowanceEmployeeInfoRepository.getInstance().getById(mp_payroll_billDTO.allowanceEmployeeInfoId);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getLanguage(loginDTO);
    String pdfFileName = "Payroll Bill of Parliament Member "
            + DateUtils.getMonthYear(mp_payroll_billDTO.monthYear, "English", " ") + " "
            + employeeInfoDTO.nameEn.replace(".", " ");
    String bankName = "";
    if (employeeInfoDTO.bankNameId.length() > 0) {
        bankName = Bank_nameRepository.getInstance().getText("BANGLA", Long.parseLong(employeeInfoDTO.bankNameId));
    }
%>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    .fix-fill {
        overflow: hidden;
        white-space: nowrap;
    }

    #to-print-div * {
        font-size: 12px;
    }

    #to-print-div h1 {
        font-size: 16px;
        font-weight: bold;
    }

    #to-print-div h2 {
        font-size: 14px;
        font-weight: bold;
    }

    #to-print-div h3 {
        font-size: 13px;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .5in;
        background: white;
        margin-bottom: 10px;
    }

    .foot-note {
        font-size: 11px !important;
    }

    .blank-to-fill {
        display: inline-block;
        border-bottom: 1px dotted black;
        width: inherit;
    }

    .table-bordered > :not(caption) > * {
        border-color: black;
    }

    .table-bordered > :not(caption) > * > * {
        border-color: black;
    }

    .table-bordered td,
    .table-bordered th {
        padding: 5px;
    }

    .align-top {
        vertical-align: top;
    }

    th {
        text-align: center;
    }
</style>
<div class="kt-content p-0" id="kt_content">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title prp-page-title">
                        <%=UtilCharacter.getDataByLanguage(Language, "সংসদ সদস্যের পেরোল বিল", "Payroll Bill of Parliament Member")%>
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body" id="bill-div">

                <div class="ml-auto m-5">
                    <button type="button" class="btn" id='download-pdf'
                            onclick="downloadTemplateAsPdf('to-print-div', '<%=pdfFileName%>');">
                        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                </div>

                <div style="margin: auto;">
                    <%--Reference Documents from Parliament\Kawsar, AO, Fin 01550445791\O.T Bil Forwarding  All  February  21--%>
                    <div class="container" id="to-print-div">
                        <section class="page shadow" data-size="A4">
                            <div class="text-center">
                                <h1>বাংলাদেশ জাতীয় সংসদ</h1>
                                <h2 style="padding-top: 10px">
                                    মাননীয় সংসদ সদস্যের মাসিক পারিতোষিক ও নির্দিষ্ট ভাতার বিবরণী
                                </h2>
                            </div>
                            <div class="row offset-lg-9 offset-sm-6" style="padding-top: 20px">
                                কোড নং- &nbsp;
                                <div style="border: 1px solid black">
                                    ১০২০২০৬১০০০১৫
                                </div>
                            </div>
                            <div>
                                <div class="mt-4 offset-1">
                                    <div class="row">
                                        <div class="col-8 fix-fill">
                                            <b>নাম:</b>
                                            <div class="blank-to-fill text-center">
                                                <%=billModel.name%>
                                            </div>
                                        </div>
                                        <div class="col-4 fix-fill">
                                            পদমর্যাদা : সংসদ সদস্য
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-8 fix-fill">
                                            <b>নির্বাচনী এলাকা নংঃ</b>
                                            <div class="blank-to-fill text-center">
                                                <%=billModel.electionConstituencyName%>
                                            </div>
                                        </div>
                                        <div class="col-4 fix-fill">
                                            <b>ব্যাংকের নাম</b>
                                            <div class="blank-to-fill">
                                                <%=bankName%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-6 fix-fill">
                                            <b>মাসঃ</b>
                                            <div class="blank-to-fill text-center">
                                                <%=DateUtils.getMonthName(mp_payroll_billDTO.monthYear, "BANGLA")%>
                                            </div>
                                        </div>
                                        <div class="col-4 fix-fill">
                                            সাল
                                            <div class="blank-to-fill text-center">
                                                <%=DateUtils.getYearName(mp_payroll_billDTO.monthYear, "BANGLA")%>
                                            </div>
                                        </div>
                                        <div class="col-2 fix-fill"></div>
                                    </div>
                                    <table class="table-bordered mt-3 w-100">
                                        <thead>
                                        <tr style="height: 30px;background-color:lightgrey">
                                            <th width="10%">অর্থনৈতিক কোড</th>
                                            <th width="25%">প্রাপ্তি</th>
                                            <th width="10%">টাকা</th>
                                            <th width="12%">অর্থনৈতিক কোড</th>
                                            <th width="25%">কর্তন</th>
                                            <th width="12%">টাকা</th>
                                            <th width="6%">পয়সা</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <%
                                            int allowanceSize = allowanceModels.size();
                                            int deductionSize = deductionModels.size();
                                            int index1 = 0, index2 = 0;
                                            while (index1 < allowanceSize || index2 < deductionSize) {
                                        %>
                                        <tr>
                                            <td>
                                                <%
                                                    if (index1 < allowanceSize) {
                                                %>
                                                <%=allowanceModels.get(index1).code%>
                                                <%
                                                    }
                                                %>
                                            </td>
                                            <td>
                                                <%
                                                    if (index1 < allowanceSize) {
                                                %>
                                                <%=allowanceModels.get(index1).name%>
                                                <%
                                                    }
                                                %>
                                            </td>
                                            <td>
                                                <%
                                                    if (index1 < allowanceSize) {
                                                %>
                                                <%=allowanceModels.get(index1).amount%>/-
                                                <%
                                                    }
                                                %>
                                            </td>
                                            <td>
                                                <%
                                                    if (index2 < deductionSize) {
                                                %>
                                                <%=deductionModels.get(index2).code%>
                                                <%
                                                    }
                                                %>
                                            </td>
                                            <td>
                                                <%
                                                    if (index2 < deductionSize) {
                                                %>
                                                <%=deductionModels.get(index2).name%>
                                                <%
                                                    }
                                                %>
                                            </td>
                                            <td>
                                                <%
                                                    if (index2 < deductionSize) {
                                                %>
                                                <%=deductionModels.get(index2).amount%>/-
                                                <%
                                                    }
                                                %>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <%
                                                index1++;
                                                index2++;
                                            }
                                        %>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td></td>
                                            <td class="text-right"><b>মোট প্রাপ্তিঃ</b></td>
                                            <td><%=billModel.totalAllowance%>/-</td>
                                            <td></td>
                                            <td class="text-right"><b>মোট কর্তনঃ</b></td>
                                            <td><%=billModel.totalDeduction%>/-</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="text-right"><b>ব্যাংকে নীট প্রেরণ</b></td>
                                            <td><%=billModel.netAmount%>/-</td>
                                            <td></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                    <div class="text-center" style="margin-top: 30px">
                                        <h2 style="display: inline-block; border-bottom: 1px solid black">
                                            সদয় জ্ঞাতব্য
                                        </h2>
                                    </div>
                                    <div class="row-mt-3">
                                        <div class="row" style="padding-top: 10px">
                                            &nbsp;&nbsp;&nbsp;১। &nbsp;&nbsp;&nbsp;&nbsp;মনোনীত ব্যাংক পরিবর্তনের
                                            প্রয়োজন হইলে তাহা অর্থ শাখা-২ অথবা হিসাব রক্ষণ অফিসে সংশ্লিষ্ট মাসের ১৫
                                            তারিখের মধ্যে
                                        </div>
                                        <div class="row" style="padding-left: 5px">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;অবহিত করতে হইবে।
                                            ১৫ তারিখের পর গৃহীত তথ্যের উপর পরবর্তী মাসে কার্যকরী ব্যবস্থা গৃহীত হইবে।
                                        </div>
                                        <div class="row" style="padding-top: 5px">
                                            &nbsp;&nbsp;&nbsp;২। &nbsp;&nbsp;&nbsp;&nbsp;কোন কারণে প্রাপ্য পারিতোষিক ও
                                            ভাতা হইতে কম/বেশী পাইলে অর্থ শাখা-২ অথবা হিসাবরক্ষণ অফিসকে জ্ঞাত করিতে হইবে।
                                        </div>
                                        <div class="row" style="padding-top: 5px">
                                            &nbsp;&nbsp;&nbsp;৩। &nbsp;&nbsp;&nbsp;&nbsp;সর্বপ্রকার যোগাযোগের সময়
                                            অনুগ্রহপূর্বক নির্বাচনী এলাকার নম্বর উল্লেখ করিবেন।
                                        </div>
                                    </div>
                                    <div class="row mt-5" style="padding-top: 60px">
                                        <div class="col-3 fix-fill">
                                            &nbsp;&nbsp;অডিটর
                                        </div>
                                        <div class="col-3 fix-fill">
                                            &nbsp;&nbsp;সুপার
                                        </div>
                                        <div class="col-6 fix-fill text-center">
                                            নিরীক্ষা ও হিসাবরক্ষণ কর্মকর্তা
                                        </div>
                                        <div class="col-6 fix-fill"></div>
                                        <div class="col-6 fix-fill text-center">
                                            সিএও/বাংলাদেশ জাতীয় সংসদ সচিবালয়, ঢাকা
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
</div>
<script>
    function downloadTemplateAsPdf(divId, fileName) {
        let content = document.getElementById(divId);
        const opt = {
            margin: 0.5,
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4'}
        };
        html2pdf().from(content).set(opt).save();
    }
</script>