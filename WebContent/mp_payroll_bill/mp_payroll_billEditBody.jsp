<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="mp_payroll_bill.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@include file="../pb/addInitializer2.jsp" %>

<%
    actionName = request.getParameter("actionType").equals("edit") ? "edit" : "add";
    String formTitle = LM.getText(LC.MP_PAYROLL_BILL_ADD_MP_PAYROLL_BILL_ADD_FORMNAME, loginDTO);
    String context = request.getContextPath() + "/";
    long currentElectionId = Election_detailsRepository.getInstance().getRunningElectionDetailsDTO().iD;
%>

<style>
    .template-row {
        display: none;
    }

    .custom-modal {
        width: 300px !important; /* Use !important in case you want to override another val*/
        height: 200px;
        position: absolute; /*You can use fixed too*/
        top: 50%;
        left: 50%;
        margin-top: 30px;
        margin-left: 20px;
        display: none; /* You want it to be hidden, and show it using jquery*/
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal" id="bill-form" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12 row mt-2">
                        <div class="col-md-6 form-group">
                            <label class="h5" for="electionDetailsId">
                                <%=LM.getText(LC.EMPLOYEE_PARLIAMENT_NUMBER, loginDTO)%>
                            </label>
                            <select id="electionDetailsId" name='electionDetailsId'
                                    class='form-control rounded shadow-sm'
                                    onchange="electionDetailsChanged(this);">
                                <%=Election_detailsRepository.getInstance().buildOptions(Language, currentElectionId)%>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label class="h5" for="electionDetailsId">
                                <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_MONTHYEAR, loginDTO)%>
                            </label>
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="monthYear_js"/>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                <jsp:param name="HIDE_DAY" value="true"/>
                            </jsp:include>
                            <input type='hidden' name='monthYear' id='monthYear' value=''>
                        </div>
                    </div>
                </div>
                <div class="mt-5">
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <div class="border">
                                <div class="text-center" style="margin-top: 10px">
                                    <h5>
                                        <%=getDataByLanguage(
                                                Language,
                                                "পারিতোষিক ও ভাতা",
                                                "Gratuity and Allowance"
                                        )%>
                                    </h5>
                                </div>
                                <div class="mt-3 table-responsive">
                                    <table id="allowance-table" class="table table-bordered table-striped text-nowrap">
                                        <thead>
                                        <tr>
                                            <th>
                                                <%=getDataByLanguage(
                                                        Language,
                                                        "অর্থনৈতিক গ্রুপ",
                                                        "Economic Group"
                                                )%>
                                            </th>
                                            <th>
                                                <%=getDataByLanguage(
                                                        Language,
                                                        "বিবরণ",
                                                        "Description"
                                                )%>
                                            </th>
                                            <th>
                                                <%=getDataByLanguage(
                                                        Language,
                                                        "টাকার পরিমাণ",
                                                        "Amount(Taka)"
                                                )%>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody class="main-tbody">
                                        </tbody>
                                        <tr class="template-row">
                                            <td class="row-data-code"></td>
                                            <td class="row-data-name"></td>
                                            <td class="row-data-amount"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <div class="border">
                                <div class="text-center" style="margin-top: 10px">
                                    <h5>
                                        <%=getDataByLanguage(
                                                Language,
                                                "কর্তন ও আদায়",
                                                "Deduction and Collection"
                                        )%>
                                    </h5>
                                </div>
                                <div class="mt-3 table-responsive">
                                    <table id="deduction-table" class="table table-bordered table-striped text-nowrap">
                                        <thead>
                                        <tr>
                                            <th>
                                                <%=getDataByLanguage(
                                                        Language,
                                                        "অর্থনৈতিক গ্রুপ",
                                                        "Economic Group"
                                                )%>
                                            </th>
                                            <th>
                                                <%=getDataByLanguage(
                                                        Language,
                                                        "বিবরণ",
                                                        "Description"
                                                )%>
                                            </th>
                                            <th>
                                                <%=getDataByLanguage(
                                                        Language,
                                                        "টাকার পরিমাণ",
                                                        "Amount(Taka)"
                                                )%>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody class="main-tbody">
                                        </tbody>
                                        <tr class="template-row">
                                            <td class="row-data-code"></td>
                                            <td class="row-data-name"></td>
                                            <td class="row-data-amount"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mt-5 table-responsive">
                    <table id="bill-table" class="table table-bordered table-striped text-nowrap">
                        <thead>
                        <tr>
                            <th>
                                <%=getDataByLanguage(
                                        Language,
                                        "সংসদ সদস্য",
                                        "Parliament Member"
                                )%>
                            </th>
                            <th>
                                <%=getDataByLanguage(
                                        Language,
                                        "নির্বাচনী এলাকা",
                                        "Election Constituency"
                                )%>
                            </th>
                            <th>
                                <%=getDataByLanguage(
                                        Language,
                                        "পারিতোষিক ও ভাতা(টাকা)",
                                        "Gratuity and Allowance(Taka)"
                                )%>
                            </th>
                            <th>
                                <%=getDataByLanguage(
                                        Language,
                                        "কর্তন ও আদায় (টাকা)",
                                        "Deduction and Collection(Taka)"
                                )%>
                            </th>
                            <th>
                                <%=getDataByLanguage(
                                        Language,
                                        "নীট দাবী (টাকা)",
                                        "Net Amount(Taka)"
                                )%>
                            </th>
                        </tr>
                        </thead>
                        <tbody class="main-tbody">
                        </tbody>

                        <%--don't put these tr inside tbody--%>
                        <tr class="loading-gif" style="display: none;">
                            <td class="text-center" colspan="100%">
                                <div class="search-loader-container-circle ">
                                    <div class="search-loader-circle"></div>
                                </div>
<%--                                <img alt="" class="loading"--%>
<%--                                     src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">--%>
<%--                                <span><%=getDataByLanguage(Language, "লোড হচ্ছে...", "Loading...")%></span>--%>
                            </td>
                        </tr>

                        <tr class="template-row">
                            <input type="hidden" name="employeeRecordsId" value="-1">
                            <input type="hidden" name="mpPayrollBillId" value="-1">
                            <td class="row-data-name"></td>
                            <td class="row-data-electionConstituencyName"></td>
                            <td class="row-data-totalAllowance"></td>
                            <td class="row-data-totalDeduction"></td>
                            <td class="row-data-netAmount"></td>
                        </tr>
                    </table>
                </div>
                <div class="" style="position: relative">
                    <div class="custom-modal">
                        <div class="spinner-grow text-primary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-grow text-secondary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-grow text-success" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-grow text-danger" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-grow text-warning" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-grow text-info" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-grow text-light" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-grow text-dark" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 mt-3 text-right">
                        <button id="submit-btn"
                                class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                                type="button" onclick="submitForm()">
                            <%=getDataByLanguage(Language, "সাবমিট", "Submit")%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<%@include file="../common/table-sum-utils.jsp" %>

<script type="text/javascript">
    let actionName = 'add';

    function showOrHideLoadingGif(tableId, toShow) {
        const loadingGif = $('#' + tableId + ' tr.loading-gif');
        if (toShow) {
            document.querySelector('#' + tableId + ' tbody.main-tbody').innerHTML = '';
            document.querySelectorAll('#' + tableId + ' tfoot').forEach(tfoot => tfoot.remove());
            clearOtherTables();
            loadingGif.show();
        } else loadingGif.hide();
    }

    function clearMonthYear() {
        resetDateById('monthYear_js');
    }

    function noDataFound() {
        const tableBody = document.querySelector('#bill-table tbody.main-tbody');
        const emptyTableText = '<%=getDataByLanguage(Language, "কোন তথ্য পাওয়া যায়নি", "No data found")%>';
        tableBody.innerHTML = '<tr class="text-center"><td colspan="100%">'
            + emptyTableText + '</td></tr>';

        document.querySelectorAll('#bill-table tfoot')
            .forEach(tfoot => tfoot.remove());
        clearOtherTables();
    }

    function clearTable() {
        const tableBody = document.querySelector('#bill-table tbody.main-tbody');
        tableBody.innerHTML = '';
        document.querySelectorAll('#bill-table tfoot')
            .forEach(tfoot => tfoot.remove());
        clearOtherTables();
    }

    function clearOtherTables() {
        const allowanceBody = document.querySelector('#allowance-table tbody.main-tbody');
        const deductionBody = document.querySelector('#deduction-table tbody.main-tbody');
        allowanceBody.innerHTML = '';
        deductionBody.innerHTML = '';
        document.querySelectorAll('#allowance-table tfoot')
            .forEach(tfoot => tfoot.remove());
        document.querySelectorAll('#deduction-table tfoot')
            .forEach(tfoot => tfoot.remove());
    }

    function clearNextLevels(startIndex) {
        const toClearFunctions = [
            clearMonthYear, clearTable
        ];
        for (let i = startIndex; i < toClearFunctions.length; i++) {
            toClearFunctions[i]();
        }
    }

    async function electionDetailsChanged() {
        clearNextLevels(0);
        const electionDetailsId = document.getElementById('electionDetailsId').value;
        if (electionDetailsId !== "") {
            const url = 'Mp_payroll_billServlet?actionType=ajax_getParliamentDateRange'
                + '&electionDetailsId=' + electionDetailsId;
            const response = await fetch(url);
            const {startDate, endDate} = await response.json();
            console.log("startDate", startDate);
            console.log("endDate", endDate);
            setMinDateByTimestampAndId("monthYear_js",startDate);
            setMaxDateByTimestampAndId("monthYear_js",endDate);
        }
    }

    async function monthYearChanged(monthYear) {
        const electionDetailsId = document.getElementById('electionDetailsId').value;
        if (electionDetailsId === '') return;

        showOrHideLoadingGif('bill-table', true);

        const url = 'Mp_payroll_billServlet?actionType=ajax_getMpPayrollData'
            + '&monthYear=' + monthYear
            + '&electionDetailsId=' + electionDetailsId;
        const response = await fetch(url);

        const {isAlreadyAdded, billModels, allowanceModels, deductionModels} = await response.json();
        console.log("isAlreadyAdded", isAlreadyAdded);
        console.log("billModels", billModels);
        console.log("allowanceModels", allowanceModels);
        console.log("deductionModels", deductionModels);

        actionName = isAlreadyAdded?'edit':'add';

        const tableBody = document.querySelector('#bill-table tbody');
        const templateRow = document.querySelector('#bill-table tr.template-row');

        const allowanceTableBody = document.querySelector('#allowance-table tbody');
        const allowanceTemplateRow = document.querySelector('#allowance-table tr.template-row');

        const deductionTableBody = document.querySelector('#deduction-table tbody');
        const deductionTemplateRow = document.querySelector('#deduction-table tr.template-row');

        showOrHideLoadingGif('bill-table', false);
        if (billModels.length == 0) {
            noDataFound();
        } else {
            clearTable();
            allowanceModels.forEach(model => showConfigurationModelInTable(allowanceTableBody, allowanceTemplateRow, model));
            deductionModels.forEach(model => showConfigurationModelInTable(deductionTableBody, deductionTemplateRow, model));
            billModels.forEach(model => showModelInTable(tableBody, templateRow, model));
            calculateAllRowTotal();
        }
    }

    function setRowData(templateRow, payrollBillModel) {
        const rowDataPrefix = 'row-data-';
        for (const key in payrollBillModel) {
            const td = templateRow.querySelector('.' + rowDataPrefix + key);
            if (!td) continue;
            td.innerText = payrollBillModel[key];
        }
    }

    function showConfigurationModelInTable(tableBody, templateRow, payrollBillModel) {
        const modelRow = templateRow.cloneNode(true);
        modelRow.classList.remove('template-row');
        setRowData(modelRow, payrollBillModel);
        tableBody.append(modelRow);
    }

    function showModelInTable(tableBody, templateRow, payrollBillModel) {
        const modelRow = templateRow.cloneNode(true);
        modelRow.classList.remove('template-row');
        modelRow.querySelector('input[name="employeeRecordsId"]').value = payrollBillModel.employeeRecordsId;
        modelRow.querySelector('input[name="mpPayrollBillId"]').value = payrollBillModel.mpPayrollBillId;
        setRowData(modelRow, payrollBillModel);
        tableBody.append(modelRow);
    }

    function calculateAllRowTotal() {
        const colIndicesToSum = [4];
        const totalTitleColSpan = 1;
        const totalTitle = '<%=getDataByLanguage(Language, "সর্বমোট", "Total")%>';
        setupTotalRow('bill-table', totalTitle, totalTitleColSpan, colIndicesToSum, null, '<%=Language%>');
        setupTotalRow('allowance-table', totalTitle, 1, [2], null, '<%=Language%>');
        setupTotalRow('deduction-table', totalTitle, 1, [2], null, '<%=Language%>');
    }

    function setButtonDisableState(value) {
        $('#submit-btn').prop('disabled', value);
    }

    const form = $('#bill-form');

    function submitForm() {
        var $myModal = $('.custom-modal');
        setButtonDisableState(true);
        showToastSticky("সাবমিট প্রক্রিয়াধীন", "Submit Processing");
        $myModal.fadeIn();

        $.ajax({
            type: "POST",
            url: "Mp_payroll_billServlet?actionType=ajax_" + actionName,
            data: form.serialize(),
            dataType: 'JSON',
            success: function (response) {
                if (response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                    setButtonDisableState(false);
                } else if (response.responseCode === 200) {
                    $myModal.fadeOut();
                    window.location.assign(getContextPath() + response.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                setButtonDisableState(false);
            }
        });
    }

    function init() {
        $('#monthYear_js').on('datepicker.change', (event, param) => {
            const isValidDate = dateValidator('monthYear_js', true);
            const monthYear = getDateStringById('monthYear_js');
            if (isValidDate) {
                $('#monthYear').val(monthYear);
                monthYearChanged(monthYear);
            } else {
                clearTable();
            }
        });
    }

    $(document).ready(function () {
        init();
        electionDetailsChanged();
    });
</script>