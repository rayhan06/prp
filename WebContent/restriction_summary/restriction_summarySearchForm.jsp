
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="restriction_summary.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
String navigator2 = "navRESTRICTION_SUMMARY";
String servletName = "Restriction_summaryServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.RESTRICTION_SUMMARY_ADD_RESTRICTIONCONFIGID, loginDTO)%></th>
								<th><%=LM.getText(LC.RESTRICTION_SUMMARY_ADD_RESTRICTIONCONFIGDETAILSID, loginDTO)%></th>
								<th><%=LM.getText(LC.RESTRICTION_SUMMARY_ADD_RESTRICTIONCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.RESTRICTION_SUMMARY_ADD_EMPLOYEERECORDID, loginDTO)%></th>
								<th><%=LM.getText(LC.RESTRICTION_SUMMARY_ADD_RESTRICTIONREASON, loginDTO)%></th>
								<th><%=LM.getText(LC.RESTRICTION_SUMMARY_ADD_INSERTEDBY, loginDTO)%></th>
								<th><%=LM.getText(LC.RESTRICTION_SUMMARY_ADD_INSERTTIONDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
								<th><%=LM.getText(LC.RESTRICTION_SUMMARY_SEARCH_RESTRICTION_SUMMARY_EDIT_BUTTON, loginDTO)%></th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Restriction_summaryDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Restriction_summaryDTO restriction_summaryDTO = (Restriction_summaryDTO) data.get(i);
																																
											
											%>
											<tr>
								
		
											<td>
											<%=Utils.getDigits(restriction_summaryDTO.restrictionConfigId, Language)%>
											</td>
		
											<td>
											<%=Utils.getDigits(restriction_summaryDTO.restrictionConfigDetailsId, Language)%>
											</td>
		
											<td>
											<%=CatRepository.getInstance().getText(Language, "restriction", restriction_summaryDTO.restrictionCat)%>
											</td>
		
											<td>
											<%=Utils.getDigits(restriction_summaryDTO.employeeRecordId, Language)%>
											</td>
		
											<td>
											<%=restriction_summaryDTO.restrictionReason%>
											</td>
		
											<td>
											<%=Utils.getDigits(restriction_summaryDTO.insertedBy, Language)%>
											</td>
		
											<td>
											<%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, restriction_summaryDTO.insertionDate), Language)%>
											</td>
		
		
		
		
	
											<%CommonDTO commonDTO = restriction_summaryDTO; %>
											<%@include file="../pb/searchAndViewButton.jsp"%>											
																						
											<td class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=restriction_summaryDTO.iD%>'/></span>
												</div>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			