<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="restriction_summary.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
Restriction_summaryDTO restriction_summaryDTO = new Restriction_summaryDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	restriction_summaryDTO = Restriction_summaryDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = restriction_summaryDTO;
String tableName = "restriction_summary";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.RESTRICTION_SUMMARY_ADD_RESTRICTION_SUMMARY_ADD_FORMNAME, loginDTO);
String servletName = "Restriction_summaryServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Restriction_summaryServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=restriction_summaryDTO.iD%>' tag='pb_html'/>
	
														<input type='hidden' class='form-control'  name='restrictionConfigId' id = 'restrictionConfigId_hidden_<%=i%>' value='<%=restriction_summaryDTO.restrictionConfigId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='restrictionConfigDetailsId' id = 'restrictionConfigDetailsId_hidden_<%=i%>' value='<%=restriction_summaryDTO.restrictionConfigDetailsId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.RESTRICTION_SUMMARY_ADD_RESTRICTIONCAT, loginDTO)%>															</label>
                                                            <div class="col-8">
																<select class='form-control'  name='restrictionCat' id = 'restrictionCat_category_<%=i%>'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("restriction", Language, restriction_summaryDTO.restrictionCat);
																%>
																<%=Options%>
																</select>
	
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='employeeRecordId' id = 'employeeRecordId_hidden_<%=i%>' value='<%=restriction_summaryDTO.employeeRecordId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.RESTRICTION_SUMMARY_ADD_RESTRICTIONREASON, loginDTO)%>															</label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='restrictionReason' id = 'restrictionReason_text_<%=i%>' value='<%=restriction_summaryDTO.restrictionReason%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.RESTRICTION_SUMMARY_ADD_INSERTEDBY, loginDTO)%>															</label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='insertedBy' id = 'insertedBy_text_<%=i%>' value='<%=restriction_summaryDTO.insertedBy%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.RESTRICTION_SUMMARY_ADD_INSERTTIONDATE, loginDTO)%>															</label>
                                                            <div class="col-8">
																<%value = "inserttionDate_js_" + i;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='inserttionDate' id = 'inserttionDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(restriction_summaryDTO.insertionDate))%>' tag='pb_html'>
															</div>
                                                      </div>									
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.RESTRICTION_SUMMARY_ADD_RESTRICTION_SUMMARY_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.RESTRICTION_SUMMARY_ADD_RESTRICTION_SUMMARY_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);
	preprocessDateBeforeSubmitting('inserttionDate', row);

	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Restriction_summaryServlet");	
}

function init(row)
{

	setDateByStringAndId('inserttionDate_js_' + row, $('#inserttionDate_date_' + row).val());

	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	


</script>






