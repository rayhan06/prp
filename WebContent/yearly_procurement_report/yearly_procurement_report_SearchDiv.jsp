<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@ page import="pb.*" %>
<%@ page import="java.util.Calendar" %>
<%
    String Language = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_EDIT_LANGUAGE, loginDTO);
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);

    String dropDown = "<select class='form-control' onchange='ajaxSubmit()' name='procurementYear' id='procurementYear_id' tag='pb_html'>dropDownOptions</select>";
    String option = "<option value='xe'>xb</option>";
    String options = "";
    for (int year = -4; year < 5; year++) {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.YEAR, year);
        Date today = c.getTime();
        SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
        String todayString = sf.format(today);
        int currentYear = Integer.parseInt(todayString.substring(6));
        int previousYear = currentYear - 1;
        int nextYear = currentYear + 1;

        String firstYear = "";
        String secondYear = "";

        int month = Integer.parseInt(todayString.substring(3, 5));
        firstYear = month > 6 ? String.valueOf(currentYear) : String.valueOf(previousYear);
        secondYear = month > 6 ? String.valueOf(nextYear).substring(2) : String.valueOf(currentYear).substring(2);

        String reportYear = firstYear + "-" + secondYear;
        String reportYearByLanguage = Utils.getDigits(reportYear, Language);

        String optionReplaced = option.replaceAll("xe", reportYear);
        optionReplaced = optionReplaced.replaceAll("xb", reportYearByLanguage);
        if (year == 0) optionReplaced = optionReplaced.replaceAll("<option", "<option selected");
        options += optionReplaced;
    }

    dropDown = dropDown.replaceAll("dropDownOptions", options);

%>

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>
<div id="reportName" class="search-criteria-div">
    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right">
            <%=LM.getText(LC.YEARLY_PROCUREMENT_REPORT_WHERE_REPORTNAME, loginDTO)%>
        </label>
        <div class="col-md-8">
            <input class='form-control' name='reportName' id='reportName_id' onKeyUp="ajaxSubmit();" value=""/>
        </div>
    </div>
</div>
<div id="procurementYear" class="search-criteria-div">
    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right">
            <%=LM.getText(LC.YEARLY_PROCUREMENT_REPORT_WHERE_PROCUREMENTYEAR, loginDTO)%>
        </label>
        <div class="col-md-8">
            <%=dropDown%>
        </div>
    </div>
</div>
<%--<%@include file="../pbreport/yearmonth.jsp"%>--%>
<%--<%@include file="../pbreport/calendar.jsp"%>--%>
<script type="text/javascript">
    function init() {
        dateTimeInit($("#Language").val());
    }

    function PreprocessBeforeSubmiting() {
    }
</script>