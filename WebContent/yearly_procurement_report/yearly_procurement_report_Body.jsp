<%@page import="util.ActionTypeConstant" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="centre.CentreDAO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String userfullName = userDTO.fullName;
    int userCentre = userDTO.centreType;
    CentreDAO centreDAO = new CentreDAO();
    String centreName = centreDAO.getCentreNameByCentreID(userCentre);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.YEARLY_PROCUREMENT_REPORT_OTHER_REPORT_GENERATION, loginDTO)%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="ReportForm"
              action="<%=request.getContextPath()%>/Yearly_procurement_report_Servlet?actionType=<%=ActionTypeConstant.REPORT_RESULT%>">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=LM.getText(LC.YEARLY_PROCUREMENT_REPORT_OTHER_REPORT_GENERATION, loginDTO)%>
                                            </h4>
                                        </div>
                                    </div>
                                  
                                    <div id="searchCriteria" hide="false">
                                        <%@include file="yearly_procurement_report_SearchDiv.jsp" %>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="my-5" id="report-div">
                    <h5 class="table-title caption-subject" id="report-name">
                        <%=LM.getText(LC.YEARLY_PROCUREMENT_REPORT_OTHER_YEARLY_PROCUREMENT_REPORT, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-striped text-nowrap" id="reportTable">
                            <thead>
                            <tr>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>
                            </tbody>

                        </table>
                    </div>
                </div>
                <%@include file="../pbreport/pagination.jsp" %>
            </div>
        </form>
    </div>
</div>