
var currentPageNo = 1;
var totalRecord = 0;
var totalPageNo = 0;
var recordPerPage = 0;

var reportTable = $('#reportTable');
var reportForm =  $("#ReportForm");
//init DataTable first time without any data
var reportDataTable = reportTable.DataTable({
    'searching': false,
    'destroy': true,
    'ordering': false,
    'scrollX': true,
    'scrollY': true,
    'paging': false
});


function isNumeric(n)
{
	return !isNaN(parseFloat(n)) && isFinite(n);
}

function isDate(n)
{
	var date = new Date(n);
	return date instanceof Date && !isNaN(date.valueOf());
}

function searchByDateChanged()
{
	if($('#search_by_date').prop('checked')) 
	{
  		$("#ymdiv").css("display", "none");
		$("#calendardiv").css("display", "block");
	}
	else 
	{
	    $("#ymdiv").css("display", "block");
		$("#calendardiv").css("display", "none");
	}
}

function processYMAndSubmit()
{
	if($("#dYear").val() != "" && $("#dMonth").val() != "")
	{
		var year = parseInt($("#dYear").val());
		var month = parseInt($("#dMonth").val());
		var day = 1;
		console.log("year = " + year  + " month = " + month  + " day = " + day);
		$("#startDate").val(new Date(year, month, day).getTime());
		
		month ++;
		if(month == 12)
		{
			month = 0;
			year ++;
		}
		$("#endDate").val(new Date(year, month, day).getTime());
		
	}
	else
	{
		$("#startDate").val('0');
		$("#endDate").val('91605117600000');
	}
	
	console.log("startdate = " + $("#startDate").val());
	console.log("enddate = " + $("#endDate").val());
	ajaxSubmit();
}

function processCalendarAndSubmit(startDateInput, endDateInput)
{
	console.log("calendar change called");
	var startDate = getBDFormattedDate(startDateInput);
	if(startDate == '')
	{
		startDate = '0';
	}	
	$("#startDate").val(startDate);
	
	var endDate = getBDFormattedDate(endDateInput);
	if(endDate == '')
	{
		endDate = '91605117600000';
	}	
	$("#endDate").val(endDate);
	
	console.log("startdate = " + $("#startDate").val());
	console.log("enddate = " + $("#endDate").val());
	
	ajaxSubmit();
}

function processNewCalendarDateAndSubmit()
{
	console.log("calendar change called");
	$("#startDate").val(getDateTimestampById('startDate_js'));
	
	$("#endDate").val(getDateTimestampById('endDate_js'));
	
	console.log("startdate = " + $("#startDate").val());
	console.log("enddate = " + $("#endDate").val());
	
	ajaxSubmit();
}

function comparator(x, y, dir)
{
	if(dir == 1)
	{
		if(isNumeric(x.innerHTML) && isNumeric(y.innerHTML))
  	    {
			if(parseInt(x.innerHTML) < parseInt(y.innerHTML))
			{
				return true;
			}
	    	 
  	  	}
		else if(isDate(x.innerHTML) && isDate(y.innerHTML))
		{
			console.log("date found: " + x.innerHTML);
			return (new Date(x.innerHTML) > new Date(y.innerHTML));
		}
		else if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) 
		{
			return true;
		}
	}
	else
	{
		if(isNumeric(x.innerHTML) && isNumeric(y.innerHTML))
  	    {
			if(parseInt(x.innerHTML) > parseInt(y.innerHTML))
			{
				return true;
			}
	    	 
  	  	}
		else if(isDate(x.innerHTML) && isDate(y.innerHTML))
		{
			console.log("date found: " + x.innerHTML);
			return (new Date(x.innerHTML) < new Date(y.innerHTML));
		}
		else if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) 
		{
			return true;
		}
	}
}

function customadd(x, y)
{
	var z;
	if(isNumeric(x) && isNumeric(y))
	{
		z = parseInt(x) + parseInt(y);
	}
	else
	{
		z = "";
	}
	//console.log("x = " + x + " y = " + y + " z = " + z)
	return z;
}

function sortTable(row, dir) 
{
	console.log("sorting");
	var table, rows, switching, i, x, y, shouldSwitch;
	table = document.getElementById("reportTable");
	switching = true;
	/* Make a loop that will continue until
	no switching has been done: */
	while (switching) 
	{
		// Start by saying: no switching is done:
		switching = false;
		rows = table.rows;
		/* Loop through all table rows (except the
		first, which contains table headers): */
		for (i = 1; i < (rows.length - 2); i++) 
		{
			// Start by saying there should be no switching:
			shouldSwitch = false;
			/* Get the two elements you want to compare,
			one from current row and one from the next: */
			x = rows[i].getElementsByTagName("TD")[row];
			y = rows[i + 1].getElementsByTagName("TD")[row];
			// Check if the two rows should switch place:

			if(comparator(x, y, dir) == true)
			{
				shouldSwitch = true;
				break;
			}
	  
		}

		if (shouldSwitch) 
		{
		  /* If a switch has been marked, make the switch
		  and mark that a switch has been done: */
		  rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
		  switching = true;
		}
	}
	console.log("sorting done");
}


function getArrows(j)
{
	var arrows = "<span class=\"up-down-link\"> " + 
	"<a class=\"up-link\"><span><i onclick='sortTable(" + j + ", 2)' class=\"fa fa-arrow-circle-up\"></i></span></a>" +
	"<a class=\"down-link\"><span><i onclick='sortTable(" + j + ", 1)' class=\"fa fa-arrow-circle-down\"></i></span></a>" +
	"</span>";
	
	return arrows;
}

function formatpagenoAndRPP()
{
	var pageno = document.getElementById("pageno").value;
	var RECORDS_PER_PAGE = document.getElementById("RECORDS_PER_PAGE").value;
	if(pageno == "" || pageno < 1)
	{
		document.getElementById("pageno").value = 1;
	}
	if(RECORDS_PER_PAGE == "" || RECORDS_PER_PAGE < 1)
	{
		document.getElementById("RECORDS_PER_PAGE").value = 100;
	}
}

function ajaxSubmit()
{
	console.log("Ajax default submitting");
	formatpagenoAndRPP();
	var pageno = document.getElementById("pageno").value;
	var RECORDS_PER_PAGE = document.getElementById("RECORDS_PER_PAGE").value;
	if(pageno > document.getElementById("tatalPageNo").innerHTML)
	{
		pageno = 1;
	}
	$("input[name=pageno]").val(pageno);
	$("input[name=RECORDS_PER_PAGE]").val(RECORDS_PER_PAGE);

	ajaxMiniSubmit();
}

function ajaxMiniSubmit()
{
	console.log("Ajax submitting 2");
	
	console.log("params: " + reportForm.serialize());

	submit();
}

function firstloadSubmit()
{
	console.log("Ajax firstload submitting");
	formatpagenoAndRPP();
	var pageno = 1;
	var RECORDS_PER_PAGE = document.getElementById("RECORDS_PER_PAGE").value;
	$("input[name=pageno]").val(pageno);
	$("input[name=RECORDS_PER_PAGE]").val(RECORDS_PER_PAGE);

	ajaxMiniSubmit();
}

function lastloadSubmit()
{
	console.log("Ajax lastload submitting");
	formatpagenoAndRPP();
	var RECORDS_PER_PAGE = document.getElementById("RECORDS_PER_PAGE").value;
	var pageno = document.getElementById("tatalPageNo").innerHTML;
	$("input[name=pageno]").val(pageno);
	$("input[name=RECORDS_PER_PAGE]").val(RECORDS_PER_PAGE);

	ajaxMiniSubmit();
}

function nextloadSubmit()
{
	console.log("Ajax nextload submitting");
	formatpagenoAndRPP();
	var pageno = parseInt(document.getElementById("pageno").value) + 1;
	var RECORDS_PER_PAGE = document.getElementById("RECORDS_PER_PAGE").value;
	if(pageno > document.getElementById("tatalPageNo").innerHTML)
	{
		pageno = 1;
	}
	$("input[name=pageno]").val(pageno);
	$("input[name=RECORDS_PER_PAGE]").val(RECORDS_PER_PAGE);

	ajaxMiniSubmit();
}

function prevloadSubmit()
{
	console.log("Ajax prevload submitting");
	formatpagenoAndRPP();
	var pageno = parseInt(document.getElementById("pageno").value) - 1;
	var RECORDS_PER_PAGE = document.getElementById("RECORDS_PER_PAGE").value;
	if(pageno < 1)
	{
		pageno = document.getElementById("tatalPageNo").innerHTML;
	}
	$("input[name=pageno]").val(pageno);
	$("input[name=RECORDS_PER_PAGE]").val(RECORDS_PER_PAGE);

	ajaxMiniSubmit();
}

var columnDataFromater = 
{
    exportOptions: 
	{
        format: 
		{
            body: function (data, row, column, node) 
			{
            	/*data.replace( /[$,.]/g, '' )*/ 
                return data.replace(/(&nbsp;|<([^>]+)>)/ig, "");/*remove html tag from data when exporting*/
            }
        }
    }
};

var createTable = function (payload)
{
	var data = payload.resultRows;

    var createdHTML = "";
    var isBodyCreatedStart = false;
    var isHeadCreated = false;
    
    
    createdHTML += "<thead><tr>";
    
    var j = 0;
    var list = data[0];
    var sums = [];
    $.each(list, function (innerIndex, listItem) 
	{

        // createdHTML += "<th class='text-center-report'><span style='margin-left:-10px'>" + listItem + getArrows(j) + "</span></th>";
        createdHTML += "<th class='text-center-report'><span style='margin-left:-10px'>" + listItem + "</span></th>";
        sums[j] = 0;
        j ++;
    });
   
    createdHTML += "</tr></thead>";
    
    var onlyData = data.slice(1);
    
    
    
    // createdHTML += "<tbody>";
    $.each(onlyData, function (index, list) 
    {
    	// createdHTML += "<tr>";
    	var j = 0;
        $.each(list, function (innerIndex, listItem) 
		{
        	
            // createdHTML += "<td class='text-center-report'>" + listItem + "</td>";
            sums[j] = customadd(sums[j] , listItem);

            j ++;
        })
        console.log(" ");
        // createdHTML += "</tr>";
    });

    // createdHTML += "</tbody>";

	createdHTML += payload.tbodyHtml;


    createdHTML += "<tfoot><tr>";

	createdHTML += "<td class='text-center-report'><span><strong>" + "</strong></span></td>";
    $.each(sums, function (innerIndex, listItem) 
	{
		if (addables[innerIndex] == null) return true;
		if(addables[innerIndex] == 0)
		{
			console.log("innerIndex = " + innerIndex + " addables[innerIndex] = " + addables[innerIndex]);
			listItem ="";
		}
		else
		{
			console.log("null addable for innerIndex = " + innerIndex);
		}

		createdHTML += "<td class='text-center-report' style='text-align: right;'><span><strong>" + listItem + "</strong></span></td>";
        j ++;
    });
    createdHTML += "</tr></tfoot>";

    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = mm + '/' + dd + '/' + yyyy;
    
    reportDataTable.destroy();
    reportTable.empty();
    reportTable.attr("width", "100%");
    reportTable.append(createdHTML);
    reportDataTable = reportTable.DataTable(
	{
        'searching': false,
        'destroy': true,
        'ordering': false,
        'scrollX': true,
        'scrollY': true,
        'paging': false,
        'pagingType': 'simple',
        'dom': 'Bfrtip',
        'buttons': [
            $.extend(true, {}, columnDataFromater, 
			{
            	className: 'export-btn',
                extend: 'copyHtml5', 
                footer: true 
            }),
            $.extend(true, {}, columnDataFromater,
			{
            	className: 'export-btn',
                extend: 'excelHtml5',
                footer: true,
				customize: function( xlsx ) {
					var sheet = xlsx.xl.worksheets['sheet1.xml'];

					$('c[r^="A"]', sheet).attr( 's', '2' );
					$('c[r^="B"]', sheet).attr( 's', '2' );
					$('c[r^="C"]', sheet).attr( 's', '2' );
					// $('c[r^="A1"]', sheet).text('');

					// var row = 3;
					// var num_columns = 19;
					//
					// $('row', sheet).each(function(x) {
					//
					// 		// console.log("");
					// 		// console.log("---- row x: " + x);
					//
					// 		for(var i=0; i<num_columns; i++) {
					//
					// 			// console.log(output_table.row(':eq('+x+')').data());
					//
					// 			if ($(output_table.cell(':eq('+x+')', i).node()).hasClass('boldType')) {
					// 				// console.log('YES - sig-w - row ' + x + ', column ' + i);
					// 				$('row:nth-child('+(row)+') c', sheet).eq(i).attr('s', '2');
					// 				break;
					// 			}
					//
					// 		}
					//
					// 		row++;
					//
					// });


					// $('row:nth-child(2) c', sheet).attr('s', '7');

				}
            }),
            // $.extend(true, {}, columnDataFromater,
			// {
            // 	className: 'export-btn',
            //     extend: 'csvHtml5',
            //     footer: true
            // }),
			// $.extend(true, {}, columnDataFromater,
			// {
			// 	className: 'export-btn',
            //     extend: 'pdfHtml5',
            //     orientation: 'landscape',
            //     pageSize: 'A4',
            //     footer: true
            // }),
			{
            	className: 'export-btn',
                extend: 'print',
                text: 'Print',
                orientation: 'landscape',
                pageSize: 'A0',
                title: $("#report-name").html() + ', ' + today,
                autoPrint: true,
                footer: true,
                header: true 
            }
        ]
        
    });
    $("#report-div").show();

    $("#reportTable_info").html("Total "+totalRecord+" entries");
}

var addables = [0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0];

function createTableCallback(data) 
{
	if (data['responseCode'] == 200) {
	    createTable(data.payload);
	}else {
		toastr.error(data['msg'] + " code: " + data['responseCode']);
	}
}


var updateRecordInfo = function(totalRecord,recordPerPage)
{
	totalPageNo = Math.ceil(parseFloat(totalRecord)/parseFloat(recordPerPage));
	$("#tatalPageNo").html(totalPageNo);
	$("#tatalPageNoMobile").html(totalPageNo);
	$("input[name=totalRecord]").val(totalRecord);
	
}

var getTotalDataCountCallBack = function(data)
{
	recordPerPage = $("input[name=RECORDS_PER_PAGE]").val();
	totalRecord = data.payload;
	$("#reportTable_info").html("Total "+totalRecord+" entries");
	updateRecordInfo(totalRecord,recordPerPage);
}



var submit = function()
{
	console.log("submitting");
	//flashOldReport();
	$(".navigator").show();
	PreprocessBeforeSubmiting(0, "report");
	callAjax(reportForm.attr('action'), reportForm.serialize(), createTableCallback, "GET");
	console.log("returned 2");
	//PostprocessAfterSubmiting(0);
	if(document.getElementById("searchCriteria").getAttribute("hide") == "true")
	{				
		document.getElementById("searchCriteria").remove();
		document.getElementById("button-div").remove();
	}						      
}
	
$(document).ready(function () 
{

	/*alert("change bottom -24")
	$(".dt-buttons").css("margin-top","-2px !important")*/
	console.log("we r ready 2");
	$('#startDate_js').on('datepicker.change', (event, param) => {
   		processNewCalendarDateAndSubmit();
	});
	$('#endDate_js').on('datepicker.change', (event, param) => {
   		processNewCalendarDateAndSubmit();
	});
	init(0);   
    ajaxSubmit();


 
});






