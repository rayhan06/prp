<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="util.*" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberInWord" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="finance.FinanceUtil" %>
<%@ page import="overtime_bill.Overtime_billStatus" %>
<%@ page import="user.UserDTO" %>
<%@ page import="food_bill.Food_billDTO" %>
<%@ page import="food_allowance.Food_allowanceDTO" %>
<%@ page import="food_allowance.Food_allowanceDAO" %>
<%@ page import="food_allowance.Food_allowanceModel" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="parliament_session.Parliament_sessionRepository" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="overtime_bill.OvertimeInfo" %>
<%@ page import="bill_approval_history.Bill_approval_historyDTO" %>
<%@ page import="food_bill.Food_billDAO" %>
<%@ page import="bill_approval_history.BillApprovalStatus" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="bill_approval_history.Bill_approval_historyDAO" %>
<%@page pageEncoding="UTF-8" %>

<%
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = "english".equalsIgnoreCase(Language);

    Food_billDTO foodBillDTO = (Food_billDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);

    List<Food_allowanceDTO> allowanceDTOs = Food_allowanceDAO.getInstance().findByFoodBillId(foodBillDTO.iD);
    List<Food_allowanceModel> allowanceModels =
            allowanceDTOs.stream()
                         .map(allowanceDTO -> new Food_allowanceModel(allowanceDTO, "Bangla"))
                         .collect(Collectors.toList());
    Food_allowanceModel.fixOrdering(allowanceModels);

    String convertedBillTotal = StringUtils.convertToBanNumber(String.format("%d", foodBillDTO.billAmount));
    String formattedBillTotal = BangladeshiNumberFormatter.getFormattedNumber(convertedBillTotal);

    Map<Long, Long> billDateBySortedMonth
            = foodBillDTO.sortedBillDateList.stream()
                                            .collect(Collectors.groupingBy(
                                                    DateUtils::get1stDayOfMonth,
                                                    TreeMap::new,
                                                    Collectors.counting()
                                            ));
    int numberOfMonths = billDateBySortedMonth.size();
    String monthNames = billDateBySortedMonth.keySet()
                                             .stream()
                                             .sequential()
                                             .map(monthTimeStamp -> DateUtils.getMonthYear(monthTimeStamp, "bangla", "/"))
                                             .collect(Collectors.joining(", "));
    String billTypeText = foodBillDTO.getBillTypeText(false);
    String dailyRateStr = StringUtils.convertToBanNumber(String.format("%d", foodBillDTO.dailyRate));
    String dailyRateFormattedStr = BangladeshiNumberFormatter.getFormattedNumber(dailyRateStr);
    String dailyRateInWord = BangladeshiNumberInWord.convertToWord(dailyRateStr).trim();

    TreeMap<Integer, Bill_approval_historyDTO> approvalHistoryBySortedLevel =
            Bill_approval_historyDAO.getInstance().getApprovalHistoryBySortedLevel(foodBillDTO, userDTO);
    // make size at least 2 to push single approver sign to right
    while (approvalHistoryBySortedLevel.size() < 2) {
        approvalHistoryBySortedLevel.put(-1, null);
    }
    String financeSerialNumber = "";
    if (foodBillDTO.financeSerialNumber > 0) {
        financeSerialNumber = StringUtils.convertToBanNumber(String.format("%d", foodBillDTO.financeSerialNumber));
    }
%>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    .page {
        background: white;
        padding: .05in;
        page-break-after: always;
    }

    .table-bordered-custom th,
    .table-bordered-custom td {
        border: 1px solid #000;
        padding: 2px;
    }

    th {
        text-align: center;
    }

    .signature-image {
        width: 200px !important;
        height: 53px !important;
    }

    .signature-div {
        color: #a406dc;
        font-size: 10px !important;
    }

    .signature-div * {
        font-size: 10px !important;
    }

    .underline {
        border-bottom: 1px solid black;
        padding-bottom: 2px;
    }

    div.bill-heading {
        font-weight: bold;
        font-size: 1.1rem;
    }

    .signature-container-div {
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding: 1rem;
    }

    div.finance-serial-number {
        font-weight: bold;
        font-size: 1.8rem;
    }

    @media print {
        @page {
            size: landscape;
            margin: .25in;
        }
    }
</style>

<div class="loader-container-circle" id="full-page-loader">
    <div class="loader-circle"></div>
</div>

<div class="kt-content p-0" id="kt_content">
    <div class="">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title prp-page-title">
                        <%=UtilCharacter.getDataByLanguage(Language, "খাবার ভাতা বিল", "Food Allowance Bill")%>
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body page-bg" id="bill-div">

                <div class="ml-auto m-3">
                    <button type="button" class="btn" id='download-pdf'
                            onclick="printDivWithJqueryPrint('to-print-div');">
                        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                </div>

                <div style="margin: auto;">
                    <div class="container" id="to-print-div">
                        <%
                            int rowsPerPage = 7;
                            int index = 0;
                            boolean isLastPage = false;
                            long totalAmountRunningTotal = 0;
                            while (index < allowanceModels.size()) {
                                boolean isFirstPage = (index == 0);
                        %>
                        <section class="page">
                            <div class="text-right finance-serial-number">
                                <%=financeSerialNumber%>
                            </div>
                            <div class="text-center">
                                <div class="bill-heading">বাংলাদেশ জাতীয় সংসদ সচিবালয়</div>
                                <div class="bill-heading">
                                    <span class="underline">
                                    <%=Office_unitsRepository.getInstance().geText("Bangla", foodBillDTO.officeUnitId)%>
                                    </span>
                                </div>
                                <div class="bill-heading">
                                    <span class="underline"><%=monthNames%>-ইং মাসের সংসদ অধিবেশনকালীন কার্যদিবস এর <%=billTypeText%> দুপুরের খাবার বিল</span>
                                </div>
                            </div>

                            <div class="mt-2">
                                <%=foodBillDTO.ordinanceText%> মোতাবেক অত্র কার্যালয়ে কর্মরত নিম্নলিখিত
                                কর্মকর্তা/কর্মচারীগণের অধিবেশন চলাকালীন
                                দুপুরের খাবার/ইফতারী বাবদ <%=dailyRateFormattedStr%> (<%=dailyRateInWord%>) টাকা হারে
                                খাবার বিল।
                            </div>

                            <div class="mt-3">
                                <table class="table-bordered-custom w-100">
                                    <thead>
                                    <tr>
                                        <th rowspan="2">ক্রমিক নং</th>
                                        <th rowspan="2">নাম ও পদবী</th>
                                        <th colspan="<%=numberOfMonths%>">মাসের নাম</th>
                                        <th rowspan="2">মোট দিন</th>
                                        <th rowspan="2">টাকার হার</th>
                                        <th rowspan="2">মোট টাকা</th>
                                        <th rowspan="2">কর্মকর্তা/কর্মচারীর স্বাক্ষর</th>
                                    </tr>
                                    <tr>
                                        <%for (Map.Entry<Long, Long> monthBillDateCount : billDateBySortedMonth.entrySet()) {%>
                                        <td class="text-center">
                                            <%=DateUtils.getMonthYear(monthBillDateCount.getKey(), "bangla", "/")%>
                                        </td>
                                        <%}%>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <%if (!isFirstPage) {%>
                                    <tr>
                                        <td></td>
                                        <td colspan="<%=3 + numberOfMonths%>" class="text-right">
                                            পূর্ব পৃষ্ঠার জের=
                                        </td>
                                        <td class="text-right">
                                            <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                    StringUtils.convertToBanNumber(String.valueOf(totalAmountRunningTotal))
                                            )%>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <%}%>

                                    <%
                                        int rowsInThisPage = 0;
                                        while (index < allowanceModels.size() && rowsInThisPage < rowsPerPage) {
                                            isLastPage = (index == (allowanceModels.size() - 1));
                                            rowsInThisPage++;
                                            Food_allowanceModel model = allowanceModels.get(index++);
                                            totalAmountRunningTotal += model.totalAmount;
                                            TreeMap<Long, List<OvertimeInfo>> overtimeInfoListBySortedMonth
                                                    = model.getOvertimeInfoListBySortedMonth(foodBillDTO.sortedBillDateList);
                                    %>
                                    <tr>
                                        <td class="text-center">
                                            <%=StringUtils.convertToBanNumber(String.format("%d", model.serialNo))%>.
                                        </td>
                                        <td>
                                            <%=model.name%><br>
                                            <%=model.organogramName%>
                                        </td>
                                        <%for (Map.Entry<Long, Long> monthBillDateCount : billDateBySortedMonth.entrySet()) {%>
                                        <td class="text-center">
                                            <%=Food_allowanceModel.getPresentDatesString(
                                                    overtimeInfoListBySortedMonth.get(monthBillDateCount.getKey()),
                                                    false
                                            )%>
                                        </td>
                                        <%}%>
                                        <td class="text-center">
                                            <%=StringUtils.convertToBanNumber(String.format("%d", model.days))%>
                                        </td>
                                        <td class="text-center">
                                            <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                    StringUtils.convertToBanNumber(String.format("%d", model.dailyRate))
                                            )%>
                                        </td>
                                        <td class="text-right">
                                            <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                    StringUtils.convertToBanNumber(String.format("%d", model.totalAmount))
                                            )%>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <%}%>
                                    <tr>
                                        <td></td>
                                        <td colspan="<%=3 + numberOfMonths%>" class="text-right">
                                            <%=isLastPage ? "সর্বমোট" : "উপমোট"%>=
                                        </td>
                                        <td class="text-right">
                                            <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                    StringUtils.convertToBanNumber(String.valueOf(totalAmountRunningTotal))
                                            )%>
                                        </td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <%if (isLastPage) {%>
                            <div class="mt-2">
                                প্রত্যয়ন করা যাচ্ছে যে, উপরোল্লিখিত কর্মকর্তা/কর্মচারীগণ জাতীয় সংসদের অধিবেশন/সংসদীয়
                                কমিটি বৈঠক ও অন্যান্য জরুরী কাজের জন্য
                                অফিস সময়ের পরে জনস্বার্থে কর্তৃপক্ষের অনুমতিক্রমে অতিরিক্ত সময় কর্মরত ছিলেন।
                            </div>

                            <div class="text-center w-100 mt-1">
                                বিলটি সঠিক আছে এবং পাস করা হল।<br>
                                সর্বমোট=<%=formattedBillTotal%>/-
                                (<%=BangladeshiNumberInWord.convertToWord(convertedBillTotal)%> টাকা মাত্র)।
                            </div>

                            <%--Signature Div--%>
                            <div class="row col-12 mt-1">
                                <div class="signature-container-div">
                                    <%--prepared by--%>
                                    <div class="text-center">
                                        <%if (!foodBillDTO.isInPreviewStage) {%>
                                        <div class="bill-heading">প্রস্তুতকারক</div>
                                        <div class="signature-div">
                                            <div>
                                                <img class="signature-image"
                                                     src='<%=StringUtils.getBase64EncodedImageStr(foodBillDTO.preparedSignature)%>'/>
                                                <br>
                                                <%=StringUtils.getFormattedTime(false, foodBillDTO.preparedTime)%>
                                            </div>
                                            <div>
                                                <%=foodBillDTO.preparedNameBn%><br>
                                                <%=foodBillDTO.preparedOrgNameBn%><br>
                                                <%=foodBillDTO.preparedOfficeNameBn%><br>
                                            </div>
                                        </div>
                                        <%}%>
                                    </div>

                                    <%
                                        for (Map.Entry<Integer, Bill_approval_historyDTO> entry : approvalHistoryBySortedLevel.entrySet()) {
                                            Integer level = entry.getKey();
                                            Bill_approval_historyDTO approvalHistoryDTO = entry.getValue();
                                    %>
                                    <div class="text-center">
                                        <div class="bill-heading">&nbsp;</div>
                                        <%if (approvalHistoryDTO != null) {%>
                                        <div class="signature-div">
                                            <%if (!foodBillDTO.isInPreviewStage) {%>
                                            <%if (approvalHistoryDTO.billApprovalStatus == BillApprovalStatus.APPROVED) {%>
                                            <div>
                                                <img class="signature-image"
                                                     src='<%=StringUtils.getBase64EncodedImageStr(approvalHistoryDTO.signature)%>'/>
                                                <br>
                                                <%=StringUtils.getFormattedTime(false, approvalHistoryDTO.approvalTime)%>
                                            </div>
                                            <%} else if (approvalHistoryDTO.isThisUsersHistory(userDTO)) {%>
                                            <button id="layer1-approve-btn"
                                                    class="btn-sm shadow text-white border-0 submit-btn mt-3 btn-border-radius"
                                                    type="button"
                                                    onclick="approveApplication(<%=foodBillDTO.iD%>,<%=level%>)">
                                                <%=isLanguageEnglish ? "Approve" : "অনুমোদন করুন"%>
                                            </button>
                                            <%} else {%>
                                            <div class="btn btn-sm border-0 shadow mt-2"
                                                 style="background-color: #bb0a0a; color: white; border-radius: 8px;cursor: text">
                                                <%=isLanguageEnglish ? "Waiting for Approval" : "অনুমোদনের অপেক্ষায়"%>
                                            </div>
                                            <%}%>
                                            <%}%>
                                            <div style="margin-top: 5px;">
                                                <%=approvalHistoryDTO.nameBn%><br>
                                                <%=approvalHistoryDTO.orgNameBn%><br>
                                                <%=approvalHistoryDTO.officeNameBn%><br>
                                            </div>
                                        </div>
                                        <%}%>
                                    </div>
                                    <%}%>
                                </div>
                            </div>

                            <div class="row col-12">
                                <div class="col-4 text-center"></div>
                                <div class="col-4 text-center">
                                    <%=FinanceUtil.getFinance1HeadDesignation("Bangla")%><br>
                                    বাংলাদেশ জাতীয় সংসদ সচিবালয়
                                </div>
                                <div class="col-4 text-center"></div>
                            </div>
                            <%}%>
                        </section>
                        <%}%>
                    </div>

                    <%if (foodBillDTO.isInPreviewStage) {%>
                    <%--Action Button Div--%>
                    <div id="action-btn-div" class="row">
                        <div class="col-12 mt-3 text-right">
                            <button id="edit-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button"
                                    onclick="location.href = 'Food_billServlet?actionType=getEditPage&ID=<%=foodBillDTO.iD%>'">
                                <%=isLanguageEnglish ? "Edit" : "এডিট করুন"%>
                            </button>
                            <button id="submit-btn"
                                    class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                                    type="button" onclick="submitFoodBill()">
                                <%=isLanguageEnglish ? "Submit" : "জমা দিন"%>
                            </button>
                        </div>
                    </div>
                    <%}%>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="../utility/jquery_print.jsp"/>

<script>
    const layer1ApproveBtn = $('#layer1-approve-btn');
    const layer2ApproveBtn = $('#layer2-approve-btn');
    const fullPageLoader = $('#full-page-loader');

    function setButtonDisableState(value) {
        layer1ApproveBtn.prop('disabled', value);
        layer1ApproveBtn.prop('disabled', value);
        $('#submit-btn').prop('disabled', value);
        $('#edit-btn').prop('disabled', value);
    }

    function downloadTemplateAsPdf(divId, fileName, orientation = 'portrait') {
        let content = document.getElementById(divId);
        const opt = {
            margin: [.1, .2],
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4', orientation: orientation}
        };
        html2pdf().from(content).set(opt).save();
    }

    function approveApplication(id, level) {
        setButtonDisableState(true);
        let msg = '<%=isLanguageEnglish? "Are you sure to approve application?" : "আপনি কি আবেদন অনুমোদনের ব্যাপারে নিশ্চিত?"%>';
        let confirmButtonText = '<%=StringUtils.getYesNo(Language, true)%>';
        let cancelButtonText = '<%=StringUtils.getYesNo(Language, false)%>';
        messageDialog('', msg, 'success', true, confirmButtonText, cancelButtonText,
            () => {
                approveFoodBill(id, level);
            }, () => {
                setButtonDisableState(false);
            }
        );
    }

    function approveFoodBill(id, level) {
        const data = {
            id: id, level: level
        };

        $.ajax({
            type: "POST",
            url: "Food_billServlet?actionType=ajax_approve",
            data: data,
            dataType: 'JSON',
            success: function (response) {
                setButtonDisableState(false);
                if (response.success) {
                    location.reload();
                } else {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToast(response.message, response.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#toast_message').css('background-color', '#ff6063');
                showToast("সার্ভারে সমস্যা", "Server Error");
                setButtonDisableState(false);
            }
        });
    }

    function submitFoodBill() {
        const data = {
            id: <%=foodBillDTO.iD%>
        };
        console.log({data});
        setButtonDisableState(true);
        fullPageLoader.show();
        $.ajax({
            type: "POST",
            url: "Food_billServlet?actionType=ajax_submit",
            data: data,
            dataType: 'JSON',
            success: function (response) {
                setButtonDisableState(false);
                fullPageLoader.hide();
                if (response.success) {
                    window.location.assign(getContextPath() + response.message);
                } else {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.message, response.message);
                }
            },
            error: function () {
                setButtonDisableState(false);
                fullPageLoader.hide();
                $('#toast_message').css('background-color', '#ff6063');
                showToastSticky("সাবমিট করতে ব্যর্থ হয়েছে", "Failed to submit");
            }
        });
    }
</script>