<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="overtime_allowance.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="food_bill_type_config.Food_bill_type_configRepository" %>
<%@ page import="food_bill_submission_config.Food_bill_submission_configDAO" %>

<%
    Overtime_allowanceDTO overtime_allowanceDTO;
    overtime_allowanceDTO = (Overtime_allowanceDTO) request.getAttribute("overtime_allowanceDTO");
    CommonDTO commonDTO = overtime_allowanceDTO;
    if (overtime_allowanceDTO == null) {
        overtime_allowanceDTO = new Overtime_allowanceDTO();
    }
    String tableName = "overtime_allowance";
%>

<%@include file="../pb/addInitializer.jsp" %>

<%
    actionName = request.getParameter("actionType").equals("edit") ? "edit" : "add";
    String servletName = "Food_billServlet";
    String context = request.getContextPath() + "/";
    boolean isLangEn = "english".equalsIgnoreCase(Language);
    String formTitle = isLangEn ? "Food Allowance" : "খাবার ভাতা";
%>

<style>
    .template-row {
        display: none;
    }

    #food-bill-finance-table tfoot {
        text-align: right;
    }

    .loader-container-circle {
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .2);
        position: fixed;
        top: 0;
        left: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: 10;
        visibility: visible;
    }

    @media (min-width: 1015px) {
        .loader-container-circle {
            width: calc(100% + 260px);
            height: 100%;
            background-color: rgba(0, 0, 0, 0.1);
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            z-index: 3;
            visibility: visible;
        }
    }

    .loader-circle {
        width: 50px;
        height: 50px;
        border: 5px solid;
        color: #3498db;
        border-radius: 50%;
        border-top-color: transparent;
        animation: loader 1.2s linear infinite;
    }

    @keyframes loader {
        25% {
            color: #2ecc71;
        }
        50% {
            color: #f1c40f;
        }
        75% {
            color: #e74c3c;
        }
        to {
            transform: rotate(360deg);
        }
    }
</style>

<div class="loader-container-circle" id="full-page-loader">
    <div class="loader-circle"></div>
</div>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal" id="food-bill-finance-form" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12 row mt-2">
                        <div class="col-lg-4 col-md-6 form-group">
                            <label class="h5" for="budgetMappingId">
                                <%=LM.getText(LC.BUDGET_OPERATION_CODE, loginDTO)%>
                            </label>
                            <select id="budgetMappingId" name='budgetMappingId'
                                    class='form-control rounded shadow-sm'
                                    onchange="budgetMappingIdChanged(this);">
                                <%=Food_bill_type_configRepository.getInstance().buildBudgetMappingDropDown(
                                        Language,
                                        null,
                                        true
                                )%>
                            </select>
                        </div>

                        <div class="col-lg-4 col-md-6 form-group">
                            <label class="h5" for="economicSubCodeId">
                                <%=isLangEn ? "Economic Code" : "অর্থনৈতিক কোড"%>
                            </label>
                            <select class='form-control rounded shadow-sm'
                                    name='economicSubCodeId' id='economicSubCodeId'
                                    onchange="economicSubCodeIdChanged()">
                                <%=Food_bill_type_configRepository.getInstance().buildEconomicSubCodeDropDown(
                                        Language,
                                        null
                                )%>
                            </select>
                        </div>

                        <div class="col-lg-4 col-md-6 form-group">
                            <label class="h5" for="foodBillSubmissionConfigId">
                                <%=isLangEn ? "Parliament Session" : "সংসদ অধিবেশন"%>
                            </label>
                            <select id="foodBillSubmissionConfigId" name='foodBillSubmissionConfigId'
                                    class='form-control rounded shadow-sm'
                                    onchange="foodBillSubmissionConfigIdChanged()"
                            >
                                <%=Food_bill_submission_configDAO.getInstance().buildOptions(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="mt-4 table-responsive">
                    <table id="food-bill-finance-table" class="table table-bordered table-striped text-nowrap">
                        <thead>
                        <tr class="text-center">
                            <th>
                                <%=isLangEn ? "No" : "নং"%>
                            </th>
                            <th>
                                <%=isLangEn ? "Office/Section" : "কার্যালয়/শাখা"%>
                            </th>
                            <th>
                                <%=isLangEn ? "Manpower" : "জনবল"%>
                            </th>
                            <th>
                                <%=isLangEn ? "Bill Amount" : "টাকার পরিমাণ"%>
                            </th>
                            <th>
                                <%=isLangEn ? "View Details" : "বিস্তারিত দেখুন"%>
                            </th>
                        </tr>
                        </thead>
                        <tbody class="main-tbody">
                        </tbody>
                        <tr class="template-row">
                            <input type="hidden" name="foodBillId" value="-1">
                            <td class="row-data-serialNo text-center"></td>
                            <td class="row-data-officeName text-center"></td>
                            <td class="row-data-employeeCount text-right"></td>
                            <td class="row-data-billAmount text-right"></td>
                            <td class="details-button">
                                <button type="button"
                                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                                        style="color: #ff6b6b;"
                                >
                                    <i class="fa fa-eye"></i>
                                </button>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="row">
                    <div id="action-button-div" class="col-12 mt-3 text-right">
                        <button id="submit-btn"
                                class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                                type="button" onclick="submitForm('prepareBill')">
                            <%=isLangEn ? "Prepare Bill" : "বিল প্রস্তুত করুন"%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<%@include file="../common/table-sum-utils.jsp" %>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script type="text/javascript">
    let actionName = 'add';
    let billAmount = '';
    let totalEmployeeCount = '';
    let isAlreadyAdded = false;
    let foodBillSummaryModels = [];
    const $submitBtn = $('#submit-btn');
    const isLangEn = <%=isLangEn%>;

    const $budgetMappingIdSelect = $('#budgetMappingId');
    const $economicSubCodeIdSelect = $('#economicSubCodeId');
    const $foodBillSubmissionConfigIdSelect = $('#foodBillSubmissionConfigId');

    const form = $('#food-bill-finance-form');
    const fullPageLoader = $('#full-page-loader');

    const $actionButtonDiv = $("#action-button-div");

    $(document).ready(function () {
        setButtonDisableState(false);
        $actionButtonDiv.hide();
        fullPageLoader.hide();

        form.validate({
            rules: {
                budgetInstitutionalGroupId: "required",
                budgetOfficeId: "required",
                monthYear: "required"
            },
            messages: {
                budgetInstitutionalGroupId: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                budgetOfficeId: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                electionDetailsId: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                parliamentSessionId: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>'
            }
        });
    });

    function clearEconomicSubCode() {
        $economicSubCodeIdSelect.val('');
    }

    function clearFoodBillSubmissionConfigId() {
        $foodBillSubmissionConfigIdSelect.val('');
    }

    function clearTable(hideEmptyTableText) {
        const tableBody = document.querySelector('#food-bill-finance-table tbody.main-tbody');
        const emptyTableText = '<%=getDataByLanguage(Language, "কোন তথ্য পাওয়া যায়নি", "No data found")%>';
        if (hideEmptyTableText === true) {
            tableBody.innerHTML = ''
        } else {
            tableBody.innerHTML = '<tr class="text-center"><td colspan="100%">'
                                  + emptyTableText + '</td></tr>';
        }
        document.querySelectorAll('#food-bill-finance-table tfoot')
                .forEach(tfoot => tfoot.remove());
    }

    function clearNextLevels(startIndex) {
        const toClearFunctions = [
            clearEconomicSubCode, clearFoodBillSubmissionConfigId, clearTable
        ];
        for (let i = startIndex; i < toClearFunctions.length; i++) {
            toClearFunctions[i]();
        }
    }

    function budgetMappingIdChanged() {
        clearNextLevels(0);
    }

    function economicSubCodeIdChanged() {
        clearNextLevels(1);
    }

    async function foodBillSubmissionConfigIdChanged() {
        const budgetMappingId = $budgetMappingIdSelect.val();
        const economicSubCodeId = $economicSubCodeIdSelect.val();
        const foodBillSubmissionConfigId = $foodBillSubmissionConfigIdSelect.val();
        const isInputsInvalid = budgetMappingId === ''
                                || economicSubCodeId === ''
                                || foodBillSubmissionConfigId === '';
        if (isInputsInvalid) {
            console.warn("required arguments missing. not calling ajax");
            return;
        }
        const url = 'Food_billServlet?actionType=ajax_getBillDataForFinance'
                    + '&budgetMappingId=' + budgetMappingId
                    + '&economicSubCodeId=' + economicSubCodeId
                    + '&foodBillSubmissionConfigId=' + foodBillSubmissionConfigId;

        const tableBody = document.querySelector('#food-bill-finance-table tbody.main-tbody');
        const templateRow = document.querySelector('#food-bill-finance-table tr.template-row');

        try {
            fullPageLoader.show();
            $actionButtonDiv.hide();
            clearTable();

            const response = await fetch(url);
            const resJson = await response.json();
            if (resJson.success == null || resJson.success === false) {
                let errorMessage = isLangEn ? 'Server Error' : 'সার্ভারে সমস্যা';
                if (resJson.error != null) {
                    errorMessage = resJson.error;
                }
                tableBody.innerHTML =
                    '<tr><td colspan="100%" class="text-center" style="font-weight: bold">'
                    + errorMessage
                    + '</td></tr>';
                return;
            }
            foodBillSummaryModels = resJson.foodBillSummaryModels;
            billAmount = resJson.billAmount;
            totalEmployeeCount = resJson.totalEmployeeCount;
            if (foodBillSummaryModels.length === 0) {
                clearTable();
                $actionButtonDiv.hide();
            } else {
                clearTable(true);
                $actionButtonDiv.show();
                for (let i = 0; i < foodBillSummaryModels.length; ++i) {
                    foodBillSummaryModels[i].serialNo = convertToBanglaNumIfBangla(String(i + 1), '<%=Language%>');
                    showModelInTable(tableBody, templateRow, foodBillSummaryModels[i]);
                }
                showAllRowTotal();
            }
        }  catch (error) {
            console.error(error);
        } finally {
            fullPageLoader.hide();
        }
    }

    function setRowData(templateRow, foodBillSummaryModel) {
        const rowDataPrefix = 'row-data-';
        for (const key in foodBillSummaryModel) {
            const td = templateRow.querySelector('.' + rowDataPrefix + key);
            if (!td) continue;
            td.innerText = foodBillSummaryModel[key];
        }
    }

    function showModelInTable(tableBody, templateRow, foodBillSummaryModel) {
        const modelRow = templateRow.cloneNode(true);
        modelRow.classList.remove('template-row');
        modelRow.querySelector('input[name="foodBillId"]').value = foodBillSummaryModel.foodBillId;
        const detailsButton = modelRow.querySelector('td.details-button button');
        detailsButton.onclick = function () {
            location.href = 'Food_billServlet?actionType=view&ID=' + foodBillSummaryModel.foodBillId;
        }
        setRowData(modelRow, foodBillSummaryModel);
        tableBody.append(modelRow);
    }

    function showAllRowTotal() {
        const totalTitleColSpan = 2;
        const totalTitle = '<%=getDataByLanguage(Language, "সর্বমোট", "Total")%>';
        setupTotalRowWithKnownValues([totalEmployeeCount, billAmount], 'food-bill-finance-table', [2, 3], totalTitle, totalTitleColSpan, 0);
    }

    function submitForm(source) {
        if (foodBillSummaryModels.length === 0) {
            console.warn("foodBillSummaryModels is empty! Unable to submit");
            return;
        }
        if (isAlreadyAdded) {
            submitAfterConfirmation(source);
            return;
        }
        let message;
        if (isLangEn) {
            message = 'Food Bill for '
                      + foodBillSummaryModels.length
                      + ' offices shall be recorded in Budget Register and Bill Register. Are you sure?'
        } else {
            message = 'মোট '
                      + convertToBanglaNumIfBangla(String(foodBillSummaryModels.length), 'bangla')
                      + ' টি দপ্তরের খাবার ভাতা বাজেট রেজিস্টার ও বিল রেজিস্টারে নিবন্ধিত হতে যাচ্ছে। আপনি কি নিশ্চিত?'
        }
        const confirmButtonText = '<%=StringUtils.getYesNo(Language, true)%>';
        const cancelButtonText = '<%=StringUtils.getYesNo(Language, false)%>';
        messageDialog('', message, 'success', true, confirmButtonText, cancelButtonText,
            () => {
                submitAfterConfirmation(source);
            }, () => {
                setButtonDisableState(false);
            }
        );
    }

    function submitAfterConfirmation(source) {
        if (!form.valid()) return;
        setButtonDisableState(true);
        fullPageLoader.show();
        $.ajax({
            type: "POST",
            url: "Food_billServlet?actionType=ajax_prepareBill_" + actionName + "&source=" + source,
            data: form.serialize(),
            dataType: 'JSON',
            success: function (response) {
                setButtonDisableState(false);
                fullPageLoader.hide();
                if (response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                } else if (response.responseCode === 200) {
                    window.location.replace(getContextPath() + response.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                             + ", Message: " + errorThrown);
                setButtonDisableState(false);
                fullPageLoader.hide();
            }
        });
    }

    function setButtonDisableState(value) {
        $submitBtn.prop('disabled', value);
    }
</script>






