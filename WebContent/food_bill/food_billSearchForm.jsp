<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="overtime_allowance.*" %>
<%@ page import="util.*" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="util.StringUtils" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="overtime_bill.Overtime_billStatus" %>
<%@ page import="food_bill.Food_billDTO" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="parliament_session.Parliament_sessionRepository" %>
<%@ page import="bill_approval_history.BillApprovalStatus" %>
<%@page pageEncoding="UTF-8" %>

<%
    String navigator2 = "navOVERTIME_ALLOWANCE";
    String servletName = "Food_billServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead class="text-center">
        <tr>
            <th>
                <%=isLanguageEnglish ? "Bill Type" : "বিলের ধরণ"%>
            </th>
            <th>
                <%=isLanguageEnglish? "Office/Section" : "কার্যালয়/শাখা"%>
            </th>
            <th>
                <%=isLanguageEnglish? "Manpower" : "জনবল"%>
            </th>
            <th>
                <%=isLanguageEnglish? "Amount" : "পরিমাণ"%>
            </th>
            <th>
                <%=isLanguageEnglish? "Preparer & Date" : "প্রস্তুতকারী ও প্রস্তুতির সময়"%>
            </th>
            <th>
                <%=isLanguageEnglish? "Approval Status" : "অনুমোদনের অবস্থা"%>
            </th>
            <th>
                <%=isLanguageEnglish? "Action" : "কার্যকলাপ"%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Food_billDTO> data = (List<Food_billDTO>) recordNavigator.list;
            try {
                if (data != null) {
                    for (Food_billDTO foodBillDTO :  data) {
                        BillApprovalStatus billApprovalStatus = BillApprovalStatus.getFromValue(foodBillDTO.approvalStatus);
        %>
                        <tr>
                            <td>
                                <%=foodBillDTO.getBillNameText(isLanguageEnglish)%>
                            </td>
                            <td>
                                <%=Office_unitsRepository.getInstance().geText(Language, foodBillDTO.officeUnitId)%>
                            </td>
                            <td>
                                <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.format("%d", foodBillDTO.employeeCount))%>
                            </td>
                            <td>
                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                        StringUtils.convertBanglaIfLanguageIsBangla(
                                                Language,
                                                String.format("%d", foodBillDTO.billAmount)
                                        )
                                )%>/-
                            </td>
                            <td>
                                <%if(foodBillDTO.isInPreviewStage){%>
                                <div class="btn btn-sm border-0 shadow mt-2"
                                     style="background-color: <%=billApprovalStatus.getColor()%>; color: white; border-radius: 8px;cursor: text">
                                    <%=isLanguageEnglish ? "Draft" : "খসড়া"%>
                                </div>
                                <%} else {%>
                                <%=foodBillDTO.preparedNameBn%><br>
                                <%=StringUtils.getFormattedTime(false, foodBillDTO.preparedTime)%>
                                <%}%>
                            </td>
                            <td>
                                <%if(!foodBillDTO.isInPreviewStage){%>
                                <div class="btn btn-sm border-0 shadow mt-2"
                                     style="background-color: <%=billApprovalStatus.getColor()%>; color: white; border-radius: 8px;cursor: text">
                                    <%=billApprovalStatus.getName(isLanguageEnglish)%>
                                </div>
                                <%}%>
                            </td>
                            <td>
                                <button
                                        type="button"
                                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                                        style="color: #ff6b6b;"
                                        onclick="location.href='<%=servletName%>?actionType=view&ID=<%=foodBillDTO.iD%>'"
                                >
                                    <i class="fa fa-eye"></i>
                                </button>

                                <%if(foodBillDTO.isEditable(userDTO, Language)){%>
                                <button
                                        type="button"
                                        class="btn-sm border-0 shadow btn-border-radius text-white"
                                        style="background-color: #ff6b6b;"
                                        onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=foodBillDTO.iD%>'"
                                >
                                    <i class="fa fa-edit"></i>
                                </button>
                                <%}%>
                                <%if(foodBillDTO.isDeletableByUser(userDTO)){%>
                                <button type="button"
                                        data-delete-btn
                                        class="btn-sm border-0 shadow btn-border-radius text-white"
                                        style="background-color: #ff6b6b;"
                                        onclick="openDeleteWarning(<%=foodBillDTO.iD%>)"
                                >
                                    <i class="fa fa-trash"></i>
                                </button>
                                <%}%>
                            </td>

                        </tr>
        <%
                    }
                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>


<script>
    function openDeleteWarning(foodBillId) {
        const message = utilIsBangla ? "আপনি কি রেকর্ড মুছে ফেলার ব্যাপারে নিশ্চিত?"
                                     : "Are you sure you want to delete the record?";
        deleteDialog(
            message,
            () => deleteFoodBillDTO(foodBillId),
            () => {}
        );
    }

    async function deleteFoodBillDTO(id) {
        const url = '<%=servletName%>?actionType=ajax_deleteBill&ID=' + id;
        try {
            const res = await fetch(url, {method: 'POST'});
            const resJson = await res.json();
            console.log(resJson);
            if (resJson.success === true) {
                location.reload();
                return;
            }
            $('#toast_message').css('background-color', '#ff6063');
            showToast(resJson.message, resJson.message);
        } catch (error) {
            console.log(error);
            $('#toast_message').css('background-color', '#ff6063');
            showToast('<%=isLanguageEnglish? "Failed to delete" : "ডিলিট করতে ব্যর্থ হয়েছে"%>');
        }
    }
</script>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>