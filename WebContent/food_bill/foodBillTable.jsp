<%@ page import="util.StringUtils" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="overtime_bill.OvertimeType" %>
<%@ page import="food_allowance.Food_allowanceModel" %>
<%@ page import="java.util.TreeMap" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="java.util.Map" %>
<%@ page import="food_bill.Food_billTableData" %>
<%@page pageEncoding="UTF-8" %>

<%
    String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;

    Food_billTableData foodBillTableData = (Food_billTableData) request.getAttribute("foodBillTableData");
    Food_allowanceModel.fixOrdering(foodBillTableData.allowanceModels);
    boolean setEmptyDataError = foodBillTableData.errorMessage == null &&
                                (foodBillTableData.allowanceModels == null || foodBillTableData.allowanceModels.isEmpty());
    if (setEmptyDataError) {
        foodBillTableData.errorMessage = isLangEn ? "No new information found to add" : "যোগ করার মত নতুন কোন তথ্য পাওয়া যায়নি";
    }

    Gson gson = new Gson();
    Map<Long, Long> billDateBySortedMonth =
            foodBillTableData
                    .sortedBillDates
                    .stream()
                    .collect(Collectors.groupingBy(
                            DateUtils::get1stDayOfMonth,
                            TreeMap::new,
                            Collectors.counting()
                    ));
%>

<%if (foodBillTableData.errorMessage != null) {%>

<input type="hidden" name="isEditable" value="false">
<div id="no-data-found-div" class="no-data-found-div">
    <%=foodBillTableData.errorMessage%>
</div>

<%} else {%>

<input type="hidden" name="isEditable" value="<%=foodBillTableData.isEditable%>">

<style>
    .serial-column {
        max-width: 12ch;
    }

    .erase-column {
        max-width: 10ch;
    }
</style>

<div class="table-responsive">
    <table class="table table-bordered text-nowrap">
        <thead>
        <tr>
            <%if (Boolean.TRUE.equals(foodBillTableData.isEditable)) {%>
            <th rowspan="2" class="erase-column">
                <%=isLangEn ? "Erase" : "মুছুন"%>
            </th>
            <%}%>
            <th rowspan="2" class="serial-column">
                <%=isLangEn ? "Serial<br>No" : "ক্রমিক<br>নং"%>
            </th>
            <th rowspan="2">
                <%=isLangEn ? "Employee" : "কর্মকর্তা/কর্মচারী"%>
            </th>
            <%for (Map.Entry<Long, Long> monthBillDateCount : billDateBySortedMonth.entrySet()) {%>
            <td class="text-center" colspan="<%=monthBillDateCount.getValue()%>">
                <%=DateUtils.getMonthYear(monthBillDateCount.getKey(), language, "/")%>
            </td>
            <%}%>
            <th rowspan="2">
                <%=isLangEn ? "Total Days" : "মোট দিন"%>
            </th>
        </tr>
        <tr>
            <%
                for (long billDate : foodBillTableData.sortedBillDates) {
                    int dayOfTheMonth = DateUtils.getDayOfTheMonth(billDate);
            %>
            <th>
                <%=isLangEn ? dayOfTheMonth : StringUtils.convertToBanNumber(String.format("%d", dayOfTheMonth))%>
            </th>
            <%}%>
        </tr>
        </thead>

        <tbody>
        <%if (Boolean.TRUE.equals(foodBillTableData.isEditable)) {%>
        <input type="hidden"
               id="deletedFoodAllowanceIds"
               value="<%=gson.toJson(foodBillTableData.deletedFoodAllowanceIds)%>">
        <%}%>
        <%for (Food_allowanceModel model : foodBillTableData.allowanceModels) {%>
        <tr
                <%if (foodBillTableData.isEditable) {%>
                data-model='<%=gson.toJson(model)%>' class="has-allowance-model"
                <%}%>
        >
            <%if (Boolean.TRUE.equals(foodBillTableData.isEditable)) {%>
            <td class="erase-column">
                <button type="button"
                        class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #ff6b6b;"
                        onclick="eraseBtnClicked(this)">
                    <i class="fa fa-trash"></i>
                </button>
            </td>
            <%}%>
            <td class="text-center serial-column">
                <%if (!foodBillTableData.isEditable) {%>
                <%=StringUtils.convertBanglaIfLanguageIsBangla(language, String.format("%d", model.serialNo))%>.
                <%} else {%>
                <input type="number" min="1"
                       class='form-control rounded shadow-sm' style="min-width: 10ch; max-width: 12ch;"
                       name="serialNo"
                       value="<%=model.serialNo%>" onchange="changeSerialNumber(this)"/>
                <%}%>
            </td>
            <td style="position: sticky; left: 0; background-color: white">
                <%=model.name%><br>
                <%=model.organogramName%>
            </td>
            <%
                for (int i = 0; i < foodBillTableData.sortedBillDates.size(); ++i) {
                    int overtimeTypeIntValue = model.sortedBillDateOtTypeList.get(i);
                    OvertimeType overtimeType = OvertimeType.getByIntValue(overtimeTypeIntValue);
            %>
            <td class="text-center">
                <%if (!foodBillTableData.isEditable) {%>
                <%=overtimeType.getName()%>
                <%} else {%>
                <select class='form-control rounded shadow-sm' style="min-width: 10ch; max-width: 14ch;"
                        onchange="changeOvertimeTypeDaysCount(this, <%=i%>)">
                    <%=OvertimeType.buildOption(overtimeType, language)%>
                </select>
                <%}%>
            </td>
            <%}%>
            <td class="days-count text-center">
                <%=StringUtils.convertBanglaIfLanguageIsBangla(language, String.format("%d", model.days))%>
            </td>
        </tr>
        <%}%>
        </tbody>
    </table>
</div>
<%}%>