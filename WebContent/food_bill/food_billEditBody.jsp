<%@page pageEncoding="UTF-8" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="user.UserDTO" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="food_bill.Food_billServlet" %>
<%@ page import="food_bill.Food_billDTO" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="language.LM" %>
<%@ page import="language.LC" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="overtime_bill.OvertimeType" %>
<%@ page import="food_allowance.Food_allowanceModel" %>
<%@ page import="java.util.List" %>
<%@ page import="parliament_session.Parliament_sessionRepository" %>
<%@ page import="food_bill_submission_config.Food_bill_submission_configDAO" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    Office_unitsDTO officeUnitsDTO = Food_billServlet.getOfficeUnitFromUser(userDTO);
    boolean canSelectOffice = Food_billServlet.isAllowedToSeeAllOfficeBill(userDTO);

    String context = request.getContextPath() + "/";
    boolean isLanguageEnglish = "english".equalsIgnoreCase(Language);
    String formTitle = isLanguageEnglish ? "FOOD BILL SUBMIT (OFFICE/SECTION)" : "খাবার ভাতার বিল জমাদান (কার্যালয়/শাখা)";

    String actionName = "ajax_add";
    boolean isEditPage = false;

    Food_billDTO foodBillDTO = (Food_billDTO) request.getAttribute("foodBillDTO");

    if ("getEditPage".equalsIgnoreCase(request.getParameter("actionType"))) {
        actionName = "ajax_edit";
        isEditPage = true;
    }
    String notEditableCause = (String) request.getAttribute("notEditableCause");
%>


<style>
    table th {
        vertical-align: top;
        text-align: center;
    }

    .loader-container-circle {
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .2);
        position: fixed;
        top: 0;
        left: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: 10;
        visibility: visible;
    }

    @media (min-width: 1015px) {
        .loader-container-circle {
            width: calc(100% + 260px);
            height: 100%;
            background-color: rgba(0, 0, 0, 0.1);
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            z-index: 3;
            visibility: visible;
        }
    }

    .loader-circle {
        width: 50px;
        height: 50px;
        border: 5px solid;
        color: #3498db;
        border-radius: 50%;
        border-top-color: transparent;
        animation: loader 1.2s linear infinite;
    }

    @keyframes loader {
        25% {
            color: #2ecc71;
        }
        50% {
            color: #f1c40f;
        }
        75% {
            color: #e74c3c;
        }
        to {
            transform: rotate(360deg);
        }
    }

    .no-data-found-div {
        text-align: center;
        background-color: #eee;
        padding: .875rem;
    }

    .template-div {
        display: none !important;
    }

    .height-auto {
        height: auto !important;
    }
</style>

<div class="loader-container-circle" id="full-page-loader">
    <div class="loader-circle"></div>
</div>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal" id="overtime-bill-form" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 row form-group">
                        <label class="col-md-3 col-form-label text-md-right">
                            <%=LM.getText(LC.HM_OFFICE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <%if (isEditPage) {%>
                            <div class="form-control height-auto">
                                <%=Office_unitsRepository.getInstance().geText(Language, foodBillDTO.officeUnitId)%>
                                <input type="hidden" name='officeUnitId' id='office_units_id_input'
                                       value="<%=foodBillDTO.officeUnitId%>">
                            </div>
                            <%} else if (canSelectOffice) {%>
                            <button type="button"
                                    class="btn btn-block shadow btn-primary text-white btn-border-radius"
                                    id="office_units_id_modal_button"
                                    onclick="officeModalButtonClicked();">
                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                            </button>
                            <div class="input-group" id="office_units_id_div" style="display: none">
                                <input type="hidden" name='officeUnitId' id='office_units_id_input' value="">
                                <button type="button"
                                        class="btn btn-secondary btn-block shadow-sm btn-border-radius form-control height-auto"
                                        id="office_units_id_text"
                                        onclick="officeModalEditButtonClicked()">
                                </button>
                                <span class="input-group-btn" style="width: 5%;padding-right: 5px;"
                                      tag='pb_html'>
                                    <button type="button" class="btn btn-outline-danger"
                                            style="height: 100%;"
                                            onclick="crsBtnClicked('office_units_id');"
                                            id='office_units_id_crs_btn' tag='pb_html'>
                                        &times;
                                    </button>
                                </span>
                            </div>
                            <%} else if (officeUnitsDTO != null) {%>
                            <div class="form-control height-auto">
                                <%=isLanguageEnglish ? officeUnitsDTO.unitNameEng : officeUnitsDTO.unitNameBng%>
                                <input type="hidden" name='officeUnitId' id='office_units_id_input'
                                       value="<%=officeUnitsDTO.iD%>">
                            </div>
                            <%} else {%>
                            <div class="form-control height-auto">
                                <%=isLanguageEnglish ? "Could not find users Office" : "ইউজারের দপ্তর পাওয়া যায়নি"%>
                                <input type="hidden" name='officeUnitId' id='office_units_id_input' value="-1">
                            </div>
                            <%}%>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 row form-group">
                        <label class="col-md-3 col-form-label text-md-right" for="foodBillSubmissionConfigId">
                            <%=isLanguageEnglish ? "Parliament Session" : "সংসদ অধিবেশন"%>
                        </label>
                        <div class="col-md-9">
                            <%if (isEditPage) {%>
                            <div class="form-control rounded shadow-sm">
                                <%=Food_bill_submission_configDAO.getInstance().getTextById(foodBillDTO.foodBillSubmissionConfigId, isLanguageEnglish)%>
                                <input type="hidden"
                                       id="foodBillSubmissionConfigId"
                                       name="foodBillSubmissionConfigId"
                                       value="<%=foodBillDTO.foodBillSubmissionConfigId%>">
                            </div>
                            <%} else {%>
                            <select id="foodBillSubmissionConfigId"
                                    name='foodBillSubmissionConfigId'
                                    class='form-control rounded shadow-sm'
                                    onchange="loadData()"
                            >
                                <%=Food_bill_submission_configDAO.getInstance().buildOptionsOfSubmittableDTOs(
                                        officeUnitsDTO == null ? null : officeUnitsDTO.iD,
                                        System.currentTimeMillis(),
                                        Language
                                )%>
                            </select>
                            <%}%>
                        </div>
                    </div>
                </div>

                <%if (notEditableCause != null) {%>
                <div class="container text-center m-4 text-danger">
                    <h5>
                        <%=notEditableCause%>
                    </h5>
                </div>
                <%}%>

                <div id="food-bill-ajax-data-table-div"
                     class="mt-4 w-100"
                     style="overflow-x: scroll"
                >
                    <%if (isEditPage) {%>
                    <jsp:include page="foodBillTable.jsp"/>
                    <%} else {%>
                    <div class="no-data-found-div">
                        <%=isLanguageEnglish ? "No Data Found" : "কোন তথ্য পাওয়া যায়নি"%>
                    </div>
                    <%}%>
                </div>

                <div id="no-data-found-div" class="template-div no-data-found-div">
                    <%=isLanguageEnglish ? "No Data Found" : "কোন তথ্য পাওয়া যায়নি"%>
                </div>

                <div id="action-btn-div" class="row" style="display: none">
                    <div class="col-12 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button"
                                onclick="location.href = '<%=request.getHeader("referer")%>'">
                            <%=isLanguageEnglish ? "Cancel" : "বাতিল করুন"%>
                        </button>
                        <button id="preview-btn"
                                class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                                type="button" onclick="submitForm()">
                            <%=isLanguageEnglish ? "See Preview" : "প্রিভিউ দেখুন"%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script>
    const noDataFoundDiv = document.getElementById('no-data-found-div');
    const ajaxDataTableDiv = document.getElementById('food-bill-ajax-data-table-div');
    const isEditPage = <%=isEditPage%>;
    const fullPageLoader = $('#full-page-loader');
    const actionBtnDiv = $('#action-btn-div');
    const foodBillSubmissionConfigIdInput = $('#foodBillSubmissionConfigId');
    const officeUnitsIdInput = $('#office_units_id_input');
    let foodBillInputsValid = false;

    function eraseBtnClicked(btnElement) {
        const nearestTr = btnElement.closest('tr.has-allowance-model');
        const foodAllowanceModel = JSON.parse(nearestTr.dataset.model);
        if(foodAllowanceModel.foodAllowanceId > 0) {
            const deletedFoodAllowanceIdsInput = document.getElementById('deletedFoodAllowanceIds');
            const deletedFoodAllowanceIds = JSON.parse(deletedFoodAllowanceIdsInput.value);
            deletedFoodAllowanceIds.push(foodAllowanceModel.foodAllowanceId);
            deletedFoodAllowanceIdsInput.value = JSON.stringify(deletedFoodAllowanceIds);
        }
        nearestTr.remove();
        [...document.querySelectorAll('input[name="serialNo"]')]
            .forEach((serialNoInput, index) => {
                serialNoInput.value = index + 1;
                changeSerialNumber(serialNoInput);
            });
    }

    $(() => {
        fullPageLoader.hide();
        if (isEditPage) {
            actionBtnDiv.show();
        } else {
            actionBtnDiv.hide();
        }
    });

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
    }

    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        $('#office_units_id_modal_button').hide();
        $('#office_units_id_div').show();
        $('#office_error_id').hide();

        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        officeUnitsIdInput.val(selectedOffice.id);
    }

    function clearTable() {
        ajaxDataTableDiv.innerHTML = '';
        const noDataFoundDivClone = noDataFoundDiv.cloneNode(true);
        noDataFoundDivClone.classList.remove('template-div');
        ajaxDataTableDiv.append(noDataFoundDivClone);
    }

    async function loadData() {
        const foodBillSubmissionConfigId = foodBillSubmissionConfigIdInput.val();
        const officeUnitsId = officeUnitsIdInput.val();

        clearTable();
        if (foodBillSubmissionConfigId === '' || officeUnitsId === '') {
            return;
        }
        foodBillInputsValid = true;
        fullPageLoader.show();

        console.log({
            foodBillSubmissionConfigId,
            officeUnitsId
        });
        const url = 'Food_billServlet?actionType=ajax_getFoodBillData'
                    + '&foodBillSubmissionConfigId=' + foodBillSubmissionConfigId
                    + '&officeUnitsId=' + officeUnitsId;
        const response = await fetch(url);
        ajaxDataTableDiv.innerHTML = await response.text();
        const isEditableInput = ajaxDataTableDiv.querySelector('input[name="isEditable"]');
        const isEditable = isEditableInput && (isEditableInput.value === 'true');
        if (isEditable) {
            actionBtnDiv.show();
        } else {
            actionBtnDiv.hide();
        }
        fullPageLoader.hide();
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: function (selectedOffice) {
                viewOfficeIdInInput(selectedOffice);
                loadData();
            }
        }]
    ]);

    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function officeModalEditButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        officeSearchSetSelectedOfficeLayers(officeUnitsIdInput.val());
        $('#search_office_modal').modal();
    }

    const KA_VALUE = <%=OvertimeType.KA.getIntValue()%>;

    function calculateAndSetDays(foodAllowanceModel) {
        let days = 0;
        for (let overtimeType of foodAllowanceModel.sortedBillDateOtTypeList) {
            if (overtimeType === KA_VALUE) ++days;
        }
        foodAllowanceModel.days = days;
    }

    function changeOvertimeTypeDaysCount(selectItem, dayIndex) {
        const nearestTr = selectItem.closest('tr.has-allowance-model');
        const foodAllowanceModel = JSON.parse(nearestTr.dataset.model);
        foodAllowanceModel.sortedBillDateOtTypeList[dayIndex] = Number(selectItem.value);
        calculateAndSetDays(foodAllowanceModel);
        const daysCountTd = nearestTr.querySelector('td.days-count');
        daysCountTd.innerText = convertToBanglaNumIfBangla(String(foodAllowanceModel.days), '<%=Language%>');
        nearestTr.dataset.model = JSON.stringify(foodAllowanceModel);
    }

    function changeSerialNumber(selectItem) {
        const nearestTr = selectItem.closest('tr.has-allowance-model');
        const foodAllowanceModel = JSON.parse(nearestTr.dataset.model);
        foodAllowanceModel.serialNo = Number(selectItem.value);
        nearestTr.dataset.model = JSON.stringify(foodAllowanceModel);
    }

    const id = '<%=isEditPage? foodBillDTO.iD : -1%>';

    function submitForm(source) {
        const foodBillSubmissionConfigId = foodBillSubmissionConfigIdInput.val();
        const officeUnitsId = officeUnitsIdInput.val();
        if (officeUnitsId === '') {
            $('#toast_message').css('background-color', '#ff6063');
            showToast("দপ্তর বাছাই করুন", "Select Office");
            return;
        }

        if (foodBillSubmissionConfigId === '') {
            $('#toast_message').css('background-color', '#ff6063');
            showToast("সংসদ অধিবেশন বাছাই করুন", "Select Parliament Session");
            return;
        }

        const foodAllowanceModels = Array.from(document.querySelectorAll('tr.has-allowance-model'))
                                         .map(tableTr => JSON.parse(tableTr.dataset.model));
        if (foodAllowanceModels.length === 0) {
            $('#toast_message').css('background-color', '#ff6063');
            showToast("জমা দেবার মত কোন তথ্য নেই", "No Data To Submit");
            return;
        }
        const data = {
            id: id,
            officeUnitsId: officeUnitsId,
            foodBillSubmissionConfigId: foodBillSubmissionConfigId,
            foodAllowanceModels: JSON.stringify(foodAllowanceModels),
            deletedFoodAllowanceIds: document.getElementById('deletedFoodAllowanceIds').value,
            source: "preview"
        };
        console.log({data});

        setButtonDisableState(true);
        fullPageLoader.show();
        $.ajax({
            type: "POST",
            url: "Food_billServlet?actionType=<%=actionName%>",
            data: data,
            dataType: 'JSON',
            success: function (response) {
                setButtonDisableState(false);
                fullPageLoader.hide();
                if (response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                } else if (response.responseCode === 200) {
                    window.location.replace(getContextPath() + response.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                setButtonDisableState(false);
                fullPageLoader.hide();
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                             + ", Message: " + errorThrown);
            }
        });
    }

    function setButtonDisableState(value) {
        $('#preview-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }
</script>