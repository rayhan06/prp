<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="language.LM" %>
<%@ page import="language.LC" %>
<%@ page import="food_bill_submission_config.Food_bill_submission_configDAO" %>
<%@ page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String formTitle = isLangEn ? "OFFICES NOT SUBMITTED FOOD BILL" : "খাবার ভাতা জমা দেয়নি এমন অফিস";
    String context = request.getContextPath() + "/";
%>

<style>
    .template-row {
        display: none;
    }

    #food-bill-missing-offices-table tfoot {
        text-align: right;
    }

    .loader-container-circle {
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .2);
        position: fixed;
        top: 0;
        left: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: 10;
        visibility: visible;
    }

    @media (min-width: 1015px) {
        .loader-container-circle {
            width: calc(100% + 260px);
            height: 100%;
            background-color: rgba(0, 0, 0, 0.1);
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            z-index: 3;
            visibility: visible;
        }
    }

    .loader-circle {
        width: 50px;
        height: 50px;
        border: 5px solid;
        color: #3498db;
        border-radius: 50%;
        border-top-color: transparent;
        animation: loader 1.2s linear infinite;
    }

    @keyframes loader {
        25% {
            color: #2ecc71;
        }
        50% {
            color: #f1c40f;
        }
        75% {
            color: #e74c3c;
        }
        to {
            transform: rotate(360deg);
        }
    }
</style>

<div class="loader-container-circle" id="full-page-loader">
    <div class="loader-circle"></div>
</div>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal" id="overtime-allowance-form" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="ml-auto m-3">
                    <button type="button" class="btn" id='download-pdf'
                            onclick="printDivWithJqueryPrint('to-print-div');">
                        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                </div>
                <section class="page" id="to-print-div">
                    <div class="row">
                        <div class="col-12 row mt-2">
                            <div class="col-sm-6 col-md-4 form-group">
                                <label class="h5" for="budgetInstitutionalGroup">
                                    <%=LM.getText(LC.BUDGET_INSTITUTIONAL_GROUP, loginDTO)%>
                                </label>
                                <select id="budgetInstitutionalGroup"
                                        name='budgetInstitutionalGroupId'
                                        class='form-control rounded shadow-sm'
                                        onchange="institutionalGroupChanged(this);"
                                >
                                    <%=Budget_institutional_groupRepository.getInstance().buildOptions(
                                            Language, 0L, false
                                    )%>
                                </select>
                            </div>

                            <div class="col-sm-6 col-md-4 form-group">
                                <label class="h5" for="budgetOffice">
                                    <%=LM.getText(LC.BUDGET_OFFICE, loginDTO)%>
                                </label>
                                <select id="budgetOffice" name='budgetOfficeId'
                                        class='form-control rounded shadow-sm'
                                        onchange="budgetOfficeChanged(this);">
                                    <%--Dynamically Added with AJAX--%>
                                </select>
                            </div>

                            <div class="col-sm-6 col-md-4 form-group">
                                <label class="h5" for="foodBillSubmissionConfigId">
                                    <%=isLangEn ? "Parliament Session" : "সংসদ অধিবেশন"%>
                                </label>
                                <select id="foodBillSubmissionConfigId" name="foodBillSubmissionConfigId"
                                        class='form-control rounded shadow-sm'
                                        onchange="foodBillSubmissionConfigIdChanged();"
                                >
                                    <%=Food_bill_submission_configDAO.getInstance().buildOptions(Language, null)%>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="mt-4 w-100">
                        <table id="food-bill-missing-offices-table"
                               class="table table-bordered table-striped text-nowrap">
                            <thead>
                            <tr class="text-center">
                                <th>
                                    <%=isLangEn ? "Serial No" : "ক্রমিক নং"%>
                                </th>
                                <th>
                                    <%=isLangEn ? "Office/Section" : "কার্যালয়/শাখা"%>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="main-tbody">
                            </tbody>

                            <tr class="template-row">
                                <input type="hidden" name="overtimeBillId" value="-1">
                                <td class="row-data-serialNo text-center"></td>
                                <td class="row-data-officeName text-center"></td>
                            </tr>
                        </table>
                    </div>
                </section>
            </div>
        </form>
    </div>
</div>

<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<jsp:include page="../utility/jquery_print.jsp"/>

<script type="text/javascript">
    const fullPageLoader = $('#full-page-loader');
    const foodBillSubmissionConfigIdSelect = document.getElementById('foodBillSubmissionConfigId');

    $(document).ready(function () {
        fullPageLoader.hide();
    });

    function clearOffice() {
        document.getElementById('budgetOffice').innerHTML = '';
    }

    function clearTable(hideEmptyTableText) {
        const tableBody = document.querySelector('#food-bill-missing-offices-table tbody.main-tbody');
        const emptyTableText = '<%=getDataByLanguage(Language, "কোন তথ্য পাওয়া যায়নি", "No data found")%>';
        if (hideEmptyTableText === true) {
            tableBody.innerHTML = ''
        } else {
            tableBody.innerHTML = '<tr class="text-center"><td colspan="100%">'
                                  + emptyTableText + '</td></tr>';
        }
        document.querySelectorAll('#food-bill-missing-offices-table tfoot')
                .forEach(tfoot => tfoot.remove());
    }

    function clearFoodBillSubmissionConfigId() {
        foodBillSubmissionConfigIdSelect.value = '';
    }

    function clearNextLevels(startIndex) {
        const toClearFunctions = [
            clearOffice, clearFoodBillSubmissionConfigId, clearTable
        ];
        for (let i = startIndex; i < toClearFunctions.length; i++) {
            toClearFunctions[i]();
        }
    }

    async function institutionalGroupChanged(selectElement) {
        const selectedInstitutionalGroupId = selectElement.value;
        clearNextLevels(0);
        if (selectedInstitutionalGroupId === '') return;
        const url = 'Budget_mappingServlet?actionType=getBudgetOfficeList&withCode=false'
                    + '&budget_instituitional_group_id=' + selectedInstitutionalGroupId;

        const response = await fetch(url);
        document.getElementById('budgetOffice').innerHTML = await response.text();
    }

    function budgetOfficeChanged() {
        clearNextLevels(1);
    }

    async function foodBillSubmissionConfigIdChanged() {
        const budgetOfficeId = document.getElementById('budgetOffice').value;
        const foodBillSubmissionConfigId = foodBillSubmissionConfigIdSelect.value;
        if (budgetOfficeId === '' || foodBillSubmissionConfigId === '') {
            console.warn('mandatory data missing not calling ajax');
            return;
        }
        const params = {
            budgetOfficeId,
            foodBillSubmissionConfigId
        };
        fullPageLoader.show();
        try {
            await fetchAndShowData(params);
        } catch (error) {
            console.log(error);
            $('#toast_message').css('background-color', '#ff6063');
            showToastSticky(error.message, error.message);
        }
        fullPageLoader.hide();
    }

    async function fetchAndShowData(params) {
        const url = 'Food_billServlet?actionType=ajax_getMissingOfficesList'
                    + '&budgetOfficeId=' + params.budgetOfficeId
                    + '&foodBillSubmissionConfigId=' + params.foodBillSubmissionConfigId;

        let response, resJson;
        try {
            response = await fetch(url);
            resJson = await response.json();
            console.log({resJson});
            if (!resJson.success) {
                throw new Error(resJson.error);
            }
            resJson = resJson.data;
            console.log({resJson});
        } catch (error) {
            throw error;
        }
        const missingOfficesName = resJson.missingOfficesName;

        const tableBody = document.querySelector('#food-bill-missing-offices-table tbody');
        const templateRow = document.querySelector('#food-bill-missing-offices-table tr.template-row');

        if (missingOfficesName.length === 0) {
            clearTable();
        } else {
            clearTable(true);
            for (let i = 0; i < missingOfficesName.length; ++i) {
                const missingOfficesNameModel = {
                    serialNo: convertToBanglaNumIfBangla(String(i + 1), '<%=Language%>'),
                    officeName: missingOfficesName[i]
                }
                showModelInTable(tableBody, templateRow, missingOfficesNameModel);
            }
        }
    }

    function setRowData(templateRow, overtimeBillSummaryModel) {
        console.log(overtimeBillSummaryModel);
        const rowDataPrefix = 'row-data-';
        for (const key in overtimeBillSummaryModel) {
            const td = templateRow.querySelector('.' + rowDataPrefix + key);
            if (!td) continue;
            td.innerText = overtimeBillSummaryModel[key];
        }
    }

    function showModelInTable(tableBody, templateRow, overtimeBillSummaryModel) {
        console.log(overtimeBillSummaryModel);
        const modelRow = templateRow.cloneNode(true);
        modelRow.classList.remove('template-row');
        setRowData(modelRow, overtimeBillSummaryModel);
        tableBody.append(modelRow);
    }
</script>