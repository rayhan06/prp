<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="java.util.List" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VM_FUEL_REQUEST_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;

	List<Fiscal_yearDTO> fiscal_yearDTOList = Fiscal_yearRepository.getInstance().getFiscal_yearList();
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row mx-2">
    <div class="col-12">
		<div id="visitDate" class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 col-form-label text-md-right">
					 <%=LM.getText(LC.VM_FUEL_REQUEST_REPORT_OTHER_APPLICATION_START_DATE, loginDTO)%>
				</label>
				<div class="col-md-9">
					<jsp:include page="/date/date.jsp">
						<jsp:param name="DATE_ID" value="startDate_js"></jsp:param>
						<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
					</jsp:include>
					<input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
						   data-label="Document Date" id='startDate' name='application_start_date' value=""
						   tag='pb_html'
					/>
				</div>
			</div>
		</div>
		<div id="visitDate_3" class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 col-form-label text-md-right">
					 <%=LM.getText(LC.VM_FUEL_REQUEST_REPORT_OTHER_APPLICATION_END_DATE, loginDTO)%>
				</label>
				<div class="col-md-9">
					<jsp:include page="/date/date.jsp">
						<jsp:param name="DATE_ID" value="endDate_js"></jsp:param>
						<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
					</jsp:include>
					<input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
						   data-label="Document Date" id='endDate' name='application_end_date' value=""
						   tag='pb_html'
					/>
				</div>
			</div>
		</div>


		<div  class="search-criteria-div" style = "display: none;">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-md-right">
					<%=LM.getText(LC.VM_FUEL_REQUEST_REPORT_WHERE_REQUESTEREMPID, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<input class='form-control'  name='requesterEmpId' id = 'requesterEmpId' value=""/>							
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-md-right">
					<%=LM.getText(LC.VM_FUEL_REQUEST_REPORT_WHERE_VEHICLETYPE, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='vehicleType' id = 'vehicleType' >
						<%
							Options = CatDAO.getOptions(Language, "vehicle_type", CatDTO.CATDEFAULT);
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div" >
			<div class="form-group row">
				<label class="col-sm-3 control-label text-md-right">
					<%=LM.getText(LC.VM_FUEL_REQUEST_REPORT_WHERE_FISCALYEARID, loginDTO)%>
				</label>
				<div class="col-sm-9">
<%--					<input class='form-control'  name='fiscalYearId' id = 'fiscalYearId' value=""/>							--%>

						<select class='form-control'  name='fiscalYearId' id = 'fiscalYearId'  >
							<%
								boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
								String fiscalYearOptions = Utils.buildSelectOption(isLanguageEnglish);
								StringBuilder option = new StringBuilder();
								for(Fiscal_yearDTO fiscal_yearDTO:fiscal_yearDTOList){
									option.append("<option value = '").append(fiscal_yearDTO.id).append("'>");
									option.append(isLanguageEnglish ? fiscal_yearDTO.nameEn : fiscal_yearDTO.nameBn).append("</option>");
								}
								fiscalYearOptions+=option.toString();
							%>
							<%=fiscalYearOptions%>
						</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-md-right">
					<%=LM.getText(LC.VM_FUEL_REQUEST_REPORT_WHERE_STATUS, loginDTO)%>
				</label>
				<div class="col-sm-9">
<%--					<input class='form-control'  name='status' id = 'status' value=""/>							--%>

						<select class='form-control'  name='status' id = 'status' >
							<%
								Options = CatDAO.getOptions(Language, "vm_fuel_request_approval_status", CatDTO.CATDEFAULT);
							%>
							<%=Options%>
						</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div" style = "display: none;">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-md-right">
					<%=LM.getText(LC.VM_FUEL_REQUEST_REPORT_WHERE_ISDELETED, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<input class='form-control'  name='isDeleted' id = 'isDeleted' value=""/>							
				</div>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">

$(document).ready(() => {
	showFooter = false;
});

function init()
{
    dateTimeInit($("#Language").val());
}
function PreprocessBeforeSubmiting()
{
}
</script>