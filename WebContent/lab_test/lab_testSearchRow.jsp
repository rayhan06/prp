<%@page pageEncoding="UTF-8" %>

<%@page import="lab_test.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.LAB_TEST_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_LAB_TEST;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Lab_testDTO lab_testDTO = (Lab_testDTO) request.getAttribute("lab_testDTO");
    CommonDTO commonDTO = lab_testDTO;
    String servletName = "Lab_testServlet";


    System.out.println("lab_testDTO = " + lab_testDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Lab_testDAO lab_testDAO = (Lab_testDAO) request.getAttribute("lab_testDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td id='<%=i%>_nameEn' class="text-nowrap">
    <%
        value = lab_testDTO.nameEn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_normalRangeColumnName' class="text-nowrap">
    <%
        value = lab_testDTO.normalRangeColumnName + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_description' class="text-nowrap">
    <%
        value = lab_testDTO.description + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_specimen' class="text-nowrap">
    <%
        value = lab_testDTO.specimen + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td>
<%=Utils.getDigits(lab_testDTO.ordering, Language)%>
</td>

<td>
<%=Utils.getYesNo(lab_testDTO.showInDropdown, Language)%>
</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Lab_testServlet?actionType=view&ID=<%=lab_testDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Lab_testServlet?actionType=getEditPage&ID=<%=lab_testDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td class="text-right" id='<%=i%>_checkbox'>
    <div class='checker'>
        <span class='chkEdit'>
            <input type='checkbox' name='ID' value='<%=lab_testDTO.iD%>'/>
        </span>
    </div>
</td>
																						
											

