<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="lab_test.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    Lab_testDTO lab_testDTO;
    lab_testDTO = (Lab_testDTO) request.getAttribute("lab_testDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (lab_testDTO == null) {
        lab_testDTO = new Lab_testDTO();

    }
    System.out.println("lab_testDTO = " + lab_testDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.LAB_TEST_ADD_LAB_TEST_ADD_FORMNAME, loginDTO);
    String servletName = "Lab_testServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.LAB_TEST_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    boolean isLangEng = Language.equalsIgnoreCase("english");
%>

<style>
    @media(max-width: 991.98px) {
        table input.form-control{
            width: auto!important;
        }
    }

</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Lab_testServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=lab_testDTO.iD%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.LAB_TEST_ADD_NAMEEN, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='nameEn'
                                                   id='nameEn_text_<%=i%>' value='<%=lab_testDTO.nameEn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                          
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.LAB_TEST_ADD_NORMALRANGECOLUMNNAME, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='normalRangeColumnName'
                                                   id='normalRangeColumnName_text_<%=i%>'
                                                   value='<%=lab_testDTO.normalRangeColumnName%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.LAB_TEST_ADD_DESCRIPTION, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='description'
                                                   id='description_text_<%=i%>' value='<%=lab_testDTO.description%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.LAB_TEST_ADD_SPECIMEN, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='specimen'
                                                   id='specimen_text_<%=i%>' value='<%=lab_testDTO.specimen%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=isLangEng?"Order":"ক্রম"%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='ordering' class='form-control' name='ordering'
                                                   id='ordering_text_<%=i%>' value='<%=lab_testDTO.ordering%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=isLangEng?"In Dropdown":"ড্রপডাউনে দেখান"%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='checkbox' class='form-control-sm' name='showInDropdown'
                                                   id='showInDropdown_text_<%=i%>' 
                                                   <%=lab_testDTO.showInDropdown?"checked":""%>
                                                   
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.LAB_TEST_ADD_LAB_TEST_LIST, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped text-nowrap">
                                <thead>
                                <tr>
                                	<th><%=isLangEng?"Order":"ক্রম"%>
                                    </th>
                                    <th><%=LM.getText(LC.LAB_TEST_ADD_LAB_TEST_LIST_NAMEEN, loginDTO)%>
                                    </th>
                                   
                                    <th><%=LM.getText(LC.LAB_TEST_ADD_LAB_TEST_LIST_NORMALRANGE, loginDTO)%>
                                    </th>
                                    <th><%=isLangEng?"Unit":"একক"%>
                                    </th>
                                    <th><%=LM.getText(LC.HM_TITLE, loginDTO)%>
                                    </th>
                                    <th><%=isLangEng?"Show In Dropdown":"ড্রপডাউনে দেখান"%>
                                    </th>
                                    <th><%=LM.getText(LC.LAB_TEST_ADD_LAB_TEST_LIST_REMOVE, loginDTO)%>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="field-LabTestList">


                                <%
                                    if (actionName.equals("edit")) {
                                        int index = -1;


                                        for (LabTestListDTO labTestListDTO : lab_testDTO.labTestListDTOList) {
                                            index++;

                                            System.out.println("index index = " + index);

                                %>

                                <tr id="LabTestList_<%=index + 1%>">
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control' name='labTestList.iD'
                                               id='iD_hidden_<%=childTableStartingID%>' value='<%=labTestListDTO.iD%>'
                                               tag='pb_html'/>

                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control' name='labTestList.labTestId'
                                               id='labTestId_hidden_<%=childTableStartingID%>'
                                               value='<%=labTestListDTO.labTestId%>' tag='pb_html'/>
                                    </td>
                                    <td>


                                        <input type='number' class='form-control' name='labTestList.ordering'
                                               id='ordering_text_<%=childTableStartingID%>'
                                               value='<%=labTestListDTO.ordering%>' tag='pb_html'/>
                                    </td>
                                    
                                    <td>


                                        <input type='text' class='form-control' name='labTestList.nameEn'
                                               id='nameEn_text_<%=childTableStartingID%>'
                                               value='<%=labTestListDTO.nameEn%>' tag='pb_html'/>
                                    </td>
                                  
                                    <td>


                                        <input type='text' class='form-control' name='labTestList.normalRange'
                                               id='normalRange_text_<%=childTableStartingID%>'
                                               value='<%=labTestListDTO.normalRange%>' tag='pb_html'/>
                                    </td>
                                    <td>


                                        <input type='text' class='form-control' name='labTestList.unit'
                                               id='unit_text_<%=childTableStartingID%>'
                                               value='<%=labTestListDTO.unit%>' tag='pb_html'/>
                                    </td>
                                    <td>
                                        <input type='checkbox' class='form-control-sm'
                                               name='labTestList.isTitle_cb'
                                               id='isTitle_checkbox_<%=childTableStartingID%>'
                                               value='true'
                                            <%=labTestListDTO.isTitle ? ("checked") : ""%>
                                               onchange="checkIstitle(this.id)"
                                               tag='pb_html'>
                                        <input type='hidden' name='labTestList.isTitle'
                                               id='isTitle_<%=childTableStartingID%>'
                                               value='<%=labTestListDTO.isTitle%>' tag='pb_html'/>
                                    </td>
                                     <td>
                                        <input type='checkbox' class='form-control-sm'
                                               name='labTestList.showInDropdown_cb'
                                               id='showInDropdown_checkbox_<%=childTableStartingID%>'
                                               value='true'
                                            <%=labTestListDTO.showInDropdown ? ("checked") : ""%>
                                               onchange="checkShowInDropdown(this.id)"
                                               tag='pb_html'>
                                        <input type='hidden' name='labTestList.showInDropdown'
                                               id='showInDropdown_<%=childTableStartingID%>'
                                               value='<%=labTestListDTO.showInDropdown%>' tag='pb_html'/>
                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control' name='labTestList.isDeleted'
                                               id='isDeleted_hidden_<%=childTableStartingID%>'
                                               value='<%=labTestListDTO.isDeleted%>' tag='pb_html'/>

                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='labTestList.lastModificationTime'
                                               id='lastModificationTime_hidden_<%=childTableStartingID%>'
                                               value='<%=labTestListDTO.lastModificationTime%>' tag='pb_html'/>
                                    </td>
                                    <td>
										<span id='chkEdit'>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
                                                   class="form-control-sm"/>
										</span>
                                    </td>
                                </tr>
                                <%
                                            childTableStartingID++;
                                        }
                                    }
                                %>

                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <div class=" text-right mt-3">
                                <button
                                        id="add-more-LabTestList"
                                        name="add-moreLabTestList"
                                        type="button"
                                        class="btn btn-sm text-white add-btn shadow">
                                    <i class="fa fa-plus"></i>
                                    <%=LM.getText(LC.HM_ADD, loginDTO)%>
                                </button>
                                <button
                                        id="remove-LabTestList"
                                        name="removeLabTestList"
                                        type="button"
                                        class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        </div>

                        <%LabTestListDTO labTestListDTO = new LabTestListDTO();%>

                        <template id="template-LabTestList">
                            <tr>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='labTestList.iD' id='iD_hidden_'
                                           value='<%=labTestListDTO.iD%>' tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='labTestList.labTestId'
                                           id='labTestId_hidden_' value='<%=labTestListDTO.labTestId%>' tag='pb_html'/>
                                </td>
                                
                                <td>


                                        <input type='number' class='form-control' name='labTestList.ordering'
                                               id='ordering_text_'
                                               value='<%=labTestListDTO.ordering%>' tag='pb_html'/>
                                  </td>
                                <td>


                                    <input type='text' class='form-control' name='labTestList.nameEn' id='nameEn_text_'
                                           value='<%=labTestListDTO.nameEn%>' tag='pb_html'/>
                                </td>
                                
                                <td>


                                    <input type='text' class='form-control' name='labTestList.normalRange'
                                           id='normalRange_text_' value='<%=labTestListDTO.normalRange%>'
                                           tag='pb_html'/>
                                </td>
                                
                                <td>


                                        <input type='text' class='form-control' name='labTestList.unit'
                                               id='unit_text_'
                                               value='<%=labTestListDTO.unit%>' tag='pb_html'/>
                                 </td>
                                <td>


                                    <input type='checkbox' class='form-control-sm'
                                           name='labTestList.isTitle_cb'
                                           id='isTitle_checkbox_'
                                           value='true'
                                        <%=labTestListDTO.isTitle ? ("checked") : ""%>
                                           onchange="checkIstitle(this.id)"
                                           tag='pb_html'>
                                    <input type='hidden' name='labTestList.isTitle' id='isTitle_'
                                           value='<%=labTestListDTO.isTitle%>' tag='pb_html'/>

                                </td>
                                
                                <td>
                                        <input type='checkbox' class='form-control-sm'
                                               name='labTestList.showInDropdown_cb'
                                               id='showInDropdown_checkbox_'
                                               value='true'
                                            <%=labTestListDTO.showInDropdown ? ("checked") : ""%>
                                               onchange="checkShowInDropdown(this.id)"
                                               tag='pb_html'>
                                        <input type='hidden' name='labTestList.showInDropdown'
                                               id='showInDropdown_'
                                               value='<%=labTestListDTO.showInDropdown%>' tag='pb_html'/>
                               </td>
                                    
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='labTestList.isDeleted'
                                           id='isDeleted_hidden_' value='<%=labTestListDTO.isDeleted%>' tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='labTestList.lastModificationTime'
                                           id='lastModificationTime_hidden_'
                                           value='<%=labTestListDTO.lastModificationTime%>' tag='pb_html'/>
                                </td>
                                <td>
											<span id='chkEdit'>
												<input type='checkbox' name='checkbox' value='' deletecb='true'
                                                       class="form-control-sm"/>
											</span>
                                </td>
                            </tr>

                        </template>
                    </div>
                </div>
                <div class="form-actions text-center mb-4">
                    <button type="button" id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.LAB_TEST_ADD_LAB_TEST_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.LAB_TEST_ADD_LAB_TEST_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {


        for (i = 1; i < child_table_extra_id; i++) {
            if (document.getElementById("isTitle_checkbox_" + i)) {
                if (document.getElementById("isTitle_checkbox_" + i).getAttribute("processed") == null) {
                    preprocessCheckBoxBeforeSubmitting('isTitle', i);
                    document.getElementById("isTitle_checkbox_" + i).setAttribute("processed", "1");
                }
            }
        }
        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Lab_testServlet");
    }

    function init(row) {


        for (i = 1; i < child_table_extra_id; i++) {
        }

    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-LabTestList").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-LabTestList");

            $("#field-LabTestList").append(t.html());
            SetCheckBoxValues("field-LabTestList");

            var tr = $("#field-LabTestList").find("tr:last-child");

            tr.attr("id", "LabTestList_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                console.log(index + ": " + $(this).attr('id'));
            });


            child_table_extra_id++;

        });


    $("#remove-LabTestList").click(function (e) {
        var tablename = 'field-LabTestList';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[deletecb="true"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
    });


    function checkIstitle(id) {
        console.log("istitle");
        var row = id.split("_")[2];
        var hiddenId = "isTitle_" + row;
        if ($("#" + id).prop("checked")) {
            $("#" + hiddenId).val("true");
        } else {
            $("#" + hiddenId).val("false");
        }
    }
    
    function checkShowInDropdown(id)
    {
    	console.log("showInDropdown(");
        var row = id.split("_")[2];
        var hiddenId = "showInDropdown_" + row;
        if ($("#" + id).prop("checked")) {
            $("#" + hiddenId).val("true");
        } else {
            $("#" + hiddenId).val("false");
        }
    }

</script>






