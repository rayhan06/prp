<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="lab_test.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>

<%
    String servletName = "Lab_testServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.LAB_TEST_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Lab_testDAO lab_testDAO = new Lab_testDAO("lab_test");
    Lab_testDTO lab_testDTO = lab_testDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    boolean isLangEng = Language.equalsIgnoreCase("english");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.LAB_TEST_ADD_LAB_TEST_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div>
                <h5 class="table-title">
                    <%=LM.getText(LC.LAB_TEST_ADD_LAB_TEST_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.LAB_TEST_ADD_NAMEEN, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = lab_testDTO.nameEn + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>



                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.LAB_TEST_ADD_NORMALRANGECOLUMNNAME, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = lab_testDTO.normalRangeColumnName + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.LAB_TEST_ADD_DESCRIPTION, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = lab_testDTO.description + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.LAB_TEST_ADD_SPECIMEN, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = lab_testDTO.specimen + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                    </table>
                </div>
            </div>
            <div class="mt-5">
                <h5 class="table-title">
                    <%=LM.getText(LC.LAB_TEST_ADD_LAB_TEST_LIST, loginDTO)%>
                </h5>
                <div>
                    <table class="table table-bordered table-striped">
                        <tr>
                        	<th><%=isLangEng?"Order":"ক্রম"%>
                            </th>
                            <th><%=LM.getText(LC.LAB_TEST_ADD_LAB_TEST_LIST_NAMEEN, loginDTO)%>
                            </th>
                            
                            <th><%=LM.getText(LC.LAB_TEST_ADD_LAB_TEST_LIST_NORMALRANGE, loginDTO)%>
                            </th>
                            <th><%=isLangEng?"Unit":"একক"%>
                            </th>
                            
                            <th><%=isLangEng?"Show in DropDown":"ড্রপডাউনে দেখান"%>
                            </th>
                            
                            <th><%=LM.getText(LC.HM_TITLE, loginDTO)%>
                            </th>
                        </tr>
                        <%
                            LabTestListDAO labTestListDAO = new LabTestListDAO();
                            List<LabTestListDTO> labTestListDTOs = labTestListDAO.getLabTestListDTOListByLabTestID(lab_testDTO.iD);

                            for (LabTestListDTO labTestListDTO : labTestListDTOs) {
                        %>
                        <tr>
                        	<td>
                         

                                <%=Utils.getDigits(labTestListDTO.ordering, Language)%>


                            </td>
                            <td>
                             	<%=labTestListDTO.isTitle?"<b>":""%>
                                 <%=labTestListDTO.nameEn%>
								<%=labTestListDTO.isTitle?"</b>":""%>

                            </td>
                            
                            <td>
                              

                                <%=labTestListDTO.normalRange%>


                            </td>
                            <td>
                         

                                <%=Utils.getDigits(labTestListDTO.unit, Language)%>


                            </td>
                            <td>
                           
                                <%=Utils.getYesNo(labTestListDTO.showInDropdown, Language)%>


                            </td>
                            <td>
                           
                                <%=Utils.getYesNo(labTestListDTO.isTitle, Language)%>


                            </td>
                        </tr>
                        <%

                            }

                        %>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>