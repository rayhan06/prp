<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="util.UtilCharacter" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VM_VEHICLE_EDIT_LANGUAGE, loginDTO);
    String Options;
    CommonDAO.language = Language;
    CatDAO.language = Language;
    int i = 0;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=UtilCharacter.getDataByLanguage(Language, "গাড়ি অনুযায়ী লগ বুক", "VEHICLE WISE LOG BOOK")%>
                </h3>
            </div>
        </div>
            <div class="kt-portlet__body form-body">
                <form class="form-horizontal" id="bigForm" name="bigForm">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=UtilCharacter.getDataByLanguage(Language, "গাড়ির নির্বাচন করুন", "Select Vehicle")%>
                                            </h4>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLETYPECAT, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9">
                                                <select class='form-control' name='vehicleTypeCat'
                                                        id='vehicleTypeCat_category_<%=i%>' tag='pb_html'
                                                        onchange="loadVehicleList()">
                                                    <%
                                                        Options = CatRepository.getInstance().buildOptions("vehicle_type", Language, -1);
                                                    %>
                                                    <%=Options%>
                                                </select>

                                            </div>
                                        </div>

                                        <div class="form-group row" id="vehicleIdDiv">
                                            <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLEID, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9">
                                                <select class='form-control' name='vehicleId'
                                                        id='givenVehicleId_select_<%=i%>' tag='pb_html'>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 form-actions text-right mt-3">
                        <button onclick="loadAndShowData()" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                type="button">
                            <%=UtilCharacter.getDataByLanguage(Language, "লোড", "Load")%>
                        </button>
                    </div>
                </div>
                </form>
                <div class="mt-2 w-100" id="log-book" style="display:none;">
                </div>
            </div>
    </div>
</div>

<script type="text/javascript">
    let language = '<%=Language%>';
    let $logBook = $('#log-book');

    $(document).ready(function () {
        select2SingleSelector("#vehicleTypeCat_category_0", '<%=Language%>');
        select2SingleSelector("#givenVehicleId_select_0", '<%=Language%>');
    });

    function loadVehicleList() {
        clearSubType();
        let typeValue = document.getElementById('vehicleTypeCat_category_0').value;
        let url = "Vm_vehicleServlet?actionType=getByVehicleTypeForAssignment&type=" + typeValue;
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                $("#givenVehicleId_select_0").html(fetchedData);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function validation() {
        let vehicleType = document.querySelector('#vehicleTypeCat_category_0').value;
        let vehicleId = document.querySelector('#givenVehicleId_select_0').value;
        if (!vehicleType || vehicleType.length === 0) {
            toastr.error(getValueByLanguage(language, "গাড়ির টাইপ নির্বাচন করুন", "Please select vehicle type"));
            return false;
        } else if (!vehicleId || vehicleId.length === 0) {
            toastr.error(getValueByLanguage(language, "গাড়ি নির্বাচন করুন", "Please select vehicle"));
            return false;
        }
        return true;
    }

    async function loadAndShowData() {
        if (!validation()) {
            return;
        }
        let vehicleId = document.querySelector('#givenVehicleId_select_0').value;
        let url = "Vm_requisitionServlet?actionType=vehicleReceiveSearchViewFormPageForSameCar&vehicleId=" + vehicleId;
        const res = await fetch(url);
        const htmlText = await res.text();
        if (!htmlText.includes("to-print-div")) {
            toastr.error(getValueByLanguage(language, "কোন তথ্য পাওয়া যায়নি!", "No Data Found!"));
        }
        $logBook.html(htmlText);
        $logBook.show();
    }

    function clearSubType() {
        document.querySelector('#givenVehicleId_select_0').innerHTML = "";
    }
</script>






