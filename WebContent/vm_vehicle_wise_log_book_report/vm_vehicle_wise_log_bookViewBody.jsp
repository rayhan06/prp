<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="vm_requisition.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="util.TimeFormat" %>
<%@ page import="vm_fuel_request.Vm_fuel_requestRepository" %>
<%@ page import="vm_fuel_request.Vm_fuel_requestDTO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="vm_fuel_request.VmFuelRequestItemRepository" %>

<%
    List<Vm_requisitionDTO> vm_requisitionDTOS = (List<Vm_requisitionDTO>) request.getAttribute("vm_requisitionDTOs");
    vm_requisitionDTOS = vm_requisitionDTOS.stream()
            .sorted(Comparator.comparingLong(dto -> dto.receiveDate))
            .collect(Collectors.toList());
    Long vehicleId = Long.valueOf(request.getParameter("vehicleId"));
    if (vehicleId == null) {
        vehicleId = 0l;
    }

    int currentVehicleFuelType = Integer.parseInt(Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vehicleId).vehicleFuelCat);

    List<Vm_fuel_requestDTO> fuelRequestDTOs = Vm_fuel_requestRepository.getInstance()
            .getVm_fuel_requestDTOByvehicle_id(vehicleId)
            .stream()
            .filter(dto -> dto.status == 3)
            .filter(dto -> dto.isDeleted == 0)
            .sorted(Comparator.comparingLong(dto -> dto.approvedDate))
            .collect(Collectors.toList());

//    MERGE vm_requisitionDTOS AND fuelRequestDTOs
    List<Vm_requisitionDTO> combineList = new ArrayList<>();
    int reqPtr = 0;
    int fuelPtr = 0;
    while (reqPtr < vm_requisitionDTOS.size() && fuelPtr < fuelRequestDTOs.size()) {
        if (vm_requisitionDTOS.get(reqPtr).receiveDate < fuelRequestDTOs.get(fuelPtr).approvedDate) {
            combineList.add(vm_requisitionDTOS.get(reqPtr++));
        } else {
            Vm_requisitionDTO temp = new Vm_requisitionDTO();
            long fuelReqId = fuelRequestDTOs.get(fuelPtr).iD;
            long petrol = VmFuelRequestItemRepository.getInstance()
                    .getVmFuelRequestItemDTOByvmFuelRequestId(fuelReqId)
                    .stream().filter(dto -> dto.vehicleFuelCat == currentVehicleFuelType)
                    .collect(Collectors.toList())
                    .get(0).requestedAmount;
            temp.petrolPreviousStock = 0;
            temp.petrolAmount = petrol;
            temp.petrolConsumption = 0;
            temp.petrolStock = petrol + temp.petrolPreviousStock;
            temp.receiveDate = -1;
            temp.startDate = fuelRequestDTOs.get(fuelPtr).approvedDate;
            temp.requesterNameBn = "";
            combineList.add(temp);
            fuelPtr++;
        }
    }

    while (fuelPtr < fuelRequestDTOs.size()) {
        Vm_requisitionDTO temp = new Vm_requisitionDTO();
        long fuelReqId = fuelRequestDTOs.get(fuelPtr).iD;
        long petrol = VmFuelRequestItemRepository.getInstance()
                .getVmFuelRequestItemDTOByvmFuelRequestId(fuelReqId)
                .stream().filter(dto -> dto.vehicleFuelCat == currentVehicleFuelType)
                .collect(Collectors.toList())
                .get(0).requestedAmount;
        temp.petrolPreviousStock = 0;
        temp.petrolAmount = petrol;
        temp.petrolConsumption = 0;
        temp.petrolStock = petrol + temp.petrolPreviousStock;
        temp.receiveDate = -1;
        temp.startDate = fuelRequestDTOs.get(fuelPtr).approvedDate;
        temp.requesterNameBn = "";
        temp.requesterNameBn = "";
        combineList.add(temp);
        fuelPtr++;
    }

    while (reqPtr < vm_requisitionDTOS.size()) {
        combineList.add(vm_requisitionDTOS.get(reqPtr++));
    }

//    RE - CALCULATE PETROL TIMELINE
    for (int i = 0; i < combineList.size(); i++) {
        if (i == 0) continue;
        combineList.get(i).petrolPreviousStock = combineList.get(i - 1).petrolStock;
        combineList.get(i).petrolStock = combineList.get(i).petrolPreviousStock +
                combineList.get(i).petrolAmount - combineList.get(i).petrolConsumption;
    }

    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.VM_REQUISITION_ADD_VM_REQUISITION_ADD_FORMNAME, loginDTO);
    String servletName = "Vm_requisitionServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    String vehicleReceiveBtnUrl = "Vm_requisitionServlet?actionType=vehicle_receive_search";
    if (vm_requisitionDTOS.size() == 0) return;
    Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vm_requisitionDTOS.get(0).givenVehicleId);

    String context = request.getContextPath() + "/";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    Long minStartData = vm_requisitionDTOS.stream().min(Comparator.comparingLong(dto -> dto.startDate)).get().startDate;
    Long maxStartData = vm_requisitionDTOS.stream().max(Comparator.comparingLong(dto -> dto.startDate)).get().startDate;
    String pdfFileName = "Vehicle_receive_form_"
            + simpleDateFormat.format(new Date(minStartData)) + "_to_"
            + simpleDateFormat.format(new Date(maxStartData));

%>

<style>
    .text-color {
        color: #0098bf;
    }

    .form-group label {
        font-weight: 600 !important;
    }

    .form-group {
        margin-bottom: .5rem;
    }

    .page[data-size="A4-landscape"] {
        width: 297mm;
        height: 210mm;
        padding: .1in;
        background: white;
        margin-bottom: 10px;
    }

    .page-bg {
        background-color: #f9f9fb;
    }

    .fix-fill {
        overflow: hidden;
        white-space: nowrap;
    }

    #to-print-div * {
        font-size: 12px;
    }

    #to-print-div h1 {
        font-size: 16px;
        font-weight: bold;
    }

    #to-print-div h2 {
        font-size: 14px;
        font-weight: bold;
    }

    #to-print-div h3 {
        font-size: 13px;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .5in;
        background: white;
        margin-bottom: 10px;
    }

    .page[data-size="A4-landscape"] {
        width: 297mm;
        height: 210mm;
        padding: .1in;
        background: white;
        margin-bottom: 10px;
    }

    .foot-note {
        font-size: 11px !important;
    }

    .blank-to-fill {
        display: inline-block;
        border-bottom: 1px dotted black;
        width: inherit;
    }

    .table-bordered > :not(caption) > * {
        border-color: black;
    }

    .table-bordered > :not(caption) > * > * {
        border-color: black;
    }

    .table-bordered td,
    .table-bordered th {
        padding: 5px;
    }

    .align-top {
        vertical-align: top;
    }

    th {
        text-align: center;
    }

    .table-bordered-custom th,
    .table-bordered-custom td {
        border: 1px solid #000;
        padding: 4px;
    }

    .page {
        background: rgba(255, 255, 255, 0.85);
        padding: .5in;
        margin-bottom: 5px;
        page-break-after: always;
        box-shadow: rgba(131, 131, 109, 0.85);
    }

    @media print {
        @page {
            size: landscape;
            margin: .25in;
        }
    }
</style>

<div id="kt_content">
    <div class="">
        <div class="" id="bill-div">
            <%--            DOWNLOAD BUTTON--%>
            <div class="text-right mb-3">
                <button type="button" class="btn" id='download-pdf'
                        onclick="printDivWithJqueryPrint('to-print-div');">
                    <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                </button>
            </div>
            <div>
                <div class="" id="to-print-div">
                    <%------------------------------------------------------------------------------------------------------------------%>
                    <%--DYNAIMICALLY GENERATE TABLE WHICH FITS DATA IN MULTIPLE PAGE--%>
                    <%--TABLE CONFIG--%>
                    <%
                        boolean isLastPage = false;
                        final int rowsPerPage = 6;
                        int index = 0;
                        while (index < combineList.size()) {
                            boolean isFirstPage = (index == 0);
                    %>
                    <section class="page">
                        <%if (isFirstPage) {%>
                        <div class="text-center">
                            <div class="row">
                                <div class="col-4"></div>
                                <div class="col-4">
                                    <h1 class="font-weight-bold text-center">
                                        <%=UtilCharacter.getDataByLanguage(Language, "লগ বই (টোক বই)", "Log book")%>
                                    </h1>
                                </div>
                                <div class="col-4">
                                    <div class="w-100 d-flex justify-content-end mt-3">
                                        <div class="m-2 mr-4">
                                            <p class="mb-1">
                                                <%=UtilCharacter.getDataByLanguage(Language, "গাড়ির নংঃ", "Vehicle No.:")%>&nbsp;
                                                <span class="blank-to-fill"><%=vm_vehicleDTO.regNo%></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%}%>

                        <div>
                            <%--                            <div class="table-responsive">--%>
                            <div>
                                <table class="table-bordered-custom">
                                    <thead style="background-color:lightgrey">
                                    <tr>
                                        <th rowspan="2"
                                            style="vertical-align: middle;"><%=UtilCharacter.getDataByLanguage(Language, "ফরমাশদাতার নাম ও পদবী", "Name and Title of the Order Giver")%>
                                        </th>
                                        <th rowspan="2"
                                            style="vertical-align: middle;"><%=UtilCharacter.getDataByLanguage(Language, "তারিখ", "Date")%>
                                        </th>
                                        <th rowspan="2"
                                            style="vertical-align: middle;"><%=UtilCharacter.getDataByLanguage(Language, "হইতে", "To")%>
                                        </th>
                                        <th rowspan="2"
                                            style="vertical-align: middle;"><%=UtilCharacter.getDataByLanguage(Language, "গন্তব্য স্থান", "Destination")%>
                                        </th>
                                        <th rowspan="1" colspan="4"
                                            style="vertical-align: middle;"><%=UtilCharacter.getDataByLanguage(Language, "তারিখ ও সময়", "Date & Time")%>
                                        </th>
                                        <th rowspan="2"
                                            style="vertical-align: middle;"><%=UtilCharacter.getDataByLanguage(Language, "ব্যবহৃত সময়(ঘণ্টা)", "Used Time(Hour)")%>
                                        </th>
                                        <th rowspan="2"
                                            style="vertical-align: middle;"><%=UtilCharacter.getDataByLanguage(Language, "জমা করিবার সময়", "At the time of submission")%>
                                        </th>
                                        <th rowspan="2"
                                            style="vertical-align: middle;"><%=UtilCharacter.getDataByLanguage(Language, "ড্রাইভারের নাম", "Driver Name")%>
                                        </th>
                                        <th rowspan="2"
                                            style="vertical-align: middle;"><%=UtilCharacter.getDataByLanguage(Language, "কি কারণে গাড়ি ব্যবহৃত হইয়াছে তার বিবরণ", "Details of why the car was used")%>
                                        </th>
                                        <th rowspan="1" colspan="3"
                                            style="vertical-align: middle;"><%=UtilCharacter.getDataByLanguage(Language, "মাইল মিটারে রিডিং", "In miles Reading")%>
                                        </th>
                                        <th rowspan="2"
                                            style="vertical-align: middle;"><%=UtilCharacter.getDataByLanguage(Language, "গাড়ী ব্যবহারকারীর দস্তখত, ছাড়ার সময় ও তারিখ", "Car user's signature, time and date of departure")%>
                                        </th>
                                        <th rowspan="1" colspan="4"
                                            style="vertical-align: middle;"><%=UtilCharacter.getDataByLanguage(Language, "পেট্রল", "Petrol")%>
                                        </th>
                                        <th rowspan="2"
                                            style="vertical-align: middle;"><%=UtilCharacter.getDataByLanguage(Language, "মন্তব্য", "Comment")%>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th><%=UtilCharacter.getDataByLanguage(Language, "বাহির হইবার তারিখ", "Journey Starting Date")%>
                                        </th>
                                        <th><%=UtilCharacter.getDataByLanguage(Language, "সময়", "Time")%>
                                        </th>
                                        <th><%=UtilCharacter.getDataByLanguage(Language, "ফিরে আসার তারিখ", "Journey Ending Date")%>
                                        </th>
                                        <th><%=UtilCharacter.getDataByLanguage(Language, "সময়", "Time")%>
                                        </th>

                                        <th><%=UtilCharacter.getDataByLanguage(Language, "যাওয়ার সময়", "Time to go")%>
                                        </th>
                                        <th><%=UtilCharacter.getDataByLanguage(Language, "আসার সময়", "Time to come")%>
                                        </th>
                                        <th><%=UtilCharacter.getDataByLanguage(Language, "মোট", "Total")%>
                                        </th>
                                        <th><%=UtilCharacter.getDataByLanguage(Language, "পূর্বজমা", "Preface")%>
                                        </th>
                                        <th><%=UtilCharacter.getDataByLanguage(Language, "সরবরাহ", "Supply")%>
                                        </th>
                                        <th><%=UtilCharacter.getDataByLanguage(Language, "খরচ", "Cost")%>
                                        </th>
                                        <th><%=UtilCharacter.getDataByLanguage(Language, "মজুদ", "Stock")%>
                                        </th>
                                    </tr>
                                    <tr>
                                        <%
                                            for (i = 0; i < 21; i++) {
                                        %>
                                        <td class="text-center"><%=Utils.getDigits(i, Language)%>
                                        </td>
                                        <%}%>
                                    </tr>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <%if (!isFirstPage) {%>
                                    <%}%>

                                    <%
                                        int rowsInThisPage = 0;
                                        while (index < combineList.size() && rowsInThisPage < rowsPerPage) {
                                            isLastPage = (index == (combineList.size() - 1));
                                            rowsInThisPage++;
                                            Vm_requisitionDTO model = combineList.get(index++);
                                    %>
                                    <tr>
                                        <td><%=isLanguageEnglish ? (model.requesterNameEn)
                                                : (model.requesterNameBn)%>
                                            <%=model.receiveDate == -1 ? "" : ", "%>
                                            <%
                                                String postName = isLanguageEnglish ? (model.requesterOfficeUnitOrgNameEn + ", " + model.requesterOfficeUnitNameEn)
                                                        : (model.requesterOfficeUnitOrgNameBn + ", " + model.requesterOfficeUnitNameBn);
                                                postName = model.receiveDate == -1 ?
                                                        UtilCharacter.getDataByLanguage(Language, "কর্মকর্তা সার্ভিস", "Employee Service") :
                                                        postName;
                                            %>

                                            <%=postName%>
                                        </td>
                                        <td>
                                            <%
                                                value = model.startDate == -1 ? "" :
                                                        simpleDateFormat.format(new Date(model.startDate)) + "";

                                            %>
                                            <%=Utils.getDigits(value, Language)%>
                                        </td>
                                        <td><%
                                            String val = model.startAddress + "";
                                            val = val.replaceAll("\\$", "; ");
                                        %>

                                            <%=val%>
                                        </td>
                                        <td>
                                            <%
                                                val = model.endAddress + "";
                                                val = val.replaceAll("\\$", "; ");
                                            %>

                                            <%=val%>

                                        </td>
                                        <%-- DATE & TIME --%>
                                        <td>
                                            <%
                                                val = model.startDate == -1 ? "" :
                                                        model.receiveDate == -1 ? "" :
                                                                simpleDateFormat.format(new Date(model.startDate)) + "";
                                            %>

                                            <span><%=Utils.getDigits(val, Language)%></span>
                                        </td>
                                        <td>
                                            <%
                                                val = model.startTime + "";
                                            %>

                                            <span><%=Utils.getDigits(val, Language)%></span>
                                        </td>
                                        <td>
                                            <%
                                                val = model.receiveDate == -1 ? "" :
                                                        simpleDateFormat.format(new Date(model.receiveDate)) + "";
                                            %>

                                            <span><%=Utils.getDigits(val, Language)%></span>
                                        </td>
                                        <td>
                                            <%
                                                val = model.receiveTime + "";
                                            %>

                                            <span><%=Utils.getDigits(val, Language)%></span>
                                        </td>

                                        <%-- USED TIME --%>
                                        <td>
                                            <%
                                                val = TimeFormat.getHourDifference(model.startTime, model.receiveTime, model.startDate, model.receiveDate) + "";
                                            %>

                                            <span><%=Utils.getDigits(val, Language)%></span>
                                        </td>

                                        <td>

                                            <%
                                                val = model.receiveTime + "";
                                            %>
                                            <%=Utils.getDigits(val, Language)%>

                                        </td>
                                        <td>
                                            <%
                                                val = isLanguageEnglish ? model.driverNameEn
                                                        : model.driverNameBn;
                                            %>
                                            <%=val%>
                                        </td>
                                        <td>
                                            <%
                                                val = CatRepository.getName(Language, "vehicle_requisition_purpose", model.vehicleRequisitionPurposeCat);
                                            %>
                                            <%=val%>
                                        </td>
                                        <td>
                                            <%
                                                val = String.valueOf(model.meterReadingInitial);
                                                val = val.equals("0.0") ? "" : val;
                                                val = val.equals("-1.0") ? "" : val;
                                            %>

                                            <span><%=Utils.getDigits(val, Language)%></span>
                                        </td>
                                        <td>
                                            <%
                                                val = String.valueOf(model.meterReadingEnd);
                                                val = val.equals("0.0") ? "" : val;
                                                val = val.equals("-1.0") ? "" : val;
                                            %>

                                            <span><%=Utils.getDigits(val, Language)%></span>
                                        </td>
                                        <td>
                                            <%
                                                val = model.totalTripDistance == -1 ? "" :
                                                        model.totalTripDistance + "";
                                            %>

                                            <span><%=Utils.getDigits(val, Language)%></span>
                                        </td>
                                        <td></td>
                                        <td>
                                            <%
                                                val = model.petrolPreviousStock + "";
                                            %>

                                            <span><%=Utils.getDigits(val, Language)%></span>
                                        </td>
                                        <td>
                                            <%
                                                val = model.petrolAmount + "";
                                            %>

                                            <span><%=Utils.getDigits(val, Language)%></span>
                                        </td>
                                        <td>
                                            <%
                                                val = model.petrolConsumption + "";
                                            %>

                                            <span><%=Utils.getDigits(val, Language)%></span>
                                        </td>
                                        <td>
                                            <%
                                                val = model.petrolStock + "";
                                            %>

                                            <span><%=Utils.getDigits(val, Language)%></span>
                                        </td>
                                        <td></td>
                                    </tr>
                                    </tr>
                                    <%
                                        }
                                    %>
                                    </tbody>
                                    <tfoot>
                                    <%if (isLastPage) {%>
                                    <%}%>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <%if (isLastPage) {%>
                        <%--                        <div class="mt-3 table-responsive">--%>
                        <div class="mt-3">
                            <table class="table table-borderless">
                                <tr>
                                    <td class="font-weight-bold"
                                        colspan="1"><%=UtilCharacter.getDataByLanguage(Language, "গাড়ী ব্যবহারকারীর প্রতি নির্দেশঃ", "Instructions to the car user:")%>
                                    </td>
                                    <td class="font-weight-bold"
                                        colspan="1"><%=UtilCharacter.getDataByLanguage(Language, "১ | ৪ নং কলামে গাড়ী ব্যবহারকারীর প্রতিটি ভ্রমণ লিপিবদ্ধ করিবেন", "1 | Column 4 will record each trip of the car user")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold" colspan="1"></td>
                                    <td class="font-weight-bold"
                                        colspan="1"><%=UtilCharacter.getDataByLanguage(Language, "২ | ৬ নং কলামে ভ্রমণের কারণ বিশেষভাবে উল্লেখ করিবেন, কেবল সরাকারি কাজ উল্লেখ করিলে যথেষ্ট নহে", "2 | Mention the reason for traveling in column 6, not just government work.")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold" colspan="1"></td>
                                    <td class="font-weight-bold"
                                        colspan="1"><%=UtilCharacter.getDataByLanguage(Language, "৩ | ১০ নং কলামে গাড়ী ব্যবহারকারী তাহার পদবী, তারিখ এবং গাড়ী ছাড়িবার সময়সহ দস্তখত করিবেন", "3 | Column 10 will be signed by the car user along with his last name, date and time of departure of the car.")%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <%}%>
                    </section>
                    <%
                        }
                    %>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="../utility/jquery_print.jsp"/>


<script type="text/javascript">
</script>




