<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="allowance_configure.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>


<%
    String servletName = "Allowance_configureServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Allowance_configureDTO allowance_configureDTO = Allowance_configureDAO.getInstance().getDTOFromID(id);
%>
<%@include file="../pb/viewInitializer.jsp" %>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.ALLOWANCE_CONFIGURE_ADD_ALLOWANCE_CONFIGURE_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.ALLOWANCE_CONFIGURE_ADD_ALLOWANCE_CONFIGURE_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.ALLOWANCE_CONFIGURE_ADD_ALLOWANCECAT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=CatRepository.getInstance().getText(Language, "allowance", allowance_configureDTO.allowanceCat)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.ALLOWANCE_CONFIGURE_ADD_AMOUNTTYPE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=CatRepository.getInstance().getText(Language, "allowance_amount", allowance_configureDTO.amountType)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.ALLOWANCE_CONFIGURE_ADD_ALLOWANCEAMOUNT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=Utils.getDigits(allowance_configureDTO.allowanceAmount, Language)%>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.RELIGION_ALLOWANCE_SEARCH_TAXDEDUCTION, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=Utils.getDigits(allowance_configureDTO.taxDeduction, Language)%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>