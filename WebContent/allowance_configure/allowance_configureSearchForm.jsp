<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="allowance_configure.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>


<%
    String navigator2 = "navALLOWANCE_CONFIGURE";
    String servletName = "Allowance_configureServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.ALLOWANCE_CONFIGURE_ADD_ALLOWANCECAT, loginDTO)%></th>
            <th><%=LM.getText(LC.ALLOWANCE_CONFIGURE_ADD_ALLOWANCEAMOUNT, loginDTO)%></th>
            <th><%=LM.getText(LC.RELIGION_ALLOWANCE_SEARCH_TAXDEDUCTION, loginDTO)%></th>
            <th><%=LM.getText(LC.ALLOWANCE_CONFIGURE_SEARCH_ALLOWANCE_CONFIGURE_EDIT_BUTTON, loginDTO)%></th>
            <th class="">
                <div class="text-center">
                    <span>All</span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Allowance_configureDTO> data = (List<Allowance_configureDTO>) recordNavigator.list;
            try {
                if (data != null) {
                    for (Allowance_configureDTO allowance_configureDTO :  data) {
        %>
        <tr>
            <td>
                <%=CatRepository.getInstance().getText(Language, "allowance", allowance_configureDTO.allowanceCat)%>
            </td>
            <td>
                <%=Utils.getDigits(allowance_configureDTO.allowanceAmount, Language)%><%=AllowanceTypeEnum.getSymbol(allowance_configureDTO.amountType)%>
            </td>
            <td>
                <%=Utils.getDigits(allowance_configureDTO.taxDeduction, Language)%><%=AllowanceTypeEnum.getSymbol(allowance_configureDTO.taxDeductionType)%>
            </td>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=allowance_configureDTO.iD%>'">
                    <i class="fa fa-edit"></i>
                </button>
            </td>
            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID'
                                                 value='<%=allowance_configureDTO.iD%>'/></span>
                </div>
            </td>
        </tr>
        <%
                    }
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>