<%@page import="allowance_configure.Allowance_configureDTO" %>
<%@page import="common.BaseServlet" %>
<%@page import="language.LC" %>

<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="pb.CatRepository" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="pb.Utils" %>

<%
    String context = request.getContextPath() + "/";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    Allowance_configureDTO allowance_configureDTO;
    String actionName;
    Integer allowanceCat = Utils.parseOptionalInt(request.getParameter("allowanceCat"), null, null);
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        allowance_configureDTO = (Allowance_configureDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
        allowanceCat = allowance_configureDTO.allowanceCat;
    } else {
        actionName = "add";
        allowance_configureDTO = new Allowance_configureDTO();
    }
    int i = 0;
    String Language = LM.getText(LC.ALLOWANCE_CONFIGURE_EDIT_LANGUAGE, loginDTO);
    String formTitle = LM.getText(LC.ALLOWANCE_CONFIGURE_ADD_ALLOWANCE_CONFIGURE_ADD_FORMNAME, loginDTO);
    String servletName = "Allowance_configureServlet";
    String tableName = "allowance_configure";
    boolean isLangEn = "english".equalsIgnoreCase(Language);
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Allowance_configureServlet?actionType=ajax_<%=actionName%>&isPermanentTable=true&id=<%=ID%>"
              id="bigform" name="bigform" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=allowance_configureDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ALLOWANCE_CONFIGURE_ADD_ALLOWANCECAT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%if (allowanceCat == null) {%>
                                            <select class='form-control' name='allowanceCat'
                                                    id='allowanceCat_category_<%=i%>' tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("allowance", Language, allowance_configureDTO.allowanceCat)%>
                                            </select>
                                            <%} else {%>
                                            <div class="form-control">
                                                <%=CatRepository.getInstance().getText(isLangEn, "allowance", allowanceCat)%>
                                                <input type="hidden" name="allowanceCat" value="<%=allowanceCat%>">
                                            </div>
                                            <%}%>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ALLOWANCE_CONFIGURE_ADD_AMOUNTTYPE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='amountType' id='amountType_select_<%=i%>'
                                                    tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("allowance_amount", Language, allowance_configureDTO.amountType)%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ALLOWANCE_CONFIGURE_ADD_ALLOWANCEAMOUNT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='allowanceAmount'
                                                   id='allowanceAmount_number_<%=i%>'
                                                   value='<%=allowance_configureDTO.allowanceAmount%>' tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ALLOWANCE_CONFIGURE_ADD_AMOUNTTYPE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='taxDeductionType'
                                                    id='taxDeductionType_select_<%=i%>'
                                                    tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("allowance_amount", Language, allowance_configureDTO.taxDeductionType)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.RELIGION_ALLOWANCE_SEARCH_TAXDEDUCTION, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='taxDeduction'
                                                   id='taxDeduction_<%=i%>'
                                                   value='<%=allowance_configureDTO.taxDeduction%>' tag='pb_html'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="ordinanceText">
                                            <%=isLangEn ? "ordinance Description" : "আদেশের বিবরণ"%>
                                        </label>
                                        <div class="col-md-8">
                                            <textarea class="form-control"
                                                      name="ordinanceText"
                                                      id="ordinanceText"
                                                      style="resize: none"
                                                      rows="5"
                                                      maxlength="4096"
                                            ><%=allowance_configureDTO.ordinanceText%></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.ALLOWANCE_CONFIGURE_ADD_ALLOWANCE_CONFIGURE_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="button" onclick="submitAllowanceConfigureForm()">
                                <%=LM.getText(LC.ALLOWANCE_CONFIGURE_ADD_ALLOWANCE_CONFIGURE_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<script type="text/javascript">
    const bigForm = $('#bigform');
    const cancelBtn = $('#cancel-btn');
    const submitBtn = $('#submit-btn');
    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';

    function PreprocessBeforeSubmiting(row, validate) {
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Allowance_configureServlet");
    }

    function submitAllowanceConfigureForm() {
        if (bigForm.valid()) {
            submitAjaxForm();
        }
    }

    function buttonStateChange(value) {
        cancelBtn.prop("disabled", value);
        submitBtn.prop("disabled", value);
    }

    function keyDownEvent(e) {
        console.log(e);
        let isvalid = inputValidationForIntValue(e, $(this), null);
        return true == isvalid;
    }

    function keyDownEvent2(e) {
        let isvalid = inputValidationForFloatValue(e, $(this), 2, 100.0);
        return true == isvalid;
    }

    function init(row) {
        select2SingleSelector("#allowanceCat_category_0", '<%=Language%>');
        select2SingleSelector("#amountType_select_0", '<%=Language%>');
        select2SingleSelector("#taxDeductionType_select_0", '<%=Language%>');
        document.getElementById('allowanceAmount_number_0').onkeydown = keyDownEvent;
        document.getElementById('taxDeduction_0').onkeydown = keyDownEvent2;
        $.validator.addMethod('allowanceCatSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('allowanceAmountTypeSelection', function (value, element) {
            return value != 0;
        });
        bigForm.validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                allowanceCat: {
                    required: true,
                    allowanceCatSelection: true
                },
                amountType: {
                    required: true,
                    allowanceAmountTypeSelection: true
                },
                allowanceAmount: {
                    required: true,
                },
                taxDeduction: {
                    required: true,
                },
            },

            messages: {
                allowanceCat: isLangEng ? "Please Select Allowance Type" : "অনুগ্রহ করে ভাতা নির্বাচন করুন",
                amountType: isLangEng ? "Please Select Allowance Amount Type" : "অনুগ্রহ করে ভাতার পরিমাণের ধরণ প্রবেশ করুন",
                allowanceAmount: isLangEng ? "Please Enter Amount" : "অনুগ্রহ করে পরিমাণ প্রবেশ করুন",
                taxDeduction: isLangEng ? "Please Enter Amount(%)" : "অনুগ্রহ করে পরিমাণ(%) প্রবেশ করুন",
            }
        });
    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });


</script>






