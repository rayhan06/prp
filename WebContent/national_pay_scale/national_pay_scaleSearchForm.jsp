<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="national_pay_scale.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>


<%
    String navigator2 = "navNATIONAL_PAY_SCALE";
    String servletName = "National_pay_scaleServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><%=LM.getText(LC.NATIONAL_PAY_SCALE_ADD_NAMEEN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.NATIONAL_PAY_SCALE_ADD_NAMEBN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.NATIONAL_PAY_SCALE_ADD_STARTDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.NATIONAL_PAY_SCALE_ADD_ISACTIVE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.NATIONAL_PAY_SCALE_ADD_ENDDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.NATIONAL_PAY_SCALE_SEARCH_NATIONAL_PAY_SCALE_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span>All</span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<National_pay_scaleDTO>) rn2.list;

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        National_pay_scaleDTO national_pay_scaleDTO = (National_pay_scaleDTO) data.get(i);


        %>
        <tr>


            <td>
                <%=national_pay_scaleDTO.nameEn%>
            </td>

            <td>
                <%=national_pay_scaleDTO.nameBn%>
            </td>

            <td>
                <%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, national_pay_scaleDTO.startDate), Language)%>
            </td>

            <td>
                <%=Utils.getYesNo(national_pay_scaleDTO.isActive, Language)%>
            </td>

            <td>
                <%
                    if (!national_pay_scaleDTO.isActive) {
                %>
                <%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, national_pay_scaleDTO.endDate), Language)%>
                <%
                    }
                %>
            </td>


            <%CommonDTO commonDTO = national_pay_scaleDTO; %>
            <%@include file="../pb/searchAndViewButton.jsp" %>

            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID'
                                                 value='<%=national_pay_scaleDTO.iD%>'/></span>
                </div>
            </td>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			