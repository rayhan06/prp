<%@page import="national_pay_scale.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>

<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="files.*" %>
<%@page import="dbm.*" %>

<%@ page import="java.util.Date" %>

<%@ page import="util.*" %>

<%
    National_pay_scaleDTO national_pay_scaleDTO = new National_pay_scaleDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        national_pay_scaleDTO = National_pay_scaleDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = national_pay_scaleDTO;
    String tableName = "national_pay_scale";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.NATIONAL_PAY_SCALE_ADD_NATIONAL_PAY_SCALE_ADD_FORMNAME, loginDTO);
    String servletName = "National_pay_scaleServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="National_pay_scaleServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=national_pay_scaleDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.NATIONAL_PAY_SCALE_ADD_NAMEEN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='nameEn'
                                                   id='nameEn_text_<%=i%>' value='<%=national_pay_scaleDTO.nameEn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.NATIONAL_PAY_SCALE_ADD_NAMEBN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='nameBn'
                                                   id='nameBn_text_<%=i%>' value='<%=national_pay_scaleDTO.nameBn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.NATIONAL_PAY_SCALE_ADD_STARTDATE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%value = "startDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='startDate' id='startDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(national_pay_scaleDTO.startDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.NATIONAL_PAY_SCALE_ADD_ISACTIVE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='checkbox' class='form-control-sm' name='isActive'
                                                   id='isActive_checkbox_<%=i%>'
                                                   value='true'                                                                <%=(String.valueOf(national_pay_scaleDTO.isActive).equals("true"))?("checked"):""%>
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="endDateDiv">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.NATIONAL_PAY_SCALE_ADD_ENDDATE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%value = "endDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='endDate' id='endDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(national_pay_scaleDTO.endDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.NATIONAL_PAY_SCALE_ADD_FILEDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%
                                                fileColumnName = "fileDropzone";
                                                if (actionName.equals("ajax_edit")) {
                                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(national_pay_scaleDTO.fileDropzone);
                                            %>
                                            <%@include file="../pb/dropzoneEditor.jsp" %>
                                            <%
                                                } else {
                                                    ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                    national_pay_scaleDTO.fileDropzone = ColumnID;
                                                }
                                            %>

                                            <div class="dropzone"
                                                 action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=national_pay_scaleDTO.fileDropzone%>">
                                                <input type='file' style="display:none" name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value='' tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=national_pay_scaleDTO.fileDropzone%>'/>


                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.NATIONAL_PAY_SCALE_ADD_NATIONAL_PAY_SCALE_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" id="submit-btn"
                                    type="submit">
                                <%=LM.getText(LC.NATIONAL_PAY_SCALE_ADD_NATIONAL_PAY_SCALE_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    const cancelBtn = $('#cancel-btn');
    const submitBtn = $('#submit-btn');
    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';

    function PreprocessBeforeSubmiting(row, action) {
        console.log("action = " + action);
        preprocessDateBeforeSubmitting('startDate', row);
        preprocessCheckBoxBeforeSubmitting('isActive', row);
        preprocessDateBeforeSubmitting('endDate', row);
        let msg = null;

        if (document.getElementById('nameEn_text_0').value === '') {
            msg = isLangEng ? "Name(English) not found" : "নাম(ইংরেজি) পাওয়া যায় নি";
        } else if (document.getElementById('nameBn_text_0').value === '') {
            msg = isLangEng ? "Name(Bangla) not found" : "নাম(বাংলা) পাওয়া যায় নি";
        } else if (document.getElementById('startDate_date_0').value === '') {
            msg = isLangEng ? "Start date not found" : "শুরুর তারিখ পাওয়া যায় নি";
        } else if (document.getElementById('isActive_checkbox_0').checked === false) {
            if (document.getElementById('endDate_date_0').value === '') {
                msg = isLangEng ? "End date not found" : "শেষ তারিখ পাওয়া যায় নি";
            }
        }
        if (msg) {
            $('#toast_message').css('background-color', '#ff6063');
            showToastSticky(msg, msg);
            buttonStateChange(false);
            return false;
        }
        submitAddForm2();
        return false;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "National_pay_scaleServlet");
    }

    function init(row) {
        showOrHideEndDateDiv();
        setDateByStringAndId('startDate_js_' + row, $('#startDate_date_' + row).val());
        setDateByStringAndId('endDate_js_' + row, $('#endDate_date_' + row).val());

        $('#isActive_checkbox_0').change(function () {
            showOrHideEndDateDiv();
        });
        if (document.getElementById('startDate_date_0').value !== '') {
            setMinDateById("endDate_js_0", getDateStringById("startDate_js_0"));
        }

        if (document.getElementById('endDate_date_0').value !== '') {
            setMaxDateById("startDate_js_0", getDateStringById("endDate_js_0"));
        }
        $("#startDate_js_0").on('datepicker.change', () => {
            if (getDateStringById("startDate_js_0") !== '') {
                setMinDateById("endDate_js_0", getDateStringById("startDate_js_0"));
            }
        });
        $("#endDate_js_0").on('datepicker.change', () => {
            if (getDateStringById("endDate_js_0") !== '') {
                setMaxDateById("startDate_js_0", getDateStringById("endDate_js_0"));
            }
        });

    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        //CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    function showOrHideEndDateDiv() {
        let isActive = document.getElementById("isActive_checkbox_0");
        if (isActive.checked === false) {
            setDateByStringAndId('endDate_js_' + row, '');

            $('#endDateDiv').show();

        } else {
            $('#endDateDiv').hide();

        }
    }

    function buttonStateChange(value) {
        cancelBtn.prop("disabled", value);
        submitBtn.prop("disabled", value);
    }
</script>






