<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="national_pay_scale.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>


<%
    String servletName = "National_pay_scaleServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    National_pay_scaleDTO national_pay_scaleDTO = National_pay_scaleDAO.getInstance().getDTOByID(id);
    CommonDTO commonDTO = national_pay_scaleDTO;
%>
<%@include file="../pb/viewInitializer.jsp" %>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.NATIONAL_PAY_SCALE_ADD_NATIONAL_PAY_SCALE_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.NATIONAL_PAY_SCALE_ADD_NATIONAL_PAY_SCALE_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.NATIONAL_PAY_SCALE_ADD_NAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=national_pay_scaleDTO.nameEn%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.NATIONAL_PAY_SCALE_ADD_NAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=national_pay_scaleDTO.nameBn%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.NATIONAL_PAY_SCALE_ADD_STARTDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, national_pay_scaleDTO.startDate), Language)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.NATIONAL_PAY_SCALE_ADD_ISACTIVE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=Utils.getYesNo(national_pay_scaleDTO.isActive, Language)%>
                                    </div>
                                </div>
                                <%
                                    if (!national_pay_scaleDTO.isActive) {
                                %>
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.NATIONAL_PAY_SCALE_ADD_ENDDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, national_pay_scaleDTO.endDate), Language)%>
                                    </div>
                                </div>
                                <%
                                    }
                                %>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>