
<div class="box box-primary">

    <div class="box-header with-border">
        <h3 class="box-title">
            <i class="fa fa-gift"></i>
        </h3>
    </div>

    <div class="box-body">
        <h3 class="box-title">
            Your Password has been successfully changed. Please re login.
        </h3>

        <form action="LogoutServlet">
            <input type="submit" value="OK"/>
        </form>
    </div>

</div>