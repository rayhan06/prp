<%@page import="util.*" %>
<%@page import="theme.ThemeRepository" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="config.GlobalConfigConstants" %>
<%@page import="config.GlobalConfigurationRepository" %>
<%@page import="java.util.Calendar" %>
<%@ page import="marquee_text.Marquee_textRepository" %>
<%@ page import="marquee_text.Marquee_textDTO" %>
<%@ page import="java.util.List" %>
<%@page language="java" %>
<%
    String loginURL = request.getRequestURL().toString();
    String context_folder = request.getContextPath();

    LM.getInstance();
    String context = "../../.." + request.getContextPath() + "/";
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1">
    <title>Parliament Login</title>
    <link rel="stylesheet" href="<%=context_folder%>/assets/css/login.css">
    <link rel="stylesheet" href="https://fonts.maateen.me/solaiman-lipi/font.css">

    <link
            rel="stylesheet"
            href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
            integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p"
            crossorigin="anonymous"
    />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"
          integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA=="
          crossorigin="anonymous" referrerpolicy="no-referrer"
    />

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


    <style>

        #togglePassword {
            cursor: pointer;
        }

        @media (max-width: 375px) {
            #togglePassword {
                position: relative;
                top: -54px !important;
                left: 91% !important;
            }
        }

        @media (min-width: 376px) and (max-width: 575.98px) {
            #togglePassword {
                position: relative;
                top: -54px !important;
                left: 93% !important;
            }
        }


        @media (min-width: 576px) and (max-width: 767.98px) {
            #togglePassword {
                position: relative;
                top: -54px;
                left: 93%;
            }
        }


        @media (min-width: 768px) and (max-width: 991.98px) {
            #togglePassword {
                position: relative;
                top: -54px;
                left: 93%;
            }
        }


        @media (min-width: 992px) and (max-width: 1199.98px) {
            #togglePassword {
                position: relative;
                top: -54px;
                left: 93%;
            }
        }

        @media (min-width: 1200px) and  (max-width: 1399.98px) {
            #togglePassword {
                position: relative;
                top: -54px;
                left: 93%;
            }
        }

        @media (min-width: 1400px) and  (max-width: 1599.98px) {
            #togglePassword {
                position: relative;
                top: -54px;
                left: 95%;
            }
        }

        @media (min-width: 1600px) and  (max-width: 1799.98px) {
            #togglePassword {
                position: relative;
                top: -54px;
                left: 95%;
            }
        }


        @media (min-width: 1800px) {
            #togglePassword {
                position: relative;
                top: -54px;
                left: 95%;
            }
        }

        .notification-text {
            color: #fff;
            display: flex;
            align-items: flex-start;
        }

        .notification-text:hover, .notification-text:focus {
            color: #e0e0e0;
            text-decoration: none;
            transition: .3s;
        }

        li a::before {
            content: '*';
            color: #fff;
            font-size: 24px;
            margin-right: 7px;
        }
    </style>

</head>

<%--<body style="background: url( <%=context_folder%>/assets/images/loginbg.png)no-repeat center;">--%>
<body style="background: rgb(16,66,106);background: linear-gradient(90deg, rgba(16,66,106,1) 1%, rgba(7,106,154,1) 32%, rgba(10,67,113,1) 67%); background-position: center; background-size: cover">

<%
    List<Marquee_textDTO> marqueeTextDTOs = Marquee_textRepository.getInstance().getVisibleDtoListForLoginPage(System.currentTimeMillis());
%>
<marquee class="mt-2" onmouseover="this.stop()" onmouseout="this.start()">
    <ul style="display: flex" class="mb-0">
        <%for(Marquee_textDTO marqueeTextDTO : marqueeTextDTOs){%>
        <li class="mr-5">
            <a class="notification-text" href="<%=marqueeTextDTO.getUrl()%>">
                <%=marqueeTextDTO.getText(false)%>
            </a>
        </li>
        <%}%>
    </ul>
</marquee>

<div class="toparea">
    <div class="toplef">
        <img src="<%=context_folder%>/assets/images/perliament_logo.png" alt="mujib">
    </div>
    <div class="topright">
        <img src="<%=context_folder%>/assets/images/mujib_borsho_logo.png" alt="prp">
    </div>
</div>
<div class="middlearea" style="margin-top: -20px;">
    <div class="middleimage">
        <img src="<%=context_folder%>/assets/images/sonshod_min.png" alt="">
    </div>

    <div class="middleform">
        <form id="loginform" action="<%=request.getContextPath()%>/LoginServlet" method="post" class="box">

            <!-- <h1>প্রবেশ করুন</h1> -->
            <a href="#" style="width: 130px; height: 65px; margin-bottom: 30px;">
                <img src="<%=context_folder%>/assets/images/prplogo.png" alt="prplogo">
            </a>
            <div class="pt-2">
                <jsp:include page='../common/flushActionStatus.jsp'/>
            </div>

            <div class="inputfiled">
                <input type="text" name="username" id="udername" placeholder="ব্যবহারকারীর আইডি"
                       value='<%=request.getParameter("username")!=null?request.getParameter("username"):""%>'>
                <input type="password" name="password" id="pass" placeholder="পাসওয়ার্ড"
                       value='<%=request.getParameter("password")!=null?request.getParameter("password"):""%>'>
                <span
                        class="fa fa-eye-slash"
                        id="togglePassword"
                ></span>
                <div id="g-recaptcha-id" class="" style="margin-top: -25px">
                    <div class="g-recaptcha"
                         data-sitekey="6LezMgYhAAAAAIA0j46GR5lLmcT752Z3MttO9YRa"
                         data-callback="recaptcha_callback"
                         data-expired-callback="expCallback"
                    >
                    </div>
                </div>
            </div>
            <input type="submit" style="position: absolute; left: -9999px"/>
            <div class="allsubmitbutton" style="margin-top: 20px">
                <p class="passwordtext" style="cursor: pointer"><a onclick="goToPinCodeRetrieve()">পাসওয়ার্ড ভুলে
                    গেছেন?</a></p>
                <a href="javascript:" class="submitform"> <img src="<%=context_folder%>/assets/images/submit.png"
                                                               alt="Submit"> </a>
            </div>
        </form>
    </div>
</div>


<%--<script src="<%=context%>/assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>--%>

<script src="https://www.google.com/recaptcha/api.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous"
        referrerpolicy="no-referrer">
</script>
<script
        src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"
        integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw=="
        crossorigin="anonymous"
        referrerpolicy="no-referrer">
</script>


<script>

    let isCaptchaChecked = false;

    const captchaNotRequired = location.hostname !== 'prp.parliament.gov.bd';

    if (captchaNotRequired) {
        $('#g-recaptcha-id').hide();
    }

    function goToPinCodeRetrieve() {
        window.location.href = "<%=request.getContextPath()%>/Entry_pageServlet?actionType=pinCodeRetrieve";
    }

    $(document).ready(() => {
        toastr.options = {
            closeButton: true,
            debug: false,
            newestOnTop: false,
            progressBar: false,
            positionClass: "toast-top-right",
            preventDuplicates: false,
            showDuration: "1000",
            hideDuration: "1000",
            timeOut: "5000",
            extendedTimeOut: "1000",
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "fadeIn",
            hideMethod: "fadeOut",
        };

        const togglePassword = document.querySelector("#togglePassword");
        const password = document.querySelector("#pass");

        togglePassword.addEventListener("click", function (e) {
            // toggle the type attribute
            const type =
                password.getAttribute("type") === "password" ? "text" : "password";
            password.setAttribute("type", type);
            // toggle the eye / eye slash icon

            if (password.getAttribute("type") === "password") {
                $("#togglePassword").removeClass("fa fa-eye");
                $("#togglePassword").addClass("fa fa-eye-slash");
            } else {
                $("#togglePassword").removeClass("fa fa-eye-slash");
                $("#togglePassword").addClass("fa fa-eye");
            }
        });

        $(".submitform").on("click", function () {
            if (captchaNotRequired || isCaptchaChecked) {
                $("#loginform").submit();
            } else {
                toastr.error("Captcha Is Required!");
            }
        });
    });

    function recaptcha_callback() {
        isCaptchaChecked = true
    }

    function expCallback() {
        isCaptchaChecked = false
    }
</script>

</body>
</html>