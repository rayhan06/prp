<%@page import="org.apache.log4j.Logger"%><%@page import="language.*"%><%@page import="forgetPassword.VerificationConstants"%><%@page import="config.*"%><%@page import="java.util.Calendar"%><%@ page language="java" %>
<%
Logger otpLogger = Logger.getLogger("otpVerifier_jsp");
String loginURL=request.getRequestURL().toString();
String context = "../../.."  + request.getContextPath() + "/";
String context_folder = request.getContextPath();
boolean pageValid = true;
if(session.getAttribute(VerificationConstants.CACHE_OBJECT) == null)
{
	otpLogger.debug("cache object not found. so redirecting to home");
	pageValid = false;		
}

if(Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OTP_IN_LOGIN_ENABLED_FOR_CLIENT).value) == 0
	&& Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OTP_IN_LOGIN_ENABLED_FOR_ADMIN).value) == 0)
{
	otpLogger.debug("otp not enabled. so redirecting to home");
	pageValid = false;	
}
if(!pageValid)
{
	response.sendRedirect(context);
}
%>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->    
<head>
<meta charset="utf-8" />
<title><%=LM.getText(LC.GLOBAL_OTP_VERIFICATION_TITLE) %></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />        
<link href="<%=context_folder%>/assets/css/google_font.css" rel="stylesheet" type="text/css" />
<link href="<%=context_folder%>/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<%=context_folder%>/assets/css/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<%=context_folder%>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<%=context_folder%>/assets/css/uniform.default.css" rel="stylesheet" type="text/css" />
<link href="<%=context_folder%>/assets/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />      
<link href="<%=context_folder%>/assets/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />      
<link href="<%=context_folder%>/assets/css/forgot_password.css" rel="stylesheet" type="text/css" />        

<style type="text/css">
.login{
background-color: #fff !important;
}
.login .copyright {
color: black !important;
}
.login .content h3{
color: black !important;
}
@media only screen and (min-width: 760px) {
.quick-links span{
font-size: 18px !important;
}
.quick-links a{
 font-size: 20px !important;
 margin-left: 10px;
 text-decoration: underline;
}
}
</style>
</head>    		
<body class=" login">
<div class="menu-toggler sidebar-toggler"></div>        
<div class="logo">
<a href="<%=context_folder%>">
<img  class="login-page-logo" src="<%=context_folder%>/images/company-name.png" alt="" /> </a>
</div>        
<div class="content">            
<form id="otp-form" class="login-form" action="<%=context_folder%>/<%=session.getAttribute(VerificationConstants.CACHE_OTP_VERIFICATION_URL)%>" method="post">
<input type="hidden" name="csrfPreventionSalt" value="${csrfPreventionSalt}"/>
<input type="hidden" name="<%=VerificationConstants.CACHE_FORWARD%>" value="<%=session.getAttribute(VerificationConstants.CACHE_FORWARD)%>"/>
<h3 class="form-title font-purple"><%=LM.getText(LC.GLOBAL_OTP_VERIFICATION) %></h3>             
<jsp:include page='../common/flushActionStatus.jsp' />
<div class="form-group">
<label class="control-label visible-ie8 visible-ie9"><%=LM.getText(LC.GLOBAL_OTP) %></label>
<input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="OTP" name="token" /> </div>     
<div class="form-actions">                	
<button type="submit" class="btn green-jungle  btn-block uppercase"><%=LM.getText(LC.GLOBAL_SUBMIT) %></button>
<button type="button" id="resendOTP" class="btn grey  btn-block uppercase"><%=LM.getText(LC.GLOBAL_RESEND_OTP) %></button>
</div>
</form>            
</div>         
<div class="copyright"> <%=Calendar.getInstance().get(Calendar.YEAR) %><%=LM.getText(LC.GLOBAL_COPY_RIGHT) %> </div>	       
<!--[if lt IE 9]>
<script src="<%=context_folder%>/assetsglobal/plugins/respond.min.js"></script>
<script src="<%=context_folder%>/assetsglobal/plugins/excanvas.min.js"></script> 
<![endif]-->        
<script src="<%=request.getContextPath()%>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/assets/scripts/common/otpVerifier.js" type="text/javascript"></script>  
<script>
$("#reloadCaptcha").click(function() {				
	$("#captcha").attr('src', $("#captcha").attr('src') + '?' + Math.random());					
});
</script>        
</body>
</html>