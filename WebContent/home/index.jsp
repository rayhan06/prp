<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@ page import="common.RoleEnum" %>
<%
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    if (userDTO.roleID == SessionConstants.DOCTOR_ROLE) {
        request.setAttribute("dashboardPath", "../dashboard/doctor_dashboard.jsp");
    } 
    else if (userDTO.roleID == SessionConstants.INVENTORY_MANGER_ROLE || userDTO.roleID == SessionConstants.PHARMACY_PERSON) 
    {
        request.setAttribute("dashboardPath", "../dashboard/inventory_dashboard.jsp");
    }
    else if (userDTO.roleID == SessionConstants.NURSE_ROLE) {
        request.setAttribute("dashboardPath", "../dashboard/nurse_dashboard.jsp");
    }
    else if (userDTO.roleID == SessionConstants.XRAY_GUY) {
        request.setAttribute("dashboardPath", "../dashboard/xray_dashboard.jsp");
    }
    else if (userDTO.roleID == SessionConstants.RADIOLOGIST) {
        request.setAttribute("dashboardPath", "../dashboard/radio_dashboard.jsp");
    }
    else if (userDTO.roleID == SessionConstants.PATHOLOGY_HEAD) {
        request.setAttribute("dashboardPath", "../dashboard/pathology_head_dashboard.jsp");
    }
    else if (userDTO.roleID == SessionConstants.LAB_TECHNITIAN_ROLE) {
        request.setAttribute("dashboardPath", "../dashboard/lab_dashboard.jsp");
    }
    else if (userDTO.roleID == SessionConstants.MEDICAL_RECEPTIONIST_ROLE) {
        request.setAttribute("dashboardPath", "../dashboard/receptionist_dashboard.jsp");
    }
    else if (userDTO.roleID == SessionConstants.TICKET_ADMIN_ROLE) {
        request.setAttribute("dashboardPath", "../dashboard/ticketDashboard.jsp");
    }
    else if (userDTO.roleID == SessionConstants.PHYSIOTHERAPIST_ROLE) {
        request.setAttribute("dashboardPath", "../dashboard/physiotherapist_dashboard.jsp");
    }
    else if (userDTO.roleID == SessionConstants.RECRUITMENT_ADMIN_ROLE) {
        request.setAttribute("dashboardPath", "../recruitment_dashboard/recruitment_dashboardBody.jsp");
    }
    else if (userDTO.roleID == SessionConstants.GATE_MANAGEMENT_ADMIN_ROLE)
    {
        request.setAttribute("dashboardPath", "../dashboard/gatePassDashboard.jsp");
    }
    else if (userDTO.roleID == SessionConstants.PBS_ROLE)
    {
        request.setAttribute("dashboardPath", "../dashboard/pbsDashboard.jsp");
    }
    else if (userDTO.roleID == SessionConstants.SERJEANT_AT_ARMS_ROLE)
    {
        request.setAttribute("dashboardPath", "../dashboard/sergeantDashboard.jsp");
    }
    else if (userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE) {
        request.setAttribute("dashboardPath", "../dashboard/medical_admin_dashboard.jsp");
    }
    else if (userDTO.roleID == SessionConstants.VEHICLE_ADMIN_ROLE) {
        request.setAttribute("dashboardPath", "../vm_dashboard/vehicle_dashboardBody.jsp");
    }
    else if (userDTO.roleID == SessionConstants.ASSET_ADMIN_ROLE) {
        request.setAttribute("dashboardPath", "../am_dashboard/asset_dashboardBody.jsp");
    }
    else if (userDTO.roleID == SessionConstants.INVENTORY_ADMIN_ROLE) {
        request.setAttribute("dashboardPath", "../pi_dashboard/pi_dashboardBody.jsp");
    }
    else if (userDTO.roleID == RoleEnum.EMPLOYEE_OF_PARLIAMENT.getRoleId()) {
        request.setAttribute("dashboardPath", "../dashboard/employee_dashboard.jsp");
    } else {
        request.setAttribute("dashboardPath", "../dashboard/hr_admin_dashboard.jsp");
    }
%>
<%
System.out.println("#############################Getting dashboard from index##############################");
%>
<jsp:include page="../common/dashboard_layout.jsp" flush="true">
    <jsp:param name="title" value="Dashboard"/>
    <jsp:param name="body" value="${dashboardPath}"/>
</jsp:include> 