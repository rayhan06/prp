<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="language.LM" %>
<%@ page import="language.LC" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    String servletType = request.getParameter("servletType");
    String pageTitle = request.getParameter("pageTitle");

    LoginDTO loginDTO2 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
%>

<input type='hidden' id='servletType' value='<%=servletType%>'/>
<div class="row" style="margin: 0px !important;">
    <div class="col-md-12" style="padding: 5px !important;">
        <div id="ajax-content">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <%=(LM.getText(LC.DESIGNATION_LEVEL_EDIT, loginDTO2))%>
                    </div>
                </div>
                <label>পদবি স্তর শূন্য অথবা ফাঁকা হলে ডিফল্টভাবে '৯৯৯' বসবে।</label>

                <div>
                    <table id="designation_table" style="width:100%">
                        <tr id="table_head">
                            <th>Designation</th>
                            <th>Level</th>
                            <th>Sequence</th>
                        </tr>
                    </table>
                </div>
                <button class="btn btn-success" onclick="submit()">Submit</button>
            </div>
        </div>
    </div>
</div>


<div id="snackbar">Success</div>


<script type="text/javascript">

    var store_designation = [];

    function submit() {
        console.log(store_designation);

        var changed_designation = [];
        store_designation.forEach(function (value) {
            var id = document.getElementById('designation_id_' + value.id).innerText;
            var level = document.getElementById('designation_level_' + value.id).value;
            var sequence = document.getElementById('designation_sequence_' + value.id).value;

            if (parseInt(id) === value.id) {
                console.log('id same ', id);
                if (parseInt(level) !== value.designation_level || parseInt(sequence) !== value.designation_sequence) {
                    console.log('sequence change ', id, ' ', sequence, ' ', level);
                    changed_designation.push({id: id, level: level, sequence: sequence});
                }
            }
        });

        console.log(changed_designation);
        var data = "";
        changed_designation.forEach(function (value) {
            data += "&data=" + value.id + " " + value.level + " " + value.sequence;
        });

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                var x = document.getElementById("snackbar");
                x.className = "show";
                setTimeout(function () {
                    x.className = x.className.replace("show", "");
                }, 3000);
            }
        };
        xhttp.open("Post", "DesignationLabelChangeServlet?actionType=updateLevel&data=" + data, true);
        xhttp.send();
    }

    function getDesignation() {

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {

                JSON.parse(this.responseText).forEach(function (val) {
                    store_designation.push(val);
                    if (val.designation_level === null || val.designation_level === 0) {
                        val.designation_level = 999;
                    }
                    var html = "<tr>\n" +
                        "<th id='designation_id_" + val.id + "' style='display: none' >" + val.id + "</th>\n" +
                        "<th>" + val.designation_bng + "</th>\n" +
                        "<th><input id='designation_level_" + val.id + "' type='text' value='" + val.designation_level + "'/></th>\n" +
                        "<th><input id='designation_sequence_" + val.id + "' type='text' value='" + val.designation_sequence + "'/></th>\n" +
                        "</tr>";
                    document.getElementById('designation_table').innerHTML += html;
                });
            }
        };
        xhttp.open("Get", "DesignationLabelChangeServlet?actionType=getDesignation", true);
        xhttp.send();
    }

    window.onload = function (ev) {
        getDesignation();
    };
</script>

<style>
    #snackbar {
        visibility: hidden; /* Hidden by default. Visible on click */
        min-width: 500px; /* Set a default minimum width */
        margin-left: -125px; /* Divide value of min-width by 2 */
        background-color: #10ca08; /* Black background color */
        color: #fff; /* White text color */
        text-align: center; /* Centered text */
        border-radius: 2px; /* Rounded borders */
        padding: 16px; /* Padding */
        position: fixed; /* Sit on top of the screen */
        z-index: 1; /* Add a z-index if needed */
        left: 50%; /* Center the snackbar */
        bottom: 30px; /* 30px from the bottom */
    }

    /* Show the snackbar when clicking on a button (class added with JavaScript) */
    #snackbar.show {
        visibility: visible; /* Show the snackbar */
        /* Add animation: Take 0.5 seconds to fade in and out the snackbar.
        However, delay the fade out process for 2.5 seconds */
        -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }

    /* Animations to fade the snackbar in and out */
    @-webkit-keyframes fadein {
        from {
            bottom: 0;
            opacity: 0;
        }
        to {
            bottom: 30px;
            opacity: 1;
        }
    }

    @keyframes fadein {
        from {
            bottom: 0;
            opacity: 0;
        }
        to {
            bottom: 30px;
            opacity: 1;
        }
    }

    @-webkit-keyframes fadeout {
        from {
            bottom: 30px;
            opacity: 1;
        }
        to {
            bottom: 0;
            opacity: 0;
        }
    }

    @keyframes fadeout {
        from {
            bottom: 30px;
            opacity: 1;
        }
        to {
            bottom: 0;
            opacity: 0;
        }
    }
</style>

<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>

