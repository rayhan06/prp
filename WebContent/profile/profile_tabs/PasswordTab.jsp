<%@ page import="model.employee_records" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="language.LM" %>
<%@ page import="language.LC" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    employee_records user = (employee_records) request.getAttribute("employee_records");
    LoginDTO loginDTO1 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO1 = UserRepository.getInstance().getUserDtoByUserId(loginDTO1.userID);
%>
<div class="kt-portlet__body">
    <div class="form-group row">
        <label class="col-4 col-form-label">বর্তমান পাসওয়ার্ড</label>
        <div class="col-6">
            <input id="old_pass" type="password" class="form-control">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-4 col-form-label">নতুন পাসওয়ার্ড</label>
        <div class="col-6">
        <input id="new_pass" type="password" class="form-control">
        <span class="form-text text-muted">* পাসওয়ার্ড নূন্যতম ৬ অক্ষরের হতে হবে। অন্তত একটি A-Z অথবা a-z থাকতে হবে।</span>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-4 col-form-label">পুনরায় নতুন পাসওয়ার্ডটি দিন</label>
        <div class="col-6">
            <input id="new_pass_again" type="password" class="form-control">
        </div>
    </div>
    <div class="kt-form__actions">
    <input type="button" class="btn btn-outline-brand btn-elevate-hover" value="<%=(LM.getText(LC.GLOBAL_SUBMIT, loginDTO1))%>" onclick="onChangePasswordClick()" />
    </div>
</div>