<%@ page import="model.employee_records" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="language.LM" %>
<%@ page import="util.CommonConstant" %>
<%@ page import="language.LC" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    employee_records user = (employee_records) request.getAttribute("employee_records");
    LoginDTO loginDTO0 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO0 = UserRepository.getInstance().getUserDtoByUserId(loginDTO0.userID);

    String profile;
    String edit;
    String Information;
    String History;
    String Home;
    int my_language = LM.getLanguageIDByUserDTO(userDTO0) == CommonConstant.Language_ID_English ? 2 : 1;
    if (my_language == 2) {
        profile = "Profile";
        edit = "Edit";
        Information = "Information";
        History = "History";
        Home = "Home";
    } else {
        profile = "প্রফাইল";
        edit = "সম্পাদনা";
        Information = "তথ্য";
        History = "কর্ম ইতিহাস";
        Home = "হোম";
    }
%>
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label" id="edit_profile">
            <h3 class="kt-portlet__head-title kt-font-primary mr-3"><%=profile%>
            </h3>
            <button class="btn btn-outline-brand btn-bold btn-sm" onclick="onEditProfileInfoClick()">
                <%=edit%>
            </button>
        </div>
        <div class="kt-portlet__head-toolbar mt-4">
            <div class="kt-portlet__head-actions">
                <button class="btn btn-outline-brand btn-bold btn-sm" onclick="onDisplayProfileClick()"><%=Information%>
                </button>
                <button class="btn btn-outline-brand btn-bold btn-sm" onclick="onHistoryClick()"><%=History%>
                </button>
                <button class="btn btn-outline-brand btn-bold btn-sm" onclick="onHomeClick()"><%=Home%>
                </button>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" id="profile_div">
        <table>
            <tbody>
            <tr>
                <th>নাম (বাংলা)</th>
                <td><%=user.getName_bng()%>
                </td>
                <th>নাম (ইংরেজি)</th>
                <td><%=user.getName_eng()%>
                </td>
            </tr>
            <tr>
                <th>পিতার নাম (বাংলা)</th>
                <td><%=user.getFather_name_bng()%>
                </td>
                <th>পিতার নাম (ইংরেজি)</th>
                <td><%=user.getFather_name_eng()%>
                </td>
            </tr>
            <tr>
                <th>মাতার নাম (বাংলা)</th>
                <td><%=user.getMother_name_bng()%>
                </td>
                <th>মাতার নাম (ইংরেজি)</th>
                <td><%=user.getMother_name_eng()%>
                </td>
            </tr>
            <tr>
                <th>জন্ম তারিখ</th>
                <td><%
                    String Options;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = new Date();
                    String datestr = dateFormat.format(user.getDate_of_birth());
                    out.println(datestr);
                %>
                </td>
                <th>পরিচয়পত্র নম্বর</th>
                <td><%=user.getNid()%>
                </td>
            </tr>
            <tr>
                <th> লিঙ্গ</th>
                <td><%=user.getGender()%>
                </td>
                <th> ধর্ম</th>
                <td><%=user.getReligion()%>
                </td>
            </tr>
            <tr>
                <th>রক্তের গ্রুপ</th>
                <td><%=user.getBlood_group()%>
                </td>
                <th>বৈবাহিক অবস্থা</th>
                <td><%=user.getMarital_status()%>
                </td>
            </tr>
            <tr>
                <th>ব্যক্তিগত ই-মেইল</th>
                <td><%=user.getPersonal_email()%>
                </td>
                <th>ব্যক্তিগত মোবাইল নম্বর</th>
                <td><%=user.getPersonal_mobile()%>
                </td>
            </tr>
            <tr>
                <th>বিকল্প মোবাইল নম্বর</th>
                <td><%=user.getAlternative_mobile()%>
                </td>
                <th>লগইন নেম</th>
                <td><%=userDTO0.userName%>
                </td>
            </tr>
            </tbody>

        </table>
    </div>
    <div id="history_div" style="display: none" class="kt-portlet__body">
        <div>
            <label id="employee_name"><%=userDTO0.fullName%>
            </label>
            <label id="user_name">(<%=userDTO0.userName%>)</label>
            <label>: কর্ম ইতিহাস</label>
        </div>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>পদবি</th>
                <th>শাখা</th>
                <th>অফিস</th>
                <th>সময়কাল</th>
            </tr>
            </thead>
            <tbody id="data_body">
            <tr>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>