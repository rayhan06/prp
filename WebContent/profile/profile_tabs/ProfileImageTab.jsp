<%@ page import="model.employee_records" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="model.profile_images" %>
<%@ page import="language.LM" %>
<%@ page import="language.LC" %>
<%@ page import="util.CommonConstant" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    employee_records user = (employee_records) request.getAttribute("employee_records");
    LoginDTO loginDTO2 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO2 = UserRepository.getInstance().getUserDtoByUserId(loginDTO2.userID);

    profile_images profileImage = (profile_images) request.getAttribute("profile_image");
    String[] tag = profileImage.getImage().split(";");
    String imageType = tag[0];
    String image = tag[1];

    int my_language3 = LM.getLanguageIDByUserDTO(userDTO2) == CommonConstant.Language_ID_English ? 2 : 1;
    String image_name;

    if (my_language3 == 1) {
        image_name = "ছবি আপলোড করতে এখানে ক্লিক করুন";
    } else {
        image_name = "Click here to upload image";
    }
%>
<div class="kt-portlet__body">
    <h5 class=" kt-font-primary">প্রোফাইল ছবি পরিবর্তন</h5>
    <img class="round-image" height="150px" width="150px" src="data:image/<%=imageType%>;base64,<%=image%>"
         alt="Profile Image"/>
    <br>
    <div class="form-group row">
        <div class="col-lg-4 col-md-10 col-sm-12">
            <div class="dropzone dropzone-default dropzone-brand dz-clickable">
                <div class="dropzone-msg dz-message needsclick">
                    <%--<h3 class="dropzone-msg-title">Drop files here or click to upload.</h3>--%>
                    <%--<span class="dropzone-msg-desc">Upload up to 10 files</span>--%>
                    <div class="dropzone-msg-title" style="display:block;width:120px; height:30px;"
                         onclick="document.getElementById('getFile').click()">
                        <%=image_name%>
                    </div>
                    <input type='file' id="getFile" style="display:none">
                </div>
            </div>
        </div>
    </div>
    <div class="kt-form__actions">
        <input type="button" class="btn btn-outline-brand btn-elevate-hover"
               value="<%=(LM.getText(LC.GLOBAL_SUBMIT, loginDTO2))%>" onclick="onSelectImageClick()"/>
    </div>
</div>