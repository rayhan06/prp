<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="model.employee_records" %>
<%@ page import="model.profile_images" %>
<%@ page import="util.CommonConstant" %>
<%@ page import="language.LM" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    String servletType = request.getParameter("servletType");
    String pageTitle = request.getParameter("pageTitle");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
    profile_images profileImage = (profile_images) request.getAttribute("profile_image");
    ArrayList<HashMap<String, Object>> office_name = (ArrayList<HashMap<String, Object>>) request.getAttribute("office_name");

    String[] tag = profileImage.getImage().split(";");
    String imageType = tag[0];
    String image = tag[1];

    String profile;
    String Password;
    String Protikolpo;
    String ProfilePicture;
    String Signature;
    String Notification;
    String Share;
    String email;
    String phone;
    int my_language = LM.getLanguageIDByUserDTO(userDTO) == CommonConstant.Language_ID_English ? 2 : 1;
    if (my_language == 2) {
        profile = "Profile";
        Password = "Password";
        Protikolpo = "Protikolpo";
        ProfilePicture = "Profile Picture";
        Signature = "Signature";
        Notification = "Notification";
        Share = "Share";
        email = "Email";
        phone = "Phone";
    } else {
        profile = "প্রফাইল";
        Password = "পাসওয়ার্ড";
        Protikolpo = "প্রতিকল্প";
        ProfilePicture = "প্রফাইল ছবি";
        Signature = "স্বাক্ষর";
        Notification = "নোটিফিকেশন";
        Share = "শেয়ার";
        email = "ইমেইল";
        phone = "মোবাইল";
    }
%>
<input type="hidden" id="employee_record_id" value="<%=userDTO.employee_record_id%>">
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">
            <div class="kt-grid__item kt-app__toggle kt-app__aside" id="kt_user_profile_aside">
                <div class="kt-portlet kt-portlet--height-fluid-">
                    <div class="kt-portlet__body ">
                        <div class="kt-widget kt-widget--user-profile-1">
                            <div class="kt-widget__head">
                                <div class="kt-widget__media">
                                    <img class="round-image" src="data:image/<%=imageType%>;base64,<%=image%>"
                                         alt="Profile Image"/>
                                </div>
                                <div class="kt-widget__content">
                                    <div class="kt-widget__section">
                                        <p href="#" class="kt-widget__username">
                                            <%=userDTO.fullName%>
                                            <i class="flaticon2-correct kt-font-success"></i>
                                        </p>
                                        <span class="kt-widget__subtitle">
                                           <%
                                               if (office_name != null) {
                                                   if (my_language == 2) {
                                                       out.println(office_name.get(0).get("office_name_eng"));
                                                   } else {
                                                       out.println(office_name.get(0).get("office_name_bng"));
                                                   }
                                               }
                                           %>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-widget__body">
                                <div class="kt-widget__content">
                                    <div class="kt-widget__info">
                                        <span class="kt-widget__label"><%=email%>: </span>
                                        <a href="#" class="kt-widget__data"><%=userDTO.mailAddress%>
                                        </a>
                                    </div>
                                    <div class="kt-widget__info">
                                        <span class="kt-widget__label"><%=phone%>: </span>
                                        <a href="#" class="kt-widget__data"><%=userDTO.phoneNo%>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-grid__item kt-grid__item--fluid kt-app__content">
                <div class="row">
                    <div class="col-xl-6">
                        <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
                            <div class="kt-portlet__body px-0">
                                <div class="tab">
                                    <ul class="nav nav-tabs nav-fill" role="tablist">
                                        <li class="nav-item">
                                            <a id="btnProfile" class=" nav-link"
                                               onclick="onClickTab(event, 'profile', 'profile_body', 'getProfilePage')"><%=profile%>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class=" nav-link"
                                               onclick="onClickTab(event, 'password', 'password_body', 'getPasswordPage')"><%=Password%>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class=" nav-link"
                                               onclick="onClickTab(event, 'profile_pic', 'profile_image_body', 'getPPPage')"><%=ProfilePicture%>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class=" nav-link"
                                               onclick="onClickTab(event, 'signature', 'signature_body', 'getSignaturePage')">
                                                <%=Signature%>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class=" nav-link"
                                               onclick="onClickTab(event, 'protikolpo', 'protikolpo_body', 'getProtikolpoPage')">
                                                <%=Protikolpo%>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class=" nav-link"
                                               onclick="onClickTab(event, 'notification')"><%=Notification%>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class=" nav-link" onclick="onClickTab(event, 'share')"><%=Share%>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                                <div id="profile" class="tabcontent">
                                    <div id="profile_body"></div>
                                </div>

                                <div id="password" class="tabcontent">
                                    <div id="password_body"></div>
                                </div>

                                <div id="profile_pic" class="tabcontent">
                                    <div id="profile_image_body"></div>
                                </div>

                                <div id="signature" class="tabcontent">
                                    <div id="signature_body"></div>
                                </div>

                                <div id="protikolpo" class="tabcontent">
                                    <div id="protikolpo_body"></div>
                                </div>

                                <div id="notification" class="tabcontent">
                                    <h4>Notification</h4>
                                </div>

                                <div id="share" class="tabcontent">
                                    <h4>Share</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function onClickTab(evt, tab_name, tab_body_id, action_type) {
        let i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("nav-link");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(tab_name).style.display = "block";
        evt.currentTarget.className += " active";
        getTab(tab_body_id, action_type)
    }

    function getTab(tab_id, action_type) {
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById(tab_id).innerHTML = this.responseText;
                if (action_type === 'getPPPage' || action_type === 'getSignaturePage') init(action_type);
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("GET", "ProfileServlet?actionType=" + action_type, true);
        xhttp.send();
    }
</script>
</body>
</html>
<%--Profile--%>
<script>
    function onEditProfileInfoClick() {
        const id = document.getElementById('employee_record_id').value;
        document.location.href = '/Employee_recordsServlet?actionType=getEditPage&ID=' + id;
    }

    function onDisplayProfileClick() {
        document.getElementById('profile_div').style.display = '';
        document.getElementById('edit_profile').style.display = '';
        document.getElementById('history_div').style.display = 'none';
    }

    function onHistoryClick() {
        document.getElementById('profile_div').style.display = 'none';
        document.getElementById('edit_profile').style.display = 'none';
        document.getElementById('history_div').style.display = '';
        getWorkHistory();
    }

    function onHomeClick() {
        document.location.href = '/';
    }
</script>
<%--Password--%>
<script>
    function onChangePasswordClick() {
        const old_pass = document.getElementById('old_pass').value;
        const new_pass = document.getElementById('new_pass').value;
        const new_pass_again = document.getElementById('new_pass_again').value;
        const r = validatePassword(old_pass, new_pass, new_pass_again);
        if (r === true) {
            const formData = new FormData();
            formData.append('password', new_pass);
            formData.append('password_old', old_pass);
            const xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    if (this.responseText == 'true') {
                        showToast(success_message_bng, success_message_eng);
                        location.reload();
                    } else {
                        showError(failed_message_bng, failed_message_eng);
                    }
                } else if (this.readyState == 4 && this.status != 200) {
                    alert('failed ' + this.status);
                }
            };
            xhttp.open("POST", "ProfileServlet?actionType=changePassword", true);
            xhttp.send(formData);
        }
    }
</script>
<script>
    let image_type = null;
    let image_file = null;

    function init(action_type) {
        image_type = action_type;
        document.querySelector('input[type="file"]').addEventListener('change', function () {
            if (this.files && this.files[0]) {
                let img = document.querySelector('img');
                img.src = URL.createObjectURL(this.files[0]);
                image_file = this.files[0];
            }
        });
    }

    function onSelectImageClick() {
        if (image_file == null || image_file.length == 0) return;
        const formData = new FormData();
        formData.append('image_type', image_type === 'getPPPage' ? 'profile' : 'signature');
        formData.append('image', image_file);
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText == 'true') {
                    showToast(success_message_bng, success_message_eng);
                    location.reload();
                }
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("POST", "ProfileServlet?actionType=profileImage", true);
        xhttp.send(formData);
    }
</script>
<script>
    function onSelectSignatureClick() {
        if (image_file == null || image_file.length == 0) return;
        const formData = new FormData();
        formData.append('image_type', image_type === 'getPPPage' ? 'profile' : 'signature');
        formData.append('image', image_file);
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText == 'true') {
                    showToast(success_message_bng, success_message_eng);
                    location.reload();
                }
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("POST", "ProfileServlet?actionType=profileImage", true);
        xhttp.send(formData);
    }
</script>
<script>
    let policy_roles = [];
    let designation_id = 0;

    function onSubmitVacation() {
        if (!validateProtikolpoData()) return false;
        const data = {};
        data.start_time = document.getElementById('vacation_start').value;
        data.end_time = document.getElementById('vacation_end').value;
        try {
            data.policy_id = document.getElementById('policy_select').value;
            data.policy_name = document.getElementById('policy_select').options[document.getElementById('policy_select').selectedIndex].text;
        } catch (e) {
            console.log('ignore: ', e);
        }
        let protikolpo_no = 1;
        if (document.getElementById('first_protikolpo').checked) {
            protikolpo_no = 1;
            designation_id = document.getElementById('first_protikolpo').value;
        }
        if (document.getElementById('second_protikolpo').checked) {
            protikolpo_no = 2;
            designation_id = document.getElementById('second_protikolpo').value;
        }

        const parm = "?actionType=addVacation"
            + "&deg_id=" + designation_id
            + "&policy_name=" + data.policy_name
            + "&start_date=" + data.start_time
            + "&end_date=" + data.end_time
            + "&protikolpo_no=" + protikolpo_no;

        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200 && this.responseText !== '' && this.responseText.length > 0) {
                showToast(success_message_bng, success_message_eng);
            }
        };
        xhttp.open("Post", "ProtikolpoSetupServlet" + parm, true);
        xhttp.send();
    }

    function getPolicyRoles() {
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200 && this.responseText !== '' && this.responseText.length > 0) {
                policy_roles = [];
                const op = document.getElementById('policy_select');
                op.options.length = 0;
                JSON.parse(this.responseText).forEach(function (val) {
                    policy_roles.push({id: val.id, name_eng: val.name_eng, name_bng: val.name_bng});
                    op.options[op.options.length] = new Option(val.name_bng, val.id);
                });
            }
        };
        xhttp.open("Get", "ProtikolpoSetupServlet?actionType=getPolicyRoles", true);
        xhttp.send();
    }

    function validateProtikolpoData() {
        const a = document.getElementById('vacation_start').value;
        const b = document.getElementById('vacation_end').value;

        if (a == undefined || a == null || b == undefined || b == null || a.length == 0 || b.length == 0) {
            showError('প্রতিকল্প শুরু এবং শেষের তারিখ দিন', 'Please provide protikolpo start and end date');
            return false;
        }
        return true;
    }

</script>
<script>
    function getWorkHistory() {
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                let html = '';
                JSON.parse(this.responseText).forEach(function (val) {
                    console.log(val);
                    html += '<tr>\n' +
                        '<td>' + val.designation_bng + '</td>\n' +
                        '<td>' + val.unit_name_bng + '</td>\n' +
                        '<td>' + val.office_name_bng + '</td>\n' +
                        '<td>' + getWorkDuration(val.joining_date, val.last_office_date) + '</td>\n' +
                        '</tr>'
                });
                document.getElementById('data_body').innerHTML = html;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("GET", "ProfileServlet?actionType=getWorkHistory", true);
        xhttp.send();
    }
</script>
<script>
    function getWorkDuration(st_date, ed_date) {
        if (st_date == undefined || st_date == null || st_date == 0) {
            if (ed_date == undefined || ed_date == null || ed_date == 0) return ('...');
            const date0 = new Date(parseInt(ed_date));
            const to = date0.toLocaleDateString();
            return ('..' + ' - ' + to);
        }
        const date = new Date(parseInt(st_date));
        const from = date.toLocaleDateString();
        if (ed_date == undefined || ed_date == null || ed_date == 0) return (from + ' - Running');
        const date0 = new Date(parseInt(ed_date));
        const to = date0.toLocaleDateString();
        return (from + ' - ' + to);
    }

    function validatePassword(old_pass, new_pass, new_pass_again) {
        if (old_pass == undefined || old_pass == null || old_pass.length == 0) {
            showError('বর্তমান পাসওয়ার্ড দিন', 'Provide old password');
            return false;
        }
        if (new_pass == undefined || new_pass == null) {
            showError('নতুন পাসওয়ার্ড দিন', 'Provide new password');
            return false;
        }
        if (new_pass !== new_pass_again) {
            showError('পাসওয়ার্ড মেলে নি ', "Password doesn't match");
            return false;
        }
        if (new_pass.length < 6 || new_pass_again.length < 6 || !hasAZ(new_pass) || !hasAZ(new_pass_again)) {
            showError('পাসওয়ার্ড নূন্যতম ৬ অক্ষরের হতে হবে। অন্তত একটি A-Z অথবা a-z থাকতে হবে ', 'Password must have 6 character and contai A-Z or a-z');
            return false;
        }
        return true;
    }

    function hasAZ(a) {
        for (let i = 0; i < a.length; i++) {
            if (a[i] >= 'a' && a[i] <= 'z') return true;
            if (a[i] >= 'A' && a[i] <= 'Z') return true;
        }
        return false;
    }

    $(document).ready(function () {
        document.getElementById('btnProfile').click();
    });
</script>
<style>
    body {
        font-family: Arial;
    }

    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    .round-image {
        margin: 15px 0;
        -webkit-border-radius: 50% !important;
        -moz-border-radius: 50% !important;
        border-radius: 50% !important;
    }

    .nav-tabs {
        margin-bottom: 0px;
    }

    .nav-tabs .nav-item .nav-link {
        font-size: 1.3rem;
    }
</style>