function validateInput() {
    console.log('validate input');
}

function checkBBSCode(table, column, data, parentColumn, parentColumnId, exceptId = null) {
    let a = hasSameBBSCode(table, column, data, parentColumn, parentColumnId, exceptId);
    let b = checkInt(data, 3);
    return a == true && b == true;
}

function checkMinistryBBSCode(bbs_code, id) {
    const xhttp = new XMLHttpRequest();
    xhttp.open("GET", "CommonServlet" + "?actionType=hasMinistryBBSCode&" + "bbs=" + bbs_code + "&id=" + id, false);
    xhttp.send();

    if (xhttp.status === 200) {
        console.log(xhttp.responseText);
        if (xhttp.responseText == 'true') {
            showError("কোড টি পূর্বে ব্যাবহিত হয়েছে, অনুগ্রহ করে নতুন কোড দিন", "This BBS Code already exist, please provide a new one");
            return false;
        } else {
            //showError("কোড টি আগে থেকেই আছে, অনুগ্রহ করে আবার দিন", "This BBS Code already exist, please provide a new one");
            return true;
        }
    }
}

function checkPostCode(table, column, data, parentColumn, parentColumnId, exceptId = null) {
    let a = hasSameBBSCode(table, column, data, parentColumn, parentColumnId, exceptId);
    let b = checkInt(data, 4);
    return a == true && b == true;
}

function checkOfficeCode(code, id) {
    let a = hasSameOfficeCode(code, id);
    let b = checkInt(code, 4);
    return a == true && b == true;
}

function checkBatchNo(batch, id) {
    let a = hasSameBatchNo(batch, id);
    let b = checkLength(batch, 2);
    return a == true && b == true;
}

function checkUnitCode(code, id) {
    let a = hasUnitCode(code, id);
    //let b = checkLength(code, 4);
    return a == true;
}

function checkInt(val, size) {
    if (val === undefined || val === null) {
        showError("অনুগ্রহ করে সব তথ্য দিন", "Please fill all input correctly");
        return false;
    }
    if (!hasInteger(val)) {
        showError("শুধুমাত্র সংখ্যা দিন", "Please provide number only");
        return false;
    }
    const code = (size == 1 ? '১' : (size == 2 ? '২' : (size == 3 ? '৩' : (size == 4 ? '৪' : (size == 5 ? '৫' : (size == 6 ? '৬' : (size == 7 ? '৭' : (size == 8 ? '৮' : (size == 9 ? '৯' : '১০')))))))));
    if (val.length > size) {
        showError("কোড সংখ্যার দীর্ঘ " + code + " এর বেশি নয়", "Code must be " + size + " digit integer");
        return false;
    }
    return true;
}

function checkLength(val, size) {
    if (val === undefined || val === null) {
        showError("অনুগ্রহ করে সব তথ্য দিন", "Please fill all input correctly");
        return false;
    }
    const code = (size == 1 ? '১' : (size == 2 ? '২' : (size == 3 ? '৩' : (size == 4 ? '৪' : (size == 5 ? '৫' : (size == 6 ? '৬' : (size == 7 ? '৭' : (size == 8 ? '৮' : (size == 9 ? '৯' : '১০')))))))));
    if (val.length > size) {
        showError("ব্যাচ নম্বরের দীর্ঘ " + code + " এর বেশি নয়", "Code must be " + size + " digit integer");
        return false;
    }
    return true;
}

function checkYear(string, min = 1, max = 255) {
    if (string === undefined || string === null) {
        showError("অনুগ্রহ করে সব তথ্য দিন", "Please fill all input correctly");
        return false;
    }
    if (hasSpecialCharacter(string)) {
        showError("<>@!#$%^&*()_+[]{}?:;|'\"\\,/~`-= এই অক্ষর গুলো সঠিক নয়", "<>@!#$%^&*()_+[]{}?:;|'\"\\,/~`-= This character are not valid");
        return false;
    }

    if (!(string.length >= min && string.length <= max)) {
        showError("সর্বনিম্ন এবং সর্বোচ্চ 8 টি অক্ষর দিন", "Please provide at least and at most 4 character");
        return false;
    }
    return true;
}

function checkPassword(a, b) {
    if (a === undefined || a === null) {
        showError("অনুগ্রহ করে পাসওয়ার্ড দিন", "Please provide your password");
        return false;
    }
    if (a.length < 8) {
        showError("পাসওয়ার্ডের সংখ্যা ৮ বা তার অধিক", "Password length must greater than 8");
        return false;
    }
    if (a != b) {
        showError("পাসওয়ার্ড একই নয়", "Password doesn't match");
        return false;
    }
    return true;
}

function checkString(string, min = 1, max = 255) {
    if (string === undefined || string === null) {
        showError("অনুগ্রহ করে সব তথ্য দিন", "Please fill all input correctly");
        return false;
    }
    if (hasSpecialCharacter(string)) {
        showError("<>@!#$%^&*()_+[]{}?:;|'\"\\,/~`-= এই অক্ষর গুলো সঠিক নয়", "<>@!#$%^&*()_+[]{}?:;|'\"\\,/~`-= This character are not valid");
        return false;
    }

    if (!(string.length >= 1 && string.length <= 255)) {
        showError("সর্বনিম্ন ১ এবং সর্বোচ্চ ২৫৫ টি অক্ষর দিন", "Please provide min 1 and maximum 255 character");
        return false;
    }
    return true;
}

function hasSpecialCharacter(string) {
    if (string === null || string === undefined) return false;
    const specialChars = "<>@!#$%^&*()_+[]{}?:;|'\"\\,./~`-=";
    for (i = 0; i < specialChars.length; i++) {
        if (string.indexOf(specialChars[i]) > -1) {
            return true
        }
    }
    return false;
}

function hasInteger(string) {
    if (string === null || string === undefined) return false;
    const specialChars = "0987654321";
    for (i = 0; i < specialChars.length; i++) {
        if (string.indexOf(specialChars[i]) > -1) {
            return true
        }
    }
    return false;
}

function hasSameBBSCode(table, column, data, parentColumn, parentColumnId, exceptId = null) {
    const xhttp = new XMLHttpRequest();
    xhttp.open("GET", "CommonServlet" + "?actionType=getUniqueBBSCode&" +
        "table=" + table + "&column=" + column + "&data=" + data + "&parent=" + parentColumn
        + "&parent_id=" + parentColumnId + (exceptId !== null ? "&except_id=" + exceptId : ""), false);
    xhttp.send();

    if (xhttp.status === 200) {
        console.log(xhttp.responseText);
        if (xhttp.responseText == 'true') {
            showError("কোড টি আগে থেকেই আছে, অনুগ্রহ করে আবার দিন", "This BBS Code already exist, please provide a new one");
            return false;
        } else {
            //showError("কোড টি আগে থেকেই আছে, অনুগ্রহ করে আবার দিন", "This BBS Code already exist, please provide a new one");
            return true;
        }
    }
}

function hasSameOfficeCode(code, id) {
    const xhttp = new XMLHttpRequest();
    xhttp.open("GET", "CommonServlet" + "?actionType=hasOfficeCode&" + "code=" + code + "&id=" + id, false);
    xhttp.send();

    if (xhttp.status === 200) {
        console.log(xhttp.responseText);
        if (xhttp.responseText == 'true') {
            showError("কোড টি পূর্বে ব্যাবহিত হয়েছে, অনুগ্রহ করে নতুন কোড দিন", "This BBS Code already exist, please provide a new one");
            return false;
        } else {
            //showError("কোড টি আগে থেকেই আছে, অনুগ্রহ করে আবার দিন", "This BBS Code already exist, please provide a new one");
            return true;
        }
    }
}

function hasSameBatchNo(batch, id) {
    const xhttp = new XMLHttpRequest();
    xhttp.open("GET", "CommonServlet" + "?actionType=hasBatchNo&" + "batch=" + batch + "&id=" + id, false);
    xhttp.send();

    if (xhttp.status === 200) {
        console.log(xhttp.responseText);
        if (xhttp.responseText == 'true') {
            showError("ব্যাচ নম্বর টি পূর্বে ব্যাবহিত হয়েছে, অনুগ্রহ করে নতুন ব্যাচ নম্বর দিন", "This batch no is already exist, please provide a new one");
            return false;
        } else {
            //showError("কোড টি আগে থেকেই আছে, অনুগ্রহ করে আবার দিন", "This BBS Code already exist, please provide a new one");
            return true;
        }
    }
}

function hasUnitCode(code, id) {
    const xhttp = new XMLHttpRequest();
    xhttp.open("GET", "CommonServlet" + "?actionType=hasUnitCode&" + "code=" + code + "&id=" + id, false);
    xhttp.send();

    if (xhttp.status === 200) {
        console.log(xhttp.responseText);
        if (xhttp.responseText == 'true') {
            showError("কোড টি পূর্বে ব্যাবহিত হয়েছে, অনুগ্রহ করে নতুন কোড দিন", "This code no already exist, please provide a new one");
            return false;
        } else {
            //showError("কোড টি আগে থেকেই আছে, অনুগ্রহ করে আবার দিন", "This BBS Code already exist, please provide a new one");
            return true;
        }
    }
}

function searchDataFound() {
    showToast("তথ্য পাওয়া গেছে", "Data found");
}

function noSearchDataFound() {
    showToast("তথ্য পাওয়া যায় নি", "No data found");
}

function insertToastMessage() {
    const url_string = window.location.toString();
    const url = new URL(url_string);
    const c = url.searchParams.get("success");
    if (c !== undefined && c !== null && c == 'true') {
        showToast(success_message_bng, success_message_eng);
        let go_to = url_string.split("&success")[0];
        history.pushState({}, null, go_to);
    }
    console.log('location: ', url);
}

function tryParseInt(val) {
    const b = isNumeric(val);
    console.log(b);
    return b;
}

function isNumeric(value) {
    return /^-{0,1}\d+$/.test(value);
}