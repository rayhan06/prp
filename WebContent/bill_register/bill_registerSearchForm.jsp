<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="bill_management.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoRepository" %>
<%@ page import="util.StringUtils" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@ page import="supplier_institution.Supplier_institutionRepository" %>
<%@ page import="budget_register.Budget_registerDTO" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="static util.StringUtils.convertBanglaIfLanguageIsBangla" %>
<%@ page import="static util.StringUtils.getFormattedDate" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="bill_register.Bill_registerDTO" %>
<%@page contentType="text/html;charset=utf-8" %>

<%
    String navigator2 = "navBILL_MANAGEMENT";
    String servletName = "Bill_managementServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=getDataByLanguage(Language, "অর্থ বছর", "Economic Year")%></th>
            <th><%=getDataByLanguage(Language, "বাজেট অপারেশন কোড", "Budget Operation Code")%></th>
            <th><%=getDataByLanguage(Language, "অর্থনৈতিক কোড", "Economic Code")%></th>
            <th><%=getDataByLanguage(Language, "তারিখ", "Date")%></th>
            <th><%=getDataByLanguage(Language, "বিল নং", "Bill Number")%></th>
            <th><%=getDataByLanguage(Language, "বিলের বিবরণ", "Bill Description")%></th>
            <th><%=getDataByLanguage(Language, "বিলের টাকার পরিমাণ", "Bill Amount")%></th>
            <th><%=getDataByLanguage(Language, "টোকেন নং", "Token Number")%></th>
            <th><%=getDataByLanguage(Language, "তারিখ", "Date")%></th>
            <th><%=getDataByLanguage(Language, "চেক নং", "Cheque Number")%></th>
            <th><%=getDataByLanguage(Language, "তারিখ", "Date")%></th>
            <th><%=getDataByLanguage(Language, "চেকের টাকার পরিমাণ", "Cheque Amount")%></th>
            <th><%=getDataByLanguage(Language, "চেক রেজিস্টারে যোগ করুন", "Add to Cheque Register")%></th>
        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Bill_registerDTO> data = (List<Bill_registerDTO>) recordNavigator.list;
            try {
                if (data != null) {
                    for (Bill_registerDTO billRegisterDTO : data) {
        %>
                        <tr>
                            <td>
                                <%=BudgetSelectionInfoRepository.getInstance().getEconomicYearById(Language, billRegisterDTO.budgetSelectionInfoId)%>
                            </td>
                            <td>
                                <%=Budget_mappingRepository.getInstance().getCode(Language, billRegisterDTO.budgetMappingId)%>
                            </td>
                            <td>
                                <%=Economic_sub_codeRepository.getInstance().getCode(billRegisterDTO.economicSubCodeId, Language)%>
                            </td>
                            <td>
                                <%=getFormattedDate(Language, billRegisterDTO.billDate)%>
                            </td>
                            <td>
                                <%=convertBanglaIfLanguageIsBangla(
                                        Language,
                                        String.format("%d", billRegisterDTO.iD)
                                )%>
                            </td>
                            <td>
                                <%=billRegisterDTO.description%>
                            </td>
                            <td>
                                <%=BangladeshiNumberFormatter.getFormattedNumber(convertBanglaIfLanguageIsBangla(
                                        Language,
                                        String.format("%d", billRegisterDTO.billAmount)
                                ))%>/-
                            </td>
                            <td>
                                <%=convertBanglaIfLanguageIsBangla(
                                        Language,
                                        billRegisterDTO.tokenNumber
                                )%>
                            </td>
                            <td>
                                <%=getFormattedDate(Language, billRegisterDTO.tokenDate)%>
                            </td>
                            <td>
                                <%=convertBanglaIfLanguageIsBangla(
                                        Language,
                                        billRegisterDTO.chequeNumber
                                )%>
                            </td>
                            <td>
                                <%=getFormattedDate(Language, billRegisterDTO.chequeDate)%>
                            </td>
                            <td>
                                <%if(billRegisterDTO.chequeAmount > 0) {%>
                                <%=BangladeshiNumberFormatter.getFormattedNumber(convertBanglaIfLanguageIsBangla(
                                        Language,
                                        String.format("%d", billRegisterDTO.chequeAmount)
                                ))%>/-
                                <%}%>
                            </td>
                            <td>
                                <button type="button"
                                        class="btn-sm border-0 shadow btn-border-radius text-white text-center"
                                        style="background-color: #ff6b6b;"
                                        onclick="location.href='Cheque_registerServlet?actionType=addToRegister&billRegisterId=<%=billRegisterDTO.iD%>'">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </td>
                        </tr>
        <%
                    }
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>">
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>">
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>">
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>">
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>">