<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page pageEncoding="UTF-8" %>

<%@ page import="pb.*" %>
<%

    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.OFFICE_HEAD_REPORT_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    boolean isLangEng = Language.equalsIgnoreCase("English");
    String context = "../../.." + request.getContextPath() + "/assets/";
%>
<jsp:include page="../employee_assign/officeMultiSelectTagsUtil.jsp"/>

<script src="<%=context%>scripts/util1.js"></script>
<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>
<div class="row">
    <div class="col-12">
        <div id="officeUnitId" class="search-criteria-div col-md-6 officeModalClass">
            <div class="form-group row">
                <label class="col-md-3 control-label text-md-right">
                    <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_OFFICEUNITID, loginDTO)%>
                </label>
                <div class="col-md-9">
                    <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                            id="office_units_id_modal_button"
                            onclick="officeModalButtonClicked();">
                        <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                    </button>
                    <input type="hidden" name='officeUnitIds' id='officeUnitIds_input' value="">
                </div>
            </div>
        </div>


        <div class="search-criteria-div col-12" id="selected-offices" style="display: none;">
            <div class="form-group row">
                <div class="offset-1 col-10 tag-container" id="selected-offices-tag-container">
                    <div class="tag template-tag">
                        <span class="tag-name"></span>
                        <i class="fas fa-times-circle tag-remove-btn"></i>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="search-criteria-div col-md-6 officeModalClass">
                <div class="form-group row">
                    <label class="col-sm-4 col-xl-3 col-form-label text-md-right" for="onlySelectedOffice_checkbox">
                        <%=isLangEng ? "Only Selected Office" : "শুধুমাত্র নির্বাচিত অফিস" %>
                    </label>
                    <div class="col-1" id='onlySelectedOffice'>
                        <input type='checkbox' class='form-control-sm mt-1' name='onlySelectedOffice'
                               id='onlySelectedOffice_checkbox'
                               onchange="this.value = this.checked;" value='false'>
                    </div>
                    <div class="col-8"></div>
                </div>
            </div>
    </div>
</div>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>


<script type="text/javascript">
    $(document).ready(() => {
        showFooter = false;
    });

    function init() {
        dateTimeInit($("#Language").val());
        addables = [0, 0, 0, 0];
    }

    function PreprocessBeforeSubmiting() {
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInTags,
            isMultiSelect: true,
            keepLastSelectState: true
        }]
    ]);

    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }
</script>