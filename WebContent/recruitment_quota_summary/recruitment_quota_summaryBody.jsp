﻿<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDAO" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDAO" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="recruitment_job_specific_exam_type.JobSpecificExamTypeDao" %>


<%


    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);


    Recruitment_job_descriptionDAO jobDescriptionDAO = new Recruitment_job_descriptionDAO();
    Job_applicant_applicationDAO jobApplicantApplicationDAO = new Job_applicant_applicationDAO();

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date todayDate = new Date();

    int pagination_number = 0;
    RecordNavigator rn = new RecordNavigator();
    request.getSession().setAttribute(SessionConstants.NAV_JOB_APPLICANT_APPLICATION, rn);
    String context = "../../.." + request.getContextPath() + "/";


//    System.out.println("#####");
//    System.out.println(new JobSpecificExamTypeDao().getNextLevelFromJobIdAndCurrentLevel(2000, 1));
//    System.out.println(new JobSpecificExamTypeDao().getPreviousLevelFromJobIdAndCurrentLevel(2000, 1));
//    System.out.println("#####");

%>

<!--new layout start-->

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important; margin-bottom: -18px">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.CANDIDATE_LIST_QUOTA_SUMMARY, loginDTO)%>
                </h3>
            </div>
        </div>

        <form id="candidate-form" action="RecruitmentQuotaSummaryServlet?actionType=DownloadTemplate"
              name="candidate-form" method="POST">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=LM.getText(LC.CANDIDATE_LIST_QUOTA_SUMMARY, loginDTO)%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label text-md-right">
                                            <%=LM.getText(LC.CANDIDATE_LIST_STATUS, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <select onchange="statusChange()" class='form-control' name='status'
                                                    id='status' tag='pb_html'>
                                                <%=jobDescriptionDAO.getStatusList(Language)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label text-md-right">
                                            <%=LM.getText(LC.CANDIDATE_LIST_POST_NAME, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <select onchange="onJobChange()" class='form-control' name='job'
                                                    id='job' tag='pb_html'>
                                                <%=jobDescriptionDAO.getJobListForSummary("ALL", Language)%>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-3 row">
                    <div class="col-md-10 form-actions text-right">
                        <a style="color: white;background-color: #00a1d4;cursor: pointer;border-radius: 8px;"
                           class="btn btn-sm text-white shadow" onclick="onSubmit('')">
                            <i class="fas fa-spinner"></i>
                            <%=LM.getText(LC.CANDIDATE_LIST_LOAD, loginDTO)%>
                        </a>
                        <button type="submit" class="btn btn-sm btn-success text-white shadow ml-2"
                                style="border-radius: 8px;"
                                id="download_excel" name="download_excel"><i
                                class="fas fa-file-excel"></i><%=LM.getText(LC.CANDIDATE_LIST_DOWNLOAD_EXCEL, loginDTO) %>
                        </button>
                    </div>
                </div>
                <div class="mt-5" id='data-div-parent' style="display: none">
                    <div id='data-div' class="text-nowrap">
                    </div>
                    <% pagination_number = 1;%>
                    <%@include file="../common/pagination_with_go2.jsp" %>
                </div>
            </div>
        </form>
    </div>
</div>

<!--new layout end-->

<%--<div class="box box-primary">--%>
<%--    <div class="box-header with-border">--%>
<%--        <h3 class="box-title"><i class="fa fa-gift"></i>--%>
<%--            <%=LM.getText(LC.CANDIDATE_LIST_QUOTA_SUMMARY, loginDTO)%>--%>
<%--&lt;%&ndash;            QUOTA_SUMMARY&ndash;%&gt;--%>

<%--        </h3>--%>

<%--    </div>--%>

<%--    <div class="box-body">--%>
<%--        <form id = "candidate-form" action="RecruitmentQuotaSummaryServlet?actionType=DownloadTemplate" name = "candidate-form" method="POST" >--%>
<%--            <div class="form-body">--%>
<%--                <div class="form-row">--%>


<%--                    <div class="form-group col-md-4">--%>
<%--                        <label class="col-lg-3 control-label" style="font-size: 12px; font-weight: bold">--%>
<%--                            &lt;%&ndash;                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%>&ndash;%&gt;--%>
<%--                                <%=LM.getText(LC.CANDIDATE_LIST_STATUS, loginDTO)%>--%>
<%--                            &lt;%&ndash;                        <span class="required"> * </span>&ndash;%&gt;--%>
<%--                        </label>--%>
<%--                        <div class="form-group ">--%>
<%--                            <div class="col-lg-9 " >--%>
<%--                                <select onchange="statusChange()" class='form-control'  name='status' id = 'status'   tag='pb_html'>--%>

<%--                                    <%=jobDescriptionDAO.getStatusList(Language)%>--%>
<%--                                </select>--%>

<%--                            </div>--%>
<%--                        </div>--%>


<%--                    </div>--%>


<%--                    <div class="form-group col-md-4">--%>
<%--                        <label class="col-lg-3 control-label" style="font-size: 12px; font-weight: bold">--%>
<%--                            &lt;%&ndash;                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%>&ndash;%&gt;--%>
<%--                                <%=LM.getText(LC.CANDIDATE_LIST_POST_NAME, loginDTO)%>--%>
<%--                            &lt;%&ndash;                        <span class="required"> * </span>&ndash;%&gt;--%>
<%--                        </label>--%>
<%--                        <div class="form-group ">--%>
<%--                            <div class="col-lg-9 " id = 'job_div'>--%>
<%--                                <select onchange="onJobChange()" class='form-control'  name='job' id = 'job'   tag='pb_html'>--%>

<%--                                    <%=jobDescriptionDAO.getJobListForSummary( "ALL", Language)%>--%>
<%--                                </select>--%>

<%--                            </div>--%>
<%--                        </div>--%>


<%--                    </div>--%>


<%--                </div>--%>


<%--                <div id = "submit-button-div" class="form-actions text-center">--%>
<%--                    <a style="color: white"  class="btn btn-success"  onclick="onSubmit('')">--%>

<%--                        <%=LM.getText(LC.CANDIDATE_LIST_LOAD, loginDTO)%>--%>
<%--&lt;%&ndash;                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_DESCRIPTION_SUBMIT_BUTTON, loginDTO)%>&ndash;%&gt;--%>

<%--                    </a>--%>
<%--                </div>--%>


<%--                <div  id = "submit-download-div" class="form-actions text-center">--%>

<%--                    <input type="submit" class="btn-info" style="color: white;text-align:center;display: inline-block;float: right;" value="<%=LM.getText(LC.CANDIDATE_LIST_DOWNLOAD_EXCEL, loginDTO) %>" id="download_excel" name="download_excel">--%>

<%--                    <!--<input type="submit" class="btn-info" style="color: white;text-align:center;display: inline-block;float: right;" value="Download Excel" id="download_excel" name="download_excel">-->--%>
<%--                </div>--%>
<%--                </br></br>--%>
<%--&lt;%&ndash;                <input type="hidden" id="hidden_job" name="job" value="" />&ndash;%&gt;--%>
<%--&lt;%&ndash;                <input type="hidden" id="hidden_status" name="status" value="" />&ndash;%&gt;--%>
<%--            </div>--%>


<%--        </form>--%>


<%--        <div id = 'data-div-parent' style="display: none">--%>
<%--            <div id = 'data-div' >--%>

<%--            </div>--%>
<%--            <% pagination_number = 1;%>--%>
<%--            <%@include file="../common/pagination_with_go2.jsp"%>--%>

<%--        </div>--%>


<%--    </div>--%>
<%--</div>--%>


<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">

    let language = '<%=Language%>';


    function statusChange() {
        // hideLoadButton();
        let statusValue = document.getElementById('status').value;
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('job').innerHTML = this.responseText;
            } else if (this.readyState == 4 && this.status != 200) {
            }
        };

        let params = "Recruitment_job_descriptionServlet?actionType=getJobListForSummaryByStatus";
        params = params + "&language=" + language + "&status=" + statusValue;
        xhttp.open("Get", params, true);
        xhttp.send();
    }

    function showLoadButton() {
        document.getElementById('submit-button-div').style.display = 'block';
        document.getElementById('submit-download-div').style.display = 'block';
        var job_name = $("#job>option:selected").text();
        var level_name = $("#level>option:selected").text();
        document.getElementById("job_name").value = job_name;
        document.getElementById("level_name").value = level_name;
    }

    function hideLoadButton() {
        document.getElementById('submit-button-div').style.display = 'none';
        document.getElementById('submit-download-div').style.display = 'none';
    }

    function showDataDiv() {
        document.getElementById('data-div-parent').style.display = 'block';
    }

    function hideDataDiv() {
        document.getElementById('data-div-parent').style.display = 'none';
    }

    function onJobChange() {
        let jobValue = document.getElementById('job').value;
        if (jobValue && jobValue.length > 0) {
            // showLoadButton();
        } else {
            // hideLoadButton();
            document.getElementById('level').innerHTML = '';
        }
    }

    function onSubmit(params) {
        let jobValue = document.getElementById('job').value;
        let statusValue = document.getElementById('status').value;

        console.log({jobValue, statusValue})

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('data-div').innerHTML = this.responseText;
                setPageNo();
                showDataDiv();
            } else if (this.readyState == 4 && this.status != 200) {
            }
        };

        params = params + "&jobId=" + jobValue
            + "&status=" + statusValue;
        let prefixParam = "RecruitmentQuotaSummaryServlet?actionType=getData" + params;
        // console.log(prefixParam)

        xhttp.open("Get", prefixParam, true);
        xhttp.send();
    }

    function allfield_changed(go, pagination_number) {
        // console.log('came here')


        let params = '&search=true&ajax=true';

        // var extraParams = document.getElementsByName('extraParam');
        // extraParams.forEach((param) => {
        //     params += "&" + param.getAttribute("tag") + "=" + param.value;
        // })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            // console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[0].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;

        // console.log(params)
        onSubmit(params);

    }


    let numberBangla = {
        '0': '০',
        '1': '১',
        '2': '২',
        '3': '৩',
        '4': '৪',
        '5': '৫',
        '6': '৬',
        '7': '৭',
        '8': '৮',
        '9': '৯'
    };

    function toBn(retStr) {
        retStr = retStr + "";
        for (let x in numberBangla) {
            retStr = retStr.replace(new RegExp(x, 'g'),
                numberBangla[x]);
        }
        return retStr;
    }

    function convertToBn(value) {
        if (language == 'English') {
            return value;
        }

        return toBn(value);
    }

</script>
