<%@page pageEncoding="UTF-8" %>

<%@page import="recruitment_job_description.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="files.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@ page import="approval_execution_table.*"%>
<%@ page import="approval_path.*"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="employee_pay_scale.Employee_pay_scaleDAO" %>
<%@ page import="employee_pay_scale.Employee_pay_scaleDTO" %>
<%@ page import="employee_pay_scale.Employee_pay_scaleRepository" %>
<%@ page import="job_applicant_application.CandidateCountDTO" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDAO" %>
<%@ page import="java.util.stream.Collectors" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_RECRUITMENT_JOB_DESCRIPTION;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
	List<CategoryLanguageModel> categoryLanguageModelList = (List<CategoryLanguageModel>)request.getAttribute("categoryLanguageModelList");
	Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = (Recruitment_job_descriptionDTO)request.getAttribute("recruitment_job_descriptionDTO");

CommonDTO commonDTO = recruitment_job_descriptionDTO;
String servletName = "Recruitment_job_descriptionServlet";

Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
Approval_execution_tableDTO approval_execution_tableDTO = null;
String Message = "Done";
approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("recruitment_job_description", recruitment_job_descriptionDTO.iD);

System.out.println("recruitment_job_descriptionDTO = " + recruitment_job_descriptionDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";

	int serial = (int)request.getAttribute("serial");
Recruitment_job_descriptionDAO recruitment_job_descriptionDAO = (Recruitment_job_descriptionDAO)request.getAttribute("recruitment_job_descriptionDAO");

FilesDAO filesDAO = new FilesDAO();
Employee_pay_scaleDAO employee_pay_scaleDAO = new Employee_pay_scaleDAO();

String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

	List<CandidateCountDTO> candidateCountDTOS = new Job_applicant_applicationDAO().
			getQuotaCount(recruitment_job_descriptionDTO.iD);
	Map<Integer, CandidateCountDTO> candidateCountDTOMap = new HashMap(
			candidateCountDTOS.stream().collect(Collectors.toMap(s -> s.quotaId, s -> s))) ;

%>

											
											<td>
												<%= Utils.getDigits(serial + "", Language)%>
											</td>
											
											<td id = '<%=i%>_jobTitleEn'>
											<%
											value = UtilCharacter.getDataByLanguage(Language, recruitment_job_descriptionDTO.
													jobTitleBn, recruitment_job_descriptionDTO.jobTitleEn);
											%>
														
											<%=value%>
				
			
											</td>

<% for(CategoryLanguageModel model: categoryLanguageModelList){ %>

	<%
		int count = 0;
		if(candidateCountDTOMap.get(model.categoryValue) != null){
			count = candidateCountDTOMap.get(model.categoryValue).applied;
		}

		String countValue = UtilCharacter.convertDataByLanguage(Language, count + "");
	%>

	<td>
		<%=countValue%>
	</td>

<%} %>

<%--											<td id = '<%=i%>_jobGradeCat'>--%>
<%--											<%--%>
<%--											String grade = CatDAO.getName(Language, "job_grade", recruitment_job_descriptionDTO.jobGradeCat);--%>
<%--											String payScale = "";--%>
<%--											Employee_pay_scaleDTO employee_pay_scaleDTO = Employee_pay_scaleRepository.getInstance().getEmployee_pay_scaleDTOByID(recruitment_job_descriptionDTO.employeePayScaleType);--%>
<%--											if(employee_pay_scaleDTO != null){--%>
<%--												payScale = CatDAO.getName(Language, "national_pay_scale_type", employee_pay_scaleDTO.nationalPayScaleCat);--%>

<%--											}--%>
<%--											%>	--%>
<%--														--%>
<%--											<%= grade + " (" + payScale + " )"%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>

<%--											--%>
<%--											<td id = '<%=i%>_jobPurpose'>--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.jobPurpose + "";--%>
<%--											%>--%>

<%--											<%=value%>--%>


<%--											</td>--%>

<%--		--%>
<%--											--%>
<%--											<td id = '<%=i%>_numberOfVacancy'>--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.numberOfVacancy + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>


<%

%>

<%

%>

<%--<td>--%>
<%--	<%=Utils.getDigits(candidateCountDTO.applied + "", Language)%>--%>
<%--</td>--%>
<%--<td>--%>
<%--	<%=Utils.getDigits(candidateCountDTO.accepted + "", Language)%>--%>
<%--</td>--%>
<%--<td>--%>
<%--	<%=Utils.getDigits(candidateCountDTO.declined + "", Language)%>--%>
<%--</td>--%>
		

																						
											
<script>
window.onload =function ()
{
    console.log("using ckEditor");
    CKEDITOR.replaceAll();
}
	
</script>	

