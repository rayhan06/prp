<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.StringUtils" %>
<%@ page import="vm_fuel_request.*" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ page import="vm_maintenance.Vm_maintenanceDAO" %>
<%@ page import="vm_maintenance.Vm_maintenanceRepository" %>
<%@ page import="vm_maintenance.Vm_maintenanceDTO" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentDTO" %>
<%@ page import="vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentRepository" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.sql.SQLOutput" %>

<%
    int level = vm_fuel_request_approval_mappingDTO.sequence;
//    System.out.println("###");
//    System.out.println();
//    System.out.println("###");
    Vm_maintenanceDTO vm_maintenanceDTO = Vm_maintenanceRepository.getInstance().
            getVm_maintenanceDTOByID(vm_fuel_request_approval_mappingDTO.vm_fuel_requestId);
%>
<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>





<td id = '<%=i%>_vehicleId'>
    <%
        Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vm_maintenanceDTO.vehicleId);
        value = "";
        if(vm_vehicleDTO != null){
            value = vm_vehicleDTO.regNo;
        }
    %>

    <%=Utils.getDigits(value, Language)%>


</td>



<td id = '<%=i%>_vehicleDriverAssignmentId'>
    <%
        Vm_vehicle_driver_assignmentDTO assignmentDTO = Vm_vehicle_driver_assignmentRepository.getInstance()
                .getVm_vehicle_driver_assignmentDTOByID(vm_maintenanceDTO.vehicleDriverAssignmentId);
        value = "";
        if(assignmentDTO != null){
            value = UtilCharacter.getDataByLanguage(Language,
                    assignmentDTO.employeeRecordNameBn, assignmentDTO.employeeRecordName);
        }
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id = '<%=i%>_lastMaintenanceDate'>
    <%
        value = "";
        if(vm_vehicleDTO != null && vm_vehicleDTO.last_maintenance_date > 0){
            value = simpleDateFormat.format(new Date(Long.parseLong
                    (vm_vehicleDTO.last_maintenance_date + "")));
        }

//											value = vm_maintenanceDTO.lastMaintenanceDate + "";
    %>
    <%
    %>
    <%=Utils.getDigits(value, Language)%>


</td>

<td id = '<%=i%>_applicationDate'>
    <%
        value = vm_maintenanceDTO.applicationDate + "";
    %>
    <%
        String formatted_applicationDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_applicationDate, Language)%>


</td>

<td id = '<%=i%>_vehicleOfficeName'>
    <%
        value = UtilCharacter.getDataByLanguage(Language,
                vm_maintenanceDTO.vehicleOfficeNameBn, vm_maintenanceDTO.vehicleOfficeName);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>




<%--<td id='<%=i%>_cardInfoId'>--%>
<%--    <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(vm_fuel_request_approval_mappingDTO.vm_fuel_requestId))%>--%>
<%--</td>--%>

<%--<td id='<%=i%>_cardEmployeeId'>--%>
<%--    <%--%>
<%--        CardEmployeeInfoDTO cardEmployeeInfoDTO = mapByCardEmployeeInfoId.get(vm_fuel_request_approval_mappingDTO.cardEmployeeInfoId);--%>
<%--        String str = null;--%>
<%--        if (cardEmployeeInfoDTO != null) {--%>
<%--            str = isLanguageEnglish ? cardEmployeeInfoDTO.nameEn : cardEmployeeInfoDTO.nameBn;--%>
<%--        }--%>
<%--        CardEmployeeOfficeInfoDTO cardEmployeeOfficeInfoDTO = mapByCardEmployeeOfficeInfoDTOId.get(vm_fuel_request_approval_mappingDTO.cardEmployeeOfficeInfoId);--%>
<%--        if (cardEmployeeOfficeInfoDTO != null) {--%>
<%--            String s1 = isLanguageEnglish ? cardEmployeeOfficeInfoDTO.organogramEng + "<br>" + cardEmployeeOfficeInfoDTO.officeUnitEng--%>
<%--                    : cardEmployeeOfficeInfoDTO.organogramBng + "<br>" + cardEmployeeOfficeInfoDTO.officeUnitBng;--%>
<%--            if (str == null) {--%>
<%--                str = s1;--%>
<%--            } else {--%>
<%--                str += "<br>" + s1;--%>
<%--            }--%>
<%--        }--%>
<%--        if (str == null) {--%>
<%--            str = "";--%>
<%--        }--%>
<%--    %>--%>
<%--    <%=str%>--%>
<%--</td>--%>
<%--<td id='<%=i%>_cardEmployeeImagesId' style="text-align: center">--%>
<%--    <%--%>
<%--        CardEmployeeImagesDTO cardEmployeeImagesDTO = mapByEmployeeImagesDTOId.get(vm_fuel_requestDTO.cardEmployeeImagesId);--%>
<%--        byte[] encodeBase64Photo = Base64.encodeBase64(cardEmployeeImagesDTO.photo);--%>
<%--    %>--%>
<%--    <img width="75px" height="75px"--%>
<%--         src='data:image/jpg;base64,<%=encodeBase64Photo != null ? new String(encodeBase64Photo) : ""%>'>--%>
<%--</td>--%>

<td id='<%=i%>_cardApprovalStatusCat'>
<%--    <span class="btn btn-sm border-0 text-white btn-border-radius"--%>
<%--          style="background-color: <%=ApprovalStatus.getColor(vm_fuel_request_approval_mappingDTO.vm_fuel_requestApprovalStatusCat)%>; cursor: text">--%>
<%--    </span>--%>
    <%=CatRepository.getInstance().getText(Language, "card_approval_status", vm_fuel_request_approval_mappingDTO.vm_fuel_requestApprovalStatusCat)%>
</td>

<%--<td id='<%=i%>_approverId'>--%>
<%--    <%--%>
<%--        Vm_fuel_requestApprovalDTO cardApprovalDTO = mapByCardApprovalDTOId.get(vm_fuel_request_approval_mappingDTO.vm_fuel_requestApprovalId);--%>
<%--        String str = null;--%>
<%--        if (cardApprovalDTO == null) {--%>
<%--            str = "";--%>
<%--        } else {--%>
<%--            str = isLanguageEnglish ? cardApprovalDTO.nameEng + "<br>" + cardApprovalDTO.organogramEng + "<br>" + cardApprovalDTO.officeUnitEng--%>
<%--                    : cardApprovalDTO.nameBng + "<br>" + cardApprovalDTO.organogramBng + "<br>" + cardApprovalDTO.officeUnitBng;--%>
<%--        }--%>
<%--    %>--%>
<%--    <%=str%>--%>
<%--</td>--%>

<%--action Button Column now hidden --%>

<%--<td id='<%=i%>_approveButton'>--%>
<%--    <%--%>
<%--        boolean hasNextApprover = TaskTypeApprovalPathRepository.getInstance().hasNextApprover(vm_fuel_request_approval_mappingDTO.taskTypeId, vm_fuel_request_approval_mappingDTO.sequence + 1);--%>
<%--        int sequence = vm_fuel_request_approval_mappingDTO.sequence;--%>
<%--        cardInfoId = vm_fuel_request_approval_mappingDTO.cardInfoId;--%>
<%--        boolean policeVerification = vm_fuel_requestDTO.isPoliceVerificationRequired;--%>
<%--        validationStartDate = vm_fuel_requestDTO.validationFrom;--%>
<%--        validationEndDate = vm_fuel_requestDTO.validationTo;--%>
<%--        if (vm_fuel_request_approval_mappingDTO.cardApprovalStatusCat == ApprovalStatus.PENDING.getValue()) {--%>
<%--    %>--%>
<%--        <div class="col-12">--%>
<%--            <a class="btn btn-sm border-0 btn-danger shadow m-2" style="border-radius: 8px; color: white; cursor: pointer"--%>
<%--               onclick="popUp('reject', 'Vm_fuel_request_approval_mappingServlet?actionType=rejectCard&cardInfoId=<%=cardInfoId%>');">--%>
<%--                <%=LM.getText(LC.CARD_APPROVAL_MAPPING_DO_NOT_APPOVE, loginDTO)%>--%>
<%--            </a>--%>
<%--        </div>--%>
<%--        <%--%>
<%--            if (hasNextApprover) {--%>
<%--        %>--%>
<%--            <% if (policeVerification && sequence == 2) {%>--%>
<%--                <div class="col-12">--%>
<%--                    <a class="btn btn-sm border-0 btn-success shadow m-2" style="border-radius: 8px;"--%>
<%--                       href="Police_verificationServlet?actionType=getAddPage&cardInfoId=<%=cardInfoId%>">--%>
<%--                        <%=LM.getText(LC.CARD_APPROVAL_POLICE_VERIFICATION, loginDTO)%>--%>
<%--                    </a>--%>
<%--                </div>--%>
<%--            <%} else {%>--%>
<%--                <div class="col-12">--%>
<%--                    <a class="btn btn-sm border-0 btn-success shadow m-2" style="border-radius: 8px; color: white; cursor: pointer"--%>
<%--                       onclick="popUp('approve', 'Vm_fuel_request_approval_mappingServlet?actionType=approvedCard&cardInfoId=<%=cardInfoId%>');">--%>
<%--                        <%=LM.getText(LC.CARD_APPROVAL_MAPPING_APPROVAL, loginDTO)%>--%>
<%--                    </a>--%>
<%--                </div>--%>
<%--            <%--%>
<%--                }--%>

<%--        } else {--%>
<%--        %>--%>
<%--            &lt;%&ndash;last layer of approval : modal for start-end date&ndash;%&gt;--%>
<%--            <div class="col-12">--%>
<%--                <button type="button" class="btn btn-sm border-0 btn-success shadow m-2" style="border-radius: 8px;"--%>
<%--                        onclick="openModal()"><%=LM.getText(LC.CARD_APPROVAL_MAPPING_APPROVAL, loginDTO)%>--%>
<%--                </button>--%>
<%--            </div>--%>
<%--        <%}%>--%>
<%--    <%--%>
<%--        } else {%>--%>
<%--            <span class="btn btn-sm border-0 shadow"--%>
<%--                  style="background-color: <%=color%>; color: white; border-radius: 8px;cursor: text">--%>
<%--            <%=LM.getText(LC.COMPLETED_COMPLETED, loginDTO)%>--%>
<%--            </span>--%>
<%--    <%--%>
<%--        }--%>
<%--    %>--%>
<%--</td>--%>

<td>
    <%
        if(level == 1 && vm_fuel_request_approval_mappingDTO.vm_fuel_requestApprovalStatusCat == ApprovalStatus.PENDING.getValue()){
    %>
    <button type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="event.preventDefault();location.href='Vm_maintenance_request_approval_mappingServlet?actionType=getApprovalPage&ID=<%=vm_fuel_request_approval_mappingDTO.vm_fuel_requestId%>&language=<%=Language%>'">
        <i class="fa fa-eye"></i>
    </button>

    <% } else if(level == 2 && vm_fuel_request_approval_mappingDTO.vm_fuel_requestApprovalStatusCat == ApprovalStatus.PENDING.getValue()){ %>
    <button type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="event.preventDefault();location.href='Vm_maintenance_request_approval_mappingServlet?actionType=getApprovalPageForAO&ID=<%=vm_fuel_request_approval_mappingDTO.vm_fuel_requestId%>&language=<%=Language%>'">
        <i class="fa fa-eye"></i>
    </button>
    <% } %>

<%--    <%--%>
<%--        Vm_fuel_requestDAO vm_fuel_requestDAO = new Vm_fuel_requestDAO();--%>
<%--        Vm_fuel_requestDTO vm_fuel_requestDTO = vm_fuel_requestDAO.getDTOByID(vm_fuel_request_approval_mappingDTO.vm_fuel_requestId);--%>
<%--        if (vm_fuel_requestDTO.status == Vm_fuel_requestStatusEnum.APPROVED.getValue()) {--%>
<%--    %>--%>
<%--    <button class="btn btn-sm border-0 shadow" style="background-color: #5867dd; color: white; border-radius: 8px; width:45%;"--%>
<%--            onclick="event.preventDefault();location.href='Vm_fuel_request_approval_mappingServlet?actionType=getPaymentPage&cardInfoId=<%=vm_fuel_request_approval_mappingDTO.vm_fuel_requestId%>&language=<%=Language%>'">--%>
<%--        <%=LM.getText(LC.VM_REQUISITION_ADD_ACCEPT_PAYMENT, loginDTO)%>--%>
<%--    </button>--%>
<%--    <%--%>
<%--        }--%>
<%--    %>--%>
</td>

																						
											

