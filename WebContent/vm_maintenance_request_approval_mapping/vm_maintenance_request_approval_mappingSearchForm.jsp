<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="java.util.Enumeration" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="vm_maintenance.Vm_maintenance_request_approval_mappingDTO" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page pageEncoding="UTF-8" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String value = "";
    String Language = LM.getText(LC.CARD_APPROVAL_MAPPING_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = "English".equalsIgnoreCase(Language);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String navigator2 = SessionConstants.NAV_CARD_APPROVAL_MAPPING;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
    Map<Long, Vm_fuel_requestApprovalDTO> mapByCardApprovalDTOId;
    long validationStartDate = SessionConstants.MIN_DATE;
    long validationEndDate = SessionConstants.MIN_DATE;
    long cardInfoId = -1;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<%
    if (!hasAjax) {
        Enumeration<String> parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {

            String paramName = parameterNames.nextElement();

            if (!paramName.equalsIgnoreCase("actionType")) {
                String[] paramValues = request.getParameterValues(paramName);
                for (int i = 0; i < paramValues.length; i++) {
                    String paramValue = paramValues[i];

%>

<% }
}
}
} %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <%--            <th><%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARDINFOID, loginDTO)%>--%>
            <%--            </th>--%>
            <%--            <th><%=LM.getText(LC.CARD_APPROVAL_MAPPING_CARD_REQUESTER_EMPLOYEE, loginDTO)%>--%>
            <%--            </th>--%>
            <%--            <th><%=LM.getText(LC.CARD_INFO_ADD_CARDEMPLOYEEIMAGESID, loginDTO)%></th>--%>

            <th><%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLEID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLEDRIVERASSIGNMENTID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_MAINTENANCE_ADD_LASTMAINTENANCEDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_MAINTENANCE_ADD_APPLICATIONDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLEOFFICENAME, loginDTO)%>
            </th>

            <th><%=UtilCharacter.getDataByLanguage(Language, "অনুমোদন অবস্থা", "Approval Status")%>
            </th>
            <%--            <th><%=LM.getText(LC.CARD_APPROVAL_MAPPING_CARD_APPROVER, loginDTO)%>--%>
            <%--            </th>--%>
            <%--            <th><%=LM.getText(LC.CARD_APPROVAL_ACTION,loginDTO)%></th>--%>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Vm_maintenance_request_approval_mappingDTO> data = (List<Vm_maintenance_request_approval_mappingDTO>) session.getAttribute(SessionConstants.VIEW_CARD_APPROVAL_MAPPING);
            if (data != null && data.size() > 0) {
                List<Long> cardApprovalIds = new ArrayList<>();
                data.forEach(dto -> {
                    cardApprovalIds.add(dto.vm_fuel_requestApprovalId);
                });
//                    List<Vm_fuel_requestApprovalDTO> cardApprovalDTOList = Vm_fuel_requestApprovalRepository.getInstance().getByIds(cardApprovalIds);
//                    mapByCardApprovalDTOId = cardApprovalDTOList.stream()
//                            .collect(Collectors.toMap(dto->dto.iD,dto->dto,(e1,e2)->e1));
                for (int i = 0; i < data.size(); i++) {
                    Vm_maintenance_request_approval_mappingDTO vm_fuel_request_approval_mappingDTO = data.get(i);
        %>
        <tr id='tr_<%=i%>'>
            <%@include file="vm_maintenance_request_approval_mappingSearchRow.jsp" %>
        </tr>
        <%
                }
            }
        %>
        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>
<%--<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"--%>
<%--     aria-hidden="true" id="approval_modal">--%>
<%--    <div class="modal-dialog modal-lg">--%>
<%--        <div class="modal-content">--%>
<%--            &lt;%&ndash;------------------------------HEADER--------------------------------------------&ndash;%&gt;--%>
<%--            <div class="modal-header">--%>
<%--                <h4 class="caption" style="color: #56b2cf; margin-left: 10px">--%>
<%--                    <%=LM.getText(LC.CARD_APPROVAL_MAPPING_APPROVAL, loginDTO)%>--%>
<%--                </h4>--%>
<%--            </div>--%>

<%--            &lt;%&ndash;------------------------------BODY--------------------------------------------&ndash;%&gt;--%>
<%--            <div class="modal-body modal-lg">--%>
<%--                <div class="row my-5">--%>
<%--                    <div class="col-12">--%>
<%--                        <form class="form-horizontal text-right"--%>
<%--                              action="Vm_fuel_request_approval_mappingServlet?actionType=approvedCard"--%>
<%--                              id="permanentCardForm" name="permanentCardForm" method="POST"--%>
<%--                              enctype="multipart/form-data" onsubmit="return PreprocessBeforeSubmiting()">--%>

<%--                            <input type="hidden" name="cardInfoId" id="cardInfoId" value='<%=cardInfoId%>'>--%>
<%--                            <div class="form-group row ">--%>
<%--                                <label class="col-3 col-form-label">--%>
<%--                                    <%=LM.getText(LC.CARD_APPROVAL_MAPPING_VALIDATION_START_DATE, loginDTO)%>--%>
<%--                                    <span class="required">*</span>--%>
<%--                                </label>--%>
<%--                                <div class="col-9"--%>
<%--                                     id='validation_date_startdiv'>--%>
<%--                                    <jsp:include page="/date/date.jsp">--%>
<%--                                        <jsp:param name="DATE_ID"--%>
<%--                                                   value="validation-start-date-js"></jsp:param>--%>
<%--                                        <jsp:param name="LANGUAGE"--%>
<%--                                                   value="<%=Language%>"></jsp:param>--%>
<%--                                    </jsp:include>--%>
<%--                                    <input type='hidden' class='form-control'--%>
<%--                                           id='validation-start-date'--%>
<%--                                           name='validationStartDate' value=''--%>
<%--                                           tag='pb_html'/>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                            <div class="form-group row ">--%>
<%--                                <label class="col-3 col-form-label">--%>
<%--                                    <%=LM.getText(LC.CARD_APPROVAL_MAPPING_VALIDATION_END_DATE, loginDTO)%>--%>
<%--                                    <span class="required">*</span>--%>
<%--                                </label>--%>
<%--                                <div class="col-9"--%>
<%--                                     id='validation_date_enddiv'>--%>
<%--                                    <jsp:include page="/date/date.jsp">--%>
<%--                                        <jsp:param name="DATE_ID"--%>
<%--                                                   value="validation-end-date-js"></jsp:param>--%>
<%--                                        <jsp:param name="LANGUAGE"--%>
<%--                                                   value="<%=Language%>"></jsp:param>--%>
<%--                                        <jsp:param name="END_YEAR"--%>
<%--                                                   value="2050"></jsp:param>--%>
<%--                                    </jsp:include>--%>
<%--                                    <input type='hidden' class='form-control'--%>
<%--                                           id='validation-end-date'--%>
<%--                                           name='validationEndDate' value=''--%>
<%--                                           tag='pb_html'/>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                            <div class="row my-5" align="right">--%>
<%--                                <div class="col-12">--%>
<%--                                    <button type="button" class="btn btn-danger shadow" style="border-radius: 8px;"--%>
<%--                                            data-dismiss="modal"--%>
<%--                                    ><%=LM.getText(LC.CLOSE_CLOSE, loginDTO)%>--%>
<%--                                    </button>--%>
<%--                                    <button class="btn btn-success shadow ml-2" style="border-radius: 8px;"--%>
<%--                                            type="submit"><%=LM.getText(LC.CARD_INFO_ADD_CARD_INFO_SUBMIT_BUTTON, loginDTO)%>--%>
<%--                                    </button>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                        </form>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>

<%--            &lt;%&ndash;            &lt;%&ndash;------------------------------FOOTER--------------------------------------------&ndash;%&gt;&ndash;%&gt;--%>
<%--            &lt;%&ndash;            <div class="modal-footer border-0">&ndash;%&gt;--%>

<%--            &lt;%&ndash;            </div>&ndash;%&gt;--%>
<%--        </div>--%>
<%--    </div>--%>
<%--</div>--%>
<%--<script>--%>
<%--    $(document).ready(function () {--%>
<%--        setDateByTimestampAndId('validation-start-date-js', <%=validationStartDate%>);--%>
<%--        setDateByTimestampAndId('validation-end-date-js', <%=validationEndDate%>);--%>
<%--        $('#validation-start-date-js').on('datepicker.change', () => {--%>
<%--            setMinDateById('validation-end-date-js', getDateStringById('validation-start-date-js'));--%>
<%--        });--%>
<%--    });--%>
<%--    $('#approval_modal').on('show.bs.modal', function () {--%>

<%--    });--%>

<%--    function openModal() {--%>
<%--        $("#approval_modal").modal();--%>
<%--    }--%>

<%--    function PreprocessBeforeSubmiting() {--%>
<%--        $('#validation-start-date').val(getDateStringById('validation-start-date-js'));--%>
<%--        $('#validation-end-date').val(getDateStringById('validation-end-date-js'));--%>
<%--        console.log("validation-start-date : " + $('#validation-start-date').val());--%>
<%--        console.log("validation-end-date : " + $('#validation-end-date').val());--%>
<%--        return dateValidator('validation-start-date-js', true, {--%>
<%--            'errorEn': 'Enter Validation Start Date!',--%>
<%--            'errorBn': 'ভ্যালিডির শুরুর দিন প্রবেশ করুন!'--%>
<%--        }) && dateValidator('validation-end-date-js', true, {--%>
<%--            'errorEn': 'Enter Validation End Date!',--%>
<%--            'errorBn': 'ভ্যালিডির শেষের দিন প্রবেশ করুন!'--%>
<%--        });--%>
<%--    }--%>

<%--    function popUp(action, redirectUrl){--%>
<%--        let message;--%>
<%--        let title;--%>
<%--        switch (action){--%>
<%--            case 'reject':--%>
<%--                message = '<%=isLanguageEnglish? "Are you sure to reject?" : "আপনি কি বাতিল করার ব্যাপারে নিশ্চিত?"%>';--%>
<%--                title = '<%=isLanguageEnglish? "Reject Card" : "কার্ড বাতিল করুন"%>';--%>
<%--                break;--%>
<%--            case 'approve':--%>
<%--                message = '<%=isLanguageEnglish? "Are you sure to approve?" : "আপনি কি অনুমোদন করার ব্যাপারে নিশ্চিত?"%>';--%>
<%--                title = '<%=isLanguageEnglish? "Approve Card" : "কার্ড অনুমোদন করুন"%>';--%>
<%--                break;--%>
<%--        }--%>
<%--        messageDialog(title, message, 'error', true, title, () => {--%>
<%--            window.location = redirectUrl;--%>
<%--        });--%>
<%--    }--%>
<%--</script>--%>

			