<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="recruitment_test_name.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
Recruitment_test_nameDTO recruitment_test_nameDTO = new Recruitment_test_nameDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	recruitment_test_nameDTO = Recruitment_test_nameDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = recruitment_test_nameDTO;
String tableName = "recruitment_test_name";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.RECRUITMENT_TEST_NAME_ADD_RECRUITMENT_TEST_NAME_ADD_FORMNAME, loginDTO);
String servletName = "Recruitment_test_nameServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>

                                                    <input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=recruitment_test_nameDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.RECRUITMENT_TEST_NAME_ADD_NAMEEN, loginDTO)%><span class="required" style="color: red;"> * </span></label>
                                                            <div class="col-8">
																<input type='text' class='form-control englishOnly noPaste'  name='nameEn' id = 'nameEn_text_<%=i%>' value='<%=recruitment_test_nameDTO.nameEn%>'   tag='pb_html'/>
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.RECRUITMENT_TEST_NAME_ADD_NAMEBN, loginDTO)%><span class="required" style="color: red;"> * </span></label>
                                                            <div class="col-8">
																<input type='text' class='form-control noEnglish noPaste'  name='nameBn' id = 'nameBn_text_<%=i%>' value='<%=recruitment_test_nameDTO.nameBn%>'   tag='pb_html'/>
															</div>
                                                      </div>									

									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.RECRUITMENT_TEST_NAME_ADD_RECRUITMENT_TEST_NAME_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button" onclick="submitForm()">
                                <%=LM.getText(LC.RECRUITMENT_TEST_NAME_ADD_RECRUITMENT_TEST_NAME_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

const recruitmentTestForm = $("#bigform");

function submitForm(){
    buttonStateChange(true);
    if(PreprocessBeforeSubmiting()){
        let url =  "<%=servletName%>?actionType=<%=actionName%>";
        $.ajax({
            type : "POST",
            url : url,
            data : recruitmentTestForm.serialize(),
            dataType : 'JSON',
            success : function(response) {
                if(response.responseCode === 0){
                    $('#toast_message').css('background-color','#ff6063');
                    showToastSticky(response.msg,response.msg);
                    buttonStateChange(false);
                }else if(response.responseCode === 200){
                    window.location.replace(getContextPath()+response.msg);
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChange(false);
            }
        });
    }else{
        buttonStateChange(false);
    }
}

function buttonStateChange(value){
    $('#submit-btn').prop('disabled',value);
    $('#cancel-btn').prop('disabled',value);
}

function PreprocessBeforeSubmiting()
{
    recruitmentTestForm.validate();
    return recruitmentTestForm.valid();
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Recruitment_test_nameServlet");	
}

function init(row)
{


	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;

$(".englishOnly").keypress(function(event){
    var allowed = "~`!@#$%^&*()_-+=|\'\"\\|:?/;<>,.}]{[";
    if (allowed.includes(event.key))return true;
    var ew = event.which;
    if(ew == 32)
        return true;
    if(48 <= ew && ew <= 57)
        return true;
    if(65 <= ew && ew <= 90)
        return true;
    if(97 <= ew && ew <= 122)
        return true;
    return false;
});
$(".noEnglish").keypress(function(event){
    var allowed = "~`!@#$%^&*()_-+=|\'\"\\|:?/;<>,.}]{[";
    if (allowed.includes(event.key))return true;
    var ew = event.which;

    if(48 <= ew && ew <= 57)
        return false;
    if(65 <= ew && ew <= 90)
        return false;
    if(97 <= ew && ew <= 122)
        return false;
    return true;
});
function onInputPasteNoEnglish(event) {
    var clipboardData = event.clipboardData || window.clipboardData;
    var data = clipboardData.getData("Text");
    var valid = data.toString().split('').every(char => {
        var allowed = "~`!@#$%^&*()_-+=|\'\"\\|:?/;<>,.}]{[";
        if (allowed.includes(char)) return true;
        if ('0' <= char && char <= '9')
            return false;
        if ('a' <= char && char <= 'z')
            return false;
        if ('A' <= char && char <= 'Z')
            return false;
        return true;
    });
    if (valid) return;
    event.stopPropagation();
    event.preventDefault();
}

function onInputPasteEnglishOnly(event) {
    var clipboardData = event.clipboardData || window.clipboardData;
    var data = clipboardData.getData("Text");
    var valid = data.toString().split('').every(char => {
        var allowed = "~`!@#$%^&*()_-+=|\'\"\\|:?/;<>,.}]{[";
        if (allowed.includes(char)) return true;
        if (char.toString().trim() == '')
            return true;
        if ('0' <= char && char <= '9')
            return true;
        if ('a' <= char && char <= 'z')
            return true;
        if ('A' <= char && char <= 'Z')
            return true;
        return false;
    });
    if (valid) return;
    event.stopPropagation();
    event.preventDefault();

    document.querySelectorAll("input.noPaste").forEach(function (input) {
        input.addEventListener("paste", onInputPaste);
    });

    var elements = document.getElementsByClassName("englishOnly");

    for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener('paste', onInputPasteEnglishOnly);
    }

    elements = document.getElementsByClassName("noEnglish");

    for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener('paste', onInputPasteNoEnglish);
    }
    function onInputPaste(event) {
        var clipboardData = event.clipboardData || window.clipboardData;
        if (/^[0-9০-৯]+$/g.test(clipboardData.getData("Text"))) return;
        event.stopPropagation();
        event.preventDefault();
    }
}



</script>






