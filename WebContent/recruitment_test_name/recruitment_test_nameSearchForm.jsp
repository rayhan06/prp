<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="recruitment_test_name.*" %>
<%@ page import="util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>


<%
    String navigator2 = "navRECRUITMENT_TEST_NAME";
    String servletName = "Recruitment_test_nameServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>
<%@ include file="../recruitment_seat_plan/manualLoader.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><%=LM.getText(LC.RECRUITMENT_TEST_NAME_ADD_NAMEEN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.RECRUITMENT_TEST_NAME_ADD_NAMEBN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.USER_ORGANOGRAM_ADD_STATUS, loginDTO)%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language,"সম্পাদনা","Configure")%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.RECRUITMENT_TEST_NAME_SEARCH_RECRUITMENT_TEST_NAME_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span>All</span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<Recruitment_test_nameDTO>) rn2.list;

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Recruitment_test_nameDTO recruitment_test_nameDTO = (Recruitment_test_nameDTO) data.get(i);


        %>
        <tr>

            <input type="hidden" value="<%=recruitment_test_nameDTO.recruitmentTestNamePublishCat%>" id="hidden-publish-cat-<%=i%>">
            <input type="hidden" value="<%=recruitment_test_nameDTO.iD%>" id="id-<%=i%>">
            <td>
                <%
                    value = recruitment_test_nameDTO.nameEn + "";
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>

            <td>
                <%
                    value = recruitment_test_nameDTO.nameBn + "";
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>


            <td id="publish-cat-<%=i%>">

                <%
                    value = CatRepository.getInstance().getText(Language, "recruitment_test_publish", recruitment_test_nameDTO.recruitmentTestNamePublishCat);
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>

            <td>
                <button class="btn btn-sm border-0 shadow" type="button" id="status-change-btn-<%=i%>"
                      style="background-color: <%=Recruitment_test_nameEnum.getColor(recruitment_test_nameDTO.recruitmentTestNamePublishCat)%>;
                              color: white; border-radius: 8px;cursor: pointer" onclick="RecTestStatusChange(this)">
                    <%
                        int btnCat = 1;
                        if(recruitment_test_nameDTO.recruitmentTestNamePublishCat == 1){
                            btnCat = 2;
                        }
                    %>
                        <%=CatRepository.getInstance().getText(
                                Language, "recruitment_test_publish", btnCat
                        )%>
                </button>
            </td>


            <%CommonDTO commonDTO = recruitment_test_nameDTO; %>
            <%@include file="../pb/searchAndViewButton.jsp" %>

            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID'
                                                 value='<%=recruitment_test_nameDTO.iD%>'/></span>
                </div>
            </td>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>

<script>

    const fullPageLoader = $('#full-page-loader');
    $(document).ready(function () {
        fullPageLoader.hide();
    });
    function buttonStateChange(value) {
        $('#status-change-btn').prop('disabled', value);
    }
    function RecTestStatusChange(selected){

        let splitArr  = selected.id.split("-");
        let hiddenSelectedIndex = splitArr[splitArr.length-1];
        let hiddenSelectedId = "hidden-publish-cat-"+hiddenSelectedIndex;
        let tdSelectedId = "publish-cat-"+hiddenSelectedIndex;
        let id = "id-"+hiddenSelectedIndex;
        let publishStatus = $('#'+hiddenSelectedId).val();
        let idVal = $('#'+id).val();

        fullPageLoader.show();
        let url = "<%=servletName%>?actionType=updatePublishStatus&curPublishStatus="+publishStatus+"&id="+idVal;
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (response) {
                
                $('#'+hiddenSelectedId).val(response.recruitmentTestNamePublishStatusCat);
                document.getElementById(selected.id).innerText = response.recruitmentTestNamePublishStatusText;
                document.getElementById(tdSelectedId).innerText = response.recruitmentTestNamePublishOtherStatusText;
                $("#"+selected.id).css('background-color',response.color);
                fullPageLoader.hide();

            },
            error: function (error) {
                fullPageLoader.hide();
                console.log(error);
            }
        });

    }

</script>


			