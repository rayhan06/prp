<%@ page import="java.util.List" %>
<%@ page import="employee_vaccination_info.EmployeeVaccineModel" %>
<%@ page import="pb.Utils" %>
<%@ page import="pb.CatRepository" %>
<%@ page pageEncoding="UTF-8" %>

<input type="hidden" data-ajax-marker="true">
<table class="table table-bordered table-striped" style="font-size:14px">
    <thead>
    <tr>
        <th>
            <b>
                <%=isLanguageEnglish ? "Dose Number" : "কত তম ডোস"%>
            </b>
        </th>
        <th>
            <b>
                <%=isLanguageEnglish ? "Date of Vaccination" : "ভ্যাক্সিন গ্রহণের তারিখ"%>
            </b>
        </th>
        <th>
            <b>
                <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_VACCINENAME, loginDTO)%>
            </b>
        </th>
        <th>
            <b>
                <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_CERTIFICATENUMBER, loginDTO)%>
            </b>
        </th>
        <th>
            <b>
                <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_VACCINECENTER, loginDTO)%>
            </b>
        </th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <%
        List<EmployeeVaccineModel> savedVaccineModelList = (List<EmployeeVaccineModel>) request.getAttribute("savedVaccineModelList");
        int vaccineIndex = 0;
        if (savedVaccineModelList != null && savedVaccineModelList.size() > 0) {
            for (EmployeeVaccineModel employeeVaccineModel : savedVaccineModelList) {
                ++vaccineIndex;
    %>
    <tr>
        <td style="vertical-align: middle;text-align: left">
            <%=Utils.getDigits(employeeVaccineModel.doseNumber, Language)%>
        </td>
        <td style="vertical-align: middle;text-align: left">
            <%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, employeeVaccineModel.doseTime), Language)%>
        </td>
        <td style="vertical-align: middle;text-align: left">
            <%=CatRepository.getInstance().getText(Language, "covid_vaccine", employeeVaccineModel.vaccineName)%>
        </td>
        <td style="vertical-align: middle;text-align: left">
            <%=employeeVaccineModel.certificateNumber%>
        </td>
        <td style="vertical-align: middle;text-align: left">
            <%=employeeVaccineModel.vaccineCenter%>
        </td>
        <td style="vertical-align: middle;text-align: center">
            <form action="Employee_vaccination_infoServlet?isPermanentTable=true&actionType=delete&tab=2&ID=<%=employeeVaccineModel.dto.iD%>&empId=<%=ID%>&userId=<%=request.getParameter("userId")%>"
                  method="POST" id="tableForm_vaccine<%=vaccineIndex%>" enctype="multipart/form-data">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button class="btn-success" type="button"
                            title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_EDIT, loginDTO) %>"
                            onclick="location.href='<%=request.getContextPath()%>/Employee_vaccination_infoServlet?actionType=getEditPage&tab=6&ID=<%=employeeVaccineModel.dto.iD%>&empId=<%=ID%>&userId=<%=request.getParameter("userId")%>'">
                        <i class="fa fa-edit"></i>&nbsp
                    </button>
                    <button class="btn-danger" type="button"
                            title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_DELETE, loginDTO) %>"
                            onclick="deleteItem('tableForm_vaccine',<%=vaccineIndex%>)"><i class="fa fa-trash"></i>&nbsp
                    </button>
                </div>
            </form>
        </td>
    </tr>
    <%
            }
        }
    %>
    </tbody>
</table>

<div class="row">
    <div class="col-12 mt-3 text-right">
        <button class="btn btn-gray m-t-10 rounded-pill"
                onclick="location.href='Employee_vaccination_infoServlet?actionType=getAddPage&tab=6&empId=<%=ID%>&userId=<%=request.getParameter("userId")%>'">
            <i class="fa fa-plus"></i>&nbsp; <%= LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_EMPLOYEE_VACCINATION_INFO_ADD_BUTTON, loginDTO) %>
        </button>
    </div>
</div>