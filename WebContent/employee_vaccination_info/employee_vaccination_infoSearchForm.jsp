<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="employee_vaccination_info.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>


<%
    String navigator2 = "navEMPLOYEE_VACCINATION_INFO";
    String servletName = "Employee_vaccination_infoServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_EMPLOYEERECORDSID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_EMPLOYEENAMEEN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_EMPLOYEENAMEBN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_ISFULLYVACCINATED, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_TOTALDOSES, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_DOSE1TIME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_DOSE2TIME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_BOOSTERTIME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_CERTIFICATENUMBER, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_VACCINENAME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_VACCINECENTER, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_SEARCH_EMPLOYEE_VACCINATION_INFO_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span>All</span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<Employee_vaccination_infoDTO>) rn2.list;

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Employee_vaccination_infoDTO employee_vaccination_infoDTO = (Employee_vaccination_infoDTO) data.get(i);


        %>
        <tr>


            <td>
                <%=Utils.getDigits(employee_vaccination_infoDTO.employeeRecordsId, Language)%>
            </td>

            <td>
                <%=employee_vaccination_infoDTO.employeeNameEn%>
            </td>

            <td>
                <%=employee_vaccination_infoDTO.employeeNameBn%>
            </td>

            <td>
                <%=Utils.getYesNo(employee_vaccination_infoDTO.isFullyVaccinated, Language)%>
            </td>

            <td>
                <%=Utils.getDigits(employee_vaccination_infoDTO.totalDoses, Language)%>
            </td>

            <td>
                <%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, employee_vaccination_infoDTO.dose1Time), Language)%>
            </td>

            <td>
                <%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, employee_vaccination_infoDTO.dose2Time), Language)%>
            </td>

            <td>
                <%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, employee_vaccination_infoDTO.boosterTime), Language)%>
            </td>

            <td>
                <%=employee_vaccination_infoDTO.certificateNumber%>
            </td>

            <td>
                <%=CatRepository.getInstance().getText(Language,"covid_vaccine", employee_vaccination_infoDTO.vaccineName)%>
            </td>

            <td>
                <%=employee_vaccination_infoDTO.vaccineCenter%>
            </td>


            <%CommonDTO commonDTO = employee_vaccination_infoDTO; %>
            <%@include file="../pb/searchAndViewButton.jsp" %>

            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID'
                                                 value='<%=employee_vaccination_infoDTO.iD%>'/></span>
                </div>
            </td>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			