<%@page import="employee_vaccination_info.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.util.Date" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="common.BaseServlet" %>

<%
    String empIdStr = request.getParameter("empId");
    if (empIdStr == null) {
        empIdStr = (String) request.getAttribute("empId");
    }
    long empId = 0;
    if (empIdStr != null) {
        empId = Long.parseLong(empIdStr);
    }


    Employee_vaccination_infoDTO vaccinationInfoDTO;

    String context = request.getContextPath() + "/";

%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_EMPLOYEE_VACCINATION_INFO_ADD_FORMNAME, loginDTO);
    boolean isEditPage = false;
    if ("ajax_edit".equals(actionName)) {
        vaccinationInfoDTO = (Employee_vaccination_infoDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
        isEditPage = true;
    } else {
        vaccinationInfoDTO = new Employee_vaccination_infoDTO();
        vaccinationInfoDTO.employeeRecordsId = empId;
    }

    String url = "Employee_vaccination_infoServlet?actionType=" + actionName + "&isPermanentTable=true&empId=" + empId;
    if (request.getParameter("tab") != null) {
        url += "&tab=" + request.getParameter("tab") + "&userId=" + request.getParameter("userId");
    }
    String doseNumberStr = "";
    if (vaccinationInfoDTO.doseNumber > 0) {
        doseNumberStr = String.format("%d", vaccinationInfoDTO.doseNumber);
    }
    String doseDateStr = "";
    if (vaccinationInfoDTO.doseTime == SessionConstants.MIN_DATE) {
        doseDateStr = dateFormat.format(new Date(vaccinationInfoDTO.doseTime));
    }
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="<%=url%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=vaccinationInfoDTO.iD%>'/>

                                    <input type='hidden' class='form-control' name='employeeRecordsId'
                                           id='employeeRecordsId_hidden_<%=i%>'
                                           value='<%=vaccinationInfoDTO.employeeRecordsId%>'/>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right" for="doseNumber">
                                            <%=isLanguageEnglish ? "Dose Number" : "কত তম ডোস"%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control'
                                                   id='doseNumber' name='doseNumber'
                                                   data-only-number="true"
                                                   value='<%=doseNumberStr%>'
                                            >
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=isLanguageEnglish ? "Date of Vaccination" : "ভ্যাক্সিন গ্রহণের তারিখ"%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-8">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="doseTime_js"/>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                            </jsp:include>
                                            <input type='hidden' name='doseTime' id='doseTime' value=''>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right" for="vaccineName">
                                            <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_VACCINENAME, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-8">
                                            <select class='form-control' name='vaccineName' id='vaccineName'>
                                                <%=CatRepository.getInstance().buildOptions("covid_vaccine", Language, vaccinationInfoDTO.vaccineName)%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_CERTIFICATENUMBER, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='certificateNumber'
                                                   id='certificateNumber'
                                                   value='<%=vaccinationInfoDTO.certificateNumber%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right" for="vaccineCenter">
                                            <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_VACCINECENTER, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <textarea type='text' class='form-control'
                                                      name='vaccineCenter' id='vaccineCenter'
                                                      id='vaccineCenter_text_<%=i%>' rows="4"
                                                      placeholder="<%=isLanguageEnglish?"Enter vaccine center":"ভ্যাক্সিনের সেন্টার লিখুন"%>"
                                                      onkeyup="updateDescriptionLen()"
                                                      style="text-align: left;resize: none; width: 100%"
                                                      style="text-align: left;resize: none; width: 100%"><%=vaccinationInfoDTO.vaccineCenter == null ? "" : vaccinationInfoDTO.vaccineCenter.trim()%></textarea>
                                            <p id="center_len" style="width: 100%;text-align: right;font-size: small">
                                                <%=(vaccinationInfoDTO.vaccineCenter == null ? "" : vaccinationInfoDTO.vaccineCenter.trim()).length()%>
                                                /1024
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button">
                                <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_EMPLOYEE_VACCINATION_INFO_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="submit">
                                <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_EMPLOYEE_VACCINATION_INFO_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script type="text/javascript">

    function updateDescriptionLen() {
        $('#center_len').text($('#vaccineCenter_text_0').val().length + "/1024");
    }

    const cancelBtn = $('#cancel-btn');
    const submitBtn = $('#submit-btn');
    const doseTimeInput = $('#doseTime');
    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';

    function PreprocessBeforeSubmiting(row, action) {
        submitAddForm2();
        return false;
    }

    function keyDownEvent(e) {
        let isvalid = inputValidationForIntValue(e, $(this), 3);
        return true == isvalid;
    }

    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Employee_vaccination_infoServlet");
    }

    function init() {
        document.querySelectorAll('[data-only-number="true"]')
                .forEach(inputField => inputField.onkeydown = keyDownEvent);
        select2SingleSelector('#vaccineName', '<%=Language%>');
        $('#doseTime_js').on('datepicker.change', (event, param) => {
            const isValidDate = dateValidator('doseTime_js', true);
            const dateStr = isValidDate ? getDateStringById('doseTime_js') : '';
            doseTimeInput.val(dateStr);
        });
        <%if(isEditPage){%>
        setDateByTimestampAndId('doseTime_js', <%=vaccinationInfoDTO.doseTime%>);
        doseTimeInput.val(getDateStringById('doseTime_js'));
        <%}%>
    }

    $(document).ready(function () {
        init();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    function buttonStateChange(value) {
        cancelBtn.prop("disabled", value);
        submitBtn.prop("disabled", value);
    }
</script>






