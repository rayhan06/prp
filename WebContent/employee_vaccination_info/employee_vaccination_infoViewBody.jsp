<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="employee_vaccination_info.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>

<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.*" %>


<%
    String servletName = "Employee_vaccination_infoServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Employee_vaccination_infoDTO employee_vaccination_infoDTO = Employee_vaccination_infoDAO.getInstance().getDTOByID(id);
    CommonDTO commonDTO = employee_vaccination_infoDTO;
%>
<%@include file="../pb/viewInitializer.jsp" %>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_EMPLOYEE_VACCINATION_INFO_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_EMPLOYEE_VACCINATION_INFO_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_EMPLOYEERECORDSID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=Utils.getDigits(employee_vaccination_infoDTO.employeeRecordsId, Language)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_EMPLOYEENAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=employee_vaccination_infoDTO.employeeNameEn%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_EMPLOYEENAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=employee_vaccination_infoDTO.employeeNameBn%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_ISFULLYVACCINATED, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=Utils.getYesNo(employee_vaccination_infoDTO.isFullyVaccinated, Language)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_TOTALDOSES, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=Utils.getDigits(employee_vaccination_infoDTO.totalDoses, Language)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_DOSE1TIME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, employee_vaccination_infoDTO.dose1Time), Language)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_DOSE2TIME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, employee_vaccination_infoDTO.dose2Time), Language)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_BOOSTERTIME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, employee_vaccination_infoDTO.boosterTime), Language)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_CERTIFICATENUMBER, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=employee_vaccination_infoDTO.certificateNumber%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_VACCINENAME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=CatRepository.getInstance().getText(Language, "covid_vaccine", employee_vaccination_infoDTO.vaccineName)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_VACCINECENTER, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=employee_vaccination_infoDTO.vaccineCenter%>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>