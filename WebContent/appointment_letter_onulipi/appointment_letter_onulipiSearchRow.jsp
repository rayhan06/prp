<%@page pageEncoding="UTF-8" %>

<%@page import="appointment_letter_onulipi.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.APPOINTMENT_LETTER_ONULIPI_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_APPOINTMENT_LETTER_ONULIPI;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Appointment_letter_onulipiDTO appointment_letter_onulipiDTO = (Appointment_letter_onulipiDTO)request.getAttribute("appointment_letter_onulipiDTO");
CommonDTO commonDTO = appointment_letter_onulipiDTO;
String servletName = "Appointment_letter_onulipiServlet";


System.out.println("appointment_letter_onulipiDTO = " + appointment_letter_onulipiDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Appointment_letter_onulipiDAO appointment_letter_onulipiDAO = (Appointment_letter_onulipiDAO)request.getAttribute("appointment_letter_onulipiDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
		
											<td id = '<%=i%>_appointmentLetterId'>
											<%
											value = appointment_letter_onulipiDTO.appointmentLetterId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_employeeRecordId'>
											<%
											value = appointment_letter_onulipiDTO.employeeRecordId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_unitId'>
											<%
											value = appointment_letter_onulipiDTO.unitId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_postId'>
											<%
											value = appointment_letter_onulipiDTO.postId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_employeeRecordName'>
											<%
											value = appointment_letter_onulipiDTO.employeeRecordName + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_employeeRecordNameBn'>
											<%
											value = appointment_letter_onulipiDTO.employeeRecordNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_unitName'>
											<%
											value = appointment_letter_onulipiDTO.unitName + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_unitNameBn'>
											<%
											value = appointment_letter_onulipiDTO.unitNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_postName'>
											<%
											value = appointment_letter_onulipiDTO.postName + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_postNameBn'>
											<%
											value = appointment_letter_onulipiDTO.postNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
											<td id = '<%=i%>_insertedBy'>
											<%
											value = appointment_letter_onulipiDTO.insertedBy + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_modifiedBy'>
											<%
											value = appointment_letter_onulipiDTO.modifiedBy + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
	

											<td>
												<a href='Appointment_letter_onulipiServlet?actionType=view&ID=<%=appointment_letter_onulipiDTO.iD%>'><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></a>
										
											</td>
	
											<td id = '<%=i%>_Edit'>																																	
	
												<a href='Appointment_letter_onulipiServlet?actionType=getEditPage&ID=<%=appointment_letter_onulipiDTO.iD%>'><%=LM.getText(LC.APPOINTMENT_LETTER_ONULIPI_SEARCH_APPOINTMENT_LETTER_ONULIPI_EDIT_BUTTON, loginDTO)%></a>
																				
											</td>											
											
											
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=appointment_letter_onulipiDTO.iD%>'/></span>
												</div>
											</td>
																						
											

