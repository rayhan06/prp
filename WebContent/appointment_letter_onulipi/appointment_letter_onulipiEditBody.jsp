<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="appointment_letter_onulipi.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>

<%
    Appointment_letter_onulipiDTO appointment_letter_onulipiDTO;
    appointment_letter_onulipiDTO = (Appointment_letter_onulipiDTO) request.getAttribute("appointment_letter_onulipiDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (appointment_letter_onulipiDTO == null) {
        appointment_letter_onulipiDTO = new Appointment_letter_onulipiDTO();

    }
    System.out.println("appointment_letter_onulipiDTO = " + appointment_letter_onulipiDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.APPOINTMENT_LETTER_ONULIPI_ADD_APPOINTMENT_LETTER_ONULIPI_ADD_FORMNAME, loginDTO);
    String servletName = "Appointment_letter_onulipiServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
%>

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    String Language = LM.getText(LC.APPOINTMENT_LETTER_ONULIPI_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%>
        </h3>
    </div>
    <div class="box-body">
        <form class="form-horizontal"
              action="Appointment_letter_onulipiServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="form-body">
                <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                       value='<%=appointment_letter_onulipiDTO.iD%>' tag='pb_html'/>
                <input type='hidden' class='form-control' name='appointmentLetterId'
                       id='appointmentLetterId_hidden_<%=i%>'
                       value='<%=appointment_letter_onulipiDTO.appointmentLetterId%>' tag='pb_html'/>
                <input type='hidden' class='form-control' name='employeeRecordId' id='employeeRecordId_hidden_<%=i%>'
                       value='<%=appointment_letter_onulipiDTO.employeeRecordId%>' tag='pb_html'/>
                <input type='hidden' class='form-control' name='unitId' id='unitId_hidden_<%=i%>'
                       value='<%=appointment_letter_onulipiDTO.unitId%>' tag='pb_html'/>
                <input type='hidden' class='form-control' name='postId' id='postId_hidden_<%=i%>'
                       value='<%=appointment_letter_onulipiDTO.postId%>' tag='pb_html'/>
                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.APPOINTMENT_LETTER_ONULIPI_ADD_EMPLOYEERECORDNAME, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='employeeRecordName_div_<%=i%>'>
                        <input type='text' class='form-control' name='employeeRecordName'
                               id='employeeRecordName_text_<%=i%>'
                               value='<%=appointment_letter_onulipiDTO.employeeRecordName%>' tag='pb_html'/>
                    </div>
                </div>
                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.APPOINTMENT_LETTER_ONULIPI_ADD_EMPLOYEERECORDNAMEBN, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='employeeRecordNameBn_div_<%=i%>'>
                        <input type='text' class='form-control' name='employeeRecordNameBn'
                               id='employeeRecordNameBn_text_<%=i%>'
                               value='<%=appointment_letter_onulipiDTO.employeeRecordNameBn%>' tag='pb_html'/>
                    </div>
                </div>


                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.APPOINTMENT_LETTER_ONULIPI_ADD_UNITNAME, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='unitName_div_<%=i%>'>
                        <input type='text' class='form-control' name='unitName' id='unitName_text_<%=i%>'
                               value='<%=appointment_letter_onulipiDTO.unitName%>' tag='pb_html'/>
                    </div>
                </div>


                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.APPOINTMENT_LETTER_ONULIPI_ADD_UNITNAMEBN, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='unitNameBn_div_<%=i%>'>
                        <input type='text' class='form-control' name='unitNameBn' id='unitNameBn_text_<%=i%>'
                               value='<%=appointment_letter_onulipiDTO.unitNameBn%>' tag='pb_html'/>
                    </div>
                </div>


                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.APPOINTMENT_LETTER_ONULIPI_ADD_POSTNAME, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='postName_div_<%=i%>'>
                        <input type='text' class='form-control' name='postName' id='postName_text_<%=i%>'
                               value='<%=appointment_letter_onulipiDTO.postName%>' tag='pb_html'/>
                    </div>
                </div>


                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.APPOINTMENT_LETTER_ONULIPI_ADD_POSTNAMEBN, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='postNameBn_div_<%=i%>'>
                        <input type='text' class='form-control' name='postNameBn' id='postNameBn_text_<%=i%>'
                               value='<%=appointment_letter_onulipiDTO.postNameBn%>' tag='pb_html'/>
                    </div>
                </div>


                <input type='hidden' class='form-control' name='insertionDate' id='insertionDate_hidden_<%=i%>'
                       value='<%=appointment_letter_onulipiDTO.insertionDate%>' tag='pb_html'/>


                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.APPOINTMENT_LETTER_ONULIPI_ADD_INSERTEDBY, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='insertedBy_div_<%=i%>'>
                        <input type='text' class='form-control' name='insertedBy' id='insertedBy_text_<%=i%>'
                               value='<%=appointment_letter_onulipiDTO.insertedBy%>' tag='pb_html'/>
                    </div>
                </div>


                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.APPOINTMENT_LETTER_ONULIPI_ADD_MODIFIEDBY, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='modifiedBy_div_<%=i%>'>
                        <input type='text' class='form-control' name='modifiedBy' id='modifiedBy_text_<%=i%>'
                               value='<%=appointment_letter_onulipiDTO.modifiedBy%>' tag='pb_html'/>
                    </div>
                </div>


                <input type='hidden' class='form-control' name='isDeleted' id='isDeleted_hidden_<%=i%>'
                       value='<%=appointment_letter_onulipiDTO.isDeleted%>' tag='pb_html'/>


                <input type='hidden' class='form-control' name='lastModificationTime'
                       id='lastModificationTime_hidden_<%=i%>'
                       value='<%=appointment_letter_onulipiDTO.lastModificationTime%>' tag='pb_html'/>


                <div class="form-actions text-center">
                    <a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
                        <%=LM.getText(LC.APPOINTMENT_LETTER_ONULIPI_ADD_APPOINTMENT_LETTER_ONULIPI_CANCEL_BUTTON, loginDTO)%>
                    </a>
                    <button class="btn btn-success" type="submit">

                        <%=LM.getText(LC.APPOINTMENT_LETTER_ONULIPI_ADD_APPOINTMENT_LETTER_ONULIPI_SUBMIT_BUTTON, loginDTO)%>

                    </button>
                </div>

            </div>

        </form>

    </div>
</div>

<script type="text/javascript">


    $(document).ready(function () {

        dateTimeInit("<%=Language%>");
    });

    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Appointment_letter_onulipiServlet");
    }

    function init(row) {


    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






