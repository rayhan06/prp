<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="payroll_allowance_configuration.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="employee_records.EmploymentEnum" %>
<%@ page import="economic_sub_code.EconomicSubCodeModel" %>
<%@ page import="com.google.gson.Gson" %>

<%@include file="../pb/addInitializer2.jsp" %>

<%
    String formTitle = LM.getText(LC.PAYROLL_ALLOWANCE_CONFIGURATION_ADD_PAYROLL_ALLOWANCE_CONFIGURATION_ADD_FORMNAME, loginDTO);
    int PRIVILEGED_EMPLOYEE_CAT = EmploymentEnum.PRIVILEGED.getValue();
    List<EconomicSubCodeModel> subCodeModels =
            Payroll_allowance_configurationRepository.getInstance()
                    .getEconomicSubCodeModels(PRIVILEGED_EMPLOYEE_CAT, Language);
%>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    .template-row {
        display: none;
    }

    .row-data-code {
        width: 20%;
    }

    .row-data-name {
        width: 60%;
    }

    input[readonly],
    textarea[readonly] {
        font-weight: bold;
        background-color: rgba(231, 231, 231, .5) !important;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" enctype="multipart/form-data">
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row mt-4 mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="employmentCat">
                                            <%=LM.getText(LC.PAYROLL_ALLOWANCE_CONFIGURATION_ADD_EMPLOYMENTCAT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type="text" class='form-control' readonly
                                                   value="<%=CatRepository.getName(Language, "employment", PRIVILEGED_EMPLOYEE_CAT)%>">
                                            <input type='hidden' name='employmentCat' id="employmentCat"
                                                   value='<%=PRIVILEGED_EMPLOYEE_CAT%>'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5">
                    <div class="table-responsive">
                        <input type="hidden" name="economicSubCodes" id="economicSubCodes-input">
                        <table class="table table-bordered table-striped text-nowrap" id="budget-view-table">
                            <thead>
                            <tr>
                                <th class="row-data-code">
                                    <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, loginDTO)%>
                                </th>
                                <th class="row-data-name">
                                    <%=LM.getText(LC.BUDGET_DESCRIPTION, loginDTO)%>
                                </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                            <%--Template Row-> to be cloned to add new row.
                                CONVENTIONS:
                                    * one tr with template-row class
                                    * td with class -> row-data-filedNameInJson
                            --%>
                            <tr class="template-row">
                                <td class="row-data-code"></td>
                                <td class="row-data-name"></td>
                                <td class="row-data-add-btn">
                                    <button class='btn btn-sm cancel-btn text-white shadow pl-4'
                                            style="padding-right: 14px;" type="button"
                                            onclick="unSelectRow(this);">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="text-right">
                        <button class="btn btn-sm btn-success shadow btn-border-radius"
                                id="add-economic-code-btn"
                                type="button">
                            <%=LM.getText(LC.BUDGET_ADD_ECONOMIC_CODE, loginDTO)%>
                        </button>
                    </div>
                </div>
                <div class="row my-5">
                    <div class="col-12 text-center">
                        <button id="cancel-btn" type="button"
                                class="btn btn-sm shadow text-white btn-border-radius cancel-btn mx-2"
                                onclick="location.href='<%=request.getHeader("referer")%>'">
                            <%=LM.getText(LC.CARD_INFO_ADD_CARD_INFO_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button id="submit-btn" class="btn btn-sm shadow text-white submit-btn" type="button"
                                onclick="submitForm();">
                            <%=LM.getText(LC.PERFORMANCE_LOG_REPORT_SAVE, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="codeSelectModal.jsp"/>

<script type="text/javascript">
    const form = $('#bigform');

    function submitForm() {
        const economicSubCodes = Array.from(economicSubCodeMap.keys());
        $('#economicSubCodes-input').val(JSON.stringify(economicSubCodes));

        setButtonDisableState(true);
        $.ajax({
            type: "POST",
            url: "Payroll_allowance_configurationServlet?actionType=ajax_add",
            data: form.serialize(),
            dataType: 'JSON',
            success: function (response) {
                if (response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                    setButtonDisableState(false);
                } else if (response.responseCode === 200) {
                    window.location.replace(getContextPath() + response.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                setButtonDisableState(false);
            }
        });
    }

    function setButtonDisableState(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function getSortIndex(economicCodeId) {
        let code = economicCodeId.replaceAll(economicCodeIdPrefix, '');
        code = code.padEnd(7, '0'); // make all 7 digit number
        return Number(code);
    }

    function sortBudgetViewTable() {
        const tableBody = document.getElementById('budget-view-table').querySelector('tbody');
        const rowsObj = Array.from(tableBody.rows).map(row => {
            return {
                data: getSortIndex(row.id),
                rowElement: row
            };
        });
        rowsObj.sort((a, b) => {
            if (a.data === b.data) return 0;
            return a.data < b.data ? -1 : 1;
        });
        tableBody.innerHTML = '';
        rowsObj.forEach(rowObj => tableBody.append(rowObj.rowElement));
    }

    $(document).ready(function () {
        $('#add-economic-code-btn').on('click', function () {
            $('#economic-code-modal').modal();
        });
        // modal on close event
        $('#economic-code-modal').on("hidden.bs.modal", sortBudgetViewTable);

        const economicSubCodeModels = JSON.parse('<%=new Gson().toJson(subCodeModels)%>');
        if (Array.isArray(economicSubCodeModels)) {
            economicSubCodeModels.forEach(economicSubCodeModel => {
                addDataInTable('budget-view-table', economicSubCodeModel, true);
                economicSubCodeMap.set(
                    Number(economicSubCodeModel.id),
                    economicSubCodeModel
                );
            });
            sortBudgetViewTable();
            $('#economic-code-3211109').find('.row-data-add-btn')[0].innerHTML='';//This code non removable
        }
    });
</script>