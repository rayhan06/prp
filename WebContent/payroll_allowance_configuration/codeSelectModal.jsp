<%@page import="sessionmanager.SessionConstants" %>
<%@page import="user.UserRepository" %>
<%@page import="user.UserDTO" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.CommonDAO" %>
<%@ page import="pb.CatDAO" %>
<%@ page import="payroll_allowance_configuration.Payroll_allowance_configurationServlet" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = LM.getText(LC.CARD_INFO_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    String context = request.getContextPath() + "/";
%>

<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="economic-code-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h4 class="caption" style="color: #56b2cf; margin-left: 10px">
                    <%=LM.getText(LC.BUDGET_SEARCH_ECONOMIC_CODE, userDTO)%>
                </h4>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body modal-xl">
                <div class="col-md-12">
                    <div class="table-responsive px-3">
                        <table class="table table-bordered table-striped  text-nowrap" id="economic-code-table">
                            <thead>
                            <tr>
                                <th class="row-data-code">
                                    <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, loginDTO)%>
                                </th>
                                <th class="row-data-name">
                                    <%=LM.getText(LC.BUDGET_DESCRIPTION, loginDTO)%>
                                </th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody class="main-tbody">
                            </tbody>

                            <%--don't put these tr inside tbody--%>
                            <tr class="loading-gif" style="display: none;">
                                <td class="text-center" colspan="100%">
                                    <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
                                    <span><%=getDataByLanguage(Language, "লোড হচ্ছে...", "Loading...")%></span>
                                </td>
                            </tr>
                            <%--Template Row-> to be cloned to add new row.
                                CONVENTIONS:
                                    * one tr with template-row class
                                    * td with class -> row-data-filedNameInJson
                             --%>
                            <tr class="template-row">
                                <td class="row-data-code"></td>
                                <td class="row-data-name"></td>
                                <td class="row-data-add-btn">
                                    <button class='btn btn-sm shadow d-flex justify-content-between align-items-center'
                                            style="background-color: #66ce5f; color: white; border-radius: 8px;"
                                            type="button"
                                            onclick="selectRow(this)">
                                        <i class="fa fa-plus"></i>
                                        <%=LM.getText(LC.HM_ADD, loginDTO)%>
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <%--------------------------------FOOTER----------------------------------------------%>
                <div class="modal-footer border-0">
                    <button type="button" class="btn cancel-btn text-white shadow btn-border-radius"
                            data-dismiss="modal">
                        <%=LM.getText(LC.CLOSE_CLOSE, loginDTO)%>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    const economicCodeIdPrefix = 'economic-code-';
    const economicCodeTableId = 'economic-code-table';
    const economicSubCodeMap = new Map(); // TODO: init map with existing data
    let economicSubCodesJson = null;

    function showOrHideLoadingGif(tableId, toShow) {
        const loadingGif = $('#' + tableId + ' tr.loading-gif');
        if(toShow) loadingGif.show();
        else loadingGif.hide();
    }

    // modal on load event
    $('#economic-code-modal').on('show.bs.modal', function () {
        economicCodeChanged('<%=Payroll_allowance_configurationServlet.EMPLOYEE_REMUNERATION_ECONOMIC_CODE%>');
    });

    async function economicCodeChanged(selectedEconomicCode) {
        document.getElementById(economicCodeTableId).querySelector('tbody.main-tbody').innerHTML = '';

        if (selectedEconomicCode === '') return;
        const url = 'Budget_mappingServlet?actionType=getEconomicSubCode&economicCode=' + selectedEconomicCode;

        showOrHideLoadingGif(economicCodeTableId, true);
        if(economicSubCodesJson === null){
            const response = await fetch(url);
            economicSubCodesJson = await response.json();
        }
        showOrHideLoadingGif(economicCodeTableId, false);
        showSubCodesInTable(economicSubCodesJson, economicSubCodeMap);
    }

    function showSubCodesInTable(economicSubCodes, economicSubCodeMap) {
        if (!Array.isArray(economicSubCodes)) return;

        for (let economicSubCode of economicSubCodes) {
            const economicSubCodeId = Number(economicSubCode.id);
            if (economicSubCodeMap.has(economicSubCodeId)) continue;

            addDataInTable('economic-code-table', economicSubCode);
        }

        const tableBody = document.querySelector('#economic-code-table tbody');
        if (tableBody.innerText.trim() === '') {
            tableBody.innerHTML =
                '<tr><td colspan="3" class="text-center">'
                + '<%=LM.getText(LC.BUDGET_CODE_SELECTION_NO_UNSELECTED_ECONOMIC_CODE_FOUND, loginDTO)%>'
                + '</td></tr>';
        }
    }

    function addDataInTable(tableId, jsonData, toInsertInEconomicGroup) {
        const table = document.getElementById(tableId);

        const templateRow = table.querySelector('.template-row').cloneNode(true);
        templateRow.id = economicCodeIdPrefix + jsonData.economicSubCode;
        templateRow.classList.remove('template-row');

        templateRow.dataset.rowData = JSON.stringify(jsonData);

        templateRow.querySelector('td.row-data-code').innerText = jsonData.code;
        templateRow.querySelector('td.row-data-name').innerText = jsonData.name;

        const tableBody = table.querySelector('tbody');
        if (!toInsertInEconomicGroup) tableBody.append(templateRow);
        else addWithEconomicGroup(jsonData, tableBody, templateRow);
    }

    function createEconomicGroupingTr(economicCodeObj) {
        const tr = document.createElement('tr');
        tr.id = economicCodeIdPrefix + economicCodeObj.code;
        const td = document.createElement('td');
        td.colSpan = 3;
        td.innerHTML = '<b>' + convertToBanglaBasedOnLanguage(economicCodeObj.code, '<%=Language%>')
            + ' - ' + economicCodeObj.name + '</b>';
        tr.dataset.numberOfChildren = '0';
        tr.append(td);
        return tr;
    }

    function getEconomicGroupRow(jsonData, tableBody) {
        let economicGroupRow = document.getElementById(economicCodeIdPrefix + jsonData.economicGroup);
        if (economicGroupRow == null) {
            const economicGroupObj = {
                code: jsonData.economicGroup,
                name: jsonData.economicGroupName
            };
            economicGroupRow = createEconomicGroupingTr(economicGroupObj);
            tableBody.append(economicGroupRow);
        }
        return document.getElementById(economicCodeIdPrefix + jsonData.economicGroup);
    }

    function addWithEconomicGroup(jsonData, tableBody, economicSubCodeRow) {
        let economicCodeRow = document.getElementById(economicCodeIdPrefix + jsonData.economicCode);
        if (economicCodeRow == null) {
            let economicGroupRow = getEconomicGroupRow(jsonData, tableBody);

            const economicCodeObj = {
                code: jsonData.economicCode,
                name: jsonData.economicCodeName
            };
            economicCodeRow = createEconomicGroupingTr(economicCodeObj);

            $(economicCodeRow).insertAfter(economicGroupRow);
            economicGroupRow.dataset.numberOfChildren++;
        }
        economicCodeRow = document.getElementById(economicCodeIdPrefix + jsonData.economicCode);

        economicSubCodeRow.id = economicCodeIdPrefix + jsonData.economicSubCode;
        $(economicSubCodeRow).insertAfter(economicCodeRow);
        economicCodeRow.dataset.numberOfChildren++;
    }

    function convertToBanglaBasedOnLanguage(numberStr, language) {
        if ("english" === language.toLowerCase()) return numberStr;
        let str = String(numberStr);
        str = str.replaceAll('0', '০');
        str = str.replaceAll('1', '১');
        str = str.replaceAll('2', '২');
        str = str.replaceAll('3', '৩');
        str = str.replaceAll('4', '৪');
        str = str.replaceAll('5', '৫');
        str = str.replaceAll('6', '৬');
        str = str.replaceAll('7', '৭');
        str = str.replaceAll('8', '৮');
        str = str.replaceAll('9', '৯');
        return str;
    }

    function getRowDataAndMoveRow(rowButtonElement, destTableId) {
        const containingRow = rowButtonElement.parentNode.parentNode;
        const rowData = JSON.parse(containingRow.dataset.rowData);
        if (destTableId) addDataInTable(destTableId, rowData, true);
        containingRow.remove();
        return rowData;
    }

    function selectRow(rowButtonElement) {
        const economicSubCode = getRowDataAndMoveRow(rowButtonElement, 'budget-view-table');
        economicSubCodeMap.set(
            Number(economicSubCode.id),
            economicSubCode
        );
    }

    function unSelectRow(rowButtonElement) {
        const economicSubCode = getRowDataAndMoveRow(rowButtonElement);
        economicSubCodeMap.delete(Number(economicSubCode.id));

        const economicCodeTrId = economicCodeIdPrefix + economicSubCode.economicCode;
        const economicCodeTr = document.getElementById(economicCodeTrId);
        economicCodeTr.dataset.numberOfChildren--;
        if (economicCodeTr.dataset.numberOfChildren === '0') {
            economicCodeTr.remove();
            const economicGroupTrId = economicCodeIdPrefix + economicSubCode.economicGroup;
            const economicGroupTr = document.getElementById(economicGroupTrId);
            economicGroupTr.dataset.numberOfChildren--;
            if (economicGroupTr.dataset.numberOfChildren === '0')
                economicGroupTr.remove();
        }
    }
</script>