<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="payroll_allowance_configuration.*" %>
<%@ page import="util.*" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>

<%
    String navigator2 = "navPAYROLL_ALLOWANCE_CONFIGURATION";
    String servletName = "Payroll_allowance_configurationServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th>
                <%=LM.getText(LC.PAYROLL_ALLOWANCE_CONFIGURATION_ADD_EMPLOYMENTCAT, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, loginDTO)%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Payroll_allowance_configurationDTO> data = (List<Payroll_allowance_configurationDTO>) rn2.list;
            try {
                if (data != null) {
                    for (Payroll_allowance_configurationDTO dto : data) {
        %>
        <tr>
            <td>
                <%=CatRepository.getInstance().getText(Language, "employment", dto.employmentCat)%>
            </td>
            <td>
                <%=Economic_sub_codeRepository.getInstance().getText(Language, dto.economicSubCodeId)%>
            </td>
        </tr>
        <%
                    }
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>