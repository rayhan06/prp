<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="util.*" %>
<%@ page import="budget_mapping.Budget_mappingDTO" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="budget_operation.Budget_operationRepository" %>
<%@ page import="static util.StringUtils.convertToBanNumber" %>
<%@ page import="overtime_allowance.Overtime_allowanceDTO" %>
<%@ page import="static util.StringUtils.convertToBanNumber" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="budget.BudgetCategoryEnum" %>
<%@ page import="religion_allowance.Religion_allowanceDTO" %>
<%@ page import="allowance_configure.AllowanceCatEnum" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="religion_allowance.Religion_allowanceDAO" %>
<%@ page import="pb.Utils" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberInWord" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@page pageEncoding="UTF-8" %>

<%
    List<Religion_allowanceDTO> allowanceDTOs = (List<Religion_allowanceDTO>) request.getAttribute("religionAllowanceDTOList");
    Religion_allowanceDAO religion_allowanceDAO = Religion_allowanceDAO.getInstance();
    int eidFitrAmount = religion_allowanceDAO.getSelectedFestivalSum(allowanceDTOs, AllowanceCatEnum.EID_UL_FITR.getValue());
    int eidAzhaAmount = religion_allowanceDAO.getSelectedFestivalSum(allowanceDTOs, AllowanceCatEnum.EID_UL_AZHA.getValue());
    int hinduAmount = religion_allowanceDAO.getSelectedFestivalSum(allowanceDTOs, AllowanceCatEnum.HINDU_FESTIVAL.getValue());
    int buddhismAmount = religion_allowanceDAO.getSelectedFestivalSum(allowanceDTOs, AllowanceCatEnum.BUDDHISM_ALLOWANCE.getValue());
    int cristianityAmount = religion_allowanceDAO.getSelectedFestivalSum(allowanceDTOs, AllowanceCatEnum.CHRISTIANITY_ALLOWANCE.getValue());
    int banglaNewYearAmount = religion_allowanceDAO.getSelectedFestivalSum(allowanceDTOs, AllowanceCatEnum.BANGLA_NEW_YEAR_ALLOWANCE.getValue());
    long sumTotalAmount = eidFitrAmount + eidAzhaAmount + hinduAmount + buddhismAmount + cristianityAmount + banglaNewYearAmount;
    String totalAmount = convertToBanNumber(String.valueOf(sumTotalAmount));
    int year = (int) request.getAttribute("year");
    long budgetOfficeId = (long) request.getAttribute("budgetOfficeId");
    Budget_mappingDTO budgetMappingDTO = Budget_mappingRepository.getInstance()
            .getDTO(budgetOfficeId, BudgetCategoryEnum.OPERATIONAL.getValue());

    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getLanguage(loginDTO);

    String pdfFileName = "Festival Bill Forwarding "
            + "Year " + year + " "
            + Budget_officeRepository.getInstance().getText(budgetOfficeId, "English");

    String budgetOfficeNameBn = Budget_officeRepository.getInstance().getText(budgetOfficeId, "Bangla");
    long billTime = new Date().getTime();
    String voucherNumber = "";
    if (allowanceDTOs.size() > 0) {
        voucherNumber = allowanceDTOs.get(0).voucherNumber;
        billTime = allowanceDTOs.get(0).lastModificationTime;
    }

%>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    .fix-fill {
        overflow: hidden;
        white-space: nowrap;
    }

    #to-print-div * {
        font-size: 12px;
    }

    #to-print-div h1 {
        font-size: 16px;
        font-weight: bold;
    }

    #to-print-div h2 {
        font-size: 14px;
        font-weight: bold;
    }

    #to-print-div h3 {
        font-size: 13px;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .5in;
        background: white;
        margin-bottom: 10px;
    }

    .foot-note {
        font-size: 11px !important;
    }

    .blank-to-fill {
        display: inline-block;
        border-bottom: 1px dotted black;
        width: inherit;
    }

    .table-bordered > :not(caption) > * {
        border-color: black;
    }

    .table-bordered > :not(caption) > * > * {
        border-color: black;
    }

    .table-bordered td,
    .table-bordered th {
        padding: 5px;
    }

    .align-top {
        vertical-align: top;
    }

    th {
        text-align: center;
    }
</style>

<div class="kt-content p-0" id="kt_content">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title prp-page-title">
                        <%=UtilCharacter.getDataByLanguage(Language, "উৎসব ভাতা বিল", "Festival Allowance Bill")%>
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body" id="bill-div">
                <div class="ml-auto m-3">
                    <button type="button" class="btn" id='download-pdf'
                            onclick="downloadTemplateAsPdf('to-print-div', '<%=pdfFileName%>');">
                        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                </div>

                <div style="margin: auto;">
                    <%--Reference Documents from Parliament\Kawsar, AO, Fin 01550445791\O.T Bil Forwarding  All  February  21--%>
                    <div class="container" id="to-print-div">
                        <section class="page shadow" data-size="A4">
                            <div class="text-center">
                                <h1>সংস্থাপন কর্মচারীগণের বেতনের বিল</h1>
                                <h2>
                                    বাংলাদেশ জাতীয় সংসদের
                                    <%=budgetOfficeNameBn%>
                                    কার্যালয়ে
                                    কর্মরত <%=convertToBanNumber(String.valueOf(allowanceDTOs.size()))%> জন কর্মকর্তার
                                    <%=DateUtils.getMonthYear(billTime, Language, ", ")%> খ্রিঃ মাসের নিয়মিত উৎসব ভাতা
                                    বিল
                                </h2>
                                <h3>
                                    দপ্তরের
                                    নামঃ&nbsp;<%=budgetOfficeNameBn%>
                                </h3>
                                <h3>
                                    <%-- ১০২ সংসদ কোড-অপারেশন কোড-৩১১১৩২৭ অধিকাল ভাতা কোড --%>
                                    কোড নংঃ
                                    ১০২-<%=Budget_operationRepository.getInstance().getCode(budgetMappingDTO.budgetOperationId, "Bangla")%>
                                </h3>
                            </div>

                            <div>
                                <div class="mt-4">
                                    <div class="row">
                                        <div class="col-3 fix-fill">
                                            টোকেন নং
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="col-3 fix-fill">
                                            তারিখ
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="col-3 fix-fill">
                                            ভাউচার নং
                                            <div class="blank-to-fill">&nbsp; <%=voucherNumber%>
                                            </div>
                                        </div>
                                        <div class="col-3 fix-fill">
                                            তারিখ
                                            <div class="blank-to-fill">&nbsp; <%=Utils.getDigits(new SimpleDateFormat("dd-MM-yyyy").format(new Date(billTime)), "Bangla")%>
                                            </div>
                                        </div>
                                    </div>

                                    <table class="table-bordered mt-2 w-100">
                                        <thead>
                                        <tr>
                                            <th width="3%"></th>
                                            <th width="37%">নির্দেশাবলী</th>
                                            <th width="35%">বিবরণ</th>
                                            <th width="25%">টাকা</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="align-top"> ১</td>
                                            <td class="align-top">
                                                অবিলিকৃত/স্থ’গিত টাকা যথাযথ কলামে লাল কালিতে লিখিতে হইবে এবং যোগ দেওয়ার
                                                সময় উহা বাদ
                                                রাখিতে হইবে।
                                            </td>
                                            <td>
                                                ** উৎসব ভাতা
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(Utils.getDigits(sumTotalAmount, "BANGLA"))%>/-
                                            </td>
                                        </tr>


                                        <tr>
                                            <td class="align-top" rowspan="3"> ২</td>
                                            <td class="align-top" rowspan="3">
                                                বেতন বৃদ্ধিও সার্টিফিকেট বা অনুুপস্থিত কর্মচারীগণের তালিকায়ক স্থান পায়
                                                নাই এমন ঘটনাসমূহ যথা-মৃত্যু, অবসর গ্রহণ, স্থায়ী বদরী ও প্রথম নিয়োগ
                                                মন্তব্য কলামে
                                                লিখিতে
                                                হইবে।
                                            </td>
                                            <td><br></td>
                                            <td><br></td>
                                        </tr>
                                        <tr>
                                            <td><br></td>
                                            <td><br></td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <strong>
                                                    মোট দাবী (ক) =
                                                </strong>
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(totalAmount)%>/-
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="align-top" rowspan="2"> ৩</td>
                                            <td class="align-top" rowspan="2">
                                                কোন দাবিকৃত বেতন বৃদ্ধি সরকারী কর্মচারীর দক্ষতার সীমা অতিক্রম করার আওতায়
                                                পডিলে
                                                সংশ্লিষ্ট কর্মচারী উক্ত সীমা অতিক্রম করার উপযুক্ত কর্তৃপক্ষের প্রত্যায়ন
                                                দ্বারা
                                                সমর্থিত হইতে হইবে। (এস আর ১৫৬)।
                                            </td>
                                            <td class="text-center">

                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                        </tr>

                                        <tr>
                                            <td class="align-top" rowspan="2"> ৪</td>
                                            <td class="align-top" rowspan="2">
                                                অধঃস্তন সরকারী কর্মচারী এবং এস. আর. ১৫২ তে উলি­খিত সরকারী সরকারী
                                                কর্মচারদের নাম
                                                বেতনের বিলে বাদ দেওয়া যাইতে পারে।
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                        </tr>

                                        <tr>
                                            <td class="align-top" rowspan="3"> ৫</td>
                                            <td class="align-top" rowspan="3">
                                                অধঃস্তন সরকারী কর্মচারী এবং এস. আর. ১৫২ তে উলি­খিত সরকারী সরকারী
                                                কর্মচারদের নাম
                                                বেতনের বিলে বাদ দেওয়া যাইতে পারে।
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                        </tr>

                                        <tr>
                                            <td class="align-top" rowspan="4"> ৬</td>
                                            <td class="align-top" rowspan="4">
                                                স্থায়ী পদে নিযুক্ত ব্যক্তিদের নাম স্থায়ী পদের বেতন গ্রহণের মাপ কাঠিতে
                                                জ্যেষ্ঠত্বের
                                                ক্রম অনুসারে লিখিতে হইবে এবং খালি পদসমূহ স্থানাপন্ন লোকদিগকে দেখাইতে
                                                হইবে।
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                        </tr>

                                        <tr>
                                            <td class="align-top" rowspan="4"> ৭</td>
                                            <td class="align-top" rowspan="4">
                                                বেতন বিলে কর্তন ও অদায়ের পৃথক পৃথক সিডিউল বেতনের বিলে সংযুক্ত করিতে
                                                হইবে।
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <strong>
                                                    মোট কর্তন আদায় (খ)
                                                </strong>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <strong>
                                                    নীট দাবী (ক-খ)
                                                </strong>
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(totalAmount)%>/-
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <br>
                                                প্রদানের জন্য নীট টাকার প্রয়োজন
                                                কথায় <%=BangladeshiNumberInWord.convertToWord(String.valueOf(Utils.getDigits(totalAmount,"BANGLA")))%>
                                                টাকা (মাত্র)
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="foot-note">
                                <div>
                                    * কেবল মাত্র অর্থনৈতিক কোড বুঝায়।<br>
                                    ** সর্ম্পূণ ১৩ অংকের কোড দেওয়া হইয়াছে।
                                </div>
                            </div>
                        </section>

                        <section class="page shadow" data-size="A4">
                            <p>
                                ১. (ক) বিলের টাকা বুঝিয়া পাইলাম <br>
                                (খ) প্রত্যয়ন করিতেছি যে, নিম্নে বিশদভাবে বর্ণিত টাকা (যাহা এই বিল হইতে কর্তন করিয়া ফেরত
                                দেওয়া
                                হইয়াছে) ব্যতীত এই তারিখের <br> ১* মাস/২ মাস/৩ মাস পূর্বে উত্তোলিত বিলের অন্তভূক্ত টাকা
                                যথার্থ
                                ব্যক্তিদের
                                প্রদান করা হইয়াছে। <br>
                                * প্রযোজ্য ক্ষেত্রে টিক ঢিহ্ন দিন। <br>
                                (গ) প্রত্যয়ন করিতেছি যে, কর্মচারীদের নিকট হইতে অর্থ প্রাপ্তির ষ্ট্যাম্পসহ রশিদ গ্রহন
                                করিয়া বেতন সহিত
                                সংযুক্ত করা হইয়াছে।<br>
                                ২. বিলের সাথে একটি অনুপস্থিতির তালিকা প্রদান করা হইল।<br>
                                ৩. প্রত্যয়ন করা যাইতেছে যে, এই কার্যালয়ের সকল নিয়োগ, স্থায়ী ও অস্থায়ী পদোন্নতি সংক্রান্ত
                                তথ্যাদি সংশ্লিষ্ট কর্মচারীগণের নিজ নিজ চাকুরী বহিতে আমার সত্যায়নে লিপিবদ্ধ হইয়াছে।<br>
                                ৪. প্রত্যায়ন করা যাইতেছে, চাকুরী বহিতে প্রাপ্য ছুটির হিসাব এবং প্রযোজ্য ছুটির বিধি
                                অনুয়ায়ী
                                প্রাপ্য ছুটি ছাড়া কাহাকেও কোন ছুটি মঞ্জুর করা হয় নাই। আমি নিশ্চিত যে তাহাদের ছুটি পাওনা
                                ছিল এবং সকল
                                ছুটির মঞ্জুরী ও ছুটিতে বা ছুটি হইতে ফিরিয়া আসা, সাময়িক কর্মচ্যুতি ও অন্য কাজে যাওয়া ও
                                অন্যান্য ঘটনা
                                নিয়ম
                                মোতাবেক চাকুরী বহিতে এবং ছুটির হিসাবে আমার সত্যায়নে লিপিবদ্ধ করা হইযাছে।<br>
                                ৫. প্রত্যায়ন করা যাইতেছে যে, যে সকল সরকারী কর্মচারীর নাম উলে­খ করা হয় নাই, কিন্তু এই
                                বিলে
                                বেতন দাবী করা হইয়াছে। চলতি মাসে তাহারা যথার্থই সরকারী চাকুরীতে নিয়োজিত ছিলেন।<br>
                                ৬. প্রত্যায়ন করা যাইতেছে যে, যে সকল সরকারী কর্মচারীর বাড়ী ভাড়া ভাতা এই বিলে দাবী করা
                                হইয়াছে,
                                তাহারা সরকারী কোন বাসস্থানে বসবাস করেন নাই।<br>
                                ৭. প্রত্যায়ন করা যাইতেছে, যে ক্ষেত্রে ছুটির/অস্থায়ী বদলী কালীন সময়ের জন্য ক্ষতিপূরণ ভাতা
                                দাবী
                                করা হইয়াছে, সেই ক্ষেত্রে কর্মচারীর একই বা স¦পদে ফিরিয়া আসার সম্ভাব্যতা ছুটি/অস্থায়ী
                                বদলীর মূল আদেশে
                                লিপিবদ্ধ করা হইয়াছে।<br>
                                ৮. প্রত্যায়ন করা যাইতেছে যে, কর্মচারীদের ছুটি কালীন বেতন, ছুটিতে যাওয়ার সময় যে হারে বেতন
                                গ্রহণ করিতেছিলেন, সেই হাওে দাবী করা হইয়াছে।<br>
                                ৯. প্রত্যায়ন করা যাইতেছে যে, অবসর গ্রহণ করিয়াছেন এমন কোন কর্মচারীর নাম এই বিলে
                                অন্তর্ভূক্ত
                                করা হয় নাই।<br>
                            </p>

                            <h3 class="text-center">অনুপস্থিত ব্যক্তির ফেরত দেওয়া বেতনের বিবরণ</h3>
                            <table class="table-bordered w-100">
                                <thead>
                                <tr>
                                    <th width="8%">সেকশন</th>
                                    <th width="50%">নাম</th>
                                    <th width="15%">সময়</th>
                                    <th width="27%">টাকার অংক (টা./পয়সা)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td rowspan="4"></td>
                                    <td rowspan="4"></td>
                                    <td class="text-right">
                                        বাজেটে বরাদ্দ =
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-right">
                                        মোট খরচ =
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-right">
                                        অবশিষ্ট =
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <br>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                            <div class="row mt-4">
                                <div class="col-6">
                                    <div class="mt-1">
                                        <div class="fix-fill mt-4 w-100">
                                            স্থান
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            তারিখ
                                            <div class="blank-to-fill"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mt-1">
                                        <div class="fix-fill mt-4 w-100">
                                            আয়ন কর্মকর্তার স্বাক্ষর
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            নাম
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            পদবী
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-5 w-100">
                                            সীল
                                            <div class="blank-to-fill"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mt-3" style="border-top: 5px solid black;">
                                <div class="text-center mt-2">
                                    <h2>হিসাবরক্ষণ অফিসে ব্যবহারের জন্য</h2>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-4 fix-fill">
                                        টাকা
                                        <div class="blank-to-fill">&nbsp;<%=BangladeshiNumberFormatter.getFormattedNumber(Utils.getDigits(totalAmount,"BANGLA"))%>/-</div>
                                    </div>
                                    <div class="col-8 fix-fill">
                                        (কথায়)
                                        <div class="blank-to-fill">&nbsp;<%=BangladeshiNumberInWord.convertToWord(String.valueOf(Utils.getDigits(totalAmount,"BANGLA")))%></div>
                                    </div>
                                    প্রদানের জন্য পাস কর হল
                                </div>

                                <div class="row mt-5">
                                    <div class="col-4">
                                        <div>
                                            <strong>অডিটর (স্বাক্ষর)</strong>
                                        </div>
                                        <div class="fix-fill mt-5">
                                            নাম........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                        </div>
                                        <div class="fix-fill mt-3">
                                            তাং........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div>
                                            <strong>সুপার (স্বাক্ষর)</strong>
                                        </div>
                                        <div class="fix-fill mt-5">
                                            নাম........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                        </div>
                                        <div class="fix-fill mt-3">
                                            তাং........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div>
                                            <strong>হিসাবরক্ষণ অফিসার (স্বাক্ষর)</strong>
                                        </div>
                                        <div class="fix-fill mt-5">
                                            নাম........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                        </div>
                                        <div class="fix-fill mt-3">
                                            তাং........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
</div>

<script>
    function downloadTemplateAsPdf(divId, fileName) {
        let content = document.getElementById(divId);
        const opt = {
            margin: 0.5,
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4'}
        };
        html2pdf().from(content).set(opt).save();
    }
</script>