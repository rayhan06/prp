<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="religion_allowance.*" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>

<%
    //    Religion_allowanceDTO religion_allowanceDTO;
//    religion_allowanceDTO = (Religion_allowanceDTO) request.getAttribute("religion_allowanceDTO");
//    CommonDTO commonDTO = religion_allowanceDTO;
//    if (religion_allowanceDTO == null) {
//        religion_allowanceDTO = new Religion_allowanceDTO();
//
//    }
    String tableName = "religion_allowance";
%>
<%@include file="../pb/addInitializer.jsp" %>
<%
    String formTitle = LM.getText(LC.RELIGION_ALLOWANCE_ADD_RELIGION_ALLOWANCE_ADD_FORMNAME, loginDTO);
    String servletName = "Religion_allowanceServlet";
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String context = request.getContextPath() + "/";
    int currentYear = Calendar.getInstance().get(Calendar.YEAR);
%>

<style>
    .template-row {
        display: none;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              id="allowance-form" name="allowanceform" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12">
                        <div class="row mt-2">
                            <div class="col-md-4 form-group">
                                <label class="h5" for="budgetInstitutionalGroup">
                                    <%=LM.getText(LC.BUDGET_INSTITUTIONAL_GROUP, loginDTO)%>
                                </label>
                                <select id="budgetInstitutionalGroup"
                                        name='budgetInstitutionalGroupId'
                                        class='form-control rounded shadow-sm'
                                        onchange="institutionalGroupChanged(this);">
                                    <%=Budget_institutional_groupRepository.getInstance().buildOptions(
                                            Language, 0L, false
                                    )%>
                                </select>
                            </div>
                            <div class="col-md-4 form-group">

                                <label class="h5" for="budgetOffice">
                                    <%=LM.getText(LC.BUDGET_OFFICE, loginDTO)%>
                                </label>
                                <select id="budgetOffice" name='budgetOfficeId'
                                        class='form-control rounded shadow-sm'
                                        onchange="clearNextLevels(1);"
                                >
                                    <%--Dynamically Added with AJAX--%>
                                </select>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="h5"><%=LM.getText(LC.ALLOWANCE_CAT_RELIGION_EVENT, loginDTO)%>
                                </label>
                                <select class='form-control' name='religion'
                                        id='religion' tag='pb_html' onchange="searchSubmit()">
                                    <%
                                        Options = Religion_allowanceDAO.getInstance().buildReligionAllowanceCat(Language, -1L);
                                    %>
                                    <%=Options%>
                                </select>

                            </div>
                            <div class="col-3" hidden>
                                <label class="h5"><%=LM.getText(LC.ADD_RELIGION_ALLOWANCE_YEAR_YEAR, loginDTO)%>
                                </label>
                                <select class='form-control' name='year'
                                        onchange="clearNextLevels(1);" id='year' tag='pb_html'
                                >
                                    <%
                                        Options = Religion_allowanceDAO.getInstance().buildYears(Language, -1L);
                                    %>
                                    <%=Options%>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="modal-body" id="loader">
                        <img alt="" class="loading"
                             src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
                        <span>Loading...</span>
                    </div>

                    <div class="table-responsive mt-4">
                        <table class="table text-nowrap" id="allowance-view-table">
                            <thead>
                            <tr>
                                <th><%=isLanguageEnglish ? "Employee Name" : "কর্মকর্তার নাম"%>
                                </th>
                                <th><%=isLanguageEnglish ? "Designation" : "পদবী"%>
                                </th>
                                <th><%=isLanguageEnglish ? "Mobile Number" : "মোবাইল নাম্বার"%>
                                </th>
                                <th><%=isLanguageEnglish ? "Savings Account Number" : "সঞ্চয়ী একাউন্ট নাম্বার"%>
                                </th>
                                <th><%=isLanguageEnglish ? "Main Salary" : "মূল বেতন"%>
                                </th>
                                <th><%=isLanguageEnglish ? "Bonus amount" : "বোনাসের পরিমাণ"%>
                                </th>
                                <th><%=isLanguageEnglish ? "Tax Deduction" : "কর কর্তন"%>
                                </th>
                                <th><%=isLanguageEnglish ? "Net Amount" : "নেট পরিমাণ"%>
                                </th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                            <tr class="template-row">
                                <td class="row-data-name"></td>
                                <td class="row-data-designation"></td>
                                <td class="row-data-mobile-number"></td>
                                <td class="row-data-savings-number"></td>
                                <td class="row-data-main-salary"></td>
                                <td class="row-data-bonus-amount"></td>
                                <td class="row-data-tax-deduction"></td>
                                <td class="row-data-net-amount"></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <input type="hidden" name="religionAllowanceModels" id="religionAllowanceModels">
                <div class="row mt-4">
                    <div class="col-12 mt-3 text-right">
                        <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                type="button" onclick="submitReligionAllowanceForm()">
                            <%=getDataByLanguage(Language, "বিল প্রস্তুত করুন", "Prepare Bill")%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<%@include file="../common/table-sum-utils.jsp" %>
<script type="text/javascript">
    const allowanceModelMap = new Map();
    const bigForm = $('#allowance-form');
    const billBtn = $('#bill-btn');
    const submitBtn = $('#submit-btn');
    const year = <%=currentYear%>;

    function PreprocessBeforeSubmiting(row, validate) {

        //submitReligionAllowanceForm();
        return false;
    }

    function submitReligionAllowanceForm() {
        if (!bigForm.valid()) return;
        setButtonState(true);
        // const allowanceModels = Array.from(allowanceModelMap.values());

        // for (let allowanceModel of allowanceModels) {
        //     allowanceModel.taxDeduction = document.getElementById('taxDeduction_' + allowanceModel.employeeRecordsId).value;
        //     allowanceModelMap.set(Number(allowanceModel.employeeRecordsId), allowanceModel);
        // }
        $('#religionAllowanceModels').val(JSON.stringify(Array.from(allowanceModelMap.values())));
        $.ajax({
            type: "POST",
            url: "Religion_allowanceServlet?actionType=ajax_<%=actionName%>&isPermanentTable=true&id=<%=ID%>",
            data: bigForm.serialize(),
            dataType: 'JSON',
            success: function (response) {
                if (response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                    setButtonState(false);
                } else if (response.responseCode === 200) {
                    window.location.assign(getContextPath() + response.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                setButtonState(false);
            }
        });
    }

    function setButtonState(value) {
        submitBtn.prop("disabled", value);
    }

    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Religion_allowanceServlet");
    }

    function init() {
        clearTable(true)
    }

    $(() => {
        <%--$('#bill-btn').click(function () {--%>
        <%--    const budgetOfficeId = document.getElementById('budgetOffice').value;--%>
        <%--    location.href = '<%=servletName%>?actionType=generateBill&year=<%=currentYear%>&budgetOfficeId=' + budgetOfficeId--%>
        <%--});--%>
        $('#loader').hide();
        clearTable(true);

        bigForm.validate({
            rules: {
                budgetInstitutionalGroupId: "required",
                budgetOfficeId: "required",
                religion: "required"
            },
            messages: {
                budgetInstitutionalGroupId: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                budgetOfficeId: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                religion: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>'
            }
        });
    });

    function clearTable(isFirstTime) {
        allowanceModelMap.clear();
        const noDataText = '<%=isLanguageEnglish? "No data found!" : "কোনো তথ্য পাওয়া যায় নি!"%>';
        const table = document.querySelector('#allowance-view-table');
        if (!isFirstTime) {
            table.querySelector('tbody').innerHTML = '<tr><td colspan="100%" class="text-center">' + noDataText + '</td></tr>';
        } else {
            table.querySelector('tbody').innerHTML = ''
        }

        table.querySelectorAll('tfoot').forEach(tfoot => tfoot.remove());
    }

    function keyDownEvent(e) {
        let isvalid = inputValidationForFloatValue(e, $(this), 2, 100.0);
        return true == isvalid;
    }

    function keyUpEvent(e) {
        const id = e.path[0].id.split("_")[1];
        if (id == '') {
            return;
        }
        let taxDeduction = $(this).val();
        if (taxDeduction == '') {
            taxDeduction = 0.0;
        }
        const bonusAmount = document.getElementById('bonus_amount_' + id);
        const netAmount = document.getElementById('net_amount_' + id);
        netAmount.innerText = Number(bonusAmount.innerText) - Number(bonusAmount.innerText) * (Number(taxDeduction) / 100.0)
    }

    function getAllowanceInputElement(id, value) {
        const inputElement = document.createElement('input');
        inputElement.classList.add('form-control');
        inputElement.type = 'text';
        inputElement.id = id;
        inputElement.value = value;
        inputElement.onkeydown = keyDownEvent;
        inputElement.onkeyup = keyUpEvent;
        return inputElement;
    }

    function addDataInTable(tableId, jsonData) {
        const table = document.getElementById(tableId);
        const templateRow = table.querySelector('.template-row').cloneNode(true);
        templateRow.id = jsonData.employeeRecordsId;
        templateRow.classList.remove('template-row');

        templateRow.dataset.rowData = JSON.stringify(jsonData);

        templateRow.querySelector('td.row-data-name').innerText = jsonData.employeeName;
        templateRow.querySelector('td.row-data-designation').innerText = jsonData.designation;
        templateRow.querySelector('td.row-data-mobile-number').innerText = jsonData.mobileNumber;
        templateRow.querySelector('td.row-data-savings-number').innerText = jsonData.savingAccountNumber;
        templateRow.querySelector('td.row-data-main-salary').innerText = jsonData.mainSalary;
        templateRow.querySelector('td.row-data-bonus-amount').innerText = jsonData.bonusAmount;
        templateRow.querySelector('td.row-data-net-amount').innerText = jsonData.netAmount;
        templateRow.querySelector('td.row-data-tax-deduction').innerText = jsonData.taxDeduction;
        // const taxDeduction = templateRow.querySelector('td.row-data-tax-deduction');
        // taxDeduction.append(
        //     getAllowanceInputElement(
        //         'taxDeduction_' + jsonData.employeeRecordsId,
        //         jsonData.taxDeduction
        //     )
        // );

        const tableBody = table.querySelector('tbody');
        tableBody.append(templateRow);
    }

    function clearReligion() {
        document.getElementById('religion').value = '';
    }

    // function clearYear() {
    //     document.getElementById('year').innerHTML = '';
    // }

    function clearOffice() {
        document.getElementById('budgetOffice').innerHTML = '';
    }

    function clearNextLevels(startIndex) {
        const toClearFunctions = [
            clearOffice, clearReligion
        ];
        for (let i = startIndex; i < toClearFunctions.length; i++) {
            toClearFunctions[i]();
        }
        clearTable(true)
    }

    async function institutionalGroupChanged(selectElement) {
        const selectedInstitutionalGroupId = selectElement.value;
        clearNextLevels(0);
        if (selectedInstitutionalGroupId === '') return;
        setButtonState(true);
        const url = 'Budget_mappingServlet?actionType=getBudgetOfficeList&withCode=false'
            + '&budget_instituitional_group_id=' + selectedInstitutionalGroupId;

        const response = await fetch(url);
        document.getElementById('budgetOffice').innerHTML = await response.text();
        setButtonState(false);
    }

    function changePrepareBillBtnText(isAlreadyAdded) {
        let btnText = '<%=isLanguageEnglish ? "Prepare Bill" : "বিল প্রস্তুত করুন"%>';
        if(isAlreadyAdded) {
            btnText = '<%=isLanguageEnglish ? "Show Prepared Bill" : "প্রস্তুতকৃত বিল দেখুন"%>';
        }
        submitBtn.text(btnText);
    }

    async function searchSubmit() {
        clearTable();
        const selectedReligion = document.getElementById('religion').value;
        const budgetOfficeId = document.getElementById('budgetOffice').value;
        const selectedYear = year;
        if (selectedReligion === '' || budgetOfficeId === '' || selectedYear === '') {
            return;
        }
        setButtonState(true);
        $('#allowance-view-table').hide();
        $('#loader').show();
        // console.log(selectedReligion, selectedYear)
        const url = 'Religion_allowanceServlet?actionType=ajax_getEmployeeList&allowanceCat='
            + selectedReligion + '&year=' + selectedYear + '&budgetOfficeId=' + budgetOfficeId;

        const response = await fetch(url);
        const resJson = await response.json();
        const religionAllowanceModels = resJson.allowanceModels;
        const isAlreadyAdded = resJson.isAlreadyAdded;
        changePrepareBillBtnText(isAlreadyAdded);
        setButtonState(false);
        $('#loader').hide();
        $('#allowance-view-table').show();
        clearTable();
        const tbody = document.getElementById('allowance-view-table').querySelector('tbody');
        tbody.innerHTML = '';

        const hasData = Array.isArray(religionAllowanceModels) && religionAllowanceModels.length > 0;
        if (hasData) {
            if (religionAllowanceModels.length == 0) {
                clearTable()
            }
            for (const religionAllowanceModel of religionAllowanceModels) {
                console.log("aisi");
                addDataInTable('allowance-view-table', religionAllowanceModel);
                allowanceModelMap.set(
                    Number(religionAllowanceModel.employeeRecordsId),
                    religionAllowanceModel
                );
            }

            const colIndicesToSum = [7];
            const totalTitleColSpan = 1;
            const totalTitle = '<%=getDataByLanguage(Language, "সর্বমোট", "Total")%>';
            setupTotalRow('allowance-view-table', totalTitle, totalTitleColSpan, colIndicesToSum, 0);
        } else {
            clearTable();
        }
    }

    $(document).ready(function () {
        init(row);
    });
</script>






