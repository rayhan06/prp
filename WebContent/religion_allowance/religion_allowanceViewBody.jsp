<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="religion_allowance.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>
<%@ page import="allowance_employee_info.AllowanceEmployeeInfoDTO" %>
<%@ page import="allowance_employee_info.AllowanceEmployeeInfoRepository" %>


<%
    String servletName = "Religion_allowanceServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Religion_allowanceDTO religion_allowanceDTO = Religion_allowanceDAO.getInstance().getDTOFromID(id);
    AllowanceEmployeeInfoDTO infoDTO= AllowanceEmployeeInfoRepository.getInstance().getById(religion_allowanceDTO.allowanceEmployeeInfoId);
    CommonDTO commonDTO = religion_allowanceDTO;
%>
<%@include file="../pb/viewInitializer.jsp" %>



<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.RELIGION_ALLOWANCE_ADD_RELIGION_ALLOWANCE_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-md-8 offset-md-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.RELIGION_ALLOWANCE_ADD_RELIGION_ALLOWANCE_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.ALLOWANCE_CAT_RELIGION_EVENT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=CatRepository.getInstance().getText(Language,"allowance",religion_allowanceDTO.allowanceCat)%>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_EMPLOYEERECORDSID, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=isLanguageEnglish?infoDTO.nameEn:infoDTO.nameBn%>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.RELIGION_ALLOWANCE_SEARCH_OFFICE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=isLanguageEnglish?infoDTO.officeNameEn:infoDTO.officeNameBn%>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.RELIGION_ALLOWANCE_SEARCH_DESIGNATION, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=isLanguageEnglish?infoDTO.organogramNameEn:infoDTO.organogramNameBn%>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.RELIGION_ALLOWANCE_SEARCH_MAIN_SALARY, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=Utils.getDigits(infoDTO.mainSalary,Language)%>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.RELIGION_ALLOWANCE_ADD_BONUSAMOUNT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=Utils.getDigits(religion_allowanceDTO.bonusAmount, Language)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.RELIGION_ALLOWANCE_ADD_TAXDEDUCTION, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=Utils.getDigits(religion_allowanceDTO.taxDeduction, Language)%>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.RELIGION_ALLOWANCE_SEARCH_NET_AMOUNT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=Utils.getDigits(religion_allowanceDTO.bonusAmount*(1-(religion_allowanceDTO.taxDeduction/100.0)), Language)%>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>