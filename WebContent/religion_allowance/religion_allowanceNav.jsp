<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="religion_allowance.Religion_allowanceDAO" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    System.out.println("Inside nav.jsp");
    String url = "Religion_allowanceServlet?actionType=search";
    String toolbarOpen=request.getParameter("toolbarOpen")==null?"0":request.getParameter("toolbarOpen");
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0,false)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.ALLOWANCE_CAT_RELIGION_EVENT, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='allowance_cat'
                                    id='allowance_cat' tag='pb_html'>
                                <%=Religion_allowanceDAO.getInstance().buildReligionAllowanceCat(Language, -1L)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.ADD_RELIGION_ALLOWANCE_YEAR_YEAR, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='year'
                                    id='year' tag='pb_html'>
                                <%=Religion_allowanceDAO.getInstance().buildYears(Language, -1L)%>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <div class="search-loader-container-circle ">
            <div class="search-loader-circle"></div>
        </div>
    </div>
</template>

<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>
<script type="text/javascript">
    const allowanceCatSelector = $('#allowance_cat');
    const yearSelector = $('#year');
    const anyFieldSelector = $('#anyfield');

    function resetInputs() {
        allowanceCatSelector.select2("val", '-1');
        yearSelector.select2("val", '-1');
        anyFieldSelector.val('');
    }

    window.addEventListener('popstate', e => {
        if (e.state) {
            let params = e.state;
            dosubmit(params, false);
            resetInputs();
            let arr = params.split('&');
            arr.forEach(e => {
                let item = e.split('=');
                if (item.length === 2) {
                    switch (item[0]) {
                        case 'allowance_cat':
                            allowanceCatSelector.select2("val", item[1]);
                            break;
                        case 'year':
                            yearSelector.select2("val", item[1]);
                            break;
                        case 'AnyField':
                            anyFieldSelector.val(item[1]);
                            break;
                        default:
                            setPaginationFields(item);
                    }
                }
            });
        } else {
            dosubmit(null, false);
            resetInputs();
            resetPaginationFields();
        }
    });
    $(document).ready(function () {
        readyInit('Religion_allowanceServlet');
        select2SingleSelector("#allowance_cat", '<%=Language%>');
        select2SingleSelector("#year", '<%=Language%>');
        if('<%=toolbarOpen%>'=='1'){
            document.querySelector('.kt-portlet__body').style.display = 'block';
            document.querySelector('#kt_portlet_tools_1').classList.remove('kt-portlet--collapse')
        }
    });

    function dosubmit(params, pushState = true) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (pushState) {
                    history.pushState(params, '', 'Religion_allowanceServlet?actionType=search&' + params);
                }
                setTimeout(() => {
                    document.getElementById('tableForm').innerHTML = this.responseText ;
                    setPageNo();
                    searchChanged = 0;
                }, 500);
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        let url = "<%=action%>&ajax=true&isPermanentTable=true";
        if (params) {
            url += "&" + params;
        }
        xhttp.open("Get", url, false);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number, pushState = true) {
        var params = 'AnyField=' + document.getElementById('anyfield').value;
        params += '&allowance_cat=' + document.getElementById('allowance_cat').value;
        params += '&year=' + document.getElementById('year').value;
        params += '&search=true';
        if(document.getElementsByClassName("kt-portlet__body")[0].style['display']!='none'){
            params+='&toolbarOpen=1';
        }
        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params, pushState);

    }

</script>

