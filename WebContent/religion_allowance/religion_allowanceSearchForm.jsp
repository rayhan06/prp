<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="religion_allowance.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>
<%@ page import="allowance_employee_info.AllowanceEmployeeInfoDTO" %>
<%@ page import="allowance_employee_info.AllowanceEmployeeInfoRepository" %>


<%
    String navigator2 = "navRELIGION_ALLOWANCE";
    String servletName = "Religion_allowanceServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_EMPLOYEERECORDSID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.ALLOWANCE_CAT_RELIGION_EVENT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.RELIGION_ALLOWANCE_ADD_BONUSAMOUNT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.RELIGION_ALLOWANCE_ADD_TAXDEDUCTION, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span>All</span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Religion_allowanceDTO> data = (List<Religion_allowanceDTO>) recordNavigator.list;

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Religion_allowanceDTO religion_allowanceDTO = data.get(i);


        %>
        <tr>
            <td>
                <%
                    AllowanceEmployeeInfoDTO infoDTO= AllowanceEmployeeInfoRepository.getInstance().getById(religion_allowanceDTO.allowanceEmployeeInfoId);
                %>
                <%=isLanguageEnglish?infoDTO.nameEn :infoDTO.nameBn%>
                <br>
                <%=isLanguageEnglish?"<b>"+infoDTO.organogramNameEn+"</b><br>"+infoDTO.officeNameEn
                        : "<b>"+infoDTO.organogramNameBn+"</b><br>"+infoDTO.officeNameBn%>
            </td>
            <td>
                <%=CatRepository.getInstance().getText(Language,"allowance",religion_allowanceDTO.allowanceCat)%>
            </td>
            <td>
                <%=Utils.getDigits(religion_allowanceDTO.bonusAmount, Language)%>

            </td>

            <td>
                <%=Utils.getDigits(religion_allowanceDTO.taxDeduction, Language)+'%'%>
            </td>



            <%CommonDTO commonDTO = religion_allowanceDTO; %>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=view&ID=<%=commonDTO.iD%>'"
                >
                    <i class="fa fa-eye"></i>
                </button>
            </td>

            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID'
                                                 value='<%=religion_allowanceDTO.iD%>'/></span>
                </div>
            </td>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			