CREATE TABLE leave_reliever_mapping (
ID BIGINT(20) UNSIGNED PRIMARY KEY,
organogram_id BIGINT(20) NOT NULL,
leave_reliever_1 BIGINT(20) NOT NULL,
leave_reliever_2 BIGINT(20) NOT NULL,
insertion_date BIGINT(20) DEFAULT 0,
inserted_by VARCHAR(255) DEFAULT NULL,
modified_by VARCHAR(255) DEFAULT NULL,
isDeleted INT(11) NULL DEFAULT 0,
lastModificationTime BIGINT(20) DEFAULT 0
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

alter table employee_leave_details add employee_name_eng varchar(255) not null;

alter table employee_leave_details add employee_name_bng varchar(512) not null;

alter table employee_leave_details add office_unit_id bigint not null;

alter table employee_leave_details add office_eng varchar(255) not null;

alter table employee_leave_details add office_bng varchar(512) null;

alter table employee_leave_details add organogram_id bigint not null;

alter table employee_leave_details add organogram_eng varchar(255) not null;

alter table employee_leave_details add organogram_bng varchar(512) not null;