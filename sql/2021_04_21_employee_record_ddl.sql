alter table employee_records change emp_Class employee_class_cat int default 0;
update prp.category set domain_name = 'employment' where domain_name='employment_type';
update prp.category set domain_name = 'emp_officer' where domain_name='emp_officer_type';
alter table employee_records change emp_officer_type_cat emp_officer_cat int default 0;
alter table employee_records change gender gender_cat int(10) default 0;
alter table employee_offices alter column organogram_role set default 10401;